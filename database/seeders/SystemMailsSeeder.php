<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SystemMailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //system_mails
        //system_mail_idiomas
        //$id = DB::getPdo()->lastInsertId();

        DB::table('system_mail_idiomas')->delete();
        DB::table('system_mails')->delete();

        $system_mails = [

            [ 'id'=> 1,
            'name' => 'web.curso',
            'trigger'=> 'Web: formulario info curso',
            'destino'=> 0,
            'destino_notas'=> 'Plataforma y configuración por Categorías y Subcategorías',
            'template'=> 'web_curso', //emails.
            'asunto'=> "Solicitud información online web",
            'contenido'=> null
            ],

            [ 'id'=> 2,
                'name' => 'web.catalogo',
                'trigger'=> 'Web: info para descarga de catálogos',
                'destino'=> 0,
                'destino_notas'=> 'Configuración plataforma',
                'template'=> 'web_catalogo',
                'asunto'=> "Solicitud información en Catálogo",
                'contenido'=> null
            ],

            [ 'id'=> 3,
                'name' => 'web.registro',
                'trigger'=> 'Registro Web',
                'destino'=> 0,
                'destino_notas'=> 'Plataforma',
                'template'=> 'web_registro',
                'asunto'=> "Registro Web [tag]VIAJERO_TIPO[/tag]",
                'contenido'=> null
            ],

            [ 'id'=> 4,
                'name' => 'area',
                'trigger'=> 'Área Cliente',
                'destino'=> 3,
                'destino_notas'=> 'Viajero / Tutores',
                'template'=> 'area',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 5,
                'name' => 'viajero.datos',
                'trigger'=> 'Modificación de datos ficha Viajero',
                'destino'=> 0,
                'destino_notas'=> 'Usuario asignado y configuración por Categorías y Subcategorías',
                'template'=> 'viajero_datos',
                'asunto'=> "Datos modificados",
                'contenido'=> null
            ],

            [ 'id'=> 6,
                'name' => 'web.contacto',
                'trigger'=> "Formulario de contacto",
                'destino'=> 0,
                'destino_notas'=> 'Plataforma',
                'template'=> 'web_contacto', //emails.
                'asunto'=> "Solicitud de información",
                'contenido'=> null
            ],

            [ 'id'=> 101,
                'name' => 'booking.aviso',
                'trigger'=> 'Notificación de Booking nuevo',
                'destino'=> 0,
                'destino_notas'=> 'Configuración Plataforma y configuración por Categorías y Subcategorías',
                'template'=> 'booking_aviso',
                'asunto'=> "Nuevo Booking [tag]BOOKING_TIPO[/tag] : [tag]BOOKING_CURSO[/tag]",
                'contenido'=> null
            ],

            [ 'id'=> 102,
                'name' => 'booking.confirmacion',
                'trigger'=> 'Booking confirmado',
                'destino'=> 3,
                'destino_notas'=> 'Viajero y Tutores',
                'template'=> 'booking',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 103,
                'name' => 'booking.tarea',
                'trigger'=> 'Nueva tarea de Booking',
                'destino'=> 0,
                'destino_notas'=> 'Asignado',
                'template'=> 'booking_tarea',
                'asunto'=> 'Tarea nueva',
                'contenido'=> null
            ],

            [ 'id'=> 104,
                'name' => 'viajero.tarea',
                'trigger'=> 'Nueva tarea',
                'destino'=> 0,
                'destino_notas'=> 'Asignado',
                'template'=> 'tarea',
                'asunto'=> 'Tarea nueva',
                'contenido'=> null
            ],

            [ 'id'=> 105,
                'name' => 'booking.incidencia',
                'trigger'=> 'Nueva incidencia de Booking',
                'destino'=> 0,
                'destino_notas'=> 'Asignado',
                'template'=> 'booking_incidencia',
                'asunto'=> 'Incidencia Booking nueva',
                'contenido'=> null
            ],

            [ 'id'=> 106,
                'name' => 'booking.cancelar',
                'trigger'=> 'Cancelación de Booking',
                'destino'=> 0,
                'destino_notas'=> 'Asignado y y configuración por Categorías',
                'template'=> 'booking_cancelar',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 107,
                'name' => 'booking.ovbkg',
                'trigger'=> 'Booking con Ovbkg',
                'destino'=> 0,
                'destino_notas'=> 'Configuración Plataforma',
                'template'=> 'booking_overbooking',
                'asunto'=> 'New Ovbkg en Convocatoria [tag]BOOKING_CONVOCATORIA[/tag]',
                'contenido'=> null
            ],

            [ 'id'=> 108,
                'name' => 'booking.importes',
                'trigger'=> 'Modificación de los importes de Booking',
                'destino'=> 0,
                'destino_notas'=> 'Configuración Plataforma',
                'template'=> 'booking_cambio_importes',
                'asunto'=> 'Cambio Importes Booking [tag]BOOKING_ID[/tag]',
                'contenido'=> null
            ],

            [ 'id'=> 109,
                'name' => 'booking.pago',
                'trigger'=> 'Nuevo pago de Booking',
                'destino'=> 3,
                'destino_notas'=> 'Viajero (sólo si NO es menor) y Tutores',
                'template'=> 'booking_pago',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 110,
                'name' => 'booking.nota_pago',
                'trigger'=> 'Solicitud de segundo pago de Booking (Aviso)',
                'destino'=> 3,
                'destino_notas'=> 'Viajero y Tutores',
                'template'=> 'booking_notapago',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 111,
                'name' => 'booking.vuelo',
                'trigger'=> 'Cambio en vuelo de Booking',
                'destino'=> 0,
                'destino_notas'=> 'Configuración Plataforma',
                'template'=> 'booking_vuelo',
                'asunto'=> 'Cambio Vuelo en Booking [tag]BOOKING_ID[/tag] de [tag]BOOKING_VIAJERO[/tag]',
                'contenido'=> null
            ],

            [ 'id'=> 112,
                'name' => 'booking.vuelo_billetes',
                'trigger'=> 'Modificación de datos de Booking que afecta a los billetes de vuelo',
                'destino'=> 0,
                'destino_notas'=> 'Configuración Plataforma',
                'template'=> 'booking_billetes',
                'asunto'=> 'Cambio datos para billetes de [tag]BOOKING_VUELO[/tag]',
                'contenido'=> null
            ],

            [ 'id'=> 113,
                'name' => 'booking.cuestionario',
                'trigger'=> 'Recordatorio de cuestionarios de Booking',
                'destino'=> 3,
                'destino_notas'=> 'Viajero y Tutores',
                'template'=> 'booking_cuestionario',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 114,
                'name' => 'booking.doc_especifico',
                'trigger'=> 'Viajero/Tutor envía Documento específico',
                'destino'=> 0,
                'destino_notas'=> 'Asignado y configuración por Categorías y Subcategorías',
                'template'=> 'booking_doc_especifico',
                'asunto'=> 'Documento específico área cliente nuevo',
                'contenido'=> null
            ],

            [ 'id'=> 115,
                'name' => 'booking.doc_especifico_rechazado',
                'trigger'=> 'Documento específico rechazado',
                'destino'=> 3,
                'destino_notas'=> 'Viajero y Tutores',
                'template'=> 'booking_doc_especifico',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 116,
                'name' => 'booking.factura',
                'trigger'=> 'Factura de Booking',
                'destino'=> 3,
                'destino_notas'=> 'Viajero y Tutores',
                'template'=> 'booking_factura',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 117,
                'name' => 'booking.firma_pendiente',
                'trigger'=> 'Solicitud de firma digital de Booking',
                'destino'=> 3,
                'destino_notas'=> 'Viajero (sólo si NO es menor) y Tutores',
                'template'=> 'booking_firma_pendiente',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 118,
                'name' => 'booking.firma',
                'trigger'=> 'Volver a solicitar firma digital de Booking por cambios',
                'destino'=> 3,
                'destino_notas'=> 'Viajero (sólo si NO es menor) y Tutores',
                'template'=> 'booking_firma',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 119,
                'name' => 'booking.online',
                'trigger'=> 'Notificació de pre-inscripción en Booking',
                'destino'=> 3,
                'destino_notas'=> 'Viajero y Tutores',
                'template'=> 'booking_online',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 120,
                'name' => 'booking.reunion',
                'trigger'=> 'Notificació reunión de Booking',
                'destino'=> 3,
                'destino_notas'=> 'Viajero y Tutores',
                'template'=> 'booking_reunion',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 121,
                'name' => 'booking.caducar1_viajero',
                'trigger'=> 'Primer aviso de pre-reserva de Booking a punto de caducar a Viajero',
                'destino'=> 1,
                'destino_notas'=> 'Viajero',
                'template'=> 'booking_caduca_aviso1v',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 122,
                'name' => 'booking.caducar1_tutores',
                'trigger'=> 'Primer aviso de pre-reserva de Booking a punto de caducar a Tutores',
                'destino'=> 2,
                'destino_notas'=> 'Tutores',
                'template'=> 'booking_caduca_aviso1t',
                'asunto'=> null,
                'contenido'=> null
            ],

            [ 'id'=> 123,
                'name' => 'booking.caducar2',
                'trigger'=> 'Segundo aviso (1 día antes) de pre-reserva de Booking a punto de caducar a Asignado',
                'destino'=> 0,
                'destino_notas'=> 'Asignado',
                'template'=> 'booking_caduca_aviso2',
                'asunto'=> 'Pre-reserva [tag]BOOKING_VIAJERO[/tag] caduca mañana',
                'contenido'=> null
            ],

        ];

        foreach($system_mails as $m)
        {
            DB::table('system_mails')->insert($m);
        }

        $system_mail_idiomas = [

            [
                'mail_id' => 4,
                'idioma'=> 'es',
                'asunto'=> "Acceso Área Cliente",
                'contenido'=> null
            ],
            [
                'mail_id' => 4,
                'idioma'=> 'ca',
                'asunto'=> "Accés Àrea Client",
                'contenido'=> null
            ],

            [
                'mail_id' => 102,
                'idioma'=> 'es',
                'asunto'=> "Confirmación de vuestra inscripción con [tag]PLATAFORMA[/tag]",
                'contenido'=> null
            ],
            [
                'mail_id' => 102,
                'idioma'=> 'ca',
                'asunto'=> "Confirmació de la vostra inscripció amb [tag]PLATAFORMA[/tag]",
                'contenido'=> null
            ],

            [
                'mail_id' => 106,
                'idioma'=> 'es',
                'asunto'=> "Cancelación [tag]BOOKING_VIAJERO[/tag]",
                'contenido'=> null
            ],
            [
                'mail_id' => 106,
                'idioma'=> 'ca',
                'asunto'=> "Cancel·lació [tag]BOOKING_VIAJERO[/tag]",
                'contenido'=> null
            ],

            [
                'mail_id' => 109,
                'idioma'=> 'es',
                'asunto'=> "Pago recibido",
                'contenido'=> null
            ],
            [
                'mail_id' => 109,
                'idioma'=> 'ca',
                'asunto'=> "Pagament rebut",
                'contenido'=> null
            ],

            [
                'mail_id' => 110,
                'idioma'=> 'es',
                'asunto'=> "Segundo pago",
                'contenido'=> null
            ],
            [
                'mail_id' => 110,
                'idioma'=> 'ca',
                'asunto'=> "Segon pagament",
                'contenido'=> null
            ],

            [
                'mail_id' => 113,
                'idioma'=> 'es',
                'asunto'=> "Recordatorio cuestionario Inscripción",
                'contenido'=> null
            ],
            [
                'mail_id' => 113,
                'idioma'=> 'ca',
                'asunto'=> "Recordatori qüestionari Inscripció",
                'contenido'=> null
            ],

            [
                'mail_id' => 115,
                'idioma'=> 'es',
                'asunto'=> "Aviso de documento rechazado",
                'contenido'=> null
            ],
            [
                'mail_id' => 115,
                'idioma'=> 'ca',
                'asunto'=> "Avís de document rebutjat",
                'contenido'=> null
            ],

            [
                'mail_id' => 116,
                'idioma'=> 'es',
                //'asunto'=> "Factura [tag]FACTURA_NUM[/tag]",
                'asunto'=> "Factura",
                'contenido'=> null
            ],
            [
                'mail_id' => 116,
                'idioma'=> 'ca',
                //'asunto'=> "Factura [tag]FACTURA_NUM[/tag]",
                'asunto'=> "Factura",
                'contenido'=> null
            ],

            [
                'mail_id' => 117,
                'idioma'=> 'es',
                'asunto'=> "Solicitud de firma digital de vuestra inscripción con [tag]PLATAFORMA[/tag]",
                'contenido'=> null
            ],
            [
                'mail_id' => 117,
                'idioma'=> 'ca',
                'asunto'=> "Sol·licitud de signatura digital de la vostra inscripció amb [tag]PLATAFORMA[/tag]",
                'contenido'=> null
            ],

            [
                'mail_id' => 118,
                'idioma'=> 'es',
                'asunto'=> "Solicitud de firma digital de vuestra inscripción con [tag]PLATAFORMA[/tag]",
                'contenido'=> null
            ],
            [
                'mail_id' => 118,
                'idioma'=> 'ca',
                'asunto'=> "Sol·licitud de signatura digital de la vostra inscripció amb [tag]PLATAFORMA[/tag]",
                'contenido'=> null
            ],

            [
                'mail_id' => 119,
                'idioma'=> 'es',
                'asunto'=> "Pre-inscripción en el programa [tag]BOOKING_CURSO[/tag]",
                'contenido'=> null
            ],
            [
                'mail_id' => 119,
                'idioma'=> 'ca',
                'asunto'=> "Pre-inscripció en el programa [tag]BOOKING_CURSO[/tag]",
                'contenido'=> null
            ],

            [
                'mail_id' => 120,
                'idioma'=> 'es',
                'asunto'=> "Sesión informativa [tag]PLATAFORMA[/tag]",
                'contenido'=> null
            ],
            [
                'mail_id' => 120,
                'idioma'=> 'ca',
                'asunto'=> "Sessió informativa [tag]PLATAFORMA[/tag]",
                'contenido'=> null
            ],

            [
                'mail_id' => 121,
                'idioma'=> 'es',
                'asunto'=> "Pre-reserva en [tag]BOOKING_CURSO[/tag] a punto de caducar",
                'contenido'=> null
            ],
            [
                'mail_id' => 121,
                'idioma'=> 'ca',
                'asunto'=> "Pre-reserva a [tag]BOOKING_CURSO[/tag] a punt de caducar",
                'contenido'=> null
            ],

            [
                'mail_id' => 122,
                'idioma'=> 'es',
                'asunto'=> "Pre-reserva en [tag]BOOKING_CURSO[/tag] a punto de caducar",
                'contenido'=> null
            ],
            [
                'mail_id' => 122,
                'idioma'=> 'ca',
                'asunto'=> "Pre-reserva a [tag]BOOKING_CURSO[/tag] a punt de caducar",
                'contenido'=> null
            ],

        ];

        foreach($system_mail_idiomas as $m)
        {
            DB::table('system_mail_idiomas')->insert($m);
        }

    }
}
