<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use VCN\Models\Exams\{Examen, Pregunta, Respuesta};

class ExamsIngles1 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('examenes')->where('id', 1)->delete();
        // DB::table('examen_preguntas')->where('examen_id', 1)->delete();
        // DB::table('examen_respuestas')->where('examen_id', 1)->delete();

        Examen::where('id', 1)->delete();
        Pregunta::where('examen_id', 1)->delete();
        Respuesta::where('examen_id', 1)->delete();

        $examenes = [
            [
                'id' => 1,
                'name' => 'Test Inglés v.1',
                'title' => 'Test Inglés',
                'template'=> 'test-ingles-1',
                'bloques' => 4,
            ]
        ];
        
        foreach($examenes as $m)
        {
            Examen::create($m);
        }

        $opcionesP2 = ['make','catch','take','keep'];
        $opcionesP3 = ['until','spoke','been','behind','out','through','taking','panic','caught','talking','worried','banana','for','to','gone','on','at','without','taken','and'];

        $preguntas = [
            // --- BLOQUE 1 ---
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 0,
                'pregunta'  => 'High Schools International work with schools **** Australia, England and the USA',
                'opciones'  => ['at','from','in','by'],
                'respuesta' => 2,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 1,
                'pregunta'  => 'English people **** tea every day.',
                'opciones'  => ['drinks','drinking','drink','are drinking'],
                'respuesta' => 2,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 2,
                'pregunta'  => 'The USA is **** England.',
                'opciones'  => ['the biggest','bigger than','big enough','bigger'],
                'respuesta' => 1,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 3,
                'pregunta'  => '**** you like some more cake?',
                'opciones'  => ['Will','Are','Do','Would'],
                'respuesta' => 3,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 4,
                'pregunta'  => 'Have you ever **** to Australia?',
                'opciones'  => ['been','went','gone','go'],
                'respuesta' => 0,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 5,
                'pregunta'  => 'Excuse me, could you **** me the way to the bank?',
                'opciones'  => ['say','talk','say to','tell'],
                'respuesta' => 3,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 6,
                'pregunta'  => 'Did you **** the film on TV last night?',
                'opciones'  => ['watched','seen','see','saw'],
                'respuesta' => 2,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 7,
                'pregunta'  => 'What **** to do this weekend?',
                'opciones'  => ['are you going','will you','must go','are you'],
                'respuesta' => 0,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 8,
                'pregunta'  => 'They fell in love while they **** in England.',
                'opciones'  => ['was studying','are studying','were studying','have been studying'],
                'respuesta' => 2,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 9,
                'pregunta'  => 'I´ve been learning English **** I was twelve years old.',
                'opciones'  => ['for','until','before','since'],
                'respuesta' => 3,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 10,
                'pregunta'  => 'If you don´t have a good dictionary, you **** be able to learn English well.',
                'opciones'  => ['will','musn´t','must','won´t'],
                'respuesta' => 3,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 11,
                'pregunta'  => 'Shops in England close at 5.30, ****?',
                'opciones'  => ['aren´t','don´t they','no','are they'],
                'respuesta' => 1,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 12,
                'pregunta'  => 'If I´d known about the party last night, **** gone.',
                'opciones'  => ['I´d','I´d have','I´ll have','I would'],
                'respuesta' => 1,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 13,
                'pregunta'  => '**** he didn´t revise, he failed the exam.',
                'opciones'  => ['Although','However','Despite','Because'],
                'respuesta' => 3,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 14,
                'pregunta'  => 'I´ve been staying in England for six months but I just haven´t **** to the weather.',
                'opciones'  => ['used','be used','get used','got used'],
                'respuesta' => 3,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 15,
                'pregunta'  => 'By this time next week, Teresa **** her English exam.',
                'opciones'  => ['will take','will have taken','will have been taking','is going to take'],
                'respuesta' => 1,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 16,
                'pregunta'  => 'I´ve been working all day, and now I´m absolutely worn ****.',
                'opciones'  => ['out','up','over','in'],
                'respuesta' => 0,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 17,
                'pregunta'  => 'The house looks very old. It **** built at least a hundred years ago.',
                'opciones'  => ['must be','must have','must have been','can´t have been'],
                'respuesta' => 2,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 18,
                'pregunta'  => 'He´s **** old enough to be here on his own, let alone travel by himself.',
                'opciones'  => ['only','hardly','solely','almost'],
                'respuesta' => 1,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 19,
                'pregunta'  => 'There´s a depression coming so it´s **** to rain tomorrow.',
                'opciones'  => ['possibly','bound','surely','certainly'],
                'respuesta' => 1,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 1,
                'numero'    => 20,
                'pregunta'  => '**** I´d had the time, I wouldn´t have done my homework.',
                'opciones'  => ['Only if','Unless','Even if','Although'],
                'respuesta' => 2,
            ],
            // --- BLOQUE 2 ---
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 21,
                'pregunta'  => '**** a photo',
                'opciones'  => $opcionesP2,
                'respuesta' => 2,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 22,
                'pregunta'  => '**** a noise',
                'opciones'  => $opcionesP2,
                'respuesta' => 0,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 23,
                'pregunta'  => '**** the sun',
                'opciones'  => $opcionesP2,
                'respuesta' => 1,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 24,
                'pregunta'  => '**** some medicine',
                'opciones'  => $opcionesP2,
                'respuesta' => 2,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 25,
                'pregunta'  => '**** a cold',
                'opciones'  => $opcionesP2,
                'respuesta' => 1,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 26,
                'pregunta'  => '**** a secret',
                'opciones'  => $opcionesP2,
                'respuesta' => 3,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 27,
                'pregunta'  => '**** a diary',
                'opciones'  => $opcionesP2,
                'respuesta' => 3,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 28,
                'pregunta'  => '**** a thief',
                'opciones'  => $opcionesP2,
                'respuesta' => 1,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 29,
                'pregunta'  => '**** an excuse',
                'opciones'  => $opcionesP2,
                'respuesta' => 0,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 30,
                'pregunta'  => '**** fire',
                'opciones'  => $opcionesP2,
                'respuesta' => 0,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 31,
                'pregunta'  => '**** someone´s eye',
                'opciones'  => $opcionesP2,
                'respuesta' => 1,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 32,
                'pregunta'  => '**** up for it',
                'opciones'  => $opcionesP2,
                'respuesta' => 0,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 33,
                'pregunta'  => '**** a good impression',
                'opciones'  => $opcionesP2,
                'respuesta' => 0,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 34,
                'pregunta'  => '**** the money and run',
                'opciones'  => $opcionesP2,
                'respuesta' => 2,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 2,
                'numero'    => 35,
                'pregunta'  => '**** watch',
                'opciones'  => $opcionesP2,
                'respuesta' => 3,
            ],
            // --- BLOQUE 3.1 ---
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 0,
                'pregunta'  => '1',
                'opciones'  => $opcionesP3,
                'respuesta' => 13,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 2,
                'pregunta'  => '2',
                'opciones'  => $opcionesP3,
                'respuesta' => 5,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 3,
                'pregunta'  => '3',
                'opciones'  => $opcionesP3,
                'respuesta' => 16,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 4,
                'pregunta'  => '4',
                'opciones'  => $opcionesP3,
                'respuesta' => 4,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 5,
                'pregunta'  => '5',
                'opciones'  => $opcionesP3,
                'respuesta' => 1,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 6,
                'pregunta'  => '6',
                'opciones'  => $opcionesP3,
                'respuesta' => 9,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 7,
                'pregunta'  => '7',
                'opciones'  => $opcionesP3,
                'respuesta' => 3,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 8,
                'pregunta'  => '8',
                'opciones'  => $opcionesP3,
                'respuesta' => 8,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 9,
                'pregunta'  => '9',
                'opciones'  => $opcionesP3,
                'respuesta' => 11,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 10,
                'pregunta'  => '10',
                'opciones'  => $opcionesP3,
                'respuesta' => 19,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 11,
                'pregunta'  => '11',
                'opciones'  => $opcionesP3,
                'respuesta' => 10,
            ],
            // --- BLOQUE 3.2 ---
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 12,
                'pregunta'  => 'How many important zoos are there in Australia?',
                'opciones'  => ['four','many','three','one'],
                'respuesta' => 0,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 13,
                'pregunta'  => 'Melbourne Zoo has been in the same place',
                'opciones'  => ['since 1857','for 100 years','for more than 100 years','since it was first established'],
                'respuesta' => 2,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 14,
                'pregunta'  => 'The animals at Melbourne Zoo',
                'opciones'  => ['live in cages','wander around the zoo freely','live in places similar to their natural habitat','were all bred in the zoo'],
                'respuesta' => 2,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 15,
                'pregunta'  => 'Melbourne Zoo',
                'opciones'  => ['is the oldest zoo in the world','was established in 1862','is outside Melbourne','is the oldest zoo in Australia'],
                'respuesta' => 3,
            ],
            [
                'examen_id' => 1,
                'bloque'    => 3,
                'numero'    => 16,
                'pregunta'  => 'Which statement best describes the role of modern zoos?',
                'opciones'  => ['Zoos entertain people','Zoos educate people about breeding animals','Zoos have three important roles','Zoos protect animals and their habitat'],
                'respuesta' => 3,
            ],
            // --- BLOQUE 4 ---
            [
                'examen_id' => 1,
                'bloque'    => 4,
                'numero'    => 0,
                'pregunta'  => 'What are the advantages and disadvantages of studying abroad?',
                'opciones'  => [],
                'respuesta' => "Write your answer below. Write 200 - 250 words. Time: 30 minutes",
            ],
        ];

        foreach($preguntas as $m)
        {
            Pregunta::create($m);
        }        
    }
}
