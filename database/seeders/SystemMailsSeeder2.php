<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SystemMailsSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $system_mails = [

            [   
                'id'=> 7,
                'name' => 'system.password',
                'trigger'=> 'Recuperación de contraseña',
                'destino'=> 0,
                'destino_notas'=> '',
                'template'=> 'system_password', //emails.
                'asunto'=> "Recuperación de contraseña",
                'contenido'=> null
            ],
        ];

        foreach($system_mails as $m)
        {
            DB::table('system_mails')->insert($m);
        }

    }
}
