<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PatchUtf8mb8mb4Unicode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('ALTER TABLE `viajero_logs` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');
        DB::unprepared('ALTER TABLE `booking_logs` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');
        DB::unprepared('ALTER TABLE `tutor_logs` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');
        DB::unprepared('ALTER TABLE `booking_incidencia_logs` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');
        DB::unprepared('ALTER TABLE `reunion_logs` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');
        DB::unprepared('ALTER TABLE `system_logs` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('ALTER TABLE `viajero_logs` CONVERT TO CHARACTER SET utf8');
        DB::unprepared('ALTER TABLE `booking_logs` CONVERT TO CHARACTER SET utf8');
        DB::unprepared('ALTER TABLE `tutor_logs` CONVERT TO CHARACTER SET utf8');
        DB::unprepared('ALTER TABLE `booking_incidencia_logs` CONVERT TO CHARACTER SET utf8');
        DB::unprepared('ALTER TABLE `reunion_logs` CONVERT TO CHARACTER SET utf8');
        DB::unprepared('ALTER TABLE `system_logs` CONVERT TO CHARACTER SET utf8');
    }
}
