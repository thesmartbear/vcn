<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposParaPrivado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especialidades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create('subespecialidades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();

            $table->integer('especialidad_id')->unsigned();

            $table->foreign('especialidad_id')
                ->references('id')->on('especialidades')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->tinyInteger('propietario')->default(0);
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->tinyInteger('propietario')->default(0);

            $table->integer('especialidad_id')->unsigned()->nullable();
            $table->string('subespecialidad_id')->nullable();
        });

        Schema::table('cms_paginas', function (Blueprint $table) {
            $table->tinyInteger('propietario')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_paginas', function (Blueprint $table) {
            $table->dropColumn('propietario');
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('propietario');
            $table->dropColumn('especialidad_id');
            $table->dropColumn('subespecialidad_id');
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('propietario');
        });

        Schema::drop('subespecialidades');
        Schema::drop('especialidades');
    }
}
