<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Output\ConsoleOutput;


class AddOrigienesToSolicitudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->integer('origen_id')->unsigned()->nullable();
            $table->integer('suborigen_id')->unsigned()->nullable();
            $table->integer('suborigendet_id')->unsigned()->nullable();
        });

        $output = new ConsoleOutput();

        $output->writeln('Actualizando Solicitudes desde Viajero...');
        $i = 0;
        \VCN\Models\Solicitudes\Solicitud::chunk(500, function($solicitudes) use (&$i, $output){
            $j = 0;
            foreach($solicitudes as $solicitud)
            {
                $solicitud->origen_id = $solicitud->viajero->origen_id ?: 0;
                $solicitud->suborigen_id = $solicitud->viajero->suborigen_id ?: 0;
                $solicitud->suborigendet_id = $solicitud->viajero->suborigendet_id ?: 0;
                $solicitud->save();

                $i++;
                $j++;
            }
            $output->writeln("SubTotal: $j / $i");
        });

        $output->writeln("Total: $i");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->dropColumn('origen_id');
            $table->dropColumn('suborigen_id');
            $table->dropColumn('suborigendet_id');
        });
    }
}
