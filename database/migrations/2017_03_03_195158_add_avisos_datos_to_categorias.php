<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvisosDatosToCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->text('avisos_datos')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->text('avisos_datos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('avisos_datos');
        });

        Schema::table('subbcategorias', function (Blueprint $table) {
            $table->dropColumn('avisos_datos');
        });
    }
}
