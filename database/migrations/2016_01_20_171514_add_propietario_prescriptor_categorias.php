<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPropietarioPrescriptorCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prescriptor_categorias', function (Blueprint $table) {
            $table->tinyInteger('propietario')->default(0);
        });

        Schema::table('prescriptor_comisiones_cat', function (Blueprint $table) {
            $table->tinyInteger('propietario')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prescriptor_categorias', function (Blueprint $table) {
            $table->dropColumn('propietario');
        });

        Schema::table('prescriptor_comisiones_cat', function (Blueprint $table) {
            $table->dropColumn('propietario');
        });
    }
}
