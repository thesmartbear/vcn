<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingMonedas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_monedas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('booking_id')->unsigned();
            $table->integer('moneda_id')->unsigned();
            $table->decimal('rate', 10)->default(1);

            $table->foreign('booking_id')
                ->references('id')->on('bookings')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_monedas');
    }
}
