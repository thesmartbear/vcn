<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMasCamposViajero extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->string('escuela')->nullable();
            $table->boolean('ingles')->default(0);
            $table->string('ingles_academia')->nullable();
            $table->text('hermanos')->nullable();;
            $table->string('telefonos')->nullable();;
            $table->string('idioma_contacto',2);
        });

        Schema::table('tutores', function (Blueprint $table) {
            $table->string('nif')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutores', function (Blueprint $table) {
            $table->dropColumn('nif');
        });

        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->dropColumn('escuela');
            $table->dropColumn('ingles');
            $table->dropColumn('ingles_academia');
            $table->dropColumn('hermanos');
            $table->dropColumn('telefonos');
            $table->dropColumn('idioma_contacto');
        });
    }
}
