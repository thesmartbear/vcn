<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMonedaToPago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_pagos', function (Blueprint $table) {
            $table->integer('moneda_id')->unsigned();
            $table->decimal('rate', 10)->default(1);
            $table->renameColumn('importe', 'importe_pago');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_pagos', function (Blueprint $table) {
            $table->dropColumn('moneda_id');
            $table->dropColumn('rate');
            $table->renameColumn('importe_pago', 'importe');
        });
    }
}
