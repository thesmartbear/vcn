<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvocatoriaAbiertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocatoria_abiertas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('convocatory_open_name')->nullable();
            $table->boolean('convocatory_open_start_day')->nullable();
            $table->dateTime('convocatory_open_valid_start_date')->nullable();
            $table->dateTime('convocatory_open_valid_end_date')->nullable();
            $table->boolean('convocatory_open_status')->nullable();

            $table->integer('course_id')->unsigned();
            $table->integer('convocatory_open_currency_id')->unsigned()->nullable();

            $table->text('convocatory_open_price_include')->nullable();

            $table->foreign('course_id')
                ->references('id')->on('cursos')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('convocatory_open_currency_id')
                ->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('convocatoria_abiertas');
    }
}
