<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('viajero_id')->unsigned();
            $table->integer('curso_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->boolean('fase')->default(0);

            $table->integer('weeks')->nullable();

            $table->decimal('total', 10)->nullable();
            $table->string('order_number');
            $table->date('pago_fecha')->nullable();

            $table->integer('contacto_request_id')->unsigned();
            $table->text('quota_ids', 65535)->nullable();

            //Convocatoria
            $table->integer('convocatory_open_id')->unsigned();
            $table->decimal('convocatory_open_price', 10);
            $table->integer('convocatory_close_id')->unsigned();
            $table->decimal('convocatory_close_price', 10);

            //Alojamiento
            $table->integer('accommodation_id')->unsigned();
            $table->decimal('accommodation_price', 10);
            $table->integer('accommodation_currency_id')->unsigned();
            $table->date('accommodation_start_date')->nullable();
            $table->date('accommodation_end_date')->nullable();
            $table->decimal('accommodation_total_amount', 10)->nullable();
            $table->integer('accommodation_weeks')->nullable();

            //Descuento
            $table->decimal('center_discount_amount', 10)->nullable();
            $table->integer('center_discount_percent')->nullable();

            //Curso
            $table->date('course_start_date')->nullable();
            $table->date('course_end_date')->nullable();
            $table->decimal('course_price', 10); //por semana
            $table->integer('course_currency_id')->unsigned();
            $table->decimal('course_total_amount', 10)->nullable();

            $table->foreign('viajero_id')
                ->references('id')->on('viajeros')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('curso_id')
                ->references('id')->on('cursos')
                ->onDelete('cascade')->onUpdate('cascade');


            // $table->string('Ds_Date')->nullable();
            // $table->string('Ds_Hour')->nullable();
            // $table->string('Ds_Amount')->nullable();
            // $table->string('Ds_Currency')->nullable();
            // $table->string('Ds_Order')->nullable();
            // $table->string('Ds_MerchantCode')->nullable();
            // $table->string('Ds_Terminal')->nullable();
            // $table->string('Ds_Signature')->nullable();
            // $table->string('Ds_Response')->nullable();
            // $table->string('Ds_TransactionType')->nullable();
            // $table->string('Ds_SecurePayment')->nullable();
            // $table->string('Ds_MerchantData')->nullable();
            // $table->string('Ds_Card_Country')->nullable();
            // $table->string('Ds_AuthorisationCode')->nullable();
            // $table->string('Ds_ConsumerLanguage')->nullable();
            // $table->string('Ds_Card_Type')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }
}
