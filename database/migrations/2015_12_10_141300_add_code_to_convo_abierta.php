<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeToConvoAbierta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->string('convocatory_open_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->dropColumn('convocatory_open_code');
        });
    }
}
