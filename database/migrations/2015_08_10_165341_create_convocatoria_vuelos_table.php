<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvocatoriaVuelosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocatoria_vuelos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('convocatory_flight_name')->nullable();
            $table->text('convocatory_flight_info', 65535)->nullable();
            $table->decimal('convocatory_flight_cost', 10)->nullable();
            $table->boolean('convocatory_flight_seats')->nullable();

            $table->integer('convocatory_id')->unsigned();

            $table->integer('plazas')->nullable();
            $table->integer('plazas_vuelo')->nullable();

            //ida
            $table->string('ida_company')->nullable();
            $table->string('ida_vuelo')->nullable();
            $table->string('ida_salida_aeropuerto')->nullable();
            $table->date('ida_salida_fecha')->nullable();
            $table->time('ida_salida_hora')->nullable();
            $table->string('ida_llegada_aeropuerto')->nullable();
            $table->date('ida_llegada_fecha')->nullable();
            $table->time('ida_llegada_hora')->nullable();

            //vuelta
            $table->string('vuelta_company')->nullable();
            $table->string('vuelta_vuelo')->nullable();
            $table->string('vuelta_salida_aeropuerto')->nullable();
            $table->date('vuelta_salida_fecha')->nullable();
            $table->time('vuelta_salida_hora')->nullable();
            $table->string('vuelta_llegada_aeropuerto')->nullable();
            $table->date('vuelta_llegada_fecha')->nullable();
            $table->time('vuelta_llegada_hora')->nullable();

            $table->foreign('convocatory_id')
                ->references('id')->on('convocatoria_cerradas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('convocatoria_vuelos');
    }
}
