<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajeroTareasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajero_tareas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('viajero_id')->unsigned();

            $table->dateTime('fecha')->nullable();
            $table->string('tipo')->nullable();
            $table->text('notas')->nullable();

            $table->dateTime('aviso_fecha')->nullable();
            $table->tinyInteger('aviso')->nullable();

            $table->integer('user_id')->unsigned(); //quien la crea
            $table->integer('asign_to')->unsigned()->nullable(); //asignado a

            $table->boolean('estado')->default(0);
            $table->dateTime('estado_fecha')->nullable();
            $table->integer('estado_user_id')->unsigned(); //quien lo cambia

            $table->foreign('viajero_id')
                ->references('id')->on('viajeros')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('viajero_tareas');
    }
}
