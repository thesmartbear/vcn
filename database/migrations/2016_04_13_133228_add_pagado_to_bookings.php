<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPagadoToBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('pagado')->default(0);
            $table->date('pagado_fecha')->nullable();
            $table->decimal('pagado_total',10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('pagado');
            $table->dropColumn('pagado_fecha');
            $table->dropColumn('pagado_total');
        });
    }
}
