<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposToOficinas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provincias', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->integer('pais_id')->unsigned();

            $table->foreign('pais_id')
                ->references('id')->on('paises')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('ciudades', function (Blueprint $table) {
            $table->integer('provincia_id')->unsigned();
        });

        Schema::table('oficinas', function (Blueprint $table) {
            $table->string('direccion')->nullable();
            $table->string('cp',5)->nullable();
            $table->string('poblacion',50)->nullable();
            $table->string('persona')->nullable();
            $table->string('telefono',12)->nullable();
            $table->string('email')->nullable();
            $table->integer('provincia_id')->unsigned()->nullable();
            $table->integer('pais_id')->unsigned()->nullable();

            $table->string('banco',25)->nullable();
            $table->string('iban',24)->nullable();

        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('oficina_id')->unsigned()->default(0);
        });

        Schema::table('viajeros', function (Blueprint $table) {
            $table->integer('oficina_id')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oficinas', function (Blueprint $table) {
            $table->dropColumn('direccion');
            $table->dropColumn('cp');
            $table->dropColumn('poblacion');
            $table->dropColumn('persona');
            $table->dropColumn('telefono');
            $table->dropColumn('email');
            $table->dropColumn('provincia_id');
            $table->dropColumn('pais_id');
            $table->dropColumn('banco');
            $table->dropColumn('iban');
        });

        Schema::table('ciudades', function (Blueprint $table) {
            $table->dropColumn('provincia_id');
        });

        Schema::drop('provincias');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('oficina_id');
        });

        Schema::table('viajeros', function (Blueprint $table) {
            $table->dropColumn('oficina_id');
        });
    }
}
