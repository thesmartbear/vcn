<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamenVinculadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examen_vinculados', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->bigInteger('examen_id')->unsigned();

            $table->string('modelo');
            $table->integer('modelo_id')->unsigned();

            $table->foreign('examen_id')
                ->references('id')->on('examenes')
                ->onDelete('cascade');
        });

        Schema::table('examenes', function (Blueprint $table) {
            $table->tinyInteger('propietario')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('examenes', function (Blueprint $table) {
            $table->dropColumn('propietario');
        });

        Schema::dropIfExists('examen_vinculados');
    }
}
