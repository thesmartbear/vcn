<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NuevaHome2020 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->boolean('home_bloque')->default(0);
            $table->boolean('home_bloque_height')->default(0);
            $table->boolean('home_bloque_slide')->default(0);
        });

        Schema::table('cms_paginas', function (Blueprint $table) {
            $table->integer('page_id')->unsigned()->nullable(); //pagina padre
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->dropColumn(['home_bloque', 'home_bloque_height', 'home_bloque_slide']);
        });

        Schema::table('cms_paginas', function (Blueprint $table) {
            $table->dropColumn('page_id');
        });
    }
}
