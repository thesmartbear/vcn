<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centros', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('provider_id')->unsigned();

            $table->string('name')->nullable();

            $table->integer('country_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->integer('currency_id')->unsigned();

            $table->decimal('commission', 4)->nullable();
            $table->text('settingup')->nullable();
            $table->text('foods')->nullable();
            $table->text('transport')->nullable();
            $table->boolean('internet')->nullable();
            $table->text('comment')->nullable();
            $table->text('description')->nullable();
            $table->text('address')->nullable();
            $table->text('center_images')->nullable();
            $table->text('center_activities')->nullable();
            $table->text('center_excursions')->nullable();
            $table->text('center_video')->nullable();
            $table->string('center_image_portada')->nullable();

            $table->foreign('provider_id')
                ->references('id')->on('proveedores')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('country_id')
                ->references('id')->on('paises')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('city_id')
                ->references('id')->on('ciudades')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('currency_id')
                ->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('centros');
    }
}
