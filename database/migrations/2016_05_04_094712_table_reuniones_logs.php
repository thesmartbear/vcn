<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableReunionesLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reunion_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('reunion_id')->unsigned();

            $table->text('notas')->nullable();
            $table->json('bookings')->nullable();

            $table->foreign('reunion_id')
                ->references('id')->on('reuniones')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reunion_logs');
    }
}
