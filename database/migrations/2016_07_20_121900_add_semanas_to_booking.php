<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSemanasToBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('semanas')->nullable();
        });

        //Actualizamos el valor
        foreach( \VCN\Models\Bookings\Booking::all() as $booking )
        {
            $booking->semanas = $booking->semanas_unit;
            $booking->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('semanas');
        });
    }
}
