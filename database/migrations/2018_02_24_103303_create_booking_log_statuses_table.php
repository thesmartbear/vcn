<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingLogStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_log_status', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('booking_id')->unsigned();
            $table->integer('status1')->unsigned();
            $table->integer('status2')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('notas')->nullable();

            $table->foreign('booking_id')
                ->references('id')->on('bookings')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_log_status');
    }
}
