<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeviceToUserActividad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_actividad', function (Blueprint $table) {
            $table->time('desktop_hora_ini');
            $table->time('desktop_hora_fin');
            $table->time('mobile_hora_ini');
            $table->time('mobile_hora_fin');
            $table->time('tablet_hora_ini');
            $table->time('tablet_hora_fin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_actividad', function (Blueprint $table) {
            $table->dropColumn(['desktop_hora_ini','desktop_hora_fin','mobile_hora_ini','mobile_hora_fin','tablet_hora_ini','tablet_hora_fin']);
        });
    }
}
