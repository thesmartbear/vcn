<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CambioBookingPagosImporte extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_pagos', function (Blueprint $table) {
            $table->decimal('importe',10);
        });

        foreach(\VCN\Models\Bookings\BookingPago::all() as $pago)
        {
            $pago->importe = $pago->importe_pago * $pago->rate;
            $pago->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_pagos', function (Blueprint $table) {
            $table->dropColumn('importe');
        });
    }
}
