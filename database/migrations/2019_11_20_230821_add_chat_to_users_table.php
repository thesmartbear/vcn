<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use VCN\Models\User;
use Illuminate\Support\Str;

class AddChatToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            
            $table->boolean('es_guest')->default(0);
            $table->integer('last_activity')->default(0);
            $table->integer('chat_id')->nullable();
            $table->integer('chat_last_activity')->default(0);
            $table->boolean('chat_admin')->default(0);

        });

        for($i=1;$i<=50;$i++)
        {
            $r = Str::random(6);
            $u = User::create([
                'username' => "Visitante-$r",
                'email' => "visitante-$i@estudiaryviajar.com",
                'password' => bcrypt("ninguno-$i"),
                'es_guest' => 1
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['es_guest', 'last_activity', 'chat_id', 'chat_last_activity', 'chat_admin']);
        });

        User::where('username', 'like', 'Visitante-%')->delete();
    }
}
