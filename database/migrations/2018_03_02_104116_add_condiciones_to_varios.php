<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCondicionesToVarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->longText('condiciones')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->longText('condiciones')->nullable();
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->longText('condiciones')->nullable();
        });

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->longText('condiciones')->nullable();
        });

        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->longText('condiciones')->nullable();
        });

        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->longText('condiciones')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('condiciones');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('condiciones');
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('condiciones');
        });

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('condiciones');
        });

        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->dropColumn('condiciones');
        });

        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->dropColumn('condiciones');
        });
    }
}
