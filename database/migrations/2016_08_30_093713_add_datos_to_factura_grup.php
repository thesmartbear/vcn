<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDatosToFacturaGrup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_facturas_grup', function (Blueprint $table) {
            $table->string('nif',20)->nullable();
            $table->string('razonsocial',80)->nullable();
            $table->string('direccion')->nullable();
        });

        Schema::table('booking_pagos', function (Blueprint $table) {
            $table->date('fecha')->change();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_facturas_grup', function (Blueprint $table) {
            $table->dropColumn('nif');
            $table->dropColumn('razonsocial');
            $table->dropColumn('direccion');
        });
    }
}
