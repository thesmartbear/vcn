<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfigWebRegistro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->json('registro')->nullable();
        });

        Schema::table('plataformas', function (Blueprint $table) {
            $table->boolean('web_registro')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('registro');
        });

        Schema::table('plataformas', function (Blueprint $table) {
            $table->dropColumn('web_registro');
        });
    }
}
