<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposToPromos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_promos', function (Blueprint $table) {
            $table->integer('orden')->unsigned()->default(0);
            $table->boolean('targetblank_panel')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_promos', function (Blueprint $table) {
            $table->dropColumn('orden');
            $table->dropColumn('targetblank_panel');
        });
    }
}
