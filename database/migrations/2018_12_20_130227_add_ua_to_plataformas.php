<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUaToPlataformas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plataformas', function (Blueprint $table) {
            $table->string('ganalytics')->nullable();
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->string('ganalytics')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plataformas', function (Blueprint $table) {
            $table->dropColumn('ganalytics');
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('ganalytics');
        });
    }
}
