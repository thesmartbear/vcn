<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugsToCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->string('slug');
        });
        Schema::table('subcategorias', function (Blueprint $table) {
            $table->string('slug');
        });
        Schema::table('subcategoria_detalles', function (Blueprint $table) {
            $table->string('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
        Schema::table('subcategoria_detalles', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
