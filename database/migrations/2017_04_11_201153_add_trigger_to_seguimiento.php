<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTriggerToSeguimiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('status_checklists', function (Blueprint $table) {
            $table->string('seguimiento')->nullable();
        });

        Schema::table('status_checklists_booking', function (Blueprint $table) {
            $table->string('seguimiento')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('status_checklists', function (Blueprint $table) {
            $table->dropColumn('seguimiento');
        });

        Schema::table('status_checklists_booking', function (Blueprint $table) {
            $table->dropColumn('seguimiento');
        });
    }
}
