<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLopdChecks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajeros', function (Blueprint $table) {

            $table->boolean('lopd_check1')->nullable();
            $table->boolean('lopd_check2')->nullable();
            $table->boolean('lopd_check3')->nullable();

        });

        Schema::table('tutores', function (Blueprint $table) {

            $table->boolean('lopd_check1')->nullable();
            $table->boolean('lopd_check2')->nullable();
            $table->boolean('lopd_check3')->nullable();

        });

        Schema::table('bookings', function (Blueprint $table) {

            $table->boolean('lopd_check1')->nullable();
            $table->boolean('lopd_check2')->nullable();
            $table->boolean('lopd_check3')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajeros', function (Blueprint $table) {
            $table->dropColumn(['lopd_check1','lopd_check2','lopd_check3']);
        });

        Schema::table('tutores', function (Blueprint $table) {
            $table->dropColumn(['lopd_check1','lopd_check2','lopd_check3']);
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn(['lopd_check1','lopd_check2','lopd_check3']);
        });
    }
}
