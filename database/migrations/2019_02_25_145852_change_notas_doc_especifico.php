<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNotasDocEspecifico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_doc_especificos', function (Blueprint $table) {
            $table->text('notas')->change();
        });

        Schema::table('system_doc_especifico_langs', function (Blueprint $table) {
            $table->text('notas')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_doc_especificos', function (Blueprint $table) {
            $table->string('notas')->change();
        });

        Schema::table('system_doc_especifico_langs', function (Blueprint $table) {
            $table->string('notas')->change();
        });
    }
}
