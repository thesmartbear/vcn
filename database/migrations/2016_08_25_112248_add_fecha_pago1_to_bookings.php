<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingPago;

class AddFechaPago1ToBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->date('fecha_pago1')->nullable();
        });

        //Actualizar:
        $bookings = Booking::whereIn('status_id',[1,3,4,5,6])->groupBy('id')->get();

        $i=0;
        foreach($bookings as $b)
        {
            $pago = BookingPago::where('booking_id',$b->id)->orderBy('fecha','ASC')->first();
            if($pago)
            {
                $b->fecha_pago1 = $pago->fecha;
                $b->save();
            }
            else
            {
                echo $b->id.",";
            }

            $i++;
        }

        echo "Result: $i";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('fecha_pago1');
        });
    }
}
