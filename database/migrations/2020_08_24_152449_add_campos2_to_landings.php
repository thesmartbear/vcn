<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCampos2ToLandings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_landings', function (Blueprint $table) {
            $table->integer('catalogo_id')->unsigned()->default(0);
            
            // Antes del formulario
            $table->text('form_pre')->nullable();
            // Debajo del formulario
            $table->text('form_after')->nullable();
            // Debajo de Destinos/Programas
            $table->text('lista_after')->nullable();
            // Encima de 'Sobre nosotros'
            $table->text('nosotros_pre')->nullable();
            // Encima de 'Porqué British Summer'
            $table->text('porque_pre')->nullable();
            // Encima de Testimonios
            $table->text('testimonios_pre')->nullable();
            // Encima de Sellos de calidad
            $table->text('sellos_pre')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_landings', function (Blueprint $table) {
            $table->dropColumn('catalogo_id');
            $table->dropColumn(['form_pre','form_after','lista_after','nosotros_pre','porque_pre','testimonios_pre','sellos_pre']);
        });
    }
}
