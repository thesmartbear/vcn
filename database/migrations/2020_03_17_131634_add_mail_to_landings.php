<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMailToLandings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_landings', function (Blueprint $table) {
            $table->string('avisos')->nullable();
        });

        $aviso = [ 'id'=> 125,
            'name' => 'landing.aviso',
            'trigger'=> 'Aviso formulario Landing',
            'destino'=> 0,
            'destino_notas'=> 'Configuración Landing',
            'template'=> 'landing_form',
            'asunto'=> 'Nuevo lead landing web',
            'contenido'=> null
        ];

        DB::table('system_mails')->insert($aviso);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('system_mails')->where('id',125)->delete();

        Schema::table('cms_landings', function (Blueprint $table) {
            $table->dropColumn('avisos');
        });
    }
}
