<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDayToConvoAbierta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alojamientos', function (Blueprint $table) {
            $table->boolean('start_day')->nullable();
            $table->boolean('end_day')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alojamientos', function (Blueprint $table) {
            $table->dropColumn('start_day');
            $table->dropColumn('end_day');
        });
    }
}
