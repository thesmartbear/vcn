<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposPocketToCursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->boolean('pocket')->default(0);
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->boolean('pocket')->default(0);
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->boolean('pocket_dinero_check')->default(0);
            $table->string('pocket_dinero')->nullable();
            $table->boolean('pocket_prueba_nivel_check')->default(0);
            $table->boolean('pocket_monitores_check')->default(0);
            $table->text('pocket_monitores')->nullable();
            $table->text('pocket_normas_check')->nullable();
        });

        $c = [
            'name' => 'pocket_dinero',
            'nombre' => 'Dinero bolsillo',
            'modelo' => 'Curso',
            'tipo' => 2,
            'default'=> 75,
            'traducible' => 1,
        ];
        \VCN\Models\System\Campo::create($c);

        $c = [
            'name' => 'pocket_prueba_nivel',
            'nombre' => 'Prueba de nivel',
            'modelo' => 'Curso',
            'tipo' => 0,
            'traducible' => 1,
        ];
        \VCN\Models\System\Campo::create($c);

        $c = [
            'name' => 'pocket_monitores',
            'nombre' => 'Monitores',
            'modelo' => 'Curso',
            'tipo' => 2,
            'textarea'=> 1,
            'traducible' => 1,
        ];
        \VCN\Models\System\Campo::create($c);

        $c = [
            'name' => 'pocket_normas',
            'nombre' => 'Normas Genéricas',
            'modelo' => 'Curso',
            'tipo' => 0,
            'traducible' => 1,
        ];
        \VCN\Models\System\Campo::create($c);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \VCN\Models\System\Campo::truncate();

        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn(['pocket_dinero_check','pocket_dinero','pocket_prueba_nivel_check','pocket_monitores','pocket_monitores_check','pocket_normas_check']);
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('pocket');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('pocket');
        });
    }
}
