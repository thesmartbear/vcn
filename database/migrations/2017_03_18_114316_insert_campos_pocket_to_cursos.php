<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCamposPocketToCursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->boolean('material_check')->default(0);
            $table->boolean('anterior_participantes_check')->default(0);
            $table->boolean('anterior_nacionalidades_check')->default(0);

            //los campos para valores ya estabas añadidos
            // $table->text('material')->nullable();
            // $table->text('anterior_participantes')->nullable(); //ya estaba añadido
            // $table->text('anterior_nacionalidades')->nullable(); //ya estaba añadido
        });

        $c = [
            'name' => 'material',
            'nombre' => 'Material/Ropa para actividades específicas',
            'modelo' => 'Curso',
            'tipo' => 2,
            'textarea' => 1,
            'traducible' => 1,
        ];
        \VCN\Models\System\Campo::create($c);

        $c = [
            'name' => 'anterior_participantes',
            'nombre' => 'Nº participantes totales del año pasado',
            'modelo' => 'Curso',
            'tipo' => 2,
            'textarea' => 0,
            'traducible' => 1,
        ];
        \VCN\Models\System\Campo::create($c);

        $c = [
            'name' => 'anterior_nacionalidades',
            'nombre' => 'Nacionalidades año pasado',
            'modelo' => 'Curso',
            'tipo' => 2,
            'textarea' => 1,
            'traducible' => 1,
        ];
        \VCN\Models\System\Campo::create($c);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('material_nivel_check');
            $table->dropColumn('anterior_participantes_check');
            $table->dropColumn('anterior_nacionalidades_check');
        });
    }
}
