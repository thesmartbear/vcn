<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlazasToVuelos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('plazas');
            $table->dropColumn('plazas_reservadas');
        });

        Schema::create('convocatoria_plazas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('convocatory_id')->unsigned();
            $table->integer('alojamiento_id')->unsigned();

            $table->integer('plazas_totales')->nullable()->default(0);
            $table->integer('plazas_bloqueadas')->nullable()->default(0);

            $table->foreign('convocatory_id')
                ->references('id')->on('convocatoria_cerradas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('alojamiento_id')
                ->references('id')->on('alojamientos')
                ->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::dropIfExists('convocatoria_vuelos');
        Schema::create('vuelos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->text('info')->nullable();
            $table->decimal('precio', 10)->nullable();

            $table->integer('plazas_totales')->nullable()->default(0);
            $table->integer('plazas_umbral')->nullable()->default(0);
            $table->integer('plazas_bloqueadas')->nullable()->default(0);

            //ida
            $table->string('ida_company')->nullable();
            $table->string('ida_vuelo')->nullable();
            $table->string('ida_salida_aeropuerto')->nullable();
            $table->date('ida_salida_fecha')->nullable();
            $table->time('ida_salida_hora')->nullable();
            $table->string('ida_llegada_aeropuerto')->nullable();
            $table->date('ida_llegada_fecha')->nullable();
            $table->time('ida_llegada_hora')->nullable();

            //vuelta
            $table->string('vuelta_company')->nullable();
            $table->string('vuelta_vuelo')->nullable();
            $table->string('vuelta_salida_aeropuerto')->nullable();
            $table->date('vuelta_salida_fecha')->nullable();
            $table->time('vuelta_salida_hora')->nullable();
            $table->string('vuelta_llegada_aeropuerto')->nullable();
            $table->date('vuelta_llegada_fecha')->nullable();
            $table->time('vuelta_llegada_hora')->nullable();


        });

        Schema::create('convocatoria_vuelos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('convocatory_id')->unsigned();
            $table->integer('vuelo_id')->unsigned();

            $table->foreign('convocatory_id')
                ->references('id')->on('convocatoria_cerradas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('vuelo_id')
                ->references('id')->on('vuelos')
                ->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('vuelo_id')->unsigned();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('convocatoria_vuelos');
        Schema::drop('vuelos');
        Schema::drop('convocatoria_plazas');

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->integer('plazas')->nullable();
            $table->integer('plazas_reservadas')->nullable();
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('vuelo_id');
        });
    }
}
