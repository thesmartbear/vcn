<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriaWebsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name',60);
            $table->string('titulo');
            $table->integer('category_id')->unsigned()->nullable(); //categoria padre

            $table->boolean('activo')->default(0);

            $table->string('seo_url')->nullable();
            $table->string('seo_titulo')->nullable();
            $table->text('seo_descripcion')->nullable();
            $table->string('seo_keywords')->nullable();

            $table->text('descripcion')->nullable();
            $table->string('color',7)->nullable();
            $table->tinyInteger('propietario')->default(0);

            //condiciones
            $table->boolean('excluye')->default(0);
            $table->string('idioma')->nullable();
            $table->boolean('idioma_excluye')->default(0);
            $table->tinyInteger('tipoc')->default(0); //1:cerrada, 2:abierta, 3:multi
            $table->string('categorias')->nullable();
            $table->string('subcategorias')->nullable();
            $table->string('subcategoriasdet')->nullable();

            $table->string('imagen')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms_categorias');
    }
}
