<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInfoCampamentoToCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->boolean('es_info_campamento')->default(0);
        });

        Schema::table('bookings', function (Blueprint $table) {
           $table->json('datos_campamento')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('es_info_campamento');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('datos_campamento');
        });
    }
}
