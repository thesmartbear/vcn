<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComisionToCurso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->decimal('commission', 4)->nullable();
        });

        foreach(\VCN\Models\Cursos\Curso::all() as $c)
        {
            if($c->centro)
            {
                $c->commission = $c->centro->commission;
                $c->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('commission');
        });
    }
}
