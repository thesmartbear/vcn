<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajeroArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajero_archivos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('viajero_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->date('fecha');
            $table->string('doc');
            $table->boolean('visible')->default(0);

            $table->foreign('viajero_id')
                ->references('id')->on('viajeros')
                ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('user_id')
            //     ->references('id')->on('users')
            //     ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('viajero_archivos');
    }
}
