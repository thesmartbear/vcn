<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDecimalesCambio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monedas', function (Blueprint $table) {
            $table->decimal('currency_rate', 5, 4)->change();
        });

        Schema::table('booking_monedas', function (Blueprint $table) {
            $table->decimal('rate', 5, 4)->change();
        });

        Schema::table('booking_pagos', function (Blueprint $table) {
            $table->decimal('rate', 5, 4)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('monedas', function (Blueprint $table) {
            $table->decimal('currency_rate', 10)->change();
        });

        Schema::table('booking_monedas', function (Blueprint $table) {
            $table->decimal('rate', 10)->change();
        });

        Schema::table('booking_pagos', function (Blueprint $table) {
            $table->decimal('rate', 10)->change();
        });
    }
}
