<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePdfCondiciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->text('pdf_condiciones')->change();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->text('pdf_condiciones')->change();

        });

        foreach(\VCN\Models\Categoria::all() as $cat)
        {
            $cat->pdf_condiciones = null;
            $cat->save();
        }

        foreach(\VCN\Models\Subcategoria::all() as $cat)
        {
            $cat->pdf_condiciones = null;
            $cat->save();
        }

        Schema::table('plataformas', function (Blueprint $table) {
            $table->boolean('tpv_activo_banco')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->string('pdf_condiciones')->change();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->string('pdf_condiciones')->change();
        });

        Schema::table('plataformas', function (Blueprint $table) {
            $table->dropColumn('tpv_activo_banco');
        });
    }
}
