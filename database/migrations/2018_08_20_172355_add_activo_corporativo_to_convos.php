<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActivoCorporativoToConvos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->boolean('activo_corporativo')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('activo_corporativo');
        });
    }
}
