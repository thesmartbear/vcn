<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrescriptorIdToInformeVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('informe_ventas', function (Blueprint $table) {
            $table->integer('prescriptor_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('informe_ventas', function (Blueprint $table) {
            $table->dropColumn('prescriptor_id');
        });
    }
}
