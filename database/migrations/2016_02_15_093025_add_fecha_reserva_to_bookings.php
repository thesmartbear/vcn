<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingLog;

class AddFechaReservaToBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->date('fecha_reserva')->nullable();
        });

        //Actualizar:
        $bookings = Booking::whereIn('status_id',[1,3,4,5,6])->groupBy('id')->get();

        $i=0;
        foreach($bookings as $b)
        {
            $log = BookingLog::where('booking_id',$b->id)->where('tipo','LIKE','%-> Pre-Booking]')->first();
            if($log)
            {
                $b->fecha_reserva = $log->created_at;
                $b->save();
            }

            $i++;
        }

        echo "Result: $i";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('fecha_reserva');
        });
    }
}
