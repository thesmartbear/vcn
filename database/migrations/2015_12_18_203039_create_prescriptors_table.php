<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrescriptorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oficinas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->tinyInteger('propietario')->default(0);
        });

        Schema::create('prescriptor_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
        });

        Schema::create('prescriptor_comisiones_cat', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
        });

        Schema::create('prescriptores', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            //Generales
            $table->string('name');
            $table->string('sucursal')->nullable();
            $table->string('direccion')->nullable();
            $table->string('cp',10)->nullable();
            $table->string('poblacion')->nullable();

            $table->string('provincia')->nullable();
            $table->string('pais')->nullable();

            $table->string('idioma',2);


            //Contacto
            $table->string('persona')->nullable();
            $table->string('telefono1')->nullable();
            $table->string('telefono2')->nullable();
            $table->string('email')->nullable();
            $table->text('notas')->nullable();

            //Fiscales
            $table->string('nif')->nullable();
            $table->string('razon_social')->nullable();
            $table->string('domicilio_social')->nullable();

            $table->integer('categoria_id')->unsigned();
            $table->integer('comision_cat_id')->unsigned()->nullable();
            $table->integer('oficina_id')->unsigned()->nullable();

            $table->foreign('categoria_id')
                ->references('id')->on('prescriptor_categorias')
                ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('comision_cat_id')
            //     ->references('id')->on('prescriptor_comisiones_cat')
            //     ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('oficina_id')
            //     ->references('id')->on('oficinas')
            //     ->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('prescriptor_id')->unsigned();

            // $table->foreign('prescriptor_id')
            //     ->references('id')->on('prescriptores')
            //     ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('viajeros', function (Blueprint $table) {
            $table->integer('prescriptor_id')->unsigned();

            // $table->foreign('prescriptor_id')
            //     ->references('id')->on('prescriptores')
            //     ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('prescriptor_id');
        });

        Schema::table('viajeros', function (Blueprint $table) {
            $table->dropColumn('prescriptor_id');
        });

        Schema::drop('prescriptores');
        Schema::drop('oficinas');
        Schema::drop('prescriptor_comisiones_cat');
        Schema::drop('prescriptor_categorias');
    }
}
