<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutores', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->nullable();//->unique('tutores_email_UNIQUE');
            $table->string('phone')->nullable();
            $table->string('movil')->nullable();

            $table->string('profesion')->nullable();
            $table->string('empresa')->nullable();
            $table->string('empresa_phone')->nullable();

            $table->integer('user_id')->unsigned()->default(0);

            // $table->foreign('user_id')
            //     ->references('id')->on('users')
            //     ->onDelete('cascade')->onUpdate('cascade');

            // $table->integer('cliente_id')->unsigned();

            // $table->foreign('cliente_id')
            //     ->references('id')->on('clientes')
            //     ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tutores');
    }
}
