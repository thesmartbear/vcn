<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoreunionToConvocatoria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->boolean('reunion_no')->default(0);
        });

        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->boolean('reunion_no')->default(0);
        });

        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->boolean('reunion_no')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('reunion_no');
        });

        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->dropColumn('reunion_no');
        });

        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->dropColumn('reunion_no');
        });
    }
}
