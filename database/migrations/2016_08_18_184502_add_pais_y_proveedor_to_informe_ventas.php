<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaisYProveedorToInformeVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('pais_id')->unsigned()->nullable()->default(0);
            $table->integer('proveedor_id')->unsigned()->nullable()->default(0);
        });

        //Actualizamos el valor
        foreach( \VCN\Models\Bookings\Booking::all() as $booking )
        {
            $booking->proveedor_id = $booking->centro->provider_id;
            $booking->pais_id = $booking->centro->pais_id;
            $booking->save();
        }

        Schema::table('informe_ventas', function (Blueprint $table) {
            $table->integer('pais_id')->unsigned()->nullable()->default(0);
            $table->integer('proveedor_id')->unsigned()->nullable()->default(0);
        });

        //Recontamos ventas
        // \VCN\Models\Informes\Venta::procesarBookings();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('informe_ventas', function (Blueprint $table) {
            $table->dropColumn('pais_id');
            $table->dropColumn('proveedor_id');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('pais_id');
            $table->dropColumn('proveedor_id');
        });
    }
}
