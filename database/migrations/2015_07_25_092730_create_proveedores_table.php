<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedores', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable()->index('name_INDEX');
            $table->string('contact_name')->nullable();
            $table->string('contact_email', 45)->nullable();
            $table->string('contact_number', 45)->nullable();
            $table->string('contact_mobil', 45)->nullable();
            $table->decimal('commission', 4)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proveedores');
    }
}
