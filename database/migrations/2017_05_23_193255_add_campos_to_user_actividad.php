<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposToUserActividad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_actividad', function (Blueprint $table) {
            $table->integer('inactividad')->default(0);
            $table->integer('inactividad_max')->default(0);

            $table->integer('desktop_inactividad')->default(0);
            $table->integer('desktop_inactividad_max')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_actividad', function (Blueprint $table) {
            $table->dropColumn(['inactividad','inactividad_max']);
            $table->dropColumn(['desktop_inactividad','desktop_inactividad_max']);
        });
    }
}
