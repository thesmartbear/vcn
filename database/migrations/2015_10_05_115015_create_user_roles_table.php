<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_roles', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->text('permisos'); //json

            $table->timestamps();
        });

        $statement = "ALTER TABLE user_roles AUTO_INCREMENT = 20;";
        DB::unprepared($statement);

        Schema::table('users', function (Blueprint $table) {

            $table->json('config')->nullable(); //JSON: permisos, roles ...

            // $table->foreign('roleid')
            //     ->references('id')->on('user_roles')
            //     ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('config');
        });

        Schema::drop('user_roles');
    }
}
