<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');

            $table->text('name');
            $table->text('lname');
            $table->text('company');
            $table->text('email');
            $table->text('address1');
            $table->text('address2');
            $table->text('city');
            $table->text('state');
            $table->text('postcode');
            $table->text('country');
            $table->text('phone');
            $table->text('password');
            $table->decimal('balance', 10);
            $table->date('datecreated');
            $table->text('notes');
            $table->integer('groupid');
            $table->dateTime('lastlogin')->nullable();
            $table->text('ip');
            $table->text('host');
            $table->enum('status', array('Active','Inactive','Closed'))->default('Active');
            $table->text('pwresetkey');
            $table->integer('pwresetexpiry');
            $table->enum('emailnotify', array('Yes','No'))->default('Yes');
            $table->enum('acctype', array('Customer','Vendor','Cash','Bank','Investor','Partner','Employee','Consultant','Income','Expense','TAX','Credit-Card','Inventory','Long-Term-Liability','Accounts-Payable','Accounts-Receivable','Equity','Account-Credit','Cost-of-Goods-Sold','Other'));
            $table->text('email2')->nullable();
            $table->enum('client', array('Yes','No'))->nullable()->default('No');
            // $table->index(['name','lname'], 'firstname_lastname');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accounts');
    }
}
