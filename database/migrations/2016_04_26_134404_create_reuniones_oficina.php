<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReunionesOficina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reuniones', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('oficina_id')->unsigned();

            $table->integer('curso_id')->unsigned()->nullable();
            $table->integer('convocatoria_id')->unsigned()->nullable();
            $table->boolean('convocatoria_tipo')->nullable(); //1:cerrada, 2:abierta, 3:multi

            $table->date('fecha');
            $table->time('hora');
            $table->string('lugar');

            $table->foreign('oficina_id')
                ->references('id')->on('oficinas')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reuniones');
    }
}
