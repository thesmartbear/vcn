<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActivoWebToCurso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->boolean('activo_web')->default(1);
        });

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->boolean('activo_web')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('activo_web');
        });

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('activo_web');
        });
    }
}
