<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposViajeroDatos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->boolean('trinity_exam')->default(0);
            $table->string('trinity_any',4)->nullable();
            $table->tinyInteger('trinity_nivel')->default(0);
            $table->boolean('cic_thau')->default(0); //0: no, 1:bcn, 2:sant cugat
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->tinyInteger('nivel_cic')->default(0);
            $table->tinyInteger('nivel_trinity')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->dropColumn('trinity_exam');
            $table->dropColumn('trinity_any');
            $table->dropColumn('trinity_nivel');
            $table->dropColumn('cic_thau');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('nivel_cic');
            $table->dropColumn('nivel_trinity');
        });
    }
}
