<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMascamposToViajero extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajeros', function (Blueprint $table) {
            $table->string('emergencia_contacto',80)->nullable();
            $table->string('emergencia_telefono',25)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajeros', function (Blueprint $table) {
            $table->dropColumn('emergencia_contacto');
            $table->dropColumn('emergencia_telefono');
        });
    }
}
