<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderToCategoriasWeb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->boolean('orden')->default(0);
            $table->boolean('excluye_categorias')->default(0);
            $table->boolean('excluye_subcategorias')->default(0);
            $table->boolean('excluye_subcategoriasdet')->default(0);

            $table->dropColumn('excluye');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->dropColumn('orden');
            $table->dropColumn('excluye_categorias');
            $table->dropColumn('excluye_subcategorias');
            $table->dropColumn('excluye_subcategoriasdet');

            $table->boolean('excluye')->default(0);
        });
    }
}
