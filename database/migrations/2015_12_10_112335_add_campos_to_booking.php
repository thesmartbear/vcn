<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposToBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('transporte_otro')->default(0);
            $table->string('transporte_detalles')->nullable();
            $table->boolean('transporte_ok')->default(0);
            $table->boolean('transporte_no')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('transporte_otro');
            $table->dropColumn('transporte_detalles');
            $table->dropColumn('transporte_ok');
            $table->dropColumn('transporte_no');
        });
    }
}
