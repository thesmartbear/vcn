<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajeros', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->nullable();//->unique('viajeros_email_UNIQUE');
            $table->string('phone')->nullable();
            $table->string('movil')->nullable();
            $table->date('fechanac')->nullable();
            $table->text('notas')->nullable();
            $table->char('sexo',1);

            // $table->string('ciudad')->nullable();
            // $table->string('destinos')->nullable();
            // $table->string('cursos')->nullable();

            $table->tinyInteger('rating');
            $table->boolean('es_cliente')->default(false); //solicitud/bookings

            $table->integer('solicitud_id')->unsigned()->default(0);
            $table->integer('status_id')->unsigned()->default(1); //solicitud

            $table->integer('booking_id')->unsigned()->default(0); //datos del ultimo booking
            $table->integer('booking_status_id')->unsigned()->default(0);

            $table->integer('asign_to')->unsigned()->nullable();
            $table->boolean('archivado')->default(0); //???
            $table->string('archivado_motivo')->nullable();

            $table->integer('user_id')->unsigned()->default(0);

            // $table->integer('category_id')->unsigned()->nullable();
            // $table->integer('subcategory_id')->unsigned()->nullable();

            $table->integer('origen_id')->unsigned()->nullable();
            $table->integer('suborigen_id')->unsigned()->nullable();
            $table->integer('suborigendet_id')->unsigned()->nullable();

            // $table->foreign('category_id')
            //     ->references('id')->on('categorias');
            // $table->foreign('subcategory_id')
            //     ->references('id')->on('subcategorias');

            // $table->foreign('user_id')
            //     ->references('id')->on('users')
            //     ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('origen_id')
            //     ->references('id')->on('viajero_origenes');
            // $table->foreign('suborigen_id')
            //     ->references('id')->on('viajero_suborigenes');
            // $table->foreign('suborigendet_id')
            //     ->references('id')->on('viajero_suborigenesdet');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('viajeros');
    }
}
