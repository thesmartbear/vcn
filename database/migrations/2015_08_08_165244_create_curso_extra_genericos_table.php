<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursoExtraGenericosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso_extra_genericos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('generics_id')->unsigned();
            $table->integer('course_id')->unsigned();

            $table->foreign('generics_id')
                ->references('id')->on('extras')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('course_id')
                ->references('id')->on('cursos')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('curso_extra_genericos');
    }
}
