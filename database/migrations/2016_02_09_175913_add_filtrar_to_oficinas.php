<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiltrarToOficinas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oficinas', function (Blueprint $table) {
            $table->boolean('ver_otras')->default(0);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('oficina');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oficinas', function (Blueprint $table) {
            $table->dropColumn('ver_otras');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('oficina')->nullable();
        });
    }
}
