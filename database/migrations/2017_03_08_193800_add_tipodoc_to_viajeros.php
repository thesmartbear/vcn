<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipodocToViajeros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->boolean('tipodoc')->default(0);
        });

        Schema::table('tutores', function (Blueprint $table) {
            $table->boolean('tipodoc')->default(0);
        });

        Schema::table('booking_datos', function (Blueprint $table) {
            $table->boolean('tipodoc')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->dropColumn('tipodoc');
        });

        Schema::table('tutores', function (Blueprint $table) {
            $table->dropColumn('tipodoc');
        });

        Schema::table('booking_datos', function (Blueprint $table) {
            $table->dropColumn('tipodoc');
        });
    }
}
