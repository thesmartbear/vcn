<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvatarToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->longText('avatar')->nullable();

        });

        Schema::table('viajeros', function (Blueprint $table) {

            $table->longText('foto')->nullable();

        });

        Schema::table('tutores', function (Blueprint $table) {

            $table->longText('foto')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('avatar');

        });

        Schema::table('tutores', function (Blueprint $table) {

            $table->dropColumn('foto');

        });

        Schema::table('viajeros', function (Blueprint $table) {

            $table->dropColumn('foto');

        });
    }
}
