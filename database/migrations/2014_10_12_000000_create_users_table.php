<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('fname');
            $table->string('lname')->nullable();
            $table->string('email');//->unique();
            $table->string('username')->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->tinyInteger('status');
            $table->tinyInteger('roleid');
            $table->text('signature')->nullable();

            $table->boolean('emailnotify')->default(true);
            $table->integer('login_attempts');
            $table->dateTime('login_last')->nullable();
            $table->string('login_ip');
            $table->string('login_host');

            // $table->json('config')->nullable(); //JSON: permisos, roles ...
            // $table->string('roles',20);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
