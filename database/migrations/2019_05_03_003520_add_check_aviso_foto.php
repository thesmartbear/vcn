<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckAvisoFoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //categoria, subcategoria, subcat_detalle, curso

        Schema::table('categorias', function (Blueprint $table) {
            $table->boolean('es_aviso_foto')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->boolean('es_aviso_foto')->nullable();
        });

        Schema::table('subcategoria_detalles', function (Blueprint $table) {
            $table->boolean('es_aviso_foto')->nullable();
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->boolean('es_aviso_foto')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('es_aviso_foto');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('es_aviso_foto');
        });

        Schema::table('subcategoria_detalles', function (Blueprint $table) {
            $table->dropColumn('es_aviso_foto');
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('es_aviso_foto');
        });
    }
}
