<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvisosOnlineToCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->text('avisos_online')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->text('avisos_online')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('avisos_online');
        });
        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('avisos_online');
        });
    }
}
