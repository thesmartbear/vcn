<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCicToViajerosDatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->boolean('cic');
            $table->string('cic_nivel',30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->dropColumn('cic');
            $table->dropColumn('cic_nivel');
        });
    }
}
