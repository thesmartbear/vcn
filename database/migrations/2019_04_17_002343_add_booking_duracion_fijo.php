<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Output\ConsoleOutput;

class AddBookingDuracionFijo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('duracion_fijo')->unsigned()->default(0);
        });

        $output = new ConsoleOutput();

        $i = 0;
        $bookings = \VCN\Models\Bookings\Booking::where('convocatory_close_id', '>', 0)->where('created_at','>','2018-01-01');
        $output->writeln("Bookings: ". $bookings->count());
        foreach($bookings->get() as $b)
        {
            $unit = $b->convocatoria->duracion_fijo ?: 1;

            $b->duracion_fijo = $unit;
            $b->save();
            
            $i++;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('duracion_fijo');
        });
    }
}
