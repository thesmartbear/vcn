<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMultiToEspecialidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('especialidades', function (Blueprint $table) {
            $table->boolean('es_multi')->default(0)->after('name');
        });

        Schema::table('convocatoria_multi_especialidades', function (Blueprint $table) {
            $table->integer('especialidad_id')->unsigned()->nullable()->after('convocatory_id');
        });

        $camps = [
            'name'=> "Camps",
            'es_multi'=> 1,
        ];
        
        $esp = \VCN\Models\Especialidad::create($camps);

        foreach(\VCN\Models\Convocatorias\ConvocatoriaMultiEspecialidad::all() as $cm)
        {
            $datase = [
                'name'=> $cm->name,
                'especialidad_id' => $esp->id
            ];

            $sesp = \VCN\Models\Subespecialidad::where('especialidad_id', $esp->id)->where('name', $cm->name)->first();
            $sesp = $sesp ?: \VCN\Models\Subespecialidad::create($datase);

            $cm->especialidad_id = $sesp->id;
            $cm->save();
        }

        Schema::table('convocatoria_multi_especialidades', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->integer('especialidad_id')->nullable(false)->change();
            // $table->foreign('especialidad_id')
            //     ->references('id')->on('subespecialidades')
            //     ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_multi_especialidades', function (Blueprint $table) {
            $table->string('name')->nullable();
        });

        $esp = \VCN\Models\Especialidad::where('name', "Camps")->first();
        foreach(\VCN\Models\Convocatorias\ConvocatoriaMultiEspecialidad::all() as $cm)
        {
            $se = \VCN\Models\Subespecialidad::find($cm->especialidad_id);

            $cm->name = $se->name;
            $cm->save();
        }

        Schema::table('convocatoria_multi_especialidades', function (Blueprint $table) {
            $table->dropColumn('especialidad_id')->unsigned();
        });

        Schema::table('especialidades', function (Blueprint $table) {
            $table->dropColumn('es_multi');
        });

        $esp->delete();
    }
}
