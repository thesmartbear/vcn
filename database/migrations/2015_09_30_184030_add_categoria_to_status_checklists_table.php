<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoriaToStatusChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('status_checklists', function (Blueprint $table) {

            $table->integer('category_id')->unsigned()->default(0);
            $table->integer('subcategory_id')->unsigned()->default(0);
            $table->integer('subcategory_det_id')->unsigned()->default(0);

        });

        Schema::table('status_checklists_booking', function (Blueprint $table) {

            $table->integer('category_id')->unsigned()->default(0);
            $table->integer('subcategory_id')->unsigned()->default(0);
            $table->integer('subcategory_det_id')->unsigned()->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('status_checklists', function (Blueprint $table) {

            $table->dropColumn('category_id');
            $table->dropColumn('subcategory_id');
            $table->dropColumn('subcategory_det_id');

        });

        Schema::table('status_checklists_booking', function (Blueprint $table) {

            $table->dropColumn('category_id');
            $table->dropColumn('subcategory_id');
            $table->dropColumn('subcategory_det_id');

        });
    }
}
