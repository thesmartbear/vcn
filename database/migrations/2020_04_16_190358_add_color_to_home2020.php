<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColorToHome2020 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->string('color_texto', 7)->nullable();
            $table->string('color_fondo', 7)->nullable();
            $table->string('color_menu', 7)->nullable();
            $table->string('color_link', 7)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home2020', function (Blueprint $table) {
            $table->dropColumn('color_texto');
            $table->dropColumn('color_fondo');
            $table->dropColumn('color_menu');
            $table->dropColumn('color_link');
        });
    }
}
