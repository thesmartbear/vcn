<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAvisoDoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('system_aviso_docs');

        Schema::create('system_aviso_docs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->boolean('activo')->default(0);
        });

        $statement = "ALTER TABLE system_aviso_docs AUTO_INCREMENT = 11;";
        DB::unprepared($statement);

        Schema::create('system_aviso_doc_idiomas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('idioma',5);
            $table->string('titulo');
            $table->text('contenido');

            $table->integer('doc_id')->unsigned();

            $table->foreign('doc_id')
                ->references('id')->on('system_aviso_docs')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('system_aviso_doc_idiomas');
        Schema::drop('system_aviso_docs');
    }
}
