<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContableToEspecialidadesMulti extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_multi_especialidades', function (Blueprint $table) {
            $table->string('contable')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_multi_especialidades', function (Blueprint $table) {
            $table->dropColumn('contable');
        });
    }
}
