<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNombrewebToCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->string('name_web')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->string('name_web')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('name_web');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('name_web');
        });
    }
}
