<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examenes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('name');
            $table->string('title');
            $table->text('texto')->nullable();
            $table->text('gracias')->nullable();
            $table->string('template', 25)->nullable();
            $table->boolean('bloques')->nullable();

            $table->boolean('activo')->default(0);

        });

        Schema::create('examen_preguntas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->bigInteger('examen_id')->unsigned();
            
            $table->string('pregunta');
            $table->text('opciones')->nullable(); //array
            $table->string('respuesta')->nullable();
            
            $table->boolean('bloque')->default(0);
            $table->integer('numero');

            $table->foreign('examen_id')
                ->references('id')->on('examenes')
                ->onDelete('cascade');
        });

        Schema::create('examen_respuestas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->bigInteger('examen_id')->unsigned();
            $table->integer('viajero_id')->unsigned();
            $table->integer('booking_id')->unsigned()->nullable();
            $table->boolean('tipo')->default(0); //0:booking, 1:viajero
            $table->boolean('status')->default(0);
            
            $table->text('respuestas')->nullable(); //array pregunta=>respuesta
            $table->integer('aciertos')->default(0);
            $table->decimal('resultado')->default(0);

            $table->text('notas')->nullable();

            $table->foreign('examen_id')
                ->references('id')->on('examenes')
                ->onDelete('cascade');

            $table->foreign('viajero_id')
                ->references('id')->on('viajeros')
                ->onDelete('cascade');
        });

        Schema::create('examen_respuesta_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->bigInteger('respuesta_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->string('tipo')->nullable();
            $table->text('notas')->nullable();

            $table->foreign('respuesta_id')
                ->references('id')->on('examen_respuestas')
                ->onDelete('cascade');
        });

        $statement = "ALTER TABLE examenes AUTO_INCREMENT = 100;";
        DB::unprepared($statement);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('examen_respuesta_logs');
        Schema::dropIfExists('examen_respuestas');
        Schema::dropIfExists('examen_preguntas');
        Schema::dropIfExists('examenes');
    }
}
