<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersToVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('informe_ventas', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->default(0);
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('subcategory_id')->unsigned()->nullable();
            $table->integer('subcategory_det_id')->unsigned()->nullable();
        });

        //Actualizamos el valor
        foreach( \DB::table('bookings')->get() as $b )
        {
            $curso = \VCN\Models\Cursos\Curso::find($b->curso_id);

            if($curso)
            {
                $booking = \VCN\Models\Bookings\Booking::find($b->id);
                $booking->category_id = $curso->category_id;
                $booking->subcategory_id = $curso->subcategory_id;
                $booking->subcategory_det_id = $curso->subcategory_det_id;
                $booking->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('informe_ventas', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('category_id');
            $table->dropColumn('subcategory_id');
            $table->dropColumn('subcategory_det_id');
        });
    }
}
