<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemContactos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_contactos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->string('phone',25)->nullable();
            $table->text('notas')->nullable();

            $table->boolean('es_sos')->default(1);
            $table->boolean('es_proveedor')->default(0);
            $table->boolean('activo')->default(0);
        });

        Schema::create('system_contacto_models', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('modelo')->nullable();
            $table->integer('modelo_id')->unsigned();
            $table->tinyInteger('plataforma')->default(0);
            $table->boolean('es_proveedor')->default(0);

            $table->integer('contacto_id')->unsigned();

            $table->foreign('contacto_id')
                ->references('id')->on('system_contactos')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('sos_proveedor')->unsigned()->nullable();
            $table->integer('sos_plataforma')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn(['sos_proveedor', 'sos_plataforma']);
        });

        Schema::dropIfExists('system_contacto_models');
        Schema::dropIfExists('system_contactos');
    }
}
