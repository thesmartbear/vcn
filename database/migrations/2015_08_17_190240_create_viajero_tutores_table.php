<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajeroTutoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajero_tutores', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tutor_id')->unsigned();
            $table->integer('viajero_id')->unsigned();

            // $table->enum('relacion', array('Padre','Madre','Tutor','Otro'))->nullable();
            $table->boolean('relacion')->default(0);
            $table->string('relacion_otro')->nullable();

            $table->foreign('tutor_id')
                ->references('id')->on('tutores')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('viajero_id')
                ->references('id')->on('viajeros')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('viajero_tutores');
    }
}
