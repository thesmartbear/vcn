<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTituloToCmsPaginas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'cms_categorias', function (Blueprint $table) {
            $table->string('home_titulo')->nullable();
            $table->string('home_titulo_link')->nullable();
            $table->boolean('home_titulo_pos')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'cms_categorias', function (Blueprint $table) {
            $table->dropColumn(['home_titulo','home_titulo_link','home_titulo_pos']);
        });
    }
}
