<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_logs', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tutor_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->integer('status_id')->unsigned(); //??
            $table->integer('category_id')->unsigned(); //??
            $table->integer('subcategory_id')->unsigned(); //??

            $table->integer('asign_to')->unsigned()->nullable();

            $table->string('tipo')->nullable();
            $table->text('notas')->nullable();
            $table->json('data')->nullable();

            $table->foreign('tutor_id')
                ->references('id')->on('tutores')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tutor_logs');
    }
}
