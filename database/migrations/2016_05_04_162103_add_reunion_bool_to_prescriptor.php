<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReunionBoolToPrescriptor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('area_reunion')->default(1);
        });

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->boolean('area_reunion')->default(1);
        });

        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->boolean('area_reunion')->default(1);
        });

        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->boolean('area_reunion')->default(1);
        });

        Schema::table('prescriptores', function (Blueprint $table) {
            $table->boolean('area_reunion')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('area_reunion');
        });

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('area_reunion');
        });

        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->dropColumn('area_reunion');
        });

        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->dropColumn('area_reunion');
        });

        Schema::table('prescriptores', function (Blueprint $table) {
            $table->dropColumn('area_reunion');
        });
    }
}
