<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDatosMedicosToClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->renameColumn('alergias','enfermedad');
        });

        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->text('alergias')->nullable();
            $table->text('tratamiento')->nullable();
            $table->text('dieta')->nullable();

            $table->text('alergias2')->nullable();
            $table->text('enfermedad2')->nullable();
            $table->text('medicacion2')->nullable();
            $table->text('tratamiento2')->nullable();
            $table->text('dieta2')->nullable();
        });

        Schema::table('booking_datos', function (Blueprint $table) {
            $table->renameColumn('alergias','enfermedad');
        });

        Schema::table('booking_datos', function (Blueprint $table) {
            $table->text('alergias')->nullable();
            $table->text('tratamiento')->nullable();
            $table->text('dieta')->nullable();

            $table->text('alergias2')->nullable();
            $table->text('enfermedad2')->nullable();
            $table->text('medicacion2')->nullable();
            $table->text('tratamiento2')->nullable();
            $table->text('dieta2')->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->dropColumn('alergias');
            $table->dropColumn('tratamiento');
            $table->dropColumn('dieta');

            $table->dropColumn('alergias2');
            $table->dropColumn('enfermedad2');
            $table->dropColumn('medicacion2');
            $table->dropColumn('tratamiento2');
            $table->dropColumn('dieta2');
        });

        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->renameColumn('enfermedad','alergias');
        });

        Schema::table('booking_datos', function (Blueprint $table) {
            $table->dropColumn('alergias');
            $table->dropColumn('tratamiento');
            $table->dropColumn('dieta');

            $table->dropColumn('alergias2');
            $table->dropColumn('enfermedad2');
            $table->dropColumn('medicacion2');
            $table->dropColumn('tratamiento2');
            $table->dropColumn('dieta2');
        });

        Schema::table('booking_datos', function (Blueprint $table) {
            $table->renameColumn('enfermedad'.'alergias');
        });
    }
}
