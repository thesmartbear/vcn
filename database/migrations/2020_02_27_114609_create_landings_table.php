<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_landings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            
            $table->string('name');
            $table->string('slug', 80);
            $table->boolean('es_general')->default(1);
            $table->string('tema', 25);
            
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->text('subtitle2')->nullable(); //separado por comas
            
            $table->text('testimonios')->nullable();
            $table->text('destinos')->nullable();
            $table->text('programas')->nullable();
            
            $table->string('form_title')->nullable();
            $table->text('form_subtitle')->nullable();
            $table->text('form_options1')->nullable();
            $table->text('form_options2')->nullable();

            $table->string('testimonios_title')->nullable();
            $table->text('testimonios_subtitle')->nullable();
            $table->string('seccion_title')->nullable();
            $table->text('seccion_subtitle')->nullable();
            $table->string('nosotros_title')->nullable();
            $table->text('nosotros_subtitle')->nullable();

            $table->text('catalogo_texto')->nullable();

            $table->string('video')->nullable();

            $table->text('gracias')->nullable();
            
            $table->tinyInteger('plataforma_id')->default(0);
            $table->boolean('activo')->default(0);
        });

        Schema::create('cms_landing_testimonios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('name',50)->nullable();
            $table->string('foto',150)->nullable();
            
            $table->string('texto')->nullable();
            $table->string('firma', 40)->nullable();

            $table->boolean('es_principal')->default(0);
            $table->string('texto2')->nullable();
            $table->string('firma2', 40)->nullable();

        });

        Schema::create('cms_landing_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            
            $table->bigInteger('landing_id')->unsigned();
            
            $table->string('nombre', 120)->nullable();
            $table->string('telefono', 25)->nullable();
            $table->string('email', 40)->nullable();
            $table->date('fechanac')->nullable();
            $table->boolean('es_viajero')->default(1);
            $table->string('tutor_nombre', 120)->nullable();
            $table->string('tutor_telefono', 25)->nullable();

            $table->string('destino', 50)->nullable();
            $table->string('duracion', 50)->nullable();

            $table->text('data')->nullable();
            $table->text('info')->nullable();

            $table->foreign('landing_id')
                ->references('id')->on('cms_landings')
                ->onDelete('cascade');
        });

        Schema::create('cms_landing_destinos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('name',50)->nullable();
            $table->string('foto',150)->nullable();
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->text('texto')->nullable();
        });

        Schema::create('cms_landing_programas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('name',50)->nullable();
            $table->string('foto',150)->nullable();
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->text('texto')->nullable();
            $table->string('edad')->nullable();
            $table->string('precio')->nullable();
            $table->string('precio_duracion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_landing_programas');
        Schema::dropIfExists('cms_landing_destinos');
        Schema::dropIfExists('cms_landing_testimonios');
        Schema::dropIfExists('cms_landing_forms');
        Schema::dropIfExists('cms_landings');
    }
}
