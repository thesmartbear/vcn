<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRutaraizToPlataformas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plataformas', function (Blueprint $table) {
            $table->boolean('ruta_raiz')->default(0); //0:categorias, 1:especialidades, 2: paises
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plataformas', function (Blueprint $table) {
            $table->dropColum('ruta_raiz');
        });
    }
}
