<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvocatoriaCerradasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocatoria_cerradas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('convocatory_close_name')->nullable();
            $table->date('convocatory_close_start_date')->nullable();
            $table->date('convocatory_close_end_date')->nullable();
            $table->integer('convocatory_close_duration_weeks')->nullable();
            $table->text('convocatory_close_price_include')->nullable();
            $table->string('convocatory_close_code')->nullable();
            $table->boolean('convocatory_close_status')->nullable();

            $table->integer('course_id')->unsigned();

            $table->integer('plazas')->nullable();
            $table->integer('plazas_reservadas')->nullable();

            $table->decimal('convocatory_close_price', 10)->nullable();
            $table->integer('convocatory_close_currency_id')->unsigned()->nullable();

            $table->boolean('convocatory_semiopen')->nullable();
            $table->boolean('convocatory_semiopen_start_day')->nullable();
            $table->boolean('convocatory_semiopen_end_day')->nullable();

            $table->foreign('course_id')
                ->references('id')->on('cursos')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('convocatoria_cerradas');
    }
}
