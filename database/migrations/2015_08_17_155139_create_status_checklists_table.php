<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_checklists', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->tinyInteger('orden');

            $table->integer('status_id')->unsigned();

            $table->foreign('status_id')
                ->references('id')->on('statuses')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('status_checklists');
    }
}
