<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCatalogosToSoliciutdes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->boolean('catalogo_envio')->default(0);
            $table->string('catalogo_envio_log')->nullable();
            $table->boolean('catalogo_enviado')->default(0);
            $table->string('catalogo_enviado_log')->nullable();
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->text('avisos_catalogo')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->text('avisos_catalogo')->nullable();
        });

        $aviso = [ 'id'=> 124,
            'name' => 'solicitud.catalogo',
            'trigger'=> 'Aviso envío catálogo',
            'destino'=> 0,
            'destino_notas'=> 'Asignado',
            'template'=> 'solicitud_catalogo',
            'asunto'=> 'Enviar catálogo',
            'contenido'=> null
        ];

        DB::table('system_mails')->insert($aviso);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->dropColumn(['catalogo_envio','catalogo_enviado','catalogo_envio_log','catalogo_enviado_log']);
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('avisos_catalogo');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('avisos_catalogo');
        });

        DB::table('system_mails')->where('id',124)->delete();
    }
}
