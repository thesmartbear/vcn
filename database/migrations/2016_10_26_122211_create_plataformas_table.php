<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlataformasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plataformas', function (Blueprint $table) {

            $table->string('tema',25);
            $table->string('logo')->default('logo.png');
            $table->string('logoweb')->default('logoweb.png');
            $table->string('idioma')->default('es');
            $table->string('idiomas')->default('es');
            $table->string('sufijo',12)->nullable();

            $table->char('moneda',3)->default('EUR');
            $table->string('moneda_format',10)->default('%s %s');
            $table->char('moneda_locale',5)->default('es_ES');
            $table->char('moneda_miles',1)->default('.');
            $table->char('moneda_decimales',1)->default(',');

            $table->boolean('area')->default(0);
            $table->boolean('facturas')->default(0);

            $table->json('status_booking')->nullable();
            $table->json('status_solicitud')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plataformas', function (Blueprint $table) {
            $table->dropColumn('tema');
            $table->dropColumn('logo');
            $table->dropColumn('logoweb');
            $table->dropColumn('idioma');
            $table->dropColumn('idiomas');
            $table->dropColumn('sufijo');

            $table->dropColumn('moneda_name');
            $table->dropColumn('moneda_format');
            $table->dropColumn('moneda_locale');
            $table->dropColumn('moneda_miles');
            $table->dropColumn('moneda_decimales');

            $table->dropColumn('area');
            $table->dropColumn('facturas');

            $table->dropColumn('status_booking');
            $table->dropColumn('status_solicitud');
        });
    }
}
