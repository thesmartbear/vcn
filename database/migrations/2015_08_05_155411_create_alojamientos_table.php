<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlojamientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alojamientos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('center_id')->unsigned();

            $table->string('accommodation_name')->nullable();
            $table->text('accommodation_description')->nullable();

            $table->integer('accommodation_type_id')->unsigned();

            $table->foreign('center_id')
                ->references('id')->on('centros')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('accommodation_type_id')
                ->references('id')->on('alojamiento_tipos')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alojamientos');
    }
}
