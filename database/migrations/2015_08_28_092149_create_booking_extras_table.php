<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_extras', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('booking_id')->unsigned();
            $table->integer('extra_id')->unsigned();

            $table->integer('moneda_id')->unsigned();

            $table->boolean('tipo')->default(0); //0:curso, 1:centro, 2:generico
            $table->decimal('precio',10)->nullable();
            $table->tinyInteger('unidades')->default(0);

            $table->foreign('booking_id')
                ->references('id')->on('bookings')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_extras');
    }
}
