<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeViajeroDatosIdioma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->renameColumn('idioma', 'idioma_nivel');
        });
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->string('idioma')->default('Inglés');
        });

        Schema::table('booking_datos', function (Blueprint $table) {
            $table->renameColumn('idioma', 'idioma_nivel');
        });
        Schema::table('booking_datos', function (Blueprint $table) {
            $table->string('idioma')->default('Inglés');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->dropColumn('idioma');
        });
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->renameColumn('idioma_nivel', 'idioma');
        });

        Schema::table('booking_datos', function (Blueprint $table) {
            $table->dropColumn('idioma');
        });
        Schema::table('booking_datos', function (Blueprint $table) {
            $table->renameColumn('idioma_nivel', 'idioma');
        });
    }
}
