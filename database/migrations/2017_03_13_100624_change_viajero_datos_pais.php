<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeViajeroDatosPais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ini_set('memory_limit', '500M');
        set_time_limit(0);

        if (Schema::hasColumn('viajero_datos', 'pais'))
        {
            Schema::table('viajero_datos', function (Blueprint $table) {
                $table->dropColumn('provincia');
                $table->dropColumn('pais');
            });
        }

        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->string('provincia');
            $table->string('pais');
        });

        $viajerodatos = \VCN\Models\Leads\ViajeroDatos::where('pais_id','>',0)->orWhere('provincia_id','>',0);
        // echo "Total: ". $viajerodatos->count();

        foreach($viajerodatos->get() as $vd)
        {
            $vd->provincia = $vd->provincia_model?$vd->provincia_model->name:"";
            $vd->pais = $vd->pais_model?$vd->pais_model->name:"";
            $vd->save();
        }

        // Schema::table('viajero_datos', function (Blueprint $table) {
        //     $table->dropColumn('provincia_id');
        //     $table->dropColumn('pais_id');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->dropColumn('provincia');
            $table->dropColumn('pais');
        });
    }
}
