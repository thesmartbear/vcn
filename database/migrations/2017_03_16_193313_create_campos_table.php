<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCamposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name'); //snake_case
            $table->text('descripcion')->nullable();
            $table->string('default')->nullable();
            $table->string('modelo');
            $table->boolean('tipo')->default(0); //0:check, 1:valor, 2:check+valor
            $table->boolean('textarea')->default(0); //text/textarea

            $table->json('traduccion')->nullable();
            $table->boolean('activo')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campos');
    }
}
