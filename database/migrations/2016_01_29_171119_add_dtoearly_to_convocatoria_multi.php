<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDtoearlyToConvocatoriaMulti extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->integer('dto_early')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->dropColumn('dto_early');
        });
    }
}
