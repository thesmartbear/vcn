<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBookingExtrasCampos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_extras', function (Blueprint $table) {
            $table->boolean('required')->nullable()->default(0);
            $table->string('name')->nullable();
            $table->integer('tipo_unidad')->unsigned()->default(0);
            $table->integer('unidad_id')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_extras', function (Blueprint $table) {
            $table->dropColumn('required');
            $table->dropColumn('name');
            $table->dropColumn('tipo_unidad');
            $table->dropColumn('unidad_id');
        });
    }
}
