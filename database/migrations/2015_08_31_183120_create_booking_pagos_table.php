<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_pagos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('booking_id')->unsigned();

            $table->date('fecha');
            $table->decimal('importe', 10);
            $table->tinyInteger('tipo');
            $table->text('notas')->nullable();

            $table->integer('user_id')->unsigned();

            $table->foreign('booking_id')
                ->references('id')->on('bookings')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_pagos');
    }
}
