<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigoContableBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->string('contable_code')->nulllable();
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->string('contable')->nulllable();
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->string('contable')->nulllable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->string('contable')->nulllable();
        });

        Schema::table('subcategoria_detalles', function (Blueprint $table) {
            $table->string('contable')->nulllable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('contable_code');
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('contable');
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('contable');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('contable');
        });

        Schema::table('subcategoria_detalles', function (Blueprint $table) {
            $table->dropColumn('contable');
        });
    }
}
