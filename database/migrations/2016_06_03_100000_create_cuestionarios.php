<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuestionarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuestionarios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->string('form');
            $table->string('destino',20);
            $table->boolean('test')->default(0); //Si es test de nivel o cuestionario

            $table->boolean('activo')->default(0);
        });

        Schema::create('cuestionario_vinculados', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('cuestionario_id')->unsigned();

            $table->string('modelo');
            $table->integer('modelo_id')->unsigned();

            $table->foreign('cuestionario_id')
                ->references('id')->on('cuestionarios')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('cuestionario_respuestas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('cuestionario_id')->unsigned();

            //viajero o tutor
            $table->integer('viajero_id')->unsigned()->nullable();
            $table->integer('tutor_id')->unsigned()->nullable();
            $table->integer('booking_id')->unsigned();

            $table->tinyInteger('estado')->default(0); //0:pendiente, 1: entregado, 2: aceptado

            $table->json('respuestas');
            $table->json('puntuaciones');
            $table->integer('resultado')->nullable();
            $table->integer('nivel')->nullable();

            $table->foreign('cuestionario_id')
                ->references('id')->on('cuestionarios')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('booking_id')
                ->references('id')->on('bookings')
                ->onDelete('cascade')->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cuestionario_respuestas');
        Schema::drop('cuestionario_vinculados');
        Schema::drop('cuestionarios');
    }
}
