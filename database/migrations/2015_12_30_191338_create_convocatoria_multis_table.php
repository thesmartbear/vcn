<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvocatoriaMultisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocatoria_multis', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->decimal('precio', 10)->nullable();
            $table->decimal('precio_extra', 10)->nullable();
            $table->integer('moneda_id')->unsigned();

            $table->string('cursos_id')->nullable(); //array comas

            $table->boolean('activa')->default(0);

        });

        Schema::create('convocatoria_multi_semanas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('convocatory_id')->unsigned();

            $table->tinyInteger('semana');
            $table->date('desde');
            $table->date('hasta');
            $table->integer('plazas_totales')->nullable();
            $table->integer('plazas_bloqueadas')->nullable();

            $table->foreign('convocatory_id')
                ->references('id')->on('convocatoria_multis')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('convocatoria_multi_especialidades', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('convocatory_id')->unsigned();

            $table->string('name');
            $table->decimal('precio', 10)->nullable();
            $table->string('semanas')->nullable(); //array comas (periodos)

            $table->foreign('convocatory_id')
                ->references('id')->on('convocatoria_multis')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('booking_multis', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('booking_id')->unsigned();
            $table->integer('especialidad_id')->unsigned();
            $table->integer('semana_id')->unsigned();
            $table->tinyInteger('n');
            $table->decimal('precio', 10)->nullable();

            $table->foreign('booking_id')
                ->references('id')->on('bookings')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('semana_id')
                ->references('id')->on('convocatoria_multi_semanas')
                ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('especialidad_id')
            //     ->references('id')->on('convocatoria_multi_especialidades')
            //     ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('convocatory_multi_id')->unsigned();
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->boolean('es_convocatoria_multi')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_multis');
        Schema::drop('convocatoria_multi_semanas');
        Schema::drop('convocatoria_multi_especialidades');
        Schema::drop('convocatoria_multis');


        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('convocatory_multi_id');
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('es_convocatoria_multi');
        });
    }
}
