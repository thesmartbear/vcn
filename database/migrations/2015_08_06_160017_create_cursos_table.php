<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('course_name')->nullable();
            $table->text('course_slug')->nullable();

            // $table->string('course_category')->nullable();
            // $table->string('course_subcategory')->nullable();

            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('subcategory_id')->unsigned()->nullable();
            $table->integer('subcategory_det_id')->unsigned()->nullable();

            $table->string('course_age')->nullable();
            $table->string('course_age_range')->nullable();
            $table->text('course_summary')->nullable();
            $table->text('course_tags')->nullable();
            $table->text('course_seo_url')->nullable();
            $table->boolean('course_active')->nullable();
            $table->boolean('course_highlight')->nullable();
            $table->boolean('course_promo')->nullable();
            $table->text('course_content')->nullable();
            $table->text('course_activities')->nullable();
            $table->text('course_excursions')->nullable();
            $table->text('course_timetable')->nullable();
            $table->text('course_testimonials_parents')->nullable();
            $table->text('course_testimonials_campers')->nullable();
            $table->text('course_provider_url')->nullable();
            $table->decimal('course_booking_amount', 10)->nullable();
            $table->text('course_language')->nullable();
            $table->string('course_minimun_language')->nullable();
            $table->text('course_images')->nullable();
            $table->string('course_accommodation', 45)->nullable();
            $table->text('course_seo_title')->nullable();
            $table->text('course_seo_description')->nullable();
            $table->text('course_seo_tags')->nullable();
            $table->integer('course_language_sessions')->nullable();
            $table->text('course_video')->nullable();

            $table->integer('center_id')->unsigned();

            $table->foreign('center_id')
                ->references('id')->on('centros')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cursos');
    }
}
