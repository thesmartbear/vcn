<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursoExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso_extras', function (Blueprint $table) {
            $table->increments('id');

            $table->string('course_extras_name')->nullable();
            $table->text('course_extras_description', 65535)->nullable();

            $table->integer('course_extras_unit')->nullable();
            $table->integer('course_extras_unit_id')->unsigned()->default(0);

            $table->decimal('course_extras_price', 10)->nullable();

            $table->integer('course_extras_currency_id')->unsigned();

            $table->boolean('course_extras_required')->nullable()->default(0);

            $table->integer('course_id')->unsigned();

            $table->foreign('course_id')
                ->references('id')->on('cursos')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('course_extras_currency_id')
                ->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('course_extras_unit_id')
            //     ->references('id')->on('extra_unidades')
            //     ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('curso_extras');
    }
}
