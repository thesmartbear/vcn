<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlojamientoCuotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alojamiento_cuotas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('accommodations_quota_name')->nullable();
            $table->integer('accommodations_quota_currency_id')->nullable();
            $table->decimal('accommodations_quota_price', 10)->nullable();

            $table->integer('accommodation_id')->unsigned();

            $table->foreign('accommodation_id')
                ->references('id')->on('alojamientos')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alojamiento_cuotas');
    }
}
