<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPromoFechasCambioFijo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('promo_cambio_fijo');
            $table->dropColumn('promo_cambio_fijo_fecha');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('promo_cambio_fijo');
            $table->dropColumn('promo_cambio_fijo_fecha');
        });


        Schema::table('categorias', function (Blueprint $table) {
            $table->json('promo_cambio_fijo_fechas')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->json('promo_cambio_fijo_fechas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('promo_cambio_fijo_fechas');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('promo_cambio_fijo_fechas');
        });


        Schema::table('categorias', function (Blueprint $table) {
            $table->boolean('promo_cambio_fijo')->default(0);
            $table->date('promo_cambio_fijo_fecha')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->boolean('promo_cambio_fijo')->default(0);
            $table->date('promo_cambio_fijo_fecha')->nullable();
        });
    }
}
