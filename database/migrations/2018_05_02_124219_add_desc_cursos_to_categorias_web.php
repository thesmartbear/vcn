<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescCursosToCategoriasWeb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->text('desc_cursos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->dropColumn('desc_cursos');
        });
    }
}
