<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlataformaFacturaNumeros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plataforma_factura_numeros', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->tinyInteger('plataforma');
            $table->integer('any');
            $table->string('numero')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plataforma_factura_numeros');
    }
}
