<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPromoCambioFijo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('promo_cambio_fijo')->default(0);
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->boolean('promo_cambio_fijo')->default(0);
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->boolean('promo_cambio_fijo')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
           $table->dropColumn('promo_cambio_fijo');
        });

        Schema::table('categorias', function (Blueprint $table) {
           $table->dropColumn('promo_cambio_fijo');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
           $table->dropColumn('promo_cambio_fijo');
        });
    }
}
