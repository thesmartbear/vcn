<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajeroLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajero_logs', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('viajero_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->integer('status_id')->unsigned(); //??
            $table->integer('category_id')->unsigned(); //??
            $table->integer('subcategory_id')->unsigned(); //??

            $table->integer('asign_to')->unsigned()->nullable();

            $table->string('tipo')->nullable();
            $table->text('notas')->nullable();

            $table->foreign('viajero_id')
                ->references('id')->on('viajeros')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('viajero_logs');
    }
}
