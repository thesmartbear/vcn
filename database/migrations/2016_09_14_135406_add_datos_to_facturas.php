<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDatosToFacturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_facturas', function (Blueprint $table) {
            $table->json('datos')->nullable();
            $table->json('precio_total')->nullable();
            $table->text('monedas')->nullable();
            $table->decimal('divisa_variacion',10);
            $table->string('total_divisa_txt',25);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_facturas', function (Blueprint $table) {
            $table->dropColumn('datos');
            $table->dropColumn('precio_total');
            $table->dropColumn('monedas');
            $table->dropColumn('divisa_variacion');
            $table->dropColumn('total_divisa_txt');
        });
    }
}
