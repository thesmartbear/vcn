<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFirmasBlob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajero_archivos', function (Blueprint $table) {

            $table->longText('firmas')->change();

        });

        Schema::table('bookings', function (Blueprint $table) {

            $table->longText('firmas')->change();

        });

        Schema::table('viajeros', function (Blueprint $table) {

            $table->longText('firma')->change();

        });

        Schema::table('tutores', function (Blueprint $table) {

            $table->longText('firma')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajero_archivos', function (Blueprint $table) {

            $table->text('firma')->change();

        });

        Schema::table('bookings', function (Blueprint $table) {

            $table->text('firmas')->change();

        });

        Schema::table('viajeros', function (Blueprint $table) {

            $table->text('firma')->change();

        });

        Schema::table('tutores', function (Blueprint $table) {

            $table->text('firma')->change();

        });
    }
}
