<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMaxlengthPhone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proveedores', function (Blueprint $table) {

            $table->string('contact_email')->change();
            $table->string('contact_number')->change();
            $table->string('contact_mobil')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proveedores', function (Blueprint $table) {

            $table->string('contact_email',45)->change();
            $table->string('contact_number',45)->change();
            $table->string('contact_mobil',45)->change();

        });
    }
}
