<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_mails', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->string('trigger');
            $table->boolean('destino')->default(0);
            $table->string('destino_notas')->nullable();
            $table->string('template', 40)->nullable();
            $table->string('asunto', 140)->nullable();
            $table->text('contenido')->nullable();
        });

        Schema::create('system_mail_idiomas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('mail_id')->unsigned();
            $table->string('idioma',3)->default(0);
            $table->string('asunto', 140)->nullable();
            $table->text('contenido')->nullable();

            $table->foreign('mail_id')
                ->references('id')->on('system_mails')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_mail_idiomas');
        Schema::dropIfExists('system_mails');
    }
}
