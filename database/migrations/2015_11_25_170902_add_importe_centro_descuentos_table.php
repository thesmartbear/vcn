<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImporteCentroDescuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('centro_descuentos', function (Blueprint $table) {
            $table->boolean('tipo')->default(0); //0=porcentaje, 1=importe
            $table->float('importe');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('centro_descuentos', function (Blueprint $table) {
            $table->dropColumn('tipo');
            $table->dropColumn('importe');
        });
    }
}
