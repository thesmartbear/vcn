<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFacturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('booking_id')->unsigned();

            $table->integer('grup_id')->unsigned()->nullable(); //facturas agrupadas y las relacionadas tendrán todas el mismo número

            $table->date('fecha');
            $table->integer('numero');
            $table->decimal('total', 10)->nullable();

            $table->tinyInteger('plataforma')->default(0);
            $table->integer('user_id')->unsigned();

            $table->boolean('parcial')->default(0);
            $table->decimal('parcial_importe', 10)->nullable();
            $table->decimal('parcial_porcentaje', 10)->nullable();


            $table->foreign('booking_id')
                ->references('id')->on('bookings')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('booking_facturas_grup', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->date('fecha');
            $table->integer('numero');
            $table->decimal('total', 10)->nullable();

            $table->json('bookings');

            $table->tinyInteger('plataforma')->default(0);
            $table->integer('user_id')->unsigned();

        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->boolean('no_facturar')->default(0);
        });
        Schema::table('subcategorias', function (Blueprint $table) {
            $table->boolean('no_facturar')->default(0);
        });
        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('no_facturar')->default(0);
            $table->json('facturas');
        });
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->boolean('no_facturar')->default(0);
        });
        Schema::table('prescriptores', function (Blueprint $table) {
            $table->boolean('no_facturar')->default(0);
        });

        Schema::table('plataformas', function (Blueprint $table) {
            $table->text('factura_pie')->nullable();
            $table->text('factura_iva_pie')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_facturas_grup');
        Schema::drop('booking_facturas');

        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('no_facturar');
        });
        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('no_facturar');
        });
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('no_facturar');
            $table->dropColumn('facturas');
        });
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('no_facturar');
        });
        Schema::table('prescriptores', function (Blueprint $table) {
            $table->dropColumn('no_facturar');
        });

        Schema::table('plataformas', function (Blueprint $table) {
            $table->dropColumn('factura_pie');
            $table->dropColumn('factura_iva_pie');
        });
    }
}
