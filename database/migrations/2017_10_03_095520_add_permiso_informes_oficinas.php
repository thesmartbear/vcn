<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermisoInformesOficinas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oficinas', function (Blueprint $table) {
            $table->boolean('ver_otras_informes')->default(0);
        });

        Schema::table('user_roles', function (Blueprint $table) {
            $table->boolean('ver_oficinas_informes')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oficinas', function (Blueprint $table) {
            $table->dropColumn('ver_otras_informes');
        });

        Schema::table('user_roles', function (Blueprint $table) {
            $table->dropColumn('ver_oficinas_informes');
        });
    }
}
