<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursoIncluyesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocatoria_cerrada_incluyes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('convocatory_id')->unsigned();
            $table->integer('incluye_id')->unsigned();
            $table->tinyInteger('valor');

            $table->foreign('convocatory_id')
                ->references('id')->on('convocatoria_cerradas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('incluye_id')
                ->references('id')->on('incluyes')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('convocatoria_abierta_incluyes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('convocatory_id')->unsigned();
            $table->integer('incluye_id')->unsigned();
            $table->tinyInteger('valor');

            $table->foreign('convocatory_id')
                ->references('id')->on('convocatoria_abiertas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('incluye_id')
                ->references('id')->on('incluyes')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->string('incluye_horario');
        });
        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->string('incluye_horario');
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->text('promo_texto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('convocatoria_cerrada_incluyes');
        Schema::drop('convocatoria_abierta_incluyes');

        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('promo_texto');
        });

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('incluye_horario');
        });

        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->dropColumn('incluye_horario');
        });
    }
}
