<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvocatoriaOfertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocatoria_ofertas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('convocatory_open_deals_name')->nullable();
            $table->dateTime('convocatory_open_deals_valid_start_date')->nullable();
            $table->dateTime('convocatory_open_deals_valid_end_date')->nullable();
            $table->boolean('convocatory_open_deals_first_week_range')->nullable();
            $table->boolean('convocatory_open_deals_second_week_range')->nullable();
            $table->decimal('convocatory_open_deals_price_per_range', 10)->nullable();
            $table->decimal('convocatory_open_deals_commision', 10)->nullable();

            $table->integer('convocatory_id')->unsigned();

            $table->foreign('convocatory_id')
                ->references('id')->on('convocatoria_abiertas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('convocatoria_ofertas');
    }
}
