<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigosToDescuentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('descuento_tipos', function (Blueprint $table) {
            $table->string('codigo')->nullable();
            $table->integer('codigo_origen')->unsigned()->nullable();
            $table->integer('codigo_suborigen')->unsigned()->nullable();
            $table->integer('codigo_suborigendet')->unsigned()->nullable();
            $table->integer('codigo_prescriptor')->unsigned()->nullable();
        });

        Schema::table('booking_descuentos', function (Blueprint $table) {
            $table->string('codigo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('descuento_tipos', function (Blueprint $table) {
            $table->dropColumn(['codigo', 'codigo_origen', 'codigo_suborigen', 'codigo_suborigendet', 'codigo_prescriptor']);
        });

        Schema::table('booking_descuentos', function (Blueprint $table) {
            $table->dropColumn('codigo');
        });
    }
}
