<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeadsFollowUpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leads_follow_up', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('lead_id')->nullable();
			$table->string('lead_stage')->nullable();
			$table->text('lead_note', 65535)->nullable();
			$table->string('lead_category', 45)->nullable();
			$table->integer('lead_assigned_to')->nullable();
			$table->integer('lead_updated_by')->nullable();
			$table->text('lead_attachmend', 65535)->nullable();
			$table->dateTime('created_at')->nullable();
			$table->string('lead_contact_type')->nullable();
			$table->enum('leads_follow_up', array('Yes','No'))->nullable()->default('No');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('leads_follow_up');
	}

}
