<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->increments('id');

            $table->date('fecha')->nullable();

            $table->integer('viajero_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();

            $table->integer('status_id')->unsigned(); //=viajero

            $table->string('ciudad')->nullable();
            $table->string('destinos')->nullable();
            $table->string('cursos')->nullable();

            $table->text('notas')->nullable();

            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('subcategory_id')->unsigned()->nullable();
            $table->integer('subcategory_det_id')->unsigned()->nullable();

            $table->foreign('viajero_id')
                ->references('id')->on('viajeros')
                ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('user_id')
            //     ->references('id')->on('users')
            //     ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('solicitudes');
    }
}
