<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposCursoycentro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->integer('anterior_participantes')->nullable();
            $table->text('anterior_nacionalidades')->nullable();
        });

        Schema::table('centros', function (Blueprint $table) {
            $table->text('lavanderia')->nullable();
            $table->text('internet_uso')->nullable();
            $table->text('normas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('anterior_participantes');
            $table->dropColumn('anterior_nacionalidades');
        });

        Schema::table('centros', function (Blueprint $table) {
            $table->dropColumn('lavanderia');
            $table->dropColumn('internet_uso');
            $table->dropColumn('normas');
        });
    }
}
