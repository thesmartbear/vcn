<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArchivarMotivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitudes', function (Blueprint $table) {

            $table->date('archivar_fecha');
            $table->string('archivar_motivo');
            $table->string('archivar_motivo_nota');

        });

        Schema::table('bookings', function (Blueprint $table) {

            $table->date('archivar_fecha');
            $table->string('archivar_motivo');
            $table->string('archivar_motivo_nota');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->dropColumn('archivar_fecha');
            $table->dropColumn('archivar_motivo');
            $table->dropColumn('archivar_motivo_nota');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('archivar_fecha');
            $table->dropColumn('archivar_motivo');
            $table->dropColumn('archivar_motivo_nota');
        });
    }
}
