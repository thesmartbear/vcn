<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCerradaCambiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocatoria_cerrada_cambios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('convocatory_id')->unsigned();

            $table->integer('cambio_id')->unsigned()->nullable(); //De cual viene (tabla: cambios)

            $table->foreign('convocatory_id')
                ->references('id')->on('convocatoria_cerradas')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('convocatoria_cerrada_cambio_monedas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('cambio_id')->unsigned();
            $table->integer('moneda_id')->unsigned();

            $table->decimal('tc_calculo', 10, 4);
            $table->decimal('tc_folleto', 10, 4);
            $table->decimal('tc_final', 10, 4);

            $table->foreign('cambio_id')
                ->references('id')->on('convocatoria_cerrada_cambios')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('moneda_id')
                ->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('convocatoria_cerrada_cambio_monedas');
        Schema::drop('convocatoria_cerrada_cambios');
    }
}
