<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlataformaToBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->tinyInteger('plataforma')->default(0);
            $table->integer('oficina_id')->unsigned()->nullable();
        });

        Schema::table('solicitudes', function (Blueprint $table) {
            $table->tinyInteger('plataforma')->default(0);
            $table->integer('oficina_id')->unsigned()->default(0);
            $table->tinyInteger('rating')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('plataforma');
            $table->dropColumn('oficina_id');
        });

        Schema::table('solicitudes', function (Blueprint $table) {
            $table->dropColumn('plataforma');
            $table->dropColumn('oficina_id');
            $table->dropColumn('rating');
        });
    }
}
