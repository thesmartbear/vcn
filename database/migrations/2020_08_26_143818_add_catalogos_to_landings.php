<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCatalogosToLandings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_landings', function (Blueprint $table) {
            $table->string('avisos_catalogo')->nullable();
            $table->text('catalogo_gracias')->nullable();
        });

        $aviso = [ 'id'=> 130,
            'name' => 'landing.aviso_catalogo',
            'trigger'=> 'Aviso formulario Landing catálogo',
            'destino'=> 0,
            'destino_notas'=> 'Configuración Landing',
            'template'=> 'landing_form_catalogo',
            'asunto'=> 'Descarga catálogo landing web',
            'contenido'=> null
        ];

        DB::table('system_mails')->insert($aviso);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('system_mails')->where('id',130)->delete();

        Schema::table('cms_landings', function (Blueprint $table) {
            $table->dropColumn('avisos_catalogo');
            $table->dropColumn('catalogo_gracias');
        });
    }
}
