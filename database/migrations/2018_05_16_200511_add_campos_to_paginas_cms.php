<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposToPaginasCms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_paginas', function (Blueprint $table) {
            $table->tinyInteger('orden')->default(0);
            $table->boolean('menu_secundario')->default(0);
            $table->boolean('activo')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_paginas', function (Blueprint $table) {
            $table->dropColumn(['menu_secundario','orden','activo']);
        });
    }
}
