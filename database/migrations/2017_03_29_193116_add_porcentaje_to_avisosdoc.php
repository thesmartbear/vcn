<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPorcentajeToAvisosdoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_aviso_docs', function (Blueprint $table) {
            $table->decimal('notapago_porcentaje', 10)->default(30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_aviso_docs', function (Blueprint $table) {
            $table->dropColumn('notapago_porcentaje');
        });
    }
}
