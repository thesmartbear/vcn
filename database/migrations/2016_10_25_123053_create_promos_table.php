<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_promos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name',60);
            $table->boolean('activo')->default(0);

            $table->integer('seccion_id')->unsigned();

            $table->string('promo1_titulo')->nullable();
            $table->string('promo1_url')->nullable();
            $table->string('promo2_titulo')->nullable();
            $table->string('promo2_url')->nullable();
            $table->string('promo3_titulo')->nullable();
            $table->string('promo3_url')->nullable();

            $table->string('panel_color',7)->nullable();
            $table->string('panel_imagen')->nullable();
            $table->string('panel_titulo')->nullable();
            $table->string('panel_url')->nullable();
            $table->tinyInteger('propietario')->default(0);

            // $table->foreign('seccion_id')
            //     ->references('id')->on('cms_categorias')
            //     ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms_promos');
    }
}
