<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_checklists', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('solicitud_id')->unsigned();
            $table->integer('checklist_id')->unsigned();
            $table->boolean('estado')->default(0);

            $table->integer('user_id')->unsigned();

            $table->foreign('solicitud_id')
                ->references('id')->on('solicitudes')
                ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('user_id')
            //     ->references('id')->on('users')
            //     ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('solicitud_checklists');
    }
}
