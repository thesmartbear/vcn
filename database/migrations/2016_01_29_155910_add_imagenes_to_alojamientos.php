<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagenesToAlojamientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alojamientos', function (Blueprint $table) {
            $table->string('image_dir')->nullable();
            $table->string('image_portada')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alojamientos', function (Blueprint $table) {
            $table->dropColumn('image_dir');
            $table->dropColumn('image_portada');
        });
    }
}
