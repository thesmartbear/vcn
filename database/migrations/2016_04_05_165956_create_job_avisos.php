<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobAvisos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_aviso_docs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->boolean('activo')->default(0);

            $table->string('titulo');
            $table->text('contenido');

        });

        DB::update("ALTER TABLE system_aviso_docs AUTO_INCREMENT = 10;");

        Schema::create('system_avisos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->boolean('tipo'); //1:bookings, 2:leads, 3:documento

            $table->boolean('activo')->default(0);
            $table->tinyInteger('plataforma')->default(0);

            $table->string('cuando_tipo',10)->nullable(); //trigger, diario,semanal,mensual,puntual
            $table->char('cuando_dia',2)->nullable();
            $table->char('cuando_mes',2)->nullable();
            $table->char('cuando_any',4)->nullable(); //puntual
            $table->time('cuando_hora')->nullable();
            $table->json('cuando_trigger'); //booking-status: status

            $table->json('filtros');
            $table->json('filtros_not');

            $table->integer('doc_id')->unsigned();

            $table->string('destino');
            $table->string('destinos');

            // $table->foreign('doc_id')
            //     ->references('id')->on('system_aviso_docs')
            //     ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('system_aviso_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('aviso_id')->unsigned();
            $table->text('notas')->nullable();

            $table->json('enviados')->nullable();
            $table->json('leidos')->nullable();

            // $table->foreign('aviso_id')
            //     ->references('id')->on('job_avisos')
            //     ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('system_aviso_logs');

        Schema::drop('system_avisos');
        Schema::drop('system_aviso_docs');
    }
}
