<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCatalogoToLandings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_landings', function (Blueprint $table) {
            $table->text('catalogo_form_title')->nullable();
            $table->text('catalogo_form_subtitle')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_landings', function (Blueprint $table) {
            $table->dropColumn('catalogo_form_title');
            $table->dropColumn('catalogo_form_subtitle');
        });
    }
}
