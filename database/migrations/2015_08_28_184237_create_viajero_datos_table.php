<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajeroDatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajero_datos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('viajero_id')->unsigned();

            $table->string('nacionalidad')->nullable();
            $table->string('documento', 45)->nullable();
            $table->string('pasaporte')->nullable();
            $table->string('pasaporte_pais')->nullable();
            $table->date('pasaporte_emision')->nullable();
            $table->date('pasaporte_caduca')->nullable();

            $table->text('direccion')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('provincia')->nullable();
            $table->string('pais')->nullable();
            $table->string('cp', 15)->nullable();
            $table->string('idioma')->nullable();
            $table->string('titulacion')->nullable();

            $table->text('alergias')->nullable();
            $table->text('medicacion')->nullable();
            $table->text('hobby')->nullable();
            $table->boolean('fumador')->nullable();
            $table->string('profesion')->nullable();
            $table->string('empresa')->nullable();
            $table->string('curso_anterior')->nullable();

            $table->text('notas')->nullable();

            $table->foreign('viajero_id')
                ->references('id')->on('viajeros')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('viajero_datos');
    }
}
