<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudLogStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_log_status', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('solicitud_id')->unsigned();
            $table->integer('status1')->unsigned();
            $table->integer('status2')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('notas')->nullable();

            $table->foreign('solicitud_id')
                ->references('id')->on('solicitudes')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('solicitud_log_status');
    }
}
