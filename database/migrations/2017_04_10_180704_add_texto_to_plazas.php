<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTextoToPlazas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->text('plazas_txt')->nullable();
        });

        Schema::table('vuelos', function (Blueprint $table) {
            $table->text('plazas_txt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('plazas_txt');
        });

        Schema::table('vuelos', function (Blueprint $table) {
            $table->dropColumn('plazas_txt');
        });
    }
}
