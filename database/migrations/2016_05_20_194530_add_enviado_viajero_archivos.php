<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnviadoViajeroArchivos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajero_archivos', function (Blueprint $table) {
            $table->boolean('notapago_enviado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajero_archivos', function (Blueprint $table) {
            $table->dropColumn('notapago_enviado');
        });
    }
}
