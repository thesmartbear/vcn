<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPdfCondicionesToCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->string('pdf_condiciones')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->string('pdf_condiciones')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('pdf_condiciones');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('pdf_condiciones');
        });
    }
}
