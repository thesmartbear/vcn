<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoToAvisosDoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_aviso_docs', function (Blueprint $table) {
            $table->boolean('tipo')->default(0); //Normal, nota de pago...
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_aviso_docs', function (Blueprint $table) {
            $table->boolean('tipo')->default(0);
        });
    }
}
