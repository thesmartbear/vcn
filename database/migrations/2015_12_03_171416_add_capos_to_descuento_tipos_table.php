<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCaposToDescuentoTiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('descuento_tipos', function (Blueprint $table) {
            $table->boolean('tipo')->default(0); //0=porcentaje, 1=importe
            $table->boolean('tipo_pc')->default(0); //0=total, 1=curso, 2=total-extras
            $table->float('valor');
            $table->integer('moneda_id')->unsigned();
            $table->tinyInteger('propietario')->default(0);

            $table->renameColumn('discount_type_name','name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('descuento_tipos', function (Blueprint $table) {
            $table->dropColumn('tipo');
            $table->dropColumn('tipo_pc');
            $table->dropColumn('valor');
            $table->dropColumn('moneda_id');
            $table->dropColumn('propietario');
            $table->renameColumn('name','discount_type_name');
        });
    }
}
