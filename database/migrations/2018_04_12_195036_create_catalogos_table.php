<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_catalogos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('grupo');
            $table->string('name');
            $table->string('idioma', 5)->nullable();
            $table->string('pdf')->nullable();
            $table->string('imagen')->nullable();
            $table->text('notas')->nullable();

            $table->tinyInteger('plataforma_id')->default(0);
            $table->boolean('activo')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms_catalogos');
    }
}
