<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('lead_account_id');

            $table->string('lead_name')->nullable();
            $table->string('lead_lastname')->nullable();
            $table->string('lead_email')->nullable()->unique('lead_email_UNIQUE');
            $table->string('lead_phone')->nullable();
            $table->string('lead_movil')->nullable();

            $table->string('lead_type')->nullable();

            $table->string('lead_traveler_name')->nullable();
            $table->string('lead_traveler_lastname')->nullable();
            $table->date('lead_traveler_birthday')->nullable();

            $table->string('lead_status')->nullable();
            // $table->dateTime('created_at')->nullable();
            $table->enum('lead_is_client', array('Yes','No'))->nullable()->default('No');
            $table->string('lead_stage')->nullable();
            $table->integer('course_id')->nullable();
            $table->integer('lead_asign_to')->nullable();
            $table->text('lead_additional_information')->nullable();
            $table->string('lead_category', 45)->nullable();
            $table->integer('lead_subcategory')->nullable();

            $table->string('lead_traveler_name2')->nullable();
            $table->string('lead_traveler_lastname2')->nullable();
            $table->date('lead_traveler_birthday2')->nullable();
            $table->string('lead_traveler_name3')->nullable();
            $table->string('lead_traveler_lastname3')->nullable();
            $table->date('lead_traveler_birthday3')->nullable();
            $table->string('lead_traveler_name4')->nullable();
            $table->string('lead_traveler_lastname4')->nullable();
            $table->date('lead_traveler_birthday4')->nullable();
            $table->string('lead_traveler_name5')->nullable();
            $table->string('lead_traveler_lastname5')->nullable();
            $table->date('lead_traveler_birthday5')->nullable();
            $table->string('lead_traveler_name6')->nullable();
            $table->string('lead_traveler_lastname6')->nullable();
            $table->date('lead_traveler_birthday6')->nullable();
            $table->string('lead_traveler_name7')->nullable();
            $table->string('lead_traveler_lastname7')->nullable();
            $table->date('lead_traveler_birthday7')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leads');
    }
}
