<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMasCamposCategoriaWeb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->boolean('cerrado')->default(0);
            $table->string('inscripcion')->nullable();
            $table->json('campos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->dropColumn('cerrado');
            $table->dropColumn('inscripcion');
            $table->dropColumn('campos');
        });
    }
}
