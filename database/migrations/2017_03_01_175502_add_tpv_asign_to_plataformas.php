<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTpvAsignToPlataformas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plataformas', function (Blueprint $table) {
            $table->integer('tpv_asign_to')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plataformas', function (Blueprint $table) {
            $table->dropColumn('tpv_asign_to');
        });
    }
}
