<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposToLandings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_landings', function (Blueprint $table) {
            $table->string('imagen')->nullable();
            $table->boolean('bloque_catalogo')->default(1);
            $table->boolean('bloque_testimonios')->default(1);
            $table->boolean('bloque_porque')->default(1);
            $table->text('porque')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_landings', function (Blueprint $table) {
            $table->dropColumn([
                'imagen', 'bloque_catalogo', 'bloque_testimonios', 'bloque_porque', 'porque'
            ]);
        });
    }
}
