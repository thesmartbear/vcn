<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informe_ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->smallInteger('any')->unsigned();
            $table->tinyInteger('semana')->unsigned();

            $table->integer('oficina_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('subcategory_id')->unsigned()->nullable();
            $table->integer('subcategory_det_id')->unsigned()->nullable();

            $table->integer('inscripciones');
            $table->integer('semanas');

            $table->json('bookings');

            // $table->foreign('oficina_id')
            //     ->references('id')->on('oficinas')
            //     ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('category_id')
            //     ->references('id')->on('categorias')
            //     ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('informe_ventas');
    }
}
