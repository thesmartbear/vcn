<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeadsNextActionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leads_next_action', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('lead_id')->nullable();
			$table->dateTime('lead_next_action_date')->nullable();
			$table->text('lead_next_action_note', 65535)->nullable();
			$table->string('lead_next_action_contact_type')->nullable();
			$table->integer('created_by')->nullable();
			$table->dateTime('created_at')->nullable();
			$table->enum('lead_next_action_done', array('Yes','No'))->nullable()->default('No');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('leads_next_action');
	}

}
