<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposToBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->decimal('curso_precio_extra',10)->nullable();
            $table->decimal('alojamiento_precio_extra',10)->nullable();
            $table->boolean('es_online')->default(0);
            $table->json('online')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('curso_precio_extra');
            $table->dropColumn('alojamiento_precio_extra');
            $table->dropColumn('es_online');
            $table->dropColumn('online');
        });
    }
}
