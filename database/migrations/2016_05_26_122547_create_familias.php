<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alojamiento_tipos', function (Blueprint $table) {
            $table->boolean('es_familia')->default(0);
        });

        Schema::create('familias', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('center_id')->unsigned();

            $table->string('name');
            $table->string('direccion')->nullable();
            $table->string('cp',10)->nullable();
            $table->string('poblacion')->nullable();
            // $table->integer('provincia_id')->unsigned()->nullable();
            $table->integer('pais_id')->unsigned()->nullable();
            // $table->string('gmap');
            $table->string('telefono')->nullable();
            $table->string('email')->nullable();
            $table->string('skype',40)->nullable();
            $table->text('rrss')->nullable();

            $table->string('distancia')->nullable();
            $table->text('transporte')->nullable();
            $table->text('animales')->nullable();
            $table->text('aficiones')->nullable();
            $table->text('notas')->nullable();

            $table->boolean('habitacion_compartida')->default(0)->nullable();
            $table->text('habitacion_compartida_notas')->nullable();

            // $table->boolean('hijos')->default(0);
            $table->json('hijos')->nullable(); //nombre, fechanac, edad, aficiones
            $table->json('hijos_fuera')->nullable(); //nombre,edad
            $table->json('familia')->nullable(); //nombre,parentesco, edad
            $table->json('adultos')->nullable(); //nombre,edad, horquilla edades, sexo, ocupacion, movil, email, aficiones, foto

            $table->string('image_portada')->nullable();

            $table->boolean('activo')->default(0);

            $table->foreign('center_id')
                ->references('id')->on('centros')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('booking_familias', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('booking_id')->unsigned();
            $table->integer('familia_id')->unsigned();

            $table->date('desde')->nullable();
            $table->date('hasta')->nullable();

            $table->boolean('area')->default(1);

            $table->foreign('booking_id')
                ->references('id')->on('bookings')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('familia_id')
                ->references('id')->on('familias')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alojamiento_tipos', function (Blueprint $table) {
            $table->dropColumn('es_familia');
        });

        Schema::drop('booking_familias');
        Schema::drop('familias');
    }
}
