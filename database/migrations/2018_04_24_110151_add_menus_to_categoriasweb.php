<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMenusToCategoriasweb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->boolean('menu_principal')->default(0);
            $table->boolean('menu_secundario')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->dropColumn('menu_principal');
            $table->dropColumn('menu_secundario');
        });
    }
}
