<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDescuentoEarliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descuentos_early', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->decimal('importe', 10)->nullable();
            $table->date('desde')->nullable();
            $table->date('hasta')->nullable();
            $table->integer('moneda_id')->unsigned();
        });

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->integer('dto_early')->unsigned();
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('dto_early')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('dto_early');
        });

        Schema::drop('descuentos_early');

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('dto_early');
        });
    }
}
