<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBloqueTcoConvocatoriaMultis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->boolean('bloque')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->dropColumn('bloque');
        });
    }
}
