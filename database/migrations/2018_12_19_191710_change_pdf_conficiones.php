<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePdfConficiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('extras', function (Blueprint $table) {
            $table->text('pdf_condiciones')->change();
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->text('pdf_cancelacion')->nullable();
        });
        Schema::table('subcategorias', function (Blueprint $table) {
            $table->text('pdf_cancelacion')->nullable();
        });
        Schema::table('cursos', function (Blueprint $table) {
            $table->text('pdf_cancelacion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('extras', function (Blueprint $table) {
            $table->string('pdf_condiciones')->change();
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('pdf_cancelacion');
        });
        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('pdf_cancelacion');
        });
        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('pdf_cancelacion');
        });
    }
}
