<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRangoCacelacionExtras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('cancelacion')->default(0);
        });

        Schema::table('extras', function (Blueprint $table) {
            $table->decimal('cancelacion_rango1', 10)->nullable();
            $table->decimal('cancelacion_rango2', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('extras', function (Blueprint $table) {
            $table->dropColumn('cancelacion_rango1');
            $table->dropColumn('cancelacion_rango2');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('cancelacion');
        });
    }
}
