<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnidadAlojamientoCuotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alojamiento_cuotas', function (Blueprint $table) {
            $table->boolean('cuota_unidad')->nullable()->default(0);
            $table->integer('cuota_unidad_id')->unsigned()->default(0);
            $table->boolean('required')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alojamiento_cuotas', function (Blueprint $table) {
            $table->dropColumn('cuota_unidad');
            $table->dropColumn('cuota_unidad_id');
            $table->dropColumn('required');
        });
    }
}
