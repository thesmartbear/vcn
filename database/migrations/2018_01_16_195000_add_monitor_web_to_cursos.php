<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMonitorWebToCursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->boolean('monitor_web')->default(0);
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('monitor_name');
            $table->dropColumn('monitor_desc');
            $table->dropColumn('monitor_foto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('monitor_web');
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->string('monitor_name')->nullable();
            $table->text('monitor_desc')->nullable();
            $table->string('monitor_foto')->nullable();
        });
    }
}
