<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChgOrdenStatuToInt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statuses_booking', function (Blueprint $table) {
            $table->integer('orden')->change();
        });

        Schema::table('status_checklists_booking', function (Blueprint $table) {
            $table->integer('orden')->change();
        });

        Schema::table('status_checklists', function (Blueprint $table) {
            $table->integer('orden')->change();
        });

        Schema::table('statuses', function (Blueprint $table) {
            $table->integer('orden')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statuses_booking', function (Blueprint $table) {
            $table->tinyInteger('orden')->change();
        });

        Schema::table('status_checklists_booking', function (Blueprint $table) {
            $table->tinyInteger('orden')->change();
        });

        Schema::table('status_checklists', function (Blueprint $table) {
            $table->tinyInteger('orden')->change();
        });

        Schema::table('statuses', function (Blueprint $table) {
            $table->tinyInteger('orden')->change();
        });
    }
}
