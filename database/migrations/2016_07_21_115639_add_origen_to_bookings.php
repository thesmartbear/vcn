<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrigenToBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('origen_id')->unsigned()->nullable();
            $table->integer('suborigen_id')->unsigned()->nullable();
            $table->integer('suborigendet_id')->unsigned()->nullable();
        });

        //Actualizamos el valor
        foreach( \VCN\Models\Bookings\Booking::all() as $booking )
        {
            $booking->origen_id = $booking->viajero->origen_id?$booking->viajero->origen_id:0;
            $booking->suborigen_id = $booking->viajero->suborigen_id?$booking->viajero->suborigen_id:0;
            $booking->suborigendet_id = $booking->viajero->suborigendet_id?$booking->viajero->suborigendet_id:0;
            $booking->save();
        }

        Schema::table('informe_ventas', function (Blueprint $table) {
            $table->decimal('total', 10)->nullable();
            $table->decimal('total_curso', 10)->nullable();
        });

        //Recontamos ventas
        // \VCN\Models\Informes\Venta::procesarBookings();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('origen_id');
            $table->dropColumn('suborigen_id');
            $table->dropColumn('suborigendet_id');
        });

        Schema::table('informe_ventas', function (Blueprint $table) {
            $table->dropColumn('total');
            $table->dropColumn('total_curso');
        });
    }
}
