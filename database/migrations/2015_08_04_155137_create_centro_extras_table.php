<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentroExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centro_extras', function (Blueprint $table) {
            $table->increments('id');

            $table->string('center_extras_name')->nullable();
            $table->text('center_extras_description')->nullable();

            $table->boolean('center_extras_unit')->nullable();
            // $table->enum('center_extras_unit', array('One time fee','Por unidad'))->nullable();

            $table->decimal('center_extras_price', 10)->nullable();

            $table->integer('center_id')->unsigned();
            $table->integer('center_extras_currency_id')->unsigned();

            $table->boolean('center_extras_required')->nullable();

            $table->integer('center_extras_unit_id')->unsigned()->default(0);

            $table->foreign('center_id')
                ->references('id')->on('centros')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('center_extras_currency_id')
                ->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('center_extras_unit_id')
            //     ->references('id')->on('extra_unidades')
            //     ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('centro_extras');
    }
}
