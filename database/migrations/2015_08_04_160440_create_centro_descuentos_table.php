<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentroDescuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centro_descuentos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('center_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('subcategory_id')->unsigned()->nullable();
            $table->integer('subcategory_det_id')->unsigned()->nullable();

            $table->integer('center_discount_percent')->nullable();

            $table->foreign('center_id')
                ->references('id')->on('centros')
                ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('category_id')
            //     ->references('id')->on('categorias')
            //     ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('subcategory_id')
            //     ->references('id')->on('subcategorias')
            //     ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('subcategory_det_id')
            //     ->references('id')->on('subcategoria_detalles')
            //     ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('centro_descuentos');
    }
}
