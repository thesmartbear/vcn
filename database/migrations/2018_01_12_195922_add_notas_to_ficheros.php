<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotasToFicheros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajero_archivos', function (Blueprint $table) {
            $table->text('notas')->nullable();
        });

        Schema::table('documentos', function (Blueprint $table) {
            $table->text('notas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajero_archivos', function (Blueprint $table) {
            $table->dropColumn('notas');
        });

        Schema::table('documentos', function (Blueprint $table) {
            $table->dropColumn('notas');
        });
    }
}
