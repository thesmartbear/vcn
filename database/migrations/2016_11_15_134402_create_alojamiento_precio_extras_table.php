<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlojamientoPrecioExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alojamiento_precio_extras', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('alojamiento_id')->unsigned();

            $table->text('name')->nullable();
            $table->decimal('importe', 10)->nullable();
            $table->integer('moneda_id')->unsigned();

            $table->boolean('duracion')->unsigned()->default(0);
            $table->boolean('duracion_fijo')->unsigned()->default(0);
            $table->boolean('duracion_tipo')->unsigned()->default(0);

            $table->integer('rango1')->unsigned()->nullable();
            $table->integer('rango2')->unsigned()->nullable();

            $table->dateTime('desde')->nullable();
            $table->dateTime('hasta')->nullable();

            $table->foreign('alojamiento_id')
                ->references('id')->on('alojamientos')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('moneda_id')
                ->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alojamiento_precio_extras');
    }
}
