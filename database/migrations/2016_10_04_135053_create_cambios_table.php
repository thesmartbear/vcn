<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCambiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cambios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name',25);
            $table->boolean('activo')->default(0);
            $table->string('categorias')->nullable(); //separados por comas

            $table->tinyInteger('plataforma')->default(0);

        });

        Schema::create('cambio_monedas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('cambio_id')->unsigned();
            $table->integer('moneda_id')->unsigned();

            $table->decimal('tc_calculo', 10, 4);
            $table->decimal('tc_folleto', 10, 4);
            $table->decimal('tc_final', 10, 4);

            $table->foreign('cambio_id')
                ->references('id')->on('cambios')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('moneda_id')
                ->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cambio_monedas');
        Schema::drop('cambios');
    }
}
