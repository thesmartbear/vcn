<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEspecialidadesToDescuentosTipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('descuento_tipos', function (Blueprint $table) {
            $table->boolean('es_multi')->default(0);
            $table->boolean('tipo_multi')->default(0);
            $table->text('especialidad_id')->nullable();
            $table->text('especialidad_importe')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('descuento_tipos', function (Blueprint $table) {
            $table->dropColumn(['es_multi','tipo_multi','especialidad_id','especialidad_importe']);
        });
    }
}
