<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CamposPrescriptores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prescriptores', function (Blueprint $table) {
            $table->dropColumn('provincia');
            $table->dropColumn('pais');

            $table->integer('provincia_id')->unsigned();
            $table->integer('pais_id')->unsigned();

            $table->foreign('provincia_id')
                ->references('id')->on('provincias')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('pais_id')
                ->references('id')->on('paises')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prescriptores', function (Blueprint $table) {
            $table->dropColumn('provincia_id');
            $table->dropColumn('pais_id');

            $table->string('provincia');
            $table->string('pais');
        });
    }
}
