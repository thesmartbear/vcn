<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extras', function (Blueprint $table) {
            $table->increments('id');

            $table->string('generic_name')->nullable();

            $table->boolean('generic_unit')->nullable();
            // $table->enum('generic_unit', array('One time fee','Por unidad'))->nullable();

            $table->decimal('generic_price', 10)->nullable();
            $table->boolean('generic_required')->nullable();

            $table->integer('generic_currency_id')->unsigned();
            $table->integer('generic_unit_id')->unsigned()->default(0);

            $table->foreign('generic_currency_id')
                ->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('generic_unit_id')
            //     ->references('id')->on('extra_unidades')
            //     ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('extras');
    }
}
