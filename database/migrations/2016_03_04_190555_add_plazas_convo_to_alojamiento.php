<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlazasConvoToAlojamiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_plazas', function (Blueprint $table) {
            $table->integer('plazas_proveedor')->nullable();
        });

        Schema::dropIfExists('alojamiento_precios_old');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_plazas', function (Blueprint $table) {
            $table->dropColumn('plazas_proveedor');
        });
    }
}
