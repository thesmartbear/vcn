<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocsModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('modelo')->nullable();
            $table->integer('modelo_id')->unsigned();
            $table->integer('user_id')->unsigned();

            // $table->integer('category_id')->unsigned();

            $table->string('doc');
            $table->tinyInteger('tipo')->default(0); //0:doc, 1:imagen, 2:timetable, ...
            $table->boolean('visible')->default(1);
            $table->string('idioma',5)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documentos');
    }
}
