<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposCmsCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->text('desc_corta')->nullable();
            $table->text('desc_lateral')->nullable();
            $table->string('video_url')->nullable();
        });

        Schema::table('cms_promos', function (Blueprint $table) {
            $table->boolean('promo1_todos')->default(0);
            $table->boolean('promo2_todos')->default(0);
            $table->boolean('promo3_todos')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->dropColumn('desc_corta');
            $table->dropColumn('desc_lateral');
            $table->dropColumn('video_url');
            $table->dropColumn('todos');
        });

        Schema::table('cms_promos', function (Blueprint $table) {
            $table->dropColumn('promo1_todos');
            $table->dropColumn('promo2_todos');
            $table->dropColumn('promo3_todos');
        });
    }
}
