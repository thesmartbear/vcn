<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvisosCursosToCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->string('avisos_cursosweb')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->string('avisos_cursosweb')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('avisos_cursosweb');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('avisos_cursosweb');
        });
    }
}
