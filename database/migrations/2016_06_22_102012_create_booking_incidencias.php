<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingIncidencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_incidencias', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('booking_id')->unsigned();

            $table->dateTime('fecha')->nullable();
            $table->string('tipo')->nullable();
            $table->text('notas')->nullable();

            $table->integer('user_id')->unsigned(); //quien la crea
            $table->integer('asign_to')->unsigned()->nullable(); //asignado a

            $table->boolean('estado')->default(0);
            $table->dateTime('estado_fecha')->nullable();
            $table->integer('estado_user_id')->unsigned(); //quien lo cambia

            $table->foreign('booking_id')
                ->references('id')->on('bookings')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('booking_incidencia_tareas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('incidencia_id')->unsigned();

            $table->dateTime('fecha')->nullable();
            $table->string('tipo')->nullable();
            $table->text('notas')->nullable();

            $table->dateTime('aviso_fecha')->nullable();
            $table->tinyInteger('aviso')->nullable();

            $table->integer('user_id')->unsigned(); //quien la crea
            $table->integer('asign_to')->unsigned()->nullable(); //asignado a

            $table->boolean('estado')->default(0);
            $table->dateTime('estado_fecha')->nullable();
            $table->integer('estado_user_id')->unsigned(); //quien lo cambia

            $table->foreign('incidencia_id')
                ->references('id')->on('booking_incidencias')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('booking_incidencia_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('incidencia_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->integer('asign_to')->unsigned()->nullable();

            $table->string('tipo')->nullable();
            $table->text('notas')->nullable();

            $table->foreign('incidencia_id')
                ->references('id')->on('booking_incidencias')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_incidencia_logs');
        Schema::drop('booking_incidencia_tareas');
        Schema::drop('booking_incidencias');
    }
}
