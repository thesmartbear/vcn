<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBookingsToSystemAvisos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_aviso_logs', function (Blueprint $table) {
            $table->json('bookings')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_aviso_logs', function (Blueprint $table) {
            $table->dropColumn('bookings');
        });
    }
}
