<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocEspecificosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_doc_especificos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->string('idioma',3)->nullable(); //?? por si solo lleva el principal?

            $table->string('modelo')->nullable();
            $table->integer('modelo_id')->unsigned();
            $table->string('doc')->nullable(); //si es descargable
            $table->string('notas')->nullable();
        });

        Schema::create('system_doc_especifico_langs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('doc_id')->unsigned()->nullable();

            $table->string('name');
            $table->string('idioma',3)->nullable();

            $table->string('doc')->nullable(); //si es descargable
            $table->string('notas')->nullable();

            $table->foreign('doc_id')
                ->references('id')->on('system_doc_especificos')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('viajero_archivos', function (Blueprint $table) {
            $table->integer('doc_especifico_id')->unsigned()->nullable();
            $table->boolean('doc_especifico_status')->default(0); //0: pendiente / 1:validado
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->text('avisos_doc')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->text('avisos_doc')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajero_archivos', function (Blueprint $table) {
            $table->dropColumn('doc_especifico_status');
            $table->dropColumn('doc_especifico_id');
        });

        Schema::drop('system_doc_especifico_langs');
        Schema::drop('system_doc_especificos');

        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('avisos_doc');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('avisos_doc');
        });
    }
}
