<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresupuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informe_presupuestos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->date('fecha');

            $table->integer('user_id')->unsigned();
            $table->integer('oficina_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('subcategory_id')->unsigned()->nullable();

            $table->integer('pais_id')->unsigned()->nullable()->default(0);
            $table->integer('proveedor_id')->unsigned()->nullable()->default(0);
            $table->integer('curso_id')->unsigned()->nullable()->default(0);

            $table->integer('presupuestos');
            $table->integer('semanas');

            $table->json('bookings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('informe_presupuestos');
    }
}
