<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFirmas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {

            $table->text('firmas')->nullable();

        });

        Schema::table('viajeros', function (Blueprint $table) {

            $table->text('firma')->nullable();

        });

        Schema::table('tutores', function (Blueprint $table) {

            $table->text('firma')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {

            $table->dropColumn('firmas');

        });

        Schema::table('viajeros', function (Blueprint $table) {

            $table->dropColumn('firma');

        });

        Schema::table('tutores', function (Blueprint $table) {

            $table->dropColumn('firma');

        });   
    }
}
