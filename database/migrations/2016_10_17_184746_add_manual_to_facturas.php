<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManualToFacturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_facturas', function (Blueprint $table) {
            $table->boolean('manual')->default(0);
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('facturas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_facturas', function (Blueprint $table) {
            $table->dropColumn('manual');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->json('facturas');
        });
    }
}
