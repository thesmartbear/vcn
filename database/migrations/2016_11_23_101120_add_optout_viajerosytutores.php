<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOptoutViajerosytutores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajeros', function (Blueprint $table) {
            $table->boolean('optout')->default(0);
            $table->date('optout_fecha')->nullable();
        });

        Schema::table('tutores', function (Blueprint $table) {
            $table->boolean('optout')->default(0);
            $table->date('optout_fecha')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajeros', function (Blueprint $table) {
            $table->dropColumn('optout');
            $table->dropColumn('optout_fecha');
        });

        Schema::table('tutores', function (Blueprint $table) {
            $table->dropColumn('optout');
            $table->dropColumn('optout_fecha');
        });
    }
}
