<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebVisitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informe_web_registros', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            //fecha, semana?, curso_id, visitas, registros_home, registros_info, array viajeros y tutores ids

            $table->tinyInteger('plataforma')->default(0);
            $table->date('fecha');
            $table->smallInteger('any')->unsigned();
            $table->tinyInteger('semana')->unsigned();

            $table->integer('curso_id')->unsigned()->nullable();
            // $table->integer('visitas')->default(0);

            $table->integer('registros_home');
            $table->integer('registros_info');

            $table->json('registros_home_viajeros');
            $table->json('registros_home_tutores');
            $table->json('registros_info_viajeros');
            $table->json('registros_info_tutores');

        });

        Schema::create('informe_web_visitas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->tinyInteger('plataforma')->default(0);
            $table->date('fecha');
            $table->smallInteger('any')->unsigned();
            $table->tinyInteger('semana')->unsigned();

            $table->integer('curso_id')->unsigned()->nullable();
            $table->integer('visitas')->default(0);

            $table->integer('user_id')->unsigned()->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('informe_web_registros');
        Schema::drop('informe_web_visitas');
    }
}
