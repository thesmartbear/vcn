<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFirmasLongtext extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajero_archivos', function (Blueprint $table) {

            //$table->mediumText('firmas')->change();
            DB::statement('ALTER TABLE viajero_archivos MODIFY firmas MEDIUMTEXT');
        });

        Schema::table('bookings', function (Blueprint $table) {

            //$table->mediumText('firmas')->change();
            DB::statement('ALTER TABLE bookings MODIFY firmas MEDIUMTEXT');
        });

        Schema::table('viajeros', function (Blueprint $table) {

            //$table->mediumText('firma')->change();
            DB::statement('ALTER TABLE viajeros MODIFY firma MEDIUMTEXT');
        });

        Schema::table('tutores', function (Blueprint $table) {

            //$table->mediumText('firma')->change();
            DB::statement('ALTER TABLE tutores MODIFY firma MEDIUMTEXT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajero_archivos', function (Blueprint $table) {

            $table->text('firmas')->change();

        });

        Schema::table('bookings', function (Blueprint $table) {

            $table->text('firmas')->change();

        });

        Schema::table('viajeros', function (Blueprint $table) {

            $table->text('firma')->change();

        });

        Schema::table('tutores', function (Blueprint $table) {

            $table->text('firma')->change();

        });
    }
}
