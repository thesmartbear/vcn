<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescripcionCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->text('descripcion')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->text('descripcion')->nullable();
        });

        Schema::table('subcategoria_detalles', function (Blueprint $table) {
            $table->text('descripcion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('descripcion');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('descripcion');
        });

        Schema::table('subcategoria_detalles', function (Blueprint $table) {
            $table->dropColumn('descripcion');
        });
    }
}
