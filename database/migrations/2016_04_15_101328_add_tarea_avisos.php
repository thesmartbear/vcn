<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTareaAvisos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_avisos', function (Blueprint $table) {
            $table->string('tarea')->nullable();
            $table->string('tarea_tipo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_avisos', function (Blueprint $table) {
            $table->dropColumn('tarea');
            $table->dropColumn('tarea_tipo');
        });
    }
}
