<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnyCursoToVentasCurso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('informe_ventas_curso', function (Blueprint $table) {
            $table->smallInteger('curso_any')->unsigned();
            $table->tinyInteger('curso_semana')->unsigned();
        });

        \VCN\Models\Informes\Venta::procesarBookings();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('informe_ventas_curso', function (Blueprint $table) {
            $table->dropColumn('curso_any');
            $table->dropColumn('curso_semana');
        });
    }
}
