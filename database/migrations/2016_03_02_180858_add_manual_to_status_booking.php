<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManualToStatusBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statuses_booking', function (Blueprint $table) {
            $table->boolean('manual')->default(false);
            $table->boolean('plazas')->default(false);
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->date('accommodation_end_date')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statuses_booking', function (Blueprint $table) {
            $table->dropColumn('manual');
            $table->dropColumn('plazas');
        });
    }
}
