<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMontoReservaToBookingsOnline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->tinyInteger('reserva_tipo')->default(0); //0:importe, 1: porcentaje
            $table->decimal('reserva_valor', 10)->nullable();
        });

        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->tinyInteger('reserva_tipo')->default(0); //0:importe, 1: porcentaje
            $table->decimal('reserva_valor', 10)->nullable();
        });

        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->tinyInteger('reserva_tipo')->default(0); //0:importe, 1: porcentaje
            $table->decimal('reserva_valor', 10)->nullable();
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->tinyInteger('reserva_tipo')->default(0); //0:importe, 1: porcentaje
            $table->decimal('reserva_valor', 10)->nullable();

            $table->dropColumn('course_booking_amount');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->tinyInteger('reserva_tipo')->default(0); //0:importe, 1: porcentaje
            $table->decimal('reserva_valor', 10)->nullable();
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->tinyInteger('reserva_tipo')->default(0); //0:importe, 1: porcentaje
            $table->decimal('reserva_valor', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('reserva_tipo');
            $table->dropColumn('reserva_valor');
        });

        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->dropColumn('reserva_tipo');
            $table->dropColumn('reserva_valor');
        });

        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->dropColumn('reserva_tipo');
            $table->dropColumn('reserva_valor');
        });

        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('reserva_tipo');
            $table->dropColumn('reserva_valor');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('reserva_tipo');
            $table->dropColumn('reserva_valor');
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('reserva_tipo');
            $table->dropColumn('reserva_valor');
        });
    }
}
