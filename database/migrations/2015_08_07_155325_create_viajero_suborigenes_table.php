<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajeroSuborigenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //REALMENTE SON PARA: COMO NOS HA CONOCIDO

        Schema::create('viajero_suborigenes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('origen_id')->unsigned();
            $table->string('name')->nullable();

            $table->foreign('origen_id')
                ->references('id')->on('viajero_origenes')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('viajero_suborigenes');
    }
}
