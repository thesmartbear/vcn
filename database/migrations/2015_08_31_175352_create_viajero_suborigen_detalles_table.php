<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajeroSuborigenDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajero_suborigen_detalles', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('suborigen_id')->unsigned();
            $table->string('name')->nullable();

            $table->foreign('suborigen_id')
                ->references('id')->on('viajero_suborigenes')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('viajero_suborigen_detalles');
    }
}
