<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposViajeros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oficinas', function (Blueprint $table) {
            $table->string('iban',30)->nullable()->change();
        });

        Schema::dropIfExists('prescriptores');
        Schema::create('prescriptores', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            //Generales
            $table->string('name');
            $table->string('sucursal')->nullable();
            $table->string('direccion')->nullable();
            $table->string('cp',10)->nullable();
            $table->string('poblacion')->nullable();
            $table->integer('provincia_id')->unsigned()->nullable();
            $table->integer('pais_id')->unsigned()->nullable();

            $table->string('idioma',2)->nullable();
            $table->tinyInteger('propietario')->default(0);

            //Contacto
            $table->string('persona')->nullable();
            $table->string('telefono1')->nullable();
            $table->string('telefono2')->nullable();
            $table->string('email')->nullable();
            $table->text('notas')->nullable();

            //Fiscales
            $table->string('nif')->nullable();
            $table->string('razon_social')->nullable();
            $table->string('domicilio_social')->nullable();

            $table->integer('categoria_id')->unsigned();
            $table->integer('comision_cat_id')->unsigned()->nullable();
            $table->integer('oficina_id')->unsigned()->nullable();

            $table->foreign('categoria_id')
                ->references('id')->on('prescriptor_categorias')
                ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('comision_cat_id')
            //     ->references('id')->on('prescriptor_comisiones_cat')
            //     ->onDelete('cascade')->onUpdate('cascade');

            // $table->foreign('oficina_id')
            //     ->references('id')->on('oficinas')
            //     ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('viajeros', function (Blueprint $table) {
            $table->boolean('es_adulto')->default(0);
            $table->date('archivado_fecha')->nullable();
            $table->string('paisnac',25)->nullable();
        });

        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->text('animales')->nullable();

            $table->integer('provincia_id')->unsigned()->nullable();
            $table->integer('pais_id')->unsigned()->nullable();

            $table->string('empresa_phone')->nullable();

            //hermanos
            $table->string('h1_nom')->nullable();
            $table->string('h1_fnac',10)->nullable();
            $table->string('h2_nom')->nullable();
            $table->string('h2_fnac',10)->nullable();
            $table->string('h3_nom')->nullable();
            $table->string('h3_fnac',10)->nullable();
            $table->string('h4_nom')->nullable();
            $table->string('h4_fnac',10)->nullable();

            $table->string('fact_nif',20)->nullable();
            $table->string('fact_razonsocial')->nullable();
            $table->string('fact_domicilio')->nullable();
            $table->string('fact_cp',10)->nullable();
            $table->string('fact_poblacion',50)->nullable();
            $table->string('fact_concepto')->nullable();
        });

        Schema::table('tutores', function (Blueprint $table) {
            $table->string('emergencia_contacto',80)->nullable();
            $table->string('emergencia_telefono',25)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prescriptores');

        Schema::table('tutores', function (Blueprint $table) {
            $table->dropColumn('emergencia_contacto');
            $table->dropColumn('emergencia_telefono');
        });

        Schema::table('viajero_datos', function (Blueprint $table) {
            $table->dropColumn('animales');

            $table->dropColumn('provincia_id');
            $table->dropColumn('pais_id');

            $table->dropColumn('h1_nom');
            $table->dropColumn('h1_fnac');
            $table->dropColumn('h2_nom');
            $table->dropColumn('h2_fnac');
            $table->dropColumn('h3_nom');
            $table->dropColumn('h3_fnac');
            $table->dropColumn('h4_nom');
            $table->dropColumn('h4_fnac');

            $table->dropColumn('fact_nif');
            $table->dropColumn('fact_razonsocial');
            $table->dropColumn('fact_domicilio');
            $table->dropColumn('fact_cp');
            $table->dropColumn('fact_poblacion');
            $table->dropColumn('fact_concepto');
        });

        Schema::table('viajeros', function (Blueprint $table) {
            $table->dropColumn('es_adulto');
            $table->dropColumn('archivado_fecha');
            $table->dropColumn('paisnac');
        });
    }
}
