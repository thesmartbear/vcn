<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrigenesToVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('informe_ventas', function (Blueprint $table) {
            $table->integer('origen_id')->unsigned()->nullable();
            $table->integer('suborigen_id')->unsigned()->nullable();
            $table->integer('suborigendet_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('informe_ventas', function (Blueprint $table) {
            $table->dropColumn('origen_id');
            $table->dropColumn('suborigen_id');
            $table->dropColumn('suborigendet_id');
        });
    }
}
