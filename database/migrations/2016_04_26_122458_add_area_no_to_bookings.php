<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAreaNoToBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('area')->default(1);
            $table->boolean('area_pagos')->default(1);
        });

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->boolean('area')->default(1);
            $table->boolean('area_pagos')->default(1);
        });

        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->boolean('area')->default(1);
            $table->boolean('area_pagos')->default(1);
        });

        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->boolean('area')->default(1);
            $table->boolean('area_pagos')->default(1);
        });

        Schema::table('prescriptores', function (Blueprint $table) {
            $table->boolean('area')->default(1);
            $table->boolean('area_pagos')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('area');
            $table->dropColumn('area_pagos');
        });

        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('area');
            $table->dropColumn('area_pagos');
        });

        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->dropColumn('area');
            $table->dropColumn('area_pagos');
        });

        Schema::table('convocatoria_multis', function (Blueprint $table) {
            $table->dropColumn('area');
            $table->dropColumn('area_pagos');
        });

        Schema::table('prescriptores', function (Blueprint $table) {
            $table->dropColumn('area');
            $table->dropColumn('area_pagos');
        });
    }
}
