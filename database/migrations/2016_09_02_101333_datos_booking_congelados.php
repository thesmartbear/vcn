<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DatosBookingCongelados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_datos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('booking_id')->unsigned()->default(0);

            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->string('lastname2')->nullable();
            $table->string('full_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('movil')->nullable();
            $table->date('fechanac')->nullable();
            $table->string('paisnac',25)->nullable();
            $table->char('sexo',1)->nullable();

            $table->boolean('es_adulto')->default(0);
            $table->string('emergencia_contacto',80)->nullable();
            $table->string('emergencia_telefono',25)->nullable();

            //viajero_datos

            $table->string('nacionalidad')->nullable();
            $table->string('documento', 45)->nullable();
            $table->string('pasaporte')->nullable();
            $table->string('pasaporte_pais')->nullable();
            $table->date('pasaporte_emision')->nullable();
            $table->date('pasaporte_caduca')->nullable();

            $table->string('tipovia')->nullable();;
            $table->text('direccion')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('provincia')->nullable();
            $table->string('pais')->nullable();
            $table->string('cp', 15)->nullable();
            $table->string('idioma')->nullable();
            $table->string('titulacion')->nullable();

            $table->text('alergias')->nullable();
            $table->text('medicacion')->nullable();
            $table->text('hobby')->nullable();
            $table->text('animales')->nullable();
            $table->boolean('fumador')->nullable();
            $table->string('profesion')->nullable();
            $table->string('empresa')->nullable();
            $table->string('empresa_phone')->nullable();
            $table->string('curso_anterior')->nullable();

            $table->integer('provincia_id')->unsigned()->nullable();
            $table->integer('pais_id')->unsigned()->nullable();

            $table->string('escuela')->nullable();
            $table->boolean('escuela_curso')->nullable();
            $table->boolean('ingles')->default(0);
            $table->string('ingles_academia')->nullable();
            $table->text('hermanos')->nullable();
            $table->string('telefonos')->nullable();
            $table->string('idioma_contacto',2);

            $table->string('h1_nom')->nullable();
            $table->string('h1_fnac',10)->nullable();
            $table->string('h2_nom')->nullable();
            $table->string('h2_fnac',10)->nullable();
            $table->string('h3_nom')->nullable();
            $table->string('h3_fnac',10)->nullable();
            $table->string('h4_nom')->nullable();
            $table->string('h4_fnac',10)->nullable();

            $table->string('fact_nif',20)->nullable();
            $table->string('fact_razonsocial',80)->nullable();
            $table->string('fact_domicilio')->nullable();
            $table->string('fact_cp',10)->nullable();
            $table->string('fact_poblacion',50)->nullable();
            $table->string('fact_concepto')->nullable();

            $table->boolean('cic')->nullable();
            $table->string('cic_nivel',30)->nullable();

            $table->boolean('trinity_exam')->default(0);
            $table->string('trinity_any',4)->nullable();
            $table->tinyInteger('trinity_nivel')->default(0);
            $table->boolean('cic_thau')->default(0); //0: no, 1:bcn, 2:sant cugat

            $table->text('notas')->nullable();

            $table->json('tutores')->nullable();

            $table->foreign('booking_id')
                ->references('id')->on('bookings')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_datos');
    }
}
