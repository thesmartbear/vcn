<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProveedorToVisitas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('informe_web_visitas', function (Blueprint $table) {
            $table->integer('visitas_proveedor')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('informe_web_visitas', function (Blueprint $table) {
            $table->dropColumn('visitas_proveedor');
        });
    }
}
