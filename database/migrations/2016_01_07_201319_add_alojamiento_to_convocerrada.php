<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlojamientoToConvocerrada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->integer('alojamiento_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_cerradas', function (Blueprint $table) {
            $table->dropColumn('alojamiento_id');
        });
    }
}
