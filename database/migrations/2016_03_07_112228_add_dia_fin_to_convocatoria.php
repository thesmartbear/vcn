<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiaFinToConvocatoria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_abiertas', function (Blueprint $table) {
            $table->boolean('convocatory_open_end_day')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_cerrada', function (Blueprint $table) {
            $table->dropColumn('convocatory_open_end_day');
        });
    }
}
