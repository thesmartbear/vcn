<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvisosDocsValores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_aviso_docs', function (Blueprint $table) {
            $table->decimal('notapago_pagar', 10)->default(30);
            $table->boolean('notapago_pagar_tipo')->default(0); //0:porcentaje, 1:importe
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_aviso_docs', function (Blueprint $table) {
            $table->dropColumn(['notapago_pagar', 'notapago_pagar_tipo']);
        });
    }
}
