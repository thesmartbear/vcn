<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVueloEtapasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('vuelos','ida_company'))
        {
            Schema::table('vuelos', function (Blueprint $table) {
                //ida
                $table->dropColumn('ida_company');
                $table->dropColumn('ida_vuelo');
                $table->dropColumn('ida_salida_aeropuerto');
                $table->dropColumn('ida_salida_fecha');
                $table->dropColumn('ida_salida_hora');
                $table->dropColumn('ida_llegada_aeropuerto');
                $table->dropColumn('ida_llegada_fecha');
                $table->dropColumn('ida_llegada_hora');

                //vuelta
                $table->dropColumn('vuelta_company');
                $table->dropColumn('vuelta_vuelo');
                $table->dropColumn('vuelta_salida_aeropuerto');
                $table->dropColumn('vuelta_salida_fecha');
                $table->dropColumn('vuelta_salida_hora');
                $table->dropColumn('vuelta_llegada_aeropuerto');
                $table->dropColumn('vuelta_llegada_fecha');
                $table->dropColumn('vuelta_llegada_hora');
            });
        }

        Schema::table('vuelos', function (Blueprint $table) {

            $table->string('transporte')->nullable(); // vuelo, autobus, barco, tren
            $table->integer('agencia_id')->unsigned();

            $table->string('localizador')->nullable();
            $table->boolean('individual')->default(0);

            $table->date('encuentro_fecha')->nullable();
            $table->time('encuentro_hora')->nullable();
            $table->string('encuentro_lugar')->nullable();
            $table->string('encuentro_terminal')->nullable();
            $table->string('encuentro_punto')->nullable();

            // $table->decimal('precio', 10)->nullable();
            $table->decimal('tasas', 10)->nullable();
            $table->decimal('margen', 10)->nullable();

            // $table->foreign('agencia_id')
            //     ->references('id')->on('agencias')
            //     ->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::create('vuelo_etapas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('vuelo_id')->unsigned();

            $table->boolean('tipo')->nullable(); //0:ida, 1:vuelta

            $table->string('company')->nullable();
            $table->string('vuelo')->nullable();
            $table->string('salida_aeropuerto')->nullable();
            $table->date('salida_fecha')->nullable();
            $table->time('salida_hora')->nullable();
            $table->string('llegada_aeropuerto')->nullable();
            $table->date('llegada_fecha')->nullable();
            $table->time('llegada_hora')->nullable();

            $table->foreign('vuelo_id')
                ->references('id')->on('vuelos')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vuelo_etapas');

        Schema::table('vuelos', function (Blueprint $table) {

            $table->dropColumn('transporte');
            $table->dropColumn('agencia_id');

            $table->dropColumn('localizador');
            $table->dropColumn('individual');

            $table->dropColumn('encuentro_fecha');
            $table->dropColumn('encuentro_hora');
            $table->dropColumn('encuentro_lugar');
            $table->dropColumn('encuentro_terminal');
            $table->dropColumn('encuentro_punto');
            $table->dropColumn('tasas');
            $table->dropColumn('margen');

        });
    }
}
