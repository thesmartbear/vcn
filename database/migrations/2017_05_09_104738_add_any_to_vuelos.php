<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnyToVuelos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vuelos', function (Blueprint $table) {
            $table->smallInteger('any')->unsigned()->nullable();
        });

        foreach(\VCN\Models\Convocatorias\Vuelo::all() as $vuelo)
        {
            $etapa1 = $vuelo->etapas->where('tipo',0)->first();
            if($etapa1 && $etapa1->salida_fecha)
            {
                $vuelo->any = substr($etapa1->salida_fecha,0,4);
                $vuelo->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vuelos', function (Blueprint $table) {
            $table->dropColumn('any');
        });
    }
}
