<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMailPlusinfomodal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $m = [ 'id'=> 131,
            'name' => 'web.informacion',
            'trigger'=> 'Web: formulario información',
            'destino'=> 0,
            'destino_notas'=> 'infobs@britishsummer.com, carme@britishsummer.com',
            'template'=> 'web_informacion', //emails.
            'asunto'=> "Solicitud de información",
            'contenido'=> null
        ];

        DB::table('system_mails')->insert($m);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('system_mails')->where('id',131)->delete();
    }
}
