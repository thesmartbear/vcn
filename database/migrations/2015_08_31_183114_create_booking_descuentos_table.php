<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDescuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_descuentos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('booking_id')->unsigned();

            $table->dateTime('fecha');
            $table->decimal('importe', 10);
            $table->integer('currency_id')->unsigned();
            $table->text('notas')->nullable();

            $table->integer('user_id')->unsigned();

            $table->foreign('booking_id')
                ->references('id')->on('bookings')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('currency_id')
                ->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_descuentos');
    }
}
