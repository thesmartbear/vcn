<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGradoExtBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('grado_ext')->nullable();
        });

        Schema::table('booking_datos', function (Blueprint $table) {
            $table->boolean('grado_ext')->nullable();
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->boolean('es_grado_ext')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('grado_ext');
        });

        Schema::table('booking_datos', function (Blueprint $table) {
            $table->dropColumn('grado_ext');
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('es_grado_ext');
        });
    }
}
