<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableConvocatoriaCerradaPrecio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocatoria_cerrada_precios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('convocatory_id')->unsigned()->nullable();

            $table->integer('moneda_id')->unsigned(); //=> convocatory_close_currency_id
            $table->decimal('precio', 10)->nullable(); //=> convocatory_close_price

            $table->decimal('proveedor', 10)->nullable();
            $table->integer('proveedor_moneda_id')->unsigned();

            $table->decimal('vuelo', 10)->nullable();
            $table->integer('vuelo_moneda_id')->unsigned();

            $table->decimal('seguro', 10)->nullable();
            $table->integer('seguro_moneda_id')->unsigned();

            $table->decimal('gastos', 10)->nullable();
            $table->integer('gastos_moneda_id')->unsigned();

            $table->decimal('monitor', 10)->nullable();
            $table->integer('monitor_moneda_id')->unsigned();

            $table->decimal('mb', 10)->nullable();
            $table->integer('mb_moneda_id')->unsigned();

            $table->decimal('comision', 10)->nullable();
            $table->integer('comision_moneda_id')->unsigned();

            $table->decimal('divisa_variacion', 10)->nullable();
            $table->decimal('divisa_total', 10)->nullable(); //total + variacion
            $table->date('divisa_fecha')->nullable();

            $table->foreign('convocatory_id')
                ->references('id')->on('convocatoria_cerradas')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        // Schema::table('bookings', function (Blueprint $table) {
        //     $table->decimal('total_variacion_divisa', 10)->nullable();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('convocatoria_cerrada_precios');

        // Schema::table('bookings', function (Blueprint $table) {
        //     $table->dropColumn('total_variacion_divisa');
        // });
    }
}
