<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbiertaPrecioExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocatoria_precio_extras', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('convocatory_id')->unsigned();

            $table->text('name')->nullable();
            $table->decimal('importe', 10)->nullable();
            $table->integer('moneda_id')->unsigned();

            $table->boolean('duracion')->unsigned()->default(0);
            $table->boolean('duracion_fijo')->unsigned()->default(0);
            $table->boolean('duracion_tipo')->unsigned()->default(0);

            $table->integer('rango1')->unsigned()->nullable();
            $table->integer('rango2')->unsigned()->nullable();

            $table->dateTime('desde')->nullable();
            $table->dateTime('hasta')->nullable();

            $table->foreign('convocatory_id')
                ->references('id')->on('convocatoria_abiertas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('moneda_id')
                ->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('convocatoria_precio_extras');
    }
}
