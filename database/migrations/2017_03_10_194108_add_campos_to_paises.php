<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposToPaises extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paises', function (Blueprint $table) {
            $table->text('descripcion')->nullable();
            $table->string('imagen')->nullable();

            $table->string('seo_titulo')->nullable();
            $table->text('seo_descripcion')->nullable();
            $table->string('seo_url')->nullable();
            $table->string('seo_keywords')->nullable();

            $table->string('slug')->nullable();

            $table->boolean('activo_web')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paises', function (Blueprint $table) {
            $table->dropColumn(['descripcion', 'imagen', 'seo_titulo', 'seo_descripcion', 'seo_url', 'seo_keywords', 'slug','activo_web']);
        });
    }
}
