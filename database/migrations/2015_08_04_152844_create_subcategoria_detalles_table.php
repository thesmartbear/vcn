<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoriaDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategoria_detalles', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();
            $table->integer('subcategory_id')->unsigned();

            $table->foreign('subcategory_id')
                ->references('id')->on('subcategorias')
                ->onDelete('cascade')->onUpdate('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subcategoria_detalles');
    }
}
