<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposToStatusChecklistBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('status_checklists_booking', function (Blueprint $table) {

            $table->string('curso_id')->nullable();
            $table->string('centro_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('status_checklists_booking', function (Blueprint $table) {

            $table->dropColumn('curso_id');
            $table->dropColumn('centro_id');

        });
    }
}
