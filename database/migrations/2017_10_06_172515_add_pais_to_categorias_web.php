<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaisToCategoriasWeb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->integer('pais_id')->unsigned()->nullable();
            $table->boolean('estructura')->default(0); //1: v1 (vieja), 2: v2 (nueva), 3,4...: futuras
        });

        foreach( \VCN\Models\CMS\CategoriaWeb::all() as $cat)
        {
            $cat->estructura = 1;
            $cat->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_categorias', function (Blueprint $table) {
            $table->dropColumn('pais_id');
            $table->dropColumn('estructura');
        });
    }
}
