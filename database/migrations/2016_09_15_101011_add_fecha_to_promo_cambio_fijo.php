<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFechaToPromoCambioFijo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->date('promo_cambio_fijo_fecha')->nullable();
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->date('promo_cambio_fijo_fecha')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('promo_cambio_fijo_fecha');
        });

        Schema::table('subcategorias', function (Blueprint $table) {
            $table->dropColumn('promo_cambio_fijo_fecha');
        });
    }
}
