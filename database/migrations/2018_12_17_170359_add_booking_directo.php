<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBookingDirecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->boolean('activo_directo')->default(1);
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('es_directo')->default(0);
        });

        Schema::table('informe_ventas', function (Blueprint $table) {
            $table->boolean('es_directo')->default(0);
        });

        Schema::table('informe_ventas_curso', function (Blueprint $table) {
            $table->boolean('es_directo')->default(0);
        });

        //@if(ConfigHelper::canEdit('bookings-directos'))
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('activo_directo');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('es_directo');
        });

        Schema::table('informe_ventas', function (Blueprint $table) {
            $table->dropColumn('es_directo');
        });

        Schema::table('informe_ventas_curso', function (Blueprint $table) {
            $table->dropColumn('es_directo');
        });
    }
}
