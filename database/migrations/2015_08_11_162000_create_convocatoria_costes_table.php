<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvocatoriaCostesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocatoria_costes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('convocatory_open_cost_name')->nullable();
            $table->dateTime('convocatory_open_cost_start_date')->nullable();
            $table->dateTime('convocatory_open_cost_end_date')->nullable();
            $table->boolean('convocatory_open_cost_first_week_range')->nullable();
            $table->boolean('convocatory_open_cost_second_week_range')->nullable();
            $table->decimal('convocatory_open_cost_price_per_range', 10)->nullable();
            $table->decimal('convocatory_open_deals_commision', 10)->nullable();

            $table->integer('convocatory_id')->unsigned();
            $table->integer('convocatories_open_currency_id')->unsigned()->nullable();

            $table->foreign('convocatory_id')
                ->references('id')->on('convocatoria_abiertas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('convocatories_open_currency_id')
                ->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('convocatoria_costes');
    }
}
