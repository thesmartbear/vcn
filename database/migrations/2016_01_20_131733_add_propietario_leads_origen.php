<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPropietarioLeadsOrigen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viajero_origenes', function (Blueprint $table) {
            $table->tinyInteger('propietario')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viajero_origenes', function (Blueprint $table) {
            $table->dropColumn('propietario');
        });
    }
}
