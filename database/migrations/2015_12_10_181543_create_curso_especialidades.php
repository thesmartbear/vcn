<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursoEspecialidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cursos', function (Blueprint $table) {
            $table->dropColumn('especialidad_id');
            $table->dropColumn('subespecialidad_id');
        });

        Schema::create('curso_especialidades', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('curso_id')->unsigned();

            $table->integer('especialidad_id')->unsigned()->nullable();
            $table->string('subespecialidad_id')->nullable();

            $table->foreign('curso_id')
                ->references('id')->on('cursos')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('curso_especialidades');

        // Schema::table('cursos', function (Blueprint $table) {
        //     $table->integer('especialidad_id')->unsigned()->nullable();
        //     $table->string('subespecialidad_id')->nullable();
        // });
    }
}
