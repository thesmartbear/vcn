<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centro_schools', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('center_id')->unsigned();

            $table->string('name');
            $table->string('direccion')->nullable();
            $table->string('cp',10)->nullable();
            $table->string('poblacion')->nullable();
            $table->string('estado')->nullable();
            $table->integer('pais_id')->unsigned()->nullable();
            $table->string('telefono')->nullable();
            $table->string('email')->nullable();
            $table->string('foto')->nullable();
            $table->text('notas')->nullable();
            $table->integer('alumnos')->nullable();

            $table->boolean('activo')->default(0);

            $table->foreign('center_id')
                ->references('id')->on('centros')
                ->onDelete('cascade')->onUpdate('cascade');
            
        });

        Schema::create('booking_schools', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('booking_id')->unsigned();
            $table->integer('school_id')->unsigned();

            $table->date('desde')->nullable();
            $table->date('hasta')->nullable();

            $table->boolean('area')->default(1);

            $table->foreign('booking_id')
                ->references('id')->on('bookings')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('school_id')
                ->references('id')->on('centro_schools')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_schools');
        Schema::drop('centro_schools');
    }
}
