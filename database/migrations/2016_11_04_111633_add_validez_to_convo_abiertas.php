<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValidezToConvoAbiertas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convocatoria_precios', function (Blueprint $table) {
            $table->dateTime('desde')->nullable();
            $table->dateTime('hasta')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convocatoria_precios', function (Blueprint $table) {
            $table->dropColumn('desde');
            $table->dropColumn('hasta');
        });
    }
}
