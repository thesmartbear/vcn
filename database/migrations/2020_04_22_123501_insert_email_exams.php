<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertEmailExams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //examen_viajero
        $mail = [ 'id'=> 126,
            'name' => 'examen.viajero',
            'trigger'=> 'Envío Test online viajero',
            'destino'=> 1,
            'destino_notas'=> 'Viajero',
            'template'=> 'examen_viajero',
            'asunto'=> null,
            'contenido'=> null
        ];
        DB::table('system_mails')->insert($mail);

        $idioma = [
            [
                'mail_id' => 126,
                'idioma'=> 'es',
                'asunto'=> "Test de inglés para [tag]VIAJERO_NAME[/tag]",
                'contenido'=> null
            ]
        ];
        DB::table('system_mail_idiomas')->insert($idioma);

        $idioma = [
            [
                'mail_id' => 126,
                'idioma'=> 'ca',
                'asunto'=> "Test d'anglès: [tag]VIAJERO_NAME[/tag]",
                'contenido'=> null
            ]
        ];
        DB::table('system_mail_idiomas')->insert($idioma);

        //examen_tutor
        $mail = [ 'id'=> 127,
            'name' => 'examen.tutor',
            'trigger'=> 'Envío Test online',
            'destino'=> 2,
            'destino_notas'=> 'Tutores',
            'template'=> 'examen_tutor',
            'asunto'=> null,
            'contenido'=> null
        ];
        DB::table('system_mails')->insert($mail);

        $idioma = [
            [
                'mail_id' => 127,
                'idioma'=> 'es',
                'asunto'=> "Test de inglés",
                'contenido'=> null
            ]
        ];
        DB::table('system_mail_idiomas')->insert($idioma);

        $idioma = [
            [
                'mail_id' => 127,
                'idioma'=> 'ca',
                'asunto'=> "Test d'anglès",
                'contenido'=> null
            ]
        ];
        DB::table('system_mail_idiomas')->insert($idioma);

        //=== AREA ==

        //area.examen_viajero
        $mail = [ 'id'=> 128,
            'name' => 'area.examen.viajero',
            'trigger'=> 'Envío Test online viajero',
            'destino'=> 1,
            'destino_notas'=> 'Viajero',
            'template'=> 'examen_viajero_area',
            'asunto'=> null,
            'contenido'=> null
        ];
        DB::table('system_mails')->insert($mail);

        $idioma = [
            [
                'mail_id' => 128,
                'idioma'=> 'es',
                'asunto'=> "Test de inglés para [tag]VIAJERO_NAME[/tag]",
                'contenido'=> null
            ]
        ];
        DB::table('system_mail_idiomas')->insert($idioma);

        $idioma = [
            [
                'mail_id' => 128,
                'idioma'=> 'ca',
                'asunto'=> "Test d'anglès: [tag]VIAJERO_NAME[/tag]",
                'contenido'=> null
            ]
        ];
        DB::table('system_mail_idiomas')->insert($idioma);

        //area.examen_tutor
        $mail = [ 'id'=> 129,
            'name' => 'area.examen.tutor',
            'trigger'=> 'Envío Test online',
            'destino'=> 2,
            'destino_notas'=> 'Tutores',
            'template'=> 'examen_tutor_area',
            'asunto'=> null,
            'contenido'=> null
        ];
        DB::table('system_mails')->insert($mail);

        $idioma = [
            [
                'mail_id' => 129,
                'idioma'=> 'es',
                'asunto'=> "Test de inglés",
                'contenido'=> null
            ]
        ];
        DB::table('system_mail_idiomas')->insert($idioma);

        $idioma = [
            [
                'mail_id' => 129,
                'idioma'=> 'ca',
                'asunto'=> "Test d'anglès",
                'contenido'=> null
            ]
        ];
        DB::table('system_mail_idiomas')->insert($idioma);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('system_mail_idiomas')->where('mail_id',126)->delete();
        DB::table('system_mails')->where('id',126)->delete();
        DB::table('system_mail_idiomas')->where('mail_id',127)->delete();
        DB::table('system_mails')->where('id',127)->delete();

        DB::table('system_mail_idiomas')->where('mail_id',128)->delete();
        DB::table('system_mails')->where('id',128)->delete();
        DB::table('system_mail_idiomas')->where('mail_id',129)->delete();
        DB::table('system_mails')->where('id',129)->delete();
    }
}
