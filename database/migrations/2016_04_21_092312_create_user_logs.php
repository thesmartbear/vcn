<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('config');
            $table->dropUnique('users_username_unique');
            $table->unique(['username', 'plataforma']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->json('config')->nullable();
        });

        Schema::create('user_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('username');
            $table->boolean('result')->default(0);
            $table->string('ip');
            $table->string('host');

            $table->text('notas')->nullable();

            $table->integer('user_id')->unsigned();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_logs');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('config');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->text('config')->nullable();
        });
    }
}
