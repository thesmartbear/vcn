<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCifToBookingFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_facturas', function (Blueprint $table) {
            $table->string('nif',20)->nullable();
            $table->string('cp',10)->nullable();
        });

        Schema::table('booking_facturas_grup', function (Blueprint $table) {
            $table->string('cp',10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_facturas', function (Blueprint $table) {
            $table->dropColumn('nif');
            $table->dropColumn('cp');
        });

        Schema::table('booking_facturas_grup', function (Blueprint $table) {
            $table->dropColumn('cp');
        });
    }
}
