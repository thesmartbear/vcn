<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitores', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
            $table->string('foto')->nullable();
            $table->text('notas')->nullable();

            $table->string('email');

            $table->tinyInteger('plataforma')->default(0);

            $table->integer('user_id')->unsigned()->default(0);
        });

        Schema::create('monitor_relaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('monitor_id')->unsigned();

            $table->string('modelo')->nullable(); //Centro, Curso, Convocatoria
            $table->integer('modelo_id')->unsigned();

            $table->foreign('monitor_id')
                ->references('id')->on('monitores')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        //Monitores de los cursos
        foreach(\VCN\Models\Cursos\Curso::where('monitor_name','<>','')->whereNotNull('monitor_name')->get() as $curso)
        {
            $data['name'] = $curso->monitor_name;
            $data['foto'] = $curso->monitor_foto;
            $data['notas'] = $curso->monitor_desc;
            $data['plataforma'] = $curso->propietario;

            $monitor = \VCN\Models\Monitores\Monitor::create($data);

            // Relaciones
            $data2['modelo'] = "Curso";
            $data2['modelo_id'] = $curso->id;
            $data2['monitor_id'] = $monitor->id;
            $monitor = \VCN\Models\Monitores\MonitorRelacion::create($data2);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('monitor_relaciones');
        Schema::drop('monitores');
    }
}
