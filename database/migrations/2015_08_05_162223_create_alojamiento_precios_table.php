<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlojamientoPreciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alojamiento_precios', function (Blueprint $table) {
            $table->increments('id');

            $table->text('accommodation_prices_description')->nullable();
            $table->decimal('accommodation_prices_price_per_week', 10)->nullable();
            $table->dateTime('accommodation_prices_start_date')->nullable();
            $table->dateTime('accommodation_prices_end_date')->nullable();

            $table->integer('accommodation_prices_currency_id')->nullable();

            $table->integer('accommodation_id')->unsigned();

            $table->foreign('accommodation_id')
                ->references('id')->on('alojamientos')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alojamiento_precios');
    }
}
