<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspecialidadWebsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_especialidades', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name',60);
            $table->string('titulo');
            $table->integer('especialidad_id')->unsigned()->nullable(); //especialidad padre
            $table->boolean('orden')->default(0);
            $table->boolean('activo')->default(0);

            $table->string('seo_url')->nullable();
            $table->string('seo_titulo')->nullable();
            $table->text('seo_descripcion')->nullable();
            $table->string('seo_keywords')->nullable();

            $table->text('descripcion')->nullable();
            $table->string('color',7)->nullable();
            $table->tinyInteger('propietario')->default(0);

            //condiciones
            $table->string('idioma')->nullable();
            $table->boolean('idioma_excluye')->default(0);
            $table->tinyInteger('tipoc')->default(0);

            $table->string('especialidades')->nullable();
            $table->string('subespecialidades')->nullable();
            $table->boolean('excluye_especialidades')->default(0);
            $table->boolean('excluye_subespecialidades')->default(0);

            $table->string('imagen')->nullable();

            $table->text('desc_corta')->nullable();
            $table->text('desc_lateral')->nullable();
            $table->string('video_url')->nullable();
            $table->string('plantilla')->nullable();

            $table->boolean('es_link')->default(0);
            $table->boolean('link_blank')->default(0);
            $table->string('link')->nullable();

            $table->boolean('cerrado')->default(0);
            $table->string('inscripcion')->nullable();
            $table->json('campos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms_especialidades');
    }
}
