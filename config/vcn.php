<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Configuración BritishSummer
    |--------------------------------------------------------------------------
    |
    */

    'version' => '0.50.0',

    /*
    |--------------------------------------------------------------------------
    | Configuración Permisos y Plataformas
    |--------------------------------------------------------------------------
    |
    */

    // 'permisos' => [
    //     'Operaciones' => ['proveedores', 'centros', 'cursos', 'alojamientos', 'vuelos', 'alojamiento-tipos', 'paises', 'ciudades','provincias'],
    //     'Admin' => ['system','maestros','convocatorias','centro-extras', 'alojamiento-precios', 'curso-extras', 'extra-genericos', 'monedas','traducciones','cms','oficinas','origenes','avisos','cuestionarios'],
    //     'Transporte' => ['convocatoria-plazas','transporte-plazas','transporte-info','plazas-privado'],
    //     'Ventas' => ['viajeros','bookings','descuentos-tipo','descuentos','prescriptores','booking-fechas','notapago'],
    //     'Informes' => ['informes'], //pendiente para cada uno
    // ],

    'permisos' => [
        'Proveedores' => ['proveedores', 'traducciones'],
        'Precios' => ['precios', 'monedas', 'extras', 'descuentos', 'precios-ccerrada', 'precios-cabierta', 'precios-cmulti', 'monedas-cambio', 'precios-vuelo', 'desactivacion-bloque'],
        'Admin' => ['configuracion', 'tablas', 'origenes', 'system', 'booking-fechas-alojamiento', 'booking-borrar', 'oficinas', 'cms', 'booking-firmas', 'booking-cambio-origen', 'monitores', 'superlogin'],
        'Transporte' => ['vuelos', 'convocatoria-plazas', 'transporte-plazas', 'plazas-privado', 'transporte-info'],
        'Ventas' => ['viajeros', 'bookings', 'prescriptores', 'origenes', 'facturas', 'bookings-directos', 'bookings-vuelos', 'viajero-eliminar'],
        'Informes' => ['informes', 'informes-cic'],
        'Listados' => ['viajeros-list', 'tutores-list', 'bookings-list', 'solicitudes-list', 'inscritos-list', 'monitores-list'],
        'Comunicación' => ['avisos', 'cuestionarios', 'cuestionarios-resumen', 'notapago', 'avisos_mailing', 'sos'],
        'Checklists' => ['checklist-bookings', 'checklist-solicitudes'],
        'Dashboard' => ['Resumen', 'Tareas', 'Ventas', 'Incidencias', 'Contactos']
    ],
    'permisos_informes' => ['Listados', 'Operativa', 'Ventas', 'Facturación', 'Vuelos'],

    'plataformas' => [
        0 => 'Todas',
        1 => 'British Summer',
        2 => 'el CIC',
        3 => 'Easy'
    ],

    'temas' => [ //carpetas: public/assets + resources/views/web
        'estudiaryviajar'   => 'Estudiar y Viajar',
        'britishsummer'     => 'British Summer',
        'britishsummerv2'   => 'British Summer v.2',
        'britishsummerv3'   => 'British Summer v.3',
        'studyfuera'        => 'StudyFuera',
        'dippy'             => 'Dippy',
        'bloques'           => 'Bloques',
        'home2020'          => 'Home 2020' //bs-v3
    ],

    'formularios' => [
        'testnivelmaxcamps' => "Test de Nivel MaxCamps",
        'padresjovenes' => "Cuestionario Padres Jóvenes",
        'padresmaxcamps' => "Cuestionario Padres Max Camps",
        'padrescoloniascic' => "Cuestionario Padres Colonias CIC",
        'testnivelcoloniascic' => "Test de Nivel Colonias CIC",
        'opinionjovenes' => "Opinión programas de jóvenes",
    ],

    'landings' => [
        'bs-general-1' => 'Landing BS General v.1',
        'bs-cursos-1' => 'Landing BS Cursos v.1',
    ],

    'examenes' => [
        'test-ingles-1' => 'Test Inglés 2020 v.1',
    ],

    'area'      => true,
    'facturas'  => false,
    'seguro'    => true, //seguro cancelacion auto
    'chat'      => false, //propio
    'nif'       => false,
    'mc_api'    => "a2d823daac0c2f08cd18257c0a08f93b-us12",

    /*
    |--------------------------------------------------------------------------
    | Configuración por Web (hostname)
    |--------------------------------------------------------------------------
    | Utilizar Session::put (Controller)
    |
    */

    //default
    'nombre' => 'Default',
    'web' => 'estudiaryviajar.com',
    'tema' => 'estudiaryviajar',
    'database' => 'mysql',

    'moneda' => 'EUR',
    'moneda_format' => '%s %s',
    'moneda_locale' => 'es_ES',
    'moneda_miles' => '.',
    'moneda_decimales' => ',',

    'propietario' => 0, //0:todos, >0: publicos y sus privados = plataforma->id
    'logo' => 'bslogo.png',

    'booking_status_prereserva' => 8,
    'booking_status_prebooking' => 1, //Reserva
    'booking_status_presalida' => 3,
    'booking_status_overbooking' => 2,
    'booking_status_archivado' => 0,
    'booking_status_cancelado' => 9,
    'booking_status_refund' => 10,
    'booking_status_vuelta' => 6,
    'booking_status_fuera' => 5,
    'booking_status_borrador' => 0,

    'solicitud_status_decidiendo' => 3,
    'solicitud_status_archivado' => 5,
    'solicitud_status_inscrito' => 6,

    'backend' => 1,
    'frontend' => 1,

    'idiomas'   => ['es', 'ca'], //array de idiomas (https://es.wikipedia.org/wiki/ISO_639-1)
    'idioma'    => 'es', //idioma por defecto (y primero en el array)
    // 'timezone'  => 'Europe/Madrid',

    //Nuevo por BBDD:
    //Configuración por host => sustituir . por -
    'viajaconnosotros-loc' => [
        'propietario'    => 1, //plataforma
        'backend'        => 1,
        'frontend'       => 1
    ],
    'vcn-test' => [
        'propietario'    => 1, //plataforma
        'backend'        => 1,
        'frontend'       => 1,
        // 'chat'          => true,
        'nif'           => true,
    ],

    'byl-estudiaryviajar-com' => [
        'propietario'    => 1, //plataforma
        'backend'        => 1,
        'frontend'       => 1
    ],
    'demo-estudiaryviajar-com' => [
        'propietario'    => 1, //plataforma
        'backend'        => 1,
        'frontend'       => 1
    ],

    'vcn-estudiaryviajar-com' => [
        'propietario'    => 1, //plataforma
        'backend'        => 1,
        'frontend'       => 1,
        'nif'           => true,
    ],

    'bs-carlituxman-com' => [
        'propietario'    => 1, //plataforma
        'backend'        => 1,
        'frontend'       => 1,
        'nif'           => true,
    ],
    'test-estudiaryviajar-com' => [
        'propietario'    => 1, //plataforma
        'backend'        => 1,
        'frontend'       => 1,
        'nif'           => true,
    ],
    'vcn2-estudiaryviajar-com' => [
        'propietario'    => 2, //plataforma
        'backend'        => 1,
        'frontend'       => 1
    ],
    'vcn3-estudiaryviajar-com' => [
        'propietario'    => 1, //plataforma
        'backend'        => 1,
        'frontend'       => 1,
        'database'      => 'mysql_dp_test',
    ],

    'testbscic-estudiaryviajar-com' => [
        'propietario'   => 1, //plataforma
        'backend'       => 1,
        'frontend'      => 1,
    ],
    'testsf-estudiaryviajar-com' => [
        'propietario'   => 1, //plataforma
        'backend'       => 1,
        'frontend'      => 1,
    ],

    'bs-estudiaryviajar-com' => [
        'propietario'   => 1, //plataforma
        'backend'       => 1,
        'frontend'      => 0,
        'nif'           => true,
    ],
    'www-britishsummer-com' => [
        'propietario'   => 1,
        'backend'       => 0,
        'frontend'      => 1,
        'nif'           => true,
    ],
    'britishsummer-com' => [
        'propietario'   => 1,
        'backend'       => 0,
        'frontend'      => 1,
        'nif'           => true,
    ],
    'bs-estudiaridiomas-com' => [
        'propietario'   => 1,
        'backend'       => 1,
        'frontend'      => 1,
        'nif'           => true,
    ],
    'cic-estudiaryviajar-com' => [
        'propietario'   => 2, //plataforma
        'backend'       => 1,
        'frontend'      => 0,
        'nif'           => true,
    ],
    'cic-estudiaridiomas-com' => [
        'propietario'   => 2,
        'backend'       => 1,
        'frontend'      => 1,
        'nif'           => true,
    ],
    'viatges-iccic-edu' => [
        'propietario'   => 2,
        'backend'       => 0,
        'frontend'      => 1,
        'nif'           => true,
    ],

    'studyfuera-estudiaryviajar-com' => [
        'propietario'   => 1,
        'backend'       => 1,
        'frontend'      => 0,
    ],
    'sf-estudiaridiomas-com' => [
        'propietario'   => 1,
        'backend'       => 1,
        'frontend'      => 1,
    ],
    'www-studyfuera-com' => [
        'propietario'   => 1,
        'backend'       => 0,
        'frontend'      => 1,
    ],
    'studyfuera-com' => [
        'propietario'   => 1,
        'backend'       => 0,
        'frontend'      => 1,
    ],

    //dippy
    'dp-estudiaryviajar-com' => [
        'propietario'    => 1, //plataforma
        'backend'        => 1,
        'frontend'       => 0
    ],

    'www-dippy-life' => [
        'propietario'    => 1, //plataforma
        'backend'        => 0,
        'frontend'       => 1
    ],

    //DO
    'test-estudiaridiomas-com' => [
        'propietario'    => 1, //plataforma
        'backend'        => 1,
        'frontend'       => 1,
        'nif'           => true,
    ],
    'bs-estudiaridiomas-com' => [
        'propietario'    => 1, //plataforma
        'backend'        => 1,
        'frontend'       => 1,
        'nif'           => true,
    ],
    'cic-estudiaridiomas-com' => [
        'propietario'    => 2, //plataforma
        'backend'        => 1,
        'frontend'       => 1,
        'nif'           => true,
    ],
    'sf-estudiaridiomas-com' => [
        'propietario'   => 1,
        'backend'       => 0,
        'frontend'      => 1,
    ],

    //Estos hay q limpiarlos!!! ya está por BBDD

    'test' => [
        'nombre' => 'BRITISH SUMMER',
        'web' => 'test.vcn2015.webfactional.com',
        'tema' => 'britishsummerv3',
        // 'database' => 'vcn_dbtest', //driver

        'moneda' => 'EUR',
        'moneda_format' => '%s %s',
        'moneda_locale' => 'es_ES',
        'propietario' => 0,
        'logo' => 'bslogo.png',
        'logoweb' => 'bslogoweb.png',
        'plantilla' => 'bstemplate.jpg',
        'sufijo' => 'bs',
        'idiomas'   => ['es', 'ca'],
        'idioma'    => 'es',
    ],

    'vcnbs-com' => [
        'nombre' => 'British Summer',
        'web' => 'vcnbs.com',
        'tema' => 'britishsummerv3',
        'database' => 'mysql',

        'moneda' => 'EUR',
        'moneda_format' => '%s %s', //1: importe (%d/%f/%.2f), 2: moneda (%s)
        'moneda_locale' => 'es_ES',
        'propietario' => 1,
        'logo' => 'bslogo.png',
        'logoweb' => 'bslogoweb.png',
        'plantilla' => 'bstemplate.jpg',
        'sufijo' => 'bs',
        'idiomas'   => ['es', 'ca'],
        'idioma'    => 'es',

        'backend' => 1,
        'frontend' => 1,
        'facturas'  => true,

    ],

    '172-16-0-16' => [
        'nombre' => 'British Summer',
        'web' => 'vcnbs.com',
        'tema' => 'britishsummerv3',
        'database' => 'mysql',

        'moneda' => 'EUR',
        'moneda_format' => '%s %s', //1: importe (%d/%f/%.2f), 2: moneda (%s)
        'moneda_locale' => 'es_ES',
        'propietario' => 1,
        'logo' => 'bslogo.png',
        'logoweb' => 'bslogoweb.png',
        'plantilla' => 'bstemplate.jpg',
        'sufijo' => 'bs',
        'idiomas'   => ['es', 'ca'],
        'idioma'    => 'es',

        'backend' => 1,
        'frontend' => 1,
        'facturas'  => true,

    ],


    'vcncic-com' => [
        'nombre' => 'CIC escola idiomes',
        'web' => 'vcncic.com',
        'tema' => 'britishsummerv3',
        'database' => 'mysql',
        'moneda' => 'EUR',
        'moneda_format' => '%s %s',
        'moneda_locale' => 'es_CA',
        'propietario' => 2,
        'logo' => 'ciclogo.png',
        'logoweb' => 'ciclogoweb.png',
        'plantilla' => 'cictemplate.jpg',
        'sufijo' => 'cic',
        'idiomas'   => ['ca', 'es'],
        'idioma'    => 'ca',

        'backend' => 1,
        'frontend' => 1,
        'facturas'  => true,
    ],

    'vcneasy-com' => [
        'nombre' => 'Easy Idiomas',
        'web' => 'vcneasy.com',
        'tema' => 'britishsummer',
        // 'database' => 'vcn_dbtest', //driver

        'moneda' => 'EUR',
        'moneda_format' => '%s %s',
        'moneda_locale' => 'es_ES',
        'propietario' => 3,
        'logo' => 'easylogo.png',
        'idiomas'   => ['es', 'ca'],
        'idioma'    => 'es',

        'backend' => 1,
        'frontend' => 1,
        'facturas'  => true,
    ],



    'vcndippy-com' => [
        'nombre' => 'Dippy',
        'web' => 'vcndippy.life',
        'tema' => 'dippy',

        'moneda' => 'EUR',
        'moneda_format' => '%s %s',
        'moneda_locale' => 'es_ES',

        'propietario' => 3,
        'logo' => 'dplogo.png',
        'logoweb' => 'dplogoweb.png',
        'plantilla' => 'dptemplate.jpg',
        'sufijo' => 'dp',
        'idiomas'   => ['es', 'ca'],
        'idioma'    => 'es',

        'backend' => 1,
        'frontend' => 1,

        'booking_status_prereserva' => 2,
        'booking_status_prebooking' => 4, //Reserva
        'booking_status_presalida' => 5,
        'booking_status_overbooking' => 3,
        'booking_status_archivado' => 0,
        'booking_status_cancelado' => 9,
        'booking_status_refund' => 10,
        'booking_status_vuelta' => 8,
        'booking_status_fuera' => 7,

        'booking_solicitud_decidiendo' => 5,
        'solicitud_status_archivado' => 2,
        'solicitud_status_inscrito' => 6,

        'plataformas' => [
            0 => 'Todas',
            3 => 'Dippy',
        ],
    ],


    'vcnsf-com' => [
        'nombre' => 'StudyFuera',
        'web' => 'vcnsf.com',
        'tema' => 'studyfuera',
        'database' => 'mysql_sf',

        'moneda' => 'MXN',
        'moneda_format' => '%2$s %1$s',
        'moneda_locale' => 'es_MX',
        'moneda_miles' => ',',
        'moneda_decimales' => '.',

        'propietario' => 4,
        'logo' => 'sflogo.png',
        'logoweb' => 'sflogoweb.png',
        'plantilla' => 'sftemplate.jpg',
        'sufijo' => 'sf',
        'idiomas'   => ['es'],
        'idioma'    => 'es',

        'backend' => 1,
        'frontend' => 1,

        'plataformas' => [
            0 => 'Todas',
            4 => 'StudyFuera',
        ],

        'booking_status_prereserva' => 2,
        'booking_status_prebooking' => 4, //Reserva
        'booking_status_presalida' => 5,
        'booking_status_overbooking' => 3,
        'booking_status_archivado' => 0,
        'booking_status_cancelado' => 9,
        'booking_status_refund' => 10,
        'booking_status_vuelta' => 8,
        'booking_status_borrador' => 0,

        'booking_solicitud_decidiendo' => 4,
        'solicitud_status_archivado' => 2,
        'solicitud_status_inscrito' => 6,
    ],


    /*
    |--------------------------------------------------------------------------
    | Configuración de menús de la aplicacion
    |--------------------------------------------------------------------------
    |
    |
    |
    */

    'menu'  => array(

        'web' => [

            //routes: translated routes / url si no es ruta
            //view: se añadirá web.PLANTILLA. delante automáticamente

            [
                'nom' => 'Test', 'route' => 'web.view', 'view' => ['loqsea'],
                'submenu' => []
            ],
            [
                'nom' => 'Cursos (Demo)', 'route' => 'web.view', 'view' => ['demo-cursos.html'],
                'submenu' => []
            ],
            [
                'nom' => 'Categorias (Demo)', 'route' => 'web.view', 'view' => ['demo-categorias.html'],
                'submenu' => []
            ],
            [
                'nom_trans' => 'quienes-somos', 'route' => 'web.pagina', 'view' => ['quienes-somos'],
                'submenu' => []
            ],
            [
                'nom' => 'Blog', 'url' => '/blog'
            ],
        ],

        //importante!! los padres ya no llevan rol, si hay un hijo aparece
        'manager' => [

            [
                'nom' => 'Info Convocatorias', 'route' => 'manage.convocatorias.cerradas.index.info', 'icono' => 'fa fa-certificate', 'rol' => [0], 'permiso' => 'informes', 'submenu' => [],
            ],

            [
                'nom' => 'Informe Ventas', 'route' => 'manage.informes.ventas', 'icono' => 'fa fa-money fa-fw', 'rol' => [1], 'permiso' => 'informes', 'submenu' => [],
            ],

            [
                'nom' => 'Tipos de cambio', 'route' => 'manage.monedas.index', 'icono' => 'fa fa-money fa-fw',
                'rol' => [1], 'permiso' => 'monedas', 'submenu' => [],
            ],

            [
                'nom' => 'Programas', 'route' => '#menu-programas', 'icono' => 'fa fa-folder fa-fw',
                'rol' => [1], 'permiso' => 'all',
                'submenu' => [
                    [
                        'nom' => 'Proveedores', 'route' => '#menu-proveedores', 'icono' => 'fa fa-folder fa-fw',
                        'rol' => [1], 'permiso' => 'proveedores',
                        'submenu' => [
                            [
                                'nom' => 'Proveedores', 'route' => 'manage.proveedores.index', 'icono' => 'fa fa-truck fa-fw',
                                'rol' => [1], 'permiso' => 'proveedores'
                            ],
                            [
                                'nom' => 'Centros', 'route' => 'manage.centros.index', 'icono' => 'fa fa-home fa-fw',
                                'rol' => [1], 'permiso' => 'proveedores'
                            ],
                            [
                                'nom' => 'Alojamientos', 'route' => 'manage.alojamientos.index', 'icono' => 'fa fa-bed fa-fw',
                                'rol' => [1], 'permiso' => 'proveedores'
                            ],
                            [
                                'nom' => 'Cursos', 'route' => 'manage.cursos.index', 'icono' => 'fa fa-book fa-fw',
                                'rol' => [1], 'permiso' => 'proveedores'
                            ],
                            [
                                'nom' => 'Centro > Extras', 'route' => 'manage.centros.extras.index', 'icono' => 'fa fa-home fa-fw',
                                'rol' => [1], 'permiso' => 'proveedores'
                            ],
                            [
                                'nom' => 'Curso > Extras', 'route' => 'manage.cursos.extras.index', 'icono' => 'fa fa-book fa-fw',
                                'rol' => [1], 'permiso' => 'proveedores'
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Precios', 'route' => '#menu-precios', 'icono' => 'fa fa-money fa-fw',
                        'rol' => [1], 'permiso' => 'precios',
                        'submenu' => [
                            [
                                'nom' => 'Convocatorias Abiertas', 'route' => 'manage.convocatorias.abiertas.index', 'icono' => 'fa fa-calendar fa-fw',
                                'rol' => [1], 'permiso' => 'precios'
                            ],
                            [
                                'nom' => 'Convocatorias Cerradas', 'route' => 'manage.convocatorias.cerradas.index', 'icono' => 'fa fa-certificate fa-fw',
                                'rol' => [1], 'permiso' => 'precios'
                            ],
                            [
                                'nom' => 'Convocatorias Multi', 'route' => 'manage.convocatorias.multis.index', 'icono' => 'fa fa-certificate fa-fw',
                                'rol' => [1], 'permiso' => 'precios'
                            ],
                            [
                                'nom' => 'Curso Incluye', 'route' => 'manage.cursos.incluyes.index', 'icono' => 'fa fa-book fa-fw',
                                'rol' => [1], 'permiso' => 'precios'
                            ],

                            [
                                'nom' => 'Tablas Tipo de cambio', 'route' => 'manage.monedas.cambios.index', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1], 'permiso' => 'monedas-cambio'
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Descuentos', 'route' => '#menu-descuentos', 'icono' => 'fa fa-tags fa-fw', 'rol' => [1], 'permiso' => 'descuentos',
                        'submenu' => [
                            [
                                'nom' => 'Cliente', 'route' => 'manage.descuentos.tipos.index', 'icono' => 'fa fa-gift fa-fw',
                                'rol' => [1], 'permiso' => 'descuentos'
                            ],
                            [
                                'nom' => 'Early Bird', 'route' => 'manage.descuentos.early-bird.index', 'icono' => 'fa fa-gift fa-fw',
                                'rol' => [1], 'permiso' => 'descuentos'
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Extras', 'route' => '#menu-extras', 'icono' => 'fa fa-star fa-fw', 'rol' => [1], 'permiso' => 'tablas',
                        'submenu' => [
                            [
                                'nom' => 'Extras Genéricos', 'route' => 'manage.extras.index', 'icono' => 'fa fa-star-half-o fa-fw',
                                'rol' => [1], 'permiso' => 'tablas'
                            ],
                            [
                                'nom' => 'Extras Tipos', 'route' => 'manage.unidades.index', 'icono' => 'fa fa-tag fa-fw',
                                'rol' => [1], 'permiso' => 'tablas'
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Tablas Geográficas', 'route' => '#menu-tablas', 'icono' => 'fa fa-globe fa-fw', 'rol' => [1], 'permiso' => 'tablas',
                        'submenu' => [
                            [
                                'nom' => 'Ciudades', 'route' => 'manage.ciudades.index', 'icono' => 'fa fa-flag fa-fw',
                                'rol' => [1], 'permiso' => 'tablas'
                            ],
                            [
                                'nom' => 'Provincias', 'route' => 'manage.provincias.index', 'icono' => 'fa fa-flag fa-fw',
                                'rol' => [1], 'permiso' => 'tablas'
                            ],
                            [
                                'nom' => 'Paises', 'route' => 'manage.paises.index', 'icono' => 'fa fa-globe fa-fw',
                                'rol' => [1], 'permiso' => 'tablas'
                            ],
                            [
                                'nom' => 'Nacionalidades', 'route' => 'manage.nacionalidades.index', 'icono' => 'fa fa-globe fa-fw',
                                'rol' => [1], 'permiso' => 'tablas'
                            ],
                        ]
                    ],
                    [
                        'nom' => 'Contactos SOS', 'route' => '#menu-csos', 'icono' => 'fa fa-phone fa-fw', 'rol' => [1], 'permiso' => 'tablas',
                        'submenu' => [
                            [
                                'nom' => 'Proveedor', 'route' => ['manage.system.contactos.index', 1], 'icono' => 'fa fa-phone fa-fw',
                                'rol' => [1], 'permiso' => 'tablas'
                            ],
                            [
                                'nom' => 'Plataforma', 'route' => ['manage.system.contactos.index', 0], 'icono' => 'fa fa-phone fa-fw',
                                'rol' => [1], 'permiso' => 'tablas'
                            ],
                        ]
                    ],
                ]
            ],


            [
                'nom' => 'Vuelos', 'route' => '#menu-vuelos', 'icono' => 'fa fa-plane fa-fw', 'rol' => [1], 'permiso' => 'vuelos',
                'submenu' => [
                    [
                        'nom' => 'Convocatorias Vuelos', 'route' => 'manage.convocatorias.vuelos.index', 'icono' => 'fa fa-plane fa-fw',
                        'rol' => [1], 'permiso' => 'vuelos'
                    ],
                    [
                        'nom' => 'Agencias', 'route' => 'manage.agencias.index', 'icono' => 'fa fa-truck fa-fw',
                        'rol' => [1], 'permiso' => 'vuelos'
                    ],
                ]
            ],

            [
                'nom' => 'Configuración', 'route' => '#menu-config', 'icono' => 'fa fa-wrench fa-fw',
                'rol' => [1], 'permiso' => 'all',
                'submenu' => [

                    [
                        'nom' => 'Orígen Leads', 'route' => '#menu-origen-leads', 'icono' => 'fa fa-folder fa-fw', 'rol' => [1], 'permiso' => 'origenes',
                        'submenu' => [
                            [
                                'nom' => 'Leads Origen', 'route' => 'manage.viajeros.origenes.index', 'icono' => 'fa fa-users fa-fw',
                                'rol' => [1], 'permiso' => 'origenes'
                            ],
                            [
                                'nom' => 'Leads Sub-Origen', 'route' => 'manage.viajeros.suborigenes.index', 'icono' => 'fa fa-users fa-fw',
                                'rol' => [1], 'permiso' => 'origenes'
                            ],
                            [
                                'nom' => 'Leads Sub-Origen Detalle', 'route' => 'manage.viajeros.suborigendetalles.index', 'icono' => 'fa fa-users fa-fw',
                                'rol' => [1], 'permiso' => 'origenes'
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Configuración', 'route' => '#menu-configuracion', 'icono' => 'fa fa-wrench fa-fw', 'rol' => [1], 'permiso' => 'configuracion',
                        'submenu' => [
                            [
                                'nom' => 'Especialidades', 'route' => 'manage.especialidades.index', 'icono' => 'fa fa-tag fa-fw',
                                'rol' => [1], 'permiso' => 'configuracion'
                            ],
                            [
                                'nom' => 'Sub-Especialidades', 'route' => 'manage.subespecialidades.index', 'icono' => 'fa fa-tags fa-fw',
                                'rol' => [1], 'permiso' => 'configuracion'
                            ],
                            [
                                'nom' => 'Tipo Alojamientos', 'route' => 'manage.alojamientos.tipos.index', 'icono' => 'fa fa-bed fa-fw',
                                'rol' => [1], 'permiso' => 'configuracion'
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Gestión Prescriptores', 'route' => '#menu-prescriptores', 'icono' => 'fa fa-building fa-fw', 'rol' => [1], 'permiso' => 'prescriptores',
                        'submenu' => [
                            [
                                'nom' => 'Prescriptores', 'route' => 'manage.prescriptores.index', 'icono' => 'fa fa-suitcase fa-fw',
                                'rol' => [1], 'permiso' => 'prescriptores'
                            ],
                            [
                                'nom' => 'Categorías', 'route' => 'manage.prescriptores.categorias.index', 'icono' => 'fa fa-tag fa-fw',
                                'rol' => [1], 'permiso' => 'prescriptores'
                            ],
                            [
                                'nom' => 'Categoría Comisiones', 'route' => 'manage.prescriptores.categorias-comision.index', 'icono' => 'fa fa-tag fa-fw',
                                'rol' => [1], 'permiso' => 'prescriptores'
                            ],
                        ]
                    ],

                ]
            ],

            [
                'nom' => 'Informes', 'route' => '#menu-informes', 'icono' => 'fa fa-pie-chart fa-fw',
                'rol' => [1], 'permiso' => 'informes',
                'submenu' => [

                    // permiso: BLOQUE.DETALLE => BLOQUE=nom . manage.informes.DETALLE
                    [
                        'nom' => 'Listados', 'route' => '#menu-listados', 'icono' => 'fa fa-list fa-fw', 'rol' => [1], 'permiso' => 'informes',
                        'submenu' => [
                            [
                                'nom' => 'Viajeros', 'route' => 'manage.viajeros.index', 'icono' => 'fa fa-suitcase fa-fw',
                                'rol' => [1], 'permiso' => 'viajeros-list'
                            ],
                            [
                                'nom' => 'Tutores', 'route' => 'manage.tutores.index', 'icono' => 'fa fa-shield fa-fw',
                                'rol' => [1], 'permiso' => 'tutores-list'
                            ],
                            [
                                'nom' => 'Monitores', 'route' => 'manage.monitores.index', 'icono' => 'fa fa-users fa-fw',
                                'rol' => [1], 'permiso' => 'monitores-list'
                            ],
                            ['nom' => '-', 'route' => '-', 'icono' => '-', 'rol' => [1]],
                            [
                                'nom' => 'Solicitudes', 'route' => 'manage.solicitudes.index', 'icono' => 'fa fa-pencil-square fa-fw',
                                'rol' => [1], 'permiso' => 'solicitudes-list'
                            ],
                            [
                                'nom' => 'Inscripciones', 'route' => 'manage.bookings.index', 'icono' => 'fa fa-pencil-square fa-fw',
                                'rol' => [1], 'permiso' => 'bookings-list'
                            ],
                            ['nom' => '-', 'route' => '-', 'icono' => '-', 'rol' => [1]],
                            [
                                'nom' => 'Inscritos', 'route' => 'manage.inscritos.index', 'icono' => 'fa fa-user fa-fw',
                                'rol' => [1], 'permiso' => 'inscritos-list'
                            ],
                            [
                                'nom' => 'Doc.Específicos', 'route' => 'manage.informes.doc-especificos', 'icono' => 'fa fa-folder fa-fw',
                                'rol' => [1], 'permiso' => 'bookings'
                            ],
                            [
                                'nom' => 'Datos contacto', 'route' => 'manage.informes.viajeros.datos', 'icono' => 'fa fa-user fa-fw',
                                'rol' => [1], 'permiso' => 'bookings'
                            ],
                            [
                                'nom' => 'No conexión Área', 'route' => 'manage.informes.area.no-conexion', 'icono' => 'fa fa-user fa-fw',
                                'rol' => [1], 'permiso' => 'bookings'
                            ],
                            [
                                'nom' => 'Tráfico', 'route' => 'manage.informes.trafico', 'icono' => 'fa fa-globe fa-fw',
                                'rol' => [1], 'permiso' => 'bookings'
                            ],

                        ]
                    ],

                    [
                        'nom' => 'Operativa', 'route' => '#menu-informes-operativa', 'icono' => 'fa fa-folder fa-fw', 'rol' => [1], 'permiso' => 'informes',
                        'submenu' => [
                            [
                                'nom' => 'Booking Grupos', 'route' => 'manage.informes.booking-grupos', 'icono' => 'fa fa-ticket fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Seguro Cancelación', 'route' => 'manage.informes.seguros', 'icono' => 'fa fa-ban fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Plazas Alojamiento', 'route' => 'manage.informes.plazas-alojamientos', 'icono' => 'fa fa-home fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Plazas Semana', 'route' => 'manage.informes.plazas-semanas', 'icono' => 'fa fa-home fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Convocatoria Multi', 'route' => 'manage.informes.convocatorias-multi', 'icono' => 'fa fa-ticket fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Info reunión', 'route' => 'manage.informes.reunion', 'icono' => 'fa fa-group fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'AVI', 'route' => 'manage.informes.avi', 'icono' => 'fa fa-ticket fa-fw',
                                'rol' => [1]
                            ],
                            // [ 'nom' => 'AVI Adultos', 'route' => ['manage.informes.avi',1], 'icono' => 'fa fa-ticket fa-fw',
                            //     'rol' => [1] ],
                            [
                                'nom' => 'Bookings Individuales', 'route' => 'manage.informes.bookings-individuales', 'icono' => 'fa fa-ticket fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Bookings Origen', 'route' => 'manage.informes.bookings-origen', 'icono' => 'fa fa-ticket fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Informe para Monitores', 'route' => 'manage.informes.monitores', 'icono' => 'fa fa-users fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Orla', 'route' => 'manage.informes.orla', 'icono' => 'fa fa-users fa-fw',
                                'rol' => [1]
                            ],
                            ['nom' => 'Incidencias', 'route' => 'manage.informes.bookings-incidencias', 'icono' => 'fa fa-exclamation-triangle fa-fw', 'rol' => [1]],

                            [
                                'nom' => 'Niveles CIC', 'route' => 'manage.informes.cic-niveles', 'icono' => 'fa fa-list-ol fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Datos InfoPersonal Camps', 'route' => 'manage.informes.cic-camps', 'icono' => 'fa fa-list-ol fa-fw',
                                'rol' => [1]
                            ],

                            [
                                'nom' => 'Control Inscripciones', 'route' => 'manage.informes.checklist', 'icono' => 'fa fa-pencil-square fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Actividad Usuarios', 'route' => 'manage.informes.actividad', 'icono' => 'fa fa-briefcase fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Contactos diarios', 'route' => 'manage.informes.contactos', 'icono' => 'fa fa-users fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Envío catálogos', 'route' => 'manage.informes.envio-catalogos', 'icono' => 'fa fa-users fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Datos contacto inscritos', 'route' => 'manage.informes.datos.inscritos', 'icono' => 'fa fa-users fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Estudiantes-Family-School', 'route' => 'manage.informes.datos.estudiantes', 'icono' => 'fa fa-building fa-fw',
                                'rol' => [1]
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Ventas', 'route' => '#menu-informes-ventas', 'icono' => 'fa fa-money fa-fw', 'rol' => [1], 'permiso' => 'informes',
                        'submenu' => [
                            [
                                'nom' => 'Listado Ventas', 'route' => 'manage.informes.ventas', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Ventas por Prescriptor', 'route' => 'manage.informes.ventas-prescriptor', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Avisos Pre-Reserva', 'route' => 'manage.informes.avisos.reserva', 'icono' => 'fa fa-bell fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Escuela y Academia', 'route' => 'manage.informes.escuelas', 'icono' => 'fa fa-graduation-cap fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Ventas Prescriptores', 'route' => 'manage.informes.ventas-prescriptores', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Origen Ventas', 'route' => 'manage.informes.ventas-origen', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                            // [ 'nom' => 'Especialidades Multi', 'route' => 'manage.informes.especialidades', 'icono' => 'fa fa-tags fa-fw',
                            //     'rol' => [1] ],
                            [
                                'nom' => 'Análisis Ventas', 'route' => 'manage.informes.ventas-analisis', 'icono' => 'fa fa-pie-chart fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Gráficos Ventas', 'route' => 'manage.informes.ventas-graficos', 'icono' => 'fa fa-bar-chart fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Descuentos', 'route' => 'manage.informes.descuentos', 'icono' => 'fa fa-gift fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Devoluciones', 'route' => 'manage.informes.devoluciones', 'icono' => 'fa fa-recycle fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'No repetidores', 'route' => 'manage.informes.no-repetidores', 'icono' => 'fa fa-user fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Edad', 'route' => 'manage.informes.edad', 'icono' => 'fa fa-birthday-cake fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Comportamiento', 'route' => 'manage.informes.trasvase', 'icono' => 'fa fa-ticket fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Márgenes', 'route' => 'manage.informes.margenes', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Análisis Solicitudes', 'route' => 'manage.informes.anal-solicitudes', 'icono' => 'fa fa-pie-chart fa-fw',
                                'rol' => [1]
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Facturación', 'route' => '#menu-informes-facturacion', 'icono' => 'fa fa-folder fa-fw', 'rol' => [1], 'permiso' => 'informes',
                        'submenu' => [
                            [
                                'nom' => 'Facturación', 'route' => 'manage.informes.facturacion-fechas', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Listado Facturación', 'route' => 'manage.informes.facturacion', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Informe 347', 'route' => 'manage.informes.facturacion-347', 'icono' => 'fa fa-file-text fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Control Precios', 'route' => 'manage.informes.facturacion-precios', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Pagos', 'route' => 'manage.informes.pagos', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Pagos agrupados', 'route' => 'manage.informes.pagos.grupo', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Pagos Envío', 'route' => 'manage.informes.pagos.envio', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Control Divisa', 'route' => 'manage.informes.control-divisa', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Facturado-Cobrado', 'route' => 'manage.informes.facturado-cobrado', 'icono' => 'fa fa-money fa-fw',
                                'rol' => [1]
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Vuelos', 'route' => '#menu-informes-vuelos', 'icono' => 'fa fa-plane fa-fw', 'rol' => [1], 'permiso' => 'informes',
                        'submenu' => [
                            [
                                'nom' => 'Plazas Vuelos', 'route' => 'manage.informes.plazas-vuelos', 'icono' => 'fa fa-plane fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Menores 12 años en grupo', 'route' => 'manage.informes.menores12', 'icono' => 'fa fa-users fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Aportes', 'route' => 'manage.informes.aportes', 'icono' => 'fa fa-plane fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Asignación Vuelos', 'route' => 'manage.informes.vuelos-asignacion', 'icono' => 'fa fa-plane fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Vuelos pre-salida', 'route' => 'manage.informes.vuelos-presalida', 'icono' => 'fa fa-plane fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Pasajeros', 'route' => 'manage.informes.vuelos-pasajeros', 'icono' => 'fa fa-plane fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'API', 'route' => 'manage.informes.vuelos-api', 'icono' => 'fa fa-plane fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'API2', 'route' => 'manage.informes.vuelos-api2', 'icono' => 'fa fa-plane fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Infovuelo Proveedor', 'route' => 'manage.informes.vuelos-proveedor', 'icono' => 'fa fa-plane fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Aeropuerto', 'route' => 'manage.informes.aeropuerto', 'icono' => 'fa fa-plane fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Infovuelo Agencia', 'route' => 'manage.informes.vuelos-agencia', 'icono' => 'fa fa-plane fa-fw',
                                'rol' => [1]
                            ],
                            [
                                'nom' => 'Parrilla vuelos', 'route' => 'manage.informes.vuelos-parrilla', 'icono' => 'fa fa-plane fa-fw',
                                'rol' => [1]
                            ],
                        ]
                    ],

                    // [ 'nom' => 'Año Escolar', 'route' => '#menu-escolar', 'icono' => 'fa fa-graduation-cap fa-fw',
                    //     'rol' => [1], 'permiso'=> 'informes',
                    //     'submenu' => [
                    //             [ 'nom' => 'Control Inscripciones', 'route' => 'manage.informes.inscripciones', 'icono' => 'fa fa-pencil-square fa-fw',
                    //                 'rol' => [1], 'permiso'=> 'informes' ],
                    //         ]
                    // ],

                ]
            ],

            [
                'nom' => 'Comunicacion', 'route' => '#menu-config', 'icono' => 'fa fa-comments fa-fw',
                'rol' => [1], 'permiso' => 'all',
                'submenu' => [
                    [
                        'nom' => 'Mailing inscritos', 'route' => '#menu-mailing-inscritos', 'icono' => 'fa fa-envelope fa-fw', 'rol' => [1], 'permiso' => 'avisos',
                        'submenu' => [
                            [
                                'nom' => 'Gestor de Avisos/Tareas', 'route' => 'manage.system.avisos.index', 'icono' => 'fa fa-bell fa-fw',
                                'rol' => [1], 'permiso' => 'avisos'
                            ],
                            [
                                'nom' => 'Gestor de Avisos - Docs', 'route' => 'manage.system.avisos.docs.index', 'icono' => 'fa fa-file fa-fw',
                                'rol' => [1], 'permiso' => 'avisos'
                            ],
                            [
                                'nom' => 'Listado', 'route' => 'manage.system.avisos.listado', 'icono' => 'fa fa-list fa-fw',
                                'rol' => [1], 'permiso' => 'avisos_mailing'
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Cuestionarios', 'route' => '#menu-cuestionarios', 'icono' => 'fa fa-list-alt fa-fw', 'rol' => [1], 'permiso' => 'all',
                        'submenu' => [
                            [
                                'nom' => 'Formularios', 'route' => 'manage.system.cuestionarios.index', 'icono' => 'fa fa-list-alt fa-fw',
                                'rol' => [1], 'permiso' => 'cuestionarios'
                            ],
                            [
                                'nom' => 'Resumen', 'route' => 'manage.system.cuestionarios.resultados', 'icono' => 'fa fa-comments fa-fw',
                                'rol' => [1], 'permiso' => 'cuestionarios-resumen'
                            ],
                            [
                                'nom' => 'Informes', 'route' => 'manage.system.cuestionarios.informes', 'icono' => 'fa fa-pie-chart fa-fw',
                                'rol' => [1], 'permiso' => 'cuestionarios-resumen'
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Tests', 'route' => '#menu-tests', 'icono' => 'fa fa-list-alt fa-fw', 'rol' => [1], 'permiso' => 'cuestionarios',
                        'submenu' => [
                            [
                                'nom' => 'Listado', 'route' => 'manage.exams.index', 'icono' => 'fa fa-list-alt fa-fw',
                                'rol' => [1], 'permiso' => 'cuestionarios'
                            ],

                        ]
                    ],

                    [
                        'nom' => 'Sincronizar', 'route' => '#menu-avisos-sync', 'icono' => 'fa fa-list-alt fa-fw', 'rol' => [1], 'permiso' => 'avisos',
                        'submenu' => [
                            [
                                'nom' => 'Mailchimp', 'route' => 'manage.system.avisos.sync', 'icono' => 'fa fa-list-alt fa-fw',
                                'rol' => [1], 'permiso' => 'avisos'
                            ],

                        ]
                    ],

                    [
                        'nom' => 'Emails de Sistema', 'route' => 'manage.system.emails.index', 'icono' => 'fa fa-envelope fa-fw', 'rol' => [1]
                    ],
                    [
                        'nom' => 'Chats', 'route' => 'manage.chat.index', 'icono' => 'fa fa-comments fa-fw', 'rol' => [1]
                    ],

                ],
            ],

            [
                'nom' => 'CMS', 'route' => '#menu-cms', 'icono' => 'fa fa-map-signs fa-fw',
                'rol' => [1], 'permiso' => 'cms',
                'submenu' => [

                    [
                        'nom' => 'CMS', 'route' => '#cms', 'icono' => 'fa fa-map-signs fa-fw', 'rol' => [1], 'permiso' => 'cms',
                        'submenu' => [
                            [
                                'nom' => 'Páginas', 'route' => 'manage.cms.paginas.index', 'icono' => 'fa fa-file-text-o fa-fw',
                                'rol' => [1], 'permiso' => 'cms'
                            ],
                            [
                                'nom' => 'Categorías Web', 'route' => 'manage.cms.categorias.index', 'icono' => 'fa fa-tag fa-fw',
                                'rol' => [1], 'permiso' => 'cms'
                            ],
                            [
                                'nom' => 'Promos', 'route' => 'manage.cms.promos.index', 'icono' => 'fa fa-gift fa-fw',
                                'rol' => [1], 'permiso' => 'cms'
                            ],
                            [
                                'nom' => 'Especialidades Web', 'route' => 'manage.cms.especialidades.index', 'icono' => 'fa fa-tag fa-fw',
                                'rol' => [1], 'permiso' => 'cms'
                            ],
                            [
                                'nom' => 'Catálogos', 'route' => 'manage.cms.catalogos.index', 'icono' => 'fa fa-file-pdf-o fa-fw',
                                'rol' => [1], 'permiso' => 'cms'
                            ],
                            // [ 'nom' => 'Landings', 'route' => 'manage.cms.landings.index', 'icono' => 'fa fa-file fa-fw',
                            //     'rol' => [1], 'permiso'=> 'cms' ],
                        ]
                    ],

                    [
                        'nom' => 'Landings', 'route' => '#cms-landings', 'icono' => 'fa fa-file fa-fw', 'rol' => [1], 'permiso' => 'cms',
                        'submenu' => [
                            [
                                'nom' => 'Landings', 'route' => 'manage.cms.landings.index', 'icono' => 'fa fa-file fa-fw',
                                'rol' => [1], 'permiso' => 'cms'
                            ],
                            [
                                'nom' => 'Testimonios', 'route' => 'manage.cms.landings.testimonios.index', 'icono' => 'fa fa-users fa-fw',
                                'rol' => [1], 'permiso' => 'cms'
                            ],
                            [
                                'nom' => 'Destinos', 'route' => 'manage.cms.landings.destinos.index', 'icono' => 'fa fa-plane fa-fw',
                                'rol' => [1], 'permiso' => 'cms'
                            ],
                            [
                                'nom' => 'Programas', 'route' => 'manage.cms.landings.programas.index', 'icono' => 'fa fa-folder fa-fw',
                                'rol' => [1], 'permiso' => 'cms'
                            ],
                            [
                                'nom' => 'Informe', 'route' => 'manage.cms.landings.informe', 'icono' => 'fa fa-pie-chart fa-fw',
                                'rol' => [1], 'permiso' => 'cms'
                            ],
                        ]
                    ],

                ]
            ],

            [
                'nom' => 'Sistema', 'route' => '#menu-config', 'icono' => 'fa fa-cog fa-fw',
                'rol' => [1], 'permiso' => 'system',
                'submenu' => [

                    [
                        'nom' => 'Configuración Sistema', 'route' => '#menu-configuracion-sistema', 'icono' => 'fa fa-key fa-fw', 'rol' => [1], 'permiso' => 'system',
                        'submenu' => [
                            [
                                'nom' => 'Categorías', 'route' => 'manage.categorias.index', 'icono' => 'fa fa-tag fa-fw',
                                'rol' => [1], 'permiso' => 'system'
                            ],
                            [
                                'nom' => 'Sub-Categorías', 'route' => 'manage.subcategorias.index', 'icono' => 'fa fa-tags fa-fw',
                                'rol' => [1], 'permiso' => 'system'
                            ],
                            [
                                'nom' => 'Detalles Sub-Categoría', 'route' => 'manage.subcategoria-detalles.index', 'icono' => 'fa fa-tags fa-fw',
                                'rol' => [1], 'permiso' => 'system'
                            ],
                            [
                                'nom' => 'Status Solicitudes', 'route' => 'manage.solicitudes.status.index', 'icono' => 'fa fa-tag fa-fw',
                                'rol' => [1], 'permiso' => 'system'
                            ],
                            [
                                'nom' => 'Status Checklist Solicitudes', 'route' => 'manage.solicitudes.checklist.index', 'icono' => 'fa fa-tags fa-fw',
                                'rol' => [1], 'permiso' => 'system'
                            ],
                            [
                                'nom' => 'Status Booking', 'route' => 'manage.bookings.status.index', 'icono' => 'fa fa-tag fa-fw',
                                'rol' => [1], 'permiso' => 'system'
                            ],
                            [
                                'nom' => 'Status Checklist Booking', 'route' => 'manage.bookings.checklist.index', 'icono' => 'fa fa-tags fa-fw',
                                'rol' => [1], 'permiso' => 'system'
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Setup', 'route' => '#system', 'icono' => 'fa fa-cog fa-fw', 'rol' => [1], 'permiso' => 'system',
                        'submenu' => [
                            [
                                'nom' => 'Usuarios', 'route' => 'manage.system.admins.index', 'icono' => 'fa fa-user-secret fa-fw',
                                'rol' => [1], 'permiso' => 'system'
                            ],
                            [
                                'nom' => 'Permisos', 'route' => 'manage.system.roles.index', 'icono' => 'fa fa-user-secret fa-fw',
                                'rol' => [1], 'permiso' => 'system'
                            ],
                            [
                                'nom' => 'Oficinas', 'route' => 'manage.system.oficinas.index', 'icono' => 'fa fa-building fa-fw',
                                'rol' => [1], 'permiso' => 'oficinas'
                            ],
                            [
                                'nom' => 'Campos', 'route' => 'manage.system.campos.index', 'icono' => 'fa fa-tags fa-fw',
                                'rol' => [1], 'permiso' => 'system'
                            ],
                        ]
                    ],

                    [
                        'nom' => 'Configuración General', 'route' => 'manage.system.plataformas.index', 'icono' => 'fa fa-cog fa-fw', 'rol' => [1]
                    ],
                ]
            ],
        ],

        // ==============================================================================================================

        'area' => [

            [
                'nom' => 'Mis Datos', 'route' => 'area.datos.index', 'icono' => 'fa fa-info', 'rol' => [11, 12],
                'submenu' => [
                    //[ 'nom' => 'Usuarios', 'route' => '', 'icono' => 'fa fa-user-secret fa-fw', 'rol' => [0] ],
                ],
            ],

            [
                'nom' => 'Mis Cursos', 'route' => 'area.cursos', 'icono' => 'fa fa-book', 'rol' => [11, 12],
                'submenu' => [
                    //[ 'nom' => 'Usuarios', 'route' => '', 'icono' => 'fa fa-user-secret fa-fw', 'rol' => [0] ],
                ],
            ],
            // [

            //     'nom' => 'Mis Favoritos', 'route' => 'area.deseos', 'icono' => 'fa fa-star', 'rol'=> [11,12],
            //         'submenu' => [
            //             //[ 'nom' => 'Usuarios', 'route' => '', 'icono' => 'fa fa-user-secret fa-fw', 'rol' => [0] ],
            //         ],
            // ],
            [

                'nom' => 'Tutores', 'route' => 'area.datos.tutores', 'icono' => 'fa fa-shield', 'rol' => [11],
                'submenu' => [
                    //[ 'nom' => 'Usuarios', 'route' => '', 'icono' => 'fa fa-user-secret fa-fw', 'rol' => [0] ],
                ],
            ],
            [

                'nom' => 'Mis Hijos', 'route' => 'area.datos.hijos', 'icono' => 'fa fa-users', 'rol' => [12],
                'submenu' => [
                    //[ 'nom' => 'Usuarios', 'route' => '', 'icono' => 'fa fa-user-secret fa-fw', 'rol' => [0] ],
                ],
            ],
        ]
    ),

];
