<?php

return array(


    'pdf' => array(
        'enabled' => true,
        'binary'  => env('WKHTMLTOPDF', '/usr/local/bin/wkhtmltopdf'),
        'timeout' => 600,
        'options' => array(
            'enable-local-file-access' => true,
        ),
        'env'     => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary'  => env('WKHTMLTOPIMAGE', '/usr/local/bin/wkhtmltoimage'),
        'timeout' => 600,
        'options' => array(),
        'env'     => array(),
    ),


);
