<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Table specific configuration options.
    |--------------------------------------------------------------------------
    |
    */

    'table' => array(

        /*
        |--------------------------------------------------------------------------
        | Table class
        |--------------------------------------------------------------------------
        |
        | Class(es) added to the table
        | Supported: string
        |
        */

        'class' => 'table table-bordered',

        /*
        |--------------------------------------------------------------------------
        | Table ID
        |--------------------------------------------------------------------------
        |
        | ID given to the table. Used for connecting the table and the Datatables
        | jQuery plugin. If left empty a random ID will be generated.
        | Supported: string
        |
        */

        'id' => '',

        /*
        |--------------------------------------------------------------------------
        | DataTable options
        |--------------------------------------------------------------------------
        |
        | jQuery dataTable plugin options. The array will be json_encoded and
        | passed through to the plugin. See https://datatables.net/usage/options
        | for more information.
        | Supported: array
        |
        */

        'options' => array(

            "sPaginationType" => "full_numbers",

            "bProcessing" => true,

            "responsive" => true,
            "bAutoWidth" => false,

            "oLanguage" => [
              "oPaginate" => [
                "sFirst" => "Primero",
                "sLast" => "Ultimo",
                "sPrevious" => "Anterior",
                "sNext" => "Siguiente"
              ],
              "sSearch" => "Buscar: ",
              "sProcessing" => "<div class='dataTables_processing_div'><i class='fa fa-refresh fa-spin fa-2x'></i></div>",
              "sLengthMenu" => "Mostrar _MENU_ registros por página",
              "sZeroRecords" => "No se encontraron resultados",
              "sInfo" => "Listando de _START_ a _END_ de _TOTAL_ registros",
              "sInfoEmpty" => "Listando de 0 a 0 de 0 registros",
              "sInfoFiltered" => "(filtrando desde _MAX_ total de registros)"
            ],
            "iDisplayLength"=> 100,
            "lengthMenu"=> [[10, 25, 50, 100, 999999], [10, 25, 50, 100, "Todos"]],
            // 'dom'=> 'Bflrtip',
            // 'dom'=> '<"top"i>rt<"bottom"flp><"clear">',
            "dom"=> '<"top"i>Bflrtip',
            'buttons'=> [
                // 'copy', 'csv', 'excel', 'pdf', 'print'
                'copy','csv','excel','print'
            ],
        ),

        /*
        |--------------------------------------------------------------------------
        | DataTable callbacks
        |--------------------------------------------------------------------------
        |
        | jQuery dataTable plugin callbacks. The array will be json_encoded and
        | passed through to the plugin. See https://datatables.net/usage/callbacks
        | for more information.
        | Supported: array
        |
        */

        'callbacks' => array(

            'drawCallback' => 'function ( oSettings ) {
                $("[data-label]").each(function(){
                    $(this).tooltip({"placement": "top", "title": $(this).data("label")});
                });
                $("[data-label_left]").each(function(){
                    $(this).tooltip({"placement": "left", "title": $(this).data("label_left")});
                });
            }'

        ),

        /*
        |--------------------------------------------------------------------------
        | Skip javascript in table template
        |--------------------------------------------------------------------------
        |
        | Determines if the template should echo the javascript
        | Supported: boolean
        |
        */

        'noScript' => false,


        /*
        |--------------------------------------------------------------------------
        | Table view
        |--------------------------------------------------------------------------
        |
        | Template used to render the table
        | Supported: string
        |
        */

        'table_view' => 'chumper.datatable::template',


        /*
        |--------------------------------------------------------------------------
        | Script view
        |--------------------------------------------------------------------------
        |
        | Template used to render the javascript
        | Supported: string
        |
        */

        'script_view' => 'chumper.datatable::javascript',
    ),


    /*
    |--------------------------------------------------------------------------
    | Engine specific configuration options.
    |--------------------------------------------------------------------------
    |
    */

    'engine' => array(

        /*
        |--------------------------------------------------------------------------
        | Search for exact words
        |--------------------------------------------------------------------------
        |
        | If the search should be done with exact matching
        | Supported: boolean
        |
        */

        'exactWordSearch' => false,

    ),
    /*
    |--------------------------------------------------------------------------
    | Allow overrides Datatable core classes
    |--------------------------------------------------------------------------
    |
    */
    'classmap' => array(
        'CollectionEngine' => 'Chumper\Datatable\Engines\CollectionEngine',
        'QueryEngine' => 'Chumper\Datatable\Engines\QueryEngine',
        'Table' => 'Chumper\Datatable\Table',
    )
);
