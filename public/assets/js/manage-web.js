
$(document).ready(function() {

    $("[data-label]").each(function(){
        $(this).tooltip({"placement": "top", "title": $(this).data("label")});
    });

    var $startDate = $("#booking-cabierta-fecha_ini").data('start');
    var $endDate = $("#booking-cabierta-fecha_ini").data('end');
    var $dayDate = $("#booking-cabierta-fecha_ini").data('day');

    var $unset = $dayDate;
    if($unset==7) { $unset = 0; }

    var $disabled = [0,1,2,3,4,5,6]; //0:domingo, 1:lunes, martes, miercoles, jueves, viernes, sabado
    if($unset==='')
    {
        $disabled = [];
    }
    else
    {
        $disabled.splice($unset, 1);
    }

    $('#booking-cabierta-fecha_ini').datetimepicker({
        locale: 'es',
        format: 'L',
        // useCurrent: true,
        daysOfWeekDisabled: $disabled,
        minDate: $startDate,
        maxDate: $endDate,
    });

    $('#booking-alojamiento-fecha_ini').datetimepicker({
        locale: 'es',
        format: 'L',
        daysOfWeekDisabled: [1,2,3,4,5],
    });


    $('#booking-cabierta-semanas').change( function() {

        var $semanas = $("#booking-cabierta-semanas").val();
        var $fecha = $("#booking-cabierta-fecha_ini").val();

        if($fecha=="") { alert("No hay Fecha de Inicio"); return; }

        var $data = { 'info': 'cabierta-semanas', 'fecha': $fecha, 'valor': $semanas};

        // console.log($url);
        // console.log($data);

        $('#booking-cabierta-precio').html("<i class='fa fa-spin fa-spinner'></i>");

        $.ajax({
            url: $url,
            type: 'GET',
            dataType : 'json',
            data: $data,
            success: function(data) { //console.log(data);

                if(data.result>0)
                {
                    $("#booking-cabierta-precio").html(data.precio_txt);
                    $("#booking-cabierta-fecha_fin").val(data.fechafin);

                    precio_total();

                    if(data.alerta)
                    {
                      bootbox.alert(data.alerta);
                    }
                }
                else
                {
                    $('#booking-cabierta-precio').html("NO");
                    // if(data.minimo)
                    // {
                    //     alert("El curso tiene que ser mínimo de "+data.minimo+" semanas");
                    // }
                    // else
                    {
                        alert(data.error);
                    }
                }

            },
            error: function(xhr, desc, err) {
                console.log(xhr.responseText);
                console.log("Details: " + desc + "\nError:" + err);
            }

        });

    });

    $('#booking-alojamiento-semanas, #booking-alojamiento').change( function() {

        var $semanas = $("#booking-alojamiento-semanas").val();
        var $alojamiento_id = $('#booking-alojamiento').val();

        var $fecha = $("#booking-alojamiento-fecha_ini").val();

        if($fecha=="") { return; }

        var $data = { 'info': 'cabierta-alojamiento', 'alojamiento_id': $alojamiento_id, 'fecha': $fecha, 'valor': $semanas};

        // console.log($url);
        // console.log($data);

        $('#booking-alojamiento-precio').html("<i class='fa fa-spin fa-spinner'></i>");

        $.ajax({
            url: $url,
            type: 'GET',
            dataType : 'json',
            data: $data,
            success: function(data) { //console.log(data);

                if(data.result==0)
                {
                    $("#booking-alojamiento-precio").html("NO");
                    alert("No hay alojamiento para esta fecha");
                }
                else
                {
                    $("#booking-alojamiento-precio").html(data.precio_txt);
                    $("#booking-alojamiento-fecha_fin").val(data.fechafin);

                    $("#booking-alojamiento-extras").html("");
                    var $html = "<ul class='incluye'>";
                    $.each(data.extras, function(i,extra) {
                        $html += "<li>" + extra.accommodations_quota_name +": " + extra.moneda_txt +"</li>";
                    });
                    $html += "</ul>";

                    $("#booking-alojamiento-extras").html($html);

                    precio_total();

                    if(data.alerta)
                    {
                        bootbox.alert(data.alerta);
                    }

                }

            },
            error: function(xhr, desc, err) {
                console.log(xhr.responseText);
                console.log("Details: " + desc + "\nError:" + err);
            }

        });

    });

});

function precio_total()
{
    var $semanasa = $("#booking-alojamiento-semanas").val();
    var $fechaa = $("#booking-alojamiento-fecha_ini").val();
    var $alojamiento_id = $('#booking-alojamiento').val();

    var $semanasc = $("#booking-cabierta-semanas").val();
    var $fechac = $("#booking-cabierta-fecha_ini").val();

    var $data = { 'info': 'cabierta-total', 'fechac': $fechac, 'semanasc': $semanasc, 'alojamiento_id': $alojamiento_id, 'fechaa': $fechaa, 'semanasa': $semanasa};

    $('#booking-total').html("<i class='fa fa-spin fa-spinner'></i>");

    // console.log($data);

    $.ajax({
        url: $url,
        type: 'GET',
        dataType : 'json',
        data: $data,
        success: function(data) { console.log(data);

            $('#booking-total').html("");

            if(data.result)
            {
                var subtotal = "";
                $.each(data.result.subtotal_txt, function( key, valor ) {
                    subtotal += valor+"<br>";
                });
                $("#booking-subtotal").html(subtotal);
                $("#booking-subtotal-div").show();

                $('#booking-total').html(data.result.total_txt);
            }

        },
        error: function(xhr, desc, err) {
            console.log(xhr.responseText);
            console.log("Details: " + desc + "\nError:" + err);
        }

    });
}