function goBack() {
    window.history.back();
}

function http_get_tab() {
    var loc = document.location.href;
    if (loc.indexOf('#') > -1) return true;

    return false;
}

function http_reload(tab) {
    var loc = location.href;
    if (loc.indexOf('#') > -1) {
        location.href = loc;
    } else {
        location.href = loc + tab;
    }

    location.reload();
}

function validaFechaPago(fecha) {
    var any = new Date().getFullYear();
    var f1 = new Date(any, 0, 1).getTime();
    var f2 = new Date(any, 11, 31).getTime();

    var from = fecha.split("/");
    var f = new Date(from[2], from[1] - 1, from[0]).getTime();

    if (f >= f1 && f <= f2) {
        return true;
    }

    return false;
}

function validaNif(value) {

    value = value.toUpperCase();
    if (!value || value == "") {
        return true;
    }

    // Basic format test

    if (!value.match('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)')) {

        return false;
    }

    // Test NIF
    if (/^[0-9]{8}[A-Z]{1}$/.test(value)) {
        return ("TRWAGMYFPDXBNJZSQVHLCKE".charAt(value.substring(8, 0) % 23) === value.charAt(8));
    }
    // Test specials NIF (starts with K, L or M)
    if (/^[KLM]{1}/.test(value)) {
        return (value[8] === String.fromCharCode(64));
    }

    return false;
}

function validaNie(value) {

    value = value.toUpperCase();
    if (!value || value == "") {
        return true;
    }

    // Basic format test
    if (!value.match('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)')) {
        return false;
    }

    // Test NIE
    //T
    if (/^[T]{1}/.test(value)) {
        return (value[8] === /^[T]{1}[A-Z0-9]{8}$/.test(value));
    }

    //XYZ
    if (/^[XYZ]{1}/.test(value)) {
        return (
            value[8] === "TRWAGMYFPDXBNJZSQVHLCKE".charAt(
                value.replace('X', '0')
                .replace('Y', '1')
                .replace('Z', '2')
                .substring(0, 8) % 23
            )
        );
    }

    return false;
}

jQuery.validator.addMethod("nifES", function (value, element) {
    "use strict";

    value = value.toUpperCase();
    if (!value || value == "") {
        return true;
    }

    // Basic format test
    if (!value.match('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)')) {
        return false;
    }

    // Test NIF
    if (/^[0-9]{8}[A-Z]{1}$/.test(value)) {
        return ("TRWAGMYFPDXBNJZSQVHLCKE".charAt(value.substring(8, 0) % 23) === value.charAt(8));
    }
    // Test specials NIF (starts with K, L or M)
    if (/^[KLM]{1}/.test(value)) {
        return (value[8] === String.fromCharCode(64));
    }

    return false;

}, "Introduzca un número NIF válido.");

/*
 * The número de identidad de extranjero ( NIE )is a code used to identify the non-nationals in Spain
 */
$.validator.addMethod("nieES", function (value) {
    "use strict";

    value = value.toUpperCase();
    if (!value || value == "") {
        return true;
    }

    // Basic format test
    if (!value.match("((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)")) {
        return false;
    }

    // Test NIE
    //T
    if (/^[T]{1}/.test(value)) {
        return (value[8] === /^[T]{1}[A-Z0-9]{8}$/.test(value));
    }

    //XYZ
    if (/^[XYZ]{1}/.test(value)) {
        return (
            value[8] === "TRWAGMYFPDXBNJZSQVHLCKE".charAt(
                value.replace("X", "0")
                .replace("Y", "1")
                .replace("Z", "2")
                .substring(0, 8) % 23
            )
        );
    }

    return false;

}, "Introduzca un número NIE válido.");


function contadorText_init(idtext, idcontador, max) {
    $("#" + idcontador).html($("#" + idtext).val().length + "/" + max);

    if ($("#" + idtext).val().length > max) {
        $("#" + idtext).css('background-color', 'red');
    }

    $("#" + idtext).keyup(function () {
        contadorText_update(idtext, idcontador, max);
    });

    $("#" + idtext).change(function () {
        contadorText_update(idtext, idcontador, max);
    });

}

function contadorText_update(idtext, idcontador, max) {
    var contador = $("#" + idcontador);
    var ta = $("#" + idtext);
    contador.html("0/" + max);

    contador.html(ta.val().length + "/" + max);
    if (parseInt(ta.val().length) > max) {
        ta.val(ta.val().substring(0, max - 1));
        contador.html(max + "/" + max);
    }

}


$(document).ready(function () {

    //Menú: opción activa:
    var $location = location.href;
    $("a[href='" + $location + "']").parent().addClass('active');
    $("a[href='" + $location + "']").parent().parent().parent().addClass('active');

    //Solicitud nueva?
    $('table.dataTable').on('click', 'a[href="#confirma"]', function (e) {

        e.preventDefault();

        var pregunta = $(this).data('pregunta');
        var action = $(this).data('action');

        bootbox.confirm(pregunta, function (result) {
            if (result) {
                document.location = action;
            }
        });
    });
    $('a[href="#confirma"]').click(function (e) {

        e.preventDefault();

        var pregunta = $(this).data('pregunta');
        var action = $(this).data('action');

        bootbox.confirm(pregunta, function (result) {
            if (result) {
                document.location = action;
            }
        });
    });

    $('table.dataTable').on('click', 'a[href="#destroy"]', function (e) {
        e.preventDefault();

        var label = $(this).data('label');
        var model = $(this).data('model');
        var action = $(this).data('action');

        $('#modalDestroy-Label').html(label);
        $('#modalDestroy-Model').html(model);
        $('#modalDestroy-Action').attr('href', action);
    });
    $('a[href="#destroy"]').click(function (e) {
        e.preventDefault();

        var label = $(this).data('label');
        var model = $(this).data('model');
        var action = $(this).data('action');

        $('#modalDestroy-Label').html(label);
        $('#modalDestroy-Model').html(model);
        $('#modalDestroy-Action').attr('href', action);
    });

    $('.dataTable').on('click', 'a[href="#info"]', function (e) {
        e.preventDefault();

        var label = $(this).data('label');
        var info = $(this).data('info');

        $('#modalInfo-Label').html(label);
        $('#modalInfo-Body').html(info);
    });
    $('a[href="#info"]').click(function (e) {
        e.preventDefault();

        var label = $(this).data('label');
        var info = $(this).data('info');

        $('#modalInfo-Label').html(label);
        $('#modalInfo-Body').html(info);
    });

    $('.dataTable').on('click', 'a[href="#prompt"]', function (e) {
        e.preventDefault();

        var label = $(this).data('label');
        var info = $(this).data('info');
        var model = $(this).data('model');
        var action = $(this).data('action');

        $('#modalPrompt-Label').html(label);
        $('#modalPrompt-Body').html(info);
        $('#modalPrompt-Model').html(model);
        $('#modalPrompt-Form').attr('action', action)
    });
    $('a[href="#prompt"]').click(function (e) {
        e.preventDefault();

        var label = $(this).data('label');
        var info = $(this).data('info');
        var model = $(this).data('model');
        var action = $(this).data('action');

        $('#modalPrompt-Label').html(label);
        $('#modalPrompt-Body').html(info);
        $('#modalPrompt-Model').html(model);
        $('#modalPrompt-Form').attr('action', action)
    });


    $('.multiselect:not(.multiselect-basic)').multiselect({
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true
        // includeSelectAllOption: true,
        // selectAllText: "Todos",
    });
    $('.multiselect-basic').multiselect();


    $('#filtro-categoriasg').multiselect({
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        selectAllText: 'Todas',
        selectAllValue: '0',
        allSelectedText: "Todas",
        nSelectedText: ' seleccionadas',
        enableClickableOptGroups: false,
        selectAllJustVisible: true,
        disableIfEmpty: true,
        maxHeight: 100,
        dropUp: true,
        selectAllName: 'categorias'
    });
    $('#filtro-subcategoriasg').multiselect({
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        selectAllText: 'Todas',
        selectAllValue: '0',
        allSelectedText: "Todas",
        nSelectedText: ' seleccionadas',
        enableClickableOptGroups: false,
        selectAllJustVisible: true,
        disableIfEmpty: true,
        maxHeight: 100,
        dropUp: true,
        selectAllName: 'subcategorias'
    });
    $('#filtro-subcategoriasdetg').multiselect({
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        selectAllText: 'Todas',
        selectAllValue: '0',
        allSelectedText: "Todas",
        nSelectedText: ' seleccionadas',
        enableClickableOptGroups: false,
        selectAllJustVisible: true,
        disableIfEmpty: true,
        maxHeight: 100,
        dropUp: true,
        selectAllName: 'subcategoriasdet'
    });

    $('#filtro-especialidadesg').multiselect({
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        selectAllText: 'Todas',
        selectAllValue: '0',
        allSelectedText: "Todas",
        nSelectedText: ' seleccionadas',
        enableClickableOptGroups: false,
        selectAllJustVisible: true,
        disableIfEmpty: true,
        maxHeight: 100,
        dropUp: true,
        selectAllName: 'especialidades'
    });
    $('#filtro-subespecialidadesg').multiselect({
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        selectAllText: 'Todas',
        selectAllValue: '0',
        allSelectedText: "Todas",
        nSelectedText: ' seleccionadas',
        enableClickableOptGroups: false,
        selectAllJustVisible: true,
        disableIfEmpty: true,
        maxHeight: 100,
        dropUp: true,
        selectAllName: 'subespecialidades'
    });

    //Manage menu disabled
    $('#navbar-manager li > a').each(function () {
        if ($(this).attr('href') == "#") {
            $(this).css('color', 'green');
        }
    });

    //tooltips
    // $("[data-tooltip]").each(function() {
    //     $(this).tooltip({'html':true, 'placement': 'bottom','title': $(this).data('tooltip')});
    // });


    //chosen
    $('.chosen-select').chosen({
        width: '100%'
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('.chosen-select-tab').chosen();
    });
    if (http_get_tab()) {
        // $('.chosen-select-tab').chosen();
    }

    //select2
    $('.select2').select2({
        width: '100%'
    });
    $("ul.select2-selection__rendered").sortable({
        containment: 'parent',
        stop: function (event, ui) {
            // event target would be the <ul> which also contains a list item for searching (which has to be excluded)
            var arr = Array.from($(event.target).find('li:not(.select2-search)').map(function () {
                return $(this).data('data').id
            }))
            // console.log(arr);

            let v = $(this).parent().parent().parent().parent().attr("id");
            let parentVal = $("#" + v + "_value");
            if (parentVal) {
                parentVal.val(arr);
            }
        }
    });
    // $(".select2_sortable").each(function(e){
        
    // });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // $('.select2').select2();
    });
    if (http_get_tab()) {
        // $('.select2').select2();
    }

    $('#modalTutores').on('shown.bs.modal', function () {
        $('.chosen-select-modal', this).chosen();
    });

    $('.chosen-select-multi').chosen();

    $("[data-label]").each(function () {
        $(this).tooltip({
            "placement": "top",
            "title": $(this).data("label")
        });
    });

    $("[data-label_left]").each(function () {
        $(this).tooltip({
            "placement": "left",
            "title": $(this).data("label_left")
        });
    });

    //rules nif/nie
    function tipodoc_nif() {
        return $('#tipodoc').val() == 0;
    }

    function tipodoc_nie() {
        return $('#tipodoc').val() == 1;
    }

    $("#tipodoc").change(function () {

        $("#documento").rules("remove");
        $("#documento").rules("add", {
            nifES: tipodoc_nif(),
            nieES: tipodoc_nie(),
        });

    });

    $("#frmValidar").validate({
        rules: {
            documento: {
                nifES: tipodoc_nif(),
                nieES: tipodoc_nie(),
            },
        }
    });

    $.extend($.validator.messages, {
        required: "Este campo es obligatorio.",
        remote: "Por favor, rellena este campo.",
        email: "Por favor, escribe una dirección de correo válida",
        url: "Por favor, escribe una URL válida.",
        date: "Por favor, escribe una fecha válida.",
        dateISO: "Por favor, escribe una fecha (ISO) válida.",
        number: "Por favor, escribe un número entero válido.",
        digits: "Por favor, escribe sólo dígitos.",
        creditcard: "Por favor, escribe un número de tarjeta válido.",
        equalTo: "Por favor, escribe el mismo valor de nuevo.",
        accept: "Por favor, escribe un valor con una extensión aceptada.",
        maxlength: $.validator.format("Por favor, no escribas más de {0} caracteres."),
        minlength: $.validator.format("Por favor, no escribas menos de {0} caracteres."),
        rangelength: $.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
        range: $.validator.format("Por favor, escribe un valor entre {0} y {1}."),
        max: $.validator.format("Por favor, escribe un valor menor o igual a {0}."),
        min: $.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
    });

});


$(function () {

    //frm-1click
    $('.frm-1click').submit(function () {
        $("input[type='submit']", this)
            .val("Enviando...")
            .attr('disabled', 'disabled');
        return true;
    });

    //tabs click
    var loc = document.location.href;
    if (loc.indexOf('#') > -1) {
        var tag = loc.substring(loc.indexOf('#'));
        $(".nav-tabs a[href='" + tag + "']").tab('show');
    }

    $('.datetime').datetimepicker({
        locale: 'es',
        format: 'L',
        // format: 'DD/MM/YYYY',
        useCurrent: true,
        // format: 'dd/mm/yyyy'
    });

    $('.datetime-time').datetimepicker({
        locale: 'es',
        useCurrent: true,
    });

    $('.datetime-hora').datetimepicker({
        locale: 'es',
        useCurrent: true,
        format: 'HH:ss',
    });

});

// tinymce.init({
//     forced_root_block : false,
//     menubar : false,
//     plugins: ["code","table", "link", "image"],
//     relative_urls: false,
//     toolbar1: "code | bold italic | styleselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | link",
//     toolbar2: "image",
//     mode : "specific_textareas",
//     editor_selector : 'myTextEditor',
// });
