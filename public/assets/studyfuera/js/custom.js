jQuery(document).ready(function($) {

	/*
	Advanced search
	=========================== */	
	$("#advanced").hide();

	$('.advanced').click(function(){
		$("#advanced").slideToggle();
		return false; 
	});
		
	$('.advanced-close').click(function(){
		$("#advanced").slideToggle("normal")
	});
	
	/*
	Contact
	=========================== */	
	//$( document ).ready(function() {
		$("#dropdown-contact").hide();
		$("#dropdown-contact").css({'visibility':'visible'});
	//});
	
	$('.btn-contact').click(function(){
		$("#dropdown-contact").slideToggle();
	});
	$('.contact-close').click(function(){
		$("#dropdown-contact").slideToggle();
	});
		
	/*
	Hidden
	=========================== */		
	$(".frame-hover, .caption-bg").css({'opacity':'0','filter':'alpha(opacity=0)'});

	
	/*
	Social and wrapper-bg hover
	=========================== */
	$('.social-link').hover(
		function() {
			$(this).find('.frame-hover').stop().fadeTo(500, 1);
			$(this).find('.social-icon').stop().fadeTo(900, 0.7);
		},
		function() {
			$(this).find('.frame-hover').stop().fadeTo(500, 0);
			$(this).find('.social-icon').stop().fadeTo(900, 1);
		}
	)
	$('.home').hover(
		function() {
			$(this).find('.frame-hover').stop().fadeTo(500, 1);
			$(this).find('.row').fadeTo(500, 0);
		},
		function() {
			$(this).find('.frame-hover').stop().fadeTo(500, 0);
			$(this).find('.row').fadeTo(500, 1);
		}
	)

	/*
	Image caption hover
	=========================== */
	/*
	$('.programa a.titular').hover(
		function() {
			$(this).parents('.overflow').children('.mask').addClass('active');
			$(".plus", $(this).parents('.overflow')).stop().delay(400).animate({top:'50%', 'opacity': 1},{queue:true,duration:500});
		},
		function() {
			$(this).parents('.overflow').children('.mask').removeClass('active');
			$(".plus", $(this).parents('.overflow')).stop().animate({top:'-50%', 'opacity': 0},{queue:false,duration:300});
		}
	)
	*/
	
});	