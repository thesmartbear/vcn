jQuery(document).ready(function($){
    var $lateral_menu_trigger = $('#cd-menu-trigger'),
        $content_wrapper = $('.cd-main-content'),
        $navigation = $('header'),
        $bgheader = $('.headerbg');
        $whatsapp = $('.whatsapp');

    //open-close lateral menu clicking on the menu icon
    $lateral_menu_trigger.on('click', function(event){
        event.preventDefault();

        toggleSearch('close');

        $lateral_menu_trigger.toggleClass('is-clicked');
        $navigation.toggleClass('lateral-menu-is-open');
        $bgheader.toggleClass('lateral-menu-is-open');
        $whatsapp.toggleClass('lateral-menu-is-open');
        $content_wrapper.toggleClass('lateral-menu-is-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
            // firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
            $('body').toggleClass('overflow-hidden');
        });
        $('#cd-lateral-nav').toggleClass('lateral-menu-is-open');

        //check if transitions are not supported - i.e. in IE9
        if($('html').hasClass('no-csstransitions')) {
            $('body').toggleClass('overflow-hidden');
        }
    });

    //close lateral menu clicking outside the menu itself
    $content_wrapper.on('click', function(event){
        if( !$(event.target).is('#cd-menu-trigger, #cd-menu-trigger span') ) {
            $lateral_menu_trigger.removeClass('is-clicked');
            $navigation.removeClass('lateral-menu-is-open');
            $bgheader.removeClass('lateral-menu-is-open');
            $whatsapp.removeClass('lateral-menu-is-open');
            $content_wrapper.removeClass('lateral-menu-is-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                $('body').removeClass('overflow-hidden');
            });
            $('#cd-lateral-nav').removeClass('lateral-menu-is-open');
            //check if transitions are not supported
            if($('html').hasClass('no-csstransitions')) {
                $('body').removeClass('overflow-hidden');
            }

        }
    });

    //open (or close) submenu items in the lateral menu. Close all the other open submenu items.
    // $('.item-has-children').children('a').on('click', function(event){
    //     event.preventDefault();
    //     $(this).toggleClass('submenu-open').next('.sub-menu').slideToggle(200).end().parent('.item-has-children').siblings('.item-has-children').children('a').removeClass('submenu-open').next('.sub-menu').slideUp(200);
    // });

    //open search form
    $('.cd-search-trigger').on('click', function(event){
        event.preventDefault();
        toggleSearch();
        $('#search').focus();
    });

    function toggleSearch(type) {
        if(type=="close") {
            //close serach
            $('.cd-search').removeClass('is-visible');
            $('.cd-search-trigger').removeClass('search-is-visible');
            $('.cd-overlay').removeClass('search-is-visible');
        } else {
            //toggle search visibility
            $('.cd-search').toggleClass('is-visible');
            $('.cd-search-trigger').toggleClass('search-is-visible');
            $('.cd-overlay').toggleClass('search-is-visible');
            if($('.cd-search').hasClass('is-visible')) $('.cd-search').find('input[type="search"]').focus();
            ($('.cd-search').hasClass('is-visible')) ? $('.cd-overlay').addClass('is-visible') : $('.cd-overlay').removeClass('is-visible') ;
        }
    }

    $("[rel=tooltip]").tooltip({
        placement: 'top',
        animation: true,

    });

    if($('html').attr('lang') == 'es'){
        titleearlybird = '<div class="tooltipinfo"><p><span>Early bird</span><br />Un precio especial para todos los alumnos que se planifican con tiempo y se inscriben en nuestros programas antes del 2 de marzo.</p></div>';
        titleplanb = '<div class="tooltipinfo"><p><span>Plan B</span><br />Otras fechas disponibles sin grupo, ni monitor, ni vuelo.</p></div>' ;
    }
    if($('html').attr('lang') == 'ca'){
        titleearlybird = '<div class="tooltipinfo"><p><span>Early bird</span><br />Un preu especial per a tots els alumnes que es planifiquen amb temps i s\'inscriuen en els nostres programes abans del 2 de març.</p></div>';
        titleplanb = '<div class="tooltipinfo"><p><span>Plan B</span><br />Altres dates disponibles sense grup, ni monitor, ni vol.</p></div>' ;
    }

    $('.earlybird, img[src="../img/earlybird.png"]').tooltip({
        placement: 'top',
        animation: true,
        html : true,
        title: titleearlybird
    });
        
});