<?php
 
//grab named inputs from html then post to #thanks
if (isset($_POST['name'])) {
	$name = strip_tags($_POST['name']);
	if(isset($_POST['email']) && $_POST['email'] != ''){
		$email = "<br /><strong>E-mail:</strong> ".strip_tags($_POST['email']);
	}else{
		$email = '';
	}
	$tel = strip_tags($_POST['tel']);
	$curso = strip_tags($_POST['curso']);
	//$opciones = strip_tags($_POST['opciones']);
	

	//generate email and send!
	$to = 'viatges@iccic.edu';
	$email_subject = "Solicitud de información";
	$email_body = "<strong>Sol·licitut d'informaci&oacute; per: $curso</strong> <br /><br />".
	"<strong>Nom:</strong> $name <br />".
	"<strong>Tel&egrave;fon:</strong> $tel".
	$email;
	$headers = "From: viatges@iccic.edu <viatges@iccic.edu>\n";
	$headers .= "Reply-To: viatges@iccic.edu";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	$result = mail($to,$email_subject,$email_body,$headers);
	if(!$result) {
		echo $mail->ErrorInfo;
	} else {

		echo '<h3>Gràcies per confiar en nosaltres</h3><h4> En breu ens posarem en contacte per oferir-te la millor solució.</h4>
		<p>&nbsp;</p>
		<h5 class="text-center">Descobreix més sobre les experiències a CIC escola idiomes i segueix-nos!</h5>

					<ul class="msgsocial">
						<ul>
						<li>
							<a href="http://twitter.com/cicviatges" class="icon" target="_blank"><i class="fa fa-twitter"></i> Twitter</a>
						</li>

						<li>
							<a href="http://www.landedblog.com/iccic" class="icon" target="_blank"><i class="fa fa-rss"></i> Landed. El bloc de la CIC</a>
						</li>
					</ul>
		';
	}

}
?>