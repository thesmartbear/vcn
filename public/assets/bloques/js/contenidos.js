	if(!jQuery.browser.mobile && !$("#header").hasClass('smallfixed')){
		if($(window).height() > 700){
			$('.filtros').affix({
				offset:{
				top: 50
				}
			});
			
			$('.info-basica').affix({
				offset:{
				top: 30
				}
			});
			//$('.info-basica').css({'margin-top': '-100px'});

		}
	
	}else{
	   	$('#sidebar').hide();
	   	$("#header").addClass('headersmall');
		$(".seccion").addClass('headersmall');
		
	}
	
	$(".seccion").width($(".referencia").width());
	/*
	$('#programas').isotope({
		  itemSelector: '.programa',
		  masonry: {
		  columnWidth: '.programa'
		}
	})
	*/
	
	
	// left: 37, up: 38, right: 39, down: 40,
	// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
	var keys = [37, 38, 39, 40];
	
	function preventDefault(e) {
	  e = e || window.event;
	  if (e.preventDefault)
		  e.preventDefault();
	  e.returnValue = false;  
	}
	
	function keydown(e) {
		for (var i = keys.length; i--;) {
			if (e.keyCode === keys[i]) {
				preventDefault(e);
				return;
			}
		}
	}
	
	function wheel(e) {
	  preventDefault(e);
	}
	
	function disable_scroll() {
	  if (window.addEventListener) {
		  window.addEventListener('DOMMouseScroll', wheel, false);
	  }
	  window.onmousewheel = document.onmousewheel = wheel;
	  document.onkeydown = keydown;
	}
	
	function enable_scroll() {
		if (window.removeEventListener) {
			window.removeEventListener('DOMMouseScroll', wheel, false);
		}
		window.onmousewheel = document.onmousewheel = document.onkeydown = null;  
	}
	
	$('#main-menu').bind('touchstart click', function(e){
		$("#menu").stop().animate({"margin-top": ($(window).height()*2)+'px', 'left' : '40%'}, {duration: 1500,easing:"easeOutExpo"});
		//$(".capa").stop().animate({"margin-top": '100%'}, {duration: 1500,easing:"easeOutExpo"});
		$("#plusinfo").stop().animate({"opacity": '0'}, {duration: 100});
		$("#chat").stop().animate({"opacity": '0'}, {duration: 100});
		$("#toTop").stop().animate({"opacity": '0'}, {duration: 100});
		$("#inside-menu").css({'display': 'table'});
		$("#inside-menu").stop().animate({"top": '0'}, {duration: 1500,easing:"easeOutExpo"});
		
		$("#menubg").stop().animate({"margin-top": ($(window).height()*2)+'px', 'left' : '40%'}, {duration: 1500,easing:"easeOutExpo"});
		
		//$("#allcontent").stop().animate({"margin-top": '100%', 'position' : 'absolute', 'left': '-10%'}, {duration: 1500,easing:"easeOutExpo"});
		disable_scroll();
		
		//$("#logo").fadeOut(200).delay(1200).fadeIn(500);
	});
	$('#menu-close').bind('touchstart click', function(){
		$("#menu").stop().animate({"margin-top": '0', 'left' : '50%'}, {duration: 1500,easing:"easeOutExpo"});
		//$(".capa").stop().animate({"margin-top": '0'}, {duration: 1500,easing:"easeOutExpo"});
		$("#plusinfo").stop().animate({"opacity": '1'}, {duration: 100});
		$("#chat").stop().animate({"opacity": '1'}, {duration: 100});
		$("#toTop").stop().animate({"opacity": '1'}, {duration: 100});
		$("#menubg").stop().animate({"margin-top": '-120px', 'left': '50%'}, {duration: 1500,easing:"easeOutExpo"});
		$("#inside-menu").stop().animate({"top": '-100%'}, {duration: 1500,easing:"easeOutExpo"});
		//$("#allcontent").stop().animate({"margin-top": '0', 'left' : '0'}, {duration: 1500,easing:"easeOutExpo"});
		enable_scroll();
		//$("#logo").fadeOut(200).delay(500).fadeIn(800);
	});
	
	
	
	
	
	if($("#header").hasClass('smallfixed')){
		$("#contenido").css({'padding-top':'260px'});
	}
	
	if($(document).scrollTop() >= 100 && (!jQuery.browser.mobile || $(document).width()>768) && !$("#header").hasClass('smallfixed')){
			$("#header").addClass('headersmall');
			$(".seccion").addClass('headersmall');
			$(".seccion").width($(".referencia").width());
	}
	
	$(document).scroll(function () {
		if(!jQuery.browser.mobile){
			var y = $(document).scrollTop();
				var scrollPercent = 100 * $(window).scrollTop() / ($(document).height() - $(window).height());
		
			if (y >=10) {
				$("#header").addClass('headersmall');
				$(".seccion").addClass('headersmall');
				$(".seccion").width($(".referencia").width());
			} else if(!$("#header").hasClass('smallfixed')){
				$("#header").removeClass('headersmall');
				$(".seccion").removeClass('headersmall');
				
			}
			/*
			if ($("#header").hasClass('headersmall')) {
				//console.log('in');
				$("#contenido").css({
				'padding-top': '260px'
			  });
			} else {
				//console.log('out');
				$("#contenido").css({
				'padding-top': '360px'
			  });
			}
			*/
			$(".seccion span").css({'width' : scrollPercent+'%'});
		}
	});
	
    $("[rel=tooltip]").tooltip({
		placement: 'top',
		animation: true,
		
	});
	
	if($('html').attr('lang') == 'es'){
		titleearlybird = '<div class="tooltipinfo"><p><span>Early bird</span><br />Un precio especial para todos los alumnos que se planifican con tiempo y se inscriben en nuestros programas antes del 29 de febrero.</p></div>';
		titleplanb = '<div class="tooltipinfo"><p><span>Plan B</span><br />Otras fechas disponibles sin grupo, ni monitor, ni vuelo.</p></div>' ;
	}
	if($('html').attr('lang') == 'ca'){
		titleearlybird = '<div class="tooltipinfo"><p><span>Early bird</span><br />Un preu especial per a tots els alumnes que es planifiquen amb temps i s\'inscriuen en els nostres programes abans del 29 de febrer.</p></div>';
		titleplanb = '<div class="tooltipinfo"><p><span>Plan B</span><br />Altres dates disponibles sense grup, ni monitor, ni vol.</p></div>' ;
	}
	
	$('.earlybird, img[src="../img/earlybird.png"]').tooltip({
		placement: 'top',
		animation: true,
		html : true,
		title: titleearlybird
	});
	
	$(".planbinfo").tooltip({
		placement: 'top',
		animation: true,
		html : true,
		title: titleplanb 
	});

	//if($('.referencia').lenght) {

		var offset = $(".referencia").offset();
		$(".titulo").css({'padding-left': offset.left});

		$(window).resize(function () {
			var offset = $(".referencia").offset();
			$(".titulo").css({'padding-left': offset.left});
			$(".seccion").width($(".referencia").width());

			if ($(window).height() > 700) {
				$('.filtros').affix({
					offset: {
						top: 50
					}
				});

				$('.info-basica').affix({
					offset: {
						top: 30
					}
				});
				if ($(document).scrollTop > 100) {
					$('.info-basica').css({'margin-top': '-100px'});
				}

			} else {
				$(window).off('.affix');

				$('.filtros').removeData('bs.affix').removeClass('affix affix-top affix-bottom');

				$('.info-basica').removeData('bs.affix').removeClass('affix affix-top affix-bottom');
				$('.info-basica').css({'margin-top': '0px'});

			}
		});

	//}
	
	$.cookie('info', $("#plusinfo").attr('class'), { expires: null });
	$("#plusinfo").addClass($.cookie('info'));
	
	
	$('#plusinfo .close').click(function () {
		$("#plusinfo").addClass('smallbtn');
		$.cookie('info', $("#plusinfo").attr('class'));
	});	
	
	$('#plusinfo .icon-info').click(function () {
		if($(this).parents('#plusinfo').hasClass('smallbtn')){
			$("#plusinfo").removeClass('smallbtn');
			$.cookie('info', $("#plusinfo").attr('class'));
		}
	});	
	
	
	$('#cursotabs a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	  $(document).scrollTop(100);
	})
	
	
	
	$('.selectpicker').selectpicker({
		style: 'btn-muted btn-small',
		width: '100%'
		
	});

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
          $('.selectpicker').selectpicker('mobile');
        } 
		
		$('.selectpicker').on('change', function () {
			$(document).scrollTop(0);
		});	
		
		
/*
	search
	=========================== */	

	
	$('.btn-search').click(function(){
		//var toggleWidth = $("#search-box").width() == 232 ? "0px" : "232px";
		if(document.location.hostname == 'viatges.iccic.edu'){
			var ancho = $(".btn-contact").width()+$(".social").width()+$(".btn-search").width()+5;
			$('#search-box input.searchbox-input').css({'width': '160px'});
		}else{
			var ancho = $(".social").width()-$(".btn-search").width()+65;	
		}
		var toggleWidth = $("#search-box").width() == ancho ? "0px" : ancho+"px";
        $('#search-box').animate({ width: toggleWidth });
		if($(".btn-search .fa").hasClass('fa-search')){
			$(".btn-search .fa").removeClass('fa-search');
			$(".btn-search .fa").addClass('fa-times');
		}else{
			$(".btn-search .fa").removeClass('fa-times');
			$(".btn-search .fa").addClass('fa-search');	
		}
		$("#search").focus();
	});		