$( document ).ready(function() {
		var markers = [
		  {coords: [343, 480], name: 'Almer\u00eda', region: 'andalucia',  content: '<p><strong>Almer\u00EDa</strong><br />Jos&eacute; Mar\u00EDa Torres<br />Centro Ingl&eacute;s<br />C/ Jacinto 1<br />04720 Almer\u00EDa<br />Tel. 950 344 888 / 639 506 690</p>'},
		  {coords: [343, 480], name: 'Almer\u00eda Centro', region: 'andalucia',  content: '<p><strong>Almer\u00EDa Centro</strong><br />Tel. 950 234 551<br />almeriacentro@britishsummer.com</p>'},
		  {coords: [289, 400], name: 'Baeza', region: 'andalucia',  content: '<p><strong>Baeza</strong><br />Aula Integral de Formaci&oacute;n S.L.<br />C/ de Rafael Alberti, 3<br />23440 Baeza<br />Tel. 953 671 759<br />antequera@britishsummer.com</p>'},
		  {coords: [220, 408], name: 'C\u00f3rdoba', region: 'andalucia',  content: '<p><strong>C&oacute;rdoba</strong><br />CLC<br />Avda. de los Molinos, 7<br />14001 C&oacute;rdoba<br />Tel. 957 487 125<br />ordoba@britishsummer.com</p>'},
		  {coords: [106, 451], name: 'Huelva', region: 'andalucia',  content: '<p><strong>Huelva</strong><br />Cenrto de Idiomas Covent Garden<br />C/ Isaac Peral, 2<br />21002 Huelva<br />Tel.: 959 28 28 29<br />huelva@britishsummer.com</p>'},
		  {coords: [282, 456], name: 'Granada', region: 'andalucia',  content: '<p><strong>Granada</strong><br />Tel. 958 161 296 / 678 050 172<br />granada@britishsummer.com</p>'},
		  {coords: [274, 419], name: 'Ja\u00e9n', region: 'andalucia',  content: '<p><strong>Ja&eacute;n</strong><br />Boulevard English Center<br />Manuel Caballero Venzal&aacute;, 1<br />Entreplanta F<br />20009 Ja&eacute;n<br />Tel. 953 271 261<br />jaen@britishsummer.com</p>'},
		  {coords: [142, 488], name: 'Jerez de la Frontera',  content: '<p><strong>Jerez</strong><br />Lola Iekeler<br />Tel. 608 277 931<br />jerez@britishsummer.com</p>'},
		  {coords: [238, 488], name: 'M&aacute;laga', region: 'andalucia', region: 'andalucia',  content: '<p><strong>M&aacute;laga</strong><br />Francisco Garc\u00EDa Barranquero<br />William Shakespeare, 2. Bl. 2. &Aacute;tico B<br />29730 Rinc&oacute;n de la Victoria<br />Tel. 656 868 344<br />malaga@britishsummer.com</p>'},
		  {coords: [238, 488], name: 'Marbella', region: 'andalucia', region: 'andalucia',  content: '<p><strong>Marbella</strong><br />Tel. 952 822 191<br />marbella@britishsummer.com</p>'},
		  
		  {coords: [421, 144], name: 'Zaragoza', region: 'aragon',  content: '<p><strong>Zaragoza</strong><br />Gran V\u00EDa, 7<br />50006 Zaragoza<br />Tel. 976 238 485<br />zaragoza@britishsummer.com</p>'},
		  
		  {coords: [174, 18], name: 'Gij\u00f3n', region: 'asturias',  content: '<p><strong>Gij&oacute;n</strong><br />Robert School of English<br />C/ Compositor Facundo de la Vi&ntilde;a 3<br />33204 Gij&oacute;n (Asturias)<br />Tel. 630 344 394<br />gijon@britishsummer.com</p>'},
		  {coords: [165, 35], name: 'Oviedo', region: 'asturias',  content: '<p><strong>Oviedo</strong><br />Tel. 655 864 930<br />oviedo@britishsummer.com</p>'},
		  
		  {coords: [271, 30], name: 'Santander', region: 'cantabria',  content: '<p><strong>Santander</strong><br />Asheville English<br />C/ Gonzalez Hoyos 7, bajo 7<br /> 39012 Valdenoja (Santander)<br />Tel. 942 390 916<br />santander@britishsummer.com</p>'},
		  
		  {coords: [276, 97], name: 'Burgos', region: 'castillaleon',  content: '<p><strong>Burgos</strong><br />Academia ILIMA<br />C/ Alonso Mart\u00EDnez 5 2<br />09003 Burgos<br />Tel. 947 207 469<br />burgos@britishsummer.com</p>'},
		  {coords: [232, 123], name: 'Palencia', region: 'castillaleon',  content: '<p><strong>Palencia</strong><br />Goal Centro de Idiomas<br />C/ Mayor Principal 136, 4 A<br />34001 Palencia<br />Tel. 979 701 224<br />palencia@britishsummer.com </p>'},
		  {coords: [222, 145], name: 'Valladolid', region: 'castillaleon',  content: '<p><strong>Valladolid</strong><br />Long View International School<br />C/ Jose Antonio Primo de Rivera 5 1 B<br />47001 Valladolid<br />Tel. 983 330 203<br />valladolid@britishsummer.com</p>'},
		  
		  {coords: [241, 257], name: 'Ciudad Real', region: 'castillalamancha',  content: '<p><strong>Ciudad Real</strong><br />Tel. 926 674 346<br />ciudadreal@britishsummer.com</p>'},
		  
		  {coords: [576, 168], name: 'Barcelona', region: 'catalunya',  content: '<p><strong>BARCELONA</strong><br />V\u00EDa Augusta, 33<br />08006 Barcelona<br />Tel. 93 200 88 88<br />Fax 93 202 23 71<br />infobs@britishsummer.com</p>'},
		  {coords: [583, 132], name: 'Girona', region: 'catalunya',  content: '<p><strong>GIRONA</strong><br />Carrer Migdia, 25<br />17002 Girona<br />Tel. 972 414 902<br />girona@britishsummer.com</p>'},
		  {coords: [583, 153], name: 'Granollers', region: 'catalunya',  content: '<p><strong>Granollers</strong><br />British Summer &amp; Cambridge<br />Pla&ccedil;a Manel Montany&agrave;, 4<br />08401 Granollers<br />Tel. 93 870 20 01<br />granollers@cambridgeschool.com</p>'},
		  {coords: [572, 145], name: 'Manresa', region: 'catalunya',  content: '<p><strong>Manresa</strong><br />British Summer &amp; Babel<br />C/ Ind&uacute;stria, 28 1o<br />08243 Manresa<br />Tel. 93 874 08 55<br />babel@babelidiomes.com</p>'},
		  {coords: [572, 145], name: 'Sant Cugat', region: 'catalunya',  content: '<p><strong>Sant Cugat</strong><br />British Summer &amp; Sprint<br />C/ Francesc Moragas, 8<br />08172 Sant Cugat del Vall\00E8s<br />Tel. 93 589 22 64<br />sprintidiomes@gmail.com</p>'},
		  {coords: [572, 145], name: 'Terrassa', region: 'catalunya',  content: '<p><strong>Terrassa</strong><br />Tweed Escola d\'idiomes<br />Plaça Salvador Espriu, 4 Local M<br />08221 Terrassa<br />Tel. 666 180 964<br />info@tweed.cat</p>'},
		  
		  {coords: [134, 351], name: 'Badajoz', region: 'extremadura',  content: '<p><strong>Badajoz</strong><br />Tel. 924 671 759 / 696 913 200<br />extremadura@britishsummer.com</p>'},
		  
		  {coords: [52, 29], name: 'A Coruña', region: 'galicia',  content: '<p><strong>A Coru&ntilde;a</strong><br />Tel. 981 170 290<br />acoruna@britishsummer.com</p>'},
		  
		  {coords: [345, 110], name: 'Calahorra', region: 'larioja',  content: '<p><strong>Calahorra</strong><br />English Center<br />Tel. 941 135 660<br />C/ General Gallarza 27, 1o izda<br />26500 Calahorra (La Rioja)<br />calahorra@britishsummer.com</p>'},
		  
		  
		  {coords: [280, 227], name: 'Madrid Zona Moraleja', region: 'madrid',  content: '<p><strong>Madrid Zona Moraleja</strong><br />Esgarviajes<br />Camino del Cura, 10<br />C.C. El Encinar de los Reyes<br />28109 Alcobendas<br />Tel. 916 255 292<br />moraleja@britishsummer.com</p>'},
		  
		  
		  {coords: [276, 230], name: 'Madrid', region: 'madrid',  content: '<p><strong>Madrid</strong><br />Paseo de la Castellana<br />136 bajos<br />28046 MADRID<br />Tel.: 91 345 95 65<br />madrid@britishsummer.com</p>'},
		  {coords: [154, 444], name: 'Sevilla',  content: '<p><strong>SEVILLA</strong><br />Pza. Cristo de Burgos<br />21. Bajo A<br />41003 Sevilla<br />Tel.: 95 421 07 85<br />sevilla@britishsummer.com</p>'},
		  
		  {coords: [350, 42], name: 'Bilbao', region: 'paisvasco',  content: '<p><strong>Vizcaya</strong><br />The Method School of English<br />C. Navarra, 6<br />48001 Bilbao<br />Tel. 944 408 579<br />bilbao@britishsummer.com</p>'},
		  
		  {coords: [450, 300], name: 'Valencia', region: 'valencia',  content: '<p><strong>Valencia</strong><br />Tel. 960 045 691<br />valencia@britishsummer.com</p>'},
		  {coords: [465, 265], name: 'Castell\u00F3n', region: 'valencia',  content: '<p><strong>Castell\u00F3n</strong><br />Tel. 964 234 098<br />castellon@britishsummer.com</p>'}
		];
		
      map = new jvm.WorldMap({
        container: $('#mapa'),
        map: 'map_es',
		backgroundColor: '#FFF',
		regionStyle: 
		{
		  initial: {
			fill: '#e5e5e5',
			"fill-opacity": 1,
			stroke: '#FFF',
			"stroke-width": 2,
			"stroke-opacity": 1
		  },
		  hover: {
			"fill-opacity": 0.6
		  },
		  selected: {
			fill: '#E2DCCF'
		  },
		  selectedHover: {
			  "fill-opacity": 1
		  }
		},
		
		
		markers: markers,
		
		markerStyle: {
		  initial: {
			fill: '#f1c40f',
			stroke: '#f1c40f'
		  },
		  hover: {
			fill: '#f1c40f',
			stroke: '#f1c40f',
			r: 8
		  },
		  selected: {
			fill: '#000000',
			stroke: '#000000',
			r: 10
		  },
		  selectedHover: {
			  fill: '#000000',
			stroke: '#000000',
			r: 10
		  }
		},
		
		
		onRegionLabelShow: function(e, label, code){
			//console.log('Show code> ' + code);
			//label.html('Anything you want');
			//or do what you want with label, it's just a jQuery object
			/*
			if(label.text() == ''){
				$('.jvectormap-label').css({'display': 'none', 'visibility': 'hidden'});
			}else{
				$('.jvectormap-label').css({'display': 'block', 'visibility': 'visible'});
			}
			*/
			$('.jvectormap-label').css({'display': 'none', 'visibility': 'hidden'});
  		},
		
		onMarkerLabelShow: function(event, label, index){
			//console.log(markers[index].name);
			$('.jvectormap-label').css({'display': 'block', 'visibility': 'visible'});
		},
		
		onMarkerClick: function(event, index){
			console.log(markers[index].content);
			//$('#mapa_info').html(markers[index].content);
			map.setSelectedRegions(markers[index].region);
			//console.log(markers[index].name);
			
			$('#mapa_info').find("strong").each(function( index2 ) {
				//console.log( index + ": " + $(this).text() );
				if ($(this).text().toUpperCase() == markers[index].name.toUpperCase()){
					$(this).parent('p').addClass('selected');
				}
			});
			
		},

		regionsSelectable: true,
        regionsSelectableOne: true,
		onRegionSelected: function(e, code, isSelected,  selectedRegions){
			//console.log('e> ' + e + ' - code> ' + code  +' - selectedRegions> ' + selectedRegions);
			//console.log(map.getRegionName(code));			
			oficinas = false;
			$('#mapa_info').html('');
			$.each(markers, function (i, elem) {
				if (elem.region == selectedRegions){
					map.setFocus(selectedRegions);
					$('#mapa_info').append(elem.content);
					$('#mapa_info').append('</div>');
					oficinas = true;
				}
			});	
			if (oficinas == false){
				map.clearSelectedRegions();
				map.clearSelectedMarkers();
				map.reset();
				$('#mapa_info').html('<p class="barcelona"><strong>BARCELONA</strong><br>V&iacute;a Augusta, 33<br>08006 Barcelona<br>Tel. 93 200 88 88<br>Fax 93 202 23 71<br><a href="mailto:infobs@britishsummer.com">infobs@britishsummer.com</a></p><p class="girona"><strong>GIRONA</strong><br>Carrer Migdia, 25<br>17002 Girona<br>Tel. 972 414 902<br><a href="mailto:girona@britishsummer.com">girona@britishsummer.com</a></p><p class="madrid"><strong>MADRID</strong><br>Paseo de la Castellana <br>136 bajos<br>28046 MADRID<br>Tel.: 91 345 95 65<br><a href="mailto:madrid@britishsummer.com">madrid@britishsummer.com</a></p><p class="sevilla"><strong>SEVILLA</strong><br>Pza. Cristo de Burgos <br>21. Bajo A<br>41003 Sevilla<br>Tel.: 95 421 07 85<br><a href="mailto:sevilla@britishsummer.com">sevilla@britishsummer.com</a></p>');
				$('#dropdown-contact .oficinas').html('Oficinas<span></span>');
			}else{
				$('#dropdown-contact .oficinas').append(': '+map.getRegionName(code));
				
			}
			
		},
		
		markersSelectable: true,
        markersSelectableOne: true
		
      });
	  
	  $('#mapa_info p').live('mouseenter touchstart', function() {
			var city = $(this).find("strong").text();
			//console.log(city);
			$.each(markers, function (i, elem) {
				//console.log('> ' + elem.name);
				if (elem.name.toUpperCase() == city.toUpperCase()){
					map.setSelectedMarkers(i);
				}
			});
	  });
	  $('#mapa_info p').live('mouseleave touchend touchcancel', function() {
			map.clearSelectedMarkers();
				
	  });
	  
	  $('<div class="reset">ver todo</div>').insertBefore($('.jvectormap-zoomin'));
	  
	  $('.reset').live('click', function() {
			map.clearSelectedRegions();
				map.clearSelectedMarkers();
				map.reset();
				$('#mapa_info').html('<p class="barcelona"><strong>BARCELONA</strong><br>V&iacute;a Augusta, 33<br>08006 Barcelona<br>Tel. 93 200 88 88<br>Fax 93 202 23 71<br><a href="mailto:infobs@britishsummer.com">infobs@britishsummer.com</a></p><p class="girona"><strong>GIRONA</strong><br>Carrer Migdia, 25<br>17002 Girona<br>Tel. 972 414 902<br><a href="mailto:girona@britishsummer.com">girona@britishsummer.com</a></p><p class="madrid"><strong>MADRID</strong><br>Paseo de la Castellana <br>136 bajos<br>28046 MADRID<br>Tel.: 91 345 95 65<br><a href="mailto:madrid@britishsummer.com">madrid@britishsummer.com</a></p><p class="sevilla"><strong>SEVILLA</strong><br>Pza. Cristo de Burgos <br>21. Bajo A<br>41003 Sevilla<br>Tel.: 95 421 07 85<br><a href="mailto:sevilla@britishsummer.com">sevilla@britishsummer.com</a></p>');
				$('#dropdown-contact .oficinas').html('Oficinas<span></span>');
	  });
	  
	 
	  
    });