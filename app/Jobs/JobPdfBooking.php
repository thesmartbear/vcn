<?php

namespace VCN\Jobs;

use VCN\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use VCN\Models\Bookings\Booking;

use Log;

class JobPdfBooking extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $booking;
    protected $force;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Booking $booking, $force = false)
    {
        $this->booking = $booking;
        $this->force = $force;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Log::info("job pdf: ". $this->booking->id );
        $this->booking->pdf_generar(false, $this->force);
    }
}
