<?php

namespace VCN\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use VCN\Models\Cursos\Curso;

class JobDataWeb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $curso;
    protected $plazas;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Curso $curso, $plazas = false)
    {
        $this->curso = $curso;
        $this->plazas = $plazas;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->curso->updateDataWeb($this->plazas);
    }
}
