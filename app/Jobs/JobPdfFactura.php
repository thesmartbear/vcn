<?php

namespace VCN\Jobs;

use VCN\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\ShouldQueue;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingFactura;

use PDF;
// use Log;

class JobPdfFactura extends Job implements ShouldQueue
{
    protected $factura;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(BookingFactura $factura)
    {
        $this->factura = $factura;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->factura->pdf_generar();
    }
}
