<?php

namespace VCN\Jobs;

use VCN\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\ShouldQueue;

use VCN\Models\Bookings\BookingFactura;

class JobPdfFacturaAbono extends Job implements ShouldQueue
{
    // use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $factura;
    protected $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(BookingFactura $factura, $file)
    {
        $this->factura = $factura;
        $this->file = $file;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->factura->pdf_generar_abono($this->file);
    }
}
