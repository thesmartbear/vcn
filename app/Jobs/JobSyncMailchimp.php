<?php

namespace VCN\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use VCN\Models\System\Aviso;

class JobSyncMailchimp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $plataforma;
    protected $apikey;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($plataforma, $apikey)
    {
        $this->plataforma = $plataforma;
        $this->apikey = $apikey;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $log = Aviso::syncMailchimp($this->plataforma, $this->apikey);
    }
}
