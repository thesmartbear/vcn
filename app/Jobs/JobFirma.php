<?php

namespace VCN\Jobs;

use VCN\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\ShouldQueue;

use \VCN\Models\Bookings\Booking;
use VCN\Helpers\MailHelper;
use Log;
use ConfigHelper;

use VCN\Models\Bookings\BookingLog;

class JobFirma extends Job implements ShouldQueue
{
    protected $booking;
    protected $template;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Booking $booking, $template)
    {
        $this->booking = $booking;
        $this->template = $template;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $booking = $this->booking;

        $pdf = $booking->getArchivoFirmaPendiente();
        // $pdf = null;
        if(!$pdf)
        {
            $booking->pdf_generar(false, true);
        }

        foreach($booking->pdfs as $p)
        {
            $p->firmas = null;
            $p->visible = false;
            $p->save();
        }

        $pdf = $booking->pdfs->sortByDesc('id')->first();
        $pdf->firmas = 'pendiente';
        $pdf->visible = 1;
        $pdf->save();

        // MailHelper::mailBookingFirma($booking, $this->template);
    }
}
