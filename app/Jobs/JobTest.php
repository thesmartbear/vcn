<?php

namespace VCN\Jobs;

use VCN\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\ShouldQueue;

use VCN\Models\Bookings\Booking;

use Log;

class JobTest extends Job implements ShouldQueue
{
    protected $booking;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        Log::info("job booking: ". $this->booking->id);

        $mailer->send('emails.reminder', ['user' => $this->user], function ($m) {
             //
        });
    }
}
