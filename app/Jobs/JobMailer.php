<?php

namespace VCN\Jobs;

use VCN\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;

class JobMailer extends Job implements ShouldQueue
{
    protected $template;
    protected $args;
    protected $destino;
    protected $asunto;
    protected $adjunto;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($template, $args, $destino, $asunto, $adjunto=null)
    {
        $this->template = $template;
        $this->args = $args;
        $this->destino = $destino;
        $this->asunto = $asunto;

        $this->adjunto = $adjunto;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $destino = $this->destino;
        $asunto = $this->asunto;

        if($destino->email == "" || !filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
        {
            return false;
        }

        $plataforma = null;
        $baseUrl = config('app.url');

        $args = $this->args;

        if(!$args)
        {
            $args = [];
        }

        $p = $destino->plataforma ?: 1;
        if( isset($args['plataforma']) )
        {
            $p = $args['plataforma'];
        }

        $plataforma = \VCN\Models\System\Plataforma::find($p);
        $baseUrl = $plataforma->area_url;

        //Log::info($this->template .":". $asunto . "::". $p);

        $args += ['base_url'=> $baseUrl];

        //Log::info($args);

        try {
            
            $mailer->send($this->template, $args, function ($m) use ($destino, $asunto, $plataforma) {

                // $swiftMessage = $m->getSwiftMessage();
                // $headers = $swiftMessage->getHeaders();
                // $headers->addTextHeader('x-mailgun-native-send', 'true');
                
                if($plataforma)
                {
                    $m->from($plataforma->email, $plataforma->name);
                }
                
                if($this->adjunto)
                {
                    $m->attach($this->adjunto);
                }

                $m->to(trim($destino->email), $destino->full_name)->subject($asunto);
            });

        }catch(\Exception $e){\Log::info($e);}
    }
}
