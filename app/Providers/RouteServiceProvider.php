<?php

namespace VCN\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'VCN\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        // $this->mapApiRoutes();

        $this->mapManageRoutes();
        $this->mapAreaRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "panel" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapManageRoutes()
    {
        Route::group([
            'middleware'  => [ 'manage','auth' ],
            'namespace'   => $this->namespace,// . '\Manage',
            // 'prefix'      => 'manage',
            // 'as'          => 'manage.',
        ], function ($router) {
            require base_path('routes/manage.php');
        });
    }

    /**
     * Define the "panel" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAreaRoutes()
    {
        Route::group([
            //'middleware'  => [ 'web', 'auth.area' ],
            'middleware'  => [ 'web' ],
            'namespace'   => $this->namespace, // . '\Area',
            // 'prefix'      => 'area',
            // 'as'          => 'area.',
        ], function ($router) {
            require base_path('routes/area.php');
        });
    }
}
