<?php

namespace VCN\Providers;

use VCN\Helpers\ConfigHelper;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Session;
class Mailgun2ServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $p = ConfigHelper::propietario();
        $p = Session::get('vcn.commands.plataforma', $p);
        
        $env = env('APP_ENV');

        if($env == "local")
        {
            return;
        }
        
        // Config::set('mail.driver', 'mailgun');
        Config::set('services.mailgun.domain', env('MAILGUN_DOMAIN_'. $p, env('MAILGUN_DOMAIN')));
        Config::set('services.mailgun.secret', env('MAILGUN_SECRET_'. $p, env('MAILGUN_SECRET')));
        
        $mgEndpoint = env('MAILGUN_ENDPOINT_'. $p, null);
        if($mgEndpoint)
        {
            Config::set('services.mailgun.endpoint', $mgEndpoint);
        }
    }
}
