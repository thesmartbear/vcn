<?php

namespace VCN\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InformeArrayExport implements FromArray, WithTitle, WithHeadings
{
    private $title;
    private $data;

    public function __construct($data, $title)
    {
        $this->title = $title;
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function headings() : array
    {
        return array_keys($this->data);
    }
}
