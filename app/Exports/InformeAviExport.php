<?php

namespace VCN\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;

use Carbon;
use ConfigHelper;

class InformeAviExport implements WithMultipleSheets, WithTitle
{
    public $bookingsExcel;
    public $tabs;

    public function __construct($bookingsExcel, $tabs)
    {
        $this->bookingsExcel = $bookingsExcel;
        $this->tabs = $tabs;
    }

    public function sheets(): array
    {
        $tabs = $this->tabs;
        $bookingsExcel = $this->bookingsExcel;

        $sheets = [];

        foreach ($tabs as $k => $tab)
        {
            foreach ($tabs[$k] as $kc => $convo_id)
            {
                $exportar = clone $bookingsExcel;
                $ftipoc = ConfigHelper::getConvocatoriaIndex($k);
                $exportar = $exportar->where($ftipoc, $convo_id)->get();

                $bxls = [];
                $i = 0;
                foreach ($exportar as $model)
                {
                    $bxls[$i]['gender'] = $model->viajero->sexo == 1 ? 'M' : 'F';
                    $bxls[$i]['apellido1'] = $model->viajero->lastname;
                    $bxls[$i]['apellido2'] = $model->viajero->lastname2;
                    $bxls[$i]['name'] = $model->viajero->name;
                    $bxls[$i]['fechanac'] = Carbon::parse($model->viajero->fechanac)->format('d/m/Y');
                    $bxls[$i]['pais_home'] = $model->viajero->paisnac;
                    $bxls[$i]['pais_dest'] = $model->curso->centro->pais_name;
                    $bxls[$i]['program'] = $model->convocatoria ? $model->convocatoria->name : "-";

                    $dateic = Carbon::parse($model->course_start_date);
                    $dateia = Carbon::parse($model->accommodation_start_date);
                    $datei = $dateic;
                    if ($dateia->lt($dateic)) {
                        $datei = $dateia;
                    }

                    $datefc = Carbon::parse($model->course_end_date);
                    $datefa = Carbon::parse($model->accommodation_end_date);
                    $datef = $datefc;
                    if ($datefa->gt($datefc)) {
                        $datef = $datefa;
                    }

                    $bxls[$i]['date_start'] = $datei->format('d/m/Y');
                    $bxls[$i]['date_end'] = $datef->format('d/m/Y');

                    $bxls[$i]['contable'] = $model->contable_code;

                    $i++;
                }

                $convo = null;
                switch ($k) {
                    case 1:
                    case 2:
                    case 5: {
                            $convo = \VCN\Models\Convocatorias\Cerrada::find($convo_id);
                        }
                        break;

                    case 3: {
                            $convo = \VCN\Models\Convocatorias\Abierta::find($convo_id);
                        }
                        break;

                    case 4: {
                            $convo = \VCN\Models\Convocatorias\ConvocatoriaMulti::find($convo_id);
                        }
                        break;
                }

                $hoja = str_limit(($convo ? str_slug($convo->name) : "- Convocatoria NO EXISTE -"), 25);

                // $excel->sheet($hoja, function ($sheet) use ($bxls) {
                //     $sheet->fromArray($bxls);
                // });

                $sheets[] = new InformeAviExportSheet($hoja, $bxls);
            }
        }
    
        return $sheets;
    }

    public function title(): string
    {
        $fecha = Carbon::now()->format('d-m-Y');
        return "Informe AVI $fecha";
    }
}
