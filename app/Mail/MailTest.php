<?php

namespace VCN\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailTest extends Mailable
{
    use Queueable, SerializesModels;

    public $template;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($template)
    {
        $this->template = $template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $asunto = config('app.name') . ": test";
        $user = \VCN\Models\User::find(1);

        return $this
            ->from( $user )
            ->subject($asunto)->markdown($this->template);

        // return $this->view('view.name');
    }
}
