<?php

namespace VCN\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailTestMarkdown extends Mailable
{
    use Queueable, SerializesModels;

    protected $asunto;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($asunto)
    {
        $this->asunto = $asunto ?: "test";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->asunto)->markdown("emails.test_markdown");
    }
}
