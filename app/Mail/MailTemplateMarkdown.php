<?php

namespace VCN\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
//use Illuminate\Contracts\Queue\ShouldQueue;

class MailTemplateMarkdown extends Mailable
{
    use Queueable, SerializesModels;

    protected $destino;
    protected $asunto;
    protected $template;
    protected $args;
    protected $adjunto;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($destino, $asunto, $template, $args, $adjunto=null)
    {
        $this->destino = $destino;
        $this->asunto = $asunto;
        $this->template = $template;
        $this->args = $args;
        $this->adjunto = $adjunto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $plataforma = null;

        $p = $this->destino->plataforma ?: 1;
        $plataforma = \VCN\Models\System\Plataforma::find($p);

        //if($plataforma)
        //{
        //    $this->from($plataforma->email, $plataforma->name);
        //}

        if($this->adjunto)
        {
            $this->attach($this->adjunto);
        }

        return $this->markdown($this->template)
            ->from($plataforma->email, $plataforma->name)
            ->subject($this->asunto)
            ->with($this->args);
    }
}
