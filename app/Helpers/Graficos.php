<?php

namespace VCN\Helpers;

use VCN\Models\Solicitudes\Solicitud;
use VCN\Models\System\Oficina;
use VCN\Models\Informes\Venta;

use Carbon;
use Lava;
use Artisan;

class Graficos {

    public static function loadTrasvase($arrayDatos, $arrayCantidad, $from, $to)
    {
        Artisan::call('view:clear');

        $data = Lava::DataTable();
        $data->addColumn('string', $from)
            ->addColumn('string', $to)
            ->addColumn('number', 'Bookings');

        foreach($arrayDatos as $k=> $destinos)
        {
            if($k==0) //Sin Booking NO
            {
                continue;
            }

            $cat = \VCN\Models\Categoria::find($k);
            $catFrom = $cat?$cat->name:"Sin Booking";

            foreach($destinos as $kd => $cantidad)
            {
                $cat = \VCN\Models\Categoria::find($kd);
                $catTo = $cat?$cat->name:"Sin Booking";

                $a = [$catFrom. " ($arrayCantidad[$k])", $catTo, $cantidad];

                $data->addRow($a);
            }
        }

        $colors = ['#a6cee3', '#b2df8a', '#fb9a99', '#fdbf6f', '#cab2d6', '#ffff99', '#1f78b4', '#33a02c'];

        $width = 800;
        $height = 650;

        $chart = Lava::SankeyChart( "Trasvase", $data, [
            'width' => $width,
            'height' => $height,
            // 'legend' => [
            //     'position' => 'none'
            // ],
            'sankey' => [
                'node' => [
                    'colors' => $colors
                ],
                'link' => [
                    'colorMode' => 'gradient',
                    'colors' => $colors
                ]
            ]

        ]);
    }

    public static function loadEdades($edades, $edades_pais, $paises)
    {
        Artisan::call('view:clear');

        $datatable = Lava::DataTable();
        $datatable->addNumberColumn('Edad');

        foreach($paises as $kpais => $vpais)
        {
            $datatable->addNumberColumn($kpais);
        }

        foreach($edades as $kedad => $vedad)
        {
            $arr = [];

            $arr[] = $kedad;
            foreach($paises as $kpais => $vpais)
            {
                $v = 0;
                if(isset($edades_pais[$kedad][$kpais]))
                {
                    $v = $edades_pais[$kedad][$kpais];
                }

                $arr[] = $v;
            }

            $datatable->addRow($arr);
        }

        $barChart = Lava::BarChart('EdadesPais', $datatable, [
            'isStacked'=> 'percent',
            'title' => 'Bookings x Edades x Pais',
            'height'=> 1000,
            'width'=> 900,
        ]);

        $filter  = Lava::NumberRangeFilter($datatable->getColumnLabel(0));
        $control = Lava::ControlWrapper($filter, 'control');
        $chart   = Lava::ChartWrapper($barChart, 'chart');

        $dash = Lava::Dashboard('EdadesPais', $datatable)
                     ->bind($control, $chart);
    }

    public static function loadVentas($valores, $ventas, $ventasCurso, $solicitudes, $ofis, $categorias_graf)
    {
        Artisan::call('view:clear');

        $anys = Venta::groupBy('any')->get()->pluck('any','any')->toArray();
        $anys[end($anys)+1] = end($anys)+1;

        //Bookings y Semanas (para semanas seleccionadas): 1 y 2
        $sem = $valores['semana'];
        $sem0 = $sem-1;

        $chart1 = Lava::DataTable();
        $chart1->addStringColumn('Año')
                ->addNumberColumn("s:$sem0")
                ->addRoleColumn('string', 'annotation')
                ->addNumberColumn("s:".$sem)
                ->addRoleColumn('string', 'annotation');

        $chart2 = Lava::DataTable();
        $chart2->addStringColumn('Año')
                ->addNumberColumn("s:$sem0")
                ->addRoleColumn('string', 'annotation')
                ->addNumberColumn("s:".$sem)
                ->addRoleColumn('string', 'annotation');

        foreach($anys as $any)
        {
            if($any>$valores['any'])
            {
                continue;
            }

            $Ventas = clone $ventas;

            $s0b = 0;
            $s0s = 0;
            if($sem0)
            {
                $Ventas0 = clone $ventas;
                $v = $Ventas0->where('any',$any)->where('semana',$sem0);
                $s0b = $v->sum('inscripciones')?:0;
                $s0s = $v->sum('semanas')?:0;
            }

            $v = $Ventas->where('any',$any)->where('semana',$sem);
            $s1b = $v->sum('inscripciones')?:0;
            $s1s = $v->sum('semanas')?:0;

            $chart1->addRow([$any, $s0b, $s0b, $s1b, $s1b]);
            $chart2->addRow([$any, $s0s, $s0s, $s1s, $s1s]);
        }

        Lava::ColumnChart('Chart-Bookings-1', $chart1, [
            'title' => 'Num. Bookings',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ],
            'colors'=> ['#1b9e77', '#d95f02'],
            'height'=> 450,
        ]);

        Lava::ColumnChart('Chart-Bookings-2', $chart2, [
            'title' => 'Num. Semanas',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ],
            'colors'=> ['#d9a202', '#7570b3'],
            'height'=> 450,
        ]);

        //Bookings x Oficina: 3
        $any = (int)$valores['any'];
        $any00 = $any;
        $any01 = $any-1;

        $chart3 = Lava::DataTable();
        $chart3->addStringColumn('Oficina')
                ->addNumberColumn("$any01:s$sem")
                ->addRoleColumn('string', 'annotation')
                ->addNumberColumn("$any00:s$sem")
                ->addRoleColumn('string', 'annotation');

        foreach($ofis as $o=>$ofi_id)
        {
            $ofi = Oficina::find($ofi_id)->name;
            $vo0 = 0;
            $vo1 = 0;

            foreach($anys as $any)
            {
                if($any>$any00)
                {
                    continue;
                }

                $vOfi = clone $ventasCurso;
                if($any<$any00)
                {
                    // $vOfi = $vOfi->where('any', $any)->where('oficina_id',$ofi_id);
                    // $vOfi = $vOfi->where('curso_any', $any00);
                    // $vo0 += $vOfi->sum('inscripciones')?:0;
                }
                elseif($any==$any00)
                {
                    // $vOfi = $vOfi->where('any', $any)->where('semana','<=',$sem)->where('oficina_id',$ofi_id);
                    // $vOfi = $vOfi->where('curso_any', $any00);
                    $vOfi = $vOfi->where('curso_any', $any00)->where('semana','<=',$sem)->where('oficina_id',$ofi_id);

                    $vo0 += $vOfi->sum('inscripciones')?:0;
                }

                $vOfi1 = clone $ventasCurso;
                if($any<$any01)
                {
                    // $vOfi1 = $vOfi1->where('any', $any)->where('oficina_id',$ofi_id);
                    // $vOfi1 = $vOfi1->where('curso_any', $any01);
                    // $vo1 += $vOfi1->sum('inscripciones')?:0;
                }
                elseif($any==$any01)
                {
                    // $vOfi1 = $vOfi1->where('any', $any)->where('semana','<=',$sem)->where('oficina_id',$ofi_id);
                    // $vOfi1 = $vOfi1->where('curso_any', $any01);
                    $vOfi1 = $vOfi1->where('curso_any', $any01)->where('semana','<=',$sem)->where('oficina_id',$ofi_id);

                    $vo1 += $vOfi1->sum('inscripciones')?:0;
                }
            }

            if($vo0 || $vo1)
            {
                $chart3->addRow([$ofi, $vo1, $vo1, $vo0, $vo0]);
            }
        }

        Lava::BarChart('Chart-Bookings-Oficinas', $chart3, [
            'title' => 'Bookings por Oficina',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ],
            'colors'=> ['#d9abb2', '#aa70b3'],
            'height'=> 800,
        ]);

        //Evolución semanal (Bookings): 4
        $any = (int)$valores['any'];
        $any0 = $any-1;

        $chart4 = Lava::DataTable();
        $chart4->addStringColumn('Semana')
                ->addNumberColumn("$any0")
                ->addNumberColumn("$any");

        $Ventas = clone $ventas;
        $Ventas0 = clone $ventas;
        $vs = $Ventas->where('any',$any);
        $vs0 = $Ventas0->where('any',$any0);

        for ($i=1; $i <= 52 ; $i++)
        {
            $vss = clone $vs;
            $vss0 = clone $vs0;

            $vss = $vss->where('semana',$i);
            $vss0 = $vss0->where('semana',$i);

            $vs0b = $vss0->sum('inscripciones')?:0;
            $vs1b = $vss->sum('inscripciones')?:0;

            $chart4->addRow([$i, $vs0b, $vs1b]);
        }

        Lava::LineChart('Chart-Bookings-Evolucion', $chart4, [
            'title' => 'Evolucion Semanal Bookings',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ],
            'colors'=> ['#d9a202', '#1b9e77'],
            'height'=> 400,
        ]);

        //Evolución semanal (Solicitudes): 5
        $any = (int)$valores['any'];
        $any0 = $any-1;

        $chart5 = Lava::DataTable();
        $chart5->addStringColumn('Semana')
                ->addNumberColumn("$any0")
                ->addNumberColumn("$any");

        $semana1 = Carbon::create($any,1,1);
        $semana0 = Carbon::create($any0,1,1);

        for ($i=1; $i <= 52 ; $i++)
        {
            $desde1 = $semana1->startOfWeek()->toDateString();
            $hasta1 = $semana1->endOfWeek()->toDateString();
            $solicitudes1 = clone $solicitudes;
            $solicitudes1 = $solicitudes1->where('fecha','>=',$desde1)->where('fecha','<=',$hasta1)->count();

            $desde0 = $semana0->startOfWeek()->toDateString();
            $hasta0 = $semana0->endOfWeek()->toDateString();
            $solicitudes0 = clone $solicitudes;
            $solicitudes0 = $solicitudes0->where('fecha','>=',$desde0)->where('fecha','<=',$hasta0)->count();

            $chart5->addRow([$i, $solicitudes0, $solicitudes1]);

            $semana1->addWeeks(1);
            $semana0->addWeeks(1);
        }

        Lava::LineChart('Chart-Solicitudes-Evolucion', $chart5, [
            'title' => 'Evolucion Semanal Solicitudes',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ],
            'colors'=> ['#7570b3', '#d9abb2'],
            'height'=> 400,
        ]);

        //Bookings x Categoría (acumulado hasta semana seleccionada): 6
        //Semanas x Categoría (acumulado hasta semana seleccionada): 7
        //Solicitudes x Categoría (acumulado hasta semana seleccionada): 8
        $any00 = (int)$valores['any'];
        $any01 = $any-1;
        $any02 = $any-2;

        $chart6 = Lava::DataTable();
        $chart6->addStringColumn('Categoría')
                ->addNumberColumn("$any02")
                ->addRoleColumn('string', 'annotation')
                ->addNumberColumn("$any01")
                ->addRoleColumn('string', 'annotation')
                ->addNumberColumn("$any")
                ->addRoleColumn('string', 'annotation');

        $chart7 = Lava::DataTable();
        $chart7->addStringColumn('Categoría')
                ->addNumberColumn("$any02")
                ->addRoleColumn('string', 'annotation')
                ->addNumberColumn("$any01")
                ->addRoleColumn('string', 'annotation')
                ->addNumberColumn("$any")
                ->addRoleColumn('string', 'annotation');

        $chart8 = Lava::DataTable();
        $chart8->addStringColumn('Categoría')
                ->addNumberColumn("$any02")
                ->addRoleColumn('string', 'annotation')
                ->addNumberColumn("$any01")
                ->addRoleColumn('string', 'annotation')
                ->addNumberColumn("$any")
                ->addRoleColumn('string', 'annotation');

        $chart9 = Lava::DataTable();
        $chart9->addStringColumn('Mes')
                ->addNumberColumn("$any02")
                ->addRoleColumn('string', 'annotation')
                ->addRoleColumn('string', 'tooltip')
                ->addNumberColumn("$any01")
                ->addRoleColumn('string', 'annotation')
                ->addRoleColumn('string', 'tooltip')
                ->addNumberColumn("$any")
                ->addRoleColumn('string', 'annotation')
                ->addRoleColumn('string', 'tooltip');

        // $ventas_cat = clone $ventas;
        
        foreach( $categorias_graf as $categoria )
        {
            $cat = $categoria->name;

            // Si por ejemplo tenemos que el filtro es semana:2 y any:2017 tenemos q mirar las ventas con pago1 <= 2/2017 en todos los años y con fecha de curso en any 2017

            $ab = 0;
            $as = 0;
            $ab1 = 0;
            $as1 = 0;
            $ab2 = 0;
            $as2 = 0;

            foreach($anys as $any)
            {
                if($any>$any00)
                {
                    continue;
                }

                $V = clone $ventasCurso;
                if($any<$any00)
                {
                    // $a = $V->where('any', $any)->where('category_id', $categoria->id);
                    // $a = $a->where('curso_any', $any00);

                    // $ab += $a->sum('inscripciones')?:0;
                    // $as += $a->sum('semanas')?:0;
                }
                elseif($any==$any00)
                {
                    // $a = $V->where('any', $any)->where('semana','<=',$sem)->where('category_id', $categoria->id);
                    // $a = $a->where('curso_any', $any00);
                    $a = $V->where('curso_any', $any00)->where('curso_semana','<=',$sem)->where('category_id', $categoria->id); 

                    $ab += $a->sum('inscripciones')?:0;
                    $as += $a->sum('semanas')?:0;
                }

                //any anteriores:

                //any01
                $V1 = clone $ventasCurso;
                if($any<$any01)
                {
                    // $a1 = $V1->where('any', $any)->where('category_id', $categoria->id);
                    // $a1 = $a1->where('curso_any', $any01);

                    // $ab1 += $a1->sum('inscripciones')?:0;
                    // $as1 += $a1->sum('semanas')?:0;
                }
                elseif($any==$any01)
                {
                    // $a1 = $V1->where('any', $any)->where('semana','<=',$sem)->where('category_id', $categoria->id);
                    // $a1 = $a1->where('curso_any', $any01);
                    $a1 = $V1->where('curso_any', $any01)->where('curso_semana','<=',$sem)->where('category_id', $categoria->id);

                    $ab1 += $a1->sum('inscripciones')?:0;
                    $as1 += $a1->sum('semanas')?:0;
                }

                //any02
                $V2 = clone $ventasCurso;
                if($any<$any02)
                {
                    // $a2 = $V2->where('any', $any)->where('category_id', $categoria->id);
                    // $a2 = $a2->where('curso_any', $any02);

                    // $ab2 += $a2->sum('inscripciones')?:0;
                    // $as2 += $a2->sum('semanas')?:0;
                }
                elseif($any==$any02)
                {
                    // $a2 = $V2->where('any', $any)->where('semana','<=',$sem)->where('category_id', $categoria->id);
                    // $a2 = $a2->where('curso_any', $any02);
                    $a2 = $V2->where('curso_any', $any02)->where('curso_semana','<=',$sem)->where('category_id', $categoria->id);

                    $ab2 += $a2->sum('inscripciones')?:0;
                    $as2 += $a2->sum('semanas')?:0;
                }
            }

            if($ab || $ab1 || $ab2)
            {
                $chart6->addRow([$cat, $ab2, $ab2, $ab1, $ab1, $ab, $ab]);
            }

            if($as || $as1 || $as2)
            {
                $chart7->addRow([$cat, $as2, $as2, $as1, $as1, $as, $as]);
            }
        }

        //chart9: Bookings x Mes
        $meses = [1,2,3,4,5,6,7,8,9,10,11,12];
        foreach($meses as $m)
        {
            $ab = 0;
            $as = 0;
            $ab1 = 0;
            $as1 = 0;
            $ab2 = 0;
            $as2 = 0;

            $ab_lbl = "";
            $ab1_lbl = "";
            $ab2_lbl = "";

            $b0 = "";
            $b1 = "";
            $b2 = "";

            foreach($anys as $any)
            {
                if($any>$any00)
                {
                    continue;
                }

                $date = Carbon::create($any, $m, 1);
                $startOfMonth = $date->startOfMonth();
                $endOfMonth = $date->copy()->endOfMonth();

                $startWeek = $startOfMonth->weekOfYear;
                $endWeek = $endOfMonth->weekOfYear;

                $startMes = $startOfMonth->month;
                $endMes = $endOfMonth->month;

                if( $startMes==1 && $startWeek>=52)
                {
                    $startWeek = 1;
                }

                // $weeks = [];
                // for( $i = $startWeek; $i <= $endWeek; $i++ )
                // {
                //     $weeks[] = $i;
                // }

                $mes = $date->format('M');

                $V = clone $ventas;
                if($any==$any00)
                {
                    $a = $V->where('any', $any)->where('semana','>=',$startWeek)->where('semana','<',$endWeek);

                    foreach($a->get() as $bkng)
                    {
                        foreach( $bkng->bookings as $b)
                        {
                            $b0 .= "$b";
                            {
                                $b0 .= ",";
                            }
                        }
                    }
                    
                    $ab += $a->sum('inscripciones')?:0;
                    $as += $a->sum('semanas')?:0;
                }

                //any01
                $V1 = clone $ventas;
                if($any==$any01)
                {
                    $a1 = $V1->where('any', $any)->where('semana','>=',$startWeek)->where('semana','<',$endWeek);

                    foreach($a1->get() as $bkng)
                    {
                        foreach( $bkng->bookings as $b)
                        {
                            $b1 .= "$b";
                            {
                                $b1 .= ",";
                            }
                        }
                    }
                    
                    $ab1 += $a1->sum('inscripciones')?:0;
                    $as1 += $a1->sum('semanas')?:0;
                }

                //any02
                $V2 = clone $ventas;
                if($any==$any02)
                {
                    $a2 = $V2->where('any', $any)->where('semana','>=',$startWeek)->where('semana','<',$endWeek);

                    foreach($a2->get() as $bkng)
                    {
                        foreach( $bkng->bookings as $b)
                        {
                            $b2 .= "$b";
                            {
                                $b2 .= ",";
                            }
                        }
                    }
                    
                    $ab2 += $a2->sum('inscripciones')?:0;
                    $as2 += $a2->sum('semanas')?:0;
                }
            }

            if($ab || $ab1 || $ab2)
            {
                $ab_lbl = "$mes/$any00: $ab bookings [$b0]";
                $ab1_lbl = "$mes/$any01: $ab1 bookings [$b1]";
                $ab2_lbl = "$mes/$any02: $ab2 bookings [$b2]";

                $chart9->addRow([$mes, $ab2, $ab2, $ab2_lbl, $ab1, $ab1, $ab1_lbl, $ab, $ab, $ab_lbl,]);
            }
        }

        //solicitudes
        $any = (int)$valores['any'];
        $any01 = $any-1;
        $any02 = $any-2;

        $semana = Carbon::create($any,1,1);
        $semana1 = Carbon::create($any01,1,1);
        $semana2 = Carbon::create($any02,1,1);

        $desde = $semana->endOfWeek()->toDateString();
        $desde1 = $semana1->endOfWeek()->toDateString();
        $desde2 = $semana2->endOfWeek()->toDateString();

        $semana->addWeeks($sem-1);
        $semana1->addWeeks($sem-1);
        $semana2->addWeeks($sem-1);

        $hasta = $semana->endOfWeek()->toDateString();
        $hasta1 = $semana1->endOfWeek()->toDateString();
        $hasta2 = $semana2->endOfWeek()->toDateString();

        $solicitudes0 = clone $solicitudes;
        $solicitudes1 = clone $solicitudes;
        $solicitudes2 = clone $solicitudes;

        $solicitudes0 = $solicitudes0->where('fecha','>=',$desde)->where('fecha','<=',$hasta);
        $solicitudes1 = $solicitudes1->where('fecha','>=',$desde1)->where('fecha','<=',$hasta1);
        $solicitudes2 = $solicitudes2->where('fecha','>=',$desde2)->where('fecha','<=',$hasta2);

        foreach( $categorias_graf as $categoria )
        {
            $cat = $categoria->name;

            $solicitudes_cat = clone $solicitudes0;
            $solicitudes_cat1 = clone $solicitudes1;
            $solicitudes_cat2 = clone $solicitudes2;

            $as = $solicitudes_cat->where('category_id', $categoria->id)->count();
            $as1 = $solicitudes_cat1->where('category_id', $categoria->id)->count();
            $as2 = $solicitudes_cat2->where('category_id', $categoria->id)->count();

            if($as || $as1 || $as2)
            {
                $chart8->addRow([$cat, $as2, $as2, $as1, $as1, $as, $as]);
            }
        }

        Lava::ColumnChart('Chart-Bookings-Categoria', $chart6, [
            'title' => "Bookings x Categoría hasta Sem. $sem",
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ],
            'colors'=> ['#ff9900', '#3366cc', '#4f9d2f'],
            'height'=> 450,
        ]);

        Lava::ColumnChart('Chart-Semanas-Categoria', $chart7, [
            'title' => "Semanas x Categoría hasta Sem. $sem",
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ],
            'colors'=> ['#ff9900', '#3366cc', '#4f9d2f'],
            'height'=> 450,
        ]);

        Lava::ColumnChart('Chart-Solicitudes-Categoria', $chart8, [
            'title' => "Solicitudes x Categoría hasta Sem. $sem",
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ],
            'colors'=> ['#ff9900', '#3366cc', '#4f9d2f'],
            'height'=> 450,
        ]);

        Lava::ColumnChart('Chart-Bookings-Mes', $chart9, [
            'title' => "Bookings x Mes",
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ],
            'colors'=> ['#ff9900', '#3366cc', '#4f9d2f'],
            'height'=> 450,
            'tooltip' => [
                'isHtml' => true
            ]
        ]);


        // Admin: Ventas acumuladas (Bookings y Semanas) para cada oficina activa
        // Bookings y Semanas x Oficinas (acumulado hasta semana seleccionada
        $user = auth()->user();
        if($user->isFullAdmin())
        {
            $any00 = (int)$valores['any'];
            $any01 = $any-1;
            $any02 = $any-2;

            // $ventas_sem = clone $ventasCurso;
            // $ventas_sem = $ventas_sem->where('semana','<=',$sem);

            foreach($ofis as $o=>$ofi_id)
            {
                //Bookings
                $chart_admin1 = Lava::DataTable();
                $chart_admin1->addStringColumn('Categoría')
                        ->addNumberColumn("$any02")
                        ->addRoleColumn('string', 'annotation')
                        ->addNumberColumn("$any01")
                        ->addRoleColumn('string', 'annotation')
                        ->addNumberColumn("$any00")
                        ->addRoleColumn('string', 'annotation');

                //Semanas
                $chart_admin2 = Lava::DataTable();
                $chart_admin2->addStringColumn('Categoría')
                        ->addNumberColumn("$any02")
                        ->addRoleColumn('string', 'annotation')
                        ->addNumberColumn("$any01")
                        ->addRoleColumn('string', 'annotation')
                        ->addNumberColumn("$any00")
                        ->addRoleColumn('string', 'annotation');

                $ventas_ofi = clone $ventasCurso;
                $ventas_ofi = $ventas_ofi->where('oficina_id',$ofi_id);

                foreach( $categorias_graf as $categoria )
                {
                    $cat = $categoria->name;

                    $ab = 0;
                    $as = 0;
                    $ab1 = 0;
                    $as1 = 0;
                    $ab2 = 0;
                    $as2 = 0;

                    foreach($anys as $any)
                    {
                        if($any>$any00)
                        {
                            continue;
                        }

                        $V = clone $ventas_ofi;
                        if($any<$any00)
                        {
                            // $a = $V->where('any', $any)->where('category_id', $categoria->id);
                            // $a = $a->where('curso_any', $any00);

                            // $ab += $a->sum('inscripciones')?:0;
                            // $as += $a->sum('semanas')?:0;
                        }
                        elseif($any==$any00)
                        {
                            // $a = $V->where('any', $any)->where('semana','<=',$sem)->where('category_id', $categoria->id);
                            // $a = $a->where('curso_any', $any00);
                            $a = $V->where('curso_any', $any00)->where('curso_semana','<=',$sem)->where('category_id', $categoria->id); 

                            $ab += $a->sum('inscripciones')?:0;
                            $as += $a->sum('semanas')?:0;
                        }

                        //any anteriores:

                        //any01
                        $V1 = clone $ventas_ofi;
                        if($any<$any01)
                        {
                            // $a1 = $V1->where('any', $any)->where('category_id', $categoria->id);
                            // $a1 = $a1->where('curso_any', $any01);

                            // $ab1 += $a1->sum('inscripciones')?:0;
                            // $as1 += $a1->sum('semanas')?:0;
                        }
                        elseif($any==$any01)
                        {
                            // $a1 = $V1->where('any', $any)->where('semana','<=',$sem)->where('category_id', $categoria->id);
                            // $a1 = $a1->where('curso_any', $any01);
                            $a1 = $V1->where('curso_any', $any01)->where('curso_semana','<=',$sem)->where('category_id', $categoria->id); 

                            $ab1 += $a1->sum('inscripciones')?:0;
                            $as1 += $a1->sum('semanas')?:0;
                        }

                        //any02
                        $V2 = clone $ventas_ofi;
                        if($any<$any02)
                        {
                            // $a2 = $V2->where('any', $any)->where('category_id', $categoria->id);
                            // $a2 = $a2->where('curso_any', $any02);

                            // $ab2 += $a2->sum('inscripciones')?:0;
                            // $as2 += $a2->sum('semanas')?:0;
                        }
                        elseif($any==$any02)
                        {
                            // $a2 = $V2->where('any', $any)->where('semana','<=',$sem)->where('category_id', $categoria->id);
                            // $a2 = $a2->where('curso_any', $any02);
                            $a2 = $V2->where('curso_any', $any02)->where('curso_semana','<=',$sem)->where('category_id', $categoria->id); 

                            $ab2 += $a2->sum('inscripciones')?:0;
                            $as2 += $a2->sum('semanas')?:0;
                        }
                    }

                    if($ab || $ab1 || $ab2)
                    {
                        $chart_admin1->addRow([$cat, $ab2, $ab2, $ab1, $ab1, $ab, $ab]);
                    }

                    if($as || $as1 || $as2)
                    {
                        $chart_admin2->addRow([$cat, $as2, $as2, $as1, $as1, $as, $as]);
                    }
                }

                $ofiname = Oficina::find($ofi_id)->name;

                Lava::ColumnChart("Chart-Bookings-Categoria-Oficina-$ofi_id", $chart_admin1, [
                    'title' => "Bookings x Categoría hasta Sem. $sem - Oficina $ofiname",
                    'titleTextStyle' => [
                        'color'    => '#eb6b2c',
                        'fontSize' => 14
                    ],
                    'colors'=> ['#ff9900', '#3366cc', '#4f9d2f'],
                    'height'=> 450,
                ]);

                Lava::ColumnChart("Chart-Semanas-Categoria-Oficina-$ofi_id", $chart_admin2, [
                    'title' => "Semanas x Categoría hasta Sem. $sem - Oficina $ofiname",
                    'titleTextStyle' => [
                        'color'    => '#eb6b2c',
                        'fontSize' => 14
                    ],
                    'colors'=> ['#ff9900', '#3366cc', '#4f9d2f'],
                    'height'=> 450,
                ]);

            }
        }

    }
}