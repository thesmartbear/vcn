<?php

namespace VCN\Helpers;

use VCN\Models\CMS\LandingForm;
use WebforceHQ\ActiveCampaign\ActiveCampaign;
use WebforceHQ\ActiveCampaign\models\ActiveCampaignContact;

class ExternalApiHelper {

    const AC_URL = "https://britishsummer.api-us1.com";
    const AC_TOKEN = "40557424b31e7a4f0e8b6b27fcdd230b6d4d7df3eea44f17e16411774e82e4f275bdca56";

    public static function AC_test()
    {
        $client = new ActiveCampaign();
        $client->initialize(self::AC_URL, self::AC_TOKEN);

        $contact = new ActiveCampaignContact();
        $contact->setEmail("jhon_doe@gmail.com");
        $contact->setFirstName("Jhon");
        $contact->setLastName("Doe");
        $contact->setPhone("+529985656464");
        $contact->fieldValues = [
            [
                'field' => 2,
                'value' => "test2",
            ],
            [
                'field' => 3,
                'value' => "test3",
            ]
        ];

        // dd($contact);

        //Fetch contacts class and perform the create request
        $contacts = $client->contacts();
        
        try{
            //Perform create request sending the contact model
             $response = $contacts->create($contact);
            
            //If response success var_dump all content
            if($response->success){
            
                echo "Contact successfully created \n";
                var_dump($response->body->contact);
                
            }else{
                //If any error or message is present print it out
                if(isset($response->body->message)){
                    echo $response->body->message;
                }
                if(isset($response->body->errors)){
                    foreach($response->body->errors as $error){
                        echo $error->title."\n";
                        echo $error->detail."\n";
                        echo $error->code."\n";
                    }
                }
                
            }
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }

    public static function AC_contactsAdd( LandingForm $form = null )
    {
        $client = new ActiveCampaign();
        $client->initialize(self::AC_URL, self::AC_TOKEN);
        
        $landing = $form->landing ?? null;

        $data = [
            [
                'field' => 2,
                'value' => $form->tutor_nombre ?? "",
            ],
            [
                'field' => 3,
                'value' => $form->tutor_telefono ?? "",
            ],
            [
                'field' => 4,
                'value' => $form->es_viajero ?? "",
            ],
            [
                'field' => 5,
                'value' => $form->fechanac_dmy ?? "",
            ],
            [
                'field' => 6,
                'value' => $form->destino ?? "",
            ],
            [
                'field' => 7,
                'value' => $form->duracion ?? "",
            ],
            [
                'field' => 8,
                'value' => $landing->slug ?? "",
            ],
            [
                'field' => 9,
                'value' => $form->info_geo ?? "",
            ],
            [
                'field' => 10,
                'value' => $form->utm_source ?? "",
            ],
            [
                'field' => 11,
                'value' => $form->utm_medium ?? "",
            ],
            [
                'field' => 12,
                'value' => $form->utm_campaign ?? "",
            ],
        ];

        $contact = new ActiveCampaignContact();
        $contact->setEmail($form->email);
        $contact->firstName = $form->nombre ?? "firstName";
        // $contact->setFirstName($form->nombre ?? "test2");
        // $contact->setLastName("");
        if((int) $form->telefono)
        {
            $contact->setPhone($form->telefono ?? 123);
        }

        $contact->fieldValues = $data;

        $contacts = $client->contacts();
        $res = false;

        try{
            //Perform create request sending the contact model
             $response = $contacts->create($contact);
            
            //If response success var_dump all content
            if($response->success){
            
                // echo "Contact successfully created \n";
                // var_dump($response->body->contact);
                $res = true;
                
                
            }else{
                //If any error or message is present print it out
                if(isset($response->body->message)){
                    // echo $response->body->message;
                    $res = false;
                }
                if(isset($response->body->errors)){
                    foreach($response->body->errors as $error){
                        // echo $error->title."\n";
                        // echo $error->detail."\n";
                        // echo $error->code."\n";

                        $res = false;
                    }
                }
                
            }
        }catch(\Exception $e){
            // echo $e->getMessage();
        }
        
        return $res;
        
    }

}