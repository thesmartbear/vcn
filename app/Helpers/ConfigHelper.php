<?php

namespace VCN\Helpers;

use DB;
use Log;
use Auth;
use File;
use Mail;
use View;
use Image;

use Avatar;
use Carbon;
use Request;
use Session;
use Storage;
use VCN\Models\User;
use VCN\Models\Leads\Viajero;
use VCN\Models\Monedas\Moneda;
use VCN\Models\System\UserRole;
// use Activity;
use VCN\Models\Bookings\Booking;
use VCN\Models\System\SystemLog;

use VCN\Models\System\Plataforma;

use Illuminate\Support\Facades\Schema;
use Spatie\ImageOptimizer\OptimizerChain;
use Spatie\ImageOptimizer\Optimizers\Jpegoptim;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class ConfigHelper
{
    public static function version()
    {
        $config = env('APP_CONFIG', 'vcn');

        $laravel = app();
        $version = config("$config.version");

        $user = auth()->user()->username;

        return "v." . $version . " [" . $laravel::VERSION . "." . gethostname() . "(" . env('APP_ENV') . ")] - ( $user )";
    }

    public static function infoLogin()
    {
        $user = self::usuario();

        if (!$user) {
            return "xxx";
        }

        $fecha = $user->login_last ? $user->login_last->format('d/m/Y H:i') : "-";

        return "[ Último acceso: " . $fecha . "]";
    }

    public static function infoActivity()
    {
        return null;

        $ru = User::whereNotIn('roleid', [0, 11, 12, 13])->pluck('id')->toArray();
        $rv = User::where('roleid', 11)->pluck('id')->toArray();
        $rt = User::where('roleid', 12)->pluck('id')->toArray();
        $rm = User::where('roleid', 13)->pluck('id')->toArray();
        $rchat = User::where('roleid', 0)->pluck('id')->toArray();
        $activities = Activity::users();

        // $activities = Activity::users()->whereIn('sessions.user_id', $roles)->get();

        $u = clone $activities;
        $u = $u->whereIn('sessions.user_id', $ru)->count();

        $v = clone $activities;
        $v = $v->whereIn('sessions.user_id', $rv)->count();

        $t = clone $activities;
        $t = $t->whereIn('sessions.user_id', $rt)->count();

        $m = clone $activities;
        $m = $m->whereIn('sessions.user_id', $rm)->count();

        $w = clone $activities;
        $w = $w->whereIn('sessions.user_id', $rchat)->count();
        // $w += User::onlineGuests()->count();

        return "ONLINE: [U:$u | V:$v | T:$t | M:$m | W:$w]";
    }

    public static function versionLite()
    {
        $config = env('APP_CONFIG', 'vcn');

        $laravel = app();
        $version = config("$config.version");

        $user = auth()->user()->username;

        return "v." . $version . " - ( $user )";
    }

    public static function token()
    {
        $key = config('app.key');
        return $t = bcrypt($key);
    }

    public static function username()
    {
        $user = self::usuario() ? self::usuario()->username : "xxx";

        return $user;
    }

    public static function avatar()
    {
        $user = auth()->user();
        $avatar = $user ? $user->avatar : null;

        if (!$avatar) {
            if ($user && $user->ficha) {
                $ficha = $user->ficha;

                if ($ficha->foto) {
                    $avatar = $ficha->foto;

                    // if(!$user->avatar)
                    // {
                    //     $user->avatar = $avatar;
                    //     $user->save();
                    // }
                }
            }
        }

        // $data = file_get_contents($path);
        // $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        if (!$avatar) {
            $avatar = Avatar::create($user->avatar_name);

            // if($user->ficha)
            // {
            //     $dir = $user->foto_dir;
            //     $foto = $dir ."avatar_". $user->id .'.jpg';
            //     $avatar->save( $foto, 60);

            //     $user->ficha->foto = $foto;
            //     $user->ficha->save();
            // }

            $avatar = $avatar->toBase64();
            $user->avatar = $avatar;
            $user->save();
        }

        // if($avatar && $user->ficha && !$user->ficha->foto)
        // {
        //     $user->ficha->foto = $user->avatar;
        //     $user->ficha->save();
        // }

        return $avatar;
    }

    public static function iframePdf($href, $name)
    {
        $type = "application/pdf";

        $error = "<a href='$href' target='_blank'>" . $name . "</a>";

        $doc = url("/") . $href;
        if (!\App::environment('local')) {
            // $doc = "https://docs.google.com/gview?url=$href"."&embedded=true";
        }

        //$ret = "<iframe class='form-iframe' src='$doc' frameborder='0'></iframe>";
        $ret = self::iframeCanvas($href);

        return $ret;
    }

    public static function iframeCanvas($href)
    {
        $id = str_random();
        $ret = "<canvas class='form-iframe' id='iframe-$id'></canvas>
                <script>
                    pdfjsLib.getDocument('" . $href . "').promise.then( function(pdf) {
                    pdf.getPage(1).then(function(page) {
            
                        var scale = 1.5;
                        var viewport = page.getViewport({scale:scale});
            
                        var canvas = document.getElementById('iframe-$id');
                        var context = canvas.getContext('2d');
            
                        canvas.height = viewport.height;
                        canvas.width = viewport.width;
            
                        var renderContext = {
                            canvasContext: context,
                            viewport: viewport
                        };
                        page.render(renderContext);
            
                    });
                });
                </script>";

        return $ret;
    }

    public static function iframeFile($file, $path, $pubpath)
    {
        $fname = $file->getFilename();
        $ffile = $path . $fname;
        $ffile = preg_replace('/(\/+)/', '/', $ffile);
        $href = $pubpath . $fname;
        $href = url("/") . preg_replace('/(\/+)/', '/', $href);

        if (file_exists($ffile) && @getimagesize($ffile)) {
            return "<img src='$href' class='form-foto'>";
        }

        $type = File::mimeType($file);

        $doc = $href;
        // if( !\App::environment('local') )
        // {
        //     $doc = "https://docs.google.com/gview?url=$href"."&embedded=true";
        // }

        $error = "<a href='$href' target='_blank'>" . $fname . "</a>";

        $ret = "<iframe class='form-iframe' src='$doc' frameborder='0'></iframe>";

        return $ret;
    }

    public static function iframe($fname, $path, $pubpath, $download = false)
    {
        $ffile = $path . "/" . $fname;
        $ffile = preg_replace('/(\/+)/', '/', $ffile);
        $href = $pubpath . "/" . $fname;
        $href = url("/") . preg_replace('/(\/+)/', '/', $href);

        if (file_exists($ffile) && @getimagesize($ffile)) {
            $ret = "<a href='$href' target='_blank'><img src='$href' class='form-foto'></a>";

            $fname = $download ? trans('area.descargar') : $fname;
            $ret = $ret . "&nbsp;<a href='$href' target='_blank' " . ($download ? 'download' : '') . ">" . $fname . "</a>";

            return $ret;
        }

        $type = file_exists($ffile) ? mime_content_type($ffile) : null;

        if (!$type) {
            return "NO/FILE";
        }

        $ret = "";
        //$doc = $href;
        if (!\App::environment('local') && $type != "application/pdf") {
            $doc = "https://docs.google.com/gview?url=$href" . "&embedded=true";
            $ret = "<iframe class='form-iframe' src='$doc' frameborder='0'></iframe>";
        }

        //pdf
        if ($type == "application/pdf") {
            $ret = self::iframeCanvas($href);
        }

        $fname = $download ? trans('area.descargar') : $fname;
        $ret = $ret . "&nbsp;<br><a href='$href' target='_blank' " . ($download ? 'download' : '') . ">" . $fname . "</a>";

        return $ret;
    }

    public static function usuario()
    {
        $user = auth()->user();
        if (!$user) {
            return null;
        }

        if ($user->roleid > 1) {
            $bMonitor = $user->roleid == 13 && $user->plataforma == 0;
            if ($bMonitor) {
                return $user;
            }

            $plataforma = ConfigHelper::config('propietario');
            if ($user->roleid == 11 || $user->roleid == 12 || $user->roleid == 13) {
                if (!$user->plataforma) {
                    // $user->plataforma = $plataforma;
                    // $user->save();
                }
            }

            if ($plataforma) {
                $user = User::where('plataforma', $plataforma)->where('username', $user->username)->first();
            }
        }

        return $user;
    }

    public static function uploadFoto($file, $dirp, $resize = 1920, $base64 = false)
    {
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file_name = str_slug($file_name) . "." . $file->getClientOriginalExtension();
        $dir = public_path($dirp);

        $file->move($dir, $file_name);

        if ($resize) {
            $img = Image::make($dir . $file_name);
            $img->resize($resize, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            //thumb
            $file = $file_name;
            if (!file_exists($dir . "thumb/")) {
                File::makeDirectory($dir . "thumb/", 0775, true);
            }
            File::copy($dir . $file, $dir . "thumb/" . $file);
            $img2 = Image::make($dir . "thumb/" . $file);
            $img2->resize(450, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
        }

        $ret = "/" . $dirp . $file_name;

        if ($base64) {
            $ret = public_path($ret);
            $data = file_get_contents($ret);
            $type = pathinfo($ret, PATHINFO_EXTENSION);
            $ret = 'data:image/' . $type . ';base64,' . base64_encode($data);
        }

        return $ret;
    }


    public static function uploadFile($file, $dirp)
    {
        return self::uploadFoto($file, $dirp, false);
    }


    public static function config_plataforma($p = null)
    {
        if (!$p) {
            $p = ConfigHelper::config('propietario');
        }
        $plataforma = DB::table('plataformas')->where('id', $p)->first();

        return $plataforma;
    }

    public static function areaPermiso(User $user, Viajero $viajero)
    {
        if ($user->roleid == 11) //Viajero
        {
            if ($viajero->id != $user->viajero->id) {
                abort(404);
            }
        } elseif ($user->roleid == 12) //Tutor
        {
            $bTutor = false;
            $user_id = $user->id;

            //Comprobamos si es tutor del viajero
            foreach ($viajero->tutores as $tutor) {
                if ($user->tutor->user_id == $user_id) {
                    $bTutor = true;
                    break;
                }
            }

            if (!$bTutor) {
                abort(404);
            }
        }
    }

    public static function areaFormEstado($t = 'lista')
    {
        $estados = [
            ''  => '',
            0   => 'pendiente',
            1   => 'completo',
            2   => 'verificado',
            3   => 'offline',
        ];

        if ($t === 'lista') {
            return $estados;
        }

        return isset($estados[$t]) ? $estados[$t] : "-";
    }


    public static function commandPlataforma($plat)
    {
        $p = Session::get('vcn.commands.plataforma', 0);
        if ($p != $plat) {
            $h = Plataforma::find($plat);

            if ($h) {
                Session::put('vcn.commands.host', $h->web);
                Session::put('vcn.commands.plataforma', $plat);

                if ($h->email) {
                    Mail::alwaysFrom($h->email, $h->name);
                }
            }
        }
    }

    public static function config_file()
    {
        $config = env('APP_CONFIG', 'vcn');

        $host = "";
        // $tipoConfig = env('APP_MULTIWEB', false);
        // if($tipoConfig)
        {
            $host = ConfigHelper::getHost();
        }

        $file = $config . '.' . $host;
        return $file;
    }

    public static function checkArtisan($force)
    {
        if($force)
        {
            return false;
        }

        if (strpos(php_sapi_name(), 'cli') !== false) {
            return true;
        }

        return false;
    }

    public static function config($param, $plat = null)
    {
        $ret = Session::get('vcn.' . $param, null);
        if ($ret) {
            // return $ret;
        }

        if ($param == "tema" && $ret) {
            $ret = Session::get('vcn.tema2', $ret);

            $ruta = \Request::route()->getName();
            if ($ret == "bloques" && $ruta == "web.curso") {
                return "dippy";
            }

            return $ret;
        }

        $config = env('APP_CONFIG', 'vcn');

        $host = "";
        // $tipoConfig = env('APP_MULTIWEB', false);
        // if($tipoConfig)
        {
            $host = ConfigHelper::getHost();
        }

        $file = $config . '.' . $host;

        // $file = Session::get('vcn.config', 'vcn');

        $art = false;

        //BBDD
        if ($plat) {
            $p = $plat;
            $art = true;
        } else {
            $p = $plat ?: Session::get('vcn.commands.plataforma', config("$file.propietario"));
            if (!$p) {
                $p = 1;
            }
            else
            {
                $art = true;
            }
        }
        
        $plataforma = self::checkArtisan($art) ? null : Plataforma::find($p);
        
        if($plataforma)
        {
            if (strpos($param, 'booking_status_') === 0) {
                $ps = substr($param, 15);
                $ret = (int) $plataforma->status_booking[$ps];

                // Session::put('vcn.'.$param, $ret);
                return $ret;
            } elseif (strpos($param, 'solicitud_status_') === 0) {
                $ps = substr($param, 17);
                $ret = (int) $plataforma->status_solicitud[$ps];

                // Session::put('vcn.'.$param, $ret);
                return $ret;
            } else {
                $columns = DB::getSchemaBuilder()->getColumnListing('plataformas');
                if (in_array($param, $columns)) {
                    $ret = $plataforma->$param;

                    Session::put('vcn.' . $param, $ret);
                    return $ret;
                }
            }
        }

        return config("$file.$param", config("$config.$param", null));
    }

    public static function view($view)
    {
        $tema = Session::get('vcn.tema');

        $path = "web." . ($tema ? $tema . "." : "") . $view;

        return $path;
    }

    public static function getHost()
    {
        // $host = $_SERVER['HTTP_HOST'];
        // $h = explode("www.", $host);
        // $h = explode(".", $h[count($h)-1]);
        // $host = $h[0];

        $host = Request::server('HTTP_HOST');

        // if($host=='127.0.0.1')
        {
            $host = Session::get('vcn.commands.host') ?: $host;
        }

        $host = str_replace(".", "-", $host);

        return $host;
    }

    public static function moneda()
    {
        $m = self::config('moneda');
        return $m;
    }

    public static function default_moneda_id()
    {
        $m = self::config('moneda');
        return Moneda::where('currency_name', $m)->first()->id;
    }

    public static function default_moneda()
    {
        $m = self::config('moneda');
        return Moneda::where('currency_name', $m)->first();
    }

    public static function default_moneda_name()
    {
        return self::config('moneda');
    }

    public static function default_plataforma()
    {
        $config = env('APP_CONFIG', 'vcn');
        $host = self::getHost();
        $file = $config . '.' . $host;

        $p = config("$file.propietario", 1);
        return Plataforma::find($p);
    }

    public static function default_oficina()
    {
        $config = env('APP_CONFIG', 'vcn');
        $host = self::getHost();
        $file = $config . '.' . $host;

        $p = config("$file.propietario", 1);
        return Plataforma::find($p)->oficina;
    }

    public static function default_catalogos()
    {
        $config = env('APP_CONFIG', 'vcn');
        $host = self::getHost();
        $file = $config . '.' . $host;

        $p = config("$file.propietario", 1);
        return Plataforma::find($p)->catalogos;
    }

    public static function catalogo_link($href, $info, $lang, $modal = null)
    {
        $ga = "gtag('event', 'catalogo', {
                'event_category': 'descarga',
                'event_label': '$lang'
            });";

        if (!$modal) {
            return "<a href='$href' target='_blank' onclick=\"$ga\">$lang</a>";
        }

        return "<a href='#' v-on:click='clickInfo' data-href='$href' data-info='$info' data-lang='$lang'>$lang</a>";
    }


    public static function idiomas()
    {
        $idiomas = Session::get('vcn.idiomas', null);

        if ($idiomas && is_array($idiomas)) {
            return $idiomas;
        }

        $config = env('APP_CONFIG', 'vcn');
        $host = "";
        // $tipoConfig = env('APP_MULTIWEB', false);
        // if($tipoConfig)
        {
            $host = self::getHost();
        }

        $file = $config . '.' . $host;

        $p = config("$file.propietario", 1);
        $plataforma = Plataforma::find($p);
        $ret = $plataforma ? explode(',', $plataforma->idiomas) : ['es'];
        $ret = array_diff($ret, ['']);

        Session::put('vcn.idiomas', $ret);

        return $ret;
    }

    public static function idioma()
    {
        return self::config('idioma');
    }

    public static function idiomaCRM()
    {
        return 'es';
    }

    public static function getCreatedBy($model)
    {
        $m = class_basename(get_class($model));

        $log = SystemLog::where('modelo_id', $model->id)->where('modelo', $m)->where('tipo', 'Nuevo')->orderBy('created_at', 'desc')->first();

        if ($log) {
            return $log->user ? $log->user->full_name : "-";
        }

        return "-";
    }

    public static function getUpdatedBy($model)
    {
        $m = class_basename(get_class($model));

        $log = SystemLog::where('modelo_id', $model->id)->where('modelo', $m)->where('tipo', 'Modificado')->orderBy('created_at', 'desc')->first();

        if ($log) {
            return $log->user ? $log->user->full_name : $log->user_id;
        }

        return "-";
    }

    public static function parseMoneda($valor, $moneda = null)
    {
        $formato = Session::get('vcn.moneda_format', null);
        $miles = Session::get('vcn.moneda_miles', null);
        $decimales = Session::get('vcn.moneda_decimales', null);

        if (!$formato) {
            $formato = self::config('moneda_format');
            $miles = self::config('moneda_miles');
            $decimales = self::config('moneda_decimales');

            Session::put('vcn.moneda_format', $formato);
            Session::put('vcn.moneda_miles', $miles);
            Session::put('vcn.moneda_decimales', $decimales);
        }

        if (!$moneda) {
            $moneda = Session::get('vcn.moneda', null);
            if (!$moneda) {
                $moneda = self::config('moneda');
                Session::put('vcn.moneda', $moneda);
            }
        }

        if (is_numeric($valor)) {
            // $valor = money_format('%!n', $valor);
            $valor = number_format($valor, 2, $decimales, $miles);
        }
        $valor = sprintf($formato, $valor, $moneda);

        return $valor;
    }

    public static function monedaCambio($moneda_id, $booking_id = 0)
    {
        if ($booking_id) {
            //miramos si tiene el booking monedas
            $booking = Booking::find($booking_id);
            if ($booking) {
                if ($booking->monedas->count() > 0) {
                    $m = $booking->monedas->where('moneda_id', $moneda_id)->first();
                    if ($m) {
                        return $m->rate;
                    }
                }

                $tc = $booking->tabla_cambio;
                if ($tc) {
                    $m = $tc->cambios->where('moneda_id', $moneda_id)->first();
                    if ($m) {
                        return ((float) $m->tc_final) ? $m->tc_final : $m->tc_folleto;
                    }
                }
            }
        }


        $m = Moneda::find($moneda_id);
        return $m ? $m->currency_rate : 1;
    }

    public static function monedaCambioCurso($moneda_id, $curso_id = 0)
    {
        if ($curso_id) {
            //miramos si tiene el booking monedas
            $curso = \VCN\Models\Cursos\Curso::find($curso_id);
            if ($curso) {
                $tc = $curso->categoria ? $curso->categoria->tabla_cambio : null;
                if ($tc) {
                    $m = $tc->cambios->where('moneda_id', $moneda_id)->first();
                    if ($m) {
                        return $m->tc_folleto;
                    }
                }
            }
        }

        $m = Moneda::find($moneda_id);
        return $m ? $m->currency_rate : 0;
    }

    public static function monedaCambioCursoWeb($moneda_id, $curso_id = 0)
    {
        if ($curso_id) {
            $tc = null;

            //miramos si tiene el booking monedas
            $curso = \VCN\Models\Cursos\Curso::find($curso_id);
            if ($curso) {
                if ($curso->es_convocatoria_cerrada) {
                    $ccc = $curso->convocatoriasCerradas->where('activo_web', 1)->first();
                    if ($ccc) {
                        $tc = $ccc->tabla_cambio;
                        if (!$tc) {
                            $any = Carbon::parse($ccc->convocatory_close_start_date)->year;
                            $tc = $curso->categoria ? $curso->categoria->getTablaCambioAny($any, $ccc->moneda) : null;
                        }
                    }
                }

                if (!$tc) {
                    $tc = $curso->categoria ? $curso->categoria->tabla_cambio : null;
                }

                if ($tc) {
                    $m = $tc->cambios->where('moneda_id', $moneda_id)->first();
                    if ($m) {
                        return $m->tc_folleto;
                    }
                }
            }
        }

        $m = Moneda::find($moneda_id);
        return $m ? $m->currency_rate : 0;
    }

    public static function monedaImporte($importe, $moneda_id)
    {
        $m = Moneda::find($moneda_id);
        $moneda = Moneda::where('currency_name', Session::get('vcn.moneda'))->first();

        if ($m->id == $moneda->id) {
            return $importe;
        } else {
            return ($importe * $m->currency_rate);
        }
    }

    public static function getTipoAdjunto($t = null)
    {
        $arr = ['pasaporte', 'vacunas', 'contrato'];

        if (!is_numeric($t))
            return $arr;

        return trans('area.adjuntos.' . $arr[$t]);
    }

    public static function getTipoCampo($t = null)
    {
        $arr = ['Check', 'Valor', 'Check + Valor'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getTipoUnidad($t = null)
    {
        $arr = ['One time fee', 'Por unidad', 'Porcentaje [Genérico]'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getTipoOnlineReserva($t = null)
    {
        $arr = ['Importe', 'Porcentaje'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getIdiomaContacto($t = null)
    {
        $arr = [
            '' => '',
            'ES' => 'Castellano',
            'CA' => 'Català',
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[strtoupper($t)];
    }

    public static function getIdiomaContactoArea($t = null)
    {
        $arr = [
            '-' => 'Comunes',
            'ES' => 'Castellano',
            'CA' => 'Català',
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getConvocatoriaTipo($t = null)
    {
        $arr = ['Todas', 'Cerrada', 'Semi', 'Abierta', 'Multi'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getConvocatoriaIndex($t = null)
    {
        $arr = ['', 'convocatory_close_id', 'convocatory_close_id', 'convocatory_open_id', 'convocatory_multi_id', 'convocatory_close_id'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getEscuelaCurso($t = null)
    {
        if (config('app.timezone') == "America/Mexico_City") 
        {
            return self::getEscuelaCursoSF($t);
        }
        
        $arr = [ 0=> '', '1ºEP', '2ºEP', '3ºEP', '4ºEP', '5ºEP', '6ºEP', '1ºESO', '2ºESO', '3ºESO', '4ºESO', '1ºBach', '2ºBach'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getEscuelaCursoSF($t = null)
    {
        $arr = [ 0=> "",
            '1º Secundaria',
            '2º Secundaria',
            '3º Secundaria',
            '1º Prepa',
            '2º Prepa',
            '3º Prepa'
        ];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getGradoExt($t = null)
    {
        if (config('app.timezone') == "America/Mexico_City") 
        {
            return self::getGradoExtSF($t);
        }

        $arr = [ 0=> '', 
            '2ºBach',
            '1ºBach',
            '4ºESO',
            '3ºESO',
            '2ºESO',
            '1ºESO',
            '6ºEP',
            '5ºEP',
            '4ºEP',
            '3ºEP',
            '2ºEP',
            '1ºEP',
        ];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getGradoExtSF($t = null)
    {
        $arr = [ 0=> "",
            '3º Prepa',
            '2º Prepa',
            '1º Prepa',
            '3º Secundaria',
            '2º Secundaria',
            '1º Secundaria'
        ];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getTestMark($t = null)
    {
        $arr = ['', '001', '002', '101', '102', '201', '202', '301', '302', '401'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getCMSTipo($t = null)
    {
        $arr = ['Página', 'Enlace'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getCICNivel($t = null)
    {
        $arr = [
            '',
            'Francès' => 'Francès',
            'Alemany' => 'Alemany',
            'P5' => 'P5',
            'J1 (1er EP)' => 'J1 (1er EP)',
            'J2 (2on EP)' => 'J2 (2on EP)',
            'J3 (3er EP)' => 'J3 (3er EP)',
            'J4 (4t EP)' => 'J4 (4t EP)',
            'JH (001/002)' => 'JH (001/002)',
            'Science 1 (1er-2on EP)' => 'Science 1 (1er-2on EP)',
            'Science 2 (3er-4rt EP)' => 'Science 2 (3er-4rt EP)',
            '101-102 Elementary (A1)' => '101-102 Elementary (A1)',
            '201-202 Pre-Intermediate (A2)' => '201-202 Pre-Intermediate (A2)',
            '301-302 Intermediate (B1)' => '301-302 Intermediate (B1)',
            '401-512 Upper Intermediate (B2)' => '401-512 Upper Intermediate (B2)',
            '600-802 Advanced (C1)' => '600-802 Advanced (C1)',
            '901-916 Proficiency (C2)' => '901-916 Proficiency (C2)',

            /* 'No se' => 'No se',
            'Francès' => 'Francès',
            'Alemany' => 'Alemany',
            'P5' => 'P5',
            'J1 (1er EP)' => 'J1 (1er EP)',
            'J2 (2on EP)' => 'J2 (2on EP)',
            'J3 (3er EP)' => 'J3 (3er EP)',
            'J4 (4t EP)' => 'J4 (4t EP)',
            'JH (001/002)' => 'JH (001/002)',
            'Science 1 (1-2 EP)' => 'Science 1 (1-2 EP)',
            'Science 2 (3-4 EP)' => 'Science 2 (3-4 EP)',
            'Elem101 (A1)' => 'Elem101 (A1)',
            'Elem 102 (A1)' => 'Elem 102 (A1)',
            '201'   => '201',
            'Pre-Int 202 (A2)' => 'Pre-Int 202 (A2)',
            'Int 301 (B1)' => 'Int 301 (B1)',
            'Int 301 (B1)' => 'Int 301 (B1)',
            'Int 302 (B1)' => 'Int 302 (B1)',
            'Up Int 401 (B2.1)' => 'Up Int 401 (B2.1)',
            'Up Int 402 (B2.1)' => 'Up Int 402 (B2.1)',
            'Up Int 403 (B2.1)' => 'Up Int 403 (B2.1)',
            'Up Int 406 (B2.2)' => 'Up Int 406 (B2.2)',
            'Up Int 416-FCE Juny (B2.2)' => 'Up Int 416-FCE Juny (B2.2)',
            'Up Int 412-FCE Dec (B2.2)' => 'Up Int 412-FCE Dec (B2.2)',
            '500 (starts in Jan)(C1.1)' => '500 (starts in Jan)(C1.1)',
            'Adv1 501-Q1 (C1.1)' => 'Adv1 501-Q1 (C1.1)',
            'Adv1 502-Q2 (C1.1)' => 'Adv1 502-Q2 (C1.1)',
            'Adv2 601 (C1.2)' => 'Adv2 601 (C1.2)',
            'Adv2 606 (C1.2)' => 'Adv2 606 (C1.2)',
            '612-CAE Des (C1.2)' => '612-CAE Des (C1.2)',
            '614- CAE Abril (C1.2)' => '614- CAE Abril (C1.2)',
            '616-CAE Juny (C1.2)' => '616-CAE Juny (C1.2)',
            'Pre-Prof 700 (C1.1)' => 'Pre-Prof 700 (C1.1)',
            'Prof 701 (C2.1)' => 'Prof 701 (C2.1)',
            'Prof 716-CPE Juny (C2.2)' => 'Prof 716-CPE Juny (C2.2)',
            'Prof 706 (C2.2)' => 'Prof 706 (C2.2)',
            'Prof 712 (C2.2)' => 'Prof 712 (C2.2)', */
        ];

        if (!$t)
            return $arr;

        return $arr[$t];
    }

    public static function getTrinityNivel($t = null)
    {
        $arr = [];
        $arr[0] = "";
        for ($i = 1; $i <= 12; $i++) {
            $arr[$i] = "Grade$i";
        }

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getCicThau($t = null)
    {
        $arr = ['No', 'Thau Barcelona', 'Thau Sant Cugat'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getTipoVia($t = null)
    {
        $arr = ['Calle', 'Avenida', 'Vía', 'Paseo', 'Pasaje', 'Plaza', 'Ronda', 'Carretera'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getTipoTransporte($t = null)
    {
        $arr = [
            0 => 'Vuelo',
            1 => 'Autobús',
            2 => 'Barco',
            3 => 'Tren'
        ];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getTipoTransporteIcono($t = 0)
    {
        $arr = [
            'fa-plane', 'fa-bus', 'fa-ship', 'fa-train',
            'Vuelo' => 'fa-plane',
            'Autobús' => 'fa-bus',
            'Barco' => 'fa-ship',
            'Tren' => 'fa-train',
        ];

        $icon = isset($arr[$t]) ? $arr[$t] : $arr[0];
        return "<i class='fa $icon fa-2x'></i>";
    }

    public static function getBookingFase($t = null)
    {
        $arr = ['Pendiente', 'Datos Curso', 'Datos Viajero', 'Resumen', 'Completa'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getArchivarMotivo($auto = false)
    {
        $arr = [
            '' => 'Seleccionar motivo',
            'Decidió no estudiar' => 'Decidió no estudiar',
            'Visa denegada' => 'Visa denegada',
            'No respondió' => 'No respondió',
            'Competencia' => 'Competencia',
            'Budget insuficiente' => 'Budget insuficiente',
            'otro' => 'Otro'
        ];

        if ($auto) {
            $arr['AUTO'] = "AUTO";
        }

        return $arr;
    }

    public static function getIdiomaNivel()
    {
        $arr = [
            '' => 'Seleccionar nivel',
            'Principiante' => 'Principiante',
            'Pre-intermedio' => 'Pre-intermedio',
            'Intermedio' => 'Intermedio',
            'Intermedio Alto' => 'Intermedio Alto',
            'Avanzado' => 'Avanzado'
        ];

        return $arr;
    }

    public static function getIdioma()
    {
        $arr = [
            'Inglés' => 'Inglés',
            'Español' => 'Español',
            'Francés' => 'Francés',
            'Alemán' => 'Alemán',
            'Chino' => 'Chino',
            'Italiano' => 'Italiano',
            'Japonés' => 'Japonés',
            'Portugués' => 'Portugués',
        ];

        return $arr;
    }

    public static function getPrecioDuracion($t = null)
    {
        $arr = ['Seleccione Duración...', 'Monto fijo', 'Por Semana', 'Por Mes', 'Por Trimestre', 'Por Semestre', 'Por Año'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getPrecioDuracionName($t = null)
    {
        $arr = ['Seleccione Duración...', 'Monto fijo', 'Semanas', 'Meses', 'Trimestres', 'Semestres', 'Años'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getPrecioDuracionUnit($t = null)
    {
        $arr = ['Días', 'Semanas', 'Meses', 'Trimestres', 'Semestres', 'Años'];

        if (!is_numeric($t))
            return $arr;

        return isset($arr[$t]) ? $arr[$t] : 0;
    }

    public static function getPrecioDuracionUnitCerrada($t = null)
    {
        $arr = ['Días', 'Semanas', 'Meses', 'Trimestres', 'Semestres', 'Años'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getPrecioDuracionTipo($t = null)
    {
        $arr = ['Cualquiera', 'A partir de', 'Rango'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getPrecioRangos($t = null)
    {
        $arr = [];
        for ($i = 0; $i <= 100; $i++) {
            $arr[$i] = "$i";
        }

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getSemanas($t = "Semanas")
    {
        $arr = [];
        $arr[0] = "-- Semanas";
        for ($i = 1; $i <= 52; $i++) {
            $arr[$i] = "$i $t";
        }

        return $arr;
    }

    public static function getAnys($t = null)
    {
        $arr = \VCN\Models\Informes\Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $end = end($arr);

        $e = $end + 1;
        $arr[$e] = $e;

        $e = $end + 2;
        $arr[$e] = $e;

        $e = $end + 3;
        $arr[$e] = $e;

        asort($arr);

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getTipoPago($t = null)
    {
        $arr = ['', 'Transferencia', 'Tarjeta de crédito', 'Ingreso en cuenta', 'Cheque'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getTutorRelacion($t = null)
    {
        $arr = ['Padre', 'Madre', 'Tutor', 'Otro'];

        // asort($arr);

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getTutorRelacionEng($t = null)
    {
        $arr = ['Father', 'Mother', 'Tutor', 'Other'];

        // asort($arr);

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getTipoTarea($t = null)
    {
        $arr = [
            'Email'         => 'Email',
            'Teléfono'      => 'Teléfono',
            'Visita'        => 'Visita',
            'BackOffice'    => 'BackOffice',
            'Feedback Cliente'  => 'Feedback Cliente',
            'WhatsApp'      => 'WhatsApp',
            'Chat'          => 'Chat',
            'Feedback'      => 'Feedback',
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getTipoSeguimiento($t = null)
    {
        $arr = [
            // ''=> '...',
            'Email 1r contacto'     => 'Enviado: Email 1r contacto',
            'Precios personalizados'  => 'Enviado: Precios personalizados',
            'Email seguimiento'     => 'Enviado: Email seguimiento',
            'Llamada contestada' => 'Enviado: Llamada contestada',
            'Llamada no contestada' => 'Enviado: Llamada no contestada',
            'BackOffice' => 'Enviado: BackOffice',
            'Chats'      => 'Enviado: Chat',

            'Email recibido'     => 'Recibido: Email recibido',
            'Llamada'  => 'Recibido: Llamada',
            'Visita'    => 'Recibido: Visita',
            'WhatsApp' => 'Recibido: WhatsApp',
            'Chat'      => 'Recibido: Chat',
            'Feedback'      => 'Recibido: Feedback',
            'Videollamada'      => 'Recibido: Videollamada',
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getTipoSeguimientoEnviado($t = null)
    {
        $arr = [
            'Email 1r contacto'     => 'Email 1r contacto',
            'Precios personalizados'  => 'Precios personalizados',
            'Email seguimiento'     => 'Email seguimiento',
            'Llamada contestada' => 'Llamada contestada',
            'Llamada no contestada' => 'Llamada no contestada',
            'BackOffice' => 'BackOffice',
            'Chats'      => 'Chats',
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getTipoSeguimientoRecibido($t = null)
    {
        $arr = [
            'Email recibido'     => 'Email recibido',
            'Llamada'  => 'Llamada',
            'Visita'    => 'Visita',
            'WhatsApp' => 'WhatsApp',
            'Chat'      => 'Chat',
            'Feedback'      => 'Feedback',
            'Videollamada'      => 'Videollamada',
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }


    public static function getTipoIncidencia($t = null)
    {
        $arr = [
            'Enfermedad'    => 'Enfermedad',
            'Accidente'     => 'Accidente',
            'Familia'       => 'Familia',
            'Comportamiento' => 'Comportamiento',
            'Viaje'         => 'Viaje',
            'Curso'         => 'Curso',
            'Otro'          => 'Otro'
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getDia($t = null)
    {
        $arr = ['', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getDiaNumero($t = null)
    {
        $arr = [];

        for ($i = 1; $i <= 31; $i++) {
            $arr[] = $i;
        }

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function menuInformePermiso($route)
    {

        $informes_menu = config('vcn.menu.manager')[6]['submenu'];

        foreach ($informes_menu as $bloque) {
            if (!isset($bloque['submenu'])) {
                continue;
            }

            foreach ($bloque['submenu'] as $detalle) {
                if ($detalle['route'] == $route) {
                    // return $detalle['permiso'];

                    $pos = strpos($route, 'manage.informes');
                    if ($pos !== false) {
                        $b = $bloque["nom"];
                        $informe = substr($route, $pos + 16);
                        return "$b.$informe";
                    }
                }
            }
        }

        return null;
    }

    public static function menuRol($menu, $rol = 0)
    {
        $m2ret = [];
        $aux1 = [];
        $aux2 = [];
        $aux3 = [];

        if (!$menu) {
            return $m2ret = [];
        }

        $bMenu = false;
        $bMenuInformes = false;
        foreach ($menu as $m1) {
            $bMenuInformes = false;

            $rolMenu = isset($m1['rol']) ? $m1['rol'] : [0];
            $nomMenu = isset($m1['nom']) ? $m1['nom'] : "";

            //informes
            if ($nomMenu == 'Informes') {
                $bMenuInformes = true;

                $ruta = $m1['route'];
                if (!UserRole::permisoInforme($ruta)) {
                    continue;
                }
            }

            if ($bMenuInformes || (in_array($rol, $rolMenu) or in_array(0, $rolMenu)) || (UserRole::permisoView(isset($m1['permiso']) ? $m1['permiso'] : ""))) {
                $aux1 = [];
                $aux2 = [];
                $aux3 = [];

                $bMenu = false;
                if (isset($m1['submenu'])) {
                    foreach ($m1['submenu'] as $m2) {
                        $aux3 = [];

                        //informes
                        $nomListado = "";
                        $bMenuListado = false;
                        if ($nomMenu == 'Informes') {
                            $bMenuInformes = true;
                            $ruta = $m2['route'];
                            if (!UserRole::permisoInforme($ruta)) {
                                continue;
                            }

                            $nomListado = isset($m2['nom']) ? $m2['nom'] : "";
                        }

                        if ($bMenuInformes || (in_array($rol, $m2['rol']) or in_array(0, $m2['rol']) || (UserRole::permisoView(isset($m2['permiso']) ? $m2['permiso'] : "")))) {
                            if (isset($m2['submenu'])) {
                                foreach ($m2['submenu'] as $m3) {
                                    //informes
                                    if ($nomMenu == 'Informes') {
                                        $bMenuInformes = true;
                                        $ruta = $m3['route'];
                                        if (!UserRole::permisoInforme($ruta)) {
                                            continue;
                                        }

                                        if ($nomListado == 'Listados') {
                                            $bMenuListado = true;
                                            $ruta = $m3['route'];
                                            $p = isset($m3['permiso']) ? $m3['permiso'] : "";
                                            if (!$p || !self::canView($p)) {
                                                continue;
                                            }
                                        }
                                    }

                                    if ($bMenuInformes || (in_array($rol, $m3['rol']) or in_array(0, $m3['rol']) || (UserRole::permisoView(isset($m3['permiso']) ? $m3['permiso'] : "")))) {
                                        if (is_array($m3['route'])) {
                                            $m3["route"] = route($m3["route"][0], $m3["route"][1]);
                                        } else {
                                            if (($m3["route"][0] != '-')) {
                                                $m3["route"] = ($m3["route"][0] == '#') ? $m3["route"] : route($m3["route"]);
                                            }
                                        }

                                        $bMenu = true;
                                        array_push($aux3, $m3);
                                    }
                                }

                                $m2["submenu"] = $aux3;
                            }

                            if (is_array($m2['route'])) {
                                $m2["route"] = route($m2["route"][0], $m2["route"][1]);
                            } else {
                                $m2["route"] = ($m2["route"][0] == '#') ? $m2["route"] : route($m2["route"]);
                            }

                            //Informes
                            if ($nomMenu == 'Informes') {
                                if (!count($aux3)) {
                                    continue;
                                }

                                $bMenu = true;
                                array_push($aux2, $m2);
                            } else {
                                $bMenu = true;
                                array_push($aux2, $m2);
                            }
                        }
                    }
                }

                $aux1["nom"] = isset($m1["nom_trans"]) ? trans($m1["nom_trans"]) : $m1["nom"];
                $aux1["base"] = isset($m1["base"]) ? $m1["base"] : "-";

                if ($rol) //manager
                {
                    if (is_array($m1['route'])) {
                        $aux1["route"] = route($m1["route"][0], $m1["route"][1]);
                    } else {
                        $aux1["route"] = ($m1["route"][0] == '#') ? $m1["route"] : route($m1["route"]);
                    }
                } else //web
                {
                    if (isset($m1["url"])) {
                        $aux1["route"] = null;
                        // $aux1["url"] = trans($m1["url"]);
                        $aux1["url"] = $m1["url"];
                    } else {
                        $aux1["route"] = $m1["route"];
                    }
                }

                // $aux1["rol"] = $m1["rol"];
                $aux1["icono"] = isset($m1["icono"]) ? $m1["icono"] : "-";
                $aux1["color"] = isset($m1["color"]) ? $m1["color"] : "-";
                $aux1["view"] = isset($m1["view"]) ? $m1["view"] : "-";

                $aux1["submenu"] = $aux2;

                // if($bMenu)
                {
                    array_push($m2ret, $aux1);
                }
            }
        }

        //limpiamos los menús submenus vacios
        foreach ($m2ret as $im1 => $m1){
            
            if ($m1['route'] && $m1['route'][0] != "#")
            {
                continue;
            }

            if (!count($m1['submenu'])) {
                unset($m2ret[$im1]);
            } else {
                $iSub = 0;
                foreach ($m1['submenu'] as $im2 => $m2) {
                    if (isset($m2['submenu'])) {
                        $iSub += count($m2['submenu']);
                        if (!count($m2['submenu'])) {
                            unset($m2ret[$im1]['submenu'][$im2]);
                        }
                    } else {
                        $iSub++;
                    }
                }

                if (!$iSub) {
                    unset($m2ret[$im1]);
                }
            }
        }


        // dd($m2ret);

        return $m2ret;
    }

    public static function menuManager()
    {
        $config = env('APP_CONFIG', 'vcn');

        $rol = Auth::user() ? Auth::user()->roleid : 0;

        return self::menuRol(config('vcn.menu.manager'), $rol);
    }

    public static function menuArea()
    {
        $config = env('APP_CONFIG', 'vcn');

        $rol = Auth::user() ? Auth::user()->roleid : 0;

        return self::menuRol(config('vcn.menu.area'), $rol);
    }

    public static function menuWeb()
    {
        $config = env('APP_CONFIG', 'vcn');

        $m = self::menuRol(self::config('menu.web'));

        if (!count($m)) {
            $m = self::menuRol(config("$config.menu.web"));
        }
        return $m;
    }


    public static function canEdit($permiso)
    {
        return UserRole::permisoEdit($permiso);
    }

    public static function canView($permiso)
    {
        return UserRole::permisoView($permiso);
    }

    public static function plataforma($p)
    {
        if (!$p) {
            return 'Todas';
        }

        $plat = Plataforma::find($p);

        return $plat ? $plat->name : "-";


        $config = env('APP_CONFIG', 'vcn');

        $plataformas = config("$config.plataformas");
        // $plataformas = self::config("plataformas");

        return $plataformas[$p];
    }

    public static function propietario()
    {
        return ConfigHelper::config('propietario');
    }

    public static function plataformaApp()
    {
        $config = env('APP_CONFIG', 'vcn');

        $p = ConfigHelper::config('propietario');

        $plat = Session::get('vcn.plataforma_name');
        if ($plat) {
            return $plat;
        }

        //$plataformas = self::config("plataformas");
        //return $plataformas[$p];

        $p = Plataforma::find($p ?: 1);
        $plat = $p->name;

        Session::put('vcn.plataforma_name', $plat);

        return  $plat;
    }

    public static function plataformas($todas = true)
    {
        // $plataformas = self::config("plataformas");

        $plataformas = [0 => "Todas"] + Plataforma::all()->pluck('name', 'id')->toArray();

        if (!$todas) {
            unset($plataformas[0]);
        }

        return $plataformas;
    }

    public static function formulario($t = null)
    {
        $arr = self::config("formularios");

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getPlataforma()
    {
        $config = env('APP_CONFIG', 'vcn');

        $plat = Session::get('vcn.plataforma');

        $plataformas = config("$config.plataformas");

        return array_search($plat, $plataformas);
    }

    public static function getPrecioRegla($model)
    {
        $ret = "";

        $txt_duracion = ConfigHelper::getPrecioDuracion($model->duracion);

        switch ($model->duracion_tipo) {
            case 0: //cualquiera
                {
                    if ($model->duracion > 1) {
                        $ret = $txt_duracion;
                    } else //Monto fijo
                    {
                        $ret = $txt_duracion . " para " . $model->rango1 . " " . ConfigHelper::getPrecioDuracionUnit($model->duracion_fijo);
                    }
                }
                break;

            case 1: //A partir de
                {
                    $ret = $txt_duracion . " para " . ConfigHelper::getPrecioDuracionTipo($model->duracion_tipo);
                    $ret .= " " . $model->rango1;
                }
                break;

            case 2: //rango
                {
                    $ret = $txt_duracion . " para " . ConfigHelper::getPrecioDuracionTipo($model->duracion_tipo);
                    $ret .= " entre " . $model->rango1 . " y " . $model->rango2;
                }
                break;
        }

        return $ret;
    }

    public static function calcularFechaDia($fecha, $dia)
    {
        $ret = Carbon::parse($fecha);

        if ($dia == 7) {
            $dia = 0;
        };

        while ($ret->dayOfWeek != $dia) {
            $ret->addDay();
        }

        return $ret;
    }

    //== avisos ==
    public static function getAvisoTipo($t = null)
    {
        $arr = ['Bookings']; //, 'Leads'];

        if (!is_numeric($t)) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getAvisoDoc($t = null)
    {
        $arr = [
            0 => 'Nota de Pago Booking (SIN INTRO)',
            1 => 'British Summer: informació vols Bournemouth',
            2 => 'Recordatorio Pasaportes',

            3 => 'Nueva LOPD - Bookings',
            4 => 'Nueva LOPD - Sin Booking',
        ];

        if (!is_numeric($t)) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getAvisoDocTipo($t = null)
    {
        $arr = [
            0 => 'Documento',
            1 => 'Nota de Pago Booking (asunto + intro + plantilla)',
        ];

        if (!is_numeric($t)) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getTipoDoc($t = null)
    {
        $arr = ['DNI', 'NIE'];

        if (!is_numeric($t)) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getAvisoCuandoTipo($t = null)
    {
        $arr = [
            'trigger' => "Trigger",
            'dia' => "Diario",
            'semana' => "Semanal",
            'mes' => "Mensual",
            'puntual' => "Puntual",
            'tarea' => "Trigger Tarea"
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getAvisoTrigger($t = null)
    {
        $arr = [
            'booking-status' => 'Booking Status',
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getAvisoDestinatario($t = null)
    {
        $arr = [
            'viajero' => 'Viajero',
            'tutores' => 'Tutores',
            'ambos' => 'Viajero y Tutores',
            'user' => 'Usuario',
            'prescriptor' => 'Prescriptor',

            // 'all'=> 'Todos', //??
            // 'email'=> 'Email', //destinos json
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getCuestionarioDestinatario($t = null)
    {
        $arr = [
            'viajero' => 'Viajero',
            'tutores' => 'Tutores',
            'ambos' => 'Viajero y Tutores',
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getIdiomaWeb($t = null)
    {
        $arr = [
            'es' => 'Español',
            'ca' => 'Català',
            'en' => 'English',
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getCategoriaPlantilla($t = null)
    {
        $arr = ['', 'ingles', 'idiomas', 'escolar', 'maxcamps', 'coloniascic', 'colectivos'];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getPaises($t = null)
    {
        $default = null;
        if (config('app.timezone') == "Europe/Madrid") {
            $default = ["ESPAÑA" => "ESPAÑA"];
        } elseif (config('app.timezone') == "America/Mexico_City") {
            $default = ["MEXICO" => "MEXICO"];
        }

        $arr = ["" => ""] + $default + \VCN\Models\Pais::orderBy('name')->pluck('name', 'name')->toArray();

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getTipoBloque($t = null)
    {
        $arr = [
            1 => '1 Semana',
            2 => '2 Semanas',
            3 => '3 Semanas',
            4 => '4 Semanas'
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getTipoRaiz($t = null)
    {
        $arr = [
            0 => 'Categorías Web',
            'Especialidades Web',
            // 'Países'
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    /**
     *
     */
    public static function getGoogle($param = "ganalytics")
    {
        $ga = Session::get("vcn.$param", null);

        if (!$ga) {
            $p = self::propietario();
            $plat = Plataforma::find($p);

            if (!$plat) {
                // \Log::warning("Error getGoogle ($p)");
                return "";
            }

            $ga = $plat->$param;
            Session::put("vcn.$param", $ga);
        }

        return $ga;
    }

    public static function getHojaInscripcion($t = null)
    {
        $sufijo = Self::config('sufijo');
        $dir = public_path("assets/inscripcion/$sufijo/condiciones/");

        if (!file_exists($dir)) {
            File::makeDirectory($dir, 0775, true);
        }

        $files = File::allFiles($dir);

        if (!$files) {
            return $t;
        }

        $arr = [0 => "Ninguna"];
        foreach ($files as $file) {
            $f = pathinfo($file)['basename'];
            $arr[$f] = $f;
        }

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function facturaNumero($any = null, $plat = null)
    {
        $plat = $plat ?: ConfigHelper::config('propietario');
        $any = $any ?: Carbon::now()->format('Y');

        $fn = DB::table('plataforma_factura_numeros')->where('plataforma', $plat)->where('any', $any)->first();

        if (!$fn) {
            $num = 1;
            $num = $any . str_pad($num, 4, "0", STR_PAD_LEFT);

            DB::table('plataforma_factura_numeros')->insert(
                ['plataforma' => $plat, 'any' => $any, 'numero' => $num]
            );
        } else {
            $num = intval(substr($fn->numero, -4));
            $num++;
            $num = $any . str_pad($num, 4, "0", STR_PAD_LEFT);

            DB::table('plataforma_factura_numeros')
                ->where('id', $fn->id)
                ->update(['numero' => $num]);
        }

        return $num;
    }

    public static function campoValor($campo)
    {
        return \VCN\Models\System\Campo::where('name', $campo)->first();
    }

    public static function getCICAcademicYearTestColonias($t = null)
    {
        $arr = [
            '',
            0 => '',
            3 => '3rd P',
            4 => '4th P',
            5 => '5th P',
            6 => '6th P',
            7 => '1st ESO',
            8 => '2nd ESO',
            9 => '3rd ESO',
            10 => '4th ESO'
        ];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getCICNivelTestColonias($t = null)
    {
        $arr = [
            '',
            'J4',
            'JH',
            '101',
            '102',
            '201',
            '202',
            '301',
            '302',
            '401',
            '402',
            '403',
        ];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function getCICenglishlevelTestColonias($t = null)
    {
        $arr = [
            '',
            'Low',
            'High',
        ];

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public static function limpiarAcentos($String)
    {
        $String = str_replace(array('á', 'à', 'â', 'ã', 'ª', 'ä'), "a", $String);
        $String = str_replace(array('Á', 'À', 'Â', 'Ã', 'Ä'), "A", $String);
        $String = str_replace(array('Í', 'Ì', 'Î', 'Ï'), "I", $String);
        $String = str_replace(array('í', 'ì', 'î', 'ï'), "i", $String);
        $String = str_replace(array('é', 'è', 'ê', 'ë'), "e", $String);
        $String = str_replace(array('É', 'È', 'Ê', 'Ë'), "E", $String);
        $String = str_replace(array('ó', 'ò', 'ô', 'õ', 'ö', 'º'), "o", $String);
        $String = str_replace(array('Ó', 'Ò', 'Ô', 'Õ', 'Ö'), "O", $String);
        $String = str_replace(array('ú', 'ù', 'û', 'ü'), "u", $String);
        $String = str_replace(array('Ú', 'Ù', 'Û', 'Ü'), "U", $String);
        $String = str_replace(array('[', '^', '´', '`', '¨', '~', ']'), "", $String);
        $String = str_replace("ç", "c", $String);
        $String = str_replace("Ç", "C", $String);
        $String = str_replace("ñ", "n", $String);
        $String = str_replace("Ñ", "N", $String);
        $String = str_replace("Ý", "Y", $String);
        $String = str_replace("ý", "y", $String);

        $String = str_replace("&aacute;", "a", $String);
        $String = str_replace("&Aacute;", "A", $String);
        $String = str_replace("&eacute;", "e", $String);
        $String = str_replace("&Eacute;", "E", $String);
        $String = str_replace("&iacute;", "i", $String);
        $String = str_replace("&Iacute;", "I", $String);
        $String = str_replace("&oacute;", "o", $String);
        $String = str_replace("&Oacute;", "O", $String);
        $String = str_replace("&uacute;", "u", $String);
        $String = str_replace("&Uacute;", "U", $String);

        return $String;
    }

    public static function es_booking_online()
    {
        if (self::config('tpv_activo')) {
            $p = self::propietario();
            $plat = Plataforma::find($p);

            if (!self::config('tpv_activo_banco')) {
                if (!$plat->tpv['comercio']) {
                    return (bool) self::config('tpv_activo_banco');
                }
            }

            return true;
        }

        return (bool) self::config('tpv_activo_banco');
    }

    public static function es_booking_online_tpv()
    {
        if (self::config('tpv_activo')) {
            $p = self::propietario();
            $plat = Plataforma::find($p);

            if (!self::config('tpv_activo_banco')) {
                if (!$plat->tpv['comercio']) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    public static function es_booking_online_transferencia()
    {
        return self::config('tpv_activo_banco');
    }

    public static function bladeCompile($value, array $args = array())
    {
        $generated = \Blade::compileString($value);

        ob_start() and extract($args, EXTR_SKIP);

        try {
            eval('?>' . $generated);
        } catch (\Exception $e) {
            ob_get_clean();
            throw $e;
        }

        $content = ob_get_clean();

        return $content;
    }

    /**
     *
     */
    public static function tipoMailTags()
    {
        $ret = [
            //[ 'text'=>  'Menu item 3', 'value'=> 'Some plain text ...'],
        ];

        foreach (MailHelper::tipoTags() as $tag => $valor) {
            $ret[] = [
                'text' => $tag, 'value' => $tag
            ];
        }

        //return response()->json($ret);
        return json_encode($ret);
    }

    public static function arrayToJson($array, $tipo = null)
    {
        $ret = [];
        $user = auth()->user();

        switch ($tipo) {
            case 'oficinas': {
                    //Sin oficina

                    if ($user->es_full_admin) {
                        $hijos1 = [];
                        foreach (User::where('oficina_id', 0)->whereNotIn('roleid', [11, 12, 13])->get() as $u) {
                            $hijos1[] = ['id' => "u-" . $u->id, 'label' => $u->full_name];
                        }
                        $ret[] = ['id' => "o-0", 'label' => '- ADMIN -', 'children' => $hijos1];
                    }

                    if (!$user->filtro_oficinas) {
                        $array = \VCN\Models\System\Oficina::where('id', $user->oficina_id)->get();
                    }

                    foreach ($array as $oficina) {
                        $hijos1 = [];
                        foreach ($oficina->usuarios as $user) {
                            $hijos1[] = ['id' => "u-" . $user->id, 'label' => $user->full_name];
                        }

                        $ret[] = ['id' => "o-" . $oficina->id, 'label' => $oficina->name, 'children' => $hijos1];
                    }
                }
                break;

            case 'categorias': {
                    foreach ($array as $categoria) {
                        $hijos1 = [];
                        foreach ($categoria->subcategorias as $scat) {
                            $hijos2 = [];
                            foreach ($scat->subcategoriasdet as $scatd) {
                                $hijos2[] = ['id' => "csd-" . $scatd->id, 'label' => $scatd->name];
                            }

                            $hijos1[] = ['id' => "cs-" . $scat->id, 'label' => $scat->name, 'children' => $hijos2];
                        }

                        $ret[] = ['id' => "c-" . $categoria->id, 'label' => $categoria->name, 'children' => $hijos1];
                    }
                }
                break;

            case 'origenes': {
                    foreach ($array as $origen) {
                        $hijos1 = [];
                        foreach ($origen->suborigenes as $sori) {
                            $hijos2 = [];
                            foreach ($sori->suborigenesdet as $sorid) {
                                $hijos2[] = ['id' => "osd-" . $sorid->id, 'label' => $sorid->name];
                            }

                            $hijos1[] = ['id' => "os-" . $sori->id, 'label' => $sori->name, 'children' => $hijos2];
                        }

                        $ret[] = ['id' => "o-" . $origen->id, 'label' => $origen->name, 'children' => $hijos1];
                    }
                }
                break;

            default: {
                    foreach ($array as $id => $title) {
                        $ret[] = ['id' => $id, 'label' => $title];
                    }
                }
                break;
        }

        return json_encode($ret);
    }

    public static function paramsFromRequest(\Illuminate\Http\Request $request)
    {
        $res = [];
        $arrOficinas = $request->get('oficinas') ?: null;
        $arrOrigenes = $request->get('origenes') ?: null;
        $arrCategorias = $request->get('categorias') ?: null;
        $arrPaises = $request->get('paises') ?: null;
        $arrAnys = $request->get('anys') ?: 0;

        $res['oficinas'] = $arrOficinas ? explode(',', $arrOficinas) : null;
        $res['origenes'] = $arrOrigenes ? explode(',', $arrOrigenes) : null;
        $res['categorias'] = $arrCategorias ? explode(',', $arrCategorias) : null;
        $res['paises'] = $arrPaises ? explode(',', $arrPaises) : null;
        $res['anys'] = $arrAnys;

        return $res;
    }

    public static function arrayFromRequest(\Illuminate\Http\Request $request, $esArray = true)
    {
        $arrOficinas = $request->get('oficinas') ?: null;
        $arrOrigenes = $request->get('origenes') ?: null;
        $arrCategorias = $request->get('categorias') ?: null;
        $arrPaises = $request->get('paises') ?: null;
        $arrAnys = $request->get('anys') ?: 0;

        if ($esArray) {
            $arrOficinas = $arrOficinas ? explode(',', $arrOficinas) : [];
            $arrOrigenes = $arrOrigenes ? explode(',', $arrOrigenes) : [];
            $arrCategorias = $arrCategorias ? explode(',', $arrCategorias) : [];
            $arrPaises = $arrPaises ? explode(',', $arrPaises) : [];
            //$arrAnys = explode(',', $arrAnys);
        } else {
            $arrOficinas = $arrOficinas ?: [];
            $arrOrigenes = $arrOrigenes ?: [];
            $arrCategorias = $arrCategorias ?: [];
            $arrPaises = $arrPaises ?: [];
        }



        $res = [];

        $res['anys'] = $arrAnys;

        $oficinas = [];
        $usuarios = [];
        foreach ($arrOficinas as $item) {
            $pre = 'o-';
            if (strpos($item, $pre) !== false) {
                $oficinas[] = (int) substr($item, strlen($pre));
                continue;
            }

            $pre = 'u-';
            if (strpos($item, $pre) !== false) {
                $usuarios[] = (int) substr($item, strlen($pre));
                continue;
            }
        }
        $res['oficinas'] = $oficinas;
        $res['usuarios'] = $usuarios;

        $origenes = [];
        $origenes_sub = [];
        $origenes_subdet = [];
        foreach ($arrOrigenes as $item) {
            $pre = 'o-';
            if (strpos($item, $pre) !== false) {
                $origenes[] = (int) substr($item, strlen($pre));
                continue;
            }

            $pre = 'os-';
            if (strpos($item, $pre) !== false) {
                $origenes_sub[] = (int) substr($item, strlen($pre));
                continue;
            }

            $pre = 'osd-';
            if (strpos($item, $pre) !== false) {
                $origenes_subdet[] = (int) substr($item, strlen($pre));
                continue;
            }
        }
        $res['origenes'] = $origenes;
        $res['origenes_sub'] = $origenes_sub;
        $res['origenes_subdet'] = $origenes_subdet;

        $categorias = [];
        $categorias_sub = [];
        $categorias_subdet = [];
        foreach ($arrCategorias as $item) {
            $pre = 'c-';
            if (strpos($item, $pre) !== false) {
                $categorias[] = (int) substr($item, strlen($pre));
                continue;
            }

            $pre = 'cs-';
            if (strpos($item, $pre) !== false) {
                $categorias_sub[] = (int) substr($item, strlen($pre));
                continue;
            }

            $pre = 'csd-';
            if (strpos($item, $pre) !== false) {
                $categorias_subdet[] = (int) substr($item, strlen($pre));
                continue;
            }
        }
        $res['categorias'] = $categorias;
        $res['categorias_sub'] = $categorias_sub;
        $res['categorias_subdet'] = $categorias_subdet;

        $array = [];
        foreach ($arrPaises as $item) {
            $pre = '';
            //if(strpos($item, $pre) !== false)
            {
                $array[] = (int) substr($item, strlen($pre));
            }
        }
        $res['paises'] = $array;

        return $res;
    }

    /**
     *
     */
    public static function queryFromArray($filtros, $tabla, $fecha = 'fecha')
    {
        // oficinas, usuarios, origenes, origenes_sub, origenes_subdet, categorias, categorias_sub, categorias_subdet, paises
        // any, status

        $query = DB::table($tabla);

        //any
        $any = $filtros['anys'] ?: Carbon::now()->year;
        $fecha1 = $any . "-01-01";
        $fecha2 = $any . "-12-31";
        $query = $query->where($fecha, '>=', $fecha1)->where($fecha, '<=', $fecha2);

        $array = $filtros['oficinas'];
        $filtro = "$tabla.oficina_id";
        if (count($array)) {
            $query = $query->whereIn($filtro, $array);
        }

        $array = $filtros['usuarios'];
        $filtro = "$tabla.user_id";
        if (count($array)) {
            $query = $query->whereIn($filtro, $array);
        }

        $array = $filtros['origenes'];
        $filtro = "$tabla.origen_id";
        if (count($array)) {
            $query = $query->whereIn($filtro, $array);
        }

        $array = $filtros['origenes_sub'];
        $filtro = "$tabla.suborigen_id";
        if (count($array)) {
            $query = $query->whereIn($filtro, $array);
        }

        $array = $filtros['origenes_subdet'];
        $filtro = "$tabla.suborigendet_id";
        if (count($array)) {
            $query = $query->whereIn($filtro, $array);
        }

        $array = $filtros['categorias'];
        $filtro = "$tabla.category_id";
        if (count($array)) {
            $query = $query->whereIn($filtro, $array);
        }

        $array = $filtros['categorias_sub'];
        $filtro = "$tabla.subcategory_id";
        if (count($array)) {
            $query = $query->whereIn($filtro, $array);
        }

        $array = $filtros['categorias_subdet'];
        $filtro = "$tabla.subcategory_det_id";
        if (count($array)) {
            $query = $query->whereIn($filtro, $array);
        }

        //Paises
        $array = $filtros['paises'];
        if (count($array) && $tabla == "solicitudes") {
            $filtro = "destinos";
            $paises = [];
            foreach ($array as $p) {
                $pais = \VCN\Models\Pais::find($p);
                if ($pais) {
                    $paises[] = $pais->name;
                }
            }

            $query = $query->whereIn($filtro, $paises);
        }

        if (count($array) && $tabla == "bookings") {
            $filtro = "curso_id";

            $centros = \VCN\Models\Centros\Centro::where('country_id', $array)->pluck('id')->toArray();
            $cursos = \VCN\Models\Cursos\Curso::whereIn('center_id', $centros)->pluck('id')->toArray();

            $query = $query->whereIn($filtro, $cursos);
        }

        return $query;
    }

    public static function getTipoBloqueHome($t = null)
    {
        $arr = [
            1 => 'Full', //YA LO TIENES TODO?
            2 => 'Fifty Texto+Foto', //BE HAPPY
            3 => 'Fifty Foto+Texto', //BE HIPPY
            4 => 'Fifty full', //TE VAS EN GRUPO / BLA DI BLUM
            5 => '60/40 F+T', //LET'S GO FAMILY!
            6 => 'Texto',
            7 => 'Logos',
            // 8 => 'Slider'
            // 10 => "Intro",
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function getTipoBloqueHomeHeight($t = null)
    {
        $arr = [
            0 => 'Por defecto',
            1 => 'Pequeño',
            2 => 'Grande'
        ];

        if (!$t) {
            return $arr;
        }

        return $arr[$t];
    }

    public static function uploadOptimize($file, $dirp, $doWebp = false, $id = null)
    {
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file_name = str_slug($file_name);

        if ($id) {
            $file_name = $id;
        }

        $fileType = $file->getClientOriginalExtension();

        $ext = ".$fileType";
        // $file_name_original = $file_name . "_original" . $ext;
        $file_name_thumb = "thumb/" . $file_name . $ext;
        $file_name_webp = $file_name . "$ext.webp";
        $file_name .= $ext;

        $dir = public_path($dirp);
        $file->move($dir, $file_name);
        // copy($dir.$file_name, $dir.$file_name_original);

        $optimizerChain = OptimizerChainFactory::create();
        $optimizerChain->optimize($dir . $file_name); //Optimizamos la original

        //thumb
        $file = $file_name;
        if (!file_exists($dir . "thumb/")) {
            File::makeDirectory($dir . "thumb/", 0775, true);
        }
        File::copy($dir . $file, $dir . "thumb/" . $file);
        $thumb = Image::make($dir . $file_name_thumb);
        $thumb->resize(450, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save();

        if ($fileType == "jpg" || $fileType == "jpeg") {
            $optimizerThumb = (new OptimizerChain)
                ->addOptimizer(new Jpegoptim([
                    '--strip-all',
                    '-m30',
                    '--all-progressive',
                ]));

            $optimizerThumb->optimize($dir . $file_name_thumb);
        } else {
            $optimizerChain->optimize($dir . $file_name_thumb);
        }

        //webp
        if($doWebp)
        {
            $webp = Image::make($dir . $file_name);
            $webp->encode('webp', 85)->save($dir . $file_name_webp);
            $optimizerChain->optimize($dir . $file_name_webp); //Optimizamos webp
            
            // borrabamos jpg para dejar el webp
            // File::delete($dir. $file_name);
        }

        $ret = "/" . $dirp . $file_name;
        
        return $ret;
    }

    public static function isCookie($type)
    {
        $ret = $_COOKIE[$type] ?? false;
        
        if(!$ret)
        {
            $ret = $_COOKIE['cookieControlPrefs'][$type] ?? false;
        }

        return $ret ? true : false;
    }
}
