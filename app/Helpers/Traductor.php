<?php

namespace VCN\Helpers;

use VCN\Models\Traducciones\Traduccion;
use VCN\Helpers\ConfigHelper;
use VCN\Models\Categoria;
use VCN\Models\Subcategoria;
use VCN\Models\SubcategoriaDetalle;

use App;

class Traductor
{
    public static function get($lang, $modelo, $campo, $id)
    {
        $t = Traduccion::where(
                ['idioma'=> $lang, 'modelo'=> $modelo, 'campo'=> $campo, 'modelo_id'=> $id])->first();

        return $t ? $t->traduccion : "";
    }

    /**
     * @param $lang
     * @param $modelo
     * @param $campo
     * @param $id
     * @param $campoOriginal
     * @return string
     */
    public static function getWeb($lang, $modelo, $campo, $id, $campoOriginal)
    {
        $t = null;

        // dd('idioma: '. $lang.' - modelo: '.$modelo.' - campo: '. $campo.' - modelo_id: '.$id .' - o: '. $campoOriginal);
        if($lang != ConfigHelper::idiomaCRM())
        {
            $t = Traduccion::where(
                ['idioma' => $lang, 'modelo' => $modelo, 'campo' => $campo, 'modelo_id' => $id])->first();
        }

        if($t && $t->count() && $campoOriginal == '' && $campo == 'name_web')
        {
            //dd('idioma: '. $lang.' - modelo: '.$modelo.' - campo: '.$campo.' - modelo_id: '.$id);
            $t = Traduccion::where(
                ['idioma' => $lang, 'modelo' => $modelo, 'campo' => 'name', 'modelo_id' => $id])->first();
            if(!$t->count())
            {
                switch($modelo)
                {
                    case 'Categoria':
                        $t = Categoria::select($campo)->where('id',$id)->first();
                        if($t->name_web == null){
                            $t = Categoria::select('name')->where('id',$id)->first();
                        }
                    break;

                    case 'Subcategoria':
                        $t = Subcategoria::select($campo)->where('id',$id)->first();
                        if($t->name_web == null){
                            $t = Subcategoria::select('name')->where('id',$id)->first();
                        }
                    break;

                    case 'SubcategoriaDetalle':
                        $t = SubcategoriaDetalle::select($campo)->where('id',$id)->first();
                        if($t->name_web == null){
                            $t = SubcategoriaDetalle::select('name')->where('id',$id)->first();
                        }
                    break;
                }
                $t->traduccion = $t->name;
            }
        }

        $ret = "";

        if($t)
        {
            if( $lang != ConfigHelper::idiomaCRM() )
            {
                // $ret = "$lang/";
            }

            if($t->traduccion != "")
            {
                $ret .= $t->traduccion;
            }
            else
            {
                $ret .= $campoOriginal;
            }
        }
        else
        {
            $ret .= $campoOriginal;
        }

        //url imagenes
        // $fotos = '/photos/';
        // if( strpos($ret, $fotos) !== false )
        // {
        //     $fotosURL = 'https://'.ConfigHelper::config('web').'/photos/';
        //     $ret = str_replace($fotos, $fotosURL, $ret);
        // }


        // cc_cookie_accept:{{print_r($_COOKIE)}}
        // <a href="#" class="cc-cookie-reset">RESET COOKIES</a>
                        
        $cookie = $_COOKIE['cc_cookie_accept'] ?? false;
        $notCookies = $cookie ? ($cookie != 'cc_cookie_accept') : true;

        if(!$notCookies)
        {
            return $ret;
        }

        $filter = preg_replace('/<iframe.*?\/iframe>/i','', $ret);
        return $filter;

        //return $t?$t->traduccion:$campoOriginal;
    }

    public static function getTrans($modelo, $campo, $id, $campoOriginal, $lang=null)
    {
        $lang = $lang ?: App::getLocale();

        return self::getWeb($lang, $modelo, $campo, $id, $campoOriginal);
    }

    public static function trans( $modelo, $campo, $ficha, $nl2br=false )
    {
        $lang = App::getLocale();

        $ret = self::getWeb($lang, $modelo, $campo, $ficha->id, $ficha->$campo );
        
        return $nl2br ? nl2br($ret) : $ret;
    }

    public static function transLang($lang, $modelo, $campo, $ficha, $nl2br=false)
    {
        $ret = self::getWeb($lang, $modelo, $campo, $ficha->id, $ficha->$campo );
        
        return $nl2br ? nl2br($ret) : $ret;
    }
}