<?php

namespace VCN\Helpers;

use VCN\Models\Leads\ViajeroTarea;
use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingFamilia;
use VCN\Models\Bookings\BookingIncidencia;
use VCN\Models\Bookings\BookingFactura;
use VCN\Models\Bookings\BookingTarea;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Bookings\BookingPago;
use VCN\Models\System\Oficina;
use VCN\Models\User;
use VCN\Models\System\Aviso;
use VCN\Models\System\AvisoDoc;
use VCN\Models\CMS\Landing;
use VCN\Models\Exams\Examen;

use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\Tutor;
use VCN\Models\System\Cuestionario;
use VCN\Models\System\Plataforma;
use VCN\Models\Leads\ViajeroArchivo;
use VCN\Models\Cursos\Curso;
use VCN\Models\Solicitudes\Solicitud;

use Mail;
use Carbon;
use Blade;
use App;
use Log;
use DB;

class MailHelper
{
    public static function bladeCompile($string, array $args = array())
    {
        $generated = Blade::compileString($string);
        $args['__env'] = app(\Illuminate\View\Factory::class);

        ob_start();
        extract($args, EXTR_SKIP);

        try {
            eval('?>' . $generated);
        } catch (\Exception $e) {
            ob_get_clean();
            throw $e;
        }

        $content = ob_get_clean();

        return $content;
    }

    public static function tipoTags($t = null)
    {
        $arr = [
            'VIAJERO_TIPO'          => '{{$ficha->tipo}}', //'[Viajero|Tutor]',
            'BOOKING_TIPO'          => '{{$ficha->tipo}}', //'[Individual|ONLINE]',

            'PLATAFORMA'            => '{{ isset($ficha) ? $ficha->plataforma_name : ConfigHelper::plataformaApp() }}',

            'BOOKING_ID'            => '{{$ficha->id}}',
            'BOOKING_PLATAFORMA'    => '{{$ficha->plataforma_name}}',
            'BOOKING_CURSO'         => '{{$ficha->programa}}',
            'BOOKING_CONVOCATORIA'  => '{{$ficha->convocatoria_name}}',
            'BOOKING_PROGRAMA'      => '{{$ficha->programa}}',
            'BOOKING_VIAJERO'       => '{{$ficha->viajero_full_name}}',
            'BOOKING_VUELO'         => '{{$ficha->vuelo_name}}',

            'VIAJERO'               => '{{$ficha->full_name}}',
            'VIAJERO_NAME'          => '{{$ficha->viajero_name}}',
            'VIAJERO_FULL_NAME'     => '{{$ficha->full_name}}',

            'DESTINO_NAME'          => '{{ $destino->name }}',
            'DESTINO_FULL_NAME'     => '{{ $destino->full_name }}',
        ];

        if (!$t)
            return $arr;

        return $arr[$t];
    }

    /**
     * @param $value
     * @param $ficha
     * @return false|mixed|string
     * @throws \Exception
     */
    public static function replaceTags($value, $ficha)
    {
        if (!$ficha) {
            return $value;
        }

        $tags = self::tipoTags();

        foreach ($tags as $tag => $tagEval) {
            $tag = "[tag]" . $tag . "[/tag]";
            if (strpos($value, $tag) !== false) {
                $value = str_replace($tag, $tagEval, $value);
            }
        }

        //$args['ficha'] = $ficha;
        //$value = self::bladeCompile($value, $args);

        return $value;
    }

    public static function enviarCompile($ficha, array &$args = array())
    {
        $booking = isset($args['ficha']) ? $args['ficha'] : (isset($args['booking']) ? $args['booking'] : null);

        if (!isset($args['ficha'])) {
            $args['ficha'] = $booking;
        }

        if (!isset($args['destino'])) {
            $args['destino'] = auth()->user();
        }

        $plataforma = $booking ? $booking->plataforma_config : ConfigHelper::default_plataforma();
        $plataforma = $plataforma ?: ConfigHelper::default_plataforma();
        $baseUrl = $plataforma->area_url;
        $args['base_url'] = $baseUrl;

        $args['booking'] = $booking;

        if( !isset($args['web']) )
        {
            $p = $booking ? $booking->plataforma : 1;
            $web = ConfigHelper::config('area_url', $p) ?: ConfigHelper::config('web', $p);
            $args['web'] = $web;
        }

        $asunto = $ficha ? $ficha->asunto : $args['asunto'];
        if (isset($args['tipo_doc']) && $args['tipo_doc'] == 1) {
            $asunto = $args['asunto'];
        }

        //ASUNTO
        $asunto =  MailHelper::replaceTags($asunto, $booking);
        $asunto = MailHelper::bladeCompile($asunto, $args);
        $args['asunto'] = $asunto;

        //ASUNTO
        $body = $ficha ? $ficha->contenido : $args['contenido'];
        $body = MailHelper::replaceTags($body, $booking);
        $body = MailHelper::bladeCompile($body, $args);
        $args['body'] = $body;

        return $args;
    }

    public static function validar($destino)
    {
        if (!$destino) {
            return false;
        }

        if (!$destino->email) {
            return false;
        }

        if (trim($destino->email) == "") {
            return false;
        }

        if (!filter_var(($destino->email), FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }

    /**
     * @param $destino
     * @param $asunto
     * @param $template
     * @param $data
     */
    public static function enviar($name, $destino, $data, $adjunto = null, $queue = true)
    {
        if (!$data) {
            $data = [];
        }

        if (!self::validar($destino)) {
            return false;
        }

        if (App::environment('local') || App::environment('staging')) {
            $queue = false;
        }

        //Avisos
        if ($name == "avisodoc") {
            $template = "emails.layout_markdown";

            self::enviarCompile(null, $data);

            try {

                $m = new \VCN\Mail\MailTemplateMarkdown($destino, $data['asunto'], $template, $data, $adjunto);
                if ($queue) {
                    Mail::to($destino)->queue($m);
                } else {
                    Mail::to($destino)->send($m);
                }

                return true;
            } catch (\Exception $e) {
                return false;
            }

            return true;
        }

        $sysemail = \VCN\Models\System\SystemMail::where('name', $name)->first();
        if (!$sysemail) {
            return false;
        }

        //$template = "emails.". $sysemail->template;
        $body = null;
        $syslang = null;

        //if($sysemail->idiomas->count())
        if (!$sysemail->asunto) {
            $syslang = $sysemail->getLang($destino->idioma_contacto ?: "es");
            //$template = "emails.". $destino->idioma_contacto .".". $sysemail->template;
        }

        $ficha = $syslang ?: $sysemail;

        $template = "emails.layout_markdown";

        $data['destino'] = $destino;
        self::enviarCompile($ficha, $data);

        // dd($data);

        //alwaysFrom
        $plat = ConfigHelper::propietario();
        $p = DB::table('plataformas')->where('id',$plat)->first();
        if($p)
        {
            if($p->email)
            {
                Mail::alwaysFrom($p->email, $p->name);
            }
        }

        try {

            $m = new \VCN\Mail\MailTemplateMarkdown($destino, $data['asunto'], $template, $data, $adjunto);
            if ($queue) {
                Mail::to($destino)->queue($m);
            } else {
                Mail::to($destino)->send($m);
            }

            return true;
        } catch (\Exception $e) {
            \Log::warning($e);
            return false;
        }

        return true;
    }

    public static function mailCurso($data)
    {
        $template = "emails.web_curso";
        $template = "web.curso";

        $curso_id = (int) $data['curso_id'];
        $curso = Curso::find($curso_id);

        if (!$curso) {
            return false;
        }

        $destino = ConfigHelper::default_plataforma();
        $destino->plataforma = $destino->id;
        $data['plataforma'] = $destino->id;

        self::enviar($template, $destino, $data);

        //if( App::environment('local') || App::environment('staging')  )
        //{
        //    Mail::send($template, $data, function ($m) use ($destino, $asunto) {
        //        $m->to($destino->email, $destino->full_name)->subject($asunto);
        //    });
        //}
        //else
        //{
        //    $job = new \VCN\Jobs\JobMailer($template, $data, $destino, $asunto);
        //    dispatch($job);
        //}

        $cat = $curso->categoria;
        if ($cat && $cat->avisos_cursosweb) {
            foreach ($cat->avisos_cursosweb as $aviso) {
                $destino = User::find($aviso);

                if ($destino->plataforma == 0 || ($destino->plataforma == $curso->propietario)) {
                    self::enviar($template, $destino, $data);

                    //if(filter_var($destino->email, FILTER_VALIDATE_EMAIL))
                    //{
                    //    $job = new \VCN\Jobs\JobMailer($template, $data, $destino, $asunto);
                    //    dispatch($job);
                    //}
                }
            }
        }

        $cat = $curso->subcategoria;
        if ($cat && $cat->avisos_cursosweb) {
            foreach ($cat->avisos_cursosweb as $aviso) {
                $destino = User::find($aviso);

                if ($destino->plataforma == 0 || ($destino->plataforma == $curso->propietario)) {
                    self::enviar($template, $destino, $data);

                    //if(filter_var($destino->email, FILTER_VALIDATE_EMAIL))
                    //{
                    //    $job = new \VCN\Jobs\JobMailer($template, $data, $destino, $asunto);
                    //    dispatch($job);
                    //}
                }
            }
        }
    }

    public static function mailCatalogo($data)
    {
        $template = "emails.web_catalogo";

        $asunto = "Solicitud información en Catálogo";

        $destino = ConfigHelper::default_catalogos();
        $job = new \VCN\Jobs\JobMailer($template, $data, $destino, $asunto);
        dispatch($job);

        $destino = ConfigHelper::default_plataforma();
        $job = new \VCN\Jobs\JobMailer($template, $data, $destino, $asunto);
        dispatch($job);
    }

    public static function mailContacto($data)
    {
        $template = "web.contacto";

        $destino = isset($data['oficina']) ? $data['oficina'] : 0;
        $destino = Oficina::where('email', $destino)->first();
        if ($destino)
        {
            self::enviar($template, $destino, $data);
        }

        $destino = ConfigHelper::default_plataforma();
        self::enviar($template, $destino, $data);
    }

    public static function mailWebInformacion($data)
    {
        $template = "web.informacion";

        $destino = ConfigHelper::default_plataforma();
        self::enviar($template, $destino, $data);
    }

    public static function mailBookingVueloBilletes($booking, $cambios, $inicial = null)
    {
        $template = "booking.vuelo_billetes";

        if (!$booking->vuelo) {
            return false;
        }

        $vuelo = $booking->vuelo;
        if ($vuelo->fecha_salida) {
            $vuelo_fs = Carbon::parse($vuelo->fecha_salida);
            if ($vuelo_fs->lt(Carbon::now())) {
                return false;
            }
        }

        $plataforma = $booking->plataforma_model;
        $avisos = $plataforma->avisos['vuelo'];
        foreach ($avisos as $aviso) {
            $destino = User::find($aviso);

            $user = auth()->user();
            $data = ['booking' => $booking, 'user' => $user, 'cambios' => $cambios, 'inicial' => $inicial];

            self::enviar($template, $destino, $data);
        }
    }

    public static function mailBookingCuestionario($cuestionario_id, $booking, $destino)
    {
        $template = "booking.cuestionario";

        $idioma = $booking->viajero->idioma_contacto;

        $cuestionario = Cuestionario::findOrFail($cuestionario_id);

        $p = $booking->plataforma ?: 1;
        $web = ConfigHelper::config('area_url', $p) ?: ConfigHelper::config('web', $p);

        $data = ['booking' => $booking, 'web' => $web, 'cuestionario' => $cuestionario];

        self::enviar($template, $destino, $data);
    }

    public static function mailArea($user_id, $password)
    {
        $template = "area";

        $user = User::find($user_id);

        $p = $user->plataforma;
        $web = ConfigHelper::config('area_url', $p) ?: ConfigHelper::config('web', $p);

        $data = ['user' => $user, 'web' => $web, 'password' => $password, 'ficha' => $user->ficha];

        return self::enviar($template, $user, $data);
    }

    public static function mailAreaCompra($user_id, $password)
    {
        return self::mailArea($user_id, $password);
    }

    public static function mailAreaEmail($ficha)
    {
        $user = $ficha->usuario;
        $password = str_random(8);
        $user->password = bcrypt($password);
        $user->status = 2;
        $user->save();

        return self::mailArea($user->id, $password);
    }

    public static function mailAreaInfo($user_id, $password)
    {

        //estó se utilizó cuando hicimos el script de creación de área de usuario

        $user = User::find($user_id);

        if (!$user) {
            return;
        }

        $ficha = null;
        if ($user->es_viajero) {
            $ficha = $user->viajero;
        } elseif ($user->es_tutor) {
            $ficha = $user->tutor;
        }

        if (!$ficha) {
            return;
        }

        $idioma = $ficha->idioma_contacto;

        $asunto = "Acceso Área Cliente";
        if ($idioma == "ca") {
            $asunto = "Accés Àrea Client";
        }

        $template = "emails.$idioma.area_info";

        // $web = "www.britishsummer.com";
        // $weba = "bs.estudiaryviajar.com";
        // if($ficha->plataforma==2) //CIC
        // {
        //     $web = "viatges.iccic.edu";
        //     $weba = "cic.estudiaryviajar.com";
        // }

        $p = $ficha->plataforma;
        $web = ConfigHelper::config('web', $p);
        $weba = ConfigHelper::config('area_url', $p) ?: ConfigHelper::config('web', $p);

        $email = trim($user->email);
        if ($email) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                try {
                    Mail::send($template, ['user' => $user, 'web' => $web, 'weba' => $weba, 'password' => $password], function ($m) use ($email, $ficha, $asunto) {
                        $m->to($email, $ficha->full_name)->subject($asunto);
                    });
                } catch (\Exception $e) {
                }
            }
        }
    }

    public static function mailAvisoDocEspecificoViajero(Booking $booking, $doc_id)
    {
        $template = 'booking.doc_especifico_rechazado';

        $va = ViajeroArchivo::find($doc_id);
        $vadoc = (int) $va->doc_especifico_idioma;
        if ($vadoc == 0) {
            $doc = \VCN\Models\System\DocEspecifico::find($va->doc_especifico_id);
        } else {
            $doc = \VCN\Models\System\DocEspecificoLang::find($va->doc_especifico_id);
        }

        $data = ['booking' => $booking, 'doc' => $doc];

        $destino = $booking->viajero;
        self::enviar($template, $destino, $data);

        $destino = $booking->viajero->tutor1;
        self::enviar($template, $destino, $data);

        $destino = $booking->viajero->tutor2;
        self::enviar($template, $destino, $data);
    }

    public static function mailAvisoDocEspecifico(Booking $booking, $doc_id)
    {
        $template = 'booking.doc_especifico';

        $va = ViajeroArchivo::find($doc_id);

        if ($va->doc_especifico_idioma == "0") {
            $doc = \VCN\Models\System\DocEspecifico::find($va->doc_especifico_id);
        } else {
            $doc = \VCN\Models\System\DocEspecificoLang::find($va->doc_especifico_id);
        }

        if (!$doc) {
            return false;
        }

        $data = ['booking' => $booking, 'doc' => $doc];

        $destino = $booking->asignado;
        self::enviar($template, $destino, $data);

        $cat = $booking->curso->categoria;
        if ($cat && $cat->avisos_doc) {
            foreach ($cat->avisos_doc as $aviso) {
                $destino = User::find($aviso);

                if ($destino->plataforma == 0 || ($destino->plataforma == $booking->plataforma)) {
                    self::enviar($template, $destino, $data);
                }
            }
        }

        $cat = $booking->curso->subcategoria;
        if ($cat && $cat->avisos_doc) {
            foreach ($cat->avisos_doc as $aviso) {
                $destino = User::find($aviso);

                if ($destino->plataforma == 0 || ($destino->plataforma == $booking->plataforma)) {
                    self::enviar($template, $destino, $data);
                }
            }
        }
    }

    public static function mailAvisoDoc($ficha_id, $aviso_id, $tipo = "booking")
    {
        $aviso = Aviso::find($aviso_id);

        $iSend = 0;

        switch ($tipo) {
            case 'bookings': {
                    $ficha = Booking::find($ficha_id);
                    $plataforma = ConfigHelper::plataforma($ficha->plataforma);
                    $idioma = $ficha->viajero->idioma_contacto;
                    $viajero = $ficha->viajero;
                    $user = $ficha->asignado;
                    $prescriptor = $ficha->prescriptor;
                }
                break;
        }

        //destinos: viajero,tutores,ambos,user,prescriptor,email
        switch ($aviso->destino) {
            case 'viajero': {
                    $destino = $viajero;
                    $iSend += self::mailAvisoDocEnviar($aviso_id, $plataforma, $idioma, $destino);
                }
                break;

            case 'tutores': {
                    $destino = $viajero->tutor1;
                    if ($destino) {
                        $iSend += self::mailAvisoDocEnviar($aviso_id, $plataforma, $idioma, $destino);
                    }

                    $destino = $viajero->tutor2;
                    if ($destino) {
                        $iSend += self::mailAvisoDocEnviar($aviso_id, $plataforma, $idioma, $destino);
                    }
                }
                break;

            case 'ambos': {
                    $destino = $viajero;
                    $iSend += self::mailAvisoDocEnviar($aviso_id, $plataforma, $idioma, $destino);

                    $destino = $viajero->tutor1;
                    if ($destino) {
                        $iSend += self::mailAvisoDocEnviar($aviso_id, $plataforma, $idioma, $destino);
                    }

                    $destino = $viajero->tutor2;
                    if ($destino) {
                        $iSend += self::mailAvisoDocEnviar($aviso_id, $plataforma, $idioma, $destino);
                    }
                }
                break;

            case 'user': {
                    $destino = $user;
                    if ($destino) {
                        $iSend += Self::mailAvisoDocEnviar($aviso_id, $plataforma, $idioma, $destino);
                    }
                }
                break;

            case 'prescriptor': {
                    $destino = $prescriptor;
                    if ($destino) {
                        $iSend += Self::mailAvisoDocEnviar($aviso_id, $plataforma, $idioma, $destino);
                    }
                }
                break;

            case 'email': {
                    // $aviso->destinos

                    //Self::mailAvisoDocEnviar($aviso_id, $plataforma, $idioma, $destino->email, $destino->full_name);
                }
                break;
        }

        return $iSend;
    }

    public static function mailAvisoDocEnviar($aviso_id, $plataforma, $idioma, $destino)
    {
        $aviso = Aviso::find($aviso_id);
        $asunto = $aviso->doc->getCampo('titulo', $idioma);
        $contenido = $aviso->doc->getCampo('contenido', $idioma);

        $template = 'avisodoc';
        $data = ['asunto' => $asunto, 'contenido' => $contenido, 'ficha' => $destino];
        return self::enviar($template, $destino, $data);
    }

    public static function mailTemplate($booking_id, $tpl = "temp", $asunto = null, $idioma = null)
    {
        $booking = Booking::find($booking_id);

        $template = "emails.$tpl";
        $subject = "temp";
        if ($asunto) {
            $subject = $asunto;
        }

        if ($idioma) {
            $template = "emails.$idioma.$tpl";
        }

        $user = auth()->user();

        $iSend = 0;

        $destino = $booking->viajero;
        if ($destino) {
            // if(filter_var($destino->email, FILTER_VALIDATE_EMAIL))
            {
                // Mail::send($template, ['booking' => $booking, 'user'=> $user], function ($m) use ($booking,$destino,$subject) {
                //     $m->to($destino->email, $destino->full_name)->subject($subject);
                // });

                try {
                    $job = new \VCN\Jobs\JobMailer($template, ['booking' => $booking, 'user' => $user], $destino, $subject);
                    app('Illuminate\Contracts\Bus\Dispatcher')->dispatch($job);

                    // if(!count(Mail::failures()))
                    {
                        $iSend++;
                    }
                } catch (\Exception $e) {
                }
            }
        }

        $destino = $booking->viajero->tutor1;
        if ($destino) {
            // if(filter_var($destino->email, FILTER_VALIDATE_EMAIL))
            {
                // Mail::send($template, ['booking' => $booking, 'user'=> $user], function ($m) use ($booking,$destino,$subject) {
                //     $m->to($destino->email, $destino->full_name)->subject($subject);
                // });

                try {
                    $job = new \VCN\Jobs\JobMailer($template, ['booking' => $booking, 'user' => $user], $destino, $subject);
                    app('Illuminate\Contracts\Bus\Dispatcher')->dispatch($job);

                    // if(!count(Mail::failures()))
                    {
                        $iSend++;
                    }
                } catch (\Exception $e) {
                }
            }
        }

        $destino = $booking->viajero->tutor2;
        if ($destino) {
            // if(filter_var($destino->email, FILTER_VALIDATE_EMAIL))
            {
                // Mail::send($template, ['booking' => $booking, 'user'=> $user], function ($m) use ($booking,$destino,$subject) {
                //     $m->to($destino->email, $destino->full_name)->subject($subject);
                // });

                try {
                    $job = new \VCN\Jobs\JobMailer($template, ['booking' => $booking, 'user' => $user], $destino, $subject);
                    app('Illuminate\Contracts\Bus\Dispatcher')->dispatch($job);

                    // if(!count(Mail::failures()))
                    {
                        $iSend++;
                    }
                } catch (\Exception $e) {
                }
            }
        }

        return $iSend;
    }

    public static function mailBookingVuelo($booking_id)
    {
        $template = "booking.vuelo";

        $booking = Booking::find($booking_id);

        if (!$booking->status_id) {
            return;
        }

        $user = auth()->user();
        $data = ['booking' => $booking, 'user' => $user];

        $plataforma = $booking->plataforma_model;
        $avisos = $plataforma->avisos['vuelo'];
        foreach ($avisos as $aviso) {
            $destino = User::find($aviso);

            self::enviar($template, $destino, $data);
        }
    }

    public static function mailFactura($factura_id)
    {
        $factura = BookingFactura::find($factura_id);
        $booking = $factura->booking;

        $iSend = 0;

        $dir = storage_path("files/bookings/" . $booking->viajero_id . "/");
        $file = "factura_" . $factura->numero . ".pdf";
        if (!file_exists($dir . $file)) {
            BookingLog::addLog($booking, "mailFactura [Err:adjunto]");
            return $iSend;
        }

        $destino = $booking->viajero;
        if ($destino) {
            $iSend += Self::mailFacturaEnviar($factura_id, $destino);
        }

        $destino = $booking->viajero->tutor1;
        if ($destino) {
            $iSend += Self::mailFacturaEnviar($factura_id, $destino);
        }

        $destino = $booking->viajero->tutor2;
        if ($destino) {
            $iSend += Self::mailFacturaEnviar($factura_id, $destino);
        }

        return $iSend;
    }

    public static function mailFacturaEnviar($factura_id, $destino)
    {
        $template = "booking.factura";

        $factura = BookingFactura::find($factura_id);
        $booking = $factura->booking;
        $data = ['booking' => $booking];

        $dir = storage_path("files/bookings/" . $booking->viajero_id . "/");
        $file = "factura_" . $factura->numero . ".pdf";
        $adjunto = $dir . $file;

        return self::enviar($template, $destino, $data, $adjunto);
    }

    public static function mailBookingPago(BookingPago $pago)
    {
        $booking = $pago->booking;

        $iSend = 0;

        $bMenor = false;
        if ($booking->curso && $booking->curso->categoria && $booking->curso->categoria->es_menor) {
            $bMenor = true;
        }

        if (!$bMenor) {
            $destino = $booking->viajero;
            if ($destino) {
                $iSend += Self::mailBookingPagoEnviar($pago, $destino);
            }
        }

        $destino = $booking->viajero->tutor1;
        if ($destino) {
            $iSend += Self::mailBookingPagoEnviar($pago, $destino);
        }

        $destino = $booking->viajero->tutor2;
        if ($destino) {
            $iSend += Self::mailBookingPagoEnviar($pago, $destino);
        }

        return $iSend;
    }

    public static function mailBookingPagoEnviar(BookingPago $pago, $destino)
    {
        $template = "booking.pago";

        $booking = $pago->booking;
        $data = ['booking' => $booking, 'pago' => $pago];

        if ($destino) {
            return self::enviar($template, $destino, $data, $pago->pdf_name);

            /*if(filter_var($destino->email, FILTER_VALIDATE_EMAIL))
            {
                try{
                    Mail::send($template, ['booking'=> $booking, 'pago'=>$pago], function ($m) use ($pago, $destino, $asunto) {

                        //adjunto
                        $m->attach($pago->pdf_name);
                        $m->to($destino->email, $destino->full_name)->subject($asunto);
                    });
                }catch(\Exception $e){}

                if(count(Mail::failures())>0)
                {
                    return 0;
                }

                return 1;
            }*/
        }
    }

    public static function mailBookingNotaPago($booking_id, $aviso_id)
    {
        $aviso = Aviso::find($aviso_id);

        // Segundo pago

        $booking = Booking::find($booking_id);

        $iSend = 0;

        $destino = $booking->viajero;
        $iSend += self::mailBookingNotaPagoEnviar($booking_id, $destino, $aviso_id);

        $destino = $booking->viajero->tutor1;
        $iSend += self::mailBookingNotaPagoEnviar($booking_id, $destino, $aviso_id);

        $destino = $booking->viajero->tutor2;
        $iSend += self::mailBookingNotaPagoEnviar($booking_id, $destino, $aviso_id);

        return $iSend;
    }

    public static function mailBookingNotaPagoEnviar($booking_id, $destino, $aviso_id = null)
    {
        $booking = Booking::find($booking_id);

        $asunto = null;
        $intro = "";
        $idioma = $booking->viajero->idioma_contacto;

        $pc = 0.30;
        if ($aviso_id)
        {
            $aviso = Aviso::find($aviso_id);
            if ($aviso->doc) // && $aviso->doc->tipo)
            {
                $asunto = $aviso->doc->getCampo('titulo', $idioma);
                $intro = $aviso->doc->getCampo('contenido', $idioma);
                $doc = $aviso->doc;

                $importe = $booking->getImporteNotaPago($doc);
            }
        }

        $template = 'booking.nota_pago';

        $data = ['intro' => $intro, 'booking' => $booking, 'importe' => $importe];
        if( $asunto )
        {
            $data['asunto'] = $asunto;
        }

        return self::enviar($template, $destino, $data);
    }

    public static function mailBookingNotaPago35($booking_id)
    {
        $booking = Booking::find($booking_id);

        $iSend = 0;

        if (!$booking->notapago35_last) {
            return $iSend;
        }

        $destino = $booking->viajero;
        if ($destino) {
            if (filter_var($destino->email, FILTER_VALIDATE_EMAIL)) {
                $iSend += Self::mailBookingNotaPago35Enviar($booking_id, $destino);
            }
        }

        $destino = $booking->viajero->tutor1;
        if ($destino) {
            if (filter_var($destino->email, FILTER_VALIDATE_EMAIL)) {
                $iSend += Self::mailBookingNotaPago35Enviar($booking_id, $destino);
            }
        }

        $destino = $booking->viajero->tutor2;
        if ($destino) {
            if (filter_var($destino->email, FILTER_VALIDATE_EMAIL)) {
                $iSend += Self::mailBookingNotaPago35Enviar($booking_id, $destino);
            }
        }

        return $iSend;
    }

    public static function mailBookingNotaPago35Enviar($booking_id, $destino)
    {
        $booking = Booking::find($booking_id);

        $idioma = $booking->viajero->idioma_contacto;

        $plataforma = ConfigHelper::plataforma($booking->plataforma);

        $template = 'emails.' . $idioma . '.booking_notapago35';

        $fecha = Carbon::parse($booking->course_start_date)->subDays(30)->format('d/m/Y');
        $subject =  "Pago Final antes del $fecha";
        if ($idioma == "ca") {
            $subject =  "Pagament final abans del $fecha";
        }

        if ($destino) {
            if (filter_var($destino->email, FILTER_VALIDATE_EMAIL)) {
                try {
                    Mail::send($template, ['booking' => $booking], function ($m) use ($booking, $destino, $subject) {

                        //adjunto
                        $dir = storage_path("files/viajeros/" . $booking->viajero_id . "/");
                        $file = $booking->notapago35_last->doc;

                        $m->attach($dir . $file);

                        $m->to($destino->email, $destino->full_name)->subject($subject);
                    });
                } catch (\Exception $e) {
                }

                if (count(Mail::failures()) > 0) {
                    return 0;
                }

                return 1;
            }
        }
    }

    public static function mailBookingCancelar($booking_id)
    {
        $template = "booking.cancelar";

        $booking = Booking::find($booking_id);

        $data = ['booking' => $booking];

        $destino = $booking->asignado;
        if ($destino) {
            self::enviar($template, $destino, $data);
        }

        $cat = $booking->curso->categoria;
        if ($cat && $cat->avisos_cancel) {
            foreach ($cat->avisos_cancel as $aviso) {
                $destino = User::find($aviso);

                if ($destino->plataforma == 0 || ($destino->plataforma == $booking->plataforma)) {
                    self::enviar($template, $destino, $data);
                }
            }
        }
    }

    public static function mailBookingTarea($tarea_id)
    {
        $template = "booking.tarea";

        $tarea = BookingTarea::find($tarea_id);

        if ($tarea->user_id == $tarea->asign_to) {
            return;
        }

        $data = ['tarea' => $tarea, 'ficha' => $tarea->booking];
        $destino = $tarea->asignado;

        self::enviar($template, $destino, $data);
    }

    public static function mailTarea($tarea_id)
    {
        $template = "viajero.tarea";

        $tarea = ViajeroTarea::find($tarea_id);

        if ($tarea->user_id == $tarea->asign_to)
            return;

        $data = ['tarea' => $tarea, 'ficha' => $tarea->viajero];
        $destino = $tarea->asignado;

        self::enviar($template, $destino, $data);
    }

    public static function mailBookingIncidencia($incidencia_id)
    {
        $template = "booking.incidencia";

        $incidencia = BookingIncidencia::find($incidencia_id);

        if ($incidencia->user_id == $incidencia->asign_to)
            return;

        $data = ['incidencia' => $incidencia, 'ficha' => $incidencia->booking];
        $destino = $incidencia->asignado;

        self::enviar($template, $destino, $data);
    }

    public static function mailOvbkg($booking_id)
    {
        $template = "booking.ovbkg";

        $booking = Booking::find($booking_id);

        //Plazas
        $plazas_vuelo = $booking->vuelo ? $booking->vuelo->plazas_disponibles : 1; //pq si no hay vuelo es como si tuviera plaza
        if ($booking->alojamiento) {
            $pa = $booking->convocatoria->getPlazas($booking->alojamiento->id);
            $plazas_alojamiento = $pa ? $pa->plazas_disponibles : 0;
        } else {
            $pa = null;
            $plazas_alojamiento = 1;
        }

        $data = ['booking' => $booking, 'plazasv' => $plazas_vuelo, 'plazasa' => $plazas_alojamiento];

        $plataforma = $booking->plataforma_model;
        $avisos = $plataforma->avisos['overbooking'];
        foreach ($avisos as $aviso) {
            $destino = User::find($aviso);

            self::enviar($template, $destino, $data);
        }

        //Autorizado por
        if ($booking->ovbkg_id) {
            $destino = User::find($booking->ovbkg_id);
            self::enviar($template, $destino, $data);
        }
    }

    public static function mailBookingAviso($booking_id)
    {
        $template = "booking.aviso";

        $booking = Booking::find($booking_id);
        // $data = ['booking' => $booking];
        $web = $booking->plataforma_web;
        $data = ['booking' => $booking, 'web' => $web];

        BookingLog::addLog($booking, "mailBookingAviso enviado [$booking->plataforma :: $web]");

        $adjunto = null;
        if ($booking->es_online_comprobante) {
            $adjunto = storage_path("files/viajeros/" . $booking->viajero_id . "/" . $booking->id . "/" . $booking->es_online_comprobante);
        }

        $adjunto = storage_path("files/viajeros/" . $booking->viajero_id . "/" . $booking->id . "/test.pdf");

        if (!file_exists($adjunto)) {
            $adjunto = null;
        }


        if ($booking->online_es_backend) {
            $destino = $booking->online_backend_user;
            if ($destino) {
                self::enviar($template, $destino, $data, $adjunto);
            }
        }

        if ($booking->es_online) {
            $destino = $booking->asignado;
            if ($destino) {
                self::enviar($template, $destino, $data, $adjunto);
            }
        }

        $cat = $booking->curso->categoria;
        if ($cat) {
            if ($booking->online_es_backend) {
                $avisos = $cat->avisos;
            } else {
                $avisos = $booking->es_online ? $cat->avisos_online : $cat->avisos;
            }

            if ($avisos) {
                foreach ($avisos as $aviso) {
                    $destino = User::find($aviso);
                    if ($destino->plataforma == 0 || ($destino->plataforma == $booking->plataforma)) {
                        self::enviar($template, $destino, $data, $adjunto);
                    }
                }
            }
        }

        $scat = $booking->curso->subcategoria;
        if ($scat) {
            if ($booking->online_es_backend) {
                $avisos = $scat->avisos;
            } else {
                $avisos = $booking->es_online ? $scat->avisos_online : $scat->avisos;
            }

            if ($avisos) {
                foreach ($avisos as $aviso) {
                    $destino = User::find($aviso);
                    if ($destino->plataforma == 0 || ($destino->plataforma == $booking->plataforma)) {
                        self::enviar($template, $destino, $data, $adjunto);
                    }
                }
            }
        }
    }

    public static function mailCambioImportes($booking_id)
    {
        $template = "booking.importes";

        $booking = Booking::find($booking_id);

        $data = ['booking' => $booking];

        $plataforma = $booking->plataforma_model;
        if (isset($plataforma->avisos['cambio-importes'])) {
            $avisos = isset($plataforma->avisos['cambio-importes']) ? $plataforma->avisos['cambio-importes'] : [];
            foreach ($avisos as $aviso) {
                $destino = User::find($aviso);

                self::enviar($template, $destino, $data);
            }
        }
    }

    public static function mailBooking($booking_id)
    {
        $template = "booking.confirmacion";

        $booking = Booking::find($booking_id);
        $data = ['booking' => $booking];

        $destino = $booking->viajero;
        if (filter_var($destino->email, FILTER_VALIDATE_EMAIL)) {
            self::enviar($template, $destino, $data);

            \VCN\Models\Leads\ViajeroLog::addLog($destino, "Mail Booking $booking_id");
            BookingLog::addLog($booking, "mailBooking Viajero enviado");
        }

        $destino = $booking->viajero->tutor1;
        if ($destino) {
            if (filter_var($destino->email, FILTER_VALIDATE_EMAIL)) {
                self::enviar($template, $destino, $data);

                \VCN\Models\Leads\TutorLog::addLog($destino, "Mail Booking $booking_id");
                BookingLog::addLog($booking, "mailBooking Tutor1 enviado");
            }
        }

        $destino = $booking->viajero->tutor2;
        if ($destino) {
            if (filter_var($destino->email, FILTER_VALIDATE_EMAIL)) {
                self::enviar($template, $destino, $data);

                \VCN\Models\Leads\TutorLog::addLog($destino, "Mail Booking $booking_id");
                BookingLog::addLog($booking, "mailBooking Tutor2 enviado");
            }
        }
    }

    public static function mailBookingFirma(Booking $booking, $cambios = false)
    {
        $template = $cambios ? "booking_firma" : "booking_firma_pendiente";

        $booking_id = $booking->id;
        $iSend = 0;

        $data = ['booking' => $booking];

        if (!$booking->es_menor) {
            $destino = $booking->viajero;

            if($destino)
            {
                $iSend += self::enviar($template, $destino, $data);

                \VCN\Models\Leads\ViajeroLog::addLog($destino, "Mail Booking firma $booking_id");
                BookingLog::addLog($booking, "mailBookingFirma Viajero enviado");
            }
        }

        $firmante = null;
        if ($booking->es_online)
        {
            //Se la pedimos al otro ???.

            if ($booking->es_tutor1) {
                $firmante = "tutor1";
            }

            if ($booking->es_tutor2) {
                $firmante = "tutor2";
            }
        }

        if (!$firmante || $firmante == "tutor1") {
            $destino = $booking->viajero->tutor1;
            
            if($destino)
            {
                if(self::enviar($template, $destino, $data))
                {
                    $iSend ++;
                    \VCN\Models\Leads\TutorLog::addLog($destino, "Mail Booking firma $booking_id");
                    BookingLog::addLog($booking, "mailBookingFirma Tutor1 enviado");
                }
                else
                {
                    \VCN\Models\Leads\TutorLog::addLog($destino, "Mail Booking firma $booking_id ERROR");
                    BookingLog::addLog($booking, "mailBookingFirma Tutor1 NO enviado");
                }
            }
        }

        if (!$firmante || $firmante == "tutor2") {
            $destino = $booking->viajero->tutor2;

            if($destino)
            {
                if(self::enviar($template, $destino, $data))
                {
                    $iSend ++;
                    \VCN\Models\Leads\TutorLog::addLog($destino, "Mail Booking firma $booking_id");
                    BookingLog::addLog($booking, "mailBookingFirma Tutor2 enviado");
                }
                else
                {
                    \VCN\Models\Leads\TutorLog::addLog($destino, "Mail Booking firma $booking_id ERROR");
                    BookingLog::addLog($booking, "mailBookingFirma Tutor2 NO enviado");
                }
            }
        }

        return $iSend;
    }

    public static function mailBookingOnline(Booking $booking, $destino)
    {
        $template = "booking.online";

        $web = $booking->plataforma_web;
        $data = ['booking' => $booking, 'web' => $web];

        self::enviar($template, $destino, $data);
    }

    public static function mailBookingOnlineOverbooking(Booking $booking)
    {
    }

    public static function mailBookingReunion($booking_id, $reunion_id = null)
    {
        $template = "booking.reunion";

        $iSend = 0;

        $booking = Booking::find($booking_id);

        $reunion = $reunion_id ? \VCN\Models\Reuniones\Reunion::find($reunion_id) : $booking->reunion;

        if (!$reunion) {
            return false;
        }

        $web = $booking->plataforma_web;
        $data = ['booking' => $booking, 'web' => $web];

        $destino = $booking->viajero;
        $iSend += self::enviar($template, $destino, $data);

        $destino = $booking->viajero->tutor1;
        $iSend += self::enviar($template, $destino, $data);

        $destino = $booking->viajero->tutor2;
        $iSend += self::enviar($template, $destino, $data);

        return $iSend;
    }

    public static function mailExamen(Examen $exam, Viajero $viajero, $destino, $area = true)
    {
        $template = $destino->es_viajero ? "examen.viajero" : "examen.tutor";
        if ($area) {
            $template = $destino->es_viajero ? "area.examen.viajero" : "area.examen.tutor";
        }

        $web = $viajero->plataforma_web;
        $link = route('area.exams.ask', [$exam->id, $viajero->id, 'viajero']);
        $data = [
            'ficha' => $viajero,
            'web' => $web,
            'examen' => $exam, 'examen_link' => $link,
            'oficina_email' => $viajero->oficina ? $viajero->oficina->email : "-",
            'oficina_tlf' => $viajero->oficina ? $viajero->oficina->telefono : "-",
        ];

        return self::enviar($template, $destino, $data);
    }

    public static function mailBookingExamen(Examen $exam, Booking $booking, $destino)
    {
        $template = $destino->es_viajero ? "area.examen.viajero" : "area.examen.tutor";

        $web = $booking->plataforma_web;
        $link = route('area.exams.ask', [$exam->id, $booking->id, 'booking']);
        $data = [
            'ficha' => $booking->viajero,
            'web' => $web,
            'examen' => $exam, 'examen_link' => $link,
            'oficina_email' => $booking->oficina ? $booking->oficina->email : "-",
            'oficina_tlf' => $booking->oficina ? $booking->oficina->telefono : "-",
        ];

        return self::enviar($template, $destino, $data);
    }

    private static function mailBookingEnviar($booking_id, $destino, $template, $asunto)
    {
        $booking = Booking::find($booking_id);

        $idioma = $booking->viajero->idioma_contacto;
        $template = 'emails.' . $idioma . "." . $template;

        if ($destino) {
            // if( App::environment('local') || App::environment('staging')  )
            // {
            //     Mail::send($template, ['booking' => $booking], function ($m) use ($booking, $destino, $asunto) {
            //         $m->to(trim($destino->email), $destino->full_name)->subject($asunto);
            //     });
            // }
            // else
            {
                $job = new \VCN\Jobs\JobMailer($template, ['booking' => $booking], $destino, $asunto);
                // app('Illuminate\Contracts\Bus\Dispatcher')->dispatch($job);
                dispatch($job);
            }
        }
    }

    public static function mailAvisoReserva($booking_id, $aviso)
    {
        $booking = Booking::find($booking_id);

        // $aviso => 1: viajero y tutores / 2 => user

        if ($aviso == 1) {
            $template = "booking.caducar1_viajero";

            $destino = $booking->viajero;
            $data = ['booking' => $booking, 'hola' => $destino];
            self::enviar($template, $destino, $data);

            //Tutores
            $template = "booking.caducar1_tutores";

            $destino = $booking->viajero->tutor1;
            $data = ['booking' => $booking, 'hola' => $destino];
            self::enviar($template, $destino, $data);

            $destino = $booking->viajero->tutor2;
            $data = ['booking' => $booking, 'hola' => $destino];
            self::enviar($template, $destino, $data);
        } elseif ($aviso == 2) {
            $template = 'booking.caducar2';

            $destino = $booking->viajero->asignado;
            $data = ['booking' => $booking, 'hola' => $destino];
            self::enviar($template, $destino, $data);
        }
    }

    public static function mailDatos($viajero_id, $datos = null, $tab = null)
    {
        $template = "viajero.datos";

        $viajero = Viajero::find($viajero_id);

        if (!$viajero) {
            return;
        }

        if (!$datos) {
            return;
        }

        $asignado = null;
        $booking = null;

        $asignado = $viajero->asignado;

        //booking
        $booking = null;
        if ($viajero->booking) {
            $booking = $viajero->booking;
        } else {
            $booking = $viajero->bookings->sortByDesc('id')->first();
        }

        $data = ['viajero' => $viajero, 'datos' => $datos, 'tab' => $tab];

        if ($booking) {
            $cat = $booking->curso->categoria;
            if ($cat && $cat->avisos_datos) {
                foreach ($cat->avisos_datos as $aviso) {
                    $destino = User::find($aviso);

                    if ($destino->plataforma == 0 || ($destino->plataforma == $booking->plataforma)) {
                        self::enviar($template, $destino, $data);
                    }
                }
            }

            $scat = $booking->curso->subcategoria;
            if ($scat && $scat->avisos_datos) {
                foreach ($scat->avisos_datos as $aviso) {
                    $destino = User::find($aviso);

                    if ($destino->plataforma == 0 || ($destino->plataforma == $booking->plataforma)) {
                        self::enviar($template, $destino, $data);
                    }
                }
            }
        }

        if ($asignado) {
            $destino = $asignado;

            self::enviar($template, $destino, $data);
        }
    }

    public static function mailWebRegistro($user_id)
    {
        $template = "web.registro";

        $user = User::find($user_id);
        $data = ['user' => $user, 'ficha' => $user->ficha];

        return self::enviar($template, $user, $data);
    }

    public static function mailEnvioCatalogo(Solicitud $solicitud)
    {
        $template = 'solicitud.catalogo';

        $viajero = $solicitud->viajero;

        $data = ['ficha' => $solicitud];

        $destino = $solicitud->asignado;
        self::enviar($template, $destino, $data);

        $cat = $solicitud->categoria;
        if ($cat && $cat->avisos_doc) {
            if ($cat->avisos_catalogo) {
                foreach ($cat->avisos_catalogo as $aviso) {
                    $destino = User::find($aviso);

                    if ($destino->plataforma == 0 || ($destino->plataforma == $viajero->plataforma)) {
                        self::enviar($template, $destino, $data);
                    }
                }
            }
        }

        $cat = $solicitud->subcategoria;
        if ($cat && $cat->avisos_doc) {
            foreach ($cat->avisos_catalogo as $aviso) {
                $destino = User::find($aviso);

                if ($destino->plataforma == 0 || ($destino->plataforma == $viajero->plataforma)) {
                    self::enviar($template, $destino, $data);
                }
            }
        }
    }

    public static function mailLanding(Landing $landing, $data = null, $esCatalogo=false)
    {
        $template = $esCatalogo ? "landing.aviso_catalogo" : "landing.aviso";
        $avisos = $esCatalogo ? $landing->avisos_catalogo : $landing->avisos;

        foreach ($avisos as $aviso) {
            $destino = User::find($aviso);

            self::enviar($template, $destino, $data);
        }

        return true;
    }
}
