<?php

namespace VCN\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

use VCN\Models\Alojamientos\AlojamientoPrecio;
use VCN\Models\Convocatorias\Precio;

class AlojamientoPrecioOld extends Model
{
    protected $table = 'alojamiento_precios_old';
}

class ConvocatoriaAbiertaPrecioOld extends Model
{
    protected $table = 'convocatoria_costes';
}

class TraspasoPrecios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'traspaso-precios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Traspaso precios al nuevo sistema de precios.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::table('alojamiento_precios')->delete();
        \DB::table('convocatoria_precios')->delete();

        foreach( AlojamientoPrecioOld::all() as $p )
        {
            $n = new AlojamientoPrecio;

            $n->name = $p->accommodation_prices_description;
            $n->importe = $p->accommodation_prices_price_per_week;
            $n->desde = $p->accommodation_prices_start_date;
            $n->hasta = $p->accommodation_prices_end_date;
            $n->moneda_id = $p->accommodation_prices_currency_id;
            $n->alojamiento_id = $p->accommodation_id;

            $n->duracion = 2;
            $n->duracion_tipo = 0;

            $n->save();

        }

        foreach( ConvocatoriaAbiertaPrecioOld::all() as $p )
        {
            $n = new Precio;

            $n->name = $p->convocatory_open_cost_name;
            $n->importe = $p->convocatory_open_cost_price_per_range;
            // $n->desde = $p->convocatory_open_cost_start_date;
            // $n->hasta = $p->convocatory_open_cost_end_date;
            $n->moneda_id = $p->convocatories_open_currency_id;
            $n->convocatory_id = $p->convocatory_id;

            $n->rango1 = $p->convocatory_open_cost_first_week_range;
            $n->rango2 = $p->convocatory_open_cost_second_week_range;

            $n->duracion = 2;
            $n->duracion_tipo = 2;

            $n->save();
        }
    }
}
