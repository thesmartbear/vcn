<?php

namespace VCN\Console\Commands;

use Illuminate\Console\Command;

use VCN\Models\User;
use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingPago;
use VCN\Models\Leads\Viajero;

use Excel;
use Mail;
use Carbon;
use ConfigHelper;
use VCN\Helpers\MailHelper;
use VCN\Models\System\Plataforma;

class Informes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'informe {tipo : Tipo de informe}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generador de informes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tipo = $this->argument('tipo');
        // $plataforma = $this->option('plataforma');
        // $user = $this->option('user');

        switch($tipo)
        {
            case 'menores12':
            {
                foreach( Plataforma::all() as $plat )
                {
                    $plataforma = $plat->id;

                    ConfigHelper::commandPlataforma($plataforma);

                    $datos = self::getInformeMenores12($plataforma);
                    $p = $plataforma;

                    $avisos = $plat->avisos['informes'] ?? null;

                    if(!$avisos) { continue; }

                    //Generar excel
                    
                    if(sizeof($datos)>0)
                    {
                        // $plat = ConfigHelper::config_plataforma($plataforma);
                        $ptxt = $plat ? $plat->name : "-";

                        $fecha = Carbon::now()->format('d-m-Y');
                        $title = "Informe Menores12 $ptxt";

                        Excel::store( new \VCN\Exports\InformeArrayExport($datos,$title), "informe_m12_p$p.xls");
                        
                        /* Excel::create("informe_m12_p$p", function($excel) use ($datos,$ptxt) {

                            $fecha = Carbon::now()->format('d-m-Y');
                            $excel->setTitle("Informe Menores12 $ptxt");
                            $excel->setCreator('VCN')
                                  ->setCompany('VCN');
                            $excel->setDescription("Informe de Menores12 $ptxt $fecha");

                            $excel->sheet("$fecha", function($sheet) use ($datos) {
                                $sheet->fromArray($datos);
                            });
                        });//->store('xls'); */

                        // $destino = User::find($user); //17
                        foreach($avisos as $aviso)
                        {
                            $destino = User::find($aviso);
                            if($destino)
                            {
                                Mail::send('emails.informe', ['tipo' => $tipo], function ($m) use ($p,$ptxt,$destino) {

                                    // $m->from('system@estudiaryviajar.com', "British Summer");

                                    $subject =  "Informe Menores12 $ptxt ". Carbon::now()->format('d-m-Y');

                                    $m->to($destino->email, $destino->full_name)->subject($subject);

                                    $informe = storage_path("app/informe_m12_p$p.xls");
                                    $m->attach($informe);
                                });
                            }
                        }
                    }
                }
            }
            break;

            case 'aviso-reservas':
            {
                foreach( Plataforma::all() as $plat )
                {
                    $plataforma = $plat->id;

                    // $this->info( $tipo .": ". $plataforma );

                    ConfigHelper::commandPlataforma($plataforma);
                    $status_id = ConfigHelper::config('booking_status_prereserva', $plataforma);

                    $bookings = Booking::where('fase',4)->where('status_id',$status_id);
                    $bookings = $bookings->where('plataforma', $plataforma);

                    $bookings = $bookings->get();

                    foreach($bookings as $b)
                    {
                        //aviso1 : viajeros y tutores -> asunto: Pre-reserva en %nombre curso% a punto de caducar
                        //aviso2 : usuario -> asunto: Pre-reserva %nbre viajero% caduca mañana
                        //caduca : $booking->setCaducado();

                        if(!$b->es_corporativo)
                        {
                            $aviso1 = $b->aviso1;
                            if ($aviso1) {
                                if ($aviso1->isToday()) {
                                    MailHelper::mailAvisoReserva($b->id, 1);
                                }
                            }

                            $aviso2 = $b->aviso2;
                            if ($aviso2) {
                                if ($aviso2->isToday()) {
                                    MailHelper::mailAvisoReserva($b->id, 2);
                                }
                            }
                        }

                        $caduca = $b->caduca;
                        if($caduca)
                        {
                            $caducado = ($caduca->diffInDays(Carbon::now(),false)>0);
                            if( $caduca->isToday() || $caducado)
                            {
                                $b->setCaducado();
                            }
                        }
                    }
                }

            }
            break;

            case 'pagos':
            {
                // $email_bs = "sergi@britishsummer.com"; //user 14
                // $email_cic = "cfisher@iccic.edu"; //user 3

                foreach( Plataforma::all() as $plat )
                {
                    $plataforma = $plat->id;

                    ConfigHelper::commandPlataforma($plataforma);

                    $datos = self::getInformePagos($plataforma);
                    $p = $plataforma;

                    $avisos = isset($plat->avisos['informe-pagos']) ? $plat->avisos['informe-pagos'] : [];

                    if(sizeof($datos)>0)
                    {
                        // $plat = ConfigHelper::config_plataforma($plataforma);
                        $ptxt = $plat ? $plat->name : "-";

                        //Generar excel
                        $fecha = Carbon::now()->format('d-m-Y');
                        $title = "Informe Pagos $ptxt";
                        Excel::store( new \VCN\Exports\InformeArrayExport($datos,$title), "informe_p$p.xls");
                        
                        /* Excel::create("informe_p$p", function($excel) use ($datos,$ptxt) {

                            $fecha = Carbon::now()->format('d-m-Y');
                            $excel->setTitle("Informe Pagos $ptxt");
                            $excel->setCreator('VCN')
                                  ->setCompany('VCN');
                            $excel->setDescription("Informe de Pagos $ptxt $fecha");

                            $excel->sheet("$fecha", function($sheet) use ($datos) {
                                $sheet->fromArray($datos);
                            });
                        });//->store('xls'); */

                        //Enviar Email
                        // $destino = User::find($user); //bs:14, cic:3
                        foreach($avisos as $aviso)
                        {
                            $destino = User::find($aviso);
                            if($destino)
                            {
                                Mail::send('emails.informe', ['tipo' => $tipo], function ($m) use ($p,$ptxt,$destino) {

                                    // $m->from('system@estudiaryviajar.com', "VCN");

                                    $subject =  "Informe Pagos $ptxt ". Carbon::now()->format('d-m-Y');
                                    $m->to($destino->email, $destino->full_name)->subject($subject);

                                    $informe = storage_path("app/informe_p$p.xls");
                                    $m->attach($informe);
                                });
                            }
                        }
                    }
                }
            }
            break;

            default:
            {
                $this->info("Tipos disponibles: pagos | menores12 | aviso-reservas");
            }
            break;
        }
    }


    private function getInformeMenores12($plataforma)
    {
        $viajeros = Viajero::plataforma()->pluck('id')->toArray();
        $bookings = Booking::where('enviado_m12',0)->whereIn('viajero_id', $viajeros)->where('transporte_ok',false)->where('vuelo_id','>',0)->get();

        // $bookings = Booking::where('enviado_m12',0)->where('transporte_ok',false)->where('vuelo_id','>',0)->get();

        $datos = [];
        $i = 0;
        foreach($bookings as $booking)
        {
            $dtb = Carbon::parse($booking->course_start_date);
            $dtv = Carbon::parse($booking->viajero->fechanac);
            $edad = $dtb->diffInYears($dtv);

            if($edad<12)
            {
                // $datos[$i][] = ;
                $datos[$i]['convocatoria'] = $booking->convocatoria->name;
                $datos[$i]['vuelo'] = $booking->vuelo->name ." : Loc. ". $booking->vuelo->localizador;
                $datos[$i]['agencia'] = $booking->vuelo->agencia?$booking->vuelo->agencia->name:"-";
                $datos[$i]['viajero'] = $booking->viajero->full_name;
                $datos[$i]['pasaporte'] = $booking->viajero->datos->pasaporte;
                $datos[$i]['edad'] = "$edad (". Carbon::parse($booking->viajero->fechanac)->format('d/m/Y') .")";

                $i++;

                $booking->enviado_m12 = 1;
                $booking->save();
            }
        }

        return $datos;
    }

    private function getInformePagos($plataforma)
    {
        if($plataforma)
        {
            $viajeros = Viajero::where('plataforma',$plataforma)->pluck('id')->toArray();
            $bookings = Booking::whereIn('viajero_id', $viajeros)->pluck('id')->toArray();
            $pagos = BookingPago::where('enviado',0)->whereIn('booking_id',$bookings)->orderBy('fecha')->get();
        }
        else
        {
            $pagos = BookingPago::where('enviado',0)->orderBy('fecha')->get();
        }

        $datos = [];
        $i = 0;
        foreach($pagos as $pago)
        {
            $viajero = $pago->booking->viajero;
            $booking = $pago->booking;
            $oficina = $viajero->oficina;

            $datos[$i]['importe'] = $pago->importe;
            $datos[$i]['fecha'] = Carbon::parse($pago->fecha)->format('d/m/Y');
            $datos[$i]['booking'] = $booking->id;
            $datos[$i]['cliente'] = $booking->viajero_id;
            $datos[$i]['razonsocial'] = $viajero->datos->fact_razonsocial ? $viajero->datos->fact_razonsocial : $viajero->full_name;
            $datos[$i]['nif'] = $viajero->datos->fact_nif?$viajero->datos->fact_nif: $viajero->datos->documento;
            $datos[$i]['tipopago'] = ConfigHelper::getTipoPago($pago->tipo);
            $datos[$i]['tipoprograma'] = $booking->curso->categoria?$booking->curso->categoria->name:'-';
            $datos[$i]['codigo'] = $booking->contable_code;
            $datos[$i]['cuenta'] = $oficina?substr($oficina->iban,-4):"-";
            $datos[$i]['oficina'] = $oficina?$oficina->name:"-";

            $i++;

            $pago->enviado = 1;
            $pago->save();
        }

        return $datos;

    }
}
