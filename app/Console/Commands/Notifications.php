<?php

namespace VCN\Console\Commands;

use Illuminate\Console\Command;

use VCN\Notifications\TestNotification;

use Carbon;

class Notifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webpush:test {user_id : UserID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PushWeb commands';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $uid = $this->argument('user_id');
        
        $t = "TEST";
        $m = "Test Notificaciones";
        $a = route('manage.index');

        $user = \VCN\Models\User::find($uid);

        $user->notify(new TestNotification($t,$m,$a));

    }
}
