<?php

namespace VCN\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Crawler\Crawler;
use DB;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $plataformas = DB::table('plataformas')->get();

        foreach($plataformas as $plat)
        {
            $url = "https://". $plat->web;
            $path = public_path('sitemap.xml');

            if($plat->id > 1)
            {
                $path = public_path('sitemap_'. $plat->sufijo .'.xml');
            }
            
            $sitemap = SitemapGenerator::create($url)
            ->configureCrawler(function (Crawler $crawler) {
                $crawler->setMaximumDepth(4);
            });
            // $sitemap = $sitemap->getSitemap();
            $sitemap->writeToFile($path);
        }
    }
}
