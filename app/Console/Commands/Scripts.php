<?php

namespace VCN\Console\Commands;

use Illuminate\Console\Command;

use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\ViajeroDatos;
use VCN\Models\Leads\ViajeroTutor;
use VCN\Models\Leads\Tutor;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Bookings\BookingTarea;
use VCN\Models\Bookings\BookingExtra;
use VCN\Models\Leads\ViajeroTarea;
use VCN\Models\Solicitudes\Solicitud;
use VCN\Models\Leads\Origen;
use VCN\Models\Pais;
use VCN\Models\Provincia;
use VCN\Models\Prescriptores\Prescriptor;
use VCN\Models\Prescriptores\Categoria as CategoriaPrescriptor;
use VCN\Models\System\Oficina;
use VCN\Models\Nacionalidad;
use VCN\Models\User;

use VCN\Models\System\Aviso;
use VCN\Models\System\AvisoLog;

use Excel;
use Carbon;
use DB;
use ConfigHelper;
use VCN\Helpers\MailHelper;
use Session;
use Request;
use File;
use Image;
use Log;

class Scripts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'script {tipo : script} {--file= : Fichero a tratar} {start?} {stop?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scripts varios';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tipo = $this->argument('tipo');

        $file = $this->option('file');

        switch ($tipo) {
            case 'demo-byl': {
                    ini_set('memory_limit', '512M');

                    $h = ConfigHelper::getHost();
                    if ($h != "demo-estudiaryviajar-com") {
                        dd($h);
                        exit;
                    }

                    // $cursos_id = [8,10,365,123];
                    $cursos_id = [8, 371, 506, 825, 241];
                    print_r($cursos_id);

                    \VCN\Models\Informes\Venta::select()->delete();
                    \VCN\Models\Informes\VentaCurso::select()->delete();

                    $bookings = \VCN\Models\Bookings\Booking::whereIn('curso_id', $cursos_id);

                    $vids = clone $bookings;
                    $vids = $vids->groupBy('viajero_id')->get()->pluck('viajero_id')->toArray();
                    $uids = \VCN\Models\Leads\Viajero::whereNotIn('id', $vids)->get()->pluck('user_id')->toArray();
                    \VCN\Models\Leads\Viajero::whereNotIn('id', $vids)->delete();
                    \VCN\Models\User::whereIn('id', $uids)->delete();

                    $tids = \VCN\Models\Leads\ViajeroTutor::whereIn('viajero_id', $vids)->pluck('viajero_id')->toArray();
                    \VCN\Models\Leads\Tutor::whereNotIn('id', $tids)->delete();
                    \VCN\Models\Leads\ViajeroTutor::whereNotIn('viajero_id', $vids)->delete();

                    $pids = clone $bookings;
                    $pids = $pids->groupBy('proveedor_id')->pluck('proveedor_id')->toArray();
                    \VCN\Models\Proveedores\Proveedor::whereNotIn('id', $pids)->delete();

                    $cids = \VCN\Models\Cursos\Curso::whereIn('id', $cursos_id)->groupBy('center_id')->get()->pluck('center_id')->toArray();
                    \VCN\Models\Centros\Centro::whereNotIn('id', $cids)->delete();

                    \VCN\Models\Convocatorias\Abierta::whereNotIn('course_id', $cursos_id)->delete();
                    \VCN\Models\Convocatorias\Cerrada::whereNotIn('course_id', $cursos_id)->delete();
                    \VCN\Models\Convocatorias\ConvocatoriaMulti::select()->delete();

                    $convos = clone $bookings;
                    $convos = $convos->groupBy('convocatory_close_id')->get()->pluck('convocatory_close_id')->toArray();

                    $vuelos = \VCN\Models\Convocatorias\CerradaVuelo::whereIn('convocatory_id', $convos)->get()->pluck('vuelo_id')->toArray();
                    \VCN\Models\Convocatorias\CerradaVuelo::whereNotIn('convocatory_id', $convos)->delete();
                    \VCN\Models\Convocatorias\Vuelo::whereNotIn('id', $vuelos)->delete();


                    \VCN\Models\Cursos\Curso::whereNotIn('id', $cursos_id)->delete();
                    \VCN\Models\Bookings\Booking::whereNotIn('curso_id', $cursos_id)->delete();

                    foreach (\VCN\Models\Bookings\Booking::all() as $b) {
                        $v = $b->viajero;

                        foreach ($v->tutores as $t) {
                            if ($t) {
                                $t->name = "Tutor $t->id";
                                $t->lastname = "Apes $t->id";
                                $t->email = "viajero_$t->id@faker.org";
                                $t->phone = "ph$t->id";
                                $t->movil = "m555$t->id";
                                $t->empresa = "e$t->id";
                                $t->empresa_phone = "e555$t->id";
                                $t->save();
                            }
                        }

                        $v->name = "Viajero $v->id";
                        $v->lastname = "Ape $v->id";
                        $v->lastname2 = "Ape2 $v->id";
                        $v->email = "viajero_$v->id@faker.org";
                        $v->phone = "ph$v->id";
                        $v->movil = "m555$v->id";
                        $v->save();
                    }

                    $this->info("byl");
                }
                break;

            case 'booking-pdf': {
                    $id = $this->argument('start');
                    $this->info("Booking $id pdf.");
                    $b = \VCN\Models\Bookings\Booking::find($id);

                    \App::setLocale($b->idioma_contacto);

                    if ($b) {
                        if ($b->pdf_generar()) {
                            $this->info("Booking $id pdf generado.");
                        } else {
                            $this->info("Booking $id pdf NO generado.");
                        }
                    } else {
                        $this->info("Booking $id no existe.");
                    }
                }
                break;

            case 'sessions': {
                    DB::table('users')->update(['remember_token' => null]);

                    $this->info("Sessions reset.");
                }
                break;

            case 'scripts': {
                    $this->scripts();
                }
                break;

            case 'dataweb': {
                
                ini_set('memory_limit', '400M');

                $p = $this->argument('start');
                ConfigHelper::commandPlataforma($p);

                $cursos = \VCN\Models\Cursos\Curso::all();
                // $cids = [982,10,25];
                // $cursos = \VCN\Models\Cursos\Curso::whereIn('id', $cids);
                
                $t = 0;
                $j = 0;
                $total = $cursos->count();
                $limit =  ($total > 100) ? 100 : $total;

                $this->info("cursos total: $total");

                for($i=0; $i<$total; $i = $i+$limit)
                {
                    $cursos = \VCN\Models\Cursos\Curso::offset($j)->limit($limit);
                    $j += $limit;

                    $this->info("cursos: $i / $j");
                    foreach($cursos->get() as $curso)
                    {
                        $curso->updateDataWeb(); 
                        $t++;
                    }
                }
                
                $this->info("dataweb: $t");
                return;

            }
            break;

            case 'dataweb-plazas': {
                
                $cursos = \VCN\Models\Cursos\Curso::all();
                $total = $cursos->count();
                $this->info("cursos total: $total");
                $i = 0;
                foreach($cursos as $curso)
                {
                    $curso->updateDataWebPlazas();
                    $i++;
                }

                $this->info("dataweb: $i");
                return;

            }
            break;

            case 'emails': {
                    $this->emailsClean();
                }
                break;

            case 'facturas': {
                    $this->scriptsFacturas();
                }
                break;

            case 'thumbs': {
                    $this->scriptsThumbs();
                }
                break;

            case 'scripts-log': {
                    $this->scriptsLog();
                }
                break;

            case 'area': {
                    $this->area();
                }
                break;

            case 'area-mails': {
                    $this->areaMails();
                }
                break;

            case 'ventas': {
                    $this->importarVentas();
                }
                break;

            case 'leads': {
                    $this->importarLeads();
                }
                break;

            case 'bs': {
                    $this->importarBS();
                }
                break;

            case 'bs-clientes-pre': {
                    $this->importarBS_clientes_pre();
                }
                break;

            case 'bs-clientes': {
                    $desde = $this->argument('start');
                    $hasta = $this->argument('stop');

                    $this->importarBS_clientes($desde, $hasta);
                }
                break;

            case 'bs-cic': {
                    $desde = $this->argument('start');
                    $hasta = $this->argument('stop');

                    $this->importarBS_bscic($desde, $hasta);
                }
                break;

            case 'importar-sf': {
                    $this->importarSF();
                }
                break;

            case 'limpieza-viajeros': {
                    $viajeros = Viajero::where('name', 'LIKE', '%xxx%')->orWhere('name', 'LIKE', '%XXX%')->where('lastname', 'LIKE', '%xxx%')->orWhere('lastname', 'LIKE', '%XXX%');
                    $this->info($viajeros->count());
                    $viajeros->delete();

                    $viajeros = Viajero::where('name', '')->orWhereNull('name');
                    $i = 0;
                    foreach ($viajeros->get() as $v) {
                        if (!$v->tutores->count()) {
                            $v->delete();
                            $i++;
                        }
                    }

                    $this->info($i);

                    // dd($viajeros->get()->where('lastname',null)->pluck('id'));
                }
                break;

            case 'solicitudes': {
                    $st = [];
                    // $st[] = ConfigHelper::config('solicitud_status_lead'); //1
                    // $st[] = ConfigHelper::config('solicitud_status_contactado'); //2
                    // $st[] = ConfigHelper::config('solicitud_status_decidiendo'); //3
                    // $st[] = ConfigHelper::config('solicitud_status_futuro'); //4
                    $st[] = ConfigHelper::config('solicitud_status_archivado'); //5
                    $st[] = ConfigHelper::config('solicitud_status_inscrito'); //6

                    //2018 y anteriores => archivado + "no contesta" 
                    $any = 2018;
                    $fini = "$any-01-01";
                    $ffin = "$any-12-31";
                    $solicitudes = Solicitud::whereNotIn('status_id', $st)->where('fecha', '<=', $ffin);

                    $total = $solicitudes->count();
                    $this->info("$any :: $total");
                    $i = 0;
                    foreach ($solicitudes->get() as $s) {
                        // $s->setStatus($s->status_id, "bug");
                        $s->setStatus(ConfigHelper::config('solicitud_status_archivado'), "NO CONTESTA");
                        $i++;
                    }
                    $this->info("Solicitudes $any [$i]");

                    //2019 != cat. año escolar(2) => archivado + "no contesta"
                    $any = 2019;
                    $fini = "$any-01-01";
                    $ffin = "$any-12-31";
                    $solicitudes = Solicitud::whereNotIn('status_id', $st)
                        ->where('fecha', '>=', $fini)->where('fecha', '<=', $ffin)->where('category_id', '<>', 2);

                    $total = $solicitudes->count();
                    $this->info("$any :: $total");
                    $i = 0;
                    foreach ($solicitudes->get() as $s) {
                        // $s->setStatus($s->status_id, "bug");
                        $s->setStatus(ConfigHelper::config('solicitud_status_archivado'), "NO CONTESTA");
                        $i++;
                    }
                    $this->info("Solicitudes $any [$i]");

                    //bug status
                    $st = [];
                    $st[] = ConfigHelper::config('solicitud_status_lead'); //1
                    $st[] = ConfigHelper::config('solicitud_status_contactado'); //2
                    $st[] = ConfigHelper::config('solicitud_status_decidiendo'); //3
                    $st[] = ConfigHelper::config('solicitud_status_futuro'); //4
                    $st[] = ConfigHelper::config('solicitud_status_archivado'); //5
                    $st[] = ConfigHelper::config('solicitud_status_inscrito'); //6

                    $any = 2019;
                    $fini = "$any-01-01";
                    $ffin = "$any-12-31";
                    $solicitudes = Solicitud::whereIn('status_id', $st)->where('fecha', '>=', $fini)->where('fecha', '<=', $ffin)->where('category_id', 2);

                    $total = $solicitudes->count();
                    $this->info("$any :: $total");
                    $i = 0;
                    foreach ($solicitudes->get() as $s) {
                        if ($s->logs->count()) {
                            continue;
                        }

                        \VCN\Models\Solicitudes\SolicitudLogStatus::addLogBug($s);
                        $i++;
                    }
                    $this->info("Solicitudes $any [$i]");

                    $solicitudes = Solicitud::whereIn('status_id', $st)->where('fecha', '>', $ffin);

                    $total = $solicitudes->count();
                    $this->info("$any :: $total");
                    $i = 0;
                    foreach ($solicitudes->get() as $s) {
                        if ($s->logs->count()) {
                            continue;
                        }

                        \VCN\Models\Solicitudes\SolicitudLogStatus::addLogBug($s);
                        $i++;
                    }
                    $this->info("Solicitudes $any [$i]");
                }
                break;

            case 'bug-pagos': //email de pagos enviado el 18/03/2020
                {
                    ini_set('memory_limit', '512M');

                    $id = $this->option('file');
                    if (!$id) {
                        $this->error("Falta ID Aviso");
                        return;
                    }

                    $aviso = Aviso::find($id);
                    if (!$aviso) {
                        $this->error("Aviso no existe");
                        return;
                    }

                    $tipo = 0;
                    if ($aviso->doc) {
                        $tipo = $aviso->doc->tipo;
                    }

                    $log = new AvisoLog;
                    $log->aviso_id = $aviso->id;
                    $log->notas = "BUG PAGOS";
                    $log->save();

                    $logs = \VCN\Models\Bookings\BookingLog::where('created_at', '>', '2020-03-17')->where('tipo', 'LIKE', 'Aviso Pago [%')->groupBy('booking_id');
                    // $this->info($logs->count());

                    $iTot = 0;
                    $iSend = 0;
                    $arrBookings = [];

                    foreach ($logs->get() as $item) {
                        $booking = $item->booking;
                        if ($booking->yaAviso($aviso->id)) {
                            continue;
                        }
                        // $this->info($booking->id);

                        $iSend += MailHelper::mailAvisoDoc($booking->id, $aviso->id, $tipo);
                        BookingLog::addLog($booking, "Aviso[$aviso->id]", "$aviso->name :: " . $aviso->doc->name);
                        $iTot++;

                        array_push($arrBookings, $booking->id);
                    }

                    $notas = "Total Bookings: $iTot";
                    $log->notas = $notas;
                    $log->bookings = $arrBookings;
                    $log->enviados = $iSend;
                    $log->save();

                    $this->info($log);
                    $this->info($notas);
                }
                break;

            default: {
                    $this->error("excel:importar {tipo} {--file=FICHERO} : Tipos disponibles: leads, bs, bs-clientes, bs-clientes-pre, scripts, ventas");
                }
                break;
        }

        if (!$file) {
            // $this->error("Falta fichero (debe estar en '/public/excel/')");
        }
    }

    private static function Actual_pais($id)
    {
        switch ($id) {
            case 1:
                return 7;
            case 2:
                return 6;
            case 6:
                return 4;
            case 8:
                return 2;
            case 9:
                return 5;
            case 10:
                return 3;
            case 13:
                return 8;
            case 14:
                return 11;
            case 15:
                return 1;
            case 18:
                return 12;
            case 19:
                return 10;
        }

        return $id + 12;
    }

    private static function Actual_origen($id)
    {
        switch ($id) {
            case 8:
                return 12;
            case 12:
                return 10;
            case 13:
                return 5;
            case 17:
                return 13;
            case 20:
                return 7;
            case 26:
                return 1;
            case 31:
                return 4;
            case 72:
                return 9;
            case 78:
                return 3;
        }

        return $id + 15;
    }

    private static function Actual_oficina($id)
    {
        switch ($id) {
            case 2:
                return 1;
            case 12:
                return 3;
            case 13:
                return 4;
            case 4:
                return 5;
        }

        return $id + 5;
    }


    private static function Actual_catPrescriptor($id)
    {
        if ($id == 1) {
            return 1;
        }

        if ($id > 2) {
            return $id - 1;
        }

        return $id + 12;
    }

    private static function Actual_nacionalidad($n)
    {
        if ($n == "SPANISH") {
            $n = "ESPAÑOLA";
        }

        if ($n == "ESPAÑOLO") {
            $n = "ESPAÑOLA";
        }

        if ($n == "ESPANYOLA") {
            $n = "ESPAÑOLA";
        }

        if ($n == "ESPANYOL") {
            $n = "ESPAÑOLA";
        }

        if ($n == "ESPAÑA") {
            $n = "ESPAÑOLA";
        }

        if ($n == "E") {
            $n = "ESPAÑOLA";
        }

        if ($n == "AMERICANO") {
            $n = "AMERICANA";
        }

        if ($n == "CATALANA") {
            $n = "ESPAÑOLA";
        }

        $nac = Nacionalidad::where('name', 'LIKE', "$n%")->first();

        if ($nac) {
            return $nac->name;
        }

        return null;
    }

    private static function Actual_paisnac($n)
    {
        if ($n == "ESPANYA") {
            $n = "ESPAÑA";
        }

        if ($n == "E") {
            $n = "ESPAÑA";
        }

        if ($n == "ESP") {
            $n = "ESPAÑA";
        }

        $p = Pais::where('name', 'LIKE', "$n%")->first();

        if ($p) {
            return $p->name;
        }

        return $n;
    }

    private function importarBS_bscic($d, $h)
    {
        // ini_set('auto_detect_line_endings', true);

        $file = "public/excel/" . $this->option('file');

        $tt = Carbon::now();
        global $tot;
        global $totRepe;

        $tot = 0;
        $totRepe = 0;

        //Total 9x3000
        if ($h > 9) {
            $h = 9;
        }

        for ($i = $d; $i <= $h; $i++) {
            $file = "public/excel/bs_clientes-$i.csv";

            $now = Carbon::now();
            $this->info("\nClientes $i: " . $now);
            Excel::load($file, function ($reader) use ($now) {

                $results = $reader->get();

                $c = count($results);
                $GLOBALS['tot'] += $c;

                $bar = $this->output->createProgressBar($c);

                $diff = Carbon::now()->diffForHumans($now);
                $this->info(" $diff s.");

                $tr = 0;

                foreach ($results as $row) {
                    $vrep = Viajero::where('name', $row->nom)->where('lastname', $row->apellido1)->where('lastname2', $row->apellido2)->first();
                    if ($vrep) {
                        if ($row->idempresa != $vrep->plataforma) {
                            $GLOBALS['totRepe']++;
                            $tr++;

                            // $this->info("\n $row");
                        }
                    }

                    $bar->advance();
                }

                // echo ".";

                $bar->finish();

                $diff = Carbon::now()->diffInSeconds($now);
                $this->info(" $diff s. (R: $tr)");
            }, 'UTF-8');
        }

        $diff = Carbon::now()->diffInSeconds($now);
        $this->info(" $diff s.");

        $this->info("\nFin de la importación de clientes [$tot] ($totRepe) " . Carbon::now());
        $diff = Carbon::now()->diffForHumans($tt);
        $this->info($diff);
    }

    private function importarBS_clientes($d, $h)
    {
        // ini_set('auto_detect_line_endings', true);

        $file = "public/excel/" . $this->option('file');

        $tt = Carbon::now();
        global $tot;
        global $totRepe;

        $tot = 0;
        $totRepe = 0;

        //Total 9x3000
        if ($h > 9) {
            $h = 9;
        }

        for ($i = $d; $i <= $h; $i++) {
            $file = "public/excel/bs_clientes-$i.csv";

            $now = Carbon::now();
            $this->info("\nClientes $i: " . $now);
            Excel::load($file, function ($reader) use ($now) {

                $results = $reader->get();

                $c = count($results);
                $GLOBALS['tot'] += $c;

                $bar = $this->output->createProgressBar($c);

                $diff = Carbon::now()->diffForHumans($now);
                $this->info(" $diff s.");

                $tr = 0;

                foreach ($results as $row) {
                    $idc = $row->idclient + 1000;
                    $vrep = Viajero::find($idc);  //si no está

                    if (!$vrep) {
                        //verificamos que no sea un repetido y ya esté para las 2 plataformas
                        $vrep = Viajero::where('plataforma', $row->idempresa)->where('name', $row->nom)->where('lastname', $row->apellido1)->where('lastname2', $row->apellido2)->first();

                        if (!$vrep) {
                            $GLOBALS['totRepe']++;
                            $tr++;

                            //importamos
                            $v = new Viajero;
                            $v->id = $row->idclient + 1000;
                            if ($row->fechaalta) {
                                $v->created_at = Carbon::createFromFormat('d/m/Y', $row->fechaalta)->format('Y-m-d');
                            }
                            $v->save();


                            $v->user_id = null;
                            $v->asign_to = null;
                            $v->booking_id = 0;
                            $v->solicitud_id = 0;
                            $v->booking_status_id = 0;
                            $v->status_id = 0;
                            //$v->archivado = 1;
                            $v->sexo = $row->sexo == "HOMBRE" ? 1 : 2;

                            $v->origen_id = self::Actual_origen($row->coneixer);
                            $v->suborigen_id = null;
                            $v->suborigendet_id = null;
                            $v->es_adulto = $row->tipus == "Adulto" ? 1 : 0;

                            // $v-> = $row->;
                            $v->oficina_id = self::Actual_oficina($row->idoficina);
                            $v->plataforma = $row->idempresa;
                            $v->name = $row->nom;
                            $v->lastname = $row->apellido1;
                            $v->lastname2 = $row->apellido2;
                            $v->phone = $row->telefon;
                            $v->movil = $row->mobil;

                            if ($row->email == $row->email_pare || $row->email == $row->email_mare) {
                                $v->email = null;
                            } else {
                                $v->email = $row->email == "-" ? null : $row->email;
                            }

                            $v->fechanac = $row->data_naix ? Carbon::createFromFormat('d/m/Y', $row->data_naix)->format('Y-m-d') : null;
                            $v->paisnac = self::Actual_paisnac($row->pais_naix);

                            if ($row->tipus == "Adulto") {
                                $v->emergencia_contacto = $row->contacte_emerg;
                                $v->emergencia_telefono = $row->tel_emerg;
                            }

                            $v->prescriptor_id = $row->idprescriptor > 0 ? $row->idprescriptor : null;
                            $v->save();

                            //ViajeroDatos
                            $vd = new ViajeroDatos;
                            $vd->viajero_id = $v->id;
                            $vd->direccion = $row->direccion;
                            // $vd-> = $row->;
                            $vd->cp = strlen($row->cp) > 4 ? $row->cp : "0" . $row->cp;
                            $vd->ciudad = $row->poblacion;
                            $vd->provincia_id = $row->provincia;
                            $vd->pais_id = self::Actual_pais($row->pais);

                            $vd->nacionalidad = self::Actual_nacionalidad($row->nacionalitat);

                            $vd->idioma_contacto = $row->idiomacontacte;
                            $vd->profesion = $row->professio;
                            $vd->fumador = $row->fumador == "VERDADERO" ? 1 : 0;
                            $vd->empresa = $row->empresa;
                            $vd->empresa_phone = $row->tel_feina;
                            $vd->documento = $row->nif;

                            $vd->h1_nom = $row->g1_nom;
                            $vd->h1_fnac = $row->g1_fnac;
                            $vd->h2_nom = $row->g2_nom;
                            $vd->h2_fnac = $row->g2_fnac;
                            $vd->h3_nom = $row->g3_nom;
                            $vd->h3_fnac = $row->g3_fnac;
                            $vd->h4_nom = $row->g4_nom;
                            $vd->h4_fnac = $row->g4_fnac;

                            $vd->alergias = null;
                            if ($row->malaltiadesc) {
                                $vd->alergias = "Enfermedad: " . $row->malaltiadesc;
                            }
                            if ($row->alergiadesc) {
                                if ($vd->alergias) {
                                    $vd->alergias = $vd->alergias . ", ";
                                }
                                $vd->alergias = "Alergia: " . $row->alergiadesc;
                            }

                            $vd->medicacion = null;
                            if ($row->tractamentdesc) {
                                $vd->medicacion = "Tratamiento: " . $row->tractamentdesc;
                            }
                            if ($row->medicacionespecial) {
                                if ($vd->medicacion) {
                                    $vd->medicacion = $vd->medicacion . ", ";
                                }
                                $vd->medicacion = "Medicación: " . $row->medicacionespecial;
                            }
                            if ($row->dietadesc) {
                                if ($vd->medicacion) {
                                    $vd->medicacion = $vd->medicacion . ", ";
                                }
                                $vd->medicacion = "Dieta: " . $row->dietadesc;
                            }

                            $vd->animales = $row->animalsdesc;
                            $vd->hobby = $row->hobbies;

                            $vd->notas = $row->observaciones;

                            $vd->fact_nif = $row->niffact;
                            $vd->fact_razonsocial = $row->rsocialfact;
                            $vd->fact_domicilio = $row->dsocialfact;
                            $vd->fact_cp = $row->cpfact;
                            $vd->fact_poblacion = $row->pobfact;
                            $vd->fact_concepto = $row->concepto;

                            $vd->save();

                            //Tutor Padre:0, madre:1
                            if ($row->nom_pare && $row->nom_pare != "-") {
                                $t = new Tutor;
                                $t->name = $row->nom_pare;
                                $t->email = $row->email_pare;
                                $t->movil = $row->mobil_pare;
                                $t->empresa_phone = $row->tel_feina_pare;
                                $t->profesion = $row->prof_pare;
                                $t->user_id = 0;
                                $t->emergencia_contacto = $row->contacte_emerg;
                                $t->emergencia_telefono = $row->tel_emerg;
                                $t->plataforma = $row->idempresa;
                                $t->save();

                                switch ($row->parentesco_t1) {
                                    case 'MADRE': {
                                            $par = 1;
                                        }
                                        break;

                                    case 'PADRE': {
                                            $par = 0;
                                        }
                                        break;

                                    default: {
                                            $par = 0;
                                        }
                                        break;
                                }

                                //ViajeroTutor
                                $vt = new ViajeroTutor;
                                $vt->tutor_id = $t->id;
                                $vt->viajero_id = $v->id;
                                $vt->relacion = $par;
                                $vt->save();
                            }

                            if ($row->nom_mare && $row->nom_mare != "-") {
                                $t = new Tutor;
                                $t->name = $row->nom_mare;
                                $t->email = $row->email_mare;
                                $t->movil = $row->mobil_mare;
                                $t->empresa_phone = $row->tel_feina_mare;
                                $t->profesion = $row->prof_mare;
                                $t->user_id = 0;
                                $t->emergencia_contacto = $row->contacte_emerg;
                                $t->emergencia_telefono = $row->tel_emerg;
                                $t->plataforma = $row->idempresa;
                                $t->save();

                                switch ($row->parentesco_t2) {
                                    case 'MADRE': {
                                            $par = 1;
                                        }
                                        break;

                                    case 'PADRE': {
                                            $par = 0;
                                        }
                                        break;

                                    default: {
                                            $par = 1;
                                        }
                                        break;
                                }

                                //ViajeroTutor
                                $vt = new ViajeroTutor;
                                $vt->tutor_id = $t->id;
                                $vt->viajero_id = $v->id;
                                $vt->relacion = $par;
                                $vt->save();
                            }
                        }
                    }

                    $bar->advance();
                }

                // echo ".";

                $bar->finish();

                $diff = Carbon::now()->diffInSeconds($now);
                $this->info(" $diff s. (R: $tr)");
            }, 'UTF-8');
        }

        $diff = Carbon::now()->diffInSeconds($now);
        $this->info(" $diff s.");

        $this->info("\nFin de la importación de clientes [$tot] ($totRepe) " . Carbon::now());
        $diff = Carbon::now()->diffForHumans($tt);
        $this->info($diff);
    }

    private function importarBS()
    {
        ini_set('auto_detect_line_endings', true);

        $file = "public/excel/" . $this->option('file');

        $tt = Carbon::now();

        // Paises
        $t = Carbon::now();
        $this->info("\nPaises: " . $t);
        Excel::selectSheets('Paises')->load($file, function ($reader) use ($t) {

            $results = $reader->get();
            $bar = $this->output->createProgressBar(count($results));

            $diff = Carbon::now()->diffForHumans($t);
            $this->info($diff);

            foreach ($results as $row) {
                if (!$row->idactual) {
                    //importar
                    $p = new Pais;
                    $p->id = $row->idpais + 12;
                    $p->name = $row->pais;
                    $p->save();
                }

                $bar->advance();
            }

            $bar->finish();
        });

        $diff = Carbon::now()->diffInSeconds($t);
        $this->info(" $diff s.");


        // Provincias
        $t = Carbon::now();
        $this->info("\nProvincias: " . $t);
        Excel::selectSheets('Provincias')->load($file, function ($reader) use ($t) {

            $results = $reader->get();
            $bar = $this->output->createProgressBar(count($results));

            $diff = Carbon::now()->diffForHumans($t);
            $this->info($diff);

            foreach ($results as $row) {
                //importar
                $p = new Provincia;
                $p->id = $row->idprov;
                $p->name = $row->provincia;
                $p->pais_id = self::Actual_pais($row->idpais);
                $p->save();

                $bar->advance();
            }

            $bar->finish();
        });

        $diff = Carbon::now()->diffInSeconds($t);
        $this->info(" $diff s.");


        //Conociste
        $t = Carbon::now();
        $this->info("\nOrigen Leads: " . $t);
        Excel::selectSheets('Conociste')->load($file, function ($reader) use ($t) {

            $results = $reader->get();
            $bar = $this->output->createProgressBar(count($results));

            $diff = Carbon::now()->diffForHumans($t);
            $this->info($diff);

            foreach ($results as $row) {
                if (!$row->idactual) {
                    //importar
                    $p = new Origen;
                    $p->id = $row->idconociste + 15;
                    $p->name = $row->descripcion;
                    $p->propietario = $row->idempresa;
                    $p->save();
                } else {
                    $p = Origen::find($row->idactual);
                    $p->propietario = 1;
                    $p->save();
                }

                $bar->advance();
            }

            $bar->finish();
        });

        $diff = Carbon::now()->diffInSeconds($t);
        $this->info(" $diff s.");


        //CategoriaPrescriptor
        $t = Carbon::now();
        $this->info("\nCategoria Prescriptor: " . $t);
        Excel::selectSheets('CategoriaPrescriptor')->load($file, function ($reader) use ($t) {

            $results = $reader->get();
            $bar = $this->output->createProgressBar(count($results));

            $diff = Carbon::now()->diffForHumans($t);
            $this->info($diff);

            foreach ($results as $row) {
                if (!$row->idactual) {
                    //importar
                    $p = new CategoriaPrescriptor;
                    $p->id = $row->idcatprescriptor + 12;
                    $p->name = $row->desccatprescriptor;
                    $p->propietario = $row->idempresa;
                    $p->save();
                } else {
                    $p = CategoriaPrescriptor::find($row->idactual);
                    $p->propietario = 1;
                    $p->save();
                }

                $bar->advance();
            }

            $bar->finish();
        });

        $diff = Carbon::now()->diffInSeconds($t);
        $this->info(" $diff s.");


        //Oficinas
        $t = Carbon::now();
        $this->info("\nOficinas: " . $t);
        Excel::selectSheets('Oficinas')->load($file, function ($reader) use ($t) {

            $results = $reader->get();
            $bar = $this->output->createProgressBar(count($results));

            $diff = Carbon::now()->diffForHumans($t);
            $this->info($diff);

            foreach ($results as $row) {
                // //importar
                if (!$row->idactual) {
                    $p = new Oficina;
                    $p->id = $row->idoficina + 5;

                    $p->name = $row->nombre;
                    $p->created_at = Carbon::parse($row->fechaalta)->format('Y-m-d');
                } else {
                    $p = Oficina::find($row->idactual);
                }

                $p->propietario = $row->idempresa;
                $p->direccion = $row->direccion;
                $p->cp = $row->cp;
                $p->poblacion = $row->poblacion;
                $p->persona = $row->personacontacto;
                $p->telefono = $row->telefon;
                $p->email = $row->email;
                $p->provincia_id = $row->idprov;
                $p->pais_id = self::Actual_pais($row->pais);
                $p->iban = $row->cuenta;
                $p->save();

                $bar->advance();
            }

            $bar->finish();
        });

        $diff = Carbon::now()->diffInSeconds($t);
        $this->info(" $diff s.");


        //Prescriptores
        $t = Carbon::now();
        $this->info("\nPrescriptores: " . $t);
        Excel::selectSheets('Prescriptores')->load($file, function ($reader) use ($t) {

            $results = $reader->get();
            $bar = $this->output->createProgressBar(count($results));

            $diff = Carbon::now()->diffForHumans($t);
            $this->info($diff);

            foreach ($results as $row) {
                //importar
                $p = new Prescriptor;
                $p->id = $row->idprescriptor;
                $p->name = $row->nombre;
                $p->sucursal = $row->sucursal;
                $p->direccion = $row->direccion;
                $p->cp = $row->cp;
                $p->poblacion = $row->poblacion;
                $p->idioma = $row->idiomacomunicacion;
                $p->persona = $row->personacontacto;
                $p->telefono1 = $row->telefono;
                $p->telefono2 = $row->telefono2;
                $p->email = $row->email;
                $p->notas = $row->observaciones;
                $p->nif = $row->nif;
                $p->razon_social = $row->razonsocial;
                $p->domicilio_social = $row->domiciliosocial;
                $p->categoria_id = self::Actual_catPrescriptor($row->idcatprescriptor);
                $p->oficina_id = self::Actual_oficina($row->idoficina);
                $p->pais_id = self::Actual_pais($row->pais);
                $p->provincia_id = $row->provincia;
                $p->propietario = $row->idempresa;
                $p->created_at = Carbon::parse($row->fechaalta)->format('Y-m-d');
                $p->save();

                $bar->advance();
            }

            $bar->finish();
        });

        $diff = Carbon::now()->diffInSeconds($t);
        $this->info(" $diff s.");

        $this->info("\nFin de la importación " . Carbon::now());
        $diff = Carbon::now()->diffForHumans($tt);
        $this->info($diff);
    }


    private function importarBS_clientes_pre()
    {

        $tt = Carbon::now();

        //foreach viajeros : hermanos => h1_nom
        //foreach viajero_datos : pais_id, provincia_id

        foreach (ViajeroDatos::where('hermanos', '<>', "")->get() as $v) {
            $v->h1_nom = $v->hermanos;
            $v->save();
        }

        foreach (ViajeroDatos::all() as $v) {
            $p = Pais::where('name', $v->pais)->first();
            $prov = Provincia::where('name', $v->provincia)->first();
            $v->pais_id = $p ? $p->id : 0;
            $v->provincia_id = $prov ? $prov->id : 0;
            if ($v->idioma_contacto == 'es') {
                $v->idioma_contacto = "ES";
            }
            if ($v->idioma_contacto == 'ca') {
                $v->idioma_contacto = "CA";
            }
            $v->save();
        }

        // Schema::table('viajero_datos', function (Blueprint $table) {
        //     $table->dropColumn('provincia');
        //     $table->dropColumn('pais');
        // });

        $this->info("\nFin Viajeros pre." . Carbon::now());
        $diff = Carbon::now()->diffForHumans($tt);
        $this->info($diff);
    }

    private function importarSF()
    {
        // ini_set('auto_detect_line_endings', true);

        // 20161002_Leads_EduExpo.xlsx
        $file = "storage/excel/" . $this->option('file');
        // dd($file);

        $tt = Carbon::now();
        global $tot;
        global $totRepe;

        $tot = 0;
        $totRepe = 0;

        // foreach(Solicitud::all() as $solicitud)
        // {
        //     if(!$solicitud->fecha)
        //     {
        //         $solicitud->fecha = $solicitud->viajero->created_at;
        //         $solicitud->save();
        //         $tot++;
        //     }
        // }
        // $this->info("\nSolicitudes: " . $tot);

        // exit;

        $now = Carbon::now();
        $this->info("\nClientes: " . $now);
        Excel::load($file, function ($reader) use ($now, $tot) {

            $results = $reader->get();

            $c = count($results);
            $GLOBALS['tot'] += $c;

            $bar = $this->output->createProgressBar($c);

            $diff = Carbon::now()->diffForHumans($now);
            $this->info(" $diff s.");

            $tr = 0;

            $plat = 1;

            foreach ($results as $row) {
                // dd($row);
                $v = new Viajero;
                $v->plataforma = $plat;
                $v->status_id = 1;
                $v->name = $row->nombre;
                $v->lastname = $row->apellido1;
                $v->lastname2 = $row->apellido2 ? $row->apellido2 : "";
                $v->sexo = $row->sexo ?: 1;
                $v->email = $row->email;
                $v->fechanac = $row->fechanac ? $row->fechanac->format('Y-m-d') : null;
                $v->movil = $row->movil;
                $v->origen_id = (int) $row->origen_id;
                $v->suborigen_id = (int) $row->suborigen_id;
                $v->suborigendet_id = (int) $row->suborigendet_id;
                $v->rating = $row->rating ?: 0;
                $v->asign_to = (int) $row->asign_to;
                $v->paisnac = $row->pais;
                $v->oficina_id = (int) $row->oficina_id;
                $v->save();

                // $v->created_at = $row->created_at;
                // $v->save();

                $pais_id = Pais::where('name', (int) $row->pais)->first();

                $vd = new ViajeroDatos;
                $vd->viajero_id = $v->id;
                $vd->ciudad = $row->ciudad;
                $vd->pais_id = $pais_id ?: null;
                $vd->save();

                $s = new Solicitud;
                $s->viajero_id = $v->id;
                $s->plataforma = $plat;
                $s->status_id = 1;
                $s->user_id = $row->asign_to;
                $s->rating = $row->rating ?: 0;
                $s->notas = $row->notas;
                $s->fecha = $v->created_at;
                // $s->category_id = (int)$row->category_id;
                // $s->subcategory_id = (int)$row->subcategory_id;
                $s->destinos = $row->destino;
                $s->save();

                $v->solicitud_id = $s->id;
                $v->save();


                $tot++;

                $bar->advance();
            }

            // echo ".";

            $bar->finish();
        }, 'UTF-8');

        $diff = Carbon::now()->diffInSeconds($now);
        $this->info(" $diff s.");

        $this->info("\nFin de la importación de clientes [$tot] ($totRepe) " . Carbon::now());
        $diff = Carbon::now()->diffForHumans($tt);
        $this->info($diff);
    }



    private function importarLeads()
    {
        $file = "storage/excel/" . $this->option('file');

        Excel::selectSheetsByIndex(0)->load($file, function ($reader) {

            $iTot = 0;
            // $reader->limit(10);

            $bar = $this->output->createProgressBar(count($reader->get()));

            foreach ($reader->get() as $row) {
                if (!$row->nombre)
                    break;

                $v = new Viajero;

                $name = explode(' ', $row->nombre);
                if (count($name) > 3) {
                    $v->name = $name[0] . " " . $name[1];
                    $v->lastname = $name[2];
                    $v->lastname2 = $name[3];
                } elseif (count($name) == 3) {
                    $v->name = $name[0];
                    $v->lastname = $name[1];
                    $v->lastname2 = $name[2];
                } elseif (count($name) == 2) {
                    $v->name = $name[0];
                    $v->lastname = $name[1];
                } else {
                    $v->name = $name[0];
                }

                $v->asign_to = $row->asign_to;
                $v->phone = $row->telefono;
                $v->email = $row->email;

                $v->user_id = 0;
                $v->status_id = $row->status_id;

                $v->origen_id = $row->solicitud_origen_id;
                $v->suborigen_id = $row->solicitud_suborigen_id;
                $v->suborigendet_id = $row->solicitud_suborigen_det_id;

                $v->save();

                //ViajeroDatos
                $vd = new ViajeroDatos;
                $vd->viajero_id = $v->id;
                $vd->ciudad = $row->ciudad;
                $vd->save();

                //Tarea
                $t = new ViajeroTarea;
                $t->viajero_id = $v->id;
                $t->fecha = $row->tarea_fecha;
                $t->tipo = $row->tarea_accion;
                $t->save();

                //Solicitud
                $s = new Solicitud;
                $s->viajero_id = $v->id;
                $s->plataforma = $v->plataforma;
                $s->fecha = $row->solicitud_fecha;
                $s->destinos = $row->pais;
                $s->category_id = $row->categoria_id;
                $s->user_id = $row->asign_to;
                $s->status_id = $row->status_id;
                $s->notas = $row->notas;
                $s->save();

                $v->solicitud_id = $s->id;
                $v->save();

                $iTot++;
                $bar->advance();
            }

            $bar->finish();
            $this->info("\nTotal: $iTot");
        });


        $this->info("\nFin de la importación");
    }

    public function importarVentas()
    {
        $file = "public/excel/ventas.xls";
        Excel::load($file, function ($reader) {

            foreach ($reader->get() as $row) {
                $this->info($row);
            }
        });
    }

    public function scriptsThumbs()
    {
        ini_set('memory_limit', '400M');

        $i = 0;
        $dir = public_path("assets/uploads/course/");
        foreach (\VCN\Models\Cursos\Curso::all() as $im) {
            $path = $dir . $im->course_images . "/";

            if (is_dir($path)) {
                $results = scandir($path);
                foreach ($results as $file) {
                    if ($file === '.' or $file === '..' or $file[0] === '.') continue;

                    if (!file_exists($path . "thumb/" . $file)) {
                        if (!is_dir($path . $file)) {
                            File::copy($path . $file, $path . "thumb/" . $file);
                            $img2 = Image::make($path . "thumb/" . $file);
                            $img2->resize(450, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save();

                            $i++;
                        }
                    } else {
                        $this->info($im->id);
                        $this->info($path . "thumb/" . $file);
                    }
                }
            }
        }
        $this->info("imagenes curso: " . $i);

        $i = 0;
        $dir = public_path("assets/uploads/alojamiento/");
        foreach (\VCN\Models\Alojamientos\Alojamiento::all() as $im) {
            $path = $dir . $im->image_dir . "/";

            if (is_dir($path)) {
                $results = scandir($path);
                foreach ($results as $file) {
                    if ($file === '.' or $file === '..' or $file[0] === '.') continue;

                    if (!file_exists($path . "thumb/" . $file)) {
                        if (!is_dir($path . $file)) {
                            File::copy($path . $file, $path . "thumb/" . $file);
                            $img2 = Image::make($path . "thumb/" . $file);
                            $img2->resize(450, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save();

                            $i++;
                        } else {
                            $this->info($im->id);
                            $this->info($path . "thumb/" . $file);
                        }
                    }
                }
            }
        }
        $this->info("imagenes alojamiento: " . $i);
    }

    public function scriptsFacturas()
    {
        return;
        ini_set('memory_limit', '400M');

        $i = 0;
        foreach (\VCN\Models\Bookings\BookingFactura::where('manual', 0)->where('numero', 'LIKE', '2017%')->get() as $factura) {
            $n = ConfigHelper::facturaNumero(2016, 1);
            $factura->numero = $n;

            $factura->fecha = '2016-12-30';

            $factura->save();

            $factura->pdf();

            $i++;
        }
        $this->info("Facturas 2017 => 2016: $i");

        $j = \VCN\Models\Bookings\BookingFactura::where('manual', 0)->where('fecha', '>', '2016-12-31')->count();
        $i = 0;
        foreach (\VCN\Models\Bookings\BookingFactura::where('manual', 0)->where('fecha', '>', '2016-12-31')->get() as $factura) {
            $factura->fecha = '2016-12-30';
            $factura->save();

            $factura->pdf();

            $i++;
        }
        $this->info("Facturas 2016(2017) => 2016: $i de $j");
    }

    /**
     *
     */
    public function scripts()
    {
        ini_set('memory_limit', '400M');
        set_time_limit(50000);

        //resize images: cursos, centros, alojamientos, schools, familias... ???

        exit;

        // curso 976
        // centro 516 
        // alojamiento 1005  (accommodation_id)
        // => 
        // centro 127 
        // alojamiento 157
        
        // convo cerrada 2510
        
        $curso = \VCN\Models\Cursos\Curso::find(976);
        if($curso)
        {
            $curso->center_id = 127;
            $curso->course_accommodation = 157;
            $curso->save();
        }

        $bookings = Booking::where('convocatory_close_id', 2510);
        $this->info($bookings->count());
        foreach($bookings->get() as $b)
        {
            $b->accommodation_id = 157;
            $b->save();
        }

        $i = 0;
        foreach( \VCN\Models\Cursos\Curso::all() as $c)
        {
            if(!$c->dataweb)
            {
                $c->updateDataWeb();
                $i++;
            }
        }
        $this->info($i);

        exit;
        //pdfs visibles
        // ->pdf_booking_last
        $bookings = Booking::where('course_start_date', '>', '2019-12-31');
        $i = 0;
        $j = 0;
        $k = 0;
        $z = 0;

        foreach ($bookings->get() as $booking) {

            $pubpath = "/files/bookings/" . $booking->viajero_id . "/";
            $path = storage_path($pubpath);

            $bPdf = 0;
            $i++;
            $nomPdf = "";

            if (is_dir($path)) {
                $results = scandir($path);
                //$files = File::allFiles($path);
                foreach ($results as $result) {
                    if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0] === '.') continue;

                    $file = $path . '/' . $result;

                    $fbooking = "ooking_" . $booking->id . "-";
                    $href = $pubpath . $result;

                    if (is_file($file)) {
                        if (strpos($result, $fbooking)) {

                            $bPdf = 1;
                            $nomPdf = $result;

                            $pdf = $booking->getArchivoFirma($result);
                            if ($pdf) {
                                $bPdf = 2;
                            } else {
                                $z++;
                            }
                        }
                    }
                }
            }

            if ($bPdf == 1) {

                if (!$booking->pdf_booking_last) {
                    $this->info("$booking->id,");
                    \VCN\Models\Leads\ViajeroArchivo::addBooking($booking, $nomPdf);
                    $j++;
                }
            } elseif ($bPdf == 2) {
                if ($k == 0) {
                    $this->info("booking 2: $booking->id [$nomPdf]");
                }
                $k++;
            }
        }

        $this->info("Total $i/$j/$k/$z PDFs sin Registro");
        exit;

        $i = 0;
        $j = 0;
        $k = 0;
        $z = 0;

        foreach ($bookings->get() as $booking) {

            foreach ($booking->pdfs as $p) {
                $p->update(['visible' => 0]);
                $j++;
            }

            $p = $booking->pdfs_firmas_last;
            if ($p) {
                $p->update(['visible' => 1]);
            } else {
                $p = $booking->pdf_booking_last;
                if ($p) {
                    $p->update(['visible' => 1]);
                }
            }

            if (!$p) {
                $k++;
            }

            $i++;
        }
        $this->info("Total $i/$j ($k / $z) PDFs");

        exit;
        //SF: Natalia: 469 : Tarea->user_id =? 469 => Viajero->user_id
        $tareas = ViajeroTarea::where('user_id', 469)->where('estado', 0)->get();
        $i = 0;
        foreach ($tareas as $t) {
            $v = $t->viajero;
            if ($v->asign_to != 469) {
                $t->asign_to = $v->asign_to;
                $t->save();
                $i++;
            }
        }
        $this->info("Total $i Tareas reasignadas");

        exit;

        $this->info("Procesando...");
        $ret = \VCN\Models\Informes\Venta::procesarBookings();
        $this->info("Total $ret bookings procesados");

        exit;
        /*$bookings = Booking::where('suborigen_id', 235);
        $this->info("Bookings so:235: ". $bookings->count());
        foreach($bookings->get() as $b)
        {
            $b->suborigendet_id = 165;
            $b->save();
        }

        $solicitudes = Solicitud::where('suborigen_id', 235);
        $this->info("Solicitudes so:235: ". $solicitudes->count());
        foreach($solicitudes->get() as $b)
        {
            $b->suborigendet_id = 165;
            $b->save();
        }

        $bookings = Booking::where('suborigen_id', 243);
        $this->info("Bookings so:243: ". $bookings->count());
        foreach($bookings->get() as $b)
        {
            $b->suborigen_id = 235;
            $b->suborigendet_id = 163;
            $b->save();
        }

        $solicitudes = Solicitud::where('suborigen_id', 243);
        $this->info("Solicitudes so:243: ". $solicitudes->count());
        foreach($bookings->get() as $b)
        {
            $b->suborigen_id = 235;
            $b->suborigendet_id = 163;
            $b->save();
        }

        $bookings = Booking::where('suborigen_id', 245);
        $this->info("Bookings so:245: ". $bookings->count());
        foreach($bookings->get() as $b)
        {
            $b->suborigen_id = 235;
            $b->suborigendet_id = 164;
            $b->save();
        }

        $solicitudes = Solicitud::where('suborigen_id', 245);
        $this->info("Solicitudes so:245: ". $solicitudes->count());
        foreach($solicitudes->get() as $b)
        {
            $b->suborigen_id = 235;
            $b->suborigendet_id = 164;
            $b->save();
        }

        //Viajeros
        $viajeros = Viajero::where('suborigen_id', 235);
        $this->info("Viajeros so:235: ". $viajeros->count());
        foreach($viajeros->get() as $b)
        {
            $b->suborigendet_id = 165;
            $b->save();
        }

        $viajeros = Viajero::where('suborigen_id', 243);
        $this->info("Viajeros so:243: ". $viajeros->count());
        foreach($viajeros->get() as $b)
        {
            $b->suborigen_id = 235;
            $b->suborigendet_id = 163;
            $b->save();
        }

        $viajeros = Viajero::where('suborigen_id', 245);
        $this->info("Viajeros so:245: ". $viajeros->count());
        foreach($viajeros->get() as $b)
        {
            $b->suborigen_id = 235;
            $b->suborigendet_id = 164;
            $b->save();
        }
        */

        $this->info("Procesando 2015");
        $ret = \VCN\Models\Informes\Venta::procesarBookings(2015);
        $this->info("Total $ret bookings procesados");

        $this->info("Procesando 2016");
        $ret = \VCN\Models\Informes\Venta::procesarBookings(2016);
        $this->info("Total $ret bookings procesados");

        $this->info("Procesando 2017");
        $ret = \VCN\Models\Informes\Venta::procesarBookings(2017);
        $this->info("Total $ret bookings procesados");

        $this->info("Procesando 2018");
        $ret = \VCN\Models\Informes\Venta::procesarBookings(2018);
        $this->info("Total $ret bookings procesados");

        $this->info("Procesando 2019");
        $ret = \VCN\Models\Informes\Venta::procesarBookings(2019);
        $this->info("Total $ret bookings procesados");

        exit;


        $tareas = ViajeroTarea::where('user_id', 354)->where('estado', 0)->get();
        foreach ($tareas as $t) {
            $t->delete();
        }


        $i = 0;
        $viajeros = Viajero::where('asign_to', 354)->get();
        foreach ($viajeros as $v) {
            foreach ($v->solicitudes as $s) {
                $bDescarta = false;
                $st = $s->status_id;
                if ($st == 1 || $st == 3 || $st == 4 || $st == 5) {
                    $s->status_id = 2;
                    $s->archivar_motivo = "No respondió";
                    $s->save();
                    $bDescarta = true;
                }

                if ($bDescarta == true) {
                    $v->status_id = 2;
                    $v->save();
                    $i++;
                }
            }
        }

        $this->info("Archivados $i");

        exit;

        $tareas = ViajeroTarea::whereNull('asign_to')->where('fecha', '>', '2018-01-01');
        $this->info("Tareas null: " . $tareas->count());
        $i = 0;
        $v = "";
        foreach ($tareas->get() as $t) {
            $t->asign_to = $t->viajero->asign_to;
            $t->save();
            $i++;
            $v .= $t->viajero_id . ",";
        }

        $this->info("Tareas: $i");
        $this->info($v);
        exit;

        $fields = ['alergias2', 'enfermedad2', 'tratamiento2', 'dieta2', 'medicacion2'];
        foreach ($fields as $field) {
            $logs = \VCN\Models\Leads\ViajeroLog::where('notas', 'LIKE', "%$field:%");
            $list1 = "";
            $list2 = "";
            $i = 0;
            foreach ($logs->get() as $l) {
                $vid = $l->viajero_id;
                $viajero = Viajero::find($vid);

                $p = $viajero->plataforma;
                if ($p == 1) {
                    $list1 .= $vid . ",";
                }

                if ($p == 2) {
                    $list2 .= $vid . ",";
                }

                $notas = $l->notas;
                $p1 = strpos($notas, $field . ':') + strlen($field) + 2;
                $p2 = strpos($notas, "=> ,", $p1) - $p1 - 1;
                $notas2 = substr($notas, $p1, $p2);

                /*
                $field0 = $viajero->datos->$field;

                $viajero->datos->$field = $notas2;
                $viajero->datos->save();

                $txt = "Recuperada info [$field -> $notas2]($field0)";
                \VCN\Models\Leads\ViajeroLog::addLog($viajero, $txt);
                */

                foreach ($viajero->bookings as $b) {
                    $field0 = $b->datos->$field;

                    $b->datos->$field = $notas2;
                    $b->datos->save();

                    $txt = "Recuperada info [$field -> $notas2]($field0)";
                    \VCN\Models\Bookings\BookingLog::addLog($b, $txt);
                }

                $i++;
            }

            $this->info("Total [$field]: $i");
            $this->info("BS:");
            $this->info($list1);
            $this->info("CIC:");
            $this->info($list2);
        }

        exit;

        $bookings = \VCN\Models\Bookings\Booking::where('created_at', '>=', '2019-01-01');
        $i = 0;
        foreach ($bookings->get() as $b) {
            $b->precio_total;
            $i++;
        }

        $this->info($i);

        exit;

        $arr = [];
        $monedas = \VCN\Models\Bookings\BookingMoneda::where('moneda_id', '!=', 4)->where('rate', 1);
        foreach ($monedas->get() as $m) {
            $bid = $m->booking_id;
            $b = Booking::find($bid);
            if (!$b->pagado && $b->monedas->count() > 0) {
                $this->info($bid);
            }
        }

        exit;

        $facturas = \VCN\Models\Bookings\BookingFactura::where('total', '<', 0);

        $i = 0;
        foreach ($facturas->get() as $factura) {
            $factura->parcial_importe *= -1;

            $abono_ptotal = $factura->precio_total;
            if ($abono_ptotal['subtotal']) {
                foreach ($abono_ptotal['subtotal'] as $k => $v) {
                    $abono_ptotal['subtotal'][$k] = "-" . $v;
                }

                foreach ($abono_ptotal['subtotal_txt'] as $k => $v) {
                    $abono_ptotal['subtotal_txt'][$k] = "-" . $v;
                }

                $abono_ptotal['total'] = "-" . $abono_ptotal['total'];
                $abono_ptotal['total_txt'] = "-" . $abono_ptotal['total_txt'];
                $abono_ptotal['total_bruto'] *= -1;
                $abono_ptotal['total_neto'] *= -1;
            }

            $factura->precio_total = $abono_ptotal;
            $factura->divisa_variacion *= -1;
            $factura->total_divisa_txt = "-" . $factura->total_divisa_txt;

            $factura->save();

            $i++;
        }

        $this->info("Total facturas abono: $i");
    }

    public function scriptsLog()
    {
        ini_set('memory_limit', '400M');

        $ids = 30655;
        $listado = \VCN\Models\Leads\Viajero::where('origen_id', null);

        $this->info("Total a tratar " . $listado->count());

        $i = 0;
        $j = 0;
        $k = 0;
        foreach ($listado->get() as $l) {
            $dato = "origen_id";
            if (!$l->origen) {
                $logs = \VCN\Models\System\SystemLog::where('modelo', 'Viajero')->where('modelo_id', $l->id)->where('notas', 'LIKE', "%\"$dato\":%");

                if ($logs->count() == 0) {
                    continue;
                }

                foreach ($logs->get() as $log) {
                    $n = json_decode($log->notas, true);

                    $dato = "origen_id";
                    if (isset($n[$dato])) {
                        $r = $n[$dato];
                        $l->$dato = $r;
                        $l->save();
                        $i++;

                        // continue;
                    }

                    $dato = "suborigen_id";
                    if (isset($n[$dato])) {
                        $r = $n[$dato];
                        $l->$dato = $r;
                        $l->save();
                        $j++;

                        // continue;
                    }

                    $dato = "suborigendet_id";
                    if (isset($n[$dato])) {
                        $r = $n[$dato];
                        $l->$dato = $r;
                        $l->save();
                        $k++;

                        // continue;
                    }
                }
            }
        }

        $this->info("Total recuperados: $i/$j/$k");

        return;

        $bookings = \VCN\Models\Bookings\Booking::whereIn('status_id', [1, 2, 3, 4, 5, 6]);
        $this->info("Total Bookings: " . $bookings->count());

        foreach ($bookings->get() as $b) {
            $b->area = $b->area_default;
            $b->area_pagos = $b->area_pagos_default;
            $b->area_reunion = $b->area_reunion_default;

            $b->save();
        }

        // $viajeros = Viajero::where('user_id','>',0);
        // $this->info("Total Viajeros: ". $viajeros->count());


        return;
        $ret = \VCN\Models\Informes\Venta::procesarBookings();
        $this->info("Total $ret bookings procesados");
    }

    public function area()
    {
        ini_set('memory_limit', '400M');

        exit;

        $bookings = \VCN\Models\Bookings\Booking::whereIn('status_id', [1, 2, 3, 4, 5, 6])->where('convocatory_close_id', 440);
        $this->info("Total Bookings: " . $bookings->count());

        // $convo = \VCN\Models\Convocatorias\Cerrada::find(440);
        // dd($convo);exit;

        $i = 0;
        foreach ($bookings->get() as $b) {
            $p = $b->plataforma;
            ConfigHelper::commandPlataforma($p);

            $b->area = $b->area_default;
            // $b->area_pagos = $b->area_pagos_default;
            // $b->area_reunion = $b->area_reunion_default;
            $b->save();

            $v = $b->viajero;

            // $i += $v->setUsuarios();
        }
        $this->info("Total: $i");
        exit;



        $uv = User::where('roleid', 11)->count();
        $ut = User::where('roleid', 12)->count();

        $this->info("Total Usuarios11: $uv, Usuarios12: $ut");

        User::where('roleid', 11)->delete();
        User::where('roleid', 12)->delete();
        // return;

        // $viajeros = Viajero::where('user_id','>',0);
        // $this->info("Total Viajeros: ". $viajeros->count());

        $i = 0;
        // $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        $bookings = \VCN\Models\Bookings\Booking::whereIn('status_id', [1, 2, 3, 4, 5, 6]);
        $this->info("Total Bookings: " . $bookings->count());
        foreach ($bookings->get() as $b) {
            $b->area = $b->area_default;
            $b->area_pagos = $b->area_pagos_default;
            $b->area_reunion = $b->area_reunion_default;
            $b->save();

            $v = $b->viajero;

            $v->user_id = 0;
            $v->save();

            foreach ($v->tutores as $t) {
                $t->user_id = 0;
                $t->save();
            }
        }

        foreach ($bookings->get() as $b) {
            $v = $b->viajero;

            $v->setUsuarios(false);
            $i++;
        }

        $this->info("Total Viajeros => Usuario: $i");

        return;
    }

    public function areaMails()
    {
        ini_set('memory_limit', '400M');
        set_time_limit(0);

        $bookings = \VCN\Models\Bookings\Booking::whereIn('status_id', [1, 2, 3, 4, 5, 6]);
        // $bookings = \VCN\Models\Bookings\Booking::whereIn('id',[1684]);
        $this->info("Total Bookings: " . $bookings->count());

        $i = 0;
        $j = 0;
        $k = 0;
        foreach ($bookings->get() as $b) {
            $v = $b->viajero;

            $bFallo = false;

            if (!$v->usuario) {
                $bFallo = true;
                $i++;
            }

            foreach ($v->tutores as $t) {
                if (!$t->usuario) {
                    $bFallo = true;
                    $j++;
                }
            }

            if ($bFallo) {
                $k += $v->setUsuarios();
            }
        }

        $this->info("Total fallo espacios: $i/$j => $k");

        return;

        //Emails de usuarios
        $j = 0;
        foreach ($bookings->get() as $b) {
            $v = $b->viajero;

            foreach ($v->tutores as $t) {
                if ($t->usuario) {
                    $passw = str_random(8);
                    $t->usuario->password = bcrypt($passw);
                    $t->usuario->save();

                    // $this->info($t->usuario->username ." / $passw");

                    MailHelper::mailAreaInfo($t->usuario->id, $passw);
                    $j++;
                }
            }

            if ($v->usuario) {
                $passw = str_random(8);
                $v->usuario->password = bcrypt($passw);
                $v->usuario->save();

                // $this->info($v->usuario->username ." / $passw");

                MailHelper::mailAreaInfo($v->usuario->id, $passw);
                $j++;
            }
        }
        $this->info("Total Mails: $j");
    }

    public function emailsClean()
    {

        $f = $this->option('file');
        $file = "storage/excel/" . $f;

        if (!$f) {
            dd($file);
        }

        $tt = Carbon::now();
        global $tot;
        global $totV;
        global $totT;
        global $totU;

        $tot = 0;
        $totV = 0;
        $totT = 0;
        $totU = 0;

        $now = Carbon::now();
        Excel::load($file, function ($reader) use ($now) {

            $results = $reader->get();

            $c = count($results);
            $GLOBALS['tot'] += $c;

            $bar = $this->output->createProgressBar($c);

            $diff = Carbon::now()->diffForHumans($now);
            $this->info(" $diff s.");

            foreach ($results as $row) {

                $email = $row->email_address;
                $viajeros = Viajero::where('email', $email);
                if ($viajeros->count()) {
                    $GLOBALS['totV']++;

                    foreach ($viajeros->get() as $v) {
                        $v->email = null;
                        $v->save();

                        if ($v->user) {
                            $v->user->delete();
                            $GLOBALS['totU']++;
                        }

                        $txt = "email borrado mailchimp";
                        \VCN\Models\Leads\ViajeroLog::addLog($v, $txt);
                    }
                }

                $tutores = Tutor::where('email', $email);
                if ($tutores->count()) {
                    $GLOBALS['totT']++;

                    foreach ($tutores->get() as $v) {
                        $v->email = null;
                        $v->save();

                        if ($v->user) {
                            $v->user->delete();
                            $GLOBALS['totU']++;
                        }

                        $txt = "email borrado mailchimp";
                        \VCN\Models\Leads\TutorLog::addLog($v, $txt);
                    }
                }

                $bar->advance();
            }

            // echo ".";

            $bar->finish();

            $diff = Carbon::now()->diffInSeconds($now);
        }, 'UTF-8');

        $diff = Carbon::now()->diffInSeconds($now);
        $this->info(" $diff s.");

        $this->info("\nFin [$tot]" . Carbon::now());
        $diff = Carbon::now()->diffForHumans($tt);
        $this->info($diff);

        // dd($GLOBALS);
    }
}
