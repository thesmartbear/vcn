<?php

namespace VCN\Console\Commands;

use Illuminate\Console\Command;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Leads\ViajeroLog;
use VCN\Models\Leads\Viajero;
use VCN\Models\Solicitudes\Solicitud;


use ConfigHelper;
use VCN\Helpers\MailHelper;
use Carbon;
use Session;
class Sistema extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sistema {tipo : Proceso} {any?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gestor del sistema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '400M');

        $tipo = $this->argument('tipo');
        $any = $this->argument('any');

        // solo para mail
        // $p = \VCN\Models\Bookings\Booking::first()->plataforma;
        // ConfigHelper::commandPlataforma($p);

        switch($tipo)
        {
            case 'bookings':
            {
                $bookings = Booking::first();
                $plat = $bookings->plataforma;

               ConfigHelper::commandPlataforma($plat);

                //==== En el extranjero ====
                $st = ConfigHelper::config('booking_status_fuera');
                $hoy = Carbon::now()->format('Y-m-d');

                $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
                $bookings = Booking::whereIn('status_id',$stplazas)->where('status_id','<>',$st)->where('course_start_date','<=',$hoy)->where('course_end_date','>',$hoy);

                $this->info("Total Bookings Extranjero ($st): ". $bookings->count());

                foreach($bookings->get() as $booking)
                {
                    $booking->setStatus($st, "Cambio de Estado AUTO");
                    
                    // $booking->status_id = $st;
                    // $booking->save();
                    // $booking->viajero->booking_status_id = $st;
                    // $booking->viajero->save();
                    // BookingLog::addLogStatusAuto($booking, $st);
                }
                //==== En el extranjero ====

                //==== Vuelta a casa ====
                $st = ConfigHelper::config('booking_status_vuelta');
                $hoy = Carbon::now()->format('Y-m-d');

                $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
                $bookings = Booking::whereIn('status_id',$stplazas)->where('status_id','<>',$st)->where('course_end_date','<=',$hoy)->where('course_start_date','<',$hoy);

                $this->info("Total Bookings Vuelta ($st): ". $bookings->count());

                foreach($bookings->get() as $booking)
                {
                    $booking->setStatus($st, "Cambio de Estado AUTO");
                    
                    // $booking->status_id = $st;
                    // $booking->save();
                    // $booking->viajero->booking_status_id = $st;
                    // $booking->viajero->save();
                    // BookingLog::addLogStatusAuto($booking, $st);
                }
                //==== Vuelta a casa ====

                //==== Pasados ====
                $st = ConfigHelper::config('booking_status_fuera');
                $bookings = Booking::where('status_id',$st)->where('course_end_date','>=',$hoy);

                $this->info("Total Bookings Pasados ($st): ". $bookings->count());

                $iPasados = 0;
                foreach($bookings->get() as $booking)
                {
                    $v = $booking->viajero;
                    if($v->bookings->count()>1)
                    {
                        foreach($v->bookings as $b)
                        {
                            if($b && !$b->es_finalizado && $v->booking->es_finalizado && $v->booking_id != $b->id)
                            {
                                // $booking->viajero->booking_id = $b->id;
                                // $booking->viajero->booking_status_id = $b->status_id;
                                // $booking->viajero->save();

                                $b1 = $v->booking_id;
                                $b2 = $b->id;

                                $v->setBooking($b2);
                                $iPasados++;

                                $txt = "Cambio de asignación Booking [$b1 -> $b2]";
                                $this->info($txt);
                                ViajeroLog::addLog($v, $txt);
                            }
                        }
                    }
                }

                $this->info("Total Viajeros modificados: ". $iPasados);

                //==== Pasados ====

            }
            break;

            case 'ventas':
            {
                $this->info("Procesando ventas ". ($any ?: "TODAS") ."...");
                $ret = \VCN\Models\Informes\Venta::procesarBookings($any);
                $this->info("Total $ret bookings procesados");
            }
            break;

            case 'booking-datos':
            {
                $i = 0;
                foreach(Booking::all() as $b)
                {
                    $b->updateDatos();
                    $i++;
                }

                $this->info("Total Bookings update: ". $i);
            }
            break;
        }


    }
}
