<?php

namespace VCN\Console\Commands;

use Illuminate\Console\Command;

// use VCN\Repositories\Leads\LeadRepository as Lead;
// use VCN\Repositories\Leads\AccountRepository as Account;
// use VCN\Repositories\UserRepository as User;
// use VCN\Repositories\Leads\TutorRepository as Tutor;
// use VCN\Repositories\Leads\ViajeroRepository as Viajero;

use VCN\Models\Leads\Lead;

use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\Tutor;
use VCN\Models\Leads\ViajeroLog;
use VCN\Models\Leads\ViajeroTarea;
use VCN\Models\Solicitudes\Solicitud;

class Traspaso extends Command
{
    // protected $lead;
    // protected $account;
    // protected $viajero;
    // protected $tutor;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'traspaso';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Traspaso Leads a nueva estructura Viajeros - Tutores.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    // public function __construct( Lead $lead, Account $account, Viajero $viajero, Tutor $tutor )
    public function __construct()
    {
        //
        parent::__construct();

        // $this->lead = $lead;
        // $this->viajero = $viajero;
        // $this->tutor = $tutor;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        \DB::table('viajeros')->delete();
        \DB::table('tutores')->delete();
        \DB::table('solicitudes')->delete(); //hay q crear una para cada lead

        // \DB::table('viajero_logs')->delete();
        // \DB::table('viajero_tareas')->delete();
        // \DB::table('viajero_tutores')->delete();

        $leads = Lead::all();
        $tutores = Lead::where('lead_type',1);
        $viajeros = Lead::where('lead_type',2);

        $leads_cli = Lead::where('lead_is_client','Yes');
        $leads_nocli = Lead::where('lead_is_client','<>','Yes');

        $this->comment("Hay ". $viajeros->count() ." viajeros.");
        $this->comment("Hay ". $tutores->count() ." tutores.");
        $this->comment("Total ". $leads->count() ." leads.");

        $this->comment("Clientes ". $leads_cli->count() );
        $this->comment("No Clientes ". $leads_nocli->count() );

        $this->comment("\n---\: PROCESO :/---");

        $bar = $this->output->createProgressBar(count($leads));

        foreach($leads as $lead)
        {
            $bar->advance();
            $v = Viajero::traspasoFromLead($lead);
        }

        $bar->finish();

        $this->comment("\n---\: RESULTADO :/---");

        $this->comment("Viajeros: ". Viajero::count() );
        $this->comment("Tutores: ". Tutor::count() );
        $this->comment("Solicitudes: ". Solicitud::count() );

        $this->comment("---\: LOG y TAREAS :/---");

        $this->comment("Logs: ". ViajeroLog::count() );
        $this->comment("Tareas: ". ViajeroTarea::count() );

    }
}
