<?php

namespace VCN\Console\Commands;

use Illuminate\Console\Command;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\System\Aviso;
use VCN\Models\System\AvisoLog;

use ConfigHelper;
use VCN\Helpers\MailHelper;
use Carbon;
use Log;

class Avisos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'avisos {tipo : Tipo de Aviso} {id? : Id de Aviso}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gestor de avisos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '400M');

        $tipo = $this->argument('tipo');

        $id = (int)$this->argument('id');

        /*
        if($id)
        {
            $aviso = Aviso::find($id);

            if(!$aviso)
            {
                return;
            }

            $p = $aviso->plataforma;
            ConfigHelper::commandPlataforma($p);

            switch($tipo)
            {
                case 'bookings-1':
                {
                    $p = $aviso->plataforma;
                    ConfigHelper::commandPlataforma($p);

                    $bookings = Booking::bookingAvisos($aviso->filtros,$aviso->filtros_not);
                    //dd($bookings->count());

                    //Creamos el log: ejecutable_log
                    $log = new AvisoLog;
                    $log->aviso_id = $aviso->id;
                    $log->save();

                    // $this->info("Bookings: ". $bookings->count());

                    //Lo desactivamos pq es puntual <= antes de la ejecución
                    $aviso->activo = false;
                    $aviso->save();

                    $iTot = 0;
                    $tTot = 0;
                    $iSend = 0;
                    $arrBookings = [];
                    foreach($bookings->get() as $booking)
                    {
                        $this->info($booking->id);

                        $importe30 = $booking->precio_sin_cancelacion;
                        $pc = floatval($aviso->doc->notapago_porcentaje) /100;

                        $importe30 = $importe30 * $pc;
                        $pagado = $booking->total_pagado;
                        if($pagado < $importe30)
                        {
                            $iSend += MailHelper::mailBookingNotaPago($booking->id,$aviso->id);
                            // BookingLog::addLog($booking, "Aviso[$aviso->id]", $aviso->name);
                            BookingLog::addLog($booking, "Aviso[$aviso->id]", "$aviso->name :: ".$aviso->doc->name);
                            $iTot++;
                            $tTot += $importe30;

                            array_push($arrBookings,$booking->id);
                        }
                    }
                }
                break;

                case 'bookings-x':
                {
                    $bookings = Booking::bookingAvisos($aviso->filtros,$aviso->filtros_not);

                    $log = new AvisoLog;
                    $log->aviso_id = $aviso->id;
                    $log->save();

                    $iTot = 0;
                    $iSend = 0;
                    $arrBookings = [];
                    foreach($bookings->get() as $booking)
                    {
                        //Pendiente: ejecutar aviso
                        //$iSend += Mail

                        array_push($arrBookings,$booking->id);
                        $iTot++;
                    }

                    $log->bookings = $arrBookings;
                    $log->notas = "Total Bookings: $iTot";
                    $log->enviados = $iSend;
                    $log->save();
                }
            }
        }
        */

        //Puntual
        switch($tipo)
        {
            case 'mailchimp-sync':
            {
                $p = 1; //bs
                $log1 = Aviso::syncMailchimp($p);
                // $p = 2; //cic
                // $log2 = Aviso::syncMailchimp($p);
            }
            break;

            case 'bookings':
            {
                //Solo activos: activo=?1
                $avisos = Aviso::where('tipo',0)->where('activo',1)->where('cuando_tipo','puntual');
                $force = false;

                if($id)
                {
                    $force = true;
                    $avisos = Aviso::where('id',$id);//->where('activo',1);
                }

                if($avisos->count()>0)
                {
                    ini_set('memory_limit', '400M');
                }

                foreach($avisos->get() as $aviso)
                {
                    if(!$aviso->ejecutable && !$force)
                    {
                        if($aviso->es_caducado)
                        {
                            //Lo desactivamos
                            $aviso->activo = false;
                            $aviso->save();

                            $log = new AvisoLog;
                            $log->aviso_id = $aviso->id;
                            $log->notas = "Caducado";
                            $log->save();
                        }
                    
                        //y lo saltamos
                        continue;
                    }

                    $p = $aviso->plataforma;
                    ConfigHelper::commandPlataforma($p);

                    $bookings = Booking::bookingAvisos($aviso->filtros,$aviso->filtros_not);
                    $this->info("Bookings: ". $bookings->count());

                    //Lo desactivamos pq es puntual <= antes de la ejecución
                    $aviso->activo = false;
                    $aviso->save();

                    $iTot = 0;
                    $tTot = 0;
                    $iSend = 0;
                    $arrBookings = [];

                    // NUEVA LOPD
                    if($aviso->doc_id==3 || $aviso->doc_id==4)
                    {
                        $aviso->procesarLOPD($bookings);
                        return;
                    }

                    //Creamos el log: ejecutable_log
                    $log = new AvisoLog;
                    $log->aviso_id = $aviso->id;
                    $log->save();

                    foreach($bookings->get() as $booking)
                    {
                        //Para modificaciones.
                        if(!$booking->yaAviso($aviso->id) || $force)
                        {
                            switch($aviso->doc_id)
                            {
                                case 0: //Nota pago viejo
                                {
                                    $importe30 = $booking->precio_sin_cancelacion;

                                    $importe30 = $importe30 * 0.30;
                                    $pagado = $booking->total_pagado;
                                    if($pagado < $importe30)
                                    {
                                        $iSend += MailHelper::mailBookingNotaPago($booking->id,$aviso->id);
                                        // BookingLog::addLog($booking, "Aviso[$aviso->id]", $aviso->name);
                                        BookingLog::addLog($booking, "Aviso[$aviso->id]", "$aviso->name :: ".$aviso->doc->name);
                                        $iTot++;
                                        $tTot += $importe30;

                                        array_push($arrBookings,$booking->id);
                                    }
                                }
                                break;

                                case 1: //Vuelo virolay
                                {
                                    $iSend += MailHelper::mailTemplate($booking->id,'temp',"British Summer: informació vols estada d'anglès a Bournemouth");
                                    BookingLog::addLog($booking, "Aviso[$aviso->id]", $aviso->name);
                                    $iTot++;

                                    array_push($arrBookings,$booking->id);
                                }
                                break;

                                case 2: //Recordatorio pasaportes
                                {
                                    $plat = $booking->viajero->plataforma_name;

                                    $asunto = "$plat: Verificación datos pasaporte - MUY IMPORTANTE";
                                    $idioma = $booking->viajero->idioma_contacto;
                                    if($idioma=="ca")
                                    {
                                        $asunto = "$plat: Verificació dades del passaport – MOLT IMPORTANT";
                                    }

                                    $iSend += MailHelper::mailTemplate($booking->id, "$idioma.booking_pasaporte", $asunto);
                                    BookingLog::addLog($booking, "Aviso[$aviso->id]", $aviso->name);
                                    $iTot++;

                                    array_push($arrBookings,$booking->id);
                                }
                                break;

                                case 3: //LOPD
                                case 4:
                                {
                                    $plat = $booking->viajero->plataforma_name;

                                    $asunto = "$plat: LOPD - MUY IMPORTANTE";
                                    $idioma = $booking->viajero->idioma_contacto;
                                    if($idioma=="ca")
                                    {
                                        $asunto = "$plat: LOPD – MOLT IMPORTANT";
                                    }

                                    $iSend += MailHelper::mailTemplate($booking->id, "$idioma.lopd", $asunto);
                                    BookingLog::addLog($booking, "Aviso[$aviso->id]", $aviso->name);
                                    $iTot++;

                                    array_push($arrBookings,$booking->id);
                                }
                                break;

                                default: //AvisoDoc
                                {
                                    $tipo = 0;
                                    if($aviso->doc)
                                    {
                                        $tipo = $aviso->doc->tipo;
                                    }

                                    switch($tipo)
                                    {
                                        case 1: //notapago
                                        {
                                            $importe30 = $booking->precio_sin_cancelacion;

                                            $pc = (float)$aviso->doc->notapago_porcentaje;
                                            $pc = floatval($pc) /100;

                                            $importe30 = $importe30 * $pc;
                                            $pagado = $booking->total_pagado;

                                            $bEnviar = false;
                                            if($aviso->doc->notapago_pagar_tipo==2)
                                            {
                                                $bEnviar = $booking->saldo_pendiente>0;
                                            }
                                            else
                                            {
                                                $bEnviar = ($pagado < $importe30);
                                            }

                                            $info = $booking->id . ": ". $pagado ."[$importe30] :: $pc";
                                            $this->info($info);

                                            if($bEnviar)
                                            {
                                                $iSend += MailHelper::mailBookingNotaPago($booking->id,$aviso->id);
                                                // BookingLog::addLog($booking, "Aviso[$aviso->id]", $aviso->name);
                                                BookingLog::addLog($booking, "Aviso[$aviso->id]", "$aviso->name :: ".$aviso->doc->name);
                                                $iTot++;
                                                $tTot += $importe30;

                                                array_push($arrBookings,$booking->id);
                                            }
                                        }
                                        break;

                                        case 0: //Documento
                                        default:
                                        {
                                            $iSend += MailHelper::mailAvisoDoc($booking->id,$aviso->id,$tipo);
                                            BookingLog::addLog($booking, "Aviso[$aviso->id]", "$aviso->name :: ".$aviso->doc->name);
                                            $iTot++;

                                            array_push($arrBookings,$booking->id);
                                        }
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    
                    $destino = $aviso->destino;
                    $notas = "Destino : $destino :: ";

                    $notas .= "Total Bookings: $iTot";
                    if($tTot)
                    {
                        $notas .= "Total Bookings: $iTot / Importe Total: $tTot";
                    }

                    $log->notas = $notas;
                    $log->bookings = $arrBookings;
                    $log->enviados = $iSend;
                    $log->save();

                    $this->info($notas);

                    // //Lo desactivamos pq es puntual
                    // $aviso->activo = false;
                    // $aviso->save();
                }
            }
            break;

            case 'booking-pagos':
            {
                $pagos = \VCN\Models\Bookings\BookingPago::where('avisado',0)->where('importe','>',0);
                $this->info("BookingPagos: ". $pagos->count());

                $year = Carbon::now()->year;

                $iTotal = 0;
                foreach($pagos->get() as $pago)
                {
                    $booking = $pago->booking;

                    if($booking->pagado)
                    {
                        $pago->avisado = 3;
                        $pago->save();

                        continue;
                    }
                    
                    if($booking->any < $year)
                    {
                        $pago->avisado = 4;
                        $pago->save();

                        continue;
                    }

                    if($booking->es_prespuesto)
                    {
                        continue;
                    }

                    if(!$booking->area_pagos)
                    {
                        continue;
                    }

                    $p = $booking->plataforma;
                    ConfigHelper::commandPlataforma($p);

                    $pago->pdf();

                    if( MailHelper::mailBookingPago($pago) )
                    {
                        BookingLog::addLog($booking, "Aviso Pago [$pago->id]");
                        $pago->avisado = 1;
                        $pago->save();

                        $iTotal++;
                    }
                }

                $this->info("Enviados: $iTotal");
            }
            break;

            case 'notapago35':
            {
                //$booking->area_pagos
                $bookings = \VCN\Models\Bookings\Booking::where('es_presupuesto',0)->where('pagado',0)->where('es_directo',0)->where('status_id',ConfigHelper::config('booking_status_prebooking'))->where('area_pagos',1);
                
                $this->info("Bookings: ". $bookings->count());

                $iTotalF = 0;
                $iTotalN = 0;
                foreach($bookings->get() as $booking)
                {
                    $booking->precio_total;
                    
                    if($booking->curso->categoria && !$booking->curso->categoria->notapago_auto)
                    {
                        continue;
                    }

                    if($booking->es_facturado)
                    {
                        continue;
                    }

                    $locale = $booking->idioma_contacto;
                    \App::setLocale($locale);

                    $npDias = $booking->curso->categoria->notapago;
                    $fsalida35 = Carbon::parse($booking->course_start_date)->subDays($npDias);
                    $fnow = Carbon::now();
                    $bFechaEnviar = $fnow->gte($fsalida35); //Comprobacion $npDias

                    if(!$bFechaEnviar)
                    {
                        continue;
                    }
                    
                    if($booking->saldo_pendiente>0)
                    {
                        $p = $booking->plataforma;
                        ConfigHelper::commandPlataforma($p);

                        $facturacion = ConfigHelper::config('facturas',$p);
                        if($facturacion)
                        {
                            $facturacion = $booking->es_facturable_auto;
                        }

                        if($facturacion && $booking->no_facturar==0) //emitir factura sistema
                        {
                            $factura = $booking->factura_last;
                            if(!$booking->es_facturado)
                            {
                                $factura = $booking->facturar_mail(); //fuerza generar el pdf
                            }

                            if( $factura )
                            {
                                if( $factura->enviada )
                                {
                                    continue;
                                }

                                //Si tiene notapago enviada, se genera pero no se envía
                                if($booking->es_notapago35_enviada)
                                {
                                    BookingLog::addLog($booking, "Factura generada (auto) [$factura->numero]", "No se envia por Nota de Pago");
                                    continue;
                                }

                                if( MailHelper::mailFactura($factura->id) )
                                {
                                    BookingLog::addLog($booking, "Factura enviada (auto) [$factura->numero]");

                                    $factura->enviada = true;
                                    $factura->save();
                                }
                                else
                                {
                                    BookingLog::addLog($booking, "Factura NO enviada (auto) [Err:email]");
                                }
                            }
                            else
                            {
                                BookingLog::addLog($booking, "Factura NO enviada (auto) [Err:factura]");
                            }

                            $iTotalF++;
                        }
                        else
                        {
                            if( $booking->notaPago35() )
                            {
                                BookingLog::addLog($booking, "NotaPago enviada (auto)");
                                $booking->notaPago35_mail();

                                $iTotalN++;
                            }
                        }
                    }
                }

                $this->info("F: $iTotalF :: NP: $iTotalN");
            }
            break;
        }

    }
}
