<?php

namespace VCN\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use ConfigHelper;
use VCN\Models\System\Aviso;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \VCN\Console\Commands\Inspire::class,
        // \VCN\Console\Commands\Traspaso::class,
        // \VCN\Console\Commands\TraspasoPrecios::class,
        \VCN\Console\Commands\Scripts::class,
        \VCN\Console\Commands\Informes::class,
        \VCN\Console\Commands\Avisos::class,
        \VCN\Console\Commands\Sistema::class,
        \VCN\Console\Commands\Notifications::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        //wf: * * * * * /usr/local/bin/php /home/vcn2015/webapps/vcndemo/artisan schedule:run >> /dev/null 2>&1

        $timezone = config('app.timezone');
        // $timezone = ConfigHelper::config('timezone');

        // $schedule->command('cache:clear')->twiceDaily(8,15);
        // $schedule->command('view:clear')->twiceDaily(8,15);

        $schedule->command('sistema bookings')
            ->dailyAt('01:00')->timezone($timezone);

        $schedule->command('informe pagos')
            ->dailyAt('03:00')->timezone($timezone);
        // 0 3 * * * php artisan informe pagos --plataforma=1

        $schedule->command('informe menores12')
            ->dailyAt('03:15')->timezone($timezone);

        $schedule->command('informe aviso-reservas')
            ->dailyAt('07:00')->timezone($timezone);

        // Nota de pago auto
        $schedule->command('avisos notapago35')
            ->dailyAt('08:00')->timezone($timezone);

        $schedule->command('avisos booking-pagos')
            ->dailyAt('10:00')->timezone($timezone);

        // == Gestor de Avisos ==
        //Bookings: puntuales
        $schedule->command('avisos bookings')->everyMinute()->timezone($timezone);

        //Bookings : dia
        $avisos = Aviso::where('tipo',0)->where('activo',1)->where('cuando_tipo','dia')->get();
        foreach($avisos as $aviso)
        {
            $schedule->command("avisos bookings $aviso->id")->daily()->at($aviso->cuando_hora)->timezone($timezone);
        }

        //Bookings: semana
        $avisos = Aviso::where('tipo',0)->where('activo',1)->where('cuando_tipo','mes')->get();
        foreach($avisos as $aviso)
        {
            switch($aviso->cuando_dia)
            {
                case 1:
                    {
                        $schedule->command("avisos bookings $aviso->id")->weekly()->mondays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;

                case 2:
                    {
                        $schedule->command("avisos bookings $aviso->id")->weekly()->tuesdays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;

                case 3:
                    {
                        $schedule->command("avisos bookings $aviso->id")->weekly()->wednesdays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;

                case 4:
                    {
                        $schedule->command("avisos bookings $aviso->id")->weekly()->thursdays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;

                case 5:
                    {
                        $schedule->command("avisos bookings $aviso->id")->weekly()->fridays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;

                case 6:
                    {
                        $schedule->command("avisos bookings $aviso->id")->weekly()->saturdays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;

                case 7:
                    {
                        $schedule->command("avisos bookings $aviso->id")->weekly()->sundays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;
            }
        }

        //Bookings: mes
        $avisos = Aviso::where('tipo',0)->where('activo',1)->where('cuando_tipo','mes')->get();
        foreach($avisos as $aviso)
        {
            switch($aviso->cuando_dia)
            {
                case 1:
                    {
                        $schedule->command("avisos bookings $aviso->id")->monthly()->mondays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;

                case 2:
                    {
                        $schedule->command("avisos bookings $aviso->id")->monthly()->tuesdays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;

                case 3:
                    {
                        $schedule->command("avisos bookings $aviso->id")->monthly()->wednesdays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;

                case 4:
                    {
                        $schedule->command("avisos bookings $aviso->id")->monthly()->thursdays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;

                case 5:
                    {
                        $schedule->command("avisos bookings $aviso->id")->monthly()->fridays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;

                case 6:
                    {
                        $schedule->command("avisos bookings $aviso->id")->monthly()->saturdays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;

                case 7:
                    {
                        $schedule->command("avisos bookings $aviso->id")->monthly()->sundays()->at($aviso->cuando_hora)->timezone($timezone);
                    }
                    break;
            }
        }

        // $schedule->command('sitemap:generate')->weeklyOn('1', '04:00');
        $schedule->command('sitemap:generate')->dailyAt('04:00');
        $schedule->command('avisos mailchimp-sync')->weeklyOn(6, '03:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
