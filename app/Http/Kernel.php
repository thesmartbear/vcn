<?php

namespace VCN\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \VCN\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \VCN\Http\Middleware\TrustProxies::class,

        \VCN\Http\Middleware\ClearLaravelViewCache::class,
        // \VCN\Http\Middleware\ActividadUsers::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \VCN\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \VCN\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,

            \VCN\Http\Middleware\ConfiguracionWeb::class,
            \VCN\Http\Middleware\ActividadUsers::class,
        ],

        'manage' => [
            \VCN\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \VCN\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,

            \VCN\Http\Middleware\Configuracion::class,
            \VCN\Http\Middleware\ActividadUsers::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        // 'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        
        'auth' => \VCN\Http\Middleware\Authenticate::class,
        'auth.area' => \VCN\Http\Middleware\AreaCliente::class,

        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \VCN\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,

        'permiso.plataforma' => \VCN\Http\Middleware\PermisoPlataforma::class,
        'permiso.view' => \VCN\Http\Middleware\PermisoView::class,
        'permiso.edit' => \VCN\Http\Middleware\PermisoEdit::class,
        'permiso.informe' => \VCN\Http\Middleware\PermisoInforme::class,
        'permiso.aislado' => \VCN\Http\Middleware\PermisoAislado::class,
        'permiso.booking' => \VCN\Http\Middleware\PermisoBookingOnline::class,

        'visitas' => \VCN\Http\Middleware\Visitas::class,
    ];
}
