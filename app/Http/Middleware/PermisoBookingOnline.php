<?php

namespace VCN\Http\Middleware;

use Closure;

use \VCN\Models\Bookings\Booking;

class PermisoBookingOnline
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $booking_id)
    {
        $user = $request->user();

        //Controlar q no se metan donde no deben
        $booking = Booking::find($booking_id);
        if(!$booking->es_online)
        {
            return redirect()->route('area.index');
        }

        if($user->es_viajero)
        {
            if($booking->viajero_id != $user->ficha->id)
            {
                return redirect()->route('area.index');
            }
        }
        elseif($user->es_tutor && $booking->tutor_id)
        {
            if($booking->tutor_id != $user->ficha->id)
            {
                return redirect()->route('area.index');
            }
        }

        return $next($request);
    }
}
