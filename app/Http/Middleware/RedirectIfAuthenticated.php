<?php

namespace VCN\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use VCN\Helpers\ConfigHelper;


class RedirectIfAuthenticated
{
    protected $redirectPathArea = '/area';
    protected $redirectPath = '/manage';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check())
        {
            //return redirect('/home');

            $user = $request->user();

            if($user->roleid == 0)
            {
                return redirect("/");
            }

            if(ConfigHelper::config('web_registro'))
            {
                if($user->registro)
                {
                    $url = $user->registro['url'];
                    return redirect()->intended($url);
                }
            }

            if($user->es_cliente)
            {
                return redirect($this->redirectPathArea);
            }
            else
            {
                return redirect($this->redirectPath);
            }
        }

        return $next($request);
    }
}
