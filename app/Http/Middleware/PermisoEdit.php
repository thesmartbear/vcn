<?php

namespace VCN\Http\Middleware;

use Closure;
use Session;
use Auth;

use VCN\Models\System\UserRole;
use VCN\Helpers\ConfigHelper;

class PermisoEdit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permiso)
    {
        if(Auth::user()->isFullAdmin())
            return $next($request);

        if(!UserRole::permisoEdit($permiso))
        {
            Session::flash('mensaje-alert', "No tiene permisos para editar.");
            return redirect()->back();
        }

        return $next($request);
    }
}
