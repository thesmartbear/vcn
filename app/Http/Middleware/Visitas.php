<?php

namespace VCN\Http\Middleware;

use Closure;

use VCN\Models\Cursos\Curso;

class Visitas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ruta = $request->route()->getName();

        $user_id = $request->user()?$request->user()->id:0;

        switch($ruta)
        {
            case 'web.curso':
            {
                // WebVisita
                $slug = $request->route()->parameters['slug'];
                $curso = Curso::where('course_slug', $slug)->first();
                $curso_id = $curso?$curso->id:0;

                \VCN\Models\Informes\WebVisita::add($curso_id, $user_id);

            }
            break;
        }


        return $next($request);
    }
}
