<?php namespace VCN\Http\Middleware;
/**************************************************************************************
 * These class is responsible for clearing the Laravel cache in sandbox environments
 *
 */
use Closure;
use Illuminate\Contracts\Foundation\Application;

use Session;
use Config;
use View;
use Auth;
use App;
use Localization;
use DB;
use Mail;

use VCN\Helpers\ConfigHelper;
use Carbon;

/**
 * Configuracion
 * Sistema de configuración multiweb
 *
 * @param request The request object.
 * @param $next The next closure.
 * @return redirects to the secure counterpart of the requested uri.
*/
class ConfiguracionWeb
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $config = env('APP_CONFIG', 'vcn');

        Session::forget('vcn.tema2');

        Session::put( 'vcn.config', $config );
        // Session::put( 'vcn.tema', config("$config.tema") );
        // Session::put( 'vcn.moneda', config("$config.moneda") );
        // Session::put( 'vcn.moneda_format', config("$config.moneda_format") );
        // $moneda_locale = config("$config.moneda_locale");

        // Session::put( 'vcn.plataforma', 0);

        $user = $request->user();
        
        if(!$user)
        {
            // return $next($request);
        }

        //LOCALE
        $locale = null;
        {
            if( $request->is('es/*') || $request->is('es') )
            {
                $locale = "es";
            }
            elseif( $request->is('ca/*') || $request->is('ca') )
            {
                $locale = "ca";
            }
        }
        
        $locale = $locale ?: Session::get('vcn.idioma', null);
        $locale = $locale ?: ConfigHelper::config('idioma');

        App::setLocale($locale);
        // Localization::setLocale($locale);
        Session::put('vcn.idioma',$locale);
        
        //Rutas backend/frontend
        $backend = false;//ConfigHelper::config('backend');
        $frontend = ConfigHelper::config('frontend');

        $ruta = $request->path();
    
        if($user && $ruta == "area")
        {
            // App::setLocale("es");
        
            View::share('usuario', $user);

            $locale = $user->ficha ? $user->ficha->idioma_contacto : "es";
            App::setLocale($locale);
            Session::put('vcn.idioma',$locale);
        }

        if($ruta=='/' && !$frontend)
        {
            return redirect()->route('area.index');
        }

        if(!strpos($ruta,'auth')===0) //por el login
        {
            //Backend accesible?
            if(!$backend && (strpos($ruta,'manage')===0 || strpos($ruta,'area')===0) )// || strpos($ruta,'auth')===0) )
            {
                abort(404);
            }
        }
        else
        {
            //Backend accesible? pero area si
            if( !$backend && (strpos($ruta,'manage')===0) )
            {
                //Proceso booking online
                if(!strpos($ruta,'ajax')===0)
                {
                    abort(404);
                }
            }

        }

        //Frontend accesible?
        if(!$frontend && (strpos($ruta,'manage')>0 && strpos($ruta,'area')>0 && strpos($ruta,'auth')>0) )
        {
            abort(404);
        }

        // $tipoConfig = env('APP_MULTIWEB', false);
        // if($tipoConfig)

        $config1 = Session::get('vcn.host',null);
        if(!$config1)
        {
            $host = ConfigHelper::getHost();

            Session::put( 'vcn.host', $host);
            Session::put( 'vcn.plataforma', ConfigHelper::config("propietario"));

            Session::put( 'vcn.config', "$config.$host");

            Session::put( 'vcn.tema', ConfigHelper::config("tema") );
            Session::put( 'vcn.moneda', ConfigHelper::config("moneda") );

            $formato = ConfigHelper::config('moneda_format');
            $miles = ConfigHelper::config('moneda_miles');
            $decimales = ConfigHelper::config('moneda_decimales');
            Session::put('vcn.moneda_format',$formato);
            Session::put('vcn.moneda_miles',$miles);
            Session::put('vcn.moneda_decimales',$decimales);

            $moneda_locale = ConfigHelper::config("moneda_locale");
            setlocale(LC_MONETARY, $moneda_locale);

            // $db = ConfigHelper::config("$config.$host.database", (config("$config.database", 'mysql')) );
            // Session::put('vcn.database', $db);
            // Config::set('database.default', $db);
        }

        //Configuración de menú

        if(!Auth::guest())
        {
            Session::put('sidebar_open', '');

            $user = auth()->user();
            if($user->roleid==11 || $user->roleid==12 || $user->roleid==13) //viajero o tutor o monitor
            {
                $menuArea = ConfigHelper::menuArea();
                // Session::put('vcn.menu.area',$menuArea);
                // View::share('menuManager', Session::get('vcn.menu.manager'));
                View::share('menuArea', $menuArea);
            }
            else
            {
                View::share('menuArea', []);

                //Para no generarlo cada vez
                // if(!Session::get('vcn.menu.manager'))
                // if(ConfigHelper::config('backend'))
                {
                    Config::set('app.debug', true);

                    $menuManager = ConfigHelper::menuManager();
                    // Session::put('vcn.menu.manager',$menuManager);

                    View::share('menuManager', $menuManager);
                    // View::share('menuManager', Session::get('vcn.menu.manager'));

                    // dd($menuManager);
                }
            }

            $usuario = unserialize(Session::get('usuario'));
            View::share('usuario', $usuario);
        }

        // if(Auth::guest())
        {
            // if(!Session::get('vcn.menu.web'))
            {
                $menuWeb = ConfigHelper::menuWeb();
                // Session::put('vcn.menu.web',$menuWeb);
                View::share('menuWeb', $menuWeb);
                // View::share('menuWeb', Session::get('vcn.menu.web'));
            }
        }

        return $next($request);
    }

}