<?php namespace VCN\Http\Middleware;
/**************************************************************************************
 * These class is responsible for clearing the Laravel cache in sandbox environments
 *
 */
use Closure;
use Illuminate\Contracts\Foundation\Application;

/**
 * ClearLaravelViewCache
 * Just redirects to next closure, but will clear Laravel views cache in sandbox/local environments
 *
 * @param request The request object.
 * @param $next The next closure.
 * @return redirects to the secure counterpart of the requested uri.
*/
class ClearLaravelViewCache
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function handle($request, Closure $next)
    {
        if (env('APP_ENV') === 'sandbox' || env('APP_ENV') === 'local') {
            $cachedViewsDirectory=app('path.storage').'/framework/views/';
            $files = glob($cachedViewsDirectory.'*');
            foreach($files as $file) {
                if(is_file($file)) {
                    @unlink($file);
                }
            }
        }

        return $next($request);
    }

}