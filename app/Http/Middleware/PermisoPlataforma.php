<?php

namespace VCN\Http\Middleware;

use Closure;
use Session;

use VCN\Models\System\UserRole;
use VCN\Helpers\ConfigHelper;

class PermisoPlataforma
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permiso, $modelo)
    {
        $user = auth()->user();

        if($user->isFullAdmin())
            return $next($request);

        $filtro = ConfigHelper::config('propietario');

        if($filtro)
        {
            if( !UserRole::permisoPlataforma($permiso, $modelo, $request->route('id')) )
            {
                Session::flash('mensaje-alert', "No tiene permisos.");
                return redirect()->route('manage.index');
            }
        }

        return $next($request);
    }
}
