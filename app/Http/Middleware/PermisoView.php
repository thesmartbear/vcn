<?php

namespace VCN\Http\Middleware;

use Closure;
use Auth;
use Session;

use VCN\Models\System\UserRole;
use VCN\Helpers\ConfigHelper;


class PermisoView
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permiso)
    {
        if(Auth::user()->isFullAdmin())
            return $next($request);

        if(!UserRole::permisoView($permiso))
        {
            Session::flash('mensaje-alert', "No tiene permisos para acceder.");
            return redirect()->route('manage.index');
            // return redirect()->back();
            // return response('Unauthorized.', 401);
        }

        return $next($request);
    }
}
