<?php

namespace VCN\Http\Middleware;

use Closure;


use \VCN\Models\System\UserActividad;
use Carbon;

use Jenssegers\Agent\Agent;

class ActividadUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if(!$user)
        {
            return $next($request);
        }

        // dd($request->header('User-Agent'));
        // dd($agent->isDesktop());

        if($user)
        {
            $t = Carbon::now()->getTimestamp();
            $user->last_activity = $t;
            $user->save();
        }

        if($user && !$user->ficha)
        {
            $agent = new Agent();

            $user_id = $user->id;
            $fecha = Carbon::now();
            $hora = $fecha->format('H:i:s');

            $actividad = UserActividad::where('user_id',$user_id)->where('fecha', $fecha->format('Y-m-d'))->first();
            if(!$actividad)
            {
                $actividad = new UserActividad;
                $actividad->user_id = $user_id;
                $actividad->fecha = $fecha;
                $actividad->hora_ini = $hora;

                if($agent->isDesktop())
                {
                    $actividad->desktop_hora_ini = $hora;
                }
                elseif($agent->isMobile())
                {
                    $actividad->mobile_hora_ini = $hora;
                }
                elseif($agent->isTablet())
                {
                    $actividad->tablet_hora_ini = $hora;
                }
            }

            //Inactividad
            $hora_ant = Carbon::parse($actividad->hora_fin);
            $resta = $fecha->diffInMinutes($hora_ant);
            $actividad->inactividad += $resta;
            if($actividad->inactividad_max < $resta)
            {
                $actividad->inactividad_max = $resta;
            }

            if($agent->isDesktop())
            {
                if($actividad->desktop_hora_ini == "00:00:00")
                {
                    $actividad->desktop_hora_ini = $hora;
                }

                //Inactividad Desktop
                $hora_ant = Carbon::parse($actividad->desktop_hora_fin);
                $resta = $fecha->diffInMinutes($hora_ant);
                $actividad->desktop_inactividad += $resta;
                if($actividad->desktop_inactividad_max < $resta)
                {
                    $actividad->desktop_inactividad_max = $resta;
                }

                $actividad->desktop_hora_fin = $hora;
            }
            elseif($agent->isMobile())
            {
                if($actividad->mobile_hora_fin == "00:00:00")
                {
                    $actividad->mobile_hora_fin = $hora;
                }

                $actividad->mobile_hora_fin = $hora;
            }
            elseif($agent->isTablet())
            {
                if($actividad->tablet_hora_fin == "00:00:00")
                {
                    $actividad->tablet_hora_fin = $hora;
                }

                $actividad->tablet_hora_fin = $hora;
            }

            $actividad->hora_fin = $hora;
            $actividad->save();
        }

        return $next($request);
    }
}
