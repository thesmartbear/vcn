<?php

namespace VCN\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use Session;
use Auth;

use VCN\Models\System\UserRole;
use VCN\Helpers\ConfigHelper;

class AreaCliente
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('auth/login');
            }
        }

        //Logueado
        $user = auth()->user();
        if($user->roleid==11 || $user->roleid==12 || $user->roleid==13)
        {
            $bMonitor = $user->roleid==13 && $user->plataforma == 0;
            if($bMonitor)
            {
                return $next($request);
            }

            $plataforma = ConfigHelper::config('propietario');
            if($user->plataforma == $plataforma)
            {
                return $next($request);
            }
            else
            {
                Auth::logout();
                return redirect()->guest('/');
            }
        }
        else //area desde manage
        {
            return $next($request);
        }

        return redirect()->route('manage.index');
    }
}
