<?php

namespace VCN\Http\Middleware;

use Closure;

use VCN\Models\System\UserRole;
use ConfigHelper;
use Session;
use Auth;

class PermisoInforme
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $ruta)
    {
        if(Auth::user()->isFullAdmin())
            return $next($request);

        if(!UserRole::permisoInforme($ruta))
        {
            Session::flash('mensaje-alert', "No tiene permisos para ver el informe.");
            return redirect()->route('manage.index');
        }

        return $next($request);
    }
}
