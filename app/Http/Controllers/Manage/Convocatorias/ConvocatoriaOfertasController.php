<?php

namespace VCN\Http\Controllers\Manage\Convocatorias;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Convocatorias\OfertaRepository as Oferta;
use VCN\Models\Convocatorias\Abierta as Convocatoria;

use Datatable;
use Input;
use Carbon;

use VCN\Repositories\Criteria\FiltroPlataformaConvocatoria;


class ConvocatoriaOfertasController extends Controller
{
    private $oferta;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Oferta $oferta )
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');

        $this->oferta = $oferta;
    }

    public function getIndex($convocatoria_id=0)
    {
        $this->oferta->pushCriteria(new FiltroPlataformaConvocatoria(true));

        if(Datatable::shouldHandle())
        {
            $col = $this->oferta->all();
            if($convocatoria_id>0)
            {
                $col = Convocatoria::find($convocatoria_id)->ofertas->sortBy('convocatory_open_deals_name');
            }

            return Datatable::collection( $col )
                ->addColumn('convocatoria', function($model) {
                    return $model->convocatoria?$model->convocatoria->convocatory_open_name:"-";
                })
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.convocatorias.ofertas.ficha',[$model->id]) ."'>$model->convocatory_open_deals_name</a>";
                })
                ->addColumn('periodo', function($model) {
                    $ret = Carbon::parse($model->convocatory_open_deals_start_date)->format('d/m/Y');
                    $ret .= " - ";
                    $ret .= Carbon::parse($model->convocatory_open_deals_end_date)->format('d/m/Y');
                    return $ret;
                })
                ->addColumn('rango', function($model) {
                    return "<span class='badge'>$model->convocatory_open_deals_first_week_range</span> <span class='badge'>$model->convocatory_open_deals_second_week_range</span>";
                })
                ->showColumns('convocatory_open_deals_price_per_range')
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Oferta' data-action='". route( 'manage.convocatorias.ofertas.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= "<a href='". route('manage.alojamientos.precios.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Precio</a>";
                    // $ret .= " <a href='". route('manage.alojamientos.cuotas.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Cuota</a>";

                    return $ret;
                })
                ->searchColumns('convocatory_open_deals_name')
                // ->orderColumns('convocatory_close_name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.convocatorias.ofertas.index', compact('convocatoria_id'));
    }

    public function getNuevo($convocatoria_id=0)
    {
        $convocatorias = Convocatoria::pluck('convocatory_open_name','id');

        $convocatoria_name = 0;
        if($convocatoria_id)
        {
            $convocatoria_name = Convocatoria::find($convocatoria_id)->convocatory_open_name;
        }

        return view('manage.convocatorias.ofertas.new', compact('convocatoria_id','convocatoria_name','convocatorias'));
    }

    public function getUpdate($id)
    {
        $convocatorias = Convocatoria::pluck('convocatory_open_name','id');

        $ficha = $this->oferta->find($id);
        return view('manage.convocatorias.ofertas.ficha', compact('ficha','convocatorias'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'convocatory_id' => 'required|numeric|min:1',
            'convocatory_open_deals_name' => 'required|max:255',
        ]);

        $data = Input::except('_token');
        $data['convocatory_open_deals_valid_start_date'] = Carbon::createFromFormat('d/m/Y',$data['convocatory_open_deals_valid_start_date'])->format('Y-m-d');
        $data['convocatory_open_deals_valid_end_date'] = Carbon::createFromFormat('d/m/Y',$data['convocatory_open_deals_valid_end_date'])->format('Y-m-d');

        if(!$id)
        {
            //nuevo
            $o = $this->oferta->create($data);
            $id = $o->id;
        }
        else
        {
            $this->oferta->update($data, $id);
        }

        return redirect()->route('manage.convocatorias.ofertas.ficha',$id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->oferta->delete($id);
        return redirect()->route('manage.convocatorias.ofertas.index');
    }
}
