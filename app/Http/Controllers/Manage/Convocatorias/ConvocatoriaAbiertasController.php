<?php

namespace VCN\Http\Controllers\Manage\Convocatorias;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Convocatorias\AbiertaRepository as Convocatoria;
use VCN\Models\Cursos\Curso;
use VCN\Models\Monedas\Moneda;
use VCN\Models\Cursos\Incluye;
use VCN\Models\Convocatorias\AbiertaIncluye as CursoIncluye;
use VCN\Models\System\Cuestionario;
use VCN\Models\Exams\Examen;
use VCN\Models\Bookings\Booking;
use VCN\Models\Convocatorias\Abierta;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Input;
use Carbon;
use DB;

use VCN\Repositories\Criteria\FiltroPlataformaCurso;

class ConvocatoriaAbiertasController extends Controller
{
    private $convocatoria;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Convocatoria $convocatoria )
    {
        $this->middleware("permiso.plataforma:precios,\VCN\Models\Convocatorias\Abierta", ['only' => ['getUpdate']]);
        $this->checkPermisos('proveedores');

        $this->convocatoria = $convocatoria;
    }

    public function getIndex(Request $request, $curso_id=0)
    {
        // $this->convocatoria->pushCriteria(new FiltroPlataformaCurso());

        if( !$curso_id && Booking::first() )
        {
            $anys = [];
            $a1 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','ASC')->first()->course_start_date)->year;
            $a2 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','DESC')->first()->course_start_date)->year;

            for($i=$a1;$i<=$a2;$i++)
            {
                $anys[$i] = $i;
            }

            $valores['any'] = $request->input('any', Carbon::now()->year);
            $valores['categorias'] = intval($request->input('categorias',0));
            $valores['subcategorias'] = intval($request->input('subcategorias',0));
            $listado = false;

            if($request->has('any'))
            {
                $listado = true;
            }
        }

        if(Datatable::shouldHandle())
        {
            $filtro = ConfigHelper::config('propietario');

            if($curso_id>0)
            {
                // $col = Curso::find($curso_id)->convocatoriasCerradasAll;//->sortBy('course_name');
                $query = DB::table('convocatoria_abiertas')->where('course_id', $curso_id);
            }
            else
            {
                $any = $valores['any'] ."-01-01";
                $anyh = $valores['any'] ."-12-31";

                $cursos = Curso::where('propietario',$filtro)->orWhere('propietario',0)->pluck('id')->toArray();

                $query = DB::table('convocatoria_abiertas')->where('convocatory_open_valid_start_date','>=',$any)->where('convocatory_open_valid_end_date','<=',$anyh)->whereIn('course_id', $cursos);

                $query = $query->select(
                    'convocatoria_abiertas.*',
                    'cursos.course_name AS curso_name'
                )
                ->join('cursos','convocatoria_abiertas.course_id','=','cursos.id');

                if($valores['categorias'])
                {
                    $query = $query->where('cursos.category_id',$valores['categorias']);
                }

                if($valores['subcategorias'])
                {
                    $query = $query->where('cursos.subcategory_id',$valores['subcategorias']);
                }

                $query = $query->orderBy('convocatory_open_name');
            }

            return Datatable::query( $query )
                ->addColumn('nombre', function($column) {
                    return "<a href='". route('manage.convocatorias.abiertas.ficha',[$column->id]) ."'>$column->convocatory_open_name</a>";
                })
                ->addColumn('activo', function($column) {
                    return $column->convocatory_open_status?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('fechas', function($column) {
                    $model = Abierta::find($column->id);
                    return "Del ". $model->start_date ." al ". $model->end_date;
                })
                ->addColumn('curso', function($column) {
                    $model = Abierta::find($column->id);
                    return $model->curso?$model->curso->course_name:"-";
                })
                ->addColumn('duracion_unit', function($column) {

                    $model = Abierta::find($column->id);
                    if($model->precios->count()>0)
                    {
                        return $model->precios->first()->duracion_name;
                    }

                    return "-";
                })
                ->showColumns('convocatory_open_code')
                ->addColumn('options', function($column) {

                    $model = Abierta::find($column->id);

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Convocatoria Abierta' data-action='". route( 'manage.convocatorias.abiertas.delete', $column->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    $ret .= " <a href='". route('manage.convocatorias.abiertas.ficha',$column->id) ."#precios' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Precio</a>";
                    $ret .= " <a href='". route('manage.convocatorias.ofertas.nuevo',$column->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Oferta</a>";

                    return $ret;
                })
                ->searchColumns('course_name')
                ->orderColumns('course_name','convocatory_open_code','curso')
                // ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        $valores['curso_id'] = $curso_id;
        return view('manage.convocatorias.abiertas.index', compact('curso_id', 'anys', 'valores', 'listado'));
    }

    public function getNuevo($curso_id=0)
    {
        $cursos = Curso::pluck('course_name','id');
        $dias = ConfigHelper::getDia();
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();

        $curso = 0;
        if($curso_id)
        {
            $curso = Curso::find($curso_id);
        }

        $incluyes = Incluye::all();

        return view('manage.convocatorias.abiertas.new', compact('curso','cursos','dias','monedas','incluyes'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->convocatoria->find($id);

        if(!$ficha)
        {
            abort(404);
        }

        $cursos = Curso::pluck('course_name','id');
        $dias = ConfigHelper::getDia();
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();

        $regla = 0;
        if($ficha->precios)
        {
            if($ficha->precios->count()>0)
            {
                $regla = $ficha->precios->first();
            }
            else
            {
                $regla = $ficha->curso->precios()->first();
            }
        }

        $incluyes = Incluye::all();
        $cuestionarios = ['' => ''] + Cuestionario::where('activo',1)->pluck('name','id')->toArray();
        $examenes = ['' => ''] + Examen::plataforma()->where('activo',1)->pluck('name','id')->toArray();

        return view('manage.convocatorias.abiertas.ficha', compact('ficha','cursos','dias','monedas','regla','incluyes','cuestionarios','examenes'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        if( $request->has('condiciones') )
        {
            $o = $this->convocatoria->find($id);
            $condiciones = $o->condiciones;

            foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)
            {
                foreach(ConfigHelper::idiomas() as $keyi=>$idioma)
                {
                    if( !isset($condiciones[$keyp]) )
                    {
                        $condiciones[$keyp] = [];
                    }

                    if( !isset($condiciones[$keyp][$idioma]) )
                    {
                        $condiciones[$keyp][$idioma] = "";
                    }

                    $condiciones[$keyp][$idioma] = $request->input("condiciones_$keyp-$idioma");
                }
            }

            $o->condiciones = $condiciones;
            $o->save();
            $o->pdfCondiciones();

            return redirect()->route('manage.convocatorias.abiertas.ficha',$id);
        }

        $this->validate($request, [
            'course_id' => 'required|numeric|min:1',
            'convocatory_open_name' => 'required|max:255',
            'convocatory_open_currency_id' => 'required|numeric|min:1',
        ]);

        $contable_new = $request->get('convocatory_open_code');
        $contable = "";

        $data = $request->except('_token','incluye','monitores');
        $data['convocatory_open_status'] = $request->has('convocatory_open_status');
        $data['convocatory_open_valid_start_date'] = $data['convocatory_open_valid_start_date']!=''?Carbon::createFromFormat('d/m/Y',$data['convocatory_open_valid_start_date'])->format('Y-m-d'):null;
        $data['convocatory_open_valid_end_date'] = $data['convocatory_open_valid_end_date']!=''?Carbon::createFromFormat('d/m/Y',$data['convocatory_open_valid_end_date'])->format('Y-m-d'):null;

        $data['reunion_no'] = $request->has('reunion_no');
        $data['area'] = $request->has('area');
        $data['area_pagos'] = $request->has('area_pagos');
        $data['area_reunion'] = $request->has('area_reunion');
        $data['incluye_horario'] = $request->get('incluye_horario') ?: "";
        $data['reserva_tipo'] = $request->get('reserva_tipo') ?: 0;

         //dd($data);

        if(!$id)
        {
            //nuevo
            $o = $this->convocatoria->create($data);
            $id = $o->id;
        }
        else
        {
            $convocatoria = $this->convocatoria->find($id);
            $contable = $convocatoria->contable;

            $this->convocatoria->update($data, $id);
        }

        $convocatoria = $this->convocatoria->find($id);

        //Updatecontable
        if($contable != $contable_new)
        {
            $convocatoria->updateContable();
        }

        //incluyes
        $incluyes = $request->input('incluye');

        //Primero borramos los checkbox por si desmarcan
        foreach($convocatoria->incluyes as $inc)
        {
            if($inc->incluye->tipo == 0)
            {
                $inc->delete();
            }
        }

        if($incluyes)
        {
            foreach($incluyes as $ik=>$iv)
            {
                $inc = CursoIncluye::where('convocatory_id',$id)->where('incluye_id',$ik)->first();
                if(!$inc)
                {
                    $inc = new CursoIncluye;
                    $inc->convocatory_id = $id;
                    $inc->incluye_id = $ik;
                    $inc->save();
                }

                $inc->valor = $iv;
                $inc->save();

                if($iv==0)
                {
                    // $inc->delete();
                }
            }
        }

        //Monitores
        \VCN\Models\Monitores\MonitorRelacion::refrescar('Abierta',$id, $request->get('monitores'));

        return redirect()->route('manage.convocatorias.abiertas.ficha',$id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->convocatoria->delete($id);
        return redirect()->route('manage.convocatorias.abiertas.index');
    }
}
