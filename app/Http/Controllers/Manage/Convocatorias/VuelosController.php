<?php

namespace VCN\Http\Controllers\Manage\Convocatorias;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Convocatorias\VueloRepository as Vuelo;
use VCN\Repositories\Convocatorias\CerradaVueloRepository as ConvocatoriaVuelo;
use VCN\Repositories\Criteria\FiltroPlataformaConvocatoria;

use VCN\Models\Convocatorias\Cerrada as Convocatoria;

use VCN\Models\Convocatorias\VueloEtapa;
use VCN\Models\Convocatorias\Vuelo as VueloModel;
use VCN\Models\Agencia;
use VCN\Models\Bookings\Booking;

use Datatable;
use Input;
use Session;
use Carbon;
use DB;
use ConfigHelper;

class VuelosController extends Controller
{
    private $vuelo;
    private $convocatoria;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Vuelo $vuelo, ConvocatoriaVuelo $convocatoria )
    {
        $this->checkPermisos('vuelos');

        $this->vuelo = $vuelo;
        $this->convocatoria = $convocatoria;
    }

    public function getIndex(Request $request, $convocatoria_id=0)
    {
        if(!$convocatoria_id)
        {
            $anys = [];
            $a1 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','ASC')->first()->course_start_date)->year;
            $a2 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','DESC')->first()->course_start_date)->year;

            for($i=$a1;$i<=$a2;$i++)
            {
                $anys[$i] = $i;
            }

            $any = $request->input('any')?$request->input('any'):Carbon::now()->year;
            $listado = false;
        }

        if($request->has('any'))
        {
            $listado = true;
        }

        if(Datatable::shouldHandle())
        {
            if($convocatoria_id>0)
            {
                $col = $this->convocatoria->findWhere(['convocatory_id'=>$convocatoria_id]);

                return Datatable::collection( $col )
                    ->addColumn('name', function($model) {
                        return "<a href='". route('manage.convocatorias.vuelos.ficha',[$model->vuelo->id]) ."'>". $model->vuelo->name ."</a>";
                    })
                    ->addColumn('plazas_totales', function($model) {
                        return $model->vuelo->plazas_totales;
                    })
                    ->addColumn('plazas_umbral', function($model) {
                        return $model->vuelo->plazas_umbral;
                    })
                    ->addColumn('plazas_bloqueadas', function($model) {
                        return $model->vuelo->plazas_bloqueadas;
                    })
                    ->addColumn('plazas_reservas', function($model) {
                        return $model->vuelo->plazas_reservas;
                    })
                    ->addColumn('plazas_prereservas', function($model) {
                        return $model->vuelo->plazas_prereservas;
                    })
                    ->addColumn('plazas_overbooking', function($model) {
                        return $model->vuelo->plazas_overbooking;
                    })
                    ->addColumn('plazas_disponibles', function($model) {
                        return $model->vuelo->plazas_disponibles;
                    })
                    ->addColumn('options', function($model) {

                        $ret = "";
                        $data = " data-label='Borrar asignación' data-model='Asignación Convocatoria Vuelo' data-action='". route( 'manage.convocatorias.vuelos.delete.asignar', [$model->id,'vuelos']) . "'";
                        $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                        // $ret .= "<a href='". route('manage.alojamientos.precios.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Precio</a>";
                        // $ret .= " <a href='". route('manage.alojamientos.cuotas.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Cuota</a>";

                        return $ret;
                    })
                    ->searchColumns('name')
                    ->orderColumns('name','plazas_totales')
                    ->setAliasMapping()
                    ->make();
            }

            //Filtros
            // $anyd = $any ."-01-01";
            // $anyh = $any ."-12-31";

            $filtro = ConfigHelper::config('propietario');

            // $qetapas = VueloEtapa::where('tipo',0)->where('salida_fecha','>=',$anyd)->where('salida_fecha','<=',$anyh)->pluck('vuelo_id')->toArray();
            // $query = DB::table('vuelos')->whereIn('id', $qetapas);

            $query = DB::table('vuelos')->where('any', $any);

            $listado = $query->count();

            return Datatable::query( $query )
                ->addColumn('name', function($column) {
                    return "<a href='". route('manage.convocatorias.vuelos.ficha',[$column->id]) ."'>$column->name</a>";
                })
                ->showColumns('plazas_totales','plazas_umbral','plazas_bloqueadas','plazas_monitor')
                // ->showColumns('plazas_reservas','plazas_prereservas','plazas_overbooking','plazas_disponibles')
                ->addColumn('plazas_reservas', function($column) {
                    $model = VueloModel::find($column->id);
                    return $model->plazas_reservas;
                })
                ->addColumn('plazas_prereservas', function($column) {
                    $model = VueloModel::find($column->id);
                    return $model->plazas_prereservas;
                })
                ->addColumn('plazas_overbooking', function($column) {
                    $model = VueloModel::find($column->id);
                    return $model->plazas_overbooking;
                })
                ->addColumn('plazas_disponibles', function($column) {
                    $model = VueloModel::find($column->id);
                    return $model->plazas_disponibles;
                })
                ->addColumn('options', function($column) {

                    $model = VueloModel::find($column->id);

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Vuelo' data-action='". route( 'manage.convocatorias.vuelos.delete', $column->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    $infov = htmlspecialchars($model->info_vuelo);
                    $infovname = htmlspecialchars($model->name);

                    $ret .= " <a href='#info' data-label=\"$infovname\" data-info=\"$infov\" data-toggle='modal' data-target='#modalInfo'><i class='fa fa-info-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','*')
                ->setAliasMapping()
                ->make();
        }

        if(!$convocatoria_id)
        {
            return view('manage.convocatorias.vuelos.index', compact('convocatoria_id', 'anys', 'any', 'listado'));
        }

        return view('manage.convocatorias.vuelos.index', compact('convocatoria_id'));
    }

    public function getConvocatorias($vuelo_id)
    {
        if(Datatable::shouldHandle())
        {
            // $this->convocatoria->pushCriteria(new FiltroPlataformaConvocatoria());

            $col = $this->convocatoria->findWhere(['vuelo_id'=>$vuelo_id]);

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.convocatorias.cerradas.ficha',[$model->convocatoria->id]) ."'>". $model->convocatoria->name ."</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar asignación' data-model='Asignación Convocatoria Vuelo' data-action='". route( 'manage.convocatorias.vuelos.delete.asignar', [$model->id]) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->make();
        }
    }

    public function getBookings($vuelo_id)
    {
        if(Datatable::shouldHandle())
        {
            // $this->convocatoria->pushCriteria(new FiltroPlataformaConvocatoria());
            $vuelo = $this->vuelo->find($vuelo_id);

            $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
            $bookings = Booking::where('vuelo_id',$vuelo_id)->whereIn('status_id',$stplazas);

            $col = $bookings->get();

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.viajeros.ficha',[$model->viajero->id]) ."'>". $model->viajero->full_name ."</a>";
                })
                ->addColumn('convocatoria', function($model) {
                    return $model->convocatoria->name;
                })
                ->addColumn('oficina', function($model) {
                    return $model->oficina?$model->oficina->name:"-";
                })
                ->addColumn('options', function($model) {

                    $ret = "";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->make();
        }
    }

    public function getNuevo(Request $request, $convo_id=0)
    {
        $agencias = Agencia::pluck('name','id');

        return view('manage.convocatorias.vuelos.new',compact('agencias','convo_id'));
    }

    public function getUpdate(Request $request, $id)
    {
        $convocatorias = Convocatoria::plataforma()->sortBy('name')->pluck('name','id');

        $agencias = Agencia::pluck('name','id');

        $ficha = $this->vuelo->find($id);

        return view('manage.convocatorias.vuelos.ficha', compact('ficha','convocatorias','agencias'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $data = $request->except('_token','tab','convocatoria_id');

        if(!$request->has('tab'))
        {
            $this->validate($request, [
                'name' => 'required|max:255',
            ]);

            $data['interno'] = $request->has('interno');
        }

        if($request->has('encuentro_fecha'))
        {
            $data['encuentro_fecha'] = Carbon::createFromFormat('d/m/Y',$data['encuentro_fecha'])->format('Y-m-d');
        }

        if(!$id)
        {
            //nuevo
            $o = $this->vuelo->create($data);
            $id = $o->id;

            $convo_id = $request->get('convocatoria_id');
            if($convo_id)
            {
                $vcdata = [
                    "convocatory_id" => $convo_id,
                    "vuelo_id" => $id
                ];
                $this->convocatoria->create($vcdata);

                $c = Convocatoria::find($convo_id);
                if($c->precio_auto)
                {
                    $c->updatePrecio();
                    Session::flash('mensaje',"Precio Convocatoria ha sido actualizado.");
                }

                Session::flash('tab','#vuelos');
                return redirect()->route('manage.convocatorias.cerradas.ficha',$convo_id);
            }
        }
        else
        {
            $this->vuelo->update($data, $id);
        }

        $vuelo = $this->vuelo->find($id);

        //Precio convos cerradas
        $iCC = 0;
        foreach($vuelo->convocatorias as $convo)
        {
            $c = $convo->convocatoria;
            if($c->precio_auto)
            {
                $p = $c->precio_auto;
                $p->vuelo = $c->precio_vuelo;
                $p->save();

                $c->updatePrecio();

                $iCC++;
            }
        }

        if($iCC)
        {
            Session::flash('mensaje',"Precio de $iCC convocatorias actualizado.");
        }

        $vuelo->billetes = $request->has('billetes');
        if( $request->has('billetes') )
        {
            $log['user'] = $request->user()->full_name;
            $log['datetime'] = Carbon::now()->format('d/m/Y H:i');

            $vuelo->billetes_log = $log;
        }

        if( $request->has('plazas_txt') )
        {
            $vuelo->plazas_txt = $request->get('plazas_txt');
        }

        $vuelo->save();

        Session::flash('tab',$request->input('tab'));

        return redirect()->route('manage.convocatorias.vuelos.ficha',$id);
    }

    public function postAsignar(Request $request)
    {
        $data = $request->except('_token','tab');

        $vid = $data['vuelo_id'];
        if(!$vid)
        {
            Session::flash('tab','#vuelos');
            Session::flash('mensaje-alert', "Debe seleccionar un vuelo.");
            return redirect()->back();
        }

        $this->convocatoria->create($data);

        $vuelo_id = $request->input('vuelo_id');
        $vuelo = $this->vuelo->find($vuelo_id);

        if(!$vuelo)
        {
            Session::flash('mensaje-alert', "Debe seleccionar un vuelo.");
            return redirect()->back();
        }

        $convo_id = $request->input('convocatory_id');
        $c = Convocatoria::find($convo_id);

        //Precio convos cerradas
        if($c->precio_auto)
        {
            $c->updatePrecio();
            Session::flash('mensaje',"Precio Convocatoria ha sido actualizado.");
        }

        $tab = $request->input('tab');
        if($tab=='vuelos')
        {
            Session::flash('tab','#vuelos');
            return redirect()->route('manage.convocatorias.cerradas.ficha',$convo_id);
        }

        Session::flash('tab','#convocatorias');
        return redirect()->route('manage.convocatorias.vuelos.ficha',$vuelo_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->vuelo->delete($id);
        return redirect()->route('manage.convocatorias.vuelos.index');
    }

    public function destroyAsignacion($id,$tab=null)
    {
        $c = $this->convocatoria->find($id);

        $vuelo_id = $c->vuelo_id;
        $convocatory_id = $c->convocatory_id;

        $vuelo = $this->vuelo->find($vuelo_id);

        //Precio convos cerradas
        if($c->convocatoria->precio_auto)
        {
            $c->convocatoria->updatePrecio();

            Session::flash('mensaje',"Precio Convocatoria ha sido actualizado.");
        }

        $this->convocatoria->delete($id);

        if($tab=='vuelos')
        {
            Session::flash('tab','#vuelos');
            return redirect()->route('manage.convocatorias.cerradas.ficha',$convocatory_id);
        }

        Session::flash('tab','#convocatorias');
        return redirect()->route('manage.convocatorias.vuelos.ficha',$vuelo_id);
    }


    /**
     * ETAPAS
     */
    public function postUpdateEtapa(Request $request, $vuelo_id)
    {
        $data = $request->except('_token','etapa_id');

        $data['salida_fecha'] = Carbon::createFromFormat('d/m/Y',$data['salida_fecha'])->format('Y-m-d');
        $data['llegada_fecha'] = Carbon::createFromFormat('d/m/Y',$data['llegada_fecha'])->format('Y-m-d');

        if($request->has('etapa_id'))
        {
            $e = VueloEtapa::find($request->input('etapa_id'));
            $e->update($data);
        }
        else
        {
            $data['vuelo_id'] = $vuelo_id;
            $e = VueloEtapa::create($data);
        }

        //Datos encuentro:
        $ida1 = VueloEtapa::where('vuelo_id', $vuelo_id)->where('tipo',0)->first();
        if($ida1)
        {
            if($ida1->id == $e->id)
            {
                $vuelo = $ida1->vuelo_parent;
                $vuelo->encuentro_fecha = $ida1->salida_fecha;
                $vuelo->encuentro_hora = Carbon::parse($ida1->salida_hora)->subHours(2)->format('H:i');
                $vuelo->encuentro_lugar = $ida1->salida_aeropuerto;
                $vuelo->encuentro_punto = $ida1->company;

                $vuelo->save();
            }
        }

        Session::flash('tab','#etapas');
        return redirect()->route('manage.convocatorias.vuelos.ficha',$vuelo_id);
    }

    public function destroyEtapa($id)
    {
        $e = VueloEtapa::find($id);
        $vuelo_id = $e->vuelo_id;

        $e->delete();

        Session::flash('tab','#etapas');
        return redirect()->route('manage.convocatorias.vuelos.ficha',$vuelo_id);
    }

}
