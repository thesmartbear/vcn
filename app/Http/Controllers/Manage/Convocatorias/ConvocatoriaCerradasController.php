<?php

namespace VCN\Http\Controllers\Manage\Convocatorias;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Convocatorias\CerradaRepository as Convocatoria;
use VCN\Models\Cursos\Curso;
use VCN\Models\Monedas\Moneda;
use VCN\Models\Cursos\Incluye;
use VCN\Models\Convocatorias\CerradaIncluye as CursoIncluye;
use VCN\Models\Convocatorias\Plaza;
use VCN\Models\Convocatorias\CerradaPrecio;
use VCN\Models\Convocatorias\Vuelo;
use VCN\Models\Alojamientos\Alojamiento;
use VCN\Models\Bookings\Booking;
use VCN\Models\System\Cuestionario;
use VCN\Models\Exams\Examen;
use VCN\Models\Monedas\Cambio;
use VCN\Models\Monedas\CambioMoneda;
use VCN\Models\Monedas\CerradaCambio;
use VCN\Models\Monedas\CerradaCambioMoneda;
use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Centros\Centro;
use VCN\Models\Convocatorias\Cerrada;
use VCN\Models\Pais;
use VCN\Models\Categoria;
use VCN\Models\Subcategoria;

use VCN\Models\Leads\Origen;
use VCN\Models\Leads\Suborigen;
use VCN\Models\Leads\SuborigenDetalle;
use VCN\Models\Prescriptores\Prescriptor;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Input;
use Carbon;
use Session;
use DB;

use VCN\Repositories\Criteria\FiltroPlataformaCurso;

class ConvocatoriaCerradasController extends Controller
{
    private $convocatoria;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Convocatoria $convocatoria )
    {
        $this->middleware("permiso.plataforma:precios,\VCN\Models\Convocatorias\Cerrada", ['only' => ['getUpdate']]);
        $this->checkPermisos('proveedores');

        $this->convocatoria = $convocatoria;
    }

    public function getIndex(Request $request, $curso_id=0)
    {
        // $this->convocatoria->pushCriteria(new FiltroPlataformaCurso());

        if( !$curso_id && Booking::first() )
        {
            $anys = [];
            $a1 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','ASC')->first()->course_start_date)->year;
            $a2 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','DESC')->first()->course_start_date)->year;

            for($i=$a1;$i<=$a2;$i++)
            {
                $anys[$i] = $i;
            }

            $valores['any'] = $request->input('any', Carbon::now()->year);
            $valores['categorias'] = intval($request->input('categorias',0));
            $valores['subcategorias'] = intval($request->input('subcategorias',0));
            $listado = false;


            if($request->has('desactivar') || $request->has('desactivar-web') )
            {
                $filtro = ConfigHelper::config('propietario');
                $any = $valores['any'] ."-01-01";
                $anyh = $valores['any'] ."-12-31";

                $cursos = Curso::where('propietario',$filtro)->orWhere('propietario',0)->pluck('id')->toArray();

                $query = DB::table('convocatoria_cerradas')->where('convocatory_close_start_date','>=',$any)->where('convocatory_close_start_date','<=',$anyh)->whereIn('course_id', $cursos);

                $query = $query->select(
                    'convocatoria_cerradas.*'
                )
                ->join('cursos','convocatoria_cerradas.course_id','=','cursos.id');

                if($valores['categorias'])
                {
                    $query = $query->where('cursos.category_id',$valores['categorias']);
                }

                if($valores['subcategorias'])
                {
                    $query = $query->where('cursos.subcategory_id',$valores['subcategorias']);
                }

                if($request->has('desactivar'))
                {
                    $campo = 'convocatory_close_status';
                    $query = $query->where($campo,1);

                    $tipo = "Activas";
                }

                if($request->has('desactivar-web'))
                {
                    $campo = "convocatoria_cerradas.activo_web";
                    $query = $query->where($campo,1);

                    $tipo = "Activas web";
                }

                $tot = $query->count();

                if($tot)
                {
                    $ids = $query->pluck('id');
                    DB::table('convocatoria_cerradas')->whereIn('id',$ids)->update([$campo=> 0]);
                }

                $txt = "Se han desactivado $tot $tipo.";
                Session::flash('mensaje-ok', $txt);
                return redirect()->back();
            }

            if($request->has('any'))
            {
                $listado = true;
            }
        }

        if(Datatable::shouldHandle())
        {
            // $col = $this->convocatoria->all();//->sortBy('convocatory_close_name');

            $filtro = ConfigHelper::config('propietario');

            if($curso_id)
            {
                // $col = Curso::find($curso_id)->convocatoriasCerradasAll;//->sortBy('course_name');
                $query = DB::table('convocatoria_cerradas')->where('course_id', $curso_id);
            }
            else
            {
                $any = $valores['any'] ."-01-01";
                $anyh = $valores['any'] ."-12-31";

                $cursos = Curso::where('propietario',$filtro)->orWhere('propietario',0)->pluck('id')->toArray();

                $query = DB::table('convocatoria_cerradas')->where('convocatory_close_start_date','>=',$any)->where('convocatory_close_start_date','<=',$anyh)->whereIn('course_id', $cursos);

                $query = $query->select(
                    'convocatoria_cerradas.*',
                    'cursos.course_name AS curso_name'
                )
                ->join('cursos','convocatoria_cerradas.course_id','=','cursos.id');

                if($valores['categorias'])
                {
                    $query = $query->where('cursos.category_id',$valores['categorias']);
                }

                if($valores['subcategorias'])
                {
                    $query = $query->where('cursos.subcategory_id',$valores['subcategorias']);
                }

                $query = $query->orderBy('convocatory_close_name');
            }

            // dd($query->count());

            return Datatable::query( $query )
                ->addColumn('nombre', function($column) {
                    return "<a href='". route('manage.convocatorias.cerradas.ficha',[$column->id]) ."'>$column->convocatory_close_name</a>";
                })
                ->addColumn('activo', function($column) {
                    return $column->convocatory_close_status?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('activo_web', function($column) {
                    return $column->activo_web?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                // ->addColumn('vuelo', function($model) {
                //     $vuelos = "";
                //     foreach($model->vuelos as $v)
                //     {
                //         $vuelos .= "<a href='". route('manage.convocatorias.vuelos.ficha',[$v->id]) ."'>$v->name</a>";

                //         $data = " data-label='Borrar' data-model='Vuelo' data-action='". route( 'manage.convocatorias.vuelos.delete', $v->id) . "'";
                //         $vuelos .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                //         $vuelos .= "<br>";
                //     }
                //     return $vuelos;
                // })
                ->addColumn('curso', function($column) {
                    $model = Cerrada::find($column->id);
                    return $model->curso?$model->curso->name:"-";
                })
                ->addColumn('duracion_unit', function($column) {
                    $model = Cerrada::find($column->id);
                    return $model->duracion_name;
                })
                ->showColumns('convocatory_close_code')
                ->addColumn('options', function($column) {

                    $model = Cerrada::find($column->id);

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Convocatoria Cerrada' data-action='". route( 'manage.convocatorias.cerradas.delete', $column->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    $ret .= " <a href='". route('manage.convocatorias.vuelos.nuevo',$column->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Vuelo</a>";
                    // $ret .= " <a href='". route('manage.alojamientos.cuotas.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Cuota</a>";

                    if(ConfigHelper::config('facturas'))
                    {
                        if(!$model->no_facturar && $model->es_facturable )
                        {
                            $ret .= " <a data-label='Emitir facturas' href='". route('manage.bookings.facturar',['Cerrada',$model->id]) ."' class='btn btn-success btn-xs'><i class='fa fa-ticket'></i></a>";
                        }
                    }

                    $data = " data-titulo='". $column->convocatory_close_name ."' data-id='$column->id' data-ajax='". route( 'manage.convocatorias.cerradas.ajax.info', $column->id) ."'";
                    $ret .= " <a class='cc-info' href='#cc-info' $data data-toggle='modal' data-target='#modalInfoPlazas'><i class='fa fa-info-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('cursos.course_name')
                ->orderColumns('curso','convocatory_close_code')
                // ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        $valores['curso_id'] = $curso_id;
        return view('manage.convocatorias.cerradas.index', compact('curso_id', 'anys', 'valores', 'listado'));
    }

    public function getIndexInfo(Request $request)
    {
        $user = $request->user();

        $anys = [];
        $a1 = Carbon::parse(Booking::whereNotNull('course_start_date')->orderBy('course_start_date','ASC')->first()->course_start_date)->year;
        $a2 = Carbon::parse(Booking::whereNotNull('course_start_date')->orderBy('course_start_date','DESC')->first()->course_start_date)->year;

        for($i=$a1;$i<=$a2;$i++)
        {
            $anys[$i] = $i;
        }

        $paises = [0=>'Todos'] + Pais::all()->sortBy('name')->pluck('name','id')->toArray();

        $cat0 = 0;
        $scat0 = 0;

        $c = Categoria::where('name', 'Jóvenes')->first();
        if($c)
        {
            $cat0 = $c->id;
        }

        $c = Subcategoria::where('name', 'Grupos con monitor')->first();
        if($c)
        {
            $scat0 = $c->id;
        }

        $valores['any'] = $request->input('any', Carbon::now()->year);
        $valores['categorias'] = intval($request->input('categorias',$cat0));
        $valores['subcategorias'] = intval($request->input('subcategorias',$scat0));

        $valores['paises'] = intval($request->input('paises',0));
        $valores['semanas'] = intval($request->input('semanas',0));
        $valores['solo_plazas'] = (bool)$request->input('solo_plazas');
        $valores['semi'] = (int)$request->get('semi',0);

        $listado = false;

        if($request->has('any'))
        {
            $listado = true;
            if(Datatable::shouldHandle())
            {
                $any = $valores['any'] ."-01-01";
                $anyh = $valores['any'] ."-12-31";

                $filtro = ConfigHelper::config('propietario');
                $cursos = Curso::where('propietario',$filtro)->orWhere('propietario',0)->pluck('id')->toArray();

                $query = DB::table('convocatoria_cerradas')->where('convocatory_close_start_date','>=',$any)->where('convocatory_close_start_date','<=',$anyh)->whereIn('course_id', $cursos);

                // convocatory_semiopen
                if($valores['semi'] == 0)
                {
                    $query->where('convocatory_semiopen', 0);
                }
                if($valores['semi'] == 2)
                {
                    $query->where('convocatory_semiopen', 1);
                }

                $query = $query->select(
                    'convocatoria_cerradas.*',
                    'cursos.course_name AS curso_name',
                    'alojamientos.accommodation_name AS alojamiento_name',
                    'centros.name as centro_name'
                )
                ->join('cursos','convocatoria_cerradas.course_id','=','cursos.id')
                ->join('centros','cursos.center_id','=','centros.id')
                ->join('alojamientos','convocatoria_cerradas.alojamiento_id','=','alojamientos.id');

                if($valores['categorias'])
                {
                    $query = $query->where('cursos.category_id',$valores['categorias']);
                }

                if($valores['subcategorias'])
                {
                    $query = $query->where('cursos.subcategory_id',$valores['subcategorias']);
                }

                if($valores['paises'])
                {
                    $query = $query->where('centros.country_id',$valores['paises']);
                }

                if($valores['semanas'])
                {
                    $query = $query->where('convocatoria_cerradas.convocatory_close_duration_weeks',$valores['semanas']);
                }


                $query = $query->orderBy('convocatory_close_name');
                $col = Cerrada::hydrate($query->get()->toArray());

                if($valores['solo_plazas'])
                {
                    $col_plazas = collect();
                    foreach($col as $c)
                    {
                        if($c->alojamiento)
                        {
                            $plazas = $c->getPlazas($c->alojamiento_id);
                            $p = $plazas?$plazas->plazas_disponibles:0;
                            if($p>0)
                            {
                                $col_plazas->push($c);
                            }
                        }
                    }

                    $col = $col_plazas;
                }

                return Datatable::collection( $col )
                // return Datatable::query( $query )
                    ->addColumn('name', function($column) {
                        return $column->convocatory_close_name;
                    })
                    ->addColumn('tipo', function($column) {
                        return $column->convocatory_semiopen?"Semi":"Cerrada";
                    })
                    ->addColumn('activo', function($column) {
                        return $column->convocatory_close_status?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                    })
                    ->addColumn('curso', function($column) {
                        return $column->curso_name;
                    })
                    ->addColumn('pais', function($column) {
                        return $column->curso->centro->pais_name;
                    })
                    ->addColumn('alojamiento', function($column) {
                        return $column->alojamiento_name;
                    })
                    ->addColumn('plazas', function($column) {
                        // $model = Cerrada::find($column->id);
                        $model = $column;

                        if($model->alojamiento)
                        {
                            $plazas = $model->getPlazas($model->alojamiento->id);
                            return $plazas?$plazas->plazas_disponibles:0;
                        }

                        return 0;
                    })
                    ->addColumn('reservas', function($column) {
                        // $model = Cerrada::find($column->id);
                        $model = $column;

                        if($model->alojamiento)
                        {
                            $plazas = $model->getPlazas($model->alojamiento->id);
                            return $plazas?$plazas->plazas_reservas:0;
                        }
                        return 0;

                    })
                    ->addColumn('vuelos', function($column) {
                        // $model = Cerrada::find($column->id);
                        $model = $column;

                        $ret = "";
                        foreach( $model->vuelos as $vuelo )
                        {
                            $infov = htmlspecialchars($vuelo->vuelo->info_vuelo);
                            $infovname = htmlspecialchars($vuelo->vuelo->name);

                            $ret .= " <a class='cc-infov' href='#info' data-label=\"$infovname\" data-info=\"$infov\" data-toggle='modal' data-target='#modalInfo'><i class='fa fa-info-circle'></i></a>";
                        }

                        return $ret;

                    })
                    ->addColumn('plazasv', function($column) {
                        // $model = Cerrada::find($column->id);
                        $model = $column;

                        $ret = 0;
                        foreach( $model->vuelos as $vuelo )
                        {
                            $ret += $vuelo->vuelo->plazas_disponibles;
                        }

                        return $ret;

                    })
                    ->addColumn('semanas', function($column) {
                        // $model = Cerrada::find($column->id);
                        $model = $column;

                        return $model->duracion;
                    })
                    ->addColumn('options', function($column) {

                        $ret = "";

                        $data = " data-titulo='". $column->convocatory_close_name ."' data-id='$column->id' data-ajax='". route( 'manage.convocatorias.cerradas.ajax.info', $column->id) ."'";
                        $ret .= " <a class='cc-info' href='#cc-info' $data data-toggle='modal' data-target='#modalInfoPlazas'><i class='fa fa-info-circle'></i></a>";

                        return $ret;
                    })
                    ->searchColumns('name','curso','alojamiento')
                    ->orderColumns('name','curso','plazas','reservas','plazasv')
                    ->setSearchStrip()->setOrderStrip()
                    ->setAliasMapping()
                    ->make();
            }
        }

        return view('manage.convocatorias.cerradas.index_info', compact('anys', 'valores', 'listado','paises'));
    }


    public function ajaxInfo(Request $request, $convocatory_id)
    {
        if( !$request->ajax() )
        {
            abort(404);
        }

        $ficha = $this->convocatoria->find($convocatory_id);

        $res = false;

        //Alojamiento
        $infoa = [];
        // foreach( $ficha->curso->alojamientos as $alojamiento )
        $alojamiento = $ficha->alojamiento;

        if($alojamiento)
        {
            $infoa[$alojamiento->id]['name'] = $alojamiento->name;

            $plazas = $ficha->getPlazas($alojamiento->id);

            $infoa[$alojamiento->id]['plazas_totales'] = $plazas?$plazas->plazas_totales:0;
            $infoa[$alojamiento->id]['plazas_reservas'] = $plazas?$plazas->plazas_reservas:0;
            $infoa[$alojamiento->id]['plazas_prereservas'] = $plazas?$plazas->plazas_prereservas:0;
            $infoa[$alojamiento->id]['plazas_overbooking'] = $plazas?$plazas->plazas_overbooking:0;
            $infoa[$alojamiento->id]['plazas_disponibles'] = $plazas?$plazas->plazas_disponibles:0;
            $infoa[$alojamiento->id]['plazas_bloqueadas'] = $plazas?$plazas->plazas_bloqueadas:0;
            $infoa[$alojamiento->id]['plazas_proveedor'] = $plazas?$plazas->plazas_proveedor:0;
        }

        //Transporte
        $infot = [];
        foreach( $ficha->vuelos as $vuelo )
        {
            $infot[$vuelo->vuelo->id]['name'] = $vuelo->vuelo->name ." [". $vuelo->vuelo->localizador ."]";

            $infot[$vuelo->vuelo->id]['plazas_totales'] = $vuelo->vuelo->plazas_totales;
            $infot[$vuelo->vuelo->id]['plazas_reservas'] = $vuelo->vuelo->plazas_reservas;
            $infot[$vuelo->vuelo->id]['plazas_prereservas'] = $vuelo->vuelo->plazas_prereservas;
            $infot[$vuelo->vuelo->id]['plazas_overbooking'] = $vuelo->vuelo->plazas_overbooking;
            $infot[$vuelo->vuelo->id]['plazas_disponibles'] = $vuelo->vuelo->plazas_disponibles;
            $infot[$vuelo->vuelo->id]['plazas_bloqueadas'] = $vuelo->vuelo->plazas_bloqueadas;
            $infot[$vuelo->vuelo->id]['plazas_umbral'] = $vuelo->vuelo->plazas_umbral;
        }

        //Edades :: por bookings??
        $infoe = [];
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        $bookings = Booking::where('convocatory_close_id',$ficha->id)->whereIn('status_id',$stplazas);
        foreach( $bookings->get() as $booking )
        {
            //Edad - chicas - chicos
            if( !isset($infoe[$booking->edad]) )
            {
                // $infoe[$booking->edad] = 0;
                $infoe[$booking->edad]['chicas'] = 0;
                $infoe[$booking->edad]['chicos'] = 0;
                $infoe[$booking->edad]['nulo'] = 0;
            }
            $infoe[$booking->edad]++;

            switch($booking->sexo)
            {
                case 1:
                {
                    $infoe[$booking->edad]['chicos']++;
                }
                break;

                case 2:
                {
                    $infoe[$booking->edad]['chicas']++;
                }
                break;

                default:
                {
                    $infoe[$booking->edad]['nulo']++;
                }
                break;
            }

        }

        $res = true;

        $result = [ 'info_transporte'=> $infot, 'info_alojamiento'=> $infoa, 'info_edades'=> $infoe, 'result'=> $res];
        return response()->json($result, 200);
    }

    public function getNuevo($curso_id=0)
    {
        $cursos = Curso::pluck('course_name','id');
        $dias = ConfigHelper::getDia();
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();

        $descuentos_early = ['' => 'No'] + \VCN\Models\Descuentos\DescuentoEarly::all()->pluck('detalle','id')->toArray();

        $curso = 0;
        $alojamientos = null;
        if($curso_id)
        {
            $curso = Curso::find($curso_id);
            // $alojamientos = Alojamiento::where('center_id',$curso->centro->id)->get()->pluck('name','id')->toArray();
            $alojamientos = [0=> 'SIN ALOJAMIENTO'] + $curso->alojamientos->pluck('name','id')->toArray();
        }

        $incluyes = Incluye::all();

        $conocidos = [""=>""] + Origen::plataforma()->pluck('name','id')->toArray();
        $prescriptores = [""=>""] + Prescriptor::plataforma()->pluck('name','id')->toArray();

        return view('manage.convocatorias.cerradas.new', compact('curso','cursos','alojamientos','dias','monedas','incluyes','descuentos_early','prescriptores','conocidos'));
    }

    public function getUpdate($id)
    {
        ini_set('memory_limit', '512M');

        $cursos = Curso::pluck('course_name','id');
        $dias = ConfigHelper::getDia();
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();

        $ficha = $this->convocatoria->find($id);

        $incluyes = Incluye::all();

        //$vuelos = Vuelo::all()->pluck('name','id');

        $vuelos = \VCN\Models\Convocatorias\Vuelo::where('any', $ficha->any)->pluck('name','id')->toArray();

        if($ficha->curso)
        {
            $alojamientos = [0=> 'SIN ALOJAMIENTO'] + $ficha->curso->alojamientos->pluck('name','id')->toArray();
        }
        else
        {
            $alojamientos = [0=> 'SIN ALOJAMIENTO'] + Alojamiento::where('center_id',$ficha->curso->centro->id)->get()->pluck('name','id')->toArray();
        }

        $descuentos_early = ['' => 'No'] + \VCN\Models\Descuentos\DescuentoEarly::all()->pluck('detalle','id')->toArray();
        $cuestionarios = ['' => ''] + Cuestionario::plataforma()->where('activo',1)->pluck('name','id')->toArray();

        $bFacturas = $ficha->es_facturable;
        $oficinas = [0=>'']  + \VCN\Models\System\Oficina::plataforma()->pluck('name', 'id')->toArray();
        $asignados = [0=>'']  + \VCN\Models\User::asignados()->pluck('username', 'id')->toArray();

        $cinfo = $ficha->corporativo_info;
        $sub = isset($cinfo['suborigen_id']) ? $cinfo['suborigen_id'] : 0;
        $subd = isset($cinfo['suborigendet_id']) ? $cinfo['suborigendet_id'] : 0;

        $prescriptores = [""=>""] + Prescriptor::plataforma()->pluck('name','id')->toArray();
        $conocidos = [""=>""] + Origen::plataforma()->pluck('name','id')->toArray();
        $conocidosSub = [""=>""] + Suborigen::where('id', $sub)->pluck('name','id')->toArray();
        $conocidosSubD = [""=>""] + SuborigenDetalle::where('id', $subd)->pluck('name','id')->toArray();

        return view('manage.convocatorias.cerradas.ficha', compact(
            'ficha','cursos','alojamientos','dias','monedas','incluyes','vuelos','descuentos_early','cuestionarios',
            'bFacturas','oficinas','asignados',
            'conocidos','conocidosSub','conocidosSubD','prescriptores'
        ));
    }

    public function postUpdate(Request $request, $id=0)
    {
        if( $request->has('condiciones') )
        {
            $o = $this->convocatoria->find($id);
            $condiciones = $o->condiciones;

            foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)
            {
                foreach(ConfigHelper::idiomas() as $keyi=>$idioma)
                {
                    if( !isset($condiciones[$keyp]) )
                    {
                        $condiciones[$keyp] = [];
                    }

                    if( !isset($condiciones[$keyp][$idioma]) )
                    {
                        $condiciones[$keyp][$idioma] = "";
                    }

                    $condiciones[$keyp][$idioma] = $request->input("condiciones_$keyp-$idioma");
                }
            }

            $o->condiciones = $condiciones;
            $o->save();
            $o->pdfCondiciones();

            return redirect()->route('manage.convocatorias.cerradas.ficha',$id);
        }


        $this->validate($request, [
            'course_id' => 'required|numeric|min:1',
            // 'alojamiento_id' => 'required|numeric|min:1',
            'convocatory_close_name' => 'required|max:255',
            'convocatory_close_start_date' => 'required',
            'convocatory_close_end_date' => 'required',
        ]);

        $contable_new = $request->get('convocatory_close_code');
        $contable = "";

        $data = $request->except('_token','incluye','monitores','corporativo_info','origen_id','suborigen_id','suborigendet_id');
        $data['convocatory_close_status'] = $request->has('convocatory_close_status');
        $data['convocatory_semiopen'] = $request->has('convocatory_semiopen');
        $data['convocatory_close_start_date'] = Carbon::createFromFormat('d/m/Y',$data['convocatory_close_start_date'])->format('Y-m-d');
        $data['convocatory_close_end_date'] = Carbon::createFromFormat('d/m/Y',$data['convocatory_close_end_date'])->format('Y-m-d');
        $data['activo_web'] = $request->has('activo_web');
        $data['activo_corporativo'] = $request->has('activo_corporativo');
        $data['reserva_tipo'] = $request->get('reserva_tipo') ?: 0;

        $data['reunion_no'] = $request->has('reunion_no');
        $data['area'] = $request->has('area');
        $data['area_pagos'] = $request->has('area_pagos');
        $data['area_reunion'] = $request->has('area_reunion');
        $data['no_facturar'] = $request->has('no_facturar');

        $data['alojamiento_id'] = $request->has('alojamiento_id')?$request->input('alojamiento_id'):0;

        $data['dto_early'] = $request->get('dto_early') ?: 0;
        $data['incluye_horario'] = $request->get('incluye_horario') ?: "";

        $convo0 = null;

        if(!$id)
        {
            //nuevo
            $o = $this->convocatoria->create($data);
            $id = $o->id;
        }
        else
        {
            $convocatoria = $this->convocatoria->find($id);
            $contable = $convocatoria->contable;

            $convo0 = clone $convocatoria;

            $this->convocatoria->update($data, $id);
        }

        $convocatoria = $this->convocatoria->find($id);

        //activo_corporativo
        $cinfo = null;
        if($convocatoria->activo_corporativo)
        {
            $cinfo = $request->get('corporativo_info');
            $cinfo['pago'] = isset($cinfo['pago']) ? $cinfo['pago'] : 0;
            $cinfo['origen_id'] = $request->get('origen_id') ?: 0;
            $cinfo['suborigen_id'] = $request->get('suborigen_id') ?: 0;
            $cinfo['suborigendet_id'] = $request->get('suborigendet_id') ?: 0;
        }
        $convocatoria->corporativo_info = $cinfo;
        $convocatoria->save();

        //Updatecontable
        if($contable != $contable_new)
        {
            $convocatoria->updateContable();
        }

        //incluyes
        $incluyes = $request->input('incluye');

        //Primero borramos los checkbox por si desmarcan
        foreach($convocatoria->incluyes as $inc)
        {
            if($inc->incluye->tipo == 0)
            {
                $inc->delete();
            }
        }

        if($incluyes)
        {
            foreach($incluyes as $ik=>$iv)
            {
                $inc = CursoIncluye::where('convocatory_id',$id)->where('incluye_id',$ik)->first();
                if(!$inc)
                {
                    $inc = new CursoIncluye;
                    $inc->convocatory_id = $id;
                    $inc->incluye_id = $ik;
                    $inc->save();
                }

                $inc->valor = $iv;
                $inc->save();

                if($iv==0)
                {
                    // $inc->delete();
                }
            }
        }

        //actualizar duracion
        if($convo0)
        {
            $d = $convo0->duracion;
            $dt = $convo0->duracion_fijo;

            $d1 = $convocatoria->duracion;
            $dt1 = $convocatoria->duracion_fijo;

            if($d != $d1 || $dt != $dt1)
            {
                $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id')->toArray();
                $bookings = Booking::where('convocatory_close_id', $convocatoria->id)->whereIn('status_id', $stplazas)->get();

                if($bookings && $bookings->count())
                {
                    Session::flash('mensaje-ok', "Se han actualizado ". $bookings->count() ." bookings.");

                    foreach($bookings as $b)
                    {
                        $b->weeks = $d1;
                        $b->semanas = $b->semanas_unit;
                        $b->duracion_fijo = $dt1;
                        $b->accommodation_weeks = $b->weeks;
                        $b->save();
                        
                        \VCN\Models\Informes\Venta::remove($b);
                        \VCN\Models\Informes\Venta::add($b, true);
                    }
                }
            }
        }

        //Monitores
        \VCN\Models\Monitores\MonitorRelacion::refrescar('Cerrada',$id, $request->get('monitores'));

        return redirect()->route('manage.convocatorias.cerradas.ficha',$id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->convocatoria->delete($id);
        return redirect()->route('manage.convocatorias.cerradas.index');
    }


    public function postPlazas(Request $request, $id=0)
    {
        $convocatoria = $this->convocatoria->find($id);

        $data = $request->except('_token');

        foreach($convocatoria->curso->alojamientos as $alojamiento)
        {
            $p = Plaza::where('convocatory_id',$id)->where('alojamiento_id',$alojamiento->id)->first();
            if(!$p)
            {
                $p = new Plaza;
                $p->convocatory_id = $id;
                $p->alojamiento_id = $alojamiento->id;
                $p->save();
            }

            $p->plazas_totales = $request->input("plazas_totales")[$alojamiento->id];
            $p->plazas_bloqueadas = $request->input("plazas_bloqueadas")[$alojamiento->id];
            $p->plazas_proveedor = $request->input("plazas_proveedor")[$alojamiento->id];
            $p->save();
        }

        $convocatoria->plazas_txt = $request->input("plazas_txt");
        $convocatoria->save();

        Session::flash('tab','#plazas');
        return redirect()->route('manage.convocatorias.cerradas.ficha',$id);
    }

    public function postPrecio(Request $request, $id)
    {
        Session::flash('tab','#precio');

        if($request->has('submit_divisa'))
        {
            $convocatoria = $this->convocatoria->find($id);

            $vd = (float)$request->get('divisa_variacion');
            $convocatoria->setDivisa($vd);

            Session::flash('tab','#precio');
            return redirect()->back();
        }

        $this->validate($request, [
            'moneda_id' => 'required|numeric|min:1',
        ]);

        $convocatoria = $this->convocatoria->find($id);
        $convocatoria->convocatory_close_currency_id = $request->moneda_id;
        $convocatoria->save();

        $data = $request->all();
        $divisa_variacion = $data['divisa_variacion'] ?: 0;
        $convocatoria->updatePrecio($divisa_variacion, $data);

        return redirect()->route('manage.convocatorias.cerradas.ficha',$id);
    }

    public function postCambio(Request $request, $id)
    {
        $convocatoria = $this->convocatoria->find($id);

        $cambio_id = $request->get('cambio_id');

        if($request->has('importar'))
        {
            $cambio = Cambio::find($cambio_id);

            $c = new CerradaCambio;
            $c->convocatory_id = $id;
            $c->cambio_id = $cambio_id;
            $c->save();

            foreach($cambio->cambios as $m)
            {
                $cm = new CerradaCambioMoneda;
                $cm->cambio_id = $c->id;
                $cm->moneda_id = $m->moneda_id;
                $cm->tc_calculo = $m->tc_calculo;
                $cm->tc_folleto = $m->tc_folleto;
                $cm->tc_final = $m->tc_final;
                $cm->save();
            }
        }
        else
        {
            $cambios = $request->input('cambios');

            $ficha = CerradaCambio::find($cambio_id);

            $i = 0;
            foreach($cambios['moneda_id'] as $m)
            {
                $cm = $ficha->cambios->where('moneda_id',(int)$m)->first();

                if(!$cm)
                {
                    $cm = new CerradaCambioMoneda;
                    $cm->cambio_id = $cambio_id;
                    $cm->moneda_id = $m;
                    $cm->save();
                }

                $cm->tc_calculo = $cambios['tc_calculo'][$i];
                $cm->tc_folleto = $cambios['tc_folleto'][$i];
                $cm->tc_final = $cambios['tc_final'][$i];
                $cm->save();

                $i++;
            }
        }

        $convocatoria->updatePrecio();

        Session::flash('tab','#precio');
        return redirect()->back();
    }
}
