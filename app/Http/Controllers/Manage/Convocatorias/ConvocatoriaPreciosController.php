<?php

namespace VCN\Http\Controllers\Manage\Convocatorias;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Convocatorias\PrecioRepository as Precio;
use VCN\Models\Monedas\Moneda;
use VCN\Models\Convocatorias\Abierta;
use VCN\Models\Convocatorias\PrecioExtra;

use Datatable;
use Session;
use Carbon;

use VCN\Helpers\ConfigHelper;

class ConvocatoriaPreciosController extends Controller
{
    private $precio;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Precio $precio )
    {
        $this->checkPermisos('precios');

        $this->precio = $precio;
    }


    public function getIndex($convocatoria_id=0, $extras=false)
    {
        if(Datatable::shouldHandle())
        {
            if($extras)
            {
                if($convocatoria_id>0)
                {
                    $col = PrecioExtra::where('convocatory_id',$convocatoria_id)->get();
                }
                else
                {
                    $col = PrecioExtra::all();
                }
            }
            else
            {
                if($convocatoria_id>0)
                {
                    $col = $this->precio->findAllBy('convocatory_id',$convocatoria_id);
                }
                else
                {
                    $col = $this->precio->all();
                }
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return $model->name;
                    // return "<a href='". route('manage.convocatorias.precios.ficha',[$model->id,$extras]) ."'>$model->name</a>";
                })
                ->addColumn('precio', function($model) {
                    return $model->importe ." ". $model->moneda_name;
                })
                ->addColumn('regla', function($model) {
                    return ConfigHelper::getPrecioRegla($model);
                })
                ->addColumn('periodo', function($model) {
                    if(!$model->desde)
                    {
                        return "Sin período";
                    }
                    return Carbon::parse($model->desde)->format('d/m/Y') ." - ". Carbon::parse($model->hasta)->format('d/m/Y');
                })
                ->addColumn('options', function($model) use ($extras) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Precio' data-action='". route( 'manage.convocatorias.precios.delete', [$model->id,$extras]) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= "<a href='$model->id' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Centro</a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('*','duracion','rango1')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.convocatorias.precios.index', compact('convocatoria_id'));
    }

    public function getUpdate($id, $extras=false)
    {
        $ficha = $extras?PrecioExtra::find($id):$this->precio->find($id);

        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();

        return view('manage.convocatorias.precios.ficha', compact('ficha', 'monedas', 'extras'));
    }


    public function postUpdate(Request $request, $id=0, $extras=false)
    {
        if($extras)
        {
            Session::flash('tab','#precio-extras');
        }
        else
        {
            Session::flash('tab','#precios');
        }

        if($request->has('test'))
        {
            $desde = $request->input('desde');
            $hasta = $request->input('hasta');
            $convocatory_id = $request->input('convocatory_id');
            $test = $request->input('test');

            $convo = Abierta::find($convocatory_id);

            $p = $convo->calcularPrecio($desde,$hasta,$test);

            Session::flash('test',$p?ConfigHelper::parseMoneda($p['importe'],$p['moneda']):"-");
            return redirect()->route('manage.convocatorias.abiertas.ficha',$convocatory_id)->withInput();
        }

        $this->validate($request, [
            'desde'     => 'required',
            'hasta'     => 'required',
            'moneda_id' => 'required',
            'importe'   => 'required',
        ]);

        $data = $request->except('_token');

        $data['desde'] = Carbon::createFromFormat('d/m/Y',$data['desde'])->format('Y-m-d');
        $data['hasta'] = Carbon::createFromFormat('d/m/Y',$data['hasta'])->format('Y-m-d');

        if(!$id)
        {
            //nuevo
            if($extras)
            {
                $o = PrecioExtra::create($data);
                $convocatoria_id = $o->convocatory_id;
            }
            else
            {
                $o = $this->precio->create($data);
                $convocatoria_id = $o->convocatory_id;
            }
        }
        else
        {
            if($extras)
            {
                $precio = PrecioExtra::find($id);
                $precio->update($data);
                $convocatoria_id = $precio->convocatory_id;
            }
            else
            {
                $this->precio->update($data, $id);
                $convocatoria_id = $this->precio->find($id)->convocatory_id;
            }
        }

        return redirect()->route('manage.convocatorias.abiertas.ficha',$convocatoria_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, $extras=false)
    {
        if($extras)
        {
            Session::flash('tab','#precio-extras');

            $precio = PrecioExtra::find($id);
            $convocatoria_id = $precio->convocatory_id;
            $precio->delete();
        }
        else
        {
            Session::flash('tab','#precios');

            $convocatoria_id = $this->precio->find($id)->convocatory_id;
            $this->precio->delete($id);
        }

        return redirect()->route('manage.convocatorias.abiertas.ficha',[$convocatoria_id,$extras]);
    }
}
