<?php

namespace VCN\Http\Controllers\Manage\Convocatorias;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Convocatorias\ConvocatoriaMultiRepository as ConvocatoriaMulti;
use VCN\Repositories\Convocatorias\ConvocatoriaMultiSemanaRepository as ConvocatoriaMultiSemana;
use VCN\Repositories\Convocatorias\ConvocatoriaMultiEspecialidadRepository as ConvocatoriaMultiEspecialidad;
use VCN\Models\Cursos\Curso;
use VCN\Models\Monedas\Moneda;
use VCN\Models\System\Cuestionario;
use VCN\Models\Exams\Examen;
use VCN\Models\Bookings\Booking;
use VCN\Models\Especialidad;
use VCN\Models\Subespecialidad;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Carbon;
use Session;

class ConvocatoriaMultiController extends Controller
{
    private $convocatoria;
    private $semana;
    private $especialidad;

    public function __construct( ConvocatoriaMulti $convocatoria, ConvocatoriaMultiSemana $semana, ConvocatoriaMultiEspecialidad $especialidad )
    {
        $this->middleware("permiso.plataforma:precios,\VCN\Models\Convocatorias\ConvocatoriaMulti", ['only' => ['getUpdate']]);
        $this->checkPermisos('precios');

        $this->convocatoria = $convocatoria;
        $this->semana = $semana;
        $this->especialidad = $especialidad;
    }


    public function getIndex($curso_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->convocatoria->all();
            if($curso_id>0) //?????
            {
                $col = Curso::find($curso_id)->convocatoriasMultiAll;
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.convocatorias.multis.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('activo', function($model) {
                    return $model->activa?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('cursos', function($model) {

                    $ret = "";
                    foreach($model->cursos as $c)
                    {
                        $ret .= $c->name .", ";
                    }

                    return $ret;

                })
                ->addColumn('options', function($model) {

                    $ret = "";

                    if(ConfigHelper::config('facturas'))
                    {
                        if(!$model->no_facturar && $model->es_facturable )
                        {
                            $ret .= " <a href='". route('manage.bookings.facturar',['ConvocatoriaMulti',$model->id]) ."' class='btn btn-success btn-xs'><i class='fa fa-ticket'></i></a>";
                        }
                    }

                    $data = " data-label='Borrar' data-model='Convocatoria Multi' data-action='". route( 'manage.convocatorias.multis.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.convocatorias.multis.index', compact('curso_id'));
    }


    public function getNuevo(Request $request, $curso_id=0)
    {
        $cursos = Curso::where('es_convocatoria_multi',true)->pluck('course_name','id');
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();

        $descuentos_early = ['' => 'No'] + \VCN\Models\Descuentos\DescuentoEarly::all()->pluck('detalle','id')->toArray();

        return view('manage.convocatorias.multis.new', compact('cursos','monedas','descuentos_early', 'curso_id'));
    }

    public function getUpdate($id)
    {
        $cursos = Curso::where('es_convocatoria_multi',true)->pluck('course_name','id');
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();

        $ficha = $this->convocatoria->find($id);

        $semanas = [];
        foreach($ficha->semanas as $semana)
        {
            $semanas[$semana->semana] = "Semana ". $semana->semana;
        }

        $eids = Especialidad::where('es_multi',1)->pluck('id')->toArray();
        $especialidades = [""=>""] + SubEspecialidad::whereIn('especialidad_id',$eids)->pluck('name','id')->toArray();

        $descuentos_early = ['' => 'No'] + \VCN\Models\Descuentos\DescuentoEarly::all()->pluck('detalle','id')->toArray();
        $cuestionarios = ['' => ''] + Cuestionario::plataforma()->where('activo',1)->pluck('name','id')->toArray();
        $examenes = ['' => ''] + Examen::plataforma()->where('activo',1)->pluck('name','id')->toArray();

        $bFacturas = $ficha->es_facturable;

        return view('manage.convocatorias.multis.ficha', compact('ficha','cursos','monedas','semanas','especialidades','descuentos_early','cuestionarios','examenes','bFacturas'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        if( $request->has('condiciones') )
        {
            $o = $this->convocatoria->find($id);
            $condiciones = $o->condiciones;

            foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)
            {
                foreach(ConfigHelper::idiomas() as $keyi=>$idioma)
                {
                    if( !isset($condiciones[$keyp]) )
                    {
                        $condiciones[$keyp] = [];
                    }

                    if( !isset($condiciones[$keyp][$idioma]) )
                    {
                        $condiciones[$keyp][$idioma] = "";
                    }

                    $condiciones[$keyp][$idioma] = $request->input("condiciones_$keyp-$idioma");
                }
            }

            $o->condiciones = $condiciones;
            $o->save();
            $o->pdfCondiciones();

            Session::flash('tab','#condiciones');
            return redirect()->route('manage.convocatorias.multis.ficha',$id);
        }
        
        $this->validate($request, [
            'name' => 'required|max:255',
            'moneda_id' => 'required',
        ]);

        $data = $request->except('_token','monitores');
        $data['activa'] = $request->has('activa');
        $data['cursos_id'] = $request->has('cursos_id') ? implode(',', $request->get('cursos_id')) : null;

        $data['reunion_no'] = $request->has('reunion_no');
        $data['area'] = $request->has('area');
        $data['area_pagos'] = $request->has('area_pagos');
        $data['area_reunion'] = $request->has('area_reunion');
        $data['dto_early'] = $request->get('dto_early') ?: 0;

        if(!$id)
        {
            //nuevo
            $o = $this->convocatoria->create($data);
            $id = $o->id;
        }
        else
        {
            $this->convocatoria->update($data, $id);
        }

        //Monitores
        \VCN\Models\Monitores\MonitorRelacion::refrescar('ConvocatoriaMulti',$id, $request->get('monitores'));

        return redirect()->route('manage.convocatorias.multis.ficha',$id);
    }

    public function destroy($id)
    {
        $this->convocatoria->delete($id);
        return redirect()->route('manage.convocatorias.multis.index');
    }

    //Semanas
    public function postSemana(Request $request, $convocatory_id)
    {
        $this->validate($request, [
            'semana' => 'required|max:255',
        ]);

        $data = $request->except('_token','edit_semana_id');

        $id = $request->input('edit_semana_id');

        $data['convocatory_id'] = $convocatory_id;
        $data['desde'] = Carbon::createFromFormat('d/m/Y',$data['desde'])->format('Y-m-d');
        $data['hasta'] = Carbon::createFromFormat('d/m/Y',$data['hasta'])->format('Y-m-d');

        if(!$id)
        {
            //nuevo
            $o = $this->semana->create($data);
            $id = $o->id;
        }
        else
        {
            $this->semana->update($data, $id);
        }

        Session::flash('tab','#semanas');

        return redirect()->route('manage.convocatorias.multis.ficha',$convocatory_id);
    }

    public function destroySemana($id)
    {
        $d = $this->semana->find($id);
        $convocatory_id = $d->convocatory_id;

        $this->semana->delete($id);

        Session::flash('tab','#semanas');

        return redirect()->route('manage.convocatorias.multis.ficha',$convocatory_id);
    }

    //Especialidades
    public function postEspecialidad(Request $request, $convocatory_id)
    {
        $data = $request->except('_token','edit_especialidad_id','contable');

        $id = $request->input('edit_especialidad_id');

        $data['convocatory_id'] = $convocatory_id;
        $data['semanas'] = $request->has('semanas')?implode(',', $request->get('semanas')):null;
        // $data['contable'] = implode(',', $request->get('contable'));

        $contables = $request->get('contable');
        $c = [];
        if($request->get('semanas'))
        {
            foreach($request->get('semanas') as $is=>$s)
            {
                if(isset($contables[$is]))
                {
                    $c[] = $contables[$is];
                }
            }
        }

        $data['contable'] = implode(',', $c);

        $contable_new = $data['contable'];
        $contable = "";

        if(!$id)
        {
            //nuevo
            $o = $this->especialidad->create($data);
            $id = $o->id;
        }
        else
        {
            $ficha = $this->especialidad->find($id);
            $contable = $ficha->contable;

            $this->especialidad->update($data, $id);
        }

        $convocatoria = $this->convocatoria->find($convocatory_id);

        //udpate contables
        //Updatecontable
        if($contable != $contable_new)
        {
            $convocatoria->updateContable();
        }

        Session::flash('tab','#especialidades');

        return redirect()->route('manage.convocatorias.multis.ficha',$convocatory_id);
    }

    public function destroyEspecialidad($id)
    {
        $d = $this->especialidad->find($id);
        $convocatory_id = $d->convocatory_id;

        $this->especialidad->delete($id);

        Session::flash('tab','#especialidades');

        return redirect()->route('manage.convocatorias.multis.ficha',$convocatory_id);
    }

}
