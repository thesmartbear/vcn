<?php

namespace VCN\Http\Controllers\Manage\Convocatorias;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Convocatorias\CosteRepository as Coste;
use VCN\Models\Convocatorias\Abierta as Convocatoria;

use Datatable;
use Input;
use Carbon;

use VCN\Helpers\ConfigHelper;

use VCN\Repositories\Criteria\FiltroPlataformaConvocatoria;


class ConvocatoriaCostesController extends Controller
{
    private $coste;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Coste $coste )
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');

        $this->coste = $coste;
    }

    public function getIndex($convocatoria_id=0)
    {
        $this->coste->pushCriteria(new FiltroPlataformaConvocatoria(true));

        if(Datatable::shouldHandle())
        {
            $col = $this->coste->all();
            if($convocatoria_id>0)
            {
                $col = Convocatoria::find($convocatoria_id)->costes;//->sortBy('convocatory_open_cost_name');
            }

            return Datatable::collection( $col )
                ->addColumn('convocatoria', function($model) {
                    return $model->convocatoria?$model->convocatoria->convocatory_open_name:"-";
                })
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.convocatorias.costes.ficha',[$model->id]) ."'>$model->convocatory_open_cost_name</a>";
                })
                ->addColumn('periodo', function($model) {
                    // $ret = Carbon::parse($model->convocatory_open_cost_start_date)->format('d/m/Y');
                    // $ret .= " - ";
                    // $ret .= Carbon::parse($model->convocatory_open_cost_end_date)->format('d/m/Y');
                    $ret = Carbon::parse($model->convocatoria->convocatory_open_valid_start_date)->format('d/m/Y');
                    $ret .= " - ";
                    $ret .= Carbon::parse($model->convocatoria->convocatory_open_valid_start_date)->format('d/m/Y');
                    return $ret;
                })
                ->addColumn('rango', function($model) {
                    return "<span class='badge'>$model->convocatory_open_cost_first_week_range</span> <span class='badge'>$model->convocatory_open_cost_second_week_range</span>";
                })
                ->addColumn('precio', function($model) {
                    return ConfigHelper::parseMoneda($model->convocatory_open_cost_price_per_range, $model->moneda->name );
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Coste por Temporada' data-action='". route( 'manage.convocatorias.costes.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= "<a href='". route('manage.alojamientos.precios.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Precio</a>";
                    // $ret .= " <a href='". route('manage.alojamientos.cuotas.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Cuota</a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.convocatorias.costes.index', compact('convocatoria_id'));
    }

    public function getNuevo($convocatoria_id=0)
    {
        $convocatorias = Convocatoria::pluck('convocatory_open_name','id');

        $convocatoria_name = 0;
        if($convocatoria_id)
        {
            $convocatoria_name = Convocatoria::find($convocatoria_id)->convocatory_open_name;
        }

        return view('manage.convocatorias.costes.new', compact('convocatoria_id','convocatoria_name','convocatorias'));
    }

    public function getUpdate($id)
    {
        $convocatorias = Convocatoria::pluck('convocatory_open_name','id');

        $ficha = $this->coste->find($id);
        return view('manage.convocatorias.costes.ficha', compact('ficha','convocatorias'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'convocatory_id' => 'required|numeric|min:1',
            'convocatory_open_cost_name' => 'required|max:255',
        ]);

        $data = Input::except('_token');
        // $data['convocatory_open_cost_start_date'] = Carbon::createFromFormat('d/m/Y',$data['convocatory_open_cost_start_date'])->format('Y-m-d');
        // $data['convocatory_open_cost_end_date'] = Carbon::createFromFormat('d/m/Y',$data['convocatory_open_cost_end_date'])->format('Y-m-d');

        if(!$id)
        {
            //nuevo
            $o = $this->coste->create($data);
            $id = $o->id;
        }
        else
        {
            $this->coste->update($data, $id);
        }

        //Fechas han de ser los de la convocatoria !!
        $o = $this->coste->find($id);
        $o->convocatory_open_cost_start_date = $o->convocatoria->convocatory_open_valid_start_date;
        $o->convocatory_open_cost_end_date = $o->convocatoria->convocatory_open_valid_end_date;
        $o->save();

        return redirect()->route('manage.convocatorias.costes.ficha',$id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->coste->delete($id);
        return redirect()->route('manage.convocatorias.costes.index');
    }
}
