<?php

namespace VCN\Http\Controllers\Manage\Bookings;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingFactura;
use VCN\Models\Bookings\BookingFacturaGrup;
use VCN\Models\Convocatorias\Cerrada;
use VCN\Models\Convocatorias\ConvocatoriaMulti;

use App;
use Datatable;
use File;
use Session;
use Carbon;
use PDF;
use ConfigHelper;


class BookingFacturasController extends Controller
{

    public function getIndex(Request $request, $booking_id)
    {
        $booking = Booking::find($booking_id);

        if(Datatable::shouldHandle())
        {
            $col = $booking->factura_list;

            return Datatable::collection( $col )
                ->addColumn('fecha', function($model) {
                    return $model->fecha->format('Y-m-d');
                })
                ->showColumns('numero')
                ->addColumn('booking_id', function($model) {
                    return "<a href='". route('manage.bookings.ficha', $model->booking_id) ."'>". $model->booking_id ."</a>";
                })
                ->addColumn('viajero', function($model) {
                    return "<a href='". route('manage.viajeros.ficha', $model->booking->viajero_id) ."'>". $model->booking->viajero->full_name ."</a>";
                })
                ->addColumn('total', function($model) {
                    return $model->grup?$model->grup->total:$model->total;
                })
                ->addColumn('grup', function($model) {
                    return $model->grup?"Si":"No";
                })
                ->addColumn('manual', function($model) {
                    return $model->manual?"Si":"No";
                })
                ->addColumn('pdf', function($model) {

                    if($model->manual)
                    {
                        return "";
                    }

                    if($model->grup)
                    {
                        $dir = "/files/facturas/";
                        $name = "factura_". $model->grup->numero .".pdf";
                    }
                    else
                    {
                        $dir = "/files/bookings/". $model->booking->viajero->id . "/";
                        $name = "factura_". $model->numero .".pdf";
                    }

                    $ret = "<a href='". route('manage.bookings.facturas.pdf', $model->id) ."' data-label='Regenerar factura con datos actualizados'><i class='fa fa-refresh'></i></a>";

                    $file = $dir . $name;
                    return "$ret &nbsp;&nbsp; <a href='$file' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    if($model->manual)
                    {
                        $data = " data-label='Borrar' data-model='Factura' data-action='". route( 'manage.bookings.facturas.delete', $model->id) . "'";
                        $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";
                    }
                    elseif($model->total>0)
                    {
                        if($model->grup)
                        {
                            return "<button class='btn btn-danger btn-xs btn-abonar-grup' data-label='Abonar Factura Grupo' data-ruta='". route('manage.bookings.facturar.abono', $model->id) ."'><i class='fa fa-undo'></i></button>";
                        }
                        else
                        {
                            // $data = " data-label='Abonar Factura' data-model='Cantidad (vacío si Total)' data-action='". route( 'manage.bookings.facturar.abono', $model->id) . "'";
                            // return " <a class='btn btn-danger btn-xs' href='#prompt' $data data-toggle='modal' data-target='#modalPrompt'><i class='fa fa-undo'></i></a>";
                            return "<a class='btn btn-danger btn-xs' data-label='Abonar Factura' href='". route('manage.bookings.facturar.abono', $model->id) ."'><i class='fa fa-undo'></i></a>";
                        }
                    }

                    return $ret;
                })
                ->searchColumns('fecha','total')
                ->orderColumns('numero','fecha','manual','grup')
                ->setAliasMapping()
                // ->setSearchStrip()->setOrderStrip()
                ->make();
        }
    }

    public function getPdf(Request $request, $factura_id)
    {
        Session::flash('tab','#facturas');

        $factura = BookingFactura::find($factura_id);
        $factura->pdf(true);

        Session::flash('mensaje-ok','Factura pdf generada');
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $factura_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, $factura_id)
    {
        Session::flash('tab','#facturas');

        $factura = BookingFactura::find($factura_id);

        if(!$factura)
        {
            Session::flash('mensaje-alert','Factura no existe');
            return redirect()->back();
        }

        if($factura->grup && !$factura->manual)
        {
            Session::flash('mensaje-alert','Factura agrupada');
            return redirect()->back();
        }

        if(!$factura->manual)
        {
            $dir = storage_path("files/bookings/". $factura->booking->viajero->id . "/");
            $name = "factura_". $factura->numero .".pdf";
            $file = $dir . $name;
            File::delete($file);
            $factura->delete();
        }
        elseif($factura->grup)
        {
            foreach($factura->grup->facturas as $fact)
            {
                $fact->delete();
            }

            $factura->grup->delete();
        }
        else
        {
            $factura->delete();
        }

        return redirect()->back();
    }

    public function getIndexByModel(Request $request, $model, $model_id)
    {
        switch($model)
        {
            case 'Cerrada':
            {
                $convo = Cerrada::find($model_id);

                $valores['convocatorias'] = $model_id;
                $valores['tipoc'] = 1;
                $valores['plataformas'] = ConfigHelper::config('propietario');
                $bookings = Booking::listadoFiltros($valores);

                $col = collect();
                foreach($bookings->get() as $booking)
                {
                    if($booking->factura_list)
                    {
                        foreach($booking->factura_list as $factura)
                        {
                            $col->push($factura);
                        }
                    }
                }
            }
            break;

            case 'ConvocatoriaMulti':
            {
                $convo = ConvocatoriaMulti::find($model_id);

                $valores['convocatorias'] = $model_id;
                $valores['tipoc'] = 4;
                $valores['plataformas'] = ConfigHelper::config('propietario');
                $bookings = Booking::listadoFiltros($valores);

                $col = collect();
                foreach($bookings->get() as $booking)
                {
                    if($booking->factura_list)
                    {
                        foreach($booking->factura_list as $factura)
                        {
                            $col->push($factura);
                        }
                    }
                }
            }
            break;

            default:
            {
                $col = [];
            }
        }

        if(Datatable::shouldHandle())
        {
            return Datatable::collection( $col )
                ->addColumn('fecha', function($model) {
                    return $model->fecha->format('Y-m-d');
                })
                ->showColumns('numero')
                ->addColumn('booking_id', function($model) {
                    return "<a href='". route('manage.bookings.ficha', $model->booking_id) ."'>". $model->booking_id ."</a>";
                })
                ->addColumn('viajero', function($model) {
                    return "<a href='". route('manage.viajeros.ficha', $model->booking->viajero_id) ."'>". $model->booking->viajero->full_name ."</a>";
                })
                ->addColumn('total', function($model) {
                    return $model->grup?$model->grup->total:$model->total;
                })
                ->addColumn('grup', function($model) {
                    return $model->grup?"Si":"No";
                })
                ->addColumn('manual', function($model) {
                    return $model->manual?"Si":"No";
                })
                ->addColumn('pdf', function($model) {

                    if($model->manual)
                    {
                        return "";
                    }

                    if($model->grup)
                    {
                        $dir = "/files/facturas/";
                        $name = "factura_". $model->grup->numero .".pdf";
                    }
                    else
                    {
                        $dir = "/files/bookings/". $model->booking->viajero->id . "/";
                        $name = "factura_". $model->numero .".pdf";
                    }

                    $file = $dir . $name;
                    return "<a href='$file' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";

                })
                ->addColumn('options', function($model) {
                    $ret = "";

                    if($model->manual)
                    {
                        $p = "Todas las Facturas agrupadas";
                        if(!$model->grup)
                        {
                            $p = "Factura manual";
                        }

                        $data = " data-label='Borrar' data-model='$p' data-action='". route( 'manage.bookings.facturas.delete', $model->id) . "'";
                        $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";
                    }
                    elseif($model->total>0)
                    {
                        if($model->grup)
                        {
                            return "<button class='btn btn-danger btn-xs btn-abonar-grup' data-label='Abonar Factura Grupo' data-ruta='". route('manage.bookings.facturar.abono', $model->id) ."'><i class='fa fa-undo'></i></button>";
                        }
                        else
                        {
                            // $data = " data-label='Abonar Factura' data-model='Cantidad (vacío si Total)' data-action='". route( 'manage.bookings.facturar.abono', $model->id) . "'";
                            // return " <a class='btn btn-danger btn-xs' href='#prompt' $data data-toggle='modal' data-target='#modalPrompt'><i class='fa fa-undo'></i></a>";
                            return "<a class='btn btn-danger btn-xs' data-label='Abonar Factura' href='". route('manage.bookings.facturar.abono', $model->id) ."'><i class='fa fa-undo'></i></a>";
                        }
                    }

                    return $ret;

                    // $data = " data-label='Borrar' data-model='Factura' data-action='". route( 'manage.bookings.facturas.delete', $model->id) . "'";
                    // $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";
                    // return $ret;
                })
                ->searchColumns('fecha','total')
                ->orderColumns('numero','fecha')
                ->setAliasMapping()
                // ->setSearchStrip()->setOrderStrip()
                ->make();
        }
    }

    public function facturarByModel(Request $request, $model, $model_id)
    {
        Session::flash('tab','#facturas');

        $manual = $request->has('manual');

        $concepto = "-";

        if($manual)
        {
            $this->validate($request, [
                "manual_numero"=> 'required',
                "manual_fecha"=> 'required',
                "manual_razonsocial"=> 'required',
                "manual_cif"=> 'required',
                "manual_cp"=> 'required',
            ]);
        }
        else
        {
            $this->validate($request, [
                'razonsocial'   => 'required',
                'direccion'     => 'required',
                'nif'           => 'required',
                'cp '           => 'required'
            ]);
        }

        switch($model)
        {
            case 'Cerrada':
            {
                $convo = Cerrada::find($model_id);

                $valores['convocatorias'] = $model_id;
                $valores['tipoc'] = 1;
                $bookings = Booking::listadoFiltros($valores);
            }
            break;

            case 'ConvocatoriaMulti':
            {
                $convo = ConvocatoriaMulti::find($model_id);

                $valores['convocatorias'] = $model_id;
                $valores['tipoc'] = 4;
                $bookings = Booking::listadoFiltros($valores);
            }
            break;

            default:
            {
                $bookings = null;
            }
        }

        if(!$bookings)
        {
            Session::flash('mensaje-alert', 'No hay bookings.');
            return redirect()->back();
        }

        //Parcial
        $parcial = $request->has('btn-parcial')?$request->input('parcial'):false;
        if($request->has('btn-parcial') && !$parcial)
        {
            $parcial = 0;
            foreach($bookings->get() as $b)
            {
                $parcial += $b->facturable;
            }
        }

        //abono y emitir
        $iTotAbono = 0;
        if( $request->has('btn-abono') )
        {
            $factura = $bookings->first()->factura;
            if($factura)
            {
                $iTotAbono = $factura->booking->facturarAbono($factura->id);
            }

            Session::flash('mensaje', "Se han abonado $iTotAbono facturas.");
        }

        $iTot = 0;
        foreach($bookings->get() as $booking)
        {
            if($booking->es_facturable && !$booking->no_facturar)
            {
                $iTot++;
            }
        }

        if(!$iTot && !$manual)
        {
            Session::flash('mensaje-alert', 'Ya se han emitido las facturas para esta convocatoria.');
            return redirect()->back();
        }

        $plat = ConfigHelper::config('propietario');

        $num = 1;
        if($manual)
        {
            $num = $request->input('manual_numero');
        }
        else
        {
            $booking = $bookings->first();
            $any = $booking ? (int)Carbon::parse($booking->fecha_inicio)->format('Y') : (int)Carbon::now()->format('Y');
            $num = ConfigHelper::facturaNumero($any);
        }

        $total = 0;
        $manual_fecha = null;
        if($manual)
        {
            $total = $request->input('manual_importe');
            if(!$total)
            {
                $total = 0;
                foreach($bookings->get() as $b)
                {
                    $total += $b->facturable;
                }
            }

            $parcial = $total;

            $manual_fecha = Carbon::createFromFormat('!d/m/Y',$request->input('manual_fecha'));
        }

        $grup = new BookingFacturaGrup;
        $grup->user_id = auth()->user()->id;
        $grup->numero = $num;
        $grup->total = $total;
        $grup->fecha = $manual?$manual_fecha:Carbon::now();
        $grup->plataforma = $plat;
        $grup->nif = $manual?$request->input('manual_cif'):$request->input('nif');
        $grup->cp = $manual?$request->input('manual_cp'):$request->input('cp');
        $grup->razonsocial = $manual?$request->input('manual_razonsocial'):$request->input('razonsocial');
        $grup->direccion = $request->input('direccion');
        $grup->bookings = $bookings->pluck('id')->toArray();
        $grup->manual = $manual;
        $grup->save();

        $grup_id = $grup->id;

        $dir = storage_path("files/convocatorias/facturas/");

        $grup_total = $parcial?:0;
        $total = $grup_total;
        foreach($bookings->get() as $booking)
        {
            if( ($booking->es_facturable && !$booking->no_facturar) || $manual)
            {
                if(!$parcial)
                {
                    $total = $booking->facturable;
                    $grup_total += $total;
                }

                $factura = new BookingFactura;
                $factura->booking_id = $booking->id;
                $factura->user_id = auth()->user()->id;
                $factura->numero = $num;
                $factura->total = $total;
                $factura->fecha = $manual ? $manual_fecha : Carbon::now();
                $factura->plataforma = $booking->plataforma;
                $factura->grup_id = $grup_id;
                $factura->manual = $manual;
                $factura->nif = $grup->cif;
                $factura->cp = $grup->cp;
                $factura->save();
            }
        }

        $grup->total = $grup_total;
        $grup->save();

        if($manual)
        {
            return redirect()->back();
        }

        //Pdf
        $name = "factura_". $num .".pdf";
        $file = $dir . $name;
        $lang = $booking->idioma_contacto;

        $info = ConfigHelper::config_plataforma();

        $template = 'manage.bookings.factura_grup';

        if(is_file($file))
        {
            File::delete($file);
        }

        // return view($template, ['ficha'=> $this, 'factura'=> $grup, 'info'=> $info, 'datos'=> $datos, 'total'=> $grup_total, 'convocatoria'=> $convo]);
        
        $p = $booking->plataforma;

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path()."/tmp");
        $pdf = PDF::loadView($template, ['ficha'=> $this, 'factura'=> $grup, 'info'=> $info, 'total'=> $grup_total, 'convocatoria'=> $convo]);

        $pdf->setOption('margin-top',30);
        $pdf->setOption('margin-right',0);
        $pdf->setOption('margin-bottom',30);
        $pdf->setOption('margin-left',0);
        $pdf->setOption('no-print-media-type',false);
        $pdf->setOption('footer-spacing',0);
        $pdf->setOption('header-font-size',9);
        $pdf->setOption('header-spacing',0);

        $pdf->setOption('header-html', 'https://'.ConfigHelper::config('web').'/assets/logos/'.$lang.'/'.ConfigHelper::config('sufijo',$p).'header.html');
        $pdf->setOption('footer-html', 'https://'.ConfigHelper::config('web').'/assets/logos/'.$lang.'/'.ConfigHelper::config('sufijo',$p).'footer.html');

        $pdf->save($file);

        return redirect()->back();
    }
}
