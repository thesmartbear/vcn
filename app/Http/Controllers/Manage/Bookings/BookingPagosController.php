<?php

namespace VCN\Http\Controllers\Manage\Bookings;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Bookings\BookingPagoRepository as BookingPago;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Monedas\Moneda;

use VCN\Helpers\ConfigHelper;
use Illuminate\Support\Facades\App;

use Datatable;
use Auth;
use Carbon;
use Session;
use PDF;

class BookingPagosController extends Controller
{
    private $pago;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( BookingPago $pago )
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');

        $this->pago = $pago;
    }

    public function getPdf($id)
    {
        $ficha = $this->pago->find($id);

        $booking = $ficha->booking;

        $idioma = $booking->viajero->idioma_contacto;

        $view = 'pdf.'. strtolower($idioma) .'.recibo';

        $name = "r_";

        $lang = $booking->idioma_contacto;

        // return view($view, compact('ficha','booking','idioma'));

        $file = $name. $id .".pdf";// ."-". Carbon::now()->format('Y-m-d_H-i-s') .".pdf";

        /*
        $pdf = PDF::loadView($view, compact('ficha','booking','idioma'))
            ->setOption('margin-top', 0)->setOption('margin-right', 0)->setOption('margin-bottom', 0)->setOption('margin-left', 0)
            ->setOption('disable-smart-shrinking',true)->setOption('zoom','0.78');
        */

        $p = $booking->plataforma;
        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path()."/tmp");
        $pdf = PDF::loadView($view, compact('ficha','booking','idioma'));
        $pdf->setOption('margin-top',30);
        $pdf->setOption('margin-right',0);
        $pdf->setOption('margin-bottom',30);
        $pdf->setOption('margin-left',0);
        $pdf->setOption('no-print-media-type',false);
        $pdf->setOption('footer-spacing',0);
        $pdf->setOption('header-font-size',9);
        $pdf->setOption('header-spacing',0);
        $pdf->setOption('header-html', 'https://'.ConfigHelper::config('web').'/assets/logos/'.$lang.'/'.ConfigHelper::config('sufijo',$p).'header.html');
        $pdf->setOption('footer-html', 'https://'.ConfigHelper::config('web').'/assets/logos/'.$lang.'/'.ConfigHelper::config('sufijo',$p).'footer.html');

        return $pdf->download($file);
    }

    public function getIndex($booking_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->pago->all();
            if($booking_id>0)
            {
                $col = Booking::find($booking_id)->pagos;//->sortBy('accommodation_prices_description');
            }

            return Datatable::collection( $col )
                ->addColumn('fecha', function($model) {
                    if(!$model->enviado && !$model->es_reserva_inicial)
                    {
                        return "<a href='". route('manage.bookings.pagos.ficha',[$model->id]) ."'>$model->fecha_dmy</a>";
                    }

                    return $model->fecha_dmy;
                })
                ->addColumn('tipo', function($model) {
                    return ConfigHelper::getTipoPago($model->tipo);
                })
                ->addColumn('importe', function($model) {
                    return $model->importe;
                })
                ->addColumn('importe_pago', function($model) {
                    if($model->rate!=1)
                    {
                        return ConfigHelper::parseMoneda($model->importe_pago,$model->moneda->name) . " [$model->rate]";
                    }
                })
                ->showColumns('notas')
                ->addColumn('avisado', function($model) {
                    $ret = "NO AVISAR";

                    if($model->avisado==0)
                    {
                        $ret = "PENDIENTE";
                    }
                    elseif($model->avisado==1)
                    {
                        $ret = "AVISADO";
                    }

                    return $ret;
                })
                ->addColumn('options', function($model) {

                    $ret = "";

                    if(!$model->enviado)
                    {
                        $data = " data-label='Borrar' data-model='Pago' data-action='". route( 'manage.bookings.pagos.delete', $model->id) . "'";
                        $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";
                    }

                    // $ret .= "<a href='$model->id' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Centro</a>";

                    $ret .= " <a href='". route('manage.bookings.pagos.pdf', [$model->id]) ."' class='btn btn-danger btn-xs'><i class='fa fa-file-pdf-o'></i> Recibo</a>";

                    return $ret;
                })
                ->searchColumns('tipo')
                ->orderColumns('*')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.bookings.pagos.index', compact('booking_id'));
    }

    public function getNuevo($booking_id=0)
    {
        $booking = Booking::find($booking_id);

        $tipos = ConfigHelper::getTipoPago();

        return view('manage.bookings.pagos.new', compact('booking'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->pago->find($id);

        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();
        $monedas_cambio = [0 => 1] + Moneda::pluck('currency_rate','id')->toArray();

        return view('manage.bookings.pagos.ficha', compact('ficha','monedas','monedas_cambio'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        Session::flash('tab','#pagos');

        $this->validate($request, [
            'fecha' => 'required',
            'tipo' => 'required|not_in:0',
            'importe_pago' => 'required',
            'moneda_id' => 'required|not_in:0',
            'rate' => 'required',
        ]);

        $data = $request->except('_token','avisar');
        $data['fecha'] = Carbon::createFromFormat('d/m/Y',$data['fecha'])->format('Y-m-d');
        $data['user_id'] = auth()->user()->id;
        $data['avisado'] = $request->has('avisar') ? 0:2;
        
        $data['importe_pago'] = floatval($request->input('importe_pago'));
        if(strpos($request->input('importe_pago'), ','))
        {
            $data['importe_pago'] = floatval(str_replace(',', '.', str_replace('.', '', $request->input('importe_pago'))));
        }
        
        $booking = Booking::find($request->get('booking_id'));

        if(!$id)
        {
            //nuevo
            $pago = $this->pago->create($data);

            $st = $pago->booking->status_id;
            $booking = $pago->booking;

            //=> pre-booking
            if( $st == 0 || $st == ConfigHelper::config('booking_status_prereserva') || $st == ConfigHelper::config('booking_status_borrador') )
            {
                //obvkg
                if($booking->ovbkg_pa || $booking->ovbkg_pv)
                {
                    $pago->booking->setStatus(ConfigHelper::config('booking_status_overbooking'));
                    BookingLog::addLog($pago->booking, "Pre-Reserva + Pago => Overbooking");    
                }
                else
                {
                    $pago->booking->setStatus(ConfigHelper::config('booking_status_prebooking'));
                    BookingLog::addLog($pago->booking, "Pre-Reserva + Pago => Pre-Booking");
                }
            }
            elseif( $st == ConfigHelper::config('booking_status_cancelado') )
            {
                $importe = $request->input('importe_pago');
                // $pago->booking->setStatus(ConfigHelper::config('booking_status_prebooking'));
                BookingLog::addLog($pago->booking, "Pago[$pago->id] [BOOKING CADUCADO] Importe: $importe");
            }
            else
            {
                $importe = $request->input('importe_pago');
                BookingLog::addLog($pago->booking, "Pago[$pago->id] Importe: $importe");
            }

            $id = $pago->id;
        }
        else
        {
            $this->pago->update($data, $id);
            $pago = $this->pago->find($id);
        }

        $booking = $pago->booking;

        //Es el primer pago => cambiamos fecha
        $p1 = $booking->pagos->sortBy('fecha')->first();
        if($p1->id == $id)
        {
            \VCN\Models\Informes\Venta::remove($booking);

            $booking->fecha_pago1 = $pago->fecha;
            $booking->save();

            \VCN\Models\Informes\Venta::add($booking);
        }

        //Avisar
        $pago->avisado = $request->has('avisar')?0:2;
        // if($booking->curso && $booking->curso->categoria && $booking->curso->categoria->es_menor)
        // {
        //     $pago->avisado = 2;
        // }

        $pago->importe = $pago->importe_pago * $pago->rate;
        $pago->save();

        $booking = Booking::find($pago->booking_id);

        if($booking->saldo_pendiente==0)
        {
            $booking->pagado();
            Session::flash('mensaje', "Inscripción marcada como pagada");
        }

        return redirect()->route('manage.bookings.ficha',$booking->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $pago = $this->pago->find($id);

        if($pago->enviado)
        {
            Session::flash('mensaje-alert', "Imposible borrar un pago enviado.");
            return back();
        }

        $booking_id = $pago->booking_id;

        BookingLog::addLog($pago->booking, "Pago Eliminado[$id] Importe: $pago->importe ($pago->importe_pago)");

        $this->pago->delete($id);

        Session::flash('tab','#pagos');
        return redirect()->route('manage.bookings.ficha',$booking_id);
    }
}
