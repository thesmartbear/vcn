<?php

namespace VCN\Http\Controllers\Manage\Bookings;

use Illuminate\Http\Request;
use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Bookings\BookingRepository as Booking;
use VCN\Repositories\Leads\ViajeroRepository as Viajero;
use VCN\Repositories\Leads\ViajeroDatosRepository as ViajeroDatos;
use VCN\Repositories\Cursos\CursoRepository as Curso;

use VCN\Models\Convocatorias\Cerrada as ConvocatoriaCerrada;
use VCN\Models\Convocatorias\Abierta as ConvocatoriaAbierta;
use VCN\Models\Convocatorias\ConvocatoriaMulti;
use VCN\Models\Convocatorias\ConvocatoriaMultiSemana;

use VCN\Models\Convocatorias\ConvocatoriaMultiEspecialidad;

use VCN\Models\Bookings\Booking as BookingModel;
use VCN\Models\Bookings\BookingMoneda;

use VCN\Models\User;
use VCN\Models\Monedas\Moneda;
use VCN\Models\Bookings\BookingExtra;
use VCN\Models\Bookings\Status;
use VCN\Models\Bookings\BookingPago;
use VCN\Models\Bookings\BookingMulti;
use VCN\Models\Cursos\CursoExtra;
use VCN\Models\Centros\CentroExtra;
use VCN\Models\Alojamientos\AlojamientoCuota;
use VCN\Models\Cursos\CursoExtraGenerico;
use VCN\Models\Leads\Tutor;
use VCN\Models\Leads\Origen;
use VCN\Models\Leads\Suborigen;
use VCN\Models\Leads\SuborigenDetalle;
use VCN\Models\Descuentos\DescuentoTipo;

use VCN\Models\Bookings\BookingChecklist;

use VCN\Models\Bookings\BookingLog;
use VCN\Models\Leads\ViajeroLog;
use VCN\Models\Prescriptores\Prescriptor;
use VCN\Models\System\Oficina;
use VCN\Models\Centros\Familia;
use VCN\Models\Bookings\BookingFamilia;
use VCN\Models\Bookings\BookingSchool;
use VCN\Models\Bookings\BookingFactura;

use VCN\Models\Informes\Venta;

use VCN\Models\Leads\Viajero as ViajeroModel;
use VCN\Models\Leads\ViajeroDatos as ViajeroDatosModel;
use VCN\Models\Leads\ViajeroTutor;
use VCN\Models\Leads\ViajeroArchivo;

use Datatable;
use Input;
use Auth, Schema;
use Session;
use Carbon;
use Form;
use PDF;
use DB;
use Log;
use File;

use VCN\Helpers\ConfigHelper;
use VCN\Helpers\MailHelper;

use VCN\Repositories\Criteria\FiltroPlataformaCentro;
use VCN\Repositories\Criteria\FiltroPlataformaViajeroRel;
use VCN\Repositories\Criteria\FiltroOficinaViajero;


class BookingsController extends Controller
{
    private $booking;
    private $viajero;
    private $datos;
    private $curso;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct(Booking $booking, Viajero $viajero, ViajeroDatos $datos, Curso $curso)
    {
        $this->checkPermisos('bookings');

        $this->booking = $booking;
        $this->viajero = $viajero;
        $this->datos = $datos;
        $this->curso = $curso;

        $this->middleware("permiso.view:bookings-list", ['only' => ['getIndex', 'getUpdate']]);
    }

    public function ajaxPost(Request $request, $booking_id)
    {
        $campo = $request->input('campo');

        $ficha = $this->booking->find($booking_id);

        if (!$ficha) {
            return response()->json(null, 500);
        }

        $id = $request->input('id') ?: 0;

        $res = false;
        $error = "Error";

        //si esta marcado como pagado no debe dejar añadir extras
        if ($ficha->pagado && $campo != "booking-total") {
            $error = "<strong>Booking marcado como pagado.</strong> <br>Si quieres realizar un cambio hay que abrir los pagos.";

            $result = ['campo' => $campo, 'id' => $id, 'result' => $res, 'error' => $error, 'pagado' => 1];
            return response()->json($result, 200);
        }

        if ($campo != 'booking-total') {
            $ficha->mailAvisoCambios();
        }

        switch ($campo) {
            case 'booking-update': {
                    $field = $request->input('field');
                    $valor = $request->get('valor');

                    $ficha->$field = $valor;

                    if ($field == "transporte_no") {
                        $ficha->transporte_otro = !$valor;
                        if (!$valor) {
                            $ficha->transporte_detalles = "";
                        }
                    } elseif ($field == "transporte_otro") {
                        $ficha->transporte_no = !$valor;
                    }

                    $ficha->save();

                    $result = ['campo' => $campo, 'valor' => $ficha->$field, 'result' => true];
                    return response()->json($result, 200);
                }
                break;

            case 'booking-total': {
                    $valor = $ficha->precio_total;

                    $ficha->total = $valor['total'];
                    $ficha->save();

                    $result = ['campo' => $campo, 'moneda' => Session::get('vcn.moneda'), 'result' => $valor];
                    return response()->json($result, 200);
                }
                break;

            case 'booking-seguro': {
                    $valor = $request->input('valor');

                    $seguro = null;
                    if ($valor == 'false') {
                        $ficha->quitCancelacion();
                    } else {
                        $seguro = $ficha->setCancelacion();
                    }

                    $seguro_txt = "";
                    if ($seguro) {
                        $seguro_txt = $seguro->full_name;
                        $pdf = $ficha->cancelacion_pdf;
                        $seguro_txt = $pdf ? "<a href='/$pdf' target='_blank'>$seguro_txt</a>" : $seguro_txt;
                    }

                    $result = ['campo' => $campo, 'seguro' => $seguro, 'seguro_txt' => $seguro_txt, 'result' => $valor];
                    return response()->json($result, 200);
                }
                break;

            case 'cmulti': {
                    $semanas = $request->input('valor');
                    $desde = intval($request->input('fecha'));

                    // $id por la semana
                    $id = ConvocatoriaMultiSemana::find($desde)->convocatory_id;
                    $ficha->convocatory_multi_id = $id;
                    $ficha->save();

                    // $cm = ConvocatoriaMulti::find($id);
                    $cm = $ficha->convocatoria;

                    $precio = $cm->calcularPrecio($semanas, $ficha->id);
                    $precio_solo = $cm->calcularPrecio($semanas, $ficha->id, true);
                    $fechas = $cm->calcularFechas($desde, $semanas);

                    // BLOQUES:
                    // $fechas = $cm->calcularFechasBloque($desde,$semanas);
                    // if(!$fechas['fecha_fin'])
                    // {
                    //     $sem = (int)$fechas['semanas'];
                    //     //$error = "Duración ". ($sem==0?'MÁXIMA: ':'MÍNIMA: ') . $sem ." Semanas para la fecha de inicio seleccionada.";
                    //     $error = "Duración ". ($sem==0?'MÁXIMA: ':'INCORRECTA ') . $sem ." Semanas para la fecha de inicio seleccionada.";

                    //     $result = ['campo'=>$campo, 'id'=> $id, 'result'=> false,
                    //         'bloques'=> $fechas['bloques'], 'semanas'=> $sem, 'error'=> $error ];
                    //     return response()->json($result, 200);
                    // }
                    // $precio = $cm->calcularPrecio($semanas,$ficha->id);
                    // $precio_solo = $cm->calcularPrecio($semanas,$ficha->id,true);
                    // :BLOQUES

                    // $ficha->convocatory_multi_price = $precio;
                    $ficha->weeks = $semanas;
                    $ficha->semanas = $ficha->semanas_unit;

                    $ficha->course_start_date = $fechas['fecha_ini'];
                    $ficha->course_end_date = $fechas['fecha_fin'];
                    $ficha->course_price = $precio_solo;
                    $ficha->course_total_amount = $precio;
                    $ficha->course_currency_id = $cm->moneda_id;
                    $ficha->save();

                    //Alojamiento = curso
                    $ficha->accommodation_start_date = $ficha->course_start_date;
                    $ficha->accommodation_end_date = $ficha->course_end_date;
                    $ficha->accommodation_weeks = $ficha->weeks;
                    $ficha->save();

                    //Descuentos
                    // $ficha->aplicarDescuento();

                    $ficha->setMultiEspecialidades($desde, $semanas);

                    $res = true;

                    BookingLog::addLog($ficha, "Selecciona Convocatoria Multi [$id]");

                    $total_txt = ConfigHelper::parseMoneda($precio_solo, $cm->moneda_name);

                    $result = [
                        'campo' => $campo, 'id' => $id, 'fecha' => $fechas['fecha_fin_txt'], 'precio' => $precio_solo, 'total_txt' => $total_txt, 'result' => $res,
                        'online' => $ficha->es_online
                    ];
                    return response()->json($result, 200);
                }
                break;

            case 'cmulti-especialidad': {
                    $e = BookingMulti::find($id);

                    $valor = $request->input('valor');

                    $m = ConvocatoriaMultiEspecialidad::find($valor);

                    $precio1 = $e->precio;
                    $valor1 = $e->especialidad_id;

                    $e->especialidad_id = $valor;
                    $e->precio = $m->precio;
                    $e->save();

                    if ($ficha->es_terminado) {
                        $ficha->course_total_amount -= $precio1;
                        $ficha->total -= $precio1;
                        $ficha->course_total_amount += $m->precio;
                        $ficha->total += $m->precio;
                        $ficha->save();

                        BookingLog::addLog($ficha, "Convocatoria Multi cambio Especialidad [$valor1 => $valor]");

                        $ficha->pdf();
                    }

                    $res = true;
                    BookingLog::addLog($ficha, "Convocatoria Multi Especialidad [$id]");

                    $precio_txt = ConfigHelper::parseMoneda($m->precio, $ficha->convocatoria->moneda_name);

                    $result = ['campo' => $campo, 'id' => $id, 'precio' => $m->precio, 'precio_txt' => $precio_txt, 'result' => $res];
                    return response()->json($result, 200);
                }
                break;

            case 'ccerrada-semi': {
                    $cc = ConvocatoriaCerrada::find($id);

                    $error = 0;
                    $fdate = $request->input('fecha', null);
                    if (strpos($fdate, 'Selecc') !== false) {
                        $fdate = null;
                    }

                    if ($cc && $cc->convocatory_semiopen && $fdate) {
                        $fecha = Carbon::createFromFormat('d/m/Y', $fdate);

                        $startDate = Carbon::createFromFormat('d/m/Y', $cc->start_date);
                        $endDate = Carbon::createFromFormat('d/m/Y', $cc->end_date);

                        if ($fecha->gte($startDate) && $fecha->lte($endDate)) {
                            $f = $fecha;
                            $ficha->course_start_date = $f->format('Y-m-d');
                            $ficha->course_end_date = $ficha->calcularFechaFin($f, $cc->convocatory_close_duration_weeks, $cc->convocatory_semiopen_start_day, $cc->convocatory_semiopen_end_day)->format('Y-m-d');
                            $ficha->weeks = $cc->convocatory_close_duration_weeks;
                            $ficha->save();

                            $ficha->semanas = $ficha->semanas_unit;
                            $ficha->duracion_fijo = $cc->duracion_fijo;

                            $ficha->accommodation_start_date = $ficha->course_start_date;
                            if ($ficha->alojamiento && $ficha->alojamiento->start_day) {
                                $ficha->accommodation_start_date = ConfigHelper::calcularFechaDia($ficha->course_start_date, $ficha->alojamiento->start_day);
                            }

                            $ficha->accommodation_end_date = $ficha->course_end_date;
                            if ($ficha->alojamiento && $ficha->alojamiento->end_day) {
                                $ficha->accommodation_end_date = ConfigHelper::calcularFechaDia($ficha->course_end_date, $ficha->alojamiento->end_day);
                            }

                            $ficha->save();

                            $reset = $request->get('reset');

                            if ($reset) {
                                $ficha->resetExtras();
                            }

                            $res = true;
                        } else {
                            $ficha->course_start_date = "";
                            $ficha->save();
                            $error = 'Error en Fecha de Inicio.';
                        }

                        $result = [
                            'campo' => $campo, 'id' => $id, 'result' => $res, 'fecha' => Carbon::parse($ficha->course_end_date)->format('d/m/Y'), 'error' => $error,
                            'online' => $ficha->es_online
                        ];
                        return response()->json($result, 200);

                        // $result = ['campo'=>$campo, 'id'=> $id, 'result'=> $res, 'fecha'=> Carbon::parse($ficha->course_end_date)->format('d/m/Y'), 'error'=> $error];
                        // return response()->json($result, 200);
                    } else {
                        $result = ['campo' => $campo, 'id' => $id, 'result' => $res, 'error' => 'Error convocatoria'];
                        return response()->json($result, 200);
                    }
                }
                break;

            case 'ccerrada': {
                    $cc = ConvocatoriaCerrada::find($id);
                    $ficha->convocatory_close_id = $id;
                    $ficha->convocatory_close_price = $cc->precio;
                    $ficha->weeks = $cc->convocatory_close_duration_weeks;
                    $ficha->semanas = $ficha->semanas_unit;
                    $ficha->duracion_fijo = $cc->duracion_fijo;

                    $ficha->course_start_date = $cc->convocatory_close_start_date;
                    $ficha->course_end_date = $cc->convocatory_close_end_date;

                    $ficha->accommodation_start_date = $ficha->course_start_date;
                    $ficha->accommodation_end_date = $ficha->course_end_date;
                    $ficha->accommodation_weeks = $ficha->weeks;
                    $ficha->accommodation_id = $cc->alojamiento_id;

                    if (!$cc->alojamiento_id) {
                        //Reset
                        $ficha->accommodation_price = 0;
                        $ficha->accommodation_currency_id = 0;
                        $ficha->accommodation_total_amount = 0;
                    }

                    $ficha->course_price = $cc->precio;
                    $ficha->course_total_amount = $cc->precio;
                    $ficha->course_currency_id = $cc->convocatory_close_currency_id;
                    $ficha->save();

                    $ficha->resetExtras();

                    //Descuentos
                    $ficha->aplicarDescuento();

                    $res = true;
                    BookingLog::addLog($ficha, "Selecciona Convocatoria Cerrada [$id]");

                    $result = ['campo' => $campo, 'id' => $id, 'result' => $res, 'online' => $ficha->es_online];
                    return response()->json($result, 200);
                }
                break;

            case 'ccerrada-alojamiento': {
                    // if($ficha->convocatoria->alojamiento_id>0 && $ficha->convocatoria->alojamiento_id!=$id)
                    // {
                    //     $result = ['campo'=>$campo, 'id'=> $id, 'result'=> false];
                    //     return response()->json($result, 200);
                    // }

                    $ficha->accommodation_id = $id;
                    $ficha->save();

                    if (!$id) {
                        $precio = 0;
                        BookingLog::addLog($ficha, "Selecciona Alojamiento (CC) [$id]");

                        $result = ['campo' => $campo, 'id' => $id, 'precio' => $precio, 'result' => true];
                        return response()->json($result, 200);
                    }

                    $weeks = $ficha->weeks;

                    $ficha->accommodation_start_date = $ficha->course_start_date;
                    $ficha->accommodation_end_date = $ficha->course_end_date;
                    $ficha->accommodation_weeks = $ficha->weeks;
                    $ficha->save();

                    if (!$ficha->accommodation_start_date || !$ficha->accommodation_end_date) {
                        $result = ['campo' => $campo, 'id' => $id, 'result' => true];
                        return response()->json($result, 200);
                    }

                    $fechaini = Carbon::createFromFormat('Y-m-d', $ficha->accommodation_start_date)->format('d/m/Y');
                    $fechafin = Carbon::createFromFormat('Y-m-d', $ficha->accommodation_end_date)->format('d/m/Y');

                    $precio = $ficha->alojamiento->calcularPrecio($fechaini, $fechafin, $weeks);

                    $ficha->accommodation_price = $precio['importe'];
                    $ficha->accommodation_currency_id = $precio['moneda_id'];
                    $ficha->accommodation_total_amount = $ficha->accommodation_price;
                    $ficha->save();

                    $res = true;

                    BookingLog::addLog($ficha, "Selecciona Alojamiento (CC) [$id]");

                    $alerta = null;
                    $alertas = $ficha->alertas;
                    $alertas['alojamiento'] = $alerta;
                    if (isset($precio['semanas']) && $precio['semanas'] < $weeks) {
                        $alerta = "La duración real del Alojamiento es de " . $precio['semanas'] . " " . $precio['duracion'];
                        $idioma = $ficha->viajero->idioma_contacto;
                        if ($idioma == "ca") {
                            $alerta = "La duració real del Allotjament es de " . $precio['semanas'] . " " . $precio['duracion'];
                        }

                        $alertas = $ficha->alertas;
                        $alertas['alojamiento'] = $alerta;
                        $ficha->alertas = $alertas;
                    }

                    $result = [
                        'campo' => $campo, 'id' => $id, 'precio' => $precio, 'result' => $res, 'alerta' => $alerta,
                        'online' => $ficha->es_online
                    ];
                    return response()->json($result, 200);
                }
                break;

            case 'cabierta': {
                    $ca = ConvocatoriaAbierta::find($id);
                    $ficha->convocatory_open_id = $id;
                    $ficha->course_currency_id = $ca->convocatory_open_currency_id;
                    $ficha->save();

                    //Descuentos
                    // $ficha->aplicarDescuento();

                    $res = true;

                    BookingLog::addLog($ficha, "Selecciona Convocatoria Abierta [$id]");
                }
                break;

            case 'cabierta-semanas': {
                    $weeks = $request->input('valor');
                    $fechaini = Carbon::createFromFormat('d/m/Y', $request->input('fecha'));

                    BookingLog::addLog($ficha, "Selecciona Convocatoria Abierta Semanas [$weeks]");

                    $curso_id = $ficha->curso_id;

                    $caducado = $fechaini->isPast();

                    $cas = ConvocatoriaAbierta::where('course_id', $curso_id)->where('convocatory_open_status', 1)->count();
                    if ($cas > 1) {
                        $id = 0;
                    }

                    if ($id > 0) {
                        $ca = ConvocatoriaAbierta::find($id);
                    } else {
                        $ca = null;
                        $cas = ConvocatoriaAbierta::where('course_id', $curso_id)->where('convocatory_open_status', 1)->get();
                        if ($cas->count() > 1) {
                            $ca = ConvocatoriaAbierta::where('course_id', $curso_id)->where('convocatory_open_status', 1)
                                ->where('convocatory_open_valid_start_date', '<=', $fechaini)
                                ->where('convocatory_open_valid_end_date', '>=', $fechaini)
                                ->first();
                        }

                        if ($cas->count() > 0 && !$ca) {
                            $ca = $cas->first();
                        }
                    }

                    if (!$ca) {
                        $result = ['campo' => $campo, 'id' => $id, 'result' => false, 'minimo' => false, 'error' => 'No hay convocatoria', 'caducado' => $caducado];
                        return response()->json($result, 200);
                    }

                    $id = $ca->id;

                    $reload = false;
                    if ($ficha->convocatory_open_id != $id) {
                        $reload = true;
                    }

                    $ficha->convocatory_open_id = $id;
                    $ficha->save();

                    $moneda = Session::get('vcn.moneda');

                    $fechafin = Carbon::createFromFormat('d/m/Y', $request->input('fecha'));
                    $fechafin = $ficha->calcularFechaFin($fechafin, $weeks, $ca->convocatory_open_start_day, $ca->convocatory_open_end_day);
                    $precio = $ca->calcularPrecio($fechaini->format('d/m/Y'), $fechafin->format('d/m/Y'), $weeks); //weeks ahora es duracion

                    if ($precio) {
                        $moneda_id = $precio['moneda_id'];
                        $moneda = $precio['moneda'];
                        $importe = $precio['importe_base'];
                        $importe_extra = $precio['importe_extra'];

                        $alerta = null;
                        $alertas = $ficha->alertas;
                        $alertas['cabierta'] = $alerta;
                        if ($precio['semanas'] < $weeks && !$precio['monto_fijo']) {
                            $alerta = "La duración real del Curso es de " . $precio['semanas'] . " " . $precio['duracion'];
                            $idioma = $ficha->viajero->idioma_contacto;
                            if ($idioma == "ca") {
                                $alerta = "La duració real del Curs es de " . $precio['semanas'] . " " . $precio['duracion'];
                            }

                            $alertas['cabierta'] = $alerta;
                            $ficha->alertas = $alertas;

                            // $weeks = $precio['semanas'];
                            // $fechafin = $ficha->calcularFechaFin($fechaini,$weeks, $ca->convocatory_open_start_day, $ca->convocatory_open_end_day);
                            // $ficha->weeks = $weeks;
                            // $ficha->course_end_date = $fechafin->format('Y-m-d');
                        }

                        $total = $importe + $importe_extra;

                        $ficha->course_price = $importe;
                        $ficha->curso_precio_extra = $importe_extra;
                        $ficha->course_total_amount = $total;
                        $ficha->convocatory_open_price = $total;

                        $ficha->course_currency_id = $moneda_id;
                        $ficha->weeks = $weeks;
                        $ficha->semanas = $ficha->semanas_unit;
                        $ficha->save();

                        //Descuento
                        $dto = $ficha->aplicarDescuento();
                        $dto = $ficha->center_discount_amount;

                        $ficha->course_start_date = $fechaini->format('Y-m-d');
                        $ficha->course_end_date = $fechafin->format('Y-m-d');

                        $ficha->save();
                        $res = true;

                        $moneda = $precio['moneda'];

                        $dto_pagar = $ficha->course_total_amount - $dto;

                        $result = [
                            'campo' => $campo, 'id' => $id, 'result' => $res,
                            'total_txt' => ConfigHelper::parseMoneda(($total), $moneda),
                            'precio_txt' => ConfigHelper::parseMoneda(($importe), $moneda),
                            'precio_extra_txt' => ConfigHelper::parseMoneda(($importe_extra), $moneda),
                            'precio_extra' => $importe_extra,
                            'fechafin' => $fechafin->format('d/m/Y'),
                            'descuento' => $dto, 'descuento_pagar' => $dto_pagar,
                            'descuento_txt' => ConfigHelper::parseMoneda($dto, $moneda),
                            'descuento_pagar_txt' => ConfigHelper::parseMoneda($dto_pagar, $moneda),
                            'caducado' => $caducado,
                            'alerta' => $alerta,
                            'reload' => $reload,
                            'convocatoria' => "[Convocatoria: " . $ca->name . "]",
                        ];
                        return response()->json($result, 200);
                    } else {
                        $min = $ca->precios->sortBy('rango1')->first();

                        $result = ['campo' => $campo, 'id' => $id, 'result' => false, 'minimo' => $min ? $min->rango1 : 0, 'error' => 'No hay rango de precios para el período', 'caducado' => $caducado];
                        return response()->json($result, 200);
                    }
                }
                break;


            case 'cabierta-alojamiento': {
                    $weeks = intval($request->input('valor'));

                    $ca = $ficha->convocatoria;

                    $alojamiento_id = $ficha->accommodation_id;

                    if (!$id) {
                        $ficha->accommodation_id = null;
                        $ficha->accommodation_weeks = 0;

                        $ficha->accommodation_start_date = $ficha->course_start_date;
                        $ficha->accommodation_end_date = $ficha->course_end_date;

                        $ficha->accommodation_price = 0;
                        $ficha->alojamiento_precio_extra = 0;
                        $ficha->accommodation_currency_id = 0;
                        $ficha->accommodation_total_amount = 0;

                        $ficha->save();

                        $result = ['campo' => $campo, 'id' => 0, 'result' => 0];
                        return response()->json($result, 200);
                    }

                    if ($request->input('fecha')) {
                        $ficha->accommodation_id = $id;
                        $ficha->accommodation_weeks = $weeks;
                        $ficha->save();

                        $fecha = Carbon::createFromFormat('d/m/Y', $request->input('fecha'));

                        $fechaini = $request->input('fecha');
                        $fechafin = $ficha->calcularFechaFin($fecha, $weeks, $ficha->alojamiento->start_day, $ficha->alojamiento->end_day, false);
                        $precio = $ficha->alojamiento->calcularPrecio($fechaini, $fechafin->format('d/m/Y'), $weeks);
                        if (!$precio) {
                            $result = ['campo' => $campo, 'id' => $id, 'result' => false, 'error' => 'No hay Alojamiento para el período'];
                            return response()->json($result, 200);
                        }
                    }

                    $reload = false;
                    if ($ficha->accommodation_id != $alojamiento_id) {
                        //ha cambiado: setExtras y Reload
                        $reload = true;
                    }

                    if ($reload) {
                        $ficha->resetExtras();
                    }

                    if (!$request->input('fecha')) {
                        $result = ['campo' => $campo, 'id' => 0, 'result' => 0];
                        return response()->json($result, 200);
                    }

                    $fecha = Carbon::createFromFormat('d/m/Y', $request->input('fecha'));
                    $ficha->accommodation_weeks = $weeks;
                    $ficha->accommodation_start_date = $fecha->format('Y-m-d');
                    $ficha->accommodation_end_date = $fechafin->format('Y-m-d');
                    $ficha->save();

                    $importe = $precio['importe_base'];
                    $importe_extra = $precio['importe_extra'];
                    $total = $importe + $importe_extra;

                    $ficha->accommodation_price = $importe;
                    $ficha->alojamiento_precio_extra = $importe_extra;
                    $ficha->accommodation_currency_id = $precio['moneda_id'];
                    $ficha->accommodation_total_amount = $total;
                    $ficha->save();

                    $semanas = $precio['semanas'];
                    $moneda = $precio['moneda'];

                    BookingLog::addLog($ficha, "Selecciona Convocatoria Abierta Alojamiento [$id]");

                    $result = [
                        'campo' => $campo,
                        'id' => $id,
                        'old_id' => $alojamiento_id,
                        'result' => $total,
                        'reload' => $reload,
                        'total_txt' => ConfigHelper::parseMoneda(($total), $moneda),
                        'precio_txt' => ConfigHelper::parseMoneda(($importe), $moneda),
                        'precio_extra_txt' => ConfigHelper::parseMoneda(($importe_extra), $moneda),
                        'precio_extra' => $importe_extra,
                        'fechafin' => $fechafin->format('d/m/Y'),
                        'semanas' => $semanas
                    ];

                    $ficha->pdf();

                    return response()->json($result, 200);
                }
                break;

            case 'extra-curso': {
                    $valor = $request->input('valor');
                    $unidades = $request->input('unidades');

                    $error = null;
                    if ($unidades > $ficha->semanas) {
                        BookingLog::addLog($ficha, "Extra Curso [$id] [unidadades: $unidades > $ficha->semanas]");
                        $unidades = $ficha->semanas;

                        $error = trans('area.booking.semanas-max', ['maxs' => $unidades]);
                    }

                    $precio = 0;

                    if ($valor == 'false') {
                        $extra = CursoExtra::find($id);

                        $e = BookingExtra::add($extra, $booking_id, 0, $unidades);

                        $precio = ConfigHelper::parseMoneda(($e->precio * $e->unidades), $e->moneda->name);

                        $res = true;

                        BookingLog::addLog($ficha, "Extra Curso [$id] [on]");
                    } else {
                        BookingExtra::where('booking_id', $booking_id)->where('tipo', 0)->where('extra_id', $id)->delete();

                        BookingLog::addLog($ficha, "Extra Curso [$id] [off]");
                    }

                    $ficha->pdf();

                    $result = ['campo' => $campo, 'id' => $id, 'result' => $res, 'precio' => $precio, 'unidades' => $unidades, 'error' => $error];
                    return response()->json($result, 200);
                }
                break;

            case 'extra-centro': {
                    $valor = $request->input('valor');
                    $unidades = $request->input('unidades');

                    $precio = 0;

                    if ($valor == 'false') {
                        $extra = CentroExtra::find($id);

                        $e = BookingExtra::add($extra, $booking_id, 1, $unidades);

                        $precio = ConfigHelper::parseMoneda(($e->precio * $e->unidades), $e->moneda_name);

                        $res = true;

                        BookingLog::addLog($ficha, "Extra Centro [$id] [on]");
                    } else {
                        BookingExtra::where('booking_id', $booking_id)->where('tipo', 1)->where('extra_id', $id)->delete();

                        BookingLog::addLog($ficha, "Extra Centro [$id] [off]");
                    }

                    $ficha->pdf();

                    $result = ['campo' => $campo, 'id' => $id, 'result' => $res, 'precio' => $precio];
                    return response()->json($result, 200);
                }
                break;

            case 'extra-alojamiento': //cuotas
                {
                    $valor = $request->input('valor');
                    $unidades = $request->input('unidades');

                    $precio = 0;

                    if ($valor == 'false') {
                        $extra = AlojamientoCuota::find($id);

                        $e = BookingExtra::add($extra, $booking_id, 3, $unidades);

                        $precio = ConfigHelper::parseMoneda(($e->precio * $e->unidades), $e->moneda_name);

                        $res = true;

                        BookingLog::addLog($ficha, "Extra Alojamiento [$id] [on]");
                    } else {
                        BookingExtra::where('booking_id', $booking_id)->where('tipo', 3)->where('extra_id', $id)->delete();

                        BookingLog::addLog($ficha, "Extra Alojamiento [$id] [off]");
                    }

                    $ficha->pdf();

                    $result = ['campo' => $campo, 'id' => $id, 'result' => $res, 'precio' => $precio];
                    return response()->json($result, 200);
                }
                break;

            case 'extra-generico': {
                    $valor = $request->input('valor');
                    $unidades = $request->input('unidades');

                    $precio = 0;

                    if ($valor == 'false') {
                        $extra = CursoExtraGenerico::find($id);

                        $e = BookingExtra::add($extra, $booking_id, 2, $unidades);

                        $iprecio = $e->precio * $e->unidades;

                        if ($extra->tipo_unidad == 2) {
                            $iprecio = $e->precio;

                            $t = $ficha->precio_total;
                            $iprecio = ($e->precio * $t['total_neto']) / 100;
                        }

                        $precio = ConfigHelper::parseMoneda($iprecio, $extra->moneda_name);

                        $res = true;

                        BookingLog::addLog($ficha, "Extra Genérico [$id] [on]");
                    } else {
                        BookingExtra::where('booking_id', $booking_id)->where('tipo', 2)->where('extra_id', $id)->delete();

                        BookingLog::addLog($ficha, "Extra Genérico [$id] [off]");
                    }

                    $ficha->pdf();

                    $result = ['campo' => $campo, 'id' => $id, 'result' => $res, 'precio' => $precio];
                    return response()->json($result, 200);
                }
                break;

            case 'extra-unidades': {
                    $valor = $request->input('valor');
                    $precio = $request->input('precio');
                    $tipo = $request->input('tipo');

                    $t = "";
                    switch ($tipo) {
                        case 0: {
                                $t = 'extra-curso';
                            }
                            break;

                        case 1: {
                                $t = 'extra-centro';
                            }
                            break;

                        case 2: {
                                $t = 'extra-generico';
                            }
                            break;

                        case 3: {
                                $t = 'extra-alojamiento';
                            }
                            break;
                    }

                    $extra = BookingExtra::where('booking_id', $booking_id)->where('tipo', $tipo)->where('extra_id', $id)->first();
                    if (!$extra) {
                        $result = ['campo' => $campo, 'id' => $id, 'tipo' => $t, 'result' => 0, 'precio' => "error"];
                        return response()->json($result, 200);
                    }

                    $extra->unidades = $valor;
                    $extra->precio = $precio;
                    $extra->save();

                    $res = $valor * $precio;
                    $precio = ConfigHelper::parseMoneda($res, $extra->moneda_name);

                    BookingLog::addLog($ficha, "Unidades Extra ($t) [$valor]");

                    $ficha->pdf();

                    $result = ['campo' => $campo, 'id' => $id, 'tipo' => $t, 'result' => $res, 'precio' => $precio];
                    return response()->json($result, 200);
                }
                break;

            case 'booking-vuelo': {
                    $valor = $request->input('valor');
                    $aporte = $request->input('aporte');

                    if ($valor == "false") {
                        $ficha->vuelo_id = $id;
                        $valor = 1;
                    } else {
                        $ficha->vuelo_id = 0;
                        $valor = 0;
                    }

                    $ficha->transporte_recogida = $aporte;

                    $ficha->save();

                    BookingLog::addLog($ficha, "Vuelo select ($id) [$valor]");

                    $result = ['campo' => $campo, 'id' => $id, 'result' => $valor, 'online' => $ficha->es_online];
                    return response()->json($result, 200);
                }
                break;
        }

        $result = ['campo' => $campo, 'id' => $id, 'result' => $res, 'error' => $error, 'online' => $ficha->es_online];
        return response()->json($result, 200);
    }

    public function dttCursos(Request $request, $viajero_id, $booking_id = 0, $presupuesto = false)
    {
        if (!$request->ajax()) {
            abort(404);
        }

        $this->curso->pushCriteria(new FiltroPlataformaCentro());

        $category_id = (int) ($request->has('cat') ? $request->get('cat') : null);
        $subcategory_id = (int) ($request->has('scat') ? $request->get('scat') : null);

        $col = $this->curso->findAllBy('course_active', 1);

        if ($category_id) {
            $col = $col->where('category_id', $category_id);
        }

        if ($subcategory_id) {
            $col = $col->where('subcategory_id', $subcategory_id);
        }

        return Datatable::collection($col)
            ->showColumns('course_name')
            // ->addColumn('course_name', function($model) {
            //     // if($model->es_convocatoria_multi)
            //     // {
            //     //     return $model->convocatorias_multi->first()->name;
            //     // }
            //     return $model->course_name;
            // })
            ->addColumn('name', function ($model) {
                return $model->course_name . " <a target='_blank' href='" . route('manage.cursos.ficha', [$model->id]) . "' class='pull-right btn btn-info btn-xs'><i class='fa fa-arrow-circle-right'></i> Ver Curso</a>";
            })
            ->addColumn('categoria', function ($model) {
                return $model->categoria_name;
            })
            ->addColumn('proveedor', function ($model) {
                return $model->centro->proveedor->name;
            })
            ->addColumn('centro', function ($model) {
                return $model->centro->name;
            })
            ->addColumn('convocatoria', function ($model) {
                $ret = $model->convocatoria_tipo;

                if ($ret == 'cerrada') {
                    $ret .= " (" . $model->convocatoriasCerradas->count() . ")";
                }

                if ($ret == 'abierta') {
                    $ret .= " (" . $model->convocatoriasAbiertas->count() . ")";
                }

                if ($ret == 'multiconvocatoria') {
                    $ret .= " (" . $model->convocatoriasMulti->count() . ")";
                }

                return $ret;
            })
            ->addColumn('options', function ($model) use ($viajero_id, $booking_id, $presupuesto) {

                $ret = "";

                $ret .= "<a href='" . route('manage.bookings.crear', [$booking_id, $viajero_id, $model->id, $presupuesto]) . "' class='btn btn-warning btn-xs'><i class='fa fa-plus-circle'></i> Seleccionar</a>";

                return $ret;
            })
            ->searchColumns('name', 'proveedor', 'centro')
            // ->orderColumns('name','categoria','proveedor','centro','convocatoria')
            ->orderColumns('*')
            ->setAliasMapping()
            ->setSearchStrip()->setOrderStrip()
            ->make();
    }



    public function getNuevo($viajero_id, $booking_id = 0)
    {
        $viajero = $this->viajero->find($viajero_id);
        // if(!$viajero->solicitud)
        // {
        //     Session::flash('mensaje-alert', 'No se puede crear una Inscripción sin una Solicitud.');
        //     return redirect()->route('manage.viajeros.ficha',$viajero_id);
        // }

        $this->curso->pushCriteria(new FiltroPlataformaCentro());

        Session::forget('vcn.booking_id');

        if (Input::has('booking_user')) {
            Session::put('vcn.booking_user', Input::get('booking_user'));
        }

        $viajero_full_name = $this->viajero->find($viajero_id)->full_name;

        return view('manage.bookings.list_cursos', compact('viajero_id', 'viajero_full_name', 'booking_id'));
    }

    public function getPresupuesto($viajero_id)
    {
        $viajero = $this->viajero->find($viajero_id);

        $this->curso->pushCriteria(new FiltroPlataformaCentro());

        $viajero_full_name = $this->viajero->find($viajero_id)->full_name;
        $presupuesto = true;
        $booking_id = 0;

        return view('manage.bookings.list_cursos', compact('viajero_id', 'viajero_full_name', 'booking_id', 'presupuesto'));
    }

    public function getIndexByViajero($viajero_id)
    {
        if (Datatable::shouldHandle()) {
            $col = $this->viajero->find($viajero_id)->bookings;

            return Datatable::collection($col)
                ->addColumn('fecha_reserva', function ($model) {
                    return $model->fecha_reserva ? $model->fecha_reserva->format('Y-m-d') : '-';
                })
                ->addColumn('fecha', function ($model) {
                    return "<a href='" . route('manage.bookings.ficha', $model->id) . "'>" . Carbon::parse($model->course_start_date)->format('d/m/Y') . "</a>";
                })
                ->addColumn('curso', function ($model) {
                    $curso = $model->curso ? "<a data-label='Ficha Curso' href='" . route('manage.cursos.ficha', $model->curso->id) . "'>" . $model->curso->name . "</a>" : "-";
                    $convocatoria = $model->convocatoria ? "<a data-label='Ficha Convocatoria' href='" . route('manage.convocatorias.cerradas.ficha', $model->convocatoria->id) . "'>" . $model->convocatoria->name . "</a>" : "-";
                    return $model->curso->es_convocatoria_cerrada ? $convocatoria : $curso;
                })
                ->addColumn('duracion', function ($column) {
                    $model = BookingModel::find($column->id);
                    return $model->convocatoria ? $model->convocatoria->duracion_name : $model->curso->duracion_name;
                })
                ->addColumn('status', function ($model) {
                    $ret = "<span class='badge'>" . $model->status_name . "</span>&nbsp;";

                    if ($model->es_directo) {
                        $ret .= "[D]";
                    }

                    // $statuses = [""=>""] + Status::where('manual',1)->orderBy('orden')->pluck('name','id')->toArray();

                    // $ret .= "<div class='pull-right'>";
                    // $ret .= Form::select("status-$model->id", $statuses, $model->status_id, array( 'data-id'=> $model->id, 'class'=> 'form-control status-item'));

                    // $ret .= "&nbsp;&nbsp;<a href='#checklist' data-id='". $model->id ."' data-label='Checklist' class='checklist-item'><i class='fa fa-tasks'></i></a>";
                    // $ret .= "</div>";

                    if ($model->incidencias->count()) {
                        $icono = "<i class='badge badge-warning badge-incidencias'>" . $model->incidencias->count() . "</i>";
                        if ($model->tiene_incidencias_pendientes) {
                            $icono = "<i class='fa fa-exclamation-triangle' style='color:red;'></i>";
                        }

                        $ret .= "<a href='" . route('manage.bookings.ficha', $model->id) . "#incidencias'>$icono</a>";
                    }

                    return $ret;
                })
                // ->addColumn('fase', function($model) {
                //     return ConfigHelper::getBookingFase($model->fase);
                // })
                ->addColumn('options', function ($model) {

                    $ret = "";

                    // Cancelar:
                    $pregunta = "Vas a cancelar una inscripción y liberar una plaza. Esta inscripción se seguirá mostrando en el área de cliente. Para que no aparezca, tienes que quitar el check 'Área cliente' en la ficha booking.";
                    if($model->tiene_facturas)
                    {
                        $pregunta .= " [TIENE FACTURAS]";
                    }
                    $data = " data-label='Cancelar' data-pregunta='$pregunta' data-action='" . route('manage.bookings.cancelar', $model->id) . "'";
                    $ret .= " <a $data href='#confirma' class='btn btn-danger btn-xs'><i class='fa fa-ban'></i></a>";

                    if ($model->viajero->booking_id != $model->id) {
                        $ret .= " <a data-label='Asociar' href='" . route('manage.bookings.asociar', [$model->id]) . "' class='btn btn-warning btn-xs'><i class='fa fa-user-plus'></i></a>";
                    }

                    $pregunta = "<br><br>Vas a borrar todo el historial de esta reserva que desaparecerá totalmente. Se recomienda utilizar este botón SÓLO en caso de haber entrado erróneamente una inscripción. Si es una cancelación, utilizar el botón rojo de Cancelar. ¿Seguir y eliminar la inscripción?";

                    if (!$model->es_empezado && ConfigHelper::canEdit('booking-borrar')) {
                        $data = " data-label='Borrar' data-model='Inscripción. $pregunta' data-action='" . route('manage.bookings.delete', [$model->id, 'v=' . $model->viajero->id]) . "'";
                        $ret .= " <a href='#destroy' $data data-alerta='' data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";
                    }

                    if ($model->es_terminado && $model->pdf_link) {
                        $ret .= " <a href='" . $model->pdf_link . "' target='_blank' class='btn btn-danger btn-xs'><i class='fa fa-file-pdf-o'></i> PDF</a>";
                    }

                    return $ret;
                })
                ->searchColumns('curso')
                ->orderColumns('fecha', 'curso', 'fase')
                ->setAliasMapping()
                ->make();
        }

        // return view('manage.bookings.index.viajero', compact('viajero_id'));
    }

    public function getIndexByAlojamiento($convocatory_id, $alojamiento_id)
    {
        if (Datatable::shouldHandle()) {
            $col = $this->booking->findWhere(['fase' => 4, 'accommodation_id' => $alojamiento_id, 'convocatory_close_id' => $convocatory_id]);

            return Datatable::collection($col)
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('fecha', function ($model) {
                    return "<a href='" . route('manage.bookings.ficha', $model->id) . "'>" . Carbon::parse($model->course_start_date)->format('d/m/Y') . "</a>";
                })
                // ->addColumn('fase', function($model) {
                //     return ConfigHelper::getBookingFase($model->fase);
                // })
                ->addColumn('status', function ($model) {
                    $ret = "<span class='badge'>" . ($model->status_name) . "</span>";
                    return $ret;
                })
                ->addColumn('oficina', function ($model) {
                    return $model->oficina_name;
                })
                ->searchColumns('viajero', 'oficina')
                ->orderColumns('fecha', 'viajero', 'oficina')
                ->setAliasMapping()
                ->make();
        }

        // return view('manage.bookings.index.viajero', compact('viajero_id'));
    }

    public function getCambiar($booking_id)
    {
        $ficha = $this->booking->find($booking_id);

        Session::put('vcn.booking_user', $ficha->user_id);

        $viajero_id = $ficha->viajero_id;;

        BookingLog::addLog($ficha, "Cambio de curso");

        $ficha->delete();

        if ($ficha->es_presupuesto) {
            return redirect()->route('manage.bookings.presupuesto', $viajero_id);
        }

        return redirect()->route('manage.bookings.nuevo', $viajero_id);
    }

    public function getCrear($booking_id = 0, $viajero_id, $curso_id, $presupuesto = false)
    {
        //Cambiar curso
        /*if($booking_id && $curso_id)
        {
            $ficha = $this->booking->find($booking_id);

            if($ficha->curso_id != $curso_id)
            {
                $ficha->curso_id = $curso_id;
                $ficha->fase = 1;
                $ficha->save();

                $ficha->deleteExtras();
                $ficha->setExtras();
            }

            return redirect()->route('manage.bookings.ficha',$booking_id);
        }*/

        // $viajero = Viajero::find($viajero_id);
        // if(!$viajero->solicitud)
        // {
        //     Session::flash('mensaje-alert', 'No se puede crear una Inscripción sin una Solicitud.');
        //     return redirect()->route('manage.viajeros.ficha',$viajero_id);
        // }

        if (!$presupuesto) {
            // $booking_id = Session::get('vcn.booking_id',0);
        }

        $booking_user = Session::get('vcn.booking_user', Auth::user()->id);

        $data = [];
        $data['viajero_id'] = $viajero_id;
        $data['curso_id'] = $curso_id;
        $data['user_id'] = $booking_user ?: 0;
        $data['es_pdf'] = false;

        if (!$booking_id) {
            $booking = $this->booking->create($data);
            if (!$booking->importes) {
                $booking->updateImportesInit();
            }

            $curso = $this->curso->find($curso_id);

            $booking_id = $booking->id;
            $viajero = $booking->viajero;

            $booking->plataforma = $booking->viajero->plataforma;
            $booking->oficina_id = $booking->viajero->oficina_asignada;
            $booking->cancelacion = false;
            $booking->promo_cambio_fijo = $booking->promo_cambio_fijo_default;
            $booking->category_id = $curso->category_id;
            $booking->subcategory_id = $curso->subcategory_id;
            $booking->subcategory_det_id = $curso->subcategory_det_id;
            $booking->prescriptor_id = $viajero->prescriptor_id;
            $booking->save();

            //Archivos booking_id 0
            if (!$presupuesto) {
                foreach ($booking->viajero->archivos->where('booking_id', 0) as $va) {
                    $va->booking_id = $booking_id;
                    $va->save();
                }

                ViajeroLog::addLog($booking->viajero, "Nuevo Booking [$booking->id]");
            } else {
                ViajeroLog::addLog($booking->viajero, "Nuevo Presupuesto [$booking->id]");
            }

            Session::put('vcn.booking_id', $booking_id);

            $booking->setExtras();
            $booking->prescriptor_no_facturar;

            BookingLog::addLog($booking, "Nuevo - Curso [$curso_id]");

            Session::forget('vcn.booking_user');
        }

        $ficha = $this->booking->find($booking_id);

        if (!$presupuesto) {
            $ficha->viajero->booking_id = $booking_id;
            $ficha->viajero->booking_status_id = $ficha->status_id;
            $ficha->viajero->save();

            //Asociamos solicitud y booking
            $sid = $ficha->viajero->solicitud_id;
            if(!$sid)
            {
                $solicitud = $ficha->viajero->solicitudes->sortByDesc('id')->first();
                $sid = $solicitud ? $solicitud->id : 0;
            }
            $ficha->solicitud_id = $sid;

            //Archivamos solicitud
            $solicitud = $ficha->viajero->solicitud;
            if ($solicitud) {
                $solicitud->setInscrito("booking-crear");
            }
        }

        $ficha->es_presupuesto = $presupuesto;

        $ficha->fase = 1;
        $ficha->save();

        // return view('manage.bookings.new', compact('ficha', 'semanas', 'descuentos'));
        return redirect()->route('manage.bookings.ficha', $booking_id);
    }

    public function postCrear(Request $request, $id)
    {
        $data = $request->except('_token');
        // $this->booking->update($data, $id);

        $esDirecto = (bool) $request->get('booking_directo');

        $fase = (int) $request->input('fase');

        $ficha = $this->booking->find($id);
        if (!$ficha) {
            abort(404);
        }
        // $fase = $ficha->fase;

        BookingLog::addLogDatos($ficha, $data, $fase); //PENDIENTE afinar??

        //Posible bug con booking_id
        if ($ficha->viajero && $ficha->viajero->booking_id != $ficha->id && !$ficha->es_presupuesto) {
            $ficha->viajero->booking_id = $ficha->id;
            $ficha->viajero->booking_status_id = $ficha->status_id;
            $ficha->viajero->save();
        }

        $fase_new = 1; //default

        $plataforma = ConfigHelper::config('propietario');

        switch ($fase) {
            case 1: //CURSO / Presupuesto
                {
                    if ($ficha->es_presupuesto) {
                        $ficha->presupuesto();

                        Session::flash('tab', '#archivos');
                        return redirect()->route('manage.viajeros.ficha', $ficha->viajero->id);
                    }

                    //booking-directo
                    if ($esDirecto) {
                        $ficha->directo();
                        return redirect()->route('manage.viajeros.ficha', $ficha->viajero->id);
                    }

                    if ($request->input('prescriptor_id')) {
                        $ficha->prescriptor_id = $request->input('prescriptor_id');
                        $ficha->save();
                    }

                    if (!$ficha->convocatoria) {
                        Session::flash('mensaje-alert', 'Debe seleccionar una convocatoria.');
                        return redirect()->route('manage.bookings.ficha', $id);
                    }

                    if ($ficha->es_convocatoria_multi && !$ficha->weeks) {
                        Session::flash('mensaje-alert', 'Debe seleccionar una duración.');
                        return redirect()->route('manage.bookings.ficha', $id);
                    }

                    $ficha->transporte_no = $request->has('transporte_no');
                    $ficha->transporte_otro = $request->has('transporte_no') ? 0 : $request->has('transporte_otro');
                    $ficha->transporte_detalles = $request->has('transporte_otro') ? $request->input('transporte_detalles') : "";
                    $ficha->transporte_requisitos = $request->has('transporte_no') ? "" : $request->input('transporte_requisitos');

                    //Puede venir puesto del tema aeropuertos.
                    $ficha->transporte_recogida = $ficha->transporte_recogida ?: ($request->has('transporte_recogida_bool') ? $request->input('transporte_recogida') : "");
                    $ficha->save();

                    //comprobamos fecha semiopen
                    if ($ficha->curso->es_convocatoria_cerrada) {
                        if (!$ficha->convocatoria) {
                            Session::flash('mensaje-alert', 'Debe seleccionar una convocatoria.');
                            return redirect()->route('manage.bookings.ficha', $id);
                        }

                        // if(!$ficha->alojamiento)
                        // {
                        //     Session::flash('mensaje-alert', 'Debe seleccionar un alojamiento.');
                        //     return redirect()->route('manage.bookings.ficha',$id);
                        // }

                        $cc = ConvocatoriaCerrada::find($ficha->convocatory_close_id);
                        if ($cc->convocatory_semiopen) {
                            $this->validate($request, [
                                'course_start_date' => 'required|date_format:d/m/Y',
                            ]);

                            /*$fecha = Carbon::createFromFormat('d/m/Y', $request->input('course_start_date'));
                        $startDate = Carbon::createFromFormat('d/m/Y',$cc->start_date);
                        $endDate = Carbon::createFromFormat('d/m/Y',$cc->end_date);

                        if( $fecha->gte($startDate) && $fecha->lte($endDate) )
                        {
                            $f = Carbon::parse($fecha);
                            $ficha->course_start_date = $f->format('Y-m-d');
                            $ficha->course_end_date = $ficha->calcularFechaFin($f,$cc->convocatory_close_duration_weeks,$cc->convocatory_semiopen_start_day,$cc->convocatory_semiopen_end_day)->format('Y-m-d');

                            $ficha->accommodation_start_date = $ficha->course_start_date;
                            if($ficha->alojamiento->start_day)
                            {
                                $ficha->accommodation_start_date = ConfigHelper::calcularFechaDia($ficha->course_start_date, $ficha->alojamiento->start_day);
                            }

                            $ficha->accommodation_end_date = $ficha->course_end_date;
                            if($ficha->alojamiento->end_day)
                            {
                                $ficha->accommodation_end_date = ConfigHelper::calcularFechaDia($ficha->course_end_date, $ficha->alojamiento->end_day);
                            }

                            $ficha->save();
                        }
                        else
                        {
                            $ficha->course_start_date = "";
                            $ficha->save();
                            Session::flash('mensaje', 'Error en Fecha de Inicio.');
                        }*/
                        }

                        if ($ficha->vuelos) {
                            if (!$ficha->vuelo && !$ficha->transporte_no && !$ficha->transporte_otro) {
                                Session::flash('mensaje-alert', 'Es obligatorio seleccionar una opción de transporte.');
                                return redirect()->route('manage.bookings.ficha', $id);
                            }
                        }

                        if ($ficha->transporte_no || $ficha->transporte_otro) {
                            $ficha->vuelo_id = 0; //Anulamos el vuelo
                            $ficha->save();
                        }

                        //Prescriptor => viajero
                        $ficha->viajero->prescriptor_id = $ficha->prescriptor_id;
                        $ficha->viajero->save();
                    } else if ($ficha->curso->es_convocatoria_abierta) {
                        $this->validate($request, [
                            'booking-cabierta-fecha_ini' => 'required',
                            // 'booking-alojamiento-fecha_ini' => 'required',

                            'booking-cabierta-semanas' => 'required|min:1',
                            // 'booking-alojamiento-semanas' => 'required|min:1',
                        ]);
                    }

                    if ($request->has('booking-seguro-ajax') && !$ficha->cancelacion) {
                        $ficha->setCancelacion();
                    }

                    $ficha->online_aeropuertos();

                    $fase_new = 2;
                }
                break;

            case 2: //datos viajero
                {
                    if (!$ficha->viajero_id) {
                        $uTutor = $ficha->online_tutor;

                        //Es tutor
                        $viajero = new ViajeroModel;
                        $viajero->plataforma = $plataforma;
                        $viajero->save();

                        $vd = new ViajeroDatosModel;
                        $vd->viajero_id = $viajero->id;
                        $vd->save();

                        if ($uTutor) {
                            $vt = new ViajeroTutor;
                            $vt->viajero_id = $viajero->id;
                            $vt->tutor_id = $uTutor->ficha->id;
                            $vt->relacion = 0;
                            $vt->save();
                        }

                        $ficha->setViajero($viajero->id, $request);
                    }

                    // $this->validate($request, [
                    //     // 'idioma' => 'required|min:1',
                    //     'name' => 'required',
                    //     'lastname' => 'required',
                    //     'sexo' => 'required',
                    //     'nacionalidad' => 'required',
                    //     // 'movil' => 'required',
                    //     'fechanac' => 'required|date_format:d/m/Y',
                    //     'pais'=> 'required|min:1',
                    //     'provincia'=> 'required|min:1',
                    //     // 'escuela_curso'=> 'required|min:1',
                    // ]);

                    $ficha->updateDatosCampamento($request);

                    $viajero = $ficha->viajero;
                    $datos = $ficha->viajero->datos;

                    //Viajero->datos
                    $data['viajero_id'] = $viajero->id;

                    $data['pasaporte_emision'] = $request->get('pasaporte_emision') ? Carbon::createFromFormat('d/m/Y', $data['pasaporte_emision'])->format('Y-m-d') : "";
                    $data['pasaporte_caduca'] = $request->get('pasaporte_caduca') ? Carbon::createFromFormat('d/m/Y', $data['pasaporte_caduca'])->format('Y-m-d') : "";
                    $data['ingles'] = $request->get('ingles_academia_bool');
                    $data['viajero_id'] = $viajero->id;

                    $data['medicacion'] = $request->input('medicacion_bool') ? $request->input('medicacion') : "";
                    $data['alergias'] = $request->input('alergias_bool') ? $request->input('alergias') : "";
                    $data['tratamiento'] = $request->input('tratamiento_bool') ? $request->input('tratamiento') : "";
                    $data['enfermedad'] = $request->input('enfermedad_bool') ? $request->input('enfermedad') : "";
                    $data['dieta'] = $request->input('dieta_bool') ? $request->input('dieta') : "";
                    $data['animales'] = $request->input('animales_bool') ? $request->input('animales') : "";
                    $data['medicacion2'] = $request->input('medicacion_bool') ? $request->input('medicacion2') : "";
                    $data['alergias2'] = $request->input('alergias_bool') ? $request->input('alergias2') : "";
                    $data['tratamiento2'] = $request->input('tratamiento_bool') ? $request->input('tratamiento2') : "";
                    $data['enfermedad2'] = $request->input('enfermedad_bool') ? $request->input('enfermedad2') : "";
                    $data['dieta2'] = $request->input('dieta_bool') ? $request->input('dieta2') : "";

                    if (!$datos) {
                        $this->datos->create($data);
                    } else {
                        $viajero->datos->update($data);
                    }

                    $data['fechanac'] = $request->has('fechanac') ? Carbon::createFromFormat('d/m/Y', $data['fechanac'])->format('Y-m-d') : null;
                    $viajero->update($data);

                    $ficha->notas = $request->input('notas');
                    $ficha->save();

                    $ficha->updateDatos();

                    if ($viajero->id && $viajero->es_email_tutor) {
                        Session::flash('mensaje', "No se actualiza el email por pertenecer ya a un Tutor [" . $viajero->es_email_tutor->full_name . "].");
                        $viajero->email = null;
                        $viajero->save();
                        $ficha->datos->email = null;
                        $ficha->datos->save();
                    }

                    //update codigo
                    $codigo = $ficha->descuentos_codigo->first();
                    if ($codigo) {
                        $dto = DescuentoTipo::where('codigo', $codigo->codigo)->where('activo', 1)->first();
                        $ficha->setDescuentoCodigo($dto);
                    }

                    $fase_new = 3;
                }
                break;

            case 3: //datos tutores
                {
                    $totTutores = $ficha->viajero->tutores->count();

                    $data = $request->input();
                    $i = 1;
                    foreach ($ficha->viajero->tutores as $tutor) {
                        $this->validate($request, [
                            "name_$i" => 'required',
                            "lastname_$i" => 'required',
                            "relacion_$i" => 'required',
                            // 'email' => 'required|email'
                        ]);

                        $tutor->name = $data["name_$i"];
                        $tutor->lastname = $data["lastname_$i"];
                        $tutor->nif = $data["nif_$i"];
                        $tutor->tipodoc = $data["tipodoc_$i"] ?: 0;
                        $tutor->phone = $data["phone_$i"];
                        $tutor->movil = $data["movil_$i"];
                        $tutor->save();
                        $tutor->pivot->relacion = $data["relacion_$i"];
                        $tutor->pivot->save();

                        $email = $data["email_$i"];
                        if ($tutor->email != $email) {
                            if (Tutor::checkEmail($email)) {
                                Session::flash('mensaje', "E-mail [$email] ya está en uso para un Tutor existente en nuestra base de datos. Compruebe los datos o contacte con una oficina nuestra.");
                                return redirect()->back(); //->withInput();
                            }
                        }

                        $tutor->email = $email;
                        $tutor->save();

                        $i++;
                    }

                    //Nuevos: 10 y 11
                    for ($i = 11; $i <= 12; $i++) {
                        if ($request->get("name_$i") != "") {
                            $this->validate($request, [
                                "name_$i" => 'required',
                                "lastname_$i" => 'required',
                                "relacion_$i" => 'required',
                                // 'email' => 'required|email'
                            ]);

                            $tutor = new Tutor;
                            $tutor->name = $data["name_$i"];
                            $tutor->lastname = $data["lastname_$i"];
                            $tutor->nif = $data["nif_$i"];
                            $tutor->tipodoc = $data["tipodoc_$i"] ?: 0;
                            $tutor->phone = $data["phone_$i"];
                            $tutor->movil = $data["movil_$i"];
                            $tutor->plataforma = $ficha->viajero->plataforma;
                            $tutor->save();

                            $vt = new ViajeroTutor;
                            $vt->viajero_id = $ficha->viajero->id;
                            $vt->tutor_id = $tutor->id;
                            $vt->relacion = $data["relacion_$i"];
                            $vt->save();

                            $email = $data["email_$i"];
                            if (Tutor::checkEmail($email)) {
                                Session::flash('mensaje', "E-mail [$email] en uso para Tutor. Compruebe los datos.");
                                return redirect()->back();
                            }

                            $tutor->email = $email;
                            $tutor->save();
                        }
                    }

                    $totTutores = $ficha->viajero->tutores->count();
                    if ($ficha->curso->es_menor) {
                        if ($totTutores < 1) {
                            Session::flash('mensaje', 'Debe tener al menos un Tutor con un email válido.');
                            return redirect()->back();
                        }

                        //if los tutores no tienen mail
                        if ($ficha->viajero->tutores->where('email', '')->count() == $totTutores) {
                            Session::flash('mensaje', 'Comprobar el campo email del/los Tutor/es.');
                            return redirect()->back();
                        }
                    }

                    if (!$ficha->factura_cif && ConfigHelper::config('nif')) {
                        $nif = (bool) $request->get('booking-nif', false);
                        if (!$nif) {
                            Session::put('vcn.booking-nif', 1);
                            return redirect()->back();
                        }

                        $fecha = Carbon::now()->format('d/m/Y - H:i');
                        $ip = $request->ip();
                        BookingLog::addLog($ficha, 'booking-nif-no', "Declara que no tiene [$fecha] [$ip]");
                    }

                    Session::forget('vcn.booking-nif');

                    $fase_new = 4;
                }
                break;

            case 4: //Resumen
                {
                    $this->validate($request, [
                        // 'condiciones' => 'required'
                    ]);

                    $data = $request->except('_token');

                    $ficha->mail_confirmacion = $request->input('mail_confirmacion');
                    $ficha->area = $request->has('area');
                    $ficha->area_pagos = $request->has('area_pagos');
                    $ficha->area_reunion = $request->has('area_reunion');
                    $ficha->save();

                    if ($ficha->curso->es_convocatoria_abierta) //convo abierta
                    {
                        $p = new BookingPago;
                        $p->booking_id = $ficha->id;
                        $p->user_id = $request->user()->id;
                        $p->fecha = Carbon::createFromFormat('d/m/Y', $request->input('pago-fecha'))->format('Y-m-d');
                        $p->importe_pago = $request->input('pago-importe');
                        $p->moneda_id = $request->input('pago-moneda');
                        $p->rate = $request->input('pago-rate');
                        $p->tipo = $request->input('pago-tipo');
                        $p->notas = 'Reserva inicial';
                        $p->avisado = $ficha->mail_confirmacion ? 0 : 2;
                        $p->importe = $p->importe_pago * $p->rate;
                        $p->save();

                        $ficha->fecha_pago1 = $p->fecha;
                        $ficha->promo_cambio_fijo = $ficha->promo_cambio_fijo_default;
                        $ficha->save();

                        $ficha->setStatus(ConfigHelper::config('booking_status_prebooking'));
                        BookingLog::addLog($ficha, "Pre-Booking + Pago");
                    } elseif ($ficha->curso->es_convocatoria_cerrada) {
                        if ($request->has('pre-reserva')) {
                            $ficha->setStatus(ConfigHelper::config('booking_status_prereserva'));
                            BookingLog::addLog($ficha, "Pre-Reserva");
                        } else {
                            $plazas_vuelo = $ficha->vuelo ? $ficha->vuelo->plazas_disponibles : 1; //pq si no hay vuelo es como si tuviera plaza
                            $plazas_umbral = $ficha->vuelo ? $ficha->vuelo->plazas_umbral : 0;

                            if ($ficha->alojamiento)
                            {
                                $pa = $ficha->convocatoria->getPlazas($ficha->alojamiento->id);
                                $plazas_alojamiento = $pa ? $pa->plazas_disponibles : 0;
                            } else {
                                $pa = null;
                                $plazas_alojamiento = 1;
                            }


                            $tipo = (int) $request->input('reserva-tipo');
                            switch ($tipo) {
                                case 1: //PLAZAS+PAGO => PRE-BOOKING
                                    {
                                        $p = new BookingPago;
                                        $p->booking_id = $ficha->id;
                                        $p->user_id = $request->user()->id;
                                        $p->fecha = Carbon::createFromFormat('d/m/Y', $request->input('pago-fecha'))->format('Y-m-d');
                                        $p->importe_pago = $request->input('pago-importe');
                                        $p->moneda_id = $request->input('pago-moneda');
                                        $p->rate = $request->input('pago-rate');
                                        $p->tipo = $request->input('pago-tipo');
                                        $p->notas = 'Reserva inicial';
                                        $p->avisado = $ficha->mail_confirmacion ? 0 : 2;
                                        $p->importe = $p->importe_pago * $p->rate;
                                        $p->save();

                                        $ficha->fecha_pago1 = $p->fecha;
                                        $ficha->promo_cambio_fijo = $ficha->promo_cambio_fijo_default;
                                        $ficha->save();

                                        $ficha->setStatus(ConfigHelper::config('booking_status_prebooking'));
                                        BookingLog::addLog($ficha, "Pre-Brooking + Pago");
                                    }
                                    break;

                                case 2: //NOPLAZAS+CANCELAR => SOLICITUD DECIDIENDO
                                    {
                                        //Volvemos a la solicitud
                                        $ficha->decidiendo();

                                        $ficha->setStatus(0, 'Booking Cancelado', 'Pre-Reserva caducada');

                                        $ficha->pdf();

                                        BookingLog::addLog($ficha, "Reserva - Cancelada");

                                        return redirect()->route('manage.viajeros.ficha', $ficha->viajero_id);
                                    }
                                    break;

                                case 3: //NOPLAZAS+PAGO=> OVERBOOKING
                                    {
                                        $p = new BookingPago;
                                        $p->booking_id = $ficha->id;
                                        $p->user_id = $request->user()->id;
                                        $p->fecha = Carbon::createFromFormat('d/m/Y', $request->input('pago-fecha'))->format('Y-m-d');
                                        $p->importe_pago = $request->input('pago-importe');
                                        $p->moneda_id = $request->input('pago-moneda');
                                        $p->rate = $request->input('pago-rate');
                                        $p->tipo = $request->input('pago-tipo');
                                        $p->notas = 'Pago Overbooking';
                                        $p->importe = $p->importe_pago * $p->rate;
                                        $p->save();

                                        $ficha->ovbkg_id = $request->input('pago-ovbkg');
                                        $ficha->ovbkg_pv = $plazas_vuelo <= 0 ? true : false;
                                        $ficha->ovbkg_pa = $plazas_alojamiento <= 0 ? true : false;
                                        $ficha->fecha_pago1 = $p->fecha;
                                        $ficha->promo_cambio_fijo = $ficha->promo_cambio_fijo_default;
                                        $ficha->save();

                                        $ficha->setStatus(ConfigHelper::config('booking_status_overbooking'));
                                        BookingLog::addLog($ficha, "Overbooking + Pago");
                                    }
                                    break;

                                default: {
                                        BookingLog::addLog($ficha, "Error [$tipo] fase 3 => 4");
                                    }
                                    break;
                            }
                        }
                    } elseif ($ficha->curso->es_convocatoria_multi) {
                        if ($request->has('pre-reserva')) {
                            $ficha->setStatus(ConfigHelper::config('booking_status_prereserva'));
                            $ficha->save();

                            BookingLog::addLog($ficha, "Pre-Reserva");
                        } else {
                            $plazas = $ficha->convocatoria->hayPlazasDisponibles($ficha->id);

                            $tipo = $request->input('reserva-tipo');
                            switch ($tipo) {
                                case 1: //PLAZAS+PAGO => PRE-BOOKING
                                    {
                                        $p = new BookingPago;
                                        $p->booking_id = $ficha->id;
                                        $p->user_id = $request->user()->id;
                                        $p->fecha = Carbon::createFromFormat('d/m/Y', $request->input('pago-fecha'))->format('Y-m-d');
                                        $p->importe_pago = $request->input('pago-importe');
                                        $p->moneda_id = $request->input('pago-moneda');
                                        $p->rate = $request->input('pago-rate');
                                        $p->tipo = $request->input('pago-tipo');
                                        $p->notas = 'Reserva inicial';
                                        $p->avisado = $ficha->mail_confirmacion ? 0 : 2;
                                        $p->importe = $p->importe_pago * $p->rate;
                                        $p->save();

                                        $ficha->fecha_pago1 = $p->fecha;
                                        $ficha->promo_cambio_fijo = $ficha->promo_cambio_fijo_default;
                                        $ficha->save();

                                        $ficha->setStatus(ConfigHelper::config('booking_status_prebooking'));
                                        BookingLog::addLog($ficha, "Pre-Brooking + Pago");
                                    }
                                    break;

                                case 2: //NOPLAZAS+CANCELAR => SOLICITUD DECIDIENDO
                                    {
                                        //Volvemos a la solicitud
                                        $ficha->decidiendo();

                                        $ficha->setStatus(0, 'Booking Cancelado', 'Pre-Reserva caducada');

                                        $ficha->pdf();

                                        BookingLog::addLog($ficha, "Reserva - Cancelada");

                                        return redirect()->route('manage.viajeros.ficha', $ficha->viajero_id);
                                    }
                                    break;

                                case 3: //NOPLAZAS+PAGO=> OVERBOOKING
                                    {
                                        $p = new BookingPago;
                                        $p->booking_id = $ficha->id;
                                        $p->user_id = $request->user()->id;
                                        $p->fecha = Carbon::createFromFormat('d/m/Y', $request->input('pago-fecha'))->format('Y-m-d');
                                        $p->importe_pago = $request->input('pago-importe');
                                        $p->moneda_id = $request->input('pago-moneda');
                                        $p->rate = $request->input('pago-rate');
                                        $p->tipo = $request->input('pago-tipo');
                                        $p->notas = 'Pago Overbooking';
                                        $p->importe = $p->importe_pago * $p->rate;
                                        $p->save();

                                        $ficha->ovbkg_id = $request->input('pago-ovbkg');
                                        $ficha->ovbkg_pa = $plazas <= 0 ? true : false;
                                        $ficha->fecha_pago1 = $p->fecha;
                                        $ficha->promo_cambio_fijo = $ficha->promo_cambio_fijo_default;
                                        $ficha->save();

                                        $ficha->setStatus(ConfigHelper::config('booking_status_overbooking'));
                                        BookingLog::addLog($ficha, "Overbooking + Pago");
                                    }
                                    break;

                                default: {
                                        BookingLog::addLog($ficha, "Error [$tipo] fase 3 => 4");
                                    }
                                    break;
                            }
                        }
                    }

                    $fase_new = 4;

                    Session::forget('vcn.booking_id');

                    if ($ficha->status_id) {
                        Session::forget('vcn.booking_corporativo');
                        Session::forget('vcn.booking_multi');
                        $ficha->pdf();
                    }
                }
                break;
        }

        if ($fase_new > $ficha->fase) {
            $ficha->fase = $fase_new;
        }
        $ficha->save();

        $ficha->extraVuelo();

        // return view('manage.bookings.new', compact('ficha','semanas','descuentos','conocidos','subconocidos','subconocidosdet'));
        return redirect()->route('manage.bookings.ficha', $id);
    }

    public function getUpdate(Request $request, $id)
    {
        $user = $request->user();

        if (!$user->checkPermisoAislado('Booking', $id)) {
            Session::flash('mensaje-alert', "No tiene permisos. [Err:A].");
            return redirect()->route('manage.index');
        }

        $ficha = $this->booking->find($id);
        if (!$ficha) {
            abort(404);
        }

        if (!$ficha->importes) {
            $ficha->updateImportesInit();
        }


        // //Ojo: en booking online la fase 3 es la de tutor => ya no! ahora tienen las mismas fases
        // if($ficha->es_online && $ficha->fase==3)
        // {
        //     $ficha->fase = 2;
        //     $ficha->save();
        // }

        return $ficha->route_getUpdate();
    }

    public function postUpdateExtras(Request $request, $id)
    {
        Session::flash('tab', '#extras');

        $ficha = $this->booking->find($id);

        if ($request->has('booking-seguro')) {
            $ficha->setCancelacion();
        } else {
            $ficha->quitCancelacion();
        }

        $ficha->mailAvisoCambios();

        return redirect()->route('manage.bookings.ficha', $id);
    }

    public function postUpdate(Request $request, $id)
    {
        Session::flash('tab', '#datos');

        $ficha = $this->booking->find($id);

        // dd($request->input());

        //Asignaciones
        if ($request->has('submit_asignar')) {
            $oficina_id = (int) $request->get('oficina_id', null);
            $bOficina = false;
            if ($oficina_id && $oficina_id != $ficha->oficina_id) {
                \VCN\Models\Informes\Venta::remove($ficha);
                $bOficina = true;
            }

            $ficha->prescriptor_id = $request->get('prescriptor_id');
            $ficha->oficina_id = $oficina_id;
            $ficha->user_id = $request->get('user_id');
            $ficha->grado_ext = $request->get('grado_ext') ?: null;
            $ficha->save();

            //Ha cambiado la oficina
            if ($bOficina) {
                \VCN\Models\Informes\Venta::add($ficha);
                Session::flash('mensaje-ok', 'Booking cambiado de Oficina. Informe de Ventas actualizado.');
            }

            Session::flash('tab', '#datos');
            return redirect()->route('manage.bookings.ficha', $id);
        }

        if ($request->has('submit_asignar_info')) {
            
            $oficina_id = (int) $request->get('oficina_id', null);
            $bOficina = false;
            if ($oficina_id && $oficina_id != $ficha->oficina_id) {
                \VCN\Models\Informes\Venta::remove($ficha);
                $bOficina = true;
            }

            $ficha->prescriptor_id = $request->get('prescriptor_id');
            $ficha->oficina_id = $oficina_id;
            $ficha->user_id = $request->get('user_id');
            $ficha->grado_ext = $request->get('grado_ext') ?: null;

            $ficha->area = $request->has('area');
            $ficha->area_pagos = $request->has('area_pagos');
            $ficha->area_reunion = $request->has('area_reunion');
            
            $ficha->save();

            //Ha cambiado la oficina
            if ($bOficina) {
                \VCN\Models\Informes\Venta::add($ficha);
                Session::flash('mensaje-ok', 'Booking cambiado de Oficina. Informe de Ventas actualizado.');
            }

            return redirect()->route('manage.bookings.ficha', $id);
        }

        if ($request->has('submit_notas')) {
            $ficha->datos->alergias2 = $request->input('alergias2');
            $ficha->datos->enfermedad2 = $request->input('enfermedad2');
            $ficha->datos->medicacion2 = $request->input('medicacion2');
            $ficha->datos->tratamiento2 = $request->input('tratamiento2');
            $ficha->datos->dieta2 = $request->input('dieta2');
            $ficha->datos->save();

            $ficha->viajero->notas = $request->get('notas');
            $ficha->viajero->save();

            $ficha->notas2 = $request->input('booking_notas2');
            $ficha->save();

            $ficha->pdf_proveedor();

            Session::flash('tab', '#datos');
            return redirect()->route('manage.bookings.ficha', $id);
        }

        if ($request->has('submit_contactos_sos')) {
            $ficha->sos_proveedor = $request->get('sos_proveedor');
            $ficha->sos_plataforma = $request->get('sos_plataforma');
            $ficha->save();

            Session::flash('tab', '#ficha');
            return redirect()->route('manage.bookings.ficha', $id);
        }

        //niveles
        if ($request->has('submit_niveles')) {
            
            // $datos = $ficha->viajero->datos;
            // $datos->cic = $request->has('cic');
            // $datos->cic_nivel = $request->get('cic_nivel');
            // $datos->trinity_exam = $request->has('trinity_exam');
            // $datos->trinity_any = $request->get('trinity_any');
            // $datos->trinity_nivel = $request->get('trinity_nivel');
            // $datos->save();

            $datos = $ficha->datos;
            $datos->cic = $request->has('cic');
            $datos->cic_nivel = $request->get('cic_nivel');
            $datos->trinity_exam = $request->has('trinity_exam');
            $datos->trinity_any = $request->get('trinity_any');
            $datos->trinity_nivel = $request->get('trinity_nivel');
            $datos->save();

            $ficha->nivel_cic = $request->get('nivel_cic');
            $ficha->nivel_trinity = $request->get('nivel_trinity');
            $ficha->save();

            Session::flash('tab', '#niveles');
            return redirect()->route('manage.bookings.ficha', $id);
        }

        $this->validate($request, [
            // 'idioma-nivel' => 'required|min:1',
            'name' => 'required',
            'lastname' => 'required',
            'sexo' => 'required',
            'nacionalidad' => 'required',
            // 'movil' => 'required',
        ]);

        $data = $request->except('_token');

        BookingLog::addLogDatos($ficha, $data, 'ficha'); //PENDIENTE afinar??

        $viajero = $ficha->viajero;
        $datos = $ficha->viajero->datos;

        //Viajero->datos
        if (!$datos) {
            $datos_id = $this->datos->create($data);
        } else {
            $datos_id = $datos->id;

            $fillable = $datos;
            $columns = DB::connection()->getSchemaBuilder()->getColumnListing("viajero_datos");
            $fillable = $fillable->getFillable();
            $fields = array_keys($request->input());

            $except = array_diff($fields, $columns);
            $except[] = 'id';

            $datad = $request->except($except);
            $datad['viajero_id'] = $viajero->id;

            $datad['pasaporte_emision'] = $request->get('pasaporte_emision') ? Carbon::createFromFormat('d/m/Y', $request->get('pasaporte_emision'))->format('Y-m-d') : null;
            $datad['pasaporte_caduca'] = $request->get('pasaporte_caduca') ? Carbon::createFromFormat('d/m/Y', $request->get('pasaporte_caduca'))->format('Y-m-d') : null;

            $datad['ingles'] = $request->get('ingles_academia_bool');
            $datad['viajero_id'] = $viajero->id;

            $datos->update($datad);
        }

        //Viajero
        $viajero_id = $viajero->id;

        $fillable = $viajero;
        $columns = DB::connection()->getSchemaBuilder()->getColumnListing("viajeros");
        $fillable = $fillable->getFillable();
        $fields = array_keys($request->input());

        $except = array_diff($fields, $columns);
        $except[] = 'id';

        $data = $request->except($except);
        $data['fechanac'] = Carbon::createFromFormat('d/m/Y', $data['fechanac'])->format('Y-m-d');
        $viajero->update($data);

        // $ficha->area = $request->has('area');
        // $ficha->area_pagos = $request->has('area_pagos');
        // $ficha->area_reunion = $request->has('area_reunion');
        $ficha->notas = $request->input('booking_notas');
        // $ficha->notas2 = $request->input('booking_notas2');
        $ficha->promo_cambio_fijo = $request->has('promo_cambio_fijo');
        $ficha->save();

        $ficha->updateDatos();
        $ficha->updateDatosCampamento($request);

        //PDF
        $ficha->pdf();

        // $ficha->notaPago35();

        Session::flash('tab', '#datos');
        return redirect()->route('manage.bookings.ficha', $id);
    }


    public function devolucion(Request $request, $id)
    {
        $booking =  $this->booking->find($id);
        $booking->setStatus(ConfigHelper::config('booking_status_cancelado'));
        Session::flash('mensaje', 'Booking cancelado correctamente. Devolución completa.');
        return back();
    }

    public function asociar(Request $request, $id)
    {
        $booking =  $this->booking->find($id);

        $viajero = $booking->viajero;

        $viajero->booking_id = $booking->id;
        $viajero->booking_status_id = $booking->status_id;
        $viajero->save();

        Session::flash('tab', '#bookings');
        Session::flash('mensaje', 'Booking asociado correctamente.');
        
        return redirect()->back();
    }

    public function cancelar(Request $request, $id)
    {
        $booking =  $this->booking->find($id);

        // if($booking->es_online)
        // {
        //     $booking->online_delete();
        //     return back();
        // }

        if ($booking) {
            $booking->cancelar();
        }

        return redirect()->route('manage.bookings.ficha', $id);

        // if($booking->saldo_pendiente <= 0)
        // {
        //     $booking->setStatus(ConfigHelper::config('booking_status_cancelado'));
        //     Session::flash('mensaje', 'Booking cancelado correctamente.');
        //     return back();
        // }
        // elseif($booking->saldo_pendiente > 0)
        // {
        //     $booking->setStatus(ConfigHelper::config('booking_status_refund'));
        //     Session::flash('mensaje-alert', 'Booking con pagos pendientes. Status Pendiente Refund.');
        //     return back();
        // }
    }

    public function pagado(Request $request, $id, $estado = true)
    {
        $booking =  $this->booking->find($id);

        $booking->pagado($estado);

        $m = "Inscripción marcada como pagada";
        if (!$estado) {
            $m = "Inscripción Pago reabierto";
            Session::flash('mensaje-ok', $m);
        } else {
            Session::flash('mensaje', $m);

            $fecha = Carbon::now()->format('d/m/Y');
            $tipo = "Checklist (Pago Completado) [" . ($estado ? "On" : "Off") . "] [$fecha]";
            BookingLog::addLog($booking, $tipo);
        }


        return back();
    }

    public function destroy(Request $request, $id)
    {
        $booking =  $this->booking->find($id);

        if (!$booking) {
            Session::flash('mensaje-alert', 'Booking no existe!');
            return back();
        }

        $viajero_id = $booking->viajero_id;

        if ($booking->saldo > 0) {
            Session::flash('mensaje-alert', 'Booking con pagos. No se puede eliminar');
            return back();
        }

        if ($booking->pagos->where('enviado', 1)->count() > 0 && $booking->pagos->where('enviado', 0)->count() > 0) {
            Session::flash('mensaje-alert', 'Booking con pagos pendientes de enviar. No se puede eliminar');
            return back();
        } else {
            $booking->delete();
        }

        // if($request->has('v'))
        // {
        //     return redirect()->route('manage.viajeros.ficha',$request->input('v'));
        // }

        return redirect()->route('manage.bookings.index', $viajero_id);
    }

    public function getPdf($id)
    {
        $ficha = $this->booking->find($id);

        return $ficha->pdf(true);

        // $pdf = PDF::loadView('manage.bookings.pdf', compact('ficha'));
        // return $pdf->download("booking_$id.pdf");

        // $pdf = PDF::loadView('admin.cuotas.factura', compact('cuota','info','toPdf'));
        // $adjunto = $pdf->output();
        // file_put_contents('factura.pdf', $adjunto);
    }

    public function ajaxChecklist(Request $request, $id)
    {
        $booking = $this->booking->find($id);
        $checkid = $request->input('check_id');
        // $estado = $request->input('estado');
        $fecha = $request->input('check_fecha');
        $user_id = $request->user()->id;

        $s = $booking->setChecklist($checkid, $fecha, $user_id);

        $user = $s->user->full_name;
        $status_id = $booking->status_id;

        $result = ['id' => $id, 'check_id' => $checkid, 'result' => $s->estado, 'status' => $status_id, 'fecha' => $fecha, 'user' => $user];
        return response()->json($result, 200);
    }

    public function ajaxGetChecklist(Request $request, $id)
    {
        $booking = $this->booking->find($id);

        // $checklist = $booking->status->checklist;
        $checklist = $booking->status->getChecklistBooking($id);
        foreach ($checklist as $check) {
            $estado = $booking->viajero->getStatusChecklistEstado($check->id);
            $check['check'] = $estado;

            $estado = $booking->viajero->getStatusChecklistClass($check->id);
            $check['class'] = $estado;
        }

        $result = ['id' => $id, 'checklist' => $checklist, 'booking' => true, 'titulo' => $booking->name, 'result' => true];
        return response()->json($result, 200);
    }

    public function ajaxStatus(Request $request, $id)
    {
        $booking = $this->booking->find($id);
        $status = $request->input('status');

        if ($booking) {
            $booking->setStatus($status, $request->input('motivo'), $request->input('motivo_nota'));
        }

        $result = ['id' => $id, 'result' => true];
        return response()->json($result, 200);
    }

    public function overbooking($id)
    {
        $ficha = $this->booking->find($id);

        $plazas_vuelo = $ficha->vuelo ? $ficha->vuelo->plazas_disponibles_ovbkg : 1;

        $plazas_alojamiento = 0;
        if($ficha->alojamiento)
        {
            $plazas_alojamiento = $ficha->convocatoria->getPlazas($ficha->alojamiento->id)->plazas_disponibles_ovbkg;
        }

        if ($ficha->status_id == ConfigHelper::config('booking_status_overbooking')) {
            if ($ficha->ovbkg_pa && $plazas_alojamiento <= 0) {
                Session::flash('mensaje-alert', "No hay plazas disponibles de Alojamiento para resolver el Overbooking (P.Vuelo disp.:$plazas_vuelo, P.Alojamiento disp.:$plazas_alojamiento).");
                return redirect()->route('manage.bookings.index');
            }

            if ($ficha->ovbkg_pv && $plazas_vuelo <= 0) {
                Session::flash('mensaje-alert', "No hay plazas disponibles de Vuelo para resolver el Overbooking (P.Vuelo disp.:$plazas_vuelo, P.Alojamiento disp.:$plazas_alojamiento).");
                return redirect()->route('manage.bookings.index');
            }

            // if($plazas_vuelo<1 || $plazas_alojamiento<1)
            // {
            //     Session::flash('mensaje-alert', "No hay plazas disponibles para resolver el Overbooking (P.Vuelo disp.:$plazas_vuelo, P.Alojamiento disp.:$plazas_alojamiento).");
            //     return redirect()->route('manage.bookings.index');
            // }

            Session::flash('mensaje', 'El overbooking se ha resuelto correctamente.');

            $ficha->setStatus(ConfigHelper::config('booking_status_prebooking'));
            BookingLog::addLog($ficha, "Overbooking => Pre-Brooking");
        }

        return redirect()->route('manage.bookings.index');
    }


    public function vueloOk($booking_id)
    {
        $booking = $this->booking->find($booking_id);
        $booking->transporte_ok = true;
        $booking->save();

        BookingLog::addLog($booking, "Vuelo OK");
        ViajeroLog::addLog($booking->viajero, "Booking [$booking_id]: Vuelo OK");

        return redirect()->route('manage.informes.menores12');
    }

    public function extraAdd(Request $request, $booking_id)
    {
        $booking = $this->booking->find($booking_id);

        //BookingExtra tipo 4
        $e = new BookingExtra;
        $e->tipo = 4;
        $e->booking_id = $booking_id;
        $e->extra_id = 0;
        $e->unidades = 1;
        $e->moneda_id = $request->has('moneda_id') ? $request->input('moneda_id') : ConfigHelper::default_moneda_id();
        $e->precio = $request->input('extra_importe');
        $e->name = $request->input('extra_name');
        $e->notas = $request->input('extra_notas');

        // $e->required = $extra->required;
        // $e->tipo_unidad = $extra->tipo_unidad;

        $e->save();

        $booking->mailAvisoCambios();
        $booking->pdf();

        BookingLog::addLog($booking, "Extra [$e->id] [on]");

        Session::flash('tab', '#extras');
        return redirect()->route('manage.bookings.ficha', $booking_id);
    }

    public function extraDestroy(Request $request)
    {
        $id = $request->input('id');

        $e = BookingExtra::find($id);

        $booking_id = $e->booking_id;
        $e->delete();

        $booking = $this->booking->find($booking_id);

        $booking->mailAvisoCambios();
        $booking->pdf();

        BookingLog::addLog($booking, "Extra [$id] [off]");

        $result = ['id' => $id, 'result' => true];
        return response()->json($result, 200);
    }

    public function postVuelo(Request $request, $booking_id)
    {
        $booking = $this->booking->find($booking_id);

        // $data = $request->except('_token');dd($data);

        $booking->transporte_no = $request->has('transporte_no');
        $booking->transporte_otro = $request->has('transporte_otro');

        if ($request->has('transporte_no') || $request->has('transporte_otro')) {
            $booking->vuelo_id = 0;
            $booking->transporte_detalles = $request->input('transporte_detalles');
        } else {
            $booking->vuelo_id = $request->input('vuelo_id');
            $booking->transporte_requisitos = $request->input('transporte_requisitos');
            $booking->transporte_recogida = $request->input('transporte_recogida');
        }

        if (!$request->has('transporte_recogida_bool')) {
            $booking->transporte_recogida = null;
        }

        $booking->save();

        $booking->extraVuelo();

        BookingLog::addLog($booking, "Cambio datos Vuelo");

        MailHelper::mailBookingVuelo($booking_id);

        Session::flash('tab', '#vuelo');
        return redirect()->route('manage.bookings.ficha', $booking_id);
    }

    public function postFechas(Request $request, $booking_id, $tipo)
    {
        $booking = $this->booking->find($booking_id);

        if ($tipo == 'alojamiento') {
            $booking->accommodation_start_date = Carbon::createFromFormat('d/m/Y', $request->input('desde'))->format('Y-m-d');
            $booking->accommodation_end_date = Carbon::createFromFormat('d/m/Y', $request->input('hasta'))->format('Y-m-d');
            $booking->save();

            BookingLog::addLog($booking, "Cambio Fechas Alojamiento");
        }

        if ($tipo == 'curso') {
            $booking->course_start_date = Carbon::createFromFormat('d/m/Y', $request->input('desde'))->format('Y-m-d');
            $booking->course_end_date = Carbon::createFromFormat('d/m/Y', $request->input('hasta'))->format('Y-m-d');
            $booking->save();

            BookingLog::addLog($booking, "Cambio Fechas Curso");
        }

        $booking->pdf();

        return redirect()->route('manage.bookings.ficha', $booking_id);
    }

    /**
     * @param $booking_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getMonedas($booking_id)
    {
        Session::flash('tab', '#monedas');

        $booking = $this->booking->find($booking_id);
        if ($booking->pagado) {
            Session::flash('mensaje-alert', 'Booking pagado. Imposible modificar tipo de cambio.');
            return back();
        }

        if (!$booking->setMonedasCambio(true)) {
            Session::flash('mensaje-alert', 'Error TC');
            return back();
        }

        return redirect()->route('manage.bookings.ficha', $booking_id);
    }

    // public function getMonedasIndex($booking_id)
    // {
    //     if(Datatable::shouldHandle())
    //     {
    //         $col = BookingMoneda::where('booking_id',$booking_id)->get();

    //         return Datatable::collection( $col )
    //             ->addColumn('name', function($model) {
    //                 return "<a href='". route('manage.bookings.monedas.ficha',[$model->id]) ."'>". $model->moneda->name ."</a>";
    //             })
    //             ->addColumn('rate', function($model) {
    //                 return $model->rate . "miniform!!";
    //             })
    //             ->searchColumns('name')
    //             ->orderColumns('name')
    //             ->setAliasMapping()
    //             ->make();
    //     }
    // }

    public function postMonedas(Request $request, $booking_id)
    {
        $booking = $this->booking->find($booking_id);

        $m = (int) $request->input('moneda_id');

        $bm = $booking->monedas->find($m);
        if ($bm) {
            $r1 = $bm->rate;
            $r2 = $request->input('rate');
            $chg = "$r1 => $r2";
            $txt = "TC Moneda (" . $bm->moneda->name . ")";

            $bm->rate = $r2;
            $bm->save();

            BookingLog::addLog($booking, $txt, $chg);
        }

        if ($booking->es_notapago35_generada) {
            $booking->notaPago35update("(monedas)");
            Session::flash('mensaje-ok', 'Nota de pago actualizada.');
        }

        Session::flash('tab', '#monedas');
        return redirect()->route('manage.bookings.ficha', $booking_id);
    }

    public function getNotaPago35(Request $request, $booking_id)
    {
        $booking = $this->booking->find($booking_id);

        $booking->notaPago35(true);

        Session::flash('tab', '#pagos');

        return redirect()->back();
    }

    public function getNotaPago35Mail(Request $request, $booking_id)
    {
        $booking = $this->booking->find($booking_id);

        $booking->notaPago35_mail();

        Session::flash('tab', '#pagos');

        return redirect()->back();
    }

    public function postFamilia(Request $request, $booking_id, $asignacion_id = 0)
    {
        Session::flash('tab', '#familia');

        $this->validate($request, [
            'desde' => 'required',
            'hasta' => 'required',
        ]);

        if (!$asignacion_id) {
            $this->validate($request, [
                'familia_id' => 'required|exists:familias,id',
            ]);
        }

        $booking = $this->booking->find($booking_id);

        $data = $request->except('_token');
        $desde = Carbon::createFromFormat('!d/m/Y', $data['desde']);
        $hasta = Carbon::createFromFormat('!d/m/Y', $data['hasta']);

        // solapar 1 dia?
        if (!$booking->familiaFechasCheck($desde, $hasta, $asignacion_id)) {
            Session::flash('mensaje-alert', "Atención. Revise las fechas de asignación de la familia, no se pueden solapar más de 1 día.");
            return redirect()->back();
        }

        if (!$booking->familiaFechasAlojamientoCheck($desde, $hasta)) {
            Session::flash('mensaje-alert', "Atención. Revise las fechas de asignación de la familia, sobrepasan las del Alojamiento.");
            return redirect()->back();
        }

        $data['desde'] = $desde;
        $data['hasta'] = $hasta;
        $data['booking_id'] = $booking_id;

        if ($asignacion_id) {
            $bf = BookingFamilia::find($asignacion_id);

            $data['familia_id'] = $bf->familia_id;

            $bf->update($data);

            BookingLog::addLog($booking, "Asignación Familia modificado [$bf->familia_id]");
        } else {
            $data['area'] = $request->has('area');

            $bf = BookingFamilia::create($data);

            BookingLog::addLog($booking, "Asignación Familia creado [$bf->familia_id]");

            if ($request->has('area')) {
                $bf->booking->mailFamilia();
                BookingLog::addLog($bf->booking, "Asignación Familia email [$bf->familia_id]");
            }
        }

        // if($bf->area)
        // {
        //     $bf->booking->mailFamilia();
        // }

        return redirect()->back();
    }


    public function deleteFamilia(Request $request, $asignacion_id)
    {
        $bf = BookingFamilia::find($asignacion_id);

        BookingLog::addLog($bf->booking, "Asignación Familia eliminada");

        $bf->delete();

        Session::flash('tab', '#familia');
        return redirect()->back();
    }

    public function setFamiliaActivo(Request $request, $asignacion_id)
    {
        $bf = BookingFamilia::find($asignacion_id);
        $bf->area = !$bf->area;
        $bf->save();

        Session::flash('tab', '#familia');
        return redirect()->back();
    }

    public function mailFamilia(Request $request, $asignacion_id)
    {
        $bf = BookingFamilia::find($asignacion_id);

        if (!$bf->area) {
            return redirect()->back();
        }

        $bf->booking->mailFamilia();

        BookingLog::addLog($bf->booking, "Asignación Familia email [$bf->familia_id]");

        Session::flash('tab', '#familia');
        return redirect()->back();
    }

    public function postSchool(Request $request, $booking_id, $asignacion_id = 0)
    {
        Session::flash('tab', '#school');

        $this->validate($request, [
            'desde' => 'required',
            'hasta' => 'required',
        ]);

        if (!$asignacion_id) {
            $this->validate($request, [
                'school_id' => 'required|exists:centro_schools,id',
            ]);
        }

        $booking = $this->booking->find($booking_id);

        $data = $request->except('_token');
        $desde = Carbon::createFromFormat('!d/m/Y', $data['desde']);
        $hasta = Carbon::createFromFormat('!d/m/Y', $data['hasta']);

        // solapar 1 dia?
        if (!$booking->schoolFechasCheck($desde, $hasta, $asignacion_id)) {
            Session::flash('mensaje-alert', "Atención. Revise las fechas de asignación de la School, no se pueden solapar más de 1 día.");
            return redirect()->back();
        }

        if (!$booking->schoolFechasAlojamientoCheck($desde, $hasta)) {
            Session::flash('mensaje-alert', "Atención. Revise las fechas de asignación de la School, sobrepasan las del Alojamiento.");
            return redirect()->back();
        }

        $data['desde'] = $desde;
        $data['hasta'] = $hasta;
        $data['booking_id'] = $booking_id;

        if ($asignacion_id) {
            $bf = BookingSchool::find($asignacion_id);

            $data['school_id'] = $bf->school_id;

            $bf->update($data);

            BookingLog::addLog($booking, "Asignación School modificado [$bf->school_id]");
        } else {
            $data['area'] = $request->has('area');

            $bf = BookingSchool::create($data);

            BookingLog::addLog($booking, "Asignación School creado [$bf->school_id]");

            if ($request->has('area')) {
                $bf->booking->mailSchool();
                BookingLog::addLog($bf->booking, "Asignación School email [$bf->school_id]");
            }
        }

        // if($bf->area)
        // {
        //     $bf->booking->mailSchool();
        // }

        return redirect()->back();
    }


    public function deleteSchool(Request $request, $asignacion_id)
    {
        $bf = BookingSchool::find($asignacion_id);

        BookingLog::addLog($bf->booking, "Asignación School eliminada");

        $bf->delete();

        Session::flash('tab', '#school');
        return redirect()->back();
    }

    public function setSchoolActivo(Request $request, $asignacion_id)
    {
        $bf = BookingSchool::find($asignacion_id);
        $bf->area = !$bf->area;
        $bf->save();

        Session::flash('tab', '#school');
        return redirect()->back();
    }

    public function mailSchool(Request $request, $asignacion_id)
    {
        $bf = BookingSchool::find($asignacion_id);

        if (!$bf->area) {
            return redirect()->back();
        }

        $bf->booking->mailSchool();

        BookingLog::addLog($bf->booking, "Asignación School email [$bf->school_id]");

        Session::flash('tab', '#school');
        return redirect()->back();
    }


    public function facturas(Request $request, $id)
    {
        $booking = $this->booking->find($id);

        $facturas = $request->input('facturas');

        $booking->facturaManual($facturas);

        Session::flash('tab', '#facturas');
        return redirect()->route('manage.bookings.ficha', $id);
    }

    public function facturarAbono(Request $request, $factura_id)
    {
        Session::flash('tab', '#facturas');

        $factura = Bookingfactura::findOrFail($factura_id);

        $factura->booking->facturarAbono($factura_id);

        if ($factura->grup) {
            return redirect()->route('manage.convocatorias.cerradas.ficha', $factura->booking->convocatoria->id);
        }

        return redirect()->route('manage.bookings.ficha', $factura->booking_id);
    }

    public function facturar(Request $request, $model, $model_id, $abono = false)
    {
        switch ($model) {
            case 'Booking': {
                    // $booking = $this->booking->find($model_id);
                    // return $booking->facturar();

                    Session::flash('tab', '#facturas');

                    $booking = $this->booking->find($model_id);
                    $booking->prescriptor_no_facturar;

                    if (!$booking->es_facturable && !$abono) {
                        Session::flash('mensaje-alert', 'No se puede emitir factura. No queda importe pendiente de facturar.');
                    } else {
                        // if($abono)
                        // {
                        //     $booking->facturarAbono();
                        // }

                        if ($booking->facturar()) {
                            Session::flash('mensaje', 'Factura emitida.');
                        }
                    }
                    return redirect()->route('manage.bookings.ficha', $model_id);
                }
                break;

            case 'Cerrada': {
                    $plat = ConfigHelper::config('propietario');

                    $stplazas = \VCN\Models\Bookings\Status::where('plazas', 1)->pluck('id');
                    $bookings = BookingModel::where('plataforma', $plat)->whereIn('status_id', $stplazas)->where('convocatory_close_id', $model_id)->get();

                    $iTot = 0;
                    foreach ($bookings as $booking) {
                        $booking->prescriptor_no_facturar;
                        if ($booking->es_facturable && !$booking->prescriptor_no_facturar) {
                            $booking->facturar();
                            $iTot++;
                        }
                    }

                    if ($iTot) {
                        Session::flash('mensaje-ok', "Facturas emitidas: $iTot");
                    } else {
                        Session::flash('mensaje-alert', 'No quedan facturas por emitir para esta Convocatoria.');
                    }

                    return redirect()->back();
                }
                break;

            case 'ConvocatoriaMulti': {
                    $plat = ConfigHelper::config('propietario');

                    $stplazas = \VCN\Models\Bookings\Status::where('plazas', 1)->pluck('id');
                    $bookings = BookingModel::where('plataforma', $plat)->whereIn('status_id', $stplazas)->where('convocatory_multi_id', $model_id)->get();

                    $iTot = 0;
                    foreach ($bookings as $booking) {
                        $booking->prescriptor_no_facturar;
                        if (!$booking->factura && !$booking->prescriptor_no_facturar) {
                            $booking->facturar();
                            $iTot++;
                        }
                    }

                    if ($iTot) {
                        Session::flash('mensaje-ok', "Facturas emitidas: $iTot");
                    } else {
                        Session::flash('mensaje-alert', 'No quedan facturas por emitir para esta Convocatoria.');
                    }

                    return redirect()->back();
                }
                break;
        }
    }

    public function facturarParcial(Request $request, $booking_id, $abono = false)
    {
        Session::flash('tab', '#facturas');

        $this->validate($request, [
            // 'valor' => 'required',
        ]);

        $booking = $this->booking->find($booking_id);
        // if($booking->factura && !$abono)
        // {
        //     Session::flash('mensaje-alert', 'No se puede emitir factura. Ya existe para este booking.');
        // }
        // else
        {
            $tipo = $request->input('tipo');
            $v = (float) $request->input('valor');

            if (!$v) {
                $v = $booking->facturable;
            }

            if ($v > $booking->facturable) {
                Session::flash('mensaje-alert', "No se puede facturar más de lo que queda pendiente de facturar.");
                return redirect()->route('manage.bookings.ficha', $booking_id);
            }

            $total = $v;
            if ($tipo == 1) //Porcentaje
            {
                $total = $booking->precio_total['total'];
                $total = ($v * $total) / 100;
            }

            $booking->facturar($total, $tipo ? $v : 0);

            Session::flash('mensaje', 'Factura emtida.');
        }

        return redirect()->route('manage.bookings.ficha', $booking_id);
    }

    public function setFacturarTipo(Request $request, $booking_id, $opcion)
    {
        Session::flash('tab', '#facturas');

        $booking = $this->booking->find($booking_id);
        $booking->no_facturar = $opcion;
        $booking->save();

        return redirect()->route('manage.bookings.ficha', $booking_id);
    }

    public function postFacturaDatos(Request $request, $booking_id)
    {
        Session::flash('tab', '#facturas');

        $booking = $this->booking->find($booking_id);

        $booking->datos->fact_nif = $request->get('fact_nif');
        $booking->datos->fact_razonsocial = $request->get('fact_razonsocial');
        $booking->datos->fact_domicilio = $request->get('fact_domicilio');
        $booking->datos->fact_poblacion = $request->get('fact_poblacion');
        $booking->datos->fact_cp = $request->get('fact_cp');
        $booking->datos->fact_concepto = $request->get('fact_concepto');
        $booking->datos->save();

        // if($booking->factura)
        // {
        //     $direccion = ($booking->datos->fact_domicilio?$booking->datos->fact_domicilio:$booking->datos->direccion) .", ". ($booking->datos->fact_cp?$booking->datos->fact_cp:$booking->datos->cp) ." ". ($booking->datos->fact_poblacion?$booking->datos->fact_poblacion:$booking->datos->ciudad);
        //     $datos = [
        //             'razonsocial'   => $request->get('fact_razonsocial'),
        //             'direccion'     => $direccion,
        //             'cif'           => $request->get('fact_nif')?:$booking->datos->fact_concepto,
        //             'concepto1'     => $booking->datos->fact_concepto?$booking->datos->fact_concepto:$booking->curso->name,
        //             'concepto2'     => $booking->datos->fact_concepto?"":ConfigHelper::parseMoneda($booking->course_total_amount, $booking->curso_moneda),
        //         ];

        //     $booking->factura->datos = $datos;
        //     $booking->factura->save();
        // }

        // $booking->facturaPDFupdate();

        return redirect()->route('manage.bookings.ficha', $booking_id);
    }

    public function setOnline(Request $request, $booking_id)
    {
        $destinatarios = $request->get('destinatarios');
        if (!$destinatarios) {
            Session::flash('mensaje-alert', "No ha seleccionado ningún destinatario.");
            return redirect()->back();
        }

        $autorizado = 0;
        if ($request->get('es_overbooking', 0)) {
            $autorizado = (int) $request->get('autorizado');
            if (!$autorizado) {
                Session::flash('mensaje-alert', "No ha seleccionado quién le ha autorizado.");
                return redirect()->back();
            }
        }

        $booking = $this->booking->find($booking_id);

        $plazas = $booking->estado_plazas;
        $hayPlazas = (bool) $plazas['hayPlazas'];
        $plazas_alojamiento = $plazas['alojamiento'];
        $plazas_vuelo = $plazas['vuelo'];

        // if(!$hayPlazas)
        // {
        //     $msj = "Ovbkg: No hay plazas disponibles. ";
        //     if($plazas_alojamiento < 1)
        //     {
        //         $msj .= " [Alojamiento]";
        //     }

        //     if($plazas_vuelo < 1)
        //     {
        //         $msj .= " [Vuelo]";
        //     }

        //     $msj .= " No se puede pasar a Online.";

        //     Session::flash('mensaje-alert', $msj);
        //     return redirect()->back();
        // }

        $online = $booking->online;
        $online['es_backend'] = 1;
        $online['backend_user_id'] = $booking->user_id;
        $online['backend_fase'] = $booking->fase;
        $booking->online = $online;

        $booking->es_online = 2;
        $booking->area = true;
        $booking->area_pagos = true;
        $booking->area_reunion = true;
        $booking->tpv_plazas = Carbon::now();
        if ($autorizado) {
            $booking->ovbkg_id = $autorizado;
            $booking->ovbkg_pv = $plazas_vuelo <= 0 ? true : false;
            $booking->ovbkg_pa = $plazas_alojamiento <= 0 ? true : false;
        }
        $booking->save();

        $viajero = $booking->viajero;
        if ($viajero)
        {
            // $viajero->setUsuarios();
            $viajero->setUsuariosForce();
        }

        $compra = array('curso_id' => $booking->curso_id, 'booking_id' => $booking->id);

        $i = 0;

        foreach ($destinatarios as $dest) {
            if (is_numeric($dest) && $dest <= 0) {
                continue;
            }

            if (is_numeric($dest)) {
                $user = User::find($dest);
                $user->compra = $compra;
                $user->save();

                MailHelper::mailBookingOnline($booking, $user);
                $i++;
                BookingLog::addLog($booking, "Enviar borrador (online) a " . $user->full_name);
            } elseif ($dest == "v") {
                if ($viajero->user) {
                    $user = $viajero->user;
                    $user->compra = $compra;
                    $user->save();

                    MailHelper::mailBookingOnline($booking, $user);
                    $i++;
                    BookingLog::addLog($booking, "Enviar borrador (online) a Viajero");
                }
            } elseif ($dest == "t1") {
                if ($viajero->tutor1 && $viajero->tutor1->user) {
                    $user = $viajero->tutor1->user;
                    $user->compra = $compra;
                    $user->save();

                    MailHelper::mailBookingOnline($booking, $user);
                    $i++;
                    BookingLog::addLog($booking, "Enviar borrador (online) a Tutor1 [$user->full_name]");
                }
            } elseif ($dest == "t2") {
                if ($viajero->tutor2 && $viajero->tutor2->user) {
                    $user = $viajero->tutor2->user;
                    $user->compra = $compra;
                    $user->save();

                    MailHelper::mailBookingOnline($booking, $user);
                    $i++;
                    BookingLog::addLog($booking, "Enviar borrador (online) a Tutor2 [$user->full_name]");
                }
            }
        }

        if ($autorizado) {
            MailHelper::mailOvbkg($booking->id);
            BookingLog::addLog($booking, "Enviar Overbooking autorizado");
        }

        BookingLog::addLog($booking, "Enviar borrador [fase: $booking->fase] (online) [Envíos: $i]");

        return redirect()->back();
    }


    public function getFirmaSolicitar($booking_id, $doc)
    {
        Session::flash('tab', '#pdf');

        $ficha = $this->booking->find($booking_id);

        $pdf = $ficha->getArchivoFirma($doc);
        $firmado = $pdf ? $pdf->es_firmado : false;

        if ($firmado) {
            return redirect()->back();
        }

        if (!$pdf) {
            $pdf = ViajeroArchivo::addBooking($ficha, $doc, true);
        }

        $ficha->setFirmaPendiente($pdf);

        MailHelper::mailBookingFirma($ficha);
        BookingLog::addLog($ficha, "Solicitud firma: $doc");

        Session::flash('mensaje-ok', "Solicitud de firma [$doc] enviada correctamente.");

        return redirect()->back();
    }

    public function ajaxFiltros(Request $request)
    {
        $res = ConfigHelper::arrayFromRequest($request);

        return response()->json($res, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getIndex(Request $request)
    {
        $valores = ConfigHelper::paramsFromRequest($request);
        $array = ConfigHelper::arrayFromRequest($request);
        if ($array['anys'] == 0)
        {
            $array['anys'] = Carbon::now()->year;
            $user_id = (int) $request->user()->id;
            $oficina_id = null;

            $user = auth()->user();
            if (!$user->filtro_oficinas) {
                $oficina_id = $user->oficina_id;
            }

            if ($oficina_id) {
                $array['oficinas'][] = $oficina_id;
            }
            $array['usuarios'][] = $user_id;

            $valores['anys'] = $array['anys'];
            $valores['oficinas'] = [];
            $valores['oficinas'][] = "u-" . $user_id;
            //$valores['oficinas'][] = "o-1";
            //$valores['oficinas'][] = $oficina_id ? "o-$oficina_id" : null;
        }

        //dd($array);

        $query = ConfigHelper::queryFromArray($array, "bookings", 'course_start_date');
        //dd($query);

        $statuses = Status::orderBy('orden')->get();

        $statuses_total = [];
        $statuses_total[0] = 0;

        $res = null;

        foreach ($statuses as $status)
        {
            $query2 = clone $query;
            //$res['st_'. $st->id] = $query2->where('status_id', $status->id)->pluck('id')->toArray();

            if ($status->id == ConfigHelper::config('booking_status_borrador')) {
                $iStatus = $status->id;
                $iSt = $query2->where(function ($q) use ($iStatus) {
                    return $q->where('bookings.status_id', $iStatus)->orWhere('bookings.status_id', 0);
                })->count();
            } else {
                $iSt = $query2->where('bookings.status_id', $status->id)->count();
            }

            $statuses_total[$status->id] = $iSt;

            if ($status->orden > 0) {
                $statuses_total[0] += $statuses_total[$status->id];
            }
        }

        //dd($statuses_total);
        //$valores = json_encode($valores);

        return view('manage.bookings.index_filtros', compact('res', 'valores', 'statuses', 'statuses_total'));
    }

    /**
     * @param Request $request
     */
    public function dttFiltros(Request $request)
    {
        $array = ConfigHelper::arrayFromRequest($request, false);

        $query = ConfigHelper::queryFromArray($array, 'bookings', 'course_start_date');

        $query = $query->select(
            'bookings.id',
            'course_start_date',
            DB::raw("CONCAT(viajeros.name,' ',viajeros.lastname,' ',viajeros.lastname2) as viajero")
        )
            ->join('viajeros', 'bookings.viajero_id', '=', 'viajeros.id');

        $status_id = $request->get('status_id') ?: 0;
        if (!$status_id) {
            $st = Status::where('orden', '>', 0)->pluck('id');
            $query = $query->whereIn('bookings.status_id', $st);
        } else {
            if ($status_id == ConfigHelper::config('booking_status_borrador')) {
                $query = $query->where(function ($q) use ($status_id) {
                    return $q->where('bookings.status_id', $status_id)->orWhere('bookings.status_id', 0);
                });
            } else {
                $query = $query->where('bookings.status_id', $status_id);
            }
        }

        $plat = ConfigHelper::config('propietario');
        if ($plat) {
            $query = $query->where('bookings.plataforma', $plat);
        }

        if (Datatable::shouldHandle()) {
            return Datatable::query($query)
                ->addColumn('fecha_reserva', function ($column) {
                    $model = BookingModel::find($column->id);
                    return $model->fecha_reserva ? $model->fecha_reserva->format('Y-m-d') : '-';
                })
                ->addColumn('name', function ($column) {
                    $model = BookingModel::find($column->id);
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('curso', function ($column) {
                    $model = BookingModel::find($column->id);
                    $curso = "<a data-label='Ficha Curso' href='" . route('manage.cursos.ficha', $model->curso->id) . "'>" . $model->curso->name . "</a>";
                    $convocatoria = $model->convocatoria ? "<a data-label='Ficha Convocatoria' href='" . route('manage.convocatorias.cerradas.ficha', $model->convocatoria->id) . "'>" . $model->convocatoria->name . "</a>" : "-";

                    if ($model->curso->es_convocatoria_multi) {
                        return $convocatoria;
                    }

                    return $model->curso->es_convocatoria_cerrada ? $convocatoria : $curso;
                })
                ->showColumns('course_start_date')
                ->addColumn('duracion', function ($column) {
                    $model = BookingModel::find($column->id);
                    return $model->convocatoria ? $model->convocatoria->duracion_name : "-";
                })
                ->addColumn('status', function ($column) {
                    $model = BookingModel::find($column->id);
                    $ret = "<span class='badge'>" . $model->status_name . "</span>&nbsp;";

                    if ($model->es_directo) {
                        $ret .= "[D]";
                    }

                    if (!$model->status)
                        return $ret;

                    if ($model->status_id == ConfigHelper::config('solicitud_status_archivado')) {
                        $status_txt = $model->status->name . " (" . $model->archivar_motivo . ")";
                        if ($model->archivar_motivo == "otro" or $model->archivar_motivo == "AUTO") {
                            $status_txt = $model->status->name . " (" . $model->archivar_motivo . ": $model->archivar_motivo_nota)";
                        }

                        $ret = "<span class='badge'>" . ($model->status ? ($status_txt) : "-") . "</span>&nbsp;";
                    }

                    if ($model->status_id == ConfigHelper::config('booking_status_overbooking')) {
                        $ret .= " <i>Aut.:[" . ($model->autorizado ? $model->autorizado->full_name : "-") . "]</i>";
                    }

                    if ($model->incidencias->count()) {
                        $icono = "<i class='badge badge-warning badge-incidencias'>" . $model->incidencias->count() . "</i>";
                        if ($model->tiene_incidencias_pendientes) {
                            $icono = "<i class='fa fa-exclamation-triangle' style='color:red;'></i>";
                        }

                        $ret .= "<a href='" . route('manage.bookings.ficha', $model->id) . "#incidencias'>$icono</a>";
                    }

                    return $ret;
                })
                ->addColumn('asignado', function ($column) {
                    $model = BookingModel::find($column->id);
                    return $model->asignado ? $model->asignado->fname . " " . $model->asignado->lname : "-";
                })
                ->addColumn('online', function ($column) {
                    $model = BookingModel::find($column->id);

                    $ret = "";
                    if ($model->es_online) {
                        $online = $model->online;
                        $ip = isset($online['ip']) ? "(" . $online['ip'] . ")" : "";
                        $fecha = $model->tpv_plazas->format('d/m/Y');

                        $ret = "[ONLINE] $fecha $ip";
                    }

                    return $ret;
                })
                ->addColumn('options', function ($column) {
                    $model = BookingModel::find($column->id);

                    $ret = "";

                    // Cancelar:
                    $pregunta = "Vas a cancelar una inscripción y liberar una plaza. Esta inscripción se seguirá mostrando en el área de cliente. Para que no aparezca, tienes que quitar el check 'Área cliente' en la ficha booking.";
                    if($model->tiene_facturas)
                    {
                        $pregunta .= " [TIENE FACTURAS]";
                    }
                    $data = " data-label='Cancelar' data-pregunta='$pregunta' data-action='" . route('manage.bookings.cancelar', $model->id) . "'";
                    $ret .= " <a $data href='#confirma' class='btn btn-danger btn-xs'><i class='fa fa-ban'></i></a>";

                    $pregunta = "<br><br>Vas a borrar todo el historial de esta reserva que desaparecerá totalmente. Se recomienda utilizar este botón SÓLO en caso de haber entrado erróneamente una inscripción. Si es una cancelación, utilizar el botón rojo de Cancelar. ¿Seguir y eliminar la inscripción?";

                    if (!$model->es_empezado && ConfigHelper::canEdit('booking-borrar')) {
                        $data = " data-label='Borrar' data-model='Inscripción. $pregunta' data-action='" . route('manage.bookings.delete', $model->id) . "'";
                        $ret .= " <a href='#destroy' $data data-alerta='' data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";
                    }

                    $ret .= " <a data-label='Inscripción' href='" . route('manage.bookings.ficha', [$model->id]) . "' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i></a>";

                    if ($model->es_terminado && $model->pdf_link) {
                        $ret .= " <a data-label='Descargar PDF' href='" . $model->pdf_link . "' target='_blank' class='btn btn-danger btn-xs'><i class='fa fa-file-pdf-o'></i></a>";
                    }

                    if ($model->status_id == ConfigHelper::config('booking_status_overbooking')) {
                        if ($model->curso->es_convocatoria_cerrada) {
                            $plazasv = $model->vuelo ? $model->vuelo->plazas_disponibles_ovbkg : 1;
                            $plazasa = 0;

                            if($model->alojamiento)
                            {   
                                $pa = $model->convocatoria ? $model->convocatoria->getPlazas($model->alojamiento->id) : null;
                                if ($pa) {
                                    $plazasa = $pa->plazas_disponibles_ovbkg;
                                }
                            }

                            $plazas = "P.Vuelo: $plazasv , P.Aloj.: $plazasa";
                            $ret .= " <a data-label='$plazas' href='" . route('manage.bookings.overbooking', [$model->id]) . "' class='btn btn-warning btn-xs'><i class='fa fa-fighter-jet'></i> OVBKG</a>";
                        } elseif ($model->curso->es_convocatoria_multi) {
                        }
                    }

                    // $auto = $model->categoria?$this->model->categoria->notapago_auto:false;
                    if (ConfigHelper::canEdit('notapago')) {
                        if ($model->status_id == ConfigHelper::config('booking_status_prebooking') || $model->status_id == ConfigHelper::config('booking_status_presalida')) {
                            if ($model->area_pagos && $model->saldo_pendiente) {
                                if (!$model->es_notapago35_generada) {
                                    // $ret .= " <a data-label='Nota Pago' href='". route('manage.bookings.notapago35', [$model->id]) ."' class='btn btn-warning btn-xs'><i class='fa fa-money'></i></a>";
                                } else {
                                    $btncolor = 'btn-warning';
                                    if ($model->es_notapago35_enviada) {
                                        $btncolor = 'btn-success';
                                    }

                                    // $ret .= " <a data-label='Enviar Nota Pago' href='". route('manage.bookings.notapago35.mail', [$model->id]) ."' class='btn $btncolor btn-xs'><i class='fa fa-money'></i> <i class='fa fa-envelope'></i></a>";
                                }
                            }
                        }
                    }

                    if ($model->curso->categoria) {
                        if (!$model->curso->categoria->no_facturar && !$model->prescriptor_no_facturar && ConfigHelper::config('facturas')) {
                            if ($model->curso->subcategoria) {
                                if (!$model->curso->subcategoria->no_facturar) {
                                    $ret .= " <a data-label='Emitir factura' href='" . route('manage.bookings.facturar', ['Booking', $model->id]) . "' class='btn btn-success btn-xs'><i class='fa fa-ticket'></i></a>";
                                }
                            } else {
                                $ret .= " <a data-label='Emitir factura' href='" . route('manage.bookings.facturar', ['Booking', $model->id]) . "' class='btn btn-success btn-xs'><i class='fa fa-ticket'></i></a>";
                            }
                        }
                    }

                    return $ret;
                })
                ->searchColumns('name', 'lastname', 'lastname2')
                ->orderColumns('fecha_reserva', 'course_start_date', 'name', 'online')
                // ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }
    }

    public function mailExamenes(BookingModel $booking)
    {
        $iSend = 0;
        foreach ($booking->area_examenes as $exam) {
            if ($exam->getRespuestaBooking($booking->id)) {
                continue;
            }

            $examTxt = $exam->name;

            //Viajero
            $viajero = $booking->viajero;
            $user = $viajero->user;

            if ($user) {
                $iSend += MailHelper::mailBookingExamen($exam, $booking, $user);
                BookingLog::addLog($booking, "Enviar Test ($examTxt) a Viajero");
            }

            //Tutores
            foreach ($viajero->tutores as $t) {
                $user = $t->user;
                if ($user) {
                    $iSend += MailHelper::mailBookingExamen($exam, $booking, $user);
                    BookingLog::addLog($booking, "Enviar Test ($examTxt) a Tutor");
                }
            }
        }

        if (!$iSend) {
            Session::flash('mensaje', "No hay Tests");
            return redirect()->back();
        }

        Session::flash('mensaje-ok', "Tests enviados");
        return redirect()->back();
    }
}
