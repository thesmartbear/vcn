<?php

namespace VCN\Http\Controllers\Manage\Bookings;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Bookings\BookingDevolucionRepository as BookingDevolucion;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Monedas\Moneda;
use VCN\Models\User;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Auth;
use Carbon;
use Session;

class BookingDevolucionesController extends Controller
{
    private $devolucion;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( BookingDevolucion $devolucion )
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');

        $this->devolucion = $devolucion;
    }

    public function getIndex($booking_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->devolucion->all();
            if($booking_id>0)
            {
                $col = Booking::find($booking_id)->devoluciones;
            }

            return Datatable::collection( $col )
                ->showColumns('importe','notas')
                ->addColumn('fecha', function($model) {
                    return "<a href='". route('manage.bookings.devoluciones.ficha',[$model->id]) ."'>$model->fecha_dmy</a>";
                })
                ->addColumn('usuario', function($model) {
                    return $model->user_id?User::find($model->user_id)->full_name:"-";
                })
                ->addColumn('moneda', function($model) {
                    return $model->currency_id?$model->moneda->name:"-";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Devolución' data-action='". route( 'manage.bookings.devoluciones.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= "<a href='$model->id' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Centro</a>";

                    return $ret;
                })
                ->searchColumns('fecha')
                ->orderColumns('*')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.bookings.devoluciones.index', compact('booking_id'));
    }

    public function getNuevo($booking_id=0)
    {
        $booking = Booking::find($booking_id);

        $monedas = Moneda::pluck('currency_name','id');

        return view('manage.bookings.devoluciones.new', compact('booking', 'monedas'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->devolucion->find($id);

        $monedas = Moneda::pluck('currency_name','id');

        return view('manage.bookings.devoluciones.ficha', compact('ficha','monedas'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'currency_id' => 'required',
            'importe' => 'required',
        ]);

        $data = $request->except('_token');
        $data['fecha'] = Carbon::createFromFormat('d/m/Y',$data['fecha'])->format('Y-m-d');
        $data['user_id'] = Auth::user()->id;

        if(!$id)
        {
            //nuevo
            $o = $this->devolucion->create($data);
            $booking_id = $o->booking_id;
            $id = $o->id;
        }
        else
        {
            $this->devolucion->update($data, $id);

            $booking_id = $this->devolucion->find($id)->booking_id;
        }

        $ficha = $this->devolucion->find($id);
        $booking = $ficha->booking;

        $booking->mailAvisoCambios();
        $booking->pdf();

        Session::flash('tab','#devoluciones');

        return redirect()->route('manage.bookings.ficha',$booking_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $dto = $this->devolucion->find($id);

        if(!$dto)
        {
            return redirect()->back();
        }

        $booking = $dto->booking;
        BookingLog::addLog($booking, "Elimina Devolución $dto->notas [$id]");

        $this->devolucion->delete($id);

        $booking->mailAvisoCambios();
        $booking->pdf();

        Session::flash('tab','#devoluciones');

        return redirect()->route('manage.bookings.ficha',$booking->id);
    }
}
