<?php

namespace VCN\Http\Controllers\Manage\Bookings;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Bookings\BookingTareaRepository as BookingTarea;
use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingIncidencia;
use VCN\Models\Bookings\BookingIncidenciaLog;
use VCN\Models\User;

use Datatable;
use Session;
use Carbon;
use ConfigHelper;
use VCN\Helpers\MailHelper;

class BookingTareasController extends Controller
{
    private $tarea;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( BookingTarea $tarea )
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');

        $this->tarea = $tarea;
    }

    public function getIndex($incidencia_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->tarea->all();
            if($incidencia_id)
            {
                $col = $this->tarea->findAllBy('incidencia_id',$incidencia_id)->sortBy('fecha');
            }

            return Datatable::collection( $col )
                ->addColumn('fecha', function($model) {
                    $ret = "";
                    $data = " data-get='". route( 'manage.bookings.incidencias.tareas.ajax.ficha', $model->id) . "' data-action='". route( 'manage.bookings.incidencias.tareas.ficha', $model->id) . "'";
                    $ret .= " <a href='#tarea-edit' $data>". Carbon::parse($model->fecha)->format('d/m/Y - H:i') ."</a>";

                    // $ret .= "<a href='". route('manage.viajeros.tareas.ficha',[$model->id]) ."' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Editar</a>";

                    return $ret;
                })
                ->showColumns('tipo','notas')
                ->addColumn('usuario', function($model) {
                    return $model->user?$model->user->full_name:"-";
                })
                ->addColumn('asignado', function($model) {
                    return $model->asignado?$model->asignado->full_name:"-";
                })
                ->addColumn('estado', function($model) {

                    $ret = "";

                    if(!$model->estado)
                    {
                        $ret = "<i class='fa fa-clock-o'></i>";

                        $ret .= "<span class='pull-right'>";
                        $ret .= " <a data-label='Completar' href='". route('manage.bookings.incidencias.tareas.completar',$model->id) ."' class='btn btn-success btn-xs'><i class='fa fa-check'></i></a>";
                        $ret .= "</span>";
                    }
                    else
                    {
                        $ret = "<i class='fa fa-check'></i>";

                        $ret .= "<span class='pull-right'>";
                        $ret .= " <a data-label='Marcar como incompleta' href='". route('manage.bookings.incidencias.tareas.completar',[$model->id,0]) ."' class='btn btn-danger btn-xs'><i class='fa fa-clock-o'></i></a>";
                        $ret .= "</span>";
                    }

                    return $ret;
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-get='". route( 'manage.bookings.incidencias.tareas.ajax.ficha', $model->id) . "' data-action='". route( 'manage.bookings.incidencias.tareas.ficha', $model->id) . "'";
                    $ret .= " <a href='#tarea-edit' $data class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Editar</a>";

                    // $ret .= "<a href='". route('manage.viajeros.tareas.ficha',[$model->id]) ."' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Editar</a>";

                    return $ret;
                })
                ->searchColumns('tipo','notas')
                ->orderColumns('fecha','notas')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.bookings.incidencias.tareas.index', compact('incidencia_id'));
    }

    public function getNuevo($incidencia_id)
    {
        $tipos = ConfigHelper::getTipoTarea();

        $incidencia_id = BookingIncidencia::find($incidencia_id)->id;
        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        return view('manage.bookings.incidencias.tareas.new', compact('incidencia_id','tipos','asignados'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->tarea->find($id);

        $tipos = ConfigHelper::getTipoTarea();

        $incidencia = $ficha->incidencia;
        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        return view('manage.bookings.incidencias.tareas.ficha', compact('ficha','incidencia','tipos','asignados'));
    }

    public function ajaxGetUpdate($id)
    {
        $ficha = $this->tarea->find($id);

        $incidencia = $ficha->incidencia;

        $ficha['hora'] = substr($ficha->fecha, 11,5);
        $ficha['fecha'] = Carbon::parse($ficha->fecha)->format('d/m/Y');

        $result = ['id'=> $id, 'datos'=> $ficha, 'incidencia'=> $incidencia, 'booking_id'=> $incidencia->booking_id, 'result'=> true ];
        return response()->json($result, 200);

    }

    public function postUpdate(Request $request, $id=0)
    {
        Session::flash('tab','#tareas');

        $this->validate($request, [
            'tarea_fecha'=> 'required',
            'tarea_hora'=> 'required',
            'tipo' => 'required',
        ]);

        $data = $request->except('_token','home','tarea_fecha','tarea_hora');
        $fecha = Carbon::createFromFormat('d/m/Y',$request->input('tarea_fecha'))->format('Y-m-d');
        $hora = $request->input('tarea_hora');
        $data['fecha'] = "$fecha $hora";

        if(!$id)
        {
            //nuevo
            $o = $this->tarea->create($data);
            $id = $o->id;

            BookingIncidenciaLog::addLog($o->incidencia, "Tarea nueva [$o->tipo]");

            $tarea = $this->tarea->find($id);
            $tarea->user_id = $request->user()->id;
            $tarea->save();
        }
        else
        {
            $this->tarea->update($data, $id);

            $tarea = $this->tarea->find($id);
            BookingIncidenciaLog::addLog($tarea->incidencia, "Tarea modificada [$tarea->tipo]");
        }

        $tarea = $this->tarea->find($id);

        //Mail asignado
        MailHelper::mailBookingTarea($tarea->id);

        return redirect()->route('manage.bookings.incidencias.ficha',$tarea->incidencia->id);
    }

    public function setCompleta(Request $request, $id, $estado=true)
    {
        $tarea = $this->tarea->find($id);

        $tarea->estado = $estado;
        $tarea->estado_fecha = Carbon::now()->format('Y-m-d');
        $tarea->estado_user_id = $request->user()->id;
        $tarea->save();

        BookingIncidenciaLog::addLog($tarea->incidencia, "Tarea estado [$tarea->tipo] [". (!$estado) ." -> $estado]");

        Session::flash('tab','#tareas');
        return redirect()->back();

        // return redirect()->route('manage.bookings.incidencias.ficha',$tarea->incidencia_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $incidencia = $this->tarea->find($id)->incidencia;

        $this->tarea->delete($id);

        BookingIncidenciaLog::addLog($incidencia, "Tarea eliminada [$o->tipo]");

        Session::flash('tab','#tareas');
        return redirect()->route('manage.bookings.incidencias.ficha',$incidencia->id);
    }

}
