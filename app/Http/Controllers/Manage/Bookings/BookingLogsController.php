<?php

namespace VCN\Http\Controllers\Manage\Bookings;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Bookings\BookingLogRepository as BookingLog;
// use VCN\Models\Bookings\BookingLog;

use VCN\Models\User;
use Datatable;
use Carbon;

use Log;

class BookingLogsController extends Controller
{
    private $log;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( BookingLog $log)
    {
        $this->log = $log;

        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');
    }

    public function getIndex($booking_id=0, $filtro=null)
    {
        // ini_set('memory_limit', '400M');

        if(Datatable::shouldHandle())
        {
            if($booking_id>0)
            {
                if($filtro)
                {
                    $col = $this->log->findWhere([ ['booking_id', $booking_id],['tipo','LIKE',"%$filtro%"] ])->sortByDesc('created_at');
                    // $col = BookingLog::where('booking_id', $booking_id)->where('tipo','LIKE',"%$filtro%");
                }
                else
                {
                    $col = $this->log->findAllBy('booking_id', $booking_id)->sortByDesc('created_at');
                    // $col = BookingLog::where('booking_id', $booking_id);
                }
            }
            else
            {
                $col = $this->log->all();
            }

            return Datatable::collection( $col )
                ->showColumns('tipo','notas')
                ->addColumn('fecha', function($model) {
                    return $model->created_at->format('d/m/Y H:i');
                })
                ->addColumn('usuario', function($model) {
                    return $model->user?$model->user->full_name:"-";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Historial' data-action='". route( 'manage.viajeros.logs.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('tipo','notas','usuario')
                ->orderColumns('fecha','*')
                ->setOrderStrip()->setSearchStrip()
                ->setAliasMapping()
                ->make();

        }

        return view('manage.bookings.logs.index', compact('booking_id'));
    }
}
