<?php

namespace VCN\Http\Controllers\Manage\Bookings;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Bookings\BookingIncidenciaLogRepository as BookingIncidenciaLog;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingIncidencia;

use Datatable;
use Session;
use ConfigHelper;

class BookingIncidenciaLogsController extends Controller
{
    private $log;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( BookingIncidenciaLog $log )
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');

        $this->log = $log;
    }


    public function getIndex($incidencia_id=0, $todos=true)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->log->all();
            if($incidencia_id)
            {
                $col = $this->log->findAllBy('incidencia_id', $incidencia_id)->sortByDesc('created_at');

                if(!$todos)
                {
                    $col = $this->log->findWhere([
                        'incidencia_id' => $incidencia_id,
                        ['tipo','<>',''],
                        ['tipo','<>','log']
                    ])->sortByDesc('created_at');
                }
            }

            return Datatable::collection( $col )
                ->showColumns('tipo','notas')
                ->addColumn('fecha', function($model) {
                    return $model->created_at->format('d/m/Y H:i');
                })
                ->addColumn('usuario', function($model) {
                    return $model->user?$model->user->full_name:"-";
                })
                ->addColumn('asignado', function($model) {
                    return $model->asignado?$model->asignado->full_name:"-";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Historial' data-action='". route( 'manage.viajeros.logs.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('tipo','notas','usuario','asignado')
                ->orderColumns('fecha','*')
                ->setOrderStrip()->setSearchStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.bookings.incidencias.logs.index', compact('booking_id'));
    }

    public function getNuevo($incidencia_id)
    {
        $tipos = ConfigHelper::getTipoTarea();

        $incidencia = BookingIncidencia::find($incidencia_id);
        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        return view('manage.bookings.incidencias.logs.new', compact('incidencia_id', 'incidencia','tipos','asignados'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->log->find($id);

        $tipos = ConfigHelper::getTipoTarea();

        $incidencia = $ficha->incidencia;
        $incidencia_id = $ficha->incidencia_id;

        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        return view('manage.viajeros.logs.ficha', compact('ficha','incidencia_id', 'incidencia','tipos','asignados'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'tipo' => 'required',
        ]);

        $data = $request->except('_token','tarea_notas');

        $data['notas'] = $request->input('tarea_notas');

        $incidencia_id = $request->input('incidencia_id');

        if(!$id)
        {
            //nuevo
            $o = $this->log->create($data);
            $id = $o->id;
        }
        else
        {
            $this->log->update($data, $id);
        }

        $t = $this->log->find($id);
        $t->user_id = $request->user()->id;
        $t->save();

        Session::flash('tab','#seguimiento');

        return redirect()->route('manage.bookings.incidencias.ficha',$incidencia_id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $o = $this->log->find($id);

        $incidencia_id = $o->incidencia_id;

        $o->delete();

        Session::flash('tab','#historial');

        return redirect()->route('manage.bookings.incidencias.ficha', $incidencia_id);
    }
}
