<?php

namespace VCN\Http\Controllers\Manage\Bookings;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Bookings\Status;
use VCN\Models\System\SystemLog;

use Datatable;
use Input;

class StatusController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->checkPermisosFullAdmin();
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection(  Status::all()->sortBy('orden') )
                ->showColumns('orden')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.bookings.status.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('manual', function($model) {
                    return $model->manual?"SI":"NO";
                })
                ->addColumn('plazas', function($model) {
                    return $model->plazas?"SI":"NO";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Status Inscrito' data-action='". route( 'manage.bookings.status.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('orden','name')
                ->orderColumns('*')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.bookings.status.index');
    }

    public function getNuevo()
    {
        return view('manage.bookings.status.new');
    }

    public function getUpdate($id)
    {
        $ficha = Status::find($id);

        return view('manage.bookings.status.ficha', compact('ficha'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'orden'   => 'required|min:1',
            'name' => 'required',
        ]);

        $o = Status::find($id);

        if(!$id)
        {
            $o = new Status;

            $o->orden = $request->input('orden');
            $o->name = $request->input('name');
            $o->manual = $request->has('manual');
            $o->plazas = $request->has('plazas');
            $o->save();

            SystemLog::addLog($o,'Nuevo');
        }
        else
        {
            $o1 = Status::find($id);

            $o->orden = $request->input('orden');
            $o->name = $request->input('name');
            $o->manual = $request->has('manual');
            $o->plazas = $request->has('plazas');
            $o->save();

            SystemLog::addLog($o,'Modificado',$o1);
        }

        return redirect()->route('manage.bookings.status.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $o = Status::find($id);
        $o->delete();

        return redirect()->route('manage.bookings.status.index');
    }
}
