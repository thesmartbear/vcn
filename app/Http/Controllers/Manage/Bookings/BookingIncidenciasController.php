<?php

namespace VCN\Http\Controllers\Manage\Bookings;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Bookings\BookingIncidenciaRepository as BookingIncidencia;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Bookings\BookingIncidenciaLog;
use VCN\Models\User;

use Datatable;
use Session;
use Carbon;
use ConfigHelper;
use VCN\Helpers\MailHelper;
use DB;

use VCN\Repositories\Criteria\FiltroPlataformaAsignado;
use VCN\Repositories\Criteria\FiltroLogueadoAsignado;

class BookingIncidenciasController extends Controller
{
    private $incidencia;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( BookingIncidencia $incidencia )
    {
        $this->checkPermisos('bookings');

        $this->incidencia = $incidencia;
    }


    public function getIndex($booking_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->incidencia->all();
            if($booking_id)
            {
                $col = $this->incidencia->findAllBy('booking_id',$booking_id)->sortBy('fecha');
            }

            return Datatable::collection( $col )
                ->addColumn('fecha', function($model) {
                    return "<a href='". route('manage.bookings.incidencias.ficha',[$model->id]) ."'>". Carbon::parse($model->fecha)->format('d/m/Y - H:i') ."</a>";
                    // return $model->created_at->format('d/m/Y');
                })
                ->showColumns('tipo','notas')
                ->addColumn('usuario', function($model) {
                    return $model->user?$model->user->full_name:"-";
                })
                ->addColumn('asignado', function($model) {
                    return $model->asignado?$model->asignado->full_name:"-";
                })
                ->addColumn('estado', function($model) {

                    $ret = "";

                    if(!$model->estado)
                    {
                        $ret = "<i class='fa fa-clock-o'></i>";

                        $ret .= "<span class='pull-right'>";
                        $ret .= " <a data-label='Resolver' href='". route('manage.bookings.incidencias.resolver',$model->id) ."' class='btn btn-success btn-xs'><i class='fa fa-check'></i></a>";
                        $ret .= "</span>";
                    }
                    else
                    {
                        $ret = "<i class='fa fa-check'></i>";

                        $ret .= "<span class='pull-right'>";
                        $ret .= " <a data-label='Marcar como no resuelta' href='". route('manage.bookings.incidencias.resolver',[$model->id,0]) ."' class='btn btn-danger btn-xs'><i class='fa fa-clock-o'></i></a>";
                        $ret .= "</span>";
                    }

                    return $ret;
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-get='". route( 'manage.bookings.incidencias.ajax.ficha', $model->id) . "' data-action='". route( 'manage.bookings.incidencias.ficha', $model->id) . "'";
                    $ret .= " <a href='#incidencia-edit' $data class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Editar</a>";

                    // $ret .= "<a href='". route('manage.viajeros.tareas.ficha',[$model->id]) ."' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Editar</a>";

                    return $ret;
                })
                ->searchColumns('tipo','notas')
                ->orderColumns('fecha','notas')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.bookings.incidencias.index', compact('booking_id'));
    }


    public function getIndexResumen(Request $request, $user_id=0,$oficina_id=0)
    {
        // $this->incidencia->pushCriteria(new FiltroPlataformaAsignado());
        // $this->incidencia->pushCriteria(new FiltroLogueadoAsignado($user_id,$oficina_id));

        if(Datatable::shouldHandle())
        {
            // $incidencias = $this->incidencia->findWhere([ 'estado'=>0 ]);
                // ,[DB::raw("DATE_FORMAT(fecha,'%Y-%m-%d')"), Carbon::today()->format('Y-m-d')] ]);

            // // dd($incidencias);
            // $col = collect();
            // foreach($incidencias as $inci)
            // {
            //     // dd($inci->tareas);
            //     // $col = $col->merge($inci->tareas);
            //     $col = $col->merge($inci->tareas->where('asign_to',$user_id));
            // }

            if(!$user_id || $user_id == "all" || $user_id == "all-off")
            {
                $user = $request->user();
            }
            else
            {
                $user = User::findOrFail($user_id);
            }

            $col = $user->incidencia_tareas;
            // dd($col);

            return Datatable::collection( $col )
                ->addColumn('fecha', function($model) {
                    return Carbon::parse($model->fecha)->format('d/m/Y - H:i');
                    // return $model->created_at->format('d/m/Y');
                })
                ->addColumn('viajero', function($model) {
                    $booking = $model->incidencia->booking;
                    return "<a href='". route('manage.viajeros.ficha',[$booking->viajero_id]) ."'>". $booking->viajero->full_name ."</a>";
                })
                ->showColumns('tipo','notas')
                ->addColumn('usuario', function($model) {
                    return $model->user?$model->user->full_name:"-";
                })
                ->addColumn('asignado', function($model) {
                    return $model->asignado?$model->asignado->full_name:"-";
                })
                ->addColumn('rating', function($model) {
                    $booking = $model->incidencia->booking;
                    return $booking->viajero->rating;
                })
                ->addColumn('estado', function($model) {

                    $ret = "";

                    if(!$model->estado)
                    {
                        $ret = "<i class='fa fa-clock-o'></i>";

                        $ret .= "<span class='pull-right'>";
                        $ret .= " <a data-label='Completar' href='". route('manage.viajeros.tareas.completar',[$model->id,1,1]) ."' class='btn btn-success btn-xs'><i class='fa fa-check'></i></a>";
                        $ret .= "</span>";
                    }
                    else
                    {
                        $ret = "<i class='fa fa-check'></i>";

                        $ret .= "<span class='pull-right'>";
                        $ret .= " <a data-label='Incompleta' href='". route('manage.viajeros.tareas.completar',[$model->id,0,1]) ."' class='btn btn-danger btn-xs'><i class='fa fa-clock-o'></i></a>";
                        $ret .= "</span>";
                    }

                    return $ret;

                })
                ->addColumn('options', function($model) {

                    return "<a data-label='Incidencia' href='". route('manage.bookings.incidencias.ficha',[$model->incidencia->id]) ."#tareas'><i class='fa fa-edit'></i></a>";
                })
                ->searchColumns('notas','viajero')
                ->orderColumns('fecha','notas','rating')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

    }

    public function getUpdate($id)
    {
        $ficha = $this->incidencia->find($id);

        $tipos = ConfigHelper::getTipoIncidencia();

        $booking_id = $ficha->booking_id;
        $booking = $ficha->booking;

        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        return view('manage.bookings.incidencias.ficha', compact('ficha','booking_id', 'booking','tipos','asignados'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        Session::flash('tab','#incidencias');

        $this->validate($request, [
            'incidencia_fecha'=> 'required',
            'incidencia_hora'=> 'required',
            'tipo' => 'required',
            'asign_to' => 'required',
        ]);

        $data = $request->except('_token','incidencia_fecha','incidencia_hora');
        $fecha = Carbon::createFromFormat('d/m/Y',$request->input('incidencia_fecha'))->format('Y-m-d');
        $hora = $request->input('incidencia_hora');
        $data['fecha'] = "$fecha $hora";

        if(!$id)
        {
            //nuevo
            $o = $this->incidencia->create($data);
            $id = $o->id;

            BookingLog::addLog($o->booking, "Incidencia [$id] nueva [$o->tipo]");
            BookingIncidenciaLog::addLog($o, "Incidencia nueva [$o->tipo]");

            $incidencia = $this->incidencia->find($id);
            $incidencia->user_id = $request->user()->id;
            $incidencia->save();
        }
        else
        {
            $this->incidencia->update($data, $id);

            $incidencia = $this->incidencia->find($id);
            BookingLog::addLog($incidencia->booking, "Incidencia [$incidencia->id] modificada [$incidencia->tipo]");
            BookingIncidenciaLog::addLog($incidencia, "Incidencia modificada [$incidencia->tipo]");
        }

        //Mail asignado
        MailHelper::mailBookingIncidencia($incidencia->id);

        // return redirect()->route('manage.bookings.ficha',$incidencia->booking_id);
        return redirect()->route('manage.bookings.incidencias.ficha',$id);
    }

    public function ajaxGetUpdate($id)
    {
        $ficha = $this->incidencia->find($id);

        $booking_id = $ficha->booking_id;
        $booking = $ficha->booking;

        $ficha['hora'] = substr($ficha->fecha, 11,5);
        $ficha['fecha'] = Carbon::parse($ficha->fecha)->format('d/m/Y');

        $result = ['id'=> $id, 'datos'=> $ficha, 'booking'=> $booking, 'titulo'=> $booking->id, 'result'=> true ];
        return response()->json($result, 200);

    }

    public function setCompleta(Request $request, $id, $estado=true)
    {
        $incidencia = $this->incidencia->find($id);

        $incidencia->estado = $estado;
        $incidencia->estado_fecha = Carbon::now()->format('Y-m-d H:i');
        $incidencia->estado_user_id = $request->user()->id;
        $incidencia->save();

        BookingLog::addLog($incidencia->booking, "Incidencia [$incidencia->id] estado [$incidencia->tipo] [". (!$estado) ." -> $estado]");
        BookingIncidenciaLog::addLog($incidencia, "Incidencia estado [$incidencia->tipo] [". (!$estado) ." -> $estado]");

        Session::flash('tab','#incidencias');
        return redirect()->route('manage.bookings.ficha',$incidencia->booking_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $booking = $this->incidencia->find($id)->booking;

        $this->incidencia->delete($id);

        Session::flash('tab','#incidencias');
        return redirect()->route('manage.bookings.ficha',$booking->id);
    }

}
