<?php

namespace VCN\Http\Controllers\Manage\Bookings;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Bookings\StatusChecklist;
use VCN\Models\Bookings\Status;
use VCN\Models\System\SystemLog;
use VCN\Models\Categoria;
use VCN\Models\Subcategoria;
use VCN\Models\SubcategoriaDetalle;
use VCN\Models\Cursos\Curso;
use VCN\Models\Centros\Centro;

use Datatable;
use Input;

class StatusChecklistsController extends Controller
{
    /**
     * Instantiate a new StatusChecklistsController instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->checkPermisosFullAdmin();
        $this->checkPermisos('checklist-bookings');
    }

    public function getIndex()
    {
        $status_id = Input::get('status_id');

        if($status_id>0)
        {
            return response()->json(StatusChecklist::where('status_id',$status_id)->pluck('name','id'), 200);
        }

        if(Datatable::shouldHandle())
        {
            return Datatable::collection( StatusChecklist::all()->sortBy('orden') )
                ->showColumns('orden')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.bookings.checklist.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('status', function($model) {
                    return $model->status->name;
                })
                ->addColumn('categoria', function($model) {
                    return $model->categoria?$model->categoria->name:"";
                })
                ->addColumn('subcategoria', function($model) {
                    return $model->subcategoria?$model->subcategoria->name:"";
                })
                ->addColumn('detalle', function($model) {
                    return $model->subcategoria_detalle?$model->subcategoria_detalle->name:"";
                })
                ->addColumn('curso', function($model) {
                    $ret = "<ul>";
                    foreach($model->cursos as $c)
                    {
                        $ret .= "<li>". $c->name ."</li>";
                    }
                    $ret .= "</ul>";
                    
                    return $ret;
                })
                ->addColumn('centro', function($model) {
                    $ret = "<ul>";
                    foreach($model->centros as $c)
                    {
                        $ret .= "<li>". $c->name ."</li>";
                    }
                    $ret .= "</ul>";
                    
                    return $ret;
                })
                ->showColumns('seguimiento')
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Checklist Status Inscritos' data-action='". route( 'manage.bookings.checklist.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                // ->orderColumns('name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.bookings.checklist.index');
    }

    public function getNuevo()
    {
        // $statuses = [""=>""] + Status::where('manual',1)->orderBy('orden')->pluck('name','id')->toArray();
        $statuses = Status::pluck('name','id')->toArray();
        $categorias = [""=>""] + Categoria::plataforma()->pluck('name','id')->toArray();
        $cursos = Curso::activos()->pluck('course_name','id')->toArray();
        $centros = Centro::all()->pluck('name','id')->toArray();

        return view('manage.bookings.checklist.new', compact('statuses','categorias','cursos','centros'));
    }

    public function getUpdate($id)
    {
        $ficha = StatusChecklist::find($id);

        $statuses = Status::pluck('name','id')->toArray();
        $categorias = [""=>""] + Categoria::plataforma()->pluck('name','id')->toArray();
        $subcategorias = [""=>""] + Subcategoria::where('category_id',$ficha->category_id)->pluck('name','id')->toArray();
        $subcategorias_det = [""=>""] + SubcategoriaDetalle::where('subcategory_id',$ficha->subcategory_id)->pluck('name','id')->toArray();
        $cursos = Curso::activos()->pluck('course_name','id')->toArray();
        $centros = Centro::all()->pluck('name','id')->toArray();

        return view('manage.bookings.checklist.ficha', compact('ficha','statuses','cursos','centros','categorias','subcategorias','subcategorias_det'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'status_id' => 'required',
            'orden' => 'required',
            'name' => 'required',
        ]);

        $o = StatusChecklist::find($id);

        if(!$id)
        {
            $o = new StatusChecklist;
        }
        else
        {
            $o1 = StatusChecklist::find($id);
        }

        $o->status_id = $request->input('status_id');
        $o->name = $request->input('name');
        $o->orden = $request->input('orden');
        $o->category_id = $request->get('category_id') ?: 0;
        $o->subcategory_id = $request->get('subcategory_id') ?:0;
        $o->subcategory_det_id = $request->get('subcategory_det_id') ?:0;
        $o->seguimiento = $request->input('seguimiento');
        $o->curso_id = implode(',', $request->get('curso_id',[]));
        $o->centro_id = implode(',', $request->get('centro_id',[]));
        $o->save();

        if(!$id)
        {
            SystemLog::addLog($o,'Nuevo');
        }
        else
        {
            SystemLog::addLog($o,'Modificado',$o1);
        }

        return redirect()->route('manage.bookings.checklist.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $o = StatusChecklist::find($id);
        $o->delete();

        return redirect()->route('manage.bookings.checklist.index');
    }
}
