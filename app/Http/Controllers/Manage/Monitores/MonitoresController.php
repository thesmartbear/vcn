<?php

namespace VCN\Http\Controllers\Manage\Monitores;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Monitores\Monitor;
use VCN\Models\Monitores\MonitorRelacion;

use Datatable;
use ConfigHelper;
use Session;

use VCN\Helpers\MailHelper;

class MonitoresController extends Controller
{
    public function __construct()
    {
        $this->checkPermisos('monitores');
        $this->middleware("permiso.view:monitores-list", ['only' => ['getIndex','getUpdate']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex(Request $request)
    {
        $user = $request->user();

        if(Datatable::shouldHandle())
        {
            return Datatable::collection( Monitor::plataforma()->get() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.monitores.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('relaciones', function($model) {
                    // return $model->relaciones->count();

                    $ret = "<ul>";
                    foreach($model->relaciones as $relacion)
                    {
                        $data = " data-label='Borrar' data-model='Relación' data-action='". route( 'manage.monitores.relaciones.delete', $relacion->id) . "'";
                        $borrar = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                        $ret .= "<li>";
                        $ret .= "<a target='_blank' href='$relacion->link'>". $relacion->modelo_name ."</a> $borrar";
                        $ret .= "</li>";
                    }
                    $ret .= "</ul>";

                    if($model->relaciones->count()>1)
                    {
                        $data = " data-label='Borrar' data-model='Todas las relaciones' data-action='". route( 'manage.monitores.relaciones.delete', [$model->id, 1]) . "'";
                        $borrar = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i> Todas</a>";

                        $ret .= "<br> $borrar";
                    }

                    return $ret;
                })
                ->addColumn('user', function($model) use ($user) {
                    $ret = $model->usuario ? $model->usuario->username : "-";

                    if($model->user && ConfigHelper::canEdit('superlogin'))
                    {
                        $ret .= "<a class='pull-right' href='". route('manage.system.admins.login', $model->usuario->id)  ."'><i class='fa fa-sign-in'></i></a>";
                    }

                    return $ret;
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Monitor' data-action='". route( 'manage.monitores.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        return view('manage.monitores.index');
    }

    public function getRelaciones(Request $request, $id)
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection( MonitorRelacion::where('monitor_id', $id)->get() )
                ->addColumn('tipo', function($model) {
                    return $model->modelo;
                })
                ->addColumn('relacion', function($model) {
                    return "<a target='_blank' href='$model->link'>". $model->modelo_name ."</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Relación' data-action='". route( 'manage.monitores.relaciones.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('relacion')
                ->orderColumns('relacion')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }
    }

    public function getNuevo()
    {
        return view('manage.monitores.new');
    }

    public function getUpdate($id)
    {
        $ficha = Monitor::find($id);

        return view('manage.monitores.ficha', compact('ficha'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $data = $request->except('foto','plataforma_check','foto_delete');

        $data['es_director'] = $request->has('es_director');

        $p = Monitor::find($id);

        $u0 = $p ? $p->email : "";
        $p0 = $p ? $p->plataforma : 0;
        
        if(!$id)
        {
            $p = Monitor::create($data);
            $id = $p->id;

        }
        else
        {
            $p->update($data);
        }

        $bSend = false;
        if($p->usuario)
        {
            if($u0 != $p->email)
            {
                $p->usuario->email = $p->email;
                $p->usuario->username = $p->email;

                $bSend = true;
            }

            if($p0 != $p->plataforma)
            {
                $p->usuario->plataforma = $p->plataforma;
            }
            
            $p->usuario->save();

            if($bSend)
            {
                MailHelper::mailAreaEmail($p);
            }
        }

        if($request->hasFile('foto'))
        {
            $file = $request->file('foto');
            $dirp = "assets/uploads/monitor/". $id . "/";

            $p->foto = ConfigHelper::uploadFoto($file, $dirp, 800);
            $p->save();
        }

        return redirect()->route('manage.monitores.ficha', $id);
    }

    public function destroy($id)
    {
        $ficha = Monitor::findOrFail($id);

        $ficha->delete();

        return redirect()->route('manage.monitores.index');
    }

    public function destroyRelacion($idr, $todas=false)
    {
        if($todas)
        {
            $ficha = Monitor::findOrFail($idr);
            MonitorRelacion::where('monitor_id', $ficha->id)->delete();
        }
        else
        {
            $ficha = MonitorRelacion::findOrFail($idr);
            $id = $ficha->id;
            $ficha->delete();
        }

        Session::flash('tab','#relaciones');

        return redirect()->back();//route('manage.monitores.ficha',$id);
    }

    public function setUser(Request $request, $id)
    {
        $ficha = Monitor::findOrFail($id);
        $result = $ficha->setUsuario();

        if(!$result)
        {
            Session::flash('mensaje-alert', "No se ha podido crear el Usuario. Revise el email");
        }

        return redirect()->route('manage.monitores.index');
    }
}
