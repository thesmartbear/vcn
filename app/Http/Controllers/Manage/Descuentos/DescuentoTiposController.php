<?php

namespace VCN\Http\Controllers\Manage\Descuentos;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Descuentos\DescuentoTipoRepository as DescuentoTipo;

use VCN\Models\Monedas\Moneda;
use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingDescuento;
use VCN\Models\Leads\Origen;
use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Prescriptores\Prescriptor;

use Datatable;
use Carbon;
use Session;
use ConfigHelper;
use VCN\Models\Leads\Suborigen;
use VCN\Models\Leads\SuborigenDetalle;
use VCN\Models\Especialidad;
use VCN\Models\Subespecialidad;

class DescuentoTiposController extends Controller
{
    protected $descuento;

    /**
     * Instantiate a new DescuentoTiposController instance.
     *
     * @return void
     */
    public function __construct(DescuentoTipo $descuento)
    {
        // $this->checkPermisosFullAdmin();
        $this->checkPermisos('descuentos');

        $this->descuento = $descuento;
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->descuento->all();

            $filtro = ConfigHelper::config('propietario');
            if( $filtro && !auth()->user()->isFullAdmin() )
            {
                $col1 = $this->descuento->findWhere(['propietario'=> $filtro]);
                $col = $this->descuento->findWhere(['propietario'=> 0]);
                $col = $col->merge($col1);
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.descuentos.tipos.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->showColumns('valor','codigo')
                ->addColumn('unidad', function($model) {
                    return $model->tipo==1?$model->moneda_name:"%";
                })
                ->addColumn('sobre', function($model) {
                    return $model->tipo_name;
                })
                ->addColumn('activo', function($model) {
                    return $model->activo?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('propietario', function($model) {

                    if($model->propietario)
                    {
                        return ConfigHelper::plataforma($model->propietario);
                    }

                    return "Común";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Tipo Descuento' data-action='". route( 'manage.descuentos.tipos.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.descuentos.tipos.index');
    }

    public function getNuevo()
    {
        $monedas = Moneda::pluck('currency_name','id');

        $conocidos = [""=>""] + Origen::plataforma()->pluck('name','id')->toArray();
        $prescriptores = [""=>""] + Prescriptor::plataforma()->pluck('name','id')->toArray();

        $eids = Especialidad::where('es_multi',1)->pluck('id')->toArray();
        $especialidades = [0=>""] + SubEspecialidad::whereIn('especialidad_id',$eids)->pluck('name','id')->toArray();

        return view('manage.descuentos.tipos.new',compact('monedas','conocidos','prescriptores','especialidades'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->descuento->find($id);

        $monedas = Moneda::pluck('currency_name','id');
        
        $prescriptores = [""=>""] + Prescriptor::plataforma()->pluck('name','id')->toArray();
        $conocidos = [""=>""] + Origen::plataforma()->pluck('name','id')->toArray();
        $conocidosSub = [""=>""] + Suborigen::where('id', $ficha->codigo_suborigen)->pluck('name','id')->toArray();
        $conocidosSubD = [""=>""] + SuborigenDetalle::where('id', $ficha->codigo_suborigendet)->pluck('name','id')->toArray();

        $eids = Especialidad::where('es_multi',1)->pluck('id')->toArray();
        $especialidades = [0=>""] + SubEspecialidad::whereIn('especialidad_id',$eids)->pluck('name','id')->toArray();

        return view('manage.descuentos.tipos.ficha', compact('ficha','monedas','conocidos','conocidosSub','conocidosSubD','prescriptores','especialidades'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $data = $request->except('_token','propietario_check', 'origen_id', 'suborigen_id', 'suborigendet_id', 'prescriptor_id', 'especialidad_id', 'especialidad_importe');
        // dd($data);

        //publico (0) /privado (1,2)
        $data['propietario'] = $request->has('propietario')?$request->input('propietario'):0;
        $data['activo'] = $request->has('activo');
        $data['valor'] = $request->get('valor') ?: 0;

        $data['codigo_origen'] = $request->has('origen_id')?$request->input('origen_id'):0;
        $data['codigo_suborigen'] = $request->has('suborigen_id')?$request->input('suborigen_id'):0;
        $data['codigo_suborigendet'] = $request->has('suborigendet_id')?$request->input('suborigendet_id'):0;
        $data['codigo_prescriptor'] = $request->has('prescriptor_id')?$request->input('prescriptor_id'):0;

        if( $request->has('propietario_check') )
        {
            $data['propietario'] = ConfigHelper::config('propietario');
        }

        if(!$id)
        {
            $this->validate($request, [
                'name' => 'required|unique:descuento_tipos|max:255',
            ]);

            //Nuevo
            $p = $this->descuento->create($data);
            $id = $p->id;
        }
        else
        {
            $p = $this->descuento->update($data, $id);
        }

        $eids = $request->get('especialidad_id');
        $eimportes = $request->get('especialidad_importe');
        foreach($eids as $ie => $e)
        {
            if(!$e)
            {
                unset($eids[$ie]);
                unset($eimportes[$ie]);
            }
        }

        $esmulti = (bool) count($eids);

        $ficha = $this->descuento->find($id);
        $ficha->es_multi = $esmulti;
        $ficha->especialidad_id = $eids;
        $ficha->especialidad_importe = $eimportes;
        $ficha->save();

        return redirect()->route('manage.descuentos.tipos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $o = $this->descuento->find($id);
        $o->delete();

        return redirect()->route('manage.descuentos.tipos.index');
    }



    public function aplicar(Request $request)
    {
        $descuento_id = $request->input('descuento_id');
        $booking_id = $request->input('booking_id');

        $descuento = $this->descuento->find($descuento_id);

        if(!$descuento)
        {
            Session::flash('mensaje-alert','Descuento no existe.');
            Session::flash('tab','#descuentos');
            return redirect()->route('manage.bookings.ficha', $booking_id);
        }


        $booking = Booking::find($booking_id);

        if( $booking->setDescuento($descuento_id) )
        {
            $booking->mailAvisoCambios();
        }

        Session::flash('tab','#descuentos');
        return redirect()->route('manage.bookings.ficha', $booking_id);
    }

    public function crear(Request $request)
    {
        $booking_id = $request->input('booking_id');

        $booking = Booking::find($booking_id);

        $d = new BookingDescuento;
        $d->booking_id = $booking_id;
        $d->fecha = Carbon::now()->format('Y-m-d');
        $d->user_id = $request->user()->id;

        $precio= $booking->precio_total;
        $ptotal = $precio['total'];
        $pcurso = $precio['total_curso'];
        $pextras = $precio['total_extras'];

        $tipo = $request->input('dto_tipo');
        $tipo_pc = $request->input('dto_tipo_pc');
        $valor = $request->input('dto_valor');

        if($tipo==0)
        {
            $d->currency_id = ConfigHelper::default_moneda_id();

            //Porcentaje
            switch($tipo_pc)
            {
                case 0: //total
                {
                    $importe = ($ptotal * $valor)/100;
                    $notas = "Descuento (". $valor ."% Total): $importe";
                }
                break;

                case 1: //curso
                {
                    $importe = ($pcurso * $valor)/100;
                    $notas = "Descuento (". $valor ."% Curso): $importe";

                    $d->currency_id = $booking->convocatoria->moneda_id;

                }
                break;

                case 2: //total menos extras
                {
                    $importe = ( ($ptotal-$pextras) * $valor)/100;
                    $notas = "Descuento (". $valor ."% Total-Extras): $importe";
                }
                break;
            }

        }
        else
        {
            //Importe
            $importe = $valor;
            // $notas = "Descuento (Importe)";
            $notas = $request->input('dto_notas');

            $d->currency_id = $request->input('dto_moneda_id');
        }

        $d->importe = $importe;
        $d->notas = $notas;
        $d->save();

        $booking->mailAvisoCambios();

        Session::flash('tab','#descuentos');
        return redirect()->route('manage.bookings.ficha', $booking_id);
    }
}
