<?php

namespace VCN\Http\Controllers\Manage\Descuentos;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Descuentos\DescuentoEarlyRepository as DescuentoEarly;

use VCN\Models\Monedas\Moneda;

use Datatable;
use Carbon;
use Session;
use ConfigHelper;

class DescuentoEarlysController extends Controller
{
    protected $descuento;

    public function __construct(DescuentoEarly $descuento)
    {
        // $this->checkPermisosFullAdmin();
        $this->checkPermisos('descuentos');

        $this->descuento = $descuento;
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->descuento->all();

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.descuentos.early-bird.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->showColumns('importe')
                ->addColumn('moneda', function($model) {
                    return $model->moneda?$model->moneda->name:"-";
                })
                ->showColumns('desde','hasta')
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Descuento Early Bird' data-action='". route( 'manage.descuentos.early-bird.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.descuentos.early.index');
    }

    public function getNuevo()
    {
        $monedas = Moneda::pluck('currency_name','id');
        return view('manage.descuentos.early.new',compact('monedas'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->descuento->find($id);

        $monedas = Moneda::pluck('currency_name','id');

        return view('manage.descuentos.early.ficha', compact('ficha','monedas'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $data = $request->except('_token');

        $data['desde'] = Carbon::createFromFormat('d/m/Y',$data['desde'])->format('Y-m-d');
        $data['hasta'] = Carbon::createFromFormat('d/m/Y',$data['hasta'])->format('Y-m-d');

        if(!$id)
        {
            //Nuevo
            $p = $this->descuento->create($data);
            $id = $p->id;
        }
        else
        {
            $p = $this->descuento->update($data, $id);
        }

        return redirect()->route('manage.descuentos.early-bird.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $o = $this->descuento->find($id);
        $o->delete();

        return redirect()->route('manage.descuentos.early-bird.index');
    }
}
