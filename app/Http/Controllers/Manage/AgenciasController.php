<?php

namespace VCN\Http\Controllers\Manage;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\AgenciaRepository as Agencia;

use Datatable;

class AgenciasController extends Controller
{
    protected $agencia;

    public function __construct(Agencia $agencia)
    {
        $this->checkPermisos('vuelos');

        $this->agencia = $agencia;
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->agencia->all()->sortBy('name');

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.agencias.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Agencia' data-action='". route( 'manage.agencias.delete', $model->id) . "'";
                    $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        // return \Response::json($this->agencia->all());
        return view('manage.agencias.index');
    }


    public function getNuevo()
    {
        return view('manage.agencias.new');
    }

    public function getUpdate($id)
    {
        $ficha = $this->agencia->find($id);

        return view('manage.agencias.ficha', compact('ficha'));
    }

    public function postUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $data = $request->except("_token");

        if(!$id)
        {
            $p = $this->agencia->create($data);
            $id = $p->id;
        }
        else
        {
            $p = $this->agencia->update($data, $id);
        }

        return redirect()->route('manage.agencias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->agencia->delete($id);
        return redirect()->route('manage.agencias.index');
    }
}
