<?php

namespace VCN\Http\Controllers\Manage;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Extras\ExtraRepository as Extra;

use VCN\Models\Monedas\Moneda;
use VCN\Models\Extras\ExtraUnidad;
use VCN\Models\Cursos\Curso;
use VCN\Models\Cursos\CursoExtraGenerico;
use VCN\Models\Categoria;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Input;
use File;

class ExtrasController extends Controller
{
    private $extra;

    /**
     * Instantiate a new ExtrasController instance.
     *
     * @return void
     */
    public function __construct( Extra $extra )
    {
        // $this->checkPermisosFullAdmin();
        $this->checkPermisos('extras');

        $this->extra = $extra;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection( $this->extra->all() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.extras.ficha',[$model->id]) ."'>$model->generic_name</a>";
                })
                ->addColumn('unidad', function($model) {

                    $ret = ConfigHelper::getTipoUnidad($model->generic_unit);

                    if($model->generic_unit)
                    {
                        $ret .= " (". ($model->generic_unit_id?$model->tipo->name:'-') .")";
                    }

                    return $ret;

                })
                ->showColumns('generic_price')
                ->addColumn('categorias', function($model) {
                    $ret = "";

                    if($model->categorias_id == null)
                    {
                        return "";
                    }

                    $cats = explode(',', $model->categorias_id);
                    $tcats = count($cats);
                    $i = 0;
                    foreach($cats as $cid)
                    {
                        if($cid)
                        {
                            $c = Categoria::find($cid);
                        }
                        else
                        {
                            return "Todas";
                        }

                        $ret .= $c->name;

                        $i++;

                        if($i<$tcats)
                        {
                            $ret .= ", ";
                        }
                    }

                    return $ret;
                })
                ->addColumn('requerido', function($model) {
                    return $model->generic_required?"<span class='label label-success'><i class='fa fa-check-circle fa-1x'></i></span>":"<span class='label label-danger'><i class='fa fa-times-circle fa-1x'></i></span>";
                })
                ->addColumn('es_cancelacion', function($model) {
                    return $model->es_cancelacion?"<span class='label label-success'><i class='fa fa-check-circle fa-1x'></i></span>":"<span class='label label-danger'><i class='fa fa-times-circle fa-1x'></i></span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Extra Genérico' data-action='". route( 'manage.extras.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('generic_name','name')
                // ->orderColumns('generic_name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.extras.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getNuevo()
    {
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();
        $unidades = ['' => ''] + ExtraUnidad::pluck('name','id')->toArray();
        $unidades_tipo = ConfigHelper::getTipoUnidad();

        $categorias = [0 => 'Todos'] + Categoria::plataforma()->pluck('name','id')->toArray();

        return view('manage.extras.new', compact('unidades','monedas','unidades_tipo','categorias'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getUpdate($id)
    {
        $monedas = Moneda::pluck('currency_name','id');
        $unidades = ExtraUnidad::pluck('name','id');
        $unidades_tipo = ConfigHelper::getTipoUnidad();

        $ficha = $this->extra->find($id);

        $categorias = [0 => 'Todos'] + Categoria::plataforma()->pluck('name','id')->toArray();

        return view('manage.extras.ficha', compact('ficha','unidades','monedas','unidades_tipo','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'generic_name' => 'required',
            'generic_price' => 'required',
            'generic_unit'  => 'required',
            'generic_unit_id'  => 'required_if:generic_unit,1',
            'generic_currency_id'    => 'required',
        ]);

        $data = $request->except('_token','_method','pdf_condiciones_delete');

        $data['generic_required'] = $request->get('generic_required');
        $data['generic_unit_id'] = $request->get('generic_unit_id') ?: 0;
        $data['es_cancelacion'] = $request->has('es_cancelacion');
        $data['es_medico'] = $request->has('es_medico');

        if( $request->hasFile('pdf_condiciones') )
        {
            $file = $request->file('pdf_condiciones');

            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file_name = str_slug($file_name) .".". $file->getClientOriginalExtension();
            $dirp = "assets/uploads/Extra/". $id . "/es/";
            $dir = public_path($dirp);

            $file->move($dir, $file_name);

            $data['pdf_condiciones'] = "/".$dirp.$file_name;
        }

        $cats = $request->get('categorias_id', []);
        if( (count($cats)>1 && $cats[0]==0) )
        {
            $cats = [];
            $cats[0] = 0;
        }

        if(!$cats)
        {
            $cats = [];
        }

        $data['categorias_id'] = implode(',', $cats);

        if(!$id)
        {
            $e = $this->extra->create($data);
            $id = $e->id;
        }
        else
        {
            $this->extra->update($data,$id);
        }

        $extra = $this->extra->find($id);

        if( $request->has('pdf_condiciones_delete'))
        {
            if(file_exists(public_path($extra->pdf_condiciones)))
            {
                \File::delete($extra->pdf_condiciones);
            }

            $extra->pdf_condiciones = null;
            $extra->save();
        }

        if($data['es_medico'])
        {
            return redirect()->route('manage.extras.index');
        }

        foreach( $cats as $cid)
        {
            if($cid == 0)
            {
                $cursos = Curso::all();
            }
            else
            {
                $cursos = Curso::where('category_id',$cid)->get();
            }

            foreach($cursos as $curso)
            {
                //Solo si no lo tiene ya
                if($curso->extrasGenericos->where('generics_id',$extra->id)->count()==0)
                {
                    $ce = new CursoExtraGenerico;
                    $ce->generics_id = $extra->id;
                    $ce->course_id = $curso->id;
                    $ce->save();
                }
            }

            if($cid == 0)
            {
                break;
            }
        }

        // return redirect()->route('manage.extras.edit',$id);
        return redirect()->route('manage.extras.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->extra->delete($id);
        return redirect()->route('manage.extras.index');
    }
}
