<?php

namespace VCN\Http\Controllers\Manage\Solicitudes;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Solicitudes\SolicitudRepository as Solicitud;
use VCN\Repositories\Leads\ViajeroRepository as Viajero;

use VCN\Models\Solicitudes\Solicitud as SolicitudModel;
use VCN\Models\Leads\Viajero as ViajeroModel;

use VCN\Models\Solicitudes\Status;
use VCN\Models\Solicitudes\SolicitudChecklist;
use VCN\Models\User;

use VCN\Models\Bookings\BookingLog;
use VCN\Models\Leads\ViajeroLog;
use VCN\Models\System\Oficina;

use VCN\Helpers\ConfigHelper;
use VCN\Repositories\Criteria\FiltroPlataformaViajeroRel;
use VCN\Repositories\Criteria\FiltroStatusOrden;
use VCN\Repositories\Criteria\FiltroOficinaViajero;

use VCN\Helpers\MailHelper;

use Datatable;
use Carbon;
use Session;
use Form;
use DB;
use Log;

class SolicitudesController extends Controller
{
    private $solicitud;
    private $viajero;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Solicitud $solicitud, Viajero $viajero )
    {
        $this->middleware("permiso.view:solicitudes-list", ['only' => ['getIndex','getUpdate']]);

        $this->solicitud = $solicitud;
        $this->viajero = $viajero;
    }

    public function getIndexBy(Request $request, $status_id=0, $user_id=0, $oficina_id=0)
    {
        $any = intval($request->input('any',Carbon::now()->format('Y')));

        if(!$user_id)
        {
            $user_id = (int)$request->user()->id;
        }

        if(!$oficina_id)
        {
            $oficina_id = 'all';
        }

        $user = auth()->user();
        if(!$user->filtro_oficinas)
        {
            $oficina_id = $user->oficina_id;
        }

        //si elige oficina que se quede seleccionado el primero de esa ofi
        if($oficina_id>0 )
        {
            $asig = User::find($user_id);
            if($asig)
            {
                if($asig->oficina_id != $oficina_id)
                {
                    $asignado = User::where('oficina_id',$oficina_id)->orderBy('fname')->first();
                    if($asignado)
                    {
                        $user_id = $asignado->id;
                    }
                }
            }
        }


        if($user_id>0 && $oficina_id>0)
        {
            $user = User::find($user_id);
            if($user->oficina_id != $oficina_id)
            {
                $error = "Combinación incorrecta. Oficina que no corresponde al usuario. Solo filtra usuario.";
                Session::flash('mensaje', $error);
            }
        }

        //Query
        $query = DB::table('solicitudes');

        if($user_id>0)
        {
            $query = $query->where('solicitudes.user_id',$user_id);
        }
        else
        {
            if($user_id=='all')
            {
                $usuarios = User::asignados()->get();
            }
            else
            {
                $usuarios = User::asignadosOff()->get();
            }

            if($oficina_id>0)
            {
                $u = $usuarios->where('oficina_id', $oficina_id)->pluck('id')->toArray();
                $query = $query->whereIn('solicitudes.user_id', $u);
            }
            else
            {
                $u = $usuarios->pluck('id')->toArray();
                $query = $query->whereIn('solicitudes.user_id', $u);

                $plat = ConfigHelper::config('propietario');
                if($plat)
                {
                    $query = $query->where( function($q) use ($plat) {
                        $q->where('solicitudes.plataforma', $plat)->orWhere('solicitudes.plataforma', 0);
                    });
                }
            }
        }

        //any
        $fecha1 = $any."-01-01";
        $fecha2 = $any."-12-31";
        $query = $query->where('fecha','>=',$fecha1)->where('fecha','<=',$fecha2);

        // dd($query->count());
        // dd($query->where('solicitudes.status_id',6)->count());

        // DB::enableQueryLog();
        $querytot = $query;

        // print_r(DB::getQueryLog());


        if(Datatable::shouldHandle())
        {
            //tab todos
            if(!$status_id)
            {
                $st = Status::where('orden','>',0)->pluck('id');
                $query = $query->whereIn('solicitudes.status_id', $st);
            }
            else
            {
                $query = $query->where('solicitudes.status_id', $status_id);
            }

            $query = $query->select(
                    'solicitudes.id',
                    'solicitudes.rating as rating',
                    'statuses.name as status',
                    'solicitudes.fecha as fecha',
                    DB::raw("CONCAT(viajeros.name,' ',viajeros.lastname) as viajero"),
                    'viajero_origenes.name as origen',
                    // 'viajero_suborigenes.name as suborigen',
                    // 'viajero_suborigen_detalles.name as suborigendet',
                    'categorias.name as categoria',
                    // 'viajero_logs.updated_at as ultimo'
                    DB::raw('(SELECT updated_at FROM viajero_logs WHERE viajero_id = viajeros.id AND tipo<>"" AND tipo<>"log" ORDER BY updated_at DESC LIMIT 1) as ultimo')
                )
                ->join('viajeros','solicitudes.viajero_id','=','viajeros.id')
                ->leftJoin('viajero_origenes','viajero_origenes.id','=','viajeros.origen_id')
                // ->leftJoin('viajero_logs','viajero_logs.viajero_id','=','viajeros.id')
                // ->leftJoin('viajero_suborigenes','viajero_suborigenes.id','=','viajeros.suborigen_id')
                // ->leftJoin('viajero_suborigen_detalles','viajero_suborigen_detalles.id','=','viajeros.suborigendet_id')
                ->leftJoin('categorias','categorias.id','=','solicitudes.category_id')
                ->leftJoin('statuses','statuses.id','=','solicitudes.status_id');
                // ->join('viajero_logs', function ($join) {
                //     $join->on('viajeros.id', '=', 'viajero_logs.viajero_id')
                //          ->where('viajero_tareas.tipo','<>','')->where('viajero_tareas.tipo','<>','log');
                // });
                
            //origen/suborigen/suborigendet
            $origen_id = $request->get('origen_id',0);
            $suborigen_id = $request->get('suborigen_id',0);
            $suborigendet_id = $request->get('suborigendet_id',0);
            
            if($origen_id)
            {
                $query = $query->where('viajeros.origen_id', $origen_id);
            }

            if($suborigen_id)
            {
                $query = $query->where('viajeros.suborigen_id', $suborigen_id);
            }

            if($suborigendet_id)
            {
                $query = $query->where('viajeros.suborigendet_id', $suborigendet_id);
            }

            $prescriptor_id = $request->get('prescriptor_id',0);
            if($prescriptor_id)
            {
                $query = $query->where('viajeros.prescriptor_id', $prescriptor_id);
            }

            return Datatable::query( $query )
                ->showColumns('fecha')
                ->addColumn('viajero', function($column) {
                    $model = SolicitudModel::find($column->id);
                    $name = $model->viajero->full_name;
                    return "<a href='". route('manage.viajeros.ficha',[$model->viajero->id]) ."'>$name</a>";
                })
                ->addColumn('categoria', function($column) {
                    $model = SolicitudModel::find($column->id);
                    return $model->categoria?$model->categoria->name:"-";
                })
                ->addColumn('origen', function($column) {
                    $model = SolicitudModel::find($column->id);
                    return $model->viajero->origen_full_name;
                })
                ->addColumn('status', function($column) {
                    $model = SolicitudModel::find($column->id);

                    $ret = "<span class='badge badge-warning'>". ($model->status?$model->status->name:"-") . "</span>&nbsp;";

                    if(!$model->status)
                        $ret = "<span class='badge'>Archivado</span>&nbsp;";

                    /*if(!$model->status)
                        return $ret;

                    if($model->status_id == 0)
                    {
                        $status_txt = $model->status->name . " (". $model->archivar_motivo.")";
                        if($model->archivar_motivo == "otro" or $model->archivar_motivo == "AUTO")
                        {
                            $status_txt = $model->status->name . " (". $model->archivar_motivo.": $model->archivar_motivo_nota)";
                        }

                        $ret = "<span class='badge'>". ($model->status?($status_txt):"-") . "</span>&nbsp;";
                    }

                    $statuses = [""=>""] + Status::orderBy('orden')->pluck('name','id')->toArray();

                    $ret .= "<div class='pull-right'>";
                    $ret .= Form::select("status-$model->id", $statuses, $model->status_id, array( 'data-id'=> $model->id, 'class'=> 'form-control status-item'));

                    $ret .= "&nbsp;&nbsp;<a href='#checklist' data-id='". $model->id ."' data-label='Checklist' class='checklist-item'><i class='fa fa-tasks'></i></a>";
                    $ret .= "</div>";
                    */

                    return $ret;
                })
                ->showColumns('rating')
                // ->addColumn('rating', function($column) {
                //     $model = SolicitudModel::find($column->id);
                //     return $model->rating;
                // })
                ->addColumn('asignado', function($column) {
                    $model = SolicitudModel::find($column->id);
                    return $model->asignado?$model->asignado->full_name:"-";
                })
                // ->addColumn('contacto', function($column) {
                //     $model = SolicitudModel::find($column->id);
                //     $s = $model->viajero->seguimientos()->orderBy('updated_at','desc')->first();
                //     return $s?$s->updated_at->format('Y-m-d'):"";
                //     // return $s?Carbon::parse($s->updated_at)->format('d/m/Y'):"-";
                // })
                ->addColumn('ultimo', function($column) {
                    return substr($column->ultimo,0,10);
                })
                ->addColumn('tarea', function($column) {
                    $model = SolicitudModel::find($column->id);
                    $t = $model->viajero->tareas->where('estado',0)->count();
                    return $t>0?"Si [$t]":"No";
                })
                ->addColumn('options', function($column) {
                    $model = SolicitudModel::find($column->id);

                    $ret = "";

                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('viajeros.name','viajeros.lastname','viajeros.lastname2')
                // ->orderColumns('fecha','viajero','rating','categoria','name','lastname','lastname2','contacto','tarea')
                ->orderColumns('fecha','ultimo','viajero','rating','origen','categoria','status')
                ->setAliasMapping()
                ->make();
        }

        $statuses = Status::orderBy('orden')->get();

        $statuses_total = [];
        $statuses_total[0] = 0;

        foreach($statuses as $status)
        {
            $qt = clone $querytot;
            $statuses_total[$status->id] = $qt->where('status_id', $status->id)->select()->count();

            if($status->orden>0)
            {
                $statuses_total[0] += $statuses_total[$status->id];
            }
        }

        $oficinas = Oficina::plataforma();

        $anys = \VCN\Models\Informes\Venta::groupBy('any')->get()->pluck('any','any')->toArray();
        if(!in_array($any, $anys))
        {
            $anys[$any] = $any;
        }

        return view('manage.solicitudes.index', compact('status_id', 'user_id', 'oficina_id', 'oficinas', 'statuses', 'statuses_total', 'anys', 'any'));
    }


    public function getIndexByViajero($viajero_id)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->viajero->find($viajero_id)->solicitudes->sortBy('fecha');

            return Datatable::collection( $col )
                ->addColumn('fecha', function($model){
                    return $model->fecha?Carbon::parse($model->fecha)->format('Y-m-d'):"Pendiente";
                    // return "<a target='_blank' href='". route('manage.solicitudes.ficha',[$model->id]) ."'>$name</a>";
                })
                ->showColumns('destinos','ciudad','cursos','notas')
                ->addColumn('asignado', function($model) {
                    return $model->asignado?$model->asignado->full_name:"-";
                })
                ->addColumn('status', function($model) {
                    return "<span class='badge'>". ($model->status?$model->status->name:"-") . "</span>";
                })
                ->showColumns('rating')
                ->addColumn('categoria', function($model) {
                    return $model->categoria?$model->categoria->name:"-";
                })
                ->addColumn('options', function($model) {

                    $ret = "";


                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('fecha','categoria')
                ->orderColumns('fecha','rating')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        // return view('manage.solicitudes.index.viajero', compact('viajero_id'));
    }

    public function getNuevo(Request $request, $viajero_id)
    {
        //Reseteamos checklist del viajero
        //es_cliente = false

        $viajero = $this->viajero->find($viajero_id);
        $solicitud = $viajero->solicitud;

        if($solicitud && !$solicitud->es_inscrito)
        {
            $solicitud->archivar('AUTO','NUEVA SOLICITUD');
        }

        //Desconectamos el booking
        $viajero->booking_id = 0;
        $viajero->es_cliente = false;
        $viajero->booking_status_id = 0;
        $viajero->status_id = 0;
        $viajero->save();

        $viajero->setStatus(1, 'Booking 0');

        //Creamos la nueva:
        $data['viajero_id'] = $viajero_id;
        $data['status_id'] = 0;
        $data['rating'] = $viajero->rating;
        $data['fecha'] = Carbon::now();
        $data['user_id'] = $request->user()->id;
        $data['origen_id'] = $viajero->origen_id;
        $data['suborigen_id'] = $viajero->suborigen_id;
        $data['suborigendet_id'] = $viajero->suborigendet_id;
        $o = $this->solicitud->create($data);

        $o->setStatus(ConfigHelper::config('solicitud_status_lead'));

        $viajero->solicitud_id = $o->id;
        $viajero->save();

        ViajeroLog::addLog($viajero, "Solicitud nueva [". $o->id ."]");

        Session::flash('tab','#solicitud');

        return redirect()->route('manage.viajeros.ficha', $viajero_id);
    }

    public function getUpdate($id)
    {
        $ficha = $this->solicitud->find($id);

        Session::flash('tab','#solicitud');

        return redirect()->route('manage.viajeros.ficha', $ficha->viajero_id);
    }

    public function postUpdate(Request $request, $id)
    {
        $data = $request->except('_token');
        $data['fecha'] = Carbon::createFromFormat('d/m/Y',$request->input('fecha'))->format('Y-m-d');

        $ficha = $this->solicitud->find($id);
        $fichaOld = clone $ficha;

        $catenvio = $request->has('catalogo_envio')?:0;
        $catenviado = $request->has('catalogo_enviado')?:0;
        $catenviado = $catenvio ? $catenviado : 0;

        $viajero = $ficha->viajero;
        $data['origen_id'] = $viajero->origen_id;
        $data['suborigen_id'] = $viajero->suborigen_id;
        $data['suborigendet_id'] = $viajero->suborigendet_id;
        $data['catalogo_envio'] = $catenvio;
        $data['catalogo_enviado'] = $catenviado;
        $ficha->update($data);

        $viajero = $ficha->viajero;
        $viajero->rating = $request->input('rating');
        $viajero->save();

        //catalogo_envio, catalogo_enviado
        $user = $request->user();
        if($fichaOld->catalogo_envio != $catenvio)
        {
            $log = "Envio catalogo [". ($catenvio ? 'ON':'OFF') ."]";
            ViajeroLog::addLog($viajero, $log);

            $ficha->catalogo_envio_log = "";
            if($catenvio)
            {
                $fecha = Carbon::now()->format('d/m/Y H:i');
                $log = "[$user->full_name : $fecha ]";
                $ficha->catalogo_envio_log = $log;

                MailHelper::mailEnvioCatalogo($ficha);
            }
            $ficha->save();
        }

        if($fichaOld->catalogo_enviado != $catenviado)
        {
            $log = "Catálogo enviado [". ($catenviado ? 'ON':'OFF') ."]";
            ViajeroLog::addLog($viajero, $log);

            $ficha->catalogo_enviado_log = "";
            if($catenviado)
            {
                $fecha = Carbon::now()->format('d/m/Y H:i');
                $log = "[$user->full_name : $fecha ]";
                $ficha->catalogo_enviado_log = $log;
            }
            $ficha->save();
        }

        Session::flash('tab','#solicitud');
        return redirect()->route('manage.viajeros.ficha', $ficha->viajero_id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->solicitud->delete($id);

        return redirect()->route('manage.solicitudes.index', [0]);
    }

    public function ajaxChecklist(Request $request, $id)
    {
        $solicitud = $this->solicitud->find($id);
        $checkid = $request->input('check_id');
        // $estado = $request->input('estado');
        $fecha = $request->input('check_fecha');
        $user_id = $request->user()->id;

        $s = $solicitud->setChecklist($checkid,$fecha,$user_id);

        $user = $s->user->full_name;
        $status_id = $solicitud->status_id;

        $result = ['id'=> $id, 'check_id'=> $checkid, 'result'=> $s->estado, 'status'=> $status_id, 'fecha'=> $fecha, 'user'=> $user ];
        return response()->json($result, 200);
    }

    public function ajaxGetChecklist(Request $request, $id)
    {
        $solicitud = $this->solicitud->find($id);

        // $checklist = $solicitud->status->checklist;
        $checklist = $solicitud->status->getChecklistSolicitud($id);
        foreach($checklist as $check)
        {
            $estado = $solicitud->viajero->getStatusChecklistEstado($check->id);
            $check['check'] = $estado;

            $estado = $solicitud->viajero->getStatusChecklistClass($check->id);
            $check['class'] = $estado;
        }

        $result = ['id'=> $id, 'checklist'=> $checklist, 'booking'=> false, 'titulo'=> $solicitud->name, 'result'=> true ];
        return response()->json($result, 200);
    }

    public function ajaxStatus(Request $request, $id)
    {
        $solicitud = $this->solicitud->find($id);
        $status = $request->input('status');

        $solicitud->setStatus($status);

        ViajeroLog::addLog($solicitud->viajero, "Solicitud [$id] cambio estado [$status]");

        if($solicitud->viajero->solicitud_id == $id)
        {
            $solicitud->viajero->setStatus($status,$request->input('motivo'),$request->input('motivo_nota'));
        }

        $result = ['id'=> $id, 'result'=> true ];
        return response()->json($result, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getIndex(Request $request)
    {
        $valores = ConfigHelper::paramsFromRequest($request);
        $array = ConfigHelper::arrayFromRequest($request);

        if($array['anys'] == 0)
        {
            $array['anys'] = Carbon::now()->year;
            $user_id = (int)$request->user()->id;
            $oficina_id = null;

            $user = auth()->user();
            if(!$user->filtro_oficinas)
            {
                $oficina_id = $user->oficina_id;
            }

            if($oficina_id)
            {
                $array['oficinas'][] = $oficina_id;
            }
            $array['usuarios'][] = $user_id;

            $valores['anys'] = $array['anys'];
            $valores['oficinas'] = [];
            $valores['oficinas'][] = "u-". $user_id;
            //$valores['oficinas'][] = "o-1";
            //$valores['oficinas'][] = $oficina_id ? "o-$oficina_id" : null;
        }

        //dd($array);

        $query = ConfigHelper::queryFromArray($array, "solicitudes");
        //dd($query);

        $statuses = Status::orderBy('orden')->get();

        $statuses_total = [];
        $statuses_total[0] = 0;

        foreach($statuses as $status)
        {
            $query2 = clone $query;
            //$res['st_'. $st->id] = $query2->where('status_id', $status->id)->pluck('id')->toArray();

            $iSt = $query2->where('solicitudes.status_id', $status->id)->count();
            $statuses_total[$status->id] = $iSt;

            if($status->orden>0)
            {
                $statuses_total[0] += $statuses_total[$status->id];
            }
        }

        //$valores = json_encode($valores);

        return view('manage.solicitudes.index_filtros', compact('valores', 'statuses', 'statuses_total') );
    }

    /**
     * @param Request $request
     */
    public function dttFiltros(Request $request)
    {
        $array = ConfigHelper::arrayFromRequest($request, false);

        $query = ConfigHelper::queryFromArray($array, 'solicitudes');

        $query = $query->select(
                'solicitudes.id',
                'solicitudes.rating as rating',
                'statuses.name as status',
                'solicitudes.fecha as fecha',
                DB::raw("CONCAT(viajeros.name,' ',viajeros.lastname) as viajero"),
                'viajero_origenes.name as origen',
                'categorias.name as categoria',
                DB::raw('(SELECT updated_at FROM viajero_logs WHERE viajero_id = viajeros.id AND tipo<>"" AND tipo<>"log" ORDER BY updated_at DESC LIMIT 1) as ultimo')
            )
            ->join('viajeros','solicitudes.viajero_id','=','viajeros.id')
            ->leftJoin('viajero_origenes','viajero_origenes.id','=','viajeros.origen_id')
            ->leftJoin('categorias','categorias.id','=','solicitudes.category_id')
            ->leftJoin('statuses','statuses.id','=','solicitudes.status_id');

        $status_id = $request->get('status_id') ?: 0;
        if(!$status_id)
        {
            $st = Status::where('orden','>',0)->pluck('id');
            $query = $query->whereIn('solicitudes.status_id', $st);
        }
        else
        {
            $query = $query->where('solicitudes.status_id', $status_id);
        }

        if(Datatable::shouldHandle())
        {
            return Datatable::query( $query )
                ->showColumns('fecha')
                ->addColumn('viajero', function($column) {
                    $model = SolicitudModel::find($column->id);
                    $name = $model->viajero->full_name;
                    return "<a href='". route('manage.viajeros.ficha',[$model->viajero->id]) ."'>$name</a>";
                })
                ->addColumn('categoria', function($column) {
                    $model = SolicitudModel::find($column->id);
                    return $model->categoria?$model->categoria->name:"-";
                })
                ->addColumn('origen', function($column) {
                    $model = SolicitudModel::find($column->id);
                    return $model->origen_full_name;
                })
                ->addColumn('status', function($column) {
                    $model = SolicitudModel::find($column->id);

                    $ret = "<span class='badge badge-warning'>". ($model->status?$model->status->name:"-") . "</span>&nbsp;";

                    if(!$model->status)
                        $ret = "<span class='badge'>Archivado</span>&nbsp;";

                    return $ret;
                })
                ->showColumns('rating')
                ->addColumn('asignado', function($column) {
                    $model = SolicitudModel::find($column->id);
                    return $model->asignado?$model->asignado->full_name:"-";
                })
                ->addColumn('ultimo', function($column) {
                    return substr($column->ultimo,0,10);
                })
                ->addColumn('tarea', function($column) {
                    $model = SolicitudModel::find($column->id);
                    $t = $model->viajero->tareas->where('estado',0)->count();
                    return $t>0?"Si [$t]":"No";
                })
                ->addColumn('options', function($column) {
                    $model = SolicitudModel::find($column->id);

                    $ret = "";

                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('viajeros.name','viajeros.lastname','viajeros.lastname2')
                // ->orderColumns('fecha','viajero','rating','categoria','name','lastname','lastname2','contacto','tarea')
                ->orderColumns('fecha','ultimo','viajero','rating','origen','categoria','status')
                ->setAliasMapping()
                ->make();
        }
        
    }


    public function setCatalogo(Request $request, SolicitudModel $solicitud)
    {
        $solicitud->setCatalogo($request->user());

        Session::flash('mensaje-ok', 'Solicitud marcada como Catálogo enviado');

        return redirect()->back();
    }
}
