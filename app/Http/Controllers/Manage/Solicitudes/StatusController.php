<?php

namespace VCN\Http\Controllers\Manage\Solicitudes;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Solicitudes\Status;
use VCN\Models\System\SystemLog;

use Datatable;
use Input;

class StatusController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->checkPermisosFullAdmin();
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection( Status::all()->sortBy('orden') )
                ->showColumns('orden')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.solicitudes.status.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Status Lead' data-action='". route( 'manage.solicitudes.status.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('orden','name')
                ->orderColumns('*')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.solicitudes.status.index');
    }

    public function getNuevo()
    {
        return view('manage.solicitudes.status.new');
    }

    public function getUpdate($id)
    {
        $ficha = Status::find($id);

        return view('manage.solicitudes.status.ficha', compact('ficha'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'orden'   => 'required|min:1',
            'name' => 'required',
        ]);

        $o = Status::find($id);

        if(!$id)
        {
            $o = new Status;

            $o->orden = $request->input('orden');
            $o->name = Input::get('name');
            $o->save();

            SystemLog::addLog($o,'Nuevo');
        }
        else
        {
            $o1 = Status::find($id);

            $o->orden = $request->input('orden');
            $o->name = Input::get('name');
            $o->save();

            SystemLog::addLog($o,'Modificado',$o1);
        }

        return redirect()->route('manage.solicitudes.status.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $o = Status::find($id);
        $o->delete();

        return redirect()->route('manage.solicitudes.status.index');
    }
}
