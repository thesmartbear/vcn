<?php

namespace VCN\Http\Controllers\Manage\Traducciones;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Traducciones\TraduccionRepository as Traduccion;
use VCN\Models\Traducciones\Traduccion as TraduccionModel;

use Session;

class TraduccionesController extends Controller
{
    private $traduccion;

    /**
     * Instantiate a new TraduccionesController instance.
     *
     * @return void
     */
    public function __construct( Traduccion $traduccion )
    {
        $this->checkPermisos('traducciones');

        $this->traduccion = $traduccion;
    }

    public function postUpdate(Request $request, $idioma, $modelo, $id)
    {
        $data = $request->except('_token');

        $traduccion = $this->traduccion->findWhere([
                ['idioma',$idioma],
                ['modelo',$modelo],
                ['modelo_id',$id]
            ]);

        $datos = [];
        foreach($data as $iCampo=>$vCampo)
        {
            $vCampo = $vCampo ?: "";

            if( $request->hasFile($iCampo) )
            {
                $file = $request->file($iCampo);

                $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file_name = str_slug($file_name) .".". $file->getClientOriginalExtension();
                $dirp = "assets/uploads/$modelo/". $id . "/$idioma/";
                $dir = public_path($dirp);

                $file->move($dir, $file_name);

                $vCampo = "/".$dirp.$file_name;
            }

            $campo = str_replace($idioma.'_', '', $iCampo);
            $datos[$campo] = $vCampo;

            $t = $traduccion->where('campo',$campo)->first();

            if($t) //udpate
            {
                $t->traduccion = $vCampo;
                $t->save();
            }
            else
            {
                $new = new TraduccionModel;
                $new->idioma = $idioma;
                $new->modelo = $modelo;
                $new->modelo_id = $id;
                $new->campo = $campo;
                $new->traduccion = $vCampo;

                $new->save();
            }


        }

        Session::flash('tab','#traduccion');
        return redirect()->back();

    }
}
