<?php

namespace VCN\Http\Controllers\Manage\Monedas;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Monedas\Moneda;
use VCN\Models\System\SystemLog;

use Datatable;
use ConfigHelper;
use Session;

class MonedasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection( Moneda::all() )
                ->addColumn('currency_name', function($model) {
                    return "<a href='". route('manage.monedas.ficha',[$model->id]) ."'>$model->currency_name</a>";
                })
                ->showColumns('currency_rate')
                ->searchColumns('currency_name')
                ->orderColumns('currency_name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.monedas.index');
    }

    public function getNuevo()
    {
        return view('manage.monedas.new');
    }

    public function getUpdate($id)
    {
        $ficha = Moneda::find($id);

        return view('manage.monedas.ficha', compact('ficha'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'currency_name' => 'required',
            'currency_rate' => 'required',
        ]);

        $p = Moneda::find($id);

        ini_set('memory_limit', '600M');
        set_time_limit(0);

        if(!$id)
        {
            $p = new Moneda;

            $p->currency_name = $request->get('currency_name');
            $p->currency_rate = $request->get('currency_rate');
            $p->save();

            SystemLog::addLog($p,'Nuevo');
        }
        else
        {
            $p1 = Moneda::find($id);

            $p->currency_name = $request->get('currency_name');
            $p->currency_rate = $request->get('currency_rate');
            $p->save();

            SystemLog::addLog($p,'Modificado',$p1);

            //Bookings:
            $i = 0;

            $st = [];
            $st[] = ConfigHelper::config('booking_status_prebooking');
            $st[] = ConfigHelper::config('booking_status_presalida');

            //setMonedasCambio
            $bookings = \VCN\Models\Bookings\Booking::whereIn('status_id', $st)->where('pagado', 0);
            foreach( $bookings->get() as $booking )
            {
                if($booking && ($booking->es_notapago35_generada || $booking->facturas->count()) )
                {
                    if( !$booking->monedas->count() )
                    {
                        $booking->setMonedasCambio();
                    }
                }
            }

            $bookings = \VCN\Models\Bookings\Booking::whereIn('status_id', $st)->where('pagado', 0);
            foreach( $bookings->get() as $booking )
            {
                if($booking && $booking->es_notapago35_generada)
                {
                    if( !$booking->monedas->count() )
                    {
                        $mu = $booking->monedas_usadas;
                        if( in_array($id, $mu) )
                        {
                            $i += $booking->notaPago35update("(cambio divisa)");
                        }
                    }
                }
            }

            if($i)
            {
                Session::flash('mensaje', "Nota de pago actualizada en $i Bookings.");
            }
        }

        return redirect()->route('manage.monedas.index');
    }

    public function destroy($id)
    {
        $ficha = Moneda::findOrFail($id);

        $ficha->delete();

        return redirect()->route('manage.monedas.index');
    }
}
