<?php

namespace VCN\Http\Controllers\Manage\Monedas;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Monedas\Cambio;
use VCN\Models\Monedas\CambioMoneda;
use VCN\Models\Convocatorias\Cerrada;
use VCN\Models\Cursos\Curso;

use VCN\Models\Categoria;

use Datatable;
use ConfigHelper;
use Session;
use Carbon;

class CambiosController extends Controller
{

    public function getIndex(Request $request, $category_id=0)
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection( Cambio::plataforma()->get() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.monedas.cambios.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('categorias', function($model) {

                    $cats = explode(',',$model->categorias);

                    $ret = "";
                    foreach($cats as $cat)
                    {
                        $c = Categoria::find($cat);
                        if($c)
                        {
                            $ret .= "<a href='". route('manage.categorias.ficha',[$cat]) ."'>". $c->name ."</a><br>";
                        }
                    }

                    return $ret;
                })
                ->addColumn('activo', function($model) {
                    return $model->activo?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('plataforma', function($model) {
                    return ConfigHelper::plataforma($model->plataforma);
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Tabla Cambio' data-action='". route( 'manage.monedas.cambios.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.monedas.cambios.index');
    }

    public function getNuevo()
    {
        $categorias = Categoria::plataforma()->pluck('name','id');

        return view('manage.monedas.cambios.new', compact('categorias'));
    }

    public function getUpdate($id)
    {
        $ficha = Cambio::findOrFail($id);

        $categorias = Categoria::plataforma()->pluck('name','id');

        return view('manage.monedas.cambios.ficha', compact('ficha','categorias'));
    }

    public function postUpdate( Request $request, $id )
    {
        $data = $request->input();
        $data['categorias'] = $request->has('categorias')?implode(',', $request->get('categorias')):null;

        if(!$id)
        {
            $this->validate($request, [
                'name' => 'required|unique:cambios|max:255',
            ]);

            $ficha = Cambio::create($data);

            $id = $ficha->id;
        }

        $ficha = Cambio::findOrFail($id);
        $ficha->update($data);

        return redirect()->route('manage.monedas.cambios.ficha', $id);
    }

    public function destroy($id)
    {
        $ficha = Cambio::findOrFail($id);

        $ficha->delete();

        return redirect()->route('manage.monedas.cambios.index');
    }

    public function postMonedas( Request $request, $id )
    {
        Session::flash('tab','#monedas');

        $ficha = Cambio::findOrFail($id);
        $ficha->save();// Para Actualizar el update_at

        $cambios = $request->input('cambios');

        if(!isset($cambios['moneda_id']))
        {
            return redirect()->route('manage.monedas.cambios.index');
        }

        $i = 0;
        foreach($cambios['moneda_id'] as $m)
        {
            $cm = $ficha->cambios->where('moneda_id',(int)$m)->first();

            if(!$cm)
            {
                $cm = new CambioMoneda;
                $cm->cambio_id = $id;
                $cm->moneda_id = $m;
                $cm->save();
            }

            $cm->tc_calculo = $cambios['tc_calculo'][$i];
            $cm->tc_folleto = $cambios['tc_folleto'][$i];
            $cm->tc_final = $cambios['tc_final'][$i];
            $cm->save();

            $i++;
        }

        //Precio convos cerradas
        $iCC = 0;
        $categorias = explode(',',$ficha->categorias);

        if($ficha->any)
        {
            $any = Carbon::parse($ficha->any."-01-01");

            foreach($categorias as $cat)
            {
                $cursos = Curso::where('category_id',$cat)->pluck('id')->toArray();
                $convocatorias = Cerrada::whereIn('course_id',$cursos)->get();
                foreach($convocatorias as $c)
                {
                    $start = Carbon::parse($c->convocatory_close_start_date);
                    if($start->gte($any))
                    {
                        if($c->precio_auto)
                        {
                            $c->updatePrecio();

                            $iCC++;
                        }
                    }
                }
            }
        }

        if($iCC)
        {
            Session::flash('mensaje',"Precio de $iCC convocatorias actualizado. Año inicio curso: $ficha->any");
        }

        Session::flash('tab','#monedas');
        return redirect()->back();

    }
}
