<?php

namespace VCN\Http\Controllers\Manage;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Pais;
use VCN\Models\Nacionalidad;
use VCN\Models\Provincia;
use VCN\Models\Ciudad;
use VCN\Models\Monedas\Moneda;
use VCN\Models\Categoria;
use VCN\Models\Subcategoria;
use VCN\Models\SubcategoriaDetalle;
use VCN\Models\Extras\ExtraUnidad;
use VCN\Models\System\SystemLog;
use VCN\Models\User;
use VCN\Models\Especialidad;
use VCN\Models\Subespecialidad;

use VCN\Models\Convocatorias\Abierta as ConvocatoriaAbierta;
use VCN\Models\Convocatorias\Cerrada as ConvocatoriaCerrada;
use VCN\Models\Convocatorias\ConvocatoriaMulti;
use VCN\Models\Alojamientos\Alojamiento;
use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Bookings\BookingDescuento;

use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\Tutor;
use VCN\Repositories\Leads\TutorRepository as TutorRepo;
// use VCN\Models\Leads\ViajeroLog;
use VCN\Models\Leads\Origen;
// use VCN\Models\Leads\Suborigen;
// use VCN\Models\Leads\SuborigenDetalle;
use VCN\Models\Solicitudes\Solicitud;
use VCN\Models\Solicitudes\Status as SolicitudStatus;

use VCN\Repositories\Leads\ViajeroLogRepository as ViajeroLog;
use VCN\Repositories\Leads\ViajeroTareaRepository as ViajeroTarea;
use VCN\Repositories\Criteria\FiltroPlataformaAsignado;
use VCN\Repositories\Criteria\FiltroPlataformaViajero;

use VCN\Models\Cursos\Curso;
use VCN\Models\Centros\Centro;
use VCN\Models\Centros\CentroDescuento;
use VCN\Models\System\Oficina;
use VCN\Models\Informes\Venta;
use VCN\Models\Informes\VentaCurso;
use VCN\Models\System\Cuestionario;
use VCN\Models\System\Plataforma;
use VCN\Models\Exams\Examen;

// use Vinkla\Instagram\Instagram;

use Datatable;
use Input;
use View;
use Carbon;
use App;
use DB;
use Session;
use Mail;
use Log;

use Config;
use Blade;
use PDF;
use File;
use Image;
use Lava;
use Artisan;
use Hash;

use VCN\Helpers\ConfigHelper;
use VCN\Helpers\MailHelper;
use VCN\Helpers\Graficos;

use VCN\Jobs\JobTest;
use setasign\Fpdi;

use Chat;

class ManageController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Fpdi\PdfParser\CrossReference\CrossReferenceException
     * @throws Fpdi\PdfParser\Filter\FilterException
     * @throws Fpdi\PdfParser\PdfParserException
     * @throws Fpdi\PdfParser\Type\PdfTypeException
     * @throws Fpdi\PdfReader\PdfReaderException
     */
    public function testPost(Request $request)
    {
        dd("testPost");

        $b641 = $request->input('signature_b64_1');

        $pic = $b641;

        // $b = Booking::first();
        // $firmas = [];
        // $firmas['viajero'] = $b641;
        // $b->firmas = $firmas;
        // $b->save();

        $fichero = public_path('assets/uploads/categorias/2/1/es/condiciones-jovenes-bs-2018.pdf');

        $pdf = new Fpdi\Fpdi();
        // $pdf->AddPage();
        
        $pagesTot = $pdf->setSourceFile($fichero);
        for ($iPage = 1; $iPage <= $pagesTot; $iPage++) {
            // import a page
            $tplId = $pdf->importPage($iPage);

            $pdf->AddPage();
            $pdf->useTemplate($tplId, ['adjustPageSize' => true]);

            // $pdf->Image(public_path('assets/logos/pocketguide-logo.jpg'),130,245, 40);
            $pdf->Image($pic, 130, 245, 70, null, 'png');

            // $pdf->SetFont('Helvetica');
            // $pdf->SetXY(5, 5);
            // $pdf->Write(8, 'A complete document imported with FPDI');
        }

        $pdf->Output();
        
    }
    
    private function testJobs()
    {
        $destino = User::find(1);
        //MailHelper::enviar("web.curso", $destino, null);
        //return;

        $template = "emails.test";
        $data = [];
        $asunto = "Testeando Queues...";

        $baseUrl = config('app.url');

        $p = $destino->plataforma ?: ConfigHelper::config('propietario');
        if($p)
        {
            $plataforma = \VCN\Models\System\Plataforma::find($p);
            $baseUrl = $plataforma->area_url;
        }

        Mail::to($destino)
            ->queue( new \VCN\Mail\MailTestMarkdown() );
        return;

        $data = $data + ['base_url'=> $baseUrl];
        
        $job = new \VCN\Jobs\JobMailer($template, $data, $destino, $asunto);
        dispatch($job);
    }

    /**
     * @param Request $request
     */
    public function test(Request $request)
    {
        // dd( $_COOKIE );
        // dd( ConfigHelper::isCookie('marketing')  );
        
        // $b = Booking::find(13768);
        // \VCN\Models\Informes\Venta::add($b);

        echo "mail 1..";
        Mail::send('emails.test', [], function ($m) {

            // $m->from('loic@britishsummer.com');

            $now = Carbon::now()->format("d/m/Y H:i");
            $subject =  "Test mail $now";

            $destino = User::find(1);
            $m->to($destino->email, $destino->full_name)->subject($subject);

            // $destino = 'cfisher@iccic.edu';
            // $m->to($destino, $destino)->subject($subject);
        });

        // return view('manage.test');

        // $routeCollection = \Route::getRoutes();

        // foreach ($routeCollection as $value) {
        //     echo $value->getPath();
        // }

        // $fecha = "13/08/2018";
        // $weeks = 2;
        // $ca = $ficha->convocatoria;
        // $fechafin = Carbon::createFromFormat( 'd/m/Y', $fecha );
        // $fechafin = $ficha->calcularFechaFin($fechafin,$weeks, $ca->convocatory_open_start_day, $ca->convocatory_open_end_day);
        // dd($fechafin);

        // $ficha = Booking::find(8828);
        // $fecha = "13/01/2019";
        // $fecha = Carbon::createFromFormat( 'd/m/Y', $fecha );        
        // $weeks = 12;

        // echo $ficha->alojamiento->start_day ." : ". $ficha->alojamiento->end_day;

        // $fechafin = $ficha->calcularFechaFin($fecha,$weeks,$ficha->alojamiento->start_day, $ficha->alojamiento->end_day, false);
        // dd($fechafin);


        //self::testJobs();
        
        // dd(phpinfo());

        dd(config('app.timezone'));

        dd("test");

        // Mail::send('emails.test', [], function ($m) {

        //     // $m->from('system@estudiaryviajar.com', "VCN");

        //     $now = Carbon::now()->format("d/m/Y H:i");
        //     $subject =  "Test mail $now";

        //     $destino = User::find(17);

        //     $m->to($destino->email, $destino->full_name)->subject($subject);
        // });

        dd("test");

        //api mailgun
        /*# Instantiate the client.
        $client = new \Http\Adapter\Guzzle6\Client();
        $mgClient = new \Mailgun\Mailgun('key-8410991e28f77a4dc915f7db0e9b18a3', $client);
        // $mgClient = new Mailgun('key-8410991e28f77a4dc915f7db0e9b18a3');
        $domain = 'estudiaryviajar.com'; // http://bin.mailgun.net/14e8b700
        $queryString = array(
            'begin'        => 'Fri, 3 May 2016 09:00:00 -0000',
            'ascending'    => 'yes',
            'limit'        =>  25,
            'pretty'       => 'yes',
            'recipient'    => 'loic@britishsummer.com'
        );

        # Make the call to the client.
        $result = $mgClient->get("$domain/events", $queryString);
        dd($result);*/

    }

    public function test_bi()
    {
        DB::enableQueryLog();

        // $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        $stplazas = [1,3,4,5,6];

        $query = DB::table('solicitudes');
        $query = $query->select(
                'bookings.id as IdInscripcion',
                DB::raw("null as Tipo"),
                'solicitudes.fecha as fechaPeticion',
                'bookings.fecha_prereserva as fechaPrereserva',
                'bookings.fecha_pago1 as fechaReserva',
                'statuses_booking.name as Estado',
                'viajeros.name as Nom',
                'viajeros.id as IdCliente',
                'viajeros.origen_id as IdConociste',
                DB::raw("YEAR(CURDATE())-YEAR(viajeros.fechanac) AS Edad"),
                'viajero_origenes.name as Conociste',
                'viajeros.lastname as Apellido1',
                'viajeros.lastname2 as Apellido2',
                'viajeros.fechanac as Data_naix',
                'bookings.plataforma as IdEmpresa',
                'plataformas.name as Empresa',

                // 'bookings.oficina_id as IdOficina',
                // 'oficinas.name as Oficina',

                DB::raw("CASE
                    WHEN (bi_oficinas.oficina_id>0) THEN bi_oficinas.bi_oficina_id
                    ELSE bookings.oficina_id
                    END as IdOficina"),

                DB::raw("CASE
                    WHEN (bi_oficinas.oficina_id>0) THEN bi_oficinas.bi_name
                    ELSE oficinas.name
                    END as Oficina"),

                'bookings.prescriptor_id as IdPrescriptor',
                'prescriptores.name as Prescriptor',
                'bookings.user_id as IdAgente',
                DB::raw("CONCAT(fname,' ',lname) as NombreAgente"),
                'cursos.center_id as IdCentro',
                'centros.name as NombreCentro',
                'bookings.curso_id as IdCurso',
                'cursos.course_name as NombreCurso',

                DB::raw("CASE
                    WHEN (cursos.category_id=4) THEN 3
                    WHEN (cursos.category_id=6) THEN 3
                    WHEN (cursos.category_id=8) THEN 7
                    WHEN (cursos.category_id=7) THEN 7
                    WHEN (cursos.category_id=5) THEN 10
                    WHEN (cursos.category_id=3) THEN 5
                    WHEN (cursos.category_id=1) THEN 7
                    WHEN (cursos.category_id=2 AND cursos.subcategory_id=8) THEN 1
                    WHEN (cursos.category_id=2 AND cursos.subcategory_id=10) THEN 8
                    ELSE cursos.category_id
                    END as IdTipoCurso"),

                // DB::raw("CASE
                //     WHEN (cursos.category_id=4) THEN 'CAMPAMENTOS'
                //     WHEN (cursos.category_id=6) THEN 'CAMPAMENTOS'
                //     WHEN (cursos.category_id=8) THEN 'ADULTO'
                //     WHEN (cursos.category_id=7) THEN 'ADULTO'
                //     WHEN (cursos.category_id=5) THEN 'GRUPO ESCOLAR'
                //     WHEN (cursos.category_id=3) THEN 'AÑO ACADEMICO'
                //     WHEN (cursos.category_id=1) THEN 'ADULTO'
                //     WHEN (cursos.category_id=2 AND cursos.subcategory_id=8) THEN 'JOVEN INDIVIDUAL'
                //     WHEN (cursos.category_id=2 AND cursos.subcategory_id=10) THEN 'JOVENES'
                //     ELSE cursos.category_id
                //     END as TipoCurso"),

                // DB::raw("CASE
                //     WHEN (bi_subcategorias.subcategory_id>0) THEN bi_subcategorias.bi_category_id
                //     WHEN (bi_categorias.category_id>0) THEN bi_categorias.bi_category_id
                //     ELSE cursos.course_name
                //     END as IdTipoCurso"),

                // DB::raw("CASE
                //     WHEN (bi_subcategorias.subcategory_id>0) THEN bi_subcategorias.bi_name
                //     WHEN (bi_categorias.category_id>0) THEN bi_categorias.bi_name
                //     ELSE cursos.course_name
                //     END as TipoCurso"),

                // 'categorias.id as IdTipoCurso',
                'categorias.name as TipoCurso',

                'cursos.course_language as IdiomaCurso',
                'cursos.category_id as IdCategoria',
                'categorias.name as CategoriaCurso',
                'bookings.course_total_amount as PrecioCursoFinal',
                'bookings.course_total_amount as PrecioCursoInicial',
                DB::raw("CASE
                    WHEN (bookings.convocatory_close_id>0) THEN bookings.convocatory_close_id
                    WHEN (bookings.convocatory_open_id>0) THEN bookings.convocatory_open_id
                    WHEN (bookings.convocatory_multi_id>0) THEN bookings.convocatory_multi_id
                    END as IdConvocatoria"),

                DB::raw("CASE
                    WHEN (bookings.convocatory_close_id>0) THEN convocatoria_cerradas.convocatory_close_name
                    WHEN (bookings.convocatory_open_id>0) THEN convocatoria_abiertas.convocatory_open_name
                    WHEN (bookings.convocatory_multi_id>0) THEN convocatoria_multis.name
                    END as NombreConvocatoria"),
                'bookings.weeks as NumSemanas',
                DB::raw("CASE
                    WHEN (bookings.accommodation_id>0) THEN true
                    WHEN (bookings.accommodation_id=0) THEN false
                    WHEN (bookings.accommodation_id IS NULL) THEN false
                    END as NoQuiereAlojamiento"),
                DB::raw("CASE
                    WHEN (bookings.fecha_reserva IS NULL) THEN false
                    ELSE true
                    END as TengoReserva"),
                DB::raw("null as Reserva"),
                'bookings.transporte_no as NoQuiereTransporte',
                'bookings.transporte_otro as QuiereOtroTransporte',
                DB::raw("null as TramitandoReserva"),
                DB::raw("null as DiaReserva"),
                DB::raw("null as TotalCostes"),
                DB::raw("null as TotalMargen"),
                'bookings.total as TotalInscripcion',
                DB::raw("null as TotalCostesFinales"),
                DB::raw("null as DiferenciaCostes"),
                'bookings.total as TotalAPagar',
                'bookings.center_discount_amount as Descuento',
                'viajero_origenes.name as Origen',
                DB::raw("null as TipusCataleg"),
                'viajero_datos.cp as CP',
                'viajero_datos.ciudad as Poblacion',
                'viajero_datos.nacionalidad as Nacionalitat',
                'viajero_datos.idioma_contacto as IdiomaContacte',
                'viajero_datos.provincia_id as IdProv',
                'provincias.name as Provincia',
                'viajero_datos.pais_id as IdPais',
                'paises.name as Pais',
                'bookings.course_start_date as FechaInicio',
                DB::raw("YEAR(bookings.fecha_pago1) as AñoFechaReserva"),
                DB::raw("MONTH(bookings.fecha_pago1) as MesFechaReserva"),
                DB::raw("DAY(bookings.fecha_pago1) as DiaFechaReserva"),
                DB::raw("WEEK(bookings.fecha_pago1) as SemanaFechaReserva"),

                DB::raw("YEAR(bookings.fecha_pago1) as AñoFechaReservaReal"),
                DB::raw("MONTH(bookings.fecha_pago1) as MesFechaReservaReal"),
                DB::raw("DAY(bookings.fecha_pago1) as DiaFechaReservaReal"),
                DB::raw("WEEK(bookings.fecha_pago1) as SemanaFechaReservaReal"),
                DB::raw("DAYOFWEEK(bookings.fecha_pago1) as DiaSemanaFechaReservaReal"),

                'bookings.created_at as FechaAlta',
                DB::raw("YEAR(bookings.created_at) as AñoFechaAlta"),
                DB::raw("MONTH(bookings.created_at) as MesFechaAlta"),
                DB::raw("DAY(bookings.created_at) as DiaFechaAlta"),
                DB::raw("WEEK(bookings.created_at) as SemanaFechaAlta"),

                DB::raw("null as AñoFechaAltaReal"),
                DB::raw("null as MesFechaAltaReal"),
                DB::raw("null as DiaFechaAltaReal"),
                DB::raw("null as SemanaFechaAltaReal"),
                DB::raw("null as DiaSemanaFechaAltaReal"),
                DB::raw("null as FechaAnula"),

                // 'viajeros.sexo as Sexo',
                DB::raw("CASE
                    WHEN (viajeros.sexo='') THEN 3
                    WHEN (viajeros.sexo>0) THEN viajeros.sexo
                    END as Sexo"),

                // DB::raw("null as PaisCurso"),
                'paisesc.name as PaisCurso',
                'centros.country_id as IdPaisCurso',

                'prescriptores.categoria_id as IdCatPrescriptor',
                'prescriptor_categorias.name as descCatPrescriptor',
                DB::raw("null as Contar")

            )
            ->join('viajeros','solicitudes.viajero_id','=','viajeros.id')
            ->join('viajero_datos','viajero_datos.viajero_id','=','viajeros.id')
            ->join('bookings','bookings.solicitud_id','=','solicitudes.id')
            ->join('cursos','bookings.curso_id','=','cursos.id')
            ->join('centros','cursos.center_id','=','centros.id')
            ->join('users','bookings.user_id','=','users.id')
            ->join('plataformas','bookings.plataforma','=','plataformas.id')
            ->leftJoin('convocatoria_cerradas','bookings.convocatory_close_id','=','convocatoria_cerradas.id')
            ->leftJoin('convocatoria_abiertas','bookings.convocatory_open_id','=','convocatoria_abiertas.id')
            ->leftJoin('convocatoria_multis','bookings.convocatory_multi_id','=','convocatoria_multis.id')
            ->leftJoin('statuses_booking','bookings.status_id','=','statuses_booking.id')
            ->leftJoin('viajero_origenes','viajeros.origen_id','=','viajero_origenes.id')
            ->join('oficinas','bookings.oficina_id','=','oficinas.id')
            ->leftJoin('categorias','cursos.category_id','=','categorias.id')
            ->leftJoin('prescriptores','bookings.prescriptor_id','=','prescriptores.id')
            ->leftJoin('provincias','viajero_datos.provincia_id','=','provincias.id')
            ->leftJoin('paises','viajero_datos.pais_id','=','paises.id')
            ->leftJoin('paises as paisesc','centros.country_id','=','paisesc.id')
            ->leftJoin('prescriptor_categorias','prescriptores.categoria_id','=','prescriptor_categorias.id')
            // ->leftJoin('bi_categorias','cursos.category_id','=','bi_categorias.category_id')
            // ->leftJoin('bi_categorias as bi_subcategorias','cursos.subcategory_id','=','bi_subcategorias.subcategory_id')
            ->leftJoin('bi_oficinas','bookings.oficina_id','=','bi_oficinas.oficina_id')
            ->whereIn('bookings.status_id', $stplazas);
            // ->groupBy('bookings.id');


        $get = $query->get();//->where('bookings.id','=',55)->get();
        // dd($get);

        // return view('manage.test', compact('get'));

        dd(DB::getQueryLog());
    }


    private $log;
    private $tarea;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct(ViajeroLog $log, ViajeroTarea $tarea)
    {
        $this->middleware("permiso.edit:full-admin", ['only' => ['postUnidadesUpdate','getUnidadesNuevo']]);
        $this->middleware("permiso.view:full-admin", ['only' => ['getUnidades','getUnidadesUpdate']]);

        $this->middleware("permiso.edit:tablas", ['only' => ['postPaisUpdate','getPaisNuevo']]);
        $this->middleware("permiso.view:tablas", ['only' => ['getPaisUpdate']]);

        $this->middleware("permiso.edit:tablas", ['only' => ['postNacionalidadUpdate','getNacionalidadNuevo']]);
        $this->middleware("permiso.view:tablas", ['only' => ['getNacionalidadUpdate']]);

        $this->middleware("permiso.edit:tablas", ['only' => ['postCiudadUpdate','getCiudadNuevo']]);
        $this->middleware("permiso.view:tablas", ['only' => ['getCiudadUpdate']]);

        $this->middleware("permiso.edit:tablas", ['only' => ['postProvinciaUpdate','getProvinciaNuevo']]);
        $this->middleware("permiso.view:tablas", ['only' => ['getProvinciaUpdate']]);

        // $this->middleware("permiso.edit:monedas", ['only' => ['postMonedaUpdate','getMonedaNuevo']]);
        // $this->middleware("permiso.view:monedas", ['only' => ['getMonedaUpdate']]);

        // $this->middleware("permiso.edit:categorias", ['only' => ['postCategoriaUpdate','getCategoriaNuevo']]);
        // $this->middleware("permiso.view:categorias", ['only' => ['getCategoriaUpdate']]);

        // $this->middleware("permiso.edit:subcategorias", ['only' => ['postSubcategoriaUpdate','getSubcategoriaNuevo']]);
        // $this->middleware("permiso.view:subcategorias", ['only' => ['getSubcategoriaUpdate']]);

        // $this->middleware("permiso.edit:subcategorias-detalle", ['only' => ['postSubcategoriaDetUpdate','getSubcategoriaDetNuevo']]);
        // $this->middleware("permiso.view:subcategorias-detalle", ['only' => ['getSubcategoriaDetUpdate']]);

        // $this->middleware("permiso.edit:especialidad", ['only' => ['postEspecialidadUpdate','getEspecialidadNuevo']]);
        // $this->middleware("permiso.view:especialidad", ['only' => ['getEspecialidadUpdate']]);

        // $this->middleware("permiso.edit:subespecialidad", ['only' => ['postSubespecialidadUpdate','getSubespecialidadNuevo']]);
        // $this->middleware("permiso.view:subespecialidad", ['only' => ['getSubespecialidadUpdate']]);

        $this->log = $log;
        $this->tarea = $tarea;
    }

    private static function comprobarInit()
    {
        // $config = env('APP_CONFIG', 'vcn');
        // Session::put( 'vcn.config', $config );
        // Session::put( 'vcn.tema', ConfigHelper::config("tema") );
        // Session::put( 'vcn.moneda', ConfigHelper::config("moneda") );

        $m = Moneda::where('currency_name',Session::get('vcn.moneda'))->first();
        if(!$m)
        {
            Session::flash('mensaje-alert', "Debe configurar la moneda por defecto [". Session::get('vcn.moneda') ."].");
        }

        if(auth()->user()->roleid == 0)
        {
            abort(404);
        }
    }

    private static function serveFile($file)
    {
        $path = storage_path('files/'.$file);

        if(!is_file($path))
        {
            abort(404);
        }

        $mime = mime_content_type($path);

        return response(
            File::get($path), 200, [
                'Content-Type' => $mime,
                'Content-Disposition' => 'inline; '.$path
            ]
        );
    }

    public function serveFileViajeros($viajero_id, $file=null)
    {
        $f = "viajeros/$viajero_id/$file";
        return Self::serveFile($f);
    }


    public function serveFileViajerosBooking($viajero_id, $booking_id, $file=null)
    {
        $f = "viajeros/$viajero_id/$booking_id/$file";
        return self::serveFile($f);
    }

    public function serveFileBookings($viajero_id, $file=null)
    {
        $f = "bookings/$viajero_id/$file";
        return self::serveFile($f);
    }

    public function serveFileFirma(Request $request, $viajero_id, $file=null)
    {
        $device = $request->header('User-Agent');
        if( str_contains($device, "Google AppsViewer") )
        {
            $f = "bookings/$viajero_id/$file";
            return Self::serveFile($f);
        }

        return redirect()->route('file.booking', [$viajero_id, $file]);
    }

    public function serveFileFacturas($file=null)
    {
        $f = "convocatorias/facturas/$file";
        return Self::serveFile($f);
    }


    public function getAutocomplete(Request $request, $list)
    {
        $term = $request->input('term');
        switch($list)
        {
            case 'aeropuertos':
            {
                $ret = \VCN\Models\Aeropuerto::where('name','LIKE',"%$term%")->orWhere('iata','LIKE',"%$term%")->orWhere('city','LIKE',"%$term%")->get();
                return response()->json($ret, 200);
            }
            break;

            case 'aerolineas':
            {
                $ret = \VCN\Models\Aerolinea::where('name','LIKE',"%$term%")->pluck('name');
                return response()->json($ret, 200);
            }
            break;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        self::comprobarInit();

        $user = auth()->user();

        if($user->filtro_oficinas)
        {
            $users = User::asignados()->get();
            $asignados = User::asignados()->get()->pluck('full_name', 'id');
        }
        else
        {
            $users = User::asignados()->where('oficina_id',$user->oficina_id)->get();
            $asignados =  User::asignados()->where('oficina_id',$user->oficina_id)->get()->pluck('full_name', 'id');
        }

        $oficinas = Oficina::plataforma();

        return view('manage.index', compact('users','user','asignados','oficinas'));
    }

    public function getContactos(Request $request)
    {
        ini_set('memory_limit', '400M');
        ini_set('max_execution_time', 0);

        Artisan::call('view:clear');

        // $this->middleware("permiso.informe:manage.index.contactos");
        
        $user = auth()->user();

        $usuarios = [];
        $users = [];
        $valores['users'] = $user->id;

        $valores['desde'] = Carbon::now()->subDays(30)->format('d/m/Y');
        $valores['hasta'] = Carbon::now()->format('d/m/Y');

        $contactos = [];
        $resultados = false;

        if($valores['desde'] && $valores['hasta'])
        {
            $desde = Carbon::createFromFormat('!d/m/Y', $valores['desde']);
            $hasta = Carbon::createFromFormat('!d/m/Y', $valores['hasta']);

            if($desde->lte($hasta))
            {
                $resultados = true;
            }
        }

        if($resultados)
        {
            $usuarios = [];
            $fechas = [];

            $usuarios = \VCN\Models\User::where('id', $valores['users'])->get();
            // dd($usuarios);

            $uId = $usuarios->pluck('id')->toArray();
            $dbContactos = DB::table('viajero_logs')->selectRaw('*, DATE_FORMAT(created_at, "%d/%m/%Y") as fecha, count(*) as contactos')
                ->where('tipo','<>','')->where('tipo','<>','log')
                ->whereIn('user_id', $uId)
                ->whereBetween('created_at', [$desde,$hasta])->groupBy('user_id','viajero_id','fecha')->get()->toArray();

            $chartc1 = Lava::DataTable();
            $chartc1->addStringColumn('Día');
            foreach($usuarios as $u)
            {
                $chartc1->addNumberColumn($u->fname);
            }

            $d = clone $desde;
            $iFechas = $hasta->diffInDays($desde);
            for ($i=0; $i < $iFechas; $i++)
            { 
                $f = $d->format('d/m/Y'); 
                $fechas[] = $f;
                $contactos[$f] = [];

                $row = [];
                $row[] = $f;
                
                $rfechas = array_filter($dbContactos, function($c) use ($f) {
                    return $c->fecha == $f;
                });

                foreach($usuarios as $u)
                {
                    $user_id = $u->id;
                    $ruser = array_filter($rfechas, function($c) use ($user_id) {
                        return $c->user_id == $user_id;
                    });

                    $c = count($ruser);
                    $contactos[$f][$u->id] = $c;

                    $row[] = $c;
                }

                $d->addDay();

                $chartc1->addRow($row);
            }

            Lava::ColumnChart('Chart-Contactos', $chartc1, [
                'title' => 'Contactos',
                'titleTextStyle' => [
                    'color'    => '#eb6b2c',
                    'fontSize' => 14
                ],
                'height'=> 450,
            ]);
        }

        // dd($valores);

        //view
        return view( 'manage.index_contactos',
                compact('users', 'contactos', 'resultados', 'valores', 'usuarios', 'fechas'
                )
            );
    }


    public function getVentas(Request $request)
    {
        ini_set('memory_limit', '400M');

        $this->middleware("permiso.informe:manage.index.ventas");

        $user = $request->user();

        if( $user->isFullAdmin() )
        {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0=>'Todas'] + Oficina::all()->sortBy('name')->pluck('name','id')->toArray();
            $categorias = [0=>'Todas'] + Categoria::all()->sortBy('name')->pluck('name','id')->toArray();
            $subcategorias = [0=>'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name','id')->toArray();
            // $subcategoriasdet = [0=>'Todas'] + SubcategoriaDetalle::all()->sortBy('name')->pluck('name','id')->toArray();

            $valores['plataformas'] = intval($request->input('plataformas', 0));
            $valores['oficinas'] = intval($request->input('oficinas',0));

            $categorias_graf = Categoria::all()->sortBy('name');
        }
        else
        {
            $plataformas = [ 0=>ConfigHelper::plataformaApp() ];

            $oficinas = [0=>'Todas'] + Oficina::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $categorias = [0=>'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $subcategorias = [0=>'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            // $subcategoriasdet = [0=>'Todas'] + SubcategoriaDetalle::plataforma()->sortBy('name')->pluck('name','id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();
            $valores['oficinas'] = $user->oficina_id;

            $categorias_graf = Categoria::plataforma()->sortBy('name');
        }

        $anys = Venta::groupBy('any')->get()->pluck('any','any')->toArray();
        $anys[end($anys)+1] = end($anys)+1;

        $semanas = [];
        for ($i=1; $i <= 52; $i++)
        {
            $semanas[$i] = $i;
        }

        $valores['any'] = intval($request->input('any',Carbon::now()->format('Y')));
        $valores['semana'] = intval($request->input('semana',0));
        $valores['categorias'] = intval($request->input('categorias',0));
        $valores['subcategorias'] = intval($request->input('subcategorias',0));
        $valores['oficinas'] = $request->input('oficinas',0);

        $resultados = $request->has('semana')?true:false;
        $ventas = null;
        $ventasCurso = null;

        if($resultados)
        {
            $user = auth()->user();
            if($user->filtro_oficinas)
            {
                $plataforma = $valores['plataformas'];
                if($plataforma)
                {
                    $ofis = Oficina::where('propietario',$plataforma)->orWhere('propietario',0)->pluck('id')->toArray();
                }
                else
                {
                    $ofis = Oficina::all()->pluck('id')->toArray();
                }

                $ventas = Venta::whereIn('oficina_id',$ofis);
                $ventasCurso = VentaCurso::whereIn('oficina_id',$ofis);
                $solicitudes = Solicitud::whereIn('oficina_id',$ofis);
            }
            else
            {
                $oficina_id = $user->oficina_id;
                $ventas = Venta::where('oficina_id',$oficina_id);
                $ventasCurso = VentaCurso::where('oficina_id',$oficina_id);
                $solicitudes = Solicitud::where('oficina_id',$oficina_id);

                $ofis[] = $oficina_id;
            }

            // filtros
            // $valores['oficinas']
            // $valores['categorias']
            // $valores['subcategorias']

            if($valores['oficinas'])
            {
                if($valores['oficinas'][0]!=0)
                {
                    $ventas = $ventas->whereIn('oficina_id', $valores['oficinas']);
                    $ventasCurso = $ventasCurso->whereIn('oficina_id', $valores['oficinas']);
                    $solicitudes = $solicitudes->whereIn('oficina_id', $valores['oficinas']);
                    $ofis = Oficina::whereIn('id', $valores['oficinas'])->orderBy('name')->pluck('id')->toArray();
                }
            }

            if($valores['categorias'])
            {
                $ventas = $ventas->where('category_id', $valores['categorias']);
                $ventasCurso = $ventasCurso->where('category_id', $valores['categorias']);
                $solicitudes = $solicitudes->where('category_id', $valores['categorias']);
            }

            if($valores['subcategorias'])
            {
                $ventas = $ventas->where('subcategory_id', $valores['subcategorias']);
                $ventasCurso = $ventasCurso->where('subcategory_id', $valores['subcategorias']);
                $solicitudes = $solicitudes->where('subcategory_id', $valores['subcategorias']);
            }
            //filtros end

            Graficos::loadVentas($valores, $ventas, $ventasCurso, $solicitudes, $ofis, $categorias_graf);
        }


        //view
        return view( 'manage.index_ventas',compact(
                'resultados','valores','anys','semanas','plataformas','oficinas','categorias','subcategorias',
                'ventas', 'ventasCurso'
            )
        );
    }


    public function getResumen()
    {
        // ini_set('max_execution_time', 300); //300 seconds = 5 minutes

        $uresumen = Input::get('u');
        $oresumen = Input::get('o');

        $user = auth()->user();

        if(!$uresumen)
        {
            $uresumen = auth()->user()->id;
        }

        if(!$user->filtro_oficinas)
        {
            $oresumen = $user->oficina_id;
        }

        //si elige oficina que se quede seleccionado el primero de esa ofi
        if($oresumen>0 )
        {
            $asig = User::find($uresumen);
            if($asig)
            {
                if($asig->oficina_id != $oresumen)
                {
                    $asignado = User::where('oficina_id',$oresumen)->orderBy('fname')->first();
                    $uresumen = $asignado->id;
                }
            }
        }

        $oficinas = Oficina::plataforma();
        if($oresumen>0)
        {
            $asignados = User::asignados()->where('oficina_id',$oresumen)->orderBy('fname')->get();
        }
        else
        {
            $asignados = User::asignados()->get();
        }

        // $oficinas = Oficina::plataforma()->pluck('name','id')->toArray();
        // $asignados = User::asignados()->get()->pluck('full_name', 'id');

        $bTodos = false;
        $bTodosUsuarios = false;

        if( is_numeric($uresumen) && is_numeric($oresumen) )
        {
            $user = User::find($uresumen);
            if($user->oficina_id != $oresumen)
            {
                $error = "Combinación incorrecta. Usuario y Oficina que no corresponden.";
                Session::flash('mensaje-alert', $error);

                return view('manage.index_resumen', compact('user','oresumen','uresumen','users','oficinas','error'));
            }
        }
        elseif( $oresumen=='all' && is_numeric($uresumen) )
        {
            Session::flash('mensaje', 'Combinación incorrecta. Si elige Usuario no se tiene en cuenta la Oficina.');
        }
        elseif( $oresumen=='all' && ($uresumen=='all' || $uresumen=='all-off') ) //Todos todos
        {
            $bTodos = true;
        }
        elseif( is_numeric($oresumen) && ($uresumen=='all' || $uresumen=='all-off') ) //Todos usuarios de 1 oficina
        {
            $bTodosUsuarios = true;
        }


        $status = SolicitudStatus::all();
        // $status = [0=>'Sin Estado'] + SolicitudStatus::all()->toArray();
        $origenes = Origen::plataforma();
        $categorias = Categoria::plataforma();

        //Filtro Plataforma
        $this->tarea->pushCriteria(new FiltroPlataformaAsignado());
        $this->log->pushCriteria(new FiltroPlataformaAsignado());

        $filtro = [];
        if(is_numeric($uresumen))
        {
            $filtro = ['user_id','=',$uresumen];;
        }

        $viajeros = null;
        $solicitudes = null;

        // $t = Carbon::now(); //tmp

        // $bTodosUsuarios = true; //tmp
        // $oresumen = 1; //tmp

        $usuarios = null;

        if($bTodos)
        {
            //todos usuarios y oficinas

            if($uresumen=='all')
            {
                $usuarios = User::asignados()->get();
            }
            else
            {
                $usuarios = User::asignadosOff()->get();
            }

            $plat = ConfigHelper::config('propietario');
            if($plat)
            {
                $usuarios = $usuarios->where('plataforma',$plat)->get();
            }
            else
            {
                $usuarios = $usuarios->get();
            }

            $v = collect([]);
            $s = collect([]);

            foreach($usuarios as $u)
            {
                $v = $v->merge( $u->viajeros );
                $s = $s->merge( $u->solicitudes );
            }

        }
        elseif($bTodosUsuarios)
        {
            //todos usuarios 1 oficina

            if($uresumen=='all')
            {
                $usuarios = User::asignados()->get();
            }
            else
            {
                $usuarios = User::asignadosOff()->get();
            }

            $plat = ConfigHelper::config('propietario');
            if($plat)
            {
                $usuarios = $usuarios->where('plataforma',$plat)->where('oficina_id',$oresumen)->get();
            }
            else
            {
                $usuarios = $usuarios->where('oficina_id',$oresumen)->get();
            }

            $v = collect([]);
            $s = collect([]);

            foreach($usuarios as $u)
            {
                $v = $v->merge( $u->viajeros );
                $s = $s->merge( $u->solicitudes );
            }
        }
        else
        {
            // $uresumen = 7; //tmp

            $user = User::find($uresumen);

            $v = $user->viajeros;
            $s = $user->solicitudes;
        }

        $vStatus = $v->groupBy('status_id')->toArray();
        $vOrigen = $v->groupBy('origen_id')->toArray();
        $vSuborigen = $v->groupBy('suborigen_id')->toArray();
        $vSuborigendet = $v->groupBy('suborigendet_id')->toArray();

        $sStatus = $s->groupBy('status_id')->toArray();
        $sOrigen = $s->groupBy('category_id')->toArray();
        $sSuborigen = $s->groupBy('subcategory_id_id')->toArray();
        $sSuborigendet = $s->groupBy('subcategory_det_id')->toArray();

        //================================================

        // Init:
        $viajeros['total'] = $v->count();
        $viajeros['status'] = [];
        $viajeros['origen'] = [];
        $viajeros['suborigen'] = [];
        $viajeros['suborigendet'] = [];

        $solicitudes['total'] = $s->count();
        $solicitudes['status'] = [];
        $solicitudes['origen'] = [];
        $solicitudes['suborigen'] = [];
        $solicitudes['suborigendet'] = [];


        $vOrigenStatus = [];
        $vSuborigenStatus = [];
        $vSuborigendetStatus = [];

        //Calculos:
        foreach($status as $st)
        {
            $viajeros['status'][$st->id] = isset($vStatus[$st->id])?count($vStatus[$st->id]):0;
            $solicitudes['status'][$st->id] = isset($sStatus[$st->id])?count($sStatus[$st->id]):0;

            $vOrigenStatus[$st->id] = $v->where('status_id',$st->id)->groupBy('origen_id')->toArray();
            $vSuborigenStatus[$st->id] = $v->where('status_id',$st->id)->groupBy('suborigen_id')->toArray();
            $vSuborigendetStatus[$st->id] = $v->where('status_id',$st->id)->groupBy('suborigendet_id')->toArray();

            $sOrigenStatus[$st->id] = $s->where('status_id',$st->id)->groupBy('category_id')->toArray();
            $sSuborigenStatus[$st->id] = $s->where('status_id',$st->id)->groupBy('subcategory_id_id')->toArray();
            $sSuborigendetStatus[$st->id] = $s->where('status_id',$st->id)->groupBy('subcategory_det_id')->toArray();
        }

        //Viajeros
        foreach($origenes as $o)
        {
            $viajeros['origen'][$o->id] = [];
            $viajeros['origen'][$o->id]['status'] = [];

            //total
            $viajeros['origen'][$o->id]['status'][0] = isset($vOrigen[$o->id])?count($vOrigen[$o->id]):0;

            foreach($status as $st)
            {
                $valor = isset($vOrigenStatus[$st->id])?(isset($vOrigenStatus[$st->id][$o->id])?count($vOrigenStatus[$st->id][$o->id]):0):0;
                $viajeros['origen'][$o->id]['status'][$st->id] = $valor;
            }

            foreach($o->suborigenes as $os)
            {
                $viajeros['suborigen'][$os->id] = [];
                $viajeros['suborigen'][$os->id]['status'] = [];

                //total
                $viajeros['suborigen'][$os->id]['status'][0] = isset($vSuborigen[$os->id])?count($vSuborigen[$os->id]):0;

                foreach($status as $st)
                {
                    $valor = isset($vSuborigenStatus[$st->id])?(isset($vSuborigenStatus[$st->id][$os->id])?count($vSuborigenStatus[$st->id][$os->id]):0):0;
                    $viajeros['suborigen'][$os->id]['status'][$st->id] = $valor;
                }

                foreach($os->suborigenesdet as $osd)
                {
                    $viajeros['suborigendet'][$osd->id] = [];
                    $viajeros['suborigendet'][$osd->id]['status'] = [];

                    //total
                    $viajeros['suborigendet'][$osd->id]['status'][0] = isset($vSuborigendet[$osd->id])?count($vSuborigendet[$osd->id]):0;

                    foreach($status as $st)
                    {
                        $valor = isset($vSuborigendetStatus[$st->id])?(isset($vSuborigendetStatus[$st->id][$osd->id])?count($vSuborigendetStatus[$st->id][$osd->id]):0):0;
                        $viajeros['suborigendet'][$osd->id]['status'][$st->id] = $valor;
                    }
                }
            }
        }



        //Solicitudes
        foreach($categorias as $o)
        {
            $solicitudes['category'][$o->id] = [];
            $solicitudes['category'][$o->id]['status'] = [];

            //total
            $solicitudes['category'][$o->id]['status'][0] = isset($sOrigen[$o->id])?count($sOrigen[$o->id]):0;

            foreach($status as $st)
            {
                $valor = isset($sOrigenStatus[$st->id])?(isset($sOrigenStatus[$st->id][$o->id])?count($sOrigenStatus[$st->id][$o->id]):0):0;
                $solicitudes['category'][$o->id]['status'][$st->id] = $valor;
            }

            foreach($o->subcategorias as $os)
            {
                $solicitudes['subcategory'][$os->id] = [];
                $solicitudes['subcategory'][$os->id]['status'] = [];

                //total
                $solicitudes['subcategory'][$os->id]['status'][0] = isset($sSuborigen[$o->id])?count($sSuborigen[$o->id]):0;

                foreach($status as $st)
                {
                    $valor = isset($sSuborigenStatus[$st->id])?(isset($sSuborigenStatus[$st->id][$o->id])?count($sSuborigenStatus[$st->id][$o->id]):0):0;
                    $solicitudes['subcategory'][$os->id]['status'][$st->id] = $valor;
                }

                foreach($os->subcategoriasdet as $osd)
                {
                    $solicitudes['subcategory_det'][$osd->id] = [];
                    $solicitudes['subcategory_det'][$osd->id]['status'] = [];

                    //total
                    $solicitudes['subcategory_det'][$osd->id]['status'][0] = isset($sSuborigendet[$o->id])?count($sSuborigendet[$o->id]):0;

                    foreach($status as $st)
                    {
                        $valor = isset($sSuborigendetStatus[$st->id])?(isset($sSuborigendetStatus[$st->id][$o->id])?count($sSuborigendetStatus[$st->id][$o->id]):0):0;
                        $solicitudes['subcategory_det'][$osd->id]['status'][$st->id] = $valor;
                    }
                }
            }
        }

        // $diff = Carbon::now()->diffInSeconds($t); //tmp
        // echo " $diff s."; //tmp

        // dd($solicitudes);


        //Seguimientos ===================================================================================================================

        // if(is_numeric($uresumen))
        // {
        //     $filtro = ['user_id','=',$uresumen];
        // }

        //Oficina
        if( is_numeric($oresumen) && $uresumen==='all' )
        {
            $s5d = collect();
            $s10d = collect();
            $s15d = collect();
            $s30d = collect();
            $s1m = collect();
            $s = collect();

            $ofis = User::asignados()->where('oficina_id',$oresumen)->pluck('id');

            foreach($ofis as $o)
            {
                $s5dOfi = $this->log->findWhere([ ['tipo','<>',''], ['tipo','<>','log'], ['user_id',$o],
                        [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'>', Carbon::today()->subDays(5)->format('Y-m-d')] ])->groupBy('viajero_id');

                $s10dOfi = $this->log->findWhere([ ['tipo','<>',''], ['tipo','<>','log'], ['user_id',$o],
                        [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'>', Carbon::today()->subDays(10)->format('Y-m-d')],
                        [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'<=', Carbon::today()->subDays(5)->format('Y-m-d')] ])->groupBy('viajero_id');

                $s15dOfi = $this->log->findWhere([ ['tipo','<>',''], ['tipo','<>','log'], ['user_id',$o],
                        [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'>', Carbon::today()->subDays(15)->format('Y-m-d')],
                        [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'<=', Carbon::today()->subDays(10)->format('Y-m-d')] ])->groupBy('viajero_id');

                $s30dOfi = $this->log->findWhere([ ['tipo','<>',''], ['tipo','<>','log'], ['user_id',$o],
                        [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'>', Carbon::today()->subDays(30)->format('Y-m-d')],
                        [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'<=', Carbon::today()->subDays(15)->format('Y-m-d')] ])->groupBy('viajero_id');

                $s1mOfi = $this->log->findWhere([ ['tipo','<>',''], ['tipo','<>','log'], ['user_id',$o],
                        [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'<=', Carbon::today()->subDays(30)->format('Y-m-d')] ])->groupBy('viajero_id');

                $sOfi = $this->log->findWhere([ ['tipo','<>',''], ['tipo','<>','log'], ['user_id',$o] ])->groupBy('viajero_id');

                $s5d = $s5d->merge($s5dOfi);
                $s10d = $s10d->merge($s10dOfi);
                $s15d = $s15d->merge($s15dOfi);
                $s30d = $s30d->merge($s30dOfi);
                $s1m = $s1m->merge($s1mOfi);
                $s = $s->merge($sOfi);
            }

            $s5d = $s5d->unique();
            $s10d = $s10d->unique();
            $s15d = $s15d->unique();
            $s30d = $s30d->unique();
            $s1m = $s1m->unique();
            $s = $s->unique();
        }
        else
        {
            $s5d = $this->log->findWhere([ ['tipo','<>',''], ['tipo','<>','log'], $filtro,
                    // [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'<=', Carbon::today()->format('Y-m-d')],
                    [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'>', Carbon::today()->subDays(5)->format('Y-m-d')] ])->groupBy('viajero_id');

            $s10d = $this->log->findWhere([ ['tipo','<>',''], ['tipo','<>','log'], $filtro,
                    [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'>', Carbon::today()->subDays(10)->format('Y-m-d')],
                    [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'<=', Carbon::today()->subDays(5)->format('Y-m-d')] ])->groupBy('viajero_id');

            $s15d = $this->log->findWhere([ ['tipo','<>',''], ['tipo','<>','log'], $filtro,
                    [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'>', Carbon::today()->subDays(15)->format('Y-m-d')],
                    [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'<=', Carbon::today()->subDays(10)->format('Y-m-d')] ])->groupBy('viajero_id');

            $s30d = $this->log->findWhere([ ['tipo','<>',''], ['tipo','<>','log'], $filtro,
                    [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'>', Carbon::today()->subDays(30)->format('Y-m-d')],
                    [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'<=', Carbon::today()->subDays(15)->format('Y-m-d')] ])->groupBy('viajero_id');

            $s1m = $this->log->findWhere([ ['tipo','<>',''], ['tipo','<>','log'], $filtro,
                    [DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d')"),'<=', Carbon::today()->subDays(30)->format('Y-m-d')] ])->groupBy('viajero_id');

            $s = $this->log->findWhere([ ['tipo','<>',''], ['tipo','<>','log'], $filtro ])->groupBy('viajero_id');
        }

        $viajeros_5d = Viajero::whereIn('id',$s5d->keys())->get();

        $notIn = $viajeros_5d;
        $viajeros_10d = Viajero::whereIn('id',$s10d->keys())->whereNotIn('id',$notIn->pluck('id'))->get();

        $notIn = $notIn->merge($viajeros_10d)->unique();
        $viajeros_15d = Viajero::whereIn('id',$s15d->keys())->whereNotIn('id',$notIn->pluck('id'))->get();

        $notIn = $notIn->merge($viajeros_15d)->unique();
        $viajeros_30d = Viajero::whereIn('id',$s30d->keys())->whereNotIn('id',$notIn->pluck('id'))->get();

        $notIn = $notIn->merge($viajeros_30d)->unique();
        $viajeros_1m = Viajero::whereIn('id',$s1m->keys())->whereNotIn('id',$notIn->pluck('id'))->get();

        $seguimientos = [
            'total' => $s->count(),
            '5d'=> $viajeros_5d,
            '10d'=> $viajeros_10d,
            '15d'=> $viajeros_15d,
            '30d'=> $viajeros_30d,
            '1m'=> $viajeros_1m
            ];

        $oficinas = Oficina::plataforma();

        return view('manage.index_resumen', compact('user','origenes','status','categorias','viajeros','solicitudes','uresumen','oresumen','seguimientos','asignados','oficinas'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getPaises()
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection( Pais::orderBy('name')->get() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.paises.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->showColumns('slug')
                ->addColumn('activo_web', function($model) {
                    return $model->activo_web?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    $ret .= "<a href='". route('manage.provincias.nuevo', [$model->id]) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Provincia</a>";
                    $ret .= " <a href='". route('manage.ciudades.nuevo', [$model->id,0]) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Ciudad</a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','slug')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.paises.index');
    }

    public function getPaisNuevo()
    {
        return view('manage.paises.new');
    }

    public function getPaisUpdate($id)
    {
        $ficha = Pais::find($id);

        return View::make('manage.paises.ficha', compact('ficha'));
    }

    public function postPaisUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name' => 'required',
            // 'slug' => 'unique'
        ]);

        $data = $request->input();
        $data['activo_web'] = $request->has('activo_web');

        $bColor = true;

        if(!$id)
        {
            $p = Pais::create($data);

            SystemLog::addLog($p,'Nuevo');
        }
        else
        {
            $p = Pais::find($id);
            $p1 = clone $p;

            $p->update($data);

            if($p1->color == $p->color)
            {
                $bColor = false;
            }

            SystemLog::addLog($p,'Modificado',$p1);
        }

        $ficha = Pais::find($id);

        //foto
        if ($request->hasFile('imagen'))
        {
            $file = $request->file('imagen');

            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file_name = str_slug($file_name) .".". $file->getClientOriginalExtension();
            $dirp = "assets/uploads/cms/paises/". $id . "/";
            $dir = public_path($dirp);

            $file->move($dir, $file_name);

            $img = Image::make($dir.$file_name);
            $img->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            //thumb
            if (!file_exists($dir."thumb/"))
            {
                File::makeDirectory($dir."thumb/", 0775, true);
            }
            File::copy($dir.$file_name, $dir."thumb/".$file_name);
            $img2 = Image::make($dir."thumb/".$file_name);
            $img2->resize(450, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            $ficha->imagen = "/".$dirp.$file_name;
            $ficha->save();
        }


        if($bColor)
        {
            $i = 0;
            foreach($ficha->centros as $centro)
            {
                foreach($centro->cursos as $c)
                {
                    $c->updateDataWebTag('pais_color', $ficha->color);
                    $i++;
                }
            }

            Session::flash('mensaje-ok', "Se han actualizado $i cursos.");
            
        }

        return redirect()->route('manage.paises.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getNacionalidades()
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection( Nacionalidad::orderBy('name')->get() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.nacionalidades.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('id','name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.nacionalidades.index');
    }

    public function getNacionalidadNuevo()
    {
        return view('manage.nacionalidades.new');
    }

    public function getNacionalidadUpdate($id)
    {
        $ficha = Nacionalidad::find($id);

        return View::make('manage.nacionalidades.ficha', compact('ficha'));
    }

    public function postNacionalidadUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $p = Nacionalidad::find($id);

        if(!$id)
        {
            $p = new Nacionalidad;
            $p->name = Input::get('name');
            $p->save();

            SystemLog::addLog($p,'Nuevo');
        }
        else
        {
            $p1 = Nacionalidad::find($id);
            $p->name = Input::get('name');
            $p->save();

            SystemLog::addLog($p,'Modificado',$p1);
        }



        return redirect()->route('manage.nacionalidades.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getCiudades()
    {
        $pais_id = Input::get('country_id');
        if($pais_id>0)
        {
            return response()->json(Ciudad::where('country_id',$pais_id)->pluck('city_name','id'), 200);
        }

        if(Datatable::shouldHandle())
        {
            return Datatable::collection( Ciudad::all() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.ciudades.ficha',[$model->id]) ."'>$model->city_name</a>";
                })
                ->addColumn('provincia', function($model) {
                    return $model->provincia?$model->provincia->name:"-";
                })
                // ->addColumn('pais', function($model) {
                //     return $model->pais->name;
                // })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.ciudades.index');
    }

    public function getCiudadNuevo($pais_id=0,$provincia_id=0)
    {
        $paises = Pais::orderBy('name')->pluck('name','id');
        $provincias = Provincia::pluck('name','id');

        return view('manage.ciudades.new', compact('paises','provincias','pais_id', 'provincia_id'));
    }

    public function getCiudadUpdate($id)
    {
        $ficha = Ciudad::find($id);

        $paises = Pais::orderBy('name')->pluck('name','id');
        $provincias = Provincia::pluck('name','id');

        return View::make('manage.ciudades.ficha', compact('ficha','paises','provincias'));
    }

    public function postCiudadUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'city_name' => 'required',
            // 'provincia_id' => 'required',
            // 'country_id' => 'required',
        ]);

        $p = Ciudad::find($id);

        if(!$id)
        {
            $p = new Ciudad;

            $p->city_name = Input::get('city_name');
            $p->country_id = Input::get('country_id');
            $p->provincia_id = Input::get('provincia_id');
            $p->save();

            SystemLog::addLog($p,'Nuevo');
        }
        else
        {
            $p1 = Ciudad::find($id);

            $p->city_name = Input::get('city_name');
            $p->country_id = Input::get('country_id');
            $p->provincia_id = Input::get('provincia_id');
            $p->save();

            SystemLog::addLog($p,'Modificado',$p1);
        }

        return redirect()->route('manage.ciudades.index');
    }

    //Provincias
    public function getProvincias()
    {
        $country_id = Input::get('country_id');
        if($country_id>0)
        {
            return response()->json(Provincia::where('pais_id',$country_id)->pluck('name','id'), 200);
        }

        if(Datatable::shouldHandle())
        {
            return Datatable::collection( Provincia::all() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.provincias.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('pais', function($model) {
                    return $model->pais->name;
                })
                ->showColumns('idioma_contacto')
                ->addColumn('oficina', function($model) {
                    return $model->oficina?$model->oficina->name:"";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    $ret .= "<a href='". route('manage.ciudades.nuevo', [$model->id]) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Ciudad</a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.provincias.index');
    }

    public function getProvinciaNuevo($id=0)
    {
        $paises = Pais::orderBy('name')->pluck('name','id');

        return view('manage.provincias.new', compact('paises','id'));
    }

    public function getProvinciaUpdate($id)
    {
        $ficha = Provincia::find($id);

        $paises = Pais::orderBy('name')->pluck('name','id');

        return View::make('manage.provincias.ficha', compact('ficha', 'paises'));
    }

    public function postProvinciaUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name' => 'required',
            'pais_id' => 'required',
        ]);

        $p = Provincia::find($id);

        if(!$id)
        {
            $p = new Provincia;

            $p->name = trim(Input::get('name'));
            $p->pais_id = Input::get('pais_id');
            $p->idioma_contacto = Input::get('idioma_contacto');
            $p->oficina_id = $request->get('oficina_id');
            $p->save();

            SystemLog::addLog($p,'Nuevo');
        }
        else
        {
            $p1 = Provincia::find($id);

            $p->name = Input::get('name');
            $p->pais_id = Input::get('pais_id');
            $p->idioma_contacto = Input::get('idioma_contacto');
            $p->oficina_id = $request->get('oficina_id');
            $p->save();

            SystemLog::addLog($p,'Modificado',$p1);
        }

        return redirect()->route('manage.provincias.index');
    }

    /**
     * CATEGORIAS
     */

    /**
     * Display a listing of the categorias.
     *
     * @return Response
     */
    public function getCategorias()
    {
        if(Datatable::shouldHandle())
        {
            $col = Categoria::all();

            $filtro = ConfigHelper::config('propietario');
            if( $filtro && !auth()->user()->isFullAdmin() )
            {
                $col = Categoria::where('propietario', 0)->orWhere('propietario',$filtro)->get();
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.categorias.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('menor', function($model) {
                    return $model->es_menor?"<i class='fa fa-check-circle'></i>":"NO";
                })
                ->addColumn('avisos', function($model) {

                    if(!$model->avisos)
                    {
                        return '';
                    }

                    $ret = "<ul>";
                    foreach($model->avisos as $aviso)
                    {
                        $u = User::find($aviso);
                        $plat = $u->plataforma_name;
                        $ret .= "<li>$u->full_name ($plat)</li>";
                    }
                    $ret .="</ul>";

                    return $ret;
                })
                ->addColumn('avisos_online', function($model) {

                    if(!$model->avisos_online)
                    {
                        return '';
                    }

                    $ret = "<ul>";
                    foreach($model->avisos_online as $aviso)
                    {
                        $u = User::find($aviso);
                        $plat = $u->plataforma_name;
                        $ret .= "<li>$u->full_name ($plat)</li>";
                    }
                    $ret .="</ul>";

                    return $ret;
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Categoría' data-action='". route( 'manage.categorias.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.categorias.index');
    }

    public function getCategoriaNuevo()
    {
        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        return view('manage.categorias.new', compact('asignados'));
    }

    public function getCategoriaUpdate($id)
    {
        ini_set('memory_limit', '400M');
        set_time_limit(50000);

        $ficha = Categoria::find($id);

        $cuestionarios = ['' => ''] + Cuestionario::plataforma()->where('activo',1)->pluck('name','id')->toArray();
        $asignados = User::asignados()->get()->pluck('full_name', 'id');
        $examenes = ['' => ''] + Examen::plataforma()->where('activo',1)->pluck('name','id')->toArray();

        return view('manage.categorias.ficha', compact('ficha','cuestionarios','examenes','asignados'));
    }

    public function destroyCategoria($id)
    {
        //Curso->categoria_id => 0;
        //CentroDescuento->categoria_id => 0;

        foreach( Curso::where('category_id', $id)->get() as $curso )
        {
            $curso->category_id = 0;
            $curso->subcategory_id = 0;
            $curso->subcategory_det_id = 0;
            $curso->save();
        }

        foreach( CentroDescuento::where('category_id', $id)->get() as $descuento )
        {
            $descuento->category_id = 0;
            $descuento->subcategory_id = 0;
            $descuento->subcategory_det_id = 0;
            $descuento->save();
        }

        Categoria::find($id)->delete();

        return redirect()->route('manage.categorias.index');
    }

    public function postCategoriaUpdate(Request $request, $id=0)
    {
        if( $request->has('condiciones') )
        {
            $o = Categoria::find($id);
            $condiciones = $o->condiciones;

            foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)
            {
                foreach(ConfigHelper::idiomas() as $keyi=>$idioma)
                {
                    if( !isset($condiciones[$keyp]) )
                    {
                        $condiciones[$keyp] = [];
                    }

                    if( !isset($condiciones[$keyp][$idioma]) )
                    {
                        $condiciones[$keyp][$idioma] = "";
                    }

                    $condiciones[$keyp][$idioma] = $request->input("condiciones_$keyp-$idioma");
                }
            }

            $o->condiciones = $condiciones;
            $o->save();
            $o->pdfCondiciones();

            return redirect()->route('manage.categorias.index');
        }

        if( $request->has('pdf_cancelacion') )
        {
            $o = Categoria::find($id);
            $pdf = $o->pdf_cancelacion;

            $dir = "assets/uploads/pdf_cancelacion/";
            $name = "Categoria_" . $o->id;

            foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)
            {
                foreach(ConfigHelper::idiomas() as $keyi=>$idioma)
                {
                    if( !isset($pdf[$keyp]) )
                    {
                        $pdf[$keyp] = [];
                    }

                    if( !isset($pdf[$keyp][$idioma]) )
                    {
                        $pdf[$keyp][$idioma] = "";
                    }

                    $file_name = $pdf[$keyp][$idioma];
                    
                    if($request->has( "pdf_$keyp-$idioma"."_delete" ))
                    {
                        $pdf[$keyp][$idioma] = "";

                        $f = public_path($file_name);
                        if(file_exists($f))
                        {
                            \File::delete($f);
                        }
                        continue;
                    }

                    $file = $request->file("pdf_$keyp-$idioma");
                    if($file)
                    {
                        $file_name = $dir . $name . "_" . $keyp . "_" . $idioma;
                        $file_name = $file_name . "." . $file->getClientOriginalExtension();
                        $file->move(public_path($dir), $file_name);
                    }

                    $pdf[$keyp][$idioma] = $file_name;
                }
            }

            $o->pdf_cancelacion = $pdf;
            $o->save();

            Session::flash('tab','#pdf_cancelacion');
            return redirect()->route('manage.categorias.index');
        }

        $this->validate($request, [
            'name' => 'required',
        ]);

        $propietario = $request->has('propietario')?$request->input('propietario'):0;
        if( $request->has('propietario_check') )
        {
            $propietario = ConfigHelper::config('propietario');
        }

        $data = $request->except('_token');
        $data['es_menor'] = $request->has('es_menor');
        $data['es_grado_ext'] = $request->has('es_grado_ext');
        $data['propietario'] = $propietario;
        $data['slug'] = $request->input('slug')!=""?$request->input('slug'):str_slug($request->input('name'));
        $data['notapago_auto'] = $request->has('notapago_auto');
        $data['no_facturar'] = $request->has('no_facturar');
        $data['pocket'] = $request->has('pocket');
        $data['es_info_campamento'] = $request->has('es_info_campamento');
        $data['web_pie'] = $request->has('web_pie');
        $data['contable'] = $request->get('contable') ?: "";

        $bPocket = false;

        if(!$id)
        {
            $o = Categoria::create($data);

            $bPocket = true;

            SystemLog::addLog($o,'Nuevo');
        }
        else
        {
            $o = Categoria::find($id);
            $o1 = clone $o;

            $bPocket = $o->pocket != $data['pocket'];

            $o->update($data);

            SystemLog::addLog($o,'Modificado',$o1);
        }

        if($bPocket)
        {
            foreach( $o->cursos as $c )
            {
                $c->pocket = $o->pocket;
                $c->save();
            }
        }

        $o->scriptUpdate();

        return redirect()->route('manage.categorias.index');
    }


    /**
     * SUBCATEGORIAS
     */

    // public function getSubcategoriasJsonMulti(Request $request)
    // {
    //     if( !$request->ajax() )
    //     {
    //         abort(404);
    //     }

    //     $category_id = Input::get('category_id');
    //     $cats = explode(',', $category_id);

    //     $subcategorias = [];

    //     foreach($cats as $c)
    //     {
    //         $cat = Categoria::find($c);

    //         if($cat)
    //         {
    //             $cnom = $cat->name;
    //             $scats = Subcategoria::where('category_id',$c)->get();
    //             foreach($scats as $sc)
    //             {
    //                 $subcategorias[$cnom][$sc->id] = $sc->subcategory_name;
    //             }
    //         }
    //     }

    //     return response()->json($subcategorias, 200);
    // }

    /**
     * Display a listing of the subcategorias.
     *
     * @return Response
     */
    public function getSubcategorias()
    {
        $category_id = Input::get('category_id');
        if($category_id>0)
        {
            return response()->json(Subcategoria::where('category_id',$category_id)->pluck('name','id'), 200);
        }

        if(Datatable::shouldHandle())
        {
            return Datatable::collection( Subcategoria::all() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.subcategorias.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('categoria', function($model) {
                    return $model->categoria->name;
                })
                ->addColumn('avisos', function($model) {

                    if(!$model->avisos)
                    {
                        return '';
                    }

                    $ret = "<ul>";
                    foreach($model->avisos as $aviso)
                    {
                        $u = User::find($aviso);
                        $plat = $u->plataforma_name;
                        $ret .= "<li>$u->full_name ($plat)</li>";
                    }
                    $ret .="</ul>";

                    return $ret;
                })
                ->addColumn('avisos_online', function($model) {

                    if(!$model->avisos_online)
                    {
                        return '';
                    }

                    $ret = "<ul>";
                    foreach($model->avisos_online as $aviso)
                    {
                        $u = User::find($aviso);
                        $plat = $u->plataforma_name;
                        $ret .= "<li>$u->full_name ($plat)</li>";
                    }
                    $ret .="</ul>";

                    return $ret;
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Sub-Categoría' data-action='". route( 'manage.subcategorias.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','categoria')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.subcategorias.index');
    }

    public function getSubcategoriaNuevo()
    {
        $categorias = Categoria::plataforma()->pluck('name','id');
        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        return view('manage.subcategorias.new', compact('categorias','asignados'));
    }

    public function getSubcategoriaUpdate($id)
    {
        ini_set('memory_limit', '400M');
        set_time_limit(50000);
        
        $ficha = Subcategoria::find($id);

        $categorias = Categoria::plataforma()->pluck('name','id');
        $cuestionarios = ['' => ''] + Cuestionario::plataforma()->where('activo',1)->pluck('name','id')->toArray();
        $asignados = User::asignados()->get()->pluck('full_name', 'id');
        $examenes = ['' => ''] + Examen::plataforma()->where('activo',1)->pluck('name','id')->toArray();

        return View::make('manage.subcategorias.ficha', compact('ficha', 'categorias','cuestionarios','examenes','asignados'));
    }

    public function destroySubcategoria($id)
    {
        foreach( Curso::where('subcategory_id', $id)->get() as $curso )
        {
            $curso->subcategory_id = 0;
            $curso->subcategory_det_id = 0;
            $curso->save();
        }

        foreach( CentroDescuento::where('subcategory_id', $id)->get() as $descuento )
        {
            $descuento->subcategory_id = 0;
            $descuento->subcategory_det_id = 0;
            $descuento->save();
        }

        Subcategoria::find($id)->delete();

        return redirect()->route('manage.subcategorias.index');
    }

    public function postSubcategoriaUpdate(Request $request, $id=0)
    {
        if( $request->has('condiciones') )
        {
            $o = Subcategoria::find($id);
            $condiciones = $o->condiciones;

            foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)
            {
                foreach(ConfigHelper::idiomas() as $keyi=>$idioma)
                {
                    if( !isset($condiciones[$keyp]) )
                    {
                        $condiciones[$keyp] = [];
                    }

                    if( !isset($condiciones[$keyp][$idioma]) )
                    {
                        $condiciones[$keyp][$idioma] = "";
                    }

                    $condiciones[$keyp][$idioma] = $request->input("condiciones_$keyp-$idioma");
                }
            }

            $o->condiciones = $condiciones;
            $o->save();
            $o->pdfCondiciones();

            return redirect()->route('manage.subcategorias.index');
        }

        if( $request->has('pdf_cancelacion') )
        {
            $o = Subcategoria::find($id);
            $pdf = $o->pdf_cancelacion;

            $dir = "assets/uploads/pdf_cancelacion/";
            $name = "SubCategoria_" . $o->id;

            foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)
            {
                foreach(ConfigHelper::idiomas() as $keyi=>$idioma)
                {
                    if( !isset($pdf[$keyp]) )
                    {
                        $pdf[$keyp] = [];
                    }

                    if( !isset($pdf[$keyp][$idioma]) )
                    {
                        $pdf[$keyp][$idioma] = "";
                    }

                    $file_name = $pdf[$keyp][$idioma];
                    
                    if($request->has( "pdf_$keyp-$idioma"."_delete" ))
                    {
                        $pdf[$keyp][$idioma] = "";

                        $f = public_path($file_name);
                        if(file_exists($f))
                        {
                            \File::delete($f);
                        }
                        continue;
                    }

                    $file = $request->file("pdf_$keyp-$idioma");
                    if($file)
                    {
                        $file_name = $dir . $name . "_" . $keyp . "_" . $idioma;
                        $file_name = $file_name . "." . $file->getClientOriginalExtension();
                        $file->move(public_path($dir), $file_name);
                    }

                    $pdf[$keyp][$idioma] = $file_name;
                }
            }

            $o->pdf_cancelacion = $pdf;
            $o->save();

            Session::flash('tab','#pdf_cancelacion');
            return redirect()->route('manage.subcategorias.index');
        }

        $this->validate($request, [
            'category_id' => 'required',
            'name' => 'required',
        ]);

        $data = $request->except('_token');
        $data['slug'] = $request->input('slug')!=""?$request->input('slug'):str_slug($request->input('name'));
        $data['no_facturar'] = $request->has('no_facturar');
        $data['pocket'] = $request->has('pocket');
        $data['contable'] = $request->get('contable') ?: "";
        $data['web_pie'] = $request->has('web_pie');

        $bPocket = false;

        if(!$id)
        {
            $o = Subcategoria::create($data);

            $bPocket = true;

            SystemLog::addLog($o,'Nuevo');
        }
        else
        {
            $o = Subcategoria::find($id);
            $o1 = clone $o;

            $bPocket = $o->pocket != $data['pocket'];

            $o->update($data);

            SystemLog::addLog($o,'Modificado',$o1);
        }

        if($bPocket)
        {
            foreach( $o->cursos as $c )
            {
                $c->pocket = $o->pocket;
                $c->save();
            }
        }

        $o->scriptUpdate();

        return redirect()->route('manage.subcategorias.index');
    }

    /**
     * SUBCATEGORIAS - DETALLE
     */

    /**
     * Display a listing of the subcategorias.
     *
     * @return Response
     */
    public function getSubcategoriasDet()
    {
        $subcategory_id = Input::get('subcategory_id');
        if($subcategory_id>0)
        {
            return response()->json(SubcategoriaDetalle::where('subcategory_id',$subcategory_id)->pluck('name','id'), 200);
        }

        if(Datatable::shouldHandle())
        {
            return Datatable::collection( SubcategoriaDetalle::all() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.subcategoria-detalles.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('subcategoria', function($model) {
                    return $model->subcategoria->name;
                })
                ->addColumn('categoria', function($model) {
                    return $model->subcategoria->categoria->name;
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Subcategoría Detalle' data-action='". route( 'manage.subcategoria-detalles.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','subcategoria','categoria')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.subcategoria-detalles.index');
    }

    public function getSubcategoriaDetNuevo()
    {
        $subcategorias = Subcategoria::pluck('name','id');
        return view('manage.subcategoria-detalles.new', compact('subcategorias'));
    }

    public function getSubcategoriaDetUpdate($id)
    {
        $ficha = SubcategoriaDetalle::find($id);

        $subcategorias = Subcategoria::pluck('name','id');

        return View::make('manage.subcategoria-detalles.ficha', compact('ficha', 'subcategorias'));
    }

    public function destroySubcategoriaDet($id)
    {
        //Curso->categoria_id => 0;
        //CentroDescuento->categoria_id => 0;

        foreach( Curso::where('subcategory_det_id', $id)->get() as $curso )
        {
            $curso->subcategory_det_id = 0;
            $curso->save();
        }

        foreach( CentroDescuento::where('subcategory_det_id', $id)->get() as $descuento )
        {
            $descuento->subcategory_det_id = 0;
            $descuento->save();
        }

        SubcategoriaDetalle::find($id)->delete();

        return redirect()->route('manage.subcategoria-detalles.index');
    }

    public function postSubcategoriaDetUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'subcategory_id' => 'required',
            'name' => 'required',
        ]);

        $o = SubcategoriaDetalle::find($id);

        if(!$id)
        {
            $o = new SubcategoriaDetalle;

            $o->subcategory_id = $request->input('subcategory_id');
            $o->name = Input::get('name');
            $o->slug = $request->input('slug')!=""?$request->input('slug'):str_slug($request->input('name'));
            $o->name_web = $request->input('name_web');
            $o->contable = $request->input('contable');
            $o->descripcion = $request->input('descripcion');
            $o->es_aviso_foto = $request->input('es_aviso_foto');
            $o->save();

            SystemLog::addLog($o,'Nuevo');
        }
        else
        {
            $o1 = SubcategoriaDetalle::find($id);

            $o->subcategory_id = $request->input('subcategory_id');
            $o->name = Input::get('name');
            $o->slug = $request->input('slug')!=""?$request->input('slug'):str_slug($request->input('name'));
            $o->name_web = $request->input('name_web');
            $o->contable = $request->input('contable');
            $o->descripcion = $request->input('descripcion');
            $o->es_aviso_foto = $request->input('es_aviso_foto');
            $o->save();

            SystemLog::addLog($o,'Modificado',$o1);
        }

        return redirect()->route('manage.subcategoria-detalles.index');
    }


    /**
     * UNIDADES EXTRA
     */

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getUnidades()
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection( ExtraUnidad::orderBy('name')->get() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.unidades.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Unidad Extra' data-action='". route( 'manage.unidades.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('id','name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.unidades.index');
    }

    public function getUnidadesNuevo()
    {
        return view('manage.unidades.new');
    }

    public function getUnidadesUpdate($id)
    {
        $ficha = ExtraUnidad::find($id);

        return View::make('manage.unidades.ficha', compact('ficha'));
    }

    public function postUnidadesUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $p = ExtraUnidad::find($id);

        if(!$id)
        {
            $p = new ExtraUnidad;

            $p->name = Input::get('name');
            $p->save();

            SystemLog::addLog($p,'Nuevo');
        }
        else
        {
            $p1 = ExtraUnidad::find($id);

            $p->name = Input::get('name');
            $p->save();

            SystemLog::addLog($p,'Modificado',$p1);
        }

        return redirect()->route('manage.unidades.index');
    }

    public function deleteUnidades($id)
    {
        ExtraUnidad::find($id)->delete();

        return redirect()->route('manage.unidades.index');
    }

    /**
     * ESPECIALIDADES
     */
    /**
     * Display a listing of the especialidades.
     *
     * @return Response
     */
    public function getEspecialidades()
    {
        if(Datatable::shouldHandle())
        {
            $col = Especialidad::all();

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    $name = $model->name;
                    if($model->es_multi)
                    {
                        $name = $name ." [MULTI]";
                    }
                    return "<a href='". route('manage.especialidades.ficha',[$model->id]) ."'>$name</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.especialidades.index');
    }

    public function getEspecialidadNuevo()
    {
        return view('manage.especialidades.new');
    }

    public function getEspecialidadUpdate($id)
    {
        $ficha = Especialidad::find($id);

        return view('manage.especialidades.ficha', compact('ficha'));
    }

    public function postEspecialidadUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $o = Especialidad::find($id);

        if(!$id)
        {
            $o = new Especialidad;

            $o->name = $request->get('name');
            $o->es_multi = $request->has('es_multi');
            $o->save();

            SystemLog::addLog($o,'Nuevo');
        }
        else
        {
            $o1 = Especialidad::find($id);

            $o->name = $request->get('name');
            $o->es_multi = $request->has('es_multi');
            $o->save();

            SystemLog::addLog($o,'Modificado',$o1);
        }

        return redirect()->route('manage.especialidades.index');
    }

    /**
     * SUBESPECIALIDADES
     */
    /**
     * Display a listing of the subcategorias.
     *
     * @return Response
     */
    public function getSubespecialidades()
    {
        $category_id = Input::get('especialidad_id');
        if($category_id>0)
        {
            return response()->json(Subespecialidad::where('especialidad_id',$category_id)->pluck('name','id'), 200);
        }

        if(Datatable::shouldHandle())
        {
            return Datatable::collection( Subespecialidad::all() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.subespecialidades.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('especialidad', function($model) {
                    return $model->especialidad->name;
                })
                ->addColumn('es_multi', function($model) {
                    return $model->especialidad->es_multi ? "MULTI" : "";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name','especialidad','es_multi')
                ->orderColumns('name','especialidad','es_multi')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.subespecialidades.index');
    }

    public function getSubespecialidadNuevo()
    {
        $especialidades = Especialidad::pluck('name','id');
        return view('manage.subespecialidades.new', compact('ficha', 'especialidades'));
    }

    public function getSubespecialidadUpdate($id)
    {
        $ficha = Subespecialidad::find($id);

        $especialidades = Especialidad::pluck('name','id');

        return View::make('manage.subespecialidades.ficha', compact('ficha', 'especialidades'));
    }

    public function postSubespecialidadUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'especialidad_id' => 'required',
            'name' => 'required',
        ]);

        $o = Subespecialidad::find($id);

        if(!$id)
        {
            $o = new Subespecialidad;

            $o->especialidad_id = $request->input('especialidad_id');
            $o->name = $request->input('name');
            $o->save();

            SystemLog::addLog($o,'Nuevo');
        }
        else
        {
            $o1 = Subespecialidad::find($id);

            $o->especialidad_id = $request->input('especialidad_id');
            $o->name = $request->input('name');
            $o->save();

            SystemLog::addLog($o,'Modificado',$o1);
        }

        return redirect()->route('manage.subespecialidades.index');
    }

    //Tareas
    public function getAvisoTareas(Request $request)
    {
        $user = $request->user();
        $user_id = $user->id;

        $ret = [
            'hora'  => Carbon::now()->format('Y-m-d H:i:s'),
            'total' => 0,
            'total_viajero' => 0,
            'tareas_viajero'=> null,
            'total_booking' => 0,
            'tareas_booking'=> null,
            'texto'
        ];

        //ViajeroTarea
        $tareas = \VCN\Models\Leads\ViajeroTarea::vencidas()->where('asign_to', $user_id)->get();

        //Solicitudes
        $solicitudes = \VCN\Models\Solicitudes\Solicitud::where('user_id',$user_id)->get();
        $tareas_sol = collect([]);
        foreach($solicitudes as $sol)
        {
            $t = \VCN\Models\Leads\ViajeroTarea::vencidas()->where('viajero_id', $sol->viajero_id)->get();
            $tareas_sol = $tareas_sol->merge($t);
        }

        $tareas = $tareas->merge($tareas_sol);
        $tareas = $tareas->unique();

        foreach($tareas as $tarea)
        {
            $tarea->fecha_dmy = Carbon::parse($tarea->fecha)->format('d/m/Y H:i');
            $tarea->info_txt = $tarea->viajero->full_name;
            $tarea->info_link = route('manage.viajeros.ficha', $tarea->viajero_id);
        }

        $ret['tareas_viajero'] = $tareas;
        $ret['total_viajero'] = $tareas->count();
        $ret['total'] += $ret['total_viajero'];

        //Bookings
        $tareas = \VCN\Models\Bookings\BookingTarea::vencidas()->where('asign_to', $user_id);
        if($tareas->count())
        {
            $tareas = $tareas->get();
            foreach($tareas as $tarea)
            {
                $tarea->fecha_dmy = Carbon::parse($tarea->fecha)->format('d/m/Y H:i');
                $tarea->info_txt = "Booking ". $tarea->incidencia->booking_id ." (". $tarea->incidencia->booking->viajero->full_name .")";
                $tarea->info_link = route('manage.bookings.ficha', $tarea->incidencia->booking_id);
            }

            $ret['total_booking'] = $tareas->count();
            $ret['tareas_booking'] = $tareas;
        }
        $ret['total'] += $ret['total_booking'];

        return response()->json($ret, 200);
    }

    public function postAvisoTareas(Request $request)
    {
        $result = true;

        $model = $request->get('modelo');
        $boton = $request->get('boton');
        $id = $request->get('id');

        $tarea = null;

        switch($model)
        {
            case 'ViajeroTarea':
                $tarea =  \VCN\Models\Leads\ViajeroTarea::find($id);
            break;

            case 'BookingTarea':
                $tarea =  \VCN\Models\Bookings\BookingTarea::find($id);
            break;
        }

        if(!$tarea)
        {
            $result = false;
            return response()->json($result, 200);
        }

        $fecha = Carbon::parse($tarea->fecha);

        switch($boton)
        {
            case 1: //5 Minutos
            {
                $fecha = $fecha->addMinutes(5);
            }
            break;

            case 2: //15 Minutos
            {
                $fecha = $fecha->addMinutes(15);
            }
            break;

            case 3: //30 Minutos
            {
                $fecha = $fecha->addMinutes(30);
            }
            break;

            case 4: //1 hora
            {
                $fecha = $fecha->addHours(1);
            }
            break;

            case 5: //1 dia
            {
                $fecha = $fecha->addDays(1);
            }
            break;
        }

        $tarea->fecha = $fecha->format('Y-m-d H:i');
        $tarea->save();

        return response()->json($result, 200);
    }

    public function setLOPD(Request $request, $esViajero, $id)
    {
        // return view('lopd');
        // $token = '$2y$10$Vui0/pcYbcvJhad/zI91gu02D4TbPcuoQ66sL4T4LX7SE4gAiBpqa';
        
        $token = $request->get('token');
        if( !$token )
        {
            return response()->json(['error' => 'Not authorized.'], 403);
        }

        $key = config('app.key');
        if( !Hash::check($key, $token) )
        {
            return response()->json(['error' => 'Invalid Token.'], 403);
        }

        $id = (int)substr($id, 5);

        $fecha = Carbon::now()->format('d/m/Y - H:i');
        $ip = $request->ip();
        
        $ficha = null;
        if($esViajero == 1)
        {
            $ficha = Viajero::find($id);
            \VCN\Models\Leads\ViajeroLog::addLog($ficha, "Nueva LOPD [$fecha] [$ip] mailing");
        }
        elseif($esViajero == 0)
        {
            $ficha = Tutor::find($id);
            \VCN\Models\Leads\TutorLog::addLog($ficha, "Nueva LOPD [$fecha] [$ip] mailing");
        }

        if(!$ficha)
        {
            return response()->json(['error' => 'Error.'], 403);
        }

        if($ficha)
        {
            $ficha->optout = false;
            $ficha->optout_fecha = null;
            $ficha->lopd_check2 = true;
            $ficha->save();
        }

        $plataforma = Plataforma::find($ficha->plataforma?:1);

        return view('lopd', compact('ficha', 'plataforma'));
    }


    public function getDatos(Request $request)
    {
        $ficha = $request->user();

        return view('manage.mis_datos', compact('ficha'));
    }

    public function postDatos(Request $request)
    {
        $ficha = $request->user();

        if( $request->has('submit_avatar_reset') )
        {
            $ficha->avatar = null;
            $ficha->save();

            Session::flash('tab',"#avatar");
        }
        elseif( $request->has('submit_avatar') )
        {
            $foto = null;
            if( $request->get('webcam64') != "" )
            {
                $foto = $request->get('webcam64');
            }
            elseif($request->hasFile('avatar'))
            {
                $file = $request->file('avatar');
                $dirp = "assets/uploads/users/". $ficha->id . "/";

                $foto = ConfigHelper::uploadFoto($file, $dirp, 400, true);
            }

            if($foto)
            {
                $ficha->avatar = $foto;
                $ficha->save();
            }

            Session::flash('tab',"#avatar");
        }


        return redirect()->back();
    }

}
