<?php

namespace VCN\Http\Controllers\Manage\Alojamientos;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Alojamientos\PrecioRepository as Precio;
use VCN\Models\Alojamientos\Alojamiento;
use VCN\Models\Alojamientos\AlojamientoPrecio;
use VCN\Models\Alojamientos\AlojamientoPrecioExtra;

use VCN\Models\Monedas\Moneda;

use Datatable;
use Input;
use Session;
use Carbon;

use VCN\Helpers\ConfigHelper;

class AlojamientoPreciosController extends Controller
{
    private $precio;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Precio $precio )
    {
        // $this->checkPermisos('precios');

        $this->middleware("permiso.edit:precios", ['only' => ['postUpdate','getNuevo','destroy']]);
        $this->middleware("permiso.view:precios", ['only' => ['getUpdate']]);


        $this->precio = $precio;
    }


    public function getIndex($alojamiento_id=0, $extras=false)
    {
        if(Datatable::shouldHandle())
        {
            if($extras)
            {
                if($alojamiento_id>0)
                {
                    $col = AlojamientoPrecioExtra::where('alojamiento_id',$alojamiento_id)->get();
                }
                else
                {
                    $col = AlojamientoPrecioExtra::all();
                }
            }
            else
            {
                if($alojamiento_id>0)
                {
                    $col = $this->precio->findAllBy('alojamiento_id',$alojamiento_id);
                }
                else
                {
                    $col = $this->precio->all();
                }
            }


            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return $model->name;
                    // return "<a href='". route('manage.alojamientos.precios.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('periodo', function($model) {
                    return Carbon::parse($model->desde)->format('d/m/Y') ." - ". Carbon::parse($model->hasta)->format('d/m/Y');
                })
                ->addColumn('precio', function($model) {
                    return $model->importe ." ". $model->moneda_name;
                })
                ->addColumn('regla', function($model) {
                    return ConfigHelper::getPrecioRegla($model);
                })
                ->addColumn('options', function($model) use ($extras) {

                    $ret = "";

                    if(ConfigHelper::canEdit('precios'))
                    {
                        $data = " data-label='Borrar' data-model='Precio' data-action='". route( 'manage.alojamientos.precios.delete', [$model->id,$extras]) . "'";
                        $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";
                    }

                    // $ret .= "<a href='$model->id' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Centro</a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('*','duracion','rango1')
                // ->orderColumns('descripcion','alojamiento')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.alojamientos.precios.index', compact('alojamiento_id'));
    }

    /*public function getNuevo($alojamiento_id=0)
    {
        $alojamiento = Alojamiento::find($alojamiento_id)->accommodation_name;

        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();

        return view('manage.alojamientos.precios.new', compact('alojamiento_id', 'alojamiento', 'monedas'));
    }
    */

    public function getUpdate($id, $extras=false)
    {
        $ficha = $extras?AlojamientoPrecioExtra::find($id):$this->precio->find($id);

        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();

        return view('manage.alojamientos.precios.ficha', compact('ficha', 'monedas','extras'));
    }

    public function postUpdate(Request $request, $id=0, $extras=false)
    {
        if($extras)
        {
            Session::flash('tab','#precio-extras');
        }
        else
        {
            Session::flash('tab','#precios');
        }

        $alojamiento_id = $request->input('alojamiento_id');

        if($request->has('test'))
        {
            $desde = $request->input('desde');
            $hasta = $request->input('hasta');
            $alojamiento_id = $request->input('alojamiento_id');
            $test = $request->input('test');

            $alojamiento = Alojamiento::find($alojamiento_id);

            $p = $alojamiento->calcularPrecio($desde,$hasta,$test);

            Session::flash('test',$p?ConfigHelper::parseMoneda($p['importe'],$p['moneda']):"-");
            return redirect()->route('manage.alojamientos.ficha',$alojamiento_id)->withInput();
        }

        $this->validate($request, [
            'desde'     => 'required',
            'hasta'     => 'required',
            'moneda_id' => 'required',
            'importe'   => 'required',
        ]);

        $data = Input::except('_token');

        // if( Carbon::createFromFormat('d/m/Y',$data['desde'])->format('Y') != Carbon::createFromFormat('d/m/Y',$data['hasta'])->format('Y') )
        // {
        //     Session::flash('mensaje-alert', "Las fechas de inicio y fin deben ser del mismo año.");
        //     return redirect()->route('manage.alojamientos.ficha',$alojamiento_id)->withInput();
        // }

        $data['desde'] = Carbon::createFromFormat('d/m/Y',$data['desde'])->format('Y-m-d');
        $data['hasta'] = Carbon::createFromFormat('d/m/Y',$data['hasta'])->format('Y-m-d');

        //mismo año no

        if(!$id)
        {
            //nuevo
            if($extras)
            {
                $o = AlojamientoPrecioExtra::create($data);
                $alojamiento_id = $o->alojamiento_id;
            }
            else
            {
                $o = $this->precio->create($data);
                $alojamiento_id = $o->alojamiento_id;
            }
        }
        else
        {
            if($extras)
            {
                $o = AlojamientoPrecioExtra::find($id);
                $o->update($data);
                $alojamiento_id = $o->alojamiento_id;
            }
            else
            {
                $this->precio->update($data, $id);
                $alojamiento_id = $this->precio->find($id)->alojamiento_id;
            }
        }

        return redirect()->route('manage.alojamientos.ficha',$alojamiento_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, $extras=false)
    {
        if($extras)
        {
            Session::flash('tab','#precio-extras');

            $precio = AlojamientoPrecioExtra::find($id);
            $alojamiento_id = $precio->alojamiento_id;
            $precio->delete();
        }
        else
        {
            Session::flash('tab','#precios');

            $alojamiento_id = $this->precio->find($id)->alojamiento_id;
            $this->precio->delete($id);
        }

        return redirect()->route('manage.alojamientos.ficha',$alojamiento_id);
    }
}
