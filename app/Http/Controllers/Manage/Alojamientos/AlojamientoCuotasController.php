<?php

namespace VCN\Http\Controllers\Manage\Alojamientos;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Alojamientos\CuotaRepository as Cuota;
use VCN\Models\Alojamientos\Alojamiento;
use VCN\Models\Alojamientos\AlojamientoPrecio;

use VCN\Models\Monedas\Moneda;
use VCN\Models\Extras\ExtraUnidad;

use Datatable;
use Input;
use Session;
use Carbon;

use VCN\Helpers\ConfigHelper;

class AlojamientoCuotasController extends Controller
{
    private $cuota;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Cuota $cuota )
    {
        $this->checkPermisos('extras');

        $this->cuota = $cuota;
    }

    public function getIndex($alojamiento_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->cuota->all();
            if($alojamiento_id>0)
            {
                $col = Alojamiento::find($alojamiento_id)->cuotas;
            }

            return Datatable::collection( $col )
                ->showColumns('accommodations_quota_price')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.alojamientos.cuotas.ficha',[$model->id]) ."'>$model->accommodations_quota_name</a>";
                })
                ->addColumn('precio', function($model) {
                    return ConfigHelper::parseMoneda($model->accommodations_quota_price,$model->moneda);
                    // return Moneda::find($model->accommodations_quota_currency_id)->name;
                })
                ->addColumn('unidades', function($model) {
                    $ret = ConfigHelper::getTipoUnidad($model->cuota_unidad);

                    if($model->cuota_unidad)
                    {
                        $ret .= " (". ($model->tipo?$model->tipo->name:'-') .")";
                    }

                    return $ret;
                })
                ->addColumn('requerido', function($model) {
                    return $model->required?"<span class='label label-success'><i class='fa fa-check-circle fa-1x'></i></span>":"<span class='label label-danger'><i class='fa fa-times-circle fa-1x'></i></span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Cuota' data-action='". route( 'manage.alojamientos.cuotas.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= "<a href='$model->id' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Centro</a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.alojamientos.cuotas.index', compact('alojamiento_id'));
    }

    public function getNuevo($alojamiento_id=0)
    {
        $alojamiento = Alojamiento::find($alojamiento_id);

        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();

        $unidades = ['' => ''] + ExtraUnidad::pluck('name','id')->toArray();
        $unidades_tipo = ConfigHelper::getTipoUnidad();

        return view('manage.alojamientos.cuotas.new', compact('alojamiento', 'monedas','unidades','unidades_tipo'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->cuota->find($id);

        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();

        $unidades = ['' => ''] + ExtraUnidad::pluck('name','id')->toArray();
        $unidades_tipo = ConfigHelper::getTipoUnidad();

        return view('manage.alojamientos.cuotas.ficha', compact('ficha', 'monedas','unidades','unidades_tipo'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        Session::flash('tab','#cuotas');

        $this->validate($request, [
            'accommodations_quota_name' => 'required',
            'accommodations_quota_currency_id' => 'required',
            'accommodations_quota_price' => 'required',
        ]);

        $data = $request->except('_token');

        $data['required'] = $request->has('required');
        $data['cuota_unidad_id'] = $request->get('cuota_unidad_id') ?: 0;

        if(!$id)
        {
            //nuevo
            $o = $this->cuota->create($data);
            $alojamiento_id = $o->accommodation_id;
        }
        else
        {
            $this->cuota->update($data, $id);

            $alojamiento_id = $this->cuota->find($id)->accommodation_id;
        }

        return redirect()->route('manage.alojamientos.ficha',$alojamiento_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $alojamiento_id = $this->cuota->find($id)->accommodation_id;

        $this->cuota->delete($id);

        Session::flash('tab','#cuotas');

        return redirect()->route('manage.alojamientos.ficha',$alojamiento_id);
    }
}
