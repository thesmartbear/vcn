<?php

namespace VCN\Http\Controllers\Manage\Alojamientos;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Alojamientos\AlojamientoTipo;

use Datatable;
use Input;

class AlojamientoTiposController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->checkPermisos('alojamiento-tipos');
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection( AlojamientoTipo::all() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.alojamientos.tipos.ficha',[$model->id]) ."'>$model->accommodation_type_name</a>";
                })
                ->addColumn('familia', function($model) {
                    return $model->es_familia?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Tipo Alojamiento' data-action='". route( 'manage.alojamientos.tipos.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.alojamientos.tipos.index');
    }

    public function getNuevo()
    {
        return view('manage.alojamientos.tipos.new');
    }

    public function getUpdate($id)
    {
        $ficha = AlojamientoTipo::find($id);

        return view('manage.alojamientos.tipos.ficha', compact('ficha'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'accommodation_type_name' => 'required',
        ]);

        $o = AlojamientoTipo::find($id);

        if(!$id)
        {
            $o = new AlojamientoTipo;
        }

        $o->accommodation_type_name = $request->input('accommodation_type_name');
        $o->es_familia = $request->has('es_familia');
        $o->notas = $request->input('notas');
        $o->save();

        return redirect()->route('manage.alojamientos.tipos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $o = AlojamientoTipo::find($id);
        $o->delete();

        return redirect()->route('manage.alojamientos.tipos.index');
    }
}
