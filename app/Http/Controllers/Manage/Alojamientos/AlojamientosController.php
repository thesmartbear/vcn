<?php

namespace VCN\Http\Controllers\Manage\Alojamientos;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Alojamientos\AlojamientoRepository as Alojamiento;
use VCN\Models\Centros\Centro;
use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Alojamientos\AlojamientoTipo;
use VCN\Models\Cursos\Curso;
use VCN\Models\Monedas\Moneda;

use Datatable;
use Input;
use Validator;
use File;
use Session;
use Image;
use ConfigHelper;

use VCN\Repositories\Criteria\FiltroPlataformaCentro;

class AlojamientosController extends Controller
{
    private $alojamiento;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Alojamiento $alojamiento )
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');

        $this->checkPermisos('proveedores');

        $this->alojamiento = $alojamiento;
    }

    public function getListJson(Request $request)
    {
        if( !$request->ajax() )
        {
            abort(404);
        }

        $centro_id = $request->input('centro_id');

        $alojamientos = $this->alojamiento->findWhere(['center_id'=>$centro_id])->pluck('full_name','id');

        return response()->json($alojamientos, 200);
    }

    public function getIndex($centro_id=0)
    {
        $this->alojamiento->pushCriteria(new FiltroPlataformaCentro());

        if(Datatable::shouldHandle())
        {
            $col = $this->alojamiento->all();
            if($centro_id>0)
            {
                $col = Centro::find($centro_id)->alojamientos->sortBy('accommodation_name');
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.alojamientos.ficha',[$model->id]) ."'>$model->accommodation_name</a>";
                })
                ->addColumn('tipo', function($model) {
                    return $model->tipo->accommodation_type_name;
                })
                ->addColumn('centro', function($model) {
                    return $model->centro?$model->centro->name:"-";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Alojamiento' data-action='". route( 'manage.alojamientos.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= "<a href='". route('manage.alojamientos.precios.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Precio</a>";
                    $ret .= " <a href='". route('manage.alojamientos.ficha',$model->id) ."#precios' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Precio</a>";
                    $ret .= " <a href='". route('manage.alojamientos.cuotas.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Cuota</a>";

                    return $ret;
                })
                ->searchColumns('name', 'centro')
                ->orderColumns('name', 'tipo', 'centro')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        return view('manage.alojamientos.index', compact('centro_id'));
    }

    public function getIndexByProveedor($proveedor_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->alojamiento->all();
            if($proveedor_id)
            {
                $col = Proveedor::find($proveedor_id)->alojamientos;
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.alojamientos.ficha',[$model->id]) ."'>$model->accommodation_name</a>";
                })
                ->addColumn('tipo', function($model) {
                    return $model->tipo->accommodation_type_name;
                })
                ->addColumn('centro', function($model) {
                    return $model->centro?$model->centro->name:"-";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Alojamiento' data-action='". route( 'manage.alojamientos.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= "<a href='$model->id' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Centro</a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.alojamientos.index_proveedor', compact('proveedor_id'));
    }

    public function getIndexByCurso($curso_id)
    {
        if(Datatable::shouldHandle())
        {
            $col = Curso::find($curso_id)->alojamientos;

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.alojamientos.ficha',[$model->id]) ."'>$model->accommodation_name</a>";
                })
                ->addColumn('tipo', function($model) {
                    return $model->tipo->accommodation_type_name;
                })
                ->addColumn('centro', function($model) {
                    return $model->centro?$model->centro->name:"-";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Alojamiento' data-action='". route( 'manage.alojamientos.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= "<a href='$model->id' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Centro</a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }
    }

    public function getNuevo($centro_id=0)
    {
        $centros = [""=>""] + Centro::plataforma()->pluck('name','id')->toArray();
        $tipos = [""=>""] + AlojamientoTipo::pluck('accommodation_type_name','id')->toArray();

        $centro = null;
        if($centro_id)
        {
            $centro = Centro::find($centro_id);
        }

        return view('manage.alojamientos.new', compact('centro_id','centro','tipos','centros'));
    }

    public function getUpdate($id)
    {
        $tipos = AlojamientoTipo::pluck('accommodation_type_name','id');

        $ficha = $this->alojamiento->find($id);
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();

        $regla = 0;
        if($ficha->precios->count()>0)
        {
            $regla = $ficha->precios->first();
        }

        if(!$ficha->image_dir)
        {
            $ficha->image_dir = "alojamiento_$id";
            $ficha->save();
        }

        return view('manage.alojamientos.ficha', compact('ficha','tipos','monedas','regla'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        if(!$id)
        {
            $this->validate($request, [
                'center_id' => 'required|numeric|min:1',
            ]);
        }

        $this->validate($request, [
            'accommodation_name' => 'required|max:255',
            'accommodation_type_id' => 'required|numeric|min:1',
        ]);

        if(!$id)
        {
            //nuevo
            $o = $this->alojamiento->create(Input::all());
            $id = $o->id;

            $o->image_dir = "alojamiento_$id";
            $o->save();
        }

        $this->alojamiento->update(Input::except('_token'), $id);

        $ficha = $this->alojamiento->find($id);

        foreach($ficha->cursos as $curso)
        {
            // $curso->updateDataWeb(true);
            $job = new \VCN\Jobs\JobDataWeb($curso);
            app('Illuminate\Contracts\Bus\Dispatcher')->dispatch($job);
        }

        return redirect()->route('manage.alojamientos.ficha',$id);
    }

    public function destroy($id)
    {
        $this->alojamiento->delete($id);

        return redirect()->route('manage.alojamientos.index');
    }


    public function postFotoUpload(Request $request, $id)
    {
        $c = $this->alojamiento->find($id);
        
        $dirp = "assets/uploads/alojamiento/". $c->image_dir . "/";
        $dir = public_path($dirp);

        /* $rules = array(
            'file' => 'image|max:30000',
        );

        $input = Input::all();
        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return response("Error", 400);
        } */

        if (Input::hasFile('file'))
        {
            $file = $request->file('file');
            ConfigHelper::uploadOptimize($file, $dirp);
            return response()->json('success', 200);

            /*
            $file = Input::file('file');
            // $file = str_slug($file->getClientOriginalName()) .".". $file->getClientOriginalExtension();

            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file = str_slug($file_name) .".". $file->getClientOriginalExtension();

            // $file = Input::file('file')->getClientOriginalName();
            Input::file('file')->move($dir, $file);

            $img = Image::make($dir.$file);
            $img->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            //thumb
            if (!file_exists($dir."thumb/"))
            {
                File::makeDirectory($dir."thumb/", 0775, true);
            }
            File::copy($dir.$file, $dir."thumb/".$file);
            $img2 = Image::make($dir."thumb/".$file);
            $img2->resize(450, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            */
        }

        return response()->json('success', 200);
    }

    public function postFotoUploadDelete($id)
    {
        $c = $this->alojamiento->find($id);
        $dir = public_path("assets/uploads/alojamiento/". $c->image_dir . "/");

        $file = Input::get('f');
        File::delete($dir.$file);

        return response()->json('success', 200);
    }

    public function deleteFoto($id, $file)
    {
        $c = $this->alojamiento->find($id);
        $dir = public_path("assets/uploads/alojamiento/". $c->image_dir . "/");
        File::delete($dir.$file);

        Session::flash('tab','#imagenes');

        return redirect()->route('manage.alojamientos.ficha',$id);
    }

    public function setFotoPortada($id, $file)
    {
        $c = $this->alojamiento->find($id);

        $c->image_portada = $file;
        $c->save();

        Session::flash('tab','#imagenes');

        return redirect()->route('manage.alojamientos.ficha',$id);
    }
}
