<?php

namespace VCN\Http\Controllers\Manage\Prescriptores;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Prescriptores\CategoriaRepository as Categoria;
use VCN\Repositories\Prescriptores\CategoriaComisionRepository as CategoriaComision;
use VCN\Repositories\Prescriptores\PrescriptorRepository as Prescriptor;

use VCN\Models\System\Oficina;

use VCN\Models\Prescriptores\Categoria as CategoriaModel;
use VCN\Models\Prescriptores\CategoriaComision as CategoriaComisionModel;

use VCN\Models\Bookings\Booking;

use Datatable;
use Session;
use ConfigHelper;
use DB;
use Carbon;

class PrescriptoresController extends Controller
{

    private $categoria;
    private $categoriacom;
    private $prescriptor;

    /**
     * Instantiate a new PrescriptoresController instance.
     *
     * @return void
     */
    public function __construct( Prescriptor $prescriptor, Categoria $categoria, CategoriaComision $categoriacom )
    {
        $this->checkPermisos('prescriptores');

        $this->categoria = $categoria;
        $this->categoriacom = $categoriacom;
        $this->prescriptor = $prescriptor;
    }

    /**
     * Prescriptor
     */
    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->prescriptor->all();

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.prescriptores.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('categoria', function($model) {
                    return $model->categoria->name;
                })
                ->addColumn('categoria_com', function($model) {
                    return $model->categoria_comision?$model->categoria_comision->name:"-";
                })
                ->addColumn('oficina', function($model) {
                    return ($model->oficina_id>0)?$model->oficina->name:"Todos";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Prescriptor' data-action='". route( 'manage.prescriptores.delete', $model->id) . "'";
                    $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','*')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.prescriptores.index');
    }

    public function getNuevo()
    {
        $categorias = [""=>""] + CategoriaModel::plataforma()->pluck('name','id')->toArray();
        $categoriascom = [""=>""] + CategoriaComisionModel::plataforma()->pluck('name','id')->toArray();
        $oficinas = [0=>"Todos"] + Oficina::plataforma()->pluck('name','id')->toArray();

        return view('manage.prescriptores.new', compact('categorias','categoriascom','oficinas'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->prescriptor->find($id);

        $categorias = [""=>""] + CategoriaModel::plataforma()->pluck('name','id')->toArray();
        $categoriascom = [""=>""] + CategoriaComisionModel::plataforma()->pluck('name','id')->toArray();
        $oficinas = [0=>"Todos"] + Oficina::plataforma()->pluck('name','id')->toArray();

        //Ventas
        $totales = null;

        $valores['prescriptores'] = $id;

        $user = auth()->user();
        if(!$user->filtro_oficinas)
        {
            $valores['oficinas'] = $user->oficina_id;
        }

        $moneda = ConfigHelper::default_moneda();

        $anyd = 2015;
        $anyh = intval(Carbon::now()->format('Y'));
        for ($i = $anyd; $i <= $anyh; $i++)
        {
            $totNum = 0;
            $totSemanas = 0;
            $totCursos = 0;
            $totTotal = 0;

            $valores['desdes'] = "01/01/$i";
            $valores['hastas'] = "31/12/$i";

            $bookings = Booking::listadoFiltros($valores);

            foreach($bookings->get() as $b)
            {
                $totNum += 1;
                $totSemanas += $b->semanas;
                $totTotal += $b->total ;

                $total = $b->course_total_amount;
                $m_id = $b->course_currency_id;
                if($m_id != $moneda->id)
                {
                    $total = $total * $b->getMonedaTasa($m_id);
                }
                $totCursos += $total ;
            }

            $totales[$i]['total_num'] = $totNum;
            $totales[$i]['total_sem'] = $totSemanas;
            $totales[$i]['total_curso'] = $totCursos;
            $totales[$i]['total'] = $totTotal;

        }

        return view('manage.prescriptores.ficha', compact('ficha','categorias','categoriascom','oficinas','totales'));
    }

    public function postUpdate(Request $request, $id)
    {
        $data = $request->except('_token','tab');

        $tab = $request->input('tab');

        if($tab=="#ficha")
        {
            $this->validate($request, [
                'name' => 'required',
                'categoria_id' => 'required|min:1',
                'comision_cat_id' => 'required|min:1',
                'oficina_id' => 'required|min:1',
            ]);
        }

        $data['area'] = $request->has('area');
        $data['area_pagos'] = $request->has('area_pagos');
        $data['area_reunion'] = $request->has('area_reunion');
        $data['no_facturar'] = $request->has('no_facturar');

        if(!$id)
        {
            $ficha = $this->prescriptor->create($data);
        }
        else
        {
            $this->prescriptor->update($data, $id);
            $ficha = $this->prescriptor->find($id);
        }

        Session::flash('tab',$tab);

        return redirect()->route('manage.prescriptores.ficha',$ficha);
    }

    public function destroy($id)
    {
        $this->prescriptor->delete($id);
        return redirect()->route('manage.prescriptores.index');
    }

    /**
     * Categorias
     */
    public function getIndexCategoria()
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->categoria->all();

            $filtro = ConfigHelper::config('propietario');
            if( $filtro && !auth()->user()->isFullAdmin() )
            {
                $col1 = $this->categoria->findWhere(['propietario'=> $filtro]);
                $col = $this->categoria->findWhere(['propietario'=> 0]);

                $col = $col->merge($col1);
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.prescriptores.categorias.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Categoria Prescriptor' data-action='". route( 'manage.prescriptores.categorias.delete', $model->id) . "'";
                    $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','*')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.prescriptores.categorias.index');
    }

    public function getNuevoCategoria()
    {
        return view('manage.prescriptores.categorias.new');
    }

    public function getUpdateCategoria($id)
    {
        $ficha = $this->categoria->find($id);

        return view('manage.prescriptores.categorias.ficha', compact('ficha'));
    }

    public function postUpdateCategoria(Request $request, $id)
    {
        $data = $request->except('_token');

        $this->validate($request, [
            'name' => 'required',
        ]);

        $data['propietario'] = $request->has('propietario')?$request->input('propietario'):0;
        if( $request->has('propietario_check') )
        {
            $data['propietario'] = ConfigHelper::config('propietario');
        }

        if(!$id)
        {
            $ficha = $this->categoria->create($data);
        }
        else
        {
            $this->categoria->update($data, $id);
        }

        return redirect()->route('manage.prescriptores.categorias.index');
    }

    public function destroyCategoria($id)
    {
        $this->categoria->delete($id);
        return redirect()->route('manage.prescriptores.categorias.index');
    }

    /**
     * Categorias Comision
     */
    public function getIndexCategoriaCom()
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->categoriacom->all();

            $filtro = ConfigHelper::config('propietario');
            if( $filtro && !auth()->user()->isFullAdmin() )
            {
                $col1 = $this->categoriacom->findWhere(['propietario'=> $filtro]);
                $col = $this->categoriacom->findWhere(['propietario'=> 0]);

                $col = $col->merge($col1);
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.prescriptores.categorias-comision.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Categoria Comisión' data-action='". route( 'manage.prescriptores.categorias-comision.delete', $model->id) . "'";
                    $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','*')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.prescriptores.categorias-comision.index');
    }

    public function getNuevoCategoriaCom()
    {
        return view('manage.prescriptores.categorias-comision.new');
    }

    public function getUpdateCategoriaCom($id)
    {
        $ficha = $this->categoriacom->find($id);

        return view('manage.prescriptores.categorias-comision.ficha', compact('ficha'));
    }

    public function postUpdateCategoriaCom(Request $request, $id)
    {
        $data = $request->except('_token');

        $this->validate($request, [
            'name' => 'required',
        ]);

        $data['propietario'] = $request->has('propietario')?$request->input('propietario'):0;
        if( $request->has('propietario_check') )
        {
            $data['propietario'] = ConfigHelper::config('propietario');
        }

        if(!$id)
        {
            $ficha = $this->categoriacom->create($data);
        }
        else
        {
            $this->categoriacom->update($data, $id);
            // $ficha = $this->categoria->find($id);
        }

        return redirect()->route('manage.prescriptores.categorias-comision.index');
    }

    public function destroyCategoriaCom($id)
    {
        $this->categoriacom->delete($id);
        return redirect()->route('manage.prescriptores.categorias-comision.index');
    }

    public function getVentas(Request $request, $id, $any)
    {
        $valores['prescriptores'] = $id;
        $valores['desdes'] = "01/01/$any";
        $valores['hastas'] = "31/12/$any";

        $user = auth()->user();
        if(!$user->filtro_oficinas)
        {
            $valores['oficinas'] = $user->oficina_id;
        }

        $bookings = Booking::listadoFiltros($valores);
        // $bookings = $bookings->select('prescriptor_id', DB::raw('count(id) as total_bookings'), DB::raw('sum(semanas) as total_semanas'), DB::raw('sum(course_total_amount) as total_curso'),DB::raw('sum(total) as total_total') );

        $col = collect();

        $moneda = ConfigHelper::default_moneda();

        $totNum = 0;
        $totSemanas = 0;
        $totCursos = 0;
        $totTotal = 0;
        foreach($bookings->get() as $booking)
        {
            $total = $booking->course_total_amount;
            $m_id = $booking->course_currency_id;
            if($m_id != $moneda->id)
            {
                $total = $total * $booking->getMonedaTasa($m_id);
            }

            if(!$col->contains('categoria_id',$booking->curso->category_id))
            {
                $obj['categoria_id'] = $booking->curso->category_id;
                $obj['name'] = $booking->curso->categoria?$booking->curso->categoria->name:"-";
                $obj['num'] = 1;
                $obj['total_sem'] = $booking->semanas_unit;
                $obj['total_curso'] = $total;
                $obj['total'] = $booking->total;

                $col->push($obj);
            }
            else
            {
                $col = $col->map(function ($item, $key) use ($booking,$total) {
                    if($item['categoria_id']==$booking->curso->category_id)
                    {
                        $item['num'] += 1;
                        $item['total_sem'] += $booking->semanas_unit;
                        $item['total_curso'] += $total;
                        $item['total'] += $booking->total;
                    }

                    return $item;
                });
            }

            $totNum += 1;
            $totSemanas += $booking->semanas_unit;
            $totCursos += $total;
            $totTotal += $booking->total;
        }

        $totales['total_num'] = $totNum;
        $totales['total_sem'] = $totSemanas;
        $totales['total_curso'] = $totCursos;
        $totales['total'] = $totTotal;

        if(Datatable::shouldHandle())
        {

            return Datatable::collection( $col )
                ->addColumn('categoria', function($model) {
                    return $model['name'];
                })
                ->addColumn('num', function($model) {
                    return $model['num'];
                })
                ->addColumn('total_sem', function($model) {
                    return $model['total_sem'];
                })
                ->addColumn('total_curso', function($model) {
                    return $model['total_curso'];
                })
                ->addColumn('total', function($model) {
                    return $model['total'];
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }
    }

    public function getVentasDetalle(Request $request, $id, $any)
    {
        $valores['prescriptores'] = $id;
        $valores['desdes'] = "01/01/$any";
        $valores['hastas'] = "31/12/$any";

        $user = auth()->user();
        if(!$user->filtro_oficinas)
        {
            $valores['oficinas'] = $user->oficina_id;
        }

        $bookings = Booking::listadoFiltros($valores);
        // $bookings = $bookings->select('prescriptor_id', DB::raw('count(id) as total_bookings'), DB::raw('sum(semanas) as total_semanas'), DB::raw('sum(course_total_amount) as total_curso'),DB::raw('sum(total) as total_total') );

        if(Datatable::shouldHandle())
        {

            $moneda = ConfigHelper::default_moneda();

            return Datatable::collection( $bookings->get() )
                ->addColumn('viajero', function($model) {
                    return "<a href='". route('manage.viajeros.ficha',[$model->viajero->id]) ."'>". $model->viajero->full_name ."</a>";
                })
                ->addColumn('curso', function($model) {
                    return $model->curso->name;
                })
                ->addColumn('convocatoria', function($model) {
                    return $model->convocatoria?$model->convocatoria->name:"-";
                })
                ->addColumn('semanas', function($model) {
                    return $model->semanas_unit;
                })
                ->addColumn('curso_total', function($model) use ($moneda) {
                    $total = $model->course_total_amount;
                    $m_id = $model->course_currency_id;
                    if($m_id != $moneda->id)
                    {
                        $total = $total * $model->getMonedaTasa($m_id);
                    }

                    return $total;
                })
                ->addColumn('fecha', function($model) {
                    $fecha = $model->fecha_reserva?$model->fecha_reserva->format('Y-m-d'):'-';
                    return $fecha;
                })
                ->addColumn('total', function($model) {
                    return $model->total;
                })
                ->addColumn('options', function($model) {
                    return "<a href='". route('manage.bookings.ficha',[$model->id]) ."'><i class='fa fa-edit'></i></a>";
                })
                ->searchColumns('viajero','curso','convocatoria')
                ->orderColumns('viajero','curso','convocatoria','semanas','curso_total','fecha')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }
    }
}
