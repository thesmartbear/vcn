<?php

namespace VCN\Http\Controllers\Manage;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Proveedores\Proveedor as ProveedorModel;
use VCN\Repositories\Proveedores\ProveedorRepository as Proveedor;

use VCN\Models\Bookings\Booking;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Session;
use View;
use Carbon;

class ProveedoresController extends Controller
{
    private $proveedor;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Proveedor $proveedor )
    {
        $this->middleware("permiso.plataforma:proveedores,\VCN\Models\Proveedores\Proveedor", ['only' => ['getUpdate']]);
        $this->checkPermisos('proveedores');

        $this->proveedor = $proveedor;
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->proveedor->all()->sortBy('name');

            $filtro = ConfigHelper::config('propietario');
            if( $filtro && !auth()->user()->isFullAdmin() )
            {
                // $col = ProveedorModel::where('propietario', 0)->orWhere('propietario',$filtro)->get();
                $col1 = $this->proveedor->findWhere(['propietario'=> $filtro]);
                $col = $this->proveedor->findWhere(['propietario'=> 0]);

                $col = $col->merge($col1);
            }

            // $col = ProveedorModel::where('propietario',$filtro)->get();

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.proveedores.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->showColumns('contact_name')
                ->addColumn('contact_email', function($model) {
                    return "<a href='mailto:". $model->contact_email ."'>$model->contact_email</a>";
                })
                ->showColumns('contact_number', 'contact_mobil', 'commission')
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Proveedor y sus Centros, ...' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    $ret .= " <a href='". route('manage.centros.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Centro</a>";

                    return $ret;
                })
                ->searchColumns('name', 'contact_name')
                ->orderColumns('name','contact_name','commission')
                // ->orderColumns('*')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        // return \Response::json($this->proveedor->all());
        return view('manage.proveedores.index');
    }


    public function getNuevo()
    {
        return view('manage.proveedores.new');
    }

    public function getUpdate($id)
    {
        $ficha = $this->proveedor->find($id);

        //Ventas
        $totales = null;

        $valores['proveedores'] = $id;

        $user = auth()->user();
        if(!$user->filtro_oficinas)
        {
            $valores['oficinas'] = $user->oficina_id;
        }

        $moneda = ConfigHelper::default_moneda();

        $anyd = 2015;
        $anyh = intval(Carbon::now()->format('Y'));
        for ($i = $anyd; $i <= $anyh; $i++)
        {
            $totNum = 0;
            $totSemanas = 0;
            $totCursos = 0;
            $totTotal = 0;

            $valores['desde'] = "01/01/$i";
            $valores['hasta'] = "31/12/$i";

            $bookings = Booking::listadoFiltros($valores);

            foreach($bookings->get() as $b)
            {
                $totNum += 1;
                $totSemanas += $b->semanas;
                $totTotal += $b->total ;

                $total = $b->course_total_amount;
                $m_id = $b->course_currency_id;
                if($m_id != $moneda->id)
                {
                    $total = $total * $b->getMonedaTasa($m_id);
                }
                $totCursos += $total ;
            }

            $totales[$i]['total_num'] = $totNum;
            $totales[$i]['total_sem'] = $totSemanas;
            $totales[$i]['total_curso'] = $totCursos;
            $totales[$i]['total'] = $totTotal;

        }

        //Centros
        $totales_centro = null;
        foreach($ficha->centros as $centro)
        {
            for ($i = $anyd; $i <= $anyh; $i++)
            {
                $totNum = 0;
                $totSemanas = 0;
                $totCursos = 0;
                $totTotal = 0;

                $valores['desde'] = "01/01/$i";
                $valores['hasta'] = "31/12/$i";
                $valores['centros'] = $centro->id;

                $bookings = Booking::listadoFiltros($valores);

                foreach($bookings->get() as $b)
                {
                    $totNum += 1;
                    $totSemanas += $b->semanas;
                    $totTotal += $b->total ;

                    $total = $b->course_total_amount;
                    $m_id = $b->course_currency_id;
                    if($m_id != $moneda->id)
                    {
                        $total = $total * $b->getMonedaTasa($m_id);
                    }
                    $totCursos += $total ;
                }

                $totales_centro[$centro->id][$i]['total_num'] = $totNum;
                $totales_centro[$centro->id][$i]['total_sem'] = $totSemanas;
                $totales_centro[$centro->id][$i]['total_curso'] = $totCursos;
                $totales_centro[$centro->id][$i]['total'] = $totTotal;

            }
        }

        return View::make('manage.proveedores.ficha', compact('ficha','totales','totales_centro'));
    }

    public function postUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $data = $request->except("_token",'propietario_check');

        //publico (0) /privado (1,2)
        $data['propietario'] = $request->has('propietario')?$request->input('propietario'):0;
        if( $request->has('propietario_check') )
        {
            $data['propietario'] = ConfigHelper::config('propietario');
        }

        if(!$id)
        {
            $this->validate($request, [
                'name' => 'required|unique:proveedores|max:255',
            ]);

            //Nuevo
            $p = $this->proveedor->create($data);
            $id = $p->id;
        }
        else
        {
            $p = $this->proveedor->update($data, $id);
        }

        Session::flash('mensaje-ok',"Proveedor guardado correctamente.");

        return redirect()->route('manage.proveedores.ficha', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->proveedor->delete($id);
        return redirect()->route('manage.proveedores.index');
    }

    public function getVentas(Request $request, $id, $any, $centro_id=null)
    {
        $valores['proveedores'] = $id;
        $valores['desde'] = "01/01/$any";
        $valores['hasta'] = "31/12/$any";

        $user = auth()->user();
        if(!$user->filtro_oficinas)
        {
            $valores['oficinas'] = $user->oficina_id;
        }

        if($centro_id)
        {
            $valores['centros'] = $centro_id;
        }

        $bookings = Booking::listadoFiltros($valores);
        // $bookings = $bookings->select('prescriptor_id', DB::raw('count(id) as total_bookings'), DB::raw('sum(semanas) as total_semanas'), DB::raw('sum(course_total_amount) as total_curso'),DB::raw('sum(total) as total_total') );

        $col = collect();

        $moneda = ConfigHelper::default_moneda();

        $totNum = 0;
        $totSemanas = 0;
        $totCursos = 0;
        $totTotal = 0;
        foreach($bookings->get() as $booking)
        {
            $total = $booking->course_total_amount;
            $m_id = $booking->course_currency_id;
            if($m_id != $moneda->id)
            {
                $total = $total * $booking->getMonedaTasa($m_id);
            }

            if(!$col->contains('categoria_id',$booking->curso->category_id))
            {
                $obj['categoria_id'] = $booking->curso->category_id;
                $obj['name'] = $booking->curso->categoria?$booking->curso->categoria->name:"-";
                $obj['num'] = 1;
                $obj['total_sem'] = $booking->semanas;
                $obj['total_curso'] = $total;
                $obj['total'] = $booking->total;

                $col->push($obj);
            }
            else
            {
                $col = $col->map(function ($item, $key) use ($booking,$total) {
                    if($item['categoria_id']==$booking->curso->category_id)
                    {
                        $item['num'] += 1;
                        $item['total_sem'] += $booking->semanas;
                        $item['total_curso'] += $total;
                        $item['total'] += $booking->total;
                    }

                    return $item;
                });
            }

            $totNum += 1;
            $totSemanas += $booking->semanas;
            $totCursos += $total;
            $totTotal += $booking->total;
        }

        $totales['total_num'] = $totNum;
        $totales['total_sem'] = $totSemanas;
        $totales['total_curso'] = $totCursos;
        $totales['total'] = $totTotal;

        if(Datatable::shouldHandle())
        {

            return Datatable::collection( $col )
                ->addColumn('categoria', function($model) {
                    return $model['name'];
                })
                ->addColumn('num', function($model) {
                    return $model['num'];
                })
                ->addColumn('total_sem', function($model) {
                    return $model['total_sem'];
                })
                ->addColumn('total_curso', function($model) {
                    return $model['total_curso'];
                })
                ->addColumn('total', function($model) {
                    return $model['total'];
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }
    }
}
