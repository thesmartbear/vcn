<?php

namespace VCN\Http\Controllers\Manage\Inscritos;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Leads\ViajeroRepository as Viajero;
use VCN\Models\Leads\Viajero as ViajeroModel;

use VCN\Models\Bookings\Status;
use VCN\Models\Leads\Origen;
use VCN\Models\Leads\Suborigen;
use VCN\Models\Leads\SuborigenDetalle;
use VCN\Models\Leads\Tutor;
use VCN\Models\Prescriptores\Prescriptor;

use VCN\Models\Categoria;
use VCN\Models\Subcategoria;
use VCN\Models\User;
use VCN\Models\System\Oficina;
use VCN\Models\Informes\Venta;

use Datatable;
use Session;
use DB;
use Carbon;

use VCN\Helpers\ConfigHelper;

use VCN\Repositories\Criteria\FiltroPlataformaAsignado;
use VCN\Repositories\Criteria\FiltroPlataformaViajero;

class InscritosController extends Controller
{
    private $viajero;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Viajero $viajero )
    {
        $this->middleware("permiso.view:inscritos-list", ['only' => ['getIndex','getUpdate']]);

        $this->viajero = $viajero;
    }

    public function setLead(Request $request, $id)
    {
        $v = $this->viajero->find($id);
        $v->setInscrito(false);

        Session::flash('mensaje', 'Inscrito pasa a Lead.');

        return redirect()->route('manage.viajeros.index');
    }

    public function getIndex(Request $request, $status_id=0, $asign_to=0, $oficina_id=0)
    {
        // $this->viajero->pushCriteria(new FiltroPlataformaAsignado());
        // $this->viajero->pushCriteria(new FiltroPlataformaViajero());

        $any = intval($request->input('any', Carbon::now()->format('Y')));

        //desde-hasta
        $fecha1 = $request->input('desde',null);
        $fecha2 = $request->input('hasta',null);
        if(!$fecha1)
        {
            $fecha1 = "01/01/$any";
            $fecha2 = "31/12/$any";
        }

        if(!$asign_to)
        {
            $asign_to = $request->user()->id;
        }

        $user = auth()->user();
        if(!$user->filtro_oficinas)
        {
            $oficina_id = $user->oficina_id;
        }


            $query = DB::table('viajeros')->where('es_cliente',1);

            $filtro = ConfigHelper::config('propietario');
            if($filtro && !is_numeric($asign_to))
            {
                $query = $query->where('viajeros.plataforma', $filtro);
            }

            if($asign_to>0)
            {
                $query = $query->where('asign_to', $asign_to);
            }
            else
            {
                if($asign_to=='all')
                {
                    $usuarios = User::asignados()->pluck('id')->toArray();
                }
                else
                {
                    $usuarios = User::asignadosOff()->pluck('id')->toArray();
                }

                $query = $query->whereIn('asign_to', $usuarios);
            }

            if($status_id>0)
            {
                $query = $query->where('booking_status_id', $status_id);
            }

            if($oficina_id>0)
            {
                $query = $query->where('viajeros.oficina_id', $oficina_id);
            }

            $query = $query->select(
                    'viajeros.id as id',
                    'bookings.course_start_date as fecha_curso',
                    'bookings.fecha_reserva as fecha_reserva',
                    'cursos.course_name as curso',
                    'statuses.name as status',
                    'viajero_origenes.name as origen',
                    'viajeros.email as email'
                )
                ->leftJoin('bookings','viajeros.id','=','bookings.viajero_id')
                ->leftJoin('viajero_origenes','viajero_origenes.id','=','viajeros.origen_id')
                ->leftJoin('cursos','cursos.id','=','bookings.curso_id')
                ->leftJoin('statuses','statuses.id','=','viajeros.status_id');

            $fecha1c = Carbon::createFromFormat('d/m/Y', $fecha1);
            $fecha2c = Carbon::createFromFormat('d/m/Y', $fecha2);
            $query = $query->where('bookings.course_start_date','>=',$fecha1c->toDateString())->where('bookings.course_start_date','<=',$fecha2c->toDateString());

            //dd($query->count());

        if(Datatable::shouldHandle())
        {
            //return Datatable::collection( $col )
            return Datatable::query($query)
                ->addColumn('name', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return "<a href='". route('manage.viajeros.ficha',[$model->id]) ."'>$model->full_name</a>";
                })
                ->addColumn('reserva', function($column) {
                    $model = ViajeroModel::find($column->id);
                    $model = $model->booking;

                    return $model?($model->fecha_reserva?$model->fecha_reserva->format('Y-m-d'):"-"):"-";
                })
                ->addColumn('curso', function($column) {
                    $model = ViajeroModel::find($column->id);
                    $model = $model->booking;

                    if($model)
                    {
                        $curso = $model->curso?"<a data-label='Ficha Curso' href='". route('manage.cursos.ficha', $model->curso->id) ."'>". $model->curso->name ."</a>":"-";
                        $convocatoria = $model->convocatoria?"<a data-label='Ficha Convocatoria' href='". route('manage.convocatorias.cerradas.ficha', $model->convocatoria->id) ."'>". $model->convocatoria->name ."</a>":"-";
                        return $model->curso->es_convocatoria_cerrada?$convocatoria:$curso;
                    }

                    return "-";
                })
                ->addColumn('fecha_curso', function($column) {
                    $model = ViajeroModel::find($column->id);
                    $model = $model->booking;

                    return $model?$model->course_start_date:"-";
                })
                ->addColumn('duracion', function($column) {
                    $model = ViajeroModel::find($column->id);
                    $model = $model->booking;

                    return $model?$model->convocatoria->duracion_name:"-";
                })
                ->addColumn('categoria', function($column) {
                    $model = ViajeroModel::find($column->id);
                    $model = $model->booking;

                    return $model?$model->curso->categoria_full_name:"-";
                })
                ->addColumn('status', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return "<span class='badge'>". ($model->status?$model->status->name:"-") . "</span>";
                })
                ->addColumn('origen', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->origen_full_name;
                })
                ->addColumn('asignado', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->asign_to?$model->asignado->fname." ".$model->asignado->lname:"-";
                })
                ->showColumns('email')
                ->addColumn('idioma', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->idioma_contacto;
                })
                ->addColumn('tutor1', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor1?$model->tutor1->name:'-';
                })
                ->addColumn('email_tutor1', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor1?$model->tutor1->email:'-';
                })
                ->addColumn('movil_tutor1', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor1?$model->tutor1->movil:'-';
                })
                ->addColumn('tutor2', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor2?$model->tutor2->name:'-';
                })
                ->addColumn('email_tutor2', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor2?$model->tutor2->email:'-';
                })
                ->addColumn('movil_tutor2', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor2?$model->tutor2->movil:'-';
                })
                ->addColumn('prescriptor', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->prescriptor?$model->prescriptor->name:'-';
                })
                ->addColumn('oficina', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->oficina?$model->oficina->name:'-';
                })
                ->addColumn('ig', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->datos?$model->datos->rs_instagram:'-';
                })
                ->addColumn('edad', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->edad;
                })
                ->addColumn('options', function($column) {

                    $model = ViajeroModel::find($column->id);

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    $ret .= " <a href='". route('manage.bookings.nuevo', $model->id) ."' class='btn btn-info btn-xs'><i class='fa fa-plus-circle'></i> Nueva Inscripción</a>";

                    if($model->es_cliente)
                    {
                        $ret .= " <a href='". route('manage.inscritos.lead',[$model->id]) ."' class='btn btn-danger btn-xs'><i class='fa fa-arrow-circle-left'></i> Volver a Lead</a>";
                    }
                    else
                    {
                        $ret .= "<span class='badge'>Inscrito</span>";
                    }

                    return $ret;
                })
                ->searchColumns('name','lastname','lastname2')
                // ->orderColumns('name','asignado','tutores')
                ->orderColumns('viajero','fecha_curso','fecha_reserva','convocatoria','curso','origen','status')
                ->setAliasMapping()
                ->make();
        }

        $statuses = Status::all();
        $statuses_total = [];

        $statuses_total[0] = $query->count();

        foreach($statuses as $status)
        {
            $query2 = clone $query;
            $query2 = $query2->where('booking_status_id', $status->id);

            $statuses_total[$status->id] = $query2->count();
        }

        $user = auth()->user();
        if(!$user->filtro_oficinas)
        {
            $oficina_id = $user->oficina_id;
        }

        $oficinas = Oficina::plataforma();
        if($oficina_id>0)
        {
            $asignados = User::where('oficina_id',$oficina_id)->orderBy('fname')->get();
        }
        else
        {
            $asignados = User::asignados();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any','any')->toArray();
        $anys[end($anys)+1] = end($anys)+1;

        return view('manage.inscritos.index', compact('status_id', 'asign_to', 'asignados', 'statuses', 'statuses_total', 'oficinas', 'oficina_id','anys','any', 'fecha1', 'fecha2'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->viajero->find($id);

        $asignados = User::asignados()->get()->pluck('full_name', 'id');
        $statuses = Status::pluck('name','id');

        $conocidos = [""=>""] + Origen::plataforma()->pluck('name','id')->toArray();
        $subconocidos = Suborigen::where('origen_id',$ficha->origen_id)->pluck('name','id');
        $subconocidosdet = SuborigenDetalle::where('suborigen_id',$ficha->suborigen_id)->pluck('name','id');

        $users = User::asignados()->get()->pluck('full_name', 'id');

        $tutores = Tutor::all()->pluck('full_name', 'id');

        $prescriptores = [""=>""] + Prescriptor::plataforma()->pluck('name', 'id')->toArray();

        // return view('manage.inscritos.ficha', compact('ficha','asignados','statuses'));
        return view('manage.viajeros.ficha', compact('ficha','asignados','prescriptores','statuses','conocidos','subconocidos','subconocidosdet', 'tutores','users','categorias','subcategorias'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            // 'inscrito_status_id' => 'required|numeric|min:1',
            'name' => 'required',
        ]);

        $data = $request->except('_token');

        if(!$id)
        {
            //nuevo
            $o = $this->viajero->create($data);
            $id = $o->id;
        }
        else
        {
            $fillable = $this->viajero->find($id);
            
            $columns = DB::connection()->getSchemaBuilder()->getColumnListing("viajeros");
            $fillable = $fillable->getFillable();
            $fields = array_keys($request->input());
            
            $except = array_diff( $fields, $columns );
            $except[] = 'id';
           
            $data = $request->except($except);
            
            $this->viajero->update($data, $id);
        }

        return redirect()->route('manage.inscritos.ficha',$id);
    }

}
