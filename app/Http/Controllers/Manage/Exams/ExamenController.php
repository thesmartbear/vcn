<?php

namespace VCN\Http\Controllers\Manage\Exams;

use VCN\Models\Exams\Examen;
use Illuminate\Http\Request;
use VCN\Http\Controllers\Controller;

use VCN\Models\Exams\Respuesta;
use VCN\Models\Exams\ExamenVinculado;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Leads\Viajero;

use Datatable;
use Session;
use ConfigHelper;
use VCN\Helpers\MailHelper;
use Carbon;

class ExamenController extends Controller
{
    const RUTA  = 'manage.exams.';
    const NAME  = 'Test';
    const NAMES = 'Tests';

    public function __construct()
    {
        $this->checkPermisos('cuestionarios');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Datatable::shouldHandle()) {
            $col = Examen::all();

            return Datatable::collection($col)
                ->showColumns('bloques')
                ->addColumn('name', function ($model) {
                    return "<a href='" . route('manage.exams.edit', [$model->id]) . "'>$model->name</a>";
                })
                ->addColumn('tema', function ($model) {
                    return $model->template;
                })
                ->addColumn('preguntas', function ($model) {
                    return $model->preguntas->count();
                })
                ->addColumn('respuestas', function ($model) {
                    return $model->respuestas->count();
                })
                ->addColumn('preview', function ($model) {
                    return "<a target='_blank' href='" . route('manage.exams.preview', [$model->id]) . "'>$model->name</a>";
                })
                ->addColumn('status', function ($model) {
                    return $model->activo ? "<span class='badge badge-help'>SI</span>" : "<span class='badge'>NO</span>";
                })
                ->addColumn('options', function ($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Test' data-action='" . route('manage.exams.destroy', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.exams.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parametros = (object) [
            'temas' => ConfigHelper::config('examenes')
        ];

        return view(self::RUTA . 'create')->with(compact('parametros'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required',
            'title'    => 'required'
        ]);

        $test = Examen::create($request->input());
        $id = $test->id;

        $name = self::NAME;
        Session::flash('mensaje-ok', "$name creado correctamente");
        return redirect()->route(self::RUTA . "index");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \VCN\Models\Exams\Examen  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Examen $exam)
    {
        $ficha = $exam;

        $parametros = (object) [
            'temas' => ConfigHelper::config('examenes')
        ];

        return view(self::RUTA . 'edit')->with(compact('ficha', 'parametros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \VCN\Models\Exams\Examen  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Examen $exam)
    {
        $this->validate($request, [
            'name'      => 'required',
            'title'    => 'required'
        ]);

        $id = $exam->id;

        $data = $request->input();
        $exam->update($data);

        $name = self::NAME;
        Session::flash('mensaje-ok', "$name modificado correctamente");
        return redirect()->route(self::RUTA . "index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \VCN\Models\Exams\Examen  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Examen $exam)
    {
        $exam->delete();

        $name = self::NAME;
        Session::flash('mensaje-ok', "$name eliminado correctamente");

        return redirect()->back();
    }

    public function getPreview(Request $request, Examen $exam)
    {
        $template = "exams." . $exam->template;
        $urlAssets = "/assets/exams/$exam->tema";

        $tipo = "booking";
        $booking_id = 0;
        $viajero_id = 0;

        $respuesta = Examen::getRespuesta($exam, $booking_id, $tipo);

        return view($template)->with(compact('exam', 'urlAssets', 'respuesta', 'booking_id', 'viajero_id'));
    }

    public function getRespuestas(Request $request, $id)
    {
        if (Datatable::shouldHandle()) {
            $col = collect();

            $exam = Examen::find($id);
            if ($exam) {
                $col = Respuesta::where('examen_id', $exam->id)->get();
            } else {
                $viajero = $request->get('viajero');
                if ($viajero) {
                    $col = Respuesta::where('viajero_id', $viajero)->get();
                }

                $curso = $request->get('Curso');
                if ($curso) {
                    $bids = Booking::where('curso_id', $curso)->pluck('id')->toArray();
                    $col = Respuesta::whereIn('booking_id', $bids)->get();
                }

                $c = $request->get('Cerrada');
                if ($c) {
                    $bids = Booking::where('convocatory_close_id', $c)->pluck('id')->toArray();
                    $col = Respuesta::whereIn('booking_id', $bids)->get();
                }

                $c = $request->get('Abierta');
                if ($c) {
                    $bids = Booking::where('convocatory_open_id', $c)->pluck('id')->toArray();
                    $col = Respuesta::whereIn('booking_id', $bids)->get();
                }

                $c = $request->get('ConvocatoriaMulti');
                if ($c) {
                    $bids = Booking::where('convocatory_multi_id', $c)->pluck('id')->toArray();
                    $col = Respuesta::whereIn('booking_id', $bids)->get();
                }

                $c = $request->get('Categoria');
                if ($c) {
                    $bids = Booking::where('category_id', $c)->pluck('id')->toArray();
                    $col = Respuesta::whereIn('booking_id', $bids)->get();
                }

                $c = $request->get('Subcategoria');
                if ($c) {
                    $bids = Booking::where('subcategory_id', $c)->pluck('id')->toArray();
                    $col = Respuesta::whereIn('booking_id', $bids)->get();
                }
            }

            return Datatable::collection($col)
                ->addColumn('tipo', function ($model) {
                    return $model->booking_id ? "booking" : "viajero";
                })
                ->addColumn('booking', function ($model) {
                    return "<a target='_blank' href='" . route('manage.bookings.ficha', $model->booking_id) . "'>$model->booking_id</a>";
                })
                ->addColumn('viajero', function ($model) {
                    return "<a target='_blank' href='" . route('manage.viajeros.ficha', $model->viajero_id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('ver', function ($model) {
                    $r = route('manage.exams.ask', $model->id);
                    return "<a target='_blank' href='$r'>Ver</a>";
                })
                ->addColumn('estado', function ($model) {
                    return $model->estado;
                })
                ->showColumns('aciertos', 'resultado', 'notas')
                ->addColumn('options', function ($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Respuesta' data-action='" . route('manage.exams.ask.destroy', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name','viajero','booking')
                ->orderColumns('name','viajero','booking')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }
    }

    public function destroyRespuesta(Respuesta $respuesta)
    {
        $respuesta->delete();

        Session::flash('mensaje-ok', "Respuesta eliminada correctamente");

        return redirect()->back();
    }

    public function getRespuesta(Respuesta $respuesta)
    {
        if (!$respuesta->status) {
            $respuesta->evaluar();
        }

        $exam = $respuesta->examen;

        $template = "exams." . $exam->template;
        $urlAssets = "/assets/exams/$exam->tema";

        return view($template)->with(compact('exam', 'urlAssets', 'respuesta'));
    }

    public function postVinculado(Request $request)
    {
        $this->validate($request, [
            'examen_id' => 'required|exists:examenes,id',
        ]);

        $data = $request->except("_token");
        $data['excluye'] = $request->has('excluye');

        $cv = ExamenVinculado::create($data);

        Session::flash('tab', '#vinculados');
        return redirect()->back();
    }

    public function deleteVinculado(Request $request, $id)
    {
        $ficha = ExamenVinculado::findOrFail($id);

        $ficha->delete();

        Session::flash('tab', '#vinculados');
        return redirect()->back();
    }

    public function getReclamar(Request $request, Examen $exam, $modelo = null, $modelo_id = null)
    {
        ini_set('memory_limit', '400M');
        set_time_limit(0);


        $iMails = 0;
        $iMails_bloque = 100;
        $iMails_sleep = 0.3;


        // $bookings = $ficha->bookings;

        $any = intval(Carbon::now()->year);
        $valores['desdes'] = "01/01/" . $any;
        $valores['hastas'] = "31/12/" . $any;
        $valores['plataformas'] = $exam->propietario ?: ConfigHelper::propietario();

        $valores['reclamar'] = $modelo;

        switch ($modelo) {
            case "Centro": {
                    $valores['centros'] = $modelo_id;
                }
                break;

            case "Curso": {
                    $valores['cursos'] = $modelo_id;
                }
                break;

            case "Categoria": {
                    $valores['categorias'] = $modelo_id;
                }
                break;

            case "Subcategoria": {
                    $valores['subcategorias'] = $modelo_id;
                }
                break;

            case "Abierta": {
                    $valores['tipoc'] = 3;
                    $valores['convocatorias'] = $modelo_id;
                }
                break;

            case "Cerrada": {
                    $valores['tipoc'] = 5;
                    $valores['convocatorias'] = $modelo_id;
                }
                break;

            case "ConvocatoriaMulti": {
                    $valores['tipoc'] = 4;
                    $valores['convocatorias'] = $modelo_id;
                }
                break;
        }

        $bookings = Booking::listadoFiltros($valores);
        $bookings = $bookings->where('es_directo', 0);
        // dd($bookings->count());

        $iTotalV = 0;
        $iTotalT = 0;

        $examTxt = $exam->name;

        foreach ($bookings->get() as $booking) {
            $r = $exam->getRespuestaBooking($booking->id);
            if ($r && !$r->status) {
                continue;
            }

            //Viajero
            $viajero = $booking->viajero;
            $user = $viajero->user;

            if ($user) {
                $iTotalV += MailHelper::mailBookingExamen($exam, $booking, $user);
                BookingLog::addLog($booking, "Enviar Test ($examTxt) a Viajero");
            }

            //Tutores
            foreach ($viajero->tutores as $t) {
                $user = $t->user;
                if ($user) {
                    $iTotalT += MailHelper::mailBookingExamen($exam, $booking, $user);
                    BookingLog::addLog($booking, "Enviar Test ($examTxt) a Tutor");
                }
            }
        }

        Session::flash('mensaje-ok', "Se ha reclamado a un total de $iTotalV Viajeros y $iTotalT Tutores.");
        Session::flash('tab', '#tests');
        return redirect()->back();
    }
}
