<?php

namespace VCN\Http\Controllers\Manage\Informes;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Bookings\Booking;

use Carbon;

class GeneraDocumentosController extends Controller
{
    // private static function descargar($doc, $filename)
    // {
    //     header( "Content-Type:   application/octet-stream" );// you should look for the real header that a word2007 document needs!!!
    //     header( 'Content-Disposition: attachment; filename='.$filename );
    // }

    /*
    public function getNotaPagoX(Request $request, $booking_id, $idioma="es")
    {

        $filename = "NotaPago_$booking_id.doc";
        if($idioma=="ca")
        {
            $filename = "NotaPagament_$booking_id.doc";
        }

        $booking = Booking::find($booking_id);

        $view = 'pdf.'. strtolower($idioma) .'.recibo35';
        $html = view($view, compact('booking','idioma'))->render();

        header("Content-type: application/vnd.ms-word");
        // header( "Content-Type: application/octet-stream" );
        header( 'Content-Disposition: attachment; filename='.$filename );

        echo $html;
        return;

        header("Content-type: application/vnd.ms-word");
        // header( "Content-Type: application/octet-stream" );
        header( 'Content-Disposition: attachment; filename='.$filename );

        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection();

        $section->addText('Hello world!');

        $section->addText('Hello world! I am formatted.',
            array('name'=>'Tahoma', 'size'=>16, 'bold'=>true));

        $phpWord->addFontStyle('myOwnStyle',
            array('name'=>'Verdana', 'size'=>14, 'color'=>'1B2232'));
        $section->addText('Hello world! I am formatted by a user defined style',
            'myOwnStyle');

        // You can also put the appended element to local object like this:
        $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        $fontStyle->setBold(true);
        $fontStyle->setName('Verdana');
        $fontStyle->setSize(22);
        $myTextElement = $section->addText('Hello World!');
        $myTextElement->setFontStyle($fontStyle);

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();


        $section->addText("Centro: ". $booking->centro->name, array('name'=>'Tahoma', 'size'=>12));
        $section->addText("Curso: ". $booking->curso->name, array('name'=>'Tahoma', 'size'=>12));


        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        // $objWriter->save($filename);
        $objWriter->save( "php://output" );

    }
    */

    public function getNotaPago(Request $request, $booking_id, $idioma="es")
    {

        $filename = "NotaPago_$booking_id.doc";
        if($idioma=="ca")
        {
            $filename = "NotaPagament_$booking_id.doc";
        }

        $booking = Booking::find($booking_id);

        $view = 'pdf.'. strtolower($idioma) .'.recibo35';
        $html = view($view, compact('booking','idioma'))->render();

        header("Content-type: application/vnd.ms-word");
        // header( "Content-Type: application/octet-stream" );
        header( 'Content-Disposition: attachment; filename='.$filename );

        echo $html;
        return;
    }

    public function getRecibo(Request $request, $booking_id, $idioma="es")
    {
        $booking = Booking::find($booking_id);
        $ficha = $booking->pagos->first();

        $filename = "Recibo_$booking_id.doc";
        if($idioma=="ca")
        {
            $filename = "Rebut_$booking_id.doc";
        }

        $view = 'pdf.'. strtolower($idioma) .'.recibo';
        $html = view($view, compact('ficha','booking','idioma'));

        header("Content-type: application/vnd.ms-word");
        header( 'Content-Disposition: attachment; filename='.$filename );

        echo $html;
        return;
    }

    public function getProveedor(Request $request, $booking_id)
    {
        $ficha = Booking::find($booking_id);

        $filename = Carbon::now()->format('Ymd_H-i-s') ."_". str_slug($ficha->viajero->full_name ?? "-") ."_". str_slug($ficha->curso->name ?? "-") ."_". $ficha->id .".doc";

        $view = 'manage.bookings.pdf_proveedor';
        $html = view($view, compact('ficha'))->render();

        header("Content-type: application/vnd.ms-word");
        // header( "Content-Type: application/octet-stream" );
        header( 'Content-Disposition: attachment; filename='.$filename );

        echo $html;
        return;
    }
}
