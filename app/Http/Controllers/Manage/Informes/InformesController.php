<?php

namespace VCN\Http\Controllers\Manage\Informes;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingPago;
use VCN\Models\Bookings\BookingExtra;
use VCN\Models\Bookings\BookingIncidencia;
use VCN\Models\Bookings\BookingFactura;
use VCN\Models\Bookings\Status as BookingStatus;
use VCN\Models\Bookings\StatusChecklist as BookingStatusChecklist;

use VCN\Models\Cursos\CursoExtraGenerico;
use VCN\Models\Extras\Extra;
use VCN\Models\System\Oficina;
use VCN\Models\Prescriptores\Prescriptor;
use VCN\Models\User;
use VCN\Models\Leads\Viajero;
use VCN\Models\Informes\Venta;
use VCN\Models\Informes\VentaCurso;
use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Cursos\Curso;
use VCN\Models\Centros\Centro;
use VCN\Models\Categoria;
use VCN\Models\Subcategoria;
use VCN\Models\SubcategoriaDetalle;
use VCN\Models\Convocatorias\Cerrada;
use VCN\Models\Convocatorias\Abierta;
use VCN\Models\Convocatorias\ConvocatoriaMulti;
use VCN\Models\Prescriptores\Categoria as CategoriaPrescriptor;
use VCN\Models\Convocatorias\Vuelo;
use VCN\Models\Agencia;
use VCN\Models\Leads\Origen;
use VCN\Models\Solicitudes\{Solicitud, SolicitudLogStatus};
use VCN\Models\Solicitudes\Status as SolicitudStatus;
use VCN\Models\Monedas\Moneda;
use VCN\Models\Convocatorias\CerradaPrecio;

use VCN\Models\Especialidad;
use VCN\Models\Subespecialidad;
use VCN\Models\Pais;
use VCN\Models\Convocatorias\VueloEtapa;
use VCN\Models\Convocatorias\CerradaVuelo;

use VCN\Models\Informes\WebVisita;
use VCN\Models\Informes\WebRegistro;
use VCN\Models\Informes\Presupuesto;

use VCN\Models\Leads\Suborigen;
use VCN\Models\Leads\SuborigenDetalle;
use VCN\Exports\InformeAviExport;

use Datatable;
use Session;
use Carbon;
use DB;
use ConfigHelper;
use Log;
use PDF;
use Excel;
use VCN\Helpers\Graficos;
use Lava;
use Artisan;

class InformesController extends Controller
{
    /**
     * Instantiate a new InformesController instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        // $this->checkPermisosFullAdmin();
        // $this->checkPermisos('informes');
        // $this->middleware("permiso.edit:informes");

        // return;

        $ajax = "manage.informes.filtros.ajax";
        $ruta = $request->route() ? $request->route()->getName() : null;
        if ($ruta && $ruta != $ajax) {
            $this->middleware("permiso.informe:$ruta");
        }
    }

    public static function getFechasByModo($desde, $hasta, $modo)
    {
        $fechas_txt = [];
        $fechas = [];

        switch ($modo) {
            case 'dias': {
                    $d = clone $desde;
                    $cols = $hasta->diffInDays($desde);
                    $fechas_txt[0] = $d->format('d/m');
                    $fechas[0] = $d->format('Y-m-d');
                    for ($i = 1; $i <= $cols; $i++) {
                        $d->addDay();
                        $fechas_txt[$i] = $d->format('d/m');
                        $fechas[$i] = $d->format('Y-m-d');
                    }
                }
                break;

            case 'semanas': {
                    $d = clone $desde;
                    $cols = $hasta->diffInWeeks($desde);
                    $fechas_txt[0] = $d->format('W');
                    $fechas[0] = (int) $d->format('W');
                    for ($i = 1; $i <= $cols; $i++) {
                        $d->addWeek();
                        $fechas_txt[$i] = $d->format('W');
                        $fechas[$i] = (int) $d->format('W');
                    }
                }
                break;

            case 'meses': {
                    $d = clone $desde;
                    $cols = $hasta->diffInMonths($desde);
                    $fechas_txt[0] = $d->month; //->format('M');
                    $fechas[0] =  (int) $d->month;
                    for ($i = 1; $i <= $cols; $i++) {
                        $d->addMonth();
                        $fechas_txt[$i] = $d->month; //->format('M');
                        $fechas[$i] = (int) $d->month;
                    }
                }
                break;

            case 'anys': {
                    $d = clone $desde;
                    $cols = $hasta->diffInYears($desde);
                    $fechas_txt[0] = $d->year;
                    $fechas[0] =  (int) $d->year;
                    for ($i = 1; $i <= $cols; $i++) {
                        $d->addYear();
                        $fechas_txt[$i] = $d->year;
                        $fechas[$i] = (int) $d->year;
                    }
                }
                break;
        }

        $ret = [
            'fechas' => $fechas,
            'fechas_txt' => $fechas_txt,
        ];

        return $ret;
    }

    public static function getFiltroByModo($modo, $campo)
    {

        $filtro = "date($campo) as fecha_grup";

        switch ($modo) {
            case 'dias': {
                    $filtro = "date($campo) as fecha_grup";
                }
                break;

            case 'semanas': {
                    $filtro = "week($campo) as fecha_grup";
                }
                break;

            case 'meses': {
                    $filtro = "month($campo) as fecha_grup";
                }
                break;

            case 'anys': {
                    $filtro = "year($campo) as fecha_grup";
                }
                break;
        }

        return $filtro;
    }


    public function getPagosGrupo(Request $request)
    {
        $desde = $request->input('desde');
        $hasta = $request->input('hasta');

        if (Datatable::shouldHandle()) {
            $filtro = ConfigHelper::config('propietario');
            if ($filtro) {
                $viajeros = Viajero::plataforma()->pluck('id')->toArray();
                $bookings = Booking::whereIn('viajero_id', $viajeros)->pluck('id')->toArray();
                $col = BookingPago::whereIn('booking_id', $bookings)->groupBy('booking_id')->get();
            } else {
                $col = BookingPago::groupBy('booking_id')->get();
            }

            if ($desde && $hasta) {
                $fini = Carbon::createFromFormat('d/m/Y', $desde)->format('Y-m-d');
                $ffin = Carbon::createFromFormat('d/m/Y', $hasta)->format('Y-m-d');

                if ($filtro) {
                    $col = BookingPago::whereIn('booking_id', $bookings)->where('fecha', '>=', $fini)->where('fecha', '<=', $ffin)->groupBy('booking_id')->get();
                } else {
                    $col = BookingPago::where('fecha', '>=', $fini)->where('fecha', '<=', $ffin)->groupBy('booking_id')->get();
                }
            }

            return Datatable::collection($col)
                ->addColumn('fecha', function ($model) {
                    return BookingPago::where('booking_id', $model->booking_id)->orderBy('fecha', 'DESC')->first()->fecha;
                })
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->booking->viajero->id) . "'>" . $model->booking->viajero->full_name . "</a>";
                })
                ->addColumn('importe', function ($model) {
                    return BookingPago::where('booking_id', $model->booking_id)->sum('importe');
                })
                ->addColumn('contable', function ($model) {
                    return $model->booking->contable_code;
                })
                ->searchColumns('viajero')
                // ->orderColumns('fecha','importe')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.informes.pagos_grupo', compact('desde', 'hasta'));
    }

    public function getPagosEnvio(Request $request)
    {
        if (Datatable::shouldHandle()) {
            $filtro = ConfigHelper::config('propietario');
            if ($filtro) {
                $viajeros = Viajero::plataforma()->pluck('id')->toArray();
                $bookings = Booking::whereIn('viajero_id', $viajeros)->pluck('id')->toArray();
                $col = BookingPago::where('enviado', 0)->whereIn('booking_id', $bookings)->get();
            } else {
                $col = BookingPago::where('enviado', 0)->get();
            }

            return Datatable::collection($col)
                ->showColumns('importe', 'fecha')
                ->showColumns('booking_id')
                ->addColumn('viajero_id', function ($model) {
                    return $model->booking->viajero_id;
                })
                ->addColumn('viajero', function ($model) {
                    $viajero = $model->booking->viajero;
                    return $viajero->datos->fact_razonsocial ? $viajero->datos->fact_razonsocial : $viajero->full_name;
                })
                ->addColumn('nif', function ($model) {
                    $viajero = $model->booking->viajero;
                    if (!$viajero->datos->fact_nif) {
                        return $viajero->datos->documento;
                    }

                    return $viajero->datos->fact_nif;
                })
                ->addColumn('tipopago', function ($model) {
                    return ConfigHelper::getTipoPago($model->tipo);
                })
                ->addColumn('tipoprograma', function ($model) {
                    return $model->booking->curso->categoria ? $model->booking->curso->categoria->name : "-";
                })
                ->addColumn('contable', function ($model) {
                    return $model->booking->contable_code;
                })
                ->addColumn('ccc', function ($model) {
                    $oficina = $model->booking->viajero->oficina;
                    if (!$oficina) {
                        return "-";
                    }
                    return substr($oficina->iban, -4);
                })
                ->addColumn('oficina', function ($model) {
                    $oficina = $model->booking->viajero->oficina;
                    if (!$oficina) {
                        return "-";
                    }

                    return $oficina->name;
                })
                ->searchColumns('viajero', 'nif')
                // ->orderColumns('fecha','importe')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.informes.pagos_envio');
    }

    public function getFacturacion347(Request $request)
    {
        $desde = $request->input('desde');
        $hasta = $request->input('hasta');

        if (Datatable::shouldHandle()) {
            ini_set('memory_limit', '400M');
            set_time_limit(0);

            if ($desde && $hasta) {
                $fini = Carbon::createFromFormat('!d/m/Y', $desde)->format('Y-m-d');
                $ffin = Carbon::createFromFormat('!d/m/Y', $hasta)->format('Y-m-d');

                $col = BookingFactura::whereNull('grup_id')->where('fecha', '>=', $fini)->where('fecha', '<=', $ffin)->get();
                $grup = BookingFactura::whereNotNull('grup_id')->where('fecha', '>=', $fini)->where('fecha', '<=', $ffin)->groupBy('grup_id')->get();
            } else {
                $col = BookingFactura::whereNull('grup_id')->get();
                $grup = BookingFactura::whereNotNull('grup_id')->groupBy('grup_id')->get();
            }

            $col = $col->merge($grup);

            $importes = [];

            foreach ($col as $factura) {
                $nif = $factura->grup ? $factura->grup->nif : $factura->nif;

                if (trim($nif) == "") {
                    $nif = "000F:" . $factura->booking->viajero_id;
                    $nif = "<a target='_blank' href='" . route('manage.bookings.ficha', $factura->booking_id) . "'>$nif</a>";
                }

                if (!isset($importes[$nif])) {
                    $detalle['nif'] = $nif;
                    $detalle['cuenta'] = 0;
                    $detalle['importe_total'] = 0;
                    $detalle['importe_1t'] = 0;
                    $detalle['importe_2t'] = 0;
                    $detalle['importe_3t'] = 0;
                    $detalle['importe_4t'] = 0;
                    $detalle['factura'] = $factura;

                    $importes[$nif] = $detalle;
                }

                $detalle = $importes[$nif];

                $importet = "importe_" . $factura->trimestre . "t";

                $detalle['importe_total'] += $factura->total;
                $detalle[$importet] += $factura->total;
                $detalle['cuenta'] += 1;

                if ($factura->grup) {
                    $detalle['factura'] = $factura;
                }

                $importes[$nif] = $detalle;
            }

            $list = collect($importes);

            // dd($importes);


            // $list = $col->groupBy('nif')->sortBy('nif');
            // dd($list);

            // $i=0;
            // foreach($list as $imp)
            // {
            //     $fact = $col->where('nif',$imp['nif']);

            //     if($fact->count()>1)
            //     {
            //         dd($fact);
            //     }
            // }

            // dd(count($importes));

            $fini = Carbon::createFromFormat('!d/m/Y', $desde);
            $ffin = Carbon::createFromFormat('!d/m/Y', $hasta);

            return Datatable::collection($list)
                ->addColumn('nif', function ($item) {
                    return $item['nif'];
                })
                ->addColumn('razonsocial', function ($item) {

                    if (!$item['nif']) {
                        return "-";
                    }

                    $fact = $item['factura'];
                    if ($fact->grup) {
                        return $fact->grup->razonsocial;
                    }

                    return $fact->datos['razonsocial'];
                })
                ->addColumn('cp', function ($item) {

                    if (!$item['nif']) {
                        return "-";
                    }

                    $fact = $item['factura'];
                    if ($fact->grup) {
                        return $fact->grup->cp;
                    }

                    return $fact->datos['cp'];
                })
                ->addColumn('provincia', function ($item) {

                    if (!$item['nif']) {
                        return "-";
                    }

                    $fact = $item['factura'];
                    if ($fact->grup) {
                        return "";
                    }

                    return $fact->viajero->datos->provincia_name;
                })
                ->addColumn('pais', function ($item) {

                    if (!$item['nif']) {
                        return "-";
                    }

                    $fact = $item['factura'];
                    if ($fact->grup) {
                        return "";
                    }

                    return $fact->viajero->datos->pais_name;
                })
                ->addColumn('importe1t', function ($item) {
                    return round($item['importe_1t'], 2);
                })
                ->addColumn('importe2t', function ($item) {
                    return round($item['importe_2t'], 2);
                })
                ->addColumn('importe3t', function ($item) {
                    return round($item['importe_3t'], 2);
                })
                ->addColumn('importe4t', function ($item) {
                    return round($item['importe_4t'], 2);
                })
                ->addColumn('importet', function ($item) {
                    return round($item['importe_total'], 2);
                })
                ->addColumn('cuenta', function ($item) {
                    return $item['cuenta'];
                })
                ->searchColumns('nif', 'razonsocial')
                ->orderColumns('nif', 'razonsocial')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        return view('manage.informes.facturacion_347', compact('desde', 'hasta'));
    }

    public function getFacturacionFechas(Request $request)
    {
        $desde = $request->input('desde');
        $hasta = $request->input('hasta');

        if (Datatable::shouldHandle()) {
            //BookingFactura

            $bookings = Booking::plataforma()->where('convocatory_multi_id', 0)->pluck('id')->toArray();

            if ($desde && $hasta) {
                $fini = Carbon::createFromFormat('!d/m/Y', $desde)->format('Y-m-d');
                $ffin = Carbon::createFromFormat('!d/m/Y', $hasta)->format('Y-m-d');

                $col = BookingFactura::whereNull('grup_id')->whereIn('booking_id', $bookings)->where('fecha', '>=', $fini)->where('fecha', '<=', $ffin)->get();
                $grup = BookingFactura::whereNotNull('grup_id')->whereIn('booking_id', $bookings)
                    ->where('fecha', '>=', $fini)->where('fecha', '<=', $ffin)->groupBy('grup_id')->get();
            } else {
                $col = BookingFactura::whereNull('grup_id')->whereIn('booking_id', $bookings)->get();
                $grup = BookingFactura::whereNotNull('grup_id')->whereIn('booking_id', $bookings)->groupBy('grup_id')->get();
            }

            $col = $col->merge($grup);

            //multis => linea por especialidad
            $bookings = Booking::plataforma()->where('convocatory_multi_id', '>', 0)->get();

            $fini = Carbon::createFromFormat('!d/m/Y', $desde);
            $ffin = Carbon::createFromFormat('!d/m/Y', $hasta);

            foreach ($bookings as $booking) {
                $factura = $booking->factura;
                // $factura = BookingFactura::whereNull('grup_id')->where('booking_id',$booking->id)->first();

                if ($factura) {
                    $contables = explode(',', $booking->contable_code);

                    foreach ($booking->facturas as $factura) {
                        if ($factura->fecha->lt($fini) || $factura->fecha->gt($ffin)) {
                            continue;
                        }

                        // si hay varias?
                        // if($booking->especialidades->count()>1 && !$factura->manual)
                        // {
                        //     $nSemanas = $booking->weeks;
                        //     $cmulti = $booking->convocatoria;

                        //     $extras = $booking->extras_importe / $nSemanas;
                        //     $dtos = $booking->descuentos_importe / $nSemanas;

                        //     $iMulti = 0;
                        //     foreach($booking->especialidades as $especialidad)
                        //     {
                        //         $iFactura = clone $factura;

                        //         $base = round( ($cmulti->precio + ($iMulti?$cmulti->precio_extra:0)), 2);

                        //         $iFactura->total = round( ($base + $especialidad->precio + $extras - $dtos), 2);
                        //         $iFactura->contable_code = isset($contables[$iMulti])?$contables[$iMulti]:$booking->contable_code;
                        //         $col = $col->push($iFactura);

                        //         $iMulti++;
                        //     }
                        // }
                        // else
                        {
                            $col = $col->push($factura);
                        }
                    }
                }
            }

            return Datatable::collection($col->sortBy('id'))
                ->addColumn('importe', function ($model) {
                    return $model->total;
                })
                ->addColumn('factura_id', function ($model) {
                    return $model->id;
                })
                ->addColumn('factura_num', function ($model) {
                    return $model->numero;
                })
                ->addColumn('booking_id', function ($model) {

                    if ($model->grup) {
                        return $model->grup->bookings ? implode(',', $model->grup->bookings) : "-";
                    }

                    return $model->booking_id;
                })
                ->addColumn('factura_fecha', function ($model) {
                    return $model->fecha->format('Y-m-d');
                })
                ->addColumn('cliente', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->booking->viajero_id) . "'>" . $model->booking->viajero_id . "</a>";
                })
                ->addColumn('razonsocial', function ($model) {

                    if ($model->grup) {
                        if ($model->manual) {
                            return $model->grup->razonsocial;
                        }

                        return $model->razonsocial;
                    }

                    $viajero = $model->booking->viajero;

                    $ret = "";
                    // $ret = $model->booking->datos?($model->booking->datos->fact_razonsocial?:$ret):$ret;

                    $ret = isset($model->datos['razonsocial']) ? ($model->datos['razonsocial'] ? $model->datos['razonsocial'] : $ret) : $ret;

                    if (!$ret) {
                        $ret = isset($model->datos['razonsocial']) ? ($model->datos['razonsocial'] ? $model->datos['razonsocial'] : $ret) : $ret;
                    }

                    if ($model->manual) {
                        $ret = isset($model->datos['razonsocial']) ? ($model->datos['razonsocial'] ? $model->datos['razonsocial'] : $ret) : $ret;
                    }

                    return $ret;
                })
                ->addColumn('nif', function ($model) {

                    if ($model->grup) {
                        if ($model->manual) {
                            return $model->grup->nif;
                        }

                        return $model->nif;
                    }

                    $ret = null;

                    $ret = isset($model->datos['cif']) ? ($model->datos['cif'] ? $model->datos['cif'] : $ret) : $ret;

                    if (!$ret) {
                        $ret = isset($model->datos['cif']) ? ($model->datos['cif'] ? $model->datos['cif'] : $ret) : $ret;
                    }

                    if ($model->manual) {
                        $ret = isset($model->datos['cif']) ? ($model->datos['cif'] ? $model->datos['cif'] : $ret) : $ret;
                    }

                    return $ret;


                    $viajero = $model->booking->viajero;

                    $ret = $viajero->datos->fact_nif ?: ($viajero->datos->documento ?: null);
                    if ($ret) {
                        return $ret;
                    }

                    $tutor = $viajero->tutor1;
                    $ret = $tutor ? $tutor->documento : null;
                    if ($ret) {
                        return $ret;
                    }

                    $tutor = $viajero->tutor2;
                    $ret = $tutor ? $tutor->documento : null;
                    if ($ret) {
                        return $ret;
                    }

                    return "-";

                    /*
                    if($model->manual)
                    {
                        $ret = isset($model->datos['cif'])?$model->datos['cif']:$ret;
                    }

                    $viajero = $model->booking->viajero;
                    if(!$ret)
                    {
                        return $viajero->datos->documento;
                    }
                    return $ret;
                    */
                })
                ->addColumn('contable', function ($model) {

                    //multis
                    if ($model->contable_code) {
                        return $model->contable_code;
                    }

                    return $model->booking->contable_code;
                })
                ->addColumn('curso', function ($model) {
                    return $model->booking->convocatoria ? $model->booking->convocatoria->name : "-";
                })
                ->addColumn('pais', function ($model) {
                    return $model->booking->curso->centro->pais_name;
                })
                ->addColumn('oficina', function ($model) {
                    return $model->booking->oficina ? $model->booking->oficina->name : "-";
                })
                ->addColumn('oficina_id', function ($model) {
                    return $model->booking->oficina_id;
                })
                ->addColumn('concepto', function ($model) {
                    return "";
                })
                ->addColumn('columna', function ($model) {
                    return 4;
                })
                ->addColumn('quien', function ($model) {
                    return $model->booking->factura_quien;
                })
                ->searchColumns('booking_id', 'factura_id', 'factura_num', 'cliente')
                ->orderColumns('factura_id', 'factura_num', 'fecha_factura', 'importe', 'oficina', 'cliente')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.informes.facturacion_fechas', compact('desde', 'hasta'));
    }

    public function getPagos(Request $request)
    {
        $desde = $request->input('desde');
        $hasta = $request->input('hasta');

        if (Datatable::shouldHandle()) {
            ini_set('memory_limit', '400M');
            set_time_limit(0);

            $filtro = ConfigHelper::config('propietario');
            if ($filtro) {
                $viajeros = Viajero::plataforma()->pluck('id')->toArray();
                $bookings = Booking::whereIn('viajero_id', $viajeros)->pluck('id')->toArray();
                $col = BookingPago::whereIn('booking_id', $bookings)->get();
            } else {
                $col = BookingPago::all();
            }

            if ($desde && $hasta) {
                $fini = Carbon::createFromFormat('d/m/Y', $desde)->format('Y-m-d');
                $ffin = Carbon::createFromFormat('d/m/Y', $hasta)->format('Y-m-d');

                if ($filtro) {
                    $col = BookingPago::whereIn('booking_id', $bookings)->where('fecha', '>=', $fini)->where('fecha', '<=', $ffin)->get();
                } else {
                    $col = BookingPago::where('fecha', '>=', $fini)->where('fecha', '<=', $ffin)->get();
                }
            }

            return Datatable::collection($col)
                ->addColumn('concepto', function ($model) {
                    return $model->booking->viajero->datos->fact_concepto ?: $model->booking->curso->name;
                })
                ->addColumn('pais', function ($model) {
                    return $model->booking->curso->centro->pais_name;
                })
                ->showColumns('fecha', 'importe')
                ->showColumns('booking_id')
                ->addColumn('fecha_ini', function ($model) {
                    return $model->booking->course_start_date;
                })
                ->addColumn('viajero_id', function ($model) {
                    return $model->booking->viajero_id;
                })
                ->addColumn('viajero', function ($model) {
                    $viajero = $model->booking->viajero;
                    return $viajero->datos->fact_razonsocial ? $viajero->datos->fact_razonsocial : $viajero->full_name;
                })
                ->addColumn('nif', function ($model) {
                    $viajero = $model->booking->viajero;
                    if (!$viajero->datos->fact_nif) {
                        return $viajero->datos->documento;
                    }

                    return $viajero->datos->fact_nif;
                })
                ->addColumn('cp', function ($model) {
                    $viajero = $model->booking->viajero;
                    return $viajero->datos->cp;
                })
                ->addColumn('contable', function ($model) {
                    return $model->booking->contable_code;
                })
                ->addColumn('ccc', function ($model) {
                    $oficina = $model->booking->viajero->oficina;
                    if (!$oficina) {
                        return "-";
                    }
                    return substr($oficina->iban, -4);
                })
                ->addColumn('oficina', function ($model) {
                    $oficina = $model->booking->viajero->oficina;
                    if (!$oficina) {
                        return "-";
                    }

                    return $oficina->name;
                })
                ->addColumn('oficina_id', function ($model) {
                    return $model->booking->oficina_id;
                })
                ->addColumn('idt', function ($model) {
                    return $model->id;
                })
                ->addColumn('columna', function ($model) {
                    return 1;
                })
                ->addColumn('fact_num', function ($model) {
                    return $model->booking->factura ? $model->booking->factura->numero : "";
                })
                ->searchColumns('viajero', 'nif')
                ->orderColumns('fecha', 'importe')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.informes.pagos', compact('desde', 'hasta'));
    }

    public function getMenores12(Request $request)
    {
        // $desde = $request->input('desde');
        // $hasta = $request->input('hasta');

        if (Datatable::shouldHandle()) {
            // if($desde && $hasta)
            // {
            //     $fini = Carbon::createFromFormat('d/m/Y',$desde)->format('Y-m-d');
            //     $ffin = Carbon::createFromFormat('d/m/Y',$hasta)->format('Y-m-d');

            //     // $col = BookingPago::where('fecha','>=',$fini)->where('fecha','<=',$ffin)->groupBy('booking_id')->get();
            // }

            $filtro = ConfigHelper::config('propietario');
            if ($filtro) {
                $viajeros = Viajero::plataforma()->pluck('id')->toArray();
                $bookings = Booking::where('status_id', '>', 0)->whereIn('viajero_id', $viajeros)->where('transporte_ok', false)->where('vuelo_id', '>', 0)->get();
            } else {
                $bookings = Booking::where('status_id', '>', 0)->where('transporte_ok', false)->where('vuelo_id', '>', 0)->get();
            }

            $col = collect([]);
            foreach ($bookings as $booking) {
                $viajero = $booking->viajero;
                // $edad = Carbon::parse($viajero->fechanac)->age;

                $dtb = Carbon::parse($booking->course_start_date);
                $dtv = Carbon::parse($viajero->fechanac);
                $edad = $dtb->diffInYears($dtv);

                // if($edad>0 && $edad<12)
                if ($edad < 12) {
                    $col->push($booking);
                }
            }

            return Datatable::collection($col)
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria->name;
                })
                ->addColumn('vuelo', function ($model) {
                    return $model->vuelo->name . " : Loc. " . $model->vuelo->localizador;
                })
                ->addColumn('agencia', function ($model) {
                    return $model->vuelo->agencia ? $model->vuelo->agencia->name : "-";
                })
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('pasaporte', function ($model) {
                    return $model->viajero->datos->pasaporte;
                })
                ->addColumn('edad', function ($model) {
                    // return Carbon::parse($model->viajero->fechanac)->age

                    $dtb = Carbon::parse($model->course_start_date);
                    $dtv = Carbon::parse($model->viajero->fechanac);
                    $edad = $dtb->diffInYears($dtv);

                    $ret = "$edad (" . Carbon::parse($model->viajero->fechanac)->format('d/m/Y') . ")";

                    return $ret;
                })
                ->addColumn('opciones', function ($model) {
                    $ret = " <a href='" . route('manage.bookings.vuelo_ok', [$model->id]) . "' class='btn btn-info btn-xs'> Vuelo OK</a>";

                    return $ret;
                })
                // ->setRowClass(function($row) {
                //     return 'row_status_'.$row->status_id;
                // })
                // ->searchColumns('name')
                // ->orderColumns('name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.informes.menores12'); //, compact('desde','hasta'));
    }

    public function getSeguros(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
        }

        $valores['oficinas'] = intval($request->input('oficinas', 0));
        $oficina_id = $valores['oficinas'];

        $results = $request->has('any');
        

        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        $desdeb = $valores['any'] ."-01-01";
        $hastab = $valores['any'] ."-12-31";

        $desde = $request->input('desde');
        $hasta = $request->input('hasta');

        $valores['desde'] = $desde;
        $valores['hasta'] = $hasta;

        $oficina = Oficina::find($oficina_id);
        $oficina_name = $oficina ? $oficina->name : "Todas";

        if (Datatable::shouldHandle())
        {
            $extras_sc = Extra::where('es_cancelacion', true)->pluck('id')->toArray();
            $extras_c = CursoExtraGenerico::whereIn('generics_id', $extras_sc)->pluck('id')->toArray();
            $extras = BookingExtra::whereIn('extra_id', $extras_sc)->where('tipo', 5)->pluck('booking_id')->toArray();

            // $extras = BookingExtra::where('name','LIKE','%Cancela%')->pluck('booking_id')->toArray();

            // $st = ConfigHelper::config('booking_status_cancelado');
            $st = \VCN\Models\Bookings\Status::where('plazas', 1)->pluck('id')->toArray();
            $st[] = ConfigHelper::config('booking_status_overbooking');
            
            if ($oficina_id > 0)
            {
                $ofis = User::where('oficina_id', $oficina_id)->pluck('id')->toArray();
                $col = Booking::whereIn('status_id', $st)->whereIn('id', $extras)->whereIn('user_id', $ofis)->groupBy('id');
                // where('status_id', '>', 0)->where('status_id', '<>', $st)
            }
            else
            {
                // where('status_id', '>', 0)->where('status_id', '<>', $st)
                $col = Booking::whereIn('status_id', $st)->whereIn('id', $extras)->groupBy('id');

                $plat = ConfigHelper::config('propietario');
                if ($plat) {
                    // $viajeros = Viajero::plataforma()->pluck('id');
                    // $col = $col->whereIn('viajero_id', $viajeros);
                    $col = $col->where('plataforma', $plat);
                }
            }

            if($desde && $hasta)
            {
                $fini = Carbon::createFromFormat('d/m/Y', $desde)->format('Y-m-d');
                $ffin = Carbon::createFromFormat('d/m/Y', $hasta)->format('Y-m-d');
                $col  = $col->whereBetween('fecha_reserva', [$fini, $ffin]);
            }

            $col = $col->whereBetween('course_start_date', [$desdeb, $hastab]);
            $col = $col->get();

            return Datatable::collection($col)
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('booking', function ($model) {
                    return "<a data-label='Booking' href='" . route('manage.bookings.ficha', $model->id) . "'>" . $model->id . "</a>";
                })
                ->addColumn('fecha_ini', function ($model) {
                    return ($model->course_start_date >= $model->accommodation_start_date) ? $model->accommodation_start_date : $model->course_start_date;
                })
                ->addColumn('fecha_fin', function ($model) {
                    return ($model->course_end_date <= $model->accommodation_end_date) ? $model->accommodation_end_date : $model->course_end_date;
                })
                ->addColumn('reserva', function ($model) {
                    return Carbon::parse($model->fecha_reserva)->format('Y-m-d');
                })
                ->addColumn('tramo', function ($model) {

                    $precioS = $model->seguro_cancelacion_en_moneda;
                    $precio = $model->total_en_moneda - $precioS;

                    $seguro = $model->cancelacion_seguro ? $model->cancelacion_seguro->extra : null;

                    if ($seguro) {
                        return "SC " . intval($seguro->cancelacion_rango1) . "-" . intval($seguro->cancelacion_rango2);
                    }

                    return "?";
                })
                ->addColumn('contable', function ($model) {
                    return $model->contable_code;
                })
                // ->addColumn('opciones', function($model) {

                // })
                ->searchColumns('viajero', 'booking')
                ->orderColumns('name', 'booking')
                ->setAliasMapping()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;
        asort($anys);

        return view('manage.informes.seguros', compact('valores', 'plataformas', 'desde', 'hasta', 'oficina_id', 'oficina_name', 'oficinas', 'anys', 'results'));
    }

    public function getAvisosReserva(Request $request)
    {
        if (Datatable::shouldHandle()) {
            $status_id = ConfigHelper::config('booking_status_prereserva');

            $col = Booking::where('status_id', '>', 0)->where('status_id', $status_id);

            // $plat = ConfigHelper::config('propietario');
            // if($plat)
            // {
            //     $col = $col->where('plataforma', $plat);
            // }
            $col = $col->get();

            return Datatable::collection($col)
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('plataforma', function ($model) {
                    return $model->plataforma_name;
                })
                ->addColumn('booking', function ($model) {
                    return "<a data-label='Booking' href='" . route('manage.bookings.ficha', $model->id) . "'>" . $model->id . "</a>";
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('curso_ini', function ($model) {
                    return $model->course_start_date;
                })
                ->addColumn('reserva', function ($model) {
                    return Carbon::parse($model->fecha_prereserva)->format('d/m/Y (D)');
                })
                ->addColumn('aviso1', function ($model) {

                    $aviso = $model->aviso1;
                    return $aviso ? $aviso->format('d/m/Y (D)') : "NO";
                })
                ->addColumn('aviso2', function ($model) {

                    //Aviso user
                    $aviso = $model->aviso2;
                    return $aviso ? $aviso->format('d/m/Y (D)') : "NO";
                })
                ->addColumn('caduca', function ($model) {

                    $caduca = $model->caduca;
                    return $caduca ? $caduca->format('d/m/Y (D)') : "NO";
                })
                ->addColumn('asignado', function ($model) {
                    return $model->viajero->asignado ? $model->viajero->asignado->full_name : "-";
                })
                ->addColumn('opciones', function ($model) {
                })
                ->searchColumns('viajero')
                ->orderColumns('curso_ini')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.informes.avisos_reserva'); //, compact('desde','hasta','oficina_id','oficina_name','oficinas'));
    }

    public function getNoRepetidores(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = intval($request->input('plataformas', 0));
        } else {
            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        $valores['any1'] = intval($request->input('any1', Carbon::now()->year));

        $valores['oficinas'] = intval($request->input('oficinas', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['subcategorias'] = intval($request->input('subcategorias', 0));

        $listado = $request->has('any');
        $total = 0;

        if ($listado) {
            $any = $valores['any']; //Año sin booking

            $fini = "$any-01-01";
            $ffin = "$any-12-31";

            // $bookings2 = Booking::where($valores)->get()->pluck('viajero_id')->toArray();
            $st = \VCN\Models\Bookings\Status::where('plazas', 1)->pluck('id')->toArray();
            $st[] = ConfigHelper::config('booking_status_overbooking');
            $bookings2 = Booking::whereIn('status_id', $st)->where('fecha_pago1', '>=', $fini)->where('fecha_pago1', '<=', $ffin)->get()->pluck('viajero_id')->toArray();

            $any1 = $valores['any1']; //Año booking
            $valores['desde'] = "01/01/$any1";
            $valores['hasta'] = "31/12/$any1";

            $valores1 = $valores;
            unset($valores1['any']);
            unset($valores1['any1']);

            $bookings = Booking::listadoFiltros($valores1)->whereNotIn('viajero_id', $bookings2);

            //Dtt
            if (Datatable::shouldHandle()) {
                $bookings = $bookings->get();

                return Datatable::collection($bookings)
                    ->addColumn('viajero', function ($model) {
                        return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                    })
                    ->addColumn('convocatoria', function ($model) {
                        return $model->convocatoria ? $model->convocatoria->name : "-";
                    })
                    ->addColumn('fecha', function ($model) {
                        return $model->fecha_pago1->format('Y-m-d');
                    })
                    ->addColumn('edad', function ($model) {
                        return $model->viajero->edad;
                    })
                    ->addColumn('tutor1_nombre', function ($model) {
                        $t = $model->viajero->tutor1;

                        return $t ? $t->full_name : "-";
                    })
                    ->addColumn('tutor1_movil', function ($model) {
                        $t = $model->viajero->tutor1;

                        return $t ? $t->movil : "-";
                    })
                    ->addColumn('tutor2_nombre', function ($model) {
                        $t = $model->viajero->tutor2;

                        return $t ? $t->full_name : "-";
                    })
                    ->addColumn('tutor2_movil', function ($model) {
                        $t = $model->viajero->tutor2;

                        return $t ? $t->movil : "-";
                    })
                    ->addColumn('prescriptor', function ($model) {
                        return $model->prescriptor ? $model->prescriptor->name : "-";
                    })
                    ->searchColumns('viajero', 'convocatoria')
                    ->orderColumns('viajero', 'convocatoria', 'fecha', 'edad', 'prescriptor')
                    ->setSearchStrip()->setOrderStrip()
                    ->setAliasMapping()
                    ->make();
            }
        }

        return view('manage.informes.no_repetidores', compact('valores', 'plataformas', 'oficinas', 'categorias', 'subcategorias', 'anys', 'listado', 'total'));
    }

    public function getControlDivisa(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = intval($request->input('plataformas', 0));
        } else {
            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        $monedas = [0 => ''] + Moneda::pluck('currency_name', 'id')->toArray();

        $valores['any'] = intval($request->input('any', Carbon::now()->format('Y')));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['monedas'] = intval($request->input('monedas', 0));
        $valores['cancelados'] = (bool) $request->input('cancelados');

        $listado = $request->has('any');

        if ($listado) {
            $any = $valores['any'];
            $valores['desdes'] = "01/01/$any";
            $valores['hastas'] = "31/12/$any";

            //Dtt
            if (Datatable::shouldHandle()) {
                if ($valores['cancelados']) {
                    $st = \VCN\Models\Bookings\Status::where('plazas', 1)->pluck('id')->toArray();
                    $sto = [ConfigHelper::config('booking_status_overbooking')];
                    $st = array_merge($st, $sto);
                    $sto = [ConfigHelper::config('booking_status_refund')];
                    $st = array_merge($st, $sto);
                    $sto = [ConfigHelper::config('booking_status_cancelado')];
                    $st = array_merge($st, $sto);

                    $valores['status'] = $st;
                }

                // $valores['tipoc'] = 5; //Todas las cerradas
                $bookings = Booking::listadoFiltros($valores);

                // monedas
                $f = isset($valores['monedas']) ? $valores['monedas'] : 0;
                if ($f) {
                    $p = CerradaPrecio::where('proveedor_moneda_id', $f)->pluck('convocatory_id')->toArray();
                    $bookings = $bookings->whereIn('convocatory_close_id', $p);
                }

                $bookings = $bookings->get();

                return Datatable::collection($bookings)
                    ->addColumn('booking', function ($model) {
                        return $model->id;
                    })
                    ->addColumn('contable', function ($model) {
                        return $model->contable_code;
                    })
                    ->addColumn('fecha', function ($model) {
                        return $model->fecha_pago1 ? $model->fecha_pago1->format('Y-m-d') : "-";
                    })
                    ->addColumn('viajero', function ($model) {
                        return $model->viajero->full_name;
                    })
                    ->addColumn('oficina', function ($model) {
                        return $model->oficina ? $model->oficina->name : "-";
                    })
                    ->addColumn('convocatoria', function ($model) {
                        return $model->convocatoria->name;
                    })
                    ->addColumn('duracion', function ($model) {
                        return $model->duracion_name;
                    })
                    ->addColumn('pais', function ($model) {
                        return $model->curso->centro->pais_name;
                    })
                    ->addColumn('proveedor', function ($model) {
                        return $model->curso->centro->proveedor->name;
                    })
                    ->addColumn('coste', function ($model) {
                        $p = $model->convocatoria->precio_auto;

                        if ($p) {
                            $precio = $p->proveedor;
                            return $precio;
                        }

                        return "-";
                    })
                    ->addColumn('divisa', function ($model) {
                        $p = $model->convocatoria->precio_auto;

                        if ($p) {
                            $m = Moneda::find($p->proveedor_moneda_id)->name;
                            return $m;
                        }

                        return "-";
                    })
                    ->addColumn('total', function ($model) {
                        return $model->total;
                    })
                    ->addColumn('pagado', function ($model) {
                        return $model->saldo_pagado;
                    })
                    ->addColumn('pendiente', function ($model) {
                        return $model->saldo_pendiente;
                    })
                    ->addColumn('status', function ($model) {
                        return $model->status_name;
                    })
                    ->searchColumns('booking', 'viajero', 'convocatoria', 'oficina', 'pais', 'proveedor')
                    ->orderColumns('booking', 'total', 'pagado', 'pendiente', 'status', 'coste', 'convocatoria', 'fecha', 'oficina', 'pais', 'proveedor')
                    ->setSearchStrip()->setOrderStrip()
                    ->setAliasMapping()
                    ->make();
            }
        }

        return view('manage.informes.control_divisa', compact('valores', 'listado', 'anys', 'categorias', 'plataformas', 'proveedores', 'monedas'));
    }

    public function getVentasGraficos(Request $request)
    {
        // ini_set('memory_limit', '400M');
        // set_time_limit(0);

        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            // $subcategoriasdet = [0=>'Todas'] + SubcategoriaDetalle::all()->sortBy('name')->pluck('name','id')->toArray();
            $categoriasp = [0 => 'Todas'] + [-1 => '- Sin Prescriptor -'] + CategoriaPrescriptor::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = intval($request->input('plataformas', 0));

            $categorias_graf = Categoria::all()->sortBy('name');
        } else {
            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            // $subcategoriasdet = [0=>'Todas'] + SubcategoriaDetalle::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $categoriasp = [0 => 'Todas'] + [-1 => '- Sin Prescriptor -'] + CategoriaPrescriptor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();

            $categorias_graf = Categoria::plataforma()->sortBy('name');
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        $semanas = [];
        for ($i = 1; $i <= 52; $i++) {
            $semanas[$i] = $i;
        }

        $valores['any'] = intval($request->input('any', Carbon::now()->format('Y')));
        $valores['semana'] = intval($request->input('semana', 0));

        $valores['oficinas'] = $request->input('oficinas', 0);
        $valores['categoriasp'] = intval($request->input('categoriasp', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['subcategorias'] = intval($request->input('subcategorias', 0));

        $resultados = $request->has('semana') ? true : false;
        $ventas = null;
        $ventasCurso = null;
        $ofis = null;

        if ($resultados) {
            $user = $request->user();
            if ($user->informe_oficinas) {
                $plataforma =  $valores['plataformas'];
                if ($plataforma) {
                    $ofis = Oficina::where('propietario', $plataforma)->orWhere('propietario', 0)->pluck('id')->toArray();
                } else {
                    $ofis = Oficina::all()->pluck('id')->toArray();
                }

                // if($valores['oficinas'])
                // {
                //     $ventas = Venta::select();
                //     $solicitudes = Solicitud::select();
                // }
                // else
                {
                    //Lo quitamos para contabilizar ventas del año anterior
                    $ventas = Venta::whereIn('oficina_id', $ofis); //->where('semana','<=',$valores['semana']);
                    $ventasCurso = VentaCurso::whereIn('oficina_id', $ofis); //->where('semana','<=',$valores['semana']);
                    $solicitudes = Solicitud::whereIn('oficina_id', $ofis);
                }
            } else {
                $oficina_id = $user->oficina_id;
                $ventas = Venta::where('oficina_id', $oficina_id);
                $ventasCurso = VentaCurso::where('oficina_id', $oficina_id);
                $solicitudes = Solicitud::where('oficina_id', $oficina_id);

                $ofis[] = $oficina_id;
            }

            // filtros
            // $valores['oficinas']
            // $valores['categoriasp']
            // $valores['categorias']
            // $valores['subcategorias']

            if ($valores['oficinas']) {
                if ($valores['oficinas'][0] != 0) {
                    $ventas = $ventas->whereIn('oficina_id', $valores['oficinas']);
                    $ventasCurso = $ventasCurso->whereIn('oficina_id', $valores['oficinas']);
                    $solicitudes = $solicitudes->whereIn('oficina_id', $valores['oficinas']);
                    $ofis = Oficina::whereIn('id', $valores['oficinas'])->orderBy('name')->pluck('id')->toArray();
                }
            }

            if ($valores['categorias']) {
                $ventas = $ventas->where('category_id', $valores['categorias']);
                $ventasCurso = $ventasCurso->where('category_id', $valores['categorias']);
                $solicitudes = $solicitudes->where('category_id', $valores['categorias']);
            }

            if ($valores['subcategorias']) {
                $ventas = $ventas->where('subcategory_id', $valores['subcategorias']);
                $ventasCurso = $ventasCurso->where('subcategory_id', $valores['subcategorias']);
                $solicitudes = $solicitudes->where('subcategory_id', $valores['subcategorias']);
            }

            if ($valores['categoriasp']) {
                $prescriptores = Prescriptor::where('categoria_id', $valores['categoriasp'])->pluck('id')->toArray();
                $ventas = $ventas->whereIn('prescriptor_id', $prescriptores);
                $ventasCurso = $ventasCurso->whereIn('prescriptor_id', $prescriptores);

                $viajeros = Viajero::whereIn('prescriptor_id', $prescriptores)->pluck('id')->toArray();
                $solicitudes = $solicitudes->whereIn('viajero_id', $viajeros);
            } elseif ($valores['categoriasp'] < 0) {
                $ventas = $ventas->whereIn('prescriptor_id', 0);
                $ventasCurso = $ventasCurso->whereIn('prescriptor_id', 0);

                $viajeros = Viajero::where('prescriptor_id', 0)->pluck('id')->toArray();
                $solicitudes = $solicitudes->whereIn('viajero_id', $viajeros);
            }
            //filtros end

            Graficos::loadVentas($valores, $ventas, $ventasCurso, $solicitudes, $ofis, $categorias_graf);
        }

        return view(
            'manage.informes.ventas_graficos',
            compact(
                'resultados',
                'valores',
                'anys',
                'semanas',
                'plataformas',
                'oficinas',
                'categoriasp',
                'categorias',
                'subcategorias',
                'ventas',
                'ventasCurso',
                'ofis'
            )
        );
    }

    public function getVentas(Request $request)
    {
        $semanaActual = Carbon::now()->weekOfYear;

        $plataforma = ConfigHelper::config('propietario');

        $cat_mc = 4;
        $camps_txt = "MAX CAMPS";
        if ($plataforma == 2) //cic
        {
            $cat_mc = 6;
            $camps_txt = "CAMPAMENTOS CIC";
        }

        $user = auth()->user();
        if ($user->informe_oficinas) {
            if ($plataforma) {
                $ofis = Oficina::where('propietario', $plataforma)->orWhere('propietario', 0)->pluck('id')->toArray();
                $voficina = Venta::whereIn('oficina_id', $ofis);

                $anys = Venta::whereIn('oficina_id', $ofis)->groupBy('any')->get();
            } else {
                $ofis = Oficina::all()->pluck('id')->toArray();

                $voficina = Venta::where('oficina_id', '>', 0);
                $anys = Venta::groupBy('any')->get();
            }

            //Dashboard stats
            $adultos = Venta::whereIn('oficina_id', $ofis)->whereIn('category_id', [1, 7, 8])->where('semana', '<=', $semanaActual)->get();
            $jovenes = Venta::whereIn('oficina_id', $ofis)->where('category_id', 2)->where('semana', '<=', $semanaActual)->get();
            $aescolar = Venta::whereIn('oficina_id', $ofis)->where('category_id', 3)->where('semana', '<=', $semanaActual)->get();
            $camps = Venta::whereIn('oficina_id', $ofis)->where('category_id', $cat_mc)->where('semana', '<=', $semanaActual)->get();
        } else {
            $oficina_id = $user->oficina_id;

            //Dashboard stats
            $adultos = Venta::where('oficina_id', $oficina_id)->whereIn('category_id', [1, 7, 8])->where('semana', '<=', $semanaActual)->get();
            $jovenes = Venta::where('oficina_id', $oficina_id)->where('category_id', 2)->where('semana', '<=', $semanaActual)->get();
            $aescolar = Venta::where('oficina_id', $oficina_id)->where('category_id', 3)->where('semana', '<=', $semanaActual)->get();
            $camps = Venta::where('oficina_id', $oficina_id)->where('category_id', $cat_mc)->where('semana', '<=', $semanaActual)->get();

            $voficina = Venta::where('oficina_id', $oficina_id);
            $anys = Venta::where('oficina_id', $oficina_id)->groupBy('any')->get();
        }


        $ventas = [];
        $oficinas = [];
        foreach ($anys as $any) {
            $vo = clone $voficina;

            $ventas[$any->any] = $vo->where('any', $any->any)->where('semana', '<=', $semanaActual)->get();
            $oficinas[$any->any] = $vo->where('any', $any->any)->where('semana', '<=', $semanaActual)->groupBy('oficina_id')->get();
        }


        return view('manage.informes.ventas', compact('ventas', 'oficinas', 'anys', 'semanaActual', 'jovenes', 'adultos', 'aescolar', 'camps', 'camps_txt'));
    }

    public function getVentasAnalisis(Request $request, $todas = false, $esCurso = false)
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);

        $semanaActual = (int) $todas ? 0 : Carbon::now()->weekOfYear;
        $esCurso = (bool) $esCurso;

        if (auth()->user()->isFullAdmin()) {
            $fil_oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
        } else {
            $fil_oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $fil_oficinas = [0 => 'Todas'] + $fil_oficinas;
            }
        }

        $valores['fil_oficinas'] = $request->input('fil_oficinas', []);
        if (count($valores['fil_oficinas']) > 1) {
            if ($valores['fil_oficinas'][0] == 0) {
                unset($valores['fil_oficinas'][0]);
            }
        } elseif (!$valores['fil_oficinas']) {
            $valores['fil_oficinas'][0] = 0;
        }

        $plataformas = null;
        $plataforma = ConfigHelper::config('propietario');

        $plataformas = ConfigHelper::plataformas();

        $valores['categorias'] = $request->get('categorias', 0);
        $valores['subcategorias'] = $request->get('subcategorias', 0);

        $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        if ($valores['categorias']) {
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->where('category_id', $valores['categorias'])->sortBy('name')->pluck('name', 'id')->toArray();
        } else {
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }


        $any = $request->get('any', Carbon::now()->year);
        $any01 = $any - 1;
        $any02 = $any - 2;

        $anys = [];
        $anys[$any02] = $any02;
        $anys[$any01] = $any01;
        $anys[$any] = $any;

        $hoy = Carbon::now()->year;
        if ($hoy != $any) {
            $anys[$hoy] = $hoy;
        }

        if ($esCurso) {
            $Clase = new VentaCurso;
        } else {
            $Clase = new Venta;
        }

        $user = auth()->user();
        if ($user->informe_oficinas) {
            if (isset($valores['fil_oficinas'][0]) && $valores['fil_oficinas'][0] == 0) {
                $ofis = Oficina::where('propietario', $plataforma)->pluck('id', 'id')->toArray();
                // $ofis[0] = 0;
            } else {
                $ofis = $valores['fil_oficinas'];
            }

            $voficina = $Clase::whereIn('oficina_id', $ofis);
        } else {
            $oficina_id = $user->oficina_id;
            $voficina = $Clase::where('oficina_id', $oficina_id);
        }

        if ($valores['categorias']) {
            $voficina = $voficina->where('category_id', $valores['categorias']);
        }

        if ($valores['subcategorias']) {
            $voficina = $voficina->where('subcategory_id', $valores['subcategorias']);
        }

        $oficinas = [];
        $ventas = [];

        $fAny = 'any';
        $fSemana = 'semana';

        if ($esCurso) {
            $fAny = 'curso_any';
            $fSemana = 'curso_semana';
        }

        //Ventas
        $vo = clone $voficina;
        $vv = clone $voficina;

        if ($semanaActual) {
            $oficinas = $vo->where($fAny, $any)->where($fSemana, '<=', $semanaActual)->groupBy('oficina_id')->get();
            $ventas = $vv->where($fAny, $any)->where($fSemana, '<=', $semanaActual)->get();
        } else {
            $oficinas = $vo->where($fAny, $any)->groupBy('oficina_id')->get();
            $ventas = $vv->where($fAny, $any)->get();
        }

        // dd($ventas->sum('inscripciones'));

        $iany = $any;
        $href = \Request::url() . "?" . http_build_query($valores);
        return view('manage.informes.ventas_analisis', compact('href', 'ventas', 'oficinas', 'anys', 'iany', 'semanaActual', 'plataformas', 'esCurso', 'fil_oficinas', 'valores', 'categorias', 'subcategorias'));
    }



    public function getFiltrosJson(Request $request)
    {
        $data = $request->input('data');

        if (!$request->ajax()) {
            abort(404);
        }

        $filtro = $data['filtro'];
        $valor = $data['valor']; //explode(',',$data['valor']);
        $destino = $data['destino'];
        $online = isset($data['online']) ? (int) $data['online'] : 0;
        $tipoc = 0;
        if (isset($data['tipoc'])) {
            $tipoc = intval($data['tipoc']);
        }

        $datos = null;
        $res = false;
        $group = false;

        // Log::info("$destino - $filtro - $tipoc");

        switch ($destino) {

            case 'users': {
                    if ($valor == 0) {
                        $datos = [0 => 'Todos'] + User::crm()->plataforma()->pluck('username', 'id')->toArray();
                    } else {
                        $datos = $users = [0 => 'Todos'] + User::crm()->where('plataforma', $valor)->pluck('username', 'id')->toArray();
                    }

                    $res = true;
                }
                break;

            case 'vuelos': {
                    $datos = [0 => 'Todos'] + Vuelo::where('any', $valor)->orderBy('name')->pluck('name', 'id')->toArray();

                    $res = true;
                }
                break;

            case 'convocatorias': {
                    switch ($filtro) {
                        case 'tipoc': {
                                $curso = 0; //???

                                switch ($tipoc) {
                                    case 1: {
                                            if ($curso == 0) {
                                                $datos = [0 => 'Todas'] + Cerrada::where('convocatory_semiopen', 0)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                            } else {
                                                $cursos = Proveedor::find($valor)->cursos->sortBy('name')->pluck('name', 'id')->toArray();
                                                $datos = [0 => 'Todas'] + Cerrada::whereIn('course_id', $cursos)->where('convocatory_semiopen', 0)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                            }
                                        }
                                        break;

                                    case 2: {
                                            if ($curso == 0) {
                                                $datos = [0 => 'Todas'] + Cerrada::where('convocatory_semiopen', 1)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                            } else {
                                                $cursos = Proveedor::find($valor)->cursos->sortBy('name')->pluck('name', 'id')->toArray();
                                                $datos = [0 => 'Todas'] + Cerrada::whereIn('course_id', $cursos)->where('convocatory_semiopen', 1)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                            }
                                        }
                                        break;

                                    case 5: //cerrada
                                        {
                                            if ($curso == 0) {
                                                $datos = [0 => 'Todas'] + Cerrada::all()->sortBy('name')->pluck('name', 'id')->toArray();
                                            } else {
                                                $cursos = Proveedor::find($valor)->cursos->sortBy('name')->pluck('name', 'id')->toArray();
                                                $datos = [0 => 'Todas'] + Cerrada::whereIn('course_id', $cursos)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                            }
                                        }
                                        break;

                                    case 3: //abierta
                                        {
                                            if ($curso == 0) {
                                                $datos = [0 => 'Todas'] + Abierta::all()->sortBy('name')->pluck('name', 'id')->toArray();
                                            } else {
                                                $cursos = Proveedor::find($valor)->cursos->sortBy('name')->pluck('name', 'id')->toArray();
                                                $datos = [0 => 'Todas'] + Abierta::whereIn('course_id', $cursos)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                            }
                                        }
                                        break;

                                    case 4: //multi
                                        {
                                            if ($curso == 0) {
                                                $datos = [0 => 'Todas'] + ConvocatoriaMulti::all()->sortBy('name')->pluck('name', 'id')->toArray();
                                            } else {
                                                $cursos = Proveedor::find($valor)->cursos->sortBy('name')->pluck('name', 'id')->toArray();

                                                $datos = [0 => 'Todas'];
                                                foreach ($cursos as $v) {
                                                    $datos += \VCN\Models\Convocatorias\ConvocatoriaMulti::whereRaw(
                                                        'find_in_set(?, cursos_id)',
                                                        [$v]
                                                    )->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                                }
                                            }
                                        }
                                        break;
                                }
                            }
                            break;

                        case 'proveedores': {
                                switch ($tipoc) {
                                    case 1:
                                    case 2:
                                    case 5: //cerrada
                                        {
                                            if ($valor == 0) {
                                                $datos = [0 => 'Todas'] + Cerrada::all()->sortBy('name')->pluck('name', 'id')->toArray();
                                            } else {
                                                $cursos = Proveedor::find($valor)->cursos->sortBy('name')->pluck('name', 'id')->toArray();
                                                $datos = [0 => 'Todas'] + Cerrada::whereIn('course_id', $cursos)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                            }
                                        }
                                        break;
                                }
                            }
                            break;

                        case 'centros': {
                                switch ($tipoc) {
                                    case 1:
                                    case 2:
                                    case 5: //cerrada
                                        {
                                            $cursos = Centro::find($valor)->cursos->sortBy('name')->pluck('name', 'id')->toArray();
                                            $datos = [0 => 'Todas'] + Cerrada::where('course_id', $valor)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                        }
                                        break;
                                }
                            }

                        case 'cursos': {
                                if ($tipoc == 0) {
                                    if (is_array($valor)) {
                                        $curso = Curso::whereIn('id', $valor)->first();
                                    } else {
                                        $curso = Curso::find($valor);
                                    }

                                    $tipoc = $curso ? $curso->convocatoria_tipo_num : 0;
                                }

                                switch ($tipoc) {
                                    case 1:
                                    case 2:
                                    case 5: //cerrada
                                        {
                                            if (is_array($valor)) {
                                                $datos = [0 => 'Todas'] + Cerrada::whereIn('course_id', $valor)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                            } else {
                                                $datos = [0 => 'Todas'] + Cerrada::where('course_id', $valor)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                            }
                                        }
                                        break;

                                    case 3: //abierta
                                        {
                                            if (is_array($valor)) {
                                                $datos = [0 => 'Todas'] + Abierta::whereIn('course_id', $valor)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                            } else {
                                                $datos = [0 => 'Todas'] + Abierta::where('course_id', $valor)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                            }
                                        }
                                        break;

                                    case 4: //Multi
                                        {
                                            if (is_array($valor)) {
                                                $datos = [0 => 'Todas'];
                                                foreach ($valor as $v) {
                                                    $datos += \VCN\Models\Convocatorias\ConvocatoriaMulti::whereRaw(
                                                        'find_in_set(?, cursos_id)',
                                                        [$v]
                                                    )->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                                }
                                            } else {
                                                $datos = [0 => 'Todas'];
                                                $datos += \VCN\Models\Convocatorias\ConvocatoriaMulti::whereRaw(
                                                    'find_in_set(?, cursos_id)',
                                                    [$valor]
                                                )->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                            }
                                        }
                                        break;
                                }
                            }
                    }

                    $res = true;
                }
                break;

            case 'categoriasp': {
                    if ($valor == 0) {
                        $datos = [0 => 'Todas'] + CategoriaPrescriptor::plataforma()->pluck('name', 'id')->toArray();
                    } else {
                        $datos = [0 => 'Todas'] + CategoriaPrescriptor::where('propietario', $valor)->orWhere('propietario', 0)->orderBy('name')->pluck('name', 'id')->toArray();
                    }

                    $res = true;
                }
                break;

            case 'oficinas': {
                    if ($valor == 0) {
                        $datos = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
                        if (auth()->user()->informe_oficinas) {
                            $datos = [0 => 'Todas'] + $datos;
                        }
                    } else {
                        $datos = [0 => 'Todas'] + Oficina::where('propietario', $valor)->orWhere('propietario', 0)->orderBy('name')->pluck('name', 'id')->toArray();
                    }

                    $res = true;
                }
                break;

            case 'not-prescriptores':
            case 'prescriptores': {
                    switch ($filtro) {
                        case 'plataformas': {
                                $p = Oficina::where('propietario', $valor)->pluck('id')->toArray();
                                $datos = [0 => 'Todos'] + Prescriptor::whereIn('oficina_id', $p)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                            }
                            break;

                        case 'oficinas': {
                                if ($valor == 0) {
                                    $datos = [0 => 'Todos'] + Prescriptor::plataforma()->pluck('name', 'id')->toArray();
                                } else {
                                    $datos = [0 => 'Todos'] + Prescriptor::where('oficina_id', $valor)->orderBy('name')->pluck('name', 'id')->toArray();
                                }
                            }
                    }

                    $res = true;
                }
                break;

            case 'proveedores': {
                    $datos = [0 => 'Todos'] + Proveedor::where('propietario', $valor)->orWhere('propietario', 0)->orderBy('name')->pluck('name', 'id')->toArray();
                    $res = true;
                }
                break;

            case 'centros': {
                    switch ($filtro) {
                        case 'plataformas': {
                                if ($tipoc) {
                                    $p = Proveedor::where('propietario', $valor)->orWhere('propietario', 0)->pluck('id')->toArray();
                                    $datos = [0 => 'Todos'] + Centro::convocatoria($tipoc)->whereIn('provider_id', $p)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                } else {
                                    $p = Proveedor::where('propietario', $valor)->orWhere('propietario', 0)->pluck('id')->toArray();
                                    $datos = [0 => 'Todos'] + Centro::whereIn('provider_id', $p)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                }
                            }
                            break;

                        case 'proveedores': {
                                if ($tipoc) {
                                    $datos = [0 => 'Todos'] + Centro::convocatoria($tipoc)->where('provider_id', $valor)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                } else {
                                    $datos = [0 => 'Todos'] + Centro::where('provider_id', $valor)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                }
                            }
                            break;

                        case 'tipoc': {
                                $datos = [0 => 'Todos'] + Centro::convocatoria($tipoc)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                            }
                            break;
                    }

                    $res = true;
                }
                break;

            case 'cursos': {
                    switch ($filtro) {
                        case 'plataformas': {
                                if ($tipoc) {
                                    $datos = [0 => 'Todos'] + Curso::convocatoria($tipoc)->where('propietario', $valor)->orWhere('propietario', 0)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                } else {
                                    $datos = [0 => 'Todos'] + Curso::where('propietario', $valor)->orWhere('propietario', 0)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                }
                            }
                            break;

                        case 'centros': {
                                if ($tipoc) {
                                    $datos = [0 => 'Todos'] + Curso::convocatoria($tipoc)->where('center_id', $valor)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                } else {
                                    $datos = [0 => 'Todos'] + Curso::where('center_id', $valor)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                }
                            }
                            break;

                        case 'proveedores': {
                                if ($tipoc) {
                                    $p = Centro::where('provider_id', $valor)->pluck('id')->toArray();
                                    $datos = [0 => 'Todos'] + Curso::convocatoria($tipoc)->whereIn('center_id', $p)->orderBy('course_name')->get()->pluck('name', 'id')->toArray();
                                } else {
                                    $p = Centro::where('provider_id', $valor)->pluck('id')->toArray();
                                    $datos = [0 => 'Todos'] + Curso::whereIn('center_id', $p)->orderBy('course_name')->get()->pluck('name', 'id')->toArray();
                                }
                            }
                            break;

                        case 'tipoc': {
                                $datos = [0 => 'Todos'] + Curso::convocatoria($tipoc)->orderBy('course_name')->get()->pluck('name', 'id')->toArray();
                            }
                            break;

                        case 'categorias': {

                                if ($online) {
                                    if ($valor == 0) {
                                        $datos = [0 => 'Todos'] + Curso::plataforma()->where('course_active', 1)->where('activo_web', 1)->sortBy('course_name')->pluck('name', 'id')->toArray();
                                    } else {
                                        $cat = Categoria::find($valor);
                                        $datos = [0 => 'Todos'] + Curso::plataforma()->where('course_active', 1)->where('activo_web', 1)->where('category_id', $cat->id)->sortBy('course_name')->pluck('name', 'id')->toArray();
                                    }
                                } else {
                                    if ($valor == 0) {
                                        $datos = [0 => 'Todos'] + Curso::plataforma()->sortBy('course_name')->pluck('name', 'id')->toArray();
                                    } else {
                                        $cat = Categoria::find($valor);
                                        $datos = [0 => 'Todos'] + Curso::plataforma()->where('category_id', $cat->id)->sortBy('course_name')->pluck('name', 'id')->toArray();
                                    }
                                }
                            }
                            break;

                        case 'subcategorias': {

                                if ($online) {
                                    if ($valor == 0) {
                                        $datos = [0 => 'Todos'] + Curso::plataforma()->where('course_active', 1)->where('activo_web', 1)->sortBy('course_name')->pluck('name', 'id')->toArray();
                                    } else {
                                        $cat = Subcategoria::find($valor);
                                        $datos = [0 => 'Todos'] + Curso::plataforma()->where('course_active', 1)->where('activo_web', 1)->where('subcategory_id', $cat->id)->sortBy('course_name')->pluck('name', 'id')->toArray();
                                    }
                                } else {
                                    if ($valor == 0) {
                                        $datos = [0 => 'Todos'] + Curso::plataforma()->sortBy('course_name')->pluck('name', 'id')->toArray();
                                    } else {
                                        $cat = Subcategoria::find($valor);
                                        $datos = [0 => 'Todos'] + Curso::plataforma()->where('subcategory_id', $cat->id)->sortBy('course_name')->pluck('name', 'id')->toArray();
                                    }
                                }
                            }
                            break;

                        case 'subcategoriasdet': {
                                if ($valor == 0) {
                                    $datos = [0 => 'Todos'] + Curso::plataforma()->orderBy('course_name')->get()->pluck('name', 'id')->toArray();
                                } else {
                                    $cat = SubcategoriaDetalle::find($valor);
                                    $datos = [0 => 'Todos'] + Curso::plataforma()->where('subcategory_det_id', $cat->id)->sortBy('course_name')->pluck('name', 'id')->toArray();
                                }
                            }
                            break;
                    }

                    $res = true;
                }
                break;

            case 'categorias': {
                    $datos = [0 => 'Todos'] + Categoria::where('propietario', $valor)->orWhere('propietario', 0)->get()->sortBy('name')->pluck('name', 'id')->toArray();

                    $res = true;
                }
                break;

            case 'subcategorias': {
                    if (is_array($valor)) {
                        $datos = [0 => 'Todas'] + Subcategoria::whereIn('category_id', $valor)->pluck('name', 'id')->toArray();
                    } else {
                        if ($valor == 0) {
                            $datos = [0 => 'Todas'] + Subcategoria::plataforma()->pluck('name', 'id')->toArray();
                        } else {
                            $datos = [0 => 'Todas'] + Subcategoria::where('category_id', $valor)->orderBy('name')->pluck('name', 'id')->toArray();
                        }
                    }

                    $res = true;
                }
                break;

            case 'subcategoriasdet': {
                    switch ($filtro) {
                        case 'categorias': {
                                if (is_array($valor)) {
                                    $sc = Subcategoria::whereIn('category_id', $valor)->pluck('id')->toArray();
                                    $datos = [0 => 'Todas'] + SubcategoriaDetalle::whereIn('subcategory_id', $sc)->orderBy('name')->pluck('name', 'id')->toArray();
                                } else {
                                    if ($valor == 0) {
                                        $datos = [0 => 'Todas'] + SubcategoriaDetalle::plataforma()->pluck('name', 'id')->toArray();
                                    } else {
                                        $sc = Subcategoria::where('category_id', $valor)->pluck('id')->toArray();
                                        $datos = [0 => 'Todas'] + SubcategoriaDetalle::whereIn('subcategory_id', $sc)->orderBy('name')->pluck('name', 'id')->toArray();
                                    }
                                }
                            }
                            break;

                        case 'subcategorias': {
                                if (is_array($valor)) {
                                    $datos = [0 => 'Todas'] + SubcategoriaDetalle::whereIn('subcategory_id', $valor)->orderBy('name')->pluck('name', 'id')->toArray();
                                } else {
                                    if ($valor == 0) {
                                        $datos = [0 => 'Todas'] + SubcategoriaDetalle::plataforma()->pluck('name', 'id')->toArray();
                                    } else {
                                        $datos = [0 => 'Todas'] + SubcategoriaDetalle::where('subcategory_id', $valor)->orderBy('name')->pluck('name', 'id')->toArray();
                                    }
                                }
                            }
                            break;
                    }

                    $res = true;
                }
                break;

                /* ------- */
            case 'categoriasg': {
                    $datos = Categoria::where('propietario', $valor)->orWhere('propietario', 0)->get()->sortBy('name')->pluck('name', 'id')->toArray();

                    $res = true;
                    $group = true;
                }
                break;

            case 'subcategoriasg': {
                    switch ($filtro) {
                        case 'categoriasg': {
                                if (is_array($valor)) {
                                    //$datos = Subcategoria::whereIn('category_id', $valor)->pluck('name', 'id')->toArray();

                                    $result = Subcategoria::whereIn('category_id', $valor)->orderBy('name')->get(['name', 'category_id', 'id'])->groupBy('category_id')->toArray();
                                } else {
                                    if ($valor == 0) {
                                        //$datos = [0 => 'Todas'] + Subcategoria::plataforma()->pluck('name', 'id')->toArray();
                                        $c = Categoria::where('propietario', $valor)->orWhere('propietario', 0)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                        $result = Subcategoria::whereIn('category_id', $c)->orderBy('name')->get(['name', 'category_id', 'id'])->groupBy('category_id')->toArray();
                                    } else {
                                        //$datos = [0 => 'Todas'] + Subcategoria::where('category_id', $valor)->orderBy('name')->pluck('name', 'id')->toArray();
                                        $result = Subcategoria::where('category_id', $valor)->orderBy('name')->get(['name', 'category_id', 'id'])->groupBy('category_id')->toArray();
                                    }
                                }

                                foreach ($result as $key => $val) {
                                    foreach ($val as $k => $v) {
                                        $datos[$key][] = ['index' => $v['id'], 'value' => $v['name']];
                                    }
                                }
                                $group = true;
                                $res = true;
                            }
                            break;
                        case 'subcategoriasg': {
                                if (is_array($valor)) {
                                    //$sc = Subcategoria::whereIn('id', $valor)->pluck('id')->toArray();
                                    $result = SubcategoriaDetalle::whereIn('subcategory_id', $valor)->orderBy('name')->get(['name', 'subcategory_id', 'id'])->groupBy('subcategory_id')->toArray();
                                } else {
                                    if ($valor == 0) {
                                        $result = SubcategoriaDetalle::plataforma()->orderBy('name')->get(['name', 'subcategory_id', 'id'])->groupBy('subcategory_id')->toArray();
                                    } else {
                                        $result = SubcategoriaDetalle::where('subcategory_id', $valor)->orderBy('name')->get(['name', 'subcategory_id', 'id'])->groupBy('subcategory_id')->toArray();
                                    }
                                }

                                foreach ($result as $key => $val) {
                                    foreach ($val as $k => $v) {
                                        $datos[$key][] = ['index' => $v['id'], 'value' => $v['name']];
                                    }
                                }
                                $group = true;
                                $res = true;
                            }
                            break;
                    }
                }
                break;

            case 'subcategoriasdetg': {
                    switch ($filtro) {

                        case 'categoriasg': {
                                if (is_array($valor)) {
                                    //$sc = Subcategoria::whereIn('id', $valor)->pluck('id')->toArray();
                                    $result = SubcategoriaDetalle::whereIn('subcategory_id', $valor)->orderBy('name')->get(['name', 'subcategory_id', 'id'])->groupBy('subcategory_id')->toArray();
                                } else {
                                    if ($valor == 0) {
                                        //$result = SubcategoriaDetalle::plataforma()->orderBy('name')->get(['name','subcategory_id','id'])->groupBy('subcategory_id')->toArray();

                                        $sc = Subcategoria::plataforma()->pluck('id')->toArray();
                                        $result = SubcategoriaDetalle::whereIn('subcategory_id', $sc)->orderBy('name')->get(['name', 'subcategory_id', 'id'])->groupBy('subcategory_id')->toArray();
                                    } else {
                                        $sc = Subcategoria::whereIn('id', $valor)->pluck('id')->toArray();
                                        $result = SubcategoriaDetalle::whereIn('subcategory_id', $sc)->orderBy('name')->get(['name', 'subcategory_id', 'id'])->groupBy('subcategory_id')->toArray();
                                    }
                                }

                                foreach ($result as $key => $val) {
                                    foreach ($val as $k => $v) {
                                        $datos[$key][] = ['index' => $v['id'], 'value' => $v['name']];
                                    }
                                }
                                $res = true;
                                $group = true;
                            }
                            break;

                        case 'subcategoriasg': {
                                if (is_array($valor)) {
                                    //$sc = Subcategoria::whereIn('id', $valor)->pluck('id')->toArray();
                                    $result = SubcategoriaDetalle::whereIn('subcategory_id', $valor)->orderBy('name')->get(['name', 'subcategory_id', 'id'])->groupBy('subcategory_id')->toArray();
                                } else {
                                    if ($valor == 0) {
                                        $result = SubcategoriaDetalle::plataforma()->orderBy('name')->get(['name', 'subcategory_id', 'id'])->groupBy('subcategory_id')->toArray();
                                    } else {
                                        $result = SubcategoriaDetalle::where('subcategory_id', $valor)->orderBy('name')->get(['name', 'subcategory_id', 'id'])->groupBy('subcategory_id')->toArray();
                                    }
                                }

                                foreach ($result as $key => $val) {
                                    foreach ($val as $k => $v) {
                                        $datos[$key][] = ['index' => $v['id'], 'value' => $v['name']];
                                    }
                                }
                                $res = true;
                                $group = true;
                            }
                            break;
                    }
                }
                break;

                /* especialidades */
            case 'especialidadesg': {
                    $datos = Especialidad::where('propietario', $valor)->orWhere('propietario', 0)->get()->sortBy('name')->pluck('name', 'id')->toArray();

                    $res = true;
                    $group = true;
                }
                break;

            case 'subespecialidadesg': {
                    switch ($filtro) {
                        case 'especialidadesg': {
                                if (is_array($valor)) {
                                    $result = Subespecialidad::whereIn('especialidad_id', $valor)->orderBy('name')->get(['name', 'especialidad_id', 'id'])->groupBy('especialidad_id')->toArray();
                                } else {
                                    if ($valor == 0) {
                                        $c = Especialidad::where('propietario', $valor)->orWhere('propietario', 0)->get()->sortBy('name')->pluck('name', 'id')->toArray();
                                        $result = Subespecialidad::whereIn('especialidad_id', $c)->orderBy('name')->get(['name', 'especialidad_id', 'id'])->groupBy('especialidad_id')->toArray();
                                    } else {
                                        $result = Subespecialidad::where('especialidad_id', $valor)->orderBy('name')->get(['name', 'especialidad_id', 'id'])->groupBy('especialidad_id')->toArray();
                                    }
                                }

                                foreach ($result as $key => $val) {
                                    foreach ($val as $k => $v) {
                                        $datos[$key][] = ['index' => $v['id'], 'value' => $v['name']];
                                    }
                                }
                                $group = true;
                                $res = true;
                            }
                            break;
                    }
                }
                break;
        }

        if ($group == true) {

            $data = ['result' => $res, 'datos' => $datos, 'group' => 'group'];
            return response()->json($data, 200);
        } else {
            if ($datos) {
                $datos = array_map(function ($index, $value) {
                    return ['index' => $index, 'value' => $value];
                }, array_keys($datos), $datos);
            }

            $data = ['result' => $res, 'datos' => $datos];

            return response()->json($data, 200);
        }
    }

    public function getBookingGrupos(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }

            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }

            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $valores['oficinas'] = intval($request->input('oficinas', 0));
        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['tipoc'] = intval($request->input('tipoc', 0));

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        // dd($valores);

        $listado = $request->has('plataformas') ? true : false;

        //Para listado: +tabs
        $tabs = [];

        if ($listado) {
            $bookings = Booking::bookingGrupos($valores);

            $bcursos = clone $bookings;
            $bcursos = $bcursos->groupBy('curso_id')->get();
            foreach ($bcursos as $b) {
                if (!isset($tabs[$b->curso->proveedor->id])) {
                    $tabs[$b->curso->proveedor->id] = [];
                }
                array_push($tabs[$b->curso->proveedor->id], $b->curso->id);
            }
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            // $bookings = Booking::bookingGrupos($valores);
            $bookings = $bookings->get();

            return Datatable::collection($bookings)
                ->addColumn('nombre', function ($model) {
                    return $model->viajero->name;
                })
                ->addColumn('apellidos', function ($model) {
                    return $model->viajero->lastname . " " . $model->viajero->lastname2;
                })
                ->addColumn('sexo', function ($model) {
                    return $model->viajero->sexo_name;
                })
                ->addColumn('fechanac', function ($model) {
                    return $model->viajero->fechanac;
                })
                ->addColumn('convocatoria', function ($model) {
                    if ($model->convocatoria) {
                        return $model->convocatoria->name;
                    }
                    return "-";
                })
                ->addColumn('convo_ini', function ($model) {
                    return $model->course_start_date;
                })
                ->addColumn('convo_fin', function ($model) {
                    return $model->course_end_date;
                })
                ->addColumn('fecha', function ($model) {
                    return $model->fecha_reserva ? $model->fecha_reserva->format('Y-m-d') : "-";
                })
                ->addColumn('alojamiento', function ($model) {
                    return $model->alojamiento ? $model->alojamiento->name : "-";
                })
                ->addColumn('status', function ($model) {
                    return $model->status_name;
                })
                ->addColumn('asignado', function ($model) {
                    return $model->asignado ? $model->asignado->full_name : "-";
                })
                ->addColumn('comentarios', function ($model) {
                    $ret = $model->notas_txt;
                    if ($model->datos) {
                        $ret .= $ret ? "<br>" : "";
                        $ret .= $model->datos->alergias_txt ? $model->datos->alergias_txt . "<br>" : "";
                        $ret .= $model->datos->enfermedad_txt ? $model->datos->enfermedad_txt . "<br>" : "";
                        $ret .= $model->datos->medicacion_txt ? $model->datos->medicacion_txt . "<br>" : "";
                        $ret .= $model->datos->tratamiento_txt ? $model->datos->tratamiento_txt . "<br>" : "";
                        $ret .= $model->datos->dieta_txt;
                    }

                    return $ret;
                })
                ->addColumn('extras', function ($model) {
                    return $model->extras_name;
                })
                // ->searchColumns('viajero')
                ->orderColumns('convo_ini', 'convo_fin', 'convocatoria', 'alojamiento', 'status', 'fecha', 'apellidos', 'nombre')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;
        asort($anys);
        
        return view('manage.informes.booking_grupos', compact('valores', 'tabs', 'listado', 'categorias', 'plataformas', 'oficinas', 'proveedores', 'centros', 'cursos', 'anys'));
    }

    public function getBookingGruposPdf(Request $request)
    {
        $valores = $request->except('_token', 'filtro1', 'filtro2');

        $listado = $request->has('plataformas') ? true : false;
        if (!$listado) {
            return redirect()->route('manage.informes.booking-grupos', $valores);
        }

        $tabs = [];
        $bookings = Booking::bookingGrupos($valores);

        $bcursos = clone $bookings;
        $bcursos = $bcursos->groupBy('curso_id')->get();
        foreach ($bcursos as $b) {
            if (!isset($tabs[$b->curso->proveedor->id])) {
                $tabs[$b->curso->proveedor->id] = [];
            }
            array_push($tabs[$b->curso->proveedor->id], $b->curso->id);

            // if(!isset($tabs[$b->curso->proveedor->id][$b->curso->id]))
            // {
            //     $tabs[$b->curso->proveedor->id][$b->curso->id] = [];
            // }
            // array_push($tabs[$b->curso->proveedor->id][$b->curso->id], $b->convocatoria->id);
        }

        $valores_txt = [];

        $valores_txt['plataformas'] = intval($valores['plataformas']) == 0 ? 'Todas' : ConfigHelper::plataforma(intval($valores['plataformas']));
        $valores_txt['oficinas'] = intval($request->input('oficinas', 0)) == 0 ? 'Todas' : Oficina::find(intval($valores['oficinas']))->name;
        $valores_txt['proveedores'] = intval($request->input('proveedores', 0)) == 0 ? 'Todos' : Proveedor::find(intval($valores['proveedores']))->name;
        $valores_txt['centros'] = intval($request->input('centros', 0)) == 0 ? 'Todos' : Centro::find(intval($valores['centros']))->name;
        $valores_txt['cursos'] = intval($request->input('cursos', 0)) == 0 ? 'Todos' : Curso::find(intval($valores['cursos']))->name;
        $valores_txt['categorias'] = intval($request->input('categorias', 0)) == 0 ? 'Todas' : Categoria::find(intval($valores['categorias']))->name;
        $valores_txt['tipoc'] = intval($request->input('tipoc', 0)) == 0 ? 'Todas' : ConfigHelper::getConvocatoriaTipo(intval($valores['tipoc']));

        // return view('manage.informes.booking_grupos_pdf',compact('valores','valores_txt','tabs'));

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView('manage.informes.booking_grupos_pdf', compact('valores', 'valores_txt', 'tabs'));
        return $pdf->setPaper('a4')->setOrientation('landscape')->download('informe.pdf');
    }

    public function getConvocatoriasMulti(Request $request)
    {
        $convo_id = $request->input('convocatorias');

        $convocatorias = [0 => ''] + ConvocatoriaMulti::all()->pluck('name', 'id')->toArray();
        $prescriptores = [0 => 'Todos'] + Prescriptor::plataforma()->pluck('name', 'id')->toArray();

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        $valores['tipoc'] = 4; //multi
        $valores['convocatorias'] = $convo_id;
        $valores['prescriptores'] = $request->input('prescriptores', 0);

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = "01/01/" . $valores['any'];
            $valores['hastas'] = "31/12/" . $valores['any'];
        }

        if (!$convo_id) {
            return view('manage.informes.convocatoria_multi', compact('convo_id', 'convocatorias', 'anys', 'valores', 'prescriptores'));
        }

        $convocatoria = ConvocatoriaMulti::find($convo_id);

        $semanas = $convocatoria->semanas->count();

        //Dtt
        if (Datatable::shouldHandle()) {

            $valores['datos'] = $request->get('datos');

            $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->get();

            if ($valores['datos']) {
                return Datatable::collection($bookings)
                    ->addColumn('nombre', function ($model) {
                        return $model->viajero->name;
                    })
                    ->addColumn('apellidos', function ($model) {
                        return $model->viajero->lastname . " " . $model->viajero->lastname2;
                    })
                    ->addColumn('semanas', function ($model) use ($semanas) {

                        $ret = "";

                        for ($i = 1; $i <= $semanas; $i++) {
                            $iSemana = $model->convocatoria->semanas->where('semana', $i)->first();
                            if ($iSemana) {
                                $e = $model->multis->where('semana_id', $iSemana->id)->first();
                                if ($e) {
                                    $ret .= "S$i, ";
                                }
                            }
                        }

                        return $ret;
                    })
                    ->addColumn('tutor1', function ($model) {
                        $t = $model->viajero->tutor1;

                        return $t ? $t->full_name : "-";
                    })
                    ->addColumn('tutor1_movil', function ($model) {
                        $t = $model->viajero->tutor1;

                        return $t ? $t->movil : "-";
                    })
                    ->addColumn('tutor2', function ($model) {
                        $t = $model->viajero->tutor2;

                        return $t ? $t->full_name : "-";
                    })
                    ->addColumn('tutor2_movil', function ($model) {
                        $t = $model->viajero->tutor2;

                        return $t ? $t->movil : "-";
                    })
                    ->addColumn('origen_txt', function ($model) {
                        return $model->full_name_origen_txt;
                    })
                    ->searchColumns('apellidos', 'nombre')
                    ->orderColumns('apellidos', 'nombre', 'origen_txt')
                    ->setAliasMapping()
                    ->setSearchStrip()->setOrderStrip()
                    ->make();
            } else {

                $dtt = Datatable::collection($bookings)
                    ->addColumn('nombre', function ($model) {
                        return $model->viajero->name;
                    })
                    ->addColumn('apellidos', function ($model) {
                        return $model->viajero->lastname . " " . $model->viajero->lastname2;
                    })
                    ->addColumn('fechanac', function ($model) {
                        return $model->viajero->fechanac;
                    })
                    ->addColumn('fecha_reserva', function ($model) {
                        return $model->fecha_reserva ? $model->fecha_reserva->format('Y-m-d') : "";
                    })
                    ->addColumn('fechaini', function ($model) {
                        return $model->course_start_date;
                    })
                    ->addColumn('semanas', function ($model) {
                        return $model->weeks;
                    })->addColumn('origen_txt', function ($model) {
                        return $model->full_name_origen_txt;
                    });

                $order = ['apellidos', 'nombre', 'semanas', 'fechanac', 'fecha_reserva', 'fechaini', 'cuestionario_resultado', 'cuestionario_nivel'];
                for ($i = 1; $i <= $semanas; $i++) {
                    $dtt->addColumn("semana$i", function ($model) use ($i) {

                        $m = $model->multis;
                        if (!$m) {
                            return "x";
                        }

                        $iSemana = $model->convocatoria->semanas->where('semana', $i)->first();
                        if (!$iSemana) {
                            return "-";
                        }

                        $e = $model->multis->where('semana_id', $iSemana->id)->first();
                        if (!$e) {
                            return "-";
                        }

                        $esp = $e->especialidad->especialidad ?? null;
                        $ret = $esp ? $esp->name : "-";

                        return $ret;
                    });

                    $order[] = "semana$i";
                }

                $dtt->addColumn('extras', function ($model) {
                    return $model->extras_name;
                })
                    ->addColumn('notas', function ($model) {
                        $ret = $model->notas_txt;
                        if ($model->datos) {
                            $ret .= $ret ? "<br>" : "";
                            $ret .= $model->datos->alergias_txt ? $model->datos->alergias_txt . "<br>" : "";
                            $ret .= $model->datos->enfermedad_txt ? $model->datos->enfermedad_txt . "<br>" : "";
                            $ret .= $model->datos->medicacion_txt ? $model->datos->medicacion_txt . "<br>" : "";
                            $ret .= $model->datos->tratamiento_txt ? $model->datos->tratamiento_txt . "<br>" : "";
                            $ret .= $model->datos->dieta_txt;
                        }

                        return $ret;
                    })
                    ->addColumn('cuestionario_resultado', function ($model) {
                        $c = $model->cuestionarios->first();
                        return $c ? $c->resultado : "-";
                    })
                    ->addColumn('cuestionario_nivel', function ($model) {
                        $c = $model->cuestionarios->first();
                        return $c ? $c->nivel : "-";
                    });


                return $dtt->searchColumns('apellidos', 'nombre')
                    ->orderColumns($order, 'origen_txt')
                    ->setAliasMapping()
                    //->setSearchStrip()->setOrderStrip()
                    ->make();
            }
        }

        return view('manage.informes.convocatoria_multi', compact('convo_id', 'convocatoria', 'convocatorias', 'semanas', 'anys', 'valores', 'prescriptores'));
    }

    public function getFacturacion(Request $request, $saldo = 0, $user_id = 0, $oficina_id = 0)
    {
        $any = intval($request->input('any', Carbon::now()->format('Y')));

        if (!$user_id) {
            $user_id = $request->user()->id;
        }

        if (!$oficina_id) {
            $oficina_id = 'all';
        }

        $user = auth()->user();
        if (!$user->informe_oficinas) {
            $oficina_id = $user->oficina_id;
        }

        //si elige oficina que se quede seleccionado el primero de esa ofi
        if ($oficina_id > 0) {
            $asig = User::find($user_id);
            if ($asig) {
                if ($asig->oficina_id != $oficina_id) {
                    $asignado = User::where('oficina_id', $oficina_id)->orderBy('fname')->first();
                    $user_id = $asignado->id;
                }
            }
        }


        if ($user_id > 0 && $oficina_id > 0) {
            $user = User::find($user_id);
            if ($user->oficina_id != $oficina_id) {
                $error = "Combinación incorrecta. Oficina que no corresponde al usuario. Solo filtra usuario.";
                Session::flash('mensaje', $error);
            }
        }

        if (Datatable::shouldHandle()) {
            $bookings = Booking::select(DB::raw('bookings.*, (bookings.total - SUM(booking_pagos.importe)) as saldo'))
                ->where('status_id', '>', 0);

            //Filtros
            if ($user_id > 0) {
                $bookings = $bookings->where('bookings.user_id', $user_id);
            } else {
                if ($user_id == 'all') {
                    $usuarios = User::asignados()->pluck('id')->toArray();
                } else {
                    $usuarios = User::asignadosOff()->pluck('id')->toArray();
                }
                $bookings = $bookings->whereIn('bookings.user_id', $usuarios);

                if ($oficina_id > 0) {
                    $bookings = $bookings->where('bookings.oficina_id', $oficina_id);
                } else {
                    $plat = ConfigHelper::config('propietario');
                    if ($plat) {
                        $bookings = $bookings->where('bookings.plataforma', $plat);
                    }
                }
            }

            $bookings = $bookings->join('booking_pagos', 'bookings.id', '=', 'booking_pagos.booking_id');

            if ($saldo == 0) {
                $bookings = $bookings->havingRaw('(bookings.total - SUM(booking_pagos.importe))=0');
            } elseif ($saldo == 1) {
                $bookings = $bookings->havingRaw('(bookings.total - SUM(booking_pagos.importe))>0');
            } elseif ($saldo == 2) {
                $bookings = $bookings->havingRaw('(bookings.total - SUM(booking_pagos.importe))<0');
            }

            $stplazas = \VCN\Models\Bookings\Status::where('plazas', 1)->pluck('id')->toArray();
            $bookings = $bookings->whereIn('status_id', $stplazas);

            $fecha1 = $any . "-01-01";
            $fecha2 = $any . "-12-31";
            $bookings = $bookings->where('course_start_date', '>=', $fecha1)->where('course_start_date', '<=', $fecha2);

            $bookings = $bookings->groupBy('booking_id')->get();

            return Datatable::collection($bookings)
                ->addColumn('fecha_reserva', function ($model) {
                    return $model->fecha_reserva ? $model->fecha_reserva->format('Y-m-d') : '-';
                })
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('curso_fecha', function ($model) {
                    return $model->course_start_date;
                })
                ->addColumn('total', function ($model) {
                    return floatval($model->total);
                })
                ->addColumn('pagado', function ($model) {
                    return floatval($model->saldo_pagado);
                })
                ->addColumn('pendiente', function ($model) {
                    return floatval($model->saldo_pendiente);
                })
                ->addColumn('oficina', function ($model) {
                    return $model->oficina ? $model->oficina->name : "-";
                })
                ->addColumn('options', function ($model) {
                    $ret = "";
                    $ret .= " <a href='" . route('manage.bookings.ficha', [$model->id]) . "' class='btn btn-success btn-xs'><i class='fa fa-pencil'></i> Ficha</a>";
                    return $ret;
                })
                ->searchColumns('name', 'viajero')
                ->orderColumns('fecha_reserva', 'curso_fecha', 'viajero', 'convocatoria')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }


        $oficinas = Oficina::plataforma();
        if ($oficina_id > 0) {
            $asignados = User::where('oficina_id', $oficina_id)->orderBy('fname')->get();
        } else {
            $asignados = User::asignados();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.facturacion', compact('saldo', 'user_id', 'oficina_id', 'asignados', 'oficinas', 'anys', 'any'));
    }


    public function getFacturacionPrecios(Request $request)
    {
        ini_set('memory_limit', '400M');
        set_time_limit(0);

        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $prescriptores = [0 => 'Todos'] + Prescriptor::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = intval($request->input('plataformas', 0));
        } else {
            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $centros = [0 => 'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
            $prescriptores = [0 => 'Tod0s'] + Prescriptor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();
        }

        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['oficinas'] = intval($request->input('oficinas', 0));
        $valores['tipoc'] = intval($request->input('tipoc', 0));
        $valores['prescriptores'] = intval($request->input('prescriptores', 0));

        $valores['desde'] = $request->input('desde', null);
        $valores['hasta'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desde']) {
            $valores['desde'] = "01/01/" . $valores['any'];
            $valores['hasta'] = "31/12/" . $valores['any'];
        }

        $listado = $request->has('centros') ? true : false;

        //Para listado: +tabs
        $results = 0;
        $totales = null;

        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);

            // dd($bookings->get());
            $results = $bookings->count();

            // $model = $bookings->first();
            // $c = ConfigHelper::parseMoneda($model->course_total_amount, $model->curso_moneda);
            // dd($c);

            $totales = [];
            $totales['total'] = 0;
            $totales['pagado'] = 0;
            $totales['pendiente'] = 0;
            $totales['curso'] = 0;
            $totales['alojamiento'] = 0;
            $totales['early'] = 0;
            $totales['descuentos'] = 0;
            $totales['seguro'] = 0;
            $totales['extras_centro'] = 0;
            $totales['extras_curso'] = 0;
            $totales['extras_alojamiento'] = 0;
            $totales['extras_generico'] = 0;
            $totales['extras_otros'] = 0;
            foreach ($bookings->get() as $model) {

                $totales['total'] += $model->total;

                $p = isset($model->importes['saldo_pagado']) ? $model->importes['saldo_pagado'] : $model->saldo_pagado;
                $totales['pagado'] += floatval($p);

                $p = isset($model->importes['saldo_pendiente']) ? $model->importes['saldo_pendiente'] : $model->saldo_pendiente;
                $totales['pendiente'] += floatval($p);

                $p = $model->course_total_amount;
                $totales['curso'] += floatval($p);

                $p = $model->accommodation_total_amount;
                $totales['alojamiento'] += floatval($p);

                $p = $model->descuento_early ? $model->descuento_early_importe : 0;
                $totales['early'] += floatval($p);

                $p = $model->precio_descuento_solo;
                $totales['descuentos'] += floatval($p);

                $p = $model->precio_cancelacion;
                $totales['seguro'] += floatval($p);

                $p = $model->extras_centro_importe;
                $totales['extras_centro'] += floatval($p);

                $p = $model->extras_curso_importe;
                $totales['extras_curso'] += floatval($p);

                $p = $model->extras_alojamiento_importe;
                $totales['extras_alojamiento'] += floatval($p);

                $p = $model->extras_generico_importe;
                $totales['extras_generico'] += floatval($p);

                $p = $model->extras_otros_importe;
                $totales['extras_otros'] += floatval($p);
            }
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            // $bookings_id = $bookings->pluck('id')->toArray();
            // $bookings = Booking::whereIn('id',$bookings_id);

            return Datatable::collection($bookings->get())
                ->addColumn('fecha_reserva', function ($model) {
                    return $model->fecha_reserva ? $model->fecha_reserva->format('Y-m-d') : '-';
                })
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('curso_fecha', function ($model) {
                    return $model->course_start_date;
                })
                ->addColumn('oficina', function ($model) {
                    return $model->oficina ? $model->oficina->name : "-";
                })
                ->addColumn('total', function ($model) {
                    return floatval($model->total);
                })
                ->addColumn('pagado', function ($model) {
                    $p = isset($model->importes['saldo_pagado']) ? $model->importes['saldo_pagado'] : $model->saldo_pagado;
                    return floatval($p);
                })
                ->addColumn('pendiente', function ($model) {
                    $p = isset($model->importes['saldo_pendiente']) ? $model->importes['saldo_pendiente'] : $model->saldo_pendiente;
                    return floatval($p);
                })
                ->addColumn('curso', function ($model) {
                    return $model->course_total_amount;
                })
                ->addColumn('curso_moneda', function ($model) {
                    return $model->curso_moneda;
                })
                ->addColumn('alojamiento', function ($model) {
                    return $model->alojamiento ? $model->accommodation_total_amount : "-";
                })
                ->addColumn('alojamiento_moneda', function ($model) {
                    return $model->alojamiento ? $model->alojamiento_moneda : "-";
                })
                ->addColumn('early', function ($model) {
                    return $model->descuento_early ? $model->descuento_early_importe : "-";
                })
                ->addColumn('early_moneda', function ($model) {
                    return $model->descuento_early ? $model->descuento_early->moneda->name : "-";
                })
                ->addColumn('descuentos', function ($model) {
                    return $model->precio_descuento_solo;
                })
                ->addColumn('seguro', function ($model) {
                    return $model->precio_cancelacion;
                })
                ->addColumn('extras_centro', function ($model) {
                    return $model->extras_centro_importe;
                })
                ->addColumn('extras_curso', function ($model) {
                    return $model->extras_curso_importe;
                })
                ->addColumn('extras_alojamiento', function ($model) {
                    return $model->extras_alojamiento_importe;
                })
                ->addColumn('extras_generico', function ($model) {
                    return $model->extras_generico_importe;
                })
                ->addColumn('extras_otros', function ($model) {
                    return $model->extras_otros_importe;
                })
                ->addColumn('origen_txt', function ($model) {
                    return $model->full_name_origen_txt;
                })
                ->addColumn('divisa', function ($model) {
                    return $model->curso_moneda;
                })
                ->addColumn('vdivisa', function ($model) {
                    return $model->vdivisa ?: 0;
                })
                ->addColumn('options', function ($model) {
                    $ret = "";
                    $ret .= " <a href='" . route('manage.bookings.ficha', [$model->id]) . "' class='btn btn-success btn-xs'><i class='fa fa-pencil'></i> Ficha</a>";
                    return $ret;
                })
                ->searchColumns('name', 'viajero')
                ->orderColumns('fecha_reserva', 'curso_fecha', 'viajero', 'convocatoria', 'pendiente', 'total', 'pagado')
                // ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.facturacion_precios', compact('valores', 'results', 'listado', 'categorias', 'centros', 'cursos', 'oficinas', 'prescriptores', 'anys', 'totales', 'plataformas'));
    }


    public function getPlazasVuelos(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        } else {
            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));

        $valores['desde'] = $request->input('desde', null);
        $valores['hasta'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desde']) {
            $valores['desde'] = "01/01/" . $valores['any'];
            $valores['hasta'] = "31/12/" . $valores['any'];
        }

        // $valores['anyd'] = Carbon::createFromFormat('d/m/Y', $valores['desde'])->year;
        // $valores['anyh'] = Carbon::createFromFormat('d/m/Y', $valores['hasta'])->year;

        $valores['plataformas'] = 0;
        $valores['oficinas'] = 0;
        $valores['tipoc'] = 5; //Todas las cerradas

        $listado = $request->has('proveedores') ? true : false;

        //Para listado: +tabs
        $results = 0;

        if ($listado) {
            $vuelos = Vuelo::listadoFiltrosTodos($valores);
            // dd($vuelos->count());

            // $bookings = Booking::listadoFiltros($valores);
            // $bookings = $bookings->where('vuelo_id','>',0);
            // $bookings_todos = clone $bookings;
            // $bookings = $bookings->groupBy('vuelo_id');

            // $bookings_aux = clone $bookings;
            // $vuelosBooking = $bookings_aux->pluck('vuelo_id')->toArray();
            // $vuelos = DB::table('vuelos')->where('any', $valores['any'])->whereNotIn('id', $vuelosBooking)->get();

            // foreach($vuelos as $v)
            // {

            // }

            // dd($bookings->get());
            $results = $vuelos->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->where('vuelo_id', '>', 0);
            $bookings_todos = clone $bookings;

            $dtt = Datatable::collection($vuelos)
                ->addColumn('curso', function ($model) {
                    return $model->convocatoria->curso->name;
                })
                ->addColumn('convocatoria', function ($model) {
                    $c = $model->convocatoria->name;
                    return "<a targe='_blank' data-label='Ficha Convocatoria' href='" . route('manage.convocatorias.cerradas.ficha', $model->convocatory_id) . "'>" . $c . "</a>";
                })
                ->addColumn('localizador', function ($model) {
                    return $model->vuelo->localizador;
                })
                ->addColumn('vuelo', function ($model) {
                    $v = $model->vuelo->name;
                    return "<a targe='_blank' data-label='Ficha Vuelo' href='" . route('manage.convocatorias.vuelos.ficha', $model->vuelo_id) . "'>" . $v . "</a>";
                })
                ->addColumn('total', function ($model) {
                    return $model->vuelo->plazas_totales;
                })
                ->addColumn('disponibles', function ($model) {
                    return $model->vuelo->plazas_disponibles;
                })
                ->addColumn('vendidas', function ($model) {
                    return $model->vuelo->plazas_vendidas;
                })
                ->addColumn('vendidas_filtro', function ($model) use ($bookings_todos) {
                    if (!$model->vuelo) {
                        return "-";
                    }

                    $b = clone $bookings_todos;
                    $v = $b->where('vuelo_id', $model->vuelo_id)->count();
                    return $v;
                })
                ->addColumn('monitor', function ($model) {
                    return $model->vuelo->plazas_monitor;
                });

            foreach (ConfigHelper::plataformas() as $kp => $vp) {
                if ($kp == 0) {
                    continue;
                }

                $dtt->addColumn('inscr' . $kp, function ($model) use ($kp) {
                    return $model->vuelo->getPlazasReservasPlataforma($kp);
                });

                $dtt->addColumn('ovbkg' . $kp, function ($model) use ($kp) {
                    return $model->vuelo->getPlazasOverbookingPlataforma($kp);
                });
            }

            $dtt->addColumn('agencia', function ($model) {
                return $model->vuelo->agencia_name;
            })
                ->addColumn('company', function ($model) {
                    return $model->vuelo->company_name;
                })
                ->searchColumns('vuelo', 'curso', 'convocatoria')
                ->orderColumns('vuelo', 'total', 'disponibles', 'curso', 'convocatoria')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip();

            return $dtt->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.plazas_vuelos', compact('valores', 'results', 'listado', 'categorias', 'proveedores', 'centros', 'cursos', 'anys'));
    }

    public function getPlazasAlojamientos(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        } else {
            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));

        $valores['desde'] = $request->input('desde', null);
        $valores['hasta'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desde']) {
            $valores['desde'] = "01/01/" . $valores['any'];
            $valores['hasta'] = "31/12/" . $valores['any'];
        }

        $valores['plataformas'] = 0;
        $valores['oficinas'] = 0;
        $valores['tipoc'] = 5; //Todas las cerradas

        $listado = $request->has('proveedores') ? true : false;

        //Para listado: +tabs
        $results = 0;

        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->where('accommodation_id', '>', 0);
            $bookings_todos = clone $bookings;
            $bookings = $bookings->groupBy('convocatory_close_id');

            // dd($bookings->get());
            $results = $bookings->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            $bookings = $bookings->get();

            $dtt = Datatable::collection($bookings)
                ->addColumn('curso', function ($model) {
                    return $model->curso->name;
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('alojamiento', function ($model) {
                    return $model->alojamiento ? $model->alojamiento->name : "-";
                })
                ->addColumn('total', function ($model) {

                    if (!$model->alojamiento) {
                        return "x";
                    }

                    $a = $model->alojamiento->id;
                    $c = $model->convocatoria;
                    if (!$c) {
                        return "-";
                    }

                    if ($model->curso->convocatoria_tipo_num == 4) //multi
                    {
                        $ret = 0;
                        foreach ($model->convocatoria->semanas as $sem) {
                            $ret += $sem->plazas_totales;
                        }

                        return $ret;
                    }

                    $plazas = $c->getPlazas($a);
                    return $plazas ? $plazas->plazas_totales : "-";
                })
                ->addColumn('disponibles', function ($model) {

                    if (!$model->alojamiento) {
                        return "x";
                    }

                    $a = $model->alojamiento->id;
                    $c = $model->convocatoria;
                    if (!$c) {
                        return "-";
                    }
                    $plazas = $c->getPlazas($a);
                    return $plazas ? $plazas->plazas_disponibles : "-";
                })
                ->addColumn('vendidas', function ($model) {

                    if (!$model->alojamiento) {
                        return "x";
                    }

                    $a = $model->alojamiento->id;
                    $c = $model->convocatoria;
                    if (!$c) {
                        return "-";
                    }

                    if ($model->curso->convocatoria_tipo_num == 4) //multi
                    {
                        $ret = 0;
                        foreach ($model->convocatoria->semanas as $sem) {
                            $ret += $sem->plazas_vendidas;
                        }

                        return $ret;
                    }

                    $plazas = $c->getPlazas($a);
                    return $plazas ? $plazas->plazas_vendidas : "-";
                })
                ->addColumn('vendidas_filtro', function ($model) use ($bookings_todos) {

                    if (!$model->alojamiento) {
                        return "x";
                    }

                    $a = $model->alojamiento->id;
                    $c = $model->convocatoria;
                    if (!$c) {
                        return "-";
                    }

                    // if($model->curso->convocatoria_tipo_num==4) //multi
                    // {
                    //     $ret = 0;
                    //     foreach($model->convocatoria->semanas as $sem)
                    //     {
                    //         $ret += $sem->plazas_vendidas;
                    //     }

                    //     return $ret;
                    // }

                    $b = clone $bookings_todos;
                    $b = $b->where('convocatory_close_id', $model->convocatory_close_id);
                    $p = $b->where('accommodation_id', $a)->count();
                    return $p;
                });

            foreach (ConfigHelper::plataformas() as $kp => $vp) {
                if ($kp == 0) {
                    continue;
                }

                $dtt->addColumn('inscr' . $kp, function ($model) use ($kp) {

                    if (!$model->alojamiento) {
                        return "x";
                    }

                    $a = $model->alojamiento->id;
                    $c = $model->convocatoria;
                    if (!$c) {
                        return "-";
                    }

                    if ($model->curso->convocatoria_tipo_num == 4) //multi
                    {
                        $ret = 0;
                        foreach ($model->convocatoria->semanas as $sem) {
                            $ret += $sem->getPlazasReservasPlataforma($kp);
                        }

                        return $ret;
                    }

                    $plazas = $c->getPlazas($a);
                    return $plazas ? $plazas->getPlazasReservasPlataforma($kp) : "-";
                });

                $dtt->addColumn('ovbkg' . $kp, function ($model) use ($kp) {

                    if (!$model->alojamiento) {
                        return "x";
                    }

                    $a = $model->alojamiento->id;
                    $c = $model->convocatoria;
                    if (!$c) {
                        return "-";
                    }

                    if ($model->curso->convocatoria_tipo_num == 4) //multi
                    {
                        $ret = 0;
                        foreach ($model->convocatoria->semanas as $sem) {
                            $ret += $sem->getPlazasOverbookingPlataforma($kp);
                        }

                        return $ret;
                    }

                    $plazas = $c->getPlazas($a);
                    return $plazas ? $plazas->getPlazasOverbookingPlataforma($kp) : "-";
                });
            }

            $dtt->searchColumns('alojamiento')
                ->orderColumns('alojamiento')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip();

            return $dtt->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.plazas_alojamientos', compact('valores', 'results', 'listado', 'categorias', 'proveedores', 'centros', 'cursos', 'anys'));
    }

    public function getPlazasSemanas(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        } else {
            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $valores['plataformas'] = 0;
        $valores['oficinas'] = 0;
        $valores['tipoc'] = 1; //Sólo las cerradas

        $listado = $request->has('proveedores') ? true : false;

        //Para listado: +tabs
        $results = 0;

        $plazas = [];
        $semanas = [];
        $edades = [];
        $subtotales = [];

        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);

            // dd($bookings->get());
            $results = $bookings->count();

            $list = clone $bookings;
            $booking_curso = $list->groupBy('curso_id');

            foreach ($booking_curso->get() as $bcurso) {
                $plazas[$bcurso->curso_id] = [];
            }

            foreach ($bookings->get() as $booking) {
                $edad = $booking->viajero->edad;
                $sexo = $booking->viajero->sexo ?: 0;
                $curso_id = $booking->curso_id;

                if (!isset($edades[$edad])) {
                    $edades[$edad] = 0;
                }
                $edades[$edad]++;

                //semanas mientras hay booking
                $fecha = Carbon::parse($booking->course_start_date);
                while ($fecha->isWeekend()) {
                    $fecha->addDay();
                }
                $semanaini = $fecha->weekOfYear;

                $fechafin = Carbon::parse($booking->course_end_date);
                // while($fechafin->isWeekend())
                // {
                //     $fechafin->addDay();
                // }
                $semanafin = $fechafin->weekOfYear;

                for ($semana = $semanaini; $semana <= $semanafin; $semana++) {
                    if (!isset($semanas[$semana])) {
                        $semanas[$semana] = 0;
                    }
                    $semanas[$semana]++;

                    //Plazas
                    if (!isset($plazas[$curso_id][$semana])) {
                        $plazas[$curso_id][$semana] = [];
                        $subtotales[$curso_id][$semana] = 0;
                    }

                    if (!isset($plazas[$curso_id][$semana][$edad])) {
                        // $plazas[$booking->curso_id][$semana][$edad] = [];
                        $plazas[$curso_id][$semana][$edad][1] = 0;
                        $plazas[$curso_id][$semana][$edad][2] = 0;
                        $plazas[$curso_id][$semana][$edad][0] = 0;
                    }

                    $plazas[$curso_id][$semana][$edad][$sexo]++;
                    $subtotales[$curso_id][$semana]++;
                }
            }

            ksort($semanas);
            ksort($edades);

            // dd($semanas);
            // dd($plazas);
            // dd($edades);
            // dd($subtotales);
        }

        return view(
            'manage.informes.plazas_semanas',
            compact('valores', 'results', 'listado', 'categorias', 'proveedores', 'centros', 'cursos', 'anys', 'plazas', 'semanas', 'edades', 'subtotales')
        );
    }

    public function getPlazasSemanasPdf(Request $request)
    {
        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $valores['plataformas'] = 0;
        $valores['oficinas'] = 0;
        $valores['tipoc'] = 1; //Sólo las cerradas

        $listado = $request->has('proveedores') ? true : false;

        //Para listado: +tabs
        $results = 0;
        $plazas = null;
        $semanas = null;

        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);

            // dd($bookings->get());
            $results = $bookings->count();

            $list = clone $bookings;
            $booking_curso = $list->groupBy('curso_id');

            $plazas = [];
            $semanas = [];
            $edades = [];
            $subtotales = [];

            foreach ($booking_curso->get() as $bcurso) {
                $plazas[$bcurso->curso_id] = [];
            }

            foreach ($bookings->get() as $booking) {
                $edad = $booking->viajero->edad;
                $sexo = $booking->viajero->sexo ?: 0;
                $curso_id = $booking->curso_id;

                if (!isset($edades[$edad])) {
                    $edades[$edad] = 0;
                }
                $edades[$edad]++;

                //semanas mientras hay booking
                $fecha = Carbon::parse($booking->course_start_date);
                while ($fecha->isWeekend()) {
                    $fecha->addDay();
                }
                $semanaini = $fecha->weekOfYear;

                $fechafin = Carbon::parse($booking->course_end_date);
                // while($fechafin->isWeekend())
                // {
                //     $fechafin->addDay();
                // }
                $semanafin = $fechafin->weekOfYear;

                for ($semana = $semanaini; $semana <= $semanafin; $semana++) {
                    if (!isset($semanas[$semana])) {
                        $semanas[$semana] = 0;
                    }
                    $semanas[$semana]++;

                    //Plazas
                    if (!isset($plazas[$curso_id][$semana])) {
                        $plazas[$curso_id][$semana] = [];
                        $subtotales[$curso_id][$semana] = 0;
                    }

                    if (!isset($plazas[$curso_id][$semana][$edad])) {
                        // $plazas[$booking->curso_id][$semana][$edad] = [];
                        $plazas[$curso_id][$semana][$edad][1] = 0;
                        $plazas[$curso_id][$semana][$edad][2] = 0;
                        $plazas[$curso_id][$semana][$edad][0] = 0;
                    }

                    $plazas[$curso_id][$semana][$edad][$sexo]++;
                    $subtotales[$curso_id][$semana]++;
                }
            }

            ksort($semanas);
            ksort($edades);

            // dd($semanas);
            // dd($plazas);
            // dd($edades);
            // dd($subtotales);
        }

        $valores_txt['proveedores'] = intval($valores['proveedores']) == 0 ? 'Todos' : Proveedor::find(intval($valores['proveedores']))->name;
        $valores_txt['categorias'] = intval($request->input('categorias', 0)) == 0 ? 'Todas' : Categoria::find(intval($valores['categorias']))->name;
        $valores_txt['centros'] = intval($request->input('centros', 0)) == 0 ? 'Todos' : Centro::find(intval($valores['centros']))->name;
        $valores_txt['cursos'] = intval($request->input('cursos', 0)) == 0 ? 'Todos' : Curso::find(intval($valores['cursos']))->name;

        $fecha = Carbon::now()->format('d-m-Y_H-i');

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView('manage.informes.plazas_semanas_pdf', compact('valores', 'valores_txt', 'plazas', 'semanas', 'edades', 'subtotales'));
        return $pdf->setPaper('a4')->setOrientation('landscape')->download("informe_plazas_semanas_$fecha.pdf");
    }

    public function getReunion(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $valores['plataformas'] = intval($valores['plataformas']);

        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));

        $valores['tipoc'] = intval($request->input('tipoc', 0));
        $valores['oficinas'] = intval($request->input('oficinas', 0));

        // $valores['desde'] = $request->input('desde',null);
        // $valores['hasta'] = $request->input('hasta',null);
        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('proveedores') ? true : false;

        //Para listado: +tabs
        $tabs = [];
        $results = 0;

        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);
            // dd($bookings->get());

            $results = $bookings->count();

            $bcursos = clone $bookings;
            $bcursos = $bcursos->groupBy('curso_id')->get();
            foreach ($bcursos as $b) {
                array_push($tabs, $b->curso->id);
            }
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            $bookings = $bookings->get();

            return Datatable::collection($bookings)
                ->addColumn('apellido1', function ($model) {
                    return $model->viajero->lastname;
                })
                ->addColumn('apellido2', function ($model) {
                    return $model->viajero->lastname2;
                })
                ->addColumn('nombre', function ($model) {
                    return $model->viajero->name;
                })
                ->addColumn('ciudad', function ($model) {
                    return $model->viajero->datos->ciudad;
                })
                ->addColumn('provincia', function ($model) {
                    return $model->viajero->datos->provincia_name;
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria->name;
                })
                ->addColumn('plataforma', function ($model) {
                    return ConfigHelper::plataforma($model->plataforma);
                })
                ->addColumn('oficina', function ($model) {
                    return $model->oficina ? $model->oficina->name : "-";
                })
                ->searchColumns('apellido1', 'apellido1', 'nombre')
                ->orderColumns('apellido1', 'apellido1', 'nombre')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.reunion', compact('valores', 'tabs', 'results', 'listado', 'categorias', 'plataformas', 'oficinas', 'proveedores', 'centros', 'cursos', 'anys'));
    }

    public function getEscuelas(Request $request)
    {
        if (Datatable::shouldHandle()) {
            $query = DB::table('bookings');
            $query = $query->select(
                'bookings.*'
            )
                ->join('viajero_datos', 'bookings.viajero_id', '=', 'viajero_datos.viajero_id')
                ->where('viajero_datos.escuela', '<>', '')->orWhere('viajero_datos.ingles', 1);

            /*
            $col = collect();

            $filtro = ConfigHelper::config('propietario');
            if( $filtro ) //&& !auth()->user()->isFullAdmin() )
            {
                $bookings = Booking::where('plataforma',$filtro)->get();
            }
            else
            {
                $bookings = Booking::all();
            }

            foreach( $bookings as $booking )
            {
                if($booking->viajero->datos->escuela || $booking->viajero->datos->ingles)
                {
                    $col->push($booking);
                    // $arr = [
                    //     'escuela'=> $booking->viajero->datos->escuela,
                    //     'academia'=> $booking->viajero->datos->ingles_academia,
                    // ];

                    // $col->push($arr);
                }
            }
            //dd($col);
            */

            return Datatable::query($query)
                ->addColumn('escuela', function ($column) {
                    $model = Booking::find($column->id);
                    return $model->viajero->datos->escuela;
                })
                ->addColumn('academia', function ($column) {
                    $model = Booking::find($column->id);
                    return $model->viajero->datos->ingles_academia;
                })
                ->addColumn('ciudad', function ($column) {
                    $model = Booking::find($column->id);
                    return $model->viajero->datos->ciudad;
                })
                ->addColumn('oficina', function ($column) {
                    $model = Booking::find($column->id);
                    return $model->oficina ? $model->oficina->name : "-";
                })
                ->addColumn('categoria', function ($column) {
                    $model = Booking::find($column->id);
                    return $model->curso->categoria_full_name;
                })
                ->addColumn('prescriptor', function ($column) {
                    $model = Booking::find($column->id);
                    return $model->prescriptor ? $model->prescriptor->name : "-";
                })
                ->searchColumns('escuela', 'academia')
                ->orderColumns('escuela', 'academia')
                ->setAliasMapping()
                //->setSearchStrip()->setOrderStrip()
                ->make();
        }

        return view('manage.informes.escuelas');
    }

    public function getAportes(Request $request)
    {
        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('any') ? true : false;

        if (Datatable::shouldHandle()) {
            $bookings = Booking::where('transporte_recogida', '<>', '');
            // dd($bookings->first());

            return Datatable::collection($bookings->get())
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('fechanac', function ($model) {
                    return $model->viajero->fechanac;
                })
                ->addColumn('localizador', function ($model) {
                    return $model->vuelo ? $model->vuelo->localizador : "-";
                })
                ->addColumn('agencia', function ($model) {
                    return $model->vuelo ? $model->vuelo->agencia->name : "-";
                })
                ->addColumn('origen', function ($model) {
                    return $model->transporte_recogida;
                })
                ->addColumn('comentarios', function ($model) {
                    return $model->transporte_requisitos;
                })
                ->addColumn('vuelo', function ($model) {
                    return $model->vuelo ? $model->vuelo->name : "-";
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('plataforma', function ($model) {
                    return ConfigHelper::plataforma($model->plataforma);
                })
                ->searchColumns('viajero')
                ->orderColumns('viajero')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.aportes', compact('valores', 'listado', 'anys'));
    }

    public function getVentasPrescriptor(Request $request)
    {
        $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        $categoriasp = [0 => 'Todas'] + CategoriaPrescriptor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();
            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();
            $plataformas = [0 => ConfigHelper::plataformaApp()];

            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
        }

        $valores['plataformas'] = intval($valores['plataformas']);

        $valores['oficinas'] = intval($request->input('oficinas', 0));
        $valores['categoriasp'] = intval($request->input('categoriasp', 0));
        $valores['categorias'] = $request->input('categorias', 0);
        $valores['subcategorias'] = $request->input('subcategorias', 0);

        $valores['desde'] = $request->input('desde', null);
        $valores['hasta'] = $request->input('hasta', null);

        $listado = $request->has('plataformas') ? true : false;

        //Para listado: +tabs
        $tabs = [];
        $results = 0;

        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->where('prescriptor_id', '>', 0);

            $results = $bookings->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            $bookings = $bookings->get();

            return Datatable::collection($bookings)
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('curso', function ($model) {
                    return $model->curso->name;
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('duracion', function ($model) {
                    return $model->weeks;
                })
                ->addColumn('duracion_name', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->duracion_name : "";
                })
                ->addColumn('fecha', function ($model) {
                    return $model->fecha_reserva ? $model->fecha_reserva->format('Y-m-d') : '-';
                })
                ->addColumn('fecha_salida', function ($model) {
                    return $model->course_start_date;
                })
                ->addColumn('categoria', function ($model) {
                    return $model->curso->categoria_full_name;
                })
                ->addColumn('prescriptor', function ($model) {
                    return $model->prescriptor ? $model->prescriptor->name : "-";
                })
                ->addColumn('categoriap', function ($model) {
                    if (!$model->prescriptor) {
                        return "-";
                    }
                    return $model->prescriptor->categoria ? $model->prescriptor->categoria->name : "-";
                })
                ->addColumn('oficina', function ($model) {
                    return $model->oficina ? $model->oficina->name : "-";
                })
                ->addColumn('fecha_p1', function ($model) {
                    return $model->fecha_pago1 ? $model->fecha_pago1->format('Y-m-d') : "-";
                })
                ->searchColumns('viajero', 'prescriptor', 'categoriap')
                ->orderColumns('viajero', 'prescriptor', 'categoriap')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        return view('manage.informes.ventas_prescriptor', compact('valores', 'tabs', 'results', 'listado', 'categoriasp', 'plataformas', 'oficinas', 'categorias', 'subcategorias'));
    }

    public function getVentasPrescriptores(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $categoriasp = [0 => 'Todas'] + CategoriaPrescriptor::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategoriasdet = [0 => 'Todas'] + SubcategoriaDetalle::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];
            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
            $categoriasp = [0 => 'Todas'] + CategoriaPrescriptor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategoriasdet = [0 => 'Todas'] + SubcategoriaDetalle::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $valores['plataformas'] = intval($valores['plataformas']);

        $valores['oficinas'] = $request->input('oficinas', 0);
        $valores['categoriasp'] = $request->input('categoriasp', 0);
        $valores['categorias'] = $request->input('categorias', 0);
        $valores['subcategorias'] = $request->input('subcategorias', 0);
        $valores['subcategoriasdet'] = $request->input('subcategoriasdet', 0);

        $fdesde = 'desde';
        $fhasta = 'hasta';
        $tipof = "1r Pago";

        if ($request->has('filtro2')) {
            $fdesde = 'desdes';
            $fhasta = 'hastas';
            $tipof = "Inicio Booking";
            $valores['filtro2'] = $tipof;
        } else {
            $valores['filtro1'] = $tipof;
        }

        $valores[$fdesde] = $request->input('desde', null);
        $valores[$fhasta] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));

        if (!$valores[$fdesde]) {
            $valores[$fdesde] = $request->input('desdes', null);
            $valores[$fhasta] = $request->input('hastas', null);
            if (!$valores[$fdesde]) {
                $valores[$fdesde] = "01/01/" . $valores['any'];
                $valores[$fhasta] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('plataformas') ? true : false;

        // dd($valores);

        //Para listado: +tabs
        $tabs = [];
        $results = 0;
        $totales = [];

        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->where('prescriptor_id', '>', 0); //->groupBy('prescriptor_id')->select('prescriptor_id', DB::raw('count(*) as total'),DB::raw('sum(semanas) as total_semanas'));

            // dd($bookings->count());

            $col = collect();

            $moneda = ConfigHelper::default_moneda();

            $totNum = 0;
            $totSemanas = 0;
            $totCursos = 0;
            $totTotal = 0;
            foreach ($bookings->get() as $booking) {
                $total = $booking->course_total_amount;
                $m_id = $booking->course_currency_id;
                if ($m_id != $moneda->id) {
                    $total = $total * $booking->getMonedaTasa($m_id);
                }

                if (!$col->contains('prescriptor_id', $booking->prescriptor_id)) {
                    $obj['prescriptor_id'] = $booking->prescriptor_id;
                    $obj['name'] = $booking->prescriptor->name;
                    $obj['num'] = 1;
                    $obj['total_sem'] = $booking->semanas;
                    $obj['total_curso'] = $total;
                    $obj['total'] = $booking->total;

                    $col->push($obj);
                } else {
                    $col = $col->map(function ($item, $key) use ($booking, $total) {
                        if ($item['prescriptor_id'] == $booking->prescriptor_id) {
                            $item['num'] += 1;
                            $item['total_sem'] += $booking->semanas;
                            $item['total_curso'] += $total;
                            $item['total'] += $booking->total;
                        }

                        return $item;
                    });
                }

                $totNum += 1;
                $totSemanas += $booking->semanas;
                $totCursos += $total;
                $totTotal += $booking->total;
            }

            $totales['total_num'] = $totNum;
            $totales['total_sem'] = $totSemanas;
            $totales['total_curso'] = $totCursos;
            $totales['total'] = $totTotal;

            $results = $bookings->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {

            return Datatable::collection($col)
                ->addColumn('prescriptor', function ($model) {
                    $p = Prescriptor::find($model['prescriptor_id']);
                    return "<a data-label='Ficha Prescriptor' href='" . route('manage.prescriptores.ficha', $p->id) . "'>" . $p->name . "</a>";
                })
                ->addColumn('num', function ($model) {
                    return $model['num'];
                })
                ->addColumn('total_sem', function ($model) {
                    return $model['total_sem'];
                })
                ->addColumn('total_curso', function ($model) {
                    return $model['total_curso'];
                    // return ConfigHelper::parseMoneda($model['total_curso']);
                })
                ->addColumn('total', function ($model) {
                    return $model['total'];
                    // return ConfigHelper::parseMoneda($model['total']);
                })
                ->addColumn('detalles', function ($model) {
                    $p = Prescriptor::find($model['prescriptor_id']);
                    return "<a target='_blank' data-label='Detalles' href='" . route('manage.prescriptores.ficha', $p->id) . "#ventas'>" . $p->name . "</a>";
                })
                ->searchColumns('prescriptor')
                ->orderColumns('prescriptor', 'num', 'total_sem', 'total_curso', 'total')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        return view('manage.informes.ventas_prescriptores', compact('valores', 'tabs', 'results', 'listado', 'categoriasp', 'plataformas', 'oficinas', 'categorias', 'subcategorias', 'totales', 'tipof'));
    }

    public function getVuelosParrilla(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        } else {
            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $aeropuertos = [0 => 'Todos'] + VueloEtapa::all()->sortBy('salida_aeropuerto')->pluck('salida_aeropuerto', 'salida_aeropuerto')->toArray();

        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['subcategorias'] = intval($request->input('subcategorias', 0));
        $valores['aeropuertos'] = $request->input('aeropuertos', 0);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));

        $listado = $request->has('any') ? true : false;
        $results = 0;

        if ($listado) {
            $cerradas = Cerrada::listadoFiltros($valores)->pluck('id')->toArray();

            $vuelos = Vuelo::where('any', $valores['any'])->pluck('id')->toArray();
            if ($valores['aeropuertos']) {
                $vuelos = VueloEtapa::whereIn('vuelo_id', $vuelos)->where('salida_aeropuerto', $valores['aeropuertos'])->pluck('vuelo_id')->toArray();
            }

            $convos = CerradaVuelo::whereIn('convocatory_id', $cerradas);
            $convos = $convos->whereIn('vuelo_id', $vuelos)->get();


            $results = $convos->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            return Datatable::collection($convos)
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria->name;
                })
                ->addColumn('vuelo', function ($model) {
                    return $model->vuelo->name;
                })
                ->addColumn('company', function ($model) {
                    return $model->vuelo->company_name;
                })
                ->addColumn('nvuelo', function ($model) {
                    $salida = $model->vuelo->salida;
                    if ($salida) {
                        return $salida->vuelo;
                    }

                    return "-";
                })
                ->addColumn('localizador', function ($model) {
                    return $model->vuelo->localizador;
                })
                ->addColumn('terminal', function ($model) {
                    return $model->vuelo->encuentro_terminal;
                })
                ->addColumn('fecha', function ($model) {
                    return $model->vuelo->encuentro_fecha;
                })
                ->addColumn('encuentro', function ($model) {
                    return $model->vuelo->encuentro_hora;
                })
                ->addColumn('salida', function ($model) {
                    $salida = $model->vuelo->salida;
                    if ($salida) {
                        return $salida->salida_hora;
                    }

                    return "-";
                })
                ->addColumn('monitor', function ($model) {
                    return $model->vuelo->plazas_monitor;
                })
                ->addColumn('plazas', function ($model) {
                    return $model->vuelo->plazas_reservas;
                })
                ->searchColumns('convocatoria', 'vuelo')
                ->orderColumns('convocatoria', 'vuelo', 'fecha', 'salida', 'monitor', 'plazas')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.vuelos_parrilla', compact('valores', 'results', 'listado', 'categorias', 'subcategorias', 'aeropuertos', 'anys'));
    }

    public function getVuelosAsignacion(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $valores['plataformas'] = intval($valores['plataformas']);

        $valores['oficinas'] = intval($request->input('oficinas', 0));
        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));

        $valores['tipoc'] = 5; //Todas las cerradas

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('plataformas') ? true : false;

        $results = 0;
        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);
            // $bookings = $bookings->where('vuelo_id','>',0);

            // dd($bookings->get());

            $results = $bookings->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            // $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->get();

            return Datatable::collection($bookings)
                ->addColumn('viajero', function ($model) {
                    return $model->viajero->full_name;
                })
                ->addColumn('curso', function ($model) {
                    return $model->curso ? $model->curso->name : "-";
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('vuelo', function ($model) {
                    if (!$model->vuelo) {
                        if ($model->transporte_otro) {
                            return "Otro";
                        }

                        return "No";
                    }

                    return $model->vuelo->name;
                })
                ->addColumn('aporte', function ($model) {
                    return $model->transporte_recogida ? $model->transporte_recogida : "-";
                })
                ->addColumn('vuelo_salida', function ($model) {
                    return $model->vuelo ? $model->vuelo->fecha_salida : "-";
                })
                ->addColumn('vuelo_llegada', function ($model) {
                    return $model->vuelo ? $model->vuelo->fecha_llegada : "-";
                })
                ->addColumn('aporte_extra', function ($model) {
                    return $model->transporte_recogida ? ConfigHelper::parseMoneda(175) : "-";
                })
                ->searchColumns('viajero', 'vuelo')
                ->orderColumns('viajero', 'vuelo', 'curso', 'convocatoria', 'aporte', 'vuelo_llegada', 'vuelo_salida')
                ->setAliasMapping()
                //->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.vuelos_asignacion', compact('valores', 'results', 'listado', 'categorias', 'plataformas', 'oficinas', 'proveedores', 'centros', 'cursos', 'anys'));
    }

    public function getVuelosPreSalida(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $valores['plataformas'] = intval($valores['plataformas']);

        $valores['oficinas'] = intval($request->input('oficinas', 0));
        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));

        $valores['tipoc'] = 5; //Todas las cerradas

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('plataformas') ? true : false;

        $results = 0;
        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->where('vuelo_id', '>', 0);

            // dd($bookings->get());

            $results = $bookings->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            // $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->get();

            return Datatable::collection($bookings)
                ->addColumn('nombre', function ($model) {
                    return $model->viajero->name;
                })
                ->addColumn('apellidos', function ($model) {
                    return $model->viajero->lastname . " " . $model->viajero->lastname2;
                })
                ->addColumn('fechanac', function ($model) {
                    return $model->viajero->fechanac;
                })
                ->addColumn('vuelo', function ($model) {
                    return $model->vuelo ? $model->vuelo->name : "-";
                })
                ->addColumn('localizador', function ($model) {
                    return $model->vuelo ? $model->vuelo->localizador : "-";
                })
                ->addColumn('vuelo_salida', function ($model) {
                    return $model->vuelo ? $model->vuelo->fecha_salida : "-";
                })
                ->addColumn('vuelo_llegada', function ($model) {
                    return $model->vuelo ? $model->vuelo->fecha_llegada : "-";
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('plataforma', function ($model) {
                    return ConfigHelper::plataforma($model->plataforma);
                })

                ->searchColumns('nombre', 'apellidos', 'vuelo')
                ->orderColumns('nombre', 'apellidos', 'vuelo', 'convocatoria', 'vuelo_llegada', 'vuelo_salida')
                ->setAliasMapping()
                //->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.vuelos_presalida', compact('valores', 'results', 'listado', 'categorias', 'plataformas', 'oficinas', 'proveedores', 'centros', 'cursos', 'anys'));
    }

    public function getVuelosPasajeros(Request $request)
    {
        $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $vuelos = [0 => 'Todos'] + Vuelo::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $agencias = [0 => 'Todas'] + Agencia::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['vuelos'] = intval($request->input('vuelos', 0));
        $valores['agencias'] = intval($request->input('agencias', 0));
        $valores['tipoc'] = 5; //Todas las cerradas

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('categorias') ? true : false;

        $results = 0;
        $tabs = [];
        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->where('vuelo_id', '>', 0);

            // dd($bookings->get());

            $results = $bookings->count();

            $bvuelos = clone $bookings;
            $bvuelos = $bvuelos->groupBy('vuelo_id')->get();
            foreach ($bvuelos as $b) {
                array_push($tabs, $b->vuelo_id);
            }
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            // $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->get();

            return Datatable::collection($bookings)
                ->addColumn('num', function ($model) {
                    return "n";
                })
                ->addColumn('viajero', function ($model) {
                    return $model->viajero->lastname . " " . $model->viajero->lastname2 . ", " . $model->viajero->name;
                })
                ->addColumn('fechanac', function ($model) {
                    return $model->viajero->fechanac;
                })
                ->addColumn('estado', function ($model) {
                    return $model->status->name;
                })
                ->addColumn('plataforma', function ($model) {
                    return $model->plataforma_name;
                })
                ->addColumn('requisitos', function ($model) {
                    return $model->transporte_requisitos;
                })
                ->addColumn('convocatoria', function ($model) {
                    if ($model->convocatoria) {
                        return $model->convocatoria->name;
                    }
                    return "-";
                })
                ->addColumn('convo_ini', function ($model) {
                    return $model->course_start_date;
                })
                ->addColumn('convo_fin', function ($model) {
                    return $model->course_end_date;
                })
                ->searchColumns('viajero', 'vuelo')
                ->orderColumns('num', 'viajero', 'convo_ini', 'convo_fin', 'requisitos', 'plataforma', 'estado')
                ->setAliasMapping()
                //->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.vuelos_pasajeros', compact('valores', 'results', 'tabs', 'listado', 'categorias', 'vuelos', 'agencias', 'anys'));
    }

    public function getVuelosApi(Request $request)
    {
        $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $agencias = [0 => 'Todas'] + Agencia::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['vuelos'] = intval($request->input('vuelos', 0));
        $valores['agencias'] = intval($request->input('agencias', 0));
        $valores['tipoc'] = 5; //Todas las cerradas

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['anyv'] = intval($request->input('anyv', Carbon::now()->year));
        $vuelos = [0 => 'Todos'] + Vuelo::where('any', $valores['anyv'])->orderBy('name')->pluck('name', 'id')->toArray();

        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('categorias') ? true : false;

        $tabs = [];
        $results = 0;
        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);
            if ($valores['vuelos'] == 0) {
                $vuelosid = Vuelo::where('any', $valores['anyv'])->pluck('id')->toArray();
                $bookings = $bookings->whereIn('vuelo_id', $vuelosid);
            }

            // dd($bookings->get());
            $results = $bookings->count();

            $bvuelos = clone $bookings;
            $bvuelos = $bvuelos->groupBy('vuelo_id')->get();
            foreach ($bvuelos as $b) {
                array_push($tabs, $b->vuelo_id);
            }
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            // $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->get();

            return Datatable::collection($bookings)
                ->addColumn('doc_tipo', function ($model) {
                    return "P";
                })
                ->addColumn('doc_pais', function ($model) {
                    return $model->viajero->datos->pasaporte_pais;
                })
                ->addColumn('doc_num', function ($model) {
                    return $model->viajero->datos->pasaporte;
                })
                ->addColumn('nacionalidad', function ($model) {
                    return $model->viajero->datos->nacionalidad;
                })
                ->addColumn('fechanac', function ($model) {
                    return $model->viajero->fechanac;
                })
                ->addColumn('genero', function ($model) {
                    return $model->viajero->sexo == 1 ? "H" : "F";
                })
                ->addColumn('doc_emision', function ($model) {
                    return $model->viajero->datos->pasaporte_emision;
                })
                ->addColumn('doc_caduca', function ($model) {
                    return $model->viajero->datos->pasaporte_caduca;
                })
                ->addColumn('apellidos', function ($model) {
                    return $model->viajero->lastname . " " . $model->viajero->lastname2;
                })
                ->addColumn('nombre', function ($model) {
                    return $model->viajero->name;
                })
                ->addColumn('plataforma', function ($model) {
                    return $model->plataforma_name;
                })
                ->searchColumns('apellidos', 'nombre', 'doc_num')
                ->orderColumns('apellidos', 'nombre', 'doc_num')
                ->setAliasMapping()
                //->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.vuelos_api', compact('valores', 'results', 'tabs', 'listado', 'categorias', 'vuelos', 'agencias', 'anys'));
    }


    public function getVuelosApi2(Request $request)
    {
        $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $agencias = [0 => 'Todas'] + Agencia::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['vuelos'] = intval($request->input('vuelos', 0));
        $valores['agencias'] = intval($request->input('agencias', 0));
        $valores['tipoc'] = 5; //Todas las cerradas

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['anyv'] = intval($request->input('anyv', Carbon::now()->year));
        $vuelos = [0 => 'Todos'] + Vuelo::where('any', $valores['anyv'])->orderBy('name')->pluck('name', 'id')->toArray();

        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('categorias') ? true : false;

        $tabs = [];
        $results = 0;
        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);
            if ($valores['vuelos'] == 0) {
                $vuelosid = Vuelo::where('any', $valores['anyv'])->pluck('id')->toArray();
                $bookings = $bookings->whereIn('vuelo_id', $vuelosid);
            }

            // dd($bookings->get());
            $results = $bookings->count();

            $bvuelos = clone $bookings;
            $bvuelos = $bvuelos->groupBy('vuelo_id')->get();
            foreach ($bvuelos as $b) {
                array_push($tabs, $b->vuelo_id);
            }
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            // $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->get();

            return Datatable::collection($bookings)
                ->addColumn('last_name', function ($model) {
                    return $model->viajero->lastname . " " . $model->viajero->lastname2;
                })
                ->addColumn('first_name', function ($model) {
                    return $model->viajero->name;
                })
                ->addColumn('title', function ($model) {
                    return $model->viajero->sexo == 1 ? "mr" : "ms";
                })
                ->addColumn('ptc', function ($model) {
                    return "";
                })
                ->addColumn('gender', function ($model) {
                    return $model->viajero->sexo == 1 ? "M" : "F";
                })
                ->addColumn('date_of_birth', function ($model) {
                    return Carbon::parse($model->viajero->fechanac)->format('dMy');
                })
                ->addColumn('passport_last_name', function ($model) {
                    return $model->viajero->lastname . " " . $model->viajero->lastname2;
                })
                ->addColumn('passport_first_name', function ($model) {
                    return $model->viajero->name;
                })
                ->addColumn('passport_number', function ($model) {
                    return $model->viajero->datos->pasaporte;
                })
                ->addColumn('passport_nationality', function ($model) {
                    $ret = $model->viajero->datos->pasaporte_pais;
                    if ($ret == "Española") {
                        $ret = "ESP";
                    }
                    return $ret;
                })
                ->addColumn('passport_issue_country', function ($model) {
                    $ret = $model->viajero->datos->nacionalidad;
                    if ($ret == "Española") {
                        $ret = "ESP";
                    }
                    return $ret;
                })
                ->addColumn('passport_expiry_date', function ($model) {
                    $fecha = $model->viajero->datos->pasaporte_caduca;
                    if ($fecha == "0000-00-00" || $fecha == "") {
                        return "";
                    }

                    return Carbon::parse($fecha)->format('dMy');
                })
                ->addColumn('visa_number', function ($model) {
                    return "";
                })
                ->addColumn('visa_type', function ($model) {
                    return "";
                })
                ->addColumn('visa_issue_date', function ($model) {
                    return "";
                })
                ->addColumn('place_of_birth', function ($model) {
                    return "";
                })
                ->addColumn('visa_place_of_issue', function ($model) {
                    return "";
                })
                ->addColumn('visa_country_of_application', function ($model) {
                    return "";
                })
                ->addColumn('address_type', function ($model) {
                    return "";
                })
                ->addColumn('address_country', function ($model) {
                    return "";
                })
                ->addColumn('address_details', function ($model) {
                    return "";
                })
                ->addColumn('address_city', function ($model) {
                    return "";
                })
                ->addColumn('address_state', function ($model) {
                    return "";
                })
                ->addColumn('address_zip_code', function ($model) {
                    return "";
                })
                ->addColumn('fqtv_carrier_1', function ($model) {
                    return "";
                })
                ->addColumn('fqtv_number_1', function ($model) {
                    return "";
                })
                ->addColumn('fqtv_carrier_2', function ($model) {
                    return "";
                })
                ->addColumn('fqtv_number_2', function ($model) {
                    return "";
                })
                ->addColumn('fqtv_carrier_3', function ($model) {
                    return "";
                })
                ->addColumn('fqtv_number_3', function ($model) {
                    return "";
                })
                ->searchColumns('last_name', 'first_name')
                ->orderColumns('last_name', 'first_name')
                ->setAliasMapping()
                //->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.vuelos_api2', compact('valores', 'results', 'tabs', 'listado', 'categorias', 'vuelos', 'agencias', 'anys'));
    }

    public function getDatosEstudiantes(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategoriasdet = [0 => 'Todas'] + SubcategoriaDetalle::all()->sortBy('name')->pluck('name', 'id')->toArray();
            
            $valores['plataformas'] = 0;
        } else {
            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategoriasdet = [0 => 'Todas'] + SubcategoriaDetalle::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();
        }

        $valores['plataformas'] = intval($request->input('plataformas', $valores['plataformas']));

        $valores['oficinas'] = intval($request->input('oficinas', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['subcategorias'] = intval($request->input('subcategorias', 0));
        $valores['subcategoriasdet'] = intval($request->input('subcategoriasdet', 0));
        $valores['paises'] = intval($request->input('paises', 0));

        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        $valores['desdes'] = $request->input('desde',null);
        $valores['hastas'] = $request->input('hasta',null);
        if(!$valores['desdes'])
        {
            $valores['desdes'] = "01/01/".$valores['any'];
            $valores['hastas'] = "31/12/".$valores['any'];
        }

        $listado = $request->has('plataformas') ? true : false;
        $results = 0;

        //dtt
        if ($listado)
        {
            // dd($valores);
            $bookings = Booking::listadoFiltros($valores);
            $results = $bookings->with('viajero')->count();

            if( Datatable::shouldHandle() )
            {
                return Datatable::collection($bookings->get())
                    ->addColumn('apellido1', function ($model) {
                        return $model->viajero->lastname;
                    })
                    ->addColumn('apellido2', function ($model) {
                        return $model->viajero->lastname2;
                    })
                    ->addColumn('name', function ($model) {
                        return $model->viajero->name;
                    })
                    ->addColumn('convocatoria', function ($model) {
                        return $model->convocatoria ? $model->convocatoria->name : "-";
                    })
                    ->addColumn('alojamiento', function ($model) {
                        $ret = $model->alojamiento ? $model->alojamiento->name : null;
    
                        if ($ret) {
                            $ret = $model->alojamiento->tipo ? $model->alojamiento->tipo->name : "";
                        }
    
                        return $ret;
                    })
                    ->addColumn('familia', function ($model) {
                        $familias = $model->familias;
    
                        if (!$familias) {
                            return "-";
                        }
    
                        // Familia (=nombre familia) / Dirección / Población / Tel1 / Tel2 / Tel3
    
                        $ret = "";
    
                        if ($familias->count() > 0)
                        {
                            $familia = $familias->sortByDesc('created_at')->first();
                            
                            $fam = $familia->familia;
                            $tlf = $fam->telefono;
    
                            if ($fam->adultos) {
                                if (isset($fam->adultos['movil'])) {
                                    if (isset($fam->adultos['movil'][0])) {
                                        $tlf .= $fam->adultos['movil'][0] ? " / " . $fam->adultos['movil'][0] : "";
                                    }

                                    if (isset($fam->adultos['movil'][1])) {
                                        $tlf .= $fam->adultos['movil'][1] ? " / " . $fam->adultos['movil'][1] : "";
                                    }
                                }
                            }
    
                            $ret .= $fam->name . ": (" . $fam->direccion . " - " . $fam->poblacion . ") [" . $fam->telefono . "]";
                        }
    
                        return $ret;
                    })
                    ->addColumn('school', function ($model) {
                        if($model->schools->first())
                        {
                            return strip_tags($model->schools_area->first()->school->name);
                        }
                    })
                    ->addColumn('school_address', function ($model) {
                        if($model->schools->first())
                        {
                            return strip_tags($model->schools_area->first()->school->direccion_completa ?? "");
                        }
                        else
                        {
                            return strip_tags($model->centro->address ?? "");
                        }
                    })
                    ->addColumn('grado', function ($model) {
                        return $model->grado_ext ? ConfigHelper::getGradoExt($model->grado_ext) : "";
                    })
                    ->searchColumns('apellido1', 'apellido2', 'name', 'contable')
                    ->orderColumns('apellido1', 'apellido2', 'name', 'contable')
                    ->setSearchStrip()->setOrderStrip()
                    ->setAliasMapping()
                    ->make();
            }
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        $paises = [0 => 'Todos'] + Pais::all()->sortBy('name')->pluck('name', 'id')->toArray();

        return view('manage.informes.estudiantes', compact('valores', 'results', 'listado', 'anys', 'paises', 'categorias', 'subcategorias', 'subcategoriasdet', 'cursos', 'oficinas', 'plataformas'));
    }

    public function getListadoAVI(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = 0;
        } else {
            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();
        }

        $valores['plataformas'] = intval($request->input('plataformas', $valores['plataformas']));

        $valores['oficinas'] = intval($request->input('oficinas', 0));
        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['subcategorias'] = intval($request->input('subcategorias', 0));
        $valores['tipoc'] = intval($request->input('tipoc', 0));
        // $valores['tipoc0'] = $valores['tipoc'];

        $valores['desde'] = $request->input('desde', null);
        $valores['hasta'] = $request->input('hasta', null);

        $valores['adultos'] = $request->has('adultos') ? true : false;

        $listado = $request->has('plataformas') ? true : false;
        $listadoExcel = $request->has('xls') ? true : false;

        //Para listado: +tabs
        $tabs = [];
        $results = 0;

        $adultos = $valores['adultos'];

        // dd($valores);

        if ($listado && !Datatable::shouldHandle()) {
            $f = $valores['tipoc'];
            $valores['convocatorias'] = intval($request->input('convocatorias', 0));

            // if($f==1 || $f==2)
            // {
            //     $valores['tipoc'] = 5; //Forzar cerrada
            // }

            $list_bookings = Booking::listadoFiltros($valores);

            if ($adultos) {
                $booking_medicos = clone $list_bookings;
                $booking_medicos = $booking_medicos->pluck('id')->toArray();

                $tipo = 2; //genérico
                $extras_medico = Extra::where('es_medico', true)->pluck('id')->toArray();
                $extras_curso = CursoExtraGenerico::whereIn('generics_id', $extras_medico)->pluck('id')->toArray();
                $extras = BookingExtra::whereIn('booking_id', $booking_medicos)->where('tipo', $tipo)->whereIn('extra_id', $extras_curso)->pluck('booking_id')->toArray();

                $bookings = $list_bookings->whereIn('id', $extras);
            } else {
                $bookings = $list_bookings;
            }

            // if(!Datatable::shouldHandle())
            {
                $f = $valores['tipoc'];
                if ($f) {
                    $ftipoc = ConfigHelper::getConvocatoriaIndex($f);

                    $bconvos = clone $bookings;
                    $bconvos = $bconvos->groupBy($ftipoc)->get();

                    $tabs[$f] = [];
                    foreach ($bconvos as $b) {
                        array_push($tabs[$f], $b->convocatoria->id);
                    }
                } else {
                    //todas las convos $arr = ['convocatory_close_id','convocatory_open_id','convocatory_multi_id'];
                    $bconvos = clone $bookings;
                    $bconvos = $bconvos->where('convocatory_close_id', '>', 0)->groupBy('convocatory_close_id')->get();

                    $tabs[1] = [];
                    foreach ($bconvos as $b) {
                        array_push($tabs[1], $b->convocatory_close_id);
                    }

                    $bconvos = clone $bookings;
                    $bconvos = $bconvos->where('convocatory_open_id', '>', 0)->groupBy('convocatory_open_id')->get();

                    $tabs[3] = [];
                    foreach ($bconvos as $b) {
                        array_push($tabs[3], $b->convocatory_open_id);
                    }

                    $bconvos = clone $bookings;
                    $bconvos = $bconvos->where('convocatory_multi_id', '>', 0)->groupBy('convocatory_multi_id')->get();

                    $tabs[4] = [];
                    foreach ($bconvos as $b) {
                        array_push($tabs[4], $b->convocatory_multi_id);
                    }
                }
            }

            //exportar a Excel
            if ($listadoExcel) {
                ini_set('memory_limit', '400M');
                set_time_limit(0);

                $bookingsExcel = clone $bookings;

                $fecha = Carbon::now()->format('d-m-Y_H-i');

                // https://vcn.test/manage/informes/avi?plataformas=0&oficinas=0&tipoc=0&categorias=6&subcategorias=0&proveedores=0&centros=252&cursos=0&desde=13%2F01%2F2020&hasta=31%2F12%2F2020&filtro1=Consultar
                return Excel::download( new InformeAviExport($bookingsExcel, $tabs), "informe_avi_$fecha.xlsx");
                
                /* Excel::create("informe_avi_$fecha", function ($excel) use ($bookingsExcel, $tabs)
                {

                    $fecha = Carbon::now()->format('d-m-Y');
                    $excel->setTitle("Informe AVI");
                    $excel->setCreator('VCN')->setCompany('VCN');
                    $excel->setDescription("Informe AVI $fecha");

                    foreach ($tabs as $k => $tab) {
                        foreach ($tabs[$k] as $kc => $convo_id)
                        {
                            $exportar = clone $bookingsExcel;
                            $ftipoc = ConfigHelper::getConvocatoriaIndex($k);
                            $exportar = $exportar->where($ftipoc, $convo_id)->get();

                            $bxls = [];
                            $i = 0;
                            foreach ($exportar as $model) {
                                $bxls[$i]['gender'] = $model->viajero->sexo == 1 ? 'M' : 'F';
                                $bxls[$i]['apellido1'] = $model->viajero->lastname;
                                $bxls[$i]['apellido2'] = $model->viajero->lastname2;
                                $bxls[$i]['name'] = $model->viajero->name;
                                $bxls[$i]['fechanac'] = Carbon::parse($model->viajero->fechanac)->format('d/m/Y');
                                $bxls[$i]['pais_home'] = $model->viajero->paisnac;
                                $bxls[$i]['pais_dest'] = $model->curso->centro->pais_name;
                                $bxls[$i]['program'] = $model->convocatoria ? $model->convocatoria->name : "-";

                                $dateic = Carbon::parse($model->course_start_date);
                                $dateia = Carbon::parse($model->accommodation_start_date);
                                $datei = $dateic;
                                if ($dateia->lt($dateic)) {
                                    $datei = $dateia;
                                }

                                $datefc = Carbon::parse($model->course_end_date);
                                $datefa = Carbon::parse($model->accommodation_end_date);
                                $datef = $datefc;
                                if ($datefa->gt($datefc)) {
                                    $datef = $datefa;
                                }

                                $bxls[$i]['date_start'] = $datei->format('d/m/Y');
                                $bxls[$i]['date_end'] = $datef->format('d/m/Y');

                                $bxls[$i]['contable'] = $model->contable_code;

                                $i++;
                            }

                            $convo = null;
                            switch ($k) {
                                case 1:
                                case 2:
                                case 5: {
                                        $convo = \VCN\Models\Convocatorias\Cerrada::find($convo_id);
                                    }
                                    break;

                                case 3: {
                                        $convo = \VCN\Models\Convocatorias\Abierta::find($convo_id);
                                    }
                                    break;

                                case 4: {
                                        $convo = \VCN\Models\Convocatorias\ConvocatoriaMulti::find($convo_id);
                                    }
                                    break;
                            }

                            $hoja = str_limit(($convo ? str_slug($convo->name) : "- Convocatoria NO EXISTE -"), 25);

                            $excel->sheet($hoja, function ($sheet) use ($bxls) {
                                $sheet->fromArray($bxls);
                            });
                        }
                    }
                })->download('xls'); //->store('xls'); */

                return redirect()->back();
            }

            // dd($bookings->get());
            $results = $bookings->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            $valores['adultos'] = $request->get('adultos');
            $adultos = (bool) $valores['adultos'];

            $f = $valores['tipoc'];
            $valores['convocatorias'] = intval($request->input('convocatorias', 0));

            // if($f==1 || $f==2)
            // {
            //     $valores['tipoc']=5; //Forzar cerrada
            // }

            $list_bookings = Booking::listadoFiltros($valores);

            if ($adultos) {
                $booking_medicos = clone $list_bookings;
                $booking_medicos = $booking_medicos->pluck('id')->toArray();

                $tipo = 2; //genérico
                $extras_medico = Extra::where('es_medico', true)->pluck('id')->toArray();
                $extras_curso = CursoExtraGenerico::whereIn('generics_id', $extras_medico)->pluck('id')->toArray();
                $extras = BookingExtra::whereIn('booking_id', $booking_medicos)->where('tipo', $tipo)->whereIn('extra_id', $extras_curso)->pluck('booking_id')->toArray();

                $bookings = $list_bookings->whereIn('id', $extras);
            } else {
                $bookings = $list_bookings;
            }

            return Datatable::collection($bookings->get())
                ->addColumn('gender', function ($model) {
                    return $model->viajero->sexo == 1 ? 'M' : 'F';
                })
                ->addColumn('apellido1', function ($model) {
                    return $model->viajero->lastname;
                })
                ->addColumn('apellido2', function ($model) {
                    return $model->viajero->lastname2;
                })
                ->addColumn('name', function ($model) {
                    return $model->viajero->name;
                })
                ->addColumn('fechanac', function ($model) {
                    return $model->viajero->fechanac;
                })
                ->addColumn('pais_home', function ($model) {
                    return $model->viajero->paisnac;
                })
                ->addColumn('pais_dest', function ($model) {
                    return $model->curso->centro->pais_name;
                })
                ->addColumn('program', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('fecha_ini', function ($model) {

                    $dateic = Carbon::parse($model->course_start_date);
                    $dateia = Carbon::parse($model->accommodation_start_date);
                    $datei = $model->course_start_date;
                    if ($dateia->lt($dateic)) {
                        $datei = $model->accommodation_start_date;
                    }

                    return $datei;
                })
                ->addColumn('fecha_fin', function ($model) {

                    $datefc = Carbon::parse($model->course_end_date);
                    $datefa = Carbon::parse($model->accommodation_end_date);
                    $datef = $model->course_end_date;
                    if ($datefa->gt($datefc)) {
                        $datef = $model->accommodation_end_date;
                    }

                    return $datef;
                })
                ->addColumn('contable', function ($model) {
                    return $model->contable_code;
                })
                ->searchColumns('apellido1', 'apellido2', 'name', 'contable')
                ->orderColumns('apellido1', 'apellido2', 'name', 'contable')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.informes.avi', compact('adultos', 'valores', 'results', 'tabs', 'listado', 'categorias', 'subcategorias', 'proveedores', 'centros', 'cursos', 'oficinas', 'plataformas'));
    }

    public function getVuelosProveedor(Request $request)
    {
        $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $convocatorias = [0 => 'Todas'] + Cerrada::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));

        $valores['tipoc'] = 5; //cerrada

        $valores['convocatorias'] = intval($request->input('convocatorias', 0));

        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        // $valores['desdes'] = $request->input('desde',null);
        // $valores['hastas'] = $request->input('hasta',null);
        // if(!$valores['desdes'])
        // {
        //     $valores['desdes'] = $request->input('desdes',null);
        //     $valores['hastas'] = $request->input('hastas',null);

        //     if(!$valores['desdes'])
        //     {
        //         $valores['desdes'] = "01/01/".$valores['any'];
        //         $valores['hastas'] = "31/12/".$valores['any'];
        //     }
        // }

        $listado = $request->has('proveedores') ? true : false;

        $results = 0;
        $tabs = [];
        if ($listado) {
            $vuelos = Vuelo::listadoFiltros($valores);

            $bprove = clone $vuelos;
            foreach ($bprove as $cv) {
                $p = $cv->convocatoria->curso->centro->proveedor->id;
                if (!in_array($p, $tabs)) {
                    $tabs[] = $p;
                }
            }

            $results = $vuelos->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            // $vuelos = $vuelos->get();
            return Datatable::collection($vuelos)
                ->addColumn('center', function ($model) {
                    return $model->convocatoria->curso->centro->name;
                })
                ->addColumn('program', function ($model) {
                    return $model->convocatoria->name;
                })
                ->addColumn('vuelo', function ($model) {
                    return $model->vuelo->name;
                })
                ->addColumn('ida_fecha', function ($model) {
                    $v = $model->vuelo ? $model->vuelo->arrival : null;

                    if ($v) {
                        return $v->llegada_fecha;
                    }

                    return "-";
                })
                ->addColumn('ida_hora', function ($model) {
                    $v = $model->vuelo ? $model->vuelo->arrival : null;

                    if ($v) {
                        return substr($v->llegada_hora, 0, 5);
                    }

                    return "-";
                })
                ->addColumn('ida_vuelo', function ($model) {
                    $v = $model->vuelo ? $model->vuelo->arrival : null;

                    if ($v) {
                        return $v->vuelo;
                    }

                    return "-";
                })
                ->addColumn('ida_airline', function ($model) {
                    $v = $model->vuelo ? $model->vuelo->arrival : null;

                    if ($v) {
                        return $v->company;
                    }

                    return "-";
                })
                ->addColumn('ida_aeropuerto', function ($model) {
                    $v = $model->vuelo ? $model->vuelo->arrival : null;

                    if ($v) {
                        return $v->llegada_aeropuerto;
                    }

                    return "-";
                })
                ->addColumn('vuelta_fecha', function ($model) {
                    $v = $model->vuelo ? $model->vuelo->return : null;

                    if ($v) {
                        return $v->salida_fecha;
                    }

                    return "-";
                })
                ->addColumn('vuelta_hora', function ($model) {
                    $v = $model->vuelo ? $model->vuelo->return : null;

                    if ($v) {
                        return substr($v->salida_hora, 0, 5);
                    }

                    return "-";
                })
                ->addColumn('vuelta_vuelo', function ($model) {
                    $v = $model->vuelo ? $model->vuelo->return : null;

                    if ($v) {
                        return $v->vuelo;
                    }

                    return "-";
                })
                ->addColumn('vuelta_airline', function ($model) {
                    $v = $model->vuelo ? $model->vuelo->return : null;

                    if ($v) {
                        return $v->company;
                    }

                    return "-";
                })
                ->addColumn('vuelta_aeropuerto', function ($model) {
                    $v = $model->vuelo ? $model->vuelo->return : null;

                    if ($v) {
                        return $v->salida_aeropuerto;
                    }

                    return "-";
                })
                ->searchColumns('center', 'program')
                ->orderColumns('center', 'program', 'ida_fecha', 'vuelta_fecha')
                ->setAliasMapping()
                //->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.vuelos_proveedor', compact('valores', 'results', 'tabs', 'listado', 'proveedores', 'cursos', 'convocatorias', 'anys'));
    }

    public function getBookingsIndividuales(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategoriasdet = [0 => 'Todas'] + SubcategoriaDetalle::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategoriasdet = [0 => 'Todas'] + SubcategoriaDetalle::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $asignados = [0 => 'Todos'] + User::asignados()->get()->pluck('full_name', 'id')->toArray();

        $valores['oficinas'] = intval($request->input('oficinas', 0));
        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['subcategorias'] = intval($request->input('subcategorias', 0));
        $valores['subcategoriasdet'] = intval($request->input('subcategoriasdet', 0));
        $valores['tipoc'] = intval($request->input('tipoc', 0));
        $valores['sos'] = intval($request->input('sos', 0));
        $valores['directos'] = intval($request->input('directos', 0));

        $valores['asignados'] = intval($request->input('asignados', 0));

        //filtro1+filtro2
        $fdesde = 'desde';
        $fhasta = 'hasta';
        $tipof = "1r Pago";
        if ($request->has('filtro2')) {
            $fdesde = 'desdes';
            $fhasta = 'hastas';
            $tipof = "Inicio Booking";
            $valores['filtro2'] = $tipof;
        } else {
            $valores['filtro1'] = $tipof;
        }
        $valores[$fdesde] = $request->input('desde', null);
        $valores[$fhasta] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores[$fdesde]) {
            $valores[$fdesde] = $request->input('desdes', null);
            $valores[$fhasta] = $request->input('hastas', null);
            if (!$valores[$fdesde]) {
                $valores[$fdesde] = "01/01/" . $valores['any'];
                $valores[$fhasta] = "31/12/" . $valores['any'];
            }
        }
        //filtro1+filtro2

        $listado = $request->has('asignados') ? true : false;

        // dd($valores);

        $results = 0;
        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);
            if ($valores['sos']) {
                /* $booking = $bookings->whereRaw(
                    'find_in_set(?, sos_proveedor)',
                    $valores['sos']
                ); */

                $bookings = $bookings->where('sos_plataforma', $valores['sos']);
            }

            $results = $bookings->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            // $bookings = Booking::bookingGrupos($valores);
            $bookings = $bookings->orderBy('course_start_date')->get();

            return Datatable::collection($bookings)
                ->addColumn('oficina', function ($model) {
                    return $model->oficina ? $model->oficina->name : "-";
                })
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Booking' target='_blank' href='" . route('manage.bookings.ficha', $model->id) . "'>" . $model->viajero->full_apellidos . "</a>";
                })
                ->addColumn('centro', function ($model) {
                    return $model->curso->centro->name;
                })
                ->addColumn('curso', function ($model) {
                    return $model->curso->name;
                })
                ->addColumn('fecha_creacion', function ($model) {
                    return $model->created_at->format('Y-m-d');
                    // return "<a data-label='Ficha Booking' target='_blank' href='". route('manage.bookings.ficha', $model->id) ."'>". $fecha ."</a>";
                })
                ->addColumn('fecha_ini', function ($model) {
                    return $model->course_start_date;
                })
                ->addColumn('fecha_fin', function ($model) {
                    return $model->course_end_date;
                })
                ->addColumn('movil', function ($model) {
                    return $model->viajero->movil;
                })
                ->addColumn('prescriptor', function ($model) {
                    return $model->prescriptor ? $model->prescriptor->name : "-";
                })
                ->addColumn('usuario', function ($model) {
                    return $model->asignado ? $model->asignado->full_name : "-";
                })
                ->addColumn('proveedor', function ($model) {
                    return $model->curso->centro->proveedor->name;
                })
                ->addColumn('tutor1_movil', function ($model) {
                    $t = $model->viajero->tutor1;

                    return $t ? $t->movil : "-";
                })
                ->addColumn('tutor2_movil', function ($model) {
                    $t = $model->viajero->tutor2;

                    return $t ? $t->movil : "-";
                })
                ->addColumn('pais', function ($model) {
                    return $model->curso->pais_name;
                })
                ->addColumn('sosp', function ($model) {
                    return $model->sos_proveedor_txt;
                })
                ->addColumn('sosp_notas', function ($model) {
                    return $model->sos_proveedor_notas;
                })
                ->addColumn('sos_plataforma', function ($model) {
                    return $model->sos_plataforma_txt;
                })
                ->searchColumns('viajero')
                ->orderColumns('fecha_creacion', 'fecha_ini', 'oficina', 'centro', 'curso', 'usuario', 'proveedor', 'prescriptor')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.bookings_individuales', compact('valores', 'results', 'listado', 'categorias', 'subcategorias', 'subcategoriasdet', 'plataformas', 'oficinas', 'proveedores', 'centros', 'cursos', 'asignados', 'anys', 'tipof'));
    }


    public function getListadoAeropuerto(Request $request)
    {
        $plataformas = ConfigHelper::plataformas();
        $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $convocatorias = [0 => 'Todas'] + Cerrada::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $valores['plataformas'] = intval($request->input('plataformas', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['convocatorias'] = $request->input('convocatorias', null);

        $valores['tipoc'] = 5; //cerrada

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('convocatorias') ? true : false;

        $results = 0;
        $tabs = [];
        if ($listado) {
            $bookings = Booking::listadoFiltros($valores, true);
            $bookings = $bookings->where('vuelo_id', '>', 0);

            // dd($bookings->get());
            $results = $bookings->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            $bookings = $bookings->get();

            return Datatable::collection($bookings)
                ->addColumn('apellido1', function ($model) {
                    return $model->viajero->lastname;
                })
                ->addColumn('apellido2', function ($model) {
                    return $model->viajero->lastname2;
                })
                ->addColumn('nombre', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->name . "</a>";
                })
                ->addColumn('poblacion', function ($model) {
                    return $model->viajero->datos->ciudad;
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria->name;
                })
                ->addColumn('vuelo', function ($model) {
                    return $model->vuelo ? $model->vuelo->name : "-";
                })
                ->addColumn('localizador', function ($model) {
                    return $model->vuelo ? $model->vuelo->localizador : "-";
                })
                ->addColumn('telefono', function ($model) {
                    return $model->viajero->telefono;
                })
                ->addColumn('movil', function ($model) {
                    return $model->viajero->movil;
                })
                ->addColumn('tutor1_movil', function ($model) {
                    $t = $model->viajero->tutor1;

                    return $t ? $t->movil : "-";
                })
                ->addColumn('tutor2_movil', function ($model) {
                    $t = $model->viajero->tutor2;

                    return $t ? $t->movil : "-";
                })
                ->addColumn('plataforma', function ($model) {
                    return $model->plataforma_name;
                })
                ->searchColumns('apellido1', 'apellido2', 'nombre', 'vuelo')
                ->orderColumns('apellido1', 'apellido2', 'nombre', 'convocatoria', 'vuelo')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.aeropuerto', compact('valores', 'results', 'listado', 'plataformas', 'categorias', 'centros', 'cursos', 'convocatorias', 'anys'));
    }

    public function getListadoAeropuertoPdf(Request $request)
    {
        $valores = $request->except('_token', 'filtro1', 'filtro2');

        $listado = $request->has('convocatorias') ? true : false;
        if (!$listado) {
            return redirect()->route('manage.informes.aeropuerto', $valores);
        }

        $tabs = [];
        unset($valores['plataformas']);
        $bookings = Booking::listadoFiltros($valores, true);

        $ftipoc = "convocatory_close_id";
        $bconvos = clone $bookings;
        $bconvos = $bconvos->groupBy($ftipoc)->get();

        // dd($bookings->orderBy('course_start_date')->get()->sortBy('viajero.lastname'));

        foreach ($bconvos as $b) {
            $convo_id = $b->convocatoria->id;
            // $tabs[] = $convo_id;

            $tabs[$convo_id] = [];
            foreach ($b->convocatoria->vuelos as $v) {
                $tabs[$convo_id][] = $v->vuelo->id;
            }
        }

        // dd($tabs);

        $valores_txt = [];

        // $valores_txt['plataformas'] = intval($valores['plataformas'])==0?'Todas':ConfigHelper::plataforma(intval($valores['plataformas']));
        $valores_txt['categorias'] = intval($request->input('categorias', 0)) == 0 ? 'Todas' : Categoria::find(intval($valores['categorias']))->name;
        $valores_txt['centros'] = intval($request->input('centros', 0)) == 0 ? 'Todos' : Centro::find(intval($valores['centros']))->name;
        $valores_txt['cursos'] = intval($request->input('cursos', 0)) == 0 ? 'Todos' : Curso::find(intval($valores['cursos']))->name;

        // return view('manage.informes.aeropuerto_pdf',compact('valores','valores_txt','tabs'));

        $fecha = Carbon::now()->format('d-m-Y_H-i');

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView('manage.informes.aeropuerto_pdf', compact('valores', 'valores_txt', 'tabs'));
        return $pdf->setPaper('a4')->setOrientation('landscape')->download("informe_aeropuerto_$fecha.pdf");
    }

    public function getOrla(Request $request, $pdf = false)
    {
        // $plataformas = ConfigHelper::plataformas();
        $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $convocatorias = [0 => 'Todas'];

        // $valores['plataformas'] = intval($request->input('plataformas',0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));

        $valores['tipoc'] = 0;
        $valores['convocatorias'] = intval($request->input('convocatorias', 0));
        if ($valores['convocatorias']) {
            $curso = Curso::find($valores['cursos']);
            $valores['tipoc'] = $curso->convocatoria_tipo_num;
        }

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        if ($valores['convocatorias']) {
            if ($valores['tipoc'] == 1) {
                $convocatorias += Cerrada::all()->sortBy('name')->pluck('name', 'id')->toArray();
            }

            if ($valores['tipoc'] == 4) {
                $convocatorias += ConvocatoriaMulti::all()->sortBy('name')->pluck('name', 'id')->toArray();
            }
        }

        $listado = $request->has('any') ? true : false;
        $results = 0;

        $bookings = Booking::listadoFiltros($valores, true);

        //xConvocatoria 1:cerrada:convocatory_close_id,4:multi:convocatory_multi_id
        $tabs = [];
        $filc = "";
        if ($listado) {
            $filc = "convocatory_close_id";
            if ($valores['tipoc'] == 4) {
                $filc = "convocatory_multi_id";
            }

            $btabs = clone $bookings;
            $btabs = $btabs->groupBy($filc)->get();
            foreach ($btabs as $b) {
                $tabs[$b->$filc] = $b->convocatoria->name;
            }
        }

        $results = $bookings->count();
        $valores['filtroc'] = $filc;

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        $template = 'manage.informes.orla';
        return view($template, compact('valores', 'results', 'listado', 'tabs', 'categorias', 'centros', 'cursos', 'convocatorias', 'anys', 'bookings'));
    }

    public function getOrlaPdf(Request $request)
    {
        $valores = $request->except('_token', 'filtro1', 'filtro2');

        $listado = $request->has('any') ? true : false;
        $results = 0;

        $bookings = Booking::listadoFiltros($valores, true);

        $tabs = [];
        $filc = "";
        if ($listado) {
            $filc = "convocatory_close_id";
            if ($valores['tipoc'] == 4) {
                $filc = "convocatory_multi_id";
            }

            $btabs = clone $bookings;
            $btabs = $btabs->groupBy($filc)->get();
            foreach ($btabs as $b) {
                $tabs[$b->$filc] = $b->convocatoria->name;
            }
        }

        $results = $bookings->count();
        $valores['filtroc'] = $filc;


        $fecha = Carbon::now()->format('d-m-Y_H-i');

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");

        $pdf = PDF::loadView('manage.informes.orla_pdf', compact('valores', 'tabs', 'bookings'));

        return $pdf->setPaper('a4')->setOrientation('landscape')->download("informe_orla_$fecha.pdf");
    }

    public function getListadoMonitores(Request $request)
    {
        $plataformas = ConfigHelper::plataformas();
        $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $convocatorias = [0 => 'Todas'];

        $valores['plataformas'] = intval($request->input('plataformas', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['tipoc'] = intval($request->input('tipoc', 0));

        $valores['convocatorias'] = $request->input('convocatorias', null);
        if ($valores['convocatorias'] || $valores['cursos']) {
            $curso = Curso::find($valores['cursos']);
            $valores['tipoc'] = $curso->convocatoria_tipo_num;
            $convocatorias = [0 => 'Todas'] + $curso->convocatorias_all->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('any') ? true : false;

        $results = 0;
        $tabs = [];
        if ($listado && !Datatable::shouldHandle()) {
            foreach (ConfigHelper::plataformas(false) as $p => $plataforma) {
                $valores['plataformas'] = $p;
                $tabs[$p] = [];
                $bookings = Booking::listadoFiltros($valores, true);

                $btabs = clone $bookings;
                $btabs = $btabs->groupBy('curso_id')->get();

                foreach ($btabs as $b) {
                    $tabs[$p][] = $b->curso_id;
                }

                // dd($bookings->get());
                $results = $bookings->count();
            }
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            $bookings = Booking::listadoFiltros($valores, true);
            $bookings = $bookings->get();

            return Datatable::collection($bookings)
                ->addColumn('apellido1', function ($model) {
                    return $model->viajero->lastname;
                })
                ->addColumn('apellido2', function ($model) {
                    return $model->viajero->lastname2;
                })
                ->addColumn('nombre', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->name . "</a>";
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria->name;
                })
                ->addColumn('fechanac', function ($model) {
                    return $model->viajero->fechanac;
                })
                ->addColumn('telefono', function ($model) {
                    return $model->viajero->telefono;
                })
                ->addColumn('movil', function ($model) {
                    return $model->viajero->movil;
                })
                ->addColumn('tutor1_movil', function ($model) {
                    $t = $model->viajero->tutor1;

                    return $t ? $t->movil : "-";
                })
                ->addColumn('tutor2_movil', function ($model) {
                    $t = $model->viajero->tutor2;

                    return $t ? $t->movil : "-";
                })
                ->addColumn('poblacion', function ($model) {
                    return $model->viajero->datos->ciudad;
                })
                ->addColumn('notas', function ($model) {
                    $ret = $model->notas_txt;
                    if ($model->datos) {
                        $ret .= $ret ? "<br>" : "";
                        $ret .= $model->datos->alergias_txt ? $model->datos->alergias_txt . "<br>" : "";
                        $ret .= $model->datos->enfermedad_txt ? $model->datos->enfermedad_txt . "<br>" : "";
                        $ret .= $model->datos->medicacion_txt ? $model->datos->medicacion_txt . "<br>" : "";
                        $ret .= $model->datos->tratamiento_txt ? $model->datos->tratamiento_txt . "<br>" : "";
                        $ret .= $model->datos->dieta_txt;
                    }

                    return $ret;
                })
                ->addColumn('plataforma', function ($model) {
                    return $model->plataforma_name;
                })
                ->addColumn('alojamiento', function ($model) {
                    $ret = $model->alojamiento ? $model->alojamiento->name : null;

                    if ($ret) {
                        $ret = $model->alojamiento->tipo ? $model->alojamiento->tipo->name : "";
                    }

                    return $ret;
                })
                ->addColumn('familia', function ($model) {
                    $familias = $model->familias;

                    if (!$familias) {
                        return "-";
                    }

                    // Familia (=nombre familia) / Dirección / Población / Tel1 / Tel2 / Tel3

                    $ret = "";

                    if ($familias->count() > 0) {
                        $ret = "<ul>";
                        foreach ($familias as $familia) {
                            $fam = $familia->familia;
                            $tlf = $fam->telefono;

                            if ($fam->adultos) {
                                if (isset($fam->adultos['movil'])) {
                                    if (isset($fam->adultos['movil'][0])) {
                                        $tlf .= $fam->adultos['movil'][0] ? " / " . $fam->adultos['movil'][0] : "";
                                    }

                                    if (isset($fam->adultos['movil'][1])) {
                                        $tlf .= $fam->adultos['movil'][1] ? " / " . $fam->adultos['movil'][1] : "";
                                    }
                                }
                            }

                            $ret .= "<li>" . $fam->name . ": (" . $fam->direccion . " - " . $fam->poblacion . ") [" . $fam->telefono . "] " . "</li>";
                        }
                        $ret .= "</ul>";
                    }

                    return $ret;
                })
                ->addColumn('extras', function ($model) {
                    return $model->extras_name;
                })
                ->searchColumns('apellido1', 'apellido2', 'nombre', 'vuelo')
                ->orderColumns('apellido1', 'apellido2', 'nombre', 'convocatoria', 'alojamiento')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.monitores', compact('valores', 'results', 'tabs', 'listado', 'plataformas', 'categorias', 'centros', 'cursos', 'convocatorias', 'anys'));
    }

    public function getListadoMonitoresPdf(Request $request)
    {
        $valores = $request->except('_token', 'filtro1', 'filtro2');

        // $listado = $request->has('convocatorias') ? true : false;
        // if (!$listado) {
        //     return redirect()->route('manage.informes.monitores', $valores);
        // }

        $tabs = [];
        unset($valores['plataformas']);
        $bookings = Booking::listadoFiltros($valores, true);

        $btabs = clone $bookings;
        $btabs = $btabs->groupBy('curso_id')->get();

        foreach ($btabs as $b) {
            $tabs[] = $b->curso_id;
        }

        // dd($tabs);

        $valores_txt = [];

        // $valores_txt['plataformas'] = intval($valores['plataformas'])==0?'Todas':ConfigHelper::plataforma(intval($valores['plataformas']));
        $valores_txt['categorias'] = intval($request->input('categorias', 0)) == 0 ? 'Todas' : Categoria::find(intval($valores['categorias']))->name;
        $valores_txt['centros'] = intval($request->input('centros', 0)) == 0 ? 'Todos' : Centro::find(intval($valores['centros']))->name;
        $valores_txt['cursos'] = intval($request->input('cursos', 0)) == 0 ? 'Todos' : Curso::find(intval($valores['cursos']))->name;

        // return view('manage.informes.monitores_pdf',compact('valores','valores_txt','tabs'));

        $fecha = Carbon::now()->format('d-m-Y_H-i');

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView('manage.informes.monitores_pdf', compact('valores', 'valores_txt', 'tabs'));
        return $pdf->setPaper('a4')->setOrientation('landscape')->download("informe_monitores_$fecha.pdf");
    }

    public function getViajerosDatos(Request $request)
    {
        $plataformas = ConfigHelper::plataformas();
        $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $convocatorias = [0 => 'Todas']; // + Cerrada::all()->sortBy('name')->pluck('name','id')->toArray();

        $valores['plataformas'] = intval($request->input('plataformas', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['tipoc'] = intval($request->input('tipoc', 0));

        $valores['convocatorias'] = intval($request->input('convocatorias', 0));

        $valores['convocatorias'] = intval($request->input('convocatorias', 0));
        if ($valores['convocatorias']) {
            $curso = Curso::find($valores['cursos']);
            $valores['tipoc'] = $curso->convocatoria_tipo_num;
        }

        // $valores['any'] = $request->input('any', Carbon::now()->format('Y'));
        // $valores['desde'] = "01/01/".$valores['any'];
        // $valores['hasta'] = "31/12/".$valores['any'];

        // $valores['desde'] = $request->input('desde',null);
        // $valores['hasta'] = $request->input('hasta',null);
        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('convocatorias') ? true : false;

        $results = 0;

        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);

            // dd($bookings->get());
            $results = $bookings->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            $bookings = $bookings->get();

            return Datatable::collection($bookings)
                ->addColumn('nombre', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->name . "</a>";
                })
                ->addColumn('apellido1', function ($model) {
                    return $model->viajero->lastname;
                })
                ->addColumn('apellido2', function ($model) {
                    return $model->viajero->lastname2;
                })
                ->addColumn('fechanac', function ($model) {
                    return $model->viajero->fechanac;
                })
                ->addColumn('centro', function ($model) {
                    return $model->curso->centro->name;
                })
                ->addColumn('curso', function ($model) {
                    return $model->curso->name;
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('oficina', function ($model) {
                    return $model->oficina ? $model->oficina->name : "-";
                })
                ->addColumn('prescriptor', function ($model) {
                    return $model->prescriptor ? $model->prescriptor->name : "-";
                })
                ->addColumn('email', function ($model) {
                    return $model->viajero->email;
                })
                ->addColumn('optin_v', function ($model) {
                    return $model->viajero->optout ? "OPTOUT" : "OPTIN";
                })
                ->addColumn('telefono', function ($model) {
                    return $model->viajero->telefono;
                })
                ->addColumn('movil', function ($model) {
                    return $model->viajero->movil;
                })
                ->addColumn('instagram', function ($model) {
                    return $model->viajero->datos->rs_instagram;
                })
                ->addColumn('tutor1_movil', function ($model) {
                    $t = $model->viajero->tutor1;

                    return $t ? $t->movil : "-";
                })
                ->addColumn('tutor2_movil', function ($model) {
                    $t = $model->viajero->tutor2;

                    return $t ? $t->movil : "-";
                })
                ->addColumn('tutor1_nombre', function ($model) {
                    $t = $model->viajero->tutor1;

                    return $t ? $t->full_name : "-";
                })
                ->addColumn('tutor1_email', function ($model) {
                    $t = $model->viajero->tutor1;

                    return $t ? $t->email : "-";
                })
                ->addColumn('optin_t1', function ($model) {
                    $t = $model->viajero->tutor1;
                    return $t ? ($t->optout ? "OPTOUT" : "OPTIN") : "-";
                })
                ->addColumn('tutor2_nombre', function ($model) {
                    $t = $model->viajero->tutor2;

                    return $t ? $t->full_name : "-";
                })
                ->addColumn('tutor2_email', function ($model) {
                    $t = $model->viajero->tutor2;

                    return $t ? $t->email : "-";
                })
                ->addColumn('optin_t2', function ($model) {
                    $t = $model->viajero->tutor2;
                    return $t ? ($t->optout ? "OPTOUT" : "OPTIN") : "-";
                })
                ->addColumn('direccion', function ($model) {
                    $datos = $model->viajero->datos;

                    return $datos->direccion;
                })
                ->addColumn('poblacion', function ($model) {
                    $datos = $model->viajero->datos;
                    return $datos->ciudad;
                })
                ->addColumn('cp', function ($model) {
                    $datos = $model->viajero->datos;
                    return $datos->cp;
                })
                ->addColumn('provincia', function ($model) {
                    $datos = $model->viajero->datos;
                    return $datos->provincia_name;
                })
                ->addColumn('dni', function ($model) {
                    return $model->viajero->datos->documento;
                })
                ->addColumn('pasaporte', function ($model) {
                    return $model->viajero->datos->pasaporte;
                })
                ->addColumn('escuela', function ($model) {
                    return $model->viajero->datos->escuela;
                })
                ->addColumn('academia', function ($model) {
                    return $model->viajero->datos->ingles_academia;
                })
                ->addColumn('fecha_ini', function ($model) {
                    return ($model->course_start_date >= $model->accommodation_start_date) ? $model->accommodation_start_date : $model->course_start_date;
                })
                ->addColumn('fecha_fin', function ($model) {
                    return ($model->course_end_date <= $model->accommodation_end_date) ? $model->accommodation_end_date : $model->course_end_date;
                })
                ->searchColumns('apellido1', 'apellido2', 'nombre')
                ->orderColumns('apellido1', 'apellido2', 'nombre', 'fecha_ini', 'fecha_fin', 'optin_v', 'optin_t1', 'optin_t2')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.viajeros_datos', compact('valores', 'results', 'listado', 'plataformas', 'categorias', 'centros', 'cursos', 'convocatorias', 'anys'));
    }


    public function getVuelosAgencia(Request $request)
    {
        $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $convocatorias = [0 => 'Todas'] + Cerrada::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));

        $valores['tipoc'] = 5; //cerrada

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $valores['convocatorias'] = intval($request->input('convocatorias', 0));

        $listado = $request->has('proveedores') ? true : false;

        $results = 0;
        $tabs = [];
        if ($listado) {
            $vuelos = Vuelo::listadoFiltros($valores);

            $pestanas = clone $vuelos;
            foreach ($pestanas as $v) {
                $p = $v->convocatoria->curso->id;
                if (!in_array($p, $tabs)) {
                    $tabs[] = $p;
                }
            }


            $results = $vuelos->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            // $vuelos = $vuelos->get();
            return Datatable::collection($vuelos)
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria->name;
                })
                ->addColumn('vuelo', function ($model) {
                    return $model->vuelo->name;
                })
                ->addColumn('localizador', function ($model) {
                    return $model->vuelo->localizador;
                })
                ->addColumn('encuentro_fecha', function ($model) {
                    return $model->vuelo->encuentro_fecha;
                })
                ->addColumn('encuentro_hora', function ($model) {
                    return substr($model->vuelo->encuentro_hora, 0, 5);
                })
                ->addColumn('encuentro_punto', function ($model) {
                    return $model->vuelo->encuentro_lugar . " - " . $model->vuelo->encuentro_terminal . " - " . $model->vuelo->encuentro_punto;
                })
                ->addColumn('infovuelo_ida', function ($model) {
                    return $model->vuelo ? $model->vuelo->infovuelo_ida : "-";
                })
                ->addColumn('infovuelo_vuelta', function ($model) {
                    return $model->vuelo ? $model->vuelo->infovuelo_vuelta : "-";
                })
                ->searchColumns('convocatoria', 'vuelo')
                ->orderColumns('encuentro_dia', 'convocatoria', 'vuelo')
                ->setAliasMapping()
                //->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.vuelos_agencia', compact('valores', 'results', 'tabs', 'listado', 'proveedores', 'centros', 'cursos', 'convocatorias', 'categorias', 'anys'));
    }

    public function getAreaNoconexion(Request $request)
    {
        // $plataformas = ConfigHelper::plataformas();

        $oficinas = Oficina::plataforma()->pluck('name', 'id')->toArray();
        if (auth()->user()->informe_oficinas) {
            $oficinas = [0 => 'Todas'] + $oficinas;
        }

        $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $valores['oficinas'] = intval($request->input('oficinas', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['subcategorias'] = intval($request->input('subcategorias', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('oficinas') ? true : false;

        $results = 0;

        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);

            // dd($bookings->pluck('viajero_id'));

            $bookings_nc = collect();

            $bLogado = false;
            foreach ($bookings->get() as $b) {
                $bLogado = false;

                foreach ($b->viajero->tutores as $t) {
                    if ($t->user) {
                        if ($t->user->login_attempts > 0) {
                            $bLogado = true;
                            break;
                        }
                    }
                }

                if (!$bLogado && $b->viajero->user) {
                    if ($b->viajero->user->login_attempts > 0) {
                        $bLogado = true;
                        continue;
                    }
                }

                if (!$bLogado) {
                    $bookings_nc->push($b);
                }
            }


            // dd($bookings->get());
            $results = $bookings_nc->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            return Datatable::collection($bookings_nc)
                ->addColumn('booking', function ($model) {
                    return "<a data-label='Booking' href='" . route('manage.bookings.ficha', $model->id) . "'>" . $model->id . "</a>";
                })
                ->addColumn('nombre', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->name . "</a>";
                })
                ->addColumn('apellidos', function ($model) {
                    return $model->viajero->lastname . " " . $model->viajero->lastname2;
                })
                ->addColumn('curso', function ($model) {
                    return $model->curso->name;
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('fecha_ini', function ($model) {
                    return $model->course_start_date;
                })
                ->addColumn('fecha_fin', function ($model) {
                    return $model->course_end_date;
                })
                ->addColumn('fecha_reserva', function ($model) {
                    return Carbon::parse($model->fecha_reserva)->format('Y-m-d');
                })
                ->addColumn('usuario', function ($model) {
                    return $model->asignado->full_name;
                })
                ->searchColumns('apellidos', 'nombre', 'booking')
                ->orderColumns('apellidos', 'nombre', 'fecha_ini', 'fecha_fin', 'fecha_reserva')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.area_noconexion', compact('valores', 'results', 'listado', 'categorias', 'subcategorias', 'oficinas', 'cursos', 'anys'));
    }

    public function getIncidencias(Request $request)
    {
        $any = intval($request->input('any', Carbon::now()->year));
        $fechai = "$any-01-01";
        $fechaf = "$any-12-31";

        if (auth()->user()->isFullAdmin()) {
            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $plataformas = ConfigHelper::plataformas();
            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $plataformas = [0 => ConfigHelper::plataformaApp()];
            $valores['plataformas'] = ConfigHelper::propietario();
        }

        if ($valores['plataformas']) {
            $users = [0 => 'Todos'] + User::crm()->where('plataforma', $valores['plataformas'])->get()->pluck('full_name', 'id')->toArray();
        } else {
            $users = [0 => 'Todos'] + User::crm()->get()->pluck('full_name', 'id')->toArray();
        }

        $user = $request->user();

        $valores['any'] = $any;
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['users'] = $request->input('users', $user->id);

        $filtro = $request->input('filtro', null);

        $results = 0;

        $p = (int) $valores['plataformas'];

        if (!$user->informe_oficinas) {
            $ofi = $user->oficina_id;
            $bookings = Booking::where('oficina_id', $ofi);

            if ($p) {
                $bookings = $bookings->where('plataforma', $p);
            }

            $bookings = $bookings->pluck('id')->toArray();
        } else {
            $valores['desdes'] = "01/01/$any";
            $valores['hastas'] = "31/12/$any";

            $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->pluck('id')->toArray();
        }

        $incidencias = BookingIncidencia::whereIn('booking_id', $bookings);
        if ($any) {
            $incidencias = $incidencias->whereBetween('fecha', [$fechai, $fechaf]);
        }

        if ($valores['users']) {
            // asign_to
            $incidencias = $incidencias->where('asign_to', $valores['users']);
        }

        $results = clone $incidencias;
        $results = $results->count();

        $results0 = clone $incidencias;
        $results0 = $results0->where('estado', 0)->count();

        $results1 = clone $incidencias;
        $results1 = $results1->where('estado', 1)->count();

        $tabs = [
            0   => "Abiertas [$results0]",
            1   => "Resueltas [$results1]",
            -1  => "Todas [$results]",
        ];

        //Dtt
        if (Datatable::shouldHandle()) {
            if ($filtro >= 0) {
                if (!$user->informe_oficinas) {
                    $incidencias = BookingIncidencia::whereIn('booking_id', $bookings)->where('estado', $filtro);
                } else {
                    $incidencias = BookingIncidencia::whereIn('booking_id', $bookings)->where('estado', $filtro);
                }
            } else {
                if (!$user->informe_oficinas) {
                    $incidencias = BookingIncidencia::whereIn('booking_id', $bookings);
                } else {
                    $incidencias = BookingIncidencia::whereIn('booking_id', $bookings);
                }
            }

            if ($any) {
                $incidencias = $incidencias->whereBetween('fecha', [$fechai, $fechaf]);
            }

            if ($valores['users']) {
                // asign_to
                $incidencias = $incidencias->where('asign_to', $valores['users']);
            }


            return Datatable::collection($incidencias->get())
                ->addColumn('fecha', function ($model) {
                    return Carbon::parse($model->fecha)->format('Y-m-d');
                })
                ->addColumn('hora', function ($model) {
                    return Carbon::parse($model->fecha)->format('H:i');
                })
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->booking->viajero->id) . "'>" . $model->booking->viajero->full_name . "</a>";
                })
                ->addColumn('booking', function ($model) {
                    return "<a data-label='Ficha Booking' href='" . route('manage.bookings.ficha', $model->booking->id) . "'>" . $model->booking->id . "</a>";
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->booking->convocatoria ? $model->booking->convocatoria->name : "-";
                })
                ->addColumn('notas', function ($model) {
                    return $model->notas;
                })
                ->addColumn('asignado', function ($model) {
                    return $model->asignado ? $model->asignado->full_name : "-";
                })
                ->addColumn('tipo', function ($model) {
                    return $model->tipo;
                })
                ->addColumn('estado_fecha', function ($model) {
                    return $model->estado ? Carbon::parse($model->estado_fecha)->format('Y-m-d') : "-";
                })
                ->addColumn('estado_hora', function ($model) {
                    return $model->estado ? Carbon::parse($model->estado_fecha)->format('H:i') : "-";
                })
                ->addColumn('status', function ($model) {
                    return $model->estado ? "Resuelta" : "Abierta";
                })
                ->addColumn('options', function ($model) {
                    return "<a data-label='Ficha Incidencia' href='" . route('manage.bookings.incidencias.ficha', $model->id) . "'><i class='fa fa-pencil'></i></a>";
                })
                ->searchColumns('tipo', 'viajero', 'convocatoria')
                ->orderColumns('fecha', 'tipo', 'fecha', 'viajero', 'convocatoria', 'tipo', 'status', 'asignado')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = \VCN\Models\Informes\Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        if (!in_array($any, $anys)) {
            $anys[$any] = $any;
        }

        return view('manage.informes.bookings_incidencias', compact('tabs', 'results', 'anys', 'valores', 'plataformas', 'categorias', 'users'));
    }


    public function getCicCamps(Request $request)
    {
        $cats = Categoria::where('es_info_campamento', 1)->pluck('id')->toArray();
        $cursos = [0 => 'Todos'] + Curso::whereIn('category_id', $cats)->get()->sortBy('name')->pluck('name', 'id')->toArray();
        $convocatorias = [0 => 'Todas'];

        // $valores['tipoc'] = 4;
        $valores['plataformas'] = 2; //Sólo CIC

        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['convocatorias'] = intval($request->input('convocatorias', 0));

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('convocatorias') ? true : false;
        $listado = $listado ?: $request->has('cursos');

        $results = 0;
        $booking = null;

        if ($listado) {
            $bookings = Booking::listadoFiltros($valores); //->whereNotNull('datos_campamento');

            $results = $bookings->count();
            $bookings1 = clone $bookings;
            $booking = $bookings1->whereNotNull('datos_campamento')->first();
        }

        $datos_campamento = [
            'campamento_nadar', 'campamento_bici', 'campamento_enuresis', 'campamento_comer',
            'campamento_mareos', 'campamento_insomnio', 'campamento_cansado',
            //'campamento_enfermedad','campamento_alergia','campamento_alergia_1','campamento_alergia_2','campamento_alergia_3','campamento_medicacion',
            'campamento_enfermizo', 'campamento_operaciones', 'campamento_discapacidad',
            'campamento_aprendizaje', 'campamento_familiar', 'campamento_miedo'
        ];

        //Dtt
        if (Datatable::shouldHandle()) {
            $dtt = Datatable::collection($bookings->get())
                ->addColumn('apellidos', function ($model) {
                    return $model->viajero->lastname . " " . $model->viajero->lastname2;
                })
                ->addColumn('nombre', function ($model) {
                    return "<a target='_blank' data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->name . "</a>";
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('sexo', function ($model) {
                    return $model->viajero->sexo == 2 ? "F" : "M";
                })
                ->addColumn('fechanac', function ($model) {
                    return $model->viajero->fechanac;
                })
                ->addColumn('alergias', function ($model) {
                    return $model->datos->alergias;
                })
                ->addColumn('medicacion', function ($model) {
                    return $model->datos->medicacion;
                })
                ->addColumn('tratamiento', function ($model) {
                    return $model->datos->tratamiento;
                })
                ->addColumn('dieta', function ($model) {
                    return $model->datos->dieta;
                })
                ->addColumn('dieta', function ($model) {
                    return $model->datos->dieta;
                })
                ->addColumn('log', function ($model) {
                    $log = $model->logs->where('tipo', "DatosCampamento")->sortByDesc('created_at')->first();
                    if ($log) {
                        return $log->notas;
                    }

                    return "";
                });

            foreach ($datos_campamento as $campo) {
                $dtt->addColumn($campo, function ($model) use ($campo) {

                    $ret = "NO";
                    if (isset($model->datos_campamento[$campo])) {
                        $ret = $model->datos_campamento[$campo];
                        if ($ret === 1) {
                            return "SI";
                        } elseif ($ret === 0) {
                            return "NO";
                        } elseif ($ret === "") {
                            return "NO";
                        }
                    }

                    return $ret;
                });
            }

            $dtt->searchColumns('apellidos', 'nombre', 'convocatoria')
                ->orderColumns('apellidos', 'nombre', 'fechanac')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip();
            return $dtt->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.cic_camps', compact('valores', 'results', 'listado', 'convocatorias', 'cursos', 'datos_campamento', 'anys'));
    }

    public function getCicNiveles(Request $request)
    {
        // $plataformas = ConfigHelper::plataformas();

        $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        // $subcategorias = [0=>'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name','id')->toArray();
        $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $convocatorias = [0 => 'Todas'] + Cerrada::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $valores['categorias'] = intval($request->input('categorias', 0));
        // $valores['subcategorias'] = intval($request->input('subcategorias',0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['convocatorias'] = intval($request->input('convocatorias', 0));
        $valores['tipoc'] = intval($request->input('tipoc', 0));

        $valores['plataformas'] = 2; //Sólo CIC

        $listado = $request->has('convocatorias') ? true : false;
        $listado = $listado ?: $request->has('cursos');
        $listado = $listado ?: $request->has('categorias');

        $results = 0;


        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);

            // $bookings_nc = collect();

            // $bLogado = false;
            // foreach($bookings->get() as $b)
            // {
            //     $bLogado = false;

            //     foreach($b->viajero->tutores as $t)
            //     {
            //         if($t->user)
            //         {
            //             if($t->user->login_attempts>0)
            //             {
            //                 $bLogado = true;
            //                 break;
            //             }
            //         }
            //     }

            //     if(!$bLogado && $b->viajero->user)
            //     {
            //         if($b->viajero->user->login_attempts>0)
            //         {
            //             $bLogado = true;
            //             continue;
            //         }
            //     }

            //     if(!$bLogado)
            //     {
            //         $bookings_nc->push($b);
            //     }
            // }


            // dd($bookings->get());

            $results = $bookings->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            return Datatable::collection($bookings->get())
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('apellidos', function ($model) {
                    return $model->viajero->lastname . " " . $model->viajero->lastname2;
                })
                ->addColumn('nombre', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->name . "</a>";
                })
                ->addColumn('sexo', function ($model) {
                    return $model->viajero->sexo == 2 ? "F" : "M";
                })
                ->addColumn('fechanac', function ($model) {
                    return $model->viajero->fechanac;
                })
                ->addColumn('nivel_trinity', function ($model) {
                    return $model->datos->trinity_nivel;
                })
                ->addColumn('trinity_any', function ($model) {
                    return $model->datos->trinity_any;
                })
                ->addColumn('nivel_trinity_camp', function ($model) {
                    return $model->datos->trinity_nivel;
                })
                ->addColumn('nivel_cic', function ($model) {
                    return $model->viajero->datos->cic_nivel;
                })
                ->addColumn('nivel_cic_camp', function ($model) {
                    // return $model->datos->cic_nivel;
                    return "?";
                })
                ->addColumn('trinity_exam', function ($model) {
                    return $model->datos->trinity_exam ? "SI" : "NO";
                })
                ->addColumn('thau', function ($model) {
                    return ConfigHelper::getCicThau($model->viajero->datos->cic_thau);
                })
                ->addColumn('escuela_curso', function ($model) {
                    return $model->datos->escuela_curso ? ConfigHelper::getEscuelaCurso($model->datos->escuela_curso) : "-";
                })
                ->searchColumns('apellidos', 'nombre', 'convocatoria')
                ->orderColumns('apellidos', 'nombre', 'convocatoria', 'nivel_trininty', 'nivel_trininty_camp', 'cic_nivel', 'cic_nivel_camp')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        return view('manage.informes.cic_niveles', compact('valores', 'results', 'listado', 'categorias', 'convocatorias', 'cursos'));
    }

    public function getVentasOrigen(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $origenes = [0 => 'Todos'] + Origen::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategoriasdet = [0 => 'Todas'] + SubcategoriaDetalle::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];
            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
            $origenes = [0 => 'Todos'] + Origen::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategoriasdet = [0 => 'Todas'] + SubcategoriaDetalle::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $valores['plataformas'] = intval($valores['plataformas']);

        $valores['oficinas'] = $request->input('oficinas', 0);
        $valores['origenes'] = $request->input('origenes', 0);

        $valores['categorias'] = $request->input('categorias', 0);
        $valores['subcategorias'] = $request->input('subcategorias', 0);
        $valores['subcategoriasdet'] = $request->input('subcategoriasdet', 0);

        $fdesde = 'desde';
        $fhasta = 'hasta';
        $tipof = "1r Pago";

        if ($request->has('filtro2')) {
            $fdesde = 'desdes';
            $fhasta = 'hastas';
            $tipof = "Inicio Booking";
            $valores['filtro2'] = $tipof;
        } else {
            $valores['filtro1'] = $tipof;
        }

        $valores[$fdesde] = $request->input('desde', null);
        $valores[$fhasta] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));

        if (!$valores[$fdesde]) {
            $valores[$fdesde] = $request->input('desdes', null);
            $valores[$fhasta] = $request->input('hastas', null);
            if (!$valores[$fdesde]) {
                $valores[$fdesde] = "01/01/" . $valores['any'];
                $valores[$fhasta] = "31/12/" . $valores['any'];
            }
        }

        $anys = [];
        $a1 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','ASC')->first()->course_start_date)->year;
        $a2 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','DESC')->first()->course_start_date)->year;

        for ($i = $a1; $i <= $a2; $i++) {
            $anys[$i] = $i;
        }

        $listado = $request->has('plataformas') ? true : false;

        //Para listado: +tabs
        $tabs = [];
        $results = 0;
        $totales = [];

        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);

            // dd($bookings->get());

            $col = collect();

            $moneda = ConfigHelper::default_moneda();

            $totNum = 0;
            $totSemanas = 0;
            $totCursos = 0;
            $totTotal = 0;
            foreach ($bookings->get() as $booking) {
                $total = $booking->course_total_amount;
                $m_id = $booking->course_currency_id;
                if ($m_id != $moneda->id) {
                    $total = $total * $booking->getMonedaTasa($m_id);
                }

                if (!$col->contains('full_name_origen_id', $booking->full_name_origen_id)) {
                    $obj['full_name_origen_id'] = $booking->full_name_origen_id;
                    $obj['name'] = $booking->full_name_origen;
                    $obj['detalles'] = $booking->full_name_origen_url;
                    $obj['num'] = 1;
                    $obj['total_sem'] = $booking->semanas;
                    $obj['total_curso'] = $total;
                    $obj['total'] = $booking->total;

                    $col->push($obj);
                } else {
                    $col = $col->map(function ($item, $key) use ($booking, $total) {
                        if ($item['full_name_origen_id'] == $booking->full_name_origen_id) {
                            $item['num'] += 1;
                            $item['total_sem'] += $booking->semanas;
                            $item['total_curso'] += $total;
                            $item['total'] += $booking->total;
                        }

                        return $item;
                    });
                }

                $totNum += 1;
                $totSemanas += $booking->semanas;
                $totCursos += $total;
                $totTotal += $booking->total;
            }

            $totales['total_num'] = $totNum;
            $totales['total_sem'] = $totSemanas;
            $totales['total_curso'] = $totCursos;
            $totales['total'] = $totTotal;

            // dd($col);

            $results = $bookings->count();
        }

        //Dtt
        if (Datatable::shouldHandle()) {

            return Datatable::collection($col)
                ->addColumn('origen', function ($model) {
                    return $model['name'];
                })
                ->addColumn('num', function ($model) {
                    return $model['num'];
                })
                ->addColumn('total_sem', function ($model) {
                    return $model['total_sem'];
                })
                ->addColumn('total_curso', function ($model) {
                    return $model['total_curso'];
                    // return ConfigHelper::parseMoneda($model['total_curso']);
                })
                ->addColumn('total', function ($model) {
                    return $model['total'];
                    // return ConfigHelper::parseMoneda($model['total']);
                })
                ->addColumn('detalles', function ($model) {
                    return $model['detalles'];
                })
                ->searchColumns('origen')
                ->orderColumns('origen', 'num', 'total_sem', 'total_curso', 'total')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        return view('manage.informes.ventas_origen', compact('valores', 'tabs', 'results', 'listado', 'origenes', 'plataformas', 'oficinas', 'categorias', 'subcategorias', 'anys', 'totales', 'tipof'));
    }

    public function getFacturadoCobrado(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $anys = [];
        $a1 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','ASC')->first()->course_start_date)->year;
        $a2 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','DESC')->first()->course_start_date)->year;

        for ($i = $a1; $i <= $a2; $i++) {
            $anys[$i] = $i;
        }

        $valores['categorias'] = $request->input('categorias', 0);
        $valores['subcategorias'] = $request->input('subcategorias', 0);
        $valores['oficinas'] = intval($request->input('oficinas', 0));

        //Tenemos 2 botones: filtro1 = pago1 y filtro2 = booking
        $fdesde = 'desde';
        $fhasta = 'hasta';
        $tipof = "1r Pago";

        if ($request->has('filtro2')) {
            $fdesde = 'desdes';
            $fhasta = 'hastas';
            $tipof = "Inicio Booking";
            $valores['filtro2'] = $tipof;
        } else {
            $valores['filtro1'] = $tipof;
        }

        $valores[$fdesde] = $request->input('desde', null);
        $valores[$fhasta] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));

        if (!$valores[$fdesde]) {
            $valores[$fdesde] = $request->input('desdes', null);
            $valores[$fhasta] = $request->input('hastas', null);
            if (!$valores[$fdesde]) {
                $valores[$fdesde] = "01/01/" . $valores['any'];
                $valores[$fhasta] = "31/12/" . $valores['any'];
            }
        }

        $valores['d'] = $valores[$fdesde];
        $valores['h'] = $valores[$fhasta];

        $listado = $request->has('plataformas') ? true : false;
        $results = 0;
        if ($listado) {
            $st = \VCN\Models\Bookings\Status::where('plazas', 1)->pluck('id')->toArray();
            $sto = [ConfigHelper::config('booking_status_overbooking')];
            $st = array_merge($st, $sto);
            $sto = [ConfigHelper::config('booking_status_refund')];
            $st = array_merge($st, $sto);
            $sto = [ConfigHelper::config('booking_status_cancelado')];
            $st = array_merge($st, $sto);

            $valores['status'] = $st;

            $bookingsAll = Booking::listadoFiltros($valores);
            $bookings = clone $bookingsAll;
            // $bookings = collect();
            // foreach($bookingsAll->get() as $b)
            // {
            //     if($b->tiene_facturas)
            //     {
            //         $bookings->push($b);
            //     }
            // }

            $results = $bookings->count();
        }

        $fdesde = $valores[$fdesde];
        $fhasta = $valores[$fhasta];

        //Dtt
        if (Datatable::shouldHandle()) {
            $col = $bookings->get();

            return Datatable::collection($col)
                ->addColumn('booking', function ($model) {
                    return "<a data-label='Booking' href='" . route('manage.bookings.ficha', $model->id) . "'>" . $model->id . "</a>";
                })
                ->addColumn('viajero', function ($model) {
                    return $model->viajero->full_name;
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria_link;
                })
                ->addColumn('facturacion', function ($model) {
                    return $model->fact_razonsocial ?: "";
                })
                ->addColumn('cobrado', function ($model) {
                    return $model->saldo;
                })
                ->addColumn('facturado', function ($model) {
                    $fact = 0;

                    foreach ($model->facturas as $f) {
                        $fact += $f->total;
                    }

                    return $fact;
                })
                ->addColumn('diferencia', function ($model) {
                    $fact = 0;

                    foreach ($model->facturas as $f) {
                        $fact += $f->total;
                    }

                    return round($fact - $model->saldo, 2);
                })
                ->addColumn('estado', function ($model) {
                    return $model->status->name;
                })
                ->addColumn('fecha_salida', function ($model) {
                    return $model->course_start_date;
                })

                ->searchColumns('viajero', 'booking', 'facturacion')
                ->orderColumns('booking', 'cobrado', 'facturado', 'diferencia')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }


        return view('manage.informes.facturado_cobrado', compact('valores', 'listado', 'results', 'plataformas', 'oficinas', 'categorias', 'subcategorias', 'anys', 'fdesde', 'fhasta', 'tipof'));
    }

    public function getChecklist(Request $request)
    {
        ini_set('memory_limit', '400M');

        $user = $request->user();

        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $u = \VCN\Models\User::crm();
            $usuarios = $u->get();
            $users = [0 => "Todos"] + $u->get()->pluck('full_name', 'id')->toArray();
            $valores['asignados'] = $request->input('asignados', []);

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if ($user->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }

            if ($user->es_aislado) {
                $usuarios = [];
                $users = [];
                $valores['asignados'] = $user->id;
            } else {
                $u = \VCN\Models\User::crm()->plataforma();
                $usuarios = $u->get();

                $users = [0 => "Todos"] + $u->get()->pluck('full_name', 'id')->toArray();
                $valores['asignados'] = $request->input('asignados', []);
            }
        }

        $paises = [0 => 'Todos'] + Pais::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $status = [0 => "Todos"] + BookingStatus::orderBy('orden')->pluck('name', 'id')->toArray();
        $anys = [];
        $a1 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','ASC')->first()->course_start_date)->year;
        $a2 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','DESC')->first()->course_start_date)->year;

        for ($i = $a1; $i <= $a2; $i++) {
            $anys[$i] = $i;
        }

        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['subcategorias'] = intval($request->input('subcategorias', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['status'] = $request->input('status', 0);
        $valores['oficinas'] = $request->input('oficinas', 0);
        $valores['paises'] = intval($request->input('paises', 0));

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        if ($valores['status'] && in_array(0, $valores['status'])) //Todos
        {
            $valores['status'] = null;
        }

        if ($valores['oficinas'] && in_array(0, $valores['oficinas'])) //Todos
        {
            $valores['oficinas'] = null;
        }

        $valores['desde_pago'] = $request->input('desde_pago', null);
        $valores['hasta_pago'] = $request->input('hasta_pago', null);

        //dd($valores);

        $listado = $request->has('plataformas') ? true : false;

        $checklist = null;
        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);
            $bookings = $bookings->where('es_directo', 0);

            if (isset($valores['desde_pago']) && isset($valores['hasta_pago'])) {
                $finiPago = Carbon::createFromFormat('d/m/Y', $valores['desde_pago'])->format('Y-m-d');
                $ffinPago = Carbon::createFromFormat('d/m/Y', $valores['hasta_pago'])->format('Y-m-d');

                $bookings = $bookings->where('fecha_pago1', '>=', $finiPago)->where('fecha_pago1', '<=', $ffinPago);
            }
            //dd($bookings->count());


            if ($valores['status']) {
                $checklist = BookingStatusChecklist::whereIn('status_id', $valores['status']);
            }

            if ($valores['categorias']) {
                if ($checklist) {
                    $checklist = $checklist->where('category_id', $valores['categorias']);
                } else {
                    $checklist = BookingStatusChecklist::where('category_id', $valores['categorias']);
                }
            }

            if ($checklist) {
                $checklist = $checklist->orderBy('status_id')->orderBy('orden')->get();
            } else {
                $checklist = []; //BookingStatusChecklist::orderBy('status_id')->orderBy('orden')->get();
            }
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            $col = $bookings->get();

            //$col = Booking::where('id', 9393)->get();

            $dtt = Datatable::collection($col)
                ->addColumn('fecha', function ($model) {
                    return $model->fecha_pago1 ? $model->fecha_pago1->format('Y-m-d') : "-";
                })
                ->addColumn('booking', function ($model) {
                    return "<a target='_blank' href='" . route('manage.bookings.ficha', $model->id) . "'>$model->id</a>";
                })
                ->addColumn('viajero', function ($model) {
                    $viajero = $model->viajero->full_name;
                    return "<a target='_blank' href='" . route('manage.viajeros.ficha', $model->viajero_id) . "'>$viajero</a>";
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->convocatoria ? $model->convocatoria->name : "-";
                })
                ->addColumn('pais', function ($model) {
                    return $model->curso->centro->pais_name;
                })
                ->addColumn('proveedor', function ($model) {
                    return $model->curso->centro->proveedor->name;
                })
                ->addColumn('duracion_name', function ($model) {
                    return $model->convocatoria->duracion_name;
                })
                ->addColumn('oficina', function ($model) {
                    $oficina = $model->viajero->oficina;
                    if (!$oficina) {
                        return "-";
                    }

                    return $oficina->name;
                })
                ->addColumn('usuario', function ($model) {
                    return $model->asignado ? $model->asignado->full_name : $model->user_id;
                });

            //dd($checklist);

            foreach ($checklist as $check) {
                $check_id = $check->id;
                $dtt->addColumn('chk_' . $check_id, function ($model) use ($check_id) {

                    $c = $model->checklists->where('checklist_id', $check_id)->first();

                    $class = $model->viajero->getStatusChecklistClass($check_id);
                    $check = "<i class='$class status-check' id='check-" . $model->id . "-$check_id' data-parent='$model->id' data-id='$check_id'></i>";

                    $ret = $check;
                    if ($c) {
                        $ret = $c->estado ? 'OK' : $check;
                        $ret .= " [" . Carbon::parse($c->fecha)->format('d/m/Y') . "] (" . ($c->user ? $c->user->full_name : ".") . ")";
                    }

                    return $ret;
                });
            }

            $dtt->searchColumns('viajero')
                ->orderColumns('fecha', 'viajero', 'pais', 'proveedor', 'usuario', 'oficina')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip();

            return $dtt->make();
        }

        return view('manage.informes.checklist', compact('valores', 'listado', 'categorias', 'subcategorias', 'cursos', 'plataformas', 'status', 'anys', 'checklist', 'oficinas', 'paises', 'users'));
    }

    public function getDescuentos(Request $request)
    {
        // ini_set('memory_limit', '400M');

        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];
        }

        $anys = [];
        if (Booking::first()) {
            $a1 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','ASC')->first()->course_start_date)->year;
            $a2 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','DESC')->first()->course_start_date)->year;

            for ($i = $a1; $i <= $a2; $i++) {
                $anys[$i] = $i;
            }
        }

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $totales = [];
        $total = [];
        $categorias = [];
        $cuenta = [];

        $listado = $request->has('plataformas') ? true : false;
        if ($listado) {
            $listado = null;

            $bookings = Booking::listadoFiltros($valores);

            $moneda = ConfigHelper::default_moneda();

            foreach ($bookings->get() as $booking) {
                foreach ($booking->descuentos as $dto) {
                    if (!isset($total[$booking->curso->category_id])) {
                        $total[$booking->curso->category_id] = 0;
                    }

                    if (!isset($categorias[$booking->curso->category_id])) {
                        if ($booking->curso->categoria) {
                            $categorias[$booking->curso->category_id] = $booking->curso->categoria->name;
                        } else {
                            $categorias[$booking->curso->category_id] = "B-" . $booking->id;
                        }
                    }

                    if (!isset($listado[$dto->notas])) {
                        $listado[$dto->notas] = 0;
                        $cuenta[$dto->notas] = 0;
                    }

                    if (!isset($totales[$dto->notas])) {
                        $totales[$dto->notas] = [];
                    }

                    if (!isset($totales[$dto->notas][$booking->curso->category_id])) {
                        $totales[$dto->notas][$booking->curso->category_id] = 0;
                    }

                    $m = $dto->moneda;
                    $importe = $dto->importe;
                    if ($m != $moneda) {
                        $importe = $importe * $booking->getMonedaTasa($m->id);
                    }

                    $listado[$dto->notas] += $importe;
                    $cuenta[$dto->notas]++;
                    $total[$booking->curso->category_id] += $importe;
                    $totales[$dto->notas][$booking->curso->category_id] += $importe;
                }
            }
        }

        return view('manage.informes.descuentos', compact('valores', 'plataformas', 'anys', 'listado', 'totales', 'total', 'cuenta', 'categorias'));
    }

    public function getDevoluciones(Request $request)
    {
        // ini_set('memory_limit', '400M');

        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];
        }

        $anys = [];
        if (Booking::first()) {
            $a1 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','ASC')->first()->course_start_date)->year;
            $a2 = Carbon::parse(Booking::whereNotNull('course_start_date')->where('course_start_date', '>', 0)->orderBy('course_start_date','DESC')->first()->course_start_date)->year;

            for ($i = $a1; $i <= $a2; $i++) {
                $anys[$i] = $i;
            }
        }

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        $listado = null;
        $totales = [];
        $total = [];
        $categorias = [];
        $cuenta = [];


        $listado = $request->has('plataformas') ? true : false;
        if ($listado) {
            
            $bookings = Booking::listadoFiltros($valores);

            $moneda = ConfigHelper::default_moneda();

            foreach ($bookings->get() as $booking) {
                foreach ($booking->devoluciones as $dto) {
                    if (!isset($total[$booking->curso->category_id])) {
                        $total[$booking->curso->category_id] = 0;
                    }

                    if (!isset($categorias[$booking->curso->category_id])) {
                        $categorias[$booking->curso->category_id] = $booking->curso->categoria->name;
                    }

                    if (!isset($listado[$dto->notas])) {
                        $listado[$dto->notas] = 0;
                        $cuenta[$dto->notas] = 0;
                    }

                    if (!isset($totales[$dto->notas])) {
                        $totales[$dto->notas] = [];
                    }

                    if (!isset($totales[$dto->notas][$booking->curso->category_id])) {
                        $totales[$dto->notas][$booking->curso->category_id] = 0;
                    }

                    $m = $dto->moneda;
                    $importe = $dto->importe;
                    if ($m != $moneda) {
                        $importe = $importe * $booking->getMonedaTasa($m->id);
                    }

                    $listado[$dto->notas] += $importe;
                    $cuenta[$dto->notas]++;
                    $total[$booking->curso->category_id] += $importe;
                    $totales[$dto->notas][$booking->curso->category_id] += $importe;
                }
            }
        }

        return view('manage.informes.devoluciones', compact('valores', 'plataformas', 'anys', 'listado', 'totales', 'total', 'cuenta', 'categorias'));
    }

    public function getTrafico(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = intval($request->input('plataformas', 0));
        } else {
            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();
        }

        $paises = [0 => 'Todos'] + Pais::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $anys = WebVisita::groupBy('any')->get()->pluck('any', 'any')->toArray();

        $valores['desde'] = $request->input('desde', null);
        $valores['hasta'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desde']) {
            $valores['desde'] = "01/01/" . $valores['any'];
            $valores['hasta'] = "31/12/" . $valores['any'];
        }

        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['paises'] = intval($request->input('paises', 0));

        $listado = null;
        $valores['listado'] = $request->input('listado', 0);

        $registros = null;
        $visitas = null;

        if ($request->has('any')) {
            $fini = Carbon::createFromFormat('d/m/Y', $valores['desde'])->format('Y-m-d');
            $ffin = Carbon::createFromFormat('d/m/Y', $valores['hasta'])->format('Y-m-d');

            //Registros:
            $col = WebRegistro::where('fecha', '>=', $fini)->where('fecha', '<=', $ffin);

            if ($valores['proveedores']) {
                $cursos = Proveedor::find($valores['proveedores'])->cursos->pluck('id')->toArray();
                $col = $col->whereIn('curso_id', $cursos);
            }

            if ($valores['paises']) {
                $centros = Centro::where('country_id', $valores['paises'])->pluck('id')->toArray();
                $cursos = Curso::whereIn('center_id', $centros)->pluck('id')->toArray();

                $col = $col->whereIn('curso_id', $cursos);
            }

            $registros = clone $col;

            //Visitas:
            $col = WebVisita::where('fecha', '>=', $fini)->where('fecha', '<=', $ffin);

            if ($valores['proveedores']) {
                $cursos = Proveedor::find($valores['proveedores'])->cursos->pluck('id')->toArray();
                $col = $col->whereIn('curso_id', $cursos);
            }

            if ($valores['paises']) {
                $centros = Centro::where('country_id', $valores['paises'])->pluck('id')->toArray();
                $cursos = Curso::whereIn('center_id', $centros)->pluck('id')->toArray();

                $col = $col->whereIn('curso_id', $cursos);
            }

            $visitas = clone $col;
        }


        //dtt
        if (Datatable::shouldHandle()) {
            if ($valores['listado'] == 'registros') {
                return Datatable::collection($registros->get())
                    ->addColumn('fecha', function ($model) {
                        return $model->fecha->format('Y-m-d');
                    })
                    ->addColumn('curso', function ($model) {
                        return $model->curso ? $model->curso->name : "Web Home";
                    })
                    ->addColumn('usuarios', function ($model) {

                        $ret = "<ul>";

                        if (!$model->curso) {
                            $arrv = $model->registros_home_viajeros;
                            $arrt = $model->registros_home_tutores;
                        } else {
                            $arrv = $model->registros_info_viajeros;
                            $arrt = $model->registros_info_tutores;
                        }

                        if ($arrv) {
                            foreach ($arrv as $v) {
                                $u = User::find($v);
                                $ret .= "<li>";
                                $ret .= "Viajero: <a href='" . route('manage.viajeros.ficha', [$u->ficha->id]) . "'>" . $u->ficha->full_name . "</a>";
                                $ret .= "</li>";
                            }
                        }

                        if ($arrt) {
                            foreach ($arrt as $t) {
                                $u = User::find($t);
                                $ret .= "<li>";
                                $ret .= "Tutor: <a href='" . route('manage.tutores.ficha', [$u->ficha->id]) . "'>" . $u->ficha->full_name . "</a>";
                                $ret .= "</li>";
                            }
                        }

                        $ret .= "</ul>";

                        return $ret;
                    })
                    ->addColumn('total', function ($model) {
                        return $model->curso ? $model->registros_info : $model->registros_home;
                    })
                    ->searchColumns('curso')
                    ->orderColumns('fecha', 'curso', 'total')
                    ->setAliasMapping()
                    ->setSearchStrip()->setOrderStrip()
                    ->make();
            } elseif ($valores['listado'] == 'visitas') {
                return Datatable::collection($visitas->get())
                    ->addColumn('fecha', function ($model) {
                        return $model->fecha->format('Y-m-d');
                    })
                    ->addColumn('curso', function ($model) {
                        return $model->curso ? $model->curso->name : "Web Home";
                    })
                    ->addColumn('tipo', function ($model) {
                        return $model->user_id ? "Registrado" : "Visitante";
                    })
                    ->addColumn('visitas', function ($model) {
                        return $model->visitas;
                    })
                    ->addColumn('proveedor', function ($model) {
                        return $model->visitas_proveedor;
                    })
                    ->searchColumns('curso')
                    ->orderColumns('fecha', 'curso', 'visitas', 'proveedor')
                    ->setAliasMapping()
                    ->setSearchStrip()->setOrderStrip()
                    ->make();
            } elseif ($valores['listado'] == 'visitas-user') {
                return Datatable::collection($visitas->get())
                    ->addColumn('fecha', function ($model) {
                        return $model->fecha->format('Y-m-d');
                    })
                    ->addColumn('curso', function ($model) {
                        return $model->curso ? $model->curso->name : "Web Home";
                    })
                    ->addColumn('user', function ($model) {
                        if (!$model->user) {
                            return "Visitante";
                        }

                        $user = $model->user;
                        if ($user->es_viajero) {
                            return "Viajero: <a href='" . route('manage.viajeros.ficha', [$model->user->ficha->id]) . "'>" . $model->user->ficha->full_name . "</a>";
                        } elseif ($user->es_tutor) {
                            return "Tutor: <a href='" . route('manage.tutores.ficha', [$model->user->ficha->id]) . "'>" . $model->user->ficha->full_name . "</a>";
                        }

                        return "-";
                    })
                    ->addColumn('visitas', function ($model) {
                        return $model->visitas;
                    })
                    ->addColumn('proveedor', function ($model) {
                        return $model->visitas_proveedor;
                    })
                    ->searchColumns('curso')
                    ->orderColumns('fecha', 'curso', 'visitas', 'proveedor')
                    ->setAliasMapping()
                    ->setSearchStrip()->setOrderStrip()
                    ->make();
            }
        }

        return view('manage.informes.trafico', compact('valores', 'plataformas', 'anys', 'proveedores', 'paises', 'registros', 'visitas'));
    }

    public function getActividadUsers(Request $request)
    {
        $valores = [];

        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();
            $valores['plataformas'] = intval($request->input('plataformas', 0));
            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
        } else {
            $plataformas = [0 => ConfigHelper::plataformaApp()];
            $valores['plataformas'] = ConfigHelper::propietario();
            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }
        }

        if ($valores['plataformas']) {
            $users = [0 => 'Todos'] + User::crm()->where('plataforma', $valores['plataformas'])->pluck('username', 'id')->toArray();
        } else {
            $users = [0 => 'Todos'] + User::crm()->pluck('username', 'id')->toArray();
        }

        $valores['desdec'] = $request->input('desde', null);
        $valores['hastac'] = $request->input('hasta', null);
        // $valores['oficinas'] = $request->input('oficinas',0);

        $valores['users'] = $request->input('users', [0]);
        if ($valores['users'] && in_array(0, $valores['users'])) //Todos
        {
            $valores['users'] = $users;
        } else {
            $valores['users'] = User::whereIn('id', $valores['users'])->pluck('username', 'id')->toArray();
        }

        $listado = count($valores['users']) ? true : false;
        if ($listado) {
            $listado = $request->input('desde', false);
            $listado = $request->input('hasta', false);
        }

        $status_solicitud = [];
        $resultados = [];
        $seguimientos_enviados = [];
        $seguimientos_recibidos = [];
        $usuarios = [];
        $fechas = [];
        $fechas_txt = [];
        $modo = "";
        $desde = null;
        $hasta = null;

        $categorias_solicitud = [];
        $categorias_booking = [];
        $categorias_presupuesto = [];
        $seguimientos = [];

        if ($listado)
        {
            $modo = $request->has('filtro_semanas') ? 'semanas' : ($request->has('filtro_meses') ? 'meses' : 'dias');

            //Filtro x Semanas
            // if($request->has('filtro_semanas'))
            // {
            //     $any = (int)$request->get('any');
            //     $semana = (int)$request->get('semana');

            //     $desde = Carbon::create($any, 1, 1);
            //     $m = (int)$desde->format("W");
            //     while($m>1)
            //     {
            //         $desde->addDay();
            //         $m = (int)$desde->format("W");
            //     }

            //     $desde->addWeeks($semana-1);
            //     $hasta = clone $desde;
            //     $hasta = $hasta->endOfWeek();

            //     $valores['desdec'] = $desde->format('d/m/Y');
            //     $valores['hastac'] = $hasta->format('d/m/Y');
            // }
            // else
            {
                $desde = Carbon::createFromFormat('!d/m/Y', $valores['desdec']);
                $hasta = Carbon::createFromFormat('!d/m/Y', $valores['hastac']);
            }

            switch ($modo) {
                case 'dias': {
                        $d = clone $desde;
                        $cols = $hasta->diffInDays($desde);
                        $fechas_txt[0] = $d->format('d/m');
                        $fechas[0] = $d->format('Y-m-d');
                        for ($i = 1; $i <= $cols; $i++) {
                            $d->addDay();
                            $fechas_txt[$i] = $d->format('d/m');
                            $fechas[$i] = $d->format('Y-m-d');
                        }
                    }
                    break;

                case 'semanas': {
                        $d = clone $desde;
                        $cols = $hasta->diffInWeeks($desde);
                        $fechas_txt[0] = $d->format('W');
                        $fechas[0] = (int) $d->format('W');
                        for ($i = 1; $i <= $cols; $i++) {
                            $d->addWeek();
                            $fechas_txt[$i] = $d->format('W');
                            $fechas[$i] = (int) $d->format('W');
                        }
                    }
                    break;

                case 'meses': {
                        $d = clone $desde;
                        $cols = $hasta->diffInMonths($desde);
                        $fechas_txt[0] = $d->month; //->format('M');
                        $fechas[0] =  (int) $d->month;
                        for ($i = 1; $i <= $cols; $i++) {
                            $d->addMonth();
                            $fechas_txt[$i] = $d->month; //->format('M');
                            $fechas[$i] = (int) $d->month;
                        }
                    }
                    break;
            }

            $usuarios = $valores['users'];
            unset($usuarios[0]);

            $users_id = [];
            foreach ($valores['users'] as $ku => $u) {
                $users_id[] = $ku;
            }
            // $valores['users'] = $users_id;

            // Bookings
            $ffecha = "date(created_at) as fecha_grup";
            if ($modo == "semanas") {
                $ffecha = "week(created_at) as fecha_grup";
            } elseif ($modo == "meses") {
                $ffecha = "month(created_at) as fecha_grup";
            }

            $bookings = Booking::listadoFiltros($valores)->whereIn('user_id', $users_id)->selectRaw("user_id, category_id, subcategory_id, $ffecha, count(*) as cuenta")->groupBy('fecha_grup', 'user_id', 'category_id', 'subcategory_id')->get();
            $resultados['bookings'] = $bookings;

            // dd($bookings);

            $cats = clone $bookings;
            $cats = $cats->groupBy('category_id');
            $categorias_booking = [];
            foreach ($cats as $cat) {
                $c = $cat->first()->category_id;

                $categoria = Categoria::find($c);
                $categorias_booking[$c] = $categoria ? $categoria->name : "-";
            }

            //Seguimientos
            foreach ($usuarios as $iu => $u) {
                $user = User::find($iu);
                $seguimientos[$iu] = $user->seguimientos_betweenFechas($desde, $hasta)->selectRaw("user_id, tipo, $ffecha, count(*) as cuenta")->groupBy('fecha_grup', 'tipo')->get();
            }

            // Solicitudes
            $st = [];
            $st[] = ConfigHelper::config('solicitud_status_futuro');
            $st[] = ConfigHelper::config('solicitud_status_archivado');
            $status_solicitud = SolicitudStatus::whereNotIn('id', $st)->pluck('name', 'id')->toArray();

            $ffecha = "date(fecha) as fecha_grup";
            if ($modo == "semanas") {
                $ffecha = "week(fecha) as fecha_grup";
            } elseif ($modo == "meses") {
                $ffecha = "month(fecha) as fecha_grup";
            }

            $solicitudes = Solicitud::listadoFiltros($valores)->whereIn('user_id', $users_id)->selectRaw("user_id, category_id, subcategory_id, $ffecha, count(*) as cuenta")->groupBy('fecha_grup', 'user_id', 'category_id', 'subcategory_id')->get();
            $resultados['solicitudes'] = $solicitudes;

            $cats = clone $solicitudes;
            $cats = $cats->groupBy('category_id');
            $categorias_solicitud = [];
            foreach ($cats as $cat) {
                $c = $cat->first()->category_id;

                $categoria = Categoria::find($c);
                $categorias_solicitud[$c] = $categoria ? $categoria->name : "-";
            }

            //Presupuestos
            $presupuestos = Presupuesto::whereIn('user_id', $users_id)->where('fecha', '>=', $desde->format('Y-m-d'))->where('fecha', '<=', $hasta->format('Y-m-d'))->selectRaw("user_id, category_id, subcategory_id, $ffecha, count(*) as cuenta")->groupBy('fecha_grup', 'user_id', 'category_id', 'subcategory_id')->get();
            $resultados['presupuestos'] = $presupuestos;

            // dd($presupuestos);

            $cats = clone $presupuestos;
            $cats = $cats->groupBy('category_id');
            $categorias_presupuesto = [];
            foreach ($cats as $cat) {
                $c = $cat->first()->category_id;

                $categoria = Categoria::find($c);
                $categorias_presupuesto[$c] = $categoria ? $categoria->name : "-";
            }
        }

        return view(
            'manage.informes.actividad',
            compact(
                'valores',
                'listado',
                'resultados',
                'users',
                'plataformas',
                'oficinas',
                // 'anys',
                // 'semanas',
                'usuarios',
                'fechas',
                'fechas_txt',
                'modo',
                'desde',
                'hasta',
                'status_solicitud',
                'categorias_solicitud',
                'categorias_booking',
                'categorias_presupuesto',
                'seguimientos'
            )
        );
    }


    public function getEdad(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = intval($request->input('plataformas', 0));
        } else {
            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        $valores['any'] = intval($request->input('any', Carbon::now()->format('Y')));

        $valores['oficinas'] = intval($request->input('oficinas', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['subcategorias'] = intval($request->input('subcategorias', 0));

        $listado = $request->has('any');
        $total = 0;

        $edades = [];
        $edades_pais = [];
        $edades_alojamiento = [];
        $edades_pya = [];
        $paises = [];
        $alojamientos = [];

        if ($listado) {
            $any = $valores['any'];
            $valores['desdes'] = "01/01/$any";
            $valores['hastas'] = "31/12/$any";

            $bookings = Booking::listadoFiltros($valores);

            $total = $bookings->count();

            foreach ($bookings->get() as $b) {
                $e = $b->edad;

                if (!isset($edades[$e])) {
                    $edades[$e] = 0;
                }
                $edades[$e]++;

                //País
                $p = $b->centro->pais_name;
                if (!isset($edades_pais[$e])) {
                    $edades_pais[$e] = [];
                }
                if (!isset($edades_pais[$e][$p])) {
                    $edades_pais[$e][$p] = 0;
                }
                $edades_pais[$e][$p]++;

                //Alojamiento
                $a = $b->alojamiento ? $b->alojamiento->tipo_name : "-";
                if (!isset($edades_alojamiento[$e])) {
                    $edades_alojamiento[$e] = [];
                }
                if (!isset($edades_alojamiento[$e][$a])) {
                    $edades_alojamiento[$e][$a] = 0;
                }
                $edades_alojamiento[$e][$a]++;

                //País + Alojamiento
                if (!isset($edades_pya[$e])) {
                    $edades_pya[$e] = [];
                }
                if (!isset($edades_pya[$e][$p])) {
                    $edades_pya[$e][$p] = [];
                }
                if (!isset($edades_pya[$e][$p][$a])) {
                    $edades_pya[$e][$p][$a] = 0;
                }
                $edades_pya[$e][$p][$a]++;
            }
            ksort($edades);
            ksort($edades_pais);
            ksort($edades_alojamiento);
            ksort($edades_pya);

            foreach ($bookings->get() as $b) {
                $p = $b->centro->pais_name;

                if (!isset($paises[$p])) {
                    $paises[$p] = 0;
                }

                $paises[$p]++;
            }
            ksort($paises);

            $alojamientos = [];
            foreach ($bookings->get() as $b) {
                $p = $b->alojamiento ? $b->alojamiento->tipo_name : "-";

                if (!isset($alojamientos[$p])) {
                    $alojamientos[$p] = 0;
                }

                $alojamientos[$p]++;
            }
            ksort($alojamientos);

            Graficos::loadEdades($edades, $edades_pais, $paises);
        }

        if (Datatable::shouldHandle()) {
            return Datatable::collection($bookings->get())
                ->addColumn('booking', function ($model) {
                    return "<a data-label='Booking' href='" . route('manage.bookings.ficha', $model->id) . "'>" . $model->id . "</a>";
                })
                ->addColumn('edad', function ($model) {
                    return $model->edad;
                })
                ->addColumn('pais', function ($model) {
                    return $model->centro->pais_name;
                })
                ->addColumn('alojamiento', function ($model) {
                    return $model->alojamiento ? $model->alojamiento->tipo_name : "-";
                })
                ->addColumn('tipo', function ($model) {
                    $cant = $model->viajero->bookings->count();
                    return $cant > 1 ? "Repetidor ($cant)" : "1er booking";
                })
                ->searchColumns('booking', 'edad')
                ->orderColumns('edad', 'pais', 'alojamiento')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        return view('manage.informes.edad', compact(
            'valores',
            'plataformas',
            'oficinas',
            'categorias',
            'subcategorias',
            'anys',
            'listado',
            'total',
            'edades',
            'paises',
            'alojamientos',
            'edades_pais',
            'edades_alojamiento'
        ));
    }

    public function getTrasvase(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = intval($request->input('plataformas', 0));
        } else {
            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }

            $valores['plataformas'] = ConfigHelper::propietario();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        $valores['any'] = intval($request->input('any', Carbon::now()->format('Y')));
        $valores['any0'] = intval($request->input('any0', Carbon::now()->subYear()->format('Y')));

        $any = intval($request->input('any', Carbon::now()->format('Y')));
        $any0 = intval($request->input('any0', Carbon::now()->subYear()->format('Y')));

        $valores['oficinas'] = intval($request->input('oficinas', 0));

        $listado = $request->has('any');
        $total = 0;

        $total_any = 0;
        $total_any0 = 0;

        $trasvase = [];
        $trasvase_cantidad = [];

        if ($listado)
        {   
            unset($valores['any']);
            unset($valores['any0']);
            
            $valores['desdes'] = "01/01/$any";
            $valores['hastas'] = "31/12/$any";
            $bookings = Booking::listadoFiltros($valores);

            $valores['desdes'] = "01/01/$any0";
            $valores['hastas'] = "31/12/$any0";
            $bookings_anterior = Booking::listadoFiltros($valores);

            $valores['any'] = $any;
            $valores['any0'] = $any0;

            $total_any = $bookings->count();
            $total_any0 = $bookings_anterior->count();

            foreach ($bookings_anterior->get() as $booking)
            {
                $viajero = $booking->viajero;
                $cat = $booking->category_id;

                if (!isset($trasvase[$cat])) {
                    $trasvase[$cat] = [];
                    $trasvase_cantidad[$cat] = 0;
                }

                $trasvase_cantidad[$cat]++;

                $bookings_viajero = clone $bookings;
                $bookings_viajero = $bookings_viajero->where('viajero_id', $viajero->id);

                if ($bookings_viajero->count()) {
                    foreach ($bookings_viajero->get() as $b) {
                        $cat0 = $b->category_id;
                        if (!isset($trasvase[$cat][$cat0])) {
                            $trasvase[$cat][$cat0] = 0;
                        }

                        $trasvase[$cat][$cat0]++;
                    }
                } else {
                    $cat0 = 0; //Sin Booking
                    if (!isset($trasvase[$cat][$cat0])) {
                        $trasvase[$cat][$cat0] = 0;
                    }

                    $trasvase[$cat][$cat0]++;
                }
            }

            //Anterior Sin booking
            $cat = 0;
            $cat0 = 0;
            $trasvase[$cat] = [];
            $trasvase_cantidad[$cat] = 0;
            foreach ($bookings->get() as $booking)
            {
                $viajero = $booking->viajero;

                $trasvase_cantidad[$cat]++;

                $bookings_viajero = clone $bookings_anterior;
                $bookings_viajero = $bookings_viajero->where('viajero_id', $viajero->id);

                if (!$bookings_viajero->count()) {
                    if (!isset($trasvase[$cat][$cat0])) {
                        $trasvase[$cat][$cat0] = 0;
                    }

                    $trasvase[$cat][$cat0]++;
                }
            }

            ksort($trasvase[$cat]);

            Graficos::loadTrasvase($trasvase, $trasvase_cantidad, $any0, $any);
        }

        return view('manage.informes.trasvase', compact(
            'valores',
            'plataformas',
            'oficinas',
            'anys',
            'listado',
            'total',
            'trasvase',
            'trasvase_cantidad',
            // 'trasvase_ant',
            // 'trasvase_ant_cantidad',
            'any',
            'any0',
            'total_any',
            'total_any0'
        ));
    }

    public function getBookingsOrigen(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $valores['categorias'] = $request->input('categorias', 0);
        $valores['subcategorias'] = $request->input('subcategorias', 0);
        $valores['oficinas'] = $request->input('oficinas', 0);


        //Tenemos 2 botones: filtro1 = pago1 y filtro2 = booking
        $fdesde = 'desde';
        $fhasta = 'hasta';
        $tipof = "1r Pago";

        if ($request->has('filtro2')) {
            $fdesde = 'desdes';
            $fhasta = 'hastas';
            $tipof = "Inicio Booking";
            $valores['filtro2'] = $tipof;
        } else {
            $valores['filtro1'] = $tipof;
        }

        $valores[$fdesde] = $request->input('desde', null);
        $valores[$fhasta] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));

        if (!$valores[$fdesde]) {
            $valores[$fdesde] = $request->input('desdes', null);
            $valores[$fhasta] = $request->input('hastas', null);
            if (!$valores[$fdesde]) {
                $valores[$fdesde] = "01/01/" . $valores['any'];
                $valores[$fhasta] = "31/12/" . $valores['any'];
            }
        }

        // dd($valores);

        $listado = $request->has('plataformas') ? true : false;
        $results = 0;
        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);

            $results = $bookings->count();
        }

        $fdesde = $valores[$fdesde];
        $fhasta = $valores[$fhasta];

        // dd($bookings->count());
        // dd($valores);

        // dd($bookings->pluck('id'));

        //Dtt
        if (Datatable::shouldHandle()) {
            // $bookings = Booking::bookingGrupos($valores);
            $bookings = $bookings->orderBy('course_start_date')->get();

            return Datatable::collection($bookings)
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' target='_blank' href='" . route('manage.viajeros.ficha', $model->viajero_id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('curso', function ($model) {
                    return "<a data-label='Booking' target='_blank' href='" . route('manage.bookings.ficha', $model->id) . "'>" . $model->curso->name . "</a>";
                })
                ->addColumn('semanas', function ($model) {
                    return $model->semanas_unit;
                })
                ->addColumn('total_curso', function ($model) {
                    return $model->total_curso;
                })
                ->addColumn('total', function ($model) {
                    return $model->total;
                })
                ->addColumn('origen', function ($model) {
                    return $model->origen_name;
                })
                ->addColumn('suborigen', function ($model) {
                    return $model->suborigen_name;
                })
                ->addColumn('suborigen_det', function ($model) {
                    return $model->suborigen_det_name;
                })
                ->addColumn('fecha_p1', function ($model) {
                    return $model->fecha_pago1 ? $model->fecha_pago1->format('Y-m-d') : "-";
                })
                ->searchColumns('viajero', 'curso', 'semanas', 'total_curso', 'total', 'origen', 'suborigen', 'suborigen_det')
                ->orderColumns('viajero', 'curso', 'semanas', 'total_curso', 'total', 'origen', 'suborigen', 'suborigen_det', 'fecha_p1')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.bookings_origen', compact('valores', 'results', 'listado', 'categorias', 'subcategorias', 'plataformas', 'oficinas', 'anys', 'fdesde', 'fhasta', 'tipof'));
    }

    public function getMargenes(Request $request)
    {
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $valores['categorias'] = $request->input('categorias', 0);
        $valores['subcategorias'] = $request->input('subcategorias', 0);
        $valores['oficinas'] = $request->input('oficinas', 0);


        //Tenemos 2 botones: filtro1 = pago1 y filtro2 = booking
        $fdesde = 'desde';
        $fhasta = 'hasta';
        $tipof = "1r Pago";

        if ($request->has('filtro2')) {
            $fdesde = 'desdes';
            $fhasta = 'hastas';
            $tipof = "Inicio Booking";
            $valores['filtro2'] = $tipof;
        } else {
            $valores['filtro1'] = $tipof;
        }

        $valores[$fdesde] = $request->input('desde', null);
        $valores[$fhasta] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));

        if (!$valores[$fdesde]) {
            $valores[$fdesde] = $request->input('desdes', null);
            $valores[$fhasta] = $request->input('hastas', null);
            if (!$valores[$fdesde]) {
                $valores[$fdesde] = "01/01/" . $valores['any'];
                $valores[$fhasta] = "31/12/" . $valores['any'];
            }
        }

        $listado = $request->has('plataformas') ? true : false;
        $results = 0;
        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);

            $results = $bookings->count();
        }

        $fdesde = $valores[$fdesde];
        $fhasta = $valores[$fhasta];

        // dd($bookings->count());
        // dd($valores);

        //Dtt
        if (Datatable::shouldHandle()) {
            // $bookings = Booking::bookingGrupos($valores);
            $bookings = $bookings->orderBy('course_start_date')->get();

            return Datatable::collection($bookings)
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' target='_blank' href='" . route('manage.viajeros.ficha', $model->viajero_id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('curso', function ($model) {
                    return "<a data-label='Booking' target='_blank' href='" . route('manage.bookings.ficha', $model->id) . "'>" . $model->curso->name . "</a>";
                })
                ->addColumn('fecha', function ($model) {
                    return $model->course_start_date;
                })
                ->addColumn('proveedor', function ($model) {
                    return $model->curso->centro->proveedor->name;
                })
                ->addColumn('pais', function ($model) {
                    return $model->curso->centro->pais_name;
                })
                ->addColumn('total_curso', function ($model) {
                    $mRate = (float) ConfigHelper::monedaCambio($model->course_currency_id, $model->id);
                    return round($model->total_curso * $mRate, 0);
                })
                ->addColumn('mb', function ($model) {
                    return $model->mb();
                })
                ->addColumn('mb1', function ($model) {
                    $mRate = (float) ConfigHelper::monedaCambio($model->course_currency_id, $model->id);
                    $t = round($model->total_curso * $mRate, 0);
                    return $t ? round(($model->mb() / $t) * 100, 0) . "%" : 0;
                })
                ->addColumn('total', function ($model) {
                    return round($model->total, 0);
                })
                ->addColumn('mb2', function ($model) {
                    return $model->total ? round(($model->mb() / $model->total) * 100, 0) . "%" : "-";
                })
                ->addColumn('semanas', function ($model) {
                    return $model->semanas_unit;
                })
                ->addColumn('origen_txt', function ($model) {
                    return $model->full_name_origen_txt;
                })
                ->addColumn('asesor', function ($model) {
                    return $model->asignado ? $model->asignado->full_name : "-";
                })
                ->addColumn('fecha_p1', function ($model) {
                    return $model->fecha_pago1 ? $model->fecha_pago1->format('Y-m-d') : "-";
                })
                ->addColumn('total_curso_div', function ($model) {
                    return round($model->total_curso, 0);
                })
                ->addColumn('mb_div', function ($model) {
                    return $model->mb(true);
                })
                ->addColumn('divisa', function ($model) {
                    return $model->curso_moneda;
                })
                ->searchColumns('viajero', 'curso', 'proveedor', 'asesor', 'pais', 'origen_txt')
                ->orderColumns('viajero', 'curso', 'fecha', 'provedor', 'pais', 'total_curso', 'mb', 'mb1', 'mb2', 'semanas', 'origen_txt', 'asesor', 'fecha_p1')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view('manage.informes.margenes', compact('valores', 'results', 'listado', 'categorias', 'subcategorias', 'plataformas', 'oficinas', 'anys', 'fdesde', 'fhasta', 'tipof'));
    }

    public function getContactos(Request $request)
    {
        ini_set('memory_limit', '400M');
        ini_set('max_execution_time', 0);

        Artisan::call('view:clear');

        // $this->middleware("permiso.informe:manage.index.contactos");

        $user = auth()->user();

        if ($user->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();
            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = intval($request->input('plataformas', 0));
            $valores['oficinas'] = $request->input('oficinas', 0);

            $u = \VCN\Models\User::crm();

            $usuarios = $u->get();

            $users = $u->get()->pluck('full_name', 'id')->toArray();
            $valores['users'] = $request->input('users', []);
        } else {
            $plataformas = [0 => ConfigHelper::plataformaApp()];
            $oficinas = [0 => 'Todas'] + Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();
            $valores['oficinas'] = $request->input('oficinas', []);
            if (!$user->filtro_oficinas) {
                $valores['oficinas'] = $user->oficina_id;
            }

            if ($user->es_aislado) {
                $usuarios = [];
                $users = [];
                $valores['users'] = $user->id;
            } else {
                $u = \VCN\Models\User::crm()->plataforma();

                $usuarios = $u->get();

                $users = $u->get()->pluck('full_name', 'id')->toArray();
                $valores['users'] = $request->input('users', []);
            }
        }

        $valores['desde'] = $request->input('desde', null);
        $valores['hasta'] = $request->input('hasta', null);

        if ($request->has('filtro_auto_1')) {
            $valores['desde'] = Carbon::now()->subDays(30)->format('d/m/Y');
            $valores['hasta'] = Carbon::now()->format('d/m/Y');
        } elseif ($request->has('filtro_auto_2')) {
            $valores['desde'] = Carbon::now()->subDays(90)->format('d/m/Y');
            $valores['hasta'] = Carbon::now()->format('d/m/Y');
        }

        $contactos = [];
        $resultados = false;

        if ($valores['desde'] && $valores['hasta']) {
            $desde = Carbon::createFromFormat('!d/m/Y', $valores['desde']);
            $hasta = Carbon::createFromFormat('!d/m/Y', $valores['hasta']);

            if ($desde->lte($hasta)) {
                $resultados = true; //$request->has('plataformas')?true:false;
            }
        }

        // dd($valores);

        $usuarios = [];
        $fechas = [];

        if ($resultados)
        {
            // dd($valores);

            if (isset($valores['users']) && count($valores['users']) && $valores['users'][0] != 0) {
                $usuarios = \VCN\Models\User::whereIn('id', $valores['users'])->get();
            } else {
                if (isset($valores['oficinas']) && count($valores['oficinas']) && $valores['oficinas'][0] != 0) {
                    $usuarios = \VCN\Models\User::crm()->whereIn('oficina_id', $valores['oficinas'])->get();
                } elseif (isset($valores['plataformas']) && $valores['plataformas'] != 0) {
                    $usuarios = \VCN\Models\User::crm()->where('plataforma', $valores['plataformas'])->get();
                } else {
                    if ($user->es_full_admin) {
                        $usuarios = \VCN\Models\User::crm()->get();
                    } else {
                        $usuarios = \VCN\Models\User::where('id', $user->id)->get();
                    }
                }
            }

            $uId = $usuarios->pluck('id')->toArray();
            $dbContactos = DB::table('viajero_logs')->selectRaw('*, DATE_FORMAT(created_at, "%d/%m/%Y") as fecha, count(*) as contactos')
                ->where('tipo', '<>', '')->where('tipo', '<>', 'log')
                ->whereIn('user_id', $uId)
                ->whereBetween('created_at', [$desde, $hasta])->groupBy('user_id', 'viajero_id', 'fecha')->get()->toArray();

            $chartc1 = Lava::DataTable();
            $chartc1->addStringColumn('Día');
            foreach ($usuarios as $u) {
                $chartc1->addNumberColumn($u->fname);
            }

            $d = clone $desde;
            $iFechas = $hasta->diffInDays($desde);
            for ($i = 0; $i < $iFechas; $i++) {
                $f = $d->format('d/m/Y');
                $fechas[] = $f;
                $contactos[$f] = [];

                $row = [];
                $row[] = $f;

                $rfechas = array_filter($dbContactos, function ($c) use ($f) {
                    return $c->fecha == $f;
                });

                foreach ($usuarios as $u) {
                    $user_id = $u->id;
                    $ruser = array_filter($rfechas, function ($c) use ($user_id) {
                        return $c->user_id == $user_id;
                    });

                    $c = count($ruser);
                    $contactos[$f][$u->id] = $c;

                    $row[] = $c;
                }

                $d->addDay();

                $chartc1->addRow($row);
            }

            // dd($contactos);

            Lava::ColumnChart('Chart-Contactos', $chartc1, [
                'title' => 'Contactos',
                'titleTextStyle' => [
                    'color'    => '#eb6b2c',
                    'fontSize' => 14
                ],
                'height' => 400,
            ]);
        }

        // dd($valores);

        //view
        return view(
            'manage.informes.contactos',
            compact(
                'users',
                'contactos',
                'resultados',
                'valores',
                'usuarios',
                'fechas',
                'plataformas',
                'oficinas'
            )
        );
    }

    public function getDocsEspecificos(Request $request)
    {
        // Plataforma / Categoria / Subcategoria / Subdetalle / Centro / Curso / Oficina / Asignado
        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategoriasdet = [0 => 'Todas'] + SubcategoriaDetalle::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = $request->input('plataformas', 0);
        } else {
            $valores['plataformas'] = ConfigHelper::propietario();

            $plataformas = [0 => ConfigHelper::plataformaApp()];

            $oficinas = Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            if (auth()->user()->informe_oficinas) {
                $oficinas = [0 => 'Todas'] + $oficinas;
            }

            $centros = [0 => 'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategoriasdet = [0 => 'Todas'] + SubcategoriaDetalle::all()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $asignados = [0 => 'Todos'] + User::asignados()->get()->pluck('full_name', 'id')->toArray();

        $valores['categorias'] = $request->input('categorias', 0);
        $valores['subcategorias'] = $request->input('subcategorias', 0);
        $valores['subcategoriasdet'] = $request->input('subcategoriasdet', 0);
        $valores['oficinas'] = $request->input('oficinas', 0);
        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['cursos'] = intval($request->input('cursos', 0));
        $valores['asignados'] = intval($request->input('asignados', 0));

        $valores['desdes'] = $request->input('desde', null);
        $valores['hastas'] = $request->input('hasta', null);
        $valores['any'] = intval($request->input('any', Carbon::now()->year));
        if (!$valores['desdes']) {
            $valores['desdes'] = $request->input('desdes', null);
            $valores['hastas'] = $request->input('hastas', null);

            if (!$valores['desdes']) {
                $valores['desdes'] = "01/01/" . $valores['any'];
                $valores['hastas'] = "31/12/" . $valores['any'];
            }
        }

        // dd($valores);

        $results = null;
        $proveedores = null;
        $listado = $request->has('plataformas') ? true : false;

        //dtt
        if ($listado) {
            $bookings = Booking::listadoFiltros($valores);

            $results = $bookings->count();
            // $results = $results>100 ? 0 : $results;
        }

        //Dtt
        if (Datatable::shouldHandle()) {
            $bookings = $listado ? $bookings->get() : [];

            // $docs = \VCN\Models\System\DocEspecifico

            $docs = collect();
            $iDocs = 0;
            foreach ($bookings as $booking) {
                foreach ($booking->documentos_especificos as $d) {
                    $d->booking_id = $booking->id;
                    $docs->push($d);

                    $iDocs++;
                }
            }

            return Datatable::collection($docs)
                ->addColumn('booking_id', function ($model) {
                    return "<a data-label='Ficha Booking' target='_blank' href='" . route('manage.bookings.ficha', $model->booking_id) . "'>" . $model->booking_id . "</a>";
                })
                ->addColumn('viajero', function ($model) {
                    $booking = $model->booking;
                    return "<a data-label='Ficha Viajero' target='_blank' href='" . route('manage.viajeros.ficha', $booking->viajero_id) . "'>" . $booking->viajero->full_name . "</a>";
                })
                ->addColumn('curso', function ($model) {
                    return $model->booking->curso->name;
                })
                ->addColumn('convocatoria', function ($model) {
                    return $model->booking->convocatoria->name;
                })
                ->addColumn('doc', function ($model) {
                    return $model->name;
                })
                ->addColumn('status', function ($model) {
                    $booking = $model->booking;
                    $adjunto = $booking->archivos->where('doc_especifico_id', $model->id)->first();

                    if ($adjunto) {
                        return $adjunto->doc_especifico_status ? "Aceptado" : "Pend. validar";
                    }

                    return "Pendiente";
                })
                ->addColumn('asignado', function ($model) {
                    return $model->booking->asignado_name;
                })
                ->addColumn('oficina', function ($model) {
                    return $model->booking->oficina_name;
                })
                ->searchColumns('viajero', 'curso', 'convocatoria')
                ->orderColumns('booking_id', 'viajero', 'curso', 'convocatoria')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }


        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;

        return view(
            'manage.informes.doc_especificos',
            compact('valores', 'listado', 'results', 'categorias', 'subcategorias', 'subcategoriasdet', 'plataformas', 'asignados', 'oficinas', 'proveedores', 'centros', 'cursos', 'anys')
        );
    }

    public function getEnvioCatalogos(Request $request)
    {
        if (Datatable::shouldHandle()) {
            $filtro = ConfigHelper::config('propietario');
            $col = Solicitud::where('catalogo_envio', 1)->where('catalogo_enviado', 0);
            if ($filtro) {
                $col = $col->where(function ($q) use ($filtro) {
                    $q->where('plataforma', $filtro)->orWhere('plataforma', 0);
                });
            }

            return Datatable::collection($col->get())
                ->addColumn('viajero', function ($model) {
                    return "<a data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                })
                ->addColumn('tipovia', function ($model) {
                    return $model->viajero->datos->tipo_via_name;
                })
                ->addColumn('direccion', function ($model) {
                    return $model->viajero->datos->direccion;
                })
                ->addColumn('cp', function ($model) {
                    return $model->viajero->datos->cp;
                })
                ->addColumn('ciudad', function ($model) {
                    return $model->viajero->datos->ciudad;
                })
                ->addColumn('idioma', function ($model) {
                    return $model->viajero->idioma_contacto;
                })
                ->addColumn('log', function ($model) {
                    return $model->catalogo_envio_log;
                })
                ->addColumn('boton', function ($model) {
                    return "<a href='" . route('manage.solicitudes.set-catalogo', $model->id) . "') class='btn btn-info'>Enviado</a>";
                })
                ->searchColumns('viajero')
                // ->orderColumns('fecha','importe')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.informes.envio_catalogos');
    }

    public function getDatosInscritos(Request $request)
    {
        $user = $request->user();

        if (auth()->user()->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $prescriptores = [0 => 'Todos'] + Prescriptor::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $proveedores = [0 => 'Todos'] + Proveedor::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
        } else {
            $plataformas = [$user->plataforma => ConfigHelper::plataformaApp()];

            $oficinas = [0 => 'Todas'] + Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $prescriptores = [0 => 'Todos'] + Prescriptor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $proveedores = [0 => 'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $centros = [0 => 'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $cursos = [0 => 'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
        }

        $paises = [0 => 'Todos'] + Pais::all()->sortBy('name')->pluck('name', 'id')->toArray();
        $convocatorias = [0 => 'Todas'];
        $anys = Venta::groupBy('any')->get()->pluck('any', 'any')->toArray();
        $anys[end($anys) + 1] = end($anys) + 1;
        $anys[0] = "-";

        $valores['plataformas'] = intval($request->input('plataformas', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['centros'] = intval($request->input('centros', 0));
        $valores['tipoc'] = intval($request->input('tipoc', 0));
        $valores['directos'] = intval($request->input('directos', 0));
        $valores['prescriptores'] = intval($request->input('prescriptores', 0));
        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['subcategorias'] = intval($request->input('subcategorias', 0));
        $valores['proveedores'] = intval($request->input('proveedores', 0));
        $valores['paises'] = intval($request->input('paises', 0));
        $valores['any'] = intval($request->input('any', 0));

        // $valores['oficinas'] = $request->input('oficinas', []);
        // $valores['cursos'] = $request->input('cursos', []);
        // $valores['convocatorias'] = $request->input('convocatorias', []);

        $valores['cursos'] = $request->has('cursos') ? implode(',', $request->get('cursos', [])) : "";
        $valores['oficinas'] = $request->has('oficinas') ? implode(',', $request->get('oficinas', [])) : "";
        $valores['convocatorias'] = $request->has('convocatorias') ? implode(',', $request->get('convocatorias', [])) : "";

        // dd($valores);

        if ($valores['convocatorias'] || $valores['cursos']) {
            $cursosSel = Curso::whereIn('id', $request->get('cursos'))->get();
            $curso = $cursosSel->first();
            // $valores['tipoc'] = $curso->convocatoria_tipo_num;
            foreach ($cursosSel as $curso) {
                $convocatorias += $curso->convocatorias_all->sortBy('name')->pluck('name', 'id')->toArray();
            }
        }

        $valoresNot['prescriptores'] = $request->has('not-prescriptores') ? implode(',', $request->get('not-prescriptores', [])) : "";
        $valoresNot['cursos'] = $request->has('not-curso') ? implode(',',$request->get('not-cursos',[])) : null;

        // dd($valores);

        $listado = $request->has('listado');

        // if ($listado && Datatable::shouldHandle())
        {
            ini_set('memory_limit', '512M');
            set_time_limit(0);

            $list_viajeros = collect();

            $list = Booking::listadoFiltros($valores, false, $valoresNot);

            if ($request->get('list') == "viajeros") {
                return Datatable::collection($list->get())
                    ->addColumn('name', function ($model) {
                        $ret = "<a href='" . route('manage.viajeros.ficha', $model->viajero_id) . "'> " . $model->viajero->name . "</a>";
                        return $ret;
                    })
                    ->addColumn('lastname', function ($model) {
                        return $model->viajero->lastname;
                    })
                    ->addColumn('lastname2', function ($model) {
                        return $model->viajero->lastname2;
                    })
                    ->addColumn('edad', function ($model) {
                        return $model->edad;
                    })
                    ->addColumn('idioma', function ($model) {
                        return $model->viajero->idioma_contacto;
                    })
                    ->addColumn('email', function ($model) {
                        return $model->viajero->email;
                    })
                    ->addColumn('movil', function ($model) {
                        return $model->viajero->movil;
                    })
                    ->addColumn('optout', function ($model) {
                        return $model->viajero->optout ? "OPTOUT" : "OPTIN";
                    })
                    ->addColumn('convocatoria', function ($model) {
                        return $model->convocatoria ? $model->convocatoria->name : "-";
                    })
                    ->addColumn('asignado', function ($model) {
                        return $model->asignado ? $model->asignado->full_name : "-";
                    })
                    ->addColumn('oficina', function ($model) {
                        return $model->oficina ? $model->oficina->name : "-";
                    })
                    ->searchColumns('name')
                    ->orderColumns('name')
                    ->setSearchStrip()->setOrderStrip()
                    ->setAliasMapping()
                    ->make();
            }

            if ($request->get('list') == "tutores") {
                $list_tutores = collect();

                $viajero_ids = $list->pluck('viajero_id')->toArray();
                $viajero_ids = array_unique($viajero_ids);

                foreach ($list->get() as $b) {
                    $v = $b->viajero;
                    if ($v->tutor1 && $v->tutor1->email) {
                        $t = $v->tutor1;
                        $t->edad = $v->edad;
                        $t->viajero_nom = $v->name;
                        $t->viajero_ape = $v->lastname . " " . $v->lastname2;
                        $t->viajero_id = $v->id;
                        $t->idioma = $v->idioma_contacto;
                        $t->convocatoria = $b->convocatoria_name;
                        $t->asignado = $b->asignado ? $b->asignado->full_name : "-";
                        $t->oficina = $b->oficina ? $b->oficina->name : "-";

                        $list_tutores->push($t);
                    }

                    if ($v->tutor2 && $v->tutor2->email) {
                        $t = $v->tutor2;
                        $t->edad = $v->edad;
                        $t->viajero_nom = $v->name;
                        $t->viajero_ape = $v->lastname . " " . $v->lastname2;
                        $t->viajero_id = $v->id;
                        $t->idioma = $v->idioma_contacto;
                        $t->convocatoria = $b->convocatoria_name;
                        $t->asignado = $b->asignado ? $b->asignado->full_name : "-";
                        $t->oficina = $b->oficina ? $b->oficina->name : "-";

                        $list_tutores->push($t);
                    }
                }

                return Datatable::collection($list_tutores)
                    ->showColumns(['lastname', 'edad', 'idioma', 'movil', 'email', 'convocatoria', 'asignado', 'oficina'])
                    ->addColumn('name', function ($model) {
                        $ret = "<a href='" . route('manage.tutores.ficha', $model->id) . "'> " . $model->name . "</a>";
                        return $ret;
                    })
                    ->addColumn('viajero_nom', function ($model) {
                        $ret = "<a href='" . route('manage.viajeros.ficha', $model->viajero_id) . "'> " . $model->viajero_nom . "</a>";
                        return $ret;
                    })
                    ->addColumn('viajero_ape', function ($model) {
                        $ret = $model->viajero_ape;
                        return $ret;
                    })
                    ->addColumn('optout', function ($model) {
                        return $model->optout ? "OPTOUT" : "OPTIN";
                    })
                    ->searchColumns('name')
                    ->orderColumns('name')
                    ->setSearchStrip()->setOrderStrip()
                    ->setAliasMapping()
                    ->make();
            }
        }

        $filtros = $request->input();

        // dd($valores);

        return view('manage.informes.datos_inscritos', compact(
            'valores',
            'valoresNot',
            'listado',
            'filtros',
            'categorias',
            'subcategorias',
            'plataformas',
            'oficinas',
            'prescriptores',
            'proveedores',
            'centros',
            'cursos',
            'convocatorias',
            'paises',
            'anys'
        ));
    }

    public function getAnalSolicitudes(Request $request)
    {
        ini_set('memory_limit', '400M');
        ini_set('max_execution_time', 0);

        Artisan::call('view:clear');

        // $this->middleware("permiso.informe:manage.index.contactos");

        $user = auth()->user();

        if ($user->isFullAdmin()) {
            $plataformas = ConfigHelper::plataformas();
            $oficinas = [0 => 'Todas'] + Oficina::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = intval($request->input('plataformas', 0));
            $valores['oficinas'] = $request->get('oficinas', []);

            $u = \VCN\Models\User::crm();

            $usuarios = $u->get();

            $users = $u->get()->pluck('full_name', 'id')->toArray();
            $valores['users'] = $request->input('users', []);
        } else {
            $plataformas = [0 => ConfigHelper::plataformaApp()];
            $oficinas = [0 => 'Todas'] + Oficina::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();
            $subcategorias = [0 => 'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name', 'id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();
            $valores['oficinas'] = $request->get('oficinas', []);
            if (!$user->filtro_oficinas) {
                $valores['oficinas'] = $user->oficina_id;
            }

            if ($user->es_aislado) {
                $usuarios = [];
                $users = [];
                $valores['users'] = $user->id;
            } else {
                $u = \VCN\Models\User::crm()->plataforma();

                $usuarios = $u->get();

                $users = $u->get()->pluck('full_name', 'id')->toArray();
                $valores['users'] = $request->input('users', []);
            }
        }

        $valores['desde'] = $request->input('desde', null);
        $valores['hasta'] = $request->input('hasta', null);

        $valores['categorias'] = intval($request->input('categorias', 0));
        $valores['subcategorias'] = intval($request->input('subcategorias', 0));
        $valores['agrupados'] = $request->has('agrupados');
        $valores['asignados'] = $valores['users'];

        $resultados = false;

        if ($valores['desde'] && $valores['hasta']) {
            $desde = Carbon::createFromFormat('!d/m/Y', $valores['desde']);
            $hasta = Carbon::createFromFormat('!d/m/Y', $valores['hasta']);

            if ($desde->lte($hasta)) {
                $resultados = true;
            }
        }

        $fechas = null;
        $fechas_txt = null;
        $solicitudes = null;
        $solicitudesPrev = null;
        $solicitudesDesc = null;
        $bookings = null;

        // dd($valores);
        if ($resultados) {
            $modo = $request->has('filtro_anys') ? 'anys' : ($request->has('filtro_semanas') ? 'semanas' : ($request->has('filtro_meses') ? 'meses' : 'dias'));
            $fechasModo = self::getFechasByModo($desde, $hasta, $modo);
            $fechas_txt = $fechasModo['fechas_txt'];
            $fechas = $fechasModo['fechas'];

            $valoresS = $valores;
            $valoresS['desdec'] = $valores['desde'];
            $valoresS['hastac'] = $valores['hasta'];

            $solicitudes = Solicitud::listadoFiltros($valoresS);
            // dd($solicitudes->count());
            $sids = $solicitudes->pluck('id')->toArray();

            $filUser = !count($valores['asignados']);

            $st = [];
            $st[] = ConfigHelper::config('solicitud_status_lead');
            $st[] = ConfigHelper::config('solicitud_status_contactado');
            $st[] = ConfigHelper::config('solicitud_status_decidiendo');
            $st[] = ConfigHelper::config('solicitud_status_futuro');
            // $st[] = ConfigHelper::config('solicitud_status_archivado');
            // $st[] = ConfigHelper::config('solicitud_status_inscrito');

            $filtro = self::getFiltroByModo($modo, 'created_at');
            $solicitudesLogs = SolicitudLogStatus::whereIn('solicitud_id', $sids)->whereIn('status2', $st)->groupBy('solicitud_id')->pluck('solicitud_id')->toArray();
            $solicitudes = Solicitud::whereIn('id', $solicitudesLogs)->selectRaw("$filtro, user_id, count(*) as cuenta")->groupBy('user_id', 'fecha_grup')->get();
            // dd($solicitudes->sum('cuenta'));

            $st = [];
            $st[] = ConfigHelper::config('solicitud_status_archivado');
            $solicitudesLogs = SolicitudLogStatus::whereIn('solicitud_id', $sids)->whereIn('status2', $st)->groupBy('solicitud_id')->pluck('solicitud_id')->toArray();
            $solicitudesDesc = Solicitud::whereIn('id', $solicitudesLogs)->selectRaw("$filtro, user_id, count(*) as cuenta")->groupBy('user_id', 'fecha_grup')->get();
            // dd($solicitudesDesc);

            $st = [];
            $st[] = ConfigHelper::config('solicitud_status_lead');
            $st[] = ConfigHelper::config('solicitud_status_contactado');
            $st[] = ConfigHelper::config('solicitud_status_decidiendo');

            $filtro = self::getFiltroByModo($modo, 'fecha');
            $solicitudesPrev = Solicitud::where('fecha', '<', $desde->toDateString())->where('status_id', $st)->selectRaw("user_id, count(*) as cuenta")->groupBy('user_id')->get();
            // dd($solicitudesPrev);

            // $solicitudes = $solicitudes->merge($solicitudesPrev);
            // dd($solicitudes);

            //bookings
            $valoresB = $valores;
            $valoresB['desde'] = $valores['desde']; //fecha_pago1
            $valoresB['hasta'] = $valores['hasta'];

            $filtro = self::getFiltroByModo($modo, 'fecha_pago1');
            $bookings = Booking::listadoFiltros($valoresB)->selectRaw("$filtro, user_id, count(*) as cuenta")->groupBy('user_id', 'fecha_grup')->get();

            $usuarios = [0 => 'Todos'];
            if (count($valores['users'])) {
                $usuarios = User::whereIn('id', $valores['users'])->get()->pluck('full_name', 'id')->toArray();
            }
        }


        /*
        Filtros: Plataforma / Oficina / Usuario (que haya la opción de Agrupados que los agrupe a todos según filtro de oficina marcado) 
        / Categoria solicitud / Subcategoria
        Desde / Hasta

        días/semanas/meses/año

        $valores['agrupados']??
        
        - SOLICITUDES: suma de "Solicitudes con cambio de status durante el periodo (incluido las nuevas)"  
        :: status:  lead / contactado / decidiendo
        + "solicitudes anteriores al periodo pero con status = lead / contactado / decidiendo"

        - INSCRIPCIONES: suma de todos los bookings de este periodo
        - % conversión = inscripción / solicitudes (no incluirlo en el gráfico)
        
        - DESCARTADAS: suma de todas las solicitudes que han pasado a status = 'descartado' durante el periodo seleccionado
        - % descarte = descartadas / solicitudes (no incluirlo en el gráfico)
        */

        // dd($solicitudesDesc->pluck('user_id'));
        return view('manage.informes.anal_solicitudes')->with(compact(
            'plataformas',
            'oficinas',
            'users',
            'categorias',
            'subcategorias',
            'valores',
            'resultados',
            'fechas',
            'fechas_txt',
            'solicitudes',
            'solicitudesPrev',
            'solicitudesDesc',
            'bookings',
            'usuarios'
        ));
    }
}
