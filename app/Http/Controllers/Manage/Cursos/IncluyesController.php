<?php

namespace VCN\Http\Controllers\Manage\Cursos;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Cursos\IncluyeRepository as Incluye;

use Datatable;

class IncluyesController extends Controller
{
    private $incluye;

    /**
     * Instantiate a new IncluyeController instance.
     *
     * @return void
     */
    public function __construct( Incluye $incluye )
    {
        $this->checkPermisos('precios');

        $this->incluye = $incluye;
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection( $this->incluye->all() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cursos.incluyes.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('tipo', function($model) {
                    return $model->tipo?"Numérico":"Checkbox";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Curso Incluye' data-action='". route( 'manage.cursos.incluyes.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.cursos.incluyes.index');
    }

    public function getNuevo()
    {
        return view('manage.cursos.incluyes.new');
    }

    public function getUpdate($id)
    {
        $ficha = $this->incluye->find($id);

        return view('manage.cursos.incluyes.ficha', compact('ficha'));
    }

    public function postUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $data = $request->except("_token");

        $data['tipo'] = $request->has("tipo");

        if(!$id)
        {
            //Nuevo
            $p = $this->incluye->create($data);
            $id = $p->id;
        }
        else
        {
            $p = $this->incluye->update($data, $id);
        }

        // return view('manage.proveedores.ficha', compact('ficha'));
        return redirect()->route('manage.cursos.incluyes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->incluye->delete($id);
        return redirect()->route('manage.cursos.incluyes.index');
    }
}
