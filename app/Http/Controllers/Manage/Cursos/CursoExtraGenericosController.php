<?php

namespace VCN\Http\Controllers\Manage\Cursos;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Cursos\CursoExtraGenericoRepository as Extra;
use VCN\Models\Cursos\Curso;

use VCN\Models\Monedas\Moneda;

use Datatable;
use Input;
use Session;

class CursoExtraGenericosController extends Controller
{
    private $extra;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Extra $extra )
    {
        $this->checkPermisos('extras');

        $this->extra = $extra;
    }

    public function getIndex($curso_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->extra->all();
            if($curso_id>0)
            {
                $col = Curso::find($curso_id)->extrasGenericos;
            }

            return Datatable::collection( $col )
                ->addColumn('generico', function($model) {
                    return $model->generico->generic_name;
                    // return "<a href='". route('manage.cursos.extras.ficha',[$model->id]) ."'>$model->course_extras_name</a>";
                })
                ->addColumn('precio', function($model) {
                    $ret = $model->generico->generic_currency_id?Moneda::find($model->generico->generic_currency_id)->currency_name:"?";
                    $ret .= " ".$model->generico->generic_price;
                    return $ret;
                })
                ->addColumn('curso', function($model) {
                    return $model->curso->course_name;
                    // return "<a href='". route('manage.cursos.extras.ficha',[$model->id]) ."'>$model->course_extras_name</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";

                    $data = " data-label='Borrar' data-model='Extra Genérico Curso' data-action='". route( 'manage.cursos.genericos.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= "<a href='". route('manage.alojamientos.precios.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Precio</a>";
                    // $ret .= " <a href='". route('manage.alojamientos.cuotas.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Cuota</a>";

                    return $ret;
                })
                ->searchColumns('course_extras_name')
                // ->orderColumns('name')
                ->setAliasMapping()
                ->make();
        }

        return view('manage.cursos.genericos.index', compact('curso_id'));
    }

    public function add(Request $request, $curso_id=0)
    {
        $data = Input::except('_token');

        if($data['generics_id'])
        {
            $o = $this->extra->create($data);
            $curso_id = $o->course_id;
        }

        Session::flash('tab','#extras-genericos');

        return redirect()->route('manage.cursos.ficha',$curso_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $curso_id = $this->extra->find($id)->course_id;

        $this->extra->delete($id);

        Session::flash('tab','#extras-genericos');

        return redirect()->route('manage.cursos.ficha',$curso_id);
    }
}
