<?php

namespace VCN\Http\Controllers\Manage\Cursos;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Cursos\CursoRepository as Curso;
use VCN\Models\Proveedores\Proveedor;

use VCN\Models\Centros\Centro;
use VCN\Models\Alojamientos\Alojamiento;
use VCN\Models\Categoria;
use VCN\Models\Subcategoria;
use VCN\Models\SubcategoriaDetalle;
use VCN\Models\Extras\Extra;
use VCN\Models\Cursos\CursoExtraGenerico;
use VCN\Models\Cursos\CursoEspecialidad;
use VCN\Models\Especialidad;
use VCN\Models\Subespecialidad;
use VCN\Models\System\Cuestionario;
use VCN\Models\Exams\Examen;
use VCN\Models\Cursos\CursoSlug;
use VCN\Models\Traducciones\Traduccion;
use VCN\Helpers\MailHelper;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Bookings\Booking;

use Datatable;
use Input;
use View;
use File;
use Session;
use Validator;
use Image;
use PDF;
use App;
use Carbon;
use Log;

use VCN\Helpers\ConfigHelper;

use VCN\Repositories\Criteria\FiltroPlataformaCentro;

class CursosController extends Controller
{
    private $curso;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Curso $curso )
    {
        $this->middleware("permiso.plataforma:proveedores,\VCN\Models\Cursos\Curso", ['only' => ['getUpdate']]);
        $this->middleware("permiso.view:booking-firmas", ['only' => ['getFirmas']]);
        $this->checkPermisos('proveedores');

        $this->curso = $curso;
    }

    public function getIndex($centro_id=0)
    {
        $this->curso->pushCriteria(new FiltroPlataformaCentro());

        if(Datatable::shouldHandle())
        {
            $col = $this->curso->all();

            // $filtro = ConfigHelper::config('propietario');
            // if( $filtro && !auth()->user()->isFullAdmin() )
            // {
            //     // $col = ProveedorModel::where('propietario', 0)->orWhere('propietario',$filtro)->get();
            //     $col1 = $this->curso->findWhere(['propietario'=> $filtro]);
            //     $col = $this->curso->findWhere(['propietario'=> 0]);

            //     $col = $col->merge($col1);
            // }

            if($centro_id>0)
            {
                $col = Centro::find($centro_id)->cursos;//->sortBy('course_name');
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cursos.ficha',[$model->id]) ."'>$model->course_name</a>";
                })
                ->addColumn('proveedor', function($model) {
                    return $model->centro->proveedor->name;
                })
                ->addColumn('centro', function($model) {
                    return $model->centro->name;
                })
                ->addColumn('convocatoria', function($model) {
                    return $model->convocatoria_tipo;
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Curso' data-action='". route( 'manage.cursos.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    $ret .= " &nbsp;<a href='". route('manage.cursos.extras.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Extra</a>";

                    $ret .= " <a href='". route('manage.cursos.ficha',$model->id) ."#convocatorias-cerradas' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-arrow-circle-right'></i> Convoc. Cerradas</a>";
                    $ret .= " <a href='". route('manage.cursos.ficha',$model->id) ."#convocatorias-abiertas' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-arrow-circle-right'></i> Convoc. Abiertas</a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','convocatoria')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        return view('manage.cursos.index', compact('centro_id'));
    }


    public function getIndexByProveedor($proveedor_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->curso->all();
            if($proveedor_id)
            {
                $col = Proveedor::find($proveedor_id)->cursos->sortBy('course_name');
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cursos.ficha',[$model->id]) ."'>$model->course_name</a>";
                })
                ->addColumn('proveedor', function($model) {
                    return $model->centro->proveedor->name;
                })
                ->addColumn('centro', function($model) {
                    return $model->centro->name;
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        return view('manage.cursos.index_proveedor', compact('proveedor_id'));
    }

    public function getNuevo($centro_id=0)
    {
        $centros = [""=>""] + Centro::plataforma()->sortBy('name')->pluck('name','id')->toArray();
        $alojamientos = Alojamiento::where('center_id',$centro_id)->get()->pluck('full_name','id');
        $categorias = [""=>""] + Categoria::plataforma()->pluck('name','id')->toArray();
        $subcategorias = [];
        $subcategorias_det = [];

        $especialidades = [""=>""] + Especialidad::where('es_multi',0)->pluck('name','id')->toArray();
        $subespecialidades = [];

        $centro = null;
        if($centro_id)
        {
            $centro = Centro::find($centro_id);
        }

        $idiomas = ConfigHelper::getIdioma();
        $idioma_niveles = ConfigHelper::getIdiomaNivel();

        return view('manage.cursos.new',
            compact('centro_id','centro','centros','alojamientos','categorias','subcategorias','subcategorias_det','especialidades','subespecialidades','idiomas','idioma_niveles'));
    }

    public function getUpdate(Request $request, $id)
    {
        $ficha = $this->curso->find($id);

        if(!$ficha)
        {
            abort(404);
        }

        if($ficha->course_images=="")
        {
            $ficha->course_images = "curso_". $ficha->id;//str_slug($ficha->name);
            $ficha->course_timetable = "curso_". $ficha->id;//str_slug($ficha->name);
            $ficha->save();
        }

        $alojamientos = Alojamiento::where('center_id',$ficha->center_id)->get()->pluck('full_name','id');

        $categorias = [""=>""] + Categoria::plataforma()->pluck('name','id')->toArray();
        $subcategorias = [""=>""] + Subcategoria::where('category_id',$ficha->category_id)->pluck('name','id')->toArray();
        $subcategorias_det = [""=>""] + SubcategoriaDetalle::where('subcategory_id',$ficha->subcategory_id)->pluck('name','id')->toArray();

        $especialidades = [""=>""] + Especialidad::pluck('name','id')->toArray();
        $subespecialidades = [""=>""] + Subespecialidad::where('especialidad_id',$ficha->especialidad_id)->pluck('name','id')->toArray();

        $idiomas = ConfigHelper::getIdioma();
        $idioma_niveles = ConfigHelper::getIdiomaNivel();

        $extra_genericos = [""=>""] + Extra::where('es_cancelacion',0)->pluck('generic_name','id')->toArray();

        $cuestionarios = ['' => ''] + Cuestionario::plataforma()->where('activo',1)->pluck('name','id')->toArray();
        $examenes = ['' => ''] + Examen::plataforma()->where('activo',1)->pluck('name','id')->toArray();

        $slug = $ficha->course_slug;
        $c = $this->curso->findWhere(['course_slug'=> $slug]);
        if($c->count()>1)
        {
            Session::flash("mensaje", "Revise el slug del Curso (SEO). Está duplicado");
        }
        //Traducción tb:
        $t = Traduccion::where(['modelo'=> 'Curso', 'idioma'=> "ca", 'campo'=> 'course_slug', 'traduccion'=> $slug]);
        if($t->count()>1)
        {
            Session::flash("mensaje", "Revise la traducción del slug del Curso (SEO). Está duplicado ". $t->count() ." veces");
        }

        return view('manage.cursos.ficha',
            compact('ficha','alojamientos','categorias','subcategorias','subcategorias_det','especialidades','subespecialidades','idiomas','idioma_niveles','extra_genericos', 'cuestionarios','examenes'));
    }


    public function postUpdate(Request $request, $id=0)
    {
        if( $request->has('condiciones') )
        {
            $o = $this->curso->find($id);
            $condiciones = $o->condiciones;

            foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)
            {
                foreach(ConfigHelper::idiomas() as $keyi=>$idioma)
                {
                    if( !isset($condiciones[$keyp]) )
                    {
                        $condiciones[$keyp] = [];
                    }

                    if( !isset($condiciones[$keyp][$idioma]) )
                    {
                        $condiciones[$keyp][$idioma] = "";
                    }

                    $condiciones[$keyp][$idioma] = $request->input("condiciones_$keyp-$idioma");
                }
            }

            $o->condiciones = $condiciones;
            $o->save();
            $o->pdfCondiciones();

            Session::flash('tab','#condiciones');
            return redirect()->route('manage.cursos.ficha',$id);
        }

        if( $request->has('pdf_cancelacion') )
        {
            $o = $this->curso->find($id);
            $pdf = $o->pdf_cancelacion;

            $dir = "assets/uploads/pdf_cancelacion/";
            $name = "Curso_" . $o->id;

            foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)
            {
                foreach(ConfigHelper::idiomas() as $keyi=>$idioma)
                {
                    if( !isset($pdf[$keyp]) )
                    {
                        $pdf[$keyp] = [];
                    }

                    if( !isset($pdf[$keyp][$idioma]) )
                    {
                        $pdf[$keyp][$idioma] = "";
                    }
                    
                    $file_name = $pdf[$keyp][$idioma];
                    
                    if($request->has( "pdf_$keyp-$idioma"."_delete" ))
                    {
                        $pdf[$keyp][$idioma] = "";

                        $f = public_path($file_name);
                        if(file_exists($f))
                        {
                            \File::delete($f);
                        }
                        continue;
                    }

                    $file = $request->file("pdf_$keyp-$idioma");
                    if($file)
                    {
                        $file_name = $dir . $name . "_" . $keyp . "_" . $idioma;
                        $file_name = $file_name . "." . $file->getClientOriginalExtension();
                        $file->move(public_path($dir), $file_name);
                    }

                    $pdf[$keyp][$idioma] = $file_name;
                }
            }

            $o->pdf_cancelacion = $pdf;
            $o->save();

            Session::flash('tab','#pdf_cancelacion');
            return redirect()->route('manage.cursos.ficha',$id);
        }

        if($request->has('_seo'))
        {
            // $this->validate($request, [
            //     'course_slug'  => 'required|max:255|unique:cursos'
            // ]);

            $curso = $this->curso->find($id);
            $data = $request->except('_token','_seo');

            $slug0 = $curso->course_slug;
            $slug1 = isset($data['course_slug'])?$data['course_slug']:'';

            $curso->setSlug($slug1);
            $this->curso->update($data, $id);

            Session::flash('tab','#seo');
            return redirect()->route('manage.cursos.ficha',$id);
        }
        elseif($request->has('_pocket'))
        {
            $curso = $this->curso->find($id);

            $data = $request->except('_token','_pocket');

            $data['pocket_dinero_check'] = $request->has('pocket_dinero_check');
            $data['pocket_prueba_nivel_check'] = $request->has('pocket_prueba_nivel_check');
            $data['pocket_monitores_check'] = $request->has('pocket_monitores_check');
            $data['pocket_normas_check'] = $request->has('pocket_normas_check');

            $data['material_check'] = $request->has('material_check');
            $data['anterior_participantes_check'] = $request->has('anterior_participantes_check');
            $data['anterior_nacionalidades_check'] = $request->has('anterior_nacionalidades_check');

            $this->curso->update($data, $id);

            Session::flash('tab','#pocket');
            return redirect()->route('manage.cursos.ficha',$id);
        }

        $data = $request->except('_token','_seo','propietario_check','monitores');

        if(!$id)
        {
            $this->validate($request, [
                'center_id' => 'required|numeric|min:1',
                'category_id' => 'required|min:1',
                'subcategory_id' => 'required|min:1',
                'course_language' => 'required|min:1',
                // 'course_slug'  => 'required|max:255|unique:cursos'
            ]);

            $data['promo_texto'] = $request->get('promo_texto') ?: "";

            $slug = str_slug($data['course_name']);
            $data['course_slug'] = $slug;
        }

        $this->validate($request, [
            'course_name' => 'required',
            'category_id' => 'required|min:1',
            'subcategory_id' => 'required|min:1',
            // 'course_slug'  => 'required|max:255|unique:cursos'
        ]);

        //dd($request->input());

        $data['course_accommodation'] = Input::get('course_accommodation')?implode(',', Input::get('course_accommodation')):null;
        $data['course_language'] = Input::get('course_language')?implode(',', Input::get('course_language')):null;
        $data['course_active'] = Input::get('course_active',false);
        $data['course_highlight'] = Input::get('course_highlight',false);
        $data['course_promo'] = Input::get('course_promo',false);
        $video = explode("=", Input::get('course_video'));
        $data['course_video'] = count($video)>1?$video[1]:'';
        // $data['subespecialidad_id'] = $request->input('subespecialidad_id')?implode(',', $request->input('subespecialidad_id')):null;
        $data['es_convocatoria_multi'] = Input::get('es_convocatoria_multi',false);
        $data['activo_web'] = $request->has('activo_web');
        $data['pocket'] = $request->has('pocket');
        $data['monitor_web'] = $request->has('monitor_web');
        $data['activo_directo'] = $request->has('activo_directo');
        $data['reserva_tipo'] = $request->get('reserva_tipo') ?: 0;

        //dd($data);

        $data['propietario'] = $request->has('propietario')?$request->input('propietario'):0;
        if( $request->has('propietario_check') )
        {
            $data['propietario'] = ConfigHelper::config('propietario');
        }

        if(!$id)
        {
            //nuevo
            $c = $this->curso->create($data);
            $id = $c->id;

            $c->course_images = "curso_". $c->id;//str_slug($c->name);
            $c->course_timetable = "curso_". $c->id;//str_slug($c->name);
            $c->save();

            $curso = $this->curso->find($id);

            //Al crear => Extras genericos
            $extrasg = Extra::where('categorias_id','0')->get();
            foreach($extrasg as $extra)
            {
                $ce = new CursoExtraGenerico;
                $ce->generics_id = $extra->id;
                $ce->course_id = $curso->id;
                $ce->save();
            }

            $extrasg = Extra::whereRaw('find_in_set(?, categorias_id)',[$curso->category_id])->get();
            foreach($extrasg as $extra)
            {
                $ce = new CursoExtraGenerico;
                $ce->generics_id = $extra->id;
                $ce->course_id = $curso->id;
                $ce->save();
            }
        }
        else
        {
            $curso = $this->curso->find($id);
            $cat0 = $curso->category_id;
            $scat0 = $curso->subcategory_id;
            $scatd0 = $curso->subcategory_det_id;

            $this->curso->update($data, $id);

            if($cat0 != $data['category_id'] || $scat0 != $data['subcategory_id'] || $scatd0 != $data['subcategory_det_id'])
            {
                $now = Carbon::now()->toDateString();
                $bookings = Booking::where('curso_id',$curso->id)->where('course_start_date','>', $now)->get();

                if($bookings && $bookings->count())
                {
                    Session::flash('mensaje-ok', "Se han actualizado ". $bookings->count() ." bookings.");
                    
                    foreach($bookings as $b)
                    {
                        \VCN\Models\Informes\Venta::remove($b);
                        \VCN\Models\Informes\Venta::add($b, true);
                    }
                }
            }
        }

        $curso = $this->curso->find($id);

        $curso->setSlug();
        $curso->updateDataWeb(true);

        if(!$curso->commission || $curso->commission == "")
        {
            $curso->commission = $curso->centro->commission;
            $curso->save();
        }

        //Monitores
        \VCN\Models\Monitores\MonitorRelacion::refrescar('Curso',$id, $request->get('monitores'));

        //Monitor foto
        // if ($request->hasFile('monitor_foto'))
        // {
        //     $file = $request->file('monitor_foto');

        //     $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        //     $file_name = str_slug($file_name) .".". $file->getClientOriginalExtension();
        //     $dirp = "assets/uploads/monitor/". $id . "/";
        //     $dir = public_path($dirp);

        //     $file->move($dir, $file_name);

        //     $img = Image::make($dir.$file_name);
        //     $img->resize(1920, null, function ($constraint) {
        //         $constraint->aspectRatio();
        //     })->save();

        //     //thumb
        //     $file = $file_name;
        //     if (!file_exists($dir."thumb/"))
        //     {
        //         File::makeDirectory($dir."thumb/", 0775, true);
        //     }
        //     File::copy($dir.$file, $dir."thumb/".$file);
        //     $img2 = Image::make($dir."thumb/".$file);
        //     $img2->resize(450, null, function ($constraint) {
        //         $constraint->aspectRatio();
        //     })->save();

        //     $curso->monitor_foto = "/".$dirp.$file_name;
        //     $curso->save();
        // }

        if ($request->hasFile('icono'))
        {
            $dirp = "assets/uploads/icono/". $id . "/";

            $file = $request->file('icono');
            $curso->icono = ConfigHelper::uploadFoto($file, $dirp);
            $curso->save();
        }

        if($curso->course_images=="")
        {
            $curso->course_images = "curso_". $curso->id;//str_slug($curso->name);
            $curso->course_timetable = "curso_". $curso->id;//str_slug($curso->name);
            $curso->save();
        }

        return redirect()->route('manage.cursos.ficha',$id);
    }


    public function postUpload(Request $request, $tipo, $id)
    {
        $curso = $this->curso->find($id);

        $dir2 = $curso->course_images;
        if($tipo=='pdf')
        {
            $dir2 = $curso->course_timetable;
        }

        $dirp = "assets/uploads/$tipo/". $dir2 . "/";
        $dir = public_path($dirp);

        /*
        if($tipo=='course')
        {
            $rules = array(
                'file' => 'image|max:30000',
            );

            $input = Input::all();
            $validation = Validator::make($input, $rules);

            if ($validation->fails()) {
                return response("Error", 400);
            }
        }
        */

        if( $request->hasFile('file'))
        {
            $file = $request->file('file');
            ConfigHelper::uploadOptimize($file, $dirp);

            $curso->updateDataWeb(true);
            
            return response()->json('success', 200);

            // $file = str_slug($file->getClientOriginalName()) .".". $file->getClientOriginalExtension();
            /*
            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file = str_slug($file_name) .".". $file->getClientOriginalExtension();

            // $file = Input::file('file')->getClientOriginalName();
            Input::file('file')->move($dir, $file);

            $img = Image::make($dir.$file);
            $img->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            //thumb
            if (!file_exists($dir."thumb/"))
            {
                File::makeDirectory($dir."thumb/", 0775, true);
            }
            File::copy($dir.$file, $dir."thumb/".$file);
            $img2 = Image::make($dir."thumb/".$file);
            $img2->resize(450, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            */
        }

        return response()->json('success', 200);
    }

    public function destroy($id)
    {
        $this->curso->delete($id);

        return redirect()->route('manage.cursos.index');
    }

    public function getPocketGuidePdf($id, $lang)
    {
        $curso = $this->curso->find($id);

        $template = 'pdf.pocketguide';

        // return view($template, ['ficha'=> $curso, 'pdf'=> true, 'lang' => $lang]);

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path()."/tmp");
        $pdf = PDF::loadView($template, ['ficha'=> $curso, 'pdf'=> true, 'lang' => $lang]);


        $pdf->setOption('margin-top',20);
        $pdf->setOption('margin-right',0);
        $pdf->setOption('margin-bottom',30);
        $pdf->setOption('margin-left',0);
        $pdf->setOption('no-print-media-type',false);
        $pdf->setOption('footer-spacing',0);
        $pdf->setOption('header-font-size',9);
        $pdf->setOption('header-spacing',0);

        $pdf->setOption('header-html', 'https://'.ConfigHelper::config('web').'/assets/logos/header-pocketguide.html');
        $pdf->setOption('footer-html', 'https://'.ConfigHelper::config('web').'/assets/logos/footer-pocketguide.html');

        $name = "pocket-guide_".str_slug($curso->name)."_".$lang.".pdf";
        return $pdf->stream($name);
    }

    public function postUploadDelete($tipo,$id)
    {
        $curso = $this->curso->find($id);

        $dir2 = $curso->course_images;
        if($tipo=='pdf')
        {
            $dir2 = $curso->course_timetable;
        }

        $dir = public_path("assets/uploads/$tipo/". $dir2 . "/");

        $file = Input::get('f');
        File::delete($dir.$file);
        
        $curso->updateDataWeb(true);

        return response()->json('success', 200);
    }

    public function deleteFile($tipo, $id, $file)
    {
        $c = $this->curso->find($id);

        $dir2 = $c->course_images;
        if($tipo=='pdf')
        {
            $dir2 = $c->course_timetable;
            Session::flash('tab','#timetables');
        }
        else
        {
            Session::flash('tab','#imagenes');
        }

        $dir = public_path("assets/uploads/$tipo/". $dir2 . "/");
        File::delete($dir.$file);

        return redirect()->route('manage.cursos.ficha',$id);
    }

    public function setFotoPortada($id, $file)
    {
        $c = $this->curso->find($id);

        $c->image_portada = $file;
        $c->save();

        Session::flash('tab','#imagenes');

        return redirect()->route('manage.cursos.ficha',$id);
    }

    public function postEspecialidad(Request $request, $id)
    {
        $curso = $this->curso->find($id);

        if($request->has('edit_especialidad_id'))
        {
            $e = CursoEspecialidad::find($request->input('edit_especialidad_id'));
            $e->subespecialidad_id = $request->input('edit_subespecialidad_id')?implode(',', $request->input('edit_subespecialidad_id')):null;
            $e->save();

            Session::flash('tab','#especialidades');
            return redirect()->route('manage.cursos.ficha',$id);
        }

        $e = new CursoEspecialidad;
        $e->curso_id = $id;
        $e->especialidad_id = $request->input('especialidad_id');
        $e->subespecialidad_id = $request->input('subespecialidad_id')?implode(',', $request->input('subespecialidad_id')):null;
        $e->save();

        $curso->updateDataWeb(true);

        Session::flash('tab','#especialidades');
        return redirect()->route('manage.cursos.ficha',$id);

    }

    public function destroyEspecialidad($id)
    {
        $e = CursoEspecialidad::find($id);
        $curso = $e->curso;

        $e->delete();

        $curso->updateDataWeb(true);

        Session::flash('tab','#especialidades');
        return redirect()->route('manage.cursos.ficha',$curso->id);
    }

    public function getFirmas(Request $request, $id)
    {
        ini_set('memory_limit', '400M');
        set_time_limit(60000);

        $curso = $this->curso->find($id);

        $user = $request->user();
        $p = $user->plataforma ?: ConfigHelper::config('propietario');

        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id')->toArray();
        $bookings = \VCN\Models\Bookings\Booking::where('curso_id', $id)->whereIn('status_id', $stplazas)->where('plataforma', $p);

        $any = Carbon::now()->year;

        $i = 0;
        $j = 0;
        $list = "";
        foreach($bookings->get() as $booking)
        {
            if($booking->any != $any)
            {
                continue;
            }

            // $job = new \VCN\Jobs\JobFirma($b, 'booking_firma_pendiente');
            // dispatch($job);
            
            BookingLog::addLog($booking, "booking_firma_pendiente");
            
            $pdf = $booking->getArchivoFirmaPendiente();
            // $pdf = null;
            if(!$pdf)
            {
                $booking->pdf(false, true, true);
            }

            foreach($booking->pdfs as $p)
            {
                $p->firmas = null;
                // if(!$p->es_firmado)
                // {
                //     $p->firmas = null;
                // }
                
                $p->visible = false;
                $p->save();
            }

            $pdf = $booking->pdfs->sortByDesc('id')->first();
            $pdf->firmas = 'pendiente';
            $pdf->visible = 1;
            $pdf->save();

            $j += MailHelper::mailBookingFirma($booking, true);

            $list .= "$booking->id,";

            $i++;

            usleep(200000);
        }

        Session::flash('mensaje-ok',"Firma solicitada a: $i Bookings [$list]");
        return redirect()->route('manage.cursos.ficha',$id);
    }
}
