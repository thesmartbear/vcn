<?php

namespace VCN\Http\Controllers\Manage\Cursos;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Cursos\CursoExtraRepository as Extra;
use VCN\Models\Cursos\Curso;
use VCN\Models\Bookings\BookingExtra;

use VCN\Models\Extras\ExtraUnidad;
use VCN\Models\Monedas\Moneda;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Input;
use Session;

class CursoExtrasController extends Controller
{
    private $extra;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Extra $extra )
    {
        // $this->checkPermisos('proveedores');
        $this->checkPermisos('extras');

        $this->extra = $extra;
    }

    public function getIndex($curso_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->extra->all();
            if($curso_id>0)
            {
                $col = Curso::find($curso_id)->extras->sortBy('course_extras_name');
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cursos.extras.ficha',[$model->id]) ."'>$model->course_extras_name</a>";
                })
                ->addColumn('precio', function($model) {
                    $ret = $model->course_extras_currency_id?Moneda::find($model->course_extras_currency_id)->currency_name:"?";
                    $ret .= " ".$model->course_extras_price;
                    return $ret;
                })
                ->addColumn('unidad', function($model) {
                    $ret = ConfigHelper::getTipoUnidad($model->course_extras_unit);

                    if($model->course_extras_unit)
                    {
                        $ret .= " (". ($model->course_extras_unit_id?$model->tipo->name:'-') .")";
                    }

                    return $ret;

                })
                ->addColumn('curso', function($model) {
                    return $model->curso?$model->curso->course_name:"-";
                })
                ->addColumn('requerido', function($model) {
                    return $model->course_extras_required?"<span class='label label-success'><i class='fa fa-check-circle fa-1x'></i></span>":"<span class='label label-danger'><i class='fa fa-times-circle fa-1x'></i></span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";

                    $nExtras = BookingExtra::where('tipo',0)->where('extra_id',$model->id)->count();
                    $txtExtra = "";
                    if($nExtras>0)
                    {
                        $txtExtra = "<br>(Este extra está en $nExtras Bookings. ¿Estás seguro de querer suprimirlo?)";
                    }

                    $data = " data-label='Borrar' data-model='Extra Curso. $txtExtra' data-action='". route( 'manage.cursos.extras.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= "<a href='". route('manage.alojamientos.precios.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Precio</a>";
                    // $ret .= " <a href='". route('manage.alojamientos.cuotas.nuevo',$model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Cuota</a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.cursos.extras.index', compact('curso_id'));
    }

    public function getNuevo($curso_id=0)
    {
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();
        $unidades = ['' => ''] + ExtraUnidad::pluck('name','id')->toArray();
        $unidades_tipo = ConfigHelper::getTipoUnidad();

        $curso = 0;
        if($curso_id)
        {
            $curso = Curso::find($curso_id);
        }

        $cursos = Curso::pluck('course_name','id');

        return view('manage.cursos.extras.new', compact('curso','cursos','monedas','unidades','unidades_tipo'));
    }

    public function getUpdate($id)
    {
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();
        $unidades = ['' => ''] + ExtraUnidad::pluck('name','id')->toArray();
        $unidades_tipo = ConfigHelper::getTipoUnidad();

        $ficha = $this->extra->find($id);

        return view('manage.cursos.extras.ficha', compact('ficha','monedas','unidades','unidades_tipo'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'course_extras_name' => 'required',
            'course_extras_price' => 'required',
            'course_extras_unit'  => 'required',
            'course_extras_unit_id'  => 'required_if:course_extras_unit,1',
            'course_extras_currency_id'    => 'required',
        ]);

        $data = Input::except('_token');

        $data['course_extras_required'] = Input::has('course_extras_required');
        $data['course_extras_unit_id'] = $request->get('course_extras_unit_id') ?: 0;

        if(!$id)
        {
            //nuevo
            $o = $this->extra->create($data);
            $curso_id = $o->course_id;
        }
        else
        {
            $this->extra->update($data, $id);

            $curso_id = $this->extra->find($id)->course_id;
        }

        Session::flash('tab','#extras');


        return redirect()->route('manage.cursos.ficha',$curso_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $curso_id = $this->extra->find($id)->course_id;

        $this->extra->delete($id);

        Session::flash('tab','#extras');

        return redirect()->route('manage.cursos.ficha',$curso_id);
    }
}
