<?php

namespace VCN\Http\Controllers\Manage\Reuniones;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Reuniones\Reunion;
use VCN\Models\Reuniones\ReunionLog;

use VCN\Models\Bookings\Booking;
use VCN\Models\Convocatorias\Abierta;
use VCN\Models\Convocatorias\Cerrada;
use VCN\Models\Convocatorias\ConvocatoriaMulti;
use VCN\Models\Cursos\Curso;

use Datatable;
use Session;
use Carbon;

class ReunionesController extends Controller
{
    public function __construct()
    {
        // $this->checkPermisos('reuniones');
    }

    public function getIndexByCurso($curso_id)
    {
        if(Datatable::shouldHandle())
        {
            $col = Reunion::where('curso_id',$curso_id)->get();

            return Datatable::collection( $col )
                ->addColumn('oficina', function($model) {
                    return $model->oficina->name;
                })
                ->addColumn('fecha', function($model) {
                    $fecha = $model->fecha->format('Y-m-d');
                    return $fecha;
                })
                ->addColumn('lugar', function($model) {
                    return "<a href='". route('manage.reuniones.ficha',[$model->id]) ."'>$model->lugar</a>";
                })
                ->showColumns('hora','notas')
                ->addColumn('options', function($model) {

                    $ret = "";

                    $ret .= " <a data-label='Enviar cambios' href='". route('manage.reuniones.cambios',[$model->id]) ."'><i class='fa fa-share-square'></i></a>";

                    $data = " data-label='Borrar' data-model='Reunión' data-action='". route( 'manage.reuniones.delete', $model->id) . "'";
                    $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('oficina')
                ->orderColumns('oficina')
                ->setAliasMapping()
                ->make();
        }
    }

    public function getIndexByConvocatoria($convocatoria_id,$tipo)
    {
        if(Datatable::shouldHandle())
        {
            $col = Reunion::where('convocatoria_tipo',$tipo)->where('convocatoria_id',$convocatoria_id)->get();

            return Datatable::collection( $col )
                ->addColumn('oficina', function($model) {
                    return $model->oficina->name;
                })
                ->addColumn('fecha', function($model) {
                    return $model->fecha->format('Y-m-d');
                })
                ->addColumn('lugar', function($model) {
                    return "<a href='". route('manage.reuniones.ficha',[$model->id]) ."'>$model->lugar</a>";
                })
                ->showColumns('hora','notas')
                ->addColumn('options', function($model) {

                    $ret = "";

                    $ret .= " <a data-label='Enviar cambios' href='". route('manage.reuniones.cambios',[$model->id]) ."'><i class='fa fa-share-square'></i></a>";

                    $data = " data-label='Borrar' data-model='Reunión' data-action='". route( 'manage.reuniones.delete', $model->id) . "'";
                    $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('oficina')
                ->orderColumns('oficina')
                ->setAliasMapping()
                ->make();
        }
    }

    public function getUpdate(Request $request, $id)
    {
        $ficha = Reunion::find($id);

        return view('manage.reuniones.ficha', compact('ficha') );
    }

    public function postUpdateCurso(Request $request, $id=0)
    {
        Session::flash('tab','#reuniones');

        $reunion = Reunion::find($id);
        if($reunion)
        {
            $data = $request->except('_token');
            $data['fecha'] = Carbon::createFromFormat('d/m/Y', $data['fecha']);
            $reunion->update($data);

            return redirect()->route('manage.cursos.ficha', $reunion->curso_id);
        }

        $this->validate($request, [
            'oficinas' => 'required',
            'fecha' => 'required',
            'hora' => 'required',
            'lugar' => 'required',
        ]);

        $oficinas = $request->get('oficinas');

        $curso_id = $request->input('curso_id');

        foreach($oficinas as $o)
        {
            $reunion = new Reunion;

            $reunion->oficina_id = $o;
            $reunion->curso_id = $curso_id;
            $reunion->fecha = Carbon::createFromFormat('d/m/Y',$request->input('fecha'));
            $reunion->hora = $request->input('hora');
            $reunion->lugar = $request->input('lugar');
            $reunion->notas = $request->input('notas');

            $reunion->save();
        }

        return redirect()->back();
    }

    public function postUpdateConvocatoria(Request $request, $id=0)
    {
        Session::flash('tab','#reuniones');

        $reunion = Reunion::find($id);
        if($reunion)
        {
            $data = $request->except('_token');
            $data['fecha'] = Carbon::createFromFormat('d/m/Y', $data['fecha']);
            $reunion->update($data);
            
            switch($reunion->convocatoria_tipo)
            {
                case 1:
                    return redirect()->route('manage.convocatorias.cerradas.ficha', $reunion->convocatoria_id);
                break;

                case 2:
                    return redirect()->route('manage.convocatorias.abiertas.ficha', $reunion->convocatoria_id);
                break;

                case 3:
                    return redirect()->route('manage.convocatorias.multis.ficha', $reunion->convocatoria_id);
                break;
            }

            return redirect()->back();
        }

        $this->validate($request, [
            'oficinas' => 'required',
            'fecha' => 'required',
            'hora' => 'required',
            'lugar' => 'required',
        ]);

        $oficinas = $request->get('oficinas');

        $convocatoria_tipo = $request->input('convocatoria_tipo');
        $convocatoria_id = $request->input('convocatoria_id');

        foreach($oficinas as $o)
        {
            $reunion = new Reunion;

            $reunion->oficina_id = $o;
            $reunion->convocatoria_tipo = $convocatoria_tipo;
            $reunion->convocatoria_id = $convocatoria_id;
            $reunion->fecha = Carbon::createFromFormat('d/m/Y',$request->input('fecha'));
            $reunion->hora = $request->input('hora');
            $reunion->lugar = $request->input('lugar');
            $reunion->notas = $request->input('notas');

            $reunion->save();
        }

        return redirect()->back();

    }

    public function delete(Request $request, $id)
    {
        Session::flash('tab','#reuniones');

        Reunion::find($id)->delete();

        return redirect()->back();
    }

    public function getEnviar(Request $request, $convocatoria_id, $convocatoria_tipo, $todos=0)
    {
        Session::flash('tab','#reuniones');

        $convocatoria = null;
        switch($convocatoria_tipo)
        {
            case 1: //cerrada
            {
                $convocatoria = Cerrada::find($convocatoria_id);
            }
            break;

            case 2: //abierta
            {
                $convocatoria = Abierta::find($convocatoria_id);
            }
            break;

            case 3: //multi
            {
                $convocatoria = ConvocatoriaMulti::find($convocatoria_id);
            }
            break;
        }


        if(!$convocatoria)
        {
            Session::flash('mensaje-alert',"Error");
            return redirect()->back();
        }


        if($todos)
        {
            //Reenviar
            $bookings = $convocatoria->bookings_completos_futuro;
        }
        else
        {
            $bookings = $convocatoria->bookings_completos_futuro->where('mail_reunion',0);
        }

        $i = 0;
        $j = 0;
        foreach($bookings as $booking)
        {
            // if($booking->es_pasado)
            // {
            //     continue;
            // }

            $r = $booking->mailReunion();
            $i += $r;
            if($r>0)
            {
                $j++;

                ReunionLog::add($booking);
            }
        }

        Session::flash('mensaje-ok',"Bookings avisados: $j, Emails: $i");

        return redirect()->back();
    }

    public function getCambios(Request $request, $id)
    {
        Session::flash('tab','#reuniones');

        $reunion = Reunion::find($id);

        if($reunion->curso_id)
        {
            $curso = Curso::find($reunion->curso_id);
            $bookings = $curso->bookings_completos_futuro;
        }
        elseif($reunion->convocatoria_id)
        {
            $convocatoria = $reunion->convocatoria;
            $bookings = $convocatoria->bookings_completos_futuro;
        }

        $i = 0;
        $j = 0;
        foreach($bookings as $booking)
        {
            if(!$booking->reunion)
            {
                continue;
            }

            // if($booking->es_pasado)
            // {
            //     continue;
            // }
            
            foreach($booking->reuniones as $reunion)
            {
                if($reunion->id == $id)
                {
                    $r = $booking->mailReunion(1);
                    $i += $r;
                    if($r>0)
                    {
                        $j++;

                        ReunionLog::add($booking, $reunion->id);
                    }

                    break;
                }
            }
        }

        Session::flash('mensaje-ok',"Bookings avisados (cambio): $j, Emails: $i");

        $ficha = $reunion;
        return view('manage.reuniones.ficha', compact('ficha') );

        // return redirect()->back();
    }

    public function getEnviarCurso(Request $request, $curso_id, $todos=0)
    {
        Session::flash('tab','#reuniones');

        $curso = Curso::find($curso_id);

        if(!$curso)
        {
            Session::flash('mensaje-alert',"Error");
            return redirect()->back();
        }

        if($todos)
        {
            //Reenviar
            $bookings = $curso->bookings_completos_futuro;
        }
        else
        {
            $bookings = $curso->bookings_completos_futuro->where('mail_reunion',0);
        }

        $i = 0;
        $j = 0;
        foreach($bookings as $booking)
        {
            if(!$booking->reunion)
            {
                continue;
            }

            // if($booking->es_pasado)
            // {
            //     continue;
            // }

            $r = $booking->mailReunion($todos);
            $i += $r;
            if($r>0)
            {
                $j++;

                ReunionLog::add($booking);
            }
        }

        Session::flash('mensaje-ok',"Bookings avisados: $j, Emails: $i");

        return redirect()->back();
    }

}
