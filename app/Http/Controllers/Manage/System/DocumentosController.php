<?php

namespace VCN\Http\Controllers\Manage\System;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\System\Documento;
use VCN\Models\System\DocEspecifico;
use VCN\Models\System\DocEspecificoLang;

use Carbon;
use Session;
use File;
use Datatable;
use ConfigHelper;

class DocumentosController extends Controller
{
    /**
     * Instantiate a new DocumentosController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');
    }

    public function getIndex(Request $request, $modelo, $modelo_id, $tipo=0)
    {
        if($tipo==1)
        {
            $dir = "/assets/uploads/doc_especificos/";
        }
        else
        {
            $dir = "/assets/uploads/documentos/";
        }

        if(Datatable::shouldHandle())
        {
            if($tipo == 1)
            {
                $list = DocEspecifico::where('modelo', $modelo)->where('modelo_id', $modelo_id)->get();
            }
            else
            {
                $list = Documento::where('modelo', $modelo)->where('modelo_id', $modelo_id)->get();
            }

            return Datatable::collection( $list )
                ->showColumns('idioma','notas')
                ->addColumn('name', function($model) use ($tipo) {
                    
                    if($tipo == 1)
                    {
                        return "<a href='". route('manage.system.docs.ficha', $model->id) ."'>". $model->name ."</a>";
                    }

                    return $model->name;
                })
                ->addColumn('langs', function($model) use ($tipo) {
                    return $model->langs->count();
                })
                ->addColumn('documento', function($model) use ($dir, $tipo) {
                    
                    $ret = "Ninguno";
                    if($model->doc)
                    {
                        $idioma = 0;
                        $dir = $dir . $model->modelo ."/". $model->modelo_id . "/$idioma/";
                        $ret = $model->doc;
                        $ret = ConfigHelper::iframe($model->doc, public_path($dir), $dir);

                        // $dir = $dir. $model->doc;
                        // $ret .= "<a target='_blank' href='$dir'><i class='fa fa-eye pull-right'></i></a>";
                    }

                    return $ret;
                })
                ->addColumn('options', function($model) use ($tipo) {
                    $ret = "";
                    $data = " data-label='Borrar' data-model='Documento específico' data-action='". route( 'manage.system.docs.delete', [$model->id, $tipo]) . "'";
                    $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name','modelo')
                ->orderColumns('name','modelo')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.system.documentos.index');
    }

    public function getUpdate(Request $request, $doc_id)
    {
        $ficha = DocEspecifico::find($doc_id);

        return view('manage.system.documentos.ficha', compact('ficha'));
    }

    public function postUpdateLangs(Request $request, $doc_id)
    {
        foreach(ConfigHelper::idiomas() as $key=>$idioma)
        {
            $d = DocEspecificoLang::where('doc_id', $doc_id)->where('idioma', $idioma)->first();
            if(!$d)
            {
                $d = new DocEspecificoLang;
                $d->doc_id = $doc_id;
                $d->idioma = $idioma;                
            }

            $d->name = $request->get("name_$idioma");
            $d->notas = $request->get("notas_$idioma");
            $d->save();

            $f = "doc_$idioma";
            if($request->hasFile($f))
            {
                $dir = public_path("assets/uploads/doc_especificos/". $d->parent->modelo ."/". $d->parent->modelo_id ."/$idioma/");

                $file = $request->file($f);
                $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file = str_slug($file_name) .".". $file->getClientOriginalExtension();

                $request->file($f)->move($dir, $file);
                
                $d->doc = $file;
                $d->save();
            }

            if($request->has($f."_delete"))
            {
                if( file_exists( public_path($d->doc)) )
                {
                    unlink(public_path($d->doc));
                }

                $d->doc = null;
                $d->save();
            }
        }

        return redirect()->back();
    }

    public function postUpdate(Request $request, $doc_id)
    {
        $user = $request->user();

        $tipo = $request->get('tipo');

        if($doc_id)
        {
            if($tipo==1)
            {
                $ficha = DocEspecifico::find($doc_id);
            }
            else
            {
                $ficha = Documento::find($doc_id);
            }
        }
        else
        {
            $ficha = new Documento;
            if($tipo==1)
            {
                $ficha = new DocEspecifico;
            }
        }

        $modelo = $request->input('modelo');
        $modelo_id = $request->input('modelo_id');
        $idioma = 0;

        $ficha->name = $request->input('doc_name');
        $ficha->idioma = $idioma;
        $ficha->modelo = $modelo;
        $ficha->modelo_id = $modelo_id;
        $ficha->notas = $request->input('doc_notas');

        if($request->hasFile('doc_adjunto'))
        {
            $dir = public_path("assets/uploads/doc_especificos/". $modelo ."/". $modelo_id ."/$idioma/");

            $file = $request->file('doc_adjunto');
            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file = str_slug($file_name) .".". $file->getClientOriginalExtension();

            $request->file('doc_adjunto')->move($dir, $file);
            $ficha->doc = $file;
        }

        if($request->has('doc_adjunto_delete'))
        {
            if( file_exists( public_path($ficha->doc)) )
            {
                unlink(public_path($ficha->doc));
            }

            $ficha->doc = null;
            $ficha->save();
        }

        $ficha->save();

        Session::flash('tab', (($tipo==1)?'#doc_especificos':'#docs'));
        return redirect()->back();
    } 

    public function postUpload(Request $request, $modelo, $modelo_id)
    {
        $idioma = strtolower($request->input('idioma'));
        $dir = public_path("assets/uploads/documentos/$idioma/". $modelo ."/". $modelo_id ."/");

        $this->validate($request, [
            // 'file' => 'image30000',
        ]);

        $file = "";
        if( $request->hasFile('file') )
        {
            $file = $request->file('file');
            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file = str_slug($file_name) .".". $file->getClientOriginalExtension();

            // $file = Input::file('file')->getClientOriginalName();
            $request->file('file')->move($dir, $file);
        }

        $doc = new Documento;
        $doc->modelo = $modelo;
        $doc->modelo_id = $modelo_id;
        $doc->doc = $file;
        $doc->user_id = $request->user()->id;
        $doc->idioma = $idioma;
        $doc->save();

        return response()->json('success', 200);
    }

    public function postDelete(Request $request, $doc_id)
    {
        $doc = Documento::find($doc_id);

        $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");

        $file = $request->input('f');
        File::delete($dir.$file);

        $doc->delete();

        Session::flash('tab','#archivos');

        return response()->json('success', 200);
    }

    public function destroy($doc_id, $tipo=0)
    {
        if($tipo==1)
        {
            $doc = DocEspecifico::find($doc_id);
            $dir = public_path("assets/uploads/doc_especificos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
        }
        else
        {
            $doc = Documento::find($doc_id);
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
        }
        
        File::delete($dir.$doc->doc);

        $doc->delete();

        Session::flash('tab', (($tipo==1)?'#doc_especificos':'#docs'));

        return redirect()->back();
    }

    public function setVisible($doc_id)
    {
        $doc = Documento::find($doc_id);
        $doc->visible = !$doc->visible;
        $doc->save();

        Session::flash('tab','#docs');

        return redirect()->back();
    }

    public function setPlataforma(Request $request, $doc_id)
    {
        $doc = Documento::find($doc_id);
        $doc->plataforma = $request->input('plataforma');
        $doc->save();

        Session::flash('tab','#docs');

        return redirect()->back();
    }
}
