<?php

namespace VCN\Http\Controllers\Manage\System;

use Illuminate\Http\Request;
use VCN\Http\Controllers\Controller;

use VCN\Models\System\SystemMail;
use VCN\Models\System\SystemMailIdioma;

use Datatable;
use Illuminate\Support\Facades\Storage;
use VCN\Helpers\ConfigHelper;
use VCN\Helpers\MailHelper;
use File;
use Session;
use Illuminate\Mail\Markdown;
use VCN\Models\User;
use VCN\Models\Bookings\Booking;

class SystemMailController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->checkPermisosFullAdmin();
    }

    /**
     * @param Request $request
     */
    public function getIndex(Request $request)
    {
        if (Datatable::shouldHandle()) {
            $list = SystemMail::all()->sortBy('id');

            return Datatable::collection($list)
                ->addColumn('name', function ($model) {
                    return "<a href='" . route('manage.system.emails.ficha', [$model->id]) . "'>$model->name</a>";
                })
                ->showColumns('trigger', 'destino_notas')
                ->addColumn('tipo', function ($model) {
                    return $model->destino_tipo;
                })
                ->addColumn('idiomas', function ($model) {

                    return $model->idiomas->count() ? "SI" : "NO";

                    /*try {
                        $ret = "";

                        if ($model->idiomas->count()) {
                            foreach ($model->idiomas as $i) {
                                $ret .= $i->idioma . ":" . ($i->contenido ? "OK" : "-");
                            }

                            return $ret;
                        }

                        return ($model->contenido ? "OK" : "-");
                    }
                    catch(\Exception $e)
                    {
                        return $model->id;
                    }*/
                })
                ->addColumn('options', function ($model) {

                    $ret = "";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.system.emails.index');
    }

    public function getUpdate($id)
    {
        $ficha = SystemMail::findOrFail($id);

        return view('manage.system.emails.ficha', compact('ficha'));
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function postUpdate(Request $request, $id)
    {
        $ficha = SystemMail::find($id);
        $template = "emails." . $ficha->template;

        $lang = null;

        $idioma = $request->get('idioma', null);
        $contenido = $request->get('contenido');
        $asunto = $request->get('asunto');

        if ($idioma) {
            $lang = $ficha->getLang($idioma);
            $template = "emails." . $idioma . "." . $ficha->template;
            $contenido = $request->get("contenido_$idioma");
            $asunto = $request->get("asunto_$idioma");

            Session::flash('tab', "#idiomas-$idioma");
        }

        //bug wysiwyg con ->
        $contenido = str_replace('&gt;', '>', $contenido);

        $ficha = $lang ?: $ficha;
        $ficha->asunto = $asunto;
        $ficha->contenido = $contenido;
        $ficha->save();

        return redirect()->route('manage.system.emails.ficha', $id);
    }

    private function getBlade($id, $idioma, $booking = null)
    {
        $ficha = SystemMail::find($id);
        //$template = "emails.". $ficha->template;

        $blade = "emails.layout_markdown";

        $idioma = $booking ? $booking->idioma_contacto : ($idioma ?: "ES");
        if ($idioma) {
            $lang = $ficha->getLang($idioma);
            //$template = "emails.". $idioma .".". $ficha->template;
        }

        $ficha = $lang ?: $ficha;

        $args['ficha'] = $booking;
        MailHelper::enviarCompile($ficha, $args);

        $markdown = new Markdown(view(), config('mail.markdown'));
        return $markdown->render($blade, $args);
    }

    public function getRender(Request $request, $id, $idioma = null)
    {
        return $this->getBlade($id, $idioma, null);
    }

    public function getPreview(Request $request, $id, $booking = null)
    {
        $p = ConfigHelper::config('propietario');
        $booking = $booking ?: Booking::where('plataforma', $p)->first();

        $ficha = SystemMail::find($id);
        if ($ficha->name == "system.password") {
            $booking = $request->user();
        }

        //test email
        //$destino = $booking->viajero;
        //MailHelper::mailAvisoDocEnviar(139, 1, 'es', $destino);
        //$booking = Booking::find(6053);

        return $this->getBlade($id, null, $booking);
    }


    public function getTest(Request $request, $id, $idioma = null)
    {
        //pruebas

        $ficha = SystemMail::find($id);

        $name = $ficha->name;
        $destino = User::find(1);
        $data = null;

        $booking = \VCN\Models\Bookings\Booking::find(5355);
        $asunto = "Test - $name - " . MailHelper::replaceTags($ficha->asunto, $booking);
        //dd($asunto);
        $contenido = MailHelper::replaceTags($ficha->contenido, $booking);
        //dd($contenido);

        $lang = null;

        if ($idioma) {
            $lang = $ficha->getLang($idioma);
        }

        $ficha = $lang ?: $ficha;

        $blade = "emails.layout_markdown";

        $args['asunto'] = $ficha->asunto;
        //$args['body'] = $ficha->contenido;

        $vars['nombre'] = "Nombre";
        $vars['telefono'] = "Telefono";
        $vars['email'] = "Email";
        $vars['viajar'] = "Viajar";
        $vars['fechanac'] = "FechaNac";
        $vars['ciudad'] = "Ciudad";
        $vars['curso_id'] = 1;
        $vars['curso'] = "Curso";
        $vars['curso_link'] = "Curso link";
        $vars['idioma'] = "ES";
        $vars['array1'] = ['es', 'ca'];
        $args['args'] = $vars;

        $body = $ficha->contenido;
        $body = ConfigHelper::bladeCompile($body, $vars);

        $args['body'] = $body;

        //$markdown = new Markdown(view(), config('mail.markdown'));
        //return $markdown->render($blade, $args);

        MailHelper::enviar($name, $destino, $args);

        Session::flash('mensaje-ok', "Test: $asunto enviado");
        return redirect()->route('manage.system.emails.ficha', $id);
    }
}
