<?php

namespace VCN\Http\Controllers\Manage\System;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\System\Plataforma;
use VCN\Models\User;
use VCN\Models\System\Oficina;

use ConfigHelper;
use Datatable;
use Session;
use DB;

class PlataformasController extends Controller
{
    public function __construct()
    {
        $this->checkPermisosFullAdmin();
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            $list = Plataforma::all();

            return Datatable::collection( $list )
                ->showColumns('id')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.system.plataformas.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->showColumns('web','email','web_estructura','ganalytics')
                ->addColumn('tema', function($model) {
                    return $model->tema?ConfigHelper::config('temas')[$model->tema]:"-";
                })
                ->searchColumns('name','web','email')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.system.plataformas.index');
    }

    public function getUpdate(Request $request, $id)
    {
        $ficha = Plataforma::findOrFail($id);

        $st_booking = ['prereserva','prebooking','presalida','overbooking','archivado','cancelado','refund','vuelta','fuera','listo'];
        $st_solicitudes = ['lead','contactado','futuro','decidiendo','archivado','inscrito'];

        $status_booking = $ficha->status_booking;
        foreach($st_booking as $st)
        {
            $status_booking[$st] = isset($status_booking[$st])?$status_booking[$st]:0;
            $ficha->status_booking = $status_booking;
            $ficha->save();
        }

        $status_solicitud = $ficha->status_solicitud;
        foreach($st_solicitudes as $st)
        {
            $status_solicitud[$st] = isset($status_solicitud[$st])?$status_solicitud[$st]:0;
            $ficha->status_solicitud = $status_solicitud;
            $ficha->save();
        }

        $status_booking = [0=> "Archivado"] + \VCN\Models\Bookings\Status::orderBy('orden')->get()->pluck('name','id')->toArray();
        $status_solicitud = [0=> ""] + \VCN\Models\Solicitudes\Status::orderBy('orden')->get()->pluck('name','id')->toArray();
        $monedas = \VCN\Models\Monedas\Moneda::pluck('currency_name','currency_name')->toArray();
        $asignados = User::asignados()->get()->pluck('full_name', 'id')->toArray();
        $oficinas = Oficina::plataforma()->pluck('name', 'id')->toArray();

        return view('manage.system.plataformas.ficha', compact('ficha', 'status_booking', 'status_solicitud','monedas','asignados','oficinas'));
    }

    public function postUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'avisos' => 'required',
            'status_booking' => 'required',
            'status_solicitud' => 'required',
        ]);

        $data = $request->all();

        $data['area'] = $request->has('area');
        $data['facturas'] = $request->has('facturas');
        $data['seguro'] = $request->has('seguro');
        $data['idiomas'] = $request->get('idiomas')?implode(',',$request->get('idiomas')):null;
        $data['nombre'] = $data['name'];
        $data['tpv_activo'] = $request->has('tpv_activo');
        $data['tpv_activo_banco'] = $request->has('tpv_activo_banco');
        $data['tpv_aeropuertos'] = $request->has('tpv_aeropuertos');
        $data['web_registro'] = $request->has('web_registro');
        $data['es_rgpd'] = $request->has('es_rgpd');
        $data['es_notificaciones'] = $request->has('es_notificaciones');

        $plataforma = Plataforma::findOrFail($id);
        $plataforma->update($data);

        $request->session()->flush();
        DB::table('sessions')->truncate();

        //Forzamos caché de configuración
        Session::put( 'vcn.tema', $plataforma->tema );
        Session::put( 'vcn.moneda', ConfigHelper::config("moneda") );
        $formato = ConfigHelper::config('moneda_format');
        $miles = ConfigHelper::config('moneda_miles');
        $decimales = ConfigHelper::config('moneda_decimales');
        Session::put('vcn.moneda_format',$formato);
        Session::put('vcn.moneda_miles',$miles);
        Session::put('vcn.moneda_decimales',$decimales);
        $moneda_locale = ConfigHelper::config("moneda_locale");
        setlocale(LC_MONETARY, $moneda_locale);

        Session::put( 'vcn.simplybook', $plataforma->simplybook );
        Session::put( 'vcn.chat_smartsupp', $plataforma->chat_smartsupp );
        
        // Session::flash();
        return redirect()->route('manage.system.plataformas.index');
    }
}
