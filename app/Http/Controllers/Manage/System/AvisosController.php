<?php

namespace VCN\Http\Controllers\Manage\System;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\System\Aviso;
use VCN\Models\System\AvisoDoc;
use VCN\Models\System\AvisoDocIdioma;
use VCN\Models\System\AvisoLog;

use VCN\Models\System\Oficina;
use VCN\Models\Prescriptores\Prescriptor;
use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Cursos\Curso;
use VCN\Models\Centros\Centro;
use VCN\Models\Categoria;
use VCN\Models\Subcategoria;
use VCN\Models\Bookings\Booking;
use VCN\Models\Solicitudes\Solicitud;
use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\Tutor;

use VCN\Models\Convocatorias\Cerrada;
use VCN\Models\Convocatorias\Abierta;
use VCN\Models\Convocatorias\ConvocatoriaMulti;

use VCN\Models\Leads\Origen;
use VCN\Models\Leads\Suborigen;
use VCN\Models\Leads\SuborigenDetalle;
use VCN\Models\Pais;

use ConfigHelper;
use Datatable;
use Carbon;
use Session;
use Illuminate\Mail\Markdown;
use VCN\Helpers\MailHelper;

use NZTim\Mailchimp\Mailchimp;

class AvisosController extends Controller
{

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("permiso.view:avisos_mailing", ['only' => ['getListado']]);
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            $filtro = ConfigHelper::config('propietario');
            if( $filtro && !auth()->user()->isFullAdmin() )
            {
                $col = Aviso::plataforma()->get();
            }
            else
            {
                $col = Aviso::all();
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.system.avisos.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('activo', function($model) {

                    $ret = $model->activo?"<a data-label='Desactivar' href='". route('manage.system.avisos.activo',[$model->id,0]). "' class='label label-success'><i class='fa fa-check-circle fa-1x'></i></a>":"<a data-label='Desactivar' href='".route('manage.system.avisos.activo',[$model->id,1])."' class='label label-danger'><i class='fa fa-times-circle fa-1x'></i></a>";

                    return $ret;
                })
                ->addColumn('tipo', function($model) {
                    return ConfigHelper::getAvisoTipo($model->tipo);
                })
                ->addColumn('cuando', function($model) {
                    return $model->cuando_detalles;
                    // return ConfigHelper::getAvisoCuandoTipo($model->cuando_tipo);
                })
                ->addColumn('doc', function($model) {

                    if($model->cuando_tipo=='tarea')
                    {
                        return $model->tarea_tipo . ": ". str_limit($model->tarea,25);
                    }


                    if($model->doc_id>=0)
                    {
                        if($model->doc_id>=10)
                        {
                            return $model->doc->name;
                        }

                        return ConfigHelper::getAvisoDoc($model->doc_id);
                    }

                    return "-";

                })
                ->addColumn('options', function($model) {

                    $ret = "";

                    $data = " data-label='Borrar' data-model='Aviso' data-action='". route( 'manage.system.avisos.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    if($model->filtros)
                    {
                        $ret .= " <a data-label='Listado simulación' href='". route('manage.system.avisos.simulacion',$model->id) ."'><i class='fa fa-list-alt'></i></a>";
                    }
                    else
                    {

                    }

                    return $ret;
                })

                ->searchColumns('name')
                ->orderColumns('created_at','name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.system.avisos.index');
    }

    public function getNuevo($tipo=1)
    {
        $user = auth()->user();
        
        if( auth()->user()->isFullAdmin() )
        {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0=>'Todas'] + Oficina::all()->sortBy('name')->pluck('name','id')->toArray();
            $prescriptores = [0=>'Todos'] + Prescriptor::all()->sortBy('name')->pluck('name','id')->toArray();

            $proveedores = [0=>'Todos'] + Proveedor::all()->sortBy('name')->pluck('name','id')->toArray();
            $centros = [0=>'Todos'] + Centro::all()->sortBy('name')->pluck('name','id')->toArray();
            $cursos = [0=>'Todos'] + Curso::all()->sortBy('name')->pluck('name','id')->toArray();
            $categorias = [0=>'Todas'] + Categoria::all()->sortBy('name')->pluck('name','id')->toArray();
            $subcategorias = [0=>'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name','id')->toArray();

            $valores['plataformas'] = 0;
        }
        else
        {
            $plataformas = [ $user->plataforma =>ConfigHelper::plataformaApp() ];

            $oficinas = [0=>'Todas'] + Oficina::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $prescriptores = [0=>'Todos'] + Prescriptor::plataforma()->sortBy('name')->pluck('name','id')->toArray();

            $proveedores = [0=>'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $centros = [0=>'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $cursos = [0=>'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $categorias = [0=>'Todos'] + Categoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $subcategorias = [0=>'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();
        }

        $convocatorias = [0=>'Todas'];
        $paisesm = [0 => 'Todos'] + Pais::all()->sortBy('name')->pluck('name', 'id')->toArray();

        $valores['oficinas'] = 0;
        $valores['prescriptores'] = 0;
        $valores['proveedores'] = 0;
        $valores['centros'] = 0;
        $valores['cursos'] = 0;
        $valores['categorias'] = 0;
        $valores['subcategorias'] = 0;
        $valores['tipoc'] = 0;
        $valores['status'] = 0;
        $valores['convocatorias'] = 0;
        $valores['directos'] = 0;
        $valores['any'] = Carbon::now()->year;
        $valores['paises'] = 0;

        $valoresNot['prescriptores'] = "";
        $valoresNot['cursos'] = 0;
        $valoresNot['paises'] = 0;

        $documentos = ConfigHelper::getAvisoDoc();
        $documentos += AvisoDoc::where('activo',1)->pluck('name','id')->toArray();

        $statuses = \VCN\Models\Bookings\Status::orderBy('orden')->pluck('name','id');

        $anys = [0=> "No"] + \VCN\Models\Informes\Venta::groupBy('any')->get()->pluck('any','any')->toArray();
        $anys[end($anys)+1] = end($anys)+1;

        if($tipo==0) //Bookings
        {
            return view('manage.system.avisos.new', 
                compact('tipo','documentos','anys','valores','valoresNot','categorias','subcategorias','plataformas','oficinas','prescriptores','proveedores','centros','cursos','statuses','convocatorias','paisesm'));
        }
        //pendiente: leads, documentos
    }


    public function getUpdate($id)
    {
        $ficha = Aviso::find($id);
        $user = auth()->user();

        if( auth()->user()->isFullAdmin() )
        {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0=>'Todas'] + Oficina::all()->sortBy('name')->pluck('name','id')->toArray();
            $prescriptores = [0=>'Todos'] + Prescriptor::all()->sortBy('name')->pluck('name','id')->toArray();

            $proveedores = [0=>'Todos'] + Proveedor::all()->sortBy('name')->pluck('name','id')->toArray();
            $centros = [0=>'Todos'] + Centro::all()->sortBy('name')->pluck('name','id')->toArray();
            $cursos = [0=>'Todos'] + Curso::all()->sortBy('name')->pluck('name','id')->toArray();
            $categorias = [0=>'Todas'] + Categoria::all()->sortBy('name')->pluck('name','id')->toArray();
            $subcategorias = [0=>'Todas'] + Subcategoria::all()->sortBy('name')->pluck('name','id')->toArray();
        }
        else
        {
            $plataformas = [ $user->plataforma => ConfigHelper::plataformaApp() ];

            $oficinas = [0=>'Todas'] + Oficina::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $prescriptores = [0=>'Todos'] + Prescriptor::plataforma()->sortBy('name')->pluck('name','id')->toArray();

            $proveedores = [0=>'Todos'] + Proveedor::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $centros = [0=>'Todos'] + Centro::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $cursos = [0=>'Todos'] + Curso::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $categorias = [0=>'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $subcategorias = [0=>'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
        }

        $convocatorias = [0=>'Todas'];

        $tipo = $ficha->tipo;
        $valores = $ficha->filtros;
        $valoresNot = $ficha->filtros_not;
        $paisesm = [0 => 'Todos'] + Pais::all()->sortBy('name')->pluck('name', 'id')->toArray();

        //Bug de filtros nuevos
        if(!isset($valores['convocatorias']))
        {
            $valores['convocatorias'] = 0;
        }

        if(!isset($valores['paises']))
        {
            $valores['paises'] = 0;
        }

        if(!isset($valores['subcategorias']))
        {
            $valores['subcategorias'] = 0;
        }

        if(!isset($valores['directos']))
        {
            $valores['directos'] = 0;
        }

        if(!isset($valores['any']))
        {
            $valores['any'] = Carbon::now()->year;
        }

        if(!isset($valoresNot['cursos']))
        {
            $valoresNot['cursos'] = 0;
        }

        if(!isset($valoresNot['paises']))
        {
            $valoresNot['paises'] = 0;
        }

        $tipoc = $valores['tipoc'];
        $valor = explode(',',$valores['cursos']);

        switch($tipoc)
        {
            case 1:
            case 2:
            case 5: //cerrada
            {
                $convocatorias = [0=>'Todas'] + Cerrada::whereIn('course_id',$valor)->get()->sortBy('name')->pluck('name','id')->toArray();
            }
            break;

            case 3: //abierta
            {
                $convocatorias = [0=>'Todas'] + Abierta::whereIn('course_id',$valor)->get()->sortBy('name')->pluck('name','id')->toArray();
            }
            break;

            case 4: //Multi
            {
                $convocatorias = [0=>'Todas'];
                $convocatorias += \VCN\Models\Convocatorias\ConvocatoriaMulti::whereRaw(
                   'find_in_set(?, cursos_id)',
                   [$valor]
                )->get()->sortBy('name')->pluck('name','id')->toArray();
            }
            break;
        }

        $documentos = ConfigHelper::getAvisoDoc();
        $documentos += AvisoDoc::where('activo',1)->pluck('name','id')->toArray();

        $statuses = \VCN\Models\Bookings\Status::orderBy('orden')->pluck('name','id');

        $anys = [0=> "No"] + \VCN\Models\Informes\Venta::groupBy('any')->get()->pluck('any','any')->toArray();
        $anys[end($anys)+1] = end($anys)+1;

        return view('manage.system.avisos.ficha', compact('ficha','tipo','valores','valoresNot', 'anys',
            'documentos','categorias','subcategorias','plataformas','oficinas','prescriptores',
            'proveedores','centros','cursos','statuses','convocatorias','paisesm'));
    }

    public function postUpdate(Request $request, $id)
    {
        $cuando_tipo = $request->get('cuando_tipo');
        if( $cuando_tipo != "tarea" && $cuando_tipo != "trigger" )
        {
            $this->validate($request, [
                'doc_id'      => 'required',
            ]);
        }

        $tipo = $request->tipo;

        if($tipo == 0) //Booking
        {
            self::postUpdateBooking($request,$id);
        }

        return redirect()->route('manage.system.avisos.index');
    }

    private function postUpdateBooking(Request $request, $id)
    {
        if(!$id)
        {
            $aviso = new Aviso;
        }
        else
        {
            $aviso = Aviso::find($id);
        }

        $cuando_tipo = $request->input('cuando_tipo');

        $p = $request->user()->plataforma;

        $plat = $request->has('plataformas')?$request->input('plataformas'):$p;
        if( $request->has('plataforma_check') )
        {
            $plat = ConfigHelper::config('propietario');
        }

        $cuando_tipo = $request->input('cuando_tipo');

        $aviso->name = $request->input('name');
        $aviso->tipo = 0;
        $aviso->activo = $request->has('activo');
        $aviso->plataforma = $plat;
        $aviso->cuando_tipo = $cuando_tipo;
        $aviso->doc_id = $request->input('doc_id') ?: "";

        switch($cuando_tipo)
        {
            case 'puntual':
            {
                $fecha = Carbon::createFromFormat('d/m/Y',$request->input('cuando_fecha'));
                $aviso->cuando_dia = $fecha->format('d');
                $aviso->cuando_mes = $fecha->format('m');
                $aviso->cuando_any = $fecha->format('Y');
                $aviso->cuando_hora = $request->input('cuando_hora')[$cuando_tipo];
            }
            break;

            case 'dia':
            {
                $aviso->cuando_dia = null;
                $aviso->cuando_mes = null;
                $aviso->cuando_any = null;

                $aviso->cuando_hora = $request->input('cuando_hora')[$cuando_tipo];
            }
            break;

            case 'semana':
            {
                $aviso->cuando_dia = $request->input('cuando_dia')[$cuando_tipo];
                $aviso->cuando_mes = null;
                $aviso->cuando_any = null;

                $aviso->cuando_hora = $request->input('cuando_hora')[$cuando_tipo];
            }
            break;

            case 'mes':
            {
                $aviso->cuando_dia = $request->input('cuando_dia')[$cuando_tipo];
                $aviso->cuando_mes = null;
                $aviso->cuando_any = null;

                $aviso->cuando_hora = $request->input('cuando_hora')[$cuando_tipo];
            }
            break;

            case 'trigger':
            {
                $aviso->cuando_dia = null;
                $aviso->cuando_mes = null;
                $aviso->cuando_any = null;
                $aviso->cuando_hora = null;

                $aviso->cuando_trigger = $request->input('cuando_trigger')[$cuando_tipo];
            }
            break;

            case 'tarea':
            {
                $aviso->cuando_dia = $request->input('cuando_dia')[$cuando_tipo];
                $aviso->cuando_mes = null;
                $aviso->cuando_any = null;
                $aviso->cuando_hora = $request->input('cuando_hora')[$cuando_tipo];

                $aviso->cuando_trigger = $request->input('cuando_trigger')[$cuando_tipo];
                $aviso->tarea = $request->input('tarea');
                $aviso->tarea_tipo = $request->input('tarea_tipo');
            }
            break;
        }

        $destino = $request->input('destino');
        $aviso->destino = $destino;
        if($destino=="email")
        {
            $aviso->destinos = $request->input('destinos');
        }

        $filtros['plataformas'] = "";
        $filtros['oficinas'] = "";
        $filtros['prescriptores'] = "";
        $filtros['proveedores'] = "";
        $filtros['centros'] = "";
        $filtros['cursos'] = "";
        $filtros['convocatorias'] = "";
        $filtros['categorias'] = "";
        $filtros['subcategorias'] = "";
        $filtros['tipoc'] = "";
        $filtros['status'] = "";
        $filtros['directos'] = "";
        $filtros['any'] = "";
        $filtros['paises'] = "";

        $filtrosNot['prescriptores'] = "";
        $filtrosNot['paises'] = "";
        $filtrosNot['cursos'] = "";

        if(!$request->has('filtros_bool'))
        {
            //Filtros
            $filtros['plataformas'] = intval($request->input('plataformas',0));
            // $filtros['oficinas'] = intval($request->input('oficinas',0));
            $filtros['proveedores'] = intval($request->input('proveedores',0));
            $filtros['centros'] = intval($request->input('centros',0));
            // $filtros['cursos'] = intval($request->input('cursos',0));
            // $filtros['convocatorias'] = intval($request->input('convocatorias',0));
            $filtros['categorias'] = intval($request->input('categorias',0));
            $filtros['subcategorias'] = intval($request->input('subcategorias',0));
            $filtros['tipoc'] = intval($request->input('tipoc',0));
            $filtros['prescriptores'] = intval($request->input('prescriptores',0));
            $filtros['directos'] = intval($request->input('directos',0));
            $filtros['any'] = $request->input('any', 0);

            //multi
            $filtros['cursos'] = $request->has('cursos') ? implode(',',$request->get('cursos',[])) : "";
            $filtros['oficinas'] = $request->has('oficinas') ? implode(',',$request->get('oficinas',[])) : "";
            $filtros['convocatorias'] = $request->has('convocatorias') ? implode(',',$request->get('convocatorias',[])) : "";
            $filtros['paises'] = $request->has('paises') ? implode(',',$request->get('paises',[])) : "";
            
            $filtros['status'] = $request->has('status')?implode(',',$request->get('status',[])):"";

            $filtrosNot['prescriptores'] = $request->has('not-prescriptores') ? implode(',',$request->get('not-prescriptores',[])) : "";
            $filtrosNot['paises'] = $request->has('not-paises') ? implode(',',$request->get('not-paises',[])) : "";
            $filtrosNot['cursos'] = $request->has('not-cursos') ? implode(',',$request->get('not-cursos',[])) : "";
        }

        if($filtros['cursos'] != 0)
        {
            $valor = $request->get('cursos',[]);
            $curso = Curso::whereIn('id',$valor)->first();

            $filtros['tipoc'] = $curso->convocatoria_tipo_num;
        }

        $aviso->filtros = $filtros;
        $aviso->filtros_not = $filtrosNot;

        $aviso->save();

        return;
    }

    public function getSimulacion($id,$nuevos=0)
    {
        $ficha = Aviso::find($id);

        // $bookings = Booking::bookingAvisos($ficha->filtros,$ficha->filtros_not);
        // dd($bookings->count());

        if(Datatable::shouldHandle())
        {
            if($ficha->tipo==0)
            {
                return self::getSimulacionBooking($id,$nuevos);
            }
        }

        return view('manage.system.avisos.simulacion', compact('ficha'));
    }

    private function getSimulacionBooking($id,$nuevos=0)
    {
        $ficha = Aviso::find($id);

        $bookings = Booking::bookingAvisos($ficha->filtros,$ficha->filtros_not);
        $destino = $ficha->destino;

        // dd($bookings->get()->where('id',5637));

        $filtro = null;
        if($ficha->doc && $ficha->doc->tipo==1)
        {
            $filtro = true;
            $pc = floatval($ficha->doc->notapago_porcentaje) /100;
        }

        if($ficha->doc && $ficha->doc->notapago_pagar_tipo==2)
        {
            $filtro = true;
        }

        
        if($ficha->doc_id == 3)
        {
            $todos = $bookings->take(200)->get();
        }
        else
        {
            $todos = $bookings->get();
        }

        $col = collect();
        foreach($todos as $booking)
        {
            if($nuevos)
            {
                if($booking->yaAviso($id))
                {
                    continue;
                }
            }

            if($filtro)
            {
                $importe30 = $booking->precio_sin_cancelacion;

                $pc = (float)$ficha->doc->notapago_porcentaje;
                $pc = floatval($pc) /100;

                $importe30 = $importe30 * $pc;
                $pagado = $booking->total_pagado;

                $bEnviar = false;
                if($ficha->doc->notapago_pagar_tipo==2)
                {
                    $bEnviar = $booking->saldo_pendiente>0;
                }
                else
                {
                    $bEnviar = ($pagado < $importe30);
                }

                if($bEnviar)
                {
                    $col = $col->push($booking);
                }
            }
            else
            {
                $col = $col->push($booking);
            }
        }

        $bookings = $col;

        return Datatable::collection( $bookings )
            ->addColumn('booking', function($model) {
                return $model->id;
            })
            ->addColumn('status', function($model) {
                return $model->status_name;
            })
            ->addColumn('fecha_reserva', function($model) {
                return $model->fecha_reserva?$model->fecha_reserva->format('Y-m-d'):"-";
            })
            ->addColumn('fecha_curso', function($model) {
                return $model->course_start_date;//$model->curso_start_date;
            })
            ->addColumn('convocatoria', function($model) {
                return $model->convocatoria?$model->convocatoria->name:"-";
            })
            ->addColumn('categoria', function($model) {
                return $model->curso->categoria?$model->curso->categoria->name:"-";
            })
            ->addColumn('viajero', function($model) {
                return $model->viajero->full_name;
            })
            ->addColumn('prescriptor', function($model) {
                return $model->prescriptor?$model->prescriptor->name:"-";
            })
            ->addColumn('plataforma', function($model) {
                return $model->plataforma_name;
            })
            ->addColumn('preview', function($model) use ($id) {
                $ret = " <a target='_blank' data-label='Vista previa' href='". route('manage.system.avisos.simulacion.preview',[$id,$model->id,]) ."'><i class='fa fa-eye'></i></a>";

                return $ret;
            })
            ->addColumn('envios', function($model) use ($destino) {
                return $model->getEmailsCount($destino);
            })
            ->searchColumns('booking','viajero','prescriptor')
            ->orderColumns('fecha_reserva','booking','fecha_curso','convocatoria','categoria','viajero','prescriptor')
            ->setAliasMapping()
            ->setSearchStrip()->setOrderStrip()
            ->make();
    }

    public function getSimulacionPreview(Request $request, $aviso_id, $booking_id)
    {
        $aviso = Aviso::find($aviso_id);
        $booking = Booking::find($booking_id);

        $idioma = $booking->viajero->idioma_contacto;

        $tipo_doc = 0;
        $doc = null;
        if($aviso->doc)
        {
            /*$tipo_doc = $aviso->doc->tipo;
            $template = "emails.avisodoc";
            if($aviso->doc->tipo == 1)
            {
                $template = "emails.$idioma.booking_notapago";
            }*/

            return $this->getDocsPreview($request, $aviso->doc_id, $booking);
        }
        else
        {
            if($aviso->doc_id == 2)
            {
                $idioma = $booking->viajero->idioma_contacto;
                $template = "emails.$idioma.booking_pasaporte";
                return view( $template, compact('booking') );
            }
            elseif($aviso->doc_id == 3)
            {
                $idioma = $booking->viajero->idioma_contacto;
                $plataforma = \VCN\Models\System\Plataforma::find($booking->plataforma);
                
                $ficha = $booking->viajero;

                $asunto_p1_es = "Fue bonito mientras duró...";
                $asunto_p1_ca = "Seguim en contacte?";

                $asunto_p2_es = "¿Seguimos en contacto?.";
                $asunto_p2_ca = "Seguim en contacte?";
                
                $asunto_booking_es = "IMPORTANTE: necesitamos tu autorización";
                $asunto_booking_ca = "IMPORTANT: ens cal la teva autorització";
                
                $p = "p".$booking->plataforma;
                $asunto = "asunto_p".$booking->plataforma."_".$idioma;

                $template = "emails.$idioma.lopd_sin_booking";
                if($booking->any == 2018)
                {
                    $template = "emails.$idioma.lopd_booking";
                    $asunto = "asunto_booking_".$idioma;
                }

                $asunto = $$asunto;
                
                return view( $template, compact('ficha','booking','plataforma','asunto') );
            }
        }

        return view('manage.system.avisos.preview', compact('template','aviso','booking','tipo_doc'));
    }


    public function getLog($id)
    {
        $ficha = Aviso::find($id);

        if(Datatable::shouldHandle())
        {
            $logs = $ficha->logs;

            return Datatable::collection( $logs )
                ->addColumn('fecha', function($model) {
                    return $model->created_at->format('Y-m-d');
                })
                ->addColumn('hora', function($model) {
                    return $model->created_at->format('H:i:s');
                })
                ->addColumn('duracion', function($model) {
                    return $model->updated_at->diffForHumans($model->created_at);
                })
                ->showColumns('notas','bookings','enviados','leidos')
                ->searchColumns('fecha','notas')
                ->orderColumns('fecha')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }
    }

    public function setActivo($id,$estado)
    {
        $ficha = Aviso::find($id);

        $ficha->activo = $estado;
        $ficha->save();

        return redirect()->route('manage.system.avisos.index');
    }

    public function delete($id)
    {
        Aviso::find($id)->delete();

        return redirect()->route('manage.system.avisos.index');
    }


    public function getDocsIndex()
    {
        if(Datatable::shouldHandle())
        {
            $col = AvisoDoc::all();

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.system.avisos.docs.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('activo', function($model) {
                    return $model->activo?"<a data-label='Desactivar' href='". route('manage.system.avisos.docs.activo',[$model->id,0]). "' class='label label-success'><i class='fa fa-check-circle fa-1x'></i></a>":"<a data-label='Activar' href='".route('manage.system.avisos.docs.activo',[$model->id,1])."' class='label label-danger'><i class='fa fa-times-circle fa-1x'></i></a>";
                })
                ->addColumn('tipo', function($model) {
                    return $model->tipo?"Nota de Pago":"Documento";
                })
                ->addColumn('options', function($model) {

                    $ret = "";

                    //$ret .= " <a target='_blank' data-label='Vista previa' href='". route('manage.system.avisos.docs.render',$model->id) ."'><i class='fa fa-eye'></i></a>";

                    $data = " data-label='Borrar' data-model='Doc Aviso' data-action='". route( 'manage.system.avisos.docs.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })

                ->searchColumns('name')
                ->orderColumns('name','tipo')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.system.avisos.docs.index');
    }

    public function setActivoDocs($id,$estado)
    {
        $ficha = AvisoDoc::find($id);

        $ficha->activo = $estado;
        $ficha->save();

        return redirect()->route('manage.system.avisos.docs.index');
    }


    public function deleteDocs($id)
    {
        AvisoDoc::find($id)->delete();

        return redirect()->route('manage.system.avisos.docs.index');
    }

    public function getDocsNuevo()
    {
        $idiomas = ConfigHelper::getIdioma();
        $tipos = [0=> 'Importe', 1=>'% (Total)',  9 => '% (Restante)'];

        return view('manage.system.avisos.docs.new', compact('idiomas', 'tipos'));
    }

    public function getDocsUpdate($id)
    {
        $ficha = AvisoDoc::find($id);

        $idiomas = ConfigHelper::getIdioma();
        $tipos = [0=> 'Importe', 1=>'% (Total)',  9 => '% (Restante)'];

        return view('manage.system.avisos.docs.ficha', compact('ficha','idiomas','tipos'));
    }

    private function getBlade($id, $idioma, $booking=null)
    {
        $doc = AvisoDoc::find($id);

        $blade = "emails.layout_markdown";
        $tipo_doc = $doc->tipo;

        $lang = $booking ? $booking->idioma_contacto : ($idioma ?: "ES");
        $lang = strtoupper($lang);
        $ficha = $doc->getLang($lang);
        $args['tipo_doc'] = $tipo_doc;

        if($tipo_doc  == 1)
        {
            $name = "booking.nota_pago";

            $args['intro'] = $ficha->contenido;
            $args['asunto'] = $ficha->titulo;

            $sysemail = \VCN\Models\System\SystemMail::where('name', $name)->first();
            if($sysemail)
            {
                $syslang = null;
                if (!$sysemail->asunto)
                {
                    $syslang = $sysemail->getLang($lang);
                }

                $ficha = $syslang ?: $sysemail;
            }

            $args['importe'] = $booking ? $booking->getImporteNotaPago($doc) : 0;
        }

        $args['ficha'] = $booking;
        MailHelper::enviarCompile($ficha, $args);

        $markdown = new Markdown(view(), config('mail.markdown'));
        return $markdown->render($blade, $args);
    }

    public function getDocsRender(Request $request, $id, $idioma=null)
    {
        //vista previa
        return $this->getBlade($id, $idioma, null);
    }

    public function getDocsPreview(Request $request, $id, $booking=null)
    {
        $p = ConfigHelper::config('propietario');
        $booking = $booking ?: Booking::where('plataforma', $p)->first();

        return $this->getBlade($id, null, $booking);
    }

    public function postDocsUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name'      => 'required',
        ]);

        if($id)
        {
            $doc = AvisoDoc::find($id);
        }
        else
        {
            $doc = new AvisoDoc;
        }

        $doc->name = $request->input('name');
        $doc->activo = $request->has('activo');
        $doc->tipo = $request->get('tipo');
        $doc->notapago_porcentaje = $request->get('notapago_porcentaje') ?: 0;
        $doc->notapago_pagar = $request->get('notapago_pagar') ?: 0;
        $doc->notapago_pagar_tipo = $request->get('notapago_pagar_tipo') ?: 0;
        $doc->save();

        if($request->has('reclamar_total_bool'))
        {
            $doc->notapago_porcentaje = 0;
            $doc->notapago_pagar = 0;
            $doc->notapago_pagar_tipo = 2;
            $doc->save();
        }

        foreach(ConfigHelper::getIdiomaContacto() as $key=>$idioma)
        {
            if($key!="")
            {
                if(!$id)
                {
                    $doct = new AvisoDocIdioma;
                    $doct->doc_id = $doc->id;
                }
                else
                {
                    $doct = $doc->idiomas->where('idioma',$key)->first();
                }

                $doct->idioma = $key;
                $doct->titulo = $request->input("titulo")[$key] ?: "";
                $doct->contenido = $request->input("contenido")[$key] ?: "";
                $doct->save();
            }
        }

        $doc->save();

        Session::flash('mensaje-ok', "Aviso guardado correctamente.");
        return redirect()->route('manage.system.avisos.docs.ficha', $doc->id);

        // return redirect()->route('manage.system.avisos.docs.index');
    }


    public function getListado(Request $request)
    {
        if( auth()->user()->isFullAdmin() )
        {
            $plataformas = ConfigHelper::plataformas();

            $oficinas = [0=>'Todas'] + Oficina::all()->sortBy('name')->pluck('name','id')->toArray();
            $prescriptores = [0=>'Todos'] + Prescriptor::all()->sortBy('name')->pluck('name','id')->toArray();

            $categorias = [0=>'Todas'] + Categoria::all()->sortBy('name')->pluck('name','id')->toArray();

            $origenes = [0=>'Todos'] + Origen::all()->sortBy('name')->pluck('name','id')->toArray();
            $suborigenes = [0=>'Todos'] + Suborigen::all()->sortBy('name')->pluck('name','id')->toArray();
            $suborigenesdet = [0=>'Todos'] + SuborigenDetalle::all()->sortBy('name')->pluck('name','id')->toArray();

            $valores['plataformas'] = $request->input('plataformas',0);
        }
        else
        {
            $plataformas = [ 0=>ConfigHelper::plataformaApp() ];

            $oficinas = [0=>'Todas'] + Oficina::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $prescriptores = [0=>'Todos'] + Prescriptor::plataforma()->sortBy('name')->pluck('name','id')->toArray();

            $categorias = [0=>'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();

            $origenes = [0=>'Todos'] + Origen::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $suborigenes = [0=>'Todos'] + Suborigen::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $suborigenesdet = [0=>'Todos'] + SuborigenDetalle::plataforma()->sortBy('name')->pluck('name','id')->toArray();

            $valores['plataformas'] = ConfigHelper::propietario();
        }

        $statuses = [0=>'Todos'] + \VCN\Models\Bookings\Status::orderBy('orden')->pluck('name','id')->toArray();
        $statusess = [0=>'Todas'] + \VCN\Models\Solicitudes\Status::orderBy('orden')->pluck('name','id')->toArray();

        $convocatorias = [0=>'Todas'];

        $anys = \VCN\Models\Informes\Venta::groupBy('any')->get()->pluck('any','any')->toArray();
        $anys[end($anys)+1] = end($anys)+1;

        $valores['oficinas'] = $request->has('oficinas')?implode(',',$request->get('oficinas',[])):"";
        $valores['prescriptores'] = $request->input('prescriptores',0);
        $valores['idiomas'] = $request->input('idiomas',0);
        $valores['categorias'] = $request->has('categorias')?implode(',',$request->get('categorias',[])):"";
        $valores['status'] = $request->has('status')?implode(',',$request->get('status',[])):"";
        $valores['statuss'] = $request->has('statuss')?implode(',',$request->get('statuss',[])):"";

        $valores['origenes'] = $request->has('origenes')?implode(',',$request->get('origenes',[])):"";
        $valores['suborigenes'] = $request->has('suborigenes')?implode(',',$request->get('suborigenes',[])):"";
        $valores['suborigenesdet'] = $request->has('suborigenesdet')?implode(',',$request->get('suborigenesdet',[])):"";

        $valores['booking'] = $request->get('booking');
        $valores['email'] = $request->has('email');
        $valores['sinbooking'] = $request->has('sinbooking');

        $valores['fndesde'] = $request->input('fndesde',null);
        $valores['fnhasta'] = null;
        if($valores['fndesde'])
        {
            $valores['fnhasta'] = $request->input('fnhasta',null);
        }

        $valores['anys'] = $request->get('anys');
        $valores['any'] = $request->get('any');

        $valoresNot['prescriptores'] = $request->has('not-prescriptores') ? implode(',',$request->get('not-prescriptores',[])) : "";
        $valoresNot['any'] = $request->input('not-any',0);
        $valoresNot['paises'] = $request->has('not-paises') ? implode(',',$request->get('not-paises',[])) : null;
        $valoresNot['cursos'] = $request->has('not-curso') ? implode(',',$request->get('not-cursos',[])) : null;

        $valores['motivos'] = $request->input('motivos',null);
        $valores['optout'] = (int)$request->input('optout',null);

        // dd($valores);

        $listado = $request->has('listado');

        if($listado && Datatable::shouldHandle() )
        {
            // dd($valores);
            $list_viajeros = collect();
            $list_tutores = collect();

            $list = null;

            if($valores['booking'])
            {
                //bookings
                $list = Booking::bookingMailing($valores,$valoresNot);
            }
            else
            {
                //solicitudes
                $f = explode(',',$valores['categorias']);
                if((int)$f[0]>0)
                {
                    $list = Solicitud::whereIn('category_id',$f);
                }
                else
                {
                    if($f[0]=="0") //todas
                    {
                        $list = Solicitud::select();
                    }
                    else
                    {
                        $list = Solicitud::where(function($query) {
                            return $query->where('category_id',0)->orWhere('category_id',null);
                        });
                    }
                }

                if( $valores['any'] )
                {
                    $any = $valores['any'];
                    $fini = "$any-01-01";
                    $ffin = "$any-12-31";
                    
                    $list = $list->where('fecha','>=',$fini)->where('fecha','<=',$ffin);
                }

                if( $valores['plataformas'] )
                {
                    $list = $list->where('plataforma',$valores['plataformas']);
                }

                $f = $valores['oficinas'];
                if($f)
                {
                    $f = explode(',',$f);
                    $list = $list->whereIn('oficina_id',$f);
                }

                $f = $valores['statuss'];
                if($f)
                {
                    $f = explode(',',$f);
                    $list = $list->whereIn('status_id',$f);
                }

                //archivar_motivo (descartado)
                $f = $valores['motivos'];
                if($f)
                {
                    $st = $request->get('statuss');
                    if(in_array(ConfigHelper::config('solicitud_status_archivado'), $st))
                    {
                        $list = $list->where('archivar_motivo',$f);
                    }
                }
            }

            $viajero_ids = $list->pluck('viajero_id')->toArray();
            $viajero_ids = array_unique($viajero_ids);

            $viajeros = Viajero::whereIn('id', $viajero_ids);
            if($valores['optout']<2)
            {
                $viajeros = $viajeros->where('optout',$valores['optout']);
            }

            $forigen = explode(',',$valores['origenes']);
            if($forigen[0]>0)
            {
                $viajeros = $viajeros->whereIn('origen_id', $forigen);
            }

            $fsuborigen = explode(',',$valores['suborigenes']);
            if($fsuborigen[0]>0)
            {
                $viajeros = $viajeros->whereIn('suborigen_id', $fsuborigen);
            }

            $fsuborigendet = explode(',',$valores['suborigenesdet']);
            if($fsuborigendet[0]>0)
            {
                $viajeros = $viajeros->whereIn('suborigendet_id', $fsuborigendet);
            }

            foreach($viajeros->get() as $v)
            {
                //Sin Booking
                if(!$valores['booking'] && $valores['sinbooking'])
                {
                    if($v->bookings_terminados->count())
                    {
                        continue;
                    }
                }

                //Email
                if($valores['email'])
                {
                    if(!$v->email && ($v->tutor1 && !$v->tutor1->email) && ($v->tutor2 && !$v->tutor2->email) )
                    {
                        continue;
                    }
                }

                //Fecha Nac.
                if($valores['fndesde'])
                {
                    if(!$v->fechanac)
                    {
                        continue;
                    }

                    $fechanac = Carbon::parse($v->fechanac);
                    $f1 = Carbon::createFromFormat('d/m/Y',$valores['fndesde']);
                    $f2 = Carbon::createFromFormat('d/m/Y',$valores['fnhasta']);
                    if($fechanac->lt($f1) || $fechanac->gt($f2))
                    {
                        continue;
                    }
                }

                //Idioma
                if($valores['idiomas'])
                {
                    if(strtoupper($v->idioma_contacto) != $valores['idiomas'])
                    {
                        continue;
                    }
                }

                //Último Booking: multiselect ($valores['anys']) + not ($valoresNot['desdes'])
                if( $valores['booking'] && $valores['anys'] && $v->last_booking_any )
                {
                    if( !in_array($v->last_booking_any, $valores['anys']) )
                    {
                        continue;
                    }

                    if($valoresNot['any'] && $v->last_booking_any == $valoresNot['any'])
                    {
                        continue;
                    }
                }

                if($v->tutor1 && $v->tutor1->email)
                {
                    $bPush = true;
                    if($valores['optout']<2)
                    {
                        $bPush = $v->tutor1->optout;
                        if($valores['optout'] == 1)
                        {
                            $bPush = !$v->tutor1->optout;
                        }
                    }

                    if($bPush)
                    {
                        $t = $v->tutor1;
                        $t->edad = $v->edad;
                        $t->viajero_nom = $v->name;
                        $t->viajero_ape = $v->lastname." ".$v->lastname2;
                        $t->viajero_id = $v->id;
                        $t->idioma = $v->idioma_contacto;
                        $t->oficina = $v->oficina ? $v->oficina->name : "-";

                        $list_tutores->push($t);
                    }
                }

                if($v->tutor2 && $v->tutor2->email)
                {
                    $bPush = true;
                    if($valores['optout']<2)
                    {
                        $bPush = $v->tutor2->optout;
                        if($valores['optout'] == 1)
                        {
                            $bPush = !$v->tutor2->optout;
                        }
                    }

                    if($bPush)
                    {
                        $t = $v->tutor2;
                        $t->edad = $v->edad;
                        $t->viajero_nom = $v->name;
                        $t->viajero_ape = $v->lastname." ".$v->lastname2;
                        $t->viajero_id = $v->id;
                        $t->idioma = $v->idioma_contacto;
                        $t->oficina = $v->oficina ? $v->oficina->name : "-";

                        $list_tutores->push($t);
                    }
                }

                if($valores['email'] && !$v->email)
                {
                    continue;
                }

                $list_viajeros->push($v);
            }

            if($request->get('list') == "viajeros")
            {

                return Datatable::collection( $list_viajeros )
                    ->showColumns('email','lastname','lastname2','movil')
                    ->addColumn('name', function($model) {
                        $ret = "<a href='". route('manage.viajeros.ficha', $model->id) ."'> ". $model->name ."</a>";
                        return $ret;
                    })
                    ->addColumn('edad', function($model) {
                        return $model->edad;
                    })
                    ->addColumn('idioma', function($model) {
                        return $model->idioma_contacto;
                    })
                    ->addColumn('optout', function($model) {
                        return $model->optout?"OPTOUT":"OPTIN";
                    })
                    ->addColumn('oficina', function($model) {
                        return $model->oficina?$model->oficina->name:"-";
                    })
                    ->searchColumns('name')
                    ->orderColumns('name')
                    ->setSearchStrip()->setOrderStrip()
                    ->setAliasMapping()
                    ->make();
            }

            if($request->get('list') == "tutores")
            {

                return Datatable::collection( $list_tutores )
                    ->showColumns('email','lastname','movil')
                    ->addColumn('name', function($model) {
                        $ret = "<a href='". route('manage.tutores.ficha', $model->id) ."'> ". $model->name ."</a>";
                        return $ret;
                    })
                    ->addColumn('edad', function($model) {
                        return $model->edad;
                    })
                    ->addColumn('idioma', function($model) {
                        return $model->idioma;
                    })
                    ->addColumn('viajero_nom', function($model) {
                        $ret = "<a href='". route('manage.viajeros.ficha', $model->viajero_id) ."'> ". $model->viajero_nom ."</a>";
                        return $ret;
                    })
                    ->addColumn('viajero_ape', function($model) {
                        $ret = $model->viajero_ape;
                        return $ret;
                    })
                    ->addColumn('optout', function($model) {
                        return $model->optout?"OPTOUT":"OPTIN";
                    })
                    ->addColumn('oficina', function($model) {
                        return $model->oficina;
                    })
                    ->searchColumns('name')
                    ->orderColumns('name')
                    ->setSearchStrip()->setOrderStrip()
                    ->setAliasMapping()
                    ->make();
            }
        }

        $filtros = $request->input();

        $optouts = [1=> 'Optout', 0=> 'Optin', 2=> 'Todos'];
        
        return view('manage.system.avisos.listado', compact('valores','valoresNot', 'listado', 'filtros',
            'categorias','plataformas','oficinas','prescriptores','anys','statuses','convocatorias', 'statusess', 'origenes', 'suborigenes', 'suborigenesdet','optouts'));
    }

    public function getSync(Request $request)
    {
        $apidefault = ConfigHelper::config('mc_api');//"a2d823daac0c2f08cd18257c0a08f93b-us12";
        
        return view('manage.system.avisos.sync', compact('apidefault'));
    }

    public function getSyncLogs(Request $request)
    {
        $p = ConfigHelper::propietario();
        $log = "p$p";
        
        if(Datatable::shouldHandle())
        {
            $logs = AvisoLog::where('aviso_id', 0)->where('enviados', "mc")->where('leidos', $log)->get();

            return Datatable::collection( $logs )
                ->addColumn('fecha', function($model) {
                    return $model->created_at->format('d/m/Y H:i');
                })
                ->showColumns('notas')
                ->addColumn('cleaned', function($model) {
                    $ret = $model->bookings['cleaned'] ?? null;
                    if($ret)
                    {
                        $ret = implode(', ', $model->bookings['cleaned']);
                    }
                    return $ret;
                })
                ->addColumn('unsubscribed', function($model) {
                    $ret = $model->bookings['unsubscribed'] ?? null;
                    if($ret)
                    {
                        $ret = implode(', ', $model->bookings['unsubscribed']);
                    }
                    return $ret;
                })
                ->searchColumns('fecha')
                ->orderColumns('fecha')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }
    }

    public function postSync(Request $request)
    {
        $p = ConfigHelper::propietario();
        $apidefault = ConfigHelper::config('mc_api');//"a2d823daac0c2f08cd18257c0a08f93b-us12";

        $apikey = $request->get('mc_api') ?: $apidefault;
        if(!$apikey)
        {
            Session::flash('mensaje', "Mailchimp:  ERROR APIKEY");
            return redirect()->route('manage.system.avisos.sync');
        }

        // $log = Aviso::syncMailchimp($p, $apikey);
        \VCN\Jobs\JobSyncMailchimp::dispatch($p,$apikey);

        Session::flash('mensaje-ok', "Sync Mailchimp en proceso... (job)");
        return redirect()->route('manage.system.avisos.sync');
    }
}
