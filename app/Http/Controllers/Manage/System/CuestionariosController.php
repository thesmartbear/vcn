<?php

namespace VCN\Http\Controllers\Manage\System;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\System\Cuestionario;
use VCN\Models\System\CuestionarioVinculado;
use VCN\Models\System\CuestionarioRespuesta;
use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingLog;
// use VCN\Models\Exams\Examen;

use VCN\Models\Cursos\Curso;
use VCN\Models\Centros\Centro;
use VCN\Models\System\Plataforma;

use Datatable;
use Session;
use ConfigHelper;
use VCN\Helpers\MailHelper;
use Carbon;
use Log;

class CuestionariosController extends Controller
{
    //

    public function __construct()
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');

        $this->middleware("permiso.view:cuestionarios", ['except' => ['getResultados','getRespuesta','getInformes','getRespuestaAjax','getSaveAjax']]);
        $this->middleware("permiso.view:cuestionarios-resumen", ['only' => ['getResultados','getRespuesta','getInformes','getRespuestaAjax','getSaveAjax']]);
    }

    public function getIndex(Request $request)
    {
        if(Datatable::shouldHandle())
        {
            $filtro = ConfigHelper::config('propietario');
            if( $filtro && !auth()->user()->isFullAdmin() )
            {
                // $col = ProveedorModel::where('propietario', 0)->orWhere('propietario',$filtro)->get();
                $col1 = Cuestionario::where(['propietario'=> $filtro])->get();
                $col = Cuestionario::where(['propietario'=> 0])->get();

                $col = $col->merge($col1);
            }
            else
            {
                $col = Cuestionario::all();
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.system.cuestionarios.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('form', function($model) {
                    return ConfigHelper::formulario($model->form);
                })
                ->addColumn('destino', function($model) {
                    return $model->destino;
                })
                ->addColumn('test', function($model) {
                    return $model->test?"Si":"No";
                })
                ->addColumn('activo', function($model) {
                    return $model->activo?"<span class='label label-success'><i class='fa fa-check-circle fa-1x'></i></span>":"<span class='label label-danger'><i class='fa fa-times-circle fa-1x'></i></span>";
                })
                ->addColumn('plataforma', function($model) {
                    return ConfigHelper::plataforma($model->propietario);
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Cuestionario' data-action='". route( 'manage.system.cuestionarios.delete', $model->id) . "'";
                    $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','*')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.system.cuestionarios.index');
    }

    public function getNuevo(Request $request)
    {
        $forms = ConfigHelper::formulario();

        return view('manage.system.cuestionarios.new', compact('forms'));
    }

    public function getUpdate(Request $request, $id)
    {
        $forms = ConfigHelper::formulario();

        $ficha = Cuestionario::findOrFail($id);

        return view('manage.system.cuestionarios.ficha', compact('ficha','forms'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'form' => 'required',
        ]);

        $forms = ConfigHelper::formulario();

        $ficha = Cuestionario::find($id);
        if(!$id)
        {
            $ficha = new Cuestionario;

            $id = $ficha->save();
        }

        $data = $request->except('_token','propietario_check');
        $data['activo'] = $request->has('activo');
        $data['test'] = $request->has('test');

        $ficha->update($data);

        return redirect()->route('manage.system.cuestionarios.index');
    }

    public function delete(Request $request, $id)
    {
        $ficha = Cuestionario::findOrFail($id);

        $ficha->delete();

        return redirect()->route('manage.system.cuestionarios.index');
    }

    public function postVinculado(Request $request)
    {
        $this->validate($request, [
            'cuestionario_id' => 'required|exists:cuestionarios,id',
        ]);

        $data = $request->except("_token");

        $cv = CuestionarioVinculado::create($data);

        Session::flash('tab','#cuestionarios');
        return redirect()->back();
    }

    public function deleteVinculado(Request $request, $id)
    {
        $ficha = CuestionarioVinculado::findOrFail($id);

        $ficha->delete();

        Session::flash('tab','#cuestionarios');
        return redirect()->back();
    }

    public function getReclamar(Request $request, $id, $modelo=null, $modelo_id=null)
    {
        $ficha = Cuestionario::findOrFail($id);

        ini_set('memory_limit', '400M');
        set_time_limit(0);

        $iMails = 0;
        $iMails_bloque = 100;
        $iMails_sleep = 0.3;

        // $bookings = $ficha->bookings;

        $any = intval(Carbon::now()->year);
        $valores['desdes'] = "01/01/".$any;
        $valores['hastas'] = "31/12/".$any;
        $valores['cuestionarios'] = $ficha->id;
        $valores['plataformas'] = $ficha->propietario?:ConfigHelper::propietario();

        $valores['reclamar'] = $modelo;
        
        switch($modelo)
        {
            case "Centro":
            {
                $valores['centros'] = $modelo_id;
            }
            break;

            case "Curso":
            {
                $valores['cursos'] = $modelo_id;
            }
            break;
            
            case "Categoria":
            {
                $valores['categorias'] = $modelo_id;
            }
            break;

            case "Subcategoria":
            {
                $valores['subcategorias'] = $modelo_id;
            }
            break;

            case "Abierta":
            {
                $valores['tipoc'] = 3;
                $valores['convocatorias'] = $modelo_id;
            }
            break;

            case "Cerrada":
            {
                $valores['tipoc'] = 5;
                $valores['convocatorias'] = $modelo_id;
            }
            break;

            case "ConvocatoriaMulti":
            {
                $valores['tipoc'] = 4;
                $valores['convocatorias'] = $modelo_id;
            }
            break;
        }

        $bookings = $ficha->bookingsFiltros($valores);
        $bookings = $bookings->where('es_directo', 0);
        // dd($bookings->count());

        $iTotalV = 0;
        $iTotalT = 0;

        foreach($bookings as $booking)
        {
            // $fecha = Carbon::parse($booking->course_start_date);
            // if($fecha->lte(Carbon::now()))
            // {
            //     continue;
            // }

            //Viajero
            if($ficha->destino != "tutores")
            {
                if(!$booking->getCuestionarioEstadoViajero($id))
                {
                    MailHelper::mailBookingCuestionario($id, $booking, $booking->viajero);
                    BookingLog::addLog($booking, "eMail Cuestionario Viajero[$id]");

                    $iTotalV++;
                    $iMails++;

                    if($iMails>$iMails_bloque)
                    {
                        $iMails = 0;
                        // sleep($iMails_sleep);
                    }
                }
            }

            //Tutores
            foreach($booking->viajero->tutores as $t)
            {
                if(!$booking->getCuestionarioEstadoTutor($id, $t->id))
                {
                    MailHelper::mailBookingCuestionario($id,$booking,$t);
                    BookingLog::addLog($booking, "eMail Cuestionario Tutor[$id]");

                    $iTotalT++;
                    $iMails ++;

                    if($iMails>$iMails_bloque)
                    {
                        $iMails = 0;
                        // sleep($iMails_sleep);
                    }
                }
            }
        }

        Session::flash('mensaje-ok', "Se ha reclamado a un total de $iTotalV Viajeros y $iTotalT Tutores.");
        Session::flash('tab','#cuestionarios');
        return redirect()->back();
    }

    public function getResultados(Request $request)
    {
        if( auth()->user()->isFullAdmin() )
        {
            // $centros = Centro::all();
            $cursos = Curso::all();

            // $valores['plataformas'] = 0;
            $valores['plataformas'] = intval($request->input('plataformas', 0));
            $plataformas = [0=> "Todas"] + Plataforma::all()->pluck('name','id')->toArray();
        }
        else
        {
            // $centros = Centro::plataforma();
            $cursos = Curso::plataforma();

            $valores['plataformas'] = ConfigHelper::propietario();
            $plataformas = null;
        }

        // $ccentros = [];
        // $c = [0=> "Todos con cuestionarios"];
        // foreach($centros as $cx)
        // {
        //     if($cx->cuestionarios->count()>0)
        //     {
        //         $c[$cx->id] = $cx->name;
        //         $ccentros[] = $cx->id;
        //     }
        // }
        // $centros = $c;

        $ccursos = [];
        $c = [0=> "Todos con cuestionarios"];
        foreach($cursos as $cx)
        {
            if($cx->cuestionarios->count()>0)
            {
                $c[$cx->id] = $cx->name;
                $ccursos[] = $cx->id;
            }
        }
        $cursos = $c;

        $cuestionarios = [0=>'Todos'] + Cuestionario::all()->sortBy('name')->pluck('name','id')->toArray();
        // $examenes = ['' => ''] + Examen::plataforma()->where('activo',1)->pluck('name','id')->toArray();

        // $valores['centros'] = intval($request->input('centros',0));
        $valores['cursos'] = intval($request->input('cursos',0));

        $valores['cuestionarios'] = intval($request->input('cuestionarios',0));

        $valores['desdes'] = $request->input('desdes',null);
        $valores['hastas'] = $request->input('hastas',null);

        // $valores['estado'] = $request->input('estado',null);

        $listado = intval($request->input('cuestionarios',0));

        $results = 0;
        $totales = null;
        $cuestionario = null;

        if($listado)
        {
            $c = Cuestionario::find($valores['cuestionarios']);
            /*
            if($valores['desdes'] && $valores['hastas'])
            {
                $bookings = $c->bookingsFiltros($valores);
            }
            else
            {
                $bookings = $c->bookings;
            }
            */
            $bookings = $c->bookingsFiltros($valores);

            $results = $bookings->count();

            //Caja de totales: respondidos/Totales
            $totales = null;
            $cuestionario = Cuestionario::find($valores['cuestionarios']);

            $bookings1 = clone $bookings;
            $respuestas = CuestionarioRespuesta::whereIn('booking_id',$bookings1->pluck('id')->toArray());

            $totales['totalv'] = $cuestionario->total_viajeros($valores);
            $totales['totalt'] = $cuestionario->total_tutores($valores);
            $totales['totalv_estados'] = $cuestionario->total_viajeros_estado($valores);
            $totales['totalt_estados'] = $cuestionario->total_tutores_estado($valores);

            $totales['bookings'] = $results;

            $bookings1 = clone $bookings;
            $cursosId = [];
            $cursos = [];
            foreach($bookings1 as $b)
            {
                $bcid = $b->curso->id;
                if(!in_array( $bcid, $cursosId) )
                {
                    $cursosId[] = $bcid;
                    $cursos[] = $b->curso->name;
                }
            }

            $totales['cursos'] = $cursos;


            //Dtt
            if(Datatable::shouldHandle())
            {
                // $bookings = $bookings->get();

                $cuestionario_id = $cuestionario->id;

                return Datatable::collection( $bookings )
                    ->addColumn('viajeroname', function($model) {
                        return "<a target='_blank' data-label='Ficha Viajero' href='". route('manage.viajeros.ficha', $model->viajero->id) ."'>". $model->viajero->name ."</a>";
                    })
                    ->addColumn('viajerolastname', function($model) {
                        return "<a target='_blank' data-label='Ficha Viajero' href='". route('manage.viajeros.ficha', $model->viajero->id) ."'>". $model->viajero->lastname ."</a>";
                    })
                    ->addColumn('curso', function($model) {
                        return $model->curso->name;
                    })
                    ->addColumn('convocatoria', function($model) {
                        return $model->convocatoria->name;
                    })
                    ->addColumn('viajero_estado', function($model) use ($cuestionario) {

                        if($cuestionario->destino=="tutores")
                        {
                            return "-";
                        }

                        $r = $model->getCuestionarioRespuesta($cuestionario->id);
                        if(!$r || $r->estado == 0)
                        {
                            return "pendiente";
                        }

                        $estado = ConfigHelper::areaFormEstado( $r->estado );

                        if($r->estado < 3)
                        {
                            $ret = "<a target='_blank' href='" . route('manage.system.cuestionarios.respuesta', $r->id) . "'>$estado</a>";
                        }
                        else
                        {
                            $ret = '<span class="text-warning">'.$estado.'</span>';
                        }

                        return $ret;
                    })
                    ->addColumn('viajero_resultado', function($model) use ($cuestionario_id) {
                        $r = $model->getCuestionarioRespuesta($cuestionario_id);
                        return isset($r->resultado)?$r->resultado:0;
                    })
                    ->addColumn('viajero_nivel', function($model) use ($cuestionario_id) {
                        $r = $model->getCuestionarioRespuesta($cuestionario_id);
                        return isset($r->nivel)?$r->nivel:0;
                    })
                    ->addColumn('escola_thau', function($model) {
                        return ConfigHelper::getCicThau($model->viajero->cic_thau?:0);
                    })
                    ->addColumn('cic_idiomas', function($model) {
                        return $model->viajero->cic ? ConfigHelper::getCICNivel($model->viajero->cic_nivel) : "";
                    })
                    ->addColumn('tutor1_estado', function($model) use ($cuestionario_id) {

                        $t = $model->viajero->tutor1;
                        if(!$t)
                        {
                            return "-";
                        }

                        $r = $model->getCuestionarioRespuestaTutor($cuestionario_id,$t->id);

                        if(!$r || $r->estado == 0)
                        {
                            return "pendiente";
                        }

                        $estado = ConfigHelper::areaFormEstado( $r->estado );

                        if($r->estado < 3 )
                        {
                            $ret = "<a target='_blank' href='" . route('manage.system.cuestionarios.respuesta', $r->id) . "'>$estado</a>";
                        }
                        else
                        {
                            $ret = '<span class="text-warning">'.$estado.'</span>';
                        }

                        return $ret;

                    })
                    ->addColumn('tutor1_resultado', function($model) use ($cuestionario_id) {

                        $t = $model->viajero->tutor1;
                        if(!$t)
                        {
                            return "-";
                        }

                        $r = $model->getCuestionarioRespuestaTutor($cuestionario_id,$model->viajero->tutor1->id);
                        return isset($r->resultado)?$r->resultado:0;
                    })
                    ->addColumn('tutor1_nivel', function($model) use ($cuestionario_id) {

                        $t = $model->viajero->tutor1;
                        if(!$t)
                        {
                            return "-";
                        }

                        $r = $model->getCuestionarioRespuestaTutor($cuestionario_id,$model->viajero->tutor1->id);
                        return isset($r->nivel)?$r->nivel:0;
                    })
                    ->addColumn('tutor2_estado', function($model) use ($cuestionario_id) {

                        $t = $model->viajero->tutor2;
                        if(!$t)
                        {
                            return "-";
                        }

                        $r = $model->getCuestionarioRespuestaTutor($cuestionario_id,$t->id);

                        if(!$r || $r->estado == 0)
                        {
                            return "pendiente";
                        }

                        $estado = ConfigHelper::areaFormEstado( $r->estado );

                        if($r->estado < 3)
                        {
                            $ret = "<a target='_blank' href='" . route('manage.system.cuestionarios.respuesta', $r->id) . "'>$estado</a>";
                        }
                        else
                        {
                            $ret = '<span class="text-warning">'.$estado.'</span>';
                        }

                        return $ret;

                    })
                    ->addColumn('tutor2_resultado', function($model) use ($cuestionario_id) {

                        $t = $model->viajero->tutor2;
                        if(!$t)
                        {
                            return "-";
                        }

                        $r = $model->getCuestionarioRespuestaTutor($cuestionario_id,$model->viajero->tutor2->id);
                        return isset($r->resultado)?$r->resultado:0;
                    })
                    ->addColumn('tutor2_nivel', function($model) use ($cuestionario_id) {

                        $t = $model->viajero->tutor2;
                        if(!$t)
                        {
                            return "-";
                        }

                        $r = $model->getCuestionarioRespuestaTutor($cuestionario_id,$model->viajero->tutor2->id);
                        return isset($r->nivel)?$r->nivel:0;
                    })
                    ->addColumn('options', function($model){
                        $r = $model;
                        if($r){
                            $boton = '<a class="btn btn-success cambiarestadobutton" data-booking="'.$r->id.'">cambiar estado</a>';
                        }
                        return $r ? $boton: '-';
                    })

                    ->searchColumns('viajerolastname','viajeroname', 'curso')
                    ->orderColumns('viajerolastname','curso', 'viajeroname','viajero_estado','viajero_resultado','viajero_nivel','tutor1_estado','tutor1_resultado','tutor1_nivel','tutor2_estado','tutor2_resultado','tutor2_nivel','options')
                    ->setAliasMapping()
                    ->setSearchStrip()->setOrderStrip()
                    ->make();
            }
        }

        // $estados = [
        //     null  => 'Todos',
        //     0   => 'pendiente',
        //     1   => 'completo',
        //     2   => 'verificado',
        //     3   => 'offline',
        // ];

        return view('manage.system.cuestionarios.resultados', compact('valores','results','listado','cursos','cuestionarios', 'totales', 'cuestionario','plataformas'));

    }

    public function getInformes(Request $request)
    {
        ini_set('memory_limit', '400M');
        set_time_limit(0);

        if (auth()->user()->isFullAdmin()) {
            // $centros = Centro::all();
            $cursos = Curso::all();

            $valores['plataformas'] = 0;
        } else {
            // $centros = Centro::plataforma();
            $cursos = Curso::plataforma();

            $valores['plataformas'] = ConfigHelper::propietario();
        }

        // $ccentros = [];
        // $c = [0=> "Todos con cuestionarios"];
        // foreach($centros as $cx)
        // {
        //     if($cx->cuestionarios->count()>0)
        //     {
        //         $c[$cx->id] = $cx->name;
        //         $ccentros[] = $cx->id;
        //     }
        // }
        // $centros = $c;

        $ccursos = [];
        $c = [0 => "Todos con cuestionarios"];
        foreach ($cursos as $cx) {
            if ($cx->cuestionarios->count() > 0) {
                $c[$cx->id] = $cx->name;
                $ccursos[] = $cx->id;
            }
        }
        $cursos = $c;

        $cuestionarios = [0 => 'Todos'] + Cuestionario::all()->sortBy('name')->pluck('name', 'id')->toArray();
        // $examenes = ['' => ''] + Examen::plataforma()->where('activo',1)->pluck('name','id')->toArray();

        // $valores['centros'] = intval($request->input('centros',0));
        $valores['cursos'] = intval($request->input('cursos', 0));

        $valores['cuestionarios'] = intval($request->input('cuestionarios', 0));

        $valores['desdes'] = $request->input('desde',null);
        $valores['hastas'] = $request->input('hasta',null);
        $valores['any'] = intval($request->input('any',Carbon::now()->year));
        if($request->input('any'))
        {
            $valores['desdes'] = $request->input('desdes',null);
            $valores['hastas'] = $request->input('hastas',null);

            if(!$valores['desdes'])
            {
                $valores['desdes'] = "01/01/".$valores['any'];
                $valores['hastas'] = "31/12/".$valores['any'];
            }
        }
        
        $anys = \VCN\Models\Informes\Venta::groupBy('any')->get()->pluck('any','any')->toArray();
        $anys[end($anys)+1] = end($anys)+1;

        // $valores['estado'] = $request->input('estado',null);

        $listado = intval($request->input('cuestionarios', 0));

        $results = 0;
        $totales = null;
        $totalesRespuestas = array();
        $cuestionario = null;
        $cuestionarioRespuestasCol = null;
        $crespuestas = array();

        if ($listado) {
            $c = Cuestionario::find($valores['cuestionarios']);
            /*
            if (($valores['desdes'] && $valores['hastas']) || $valores['cursos']) {
                $bookings = $c->bookingsFiltros($valores);
            } else {
                $bookings = $c->bookings;
            }
            */
            $bookings = $c->bookingsFiltros($valores);

            $results = $bookings->count();

            //Caja de totales: respondidos/Totales
            $cuestionario = Cuestionario::find($valores['cuestionarios']);
            $bookings1 = clone $bookings;

            //$respuestas = CuestionarioRespuesta::whereIn('booking_id', $bookings1->pluck('id')->toArray());
            $totales['totalv'] = $cuestionario->total_viajeros($valores);
            $totales['totalt'] = $cuestionario->total_tutores($valores);
            $totales['totalv_estados'] = $cuestionario->total_viajeros_estado($valores);
            $totales['totalt_estados'] = $cuestionario->total_tutores_estado($valores);
            $totales['bookings'] = $results;

            $bookings2 = $c->bookings;
            $cursosId = [];
            $cursos = [];
            $cursos[0] = "Todos";
            foreach ($bookings2 as $b) {
                $bcid = $b->curso->id;
                if (!in_array($bcid, $cursosId)) {
                    $cursosId[] = $bcid;
                    $cursos[$bcid] = $b->curso->name;
                }
            }

            $totales['cursos'] = $cursos;



            // $estados = [
            //     null  => 'Todos',
            //     0   => 'pendiente',
            //     1   => 'completo',
            //     2   => 'verificado',
            //     3   => 'offline',
            // ];


            $bArray = $bookings->pluck('id')->toArray();
            $cuestionarioRespuestasCol = CuestionarioRespuesta::whereIn('booking_id', $bArray)->where('cuestionario_id', $cuestionario->id)->get();
            $cuestionarioRespuestas = $cuestionarioRespuestasCol->toArray();
            for ($i = 0; $i < count($cuestionarioRespuestas); $i++) {
                $cuestionarioRespuestas[$i]['respuestas'] = json_decode($cuestionarioRespuestas[$i]['respuestas']);
            }


            //$resultadoMedia = number_format(round($cuestionarioRespuestasBooking->pluck('resultado')->avg()),0);
            //$nivelMedia = number_format(round($cuestionarioRespuestasBooking->pluck('nivel')->avg()),0);

            //dd($bookings->groupBy('curso_id'));
            //exit();

            //dd(Booking::whereIn('id', $cuestionarioRespuestasCol->pluck('booking_id')->toArray())->get());
            //Dtt
            if($cuestionario->form == 'padrescoloniascic' || $cuestionario->form == 'padresmaxcamps') {

                $crespuestas = array();
                //$cr = collect($cuestionarioRespuestas);
                foreach ($cuestionarioRespuestas as $crr) {
                    //echo $crr['respuestas']->r01.'<br />';
                    $crespuestas[$crr['id']] = $crr['respuestas'];
                }

                $totalesRespuestas = array();
                foreach ($crespuestas as $k => $v) {
                    if($v != null || $v != '') {
                        foreach ($v as $rvk => $rvv) {
                            //print_r($rvk);
                            $totalesRespuestas[$rvk][] = $rvv;
                        }
                    }
                }

                if (Datatable::shouldHandle()) {
                    $bookings = Booking::whereIn('id', $cuestionarioRespuestasCol->pluck('booking_id')->toArray())->get();

                    $cuestionario_id = $cuestionario->id;

                    return Datatable::collection($bookings)
                        ->addColumn('viajero', function ($model) {
                            return "<a target='_blank' data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                        })
                        ->addColumn('curso', function ($model) {
                            return $model->curso->name;
                        })
                        ->addColumn('tutor1_resultado', function ($model) use ($cuestionario_id, $cuestionario) {

                            $t = $model->viajero->tutor1;
                            if (!$t) {
                                return "-";
                            }

                            $r = $model->getCuestionarioRespuestaTutor($cuestionario_id, $model->viajero->tutor1->id);
                            $rR = '-';
                            if ($r) {
                                $rRespuestas = json_decode($r->respuestas, true);
                                $rR = '';
                                foreach ($rRespuestas as $k => $v)
                                {
                                    if($v == ''){
                                        $v = '-';
                                    }

                                    // $rR .= "$k:: ";

                                    if(strpos($k, 't') == 3)
                                    {
                                        $rR .= '<b>' . trans('forms.comentarios') .'</b> '. nl2br($v) . '<br />';
                                    }
                                    elseif (strpos($k, 'r0') !== false && $k != 'r07')// && $k != 'r08' && $k != 'r09')
                                    {
                                        $rR .= '<b>' . trans('forms.'.$cuestionario->form.'.'.str_replace("r0", "p0", $k)) . '</b>: ' . trans('forms.'.$v) . '<br />';
                                    }
                                    elseif($k == 'r07')
                                    {
                                        $rR .= '<b>' . trans('forms.'.$cuestionario->form.'.'.  str_replace("r0", "p0", $k))  . '</b>: ' . nl2br($v) . '<br />';
                                    }
                                    else {
                                        $rR .= '<b>' . trans('forms.'.$cuestionario->form.'.'. str_replace("r0", "p0", $k)) . '</b>: ' . $v . '<br />';
                                    }
                                }
                            }

                            return $r ? $rR : '-';
                        })
                        ->addColumn('tutor2_resultado', function ($model) use ($cuestionario_id, $cuestionario) {

                            $t = $model->viajero->tutor2;
                            if (!$t) {
                                return "-";
                            }

                            $r = $model->getCuestionarioRespuestaTutor($cuestionario_id, $model->viajero->tutor2->id);
                            $rR = '-';
                            if ($r) {
                                $rRespuestas = json_decode($r->respuestas, true);
                                $rR = '';
                                foreach ($rRespuestas as $k => $v)
                                {
                                    if($v == ''){
                                        $v = '-';
                                    }

                                    $rR .= "$k:: ";

                                    if(strpos($k, 't') == 3)
                                    {
                                        $rR .= '<b>' . trans('forms.comentarios') .'</b> '. nl2br($v) . '<br />';
                                    }
                                    elseif (strpos($k, 'r0') !== false && $k != 'r07')// && $k != 'r08' && $k != 'r09')
                                    {
                                        $rR .= '<b>' . trans('forms.'.$cuestionario->form.'.'.str_replace("r0", "p0", $k)) . '</b>: ' . trans('forms.'.$v) . '<br />';
                                    }
                                    elseif($k == 'r07')
                                    {
                                        $rR .= '<b>' . trans('forms.'.$cuestionario->form.'.'.  str_replace("r0", "p0", $k))  . '</b>: ' . nl2br($v) . '<br />';
                                    }
                                    else {
                                        $rR .= '<b>' . trans('forms.'.$cuestionario->form.'.'. str_replace("r0", "p0", $k)) . '</b>: ' . $v . '<br />';
                                    }
                                }
                            }

                            return $r ? $rR : '-';
                        })
                        ->addColumn('options', function ($model) {
                            $ret = "";

                            return $ret;
                        })
                        ->searchColumns('viajero', 'curso')
                        ->orderColumns('viajero', 'curso', 'tutor1_estado', 'tutor1_resultado')
                        ->setAliasMapping()
                        ->setSearchStrip()->setOrderStrip()
                        ->make();
                }
            }
            if($cuestionario->form == 'padresjovenes') {

                $crespuestas = array();
                //$cr = collect($cuestionarioRespuestas);
                foreach ($cuestionarioRespuestas as $crr) {
                    //echo $crr['respuestas']->r01.'<br />';
                    $crespuestas[$crr['id']] = $crr['respuestas'];
                }

                $totalesRespuestas = array();
                foreach ($crespuestas as $k => $v) {
                    if($v != null || $v != '') {
                        foreach ($v as $rvk => $rvv) {
                            //print_r($rvk);
                            $totalesRespuestas[$rvk][] = $rvv;
                        }
                    }
                }

                if (Datatable::shouldHandle()) {
                    $bookings = Booking::whereIn('id', $cuestionarioRespuestasCol->pluck('booking_id')->toArray())->get();

                    $cuestionario_id = $cuestionario->id;

                    return Datatable::collection($bookings)
                        ->addColumn('viajero', function ($model) {
                            return "<a target='_blank' data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                        })
                        ->addColumn('curso', function ($model) {
                            return $model->curso->name;
                        })
                        ->addColumn('tutor1_resultado', function ($model) use ($cuestionario_id, $cuestionario) {

                            $t = $model->viajero->tutor1;
                            if (!$t) {
                                return "-";
                            }

                            $r = $model->getCuestionarioRespuestaTutor($cuestionario_id, $model->viajero->tutor1->id);
                            $rR = '-';
                            if ($r) {
                                $rRespuestas = json_decode($r->respuestas, true);
                                $rR = '';
                                foreach ($rRespuestas as $k => $v) {
                                    if($v == ''){
                                        $v = '-';
                                    }
                                    if (strpos($k, 'r0') !== false && $k != 'r06' && $k != 'r07' && $k != 'r08'){
                                        $rR .= '<b>' . trans('forms.'.$cuestionario->form.'.'.str_replace("r0", "p0", $k)) . '</b>: ' . trans('forms.'.$v) . '<br />';
                                    }
                                    elseif($k == 'r06')
                                    {
                                        $rR .= '<b>' . trans('forms.'.$cuestionario->form.'.'.  str_replace("r0", "p0", $k))  . '</b>: ' . nl2br($v) . '<br />';
                                    }
                                    else
                                    {
                                        $rR .= '<b>' . trans('forms.'.$cuestionario->form.'.'. str_replace("r0", "p0", $k)) . '</b>: ' . $v . '<br />';
                                    }
                                }
                            }

                            return $r ? $rR : '-';
                        })
                        ->addColumn('tutor2_resultado', function ($model) use ($cuestionario_id, $cuestionario) {

                            $t = $model->viajero->tutor2;
                            if (!$t) {
                                return "-";
                            }

                            $r = $model->getCuestionarioRespuestaTutor($cuestionario_id, $model->viajero->tutor2->id);
                            $rR = '-';
                            if ($r) {
                                $rRespuestas = json_decode($r->respuestas, true);
                                $rR = '';
                                foreach ($rRespuestas as $k => $v) {
                                    if($v == ''){
                                        $v = '-';
                                    }
                                    if (strpos($k, 'r0') !== false && $k != 'r06' && $k != 'r07' && $k != 'r08'){
                                        $rR .= '<b>' . trans('forms.'.$cuestionario->form.'.'.str_replace("r0", "p0", $k)) . '</b>: ' . trans('forms.'.$v) . '<br />';
                                    }
                                    elseif($k == 'r06')
                                    {
                                        $rR .= '<b>' . trans('forms.'.$cuestionario->form.'.'.  str_replace("r0", "p0", $k))  . '</b>: ' . nl2br($v) . '<br />';
                                    }
                                    else
                                    {
                                        $rR .= '<b>' . trans('forms.'.$cuestionario->form.'.'. str_replace("r0", "p0", $k)) . '</b>: ' . $v . '<br />';
                                    }
                                }
                            }

                            return $r ? $rR : '-';
                        })
                        ->searchColumns('viajero', 'curso')
                        ->orderColumns('viajero', 'curso', 'tutor1_estado', 'tutor1_resultado')
                        ->setAliasMapping()
                        ->setSearchStrip()->setOrderStrip()
                        ->make();
                }
            }
            if($cuestionario->form == 'testnivelmaxcamps') {
                if (Datatable::shouldHandle()) {

                    $bookings = Booking::whereIn('id', $cuestionarioRespuestasCol->pluck('booking_id')->toArray())->get();

                    $cuestionario_id = $cuestionario->id;

                    return Datatable::collection($bookings)
                        ->addColumn('viajero', function ($model) {
                            return "<a target='_blank' data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                        })
                        ->addColumn('curso', function ($model) {
                            return $model->curso->name;
                        })
                        ->addColumn('resultado', function($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            return $r?$r->resultado:'-';
                        })
                        ->addColumn('nivel', function($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            return $r?$r->nivel:'-';
                        })

                        ->searchColumns('viajero', 'curso')
                        ->orderColumns('viajero', 'curso', 'resultado', 'nivel')
                        ->setAliasMapping()
                        ->setSearchStrip()->setOrderStrip()
                        ->make();
                }
            }
            if($cuestionario->form == 'testnivelcoloniascic') {
                if (Datatable::shouldHandle()) {

                    $bookings = Booking::whereIn('id', $cuestionarioRespuestasCol->pluck('booking_id')->toArray())->get();

                    $cuestionario_id = $cuestionario->id;

                    /*
                     'escuela_curso' => 'Academic Year',
                                            'testname' => 'Test Name',
                                            'resultado'     => 'Multiple Choice Level',
                                            'txtlevel'     => 'WT Level',
                                            'nivel'         => 'Final CIC Level'
                     */

                    return Datatable::collection($bookings)
                        /*
                        ->addColumn('', function(){
                            return '+';
                        })
                        */
                        ->addColumn('viajeroname', function ($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if ($r) {
                                $rR = "<a target='_blank' href='". route('manage.system.cuestionarios.respuesta',$r->id) ."'>".$model->viajero->name."</a>";
                            }
                            if($r->estado == 3){
                                $rR = $model->viajero->name;
                            }
                            return isset($rR) ? $rR : '-';
                            //return "<a target='_blank' data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                        })
                        ->addColumn('viajerolastname', function ($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            $ape = $model->viajero->lastname ." ".$model->viajero->lastname2;
                            if ($r) {
                                $rR = $ape;
                            }
                            if($r->estado == 3){
                                $rR = $ape;
                            }
                            return isset($rR) ? $rR : '-';
                            //return "<a target='_blank' data-label='Ficha Viajero' href='" . route('manage.viajeros.ficha', $model->viajero->id) . "'>" . $model->viajero->full_name . "</a>";
                        })
                        ->addColumn('sexo', function($model) {
                            return $model->viajero->sexo_name;
                        })
                        ->addColumn('fechanac', function($model) {
                            return $model->viajero->fechanac;
                        })
                        ->addColumn('nivel_trinity', function($model) {
                            return $model->datos->trinity_nivel;
                        })
                        ->addColumn('trinity_any', function($model) {
                            return $model->datos->trinity_any;
                        })
                        ->addColumn('nivel_cic', function($model) {
                            return $model->viajero->datos->cic_nivel;
                        })
                        ->addColumn('escuela', function($model) {
                            return $model->datos->escuela;
                        })
                        ->addColumn('curso', function ($model) {
                            return $model->curso->name;
                        })
                        ->addColumn('convocatoria', function ($model) {
                            return $model->convocatoria->name;
                        })
                        ->addColumn('escuela_curso', function($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if ($r && $r->respuestas != '') {
                                $rRespuestas = json_decode($r->respuestas, true);
                                if($rRespuestas['datos']['escuela_curso'] != 0) {
                                    $rR = ConfigHelper::getCICAcademicYearTestColonias($rRespuestas['datos']['escuela_curso']);
                                }
                            }

                            return isset($rR) ? $rR : '-';
                        })
                        ->addColumn('testname', function ($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            
                            if ($r && $r->respuestas != '')
                            {
                                $rRespuestas = json_decode($r->respuestas, true);
                                if( isset($rRespuestas['datos']['testnumber']) && $rRespuestas['datos']['testnumber'] )
                                {
                                    $rR = $rRespuestas['datos']['testnumber'];

                                    if($rRespuestas['datos']['englishlevel'])
                                    {
                                        $rR .= ' ' . ConfigHelper::getCICenglishlevelTestColonias($rRespuestas['datos']['englishlevel']);
                                    }
                                }
                            }
                            return isset($rR) ? $rR : '-';
                        })
                        ->addColumn('choicelevel', function($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);

                            if ($r && $r->puntuaciones != '') {
                                $rPuntos = $r->puntuaciones;
                                if($rPuntos['choicelevel'] != '') {
                                    $rR = ConfigHelper::getCICNivelTestColonias($rPuntos['choicelevel']) . ' (' . $rPuntos['choicepoints'] . '/25)';
                                }
                            }
                            return isset($rR) ? $rR : '-';
                        })
                        ->addColumn('ptxt', function ($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if ($r && $r->respuestas != '') {
                                $rRespuestas = json_decode($r->respuestas, true);
                                if(isset($rRespuestas['respuesta']['ptxt'])) {
                                    $rR = nl2br($rRespuestas['respuesta']['ptxt']);
                                }
                            }
                            return isset($rR) ? $rR : '-';
                        })
                        ->addColumn('textlevel', function($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if ($r && $r->puntuaciones != '') {
                                $rPuntos = $r->puntuaciones;
                                if($rPuntos['textlevel'] != '') {
                                    $level = ConfigHelper::getCICNivelTestColonias($rPuntos['textlevel']);
                                }
                            }
                            return $r && isset($level) ? $level:'';
                        })
                        ->addColumn('nivel', function($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if($r && !is_null($r->nivel)) {
                                $level = ConfigHelper::getCICNivelTestColonias($r->nivel);
                            }
                            return $r && isset($level) ? $level:'';
                        })
                        ->addColumn('oralresult', function($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if ($r && $r->puntuaciones != '') {
                                $rPuntos = $r->puntuaciones;
                                if(isset($rPuntos['oralresult']) && $rPuntos['oralresult'] != '')
                                {
                                    $oralresult = $rPuntos['oralresult'];
                                }
                            }
                            return $r && isset($oralresult) ? $oralresult:'';
                        })
                        ->addColumn('trinity', function($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if ($r && $r->puntuaciones != '')
                            {
                                $rPuntos = $r->puntuaciones; 
                                if(isset($rPuntos['trinity']) && $rPuntos['trinity'] != '')
                                {
                                    $trinity = $rPuntos['trinity'];
                                }
                            }
                            return $r && isset($trinity) ? $trinity:'';
                        })
                        ->addColumn('editar', function($model) use ($cuestionario_id){
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if($r){
                                $boton = '<a class="btn btn-success editorbutton" data-booking="'.$r->booking_id.'">editar</a>';
                            }
                            return $r ? $boton: '-';
                        })

                        ->searchColumns('viajerolastname','viajeroname', 'curso')
                        ->orderColumns('viajerolastname','viajeroname', 'curso', 'escuela_curso','testname', 'resultado','txtlevel', 'nivel')
                        ->setAliasMapping()
                        ->setSearchStrip()->setOrderStrip()
                        ->make();
                }
            }

            if($cuestionario->form == 'opinionjovenes') {

                //dd($cuestionarioRespuestas);
                $crespuestas = array();
                //$cr = collect($cuestionarioRespuestas);
                foreach ($cuestionarioRespuestas as $crr) {
                    //echo $crr['respuestas']->r01.'<br />';
                    $crespuestas[$crr['id']] = $crr['respuestas'];
                }

                $totalesRespuestas = array();
                foreach ($crespuestas as $k => $v) {
                    if($v != null || $v != '') {
                        foreach ($v as $rvk => $rvv) {
                            //print_r($rvk);
                            $totalesRespuestas[$rvk][] = $rvv;
                        }
                    }
                }
                //ksort($totalesRespuestas);
                //dd($totalesRespuestas);

                if (Datatable::shouldHandle())
                {
                    $bookings = Booking::whereIn('id', $cuestionarioRespuestasCol->pluck('booking_id')->toArray())->get();

                    $cuestionario_id = $cuestionario->id;

                    $dtt = Datatable::collection($bookings)
                        ->addColumn('viajero', function ($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);

                            if(!$r)
                            {
                                return $model->viajero->full_name;
                            }

                            return "<a target='_blank' data-label='Ver Cuestionario' href='" . route('manage.system.cuestionarios.respuesta', $r->id) . "'>" . $model->viajero->full_name . "</a>";
                        })
                        ->addColumn('curso', function ($model) {
                            return $model->curso->name;
                        })
                        ->addColumn('convocatoria', function ($model) {
                            return $model->convocatoria->name;
                        })
                        ->addColumn('alojamiento', function ($model) {
                            return $model->alojamiento->tipo->name;
                        })
                        ->addColumn('r-habitacion', function ($model) use ($cuestionario_id) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if ($r && $r->respuestas != '') {
                                $rRespuestas = json_decode($r->respuestas, true);
                                if(isset($rRespuestas['respuesta']['r01'])) {
                                    $rR = nl2br($rRespuestas['respuesta']['r01']);
                                }
                            }
                            return isset($rR) ? $rR : '-';
                        });


                    for( $i=1; $i <= 8 ; $i++ ){
                        $column = 'r0'.$i;
                        $dtt->addColumn($column, function ($model) use ($cuestionario_id, $column, $i) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if ($r && $r->respuestas != '') {
                                $rRespuestas = json_decode($r->respuestas, true);
                                if (isset($rRespuestas[$column])) {
                                    $rR = nl2br($rRespuestas[$column]);
                                }
                            }
                            return isset($rR) ? $rR : '-';
                        });
                    }

                    for( $i=1; $i <= 9 ; $i++ ){
                        $column = 'f0'.$i;
                        $dtt->addColumn($column, function ($model) use ($cuestionario_id, $column, $i) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if ($r && $r->respuestas != '') {
                                $rRespuestas = json_decode($r->respuestas, true);
                                if (isset($rRespuestas[$column])) {
                                    $rR = nl2br($rRespuestas[$column]);
                                }
                            }
                            return isset($rR) ? $rR : '-';
                        });
                    }

                    for( $i=1; $i <= 16 ; $i++ ){
                        if($i < 10) {
                            $column = 'p' . str_pad($i, 2, "0", STR_PAD_LEFT);
                        }else{
                            $column = 'p' . $i;
                        }
                        $dtt->addColumn($column, function ($model) use ($cuestionario_id, $column, $i) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if ($r && $r->respuestas != '') {
                                $rRespuestas = json_decode($r->respuestas, true);
                                if (isset($rRespuestas[$column])) {
                                    $rR = nl2br($rRespuestas[$column]);
                                }
                            }
                            return isset($rR) ? $rR : '-';
                        });
                    }

                    for( $i=10; $i <= 13 ; $i++ ){
                        $column = 'f' . $i;
                        $dtt->addColumn($column, function ($model) use ($cuestionario_id, $column, $i) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if ($r && $r->respuestas != '') {
                                $rRespuestas = json_decode($r->respuestas, true);
                                if (isset($rRespuestas[$column])) {
                                    $rR = nl2br($rRespuestas[$column]);
                                    if($column == 'f11' && isset($rRespuestas['f11o']) ){
                                        $rR.= ': '.nl2br($rRespuestas['f11o']);
                                    }
                                }
                            }
                            return isset($rR) ? $rR : '-';
                        });
                    }

                    for( $i=17; $i <= 22 ; $i++ ){
                        $column = 'p' . $i;
                        $dtt->addColumn($column, function ($model) use ($cuestionario_id, $column, $i) {
                            $r = $model->getCuestionarioRespuesta($cuestionario_id);
                            if ($r && $r->respuestas != '') {
                                $rRespuestas = json_decode($r->respuestas, true);
                                if (isset($rRespuestas[$column])) {
                                    $rR = nl2br($rRespuestas[$column]);
                                }
                            }
                            return isset($rR) ? $rR : '-';
                        });
                    }

                    return $dtt->searchColumns('viajero', 'curso')
                        ->orderColumns('viajero', 'curso', 'convocatoria')
                        ->setAliasMapping()
                        ->setSearchStrip()->setOrderStrip()
                        ->make();


                    return view('manage.system.cuestionarios.informes', compact('valores','results','listado','cursos','cuestionarios', 'totales', 'cuestionario','cuestionarioRespuestasCol','crespuestas','totalesRespuestas', 'anys'));
                }
            }

        }
        return view('manage.system.cuestionarios.informes', compact('valores','results','listado','cursos','cuestionarios', 'totales', 'cuestionario','cuestionarioRespuestasCol','crespuestas','totalesRespuestas','anys'));


    }

    public function getRespuesta(Request $request, $id)
    {

        $resultados = CuestionarioRespuesta::findOrFail($id);
        $viajero = $resultados->booking->viajero;
        $tutor = null;

        if($resultados->tutor_id != null){
            $tutor = $viajero->tutores->where('id',$resultados->tutor_id)->first();
        }
        
        $cuestionario = $resultados->cuestionario;
        $form = $resultados->cuestionario->form;

        $test = json_decode($resultados->respuestas);
        if(isset($test->datos)) {
            $datos = $test->datos;
            $respuesta = $test->respuesta;
        }else{
            $datos = '';
            $respuesta = $test;
        }


        //La ruta según vea Raquel
        return view("manage.system.cuestionarios.forms.$form", compact('viajero','datos','respuesta','resultados', 'tutor', 'cuestionario'));
    }

    public function getRespuestaAjax(Request $request)
    {

        $booking_id = $request->get('booking_id');
        $cuestionario_id = $request->get('cuestionario_id');
        $resultados = CuestionarioRespuesta::where('booking_id',$booking_id)->where('cuestionario_id', $cuestionario_id)->first();
        $viajero = $resultados->booking->viajero;

        $test = Array();

        $d = json_decode($resultados->respuestas);
        if(isset($d->respuesta)) {
            $respuesta = $d->respuesta;
            if(isset($respuesta->ptxt)) {
                $test['ptxt'] = nl2br($respuesta->ptxt);
            }
        }
        if(isset($d->datos)){
            $datos = $d->datos;
            $test['test'] = $datos->testnumber;
            $test['testnumber'] = $datos->testnumber;
            if(isset($datos->englishlevel) && $datos->englishlevel != 0) {
                $test['test'] .= ' ' . ConfigHelper::getCICenglishlevelTestColonias($datos->englishlevel);
                $test['englishlevel'] = $datos->englishlevel;
            }

            $test['escuela_curso'] = $datos->escuela_curso;

        }

        $puntos = $resultados->puntuaciones;

        $test['choicepoints'] = $puntos['choicepoints'];
        $test['choicelevel'] = ConfigHelper::getCICNivelTestColonias($puntos['choicelevel']);
        $test['choicelevelnumber'] = $puntos['choicelevel'];
        $test['textlevel'] = $puntos['textlevel'];
        $test['nivel'] = $resultados->nivel;
        $test['oralresult'] = isset($puntos['oralresult']) ? $puntos['oralresult'] : "";
        $test['trinity'] = isset($puntos['trinity']) ? $puntos['trinity'] : "";
        $test['viajero'] = $viajero->full_name;
        $test['booking_id'] = $booking_id;
        $test['estado'] = $resultados->estado;


        return response()->json($test, 200);
    }

    public function getSaveAjax(Request $request)
    {
        $booking_id = $request->get('booking_id');
        $cuestionario_id = $request->get('cuestionario_id');
        $cuestionario = CuestionarioRespuesta::where('booking_id',$booking_id)->where('cuestionario_id', $cuestionario_id)->first();

        $puntuaciones = Array();
        $puntos = $cuestionario->puntuaciones;

        $puntuaciones['choicelevel'] = $puntos['choicelevel'];
        $puntuaciones['choicepoints'] = $puntos['choicepoints'];
        $puntuaciones['textlevel'] = $request->get('textlevel');
        $puntuaciones['oralresult'] = $request->get('oralresult');
        $puntuaciones['trinity'] = $request->get('trinity');

        $cuestionario->puntuaciones = $puntuaciones;
        $cuestionario->nivel = $request->get('nivel');

        $cuestionario->estado = 2;

        $cuestionario->save();
    }

    public function getSaveOfflineAjax(Request $request)
    {
        $booking_id = $request->get('booking_id');
        $cuestionario_id = $request->get('cuestionario_id');
        $cuestionario = CuestionarioRespuesta::where('booking_id',$booking_id)->where('cuestionario_id', $cuestionario_id)->first();



        $puntuaciones = Array();
        //$puntos = $cuestionario->puntuaciones;


        $puntuaciones['choicelevel'] = $request->get('choicelevel');
        $puntuaciones['choicepoints'] = $request->get('choicepoints');
        $puntuaciones['textlevel'] = $request->get('textlevel');
        $puntuaciones['oralresult'] = $request->get('oralresult');
        $puntuaciones['trinity'] = $request->get('trinity');

        $cuestionario->puntuaciones = $puntuaciones;
        $cuestionario->nivel = $request->get('nivel');

        $datos['escuela_curso'] = $request->get('escuela_curso');
        $datos['englishlevel'] = $request->get('englishlevel');
        $datos['testnumber'] =$request->get('testnumber');
        $datos['booking'] = $booking_id;


        $test['datos'] = $datos;
        $ptxt = 'offline';
        $test['respuesta'] = json_encode($ptxt);
        $testnivel = json_encode($test);
        $cuestionario->respuestas = $testnivel;

        $cuestionario->resultado = 0;

        $cuestionario->estado = 3;

        $cuestionario->save();
    }

    public function getCambiarEstadoAjax(Request $request)
    {

        $booking_id = $request->get('booking_id');
        $cuestionario_id = $request->get('cuestionario_id');
        $resultados = CuestionarioRespuesta::where('booking_id',$booking_id)->where('cuestionario_id', $cuestionario_id)->first();
        if(count($resultados)) {
            $respuesta['booking_id'] = $booking_id;
            $respuesta['viajero'] = $resultados->booking->viajero->full_name ?? "";
            $respuesta['estado'] = $resultados->estado;
            $respuesta['estadoactual'] = ConfigHelper::areaFormEstado($resultados->estado);
        }else{
            $booking = Booking::where('id',$booking_id)->first();
            $respuesta['booking_id'] = $booking_id;
            $respuesta['viajero'] = $booking->viajero->full_name;
            $respuesta['estado'] = 0;
            $respuesta['estadoactual'] = ConfigHelper::areaFormEstado(0);
        }

        return response()->json($respuesta, 200);

    }

    public function getCambiarEstadoSaveAjax(Request $request)
    {

        $booking_id = $request->get('booking_id');
        $cuestionario_id = $request->get('cuestionario_id');
        $estado = $request->get('estado');
        $cuestionario = CuestionarioRespuesta::where('booking_id',$booking_id)->where('cuestionario_id', $cuestionario_id)->first();
        if(count($cuestionario)) {
            if($estado != 0) {
                $cuestionario->estado = $estado;
                $cuestionario->save();
            }else{
                $cuestionario->delete();
            }
        }else {
            $r = new CuestionarioRespuesta;
            $booking = Booking::find($booking_id);
            $r->cuestionario_id = $cuestionario_id;
            $r->booking_id = $booking_id;
            $r->estado = $estado;
            $r->cuestionario_id = $cuestionario_id;
            $r->booking_id = $booking_id;

            $r->viajero_id = $booking->viajero->id;
            //$r->tutor_id = $ficha->id;

            if ($estado != 0){
                $r->save();
            }else{
                return false;
            }
        }


    }
}
