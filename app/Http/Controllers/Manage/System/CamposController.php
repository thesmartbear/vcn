<?php

namespace VCN\Http\Controllers\Manage\System;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\System\Campo;

use Datatable;
use ConfigHelper;

class CamposController extends Controller
{
    public function __construct()
    {
        $this->checkPermisosFullAdmin();
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            $list = Campo::all();

            return Datatable::collection( $list )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.system.campos.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->showColumns('nombre')
                ->addColumn('tipo', function($model) {
                    return ConfigHelper::getTipoCampo($model->tipo);
                })
                ->addColumn('input', function($model) {
                    if($model->tipo)
                    {
                        return $model->textarea?"Textarea":"Texto";
                    }

                    return "-";
                })
                ->showColumns('modelo')
                ->searchColumns('name','modelo')
                ->orderColumns('name','modelo')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.system.campos.index');
    }

    public function getUpdate(Request $request, $id)
    {
        $ficha = Campo::findOrFail($id);

        return view('manage.system.campos.ficha', compact('ficha'));
    }

    public function postUpdate(Request $request, $id)
    {
        // $this->validate($request, [
        // ]);

        $data = $request->all();

        $data['traducible'] = $request->has('traducible');

        $plataforma = Campo::findOrFail($id);
        $plataforma->update($data);

        // Session::flash();
        return redirect()->route('manage.system.campos.index');
    }
}
