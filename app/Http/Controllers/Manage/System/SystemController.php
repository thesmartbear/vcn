<?php

namespace VCN\Http\Controllers\Manage\System;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\UserRepository as User;
use VCN\Repositories\System\UserRoleRepository as Role;
use VCN\Repositories\System\OficinaRepository as Oficina;

use VCN\Models\Leads\ViajeroLog;
use VCN\Models\Provincia;
use VCN\Models\Pais;
use VCN\Models\Bookings\Booking;

use VCN\Models\System\Contacto;
use VCN\Models\System\ContactoModel;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Carbon;
use Route;
use Session;
use Auth;

class SystemController extends Controller
{
    private $user;
    private $role;
    private $oficina;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct(Request $request, User $user, Role $role, Oficina $oficina )
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');

        if($request && $request->route() && $request->route()->action['as'] == "manage.system.admins.logout")
        {

        }
        elseif($request && $request->route() && $request->route()->action['as'] == "manage.system.admins.login")
        {
            $this->middleware("permiso.edit:superlogin");
        }
        else
        {
            // $this->checkPermisosFullAdmin();
            $sos = ['getContactosIndex','getContactosUpdate','postContactosUpdate','destroyContactos','postContactosAsignar'];
            $this->middleware("permiso.edit:full-admin", ['except' => $sos]);
            $this->middleware("permiso.view:full-admin", ['except' => $sos]);
            $this->middleware("permiso.edit:sos", ['only' => $sos]);
            $this->middleware("permiso.view:sos", ['only' => $sos]);
        }

        $this->user = $user;
        $this->role = $role;
        $this->oficina = $oficina;
    }

    public function getAdminsIndex(Request $request)
    {
        $user = $request->user();

        if(Datatable::shouldHandle())
        {
            $admins = $this->user->findWhere([ ['roleid','<>',11], ['roleid','<>',12], ['roleid','<>',13], ['roleid','<>',0] ]);

            return Datatable::collection( $admins )
                ->showColumns('id')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.system.admins.ficha',[$model->id]) ."'>$model->full_name</a>";
                })
                ->addColumn('username', function($model) use ($user) {
                    $ret = $model->username . " (". ConfigHelper::plataforma($model->plataforma) .")";

                    if(ConfigHelper::canEdit('superlogin'))
                    {
                        $ret .= "<a class='pull-right' href='". route('manage.system.admins.login', $model->id)  ."'><i class='fa fa-sign-in'></i></a>";
                    }

                    return $ret;
                })
                ->addColumn('role', function($model) {
                    return $model->role_name;
                })
                ->addColumn('oficina', function($model) {
                    return $model->oficina_name;
                })
                ->addColumn('estado', function($model) {
                    return $model->status?'Activo':'Inactivo';
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Usuario' data-action='". route( 'manage.system.admins.delete', $model->id) . "'";
                    $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name','username')
                ->orderColumns('name','*')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.system.admins.index');
    }

    public function getAdminsActivity($id)
    {
        $ficha = $this->user->find($id);

        if(Datatable::shouldHandle())
        {
            $actividad = $ficha->actividad->sortByDesc('fecha');

            return Datatable::collection( $actividad )
                ->addColumn('fecha', function($model) {
                    return $model->fecha->format('Y-m-d');
                })
                ->showColumns('hora_ini','hora_fin','desktop_hora_ini','desktop_hora_fin','mobile_hora_ini','mobile_hora_fin','tablet_hora_ini','tablet_hora_fin')
                ->addColumn('inactividad', function($model) {
                    $a = $model->desktop_inactividad_max?round($model->desktop_inactividad_max/60,2):0;
                    // $b = $model->desktop_inactividad?round($model->desktop_inactividad/60,2):0;
                    return $a;
                })
                ->searchColumns('fecha')
                ->orderColumns('fecha')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        abort(404);
    }

    public function getAdminsNuevo()
    {
        $roles = ["1"=>"Full-Admin"] + \VCN\Models\System\UserRole::all()->pluck('name','id')->toArray();
        $plataformas = ConfigHelper::config('vcn.plataformas');
        $oficinas = [0=>""] + $this->oficina->all()->pluck('name','id')->toArray();

        return view('manage.system.admins.new', compact('roles','plataformas','oficinas'));
    }

    public function getAdminsUpdate($id)
    {
        $ficha = $this->user->find($id);

        $roles = ["1"=>"Full-Admin"] + \VCN\Models\System\UserRole::all()->pluck('name','id')->toArray();
        $plataformas = ConfigHelper::config('vcn.plataformas');

        $oficinas = [0=>""] + $this->oficina->all()->pluck('name','id')->toArray();

        return view('manage.system.admins.ficha', compact('ficha', 'roles','plataformas','oficinas'));
    }

    public function postAdminsUpdate(Request $request, $id)
    {
        $data = $request->except('_token','password1','password2');

        $data['status'] = $request->has('status');
        $data['chat_admin'] = $request->has('chat_admin');

        if($request->input('password1')!="")
        {
            $this->validate($request, [
                'password1' => 'same:password2',
            ]);

            $data['password'] = bcrypt($request->input('password1'));
        }

        if(!$id)
        {
            $this->validate($request, [
                'password1' => 'required|same:password2',
                'fname' => 'required',
                'username' => 'required|unique:users',
                'email' => 'required|unique:users',
                'plataforma' => 'required|min:1',
            ]);

            $o = $this->user->create($data);
            $id = $o->id;
        }
        else
        {
            $this->user->update($data, $id);
        }

        $asign = $this->user->find($id);
        if($request->hasFile('avatar'))
        {
            $file = $request->file('avatar');
            $dirp = "assets/uploads/users/". $id . "/";

            $avatar = ConfigHelper::uploadFoto($file, $dirp, 400, true);
            $asign->avatar = $avatar;
            $asign->save();
        }

        if($asign->oficina_id)
        {
            foreach($asign->viajeros->where('oficina_id',null) as $v)
            {
                $oficina_id = $request->input('oficina_id');
                $oficina = $this->oficina->find($oficina_id);
                ViajeroLog::addLogOficina($v, $oficina );

                $v->oficina_id = $asign->oficina_id;
                $v->save();
            }
        }

        return redirect()->route('manage.system.admins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroyAdmin($id)
    {
        $this->user->delete($id);
        return redirect()->route('manage.system.admins.index');
    }


    /**
     * UserRole
     */

    public function getRolesIndex()
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection( $this->role->all() )
                ->showColumns('id')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.system.roles.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Permiso' data-action='". route( 'manage.system.roles.delete', $model->id) . "'";
                    $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','*')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.system.roles.index');
    }

    public function getRolesNuevo()
    {
        $permisos = config('vcn.permisos');

        return view('manage.system.roles.new', compact('permisos'));
    }

    public function getRolesUpdate($id)
    {
        $ficha = $this->role->find($id);

        $permisos = config('vcn.permisos');
        $permisos_user = json_decode($ficha->permisos);

        // dd($permisos_user);

        $informes_bloques = config('vcn.permisos_informes');

        $informes_menu = config('vcn.menu.manager')[6]['submenu'];

        foreach($informes_menu as $kmenu => $menu)
        {
            if(!isset($menu['submenu']))
            {
                continue;
            }

            foreach($menu['submenu'] as $km => $m)
            {
                if(strpos($m['route'],'manage.informes') === false)
                {
                    unset($informes_menu[$kmenu]['submenu'][$km]);
                }
            }
        }

        return view('manage.system.roles.ficha', compact('ficha','permisos','permisos_user','informes_bloques', 'informes_menu'));
    }

    public function postRolesUpdate(Request $request, $id)
    {
        $data = $request->except('_token','view','edit','delete','informes');

        if(!$id)
        {
            $this->validate($request, [
                'name' => 'required|unique:user_roles',
            ]);

            $ficha = $this->role->create($data);
        }
        else
        {
            $this->role->update($data, $id);
            $ficha = $this->role->find($id);
        }

        $data = $request->except('_token');

        $permisos = config('vcn.permisos');
        $p = [];
        foreach($permisos as $p1)
        {
            foreach($p1 as $permiso)
            {
                $p[$permiso] = [];
                $p[$permiso]['view'] = (isset($data['view'][$permiso])?$data['view'][$permiso]:0);
                $p[$permiso]['edit'] = (isset($data['edit'][$permiso])?$data['edit'][$permiso]:0);
            }
        }

        // dd($data);

        //informes
        $informes_bloques = config('vcn.permisos_informes');
        $permiso = 'informes';
        $p[$permiso]['bloques'] = [];
        foreach($informes_bloques as $bloque)
        {
            $p[$permiso]['bloques'][$bloque] = [];
            $p[$permiso]['bloques'][$bloque]['view'] = (isset($data['informes'][$bloque]['view'])?$data['informes'][$bloque]['view']:0);
            $p[$permiso]['bloques'][$bloque]['detalles'] = (isset($data['informes'][$bloque]['detalles'])?$data['informes'][$bloque]['detalles']:[]);
        }
        // dd($p[$permiso]);

        $ficha->aislado = $request->has('aislado');
        $ficha->ver_oficinas_informes = $request->has('ver_oficinas_informes');

        $ficha->permisos = json_encode($p);
        $ficha->save();

        return redirect()->route('manage.system.roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroyRole($id)
    {
        $this->role->delete($id);
        return redirect()->route('manage.system.roles.index');
    }

    /**
     * Oficinas
     */
    public function getOficinasIndex()
    {
        if(Datatable::shouldHandle())
        {
            return Datatable::collection( $this->oficina->all() )
                ->showColumns('id')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.system.oficinas.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('plataforma', function($model) {
                    return ConfigHelper::plataforma($model->propietario);
                })
                ->addColumn('activa', function($model) {
                    return $model->activa?"<span class='label label-success'><i class='fa fa-check-circle fa-1x'></i></span>":"<span class='label label-danger'><i class='fa fa-times-circle fa-1x'></i></span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Oficina' data-action='". route( 'manage.system.oficinas.delete', $model->id) . "'";
                    $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','*')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.system.oficinas.index');
    }

    public function getOficinasJSON(Request $request)
    {
        //filtrar por plataforma
        $plataforma = $request->input('plataforma');
        if($plataforma>0)
        {
            return response()->json($this->oficina->findWhere(['propietario'=>$plataforma])->pluck('name','id'), 200);
        }
    }

    public function getOficinasNuevo()
    {
        $provincias = [""=>""] + Provincia::all()->pluck('name','id')->toArray();
        $paises = [""=>""] + Pais::all()->pluck('name','id')->toArray();

        return view('manage.system.oficinas.new', compact('provincias','paises'));
    }

    public function getOficinasUpdate($id)
    {
        $ficha = $this->oficina->find($id);

        $provincias = [""=>""] + Provincia::all()->pluck('name','id')->toArray();
        $paises = [""=>""] + Pais::all()->pluck('name','id')->toArray();

        return view('manage.system.oficinas.ficha', compact('ficha','provincias','paises'));
    }

    public function postOficinasUpdate(Request $request, $id)
    {
        $data = $request->except('_token','tab');

        $this->validate($request, [
            'name' => 'required',
        ]);

        $data['activa'] = $request->has('activa');
        $data['ver_otras'] = $request->has('ver_otras');
        $data['ver_otras_informes'] = $request->has('ver_otras_informes');

        if(!$id)
        {
            $ficha = $this->oficina->create($data);
        }
        else
        {
            $this->oficina->update($data, $id);
            $ficha = $this->oficina->find($id);
        }

        return redirect()->route('manage.system.oficinas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroyOficina($id)
    {
        $this->oficina->delete($id);
        return redirect()->route('manage.system.oficinas.index');
    }

    //contactos sos
    public function getContactosIndex(Request $request, $esProveedor=false)
    {
        if(Datatable::shouldHandle())
        {

            $list = Contacto::where('es_proveedor', $esProveedor)->get();

            return Datatable::collection( $list )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.system.contactos.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->showColumns('phone')
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Contacto SOS' data-action='". route( 'manage.system.contactos.delete', $model->id) . "'";
                    $ret .= "<a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','*')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.system.contactos.index', compact('esProveedor'));
    }

    public function getContactosUpdate(Request $request, $id=0)
    {
        if(!$id)
        {
            return view('manage.system.contactos.new');
        }

        $ficha = Contacto::find($id);

        return view('manage.system.contactos.ficha', compact('ficha'));
    }


    public function postContactosUpdate(Request $request, $id)
    {
        $c = Contacto::find($id);
        if(!$id)
        {
            $c = new Contacto;
        }
        
        $c->name = $request->get('contacto_name');
        $c->phone = $request->get('contacto_phone');
        $c->es_proveedor = $request->has('es_proveedor');
        $c->activo = $request->has('activo');
        $c->save();

        if($request->get('es_tab'))
        {
            Session::flash('tab', '#contactos_sos');
            return redirect()->back();
        }

        return redirect()->route('manage.system.contactos.index', $c->es_proveedor);
    }

    public function destroyContactos(Request $request, $id)
    {
        $c = Contacto::find($id);
        $es_proveedor = $c->es_proveedor;
        $c->delete();

        return redirect()->route('manage.system.contactos.index', $es_proveedor);
    }

    public function postContactosAsignar(Request $request )
    {
        $modelo = $request->get('modelo');
        $modelo_id = $request->get('modelo_id');

        $contactos = $request->get('contacto');
        $notas = $request->get('notas');

        //Proveedor
        if(isset($contactos['p']))
        {
            ContactoModel::where('es_proveedor', 1)->where('modelo', $modelo)->where('modelo_id', $modelo_id)->whereNotIn('contacto_id', $contactos['p'])->delete();
            foreach($contactos['p'] as $contacto_id)
            {
                if(!$contacto_id)
                {
                    continue;
                }
                
                $c = ContactoModel::where('es_proveedor', 1)->where('modelo', $modelo)->where('modelo_id', $modelo_id)
                        ->where('contacto_id', $contacto_id)->first();

                if(!$c)
                {
                    $c = new ContactoModel;
                    $c->modelo = $modelo;
                    $c->modelo_id = $modelo_id;
                    $c->es_proveedor = 1;
                }

                $c->contacto_id = $contacto_id;
                $c->notas = isset($notas['p']) ? $notas['p'] : "";
                $c->save();
            }
        }
        else
        {
            ContactoModel::where('es_proveedor', 1)->where('modelo', $modelo)->where('modelo_id', $modelo_id)->delete();
        }

        foreach(ConfigHelper::plataformas() as $p=>$plataforma)
        {
            $contacto_id = (int)$contactos[$p];
            $c = ContactoModel::where('es_proveedor', 0)->where('plataforma', $p)->where('modelo', $modelo)->where('modelo_id', $modelo_id)->first();
        
            if($contacto_id)
            {
                if(!$c)
                {
                    $c = new ContactoModel;
                    $c->modelo = $modelo;
                    $c->modelo_id = $modelo_id;
                    $c->es_proveedor = 0;
                    $c->plataforma = $p;
                }

                $c->contacto_id = $contacto_id;
                $c->notas = isset($notas['plat']) ? $notas['plat'] : "";
                $c->save();
            }
            else
            {
                if($c)
                {
                    $c->delete();
                }
            }
        }

        //Update bookings by Model : convocatoria, curso, centro, proveedor

        ini_set('memory_limit', '400M');
        set_time_limit(0);

        $bookings = Booking::where('status_id','>',0)->where('course_end_date','>', Carbon::now());
        $bBookings = true;
        
        switch($modelo)
        {
            case 'Abierta':
            {
                $filtro = "convocatory_open_id";
                $bookings = $bookings->where($filtro, $modelo_id);
            }
            break;

            case 'Cerrada':
            {
                $filtro = "convocatory_close_id";
                $bookings = $bookings->where($filtro, $modelo_id);
            }
            break;

            case 'ConvocatoriaMulti':
            {
                $filtro = "convocatory_multi_id";
                $bookings = $bookings->where($filtro, $modelo_id);
            }
            break;

            case 'Curso':
            {
                $filtro = "curso_id";
                $bookings = $bookings->where($filtro, $modelo_id);
            }
            break;

            case 'Centro':
            {
                $filtro = "center_id";
                $cursos = \VCN\Models\Cursos\Curso::where('center_id',$modelo_id)->pluck('id')->toArray();
                $bookings = $bookings->whereIn("curso_id", $cursos);
            }
            break;

            case 'Proveedor':
            {
                $filtro = "proveedor_id";
                $bookings = $bookings->where($filtro, $modelo_id);
            }
            break;

            default:
            {
                $bBookings = false;
            }
            break;
        }

        $n = 0;
        $txt = "";

        if($bBookings)
        {
            foreach($bookings->get() as $b)
            {
                $b->setContactosSOS();
                $n++;
                $txt .= "$b->id,";
            }
        }

        Session::flash('mensaje-ok', "Se han actualizado $n Bookings [$txt]");

        Session::flash('tab', '#contactos_sos');
        return redirect()->back();

    }

    public function loginUser(Request $request, int $user_id)
    {
        $user = $request->user();
        $u = $this->user->find($user_id);

        if(!$u)
        {
            return redirect()->back();
        }

        $url = $request->get('url');
        
        Session::put('vcn.admin', $user->id);
        Session::put('vcn.admin_url', $url);
        Auth::login($u);

        return redirect()->route('manage.index');
    }

    public function logoutUser(Request $request)
    {
        $user = $request->user();
        $admin_id = Session::get('vcn.admin');
        $u = $this->user->find($admin_id);

        if(!$u)
        {
            return redirect()->back();
        }

        $r = Session::get('vcn.admin_url');
        
        Session::forget('vcn.admin');
        Session::forget('vcn.admin_url');
        Auth::login($u);

        // return redirect()->route('manage.index');
        return redirect()->to($r);
    }
}
