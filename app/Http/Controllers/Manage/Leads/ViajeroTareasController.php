<?php

namespace VCN\Http\Controllers\Manage\Leads;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Leads\ViajeroTareaRepository as ViajeroTarea;

use VCN\Models\User;
use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\ViajeroLog;
use VCN\Models\Solicitudes\Solicitud;

use VCN\Helpers\ConfigHelper;
use VCN\Helpers\MailHelper;

use Datatable;
use Session;
use Carbon;
use DB;

use VCN\Repositories\Criteria\FiltroPlataformaAsignado;
use VCN\Repositories\Criteria\FiltroLogueadoAsignado;

class ViajeroTareasController extends Controller
{
    private $tarea;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( ViajeroTarea $tarea )
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');

        $this->tarea = $tarea;
    }

    public function getIndex($viajero_id=0)
    {
        if(Datatable::shouldHandle())
        {
            if($viajero_id)
            {
                $col = $this->tarea->findAllBy('viajero_id',$viajero_id)->sortBy('fecha');
            }
            else
            {
                $col = $this->tarea->all();
            }

            return Datatable::collection( $col )
                ->addColumn('fecha', function($model) {
                    return "<a href='". route('manage.viajeros.tareas.ficha',[$model->id]) ."'>". Carbon::parse($model->fecha)->format('d/m/Y - H:i') ."</a>";
                    // return $model->created_at->format('d/m/Y');
                })
                ->showColumns('tipo','notas')
                ->addColumn('usuario', function($model) {
                    return $model->user?$model->user->full_name:"-";
                })
                ->addColumn('asignado', function($model) {
                    return $model->asignado?$model->asignado->full_name:"-";
                })
                ->addColumn('estado', function($model) {

                    $ret = "";

                    if(!$model->estado)
                    {
                        $ret = "<i class='fa fa-clock-o'></i>";

                        $ret .= "<span class='pull-right'>";
                        $ret .= " <a data-label='Completar' href='". route('manage.viajeros.tareas.completar',$model->id) ."' class='btn btn-success btn-xs'><i class='fa fa-check'></i></a>";
                        $ret .= "</span>";
                    }
                    else
                    {
                        $ret = "<i class='fa fa-check'></i>";

                        $ret .= "<span class='pull-right'>";
                        $ret .= " <a data-label='Incompleta' href='". route('manage.viajeros.tareas.completar',[$model->id,0]) ."' class='btn btn-danger btn-xs'><i class='fa fa-clock-o'></i></a>";
                        $ret .= "</span>";
                    }

                    return $ret;
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-get='". route( 'manage.viajeros.tareas.ajax.ficha', $model->id) . "' data-action='". route( 'manage.viajeros.tareas.ficha', $model->id) . "'";
                    $ret .= " <a href='#tarea-edit' $data class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Editar</a>";

                    // $ret .= "<a href='". route('manage.viajeros.tareas.ficha',[$model->id]) ."' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Editar</a>";

                    return $ret;
                })
                ->searchColumns('tipo','notas')
                ->orderColumns('fecha','notas')
                ->setAliasMapping()
                ->make();
        }

        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        return view('manage.viajeros.tareas.index', compact('viajero_id','asignados'));
    }

    public function getIndexResumen(Request $request, $filtro=0,$user_id=0,$oficina_id=0)
    {
        $this->tarea->pushCriteria(new FiltroPlataformaAsignado($user_id));
        $this->tarea->pushCriteria(new FiltroLogueadoAsignado($user_id,$oficina_id)); 

        /* $solicitudes = [];
        switch($user_id)
        {
            case 0:
            {
                $u = $request->user()->id;
                $solicitudes = Solicitud::where('user_id',$u)->get();
            }
            break;

            case 'null':
            {
                $solicitudes = Solicitud::where('user_id',null)->get();
            }
            break;

            case 'all':
            {
                $solicitudes = Solicitud::all();

                $ofi = User::asignados()->where('oficina_id',$oficina_id)->first();
                if($ofi)
                {
                    $solicitudes = Solicitud::where('user_id',$ofi->id)->get();
                }
            }
            break;

            case 'all-off':
            {
                $solicitudes = Solicitud::all();

                $ofi = User::asignadosOff()->where('oficina_id',$oficina_id)->first();
                if($ofi)
                {
                    $solicitudes = Solicitud::where('user_id',$ofi->id)->get();
                }
            }
            break;

            default:
            {
                $solicitudes = Solicitud::where('user_id',$user_id)->get();
            }
            break;
        } */

        if(Datatable::shouldHandle())
        {
            switch($filtro)
            {
                case 1: //caducadas
                {
                    $col = $this->tarea->findWhere([ 'estado'=>0,
                        [DB::raw("DATE_FORMAT(fecha,'%Y-%m-%d')"), '<', Carbon::today()->format('Y-m-d')] ]);

                    /* $tareas = collect([]);
                    foreach($solicitudes as $sol)
                    {
                        $t = $sol->tareas->filter(function ($item) {
                            return Carbon::parse($item->fecha)->format('Y-m-d') < Carbon::today()->format('Y-m-d');
                        });

                        $tareas = $tareas->merge($t->where('estado',0));
                    } */
                }
                break;

                case 2: //hoy
                {
                    $col = $this->tarea->findWhere([ 'estado'=>0,
                        [DB::raw("DATE_FORMAT(fecha,'%Y-%m-%d')"), Carbon::today()->format('Y-m-d')] ]);

                    /* $tareas = collect([]);
                    foreach($solicitudes as $sol)
                    {
                        $t = $sol->tareas->filter(function ($item) {
                            return Carbon::parse($item->fecha)->format('Y-m-d') == Carbon::today()->format('Y-m-d');
                        });

                        $tareas = $tareas->merge($t->where('estado',0));
                    } */
                }
                break;

                case 3: //esta semana
                {
                    $col = $this->tarea->findWhere([ 'estado'=>0,
                            [DB::raw("DATE_FORMAT(fecha,'%Y-%m-%d')"), '>', Carbon::today()->format('Y-m-d')],
                            [DB::raw("DATE_FORMAT(fecha,'%Y-%m-%d')"), '<=', Carbon::today()->addDays(6)->format('Y-m-d')]
                        ]);

                    /* $tareas = collect([]);
                    foreach($solicitudes as $sol)
                    {
                        $t = $sol->tareas->filter(function ($item) {
                            return (Carbon::parse($item->fecha)->format('Y-m-d') > Carbon::today()->format('Y-m-d') && Carbon::parse($item->fecha)->format('Y-m-d') <= Carbon::today()->addDays(6)->format('Y-m-d'));
                        });

                        $tareas = $tareas->merge($t->where('estado',0));
                    } */
                }
                break;

                case 4: //más adelante
                {
                    $col = $this->tarea->findWhere([ 'estado'=>0,
                            [DB::raw("DATE_FORMAT(fecha,'%Y-%m-%d')"), '>=', Carbon::today()->addDays(7)->format('Y-m-d')]
                        ]);

                    /* $tareas = collect([]);
                    foreach($solicitudes as $sol)
                    {
                        $t = $sol->tareas->filter(function ($item) {
                            return Carbon::parse($item->fecha)->format('Y-m-d') >= Carbon::today()->addDays(7)->format('Y-m-d');
                        });

                        $tareas = $tareas->merge($t->where('estado',0));
                    } */
                }
                break;

                case 0:
                default:
                {
                    $col = $this->tarea->all();

                    /* $tareas = collect([]);
                    foreach($solicitudes as $sol)
                    {
                        $t = $sol->tareas;

                        $tareas = $tareas->merge($t);
                    } */
                }
                break;
            }

            // $col = $col->merge($tareas);
            // $col = $col->unique();

            return Datatable::collection( $col )
                ->addColumn('fecha', function($model) {
                    return $model->fecha;
                })
                ->addColumn('viajero', function($model) {
                    return "<a href='". route('manage.viajeros.ficha',[$model->viajero->id]) ."'>". $model->viajero->full_name ."</a>";
                })
                ->showColumns('tipo','notas')
                ->addColumn('usuario', function($model) {
                    return $model->user?$model->user->full_name:"-";
                })
                ->addColumn('asignado', function($model) {
                    return $model->asignado?$model->asignado->full_name:"-";
                })
                ->addColumn('rating', function($model) {
                    return $model->viajero->rating;
                })
                ->addColumn('estado', function($model) {

                    $ret = "";

                    if(!$model->estado)
                    {
                        $ret = "<i class='fa fa-clock-o'></i>";

                        $ret .= "<span class='pull-right'>";
                        $ret .= " <a data-label='Completar' href='". route('manage.viajeros.tareas.completar',[$model->id,1,1]) ."' class='btn btn-success btn-xs'><i class='fa fa-check'></i></a>";
                        $ret .= "</span>";
                    }
                    else
                    {
                        $ret = "<i class='fa fa-check'></i>";

                        $ret .= "<span class='pull-right'>";
                        $ret .= " <a data-label='Incompleta' href='". route('manage.viajeros.tareas.completar',[$model->id,0,1]) ."' class='btn btn-danger btn-xs'><i class='fa fa-clock-o'></i></a>";
                        $ret .= "</span>";
                    }

                    return $ret;
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Tarea' data-action='". route( 'manage.viajeros.tareas.delete', $model->id) . "'";
                    // $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= "<a href='". route('manage.viajeros.tareas.ficha',[$model->id]) ."' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Editar</a>";
                    $data = " data-home='1' data-get='". route( 'manage.viajeros.tareas.ajax.ficha', $model->id) . "' data-action='". route( 'manage.viajeros.tareas.ficha', $model->id) . "'";
                    $ret .= " <a href='#tarea-edit' $data class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Editar</a>";


                    return $ret;
                })
                ->searchColumns('tipo','notas','viajero')
                ->orderColumns('fecha','notas')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

    }

    public function getNuevo($viajero_id)
    {
        $tipos = ConfigHelper::getTipoTarea();

        $viajero = Viajero::find($viajero_id)->full_name;
        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        return view('manage.viajeros.tareas.new', compact('viajero_id', 'viajero','tipos','asignados'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->tarea->find($id);

        if(!$ficha)
        {
            return back();
        }

        $tipos = ConfigHelper::getTipoTarea();

        $viajero_id = $ficha->viajero_id;
        $viajero = Viajero::find($viajero_id)->full_name;
        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        return view('manage.viajeros.tareas.ficha', compact('ficha','viajero_id', 'viajero','tipos','asignados'));
    }

    public function ajaxGetUpdate($id)
    {
        $ficha = $this->tarea->find($id);

        $viajero_id = $ficha->viajero_id;
        $viajero = Viajero::find($viajero_id);

        $ficha['hora'] = substr($ficha->fecha, 11,5);
        $ficha['fecha'] = Carbon::parse($ficha->fecha)->format('d/m/Y');

        $result = ['id'=> $id, 'tarea'=> $ficha, 'viajero'=> $viajero, 'viajero_fullname'=> $viajero->full_name, 'result'=> true ];
        return response()->json($result, 200);

    }


    public function postUpdate(Request $request, $id=0)
    {
        Session::flash('tab','#tareas');
        if( $request->input('home')==1 )
        {
            Session::flash('tab','#tasks');
        }

        $this->validate($request, [
            'tarea_fecha'=> 'required',
            'tarea_hora'=> 'required',
            'tipo' => 'required',
        ]);

        $data = $request->except('_token','home','tarea_fecha','tarea_hora');
        $fecha = Carbon::createFromFormat('d/m/Y',$request->input('tarea_fecha'))->format('Y-m-d');
        $hora = $request->input('tarea_hora');
        $data['fecha'] = "$fecha $hora";

        $viajero_id = $request->input('viajero_id');

        if(!$id)
        {
            //nuevo
            $o = $this->tarea->create($data);
            $id = $o->id;

            ViajeroLog::addLog($o->viajero, "Tarea nueva [$o->tipo]");

            $tarea = $this->tarea->find($id);
            $tarea->user_id = $request->user()->id;
            $tarea->save();
        }
        else
        {
            $this->tarea->update($data, $id);

            $tarea = $this->tarea->find($id);
            ViajeroLog::addLog($tarea->viajero, "Tarea modificada [$tarea->tipo]");
        }

        //Mail asignado
        MailHelper::mailTarea($tarea->id);

        if( $request->input('home')==1 )
        {
            return redirect()->route('manage.index');
        }

        return redirect()->route('manage.viajeros.ficha',$viajero_id);
    }

    public function setCompleta(Request $request, $id, $estado=true, $home=false)
    {
        $tarea = $this->tarea->find($id);

        $tarea->estado = $estado;
        $tarea->estado_fecha = Carbon::now()->format('Y-m-d');
        $tarea->estado_user_id = $request->user()->id;
        $tarea->save();

        ViajeroLog::addLog($tarea->viajero, "Tarea estado [$tarea->tipo] [". (!$estado) ." -> $estado]");

        if($home)
        {
            Session::flash('tab','#tasks');
            return redirect()->route('manage.index');
        }

        Session::flash('tab','#tareas');

        return redirect()->route('manage.viajeros.ficha',$tarea->viajero_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $viajero_id = $this->tarea->find($id)->viajero_id;

        $this->tarea->delete($id);

        Session::flash('tab','#tareas');

        return redirect()->route('manage.viajeros.ficha',$viajero_id);
    }
}
