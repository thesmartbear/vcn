<?php

namespace VCN\Http\Controllers\Manage\Leads;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Leads\ViajeroLogRepository as ViajeroLog;

use VCN\Models\User;
use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\ViajeroLog as ViajeroLogModel;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Session;
use Carbon;

class ViajeroLogsController extends Controller
{
    private $log;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( ViajeroLog $log )
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');

        $this->log = $log;
    }


    public function getIndex($viajero_id=0, $todos=true)
    {
        if(Datatable::shouldHandle())
        {
            if($viajero_id)
            {
                if(!$todos)
                {
                    // $col = ViajeroLogModel::where('viajero_id',$viajero_id)->where('tipo','<>','')->where('tipo','<>','log')->orderBy('created_at','DESC');
                    $col = $this->log->findWhere([
                        'viajero_id' => $viajero_id,
                        ['tipo','<>',''],
                        ['tipo','<>','log']
                    ])->sortByDesc('created_at');
                }
                else
                {
                    // $col = ViajeroLogModel::where('viajero_id',$viajero_id)->orderBy('created_at','DESC');
                    $col = $this->log->findAllBy('viajero_id', $viajero_id)->sortByDesc('created_at');
                }
            }
            else
            {
                $col = $this->log->all();
            }

            return Datatable::collection( $col )
                ->showColumns('tipo','notas')
                ->addColumn('fecha', function($model) {
                    return $model->created_at->format('d/m/Y H:i');
                })
                ->addColumn('usuario', function($model) {
                    return $model->user?$model->user->full_name:"-";
                })
                ->addColumn('asignado', function($model) {
                    return $model->asignado?$model->asignado->full_name:"-";
                })
                ->addColumn('data', function($model) {
                    return print_r($model->data,true);
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Historial' data-action='". route( 'manage.viajeros.logs.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('tipo','notas','usuario','asignado')
                ->orderColumns('fecha','*')
                ->setOrderStrip()->setSearchStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.viajeros.logs.index', compact('viajero_id'));
    }

    public function getNuevo($viajero_id)
    {
        $tipos = ConfigHelper::getTipoTarea();

        $viajero = Viajero::find($viajero_id)->full_name;
        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        return view('manage.viajeros.logs.new', compact('viajero_id', 'viajero','tipos','asignados'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->log->find($id);

        if(!$ficha)
        {
            return back();
        }

        $tipos = ConfigHelper::getTipoTarea();

        $viajero_id = $ficha->viajero_id;
        $viajero = Viajero::find($viajero_id)->full_name;
        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        return view('manage.viajeros.logs.ficha', compact('ficha','viajero_id', 'viajero','tipos','asignados'));
    }


    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'tipo' => 'required',
        ]);

        $data = $request->except('_token','tarea_notas');

        $data['notas'] = $request->input('tarea_notas');

        $viajero_id = $request->input('viajero_id');
        $user_id = $request->user()->id;

        if(!$id)
        {
            //nuevo
            $o = $this->log->create($data);
            $id = $o->id;
        }
        else
        {
            $this->log->update($data, $id);
        }

        $t = $this->log->find($id);
        $t->user_id = $user_id;
        $t->save();

        //checklist check?
        $tseguimiento = $data['tipo'];
        $viajero = Viajero::find($viajero_id);

        $ficha = null;
        $checks = null;
        $status = $viajero->status;
        if($viajero->es_booking)
        {
            $ficha = $viajero->booking;
            $checks = $status?$status->getChecklistBooking($viajero->booking_id)->where('status_id',$status->id):null;
        }
        else
        {
            if($viajero->solicitud)
            {
                $ficha = $viajero->solicitud;
                $checks = $status?$status->getChecklistSolicitud($viajero->solicitud_id)->where('status_id',$status->id):null;
            }
        }

        $iChecks = 0;
        if($ficha && $checks)
        {
            foreach($checks as $check)
            {
                if( $check->seguimiento && in_array($tseguimiento, $check->seguimiento) )
                {
                    //Marcamos
                    $fecha = Carbon::now()->format('d/m/Y');
                    $s = $ficha->setChecklist($check->id,$fecha,$user_id, true);

                    $iChecks++;
                }
            }
        }

        if(!$iChecks)
        {
            $err = "Este Tipo de Seguimiento corresponde a otro Status o no está relacionado a ninguno, de modo que no se ha marcado ningún Checklist correspondiente";
            // Session::flash('mensaje', $err);
        }if($ficha && $checks)
        {
            foreach($checks as $check)
            {
                if( $check->seguimiento && in_array($tseguimiento, $check->seguimiento) )
                {
                    //Marcamos
                    $fecha = Carbon::now()->format('d/m/Y');
                    $s = $ficha->setChecklist($check->id,$fecha,$user_id, true);

                    $iChecks++;
                }
            }
        }

        if(!$iChecks)
        {
            $err = "Este Tipo de Seguimiento corresponde a otro Status o no está relacionado a ninguno, de modo que no se ha marcado ningún Checklist correspondiente";
            // Session::flash('mensaje', $err);
        }

        Session::flash('tab','#seguimiento');

        $bookingId = $request->get('bookingId');
        if($bookingId)
        {
            return redirect()->route('manage.bookings.ficha',$bookingId);    
        }

        return redirect()->route('manage.viajeros.ficha',$viajero_id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $o = $this->log->find($id);

        $viajero_id = $o->viajero_id;

        $o->delete();

        Session::flash('tab','#historial');

        return redirect()->route('manage.viajeros.ficha', $viajero_id);
    }
}
