<?php

namespace VCN\Http\Controllers\Manage\Leads;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Leads\Origen;
use VCN\Models\Leads\Suborigen;
use VCN\Models\Informes\Venta;
use VCN\Models\Bookings\Booking;

use Datatable;
use Input;
use ConfigHelper;
use DB;
use Carbon;

class SuborigenesController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("permiso.edit:origenes", ['except' => ['getIndex']]);
        $this->middleware("permiso.view:origenes", ['except' => ['getIndex']]);

        // $this->checkPermisos('origenes');
    }

    public function getIndex()
    {
        $origen_id = Input::get('origen_id');

        if($origen_id>0)
        {
            return response()->json(Suborigen::where('origen_id',$origen_id)->orderBy('name')->pluck('name','id'), 200);
        }

        if(Datatable::shouldHandle())
        {
            return Datatable::collection( Suborigen::plataforma() )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.viajeros.suborigenes.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('origen', function($model) {
                    return $model->origen->name;
                })
                ->addColumn('plataforma', function($model) {
                    return ConfigHelper::plataforma($model->origen->propietario);
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Sub-Origen Viajero' data-action='". route( 'manage.viajeros.suborigenes.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','origen')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.viajeros.suborigenes.index');
    }

    public function getNuevo()
    {
        $origenes = [""=>""] + Origen::plataforma()->pluck('name','id')->toArray();
        return view('manage.viajeros.suborigenes.new', compact('origenes'));
    }

    public function getUpdate($id)
    {
        $ficha = Suborigen::find($id);

        $origenes = Origen::plataforma()->pluck('name','id');

        //Ventas
        $totales = null;

        $anyd = 2015;
        $anyh = intval(Carbon::now()->format('Y'));
        for ($i = $anyd; $i <= $anyh; $i++)
        {
            $totNum = 0;
            $totSemanas = 0;
            $totCursos = 0;
            $totTotal = 0;

            $ventas = $ficha->getVentas($i);
            foreach($ventas as $v)
            {
                $totNum += $v->total_bookings;
                $totSemanas += $v->total_semanas;
                $totCursos += $v->total_curso;
                $totTotal += $v->total_total ;
            }

            $totales[$i]['total_num'] = $totNum;
            $totales[$i]['total_sem'] = $totSemanas;
            $totales[$i]['total_curso'] = $totCursos;
            $totales[$i]['total'] = $totTotal;

        }

        return view('manage.viajeros.suborigenes.ficha', compact('ficha','origenes','totales'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'origen_id' => 'required',
            'name' => 'required',
        ]);

        $o = Suborigen::find($id);

        if(!$id)
        {
            $o = new Suborigen;
            $o->origen_id = Input::get('origen_id');
        }

        $o->name = Input::get('name');
        $o->save();

        return redirect()->route('manage.viajeros.suborigenes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $o = Suborigen::find($id);
        $o->delete();

        return redirect()->route('manage.viajeros.suborigenes.index');
    }

    public function getVentas(Request $request, $id, $any)
    {
        // $o = Suborigen::find($id);
        // dd($o->getVentas($any));

        if(Datatable::shouldHandle())
        {
            $o = Suborigen::find($id);

            $user = auth()->user();
            if(!$user->filtro_oficinas)
            {
                $ventas = $o->getVentas($any,$user->oficina_id);
            }
            else
            {
                $ventas = $o->getVentas($any);
            }

            return Datatable::collection( $ventas )
                ->addColumn('categoria', function($model) {
                    return $model->categoria->name;
                })
                ->addColumn('num', function($model) {
                    return $model->total_bookings;
                })
                ->addColumn('total_sem', function($model) {
                    return $model->total_semanas;
                })
                ->addColumn('total_curso', function($model) {
                    return $model->total_curso;
                })
                ->addColumn('total', function($model) {
                    return $model->total_total;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }
    }

    public function getVentasDetalle(Request $request, $id, $any)
    {
        $o = Suborigen::find($id);

        $user = auth()->user();
        if(!$user->filtro_oficinas)
        {
            $ventas = Venta::where('any',$any)->where('oficina_id',$oficina_id)->where('origen_id',$o->origen_id)->where('suborigen_id',$id);//->where('suborigendet_id',0);
        }
        else
        {
            $ventas = Venta::where('any',$any)->where('origen_id',$o->origen_id)->where('suborigen_id',$id);//->where('suborigendet_id',0);
        }

        $bookings = [];
        foreach($ventas->get() as $v)
        {
            foreach($v->bookings as $b)
            {
                $bookings[] = $b;
            }
        }

        $bookings = Booking::whereIn('id',$bookings);

        if(Datatable::shouldHandle())
        {
            $moneda = ConfigHelper::default_moneda();

            return Datatable::collection( $bookings->get() )
                ->addColumn('viajero', function($model) {
                    return "<a href='". route('manage.viajeros.ficha',[$model->viajero->id]) ."'>". $model->viajero->full_name ."</a>";
                })
                ->addColumn('curso', function($model) {
                    return $model->curso->name;
                })
                ->addColumn('convocatoria', function($model) {
                    return $model->convocatoria?$model->convocatoria->name:"-";
                })
                ->addColumn('semanas', function($model) {
                    return $model->semanas;
                })
                ->addColumn('curso_total', function($model) use ($moneda) {
                    $total = $model->course_total_amount;
                    $m_id = $model->course_currency_id;
                    if($m_id != $moneda->id)
                    {
                        $total = $total * $model->getMonedaTasa($m_id);
                    }

                    return $total;
                })
                ->addColumn('fecha', function($model) {
                    $fecha = $model->fecha_reserva?$model->fecha_reserva->format('Y-m-d'):'-';
                    return $fecha;
                })
                ->addColumn('origen', function($model) {
                    return $model->full_name_origen_txt;
                })
                ->addColumn('total', function($model) {
                    return $model->total;
                })
                ->addColumn('descuentos', function($model) {
                    return $model->precio_descuento_solo_txt;
                })
                ->addColumn('seguro', function($model) {
                    return $model->precio_cancelacion_txt;
                })
                ->addColumn('options', function($model) {
                    return "<a href='". route('manage.bookings.ficha',[$model->id]) ."'><i class='fa fa-edit'></i></a>";
                })
                ->searchColumns('viajero','curso','convocatoria')
                ->orderColumns('viajero','curso','convocatoria','semanas','curso_total','fecha')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }
    }
}
