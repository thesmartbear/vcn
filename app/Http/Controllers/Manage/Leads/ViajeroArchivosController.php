<?php

namespace VCN\Http\Controllers\Manage\Leads;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\ViajeroArchivo;

use Input;
use Carbon;
use Session;
use Validator;
use File;
use Log;

class ViajeroArchivosController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');
    }

    public function postUpload(Request $request, $id, $visible=0, $booking_id=0)
    {
        $v = Viajero::find($id);
        // $booking_id = $v->booking_id;
        $dir = storage_path("files/viajeros/". $v->id . "/");

        if($booking_id)
        {
            $dir .= "$booking_id";
            // $visible = true;
        }


        $this->validate($request, [
            // 'file' => 'image30000',
        ]);

        $file = "";
        if($request->hasFile("file"))
        {
            $file = $request->file('file');

            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file_name = str_slug($file_name) .".". $file->getClientOriginalExtension();
            $file->move($dir, $file_name);

            $va = new ViajeroArchivo;
            $va->viajero_id = $id;
            $va->booking_id = $booking_id;
            $va->user_id = auth()->user()->id;
            $va->doc = $booking_id?"/$booking_id/$file_name":$file_name;
            $va->fecha = Carbon::now();
            $va->visible = $visible;
            $va->save();
        }

        return response()->json('success', 200);
    }

    public function postDelete($viajero_id)
    {
        $v = Viajero::find($viajero_id);
        $dir = storage_path("files/viajeros/". $v->id . "/");

        $file = Input::get('f');
        File::delete($dir.$file);

        $va = ViajeroArchivo::where('viajero_id',$viajero_id)->where('doc',$file);
        $va->delete();

        Session::flash('tab','#archivos');

        return response()->json('success', 200);
    }

    public function destroy($file_id)
    {
        $va = ViajeroArchivo::find($file_id);
        $booking_id = $va->booking_id;
        $viajero_id = $va->viajero_id;

        // $dir = public_path("assets/uploads/viajeros/". $va->viajero_id . "/");
        $dir = storage_path("files/viajeros/". $va->viajero_id . "/");

        File::delete($dir.$va->doc);

        if($va->doc_especifico_id)
        {
            Session::flash('tab','#documentos_e');
        }
        else
        {
            Session::flash('tab','#archivos');   
        }

        $va->delete();

        if($booking_id)
        {
            return redirect()->route('manage.bookings.ficha',$booking_id);
        }

        return redirect()->route('manage.viajeros.ficha',$viajero_id);
    }

    public function setVisible($file_id, $booking=false)
    {
        $va = ViajeroArchivo::find($file_id);
        $va->visible = !$va->visible;
        $va->save();

        Session::flash('tab','#archivos');

        if($booking)
        {
            return redirect()->route('manage.bookings.ficha',$va->booking_id);
        }

        return redirect()->route('manage.viajeros.ficha',$va->viajero_id);
    }
    
    public function setStatus($file_id, $status=0)
    {
        $va = ViajeroArchivo::find($file_id);
        $va->doc_especifico_status = $status;
        $va->save();

        $booking = $va->booking;

        if(!$status)
        {
            if($booking)
            {
                \VCN\Helpers\MailHelper::mailAvisoDocEspecificoViajero($booking, $file_id);
            }
            
            //Borramos
            $va->delete();
        }

        Session::flash('tab','#documentos_e');

        if($booking)
        {
            return redirect()->route('manage.bookings.ficha',$booking->id);
        }

        return redirect()->route('manage.viajeros.ficha',$va->viajero_id);
    }
}
