<?php

namespace VCN\Http\Controllers\Manage\Leads;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\User;
use VCN\Models\Leads\Tutor;
use VCN\Models\Leads\TutorLog;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Session;
use Carbon;

class TutorLogsController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth', ['except' => ['index','show']]);
        // $this->middleware('auth');
    }


    public function getIndex($tutor_id=0)
    {
        if(Datatable::shouldHandle())
        {
            if($tutor_id)
            {
                
                $col = TutorLog::where('tutor_id',$tutor_id)->orderBy('created_at','DESC')->get();
            }
            else
            {
                $col = TutorLog::all();
            }

            return Datatable::collection( $col )
                ->showColumns('tipo','notas')
                ->addColumn('fecha', function($model) {
                    return $model->created_at->format('d/m/Y H:i');
                })
                ->addColumn('usuario', function($model) {
                    return $model->user?$model->user->full_name:"-";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Historial' data-action='". route( 'manage.viajeros.logs.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('tipo','notas','usuario')
                ->orderColumns('fecha','*')
                ->setOrderStrip()->setSearchStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.tutores.logs.index', compact('tutor_id'));
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $o = TutorLog::find($id);

        $tutor_id = $o->tutor_id;

        $o->delete();

        Session::flash('tab','#historial');

        return redirect()->route('manage.tutores.ficha', $tutor_id);
    }
}
