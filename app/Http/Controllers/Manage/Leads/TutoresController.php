<?php

namespace VCN\Http\Controllers\Manage\Leads;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Leads\TutorRepository as Tutor;

use VCN\Models\User;
use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\ViajeroTutor;
use VCN\Models\Leads\TutorLog;

use VCN\Models\Leads\Tutor as TutorModel;

use VCN\Helpers\ConfigHelper;
use VCN\Helpers\MailHelper;
use Datatable;
use Input;
use Session;
use DB;
use Carbon;

use VCN\Repositories\Criteria\FiltroPlataformaTutor;

class TutoresController extends Controller
{
    private $tutor;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Tutor $tutor )
    {
        $this->middleware("permiso.plataforma:tutores,\VCN\Models\Leads\Tutor", ['only' => ['getUpdate']]);
        $this->middleware("permiso.view:tutores-list", ['only' => ['getIndex','getUpdate']]);

        $this->tutor = $tutor;
    }

    public function getIndexBuscar(Request $request, $campo='nombre', $query="")
    {
        if(Datatable::shouldHandle())
        {
            $col = TutorModel::buscar($query,$campo);

            return Datatable::collection( $col )
                // ->showColumns('name','lastname')
                ->addColumn('nombre', function($model) {
                    $ret = "<a href='". route('manage.tutores.ficha',[$model->id]) ."'>$model->name $model->lastname</a>";

                    return $ret;
                })
                ->addColumn('email', function($model) {
                    return "<a href='mailto:". trim($model->email) ."'>$model->email</a>";
                })
                ->addColumn('viajeros', function($model) {

                    $ret = "";

                    $tot = $model->viajeros->count();
                    $itot = 0;
                    foreach($model->viajeros as $t)
                    {
                        $relacion = ConfigHelper::getTutorRelacion($t->pivot->relacion);

                        $ret .= "<a href='". route('manage.viajeros.ficha',[$t->id]) ."'>$t->name $t->lastname</a>";
                        $ret .= " <i>(". $relacion .")</i>";

                        if(++$itot != $tot)
                        {
                            $ret .= ",<br>";
                        }
                    }

                    return $ret;

                })
                ->showColumns('phone','movil')
                ->addColumn('empresa', function($model) {

                    $ret = $model->profesion;

                    $ret .= $model->empresa?" / $model->empresa":"";
                    $ret .= $model->empresa?" ($model->empresa_phone)":"";

                    return $ret;
                })
                ->addColumn('options', function($model) {

                    return "";
                })
                ->searchColumns('name','lastname')
                ->orderColumns('lastaname','name')
                ->setOrderStrip()->setSearchStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.tutores.index', compact('viajero_id'));
    }

    public function getIndex()
    {
        // $this->tutor->pushCriteria(new FiltroPlataformaTutor());

        $viajero_id = 0;

        if(Datatable::shouldHandle())
        {
            // $col = $this->tutor->all();
            $query = DB::table('tutores')->select();
            if($viajero_id)
            {
                // $query = DB::table('tutores')->where('');
            }

            $filtro = ConfigHelper::config('propietario');
            if($filtro)
            {
                $query = DB::table('tutores')->where('plataforma',$filtro);//->orWhere('plataforma',0);
            }

            // $dtt = Datatable::collection( $col )
            return Datatable::query( $query )
                // ->addColumn('full_name', function($column) {
                //     $model = TutorModel::find($column->id);
                //     return "<a href='". route('manage.tutores.ficha',[$model->id]) ."'>$model->full_name</a>";
                // })
                ->showColumns('name','lastname')
                ->addColumn('email', function($column) {
                    $model = TutorModel::find($column->id);
                    return "<a href='mailto:". trim($model->email) ."'>$model->email</a>";
                })
                ->addColumn('viajeros', function($column) {

                    $model = TutorModel::find($column->id);

                    $ret = "";

                    $tot = $model->viajeros->count();
                    $itot = 0;
                    foreach($model->viajeros as $t)
                    {
                        $relacion = ConfigHelper::getTutorRelacion($t->pivot->relacion);

                        $ret .= "<a href='". route('manage.viajeros.ficha',[$t->id]) ."'>$t->name $t->lastname</a>";
                        $ret .= " <i>(". $relacion .")</i>";

                        if(++$itot != $tot)
                        {
                            $ret .= ",<br>";
                        }
                    }

                    return $ret;

                })
                ->showColumns('phone','movil')
                ->addColumn('empresa', function($column) {
                    $model = TutorModel::find($column->id);

                    $ret = $model->profesion;

                    $ret .= $model->empresa?" / $model->empresa":"";
                    $ret .= $model->empresa?" ($model->empresa_phone)":"";

                    return $ret;
                })
                ->addColumn('options', function($column) {

                    $model = TutorModel::find($column->id);

                    $ret = "<a href='". route('manage.tutores.ficha', $model->id) ."' class='btn btn-success btn-xs'><i class='fa fa-edit'></i> Ficha</a>";

                    $data = " data-label='Borrar' data-model='Tutor' data-action='". route( 'manage.tutores.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= "<a href='$model->id' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Centro</a>";

                    if($model->usuario)
                    {
                        $ret .= " <a target='_blank' href='". route('manage.tutores.area', $model->id) ."' class='btn btn-success btn-xs'><i class='fa fa-sign-in'></i> Area</a>";
                    }

                    return $ret;
                })
                ->searchColumns('name','lastname','email')
                // ->orderColumns('lastaname','name')
                // ->setOrderStrip()->setSearchStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.tutores.index', compact('viajero_id'));
    }

    public function getNuevo($viajero_id=0)
    {
        $viajeros = Viajero::all()->pluck('full_name','id');

        $relaciones = ConfigHelper::getTutorRelacion();
        $tutores = $this->tutor->all()->pluck('full_name','id');

        $viajero_name = $viajero_id>0?Viajero::find($viajero_id)->full_name:null;

        return view('manage.tutores.new', compact('viajero_id','viajero_name','viajeros','relaciones','tutores'));
    }


    public function getUpdate( $id, $viajero_id=0 )
    {
        $ficha = $this->tutor->find($id);

        $relaciones = ConfigHelper::getTutorRelacion();

        return view('manage.tutores.ficha', compact('ficha','relaciones','viajero_id'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function postUpdate(Request $request, $id=0)
    {

        if($request->has('booking'))
        {
            $this->validate($request, [
                'tutor_email' => 'email|max:255',
            ]);

            $input = $request->input();

            $data['email'] = trim($input['tutor_email']);
            $data['name'] = $input['tutor_name'];
            $data['lastname'] = $input['tutor_lastname'];
            $data['phone'] = $input['tutor_phone'];
            $data['movil'] = $input['tutor_movil'];
            $data['nif'] = $input['tutor_nif'];
            $data['tipodoc'] = $input['tutor_tipodoc'];
            // $data['relacion'] = $input['tutor_relacion'];

        }
        else
        {
            $ficha = $this->tutor->find($id);

            $this->validate($request, [
                'name' => 'required',
            ]);

            //if($ficha && $ficha->user)
            //{
            //    $this->validate($request, [
            //        'email' => 'email|max:255',
            //    ]);
            //}

            $data = $request->except(['viajero_id','_token','relacion','booking_id','booking']);

        }

        $viajero_id = $request->input('viajero_id');
        $email = trim($request->input('email'));
        $emailUsado = TutorModel::checkEmail($email);
        
        $filtro = ConfigHelper::config('propietario');
        if($emailUsado)
        {
            if($id && $email == $emailUsado->email && $id == $emailUsado->id)
            {
                //Es update y no cambia el mail
            }
            else
            {
                if( $emailUsado->plataforma == $filtro )
                {
                    $mensaje = "Este email [". $data['email'] ."] ya existe para el Tutor $emailUsado->full_name";
                    if( $request->ajax() )
                    {
                        $result = ['result'=> false, 'mensaje'=> $mensaje];
                        return response()->json($result, 200);
                    }

                    Session::flash('mensaje', $mensaje );

                    if($viajero_id)
                    {
                        return redirect()->route('manage.viajeros.ficha',$viajero_id)->withInput();
                    }

                    return redirect()->route('manage.tutores.ficha',$id)->withInput()->exceptInput('email');
                }
            }
        }

        if(!$id)
        {
            //nuevo
            $o = $this->tutor->create($data);
            $id = $o->id;

            $o->plataforma = User::find($request->user()->id)->plataforma;
            $o->save();

            $vt = new ViajeroTutor;
            $vt->viajero_id = $viajero_id;
            $vt->tutor_id = $id;
            $vt->relacion = $request->input('relacion');
            $vt->save();

            TutorLog::addLog($o, "Nuevo Tutor");

            //Le creamos usuario a los tutores recien asignados
            foreach($vt->viajero->bookings->where('es_presupuesto',0) as $bkg)
            {
                if($bkg->es_terminado)
                {
                    $vt->viajero->setUsuarios();
                    break;
                }
            }
        }
        else
        {
            $ficha = $this->tutor->find($id);
            
            $email = $data['email'];
            if($ficha->email != $email)
            {
                TutorLog::addLog($ficha, "Cambio email: $ficha->email => $email");
            }

            $this->tutor->update($data, $id);

            if($ficha->user)
            {
                $ficha->user->fname = $ficha->name;
                $ficha->user->lname = $ficha->lastname;
                $ficha->user->save();
            }

            if($viajero_id)
            {
                $vt = ViajeroTutor::where('viajero_id',$viajero_id)->where('tutor_id',$id)->first();
                $vt->relacion = $request->input('relacion');
                $vt->save();

                //Le creamos usuario a los tutores recien asignados
                if(!$ficha->usuario)
                {
                    foreach($vt->viajero->bookings->where('es_presupuesto',0) as $bkg)
                    {
                        if($bkg->es_terminado)
                        {
                            $ficha->setUsuario();
                            break;
                        }
                    }
                }

                Session::flash('tab','#tutores');
                return redirect()->route('manage.viajeros.ficha',$viajero_id);
            }

            foreach($ficha->viajeros as $viajero)
            {
                $viajero_id = $viajero->id;
                $vt = ViajeroTutor::where('viajero_id',$viajero_id)->where('tutor_id',$id)->first();

                if(!$ficha->usuario)
                {
                    foreach($vt->viajero->bookings->where('es_presupuesto',0) as $bkg)
                    {
                        if($bkg->es_terminado)
                        {
                            $ficha->setUsuario();
                            break;
                        }
                    }
                }
            }
        }

        //email actualizado
        $emailUsado = TutorModel::checkEmail($data['email']);

        $tutor = $this->tutor->find($id);
        $email = trim($data['email']);
        if($email)
        {
            if($tutor->usuario && ($tutor->usuario->username!=$email))
            {
                $tutor->usuario->username = $email;
                $tutor->usuario->email = $email;
                $tutor->usuario->save();

                MailHelper::mailAreaEmail($tutor);
            }
        }
        else
        {
            //lo borramos
            if($tutor->usuario)
            {
                $tutor->usuario->delete();
                $tutor->user_id = 0;
                $tutor->save();

                Session::flash('mensaje','Al borrar el email se ha eliminado el área de cliente.');
            }
        }

        $tutor = $this->tutor->find($id);

        $es_rgpd = ConfigHelper::config('es_rgpd');
        if($es_rgpd)
        {
            $optout = !$request->has('lopd_check2');
            $tutor->optout = $optout;
            $tutor->optout_fecha = $optout?Carbon::now():null;
            $tutor->lopd_check2 = $request->has('lopd_check2');
            $tutor->save();
        }
        else
        {
            $optout = $request->has('optout');
            $tutor->optout = $optout;
            $tutor->optout_fecha = $optout?Carbon::now():null;
            $tutor->save();
        }

        if($request->has('booking'))
        {
            $booking_id = $request->input('booking_id');
            return redirect()->route('manage.bookings.ficha',$booking_id);
        }

        if( $request->ajax() )
        {
            $result = ['result'=> true];
            return response()->json($result, 200);
        }

        return redirect()->route('manage.tutores.ficha',$id);
    }

    public function postUpdateAsignar(Request $request, $id=0)
    {
        $this->validate($request, [
            'relacion' => 'required|numeric',
        ]);

        $viajero_id = $id;
        $tutor_id = $request->input('tutor_id') ?: null;

        if(!$tutor_id)
        {
            Session::flash('tab','#tutores');
            Session::flash('mensaje', "No ha especificado el tutor" );
            return redirect()->route('manage.viajeros.ficha',$viajero_id);
        }

        $vt = new ViajeroTutor;
        $vt->viajero_id = $viajero_id;
        $vt->tutor_id = $tutor_id;
        $vt->relacion = $request->input('relacion');
        $vt->save();

        //Le creamos usuario a los tutores recien asignados
        foreach($vt->viajero->bookings->where('es_presupuesto',0) as $bkg)
        {
            if($bkg->es_terminado)
            {
                $vt->viajero->setUsuarios();
                break;
            }
        }

        if( $request->ajax() )
        {
            $result = ['result'=> true];
            return response()->json($result, 200);
        }

        if($request->has('booking'))
        {
            $booking_id = $request->input('booking_id');
            return redirect()->route('manage.bookings.ficha',$booking_id);
        }

        Session::flash('tab','#tutores');

        return redirect()->route('manage.viajeros.ficha',$viajero_id);
    }

    public function destroy($id)
    {
        $tutor = $this->tutor->find($id);
        if($tutor->usuario)
        {
            $tutor->usuario->delete();
        }

        $this->tutor->delete($id);
        return redirect()->route('manage.tutores.index');
    }

    public function destroyRelacion($id)
    {
        $vt = ViajeroTutor::find($id);

        Session::flash('tab','#tutores');

        if(!$vt)
        {
            return redirect()->back();
        }

        $viajero_id = $vt->viajero_id;

        $vt->delete();

        return redirect()->route('manage.viajeros.ficha',$viajero_id);
    }

    public function getAutocomplete(Request $request)
    {
        // $this->tutor->pushCriteria(new FiltroPlataformaTutor());

        $filtro = ConfigHelper::config('propietario');

        $term = $request->input('term');

        $ret = $this->tutor->findWhere([
                ['plataforma',$filtro],
                [DB::raw("CONCAT(name,lastname)"), 'LIKE' ,"%$term%"]
            ]);

        $ret2 = $this->tutor->findWhere([
            ['plataforma',$filtro],
            [DB::raw("CONCAT(name,' ',lastname)"), 'LIKE' ,"%$term%"]
        ]);
        $ret = $ret->merge($ret2);

        $ret2 = $this->tutor->findWhere([
                ['plataforma',$filtro],
                ['name', 'LIKE' ,"%$term%"]
            ]);
        $ret = $ret->merge($ret2);

        $ret2 = $this->tutor->findWhere([
                ['plataforma',$filtro],
                ['lastname', 'LIKE' ,"%$term%"]
            ]);
        $ret = $ret->merge($ret2);

        $ret2 = $this->tutor->findWhere([
                ['plataforma',$filtro],
                ['email', 'LIKE' ,"%$term%"]
            ]);
        $ret = $ret->merge($ret2);

        $ret2 = $this->tutor->findWhere([
                ['plataforma',$filtro],
                ['phone', 'LIKE' ,"%$term%"]
            ]);
        $ret = $ret->merge($ret2);

        $ret2 = $this->tutor->findWhere([
                ['plataforma',$filtro],
                ['movil', 'LIKE' ,"%$term%"]
            ]);
        $ret = $ret->merge($ret2);

        $ret->unique();

        return response()->json($ret, 200);
    }

    public function getArea($tutor_id)
    {
        $tutor = $this->tutor->find($tutor_id);
        $usuario = $tutor->user;

        // dd($usuario);

        if(!$usuario)
        {
            return false;
        }

        $ficha = $usuario->ficha;
        Session::put('vcn.manage.area.home', route('manage.tutores.area',$ficha->id) );
        Session::put('vcn.manage.area.tipo', "tutor" );
        Session::put('vcn.manage.area.ficha', $ficha->id );

        return view('area.index',compact('usuario','ficha'));
    }

    /**
     * Visitas
     */
    public function getTrafico($id)
    {
        $tutor = $this->tutor->find($tutor_id);

        if(!$tutor->user)
        {
            return null;
        }

        $user = $tutor->usuario;
        if(!$user->visitas->count())
        {
            return null;
        }

        if( Datatable::shouldHandle() )
        {
            $col = $user->visitas;

            return Datatable::collection( $col )
                ->addColumn('fecha', function($model) {
                    return $model->fecha->format('Y-m-d');
                })
                ->addColumn('curso', function($model) {
                    return $model->curso?$model->curso->name:"Web Home";
                })
                ->addColumn('visitas', function($model) {
                    return $model->visitas;
                })
                ->searchColumns('fecha','curso')
                ->orderColumns('fecha','curso')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

    }

}
