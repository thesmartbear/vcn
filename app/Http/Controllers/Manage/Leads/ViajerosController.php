<?php

namespace VCN\Http\Controllers\Manage\Leads;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Leads\ViajeroRepository as Viajero;
use VCN\Repositories\Leads\ViajeroDatosRepository as ViajeroDatos;

use VCN\Models\Solicitudes\Solicitud;
use VCN\Models\Solicitudes\Status as StatusSolicitud;
use VCN\Models\Bookings\Status as StatusBooking;
use VCN\Models\Leads\Origen;
use VCN\Models\Leads\Suborigen;
use VCN\Models\Leads\SuborigenDetalle;
use VCN\Models\Leads\Tutor;
use VCN\Models\Leads\ViajeroTutor;
use VCN\Models\Leads\Viajero as ViajeroModel;

use VCN\Models\Categoria;
use VCN\Models\Subcategoria;
use VCN\Models\SubcategoriaDetalle;
use VCN\Models\User;
use VCN\Models\Exams\{Examen, Respuesta};

use VCN\Models\Bookings\BookingLog;
use VCN\Models\Leads\ViajeroLog;

use VCN\Models\Prescriptores\Prescriptor;
use VCN\Models\System\Oficina;

use Datatable;
use Session;
use Schema;
use Carbon;
use Excel;
use DB;
use VCN\Helpers\ConfigHelper;
use VCN\Helpers\MailHelper;

use VCN\Repositories\Criteria\FiltroPlataformaAsignado;
use VCN\Repositories\Criteria\FiltroPlataformaViajero;

use Symfony\Component\HttpFoundation\StreamedResponse;

class ViajerosController extends Controller
{
    private $viajero;
    private $datos;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Viajero $viajero, ViajeroDatos $datos )
    {
        $this->middleware("permiso.plataforma:viajeros,\VCN\Models\Leads\Viajero", ['only' => ['getUpdate']]);
        $this->checkPermisos('viajeros');

        $this->middleware("permiso.view:viajeros", ['only' => ['getIndexBuscar']]);
        $this->middleware("permiso.view:viajeros-list", ['only' => ['getIndex','getUpdate']]);

        $this->viajero = $viajero;
        $this->datos = $datos;
    }

    public function setInscrito(Request $request, $id)
    {
        $v = $this->viajero->find($id);
        $v->setInscrito(true);

        Session::flash('mensaje', 'Lead pasa a Inscrito.');
        return redirect()->route('manage.inscritos.index');
    }

    public function setTutor($rel_id,$rel)
    {
        $vt = ViajeroTutor::find($rel_id);

        $rel0 = $vt->relacion;

        $vt->relacion = $rel;
        $vt->save();

        ViajeroLog::addLogAsignado($vt->viajero, "Cambio Relación Tutor [$rel0 => $rel]");

        Session::flash('tab','#viajeros');
        return redirect()->route('manage.tutores.ficha',$vt->tutor->id);
    }

    public function getIndexByTutor($tutor_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = Tutor::find($tutor_id)->viajeros;//->sortBy('name');

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.viajeros.ficha',[$model->id]) ."'>$model->full_name</a>";
                })
                ->addColumn('relacion', function($model) {
                    return ConfigHelper::getTutorRelacion($model->pivot->relacion);
                })
                ->addColumn('asignado', function($model) {
                    return $model->asignado?$model->asignado->fname." ".$model->asignado->lname:"-";
                })
                ->showColumns('email')
                ->addColumn('status', function($model) {
                    return "<span class='badge'>". ($model->status?$model->status->name:"-") . "</span>";
                })
                ->addColumn('change', function($model) {

                    $ret = "";
                    foreach( ConfigHelper::getTutorRelacion() as $krel=>$vrel )
                    {
                        $ret .= " <a href='". route('manage.viajeros.set.tutor',[$model->pivot->id,$krel]) ."'>$vrel</a>";
                    }

                    return $ret;

                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    if(!$model->es_cliente)
                    {
                        // $ret .= "<span class='badge'>Inscrito</span>";
                    }

                    return $ret;
                })
                ->searchColumns('name','lastname','lastname2')
                ->orderColumns('name','asignado','tutores')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        return view('manage.viajeros.index_tutor', compact('tutor_id'));
    }


    public function getIndex(Request $request, $status_id=0, $asign_to=0, $oficina_id=0)
    {
        if(!$asign_to)
        {
            $this->viajero->pushCriteria(new FiltroPlataformaAsignado());
            $asign_to = $request->user()->id;
        }

        // if(!$oficina_id)
        // {
        //     $oficina_id = $request->user()->oficina_id;
        // }

        $user = auth()->user();
        if(!$user->filtro_oficinas)
        {
            $oficina_id = $user->oficina_id;
        }

        $this->viajero->pushCriteria(new FiltroPlataformaViajero());

        //si elige oficina que se quede seleccionado el primero de esa ofi
        if($oficina_id>0 )
        {
            $asig = User::find($asign_to);
            if($asig)
            {
                if($asig->oficina_id != $oficina_id)
                {
                    $asignado = User::where('oficina_id',$oficina_id)->orderBy('fname')->first();
                    $asign_to = $asignado->id;
                }
            }
        }

        $filtros = [];

        if($status_id>0)
        {
            $filtros['status_id'] = $status_id;
        }

        // if($asign_to>0)
        // {
        //     $filtros['asign_to'] = $asign_to;
        // }

        if($oficina_id>0 && $oficina_id!=='all')
        {
            $filtros['oficina_id'] = $oficina_id;

            if($oficina_id==='null')
            {
                $filtros['oficina_id'] = null;
            }
        }


        if(Datatable::shouldHandle())
        {
            $query = DB::table('viajeros');

            if($asign_to=='all')
            {
                $usuarios = User::asignados()->pluck('id')->toArray();
                $query = $query->whereIn('asign_to',$usuarios);
            }
            elseif($asign_to=='all-off')
            {
                $usuarios = User::asignadosOff()->pluck('id')->toArray();
                $query = $query->whereIn('asign_to',$usuarios);
            }

            if(count($filtros)>0)
            {
                foreach($filtros as $fk=>$fv)
                {
                    $query = $query->where($fk,$fv);
                }
            }
            else
            {
                $query = $query->select();
            }

            // $filtro = ConfigHelper::config('propietario');
            // if($filtro)
            // {
            //     $query = $query->where('plataforma', $filtro);
            // }

            $filtro = ConfigHelper::config('propietario');
            if($filtro && !is_numeric($asign_to))
            {
                $query = $query->where('plataforma', $filtro);
            }

            // return Datatable::collection( $col )
            return Datatable::query($query)
                ->showColumns('name','lastname','lastname2')
                ->addColumn('asignado', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->asignado?$model->asignado->fname." ".$model->asignado->lname:"-";
                })
                ->showColumns('email','phone','movil')
                ->addColumn('categoria', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->categoria_name;
                })
                ->addColumn('status', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->status?$model->status->name:"-";
                })
                ->showColumns('rating')
                ->addColumn('idioma', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->idioma_contacto;
                })
                ->addColumn('tutor1', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor1?$model->tutor1->name:'-';
                })
                ->addColumn('email_tutor1', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor1?$model->tutor1->email:'-';
                })
                ->addColumn('movil_tutor1', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor1?$model->tutor1->movil:'-';
                })
                ->addColumn('tutor2', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor2?$model->tutor2->name:'-';
                })
                ->addColumn('email_tutor2', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor2?$model->tutor2->email:'-';
                })
                ->addColumn('movil_tutor2', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor2?$model->tutor2->movil:'-';
                })
                // ->addColumn('status', function($column) {
                //     $model = ViajeroModel::find($column->id);
                //     return "<span class='badge'>". ($model->status?$model->status->name:"Archivado") . "</span>";
                // })
                // ->showColumns('rating')
                ->addColumn('options', function($column) {

                    $model = ViajeroModel::find($column->id);

                    $ret = "<a href='". route('manage.viajeros.ficha', $model->id) ."' class='btn btn-success btn-xs'><i class='fa fa-edit'></i> Ficha</a>";

                    // $ret .= " <a href='". route('manage.bookings.nuevo', $model->id) ."' class='btn btn-info btn-xs'><i class='fa fa-plus-circle'></i> Inscripción</a>";

                    if($model->solicitud_activa)
                    {
                        $data = " data-label='Nueva Solicitud' data-pregunta='Ya hay una solicitud abierta para este viajero. ¿Seguro que la quieres archivar y abrir una nueva?' data-action='". route('manage.solicitudes.nuevo', $model->id) . "'";
                        $ret .= " <a $data href='#confirma' class='btn btn-danger btn-xs'><i class='fa fa-plus-circle'></i> Solicitud</a>";
                    }
                    else
                    {
                        $ret .= " <a href='". route('manage.solicitudes.nuevo', $model->id) ."' class='btn btn-success btn-xs'><i class='fa fa-plus-circle'></i> Solicitud</a>";
                    }

                    if(!$model->es_cliente)
                    {
                        // $ret .= "<a href='". route('manage.viajeros.inscribir',[$model->id]) ."' class='btn btn-warning btn-xs'><i class='fa fa-arrow-circle-right'></i> Convertir a Inscrito</a>";
                    }
                    else
                    {
                        $ret .= "&nbsp;<span class='badge'>Inscrito</span>";
                    }

                    if($model->usuario)
                    {
                        $ret .= " <a target='_blank' href='". route('manage.viajeros.area', $model->id) ."' class='btn btn-success btn-xs'><i class='fa fa-sign-in'></i> Area</a>";
                    }

                    if(ConfigHelper::canEdit('viajero-eliminar'))
                    {
                        $data = " data-label='Borrar' data-model='Viajero' data-action='". route( 'manage.viajeros.delete', $model->id) . "'";
                        $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";
                    }

                    return $ret;
                })
                ->searchColumns('name','lastname','lastname2','email')
                ->orderColumns('name','lastname','lastname2','asignado','tutores','rating')
                ->setAliasMapping()
                // ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $statuses = StatusSolicitud::all()->sortBy('orden');

        $statuses_total = [];

        $query = DB::table('viajeros');
        if($asign_to=='all')
        {
            $usuarios = User::asignados()->pluck('id')->toArray();
            $query = $query->whereIn('asign_to',$usuarios);
        }
        elseif($asign_to=='all-off')
        {
            $usuarios = User::asignadosOff()->pluck('id')->toArray();
            $query = $query->whereIn('asign_to',$usuarios);
        }

        if(count($filtros)>0)
        {
            foreach($filtros as $fk=>$fv)
            {
                $query = $query->where($fk,$fv);
            }
        }
        else
        {
            $query = $query;
        }

        //Status
        $statuses_total[0] = $query->count();
        foreach($statuses as $status)
        {
            $query1 = clone $query;

            unset($filtros['status_id']);
            $filtros['status_id'] = $status->id;

            if(count($filtros)>0)
            {
                foreach($filtros as $fk=>$fv)
                {
                    $query1 = $query1->where($fk,$fv);
                }
                
                $statuses_total[$status->id] = $query1->count();
            }
            else
            {
                $statuses_total[$status->id] = $query1->count();
            }
        }

        $oficinas = Oficina::plataforma();
        if($oficina_id>0)
        {
            $asignados = User::asignados()->where('oficina_id',$oficina_id)->orderBy('fname')->get();
        }
        else
        {
            $asignados = User::asignados()->get();
        }

        // $anys = \VCN\Models\Informes\Venta::groupBy('any')->get()->pluck('any','any')->toArray();
        // $any = intval($request->input('any',Carbon::now()->format('Y')));
        // return view('manage.viajeros.index', compact('status_id', 'asign_to', 'oficina_id', 'asignados', 'statuses', 'statuses_total','oficinas', 'anys','any'));

        return view('manage.viajeros.index', compact('status_id', 'asign_to', 'oficina_id', 'asignados', 'statuses', 'statuses_total','oficinas'));
    }

    public function getIndexExcel(Request $request, $status_id=null)
    {
        // $this->viajero->pushCriteria(new FiltroPlataformaAsignado());
        // $this->viajero->pushCriteria(new FiltroPlataformaViajero());

        if(Datatable::shouldHandle())
        {
            $filtro = ConfigHelper::config('propietario');
            if($filtro)
            {
                if($status_id)
                {
                    $query = DB::table('viajeros')->where('plataforma',$filtro)->where('status_id',$status_id)->select();
                }
                else
                {
                    $query = DB::table('viajeros')->where('plataforma',$filtro)->select();
                }
            }
            else
            {
                if($status_id)
                {
                    $query = DB::table('viajeros')->where('status_id',$status_id)->select();
                }
                else
                {
                    $query = DB::table('viajeros')->select();
                }
            }

            // return Datatable::collection( $col )
            return Datatable::query($query)
                ->showColumns('name','lastname','lastname2')
                ->addColumn('asignado', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->asignado?$model->asignado->fname." ".$model->asignado->lname:"-";
                })
                ->showColumns('email','phone','movil')
                ->addColumn('categoria', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->categoria_name;
                })
                ->addColumn('status', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->status?$model->status->name:"-";
                })
                ->showColumns('rating')
                ->addColumn('idioma', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->idioma_contacto;
                })
                ->addColumn('tutor1', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor1?$model->tutor1->name:'-';
                })
                ->addColumn('email_tutor1', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor1?$model->tutor1->email:'-';
                })
                ->addColumn('movil_tutor1', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor1?$model->tutor1->movil:'-';
                })
                ->addColumn('tutor2', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor2?$model->tutor2->name:'-';
                })
                ->addColumn('email_tutor2', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor2?$model->tutor2->email:'-';
                })
                ->addColumn('movil_tutor2', function($column) {
                    $model = ViajeroModel::find($column->id);
                    return $model->tutor2?$model->tutor2->movil:'-';
                })
                ->searchColumns('name','lastname','lastname2')
                ->orderColumns('name')
                ->setAliasMapping()
                // ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $statuses = StatusSolicitud::orderBy('orden')->get();

        return view('manage.viajeros.index_excel', compact('status_id','statuses'));
    }

    public function getIndexExcelDownload(Request $request, $optout=false )
    {
        ini_set('memory_limit', '800M');
        set_time_limit(50000);
        // ini_set('max_execution_time', '0');

        $plat = ConfigHelper::config('propietario');
        $viajeros = ViajeroModel::where('plataforma',$plat);
        $total = $viajeros->count();

        $response = new StreamedResponse(function() use ($optout, $viajeros) {

            // $tutores = [];

            // if(!$optout)
            // {
            //     $tutores = Tutor::where('optout', 0)->pluck('id')->toArray();
            //     $viajeros_id = ViajeroTutor::whereIn('tutor_id', $tutores)->groupBy('viajero_id')->pluck('viajero_id')->toArray();
            //     // $viajeros = ViajeroModel::whereNotIn('id', $viajeros_id)->where('optout',0);
            // }
            // else
            // {
            //     $viajeros_id = $viajeros->pluck('id')->toArray();
            //     // $viajeros = ViajeroModel::whereIn('id', $viajeros_id);
            // }

            $total = $viajeros->count();
            // dd($viajeros->pluck('id'));

            $fecha = Carbon::now()->format('d-m-Y');

            // Open output stream
            $handle = fopen('php://output', 'w');

            // fputs($handle, chr(0xEF) . chr(0xBB) . chr(0xBF));

            $campos = [
                "id-$total",
                'nombre', 'apellido1', 'apellido2', 'email', 'fechanac', 'categoria', 'status',
                'tutor1_nombre','tutor1_email',
                'tutor2_nombre','tutor2_email',
                'idioma','asignado','oficina',
                'tipovia','direccion','cp','ciudad','provincia',
                'movil_viajero','movil_tutor1','movil_tutor2',
            ];

            if($optout)
            {
                array_push($campos,"optout");
            }
            else
            {
                array_push($campos,"origen");
                array_push($campos,"suborigen");
                array_push($campos,"suborigen_det");
            }

            $campos[] = 'num_bookings';
            $campos[] = 'ultimo_booking';

            $charset = "ISO-8859-1//IGNORE";
            // $charset = "Windows-1252";

            // Add CSV headers
            fputcsv($handle, $campos);

            $bloque = 500;
            $viajeros->chunk($bloque, function($list) use($handle,$optout,$charset) {

                foreach ($list as $v)
                {
                    $emailV = $v->email;
                    $emailT1 = $v->tutor1?$v->tutor1->email:null;
                    $emailT2 = $v->tutor2?$v->tutor2->email:null;

                    if(!$optout)
                    {
                        if($v->optout)
                        {
                            $emailV = "";
                        }

                        if($emailT1 && $v->tutor1->optout)
                        {
                            $emailT1 = "";
                        }

                        if($emailT2 && $v->tutor2->optout)
                        {
                            $emailT2 = "";
                        }
                    }

                    $campos = [

                        $v->id,
                        iconv("UTF-8", $charset,$v->name),
                        iconv("UTF-8", $charset,$v->lastname),
                        iconv("UTF-8", $charset,$v->lastname2),
                        $emailV,
                        $v->fechanac ? Carbon::parse($v->fechanac)->format('d/m/Y') : '',
                        iconv("UTF-8", $charset,$v->categoria_name),
                        $v->status ? $v->status->name:"-",
                        iconv("UTF-8", $charset,$v->tutor1?$v->tutor1->full_name:""),
                        $emailT1,
                        iconv("UTF-8", $charset,$v->tutor2?$v->tutor2->full_name:""),
                        $emailT2,
                        $v->idioma_contacto,
                        iconv("UTF-8", $charset,$v->asignado?$v->asignado->full_name:"-"),
                        iconv("UTF-8", $charset,$v->oficina?$v->oficina->name:"-"),

                        iconv("UTF-8", $charset,$v->datos?$v->datos->tipovia:""),
                        trim(iconv("UTF-8", $charset,$v->datos?$v->datos->direccion:"")),
                        $v->datos?$v->datos->cp:"",
                        iconv("UTF-8", $charset,$v->datos?$v->datos->ciudad:""),
                        iconv("UTF-8", $charset,$v->datos?$v->datos->provincia:""),
                        $v->movil,
                        $v->tutor1?$v->tutor1->movil:'-',
                        $v->tutor2?$v->tutor2->movil:'-',
                    ];

                    if($optout)
                    {
                        $fo = "";
                        if($v->optout)
                        {
                            $fo = $v->optout_fecha ? $v->optout_fecha->format('d/m/Y') : "";
                        }
                        array_push($campos, $fo );
                    }
                    else
                    {
                        array_push($campos, ($v->origen?$v->origen->name:"") );
                        array_push($campos, ($v->suborigen?$v->suborigen->name:"") );
                        array_push($campos, ($v->suborigen_det?$v->suborigen_det->name:"") );
                    }

                    array_push($campos, $v->bookings_plaza->count() );
                    array_push($campos, ($v->bookings_plaza_last?$v->bookings_plaza_last->fecha_inicio:'') );

                    // $campos = iconv("UTF-8", $charset, $campos);

                    fputcsv($handle, $campos);
                }
            });

            fclose($handle);

        }, 200, [
            'Content-Type' => 'application/csv',
            'Content-Disposition' => 'attachment; filename="listado-'.Carbon::now()->format('d-m-Y').'.csv"',
        ]);

        return $response;

        // return redirect()->route('manage.viajeros.index');
    }

    private function buscar($query, $campo)
    {
        $filtros = [];

        if($campo=='nombre')
        {
            $qterms = explode(",", $query);

            $qnom = isset($qterms[0])?str_replace(" ","%",trim($qterms[0])):null;
            $qape1 = isset($qterms[1])?str_replace(" ","%",trim($qterms[1])):null;
            $qape2 = isset($qterms[2])?str_replace(" ","%",trim($qterms[2])):null;

            if($qnom)
            {
                $filtros[] = ['name', 'LIKE' ,"%$qnom%"];
            }

            if($qape1)
            {
                $filtros[] = ['lastname', 'LIKE' ,"%$qape1%"];
            }

            if($qape2)
            {
                $filtros[] = ['lastname2', 'LIKE' ,"%$qape2%"];
            }
        }

        // $filtrop = [];
        $plat = ConfigHelper::config('propietario');
        if($plat)
        {
            $filtros[] = ['plataforma','=',$plat];
            // $filtrop = ['plataforma','=',$plat];
        }

        $col = collect([]);

        if($campo=='email')
        {
            // $col = $this->viajero->findWhere([
            //     ['email', 'LIKE' ,"%$query%"], $filtrop
            // ]);

            $col = ViajeroModel::where('plataforma', $plat)->where( function($q) use ($query) {
               return $q
                ->where('email', 'LIKE', "%$query%")
                ->orWhere('phone', 'LIKE', "%$query%")
                ->orWhere('movil', 'LIKE', "%$query%");
            })->get();
        }
        elseif($campo=='nombre')
        {
            $col = $this->viajero->findWhere($filtros);
        }

        return $col;
    }

    public function getIndexBuscar(Request $request, $campo='nombre', $query="")
    {
        //Buscador: nombre: nombre+ape1+ape2, email:e-mail
        //Tipo: 1: viajeros, 2:tutores

        // $this->viajero->pushCriteria(new FiltroPlataformaAsignado());
        // $this->viajero->pushCriteria(new FiltroPlataformaViajero());

        // $query = $request->input('query');

        if(Datatable::shouldHandle())
        {
            $col = $this->buscar($query,$campo);

            return Datatable::collection( $col )
                ->showColumns('email','rating')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.viajeros.ficha',[$model->id]) ."'>$model->full_name</a>";
                })
                ->addColumn('asignado', function($model) {
                    return $model->asignado?$model->asignado->fname." ".$model->asignado->lname:"-";
                })
                ->addColumn('status', function($model) {

                    $ret = "";
                    if($model->booking)
                    {
                        $booking = $model->booking;
                        $ret = "<span class='badge'>". $booking->status_name . "</span>&nbsp;";

                        if($booking->incidencias->count())
                        {
                            $icono = "<i class='badge badge-warning badge-incidencias'>". $booking->incidencias->count() ."</i>";
                            if($booking->tiene_incidencias_pendientes)
                            {
                                $icono = "<i class='fa fa-exclamation-triangle' style='color:red;'></i>";

                            }

                            $ret .= "<a href='". route('manage.bookings.ficha', $booking->id) ."#incidencias'>$icono</a>";
                        }

                        return $ret;
                    }

                    return "<span class='badge'>". ($model->status?$model->status->name:"-") . "</span> $ret";
                })
                ->addColumn('creado', function($model) {
                    return $model->created_at->format('d/m/Y');
                })
                ->addColumn('tutores', function($model) {

                    $ret = "";

                    $tot = $model->tutores->count();
                    $itot = 0;
                    foreach($model->tutores as $t)
                    {
                        $relacion = ConfigHelper::getTutorRelacion($t->pivot->relacion);

                        $ret .= "<a href='". route('manage.tutores.ficha',[$t->id]) ."'>$t->name $t->lastname</a>";
                        $ret .= " <i>(". $relacion .")</i>";

                        if(++$itot != $tot)
                        {
                            $ret .= ",<br> ";
                        }
                    }

                    return $ret;

                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    // $data = " data-label='Borrar' data-model='Grupo' data-action='". route( 'manage.proveedores.delete', $model->id) . "'";
                    // $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    // $ret .= " <a href='". route('manage.bookings.nuevo', $model->id) ."' class='btn btn-info btn-xs'><i class='fa fa-plus-circle'></i> Inscripción</a>";

                    if($model->solicitud_activa)
                    {
                        $data = " data-label='Nueva Solicitud' data-pregunta='Ya hay una solicitud abierta para este viajero. ¿Seguro que la quieres archivar y abrir una nueva?' data-action='". route('manage.solicitudes.nuevo', $model->id) . "'";
                        $ret .= " <a data-label='Nueva solicitud' $data href='#confirma' class='btn btn-danger btn-xs'><i class='fa fa-plus-circle'></i></a>";
                    }
                    else
                    {
                        $ret .= " <a data-label='Nueva solicitud' href='". route('manage.solicitudes.nuevo', $model->id) ."' class='btn btn-success btn-xs'><i class='fa fa-plus-circle'></i></a>";
                    }

                    if($model->booking)
                    {
                        $ret .= " <a data-label='Nueva Inscripción' href='". route('manage.bookings.ficha', [$model->booking_id]) ."' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i></a>";
                    }

                    if(!$model->es_cliente)
                    {
                        // $ret .= "<a href='". route('manage.viajeros.inscribir',[$model->id]) ."' class='btn btn-warning btn-xs'><i class='fa fa-arrow-circle-right'></i> Convertir a Inscrito</a>";
                    }
                    else
                    {
                        $ret .= "&nbsp;<span class='badge'>Inscrito</span>";
                    }

                    return $ret;
                })
                ->searchColumns('name')
                // ->orderColumns('name','asignado','tutores','rating')
                ->orderColumns('orden')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        $query = trim($request->input("query"));

        $col = $this->buscar($query,$campo);
        $totV = count($col);

        $totT = count(Tutor::buscar($query,$campo));

        return view('manage.viajeros.index_buscar', compact('campo','query','totV','totT'));
    }

    public function getNuevo()
    {
        $categorias = [""=>""] + Categoria::plataforma()->pluck('name','id')->toArray();

        $asignados = User::asignados()->get()->pluck('full_name', 'id');

        $statuses = StatusSolicitud::where('orden','>',0)->orderBy('orden')->get();
        $statuses0 = StatusSolicitud::where('orden',0)->get();
        $statuses = $statuses->merge($statuses0);
        $statuses = $statuses->pluck('name','id')->toArray();

        $conocidos = [""=>""] + Origen::plataforma()->pluck('name','id')->toArray();
        $prescriptores = [""=>""] + Prescriptor::plataforma()->pluck('name', 'id')->toArray();

        return view('manage.viajeros.new', compact('asignados','prescriptores','statuses','conocidos','categorias'));
    }

    public function getUpdate(Request $request, $id)
    {
        $user = $request->user();
        if(!$user->checkPermisoAislado('Viajero',$id))
        {
            Session::flash('mensaje-alert', "No tiene permisos. [Err:A].");
            return redirect()->route('manage.index');
        }

        if(!$id)
        {
            abort(404);
        }

        $ficha = $this->viajero->find($id);

        if(!$ficha)
        {
            abort(404);
        }

        if( $ficha->setBooking() )
        {
            $ficha = $this->viajero->find($id);
        }

        if(!$ficha->datos)
        {
            $datos = [];
            $datos['viajero_id'] = $id;
            $this->datos->create($datos);
        }

        $categorias = [""=>""] + Categoria::plataforma()->pluck('name','id')->toArray();

        $subcategorias = [""=>""];
        $subcategorias_det = [""=>""];

        if($ficha->solicitud)
        {
            $subcategorias = [""=>""] + Subcategoria::where('category_id',$ficha->solicitud->category_id)->pluck('name','id')->toArray();
            $subcategorias_det = [""=>""] + SubcategoriaDetalle::where('subcategory_id',$ficha->solicitud->subcategory_id)->pluck('name','id')->toArray();
        }
        $asignados = [""=>"Sin Asignar"] + User::asignados()->get()->pluck('full_name', 'id')->toArray();

        $statuses = [0=>"Sin Estado"] + StatusSolicitud::orderBy('orden')->pluck('name','id')->toArray();
        // $statuses = [0=>'Achivado'] + StatusSolicitud::orderBy('orden')->pluck('name','id')->toArray();
        if($ficha->es_booking)
        {
            $statuses = StatusBooking::where('manual',1)->orderBy('orden')->pluck('name','id');
        }

        $conocidos = [""=>""] + Origen::plataforma()->pluck('name','id')->toArray();
        $subconocidos = [""=>""] + Suborigen::where('origen_id',$ficha->origen_id)->orderBy('name')->pluck('name','id')->toArray();
        $subconocidosdet = [""=>""] + SuborigenDetalle::where('suborigen_id',$ficha->suborigen_id)->orderBy('name')->pluck('name','id')->toArray();

        $prescriptores = [0=>"Sin Prescriptor"] + Prescriptor::plataforma()->pluck('name', 'id')->toArray();

        $error_MailTutor = true;
        foreach($ficha->tutores as $t)
        {
            if(filter_var($t->email, FILTER_VALIDATE_EMAIL))
            {
                $error_MailTutor = false;
            }
        }

        // $tutores = [];//Tutor::plataforma()->pluck('full_name', 'id')->toArray(); //peta!
        $oficinas = [0=>''] + Oficina::plataforma()->pluck('name', 'id')->toArray();

        $ficha = $this->viajero->find($id);

        return view('manage.viajeros.ficha', compact('ficha','error_MailTutor','asignados','prescriptores','statuses','conocidos','subconocidos','subconocidosdet','categorias','subcategorias','subcategorias_det','oficinas'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $viajero = $this->viajero->find($id);

        if( $request->has('submit_asignar') ) //cambiar asign_to
        {
            ViajeroLog::addLogAsignado($viajero, $request->input('asign_to'));

            $viajero->asign_to = $request->input('asign_to');
            $viajero->save();

            if(!$viajero->plataforma)
            {
                $user = User::find($request->input('asign_to'));
                if($user)
                {
                    $viajero->plataforma = $user->plataforma?:ConfigHelper::config('propietario');
                    $viajero->save();
                }
            }

            if(!$viajero->oficina_id)
            {
                $user = User::find($request->input('asign_to'));
                if($user)
                {
                    $viajero->oficina_id = User::find($request->input('asign_to'))->oficina_id;
                    $viajero->save();
                }
            }

            if($viajero->solicitud)
            {
                $s = $viajero->solicitud;
                $s->user_id = $request->input('asign_to');
                $s->origen_id = $viajero->origen_id;
                $s->suborigen_id = $viajero->suborigen_id;
                $s->suborigendet_id = $viajero->suborigendet_id;
                $s->oficina_id = $viajero->oficina_id;
                $s->save();
            }

            return redirect()->route('manage.viajeros.ficha',$id);
        }
        elseif( $request->has('submit_prescriptor') )
        {
            ViajeroLog::addLogPrescriptor($viajero, $request->input('prescriptor_id'));

            $viajero->prescriptor_id = $request->input('prescriptor_id');
            $viajero->save();

            if($viajero->booking)
            {
                $viajero->booking->prescriptor_id = $request->input('prescriptor_id');
                $viajero->booking->save();
            }

            return redirect()->route('manage.viajeros.ficha',$id);
        }
        elseif( $request->has('submit_medicos') )
        {
            Session::flash('tab','#medicos');

            $datos = $viajero->datos;
            $datos->medicacion = $request->input('medicacion_bool')?$request->input('medicacion'):"";
            $datos->alergias = $request->input('alergias_bool')?$request->input('alergias'):"";
            $datos->tratamiento = $request->input('tratamiento_bool')?$request->input('tratamiento'):"";
            $datos->enfermedad = $request->input('enfermedad_bool')?$request->input('enfermedad'):"";
            $datos->dieta = $request->input('dieta_bool')?$request->input('dieta'):"";

            $datos->animales = $request->input('animales_bool')?$request->input('animales'):"";

            $datos->medicacion2 = $request->input('medicacion_bool')?$request->input('medicacion2'):"";
            $datos->alergias2 = $request->input('alergias_bool')?$request->input('alergias2'):"";
            $datos->tratamiento2 = $request->input('tratamiento_bool')?$request->input('tratamiento2'):"";
            $datos->enfermedad2 = $request->input('enfermedad_bool')?$request->input('enfermedad2'):"";
            $datos->dieta2 = $request->input('dieta_bool')?$request->input('dieta2'):"";
            $datos->save();

            // dd($datos);

            if($viajero->booking)
            {
                $viajero->booking->updateDatos();
            }

            //Datos medicos
            return redirect()->route('manage.viajeros.ficha',$id);
        }
        elseif( $request->has('submit_adultos') )
        {
            Session::flash('tab','#info-adulto');

            $datos = $viajero->datos;
            $datos->fumador = $request->has('fumador');
            $datos->profesion = $request->input('profesion');
            $datos->empresa = $request->input('empresa');
            $datos->save();

            if($viajero->booking)
            {
                $viajero->booking->updateDatos();
            }

            //Datos medicos
            return redirect()->route('manage.viajeros.ficha',$id);
        }
        elseif( $request->has('submit_notas') )
        {
            $viajero->notas = $request->input('notas');
            $viajero->save();

            if($viajero->booking)
            {
                $viajero->booking->updateDatos();
            }

            //Notas
            return redirect()->route('manage.viajeros.ficha',$id);
        }
        elseif( $request->has('submit_status') ) //cambiar status
        {
            if( $viajero && $viajero->es_booking )
            {
                $b = $viajero->booking;
                if($b->es_cancelado || $b->es_refund)
                {
                    $mensaje = "No se puede cambiar manualmente cuando está Cancelado";
                    Session::flash('mensaje', $mensaje );

                    return redirect()->route('manage.viajeros.ficha',$id);
                }
            }

            $viajero->setStatus($request->input('status_id'),$request->input('archivar_motivo'),$request->input('archivar_motivo_nota'));

            return redirect()->route('manage.viajeros.ficha',$id);
        }

        $this->validate($request, [
            'name' => 'required',
            //'email' => 'email|max:255',
            // 'asign_to' => 'required',
            'fechanac' => 'required',
            // 'email' => 'unique:viajeros,email,'.$ficha->id
        ]);

        $email = trim($request->input('email'));

        $filtro = ConfigHelper::config('propietario');
        $emailUsado = ViajeroModel::checkEmail($email);
        $bMail = $request->has('email');
        if($emailUsado)
        {
            if($id && $email == $emailUsado->email && $id == $emailUsado->id)
            {
                //Es update del mismo tutor y no cambia el mail
                $bMail = false;
            }
            else
            {
                if( $emailUsado->plataforma == $filtro )
                {
                    $mensaje = "Este email ya existe para el Viajero $emailUsado->full_name";
                    Session::flash('mensaje', $mensaje );

                    return redirect()->back()->withInput();
                }
            }
        }

        //=== Viajero y ViajeroDatos
        
        $data = $request->except('_token');
        // $data = $request->only($viajero->getFillable());

        //Viajero
        $data['es_adulto'] = $request->has('es_adulto');
        $data['fechanac'] = Carbon::createFromFormat('d/m/Y',$data['fechanac'])->format('Y-m-d');

        $data['email'] = trim($email);

        if(!$id)
        {
            // $this->validate($request, [
            //     'email' => 'unique:viajeros',
            // ]);

            $data['notas'] = null;
            $data['lastname2'] = $request->get('lastname2') ?: "";
            $data['rating'] = $request->get('rating') ?: 0;

            $o = $this->viajero->create($data);
            $id = $o->id;

            //Creamos la solicitud
            $s = new Solicitud;
            $s->fecha = Carbon::now();
            $s->status_id = 0;
            $s->viajero_id = $o->id;
            $s->user_id = $request->input('asign_to');

            $s->ciudad = $request->input('ciudad');
            $s->destinos = $request->input('destinos');
            $s->cursos = $request->input('cursos');
            $s->notas = $request->input('notas');
            $s->category_id = $request->input('category_id');
            $s->subcategory_id = $request->input('subcategory_id');
            $s->subcategory_det_id = $request->input('subcategory_det_id');
            $s->origen_id = $o->origen_id;
            $s->suborigen_id = $o->suborigen_id;
            $s->suborigendet_id = $o->suborigendet_id;
            $s->save();

            $s->setStatus($request->input('status_id'));

            $o->status_id = $request->input('status_id');
            $o->asign_to = $request->input('asign_to');
            $o->prescriptor_id = $request->input('prescriptor_id');
            $o->solicitud_id = $s->id;
            $o->save();

            //Default:
            $o->oficina_id = User::find($request->input('asign_to'))->oficina_id;
            $o->plataforma = User::find($request->input('asign_to'))->plataforma;
            $o->save();

            $s->plataforma = $o->plataforma;
            $s->oficina_id = $o->oficina_id;
            $s->save();

            ViajeroLog::addLogCreado($o);

            ViajeroLog::addLog($o, "Solicitud nueva [". $s->id ."]");
        }
        else
        {
            $viajero1 = $viajero->toArray();

            // $this->validate($request, [
            //     'email' => 'unique:viajeros,email,'.$id
            // ]);
            
            if($viajero->email != $email)
            {
                ViajeroLog::addLog($viajero, "Cambio email: $viajero->email => $email");
            }

            $fillable = $this->viajero->find($id);

            $columns = DB::connection()->getSchemaBuilder()->getColumnListing("viajeros");
            $fillable = $fillable->getFillable();
            $fields = array_keys($request->input());
            
            $except = array_diff( $fields, $columns );
            $except[] = 'id';
           
            $data = $request->except($except);
            $data['fechanac'] = Carbon::createFromFormat('d/m/Y',$data['fechanac'])->format('Y-m-d');

            $this->viajero->update($data, $id);

            ViajeroLog::addLog($viajero, 'Datos Actualizados');

            $viajero = $this->viajero->find($id);
            if($viajero->user)
            {
                $viajero->user->fname = $viajero->name;
                $viajero->user->lname = $viajero->lastname;
                $viajero->user->save();
            }

        }

        $viajero = $this->viajero->find($id);

        if($viajero->es_email_tutor)
        {
            Session::flash('mensaje', "No se actualiza el email por pertenecer ya a un Tutor [". $viajero->es_email_tutor->full_name ."].");
            $viajero->email = null;
            $viajero->save();
        }
        
        $optout = !$request->has('lopd_check2');

        $es_rgpd = ConfigHelper::config('es_rgpd');
        if(!$es_rgpd)
        {
            $optout = $request->has('optout');
        }

        if($optout)
        {
            ViajeroLog::addLog($viajero, 'Optout ON');
        }
        else
        {
            if($viajero->optout)
            {
                ViajeroLog::addLog($viajero, 'Optout OFF');
            }
        }

        $viajero->optout = $optout;
        $viajero->optout_fecha = $optout?Carbon::now():null;
        if($es_rgpd)
        {
            $viajero->lopd_check2 = $request->has('lopd_check2');
        }
        $viajero->save();

        //ViajeroDatos
        if(!$viajero->datos)
        {
            $datos = [];
            $datos['viajero_id'] = $id;
            $this->datos->create($datos);
        }
        else
        {
            $viajeroDatos1 = $viajero->datos->toArray();
        }

        //ViajeroDatos
        $viajero = $this->viajero->find($id);
        // $datad = $request->input();

        $fillable = $viajero->datos;
        $columns = DB::connection()->getSchemaBuilder()->getColumnListing("viajero_datos");
        $fillable = $fillable->getFillable();
        $fields = array_keys($request->input());
        
        $except = array_diff( $fields, $columns );
        $except[] = 'id';
        $except[] = 'viajero_id';

        $datad = $request->except($except);
        $datad['fumador'] = $request->has('fumador');
        $datad['pasaporte_emision'] = $request->get('pasaporte_emision') ? Carbon::createFromFormat('d/m/Y', $request->get('pasaporte_emision'))->format('Y-m-d'):"";
        $datad['pasaporte_caduca'] = $request->get('pasaporte_caduca') ? Carbon::createFromFormat('d/m/Y', $request->get('pasaporte_caduca'))->format('Y-m-d'):"";
        $datad['viajero_id'] = $id;
        $datad['cic'] = $request->has('cic');
        $datad['cic_nivel'] =  $request->has('cic') ? $request->get('cic_nivel') : "";

        if($request->get("datos_ciudad"))
        {
            $datad['ciudad'] = $request->get("datos_ciudad");    
        }
        // $datad['notas'] = $viajero->datos->notas;

        $viajero->datos->update($datad);

        //dd($viajero->datos);

        if($viajero->solicitud)
        {
            $s = $viajero->solicitud;
            $s->rating = $viajero->rating;
            $s->origen_id = $viajero->origen_id;
            $s->suborigen_id = $viajero->suborigen_id;
            $s->suborigendet_id = $viajero->suborigendet_id;
            $s->oficina_id = $viajero->oficina_id;
            $s->save();
        }

        //oficina
        if( $request->has('oficina_id') )
        {
            $oficina_id = $request->input('oficina_id');
            if($oficina_id!=$viajero->oficina_id)
            {
                $oficina = Oficina::find($oficina_id);
                ViajeroLog::addLogOficina($viajero, $oficina );

                $viajero->oficina_id = $oficina_id;
                $viajero->save();
            }
        }

        //email usuario?
        $email = trim($data['email']);
        if($email)
        {
            if($viajero->usuario && ($viajero->usuario->username!=$email))
            {
                $viajero->usuario->username = $email;
                $viajero->usuario->email = $email;
                $viajero->usuario->save();

                MailHelper::mailAreaEmail($viajero);
            }
        }
        else
        {
            //lo borramos
            if($viajero->usuario)
            {
                $viajero->usuario->delete();
                $viajero->user_id = 0;
                $viajero->save();

                Session::flash('mensaje','Al borrar el email se ha eliminado el área de cliente.');
            }
        }

        //Creamos area si no la creó bien
        if( $viajero->es_booking_area )
        {
            if(!$viajero->usuario)
            {
                $viajero->setUsuarios();
            }

            //Si algun tutor no tiene usuario
            foreach ($viajero->tutores as $tutor) {
                if (!$tutor->usuario) {
                    $viajero->setUsuarios();
                    break;
                }
            }
        }
        
        if($viajero->booking)
        {
            $viajero->booking->updateDatos();
        }

        return redirect()->route('manage.viajeros.ficha',$id);
    }


    public function archivar($id,$estado=true)
    {
        $v = $this->viajero->find($id);
        $v->status_id = 0;
        // $v->booking_id = 0;
        // $v->booking_status_id = 0;
        // $v->es_cliente = false;

        //Archivado + fecha, motivo
        $v->archivado = !$estado;
        $v->archivado_fecha = Carbon::now()->format('Y-m-d');
        $v->archivado_motivo = $estado?"Archivar":"Recuperar";
        $v->save();

        ViajeroLog::addLog($v, ($estado?"Archivar":"Recuperar"));

        return redirect()->route('manage.viajeros.index');

        // dd("archivar");
    }

    public function destroy($id)
    {
        $v = $this->viajero->find($id);

        if( $v->bookings->count()>0 )
        {
            Session::flash('mensaje-alert','Viajero con Bookings. No se puede eliminar.');
            return redirect()->route('manage.viajeros.ficha',$id);
        }

        if( $v->tutores->count()>0 )
        {
            Session::flash('mensaje-alert','Viajero con Tutores. No se puede eliminar. Debe desvincularlo.');
            return redirect()->route('manage.viajeros.ficha',$id);
        }


        if($v->usuario)
        {
            $v->usuario->delete();
        }

        $this->viajero->delete($id);
        return redirect()->route('manage.viajeros.index');
    }


    public function getDatos($id)
    {
        $viajero = $this->viajero->find($id);
        $ficha = $viajero->datos;

        if(!$ficha)
        {
            $data['viajero_id'] = $id;
            $ficha = $this->datos->create($data);
        }

        return view('manage.viajeros.ficha_datos', compact('viajero','ficha'));
    }

    public function postDatos(Request $request, $id)
    {
        $viajero = $this->viajero->find($id);
        $ficha = $viajero->datos;

        if( $request->has('facturacion') )
        {
            $data = $request->except('_token','facturacion');
            $this->datos->update($data, $ficha->id);

            Session::flash('tab','#facturas');

            return redirect()->route('manage.viajeros.ficha',$id);
        }

        $this->validate($request, [

        ]);

        $viajero = $this->viajero->find($id);
        $ficha = $viajero->datos;

        $data = $request->except('_token','alergias_bool','medicacion_bool','animales_bool','curso_anterior_bool','tratamiento_bool','enfermedad_bool','dieta_bool');
        $data['fumador'] = $request->has('fumador');
        $data['pasaporte_emision'] = $request->get('pasaporte_emision')?Carbon::createFromFormat('d/m/Y',$request->get('pasaporte_emision'))->format('Y-m-d'):"";
        $data['pasaporte_caduca'] = $request->get('pasaporte_caduca')?Carbon::createFromFormat('d/m/Y',$request->get('pasaporte_caduca'))->format('Y-m-d'):"";
        $data['ingles'] = $request->get('ingles_academia_bool');

        // $data['h1_fnac'] = $data['h1_fnac']?Carbon::createFromFormat('d/m/Y',$data['h1_fnac'])->format('Y-m-d'):"";
        // $data['h2_fnac'] = $data['h2_fnac']?Carbon::createFromFormat('d/m/Y',$data['h2_fnac'])->format('Y-m-d'):"";
        // $data['h3_fnac'] = $data['h3_fnac']?Carbon::createFromFormat('d/m/Y',$data['h3_fnac'])->format('Y-m-d'):"";
        // $data['h4_fnac'] = $data['h4_fnac']?Carbon::createFromFormat('d/m/Y',$data['h4_fnac'])->format('Y-m-d'):"";

        if(!$ficha)
        {
            //nuevo
            $o = $this->datos->create($data);
        }
        else
        {
            $this->datos->update($data, $ficha->id);
        }

        if($viajero->booking)
        {
            $viajero->booking->updateDatos();
        }

        return redirect()->route('manage.viajeros.ficha.datos',$id);
    }

    public function getArea($viajero_id)
    {
        $viajero = $this->viajero->find($viajero_id);
        $usuario = $viajero->user;

        // dd($usuario);

        if(!$usuario)
        {
            return false;
        }

        $ficha = $usuario->ficha;
        Session::put('vcn.manage.area.home', route('manage.viajeros.area',$ficha->id) );
        Session::put('vcn.manage.area.tipo', "viajero" );
        Session::put('vcn.manage.area.ficha', $ficha->id );

        return view('area.index',compact('usuario','ficha','viajero'));
    }

    public function getFacturas($viajero_id)
    {
        $viajero = $this->viajero->find($viajero_id);

        if(Datatable::shouldHandle())
        {
            $col = $viajero->facturas;

            return Datatable::collection( $col )
                ->addColumn('fecha', function($model) {
                    return $model->fecha->format('Y-m-d');
                })
                ->showColumns('numero')
                ->addColumn('booking_id', function($model) {
                    return "<a href='". route('manage.bookings.ficha', $model->booking_id) ."'>". $model->booking_id ."</a>";
                })
                ->addColumn('viajero', function($model) {
                    return "<a href='". route('manage.viajeros.ficha', $model->booking->viajero_id) ."'>". $model->booking->viajero->full_name ."</a>";
                })
                ->addColumn('total', function($model) {
                    return $model->grup?$model->grup->total:$model->total;
                })
                ->addColumn('grup', function($model) {
                    return $model->grup?"Si":"No";
                })
                ->addColumn('manual', function($model) {
                    return $model->manual?"Si":"No";
                })
                ->addColumn('pdf', function($model) {

                    if($model->manual)
                    {
                        return "";
                    }

                    if($model->grup)
                    {
                        $dir = "/files/facturas/";
                        $name = "factura_". $model->grup->numero .".pdf";
                    }
                    else
                    {
                        $dir = "/files/bookings/". $model->booking->viajero->id . "/";
                        $name = "factura_". $model->numero .".pdf";
                    }

                    $file = $dir . $name;
                    return "<a href='$file' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    if($model->manual)
                    {
                        $data = " data-label='Borrar' data-model='Factura' data-action='". route( 'manage.bookings.facturas.delete', $model->id) . "'";
                        $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";
                    }

                    return $ret;
                })
                ->searchColumns('fecha','total')
                ->orderColumns('numero','fecha','manual','grup')
                ->setAliasMapping()
                // ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        abort(404);
    }

    /**
     * Visitas
     */
    public function getTrafico($id)
    {
        $viajero = $this->viajero->find($id);

        if(!$viajero->user)
        {
            return null;
        }

        $user = $viajero->usuario;
        if(!$user->visitas->count())
        {
            return null;
        }

        if( Datatable::shouldHandle() )
        {
            $col = $user->visitas;

            return Datatable::collection( $col )
                ->addColumn('fecha', function($model) {
                    return $model->fecha->format('Y-m-d');
                })
                ->addColumn('curso', function($model) {
                    return $model->curso?$model->curso->name:"Web Home";
                })
                ->addColumn('visitas', function($model) {
                    return $model->visitas;
                })
                ->searchColumns('fecha','curso')
                ->orderColumns('fecha','curso')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

    }

    public function postExam(Request $request, ViajeroModel $viajero)
    {
        $destinatarios = $request->get('destinatarios');
        if(!$destinatarios)
        {
            Session::flash('mensaje-alert', "No ha seleccionado ningún destinatario.");
            return redirect()->back();
        }

        $exam = $request->get('examen_id', 0);
        if(!$exam)
        {
            Session::flash('mensaje-alert', "No ha seleccionado quién le ha autorizado.");
            return redirect()->back();
        }

        $exam = Examen::find($exam);
        if(!$exam)
        {
            Session::flash('mensaje-alert', "Exámen no existe.");
            return redirect()->back();
        }

        $uids = $viajero->setUsuariosForce();

        $examTxt = $exam->name;

        $respuesta = Respuesta::where('examen_id', $exam->id)->where('viajero_id', $viajero->id)->where('booking_id', 0)->first();
        if($respuesta)
        {
            Session::flash('mensaje-alert', "Test ha sido enviado.");
            return redirect()->back();
        }

        $data = [
            'examen_id'     => $exam->id,
            'viajero_id'    => $viajero->id,
            'booking_id'    => 0,
            'tipo'          => 'viajero',
            'status'        => -1,
        ];

        Respuesta::create($data);

        $i = 0;
        foreach($destinatarios as $dest)
        {
            if(is_numeric($dest) && $dest <= 0)
            {
                continue;
            }

            if(is_numeric($dest))
            {
                $user = User::find($dest);
                
                MailHelper::mailExamen($exam, $viajero, $user);
                $i++;
                ViajeroLog::addLog($viajero, "Enviar Test ($examTxt) a ". $user->full_name);
            }
            elseif($dest == "v")
            {
                if($viajero->user)
                {
                    $user = $viajero->user;

                    MailHelper::mailExamen($exam, $viajero, $user, false);
                    $i++;
                    ViajeroLog::addLog($viajero, "Enviar Test ($examTxt) a Viajero");
                }
            }
            elseif($dest == "t1")
            {
                if($viajero->tutor1 && $viajero->tutor1->user )
                {
                    $user = $viajero->tutor1->user;
                    
                    MailHelper::mailExamen($exam, $viajero, $user, false);
                    $i++;
                    ViajeroLog::addLog($viajero, "Enviar Test ($examTxt) a Tutor1 [$user->full_name]");
                }
            }
            elseif($dest == "t2")
            {
                if($viajero->tutor2 && $viajero->tutor2->user )
                {
                    $user = $viajero->tutor2->user;
                    
                    MailHelper::mailExamen($exam, $viajero, $user, false);
                    $i++;
                    ViajeroLog::addLog($viajero, "Enviar Test ($examTxt) a Tutor2 [$user->full_name]");
                }
            }
        }

        Session::flash('mensaje-ok', "Test enviado [$i envíos].");
        return redirect()->back();
    }
}
