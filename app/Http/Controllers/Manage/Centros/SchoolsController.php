<?php

namespace VCN\Http\Controllers\Manage\Centros;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Centros\SchoolRepository as School;
use VCN\Models\Centros\Centro;
use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingSchool;
use VCN\Models\Bookings\BookingLog;

use Datatable;
use Image;
use File;
use Session;
use ConfigHelper;

use VCN\Repositories\Criteria\FiltroPlataformaCentro;

class SchoolsController extends Controller
{

    private $school;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( School $school )
    {
        $this->middleware("permiso.plataforma:proveedores,\VCN\Models\Centros\School", ['only' => ['getUpdate']]);
        $this->checkPermisos('proveedores');

        $this->school = $school;
    }

    public function getIndex($centro_id=0)
    {
        $this->school->pushCriteria(new FiltroPlataformaCentro());

        if(Datatable::shouldHandle())
        {
            if($centro_id)
            {
                $col = Centro::find($centro_id)->schools;
            }
            else
            {
                $col = $this->school->all();
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.centros.schools.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('activo', function($model) {
                    return $model->activo?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('bookings', function($model) {
                    return $model->bookings->count();
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='School' data-action='". route( 'manage.centros.schools.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.centros.schools.index', compact('centro_id'));
    }

    public function getHistorial($school_id)
    {
        $school = $this->school->find($school_id);
        $bf = $school->bookings->pluck('booking_id');

        $bookings = Booking::whereIn('id',$bf);

        if( !auth()->user()->isFullAdmin() )
        {
            $filtro = ConfigHelper::config('propietario');
            $bookings = $bookings->where('plataforma',$filtro);
        }

        $b = $bookings->pluck('id')->toArray();

        $bookings = BookingSchool::where('school_id',$school_id)->whereIn('booking_id',$b);

        if(Datatable::shouldHandle())
        {
            $bookings = $bookings->get();

            return Datatable::collection( $bookings )
                ->addColumn('booking', function($model) {
                    return "<a href='". route('manage.bookings.ficha',[$model->booking_id]) ."'>$model->booking_id</a>";
                })
                ->addColumn('viajero', function($model) {
                    return "<a href='". route('manage.viajeros.ficha',[$model->booking->viajero_id]) ."'>". $model->booking->viajero->full_name ."</a>";
                })
                ->addColumn('fecha_ini', function($model) {
                    return $model->booking->course_start_date;
                })
                ->addColumn('fecha_fin', function($model) {
                    return $model->booking->course_end_date;
                })
                ->addColumn('fecha_asignacion', function($model) {
                    $log = $model->booking->logs->where('tipo',"Asignación School creado [$model->school_id]")->sortByDesc('created_at')->first();

                    if(!$log)
                    {
                        return "-";
                    }

                    return $log->created_at->format('d/m/Y H:i');
                })
                ->addColumn('user_asignacion', function($model) {
                    $log = $model->booking->logs->where('tipo',"Asignación School creado [$model->school_id]")->sortByDesc('created_at')->first();

                    if(!$log)
                    {
                        return "-";
                    }

                    return $log->user->full_name;
                })
                ->addColumn('fecha_activacion', function($model) {
                    $log = $model->booking->logs->where('tipo',"Asignación School email [$model->school_id]")->sortBy('created_at')->first();

                    if(!$log)
                    {
                        return "-";
                    }

                    return $log->created_at->format('d/m/Y H:i');
                })
                ->addColumn('user_activacion', function($model) {
                    $log = $model->booking->logs->where('tipo',"Asignación School email [$model->school_id]")->sortBy('created_at')->first();

                    if(!$log)
                    {
                        return "-";
                    }

                    return $log->user->full_name;
                })
                ->addColumn('fecha_cambio', function($model) {
                    $log = $model->booking->logs->where('tipo',"Asignación School modificado [$model->school_id]")->sortByDesc('created_at')->first();

                    if(!$log)
                    {
                        return "-";
                    }

                    return $log->created_at->format('d/m/Y H:i');
                })
                ->addColumn('user_cambio', function($model) {
                    $log = $model->booking->logs->where('tipo',"Asignación School modificado [$model->school_id]")->sortByDesc('created_at')->first();

                    if(!$log)
                    {
                        return "-";
                    }

                    return $log->user->full_name;
                })
                ->searchColumns('booking')
                ->orderColumns('booking','viajero','fecha_ini','fecha_fin')//,'fecha_activacion','fecha_asignacion','fecha_cambio')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return redirect()->back();
    }


    public function getNuevo($centro_id=0)
    {
        $centro = Centro::find($centro_id);

        return view('manage.centros.schools.new', compact('centro'));
    }

    public function getUpdate($id=0)
    {
        $ficha = $this->school->find($id);
        $centro = $ficha->centro;

        return view('manage.centros.schools.ficha', compact('centro','ficha'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'center_id' => 'required',
        ]);

        $data = $request->except('_token','foto');

        $data['activo'] = $request->has('activo');

        if(!$id)
        {
            $ficha = $this->school->create($data);
            $id = $ficha->id;
        }
        else
        {
            $this->school->update($data, $id);
            $ficha = $this->school->find($id);
        }

        $ficha->save();

        if($request->hasFile('foto'))
        {
            $dirp = "assets/uploads/school/". $id . "/";

            $file = $request->file('foto');
            ConfigHelper::uploadOptimize($file, $dirp);
            return response()->json('success', 200);

            // $file = $request->file('foto');
            // $ficha->foto = ConfigHelper::uploadFoto($file, $dirp, 800);
            // $ficha->save();
        }

        // Session::flash('tab','#schools');
        // return redirect()->route('manage.centros.ficha', $ficha->center_id);
        return redirect()->route('manage.centros.schools.ficha', $id);

    }

    public function destroy($id)
    {
        $this->school->find($id)->delete();

        Session::flash('tab','#schools');
        return redirect()->back();
    }

    public function getPdf(Request $request, $school_id)
    {
        $idiomaapp = \App::getLocale();

        $ficha = $this->school->find($school_id);

        if(!$ficha)
        {
            abort(404);
        }

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path()."/tmp");
        $pdf = \PDF::loadView('manage.centros.schools.area-pdf', compact('ficha'));
        $nombre = 'school-'.str_slug($ficha->name);

        $pdf->setOption('margin-top',30);
        $pdf->setOption('margin-right',0);
        $pdf->setOption('margin-bottom',30);
        $pdf->setOption('margin-left',0);
        $pdf->setOption('no-print-media-type',false);
        $pdf->setOption('footer-spacing',0);
        $pdf->setOption('header-font-size',9);
        $pdf->setOption('header-spacing',0);
        //$pdf->setOption('javascript-delay',20000);
        $pdf->setOption('header-html', 'https://'.ConfigHelper::config('web').'/assets/logos/'.$idiomaapp.'/'.ConfigHelper::config('sufijo').'header.html');
        $pdf->setOption('footer-html', 'https://'.ConfigHelper::config('web').'/assets/logos/'.$idiomaapp.'/'.ConfigHelper::config('sufijo').'footer.html');

        //return $pdf->stream("$nombre.pdf");
        return $pdf->download("$nombre.pdf");
    }

}
