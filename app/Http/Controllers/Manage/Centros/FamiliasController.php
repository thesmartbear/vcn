<?php

namespace VCN\Http\Controllers\Manage\Centros;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Centros\FamiliaRepository as Familia;
use VCN\Models\Centros\Centro;
use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingFamilia;
use VCN\Models\Bookings\BookingLog;

use VCN\Models\Centros\Familia as FamiliaModel;

use Datatable;
use Image;
use File;
use Session;
use ConfigHelper;

use VCN\Repositories\Criteria\FiltroPlataformaCentro;

class FamiliasController extends Controller
{

    private $familia;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Familia $familia )
    {
        $this->middleware("permiso.plataforma:proveedores,\VCN\Models\Centros\Familia", ['only' => ['getUpdate']]);
        $this->checkPermisos('proveedores');

        $this->familia = $familia;
    }

    public function getIndex($centro_id=0)
    {
        $this->familia->pushCriteria(new FiltroPlataformaCentro());

        if(Datatable::shouldHandle())
        {
            if($centro_id)
            {
                $col = Centro::find($centro_id)->familias;
            }
            else
            {
                $col = $this->familia->all();
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.centros.familias.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('activo', function($model) {
                    return $model->activo?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('bookings', function($model) {
                    return $model->bookings->count();
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Familia' data-action='". route( 'manage.centros.familias.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.centros.familias.index', compact('centro_id'));
    }

    public function getHistorial($familia_id)
    {
        // $this->familia->pushCriteria(new FiltroPlataformaCentro());

        $familia = $this->familia->find($familia_id);
        $bf = $familia->bookings->pluck('booking_id');

        $bookings = Booking::whereIn('id',$bf);

        if( !auth()->user()->isFullAdmin() )
        {
            $filtro = ConfigHelper::config('propietario');
            $bookings = $bookings->where('plataforma',$filtro);
        }

        $b = $bookings->pluck('id')->toArray();

        $bookings = BookingFamilia::where('familia_id',$familia_id)->whereIn('booking_id',$b);

        if(Datatable::shouldHandle())
        {
            $bookings = $bookings->get();

            return Datatable::collection( $bookings )
                ->addColumn('booking', function($model) {
                    return "<a href='". route('manage.bookings.ficha',[$model->booking_id]) ."'>$model->booking_id</a>";
                })
                ->addColumn('viajero', function($model) {
                    return "<a href='". route('manage.viajeros.ficha',[$model->booking->viajero_id]) ."'>". $model->booking->viajero->full_name ."</a>";
                })
                ->addColumn('fecha_ini', function($model) {
                    return $model->booking->course_start_date;
                })
                ->addColumn('fecha_fin', function($model) {
                    return $model->booking->course_end_date;
                })
                ->addColumn('fecha_asignacion', function($model) {
                    $log = $model->booking->logs->where('tipo',"Asignación Familia creado [$model->familia_id]")->sortByDesc('created_at')->first();

                    if(!$log)
                    {
                        return "-";
                    }

                    return $log->created_at->format('d/m/Y H:i');
                })
                ->addColumn('user_asignacion', function($model) {
                    $log = $model->booking->logs->where('tipo',"Asignación Familia creado [$model->familia_id]")->sortByDesc('created_at')->first();

                    if(!$log)
                    {
                        return "-";
                    }

                    return $log->user->full_name;
                })
                ->addColumn('fecha_activacion', function($model) {
                    $log = $model->booking->logs->where('tipo',"Asignación Familia email [$model->familia_id]")->sortBy('created_at')->first();

                    if(!$log)
                    {
                        return "-";
                    }

                    return $log->created_at->format('d/m/Y H:i');
                })
                ->addColumn('user_activacion', function($model) {
                    $log = $model->booking->logs->where('tipo',"Asignación Familia email [$model->familia_id]")->sortBy('created_at')->first();

                    if(!$log)
                    {
                        return "-";
                    }

                    return $log->user->full_name;
                })
                ->addColumn('fecha_cambio', function($model) {
                    $log = $model->booking->logs->where('tipo',"Asignación Familia modificado [$model->familia_id]")->sortByDesc('created_at')->first();

                    if(!$log)
                    {
                        return "-";
                    }

                    return $log->created_at->format('d/m/Y H:i');
                })
                ->addColumn('user_cambio', function($model) {
                    $log = $model->booking->logs->where('tipo',"Asignación Familia modificado [$model->familia_id]")->sortByDesc('created_at')->first();

                    if(!$log)
                    {
                        return "-";
                    }

                    return $log->user->full_name;
                })
                ->searchColumns('booking')
                ->orderColumns('booking','viajero','fecha_ini','fecha_fin')//,'fecha_activacion','fecha_asignacion','fecha_cambio')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return redirect()->back();
    }


    public function getNuevo($centro_id=0)
    {
        $centro = Centro::find($centro_id);

        return view('manage.centros.familias.new', compact('centro'));
    }

    public function getUpdate($id=0)
    {
        $ficha = $this->familia->find($id);
        $centro = $ficha->centro;

        return view('manage.centros.familias.ficha', compact('centro','ficha'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'center_id' => 'required',
        ]);

        $data = $request->except('_token','hijos_bool','animales_bool','habitacion_compartida_bool','hijos_fuera_bool','familia_bool','adultos_foto');

        $data['habitacion_compartida'] = $request->input('habitacion_compartida_bool');
        $data['activo'] = $request->has('activo');

        unset($data['adultos']['foto']);

        // dd($data);

        $fa1 = null;

        if(!$id)
        {
            $ficha = $this->familia->create($data);
            $id = $ficha->id;
        }
        else
        {
            //Guardamos antes fotos para que no se machaquen
            $ficha = $this->familia->find($id);
            $fa1 = $ficha->adultos;

            // dd($data);
            
            // $this->familia->update($data, $id);
            $ficha->update($data);
            $ficha = $this->familia->find($id);
        }

        // dd($request->input('hijos'));

        if(!$request->input('animales_bool'))
        {
            $ficha->animales = null;
        }

        if(!$request->input('hijos_bool'))
        {
            $ficha->hijos = null;
        }

        if(!$request->input('hijos_fuera_bool'))
        {
            $ficha->hijos_fuera = null;
        }

        if(!$request->input('familia_bool'))
        {
            $ficha->familia = null;
        }

        if(!$request->input('habitacion_compartida_bool'))
        {
            $ficha->habitacion_compartida_notas = null;
        }

        $ficha->save();

        // dd( $request->file("adultos") );

        //adultos->fotos
        if(is_array($ficha->adultos))
        {
            if($request->file("adultos"))
            {
                foreach($request->file("adultos")['foto'] as $k=>$file)
                {
                    if($file)
                    {
                        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                        $file_name = str_slug($file_name) .".". $file->getClientOriginalExtension();
                        $dirp = "assets/uploads/familia/". $id . "/adultos/";
                        $dir = public_path($dirp);
                        $file->move($dir, $file_name);

                        $img = Image::make($dir.$file_name);
                        $img->resize(1920, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save();

                        $fa = $ficha->adultos;
                        $fa['foto'][$k] = "/".$dirp.$file_name;

                        $ficha->adultos = $fa;
                        $ficha->save();
                    }
                    else
                    {
                        $fa = $ficha->adultos;
                        if( !isset($fa['foto']) )
                        {
                            $fa['foto'] = [];
                        }
                        $fa['foto'][$k] = isset($fa1['foto'])?isset($fa1['foto'][$k])?$fa1['foto'][$k]:null:null;

                        $ficha->adultos = $fa;
                        $ficha->save();
                    }
                }
            }
        }

        // Session::flash('tab','#familias');
        // return redirect()->route('manage.centros.ficha', $ficha->center_id);
        return redirect()->route('manage.centros.familias.ficha', $id);

    }

    public function destroy($id)
    {
        $this->familia->find($id)->delete();

        Session::flash('tab','#familias');
        return redirect()->back();
    }

    public function postFotoUpload(Request $request, $id)
    {

        $dirp = "assets/uploads/familia/". $id . "/";
        $dir = public_path($dirp);

        if ($request->hasFile('file'))
        {
            $file = $request->file('file');
            ConfigHelper::uploadOptimize($file, $dirp);
            return response()->json('success', 200);

            /*
            $file = $request->file('file');
            // $file = str_slug($file->getClientOriginalName()) .".". $file->getClientOriginalExtension();

            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file_name = str_slug($file_name) .".". $file->getClientOriginalExtension();

            $file->move($dir, $file_name);

            $img = Image::make($dir.$file_name);
            $img->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            //thumb
            if (!file_exists($dir."thumb/"))
            {
                File::makeDirectory($dir."thumb/", 0775, true);
            }
            File::copy($dir.$file, $dir."thumb/".$file);
            $img2 = Image::make($dir."thumb/".$file);
            $img2->resize(450, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            */
        }

        return response()->json('success', 200);
    }


    public function postFotoUploadDelete(Request $request, $id)
    {
        $dir = public_path("assets/uploads/familia/". $id . "/");

        $file = $request->input('f');
        File::delete($dir.$file);

        return response()->json('success', 200);
    }

    public function deleteFoto($id, $file)
    {
        $dir = public_path("assets/uploads/familia/". $id . "/");
        File::delete($dir.$file);

        return redirect()->route('manage.centros.familias.ficha',$id);
    }

    public function setFotoPortada($id, $file)
    {
        $c = $this->familia->find($id);

        $c->image_portada = $file;
        $c->save();

        return redirect()->route('manage.centros.familias.ficha',$id);
    }

    public function getPdf(Request $request, $familia_id)
    {
        $idiomaapp = \App::getLocale();

        $ficha = $this->familia->find($familia_id);

        if(!$ficha)
        {
            abort(404);
        }

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path()."/tmp");
        $pdf = \PDF::loadView('manage.centros.familias.area-pdf', compact('ficha'));
        $nombre = 'familia-'.str_slug($ficha->name);

        $pdf->setOption('margin-top',30);
        $pdf->setOption('margin-right',0);
        $pdf->setOption('margin-bottom',30);
        $pdf->setOption('margin-left',0);
        $pdf->setOption('no-print-media-type',false);
        $pdf->setOption('footer-spacing',0);
        $pdf->setOption('header-font-size',9);
        $pdf->setOption('header-spacing',0);
        //$pdf->setOption('javascript-delay',20000);
        $pdf->setOption('header-html', 'https://'.ConfigHelper::config('web').'/assets/logos/'.$idiomaapp.'/'.ConfigHelper::config('sufijo').'header.html');
        $pdf->setOption('footer-html', 'https://'.ConfigHelper::config('web').'/assets/logos/'.$idiomaapp.'/'.ConfigHelper::config('sufijo').'footer.html');

        //return $pdf->stream("$nombre.pdf");
        return $pdf->download("$nombre.pdf");
    }

}
