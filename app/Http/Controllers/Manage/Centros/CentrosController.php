<?php

namespace VCN\Http\Controllers\Manage\Centros;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Centros\CentroRepository as Centro;
use VCN\Models\Proveedores\Proveedor;

use VCN\Models\Pais;
use VCN\Models\Ciudad;
use VCN\Models\Monedas\Moneda;
use VCN\Models\Categoria;
use VCN\Models\Extras\ExtraUnidad;

use VCN\Models\System\Cuestionario;
use VCN\Models\Exams\Examen;

use Datatable;
use Input;
use View;
use File;
use Validator;
use Session;
use Image;
use ConfigHelper;

use VCN\Repositories\Criteria\FiltroPlataformaProveedor;

class CentrosController extends Controller
{

    private $centro;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Centro $centro )
    {
        $this->middleware("permiso.plataforma:proveedores,\VCN\Models\Centros\Centro", ['only' => ['getUpdate']]);
        $this->checkPermisos('proveedores');

        $this->centro = $centro;
    }

    public function getIndex($proveedor_id=0)
    {
        $this->centro->pushCriteria(new FiltroPlataformaProveedor());

        if(Datatable::shouldHandle())
        {
            if($proveedor_id)
            {
                $col = Proveedor::find($proveedor_id)->centros;
            }
            else
            {
                $col = $this->centro->all();
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.centros.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('pais', function($model) {
                    return $model->pais->name;
                })
                ->addColumn('ciudad', function($model) {
                    return $model->ciudad->city_name;
                })
                ->addColumn('moneda', function($model) {
                    return $model->moneda->currency_name;
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Centro y sus Alojamientos, Cursos, ...' data-action='". route( 'manage.centros.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    $ret .= " <a href='". route('manage.alojamientos.nuevo', $model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Alojamiento</a>";
                    $ret .= " <a href='". route('manage.centros.extras.nuevo', $model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Extra</a>";
                    $ret .= " <a href='". route('manage.cursos.nuevo', $model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Curso</a>";
                    $ret .= " <a href='". route('manage.centros.familias.nuevo', $model->id) ."' class='btn btn-success btn-xs' target='_blank'><i class='fa fa-plus-circle'></i> Crear Familia</a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.centros.index', compact('proveedor_id'));
    }

    public function getUpdate($id)
    {
        $ficha = $this->centro->find($id);

        if($ficha->center_images=="")
        {
            $ficha->center_images = str_slug($ficha->name);
            $ficha->save();
        }

        $paises = Pais::orderBy('name')->pluck('name','id');
        $ciudades = Ciudad::where('country_id',$ficha->country_id)->pluck('city_name','id');
        $monedas = Moneda::pluck('currency_name','id');
        $categorias = ['' => ''] + Categoria::plataforma()->pluck('name','id')->toArray();
        $unidades = ['' => ''] + ExtraUnidad::pluck('name','id')->toArray();

        $cuestionarios = ['' => ''] + Cuestionario::plataforma()->where('activo',1)->pluck('name','id')->toArray();
        $examenes = ['' => ''] + Examen::plataforma()->where('activo',1)->pluck('name','id')->toArray();

        return view('manage.centros.ficha', compact('ficha', 'paises', 'ciudades', 'monedas', 'categorias', 'unidades','cuestionarios','examenes'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $seo = Input::get('_seo',false);
        if($seo)
        {
            $this->validate($request, [
                'name' => 'required|max:255',
                'country_id' => 'required',
                'city_id' => 'required',
                'currency_id' => 'required',
            ]);

            $data = $request->except("_token");
            $this->centro->update($data, $id);

            Session::flash('tab','#seo');

            return redirect()->route('manage.centros.ficha',$id);
        }

        if(!$id)
        {
            $this->validate($request, [
                'provider_id' => 'required|numeric|min:1',
            ]);
        }


        $this->validate($request, [
            'name' => 'required|max:255',
            'country_id' => 'required',
            'city_id' => 'required',
            'currency_id' => 'required',
        ]);

        $data = $request->except("_token",'internet_bool','monitores');

        $video = explode("=", Input::get('center_video'));
        $data['center_video'] = count($video)>1?$video[1]:'';

        $data['internet'] = $request->has('internet_bool');

        if(!$id)
        {
            //nuevo
            $c = $this->centro->create($data);

            $c->center_images = "centro_". $c->id;// str_slug($c->name);
            $c->save();

            $id = $c->id;
        }
        else
        {
            $this->centro->update($data, $id);
        }

        $centro = $this->centro->find($id);

        if($centro->center_images=="")
        {
            $centro->center_images = "centro_". $c->id;// str_slug($centro->name);
            $centro->save();
        }

        //Monitores
        \VCN\Models\Monitores\MonitorRelacion::refrescar('Centro',$id, $request->get('monitores'));

        return redirect()->route('manage.centros.ficha',$id);
    }

    public function destroy($id)
    {
        $this->centro->delete($id);

        return redirect()->route('manage.centros.index');
    }

    public function getNuevo($proveedor_id=0)
    {
        $paises = ['' => ''] + Pais::pluck('name','id')->toArray();
        $ciudades = ['' => ''] + Ciudad::pluck('city_name','id')->toArray();
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();
        $unidades = ['' => ''] + ExtraUnidad::pluck('name','id')->toArray();

        $proveedores = Proveedor::plataforma()->pluck('name','id');

        $proveedor = null;
        if($proveedor_id)
        {
            $proveedor = Proveedor::find($proveedor_id);
        }

        return view('manage.centros.new', compact('paises', 'ciudades', 'monedas', 'proveedores', 'proveedor_id','proveedor'));
    }

    public function postFotoUpload(Request $request, $id)
    {
        $c = $this->centro->find($id);

        $dirp = "assets/uploads/center/". $c->center_images . "/";
        $dir = public_path($dirp);

        /*
        $rules = array(
            'file' => 'image|max:30000',
        );

        $input = Input::all();
        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return response("Error", 400);
        }
        */

        if (Input::hasFile('file'))
        {
            $file = $request->file('file');
            ConfigHelper::uploadOptimize($file, $dirp);
            return response()->json('success', 200);

            /*
            $file = Input::file('file');
            // $file = str_slug($file->getClientOriginalName()) .".". $file->getClientOriginalExtension();

            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file = str_slug($file_name) .".". $file->getClientOriginalExtension();

            // $file = Input::file('file')->getClientOriginalName();
            Input::file('file')->move($dir, $file);

            $img = Image::make($dir.$file);
            $img->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            //thumb
            if (!file_exists($dir."thumb/"))
            {
                File::makeDirectory($dir."thumb/", 0775, true);
            }
            File::copy($dir.$file, $dir."thumb/".$file);
            $img2 = Image::make($dir."thumb/".$file);
            $img2->resize(450, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            */
        }

        return response()->json('success', 200);
    }

    public function postFotoUploadDelete($id)
    {
        $c = $this->centro->find($id);
        $dir = public_path("assets/uploads/center/". $c->center_images . "/");

        $file = Input::get('f');
        File::delete($dir.$file);

        return response()->json('success', 200);
    }

    public function deleteFoto($id, $file)
    {
        $c = $this->centro->find($id);
        $dir = public_path("assets/uploads/center/". $c->center_images . "/");
        File::delete($dir.$file);

        Session::flash('tab','#imagenes');

        return redirect()->route('manage.centros.ficha',$id);
    }

    public function setFotoPortada($id, $file)
    {
        $c = $this->centro->find($id);

        $c->center_image_portada = $file;
        $c->save();

        Session::flash('tab','#imagenes');

        return redirect()->route('manage.centros.ficha',$id);
    }
}
