<?php

namespace VCN\Http\Controllers\Manage\Centros;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Centros\DescuentoRepository as Descuento;
use VCN\Models\Centros\Centro;

use VCN\Models\Categoria;

use Datatable;
use Session;
use Input;

class CentroDescuentosController extends Controller
{
    private $descuento;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Descuento $descuento )
    {
        $this->descuento = $descuento;
    }

    public function getIndex($centro_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->descuento->all();
            if($centro_id)
            {
                $col = Centro::find($centro_id)->descuentos;
            }

            return Datatable::collection( $col )
                ->addColumn('categoria', function($model) {
                    return $model->categoria?$model->categoria->name:"-";
                })
                ->addColumn('subcategoria', function($model) {
                    return $model->subcategoria?$model->subcategoria->name:"-";
                })
                ->addColumn('detalle', function($model) {
                    return $model->detalle?$model->detalle->name:"-";
                })
                ->addColumn('descuento', function($model) {
                    return $model->name;
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Descuento' data-action='". route( 'manage.centros.descuentos.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('categoria')
                ->orderColumns('categoria')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.centros.descuentos.index', compact('centro_id'));
    }

    public function add(Request $request, $centro_id=0)
    {
        Session::flash('tab','#descuentos');

        $this->validate($request, [
            'center_id' => 'required',
            'category_id' => 'required',
            'center_discount_percent' => 'required_if:tipo,0',
            'importe' => 'required_if:tipo,1',
        ]);

        $data = $request->except('_token');
        $data['importe'] = $request->get('importe') ?: 0;

        $this->descuento->create($data);

        // $this->descuento->update(Input::except('_token'), $centro_id);

        return redirect()->route('manage.centros.ficha',$centro_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $centro_id = $this->descuento->find($id)->center_id;

        $this->descuento->delete($id);

        Session::flash('tab','#descuentos');

        return redirect()->route('manage.centros.ficha',$centro_id);
    }
}
