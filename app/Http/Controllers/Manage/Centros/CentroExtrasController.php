<?php

namespace VCN\Http\Controllers\Manage\Centros;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\Centros\ExtraRepository as Extra;
use VCN\Models\Centros\Centro;
use VCN\Models\Bookings\BookingExtra;

use VCN\Models\Monedas\Moneda;
use VCN\Models\Extras\ExtraUnidad;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Session;
use Input;

class CentroExtrasController extends Controller
{
    private $extra;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct( Extra $extra )
    {
        // $this->checkPermisos('proveedores');
        $this->checkPermisos('extras');

        $this->extra = $extra;
    }

    public function getIndex($centro_id=0)
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->extra->all();
            if($centro_id)
            {
                $col = Centro::find($centro_id)->extras;
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.centros.extras.ficha',[$model->id]) ."'>$model->center_extras_name</a>";
                })
                ->addColumn('precio', function($model) {
                    $ret = $model->center_extras_currency_id?Moneda::find($model->center_extras_currency_id)->currency_name:"?";
                    $ret .= " ".$model->center_extras_price;
                    return $ret;
                })
                ->addColumn('unidades', function($model) {
                    $ret = ConfigHelper::getTipoUnidad($model->center_extras_unit);

                    if($model->center_extras_unit)
                    {
                        $ret .= " (". ($model->center_extras_unit_id?$model->tipo->name:'-') .")";
                    }

                    return $ret;
                })
                ->addColumn('requerido', function($model) {
                    return $model->center_extras_required?"<span class='label label-success'><i class='fa fa-check-circle fa-1x'></i></span>":"<span class='label label-danger'><i class='fa fa-times-circle fa-1x'></i></span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";

                    $nExtras = BookingExtra::where('tipo',1)->where('extra_id',$model->id)->count();
                    $txtExtra = "";
                    if($nExtras>0)
                    {
                        $txtExtra = "<br>(Este extra está en $nExtras Bookings. ¿Estás seguro de querer suprimirlo?)";
                    }

                    $data = " data-label='Borrar' data-model='Extra Centro. $txtExtra' data-action='". route( 'manage.centros.extras.delete', $model->id) . "'";
                    $ret = " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.centros.extras.index', compact('centro_id'));
    }

    public function getNuevo($centro_id)
    {
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();
        $unidades = ['' => ''] + ExtraUnidad::pluck('name','id')->toArray();
        $unidades_tipo = ConfigHelper::getTipoUnidad();

        $centro = Centro::find($centro_id);

        return view('manage.centros.extras.new', compact('centro','monedas','unidades','unidades_tipo'));
    }

    public function getUpdate($id)
    {
        $monedas = ['' => ''] + Moneda::pluck('currency_name','id')->toArray();
        $unidades = ['' => ''] + ExtraUnidad::pluck('name','id')->toArray();
        $unidades_tipo = ConfigHelper::getTipoUnidad();

        $ficha = $this->extra->find($id);

        return view('manage.centros.extras.ficha', compact('ficha','monedas','unidades','unidades_tipo'));
    }

    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'center_id'=> 'required',
            'center_extras_name' => 'required',
            'center_extras_price' => 'required',
            'center_extras_unit'  => 'required',
            'center_extras_unit_id'  => 'required_if:center_extras_unit,1',
            'center_extras_currency_id'    => 'required',
        ]);

        $data = Input::except('_token');
        $data['center_extras_required'] = $request->has('center_extras_required');
        $data['center_extras_unit_id'] = $request->get('center_extras_unit_id') ?: 0;

        if(!$id)
        {
            //nuevo
            $o = $this->extra->create($data);
            $center_id = $o->center_id;
        }
        else
        {
            $this->extra->update($data, $id);
            $center_id = $this->extra->find($id)->center_id;
        }

        Session::flash('tab','#extras');

        return redirect()->route('manage.centros.ficha',$center_id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $centro_id = $this->extra->find($id)->center_id;

        $this->extra->delete($id);

        Session::flash('tab','#extras');

        return redirect()->route('manage.centros.ficha',$centro_id);
    }
}
