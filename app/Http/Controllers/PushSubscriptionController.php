<?php

namespace VCN\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;

class PushSubscriptionController extends Controller
{
    use ValidatesRequests;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Update user's subscription.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, ['endpoint' => 'required']);

        $request->user()->updatePushSubscription(
            $request->endpoint,
            $request->key,
            $request->token
        );

        $t = "WebPush ON";
        $m = "Notificaciones habilitadas";
        $a = null;
        // \VCN\User::find(1)->notify(new \VCN\Notifications\TestNotification($t,$m,$a));
    }

    /**
     * Delete the specified subscription.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request, ['endpoint' => 'required']);

        $t = "WebPush OFF";
        $m = "Notificaciones deshabilitadas";
        $a = null;
        // \MySano\User::find(1)->notify(new \MySano\Notifications\TestNotification($t,$m,$a));

        $request->user()->deletePushSubscription($request->endpoint);

        return response()->json(null, 204);
    }
}
