<?php

namespace VCN\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function checkPermisos($permiso)
    {
        $this->middleware("permiso.edit:$permiso", ['only' => ['postUpdate','getNuevo']]);
        $this->middleware("permiso.view:$permiso", ['only' => ['getIndex','getUpdate']]);
    }

    public function checkPermisosFullAdmin()
    {
        $this->middleware("permiso.edit:full-admin");
        $this->middleware("permiso.view:full-admin");
    }
}
