<?php

namespace VCN\Http\Controllers\Web;

use DB;

use App;
use Log;
use PDF;

use View;
use Carbon;
use Session;
use Redirect;
use VCN\Models\Pais;
use VCN\Models\User;
use SitemapGenerator;
use VCN\Http\Requests;
use VCN\Models\Categoria;
use VCN\Helpers\Traductor;
use VCN\Models\CMS\Pagina;
use VCN\Helpers\MailHelper;

use VCN\Models\Leads\Tutor;
use Illuminate\Http\Request;

use VCN\Models\Cursos\Curso;
use VCN\Models\Especialidad;
use VCN\Models\Subcategoria;
use VCN\Helpers\ConfigHelper;
use VCN\Models\Leads\Viajero;

use VCN\Models\Centros\Centro;
use VCN\Models\Monedas\Moneda;
use VCN\Models\CMS\LandingForm;

use VCN\Models\Bookings\Booking;

use VCN\Models\CMS\CategoriaWeb;
use VCN\Models\System\Plataforma;
use VCN\Helpers\ExternalApiHelper;
use VCN\Models\CMS\EspecialidadWeb;
use VCN\Models\SubcategoriaDetalle;
use VCN\Http\Controllers\Controller;
use VCN\Models\CMS\{Promo, Landing};
use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Traducciones\Traduccion;
use Illuminate\Support\Facades\Response;

use VCN\Models\Alojamientos\Alojamiento;
use VCN\Models\Cursos\CursoEspecialidad;
use VCN\Http\Controllers\Web\CMS\CategoriasWebController;
use VCN\Models\Convocatorias\Abierta as ConvocatoriaAbierta;

class WebController extends Controller
{
    protected $temas = ['britishsummerv3', 'britishsummerv4', 'bloques', 'home2020'];

    public function __construct()
    {
        //$x = \Request::route()->getName();
        //dd($x);
    }


    public function getCategoriaWeb(Request $request, $query = null)
    {
        $estructura = ConfigHelper::config('web_estructura');

        // index
        if (!$query) {
            return redirect()->route('web.index');
        }

        // seo_url
        $params = explode("/", $query);
        $nivel = count($params);

        // $s = "max-fun-multiactividades-pro";
        // $s = "vilar-rural-cardona";
        // $x = CategoriaWeb::buscar301Categoria($s);
        // dd($x);

        $nParams = count($params);
        $last = $params[$nParams - 1];

        // Curso:
        if (strpos($last, ".html")) {
            $slug = substr($last, 0, strpos($last, ".html"));
            $curso = Curso::where('course_slug', $slug)->first();

            // ???? comprobar si es estructura 1 o 2 ??? => cambiamos la url a la nueva?
            // Si existe el curso => view, sino mostramos la categoría??

            $s = isset($params[$nParams - 2]) ? $params[$nParams - 2] : null;
            $catw = CategoriaWeb::buscar301Categoria($s);
            if ($curso && $catw) // && $catw->estructura == $estructura )
            {
                return $this->getCategoriaCurso($request, $catw->slug, $curso->slug, "dippy");
            }

            //URL vieja: 301 => nueva: buscar en Estructura nueva
            {
                // $s = "nueva/". substr($query, 0, strpos($query, $slug));
                // dd("404 => cat: /$s");

                // buscamos la categoría de la nueva estructura
                $curso2 = CategoriaWeb::buscar301Curso($slug);

                if ($curso2) {
                    echo "Esctructura 2 :: CURSO:";
                    // dd($curso2);

                    $curso = $curso2['Curso'];
                    $catw = $curso2['CategoriaWeb'];

                    //Montamos url según estructura2
                    $slug2 = $catw->slug . "/" . $curso->slug;
                    dd($slug2);
                    // dd($curso->slug);

                    // return redirect()->to($slug2,301);
                } else {
                    $cat = isset($params[$nParams - 2]) ? $params[$nParams - 2] : null;

                    if ($cat) {
                        dd("categoria");
                    }

                    dd("404-1");
                }
            }
        }

        //CategoriaWeb:
        $catw = CategoriaWeb::buscar301Categoria($last);
        if ($catw) {
            // echo "catw: ";
            // dd($catw->slug);

            return $this->getCategoria($catw->slug);
        } else {
            $catw = null;
            for ($i = $nParams - 1; $i > 0; $i--) {
                $p = $i - 1;
                $s = isset($params[$p]) ? $params[$p] : null;

                $catw = CategoriaWeb::buscar301Categoria($s);
                if ($catw) {
                    break;
                }
            }

            if ($catw) {
                // echo "catw anterior: ";
                // dd($catw->slug);

                // return $this->getCategoria($catw->slug);
                return redirect()->to(route('web.wn', $catw->slug), 301);
            } else {
                dd("404-2");
            }
        }

        // dd("CatWebs [$query] ($nivel): ". $param);
        dd("404-0");
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index(Request $request)
    {
        if (ConfigHelper::config('sufijo') == 'ps') {
            $view = ConfigHelper::view('welcomeps');
            return view($view);
        }

        $e = ConfigHelper::config('web_estructura');
        $tema = ConfigHelper::config('tema');
        
        //cats web
        $p = ConfigHelper::config('propietario');
        $webcats = CategoriaWeb::where('estructura', $e)->whereIn('home_propietario', [$p, 0])->where('activo', 1)->where('es_home', 1);
        $promos = Promo::whereIn('propietario', [$p, 0])->where('activo', 1)->orderBy('orden')->get();

        if( !$webcats->count() )
        {
            $webcats = \VCN\Models\Categoria::whereIn('propietario', [$p, '0'])->orderBy('id', 'ASC')->get();
        }
        else
        {
            $webcats = $webcats->where('home_bloque_slide',0);
            $webcats = $webcats->orderBy('orden')->get();
        }

        $arbol = CategoriaWeb::arbol();

        $home = true;

        //link a otro idioma
        $langs = ConfigHelper::idiomas();
        $weblangs = array();
        foreach ($langs as $lang)
        {
            //$weblangs[$lang] = 'http' . (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '') . '://' . ConfigHelper::config('web') . ($lang != ConfigHelper::config('idioma') ? '/' . $lang . '/' : '/');
            $weblangs[$lang] = 'http' . (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '') . '://' . ConfigHelper::config('web') . '/' . $lang . '/';
        }

        //home2020
        $intro = null;
        $slides = null;

        $menuPrincipal = \VCN\Models\CMS\CategoriaWeb::arbolPadres('menu_principal');

        $tema = ConfigHelper::config('tema');
        if($tema == "home2020")
        {
            $intro = CategoriaWeb::where('estructura', $e)->whereIn('home_propietario', [$p, 0])->where('activo', 1);
            $intro = $intro->where('name','home-intro')->first() ?: null;
            if(!$intro)
            {
                $intro = $menuPrincipal->first();
            }
            
            $slides = CategoriaWeb::where('estructura', $e)->whereIn('home_propietario', [$p, 0])->where('activo', 1)->orderBy('orden');
            $slides = $slides->where('home_bloque_slide', 1)->get();
        }
        
        $view = ConfigHelper::view('welcome');
        return view($view, compact('webcats', 'promos', 'arbol', 'home', 'weblangs', 'intro', 'slides', 'menuPrincipal'));
    }

    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'viajero' => 'required',
            'nombre' => 'required',
            'apellido' => 'required',
            'email' => 'required|email|max:255',
            // 'movil' => 'required'
        ]);

        $plataforma = ConfigHelper::config('propietario');

        $curso_id = (int) $request->get('curso_id');
        $viajero_tipo = (int) $request->get('viajero');

        $email = $request->get('email');

        $user = User::where('plataforma', $plataforma)->where('username', $email)->first();
        $viajero = null;
        $tutor = null;
        if ($user) {
            $viajero = Viajero::where('plataforma', $plataforma)->where('email', $email)->first();
            $tutor = Tutor::where('plataforma', $plataforma)->where('email', $email)->first();

            if ($viajero || $tutor) {
                $url = route("web.index"); //home
                if ($curso_id) {
                    $curso = \VCN\Models\Cursos\Curso::find($curso_id);
                    $url = str_slug($curso->pais_name) . '/' . $curso->course_slug . '.html';
                }

                $user->registro = ['curso_id' => $curso_id, 'url' => $url];
                $user->save();

                \VCN\Models\Informes\WebRegistro::add($user);
                Session::flash('vcn.modal-login', $user->email);

                Session::flash('vcn.modal-login-existe', 1);
                Session::flash('vcn.modal-login', $email);
                Session::flash('vcn.modal-login-tipo', $viajero ? "viajero" : "tutor");
                return redirect()->back()->withInput();
            }
        }

        if ($viajero_tipo == 1) {
            //Viajero
            $ficha = new Viajero;
            $ficha->plataforma = $plataforma;
            $ficha->email = $email;
            $ficha->name = $request->get('nombre');
            $ficha->lastname = $request->get('apellido');
            $ficha->movil = $request->get('movil');
            $ficha->save();

            //Cuenta
            $viajero = Viajero::where('plataforma', $plataforma)->where('email', $email)->first();
            $viajero->setUsuarios(true, true);
            $user = $viajero->user;
        } elseif ($viajero_tipo == 2) {
            //tutor
            //Tutor
            $ficha = new Tutor;
            $ficha->plataforma = $plataforma;
            $ficha->email = $email;
            $ficha->name = $request->get('nombre');
            $ficha->lastname = $request->get('apellido');
            $ficha->movil = $request->get('movil');
            $ficha->save();

            //Cuenta
            $tutor = Tutor::where('plataforma', $plataforma)->where('email', $email)->first();
            $tutor->setUsuario(true, true);
            $user = $tutor->user;
        }

        $url = route("web.index"); //home
        if ($curso_id) {
            $curso = \VCN\Models\Cursos\Curso::find($curso_id);
            $url = str_slug($curso->pais_name) . '/' . $curso->course_slug . '.html';
        }

        $user->registro = ['curso_id' => $curso_id, 'url' => $url];
        $user->save();

        \VCN\Models\Informes\WebRegistro::add($user);

        Session::flash('vcn.modal-login', $user->email);
        return redirect()->back()->withInput();
    }


    public function ajaxInfo(Request $request, $curso_id)
    {
        $info = $request->input('info');

        switch ($info) {
            case 'cabierta-semanas': {
                    $weeks = $request->input('valor');

                    $curso = Curso::find($curso_id);

                    if ($request->input('fecha') == "") {
                        $result = ['info' => $info, 'result' => false, 'minimo' => false, 'error' => 'No hay Fecha de Inicio'];
                        return response()->json($result, 200);
                    }

                    $fechaini = Carbon::createFromFormat('d/m/Y', $request->input('fecha'));

                    $ca = ConvocatoriaAbierta::where('course_id', $curso_id)
                        ->where('convocatory_open_valid_start_date', '<=', $fechaini)
                        ->where('convocatory_open_valid_end_date', '>=', $fechaini)
                        ->first();

                    if (!$ca) {
                        $result = ['info' => $info, 'result' => false, 'minimo' => false, 'error' => 'No hay convocatoria'];
                        return response()->json($result, 200);
                    }

                    $fechafin = $curso->calcularFechaFin($fechaini, $weeks, $ca->convocatory_open_start_day, $ca->convocatory_open_end_day);
                    $fechaini = $request->input('fecha');
                    $precio = $ca->calcularPrecio($fechaini, $fechafin->format('d/m/Y'), $weeks); //weeks ahora es duracion

                    if (!$precio) {
                        $result = ['info' => $info, 'result' => false, 'minimo' => false, 'error' => 'No hay disponibilidad'];
                        return response()->json($result, 200);
                    }

                    $alerta = null;
                    if (!isset($precio['semanas']) || !isset($precio['duracion']))
                    {
                    }
                    else
                    {
                        if ($precio['semanas'] < $weeks && !$precio['monto_fijo']) {
                            $alerta = "La duración real del Curso es de " . $precio['semanas'] . " " . $precio['duracion'];
                            $idiomaapp = App::getLocale();
                            if ($idioma == "ca") {
                                $alerta = "La duració real del Curs es de " . $precio['semanas'] . " " . $precio['duracion'];
                            }
                        }
                    }

                    $moneda_id = $precio['moneda_id'];
                    $moneda = $precio['moneda'];
                    $importe_base = $precio['importe_base'];
                    $importe_extra = $precio['importe_extra'];
                    $importe = $precio['importe'];

                    $res = true;
                    $result = [
                        'info' => $info, 'result' => $res,
                        'fechafin' => $fechafin->format('d/m/Y'),
                        'precio' => $importe,
                        'precio_txt' => ConfigHelper::parseMoneda(($importe), $moneda),
                        'alerta' => $alerta,
                    ];
                    return response()->json($result, 200);
                }
                break;

            case 'cabierta-alojamiento': {
                    $weeks = $request->input('valor');
                    $alojamiento_id = $request->input('alojamiento_id');

                    $curso = Curso::find($curso_id);
                    $alojamiento = Alojamiento::find($alojamiento_id);

                    $fechaini = Carbon::createFromFormat('d/m/Y', $request->input('fecha'));
                    $fechafin = $curso ? $curso->calcularFechaFin($fechaini, $weeks, $alojamiento->start_day, $alojamiento->end_day) : null;

                    $fechaini = $request->input('fecha');
                    $precio = $alojamiento ? $alojamiento->calcularPrecio($fechaini, $fechafin->format('d/m/Y'), $weeks) : null;

                    $total = $precio['importe'];
                    $precio_txt = ConfigHelper::parseMoneda($total, $precio['moneda']);

                    //extras obligatorios
                    $extras = $alojamiento->extras_obligatorios;
                    //divisas
                    $divisas = $alojamiento->divisas_txt;

                    $alerta = null;
                    if (!isset($precio['semanas']) || !isset($precio['duracion']))
                    {
                        // $alerta = "La duración para el Alojamiento no está disponible";
                    }
                    elseif ($precio['semanas'] < $weeks)
                    {
                        $alerta = "La duración real del Alojamiento es de " . $precio['semanas'] . " " . $precio['duracion'];
                    }

                    $result = [
                        'info' => $info, 'result' => $total, 'precio_txt' => $precio_txt,
                        'fechafin' => $fechafin->format('d/m/Y'), 'extras' => $extras, 'divisas' => $divisas,
                        'alerta' => $alerta,
                    ];
                    return response()->json($result, 200);
                }
                break;

            case 'cabierta-total': {
                    $alojamiento_weeks = $request->input('semanasa');
                    $alojamiento_id = $request->input('alojamiento_id');
                    $alojamiento = Alojamiento::find($alojamiento_id);

                    $curso_weeks = $request->input('semanasc');
                    $curso = Curso::find($curso_id);

                    $curso_fecha = $request->input('fechaa');
                    $alojamiento_fecha = $request->input('fechac');

                    $result = false;
                    // if($alojamiento_id && $alojamiento_fecha && $alojamiento_weeks && $curso_fecha && $curso_weeks)
                    {
                        $result = Booking::web_precio_total($curso_id, $curso_fecha, $curso_weeks, $alojamiento_id, $alojamiento_fecha, $alojamiento_weeks);
                    }

                    $result = ['result' => $result];

                    return response()->json($result, 200);
                }
                break;
        }

        return response()->json(true, 200);
    }

    public function view($view)
    {
        $view = ConfigHelper::view($view);

        $viewf = base_path("resources/views/" . $view);
        if (!file_exists($viewf)) {
            abort(404);
        }

        return view($view);
    }

    /**
     * @param $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function pagina($page)
    {
        $view = ConfigHelper::view($page);
        $idiomaapp = App::getLocale();

        $pagina = Pagina::where('url', $page)
            ->where(function ($query) {
                return $query
                    ->where('propietario', 0)
                    ->orWhere('propietario', ConfigHelper::config('propietario'));
            })
            ->first();

        if (!$pagina) {
            $t = Traduccion::where(
                ['idioma' => $idiomaapp, 'modelo' => "Pagina", 'campo' => 'url', 'traduccion' => $page]
            )->first();

            $pagina = $t ? Pagina::find($t->modelo_id) : null;
        }

        if (!$pagina) {
            return abort(404);
        }

        //link a otro idioma
        //las páginas aun no tienen traducción para la url!!!!!
        $langs = ConfigHelper::idiomas();
        $weblangs = array();
        foreach ($langs as $lang) {
            //$weblangs[$lang] = 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://' . ConfigHelper::config('web') . ($lang != ConfigHelper::config('idioma') ? '/' . $lang . '/' : '/')  . $pagina->url . '.html';
            $weblangs[$lang] = 'http' . (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '') . '://' . ConfigHelper::config('web') . '/' . $lang . '/';
        }

        if (!view()->exists($view)) {
            $pre = "web." . Session::get('vcn.tema');
            return view($pre . ".pagina", compact('pagina', 'weblangs'));
        }

        return view($view, compact('pagina', 'weblangs'));
    }

    public function lista($modelo, $view)
    {
        $view = ConfigHelper::view($view);
        //Pendiente: buscarlo
        return view($view, compact('modelo'));
    }

    public function ficha($modelo, $id, $view)
    {
        $view = ConfigHelper::view($view);
        //Pendiente: buscarlo
        return view($view, compact('modelo', 'id'));
    }

    public function pdf($pdf)
    {
        $ruta = public_path() . '/assets/catalogos/' . ConfigHelper::config('sufijo') . '/' . $pdf;

        if (file_exists($ruta)) {
            $headers = array('Content-Type: application/pdf',);
            return Response::download($ruta, $pdf, $headers);
        } else {
            return Redirect::to(route('web.catalogo'), 301);
        }
    }

    public function getLanding(Request $request, $slug)
    {
        $landing = Landing::where('slug', $slug)->where('activo', 1)->first();
        $sufijo = ConfigHelper::config('sufijo');
        
        if (!$landing) {
            abort(404);
        }

        $template = $landing->template;
        $urlAssets = "/assets/landings/$landing->tema";
        $gracias = Session::get('vcn.landings.gracias', false);

        return view($template)->with(compact('landing', 'urlAssets', 'gracias', 'sufijo'));
    }

    public function getLandingCatalogo(Request $request, $slug)
    {
        $landing = Landing::where('slug', $slug)->where('activo', 1)->first();
        $sufijo = ConfigHelper::config('sufijo');

        if (!$landing) {
            abort(404);
        }

        $template = $landing->template ."-catalogo";
        $urlAssets = "/assets/landings/$landing->tema";
        $gracias = Session::get('vcn.landings.gracias', false);

        return view($template)->with(compact('landing', 'urlAssets', 'gracias','sufijo'));
    }

    public function postLanding(Request $request, Landing $landing)
    {
        $this->validate($request, [
            // 'landing_id' => 'required',
        ]);

        $data = $request->input();
        $esCatalogo = $request->has('es_catalogo');
        
        $data['landing_id'] = $landing->id;

        $ip = $request->ip();
        $info = [
            'ip' => $ip,
            'geo' => geoip($ip)->toArray(),
            'utm_source'    => $data['utm_source'] ?? "",
            'utm_medium'    => $data['utm_medium'] ?? "",
            'utm_campaign'  => $data['utm_campaign'] ?? "",
        ];
        $data['info'] = $info;

        Session::flash('vcn.landings.gracias', 1);

        $hoy = Carbon::now()->format('Y-m-d H:i');

        $repe = LandingForm::where('landing_id', $landing->id)
            ->where('email', $data['email'])
            ->where('telefono', $data['telefono'])
            ->where('fechanac', $data['fechanac'])
            ->where('nombre', $data['nombre'])
            ->where('created_at', 'like', "$hoy%")->first();
        
        if($repe)
        {
            if($esCatalogo)
            {
                return redirect()->route('web.landing.catalogo', $landing->slug);    
            }
            return redirect()->route('web.landing', $landing->slug);
        }

        $f = LandingForm::create($data);
        $data['ficha'] = $f;

        ExternalApiHelper::AC_contactsAdd($f);
        MailHelper::mailLanding($landing, $data, $esCatalogo);
    
        if($esCatalogo)
        {
            return redirect()->route('web.landing.catalogo', $landing->slug);    
        }
        return redirect()->route('web.landing', $landing->slug);
    }

    public function raiz(Request $request, $param1)
    {
        $p = ConfigHelper::config('propietario');
        $plataforma = Plataforma::find($p);

        switch ($plataforma->ruta_raiz ?? 0) {
            default:
            case 0: //categorias
                {
                    return $this->getCategoria($param1);
                }
                break;

            case 1: //especialidades
                {
                    return $this->getEspecialidad($param1);
                }
                break;
        }
    }

    public function raizSub($param1, $param2)
    {
        $p = ConfigHelper::config('propietario');
        $plataforma = Plataforma::find($p ?: 1);

        switch ($plataforma->ruta_raiz)
        {
            default:
            case 0: //categorias
                {
                    return $this->getSubcategoria($param1, $param2);
                }
                break;

            case 1: //especialidades
                {
                    return $this->getSubespecialidad($param1, $param2);
                }
                break;
        }
    }

    public function raizSubDet($param1, $param2, $param3)
    {
        $p = ConfigHelper::config('propietario');
        $plataforma = Plataforma::find($p);

        switch ($plataforma->ruta_raiz) {
            default:
            case 0: //categorias
                {
                    return $this->getSubcategoriaDetalle($param1, $param2, $param3);
                }
                break;

            case 1: //especialidades
                {
                    return $this->getSubespecialidad($param1, $param2); //la misma pq no hay subespecialidad detalle
                }
                break;
        }
    }

    public function raizCurso(Request $request, $param1, $slug)
    {
        $p = ConfigHelper::config('propietario');
        $plataforma = Plataforma::find($p);

        $r = $plataforma->ruta_raiz;
        switch ($r) {
            default:
            case 0: //categorias
                {
                    return $this->getCategoriaCurso($request, $param1, $slug);
                }
                break;

            case 1: //especialidades
                {
                    return $this->getEspecialidadCurso($request, $param1, $slug);
                }
                break;
        }
    }

    public function raizPdfcurso($param1, $id)
    {
        $p = ConfigHelper::config('propietario');
        $plataforma = Plataforma::find($p);

        switch ($plataforma->ruta_raiz) {
            default:
            case 0: //categorias
                {
                    return $this->getCategoriaPdfcurso($param1, $id);
                }
                break;

            case 1: //especialidades
                {
                    return $this->getEspecialidadPdfcurso($param1, $id);
                }
                break;
        }
    }


    public function getCategoriaCurso(Request $request, $categoria, $course_slug, $tema = null)
    {
        $p = ConfigHelper::config('propietario');
        $categoria_slug = $categoria;

        $force = $tema ? true : false;
        $tema = $tema ?: ConfigHelper::config('tema');

        if ($force) {
            Session::put('vcn.tema2', $tema);
        }

        $tema = ConfigHelper::config('tema');
        $idiomaapp = App::getLocale();
        
        if (in_array($tema, $this->temas))
        {
            $cat = $categoria;

            $view = ConfigHelper::view('curso');

            if ($idiomaapp == 'es')
            {
                $categoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $cat)->where('category_id', 0)->orderBy('orden')->first();
                if (!$categoria)
                {
                    // $cates = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $cat])->first();
                    $cates = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $cat])->get();
                    foreach($cates as $cati)
                    {
                        $catlang = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('id', $cati->modelo_id)->first();
                        if($catlang)
                        {
                            App::setLocale('ca');
                            Session::put('locale', 'ca');
                            return Redirect::to('/ca/' . $cat . '/' . $course_slug . '.html', 301);
                        }
                    }
                    
                    if (!$categoria)
                    {
                        abort(404);
                    }
                }
            }
            elseif ($idiomaapp == 'ca')
            {
                // $cates = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $cat])->first();

                $categoria = null;
                $cates = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $cat])->get();
                foreach($cates as $cati)
                {
                    $categoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('id', $cati->modelo_id)->first();
                    if($categoria)
                    {
                        break;
                    }
                }

                if (!$categoria)
                {
                    $catlang = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $cat)->where('category_id', 0)->orderBy('orden')->first();
                    if ($catlang)
                    {
                        App::setLocale('es');
                        Session::put('locale', 'es');
                        return Redirect::to('../' . $catlang->seo_url . '/' . $course_slug . '.html', 301);
                    }
                }
            }

            if (!$categoria || $categoria == $cat)
            {
                abort(404);
            }
            else
            {
                $clase = $categoria->seo_url;

                if ($idiomaapp == 'es') {
                    $curso = Curso::where('course_slug', $course_slug)
                        ->where('activo_web', 1)
                        ->where(function ($query) use ($course_slug, $p) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', $p);
                        })
                        ->first();
                } elseif ($idiomaapp == 'ca') {
                    $cursoesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'course_slug', 'traduccion' => $course_slug])->first();
                    $cursoesid = $cursoesid ? $cursoesid->modelo_id : 0;
                    $curso = Curso::where('id', $cursoesid)
                        ->where('activo_web', 1)
                        ->where(function ($query) use ($cursoesid) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })
                        ->first();

                    if (!$curso) {
                        $curso = Curso::where('course_slug', $course_slug)
                            ->where('activo_web', 1)
                            ->where(function ($query) use ($course_slug) {
                                return $query

                                    ->where('propietario', 0)
                                    ->orWhere('propietario', ConfigHelper::config('propietario'));
                            })
                            ->first();
                    }
                }

                if (!$curso) {
                    //Buscamos si era un slug viejo
                    $cs = \VCN\Models\Cursos\CursoSlug::where('slug', $course_slug)->first();
                    if ($cs) {
                        $curso = \VCN\Models\Cursos\Curso::find($cs->course_id);
                        if ($curso && $curso->activo_web) {
                            $p = ConfigHelper::config('propietario');
                            if ($curso->propietario == 0 || $curso->propietario == $p) {
                                // web.curso
                                return redirect()->route('web.curso', ['param1' => $categoria_slug, 'slug' => $curso->course_slug]);
                            }
                        }
                    }

                    Session::flash('redirected', true);
                    return Redirect::to(route('web.raiz', ['param1' => $categoria->seo_url]), 301);
                }

                $cerrado = $categoria->cerrado;
                $subcategoria = '';
                $subcategoria_detalle = '';

                if ($categoria->hijos_activos && count($categoria->hijos_activos) > 0) {
                    $hijos = $categoria->hijos_activos;
                    if (!is_array($hijos->pluck('id')->toArray())) {
                        $hijosids[] = $hijos->id;
                    } else {
                        $hijosids = $hijos->pluck('id')->toArray();
                    }

                    //$subcatcurso = $curso->subcategoria->id;

                    $subcatsweb = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('category_id', $categoria->id)->where('es_link', 0)
                        //->where(function ($query) use($subcatcurso, $hijosids){
                        //  return $query
                        //->orWhereRaw('FIND_IN_SET('.$subcatcurso.',subcategorias)')
                        //->orWhereRaw('FIND_IN_SET('.$subcatcurso.',categorias)')//;
                        //})
                        ->orderBy('orden')->get();

                    if (!is_array($subcatsweb->pluck('id')->toArray())) {
                        $subcatswebids[] = $subcatsweb->id;
                    } else {
                        $subcatswebids = $subcatsweb->pluck('id')->toArray();
                    }
                    $intersect = array_intersect($hijosids, $subcatswebids);
                    $subcate = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('category_id', $categoria->id)->whereIn('id', $intersect)->orderBy('orden')->get();

                    $sc = null;
                    foreach ($subcate as $subc) {
                        if (in_array($curso->id, $subc->cursos->pluck('id')->toArray())) {
                            $sc = $subc;
                        }
                    }

                    $subcategoria = $sc ?: $subcate->first();
                    if ($subcategoria->hijos_activos && count($subcategoria->hijos_activos) > 0) {
                        $hijossub = $subcategoria->hijos_activos;
                        foreach ($hijossub as $hs) {
                            $arrayCursos = $hs->cursos->pluck('id')->toArray();
                            if (in_array($curso->id, $arrayCursos)) {
                                $subcategoria_detalle = $hs;
                            }
                        }

                        $cerrado = $subcategoria_detalle ? $subcategoria_detalle->cerrado : 0;
                    } else {
                        $subcategoria_detalle = '';
                        $cerrado = $subcategoria->cerrado;
                    }
                }


                //link a otro idioma
                $langs = ConfigHelper::idiomas();
                $weblangs = array();
                foreach ($langs as $lang) {
                    $weblangs[$lang] = 'http' . (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '') . '://' . ConfigHelper::config('web') . '/' . $lang . '/'
                        . Traductor::getWeb($lang, 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url)
                        . '/' . Traductor::getWeb($lang, 'Curso', 'course_slug', $curso->id, $curso->course_slug) . '.html';
                }


                $plantilla = ConfigHelper::getCategoriaPlantilla($categoria->plantilla);
                if (!view()->exists($view)) {
                    $pre = "web." . Session::get('vcn.tema');

                    if (isset($plantilla) && $plantilla != '' && !is_array($plantilla)) {
                        if (View::exists($pre . ".curso-" . $plantilla)) {
                            return view($pre . ".curso-" . $plantilla, compact('categoria', 'subcategoria', 'subcategoria_detalle', 'clase', 'curso', 'cerrado', 'weblangs'));
                        } else {
                            return view($pre . ".curso", compact('categoria', 'subcategoria', 'subcategoria_detalle', 'clase', 'curso', 'cerrado', 'weblangs'));
                        }
                    }
                } else {
                    if (isset($plantilla) && $plantilla != '' && !is_array($plantilla)) {
                        $pre = "web." . Session::get('vcn.tema');

                        if (View::exists($pre . ".curso-" . $plantilla)) {
                            return view($pre . ".curso-" . $plantilla, compact('categoria', 'subcategoria', 'subcategoria_detalle', 'clase', 'curso', 'cerrado', 'weblangs'));
                        } else {
                            return view($pre . ".curso", compact('categoria', 'subcategoria', 'subcategoria_detalle', 'clase', 'curso', 'cerrado', 'weblangs'));
                        }
                    } else {
                        return view($view, compact('categoria', 'subcategoria', 'subcategoria_detalle', 'clase', 'curso', 'cerrado', 'weblangs'));
                    }
                }
            }
        }


        if ($tema == 'dippy') {
            $especialidades_curso = [];

            $view = ConfigHelper::view($categoria);
            $curso = Curso::where('course_slug', $course_slug)
                ->where('activo_web', 1)
                ->where(function ($query) use ($course_slug) {
                    return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })->first();

            if (!$curso) {
                return abort(404);
                // return Self::index($request);
            } else if (($curso->propietario != 0 && $curso->propietario != $p) || $curso->activo_web == 0) {
                return abort(404);
                // return Self::index($request);
            } else {
                if ($force) {
                    if (!view()->exists($view)) {
                        $pre = "web." . $tema;
                        return view($pre . ".curso", compact('categoria', 'curso'));
                    }
                } else {
                    foreach ($curso->especialidades as $espe) {
                        $especialidades_curso[] = $espe->especialidad->name;
                        $subespecialidades_curso[] = $espe->SubespecialidadesName;
                    }
                    $curso->especialidades_list = implode(',', $especialidades_curso);

                    if (!view()->exists($view)) {
                        $pre = "web." . $tema;
                        return view($pre . ".curso", compact('categoria', 'curso'));
                    }
                }

                return view($view, compact('categoria', 'curso'));
            }
        }

        $idiomaapp = App::getLocale();
        $view = ConfigHelper::view($categoria);
        $p = ConfigHelper::config('propietario');
        if ($idiomaapp == 'es') {
            $curso = Curso::where('course_slug', $course_slug)
                ->where('activo_web', 1)
                ->where(function ($query) {
                    return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })->first();
            switch ($categoria) {
                case 'aprender-ingles-en-el-extranjero':
                    $clase = 'ingles';
                    break;
                case 'aprender-otros-idiomas-en-el-extranjero':
                    $clase = 'idiomas';
                    break;
                default:
                    $categoria = Categoria::where('slug', $categoria)
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })
                        ->first();
                    $clase = $categoria->slug;
                    break;
            }
        } elseif ($idiomaapp == 'ca') {
            $cursoesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'course_slug', 'traduccion' => $course_slug])->first();
            $cursoesid = $cursoesid ? $cursoesid->modelo_id : 0;
            $curso = Curso::where('id', $cursoesid)
                ->where('activo_web', 1)
                ->where(function ($query) {
                    return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })
                ->first();
            if (!count($curso)) {
                $curso = Curso::where('course_slug', $course_slug)
                    ->where('activo_web', 1)
                    ->where(function ($query) use ($course_slug) {
                        return $query

                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })
                    ->first();
            }
            switch ($categoria) {
                case 'aprendre-angles-a-l-estranger':
                    $clase = 'ingles';
                    break;
                case 'aprendre-altres-idiomes-a-l-estranger':
                    $clase = 'idiomas';
                    break;
                default:
                    $catesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $categoria])->first();
                    if ($catesid && count($catesid)) {
                        $categoria = Categoria::where('id', $catesid->modelo_id)
                            ->where(function ($query) {
                                return $query
                                    ->where('propietario', 0)
                                    ->orWhere('propietario', ConfigHelper::config('propietario'));
                            })
                            ->first();
                        $clase = $categoria->slug;
                    } else {
                        return abort(404);
                    }
                    break;
            }
        }

        if (!$curso) {
            //return abort(404);
            return Self::index($request);
        } elseif (($curso->propietario != 0 && $curso->propietario != $p) || $curso->activo_web == 0) {
            //return abort(404);
            return Self::index($request);
        } else {
            if (!view()->exists($view)) {
                $pre = "web." . Session::get('vcn.tema');
                return view($pre . ".curso", compact('categoria', 'curso', 'clase'));
            }

            return view($view, compact('categoria', 'curso', 'clase'));
        }
    }

    public function getCategoriaPdfcurso($categoria, $id)
    {
        $p = ConfigHelper::config('propietario');

        if (in_array(ConfigHelper::config('tema'), $this->temas)) {
            $view = ConfigHelper::view($categoria);

            $idiomaapp = App::getLocale();
            if ($idiomaapp == 'es') {
                $categoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $categoria)->where('category_id', 0)->orderBy('orden')->first();
            } elseif ($idiomaapp == 'ca') {
                $cates = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $categoria])->get();
                foreach($cates as $cate)
                {
                    $categoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('id', $cate->modelo_id)->first();
                    if($categoria)
                    {
                        break;
                    }
                }
            }
            $clase = $categoria ? $categoria->seo_url : "";

            // dd($categoria);

            if (!$categoria) {
                //return Redirect::to('/');
                return abort(404);
            } else {
                $curso = Curso::where('id', $id)
                    ->where('activo_web', 1)
                    ->where(function ($query) use ($id, $p) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', $p);
                    })
                    ->first();


                $cerrado = $categoria->cerrado;
                $subcategoria = '';
                $subcategoria_detalle = '';
                if ($categoria->hijos_activos && count($categoria->hijos_activos) > 0) {
                    $hijos = $categoria->hijos_activos;
                    if (!is_array($hijos->pluck('id')->toArray())) {
                        $hijosids[] = $hijos->id;
                    } else {
                        $hijosids = $hijos->pluck('id')->toArray();
                    }

                    //$subcatcurso = $curso->subcategoria->id;

                    $subcatsweb = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('category_id', $categoria->id)->where('es_link', 0)
                        //->where(function ($query) use($subcatcurso, $hijosids){
                        //  return $query
                        //->orWhereRaw('FIND_IN_SET('.$subcatcurso.',subcategorias)')
                        //->orWhereRaw('FIND_IN_SET('.$subcatcurso.',categorias)')//;
                        //})
                        ->orderBy('orden')->get();

                    if (!is_array($subcatsweb->pluck('id')->toArray())) {
                        $subcatswebids[] = $subcatsweb->id;
                    } else {
                        $subcatswebids = $subcatsweb->pluck('id')->toArray();
                    }
                    $intersect = array_intersect($hijosids, $subcatswebids);
                    $subcate = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('category_id', $categoria->id)->whereIn('id', $intersect)->orderBy('orden')->get();

                    $sc = null;
                    if ($curso) {
                        foreach ($subcate as $subc) {
                            if (in_array($curso->id, $subc->cursos->pluck('id')->toArray())) {
                                $sc = $subc;
                            }
                        }
                    }

                    $subcategoria = $sc ?: $subcate->first();
                    if ($subcategoria && $subcategoria->hijos_activos && count($subcategoria->hijos_activos) > 0) {
                        $hijossub = $subcategoria->hijos_activos;
                        foreach ($hijossub as $hs) {
                            $arrayCursos = $hs->cursos->pluck('id')->toArray();

                            if ($curso && in_array($curso->id, $arrayCursos)) {
                                $subcategoria_detalle = $hs;
                            }
                        }
                        $cerrado = $subcategoria_detalle ? $subcategoria_detalle->cerrado : 0;
                    } else {
                        $subcategoria_detalle = '';
                        $cerrado = $subcategoria ? $subcategoria->cerrado : 0;
                    }
                }

                if (!$curso) {
                    abort(404);
                }


                if (!view()->exists($view)) {
                    $pre = "web." . Session::get('vcn.tema');
                    // return view($pre . ".pdfcurso", compact('categoria','subcategoria','subcategoria_detalle','clase','curso'));

                    app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
                    $pdf = PDF::loadView($pre . ".pdfcurso", compact('categoria', 'subcategoria', 'subcategoria_detalle', 'clase', 'curso'));
                    $nombre = str_slug(ConfigHelper::config('nombre')) . '_' . str_slug($curso->name);

                    $pdf->setOption('margin-top', 30);
                    $pdf->setOption('margin-right', 0);
                    $pdf->setOption('margin-bottom', 30);
                    $pdf->setOption('margin-left', 0);
                    $pdf->setOption('no-print-media-type', false);
                    $pdf->setOption('footer-spacing', 0);
                    $pdf->setOption('header-font-size', 9);
                    $pdf->setOption('header-spacing', 0);
                    //$pdf->setOption('javascript-delay',20000);
                    $pdf->setOption('header-html', 'https://' . ConfigHelper::config('web') . '/assets/logos/' . $idiomaapp . '/' . ConfigHelper::config('sufijo') . 'header.html');
                    $pdf->setOption('footer-html', 'https://' . ConfigHelper::config('web') . '/assets/logos/' . $idiomaapp . '/' . ConfigHelper::config('sufijo') . 'footer.html');
                    //return $pdf->stream("$nombre.pdf");
                    return $pdf->download("$nombre.pdf");
                }
                return view($view, compact('categoria', 'subcategoria', 'subcategoria_detalle', 'clase', 'curso'));
            }
        }




        $idiomaapp = App::getLocale();
        $view = ConfigHelper::view($categoria);
        $p = ConfigHelper::config('propietario');

        if ($idiomaapp == 'es') {
            $curso = Curso::where('id', $id)
                ->where('activo_web', 1)
                ->where(function ($query) {
                    return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })->first();
            switch ($categoria) {
                case 'aprender-ingles-en-el-extranjero':
                    $clase = 'ingles';
                    break;
                case 'aprender-otros-idiomas-en-el-extranjero':
                    $clase = 'idiomas';
                    break;
                default:
                    $categoria = Categoria::where('slug', $categoria)
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })
                        ->first();
                    if ($categoria) {
                        $clase = $categoria->slug;
                    } else {
                        return abort(404);
                    }
                    break;
            }
        } elseif ($idiomaapp == 'ca') {
            $curso = Curso::where('id', $id)
                ->where('activo_web', 1)
                ->where(function ($query) use ($id) {
                    return $query

                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })
                ->first();
            switch ($categoria) {
                case 'aprendre-angles-a-l-estranger':
                    $clase = 'ingles';
                    break;
                case 'aprendre-altres-idiomes-a-l-estranger':
                    $clase = 'idiomas';
                    break;
                default:
                    $catesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $categoria])->first();
                    if ($catesid && count($catesid)) {
                        $categoria = Categoria::where('id', $catesid->modelo_id)
                            ->where(function ($query) {
                                return $query
                                    ->where('propietario', 0)
                                    ->orWhere('propietario', ConfigHelper::config('propietario'));
                            })
                            ->first();
                        $clase = $categoria->slug;
                    } else {
                        return abort(404);
                    }
                    break;
            }
        }

        if (!$curso) {
            //return abort(404);
            return Self::index($request);
        } elseif (($curso->propietario != 0 && $curso->propietario != $p) || $curso->activo_web == 0) {
            //return abort(404);
            return Self::index($request);
        } else {
            if (!view()->exists($view)) {
                $pre = "web." . Session::get('vcn.tema');
                // return view($pre . ".pdfcurso", compact('categoria', 'curso', 'clase'));

                app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
                $pdf = PDF::loadView($pre . ".pdfcurso", compact('categoria', 'curso', 'clase'));
                $nombre = str_slug(ConfigHelper::config('nombre')) . '_' . str_slug($curso->name);

                $pdf->setOption('margin-top', 30);
                $pdf->setOption('margin-right', 0);
                $pdf->setOption('margin-bottom', 30);
                $pdf->setOption('margin-left', 0);
                $pdf->setOption('no-print-media-type', false);
                $pdf->setOption('footer-spacing', 0);
                $pdf->setOption('header-font-size', 9);
                $pdf->setOption('header-spacing', 0);
                //$pdf->setOption('javascript-delay',20000);
                $pdf->setOption('header-html', 'https://' . ConfigHelper::config('web') . '/assets/logos/' . $idiomaapp . '/' . ConfigHelper::config('sufijo') . 'header.html');
                $pdf->setOption('footer-html', 'https://' . ConfigHelper::config('web') . '/assets/logos/' . $idiomaapp . '/' . ConfigHelper::config('sufijo') . 'footer.html');

                //return $pdf->stream("$nombre.pdf");
                return $pdf->download("$nombre.pdf");
            }

            return view($view, compact('categoria', 'curso', 'clase'));
        }
    }


    public function getCategoria($cat)
    {
        $idiomaapp = App::getLocale();

        if (in_array(ConfigHelper::config('tema'), $this->temas))
        {
            $view = ConfigHelper::view($cat);
            $p = ConfigHelper::config('propietario');
            $categoria = null;

            if ($idiomaapp == 'ca')
            {
                $cates = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $cat])->get();
                foreach($cates as $cati)
                {
                    $categoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('id', $cati->modelo_id)->first();
                    if($categoria)
                    {
                        break;
                    }
                }
            }
            else
            {
                $idiomaapp = "es";
                // if ($idiomaapp == 'es')
                $categoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $cat)->where('category_id', 0)->orderBy('orden')->first();
            }

            if (!$idiomaapp)
            {
                // abort(404);
            }

            if (!$categoria)
            {
                //return Redirect::to('/');
                return abort(404);
            }
            else
            {
                //link a otro idioma
                $langs = ConfigHelper::idiomas();
                $weblangs = array();
                foreach ($langs as $lang)
                {
                    $weblangs[$lang] = 'http' . (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '') . '://' . ConfigHelper::config('web') . '/' . $lang . '/' . Traductor::getWeb($lang, 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url);
                }

                $clase = $categoria->seo_url;
                if (count($categoria->cursos) == 1)
                {
                    //dd($categoria);
                    return Redirect::to($categoria->seo_url . '/' . $categoria->cursos[0]->course_slug . '.html');
                }

                if (!view()->exists($view))
                {
                    $pre = "web." . Session::get('vcn.tema');

                    $plantilla = ConfigHelper::getCategoriaPlantilla($categoria->plantilla);
                    if (isset($plantilla) && $plantilla != '' && !is_array($plantilla)) {
                        if (View::exists($pre . ".categorias-" . $plantilla)) {
                            return view($pre . ".categorias-" . $plantilla, compact('categoria', 'clase', 'weblangs'));
                        } else {
                            return view($pre . ".categorias", compact('categoria', 'clase', 'weblangs'));
                        }
                    } else {
                        return view($pre . ".categorias", compact('categoria', 'clase', 'weblangs'));
                    }
                }

                return view($view, compact('categoria', 'clase', 'weblangs'));
            }
        }

        $view = ConfigHelper::view($cat);
        $idioma = '%';

        if ($idiomaapp == 'es')
        {
            switch ($cat) {
                case 'aprender-ingles-en-el-extranjero':
                    $categoria = Categoria::whereIn('id', [1, 2, 7])
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })->get();
                    $operador = 'LIKE';
                    $idioma = '%Inglés%';
                    $categorianame = 'Aprender ingles en el extranjero';
                    $categorianameslug = $cat;
                    $clase = 'ingles';
                    break;
                case 'aprender-otros-idiomas-en-el-extranjero':
                    $categoria = Categoria::whereIn('id', [1, 2, 7])
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })->get();
                    $operador = 'NOT LIKE';
                    $idioma = 'Inglés';
                    $categorianame = 'Otros idiomas en el extranjero';
                    $categorianameslug = $cat;
                    $clase = 'idiomas';
                    break;
                default:
                    $categoria = Categoria::where('slug', $cat)
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })
                        ->first();
                    if ($categoria) {
                        $categorianame = '';
                        $operador = 'LIKE';
                        $idioma = '%';
                        $clase = $categoria->slug;
                    }
                    else
                    {
                        return abort(404);
                    }

                    break;
            }
        }
        elseif ($idiomaapp == 'ca')
        {
            switch ($cat) {
                case 'aprendre-angles-a-l-estranger':
                    $categoria = Categoria::whereIn('id', [1, 2, 7])
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })->get();
                    $operador = 'LIKE';
                    $idioma = '%Inglés%';
                    $categorianame = "Aprendre anglès a l'estranger";
                    $categorianameslug = $cat;
                    $clase = 'ingles';
                    break;
                case 'aprendre-altres-idiomes-a-l-estranger':
                    $categoria = Categoria::whereIn('id', [1, 2, 7])
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })->get();
                    $operador = 'NOT LIKE';
                    $idioma = 'Inglés';
                    $categorianame = "Altres idiomes a l'estranger";
                    $categorianameslug = $cat;
                    $clase = 'idiomas';
                    break;
                default:
                    $catesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $cat])->first();
                    if ($catesid && count($catesid)) {
                        $categoria = Categoria::where('id', $catesid->modelo_id)
                            ->where(function ($query) {
                                return $query
                                    ->where('propietario', 0)
                                    ->orWhere('propietario', ConfigHelper::config('propietario'));
                            })
                            ->first();
                        $operador = 'LIKE';
                        $idioma = '%';
                        $categorianame = '';
                        $categorianameslug = $cat;
                        $clase = $categoria->slug;
                    }
                    else
                    {
                        return abort(404);
                    }
                    break;
            }
        }

        if (!$categoria)
        {
            //return Redirect::to('/');
            return abort(404);
        }
        else
        {

            if (!view()->exists($view)) {
                $pre = "web." . Session::get('vcn.tema');
                return view($pre . ".categorias", compact('categoria', 'operador', 'idioma', 'categorianame', 'categorianameslug', 'clase'));
            }
            return view($view, compact('categoria', 'operador', 'idioma', 'categorianame', 'categorianameslug', 'clase'));
        }
    }

    public function getSubcategoria($cat, $subcat)
    {
        $categoria = null;
        $tema = ConfigHelper::config('tema');

        $idiomaapp = App::getLocale();

        if (in_array($tema, $this->temas))
        {
            $view = ConfigHelper::view($cat);
            $p = ConfigHelper::config('propietario') ?: 1;

            if ($idiomaapp == 'ca')
            {
                // $cates = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $cat])->first();
                // $categoria = $cates ? CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('id', $cates->modelo_id)->first() : null;

                $cates = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $cat])->get();
                foreach($cates as $cati)
                {
                    $categoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('id', $cati->modelo_id)->first();
                    if($categoria)
                    {
                        break;
                    }
                }

                if (!$categoria) {
                    abort(404);
                }

                $hijos = $categoria->hijos_activos;
                if (!is_array($hijos->pluck('id')->toArray())) {
                    $hijosids[] = $hijos->id;
                } else {
                    $hijosids = $hijos->pluck('id')->toArray();
                }
                $subcates = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $subcat])->whereIn('modelo_id', $hijosids)->get();

                if (!is_array($subcates->pluck('modelo_id')->toArray())) {
                    $subcatesids[] = $subcates->modelo_id;
                } else {
                    $subcatesids = $subcates->pluck('modelo_id')->toArray();
                }

                $intersect = array_intersect($hijosids, $subcatesids);
                $subcategoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->whereIn('id', $intersect)->orderBy('orden')->first();
            }
            else
            {
                $idiomaapp = "es";
                // if ($idiomaapp == 'es')
                {
                    $categoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $cat)->where('category_id', 0)->orderBy('orden')->first();
                    if (!$categoria) {
                        abort(404);
                    }
                    $subcategoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $subcat)->where('category_id', $categoria->id)->orderBy('orden')->first();
                }
                
            }

            if (!$categoria)
            {
                abort(404);
            }

            $clase = $categoria->seo_url;

            if (!$subcategoria)
            {
                //return Redirect::to('/');
                return abort(404);
            }
            else
            {
                //link a otro idioma
                $langs = ConfigHelper::idiomas();
                $weblangs = array();
                foreach ($langs as $lang)
                {
                    $weblangs[$lang] = 'http' . (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '') . '://' . ConfigHelper::config('web') . '/' . $lang . '/' . Traductor::getWeb($lang, 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url) . '/' . Traductor::getWeb($lang, 'CategoriaWeb', 'seo_url', $subcategoria->id, $subcategoria->seo_url);
                }

                if (!view()->exists($view))
                {
                    $pre = "web.$tema";
                    
                    if($tema == "home2020")
                    {
                        $plantilla = ConfigHelper::getCategoriaPlantilla($categoria->plantilla);
                        if (isset($plantilla) && $plantilla != '' && !is_array($plantilla))
                        {
                            if (View::exists($pre . ".subcategorias-" . $plantilla)) {
                                return view($pre . ".subcategorias-" . $plantilla, compact('categoria', 'subcategoria', 'clase', 'weblangs'));
                            } else {
                                return view($pre . ".subcategorias", compact('categoria', 'subcategoria', 'clase', 'weblangs'));
                            }
                        } else {
                            return view($pre . ".subcategorias", compact('categoria', 'subcategoria', 'clase', 'weblangs'));
                        }
                    }


                    if($subcategoria->hijos_activos && count($subcategoria->hijos_activos) > 0)
                    {
                        $plantilla = ConfigHelper::getCategoriaPlantilla($categoria->plantilla);
                        if (isset($plantilla) && $plantilla != '' && !is_array($plantilla))
                        {
                            if (View::exists($pre . ".subcategorias-" . $plantilla)) {
                                return view($pre . ".subcategorias-" . $plantilla, compact('categoria', 'subcategoria', 'clase', 'weblangs'));
                            } else {
                                return view($pre . ".subcategorias", compact('categoria', 'subcategoria', 'clase', 'weblangs'));
                            }
                        } else {
                            return view($pre . ".subcategorias", compact('categoria', 'subcategoria', 'clase', 'weblangs'));
                        }
                    }
                    else
                    {
                        return self::getSubcategoriaDetalle($cat, $subcat, null);
                    }
                }
                return view($view, compact('categoria', 'subcategoria', 'clase', 'weblangs'));
            }
        }

        $view = ConfigHelper::view($cat);
        $p = ConfigHelper::config('propietario');

        if ($idiomaapp == 'es')
        {
            switch ($cat) {
                case 'aprender-ingles-en-el-extranjero':
                    $categoria = Categoria::whereIn('id', [1, 2, 7])
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })->get();
                    $operador = 'LIKE';
                    $idioma = '%Inglés%';
                    $subcategoria = Categoria::where('slug', $subcat)->first(); //Subcategoria::whereIn('category_id',[1,2,7])->get();
                    $categorianame = 'Aprender ingles en el extranjero';
                    $categorianameslug = $cat;
                    $clase = 'ingles';
                    break;
                case 'aprender-otros-idiomas-en-el-extranjero':
                    $categoria = Categoria::whereIn('id', [1, 2, 7])
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })->get();
                    $operador = 'NOT LIKE';
                    $idioma = 'Inglés';
                    $subcategoria = Categoria::where('slug', $subcat)->first(); //Subcategoria::whereIn('category_id',[1,2,7])->get();
                    $categorianame = 'Otros idiomas en el extranjero';
                    $categorianameslug = $cat;
                    $clase = 'idiomas';
                    break;
                default:
                    $categoria = Categoria::where('slug', $cat)
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })
                        ->first();
                    if ($categoria) {
                        $idioma = '%';
                        $subcategoria = Subcategoria::where('slug', $subcat)->first();
                        $categorianame = '';
                        $operador = 'LIKE';
                        $idioma = '%';
                        $clase = $categoria->slug;
                    } else {
                        return abort(404);
                    }
                    break;
            }
        } 
        elseif ($idiomaapp == 'ca')
        {
            //dd(Traduccion::where(['idioma'=> 'ca', 'campo'=> 'slug', 'traduccion'=> $cat])->first());
            switch ($cat) {
                case 'aprendre-angles-a-l-estranger':
                    $categoria = Categoria::whereIn('id', [1, 2, 7])
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })->get();
                    $operador = 'LIKE';
                    $idioma = '%Inglés%';
                    $subcatesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $subcat])->first();
                    $subcategoria = Categoria::where('id', $subcatesid->modelo_id)->first(); //Subcategoria::whereIn('category_id',[1,2,7])->get();
                    $categorianame = "Aprendre anglès a l'estranger";
                    $categorianameslug = $cat;
                    $clase = 'ingles';
                    break;
                case 'aprendre-altres-idiomes-a-l-estranger':
                    $categoria = Categoria::whereIn('id', [1, 2, 7])
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })->get();
                    $operador = 'NOT LIKE';
                    $idioma = 'Inglés';
                    $subcatesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $subcat])->first();
                    $subcategoria = Categoria::where('id', $subcatesid->modelo_id)->first(); //Subcategoria::whereIn('category_id',[1,2,7])->get();
                    $categorianame = "Altres idiomes a l'estranger";
                    $categorianameslug = $cat;
                    $clase = 'idiomas';
                    break;
                default:
                    $catesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $cat])->first();
                    if ($catesid && count($catesid)) {
                        $categoria = Categoria::where('id', $catesid->modelo_id)
                            ->where(function ($query) {
                                return $query
                                    ->where('propietario', 0)
                                    ->orWhere('propietario', ConfigHelper::config('propietario'));
                            })
                            ->first();
                        $idioma = '%';
                        $subcatesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $subcat])->first();
                        $subcategoria = Subcategoria::where('id', $subcatesid->modelo_id)->first();
                        $categorianame = '';
                        $operador = 'LIKE';
                        $idioma = '%';
                        $clase = $categoria->slug;
                    } else {
                        return abort(404);
                    }
                    break;
            }
        }
        //dd($categorianame.' '.$subcategoria->name_web);


        if (!$subcategoria || !$categoria)
        {
            //return Redirect::to('/');
            return abort(404);
        } else {
            if (($categorianame != '' || SubcategoriaDetalle::where('subcategory_id', $subcategoria->id)->get()->isEmpty() !== true) && $subcategoria->id != '7') {
                if (!view()->exists($view)) {
                    $pre = "web." . Session::get('vcn.tema');
                    return view($pre . ".subcategorias", compact('categoria', 'subcategoria', 'operador', 'idioma', 'categorianame', 'categorianameslug', 'clase'));
                }

                return view($view, compact('categoria', 'subcategoria', 'operador', 'idioma', 'categorianame', 'categorianameslug', 'clase'));
            } elseif ($subcategoria->id == '7' && (ConfigHelper::config('sufijo') == 'bs' || ConfigHelper::config('sufijo') == 'cic')) {
                $cursos = Curso::where('category_id', 7)
                    ->where('course_language', $operador, $idioma)
                    ->where('activo_web', 1)
                    ->where(function ($query) use ($categoria, $operador, $idioma) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })
                    ->orderBy('course_name', 'ASC')->get();
                //dd($cursos);
                $subcategoriadet = '';
                if (!view()->exists($view)) {
                    $pre = "web." . Session::get('vcn.tema');
                    return view($pre . ".cursoslista", compact('categoria', 'subcategoria', 'subcategoriadet', 'operador', 'idioma', 'cursos', 'categorianame', 'categorianameslug', 'clase'));
                }
                return view($view, compact('categoria', 'subcategoria', 'subcategoriadet', 'operador', 'idioma', 'cursos', 'categorianame', 'categorianameslug', 'clase'));
            } else {
                $cursos = Curso::where('category_id', $categoria->id)
                    ->where('subcategory_id', $subcategoria->id)
                    ->where('course_language', $operador, $idioma)
                    ->where('activo_web', 1)
                    ->where(function ($query) use ($categoria, $subcategoria, $operador, $idioma) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })
                    ->orderBy('course_name', 'ASC')->get();
                //dd($cursos);
                $subcategoriadet = '';
                if (!view()->exists($view)) {
                    $pre = "web." . Session::get('vcn.tema');
                    return view($pre . ".cursoslista", compact('categoria', 'subcategoria', 'subcategoriadet', 'operador', 'idioma', 'cursos', 'categorianame', 'categorianameslug', 'clase'));
                }
                return view($view, compact('categoria', 'subcategoria', 'subcategoriadet', 'operador', 'idioma', 'cursos', 'categorianame', 'categorianameslug', 'clase'));
            }
        }
    }

    public function getSubcategoriaDetalle($cat, $subcat, $subcatdet)
    {
        $categoria = null;
        $idiomaapp = App::getLocale();

        if (in_array(ConfigHelper::config('tema'), $this->temas))
        {
            $view = ConfigHelper::view($cat);
            $p = ConfigHelper::config('propietario') ?: 1;

            if ($idiomaapp == 'ca')
            {
                // $cates = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $cat])->first();
                // $categoria = $cates ? CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('id', $cates->modelo_id)->first() : null;

                $cates = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $cat])->get();
                foreach($cates as $cati)
                {
                    $categoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('id', $cati->modelo_id)->first();
                    if($categoria)
                    {
                        break;
                    }
                }

                $hijosids = [];
                if ($categoria)
                {
                    $hijos = $categoria->hijos_activos;
                    if (!is_array($hijos->pluck('id')->toArray()))
                    {
                        $hijosids[] = $hijos->id;
                    }
                    else
                    {
                        $hijosids = $hijos->pluck('id')->toArray();
                    }
                }
                $subcates = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $subcat])->whereIn('modelo_id', $hijosids)->get();

                if( !is_array($subcates->pluck('modelo_id')->toArray()) )
                {
                    $subcatesids[] = $subcates->modelo_id;
                }
                else
                {
                    $subcatesids = $subcates->pluck('modelo_id')->toArray();
                }

                $intersect = array_intersect($hijosids, $subcatesids);
                $subcategoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->whereIn('id', $intersect)->orderBy('orden')->first();
            }
            else
            {
                $idiomaapp = 'es';
                // if ($idiomaapp == 'es')
                {
                    $categoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $cat)->where('category_id', 0)->orderBy('orden')->first();
                    $subcategoria = $categoria ? CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $subcat)->where('category_id', $categoria->id)->orderBy('orden')->first() : null;
                }
            }

            if ($subcatdet != null)
            {
                $subcategoriadet = null;
                if ($idiomaapp == 'es')
                {
                    $subcategoriadet = $subcategoria ? CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $subcatdet)->where('category_id', $subcategoria->id)->orderBy('orden')->first() : null;
                }
                elseif ($idiomaapp == 'ca')
                {
                    $subcatdetes = Traduccion::where(['modelo' => 'CategoriaWeb', 'idioma' => 'ca', 'campo' => 'seo_url', 'traduccion' => $subcatdet])->get();
                    $subcategoriadet = $subcategoria ? CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->whereIn('id', $subcatdetes->pluck('modelo_id')->toArray())->where('category_id', $subcategoria->id)->first() : null;
                }

                $cursos = $subcategoriadet ? $subcategoriadet->cursos : null;
                $cerrado = $subcategoriadet ? $subcategoriadet->cerrado : null;
            }
            else
            {
                $subcategoriadet = '';
                $cursos = $subcategoria->cursos;
                $cerrado = $subcategoria->cerrado;
            }

            $clase = $categoria ? $categoria->seo_url : null;

            if (!$subcategoria)
            {
                //return Redirect::to('/');
                return abort(404);
            }
            else
            {
                //link a otro idioma
                $langs = ConfigHelper::idiomas();
                $weblangs = array();

                foreach ($langs as $lang)
                {
                    $weblangs[$lang] = 'http' . (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '') . '://' . ConfigHelper::config('web') . '/' . $lang . '/' . Traductor::getWeb($lang, 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url) . '/' . Traductor::getWeb($lang, 'CategoriaWeb', 'seo_url', $subcategoria->id, $subcategoria->seo_url) . ($subcategoriadet != '' ? '/' . Traductor::getWeb($lang, 'CategoriaWeb', 'seo_url', $subcategoriadet->id, $subcategoriadet->seo_url) : '');
                }

                if (!view()->exists($view))
                {
                    $pre = "web." . Session::get('vcn.tema');

                    $plantilla = ConfigHelper::getCategoriaPlantilla($categoria->plantilla);
                    if (isset($plantilla) && $plantilla != '' && !is_array($plantilla))
                    {
                        if (View::exists($pre . ".cursoslista-" . $plantilla))
                        {
                            return view($pre . ".cursoslista-" . $plantilla, compact('categoria', 'subcategoria', 'subcategoriadet', 'clase', 'cursos', 'cerrado', 'weblangs'));
                        }
                        else
                        {
                            return view($pre . ".cursoslista", compact('categoria', 'subcategoria', 'subcategoriadet', 'clase', 'cursos', 'cerrado', 'weblangs'));
                        }
                    }
                    else
                    {
                        return view($pre . ".cursoslista", compact('categoria', 'subcategoria', 'subcategoriadet', 'clase', 'cursos', 'cerrado', 'weblangs'));
                    }
                }

                return view($view, compact('categoria', 'subcategoria', 'subcategoriades', 'clase', 'cursos', 'cerrado', 'weblangs'));
            }
        }

        $view = ConfigHelper::view($cat);
        $p = ConfigHelper::config('propietario');

        if ($idiomaapp == 'es')
        {
            switch ($cat)
            {
                case 'aprender-ingles-en-el-extranjero':
                    $categoria = Categoria::whereIn('id', [1, 2, 7])->get();
                    $operador = 'LIKE';
                    $idioma = '%Inglés%';
                    $subcategoria = Categoria::where('slug', $subcat)->first();
                    $subcategoriadet = Subcategoria::whereIn('category_id', [1, 2, 7])->where('slug', $subcatdet)->first();
                    $categorianame = 'Aprender ingles en el extranjero';
                    $categorianameslug = $cat;
                    $clase = 'ingles';
                    break;
                case 'aprender-otros-idiomas-en-el-extranjero':
                    $categoria = Categoria::whereIn('id', [1, 2, 7])->get();
                    $operador = 'NOT LIKE';
                    $idioma = 'Inglés';
                    $subcategoria = Categoria::where('slug', $subcat)->first();
                    $subcategoriadet = Subcategoria::whereIn('category_id', [1, 2, 7])->where('slug', $subcatdet)->first();
                    $categorianame = 'Otros idiomas en el extranjero';
                    $categorianameslug = $cat;
                    $clase = 'idiomas';
                    break;
                default:
                    $categoria = Categoria::where('slug', $cat)
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })
                        ->first();
                    if ($categoria) {
                        $operador = 'LIKE';
                        $idioma = '%';
                        $subcategoria = Subcategoria::where('slug', $subcat)->first();
                        $subcategoriadet = SubcategoriaDetalle::where('subcategory_id', $subcategoria->id)->where('slug', $subcatdet)->first();
                        $categorianame = '';
                        $categorianameslug = '';
                        $clase = $categoria->slug;
                    } else {
                        return abort(404);
                    }
                    break;
            }
        
        }
        elseif ($idiomaapp == 'ca')
        {
            //dd(Traduccion::where(['idioma'=> 'ca', 'campo'=> 'slug', 'traduccion'=> $subcat])->first());
            switch ($cat) {
                case 'aprendre-angles-a-l-estranger':
                    $categoria = Categoria::whereIn('id', [1, 2, 7])->get();
                    $operador = 'LIKE';
                    $idioma = '%Inglés%';
                    $subcatesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $subcat])->first();
                    $subcategoria = Categoria::where('id', $subcatesid->modelo_id)->first();
                    $subcatdetesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $subcatdet])->first();
                    $subcategoriadet = Subcategoria::where('id', $subcatdetesid->modelo_id)->first();
                    $categorianame = "Aprendre anglès a l'estranger";
                    $categorianameslug = $cat;
                    $clase = 'ingles';
                    break;
                case 'aprendre-altres-idiomes-a-l-estranger':
                    $categoria = Categoria::whereIn('id', [1, 2, 7])->get();
                    $operador = 'NOT LIKE';
                    $idioma = 'Inglés';
                    $subcatesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $subcat])->first();
                    $subcategoria = Categoria::where('id', $subcatesid->modelo_id)->first();
                    $subcatdetesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $subcatdet])->first();
                    $subcategoriadet = Subcategoria::where('id', $subcatdetesid->modelo_id)->first();
                    $categorianame = "Aprendre altres idiomes a l'estranger";
                    $categorianameslug = $cat;
                    $clase = 'idiomas';
                    break;
                default:
                    $catesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $cat])->first();
                    if ($catesid) {
                        $categoria = Categoria::where('id', $catesid->modelo_id)
                            ->where(function ($query) {
                                return $query
                                    ->where('propietario', 0)
                                    ->orWhere('propietario', ConfigHelper::config('propietario'));
                            })
                            ->first();
                        $operador = 'LIKE';
                        $idioma = '%';
                        $subcatesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $subcat])->first();
                        $subcategoria = Subcategoria::where('id', $subcatesid->modelo_id)->first();
                        $subcatdetesid = Traduccion::where(['idioma' => 'ca', 'campo' => 'slug', 'traduccion' => $subcatdet])->first();
                        if ($subcatdetesid == null) {
                            $subcategoriadet = SubcategoriaDetalle::where('subcategory_id', $subcategoria->id)->where('slug', $subcatdet)->first();
                        } else {
                            $subcategoriadet = SubcategoriaDetalle::where('id', $subcatdetesid->modelo_id)->first();
                        }
                        //dd($subcategoriadet);
                        $categorianame = '';
                        $categorianameslug = '';
                        $clase = $categoria->slug;
                    } else {
                        return abort(404);
                    }
                    break;
            }
        }

        if ($categorianame != '')
        {
            $cursos = Curso::where('subcategory_id', $subcategoriadet->id)
                ->where('course_language', $operador, $idioma)
                ->where('activo_web', 1)
                ->where(function ($query) use ($subcategoriadet, $operador, $idioma) {
                    return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })
                ->orderBy('course_name', 'ASC')->get();
            //dd($cursos);
            if (!view()->exists($view)) {
                $pre = "web." . Session::get('vcn.tema');
                return view($pre . ".cursoslista", compact('categoria', 'subcategoria', 'subcategoriadet', 'operador', 'idioma', 'cursos', 'categorianame', 'categorianameslug', 'clase'));
            }
        }

        if (!$subcategoria)
        {
            //return Redirect::to('/');
            return abort(404);
        }
        else
        {
            $cursos = Curso::where('subcategory_det_id', $subcategoriadet->id)
                ->where('course_language', $operador, $idioma)
                ->where('activo_web', 1)
                ->where(function ($query) use ($subcategoriadet, $operador, $idioma) {
                    return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })
                ->orderBy('course_name', 'ASC')->get();
            //dd($cursos);
            if (!view()->exists($view)) {
                $pre = "web." . Session::get('vcn.tema');
                return view($pre . ".cursoslista", compact('categoria', 'subcategoria', 'subcategoriadet', 'operador', 'idioma', 'cursos', 'categorianame', 'categorianameslug', 'clase'));
            }

            return view($view, compact('categoria', 'subcategoria', 'subcategoriadet', 'operador', 'idioma', 'cursos', 'categorianame', 'categorianameslug', 'clase'));
        }
    }

    public function buscarByPaisVue(Request $request)
    {
        $template = "web.dippy"; //". Session::get('vcn.tema');

        //$view = $template.'.ajax.cursos';
        $view = $template . '.welcome2';
        return view($view);

        //ajax render ???
        $data = [
            'view' => View::make($template . '.ajax.cursos')
                // ->with('userLetsSay', $userData)
                ->render()
        ];

        return Response::json($data, 200);
    }

    public function buscarByPaisVueApi(Request $request)
    {
        $back = $request->get('back') ?: 0;
        if ($back == 1) {
            $max = 0;
            $limit = 9999;
        } else {
            $max = $request->get('max') ?: 0;
            $limit = $request->get('limit') ?: 10;
        }

        //esto no está fino, es para las pruebas
        //$cursos = Curso::where('id','>',$max)->where('id','<',$max+15)->get();



        //dippy
        if ($request->get('especialidad') != null) {
            $especialidades = explode(',', $request->get('especialidad'));
        }
        if ($request->get('pais') != null) {
            $paises = explode(',', $request->get('pais'));
        }
        if ($request->get('edad') != null) {
            $edades = explode(',', $request->get('edad'));
        }
        //dd($edades);

        // if(!$especialidades)
        // {
        //     // $especialidades = Especialidad::all()->pluck('id')->toArray();
        // }

        // if(!$paises)
        // {
        //     // $paises = Pais::all()->pluck('id')->toArray();
        // }

        // DB::enableQueryLog();

        $p = ConfigHelper::config('propietario');

        $list = Curso::select();

        if (isset($especialidades)) {
            $cursos_id = [];
            $esp = EspecialidadWeb::whereIn('id', $especialidades)->get();
            foreach ($esp as $e) {
                foreach ($e->cursos as $c) {
                    array_push($cursos_id, $c->id);
                }
            }

            array_unique($cursos_id);
            $list = $list->whereIn('id', $cursos_id);
        }

        if (isset($paises)) {
            $centros = Centro::whereIn('country_id', $paises)->pluck('id')->toArray();
            $list = $list->whereIn('center_id', $centros);
        }

        $list = $list->whereIn('propietario', [$p, 0])->where('activo_web', 1)->pluck('id')->toArray();

        $cursos = collect();
        $cursos_total = 0;

        if (isset($edades)) {
            if ($edades == 19) {
                //+18 ??
            }

            foreach ($edades as $edad) {
                $c = Curso::whereRaw(
                    'find_in_set(?, course_age)',
                    [$edad]
                )
                    ->whereNotIn('id', $cursos->pluck('id')->toArray())
                    ->whereIn('id', $list)
                    ->whereIn('propietario', [$p, 0])
                    ->where('activo_web', 1)
                    ->limit($limit)->offset($max)
                    ->get();

                $cursos = $cursos->merge($c);
                $cursos_total += $c->count();
            }
        } else {
            $cursos = Curso::whereIn('id', $list)->limit($limit)->offset($max)->get();
            $cursos_total = Curso::whereIn('id', $list)->count();
        }

        $datos = array();

        foreach ($cursos as $curso) {
            $curso->pais_name = $curso->pais->name;
            $especialidades_curso = array();
            $subespecialidades_curso = array();

            foreach ($curso->especialidades as $espe) {
                $especialidades_curso[] = $espe->especialidad->name;
                $subespecialidades_curso[] = $espe->SubespecialidadesName;
            }
            $curso->especialidades_list = implode(',', array_unique(array_filter($especialidades_curso)));
            $curso->subespecialidades_list = implode('<br />', str_replace(', ', '<br />', array_unique(array_filter($subespecialidades_curso))));



            $fotoscursoname = $curso->fotoscurso;
            $fotoscentroname = $curso->fotoscentro;
            if (is_file(public_path() . "/assets/uploads/course/" . $curso->course_images . "/" . $curso->image_portada)) {
                $cursoimg = '/assets/uploads/course/' . $curso->course_images . '/' . $curso->image_portada;
            } elseif (is_file(public_path() . "/assets/uploads/center/" . $curso->centro->center_images . "/" . $curso->centro->center_image_portada)) {
                $cursoimg = '/assets/uploads/center/' . $curso->centro->center_images . '/' . $curso->centro->center_image_portada;
            } elseif (!is_file(public_path() . "/assets/uploads/course/" . $curso->course_images . "/" . $curso->image_portada) && !is_file(public_path() . "/assets/uploads/center/" . $curso->centro->center_images . "/" . $curso->centro->center_image_portada)) {
                if (count($fotoscursoname)) {
                    $cursoimg = '/assets/uploads/course/' . $curso->course_images . '/' . $fotoscursoname[rand(0, count($fotoscursoname) - 1)];
                } elseif (!count($fotoscursoname) && count($fotoscentroname)) {
                    $cursoimg = '/assets/uploads/center/' . $curso->centro->center_images . '/' . $fotoscentroname[rand(0, count($fotoscentroname) - 1)];
                } else {
                    $cursoimg = '';
                }
            }

            $curso->cursoimg = $cursoimg;
            $curso->pais_slug = str_slug($curso->pais_name);
            $curso->curso_slug = $curso->course_slug . '.html';
            $curso->fullurl = str_slug($curso->pais_name) . '/' . $curso->course_slug . '.html';
        }

        $datos['cursos'] = $cursos->toArray();
        $datos['total'] = $cursos_total;

        return response()->json($datos, 200);
        // return $datos;
    }


    public function buscarByPais(Request $request)
    {
        //dippy

        $especialidades = $request->get('especialidad');
        $paises = $request->get('pais');
        $edades = $request->get('edad');

        // if(!$especialidades)
        // {
        //     // $especialidades = Especialidad::all()->pluck('id')->toArray();
        // }

        // if(!$paises)
        // {
        //     // $paises = Pais::all()->pluck('id')->toArray();
        // }

        // DB::enableQueryLog();

        $p = ConfigHelper::config('propietario');

        $list = Curso::select();

        if ($especialidades) {
            $cursos_id = [];
            $esp = EspecialidadWeb::whereIn('id', $especialidades)->get();
            foreach ($esp as $e) {
                foreach ($e->cursos as $c) {
                    array_push($cursos_id, $c->id);
                }
            }

            array_unique($cursos_id);
            $list = $list->whereIn('id', $cursos_id);
        }

        if ($paises) {
            $centros = Centro::whereIn('country_id', $paises)->pluck('id')->toArray();
            $list = $list->whereIn('center_id', $centros);
        }

        $list = $list->whereIn('propietario', [$p, 0])->where('activo_web', 1)->pluck('id')->toArray();

        $cursos = collect();

        if ($edades) {
            if ($edades == 19) {
                //+18 ??
            }

            foreach ($edades as $edad) {
                $c = Curso::whereRaw(
                    'find_in_set(?, course_age)',
                    [$edad]
                )
                    ->whereNotIn('id', $cursos->pluck('id')->toArray())
                    ->whereIn('id', $list)
                    ->whereIn('propietario', [$p, 0])
                    ->where('activo_web', 1)
                    ->get();

                $cursos = $cursos->merge($c);
            }
        } else {
            $cursos = Curso::whereIn('id', $list)->get();
        }

        // print_r(DB::getQueryLog());

        // dd($cursos);

        $view = ConfigHelper::view('welcome');
        return view($view, compact('cursos'));
    }



    public function buscar(Request $request)
    {
        //dd('buscar');
        $search = $request->input('search');
        $view = ConfigHelper::view($request);

        $cursos = collect();

        if ($search == '')
        {
            //return abort(404);
            $categorianame = '';
            $clase = 'bs';

            if (!view()->exists($view))
            {
                $pre = "web." . Session::get('vcn.tema');
                return view($pre . ".buscar", compact('search', 'categorianame', 'cursos', 'clase'));
            }

            return view($view, compact('search', 'categorianame', 'cursos', 'clase'));
        }



        $idiomaapp = App::getLocale();
        $p = ConfigHelper::config('propietario');

        if ($idiomaapp == 'es')
        {
            $cursos = Curso::where('activo_web', 1)
                ->where(function ($query) {
                    return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })
                ->where(function ($query) use ($search) {
                    return $query
                        ->orWhere('course_name', 'LIKE', '%' . $search . '%')
                        ->orWhere('course_summary', 'LIKE', '% ' . $search . ' %')
                        ->orWhere('course_content', 'LIKE', '% ' . $search . ' %')
                        ->orWhere('course_activities', 'LIKE', '% ' . $search . ' %')
                        ->orWhere('course_language', 'LIKE', '%' . $search . '%');
                })
                ->get();

            $categorianame = 'Has buscado: ' . $search;
            $clase = 'bs';
        }
        elseif ($idiomaapp == 'ca')
        {
            $cursoesid = Traduccion::where('idioma', 'ca')
                ->where('modelo', 'Curso')
                ->where('traduccion', 'LIKE', '%' . $search . '%')
                ->get();
            if (!count($cursoesid))
            {
                $cursos = Curso::where('activo_web', 1)
                    ->where(function ($query) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })
                    ->where(function ($query) use ($search) {
                        return $query
                            ->orWhere('course_name', 'LIKE', '%' . $search . '%')
                            ->orWhere('course_summary', 'LIKE', '%' . $search . '%')
                            ->orWhere('course_content', 'LIKE', '%' . $search . '%')
                            ->orWhere('course_activities', 'LIKE', '%' . $search . '%')
                            ->orWhere('course_language', 'LIKE', '%' . $search . '%');
                    })

                    ->get();
            }
            else
            {
                foreach ($cursoesid as $cursosid) {
                    $idcursos[] = $cursosid->modelo_id;
                }

                $cursos = Curso::where('activo_web', 1)
                    ->where(function ($query) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })
                    ->whereIn('id', $idcursos)
                    ->get();
            }


            $categorianame = 'Has cercat: ' . $search;
            $clase = 'bs';
        }

        if (!$cursos->count())
        {
            //return abort(404);
            $cursos = collect();
            $categorianame = '';
        }
        else
        {

            if (in_array(ConfigHelper::config('tema'), $this->temas))
            {
                foreach ($cursos as $curso)
                {
                    $curso->catweb = '';
                    $curso->subcatweb = '';
                    $curso->subcatwebdet = '';
                    $catcurso = $curso->categoria->id;

                    $catsweb = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('category_id', 0)->whereRaw('FIND_IN_SET(' . $catcurso . ',categorias)')->orderBy('orden')->get();
                    $curso->catweb = $catsweb->first();

                    foreach ($catsweb as $cw) {
                        $arrayCursosCat = $cw->cursos->pluck('id')->toArray();

                        if (in_array($curso->id, $arrayCursosCat)) {
                            $curso->catweb = $cw;
                        }
                        if (count($cw->hijos_activos) > 0) {
                            $hijoscatweb = $cw->hijos_activos;
                            foreach ($hijoscatweb as $hcw) {
                                if (count($hcw->cursos) > 0) {
                                    $arrayCursosSub = $hcw->cursos->pluck('id')->toArray();
                                    //dd($arrayCursosSub);
                                    if (in_array($curso->id, $arrayCursosSub)) {
                                        $curso->catweb = $cw;
                                        $curso->subcatweb = $hcw;
                                        $curso->cerrado = $hcw->cerrado;
                                    }
                                }
                            }
                        }
                    }

                    if ($curso->subcatweb != '') {
                        if (count($curso->subcatweb->hijos_activos) > 0) {
                            $hijossub = $curso->subcatweb->hijos_activos;
                            foreach ($hijossub as $hs) {
                                $arrayCursos = $hs->cursos->pluck('id')->toArray();
                                if (in_array($curso->id, $arrayCursos)) {
                                    $curso->subcatwebdet = $hs;
                                    $curso->cerrado = $hs->cerrado;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!view()->exists($view))
        {
            $pre = "web." . Session::get('vcn.tema');
            $view = $pre . ".buscar";
            return view($view, compact('search', 'categorianame', 'cursos', 'clase'));
        }

        return view($view, compact('search', 'categorianame', 'cursos', 'clase'));
    }


    public function promoscursos()
    {
        $view = ConfigHelper::view('promoscursos');
        $idiomaapp = App::getLocale();
        $p = ConfigHelper::config('propietario');

        $cursos = Curso::where('activo_web', 1)
            ->where(function ($query) {
                return $query
                    ->where('propietario', 0)
                    ->orWhere('propietario', ConfigHelper::config('propietario'));
            })
            ->where('course_promo', 1)
            ->get();
        $clase = 'bs';

        if (!count($cursos)) {
            //return abort(404);
            $cursos = '';
            $categorianame = '';
        } else {

            if (in_array(ConfigHelper::config('tema'), $this->temas)) {
                foreach ($cursos as $curso) {

                    $curso->catweb = '';
                    $curso->subcatweb = '';
                    $catcurso = $curso->categoria->id;

                    $catsweb = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('category_id', 0)->whereRaw('FIND_IN_SET(' . $catcurso . ',categorias)')->orderBy('orden')->get();
                    $curso->catweb = $catsweb->first();

                    foreach ($catsweb as $cw) {
                        $arrayCursosCat = $cw->cursos->pluck('id')->toArray();

                        if (in_array($curso->id, $arrayCursosCat)) {
                            $curso->catweb = $cw;
                        }
                        if (count($cw->hijos_activos) > 0) {
                            $hijoscatweb = $cw->hijos_activos;
                            foreach ($hijoscatweb as $hcw) {
                                $arrayCursosSub = $hcw->cursos->pluck('id')->toArray();
                                if (in_array($curso->id, $arrayCursosSub)) {
                                    $curso->catweb = $cw;
                                    $curso->subcatweb = $hcw;
                                    $curso->cerrado = $curso->subcatweb->cerrado;
                                }
                            }
                        }
                    }

                    if ($curso->subcatweb != '') {
                        if (count($curso->subcatweb->hijos_activos) > 0) {
                            $hijossub = $curso->subcatweb->hijos_activos;
                            foreach ($hijossub as $hs) {
                                $arrayCursos = $hs->cursos->pluck('id')->toArray();
                                if (in_array($curso->id, $arrayCursos)) {
                                    $curso->subcatwebdet = $hs;
                                    $curso->cerrado = $curso->subcatwebdet->cerrado;
                                } else {
                                    $curso->subcatwebdet = '';
                                    $curso->cerrado = $curso->subcatweb->cerrado;
                                }
                            }
                        }
                    }
                }
            }
        }

        //link a otro idioma
        $langs = ConfigHelper::idiomas();
        $weblangs = array();
        foreach ($langs as $lang) {
            $weblangs[$lang] = 'http' . (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '') . '://' . ConfigHelper::config('web') . '/' . $lang . '/' . trans('web.cursos-en-promocion', [], $lang);
        }

        if (!view()->exists($view)) {
            $pre = "web." . Session::get('vcn.tema');
            return view($pre . ".promoscursos", compact('categorianame', 'cursos', 'clase', 'weblangs'));
        }

        return view($view, compact('categorianame', 'cursos', 'clase', 'weblangs'));
    }

    public function getPais($pais = null)
    {
        $p = Pais::where('slug', $pais)->first();
        if (!$p) {
            abort(404);
        }

        $centros = Centro::where('country_id', $p->id)->pluck('id')->toArray();
        $cursos = Curso::whereIn('center_id', $centros)->where('activo_web', 1)->get();

        //.... Raquel
        dd($cursos);
    }

    public function getPaisCurso(Request $request, $pais, $slug)
    {
        /*
        $curso = Curso::where('course_slug', $slug)
            ->where('activo_web', 1)
            ->where(function ($query) use ($slug, $p) {
                return $query
                    ->where('propietario', 0)
                    ->orWhere('propietario', $p);
            })
            ->first();

        //... Raquel
        dd($curso);
        */

        $p = ConfigHelper::config('propietario');

        $categoria = $pais;
        $view = ConfigHelper::view($categoria);
        $curso = Curso::where('course_slug', $slug)
            ->where('activo_web', 1)
            ->where(function ($query) use ($slug) {
                return $query
                    ->where('propietario', 0)
                    ->orWhere('propietario', ConfigHelper::config('propietario'));
            })->first();

        if (!$curso) {
            //return abort(404);
            return self::index($request);
        } elseif (($curso->propietario != 0 && $curso->propietario != $p) || $curso->activo_web == 0) {
            //return abort(404);
            return self::index($request);
        } else {
            foreach ($curso->especialidades as $espe) {
                $especialidades_curso[] = $espe->especialidad->name;
                $subespecialidades_curso[] = $espe->SubespecialidadesName;
            }
            if ($curso->especialidades_list) {
                $curso->especialidades_list = implode(',', array_unique(array_filter($especialidades_curso)));
                $curso->subespecialidades_list = implode(',', array_unique(array_filter($subespecialidades_curso)));
            }

            if (!view()->exists($view)) {
                $pre = "web." . Session::get('vcn.tema');
                return view($pre . ".curso", compact('categoria', 'curso'));
            }

            return view($view, compact('categoria', 'curso'));
        }
    }

    public function getPaisPdfCurso($pais, $id)
    {
        //Pendiente
    }


    public function getEspecialidad($esp)
    {
        $p = ConfigHelper::config('propietario');
        $especialidad = EspecialidadWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $esp)->where('especialidad_id', 0)->orderBy('orden')->first();

        //Pendiente
        dd($especialidad->cursos);
    }

    public function getSubespecialidad($esp, $subesp)
    {
        $p = ConfigHelper::config('propietario');
        $especialidad = EspecialidadWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $esp)->where('especialidad_id', 0)->orderBy('orden')->first();
        $subespecialidad = EspecialidadWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $subesp)->where('especialidad_id', $especialidad->id)->orderBy('orden')->first();

        //Pendiente
        dd($subespecialidad->cursos);
    }

    public function getEspecialidadCurso(Request $request, $esp, $slug)
    {
        //Pendiente
        $p = ConfigHelper::config('propietario');
        $especialidad = EspecialidadWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', $esp)->where('especialidad_id', 0)->orderBy('orden')->first();

        $curso = Curso::where('course_slug', $slug);
    }

    public function getEspecialidadPdfCurso($esp, $id)
    {
        //Pendiente
    }

    public function postCursoInfo(Request $request)
    {
        $data = $request->except('_token');

        MailHelper::mailCurso($data);

        $ret = true;
        return response()->json($ret, 200);
    }

    public function postInformacion(Request $request)
    {
        $data = $request->except('_token');

        MailHelper::mailWebInformacion($data);

        $ret = [
            'result' => true,
            'msg' => trans('web.mensajegracias'),
        ];
        
        return response()->json($ret, 200);
    }

    public function postContacto(Request $request)
    {
        $data = $request->except('_token');

        MailHelper::mailContacto($data);

        $ret = true;
        return response()->json($ret, 200);
    }

    public function postCatalogoInfo(Request $request)
    {
        $data = $request->except('_token');

        MailHelper::mailCatalogo($data);

        $ret = true;
        return response()->json($ret, 200);
    }

    public function getCatalogo()
    {
        //link a otro idioma
        $langs = ConfigHelper::idiomas();
        $weblangs = array();
        foreach ($langs as $lang) {
            $weblangs[$lang] = 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://' . ConfigHelper::config('web') . "/" . $lang . "/" . trans('web.catalogo-slug', [], $lang);
        }

        $pre = "web." . Session::get('vcn.tema');
        return view($pre . '.catalogo', compact('weblangs'));
    }

    public function getContacto()
    {
        //link a otro idioma
        $langs = ConfigHelper::idiomas();
        $weblangs = array();
        foreach ($langs as $lang) {
            $weblangs[$lang] = 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://' . ConfigHelper::config('web') . "/" . $lang . "/" . trans('web.contacto-slug', [], $lang);
        }

        $pre = "web." . Session::get('vcn.tema');
        $view = $pre . '.contacto';
        if(config('app.timezone') == "America/Mexico_City")
        {
            $view = $pre . '.contacto_sf';
        }

        $pre = "web." . Session::get('vcn.tema');
        return view($view, compact('weblangs'));
    }

    public function getInscripcion()
    {
        //link a otro idioma
        $langs = ConfigHelper::idiomas();
        $weblangs = array();
        foreach ($langs as $lang) {
            $weblangs[$lang] = 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://' . ConfigHelper::config('web') . "/" . $lang . "/" . trans('web.inscripcion-slug', [], $lang);
        }

        $pre = "web." . Session::get('vcn.tema');
        return view($pre . '.inscripcion', compact('weblangs'));
    }

    public function oldweb(Request $request)
    {
        if (ConfigHelper::config('sufijo') == 'bs' || ConfigHelper::config('sufijo') == 'cic') {
            $file = $request->path();
            if (strpos($file, '_curso.php') > 0) {
                $oldcat = substr($file, 0, strpos($file, '_curso.php'));
                if ($oldcat == 'MAXcamps' || $oldcat == 'SUMMERcamps') {
                    $oldcat = 'campamentos';
                }
                if ($oldcat == 'jovenes_academico') {
                    $oldcat = 'escolar';
                }
                $categoria = Self::findCategory($oldcat);

                if ($categoria != null) {

                    Session::flash('redirected', true);
                    if ($categoria->nivel == 2) {
                        $padre = $categoria->padre;
                        return Redirect::to(route('web.raiz.sub', ['param1' => $padre->seo_url, 'param2' => $categoria->seo_url]), 301);
                    } elseif ($categoria->nivel == 1) {
                        return Redirect::to(route('web.raiz', ['param1' => $categoria->seo_url]), 301);
                    } else {
                        abort(404);
                    }
                }
            } else {
                abort(404);
            }
        }
    }

    public function findCategory($oldcat)
    {
        $p = ConfigHelper::config('propietario');
        $categoria = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('seo_url', 'LIKE', '%' . $oldcat . '%')->whereIn('category_id', [0, 1])->orderBy('orden')->first();
        return $categoria;
    }

    public function getProveedor(Request $request, $curso_id)
    {
        $curso = Curso::findOrFail($curso_id);
        $url = $curso->course_provider_url;

        $user_id = $request->user() ? $request->user()->id : 0;

        

        return redirect()->to($url);
    }
}
