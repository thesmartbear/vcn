<?php

namespace VCN\Http\Controllers\Web\CMS;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\CMS\PaginaRepository as Pagina;

use VCN\Helpers\ConfigHelper;

use Datatable;

class PaginaController extends Controller
{

    private $pagina;

    /**
     * Instantiate a new PaginaController instance.
     *
     * @return void
     */
    public function __construct( Pagina $pagina )
    {
        $this->checkPermisos('cms');

        $this->pagina = $pagina;
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            $filtro = ConfigHelper::config('propietario');
            if( $filtro && !auth()->user()->isFullAdmin() )
            {
                // $col = ProveedorModel::where('propietario', 0)->orWhere('propietario',$filtro)->get();
                $col1 = $this->pagina->findWhere(['propietario'=> $filtro]);
                $col = $this->pagina->findWhere(['propietario'=> 0]);

                $col = $col->merge($col1);
            }
            else
            {
                $col = $this->pagina->all();
            }


            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cms.paginas.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->showColumns('titulo','url','orden')
                ->addColumn('slug', function($model) {
                    return $model->tipo_name;
                })
                ->addColumn('tipo', function($model) {

                    return $model->tipo_name;
                })
                ->addColumn('padre', function($model) {
                    if($model->padre)
                    {
                        return "<a href='". route('manage.cms.paginas.ficha',[$model->page_id]) ."'>". $model->padre->name ."</a>";
                    }

                    return "-";
                })
                ->addColumn('nivel', function($model) {

                    return $model->nivel;
                })
                ->addColumn('activo', function($model) {
                    return $model->activo?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Página' data-action='". route( 'manage.cms.paginas.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name','titulo')
                // ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.cms.paginas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getNuevo()
    {
        $padres = [0=> 'Raíz'] + $this->pagina->all()->pluck('name','id')->toArray();

        return view('manage.cms.paginas.new')->with(compact('padres'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getUpdate($id)
    {
        $ficha = $this->pagina->find($id);

        $padres = [0=> 'Raíz'] + $this->pagina->all()->pluck('name','id')->toArray();

        return view('manage.cms.paginas.ficha', compact('ficha','padres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name'      => 'required',
            'titulo'    => 'required',
            // 'url'       => 'required|unique:cms_paginas',
            'url'       => 'required',
        ]);

        $data = $request->except('_token','propietario_check');

        $data['propietario'] = $request->has('propietario')?$request->input('propietario'):0;
        if( $request->has('propietario_check') )
        {
            $data['propietario'] = ConfigHelper::config('propietario');
        }

        $data['activo'] = $request->has('activo');
        $data['menu'] = $request->has('menu');
        $data['menu_secundario'] = $request->has('menu_secundario');
        
        if(!$id)
        {
            $id = $this->pagina->create($data);
        }
        else
        {
            $this->pagina->update($data,$id);
        }

        return redirect()->route('manage.cms.paginas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->pagina->delete($id);
        return redirect()->route('manage.cms.paginas.index');
    }
}
