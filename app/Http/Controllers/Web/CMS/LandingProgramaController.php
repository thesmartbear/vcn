<?php

namespace VCN\Http\Controllers\Web\CMS;

use VCN\Models\CMS\LandingPrograma;
use Illuminate\Http\Request;
use VCN\Http\Controllers\Controller;

use Datatable;
use Session;
use ConfigHelper;

class LandingProgramaController extends Controller
{
    const RUTA = 'manage.cms.landings.programas.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Datatable::shouldHandle())
        {
            $col = LandingPrograma::all();

            return Datatable::collection( $col )
                ->showColumns('firma')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cms.landings.programas.edit',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Landing destino' data-action='". route( 'manage.cms.landings.programas.destroy', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.cms.landings.programas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(self::RUTA.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required',
            'title'      => 'required',
        ]);

        $programa = LandingPrograma::create($request->except('foto'));
        $id = $programa->id;
        
        if($request->hasFile('foto'))
        {
            $file = $request->file('foto');
            $dirp = "assets/uploads/landings/programas/". $id . "/";

            $programa->foto = ConfigHelper::uploadOptimize($file, $dirp);
            $programa->save();
        }

        Session::flash('mensaje-ok', 'Destino creado correctamente');
        return redirect()->route(self::RUTA."index");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \VCN\Models\CMS\LandingPrograma  $programa
     * @return \Illuminate\Http\Response
     */
    public function edit(LandingPrograma $programa)
    {
        $ficha = $programa;
        return view(self::RUTA.'edit')->with(compact('ficha'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \VCN\Models\CMS\LandingPrograma  $programa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LandingPrograma $programa)
    {
        $this->validate($request, [
            'name'      => 'required',
            'title'      => 'required',
        ]);
        
        $id = $programa->id;

        $programa->update($request->except('foto'));

        if($request->hasFile('foto'))
        {
            $file = $request->file('foto');
            $dirp = "assets/uploads/landings/destinos/". $id . "/";

            $programa->foto = ConfigHelper::uploadOptimize($file, $dirp);
            $programa->save();
        }

        Session::flash('mensaje-ok', 'Destino modificado correctamente');
        return redirect()->route(self::RUTA."index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \VCN\Models\CMS\LandingPrograma  $programa
     * @return \Illuminate\Http\Response
     */
    public function destroy(LandingPrograma $programa)
    {
        $programa->delete();

        Session::flash('mensaje-ok', 'Destino eliminado correctamente');

        return redirect()->back();
    }
}
