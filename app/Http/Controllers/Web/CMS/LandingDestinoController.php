<?php

namespace VCN\Http\Controllers\Web\CMS;

use VCN\Models\CMS\LandingDestino;
use Illuminate\Http\Request;
use VCN\Http\Controllers\Controller;

use Datatable;
use Session;
use ConfigHelper;

class LandingDestinoController extends Controller
{
    const RUTA = 'manage.cms.landings.destinos.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Datatable::shouldHandle())
        {
            $col = LandingDestino::all();

            return Datatable::collection( $col )
                ->showColumns('firma')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cms.landings.destinos.edit',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Landing destino' data-action='". route( 'manage.cms.landings.destinos.destroy', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.cms.landings.destinos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(self::RUTA.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required',
            'title'      => 'required',
        ]);

        $destino = LandingDestino::create($request->except('foto'));
        $id = $destino->id;

        if($request->hasFile('foto'))
        {
            $file = $request->file('foto');
            $dirp = "assets/uploads/landings/destinos/". $id . "/";

            $destino->foto = ConfigHelper::uploadOptimize($file, $dirp);
            $destino->save();
        }

        Session::flash('mensaje-ok', 'Destino creado correctamente');
        return redirect()->route(self::RUTA."index");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \VCN\Models\CMS\LandingDestino  $destino
     * @return \Illuminate\Http\Response
     */
    public function edit(LandingDestino $destino)
    {
        $ficha = $destino;
        return view(self::RUTA.'edit')->with(compact('ficha'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \VCN\Models\CMS\LandingDestino  $destino
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LandingDestino $destino)
    {
        $this->validate($request, [
            'name'      => 'required',
            'title'      => 'required',
        ]);
        
        $id = $destino->id;

        $destino->update($request->except('foto'));

        if($request->hasFile('foto'))
        {
            $file = $request->file('foto');
            $dirp = "assets/uploads/landings/destinos/". $id . "/";

            $destino->foto = ConfigHelper::uploadOptimize($file, $dirp);
            $destino->save();
        }

        Session::flash('mensaje-ok', 'Destino modificado correctamente');
        return redirect()->route(self::RUTA."index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \VCN\Models\CMS\LandingDestino  $destino
     * @return \Illuminate\Http\Response
     */
    public function destroy(LandingDestino $destino)
    {
        $destino->delete();

        Session::flash('mensaje-ok', 'Destino eliminado correctamente');

        return redirect()->back();
    }
}
