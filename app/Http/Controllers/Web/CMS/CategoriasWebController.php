<?php

namespace VCN\Http\Controllers\Web\CMS;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\CMS\CategoriaWeb;
use VCN\Models\Categoria;
use VCN\Models\Subcategoria;
use VCN\Models\SubcategoriaDetalle;
use VCN\Models\Traducciones\Traduccion;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Image;
use File;
use Session;

class CategoriasWebController extends Controller
{
    private $categoria;

    /**
     * Instantiate a new CategoriasWebController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->checkPermisos('cms');
    }

    public function getIndex(Request $request)
    {
        if(Datatable::shouldHandle())
        {
            $estructura = $request->get('estructura') ?: ConfigHelper::config('web_estructura');

            $filtro = ConfigHelper::config('propietario');

            $col = CategoriaWeb::where('estructura', $estructura);
            $col->where(function ($query) use ($filtro) {
                $query->where('propietario', $filtro)
                    ->orWhere('propietario', 0);
            });

            return Datatable::collection( $col->orderBy('orden')->get() )
                ->showColumns('orden')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cms.categorias.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('padre', function($model) {
                    if($model->padre)
                    {
                        return "<a href='". route('manage.cms.categorias.ficha',[$model->category_id]) ."'>". $model->padre->name ."</a>";
                    }

                    return "-";
                })
                ->addColumn('nivel', function($model) {
                    return $model->nivel;
                })
                ->showColumns('titulo','idioma')
                ->addColumn('tipo', function($model) {
                    $ret = $model->pais?"País: ".$model->pais->name:"Categorías";

                    // $ret .= " (v.$model->estructura)";

                    return $ret;
                })
                ->addColumn('estructura', function($model) {
                    $ret = $model->estructura;

                    return $ret;
                })
                ->addColumn('plataforma', function($model) {
                    return ConfigHelper::plataforma($model->propietario);
                })
                ->addColumn('tipo_enlace', function($model) {
                    return $model->es_link?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('menu_principal', function($model) {
                    return $model->menu_principal?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('menu_secundario', function($model) {
                    return $model->menu_secundario?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('activo', function($model) {
                    return $model->activo?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Categoría Web' data-action='". route( 'manage.cms.categorias.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name','titulo')
                ->orderColumns('orden','nivel','name','titulo','idioma','plataforma')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }
        return view('manage.cms.categorias.index');
    }

    public function getIndex301(Request $request)
    {
        $lang = $request->get("lang");
        $idioma = $lang ?: App::getLocale();

        if(Datatable::shouldHandle())
        {
            $estructura = 1;

            $filtro = ConfigHelper::config('propietario');

            $col = CategoriaWeb::where('estructura', $estructura);
            $col->where(function ($query) use ($filtro) {
                $query->where('propietario', $filtro)
                    ->orWhere('propietario', 0);
            });

            return Datatable::collection( $col->orderBy('orden')->get() )
                ->showColumns('orden')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cms.categorias.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('nivel', function($model) {
                    return $model->nivel;
                })
                ->showColumns('titulo','idioma')
                ->addColumn('slug', function($model) use ($idioma) {
                    $s = $model->getSlugIdioma($idioma);

                    $url = "<a class='pull-right' href='/$s' target='_blank'><i class='fa fa-link'></i></a>";
                    return $s . $url;
                })
                ->addColumn('301', function($model) use ($idioma) {
                    $s = $model->getSlugIdioma($idioma);
                    $catw = CategoriaWeb::buscar301Categoria($s);

                    $s = "/nueva/$s";

                    $url = "<a class='pull-right' href='$s' target='_blank'><i class='fa fa-link'></i></a>";

                    return $catw?($catw->slug . $url):"404";
                })
                ->searchColumns('name','titulo')
                ->orderColumns('orden','nivel','name','titulo','idioma','plataforma')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.cms.categorias.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getNuevo(Request $request)
    {
        if( auth()->user()->isFullAdmin() )
        {
            $plataformas = ConfigHelper::plataformas();

            //$categorias = [0 => 'Todas'] + Categoria::all()->sortBy('name')->pluck('name','id')->toArray();
            $categorias = Categoria::all()->sortBy('name')->pluck('name','id')->toArray();

            foreach($categorias as $kc => $vc){
                $subcategorias[$vc] = Subcategoria::where('category_id',$kc)->orderBy('name')->pluck('name','id')->toArray();
            }


            $subcats = Subcategoria::join('subcategoria_detalles','subcategoria_detalles.subcategory_id', '=', 'subcategorias.id')->select('subcategoria_detalles.subcategory_id', 'subcategorias.name')->groupBy('subcategorias.id')->get();
            foreach($subcats as $ksc => $vsc){
                $subcategoriasdet[$vsc->name] = SubcategoriaDetalle::where('subcategory_id',$vsc->subcategory_id)->orderBy('name')->pluck('name','id')->toArray();
            }

            $plataforma = $request->input('plataformas', 0);
        }
        else
        {
            $plataforma = ConfigHelper::propietario();

            $plataformas = [ 0=>ConfigHelper::plataformaApp() ];

            //$categorias = [0 => 'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $categorias = Categoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();

            //he quitado plataforma() porque no hace nada y no funcionaba el join
            foreach($categorias as $kc => $vc){
                $subcategorias[$vc] = Subcategoria::where('category_id',$kc)->orderBy('name')->pluck('name','id')->toArray();
            }

            $subcats = Subcategoria::join('subcategoria_detalles','subcategoria_detalles.subcategory_id', '=', 'subcategorias.id')->select('subcategoria_detalles.subcategory_id', 'subcategorias.name')->groupBy('subcategorias.id')->get();
            foreach($subcats as $ksc => $vsc){
                //echo $vsc->name.'<br />';
                $subcategoriasdet[$vsc->name] = SubcategoriaDetalle::where('subcategory_id',$vsc->subcategory_id)->orderBy('name')->pluck('name','id')->toArray();
            }
        }

        $estructura = ConfigHelper::config('web_estructura');
        $padres = [0=> 'Raíz'] + CategoriaWeb::where('estructura', $estructura)->pluck('name','id')->toArray();

        return view('manage.cms.categorias.new', compact( 'plataformas', 'categorias', 'subcategorias', 'subcategoriasdet', 'plataforma','padres'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getUpdate(Request $request, $id)
    {
        $ficha = CategoriaWeb::find($id);

        $valores = [];

        if( auth()->user()->isFullAdmin() )
        {
            $plataformas = ConfigHelper::plataformas();

            $categorias = Categoria::all()->sortBy('name')->pluck('name','id')->toArray();

            foreach($categorias as $kc => $vc){
                $subcategorias[$vc] = Subcategoria::where('category_id',$kc)->orderBy('name')->pluck('name','id')->toArray();
            }


            $subcats = Subcategoria::join('subcategoria_detalles','subcategoria_detalles.subcategory_id', '=', 'subcategorias.id')->select('subcategoria_detalles.subcategory_id', 'subcategorias.name')->groupBy('subcategorias.id')->get();
            foreach($subcats as $ksc => $vsc){
                //echo $vsc->name.'<br />';
                $subcategoriasdet[$vsc->name] = SubcategoriaDetalle::where('subcategory_id',$vsc->subcategory_id)->orderBy('name')->pluck('name','id')->toArray();
            }


            $plataforma = $request->input('plataformas', 0);
        }
        else
        {
            $plataforma = ConfigHelper::propietario();

            $plataformas = [ 0=>ConfigHelper::plataformaApp() ];

            $categorias = Categoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            foreach($categorias as $kc => $vc){
                $subcategorias[$vc] = Subcategoria::plataforma()->where('category_id',$kc)->sortBy('name')->pluck('name','id')->toArray();
            }

            //$subcategorias = Subcategoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $subcats = Subcategoria::join('subcategoria_detalles','subcategoria_detalles.subcategory_id', '=', 'subcategorias.id')->select('subcategoria_detalles.subcategory_id', 'subcategorias.name')->groupBy('subcategorias.id')->get();
            foreach($subcats as $ksc => $vsc){
                //echo $vsc->name.'<br />';
                $subcategoriasdet[$vsc->name] = SubcategoriaDetalle::plataforma()->where('subcategory_id',$vsc->subcategory_id)->sortBy('name')->pluck('name','id')->toArray();
            }

            //$subcategoriasdet = SubcategoriaDetalle::plataforma()->sortBy('name')->pluck('name','id')->toArray();
        }

        $estructura = $ficha->estructura;
        $padres = [0=> 'Raíz'] + CategoriaWeb::where('estructura', $estructura)->where('id', '!=', $ficha->id)->pluck('name','id')->toArray();

        $slug = $ficha->seo_url;
        $p = $ficha->home_propietario;

        $c = CategoriaWeb::where('seo_url', $slug)->where('estructura', $ficha->estructura)->whereIn('home_propietario',[0,$p]);
        if($c->count()>1)
        {
            Session::flash("mensaje", "Revise el slug (SEO). Está duplicado ". $c->count() ." veces");
        }
        //Traducción tb:
        $t = Traduccion::where(['modelo'=> 'CategoriaWeb', 'idioma'=> "ca", 'campo'=> 'seo_url', 'traduccion'=> $slug]);
        if($t->count()>1)
        {
            Session::flash("mensaje", "Revise la traducción del slug (SEO). Está duplicado ". $t->count() ." veces");
        }

        return view('manage.cms.categorias.ficha', compact('ficha', 'plataformas', 'categorias', 'subcategorias', 'subcategoriasdet', 'plataforma', 'padres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name'      => 'required',
            'idioma'    => 'required',
            // 'seo_url'  => 'required|max:255|unique:cms_categorias',
        ]);

        $data = $request->except('_token','imagen_off','home_propietario_check','video','video_url','imagen_delete');

        $data['propietario'] = $request->has('propietario') ? $request->input('propietario') : 0;
        $data['home_propietario'] = $request->has('home_propietario') ? $request->input('home_propietario') : 0;
        if( $request->has('home_propietario_check') )
        {
            $data['home_propietario'] = ConfigHelper::config('propietario');
            $data['propietario'] = ConfigHelper::config('propietario');
        }

        //categorias, subcategorias, subcategoriasdet : implode
        $data['idioma'] = isset($data['idioma']) ? implode(',', $data['idioma']) : null;
        $data['orden'] = isset($data['orden']) ? $data['orden'] : 0;
        $data['titulo'] = isset($data['titulo']) ? $data['titulo'] : "";

        if(isset($data['categorias']))
        {
            !is_array($data['categorias']) ? $data['categorias'] = str_split($data['categorias']) : $data['categorias'] = $data['categorias'];
            $data['categorias'] = isset($data['categorias']) ? implode(',', $data['categorias']) : null;
            if(isset($data['subcategorias'])) {
                !is_array($data['subcategorias']) ? $data['subcategorias'] = str_split($data['subcategorias']) : $data['subcategorias'] = $data['subcategorias'];
                $data['subcategorias'] = isset($data['subcategorias'])?implode(',', $data['subcategorias']):null;
            }

            if(isset($data['subcategoriasdet'])) {
                !is_array($data['subcategoriasdet']) ? $data['subcategoriasdet'] = str_split($data['subcategoriasdet']) : $data['subcategoriasdet'] = $data['subcategoriasdet'];
                $data['subcategoriasdet'] = isset($data['subcategoriasdet'])?implode(',', $data['subcategoriasdet']):null;
            }
        }
        
        if(!isset($data['subcategorias']))
        {
            $data['subcategorias'] = null;
        }

        if(!isset($data['subcategoriasdet']))
        {
            $data['subcategoriasdet'] = null;
        }

        $data['activo'] = $request->has('activo');
        $data['cerrado'] = $request->has('cerrado');
        $data['es_link'] = $request->has('es_link');
        $data['link_blank'] = $request->has('link_blank');
        $data['home_boton_activo'] = $request->has('home_boton_activo');
        $data['home_bloque_slide'] = $request->has('home_bloque_slide');

        $data['menu_principal'] = $request->has('menu_principal');
        $data['menu_secundario'] = $request->has('menu_secundario');
        $data['menu_paises'] = $request->has('menu_paises');
        $data['es_home'] = $request->has('es_home');

        if(!$id)
        {
            // $data['estructura'] = 2; //nueva estructura web

            $ficha = CategoriaWeb::create($data);
            $id = $ficha->id;
        }
        else
        {
            $ficha = CategoriaWeb::find($id);
            $ficha->update($data);
        }

        foreach($ficha->cursos as $curso)
        {
            // $curso->updateDataWeb(true);
            $job = new \VCN\Jobs\JobDataWeb($curso);
            app('Illuminate\Contracts\Bus\Dispatcher')->dispatch($job);
        }

        //Vídeo:
        if($request->hasFile('video') && !$request->has('video_delete'))
        {
            $file = $request->file('video');

            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file_name = str_slug($file_name) .".". $file->getClientOriginalExtension();
            $dirp = "assets/uploads/cms/categorias/". $id . "/";
            $dir = public_path($dirp);

            $file->move($dir, $file_name);
            $ficha->video_url = "/".$dirp.$file_name;
            $ficha->save();
        }

        if($request->has('video_delete'))
        {
            if( file_exists( public_path($ficha->video_url)) )
            {
                unlink(public_path($ficha->video_url));
            }

            $ficha->video_url = null;
            $ficha->save();
        }
        
        $imagenes = ['imagen', 'home_imagen'];
        foreach($imagenes as $imag)
        {
            if($request->hasFile($imag) && !$request->has($imag.'_delete'))
            {
                $dirp = "assets/uploads/cms/categorias/". $id . "/";
                $file = $request->file($imag);
                
                $ficha->$imag = ConfigHelper::uploadOptimize($file, $dirp);
                $ficha->save();
            }

            if($request->has($imag.'_delete'))
            {
                if( file_exists( public_path($ficha->$imag)) )
                {
                    unlink(public_path($ficha->$imag));
                }
                // $file_name_thumb = "thumb/" . $file_name . $ext;
                
                $imagen = public_path($ficha->$imag) .".webp";
                if( file_exists( $imagen ) )
                {
                    unlink(public_path($ficha->$imag));
                }

                $ficha->$imag = null;
                $ficha->save();
            }
        }

        return redirect()->route('manage.cms.categorias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $ficha = CategoriaWeb::findOrFail($id);
        $ficha->delete();

        return redirect()->route('manage.cms.categorias.index');
    }

}
