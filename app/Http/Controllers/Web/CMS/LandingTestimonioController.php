<?php

namespace VCN\Http\Controllers\Web\CMS;

use VCN\Models\CMS\LandingTestimonio;
use Illuminate\Http\Request;
use VCN\Http\Controllers\Controller;

use Datatable;
use Session;
use ConfigHelper;
class LandingTestimonioController extends Controller
{

    const RUTA = 'manage.cms.landings.testimonios.';
    const NAME  = 'Testimonio';
    const NAMES = 'Testimonios';

    public function __construct()
    {
        $this->checkPermisos('cms');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Datatable::shouldHandle())
        {
            $col = LandingTestimonio::all();

            return Datatable::collection( $col )
                ->showColumns('firma')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cms.landings.testimonios.edit',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('es_principal', function($model) {
                    return $model->es_principal ? "<span class='badge badge-help'>SI</span>" : "<span class='badge'>NO</span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Landing Testimonio' data-action='". route( 'manage.cms.landings.testimonios.destroy', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.cms.landings.testimonios.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(self::RUTA.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required',
            'firma'    => 'required'
        ]);

        $testimonio = LandingTestimonio::create($request->except('foto'));
        $id = $testimonio->id;

        if($request->hasFile('foto'))
        {
            $file = $request->file('foto');
            $dirp = "assets/uploads/landings/testimonios/". $id . "/";

            $testimonio->foto = ConfigHelper::uploadOptimize($file, $dirp);
            $testimonio->save();
        }

        $name = self::NAME;
        Session::flash('mensaje-ok', "$name creado correctamente");
        return redirect()->route(self::RUTA."index");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \VCN\Models\CMS\LandingTestimonio  $testimonio
     * @return \Illuminate\Http\Response
     */
    public function edit(LandingTestimonio $testimonio)
    {
        $ficha = $testimonio;
        return view(self::RUTA.'edit')->with(compact('ficha'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \VCN\Models\CMS\LandingTestimonio  $testimonio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LandingTestimonio $testimonio)
    {
        $this->validate($request, [
            'name'      => 'required',
            'firma'    => 'required'
        ]);

        $id = $testimonio->id;

        $data = $request->except('foto');
        $testimonio->update($data);

        if($request->hasFile('foto'))
        {
            $file = $request->file('foto');
            $dirp = "assets/uploads/landings/testimonios/". $id . "/";

            $testimonio->foto = ConfigHelper::uploadOptimize($file, $dirp);
            $testimonio->save();
        }

        $name = self::NAME;
        Session::flash('mensaje-ok', "$name modificado correctamente");
        return redirect()->route(self::RUTA."index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \VCN\Models\CMS\LandingTestimonio  $testimonio
     * @return \Illuminate\Http\Response
     */
    public function destroy(LandingTestimonio $testimonio)
    {
        $testimonio->delete();
        
        $name = self::NAME;
        Session::flash('mensaje-ok', "$name eliminado correctamente");

        return redirect()->back();
    }
}
