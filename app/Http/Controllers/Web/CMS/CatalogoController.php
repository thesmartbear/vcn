<?php

namespace VCN\Http\Controllers\Web\CMS;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\CMS\Catalogo;
use VCN\Models\System\Plataforma;

use VCN\Helpers\ConfigHelper;

use Datatable;

class CatalogoController extends Controller
{

    /**
     * Instantiate a new PaginaController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->checkPermisos('cms');
    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            $filtro = ConfigHelper::config('propietario');
            if( $filtro && !auth()->user()->isFullAdmin() )
            {
                $col = Catalogo::where('plataforma', 0)->orWhere('plataforma',$filtro)->get();
            }
            else
            {
                $col = Catalogo::all();
            }

            return Datatable::collection( $col )
                ->showColumns('grupo','idioma')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cms.catalogos.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('plataforma', function($model) {
                    return $model->plataforma ? $model->plataforma->name : "-";
                })
                ->addColumn('activo', function($model) {
                    return $model->activo?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Catálogo' data-action='". route( 'manage.cms.catalogos.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name','grupo')
                ->orderColumns('name','grupo')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.cms.catalogos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getNuevo()
    {
        return view('manage.cms.catalogos.new');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getUpdate($id)
    {
        $ficha = Catalogo::find($id);

        return view('manage.cms.catalogos.ficha', compact('ficha'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'grupo'      => 'required',
            'idioma'    => 'required',
            // 'imagen'       => 'required|unique:cms_catalogos',
            // 'pdf'       => 'required|unique:cms_catalogos',
        ]);

        $data['grupo'] = $request->get('grupo');
        $data['name'] = $request->get('name');
        $data['idioma'] = $request->get('idioma');
        $data['activo'] = $request->has('activo');
        $data['plataforma_id'] = (int)$request->get('plataforma_id');
        $plat = Plataforma::find($data['plataforma_id']);

        if(!$id)
        {
            $ficha = Catalogo::create($data);
        }
        else
        {
            $ficha = Catalogo::find($id);
            $ficha->update($data);
        }

        $campo = "pdf";
        if($request->hasFile($campo))
        {
            $file = $request->file($campo);
            $dirp = "assets/catalogos/pdf/". $plat->sufijo . "/" . $data['idioma'] . "/";

            $ficha->$campo = ConfigHelper::uploadFile($file, $dirp);
            $ficha->save();
        }

        $campo = "imagen";
        if($request->hasFile($campo))
        {
            $file = $request->file($campo);
            $dirp = "assets/catalogos/preview/". $plat->sufijo . "/" . $data['idioma'] . "/";

            $ficha->$campo = ConfigHelper::uploadFoto($file, $dirp, 800);
            $ficha->save();
        }

        return redirect()->route('manage.cms.catalogos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $ficha = Catalogo::find($id);
        $ficha->delete();

        return redirect()->route('manage.cms.catalogos.index');
    }
}
