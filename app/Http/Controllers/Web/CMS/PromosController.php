<?php

namespace VCN\Http\Controllers\Web\CMS;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\CMS\PromoRepository as Promo;

use VCN\Models\CMS\CategoriaWeb;
use ConfigHelper;
use Datatable;
use Image;
use File;

class PromosController extends Controller
{
    private $promo;

    /**
     * Instantiate a new PromosController instance.
     *
     * @return void
     */
    public function __construct( Promo $promo )
    {
        $this->checkPermisos('cms');
        $this->promo = $promo;

    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {
            $col = $this->promo->all();

            $filtro = ConfigHelper::config('propietario');
            if( $filtro && !auth()->user()->isFullAdmin() )
            {
                $col1 = $this->promo->findWhere(['propietario'=> $filtro]);
                $col = $this->promo->findWhere(['propietario'=> 0]);

                $col = $col->merge($col1);
            }

            return Datatable::collection( $col )
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cms.promos.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('seccion', function($model) {
                    return $model->seccion?$model->seccion->name:'Panel Promos';
                })
                ->addColumn('plataforma', function($model) {
                    return ConfigHelper::plataforma($model->propietario);
                })
                ->addColumn('orden', function($model) {
                    return $model->orden;
                })
                ->addColumn('activo', function($model) {
                    return $model->activo?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Promo' data-action='". route( 'manage.cms.promos.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name','plataforma')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.cms.promos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getNuevo()
    {
        $categorias = [0=>'Panel Promos'] + CategoriaWeb::plataforma()->where('category_id', 0)->orderBy('orden')->get()->pluck('name','id')->toArray();

        return view('manage.cms.promos.new', compact('categorias'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getUpdate($id)
    {
        $ficha = $this->promo->find($id);

        $categorias = [0=>'Panel Promos'] + CategoriaWeb::plataforma()->where('category_id', 0)->orderBy('orden')->get()->pluck('name','id')->toArray();

        return view('manage.cms.promos.ficha', compact('ficha','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name'      => 'required',
        ]);

        $data = $request->except('_token','panel_imagen','propietario_check');

        $data['propietario'] = $request->has('propietario')?$request->input('propietario'):0;
        if( $request->has('propietario_check') )
        {
            $data['propietario'] = ConfigHelper::config('propietario');
        }

        $data['activo'] = $request->has('activo') ? $request->input('activo') : 0;
        $data['orden'] = $request->get('orden') ?: 0;
        $data['promo_todas'] = $request->has('promo_todas') ? $request->input('promo_todas') : 0;
        $data['targetblank_panel'] = $request->has('targetblank_panel') ? $request->input('targetblank_panel') : 0;

        if(!$id)
        {
            $p = $this->promo->create($data);
            $id = $p->id;
        }
        else
        {
            $this->promo->update($data,$id);
        }

        if( $request->hasFile('panel_imagen') )
        {
            $file = $request->file('panel_imagen');

            $ficha = $this->promo->find($id);

            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file_name = str_slug($file_name) .".". $file->getClientOriginalExtension();
            $dirp = "assets/uploads/cms/promos/". $id . "/";
            $dir = public_path($dirp);

            $file->move($dir, $file_name);

            $img = Image::make($dir.$file_name);
            $img->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            //thumb
            if (!file_exists($dir."thumb/"))
            {
                File::makeDirectory($dir."thumb/", 0775, true);
            }
            File::copy($dir.$file, $dir."thumb/".$file);
            $img2 = Image::make($dir."thumb/".$file);
            $img2->resize(450, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            $ficha->panel_imagen = "/".$dirp.$file_name;
            $ficha->save();
        }

        return redirect()->route('manage.cms.promos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->promo->delete($id);
        return redirect()->route('manage.cms.promos.index');
    }
}
