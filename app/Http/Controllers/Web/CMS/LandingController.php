<?php

namespace VCN\Http\Controllers\Web\CMS;

use Illuminate\Http\Request;
use VCN\Http\Controllers\Controller;

use VCN\Models\CMS\{Landing, LandingTestimonio, LandingDestino, LandingPrograma, LandingForm};
use VCN\Models\System\Plataforma;
use VCN\Models\User;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Carbon;

class LandingController extends Controller
{
    /**
     * Instantiate a new PaginaController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->checkPermisos('cms');
    }

    public function getIndex()
    {
        if (Datatable::shouldHandle()) {
            $filtro = ConfigHelper::config('propietario');
            if ($filtro && !auth()->user()->isFullAdmin()) {
                $col = Landing::where('plataforma_id', 0)->orWhere('plataforma_id', $filtro)->get();
            } else {
                $col = Landing::all();
            }

            return Datatable::collection($col)
                ->addColumn('name', function ($model) {
                    return "<a href='" . route('manage.cms.landings.ficha', [$model->id]) . "'>$model->name</a>";
                })
                ->addColumn('slug', function ($model) {
                    return "<a target='_blank' href='" . route('web.landing', [$model->slug]) . "'>$model->slug</a>";
                })
                ->addColumn('plataforma', function ($model) {
                    return $model->plataforma ? $model->plataforma->name : "-";
                })
                ->addColumn('activo', function ($model) {
                    return $model->activo ? "<span class='badge badge-help'>SI</span>" : "<span class='badge'>NO</span>";
                })
                ->addColumn('tema', function ($model) {
                    return $model->tema_name;
                })
                ->addColumn('general', function ($model) {
                    return $model->es_general ? "<span class='badge badge-help'>SI</span>" : "<span class='badge'>NO</span>";
                })
                ->addColumn('form', function ($model) {
                    return $model->es_form ? "<span class='badge badge-help'>SI</span>" : "<span class='badge'>NO</span>";
                })
                ->addColumn('informe', function ($model) {
                    return $model->forms->count();
                })
                ->addColumn('options', function ($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Landing' data-action='" . route('manage.cms.landings.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name')
                ->orderColumns('name')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }

        return view('manage.cms.landings.index');
    }

    public function getInforme(Request $request, Landing $landing)
    {
        if (Datatable::shouldHandle()) {
            $filtro = ConfigHelper::config('propietario');
            $col = $landing->forms;

            return Datatable::collection($col)
                ->addColumn('recibido', function ($model) {
                    return $model->created_at->format('Y-m-d H:i');
                })
                ->showColumns('nombre', 'telefono', 'email', 'tutor_nombre', 'tutor_telefono', 'destino', 'duracion')
                ->addColumn('es_viajero', function ($model) {
                    return $model->es_viajero ? "SI" : "NO";
                })
                ->addColumn('origen', function ($model) {
                    return $model->es_catalogo ? "Catálogo" : "Landing";
                })
                ->addColumn('fechanac', function ($model) {
                    return $model->fechanac ? Carbon::parse($model->fechanac)->format('d/m/Y') : "-";
                })
                ->addColumn('utm', function ($model) {
                    return $model->utm;
                })
                ->addColumn('options', function ($model) {

                    $ret = "";
                    $ret = "<a href='" . route('manage.cms.landings.informe.form', [$model->id]) . "'>Ver</a>";

                    // $data = " data-label='Borrar' data-model='LandingForm' data-action='". route( 'manage.cms.landings.delete', $model->id) . "'";
                    // $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('nombre', 'destino', 'duracion')
                ->orderColumns('nombre', 'recibido', 'destino', 'duracion')
                ->setAliasMapping()
                // ->setSearchStrip()->setOrderStrip()
                ->make();
        }
    }

    public function getInformeForm(Request $request, LandingForm $form)
    {
        $ficha = $form;
        return view('manage.cms.landings.formulario')->with(compact('ficha'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getNuevo()
    {
        $parametros = (object) [
            'testimonios' => LandingTestimonio::pluck('name', 'id')->toArray(),
            'destinos' => LandingDestino::pluck('name', 'id')->toArray(),
            'programas' => LandingPrograma::pluck('name', 'id')->toArray(),
            'asignados' => User::asignados()->get()->pluck('full_name', 'id')->toArray(),
            'catalogos' => [0=> ''] + \VCN\Models\CMS\Catalogo::activos()->pluck('name', 'id')->toArray()
        ];

        return view('manage.cms.landings.new')->with(compact('parametros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getUpdate($id)
    {
        $ficha = Landing::find($id);

        $tids = [];
        $testimoniosSort = [];
        if ($ficha->testimonios) {
            $tids = $ficha->testimonios_list->pluck('id')->toArray();
            $testimoniosSort = $ficha->testimonios_list->pluck('name', 'id')->toArray();
        }
        $testimoniosSort += LandingTestimonio::whereNotIn('id', $tids)->pluck('name', 'id')->toArray();

        $dids = [];
        $destinosSort = [];
        if ($ficha->destinos) {
            $dids = $ficha->destinos_list->pluck('id')->toArray();
            $destinosSort = $ficha->destinos_list->pluck('name', 'id')->toArray();
        }
        $destinosSort += LandingDestino::whereNotIn('id', $dids)->pluck('name', 'id')->toArray();

        $pids = [];
        $programasSort = [];
        if ($ficha->programas) {
            $pids = $ficha->programas_list->pluck('id')->toArray();
            $programasSort = $ficha->programas_list->pluck('name', 'id')->toArray();
        }
        $programasSort += LandingPrograma::whereNotIn('id', $pids)->pluck('name', 'id')->toArray();

        $parametros = (object) [
            'testimonios' => $testimoniosSort,
            'destinos' => $destinosSort,
            'programas' => $programasSort,
            'asignados' => User::asignados()->get()->pluck('full_name', 'id')->toArray(),
            'catalogos' => [0=> ''] + \VCN\Models\CMS\Catalogo::activos()->pluck('name', 'id')->toArray()
        ];

        return view('manage.cms.landings.ficha', compact('ficha', 'parametros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function postUpdate(Request $request, $id = 0)
    {
        $this->validate($request, [
            'name'      => 'required',
            'slug'    => 'required',
            'title'    => 'required'
        ]);

        $data = $request->except('plataforma_id_check', 'destinos_sortable_value', 'programas_sortable_value', 'testimonios_sortable_value');
        $data['name'] = $request->get('name');
        //$data['activo'] = $request->has('activo');

        $arrV = $request->get('testimonios_sortable_value');
        if ($arrV) {
            $arrV = explode(',', $arrV);
            $data['testimonios'] = $arrV;
        }

        $arrV = $request->get('destinos_sortable_value');
        if ($arrV) {
            $arrV = explode(',', $arrV);
            $data['destinos'] = $arrV;
        }

        $arrV = $request->get('programas_sortable_value');
        if ($arrV) {
            $arrV = explode(',', $arrV);
            $data['programas'] = $arrV;
        }

        // dd($data);

        $p = $request->has('plataforma_id') ? $request->input('plataforma_id') : 0;
        if ($request->has('plataforma_id_check')) {
            $p = ConfigHelper::config('propietario');
        }
        $data['plataforma_id'] = $p;

        if (!$id) {
            $ficha = Landing::create($data);
        } else {
            $ficha = Landing::find($id);
            $ficha->update($data);
        }

        $iimg = "imagen";
        if($request->hasFile($iimg))
        {
            $file = $request->file($iimg);
            $dirp = "assets/uploads/landings/". $id . "/";

            $ficha->$iimg = ConfigHelper::uploadFoto($file, $dirp);
            $ficha->save();
        }

        return redirect()->route('manage.cms.landings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $ficha = Landing::find($id);
        $ficha->delete();

        return redirect()->route('manage.cms.landings.index');
    }
}
