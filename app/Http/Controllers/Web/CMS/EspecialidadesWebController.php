<?php

namespace VCN\Http\Controllers\Web\CMS;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Repositories\CMS\EspecialidadWebRepository as EspecialidadWeb;
use VCN\Models\Especialidad;
use VCN\Models\Subespecialidad;

use VCN\Helpers\ConfigHelper;

use Datatable;
use Image;
use File;

class EspecialidadesWebController extends Controller
{
    private $especialidad;

    /**
     * Instantiate a new CategoriasWebController instance.
     *
     * @return void
     */
    public function __construct( EspecialidadWeb $especialidad )
    {
        $this->checkPermisos('cms');
        $this->especialidad = $especialidad;

    }

    public function getIndex()
    {
        if(Datatable::shouldHandle())
        {

            $col = $this->especialidad->all();

            $filtro = ConfigHelper::config('propietario');
            if( $filtro && !auth()->user()->isFullAdmin() )
            {
                $col1 = $this->especialidad->findWhere(['propietario'=> $filtro]);
                $col = $this->especialidad->findWhere(['propietario'=> 0]);

                $col = $col->merge($col1);
            }

            return Datatable::collection( $col->sortBy('orden') )
                ->showColumns('orden')
                ->addColumn('name', function($model) {
                    return "<a href='". route('manage.cms.especialidades.ficha',[$model->id]) ."'>$model->name</a>";
                })
                ->addColumn('padre', function($model) {
                    if($model->padre)
                    {
                        return "<a href='". route('manage.cms.especialidades.ficha',[$model->especialidad_id]) ."'>". $model->padre->name ."</a>";
                    }

                    return "-";
                })
                ->addColumn('nivel', function($model) {
                    return $model->nivel;
                })
                ->showColumns('titulo','idioma')
                ->addColumn('plataforma', function($model) {
                    return ConfigHelper::plataforma($model->propietario);
                })
                ->addColumn('tipo_enlace', function($model) {
                    return $model->es_link?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('activo', function($model) {
                    return $model->activo?"<span class='badge badge-help'>SI</span>":"<span class='badge'>NO</span>";
                })
                ->addColumn('options', function($model) {

                    $ret = "";
                    $data = " data-label='Borrar' data-model='Especialidad Web' data-action='". route( 'manage.cms.especialidades.delete', $model->id) . "'";
                    $ret .= " <a href='#destroy' $data data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>";

                    return $ret;
                })
                ->searchColumns('name','titulo')
                ->orderColumns('orden','nivel','name','titulo','idioma','plataforma')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }
        return view('manage.cms.especialidades.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getNuevo(Request $request)
    {
        $especialidades = Especialidad::all()->sortBy('name')->pluck('name','id')->toArray();
        foreach($especialidades as $kc => $vc){
            $subespecialidades[$vc] = Subespecialidad::where('especialidad_id',$kc)->orderBy('name')->pluck('name','id')->toArray();
        }

        if( auth()->user()->isFullAdmin() )
        {
            $plataformas = ConfigHelper::plataformas();
            $plataforma = $request->input('plataformas', 0);
        }
        else
        {
            $plataforma = ConfigHelper::propietario();
            $plataformas = [ 0=>ConfigHelper::plataformaApp() ];
        }

        $padres = [0=> 'Raíz'] + $this->especialidad->pluck('name','id');

        return view('manage.cms.especialidades.new', compact('ficha', 'plataformas', 'especialidades', 'subespecialidades', 'plataforma','padres'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getUpdate(Request $request, $id)
    {
        $ficha = $this->especialidad->find($id);

        $valores = [];

        $especialidades = Especialidad::all()->sortBy('name')->pluck('name','id')->toArray();
        foreach($especialidades as $kc => $vc){
            $subespecialidades[$vc] = Subespecialidad::where('especialidad_id',$kc)->orderBy('name')->pluck('name','id')->toArray();
        }

        if( auth()->user()->isFullAdmin() )
        {
            $plataformas = ConfigHelper::plataformas();
            $plataforma = $request->input('plataformas', 0);
        }
        else
        {
            $plataforma = ConfigHelper::propietario();
            $plataformas = [ 0=>ConfigHelper::plataformaApp() ];
        }

        $padres = [0=> 'Raíz'] + $this->especialidad->pluck('name','id');

        return view('manage.cms.especialidades.ficha', compact('ficha', 'plataformas', 'especialidades', 'subespecialidades', 'plataforma', 'padres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function postUpdate(Request $request, $id=0)
    {
        $this->validate($request, [
            'name'      => 'required',
            'idioma'    => 'required'
        ]);

        $data = $request->except('_token');

        $data['propietario'] = $request->has('propietario')?$request->input('propietario'):0;
        if( $request->has('propietario_check') )
        {
            $data['propietario'] = ConfigHelper::config('propietario');
        }
        //dd($data);
        $data = $request->except('_token');

        //especialidades, subespecialidades : implode
        $data['idioma'] = isset($data['idioma'])?implode(',', $data['idioma']):null;

        if(isset($data['especialidades']))
        {
            !is_array($data['especialidades']) ? $data['especialidades'] = str_split($data['especialidades']) : $data['especialidades'] = $data['especialidades'];
            $data['especialidades'] = isset($data['especialidades']) ? implode(',', $data['especialidades']) : null;
            if(isset($data['subespecialidades'])) {
                !is_array($data['subespecialidades']) ? $data['subespecialidades'] = str_split($data['subespecialidades']) : $data['subespecialidades'] = $data['subespecialidades'];
                $data['subespecialidades'] = isset($data['subespecialidades'])?implode(',', $data['subespecialidades']):null;
            }
        }

        $data['activo'] = $request->has('activo');
        $data['cerrado'] = $request->has('cerrado');
        $data['es_link'] = $request->has('es_link');
        $data['link_blank'] = $request->has('link_blank');

        if(!$id)
        {
            $ficha = $this->especialidad->create($data);
            $id = $ficha->id;
        }
        else
        {
            $this->especialidad->update($data,$id);

            $ficha = $this->especialidad->find($id);
        }

        //foto
        if ($request->hasFile('imagen'))
        {
            $file = $request->file('imagen');

            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file_name = str_slug($file_name) .".". $file->getClientOriginalExtension();
            $dirp = "assets/uploads/cms/especialidades/". $id . "/";
            $dir = public_path($dirp);

            $file->move($dir, $file_name);

            $img = Image::make($dir.$file_name);
            $img->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            //thumb
            if (!file_exists($dir."thumb/"))
            {
                File::makeDirectory($dir."thumb/", 0775, true);
            }
            File::copy($dir.$file_name, $dir."thumb/".$file_name);
            $img2 = Image::make($dir."thumb/".$file_name);
            $img2->resize(450, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save();

            $ficha->imagen = "/".$dirp.$file_name;
            $ficha->save();
        }

        return redirect()->route('manage.cms.especialidades.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->especialidad->delete($id);
        return redirect()->route('manage.cms.especialidades.index');
    }

}
