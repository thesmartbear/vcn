<?php

namespace VCN\Http\Controllers\Web;

use Illuminate\Http\Request;
use VCN\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use VCN\Models\User;
use VCN\Models\Chat\ChatSoporte;
use Carbon;
use Chat;
use Session;

use Datatable;
use VCN\Helpers\MailHelper;

class ChatController extends Controller
{
    public function test(Request $request, $chatId=0)
    {
        $user = $request->user();

        $cid = $chatId ?: $user->chat_id;
        $chat = ChatSoporte::find($cid);

        Chat::message('Test '. time())
            ->from($user)
            ->to($chat)
            ->send();

        dd($chat->mensajes);
    }

    public function create(Request $request)
    {
        $user = $request->user();

        if($user && $user->es_crm_admin)
        {
            $res = [
                'userId'      => 0,
                'chatId'    => 0,
                'chatStatus' => 0,
                'chatTxt'   => 'Es un usuario CRM. NO Chat'
            ];
            return response($res);
        }

        $disponibles = User::getChatCrmDisponibles();
        $chat = null;
        $status = 0;
        
        if($disponibles->count())
        {
            $status = 2;
            
            if(!$user)
            {
                //Visitante
                $user = User::getFreeForChat();
                Auth::login($user);
            }
            
            $chat = $user->chat;
            if(!$chat)
            {
                $participants = [ $user->id ];
                $chat = Chat::createConversation($participants);//->makePrivate();

                $user->chat_id = $chat->id;
                $user->chat_last_activity = Carbon::now()->getTimestamp();
                $user->save();

                $chat = $user->chat;

                $chatData['status'] = $status;
                $chatData['cliente_id'] = $user->id;
                $chat->update(['data' => $chatData]);

                $chat->updateStatus($status);
                ChatSoporte::notificarNuevo($chat);
            }
        }
        else
        {
            $status = 20;
        }

        $data = [
            'userId'      => $user ? $user->id : 0,
            'chatId'    => $chat ? $chat->id : 0,
            'chatStatus' => $status,
            'chatTxt'   => $chat ? $chat->status_text : "No hay agentes disponibles"
        ];

        return response($data);
    }

    public function checkStatus(Request $request)
    {
        $user = $request->user();
        $chatId = $request->get('chatId');

        $chat = ChatSoporte::find($chatId);
        if(!$chat)
        {
            $res = [
                'chatId'    => 0,
                'chatStatus' => 30
            ];
            return response($res);
        }

        $st = $request->get('status', null);
        $chat->updateStatus($st);

        $res = [
            'userId'      => $user ? $user->id : 0,
            'chatId'    => $chatId,
            'chatStatus' => $chat->status,
            'chatTxt'   => $chat->status_text,
            'mensajes' => $chat->mensajes_nuevos
        ];

        return response($res);
    }

    public function getIndex(Request $request, $user_id=0)
    {
        $user = $user_id ? User::find($user_id) : $request->user();
        
        if(Datatable::shouldHandle())
        {
            if(!$user_id)
            {
                if($user->chat_admin)
                {
                    $chats = $user->chats;    
                }
                else
                {
                    $chatsEsperando = ChatSoporte::where('data','LIKE','%"status":2,%')->get();
                    $chats = $user->chats->merge($chatsEsperando);
                }
            }
            else
            {
                $chats = $user->chats;
            }
            
    
            return Datatable::collection( $chats )
                ->showColumns('id')
                ->addColumn('fecha', function($model) {
                    return $model->created_at;
                })
                ->addColumn('cliente', function($model) {
                    return $model->cliente->username;
                })
                ->addColumn('status', function($model) {
                    return $model->status_name;
                })
                ->addColumn('options', function($model) {
                    
                    $r = route('manage.chat.agente', $model->id);

                    if($model->status == 2)
                    {
                        return "<a target='_blank' href='$r'>Atender</a>";
                    }

                    if($model->status == 1)
                    {
                        return "<a target='_blank' href='$r'>Abrir</a>";
                    }

                    return "<a href='$r'>Ver</a>";

                })
                ->searchColumns('id')
                ->orderColumns('id')
                ->setSearchStrip()->setOrderStrip()
                ->setAliasMapping()
                ->make();
        }
        
        return view('chat.index')->with(compact('user'));
    }

    public function getAgente(Request $request, int $chatId)
    {
        $user = $request->user();
        $chat = ChatSoporte::find($chatId);

        if(!$chat)
        {
            Session::flash("mensaje-alert", "El chat no existe");
            return redirect()->route('manage.index');
        }

        if($chat->agente && $chat->agente->id == $user->id)
        {
            if($chat->cliente->chat_sesion_caducada)
            {
                $chat->updateStatus(13);
                Session::flash("mensaje", "El chat está en estado: ". $chat->status_text);
                // return redirect()->route('manage.chat.index');
                return view('chat.agente')->with( compact('user','chat') );
            }

            if( $chat->status == 1)
            {
                return view('chat.agente')->with( compact('user','chat') );
            }

            Session::flash("mensaje", "El chat está en estado: ". $chat->status_text);
            // return redirect()->route('manage.chat.index');
            return view('chat.agente')->with( compact('user','chat') );
        }

        $chatData = $chat->data;
        $chatData['agente_id'] = $user->id;
        $chat->update(['data' => $chatData]);

        Chat::conversation($chat)->addParticipants($user);
        $chat->updateStatus(1);

        return view('chat.agente')->with( compact('user','chat') );
        
    }

    public function postMensaje(Request $request)
    {
        $user = $request->user();
        $chatId = $request->get('chatId');

        $chat = ChatSoporte::find($chatId);
        if(!$chat)
        {
            $res = [
                'chatId'    => 0,
                'chatStatus' => 30
            ];
            return response($res);
        }

        if($user)
        {
            $user->chat_last_activity = Carbon::now()->getTimestamp();
            $user->save();
        }

        $mensaje = $request->get('mensaje');
        Chat::message($mensaje)
            ->from($user)
            ->to($chat)
            ->send();

        $res = [
            'userId'    => $user ? $user->id : 0,
            'chatId'    => $chatId,
            'mensajes' => $chat->mensajes_nuevos
        ];

        return response($res);
    }

    public function postFormulario( Request $request )
    {
        $data = [];
        $data['oficina'] = 0;
        $data['name'] = $request->get('nombre');
        $data['email'] = $request->get('email');
        $data['message'] = "Chat sin agentes";

        MailHelper::mailContacto($data);

        $res = [
            'result' => true,
        ];
        return response($res);
    }
}
