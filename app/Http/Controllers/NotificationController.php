<?php

namespace VCN\Http\Controllers;

use Illuminate\Http\Request;
use VCN\Events\NotificationRead;
use VCN\Events\NotificationReadAll;
use NotificationChannels\WebPush\PushSubscription;

use Datatable;
use Carbon;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('last', 'dismiss');
    }

    /**
     * Get user's notifications.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // if (!$request->ajax()) {
        //     abort(404);
        // }

        $user = $request->user();

        // Limit the number of returned notifications, or return all
        $query = $user->unreadNotifications();
        $limit = (int) $request->input('limit', 0);
        if ($limit) {
            $query = $query->limit($limit);
        }

        $notifications = $query->get()->each(function ($n) {
            $n->created = $n->created_at->toIso8601String();
        });

        $total = $user->unreadNotifications->count();

        return compact('notifications', 'total');
    }

    /**
     * Get user's notifications.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getListado(Request $request)
    {
        $user = $request->user();

        $query = $user->notifications();

        $notifications = $query->orderBy('created_at','DESC')->get()->each(function ($n) {
            $n->created = $n->created_at->toIso8601String();
        });

        if(Datatable::shouldHandle())
        {
            return Datatable::collection( $notifications )
                ->addColumn('nombre', function ($model) {
                    return $model->data['title'];
                })
                ->addColumn('texto', function ($model) {
                    return $model->data['body'];
                })
                ->addColumn('link', function ($model) {
                    $t = "Ver";
                    if(!$model->data['action_url'])
                    {
                        return $t;
                    }
                    return "<a target='_blank' href='". $model->data['action_url'] ."''>$t</a>";
                })
                ->addColumn('fecha_in', function ($model) {
                    return $model->created_at->format('d/m/Y - H:i');
                })
                ->addColumn('read_at', function ($model) {
                    $ret = $model->read_at ? $model->read_at->format('d/m/Y - H:i') : "NO Leído";
                    return $ret;
                })
                ->addColumn('action', function ($model) {
                    return "";
                })
                ->searchColumns('nombre')
                ->orderColumns('nombre','texto')
                ->setAliasMapping()
                ->setSearchStrip()->setOrderStrip()
                ->make();
        }

        // dd($notifications);

        return view('manage.notifications.list', compact('notifications'));
    }

    /**
     * Create a new notification.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // foreach(User::all() as $u)
        // {
        //     $u->notify(new HelloNotification);
        // }

        // $request->user()->notify(new TestNotification);

        return response()->json('Notification sent.', 201);
    }

    /**
     * Mark user's notification as read.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function markAsRead(Request $request, $id)
    {
        $notification = $request->user()
                                ->unreadNotifications()
                                ->where('id', $id)
                                ->first();

        if (is_null($notification)) {
            return response()->json('Notification not found.', 404);
        }

        $notification->markAsRead();

        event(new NotificationRead($request->user()->id, $id));
    }

    /**
     * Mark all user's notifications as read.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function markAllRead(Request $request)
    {
        $request->user()
                ->unreadNotifications()
                ->get()->each(function ($n) {
                    $n->markAsRead();
                });

        event(new NotificationReadAll($request->user()->id));
    }

    /**
     * Mark the notification as read and dismiss it from other devices.
     *
     * This method will be accessed by the service worker
     * so the user is not authenticated and it requires an endpoint.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function dismiss(Request $request, $id)
    {
        if (empty($request->endpoint)) {
            return response()->json('Endpoint missing.', 403);
        }

        $subscription = PushSubscription::findByEndpoint($request->endpoint);
        if (is_null($subscription)) {
            return response()->json('Subscription not found.', 404);
        }

        $notification = $subscription->user->notifications()->where('id', $id)->first();
        if (is_null($notification)) {
            return response()->json('Notification not found.', 404);
        }

        $notification->markAsRead();

        event(new NotificationRead($subscription->user->id, $id));
    }
}
