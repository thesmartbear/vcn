<?php

namespace VCN\Http\Controllers\Auth;

use VCN\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

use VCN\Models\User;

use ConfigHelper;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $p = ConfigHelper::config('propietario');
        $credentials = $request->only('email');

        // $user = User::where('username',$credentials)->where(function($q) {
        //     $p = ConfigHelper::config('propietario');
        //     return $q->where('plataforma',$p)->orWhere('plataforma',0);
        // })->first();

        $user = User::where( function($q) use ($credentials) {
                return $q->where('username', $credentials)->orWhere('email', $credentials);
            })->where(function($q) {
                $p = ConfigHelper::config('propietario');
                return $q->where('plataforma',$p)->orWhere('plataforma',0);
            })->first();

        $p = $user ? $user->plataforma : $p;
        if($user && $user->plataforma)
        {
            $credentials = ['plataforma'=>$p] + $credentials;
        }

        // $p = $p ?: ConfigHelper::config('propietario');
        // $plataforma = \VCN\Models\System\Plataforma::find($p);

        // $response = $this->broker()->sendResetLink($credentials, function (Message $message) use ($plataforma) {
        //     $message->from($plataforma->email, $plataforma->name);
        //     $message->subject($this->getEmailSubject());
        // });

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        //switch ($response) {
        //    case Password::RESET_LINK_SENT:
        //        return redirect()->back()->with('status', trans($response));
        //
        //    case Password::INVALID_USER:
        //        return redirect()->back()->withErrors(['email' => trans($response)]);
        //}

        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($request, $response)
                    : $this->sendResetLinkFailedResponse($request, $response);
    }

    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : 'Recuperación de contraseña';
    }
}
