<?php

namespace VCN\Http\Controllers\Area;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingPago;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Cursos\Curso;
use VCN\Models\User;
use VCN\Models\UserDeseo;
use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\ViajeroDatos;
use VCN\Models\Leads\ViajeroTutor;
use VCN\Models\Leads\TutorLog;
use VCN\Models\Leads\ViajeroLog;
use VCN\Models\Leads\Tutor;
use VCN\Models\System\Plataforma;
use VCN\Models\Categoria;
use VCN\Models\Subcategoria;

use VCN\Models\Convocatorias\Cerrada;
use VCN\Models\Convocatorias\Abierta;
use VCN\Models\Convocatorias\ConvocatoriaMulti;
use VCN\Models\Descuentos\DescuentoTipo;

use VCN\Helpers\MailHelper;

use ConfigHelper;
use Session;
use Carbon;
use Log;
use Ssheduardo\Redsys\Facades\Redsys;
use App;

class ComprasController extends Controller
{
    public function __construct(Request $request)
    {
        $user = ConfigHelper::usuario();
        view()->share('usuario', $user);

        // $locale = $user->ficha?$user->ficha->idioma_contacto:"es";
        // App::setLocale($locale);
    }

    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $email = $request->get('email');
        $curso_id = (int)$request->get('compra');
        
        $convo_id = (int)$request->get('compra-corporativo',0);
        Session::flash('vcn.compra-corporativo',$convo_id);

        $deseo = $request->get('deseo');
        Session::flash('vcn.compra-deseo',$deseo);

        $plataforma = ConfigHelper::config('propietario');
        $ficha = null;

        $user = User::where('plataforma',$plataforma)->where('username',$email)->first();
        if(!$user)
        {
            //No hay user
            $viajero = Viajero::where('plataforma',$plataforma)->where('email',$email)->first();
            $tutor = Tutor::where('plataforma',$plataforma)->where('email',$email)->first();

            Session::flash('vcn.compra-viajero', $viajero?true:false);
            Session::flash('vcn.compra-tutor', $tutor?true:false);

            if(!$viajero && !$tutor)
            {
                //Registro
                Session::flash('vcn.compra-email',$email);
                Session::flash('vcn.compra-login','registro'); //=> Registro
                return redirect()->back();
            }
            else
            {
                // Session::flash('vcn.compra-email',$email);
                // Session::flash('vcn.compra-login','cuenta'); //=> Viajero / Tutor pero no user
                // return redirect()->back();

                //Activamos los usuarios:
                if($tutor)
                {
                    $tutor->setUsuario(true,true);
                    $user = $tutor->user;
                }
                elseif($viajero)
                {
                    $viajero->setUsuarios(true,true);
                    $user = $viajero->user;
                }

                //Volvemos al caso 1 (login)
                Session::flash('vcn.compra-activacion',1);
            }

        }

        //hay user

        //User + Area => Login
        if($user->roleid==11 || $user->roleid==12)
        {
            if(!$deseo)
            {
                //Asociamos la compra al user
                $compra = array('curso_id'=> $request->get('compra'), 'convo_id'=> $convo_id, 'booking_id'=> 0);
                $user->compra = $compra;
                $user->save();
            }
            else
            {
                UserDeseo::add($request->get('compra'));
            }

            if($user->roleid==11)
            {
                Session::flash('vcn.compra-ficha','viajero');
            }

            if($user->roleid==12)
            {
                Session::flash('vcn.compra-ficha','tutor');

                //Tiene q Elegir hijos o crear nuevo
            }

            Session::flash('vcn.compra-email',$email);
            Session::flash('vcn.compra-login','login'); //User y Area => login
            return redirect()->back();
        }

        //default
        return redirect()->back();

    }

    public function postUser(Request $request)
    {
        // $this->validate($request, ['username' => 'required|email']);

        $login = $request->get('compra-login');
        $email = $request->get('username');

        $deseo = $request->get('deseo');

        $plataforma = ConfigHelper::config('propietario');

        switch($login)
        {
            case 'login':
            {
                //login

            }
            break;

            case 'registro':
            {
                //registro
                //nombre, apellido, movil

                Session::flash('vcn.compra-email',$email);
                Session::flash('vcn.compra-login','registro'); //=> Registro por si falla el validate
                $this->validate($request, [
                    'viajero'=> 'required',
                    'nombre' => 'required',
                    'apellido' => 'required',
                    'movil' => 'required'
                ]);

                $viajero = (int)$request->get('viajero');
                if($viajero==1)
                {
                    //Viajero
                    $ficha = new Viajero;
                    $ficha->plataforma = $plataforma;
                    $ficha->email = $email;
                    $ficha->name = $request->get('nombre');
                    $ficha->lastname = $request->get('apellido');
                    $ficha->movil = $request->get('movil');
                    $ficha->save();

                    //Cuenta
                    $viajero = Viajero::where('plataforma',$plataforma)->where('email',$email)->first();
                    $viajero->setUsuarios(true,true);
                    $user = $viajero->user;
                }
                elseif($viajero==2)
                {
                    //tutor
                    //Tutor
                    $ficha = new Tutor;
                    $ficha->plataforma = $plataforma;
                    $ficha->email = $email;
                    $ficha->name = $request->get('nombre');
                    $ficha->lastname = $request->get('apellido');
                    $ficha->movil = $request->get('movil');
                    $ficha->save();

                    TutorLog::addLog($ficha, "Nuevo Tutor [ONLINE]");

                    //Cuenta
                    $tutor = Tutor::where('plataforma',$plataforma)->where('email',$email)->first();
                    $tutor->setUsuario(true,true);
                    $user = $tutor->user;
                }

                if(!$deseo)
                {
                    //asociamos la compra
                    $compra = array('curso_id'=> $request->get('compra'), 'convo_id'=> $request->get('compra-corporativo'), 'booking_id'=> 0);
                    $user->compra = $compra;
                    $user->save();
                }
                else
                {
                    UserDeseo::add($request->get('compra'));
                }

                //Volvemos al caso 1 (login)
                Session::flash('vcn.compra-email',$email);
                Session::flash('vcn.compra-login','login'); //=> Login
                Session::flash('vcn.compra-activacion',1);
                Session::flash('vcn.compra-deseo',$deseo);
                return redirect()->back();
            }
            break;
        }
    }

    public function setViajero(Request $request, $booking_id, $viajero_id)
    {
        $user = ConfigHelper::usuario();

        // if(!$user->compra)
        // {
        //     // dd("NO HAY COMPRA");
        //     return redirect()->route('area.index');
        // }

        // $booking_id = $user->compra['booking_id'];
        $booking = Booking::find($booking_id);
        if(!$booking)
        {
            return redirect()->route('area.index');   
        }
        
        $booking->setViajero($viajero_id);

        return redirect()->route('area.comprar.booking', $booking->id);
    }


    /**
     * @param Request $request
     * @param null $curso_id
     * @param int $convo_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getCompra(Request $request, $curso_id=null, $convo_id=0)
    {
        $user = ConfigHelper::usuario();

        //curso_id tiene si esta identificado en el front-end cuando le da a comprar, si es null es desde el area

        if(!$user)
        {
            // Session::flash('msj-error',"Error compra - user");
            return redirect()->back();
        }

        if(!$user->ficha)
        {
            // Session::flash('msj-error',"Error compra - user");
            return redirect()->back();
        }

        $booking = null;
        $booking_id = null;

        $esBookingMulti = (bool) Session::get('vcn.booking_multi', false);

        Session::forget('vcn.booking_corporativo');
        Session::forget('vcn.booking_multi');

        if(!$curso_id)
        {
            if($user->compra)
            {
                $curso_id = $user->compra['curso_id'];
                $booking_id = $user->compra['booking_id'];
                $convo_id = isset($user->compra['convo_id']) ? $user->compra['convo_id'] : 0;

                $booking = Booking::find($booking_id);
                if($booking && $booking->es_terminado)
                {
                    $booking->onlineFin();
                    return redirect()->route('area.index');
                }
            }
            else
            {
                dd("Error compra - curso_id");
            }
        }
        /*else
        {
            if($user->compra)
            {
                //HACE OTRA COMPRA??

                // $curso_id = $user->compra['curso_id'];
                // $booking_id = $user->compra['booking_id'];
                $compra = array('curso_id'=> $curso_id, 'convo_id'=> $convo_id, 'booking_id'=> 0);
                $user->compra = $compra;
                $user->save();
            }
        }*/



        //corporativo https://vcn.loc/area/online-booking/8/1476
        $esCorporativo = false;
        if($curso_id && $convo_id && !$esBookingMulti)
        {
            $booking_id = isset($user->compra['booking_id']) ? $user->compra['booking_id'] : 0;
            $esCorporativo = true;
        }

        if(!$booking_id)
        {
            $viajero = null;
            $tutor_id = 0;
            if($user->es_viajero)
            {
                $viajero = $user->viajero;
            }
            elseif($user->es_tutor)
            {
                $tutor_id = $user->tutor->id;

                TutorLog::addLog($user->tutor, "Nuevo Booking [ONLINE]");
            }

            $booking = Booking::online_crear($curso_id,$viajero, $tutor_id, $convo_id);
            $booking_id = $booking->id;

            $compra = array('curso_id'=> $curso_id, 'convo_id'=> $convo_id, 'booking_id'=> $booking_id);
            $user->compra = $compra;
            $user->save();

            if($esCorporativo)
            {
                if( $booking->setFechasConvo($convo_id) )
                {
                    return redirect()->route('area.comprar.booking', $booking->id);
                }
            }
        }

        if(!$booking)
        {
            $compra = null;
            $user->compra = $compra;
            $user->save();

            return redirect()->route('area.index');
        }

        // if(!$booking->fase)
        // {
        //     $booking->fase = 1;
        //     $booking->save();
        // }

        return redirect()->route('area.comprar.booking');
        // return view('comprar.booking', compact('booking'));
        // return view('comprar.test', compact('booking'));
    }

    public function getUpdate(Request $request, $booking_id=null)
    {
        if($booking_id)
        {
            $this->middleware("permiso.booking:$booking_id");
        }

        $user = $request->user();

        if(!$user->compra)
        {
            // dd("NO HAY COMPRA");
            // return redirect()->route('area.index');
        }

        //Para cuando viene del pago
        if(!$booking_id)
        {
            $compra = $user->compra;
            $curso_id = $user->compra['curso_id'];
            $booking_id = $user->compra['booking_id'];
        }

        //Controlar q no se metan donde no deben
        if($booking_id)
        {
            $booking = Booking::find($booking_id);
            if(!$booking)
            {
                $compra = null;
                $user->compra = $compra;
                $user->save();

                return redirect()->route('area.index');
            }

            $booking->updateDatos();

            if($booking->status_id)
            {
                $booking->onlineFin();
                return redirect()->route('area.index');
            }

            if(!$booking->es_online)
            {
                return redirect()->route('area.index');
            }

            if($user->es_viajero)
            {
                if($booking->viajero_id != $user->ficha->id)
                {
                    return redirect()->route('area.index');
                }
            }
            elseif($user->es_tutor && $booking->tutor_id)
            {
                if($booking->tutor_id != $user->ficha->id)
                {
                    // return redirect()->route('area.index');
                }
            }

            $online = $booking->online;
            if(!isset($online['ip']))
            {
                $ip = $request->ip();
                $online = [
                    'ip' => $ip
                ];

                $booking->online = $online;
                $booking->save();
            }
        }


        $booking = Booking::find($booking_id);
        if(!$booking)
        {
            //Esto pasa cuando ha recuperado contraseña y entonces se queda loqueado
            if($curso_id)
            {
                $viajero = null;
                $tutor_id = null;
                if($user->es_viajero)
                {
                    $viajero = $user->viajero;
                }
                elseif($user->es_tutor)
                {
                    $tutor_id = $user->tutor->id;
                }
                $booking = Booking::online_crear($curso_id,$viajero, $tutor_id);
                $booking_id = $booking->id;

                $compra = array('curso_id'=> $curso_id, 'booking_id'=> $booking_id);
                $user->compra = $compra;
                $user->save();
            }
            else
            {
                // Borramos la compra del user
                $user->compra = null;
                $user->save();

                return redirect()->route('area.index');
            }
        }
        // elseif($booking->tpv_status)
        // {
        //     //Borramos la compra del user
        //     $user->compra = null;
        //     $user->save();
        // }

        $locale = $user->ficha?$user->ficha->idioma_contacto:"es";
        App::setLocale($locale);

        return $booking->route_getUpdate();

        // return view('comprar.booking', compact('booking'));

    }

    public function getCurso(Request $request, $curso_id=null, $booking_id=null, $es_reset=false)
    {
        $user = $request->user();

        if(!$curso_id && !$es_reset)
        {
            $categorias = [0=>'Todas'] + Categoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $subcategorias = [0=>'Todas'] + Subcategoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            // $subcategoriasdet = [0=>'Todas'] + SubcategoriaDetalle::plataforma()->sortBy('name')->pluck('name','id')->toArray();
            $cursos = [0=>'Todos'] + Curso::plataforma()->where('activo_web',1)->sortBy('name')->pluck('name','id')->toArray();

            return view('comprar.cursos', compact('cursos','categorias','subcategorias'));
        }

        if(!$booking_id)
        {
            $compra = $user->compra;
            $booking_id = $user->compra['booking_id'];
        }

        $booking = Booking::find($booking_id);

        $viajero = null;
        $tutor_id = null;
        if($user->es_viajero)
        {
            $viajero = $user->viajero;
        }
        elseif($user->es_tutor)
        {
            $tutor_id = $user->tutor->id;
        }

        if($es_reset)
        {
            if(!$booking)
            {
                return redirect()->route('area.index');
            }

            $curso_id = $booking->curso_id;
            $viajero = $booking->viajero;
        }

        if($booking)
        {
            if($booking->status_id>0)
            {
                return redirect()->route('area.index');
            }
            
            $booking->delete();
        }

        $booking = Booking::online_crear($curso_id,$viajero, $tutor_id);
        $booking_id = $booking->id;
        
        $compra = array('curso_id'=> $curso_id, 'booking_id'=> $booking_id);
        $user->compra = $compra;
        $user->save();

        return redirect()->route('area.comprar.booking');
    }

    public function getCancelar(Request $request, $booking_id=null)
    {
        $user = $request->user();

        if(!$booking_id && !$user->compra)
        {
            // dd("NO HAY COMPRA");
            return redirect()->route('area.index');
        }

        if(!$booking_id)
        {
            $compra = $user->compra;
            $booking_id = $user->compra['booking_id'];
        }

        $booking = Booking::find($booking_id);
        $viajero = $booking->viajero;
        $solicitud = $booking->solicitud;

        if($booking->status_id)
        {
            Session::flash('mensaje', trans('area.booking.cancelar_ko'));
            return redirect()->route('area.index');  
        }

        $booking->online_delete();

        // Borramos la compra del user
        $user->compra = null;
        $user->save();

        $booking->onlineFin();

        if($solicitud)
        {
            $solicitud->setStatus($solicitud->status_anterior, "Booking ONLINE cancelado");
        }

        if($viajero)
        {
            $viajero->es_cliente = false;
            $viajero->booking_id = 0;
            $viajero->booking_status_id = 0;
            $viajero->solicitud_id = $solicitud ? $solicitud->id : 0;
            $viajero->save();

            $st = $solicitud ? $solicitud->status_anterior : ConfigHelper::config('solicitud_status_lead');
            $viajero->setStatus( $st, "Booking ONLINE cancelado" );
            
        }

        Session::flash('mensaje-ok', trans('area.booking.cancelar_ok'));
        return redirect()->route('area.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate(Request $request)
    {
        $user = $request->user();

        $booking_id = $request->get('booking-id');
        
        if(!$booking_id)
        {
            if(!$user->compra)
            {
                // dd("NO HAY COMPRA");
                return redirect()->back();
            }
            else
            {
                $compra = $user->compra;
                $booking_id = $compra['booking_id'];
            }
        }

        if(!$booking_id)
        {
            return redirect()->route('area.index');
        }

        $booking = Booking::find($booking_id);
        $ficha = $booking;

        if($booking->tpv_tiempo_agotado)
        {
            Session::flash('mensaje-alert',trans('area.compra_tiempo_agotado'));
            return redirect()->back();
        }

        //fases
        $fase = $booking->fase;
        $paso = (int)$request->get('paso');

        $paso_new = 1;

        $plataforma = ConfigHelper::config('propietario');

        if($ficha->es_terminado)
        {
            BookingLog::addLog($booking, "Booking ya finalizado en backend ($paso)");
            return redirect()->route('area.index');
        }

        switch($paso)
        {
            case 1: //datos curso
            {
                if(!$booking->convocatoria)
                {
                    Session::flash('mensaje-alert', 'Debe seleccionar una convocatoria.');
                    return redirect()->back()->withInput();
                }

                $ficha->transporte_no = $request->has('transporte_no');
                $ficha->transporte_otro = $request->has('transporte_no')?0:$request->has('transporte_otro');
                $ficha->transporte_detalles = $request->has('transporte_otro')?$request->input('transporte_detalles'):"";
                $ficha->transporte_requisitos = $request->has('transporte_no')?"":$request->input('transporte_requisitos');

                //Puede venir puesto del tema aeropuertos.
                $ficha->transporte_recogida = $ficha->transporte_recogida?:($request->has('transporte_recogida_bool')?$request->input('transporte_recogida'):"");
                $ficha->save();

                if( $ficha->curso->es_convocatoria_cerrada )
                {
                    $cc = $ficha->convocatoria;//ConvocatoriaCerrada::find($ficha->convocatory_close_id);
                    if($cc->convocatory_semiopen)
                    {
                        if(!$ficha->course_start_date)
                        {
                            Session::flash('mensaje-alert', 'Debe indicar la fecha de Inicio y la duración.');
                            return redirect()->back()->withInput();
                        }

                        $this->validate($request, [
                            'course_start_date' => 'required|date_format:d/m/Y',
                        ]);
                    }

                    if($ficha->vuelos && !$ficha->es_corporativo)
                    {
                        if(!$ficha->vuelo && !$ficha->transporte_no && !$ficha->transporte_otro)
                        {
                            Session::flash('mensaje-alert', 'Es obligatorio seleccionar una opción de transporte.');
                            return redirect()->back()->withInput();
                        }
                    }

                    if( $ficha->transporte_no )//|| $ficha->transporte_otro )
                    {
                        $ficha->vuelo_id = 0; //Anulamos el vuelo
                        $ficha->save();
                    }
                }
                elseif( $ficha->curso->es_convocatoria_abierta )
                {
                    $this->validate($request, [
                        'booking-cabierta-fecha_ini' => 'required',
                        'booking-cabierta-semanas' => 'required|min:1',
                    ]);
                }

                if($request->has('booking-seguro-ajax') && !$ficha->cancelacion)
                {
                    $ficha->setCancelacion();
                }

                // $ficha->tpv_plazas = Carbon::now();
                // $ficha->save();

                $ficha->online_aeropuertos();

                $paso_new = 2;
            }
            break;

            case 2: //datos viajero
            {
                $validate = [
                    // 'idioma' => 'required|min:1',
                    'name' => 'required',
                    'lastname' => 'required',
                    'sexo' => 'required',
                    'nacionalidad' => 'required',
                    // 'movil' => 'required',
                    'fechanac' => 'required|date_format:d/m/Y',
                    'pais'=> 'required|min:1',
                    'provincia'=> 'required|min:1',
                    // 'escuela_curso'=> 'required|min:1', 
                ];

                if($ficha->es_grado_ext)
                {
                    $validate['grado_ext'] = 'required|not_in:"0"';
                }

                $this->validate($request, $validate);
                
                $bTutor = false;
                if(!$booking->viajero_id)
                {
                    //Es tutor
                    $viajero = new Viajero;
                    $viajero->plataforma = $plataforma;
                    $viajero->asign_to = ConfigHelper::config('tpv_asign_to');
                    $viajero->save();

                    $vd = new ViajeroDatos;
                    $vd->viajero_id = $viajero->id;
                    $vd->save();

                    $vt = new ViajeroTutor;
                    $vt->viajero_id = $viajero->id;
                    $vt->tutor_id = $user->ficha->id;
                    $vt->relacion = 0;
                    $vt->save();

                    $ficha->setViajero($viajero->id, $request);

                    $bTutor = true;
                }

                $ficha->updateDatosCampamento($request);

                if(!$bTutor)
                {
                    $viajero = $ficha->viajero;
                    $datos = $ficha->viajero->datos;

                    //Viajero->datos
                    $data = $request->except('_token');
                    $data['viajero_id'] = $viajero->id;
                    $data['pasaporte_emision'] = $request->get('pasaporte_emision')?Carbon::createFromFormat('d/m/Y',$request->get('pasaporte_emision'))->format('Y-m-d'):"";
                    $data['pasaporte_caduca'] = $request->get('pasaporte_caduca')?Carbon::createFromFormat('d/m/Y',$request->get('pasaporte_caduca'))->format('Y-m-d'):"";
                    $data['ingles'] = $request->get('ingles_academia_bool');
                    
                    $data['medicacion'] = $request->input('medicacion_bool') ? $request->input('medicacion'):"";
                    $data['alergias'] = $request->input('alergias_bool') ? $request->input('alergias'):"";
                    $data['tratamiento'] = $request->input('tratamiento_bool')?$request->input('tratamiento'):"";
                    $data['enfermedad'] = $request->input('enfermedad_bool')?$request->input('enfermedad'):"";
                    $data['dieta'] = $request->input('dieta_bool')?$request->input('dieta'):"";
                    $data['animales'] = $request->input('animales_bool')?$request->input('animales'):"";
                    $data['medicacion2'] = $request->input('medicacion_bool')?$request->input('medicacion2'):"";
                    $data['alergias2'] = $request->input('alergias_bool')?$request->input('alergias2'):"";
                    $data['tratamiento2'] = $request->input('tratamiento_bool')?$request->input('tratamiento2'):"";
                    $data['enfermedad2'] = $request->input('enfermedad_bool')?$request->input('enfermedad2'):"";
                    $data['dieta2'] = $request->input('dieta_bool')?$request->input('dieta2'):"";

                    if(!$datos)
                    {
                        $this->datos->create($data);
                    }
                    else
                    {
                        $viajero->datos->update($data);
                    }
                
                    $data['fechanac'] = $request->has('fechanac')?Carbon::createFromFormat('d/m/Y',$data['fechanac'])->format('Y-m-d'):null;
                    $viajero->update($data);
                }

                if($bTutor)
                {
                    ViajeroLog::addLog($viajero, "Nuevo Booking ONLINE [$booking->id] Tutor");
                    $ficha->user_id = $viajero->asign_to ?: ConfigHelper::config('tpv_asign_to');
                    $ficha->prescriptor_id = $ficha->prescriptor_id ?: $viajero->prescriptor_id;

                    if(!$ficha->oficina_id)
                    {
                        $ficha->oficina_id = $viajero->oficina_asignada;
                    }

                    $ficha->save();
                }

                $ficha->grado_ext = $request->get('grado_ext') ?: null;
                $ficha->notas = $request->input('notas');
                $ficha->save();

                $ficha->updateDatos();

                if($viajero->id && $viajero->es_email_tutor)
                {
                    Session::flash('mensaje', trans('area.booking.email_tutor'));
                    $viajero->email = null;
                    $viajero->save();
                    $ficha->datos->email = null;
                    $ficha->datos->save();
                }

                if($ficha->es_corporativo)
                {
                    $ficha->setCorporativo();
                }

                //update codigo
                $codigo = $ficha->descuentos_codigo->first();
                if($codigo)
                {
                    $dto = DescuentoTipo::where('codigo',$codigo->codigo)->where('activo',1)->first();
                    $ficha->setDescuentoCodigo($dto);
                }

                $paso_new = 3;
            }
            break;

            case 3: //datos tutores
            {
                $totTutores = $ficha->viajero->tutores->count();

                $data = $request->input();
                $i = 1;
                foreach($ficha->viajero->tutores as $tutor)
                {
                    $this->validate($request, [
                        "name_$i" => 'required',
                        "lastname_$i" => 'required',
                        "relacion_$i" => 'required',
                        // 'email' => 'required|email'
                    ]);

                    $tutor->name = $data["name_$i"];
                    $tutor->lastname = $data["lastname_$i"];
                    $tutor->nif = $data["nif_$i"];
                    $tutor->tipodoc = $data["tipodoc_$i"] ?: 0;
                    $tutor->phone = $data["phone_$i"];
                    $tutor->movil = $data["movil_$i"];
                    $tutor->save();
                    $tutor->pivot->relacion = $data["relacion_$i"];
                    $tutor->pivot->save();

                    $email = $data["email_$i"];
                    if($tutor->email != $email)
                    {
                        if( Tutor::checkEmail($email) )
                        {
                            Session::flash('mensaje', "E-mail [$email] ya está en uso para un Tutor existente en nuestra base de datos. Compruebe los datos o contacte con una oficina nuestra.");
                            return redirect()->back();//->withInput();
                        }
                    }

                    $tutor->email = $email;
                    $tutor->save();

                    $i++;
                }

                //Nuevos: 11 y 12
                for($i=11; $i<=12; $i++)
                {
                    if($request->get("name_$i") != "")
                    {
                        $this->validate($request, [
                            "name_$i" => 'required',
                            "lastname_$i" => 'required',
                            "relacion_$i" => 'required',
                            // 'email' => 'required|email'
                        ]);

                        $email = $data["email_$i"];
                        if( Tutor::checkEmail($email) )
                        {
                            Session::flash('mensaje', "E-mail [$email] ya está en uso para un Tutor existente en nuestra base de datos. Compruebe los datos o contacte con una oficina nuestra.");
                            return redirect()->back()->withInput();
                        }

                        $tutor = new Tutor;
                        $tutor->name = $data["name_$i"];
                        $tutor->lastname = $data["lastname_$i"];
                        $tutor->nif = $data["nif_$i"];
                        $tutor->tipodoc = $data["tipodoc_$i"] ?: 0;
                        $tutor->phone = $data["phone_$i"];
                        $tutor->movil = $data["movil_$i"];
                        $tutor->plataforma = $ficha->viajero->plataforma;
                        $tutor->save();

                        $vt = new ViajeroTutor;
                        $vt->viajero_id = $ficha->viajero->id;
                        $vt->tutor_id = $tutor->id;
                        $vt->relacion = $data["relacion_$i"];
                        $vt->save();

                        $tutor->email = $email;
                        $tutor->save();
                    }
                }

                $totTutores = $ficha->viajero->tutores->count();
                if($ficha->curso->es_menor)
                {
                    if($totTutores<1)
                    {
                        Session::flash('mensaje', trans('area.nota_tutores'));
                        return redirect()->back();
                    }

                    //if los tutores no tienen mail
                    if( $ficha->viajero->tutores->where('email','')->count() == $totTutores )
                    {
                        Session::flash('mensaje', trans('area.nota_email'));
                        return redirect()->back();
                    }
                }

                if(!$ficha->factura_cif && ConfigHelper::config('nif'))
                {
                    $nif = (bool)$request->get('booking-nif', false);
                    if(!$nif)
                    {
                        Session::put('vcn.booking-nif', 1);
                        return redirect()->back();
                    }

                    $fecha = Carbon::now()->format('d/m/Y - H:i');
                    $ip = $request->ip();
                    BookingLog::addLog($ficha, 'booking-nif-no', "Declara que no tiene [$fecha] [$ip]");
                }

                Session::forget('vcn.booking-nif');

                $paso_new = 4;
            }
            break;

            case 4: //resumen
            {
                $tpv = (int)$request->get('forma_pago');
                $comprobante = $request->input('comprobante');

                $online = $ficha->online;
                $online['tpv'] = $tpv;
                $online['comprobante'] = $comprobante;

                $ficha->online = $online;
                $ficha->save();

                $es_rgpd = ConfigHelper::config('es_rgpd');
                if($es_rgpd)
                {
                    $fecha = Carbon::now()->format('d/m/Y - H:i');
                    $ip = $request->ip();
                    BookingLog::addLog($booking, 'lopd_check1', "Acepta LOPD [$fecha] [$ip]");

                    $lopd_check2 = $request->has('lopd_check2');
                    BookingLog::addLog($booking, 'lopd_check2', "LOPD C.2 [$fecha] [$ip]: ". ($lopd_check2 ? "SI":"NO"));
                    $lopd_check3 = $request->has('lopd_check3');
                    BookingLog::addLog($booking, 'lopd_check3', "LOPD C.3 [$fecha] [$ip]: ". ($lopd_check3 ? "SI":"NO"));

                    $ficha->lopd_check1 = true;
                    $ficha->lopd_check2 = $lopd_check2;
                    $ficha->lopd_check3 = $lopd_check3;
                    $ficha->save();

                    $user->ficha->lopd_check1 = true;
                    $user->ficha->lopd_check2 = $lopd_check2;
                    $user->ficha->lopd_check3 = $lopd_check3;

                    $optout = !$lopd_check2;
                    $user->ficha->optout = $optout;
                    $user->ficha->optout_fecha = $optout?Carbon::now():null;
                    $user->ficha->save();
                }
                
                if($tpv)
                {
                    return redirect()->route('comprar.pago', $ficha->id );
                }

                if($booking->es_corporativo)
                {
                    $booking->setCorporativo();

                    $ficha->setPagoTPV(0, $booking->es_corporativo_pago);

                    if($booking->es_corporativo_pago)
                    {
                        if ($comprobante) {
                            $msj = "<h4>" . trans('area.pagar_banco_ok') . "</h4>";
                            Session::flash('mensaje-ok', $msj);
                        } else {
                            $msj = "<h4>" . trans('area.pagar_banco_ko', ['email' => ConfigHelper::config('email')]) . "</h4>";
                            Session::flash('mensaje-ok', $msj);
                        }
                    }
                }
                else
                {
                    //Transferencia: booking_status_prereserva
                    //Pago:
                    $ficha->setPagoTPV(0);

                    if($comprobante)
                    {
                        $msj = "<h4>". trans('area.pagar_banco_ok') ."</h4>";
                        Session::flash('mensaje-ok',$msj);
                    }
                    else
                    {
                        $msj = "<h4>". trans('area.pagar_banco_ko', ['email'=> ConfigHelper::config('email')]) ."</h4>";
                        Session::flash('mensaje-ok',$msj);
                    }

                }

                if(!$ficha->oficina_id)
                {
                    $ficha->oficina_id = $ficha->viajero->oficina_asignada;
                    $ficha->save();
                }

                $booking->pdf_generar(false, true);
                $booking->onlineFin();

                // return redirect()->route('area.cursos');
                return redirect()->route('area.booking.firmar', $booking->id);

                // href="{{route('comprar.pago', $ficha->id)}}"
                // <a href="{{route("comprar.pago", $ficha->id)}}" class="btn btn-danger">{{trans('area.pagar')}}</a>
                $paso_new = 4;
            }
            break;
        }

        if($paso>$fase)
        {
            $booking->fase = $paso;
        }
        elseif($paso==$fase)
        {
            $booking->fase = $paso_new;
        }

        $booking->save();

        return redirect()->route('area.comprar.booking', $booking->id);
    }

    //=== Pago ===
    public function pagoUpload(Request $request, $booking_id)
    {
        $booking = Booking::find($booking_id);
        $viajero = $booking->viajero;

        $dir = storage_path("files/viajeros/". $booking->viajero_id . "/". $booking_id ."/");

        $this->validate($request, [
            // 'file' => 'image30000',
        ]);

        $file = "";
        $file = "";
        if($request->hasFile("file"))
        {
            $file = $request->file('file');

            // $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file_name = Carbon::now()->format('Ymd') ."_comprobante_pago";
            $file_name = str_slug($file_name) .".". $file->getClientOriginalExtension();
            $file->move($dir, $file_name);

            $va = new \VCN\Models\Leads\ViajeroArchivo;
            $va->viajero_id = $viajero->id;
            $va->booking_id = $booking_id;
            $va->user_id = auth()->user()->id;
            $va->doc = $booking_id?"/$booking_id/$file_name":$file_name;
            $va->notas = "Comprobante pago";
            $va->fecha = Carbon::now();
            $va->visible = 0;
            $va->save();

            $ret = [
                'file_name'=> $file_name,
                'result'=> true
            ];

            return response()->json($ret, 200);
        }

        return response()->json(false, 500);
    }


    public function pagar(Request $request, $booking_id)
    {
        $booking = Booking::find($booking_id);

        if(!$booking->online_plazas)
        {
            Session::flash('mensaje-alert', "No hay plazas disponibles. Vuelva a empezar.");
            return redirect()->route('area.comprar.booking');
        }


        // if($booking->curso->es_convocatoria_cerrada)
        // {
        //     $plazasv = $booking->vuelo?$booking->vuelo->plazas_disponibles:1;
        //     $plazasa = 0;

        //     $pa = $booking->convocatoria?$booking->convocatoria->getPlazas($booking->alojamiento->id):null;
        //     if($pa)
        //     {
        //        $plazasa = $pa->plazas_disponibles;
        //     }

        //     if( !$plazasv )
        //     {
        //         $booking->fase = 1;
        //         $booking->convocatory_close_id = null;
        //         $booking->convocatory_open_id = null;
        //         $booking->convocatory_multi_id = null;
        //         $booking->accommodation_id = null;
        //         $booking->vuelo_id = null;
        //         $booking->save();

        //         Session::flash('mensaje-alert', "No hay plazas disponibles para el VUELO seleccionado.");
        //         return redirect()->route('area.comprar.booking');
        //     }

        //     if( !$plazasa )
        //     {
        //         $booking->fase = 1;
        //         $booking->convocatory_close_id = null;
        //         $booking->convocatory_open_id = null;
        //         $booking->convocatory_multi_id = null;
        //         $booking->accommodation_id = null;
        //         $booking->vuelo_id = null;
        //         $booking->save();

        //         Session::flash('mensaje-alert', "No hay plazas disponibles para el ALOJAMIENTO seleccionado.");
        //         return redirect()->route('area.comprar.booking');
        //     }
        // }


        //bs: {"comercio":"333629509","terminal":"001","moneda":"978","clave":"sq7HjrUOBfKmC576ILgskD5srU870gJ7"}
        //cic: {"comercio":"334832540","terminal":"001","moneda":"978","clave":"sq7HjrUOBfKmC576ILgskD5srU870gJ7"}

        $p = ConfigHelper::propietario();
        $plat = Plataforma::find($p);

        $key = $plat->tpv['clave'];
        $comercio = $plat->tpv['comercio'];

        $url_notification = route('comprar.pago.notificacion', $booking_id);
        $url_ok = route('comprar.pago.ok', $booking_id);
        $url_ko = route('comprar.pago.ko', $booking_id);

        try{

            $importe = $booking->online_reserva;
            $order = substr(time(),-5)."-B".$booking_id;

            BookingLog::addLog($booking, "Redsys [$order]($importe)");

            Redsys::setAmount($importe);
            Redsys::setOrder($order);

            Redsys::setMerchantcode($comercio);
            Redsys::setCurrency($plat->tpv['moneda']);
            Redsys::setTerminal($plat->tpv['terminal']);
            Redsys::setTransactiontype('0');
            Redsys::setMethod('T'); //Solo pago con tarjeta, no mostramos iupay

            Redsys::setNotification($url_notification); //Url de notificacion
            Redsys::setUrlOk($url_ok); //Url OK
            Redsys::setUrlKo($url_ko); //Url KO

            Redsys::setVersion('HMAC_SHA256_V1');
            Redsys::setTradeName($plat->name);

            //Datos booking
            Redsys::setTitular($booking->viajero->full_name);
            Redsys::setProductDescription("Inscripción ". $booking_id);

            $entorno = isset($plat->tpv['real'])?$plat->tpv['real']:false;
            $entorno = $entorno?'live':'test';
            Redsys::setEnviroment($entorno);

            $signature = Redsys::generateMerchantSignature($key);
            Redsys::setMerchantSignature($signature);

            $style = "display:none;";
            $cssClass = 'hidden';
            Redsys::setAttributesSubmit('btn_submit','btn_submit',"Conectando con Redsys",$style,$cssClass);

            // $form = Redsys::createForm();
            return Redsys::executeRedirection();
        }
        catch(Exception $e){
            echo $e->getMessage();
        }

        // return $form;
        return view('comprar.pagar', compact('booking','form'));
    }

    public function pagoNotificacion(Request $request, $booking_id)
    {
        //Log::info("pagoNotificacion:");
        //Log::info($request->input());

        $booking = Booking::find($booking_id);

        // $user->compra = null
        // $user->save();

        $p = $booking->plataforma;
        $plat = Plataforma::find($p);
        $key = $plat->tpv['clave'];

        BookingLog::addLog($booking, "Notificación Redsys recibida");

        //check
        $p = $request->input();
        $resultado = Redsys::check($key,$p); //true/false
        //Log::info("Resultado: $resultado");

        //params
        $p = $request->get("Ds_MerchantParameters");
        $params = Redsys::getMerchantParameters($p);

        $order = $params['Ds_Order'];
        // $order_booking_id = (int)substr($order,1,strpos($order,'-')-1);
        $order_booking_id = (int)substr($order,7);

        BookingLog::addLog($booking, "Redsys [$order]");

        if($resultado)
        {
            $result = $params['Ds_Response'];
            //Log::info("Ds_Response: $result");

            if($result == "0000" )
            {
                $booking->tpv_result = $params;
                $booking->tpv_status = $resultado;
                // $booking->fase = 5;
                $booking->save();

                // $user->compra = null
                // $user->save();

                //Pago:
                $ficha = $booking;
                $p = new BookingPago;
                $p->booking_id = $ficha->id;
                $p->user_id = 0;
                $p->fecha = Carbon::now()->format('Y-m-d');
                $p->importe_pago = $booking->online_reserva;
                $p->moneda_id = ConfigHelper::default_moneda()->id;
                $p->notas = 'Reserva inicial TPV';
                // $p->avisado = 0;
                $p->rate = 0;
                $p->tipo = 2;
                $p->importe = $booking->online_reserva;
                $p->save();

                $booking->fecha_pago1 = $p->fecha;
                $booking->promo_cambio_fijo = $ficha->promo_cambio_fijo_default;
                $booking->save();

                $booking->setStatus(ConfigHelper::config('booking_status_prebooking'));
                if($booking->viajero->solicitud)
                {
                    $booking->viajero->solicitud->setInscrito();
                }
                BookingLog::addLog($booking, "Pre-Brooking + Pago [ONLINE] [TPV]");

                $booking->pdf();
            }

        }

        //Log::info("Ds_MerchantParameters:");
        //Log::info($params);

        return response()->json('success', 200);
    }

    public function pagoOK(Request $request, $booking_id)
    {
        $user = $request->user();
        $booking = Booking::find($booking_id);

        if($booking->tpv_status)
        {
            // Borramos la compra del user
            $user->compra = null;
            $user->save();

            $booking->onlineFin();
        }

        if($booking->curso->es_convocatoria_cerrada_solo || $booking->curso->es_convocatoria_multi)
        {
            $msj = "Tu inscripción ya está confirmada. Puedes consultar tu apartado de ‘Documentos’ para tener más datos sobre tu programa.";
            Session::flash('mensaje-ok',$msj);
        }
        elseif($booking->curso->es_convocatoria_abierta || $booking->curso->es_convocatoria_semicerrada_solo)
        {
            $msj = "Inscripción completada. En breve te contactaremos para confirmar tu plaza en el programa.";
            Session::flash('mensaje-ok',$msj);
        }

        // return redirect()->route('area.cursos');
        return redirect()->route('area.booking.firmar', $booking->id);

        // return redirect()->route('area.comprar.booking', $booking_id);
    }

    public function pagoKO(Request $request, $booking_id)
    {
        $user = $request->user();
        $booking = Booking::find($booking_id);

        Session::flash('mensaje-alert',"El pago ha sido rechazado.");

        //Log::info("pago KO: $booking_id");
        //Log::info($request->input());

        return redirect()->route('area.comprar.booking', $booking_id);
    }

    /**
     * @param Request $request
     * @param $curso_id
     * @param $convo_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getCorporativo(Request $request, $curso_id, $convo_id)
    {
        $user = $request->user();

        $curso = Curso::findOrFail($curso_id);
        $convo = $curso->convocatorias_all->where('id', (int)$convo_id)->first();

        if($curso && $convo && $convo->activo_corporativo)
        {
            if( $user && ($user->es_viajero || $user->es_tutor) )
            {
                $booking = null;
                if($user->es_viajero)
                {
                    $booking = Booking::where('viajero_id', $user->ficha->id)->where('convocatory_close_id', $convo_id)->where('status_id',0)->first();
                }
                elseif($user->es_tutor)
                {
                    $booking = Booking::where('tutor_id', $user->ficha->id)->where('convocatory_close_id', $convo_id)->where('status_id',0)->first();

                    if(!$booking)
                    {
                        //extraño caso de bookings corporativos sin convo
                        foreach($user->ficha->viajeros as $v)
                        {
                            $booking = Booking::where('viajero_id', $v->id)->where('curso_id', $curso_id)->where('convocatory_close_id', 0)->where('status_id',0)->first();
                            if($booking)
                            {
                                break;
                            }
                        }
                    }
                }

                Session::put('vcn.booking_corporativo', (int)$convo_id);

                if($booking && !$booking->es_terminado)
                {
                    if( $booking->setFechasConvo($convo_id) )
                    {
                        return redirect()->route('area.comprar.booking', $booking->id);
                    }
                }

                return redirect()->route( 'area.comprar.compra', [$curso_id, $convo_id] );
            }

            Session::flash('vcn.modal', 1);
            Session::put('vcn.booking_corporativo', (int)$convo_id);

            return view('comprar.booking_corporativo', compact('curso', 'convo_id'));
        }

        abort(404);
    }

    public function getBookingMulti(Request $request, $convo_id)
    {
        $user = $request->user();

        $convo = ConvocatoriaMulti::find($convo_id);
        $curso_id = 0;

        if($convo && $convo->activa)
        {
            $curso = $convo->cursos->first();
            $curso_id = $curso ? $curso->id : 0;

            if( $user && ($user->es_viajero || $user->es_tutor) )
            {
                $booking = null;
                if($user->es_viajero)
                {
                    $booking = Booking::where('viajero_id', $user->ficha->id)->where('convocatory_multi_id', $convo_id)->where('status_id',0)->first();
                }
                elseif($user->es_tutor)
                {
                    $booking = Booking::where('tutor_id', $user->ficha->id)->where('convocatory_multi_id', $convo_id)->where('status_id',0)->first();

                    if(!$booking)
                    {
                        foreach($user->ficha->viajeros as $v)
                        {
                            $booking = Booking::where('viajero_id', $v->id)->where('convocatory_multi_id', 0)->where('status_id',0)->first();
                            if($booking)
                            {
                                break;
                            }
                        }
                    }
                }

                Session::put('vcn.booking_multi', (int)$convo_id);

                if($booking && !$booking->es_terminado)
                {
                    if( $booking->setFechasConvo($convo_id) )
                    {
                        return redirect()->route('area.comprar.booking', $booking->id);
                    }
                }

                return redirect()->route( 'area.comprar.compra', [$curso_id, $convo_id] );
            }

            Session::flash('vcn.modal', 1);
            Session::put('vcn.booking_multi', (int)$convo_id);

            return view('comprar.booking_multi', compact('curso', 'convo_id'));
        }

        abort(404);
    }

    /**
     * @param Request $request
     * @param $booking_id
     */
    public function ajaxCodigo(Request $request, $booking_id)
    {
        $result = [];

        $codigo = $request->get('codigo', null);
        $actualizar = $request->get('actualizar', null);

        $result['codigo'] = $codigo;
        $result['descuento'] = 0;
        $result['importe'] = 0;
        $result['result'] = "";
        $res = "$codigo ERROR";

        if($codigo == "" || !$codigo)
        {
            return response()->json($result, 200);
        }

        $booking = Booking::find($booking_id);
        $dto = DescuentoTipo::where('codigo',$codigo)->where('activo',1)->first();

        if(!$dto)
        {
            $res = "$codigo ERROR-NOEXISTE";
            $result['result'] = $res;
            return response()->json($result, 200);
        }

        if($booking->descuentos_codigo->count() && !$actualizar)
        {
            $res = "$codigo ERROR-NO+COD";
            $result['result'] = $res;
            return response()->json($result, 200);
        }

        if($dto->es_multi && !$booking->es_convocatoria_multi)
        {
            $res = "$codigo ERROR-NO-MULTI";
            $result['result'] = $res;
            return response()->json($result, 200);
        }

        if($dto)
        {
            $dtoBooking = $booking->descuentos_codigo;
            if($actualizar)
            {
                foreach($booking->descuentos_codigo as $d)
                {
                    $d->delete();
                }
                $dtoBooking = null;
            }

            if($dtoBooking && $dtoBooking->count())
            {
                $res = "$codigo: Ya utilizado";
                BookingLog::addLog($booking, "Código: $codigo", $res);

                $result['result'] = $res;
                return response()->json($result, 200);
            }

            $result['descuento'] = $dto ? $dto->id : 0;

            $rDto = $booking->setDescuento($dto->id, $codigo);
            $result['importe'] = $rDto ? $rDto->importe : "Error";

            $res = "$codigo: ". ConfigHelper::parseMoneda($result['importe']);
            $booking->setDescuentoCodigo($dto);
        }
        
        BookingLog::addLog($booking, "Código: $codigo [$actualizar]", $res);

        $result['result'] = $res;
        return response()->json($result, 200);
    }
}
