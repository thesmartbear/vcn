<?php

namespace VCN\Http\Controllers\Area;

use Illuminate\Http\Request;

use VCN\Http\Requests;
use VCN\Http\Controllers\Controller;

use VCN\Models\User;
use VCN\Models\UserDeseo;
use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingPago;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Bookings\BookingFamilia;
use VCN\Models\Bookings\BookingSchool;
use VCN\Models\System\Cuestionario;
use VCN\Models\System\CuestionarioRespuesta;
use VCN\Models\System\DocEspecifico;
use VCN\Models\System\DocEspecificoLang;

use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\ViajeroDatos;
use VCN\Models\Leads\Tutor;
use VCN\Models\Leads\ViajeroLog;
use VCN\Models\Leads\ViajeroArchivo;
use VCN\Models\Monitores\Monitor;
use VCN\Models\Exams\{Examen, Respuesta, RespuestaLog};

use ConfigHelper;
use View;
use Session;
use Hash;
use PDF;
use Carbon;
use App;

use VCN\Helpers\MailHelper;
use setasign\Fpdi;

class AreaController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $user = auth()->user();
            View::share('usuario', $user);

            return $next($request);
        });

        // $this->middleware("area.fichas:BookingPago", ['only' => ['getPagoPdf']]);
        // $this->middleware("area.fichas:Booking", ['only' => ['getBooking']]);
        // $this->middleware("area.fichas:BookingFamilia", ['only' => ['getFamilia']]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $user = $request->user();

        // Session::put('vcn.manage.area.home', null );
        // Session::put('vcn.manage.area.tipo', null );
        // Session::put('vcn.manage.area.ficha', null );

        if ($user->status == 2) {
            $idioma = $user->ficha->idioma_contacto ?: "es";

            $alerta = "Has entrado con una contraseña generada por el sistema. Recomendamos que la cambies ahora mismo. <a href='" . route('area.datos.password') . "''>Cambiar contraseña</a>";
            if ($idioma == "ca") {
                $alerta = "Has entrat amb una contrasenya generada pel sistema. Recomanem que la canviïs ara mateix. <a href='" . route('area.datos.password') . "''>Canviar contrasenya</a>";
            }
            // Session::flash('mensaje-alert',$alerta);
        }

        if ($user->roleid != 11 && $user->roleid != 12 && $user->roleid != 13) {
            return redirect()->route('manage.index');
        }

        if ($user->compra) {
            $compra = $user->compra;
            $booking_id = (int) $compra['booking_id'];
            $curso_id = (int) $compra['curso_id'];

            $booking_corporativo = Session::get('vcn.booking_corporativo') ?: 0;
            if ($booking_corporativo) {
                $convo_id = (int) $compra['convo_id'];
                return redirect()->route('area.comprar.compra', [$curso_id, $convo_id]);
            }

            $booking_multi = Session::get('vcn.booking_multi') ?: 0;
            if ($booking_multi) {
                $convo_id = (int) $compra['convo_id'];
                return redirect()->route('area.comprar.compra', [$curso_id, $convo_id]);
            }

            $booking = Booking::find($booking_id);
            if ($booking) {
                if (!$booking->status_id) {
                    return redirect()->route('area.comprar.booking');
                }

                $booking->onlineFin();
            }

            //compra sin booking todavía
            if ($curso_id) {
                return redirect()->route('area.comprar.compra', [$curso_id]);
            }
        }

        $pendientes = $user->ficha->bookings_area_pendientes;
        if ($pendientes && $pendientes->count()) {
            if ($pendientes->count() > 1) {
                return redirect()->route('area.pendientes');
            }

            //return redirect()->route('area.comprar.booking');
        }

        if ($user->es_tutor) {
            foreach ($user->ficha->viajeros as $viajero) {
                $pendientes = $viajero->bookings_area_pendientes;
                if ($pendientes && $pendientes->count()) {
                    return redirect()->route('area.pendientes');
                }
            }
        }

        $usuario = $user;
        return view('area.index', compact('usuario'));
    }

    public function getCursos(Request $request)
    {
        $user = ConfigHelper::usuario();

        if ($user->status == 2) {
            $alerta = "Has entrado con una contraseña generada por el sistema. Recomendamos que la cambies ahora mismo. <a href='" . route('area.datos.password') . "''>Cambiar contraseña</a>";
            if ($user->ficha->idioma_contacto == "ca") {
                $alerta = "Has entrat amb una contrasenya generada pel sistema. Recomanem que la canviïs ara mateix.. <a href='" . route('area.datos.password') . "''>Canviar contrasenya</a>";
            }
            // Session::flash('mensaje-alert',$alerta);
        }

        if ($user->roleid != 11 && $user->roleid != 12) {
            return redirect()->route('manage.index');
        }

        return view('area.index');
    }

    public function getPendientes(Request $request, $user_id = null)
    {
        if ($user_id) {
            $user = User::find($user_id);
        } else {
            $user = $request->user();
        }

        $usuario = $user;

        return view('area.pendientes', compact('usuario'));
    }

    public function getDatos(Request $request)
    {
        $user = ConfigHelper::usuario();

        $ficha = $user->ficha;

        return view('area.datos', compact('ficha'));
    }

    private static function updateViajero(Request $request, $viajero_id)
    {
        $avisar = ['nacionalidad', 'documento', 'pasaporte',  'pasaporte_pais', 'pasaporte_emision', 'pasaporte_caduca', 'alergias', 'enfermedad', 'tratamiento', 'medicacion', 'dieta'];

        $ignora1 = ['origen_id', 'suborigen_id', 'suborigendet_id', 'es_adulto', 'rating', 'notas', 'emergencia_contacto', 'emergencia_telefono', 'email'];
        $ignora2 = ['viajero_id'];

        $medicos = ['alergias', 'enfermedad', 'medicacion', 'tratamiento', 'dieta', 'animales'];

        $viajero = Viajero::find($viajero_id);
        $input = $request->input();
        $fillable1 = $viajero->getFillable();
        $fillable2 = $viajero->datos->getFillable();

        $avisos = "";
        $cambios = "";
        $tab = "datos";

        $user = $request->user();

        if ($request->has('name')) {
            //Viajero
            foreach ($fillable1 as $field) {
                if (in_array($field, $ignora1)) {
                    continue;
                }

                $old = $viajero->$field;
                $new = $request->get($field);

                if ($field == "fechanac") {
                    $new = $request->has($field) ? $request->get($field) : null;
                    $new = $new ? Carbon::createFromFormat('d/m/Y', $new)->format('Y-m-d') : "0000-00-00";
                }

                if ($old != $new) {
                    $cambios .= "$field: $old => $new, ";

                    if (in_array($field, $avisar)) {
                        $avisos .= "$field: $old => $new, ";
                    }

                    $viajero->$field = $new;
                    $viajero->save();
                }
            }

            //ViajeroDatos
            foreach ($fillable2 as $field) {
                if (in_array($field, $ignora2)) {
                    continue;
                }

                if (in_array($field, $medicos)) {
                    continue;
                }

                $old = $viajero->datos->$field;
                $new = $request->get($field);

                if ($field == "pasaporte_emision") {
                    $new = $request->has($field) ? $request->get($field) : null;
                    $new = $new ? Carbon::createFromFormat('d/m/Y', $new)->format('Y-m-d') : "0000-00-00";
                } elseif ($field == "pasaporte_caduca") {
                    $new = $request->has($field) ? $request->get($field) : null;
                    $new = $new ? Carbon::createFromFormat('d/m/Y', $new)->format('Y-m-d') : "0000-00-00";
                }

                $datos = $viajero->datos;

                if ($old != $new) {
                    $cambios .= "$field: $old => $new, ";

                    if (in_array($field, $avisar)) {
                        $avisos .= "$field: $old => $new, ";
                    }

                    $viajero->datos->$field = $new;
                    $viajero->datos->save();
                }
            }
        } elseif ($request->has('submit_datos_campamento')) {
            if ($viajero->booking) {
                $viajero->booking->updateDatosCampamento($request);
            }

            Session::flash('tab2', "#$viajero_id-datos3");
            return;
        } elseif ($request->has('submit_datos_medicos')) {
            //Datos médicos
            $datos = $viajero->datos;
            foreach ($medicos as $field) {
                $old = $viajero->datos->$field;
                $new = $request->get($field);

                $datos->$field = $request->get($field . '_bool') ? $request->get($field) : "";
                $datos->save();

                if ($old != $new) {
                    $cambios .= "$field: $old => $new, ";

                    if (in_array($field, $avisar)) {
                        $avisos .= "$field: $old => $new, ";
                    }

                    $viajero->datos->$field = $new;
                    $viajero->datos->save();
                }
            }

            $tab = "datosmedicos";

            Session::flash('tab', "#hijo-$viajero_id");
            Session::flash('tab2', "#$viajero_id-datos2");
        } elseif ($request->has('submit_avatar_reset')) {
            $viajero->foto = null;
            $viajero->save();

            if ($viajero->user) {
                $viajero->user->avatar = null;
                $viajero->user->save();
            }

            Session::flash('tab', "#avatar");
        } elseif ($request->has('submit_avatar')) {
            $foto = null;
            if ($request->get('webcam64') != "") {
                $foto = $request->get('webcam64');
            } elseif ($request->hasFile('foto')) {
                $file = $request->file('foto');
                $dirp = "assets/uploads/viajeros/" . $viajero_id . "/";

                $foto = ConfigHelper::uploadFoto($file, $dirp, 400, true);
            }

            if ($foto) {
                $viajero->foto = $foto;
                $viajero->save();

                // if($viajero->user)
                // {
                //     $viajero->user->avatar = $foto;
                //     $viajero->user->save();
                // }
            }

            Session::flash('tab2', "#$viajero_id-avatar");
            return;
        }


        if ($user->es_viajero) {
            $es_rgpd = ConfigHelper::config('es_rgpd', $user->plataforma);

            if ($es_rgpd) {
                $optout = !$request->has('lopd_check2');
                $viajero->optout = $optout;
                $viajero->optout_fecha = $optout ? Carbon::now() : null;
                $viajero->lopd_check2 = $request->has('lopd_check2') ? 1 : 0;

                if ($request->has('submit_lopd')) //popup
                {
                    $viajero->lopd_check1 = $request->has('lopd_check1') ? 1 : 0;
                    $viajero->lopd_check3 = $request->has('lopd_check3') ? 1 : 0;

                    foreach ($viajero->bookings_terminados as $booking) {
                        if ($booking->any == 2018) {
                            $fecha = Carbon::now()->format('d/m/Y - H:i');
                            $ip = $request->ip();
                            BookingLog::addLog($booking, 'lopd_check1', "Acepta LOPD [$fecha] [$ip]");

                            $lopd_check2 = $request->has('lopd_check2') ? 1 : 0;
                            BookingLog::addLog($booking, 'lopd_check2', "LOPD C.2 [$fecha] [$ip]: " . ($lopd_check2 ? "SI" : "NO"));
                            $lopd_check3 = $request->has('lopd_check3') ? 1 : 0;
                            BookingLog::addLog($booking, 'lopd_check3', "LOPD C.3 [$fecha] [$ip]: " . ($lopd_check3 ? "SI" : "NO"));

                            $booking->lopd_check1 = true;
                            $booking->lopd_check2 = $lopd_check2;
                            $booking->lopd_check3 = $lopd_check3;
                            $booking->save();
                        }
                    }
                }
            } else {
                $optout = $request->has('optout');
                $viajero->optout = $optout;
                $viajero->optout_fecha = $optout ? Carbon::now() : null;
            }

            $viajero->save();

            Session::flash('tab', "#ficha");
        }

        ViajeroLog::addLogDatos($viajero, $cambios);

        if ($viajero->booking) {
            $booking = $viajero->booking;
            $booking->updateDatos();
            $booking->pdf();
        }

        if ($avisos) {
            MailHelper::mailDatos($viajero_id, $avisos, $tab);
            ViajeroLog::addLogDatos($viajero, "Email aviso enviado");
        }
    }

    private static function updateTutor(Request $request, $tutor_id)
    {
        $tutor = Tutor::find($tutor_id);
        $input = $request->except('submit_lopd', 'foto');

        if ($request->has('submit_avatar_reset')) {
            $tutor->foto = null;
            $tutor->save();

            $tutor->user->avatar = null;
            $tutor->user->save();

            Session::flash('tab', "#avatar");
            return;
        } elseif ($request->has('submit_avatar')) {
            $foto = null;
            if ($request->get('webcam64') != "") {
                $foto = $request->get('webcam64');
            } elseif ($request->hasFile('foto')) {
                $file = $request->file('foto');
                $dirp = "assets/uploads/tutores/" . $tutor_id . "/";

                $foto = ConfigHelper::uploadFoto($file, $dirp, 400, true);
            }

            if ($foto) {
                $tutor->foto = $foto;
                $tutor->save();

                // if($tutor->user)
                // {
                //     $tutor->user->avatar = $foto;
                //     $tutor->user->save();
                // }
            }

            Session::flash('tab', "#avatar");
            return;
        }

        $es_rgpd = ConfigHelper::config('es_rgpd');
        if ($es_rgpd) {
            $optout = !$request->has('lopd_check2');
            $input['optout'] = $optout;
            $input['optout_fecha'] = $optout ? Carbon::now() : null;
            $input['lopd_check2'] = $request->has('lopd_check2') ? 1 : 0;

            if ($request->has('submit_lopd')) //popup
            {
                $input['lopd_check1'] = $request->has('lopd_check1') ? 1 : 0;
                $input['lopd_check3'] = $request->has('lopd_check3');

                foreach ($tutor->bookings_viajeros as $booking) {
                    if ($booking->any == 2018) {
                        $fecha = Carbon::now()->format('d/m/Y - H:i');
                        $ip = $request->ip();
                        BookingLog::addLog($booking, 'lopd_check1', "Acepta LOPD [$fecha] [$ip]");

                        $lopd_check2 = $request->has('lopd_check2') ? 1 : 0;
                        BookingLog::addLog($booking, 'lopd_check2', "LOPD C.2 [$fecha] [$ip]: " . ($lopd_check2 ? "SI" : "NO"));
                        $lopd_check3 = $request->has('lopd_check3') ? 1 : 0;
                        BookingLog::addLog($booking, 'lopd_check3', "LOPD C.3 [$fecha] [$ip]: " . ($lopd_check3 ? "SI" : "NO"));

                        $booking->lopd_check1 = true;
                        $booking->lopd_check2 = $lopd_check2;
                        $booking->lopd_check3 = $lopd_check3;
                        $booking->save();
                    }
                }
            }
        } else {
            $input['optout'] = $request->has('optout');
            $input['optout_fecha'] = $request->has('optout') ? Carbon::now() : null;
        }

        $tutor->update($input);
    }

    public function postDatos(Request $request)
    {
        $user = $request->user();

        if ($user->es_viajero) {
            self::updateViajero($request, $user->viajero->id);
        }

        if ($user->es_tutor) {
            self::updateTutor($request, $user->tutor->id);
        }

        Session::flash('mensaje-ok', trans('area.datos_actualizados'));
        return redirect()->back();
    }

    public function postHijos(Request $request, $viajero_id)
    {
        self::updateViajero($request, (int) $viajero_id);

        Session::flash('mensaje-ok', trans('area.datos_actualizados'));
        return redirect()->back();
    }

    public function getHijos(Request $request)
    {
        $user = ConfigHelper::usuario();

        $ficha = $user->ficha;

        if(!$ficha || !$ficha->viajeros)
        {
            return back();
        }

        foreach ($ficha->viajeros as $hijo) {
            if (!$hijo->datos) {
                $datos = [];
                $datos['viajero_id'] = $hijo->id;
                ViajeroDatos::create($datos);
            }
        }

        return view('area.datos_hijos', compact('ficha'));
    }

    public function getTutores(Request $request)
    {
        $user = ConfigHelper::usuario();

        $ficha = $user->ficha;

        return view('area.datos_tutores', compact('ficha'));
    }


    public function getPasswordCambiar()
    {
        Session::flash('tab', '#password');

        return view('area.datos');
    }

    public function postPasswordCambiar(Request $request)
    {
        $user = ConfigHelper::usuario();

        if (!$user) {
            return back();
        }

        if ($user->status < 1) {
            return back();
        }

        $this->validate($request, [
            'password'  => 'required',
            'newpass'  => 'required|confirmed|min:6',
        ]);

        $pass = trim($request->input('password'));
        if (!Hash::check($pass, $user->password)) {
            Session::flash('mensaje-alert', "Contraseña anterior incorrecta.");
            return back();
        }

        $newpass = $request->input('newpass');

        $user->password = bcrypt($newpass);
        $user->status = 1;
        $user->save();

        Session::flash('mensaje-ok', "Contraseña modificada correctamente.");
        return redirect()->route('area.index');
    }

    public function getBooking(Request $request, $booking_id)
    {
        $booking = Booking::find($booking_id);

        if (!$booking) {
            abort(404);
        }

        //Seguridad
        ConfigHelper::areaPermiso($request->user(), $booking->viajero);

        return redirect()->back();
    }

    public function getPdfMonitor(Request $request, $id)
    {
        $booking = Booking::find($id);

        if (!$booking) {
            abort(404);
        }

        ConfigHelper::areaPermiso($request->user(), $booking->viajero);

        return $booking->pdf_monitor();
    }

    public function getPagoPdf(Request $request, $id)
    {
        $ficha = BookingPago::find($id);

        $booking = $ficha->booking;

        ConfigHelper::areaPermiso($request->user(), $booking->viajero);

        $idioma = $booking->viajero->idioma_contacto;

        $view = 'pdf.' . $idioma . '.recibo';

        $r = $booking->id . "_" . Carbon::parse($ficha->fecha)->format('d-m-Y');

        $name = "recibo_";
        if ($idioma == "ca") {
            $name = "rebut_";
        }

        $file = $name . $r . ".pdf"; // ."-". Carbon::now()->format('Y-m-d_H-i-s') .".pdf";

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView($view, compact('ficha', 'booking', 'idioma'))
            ->setOption('margin-top', 0)->setOption('margin-right', 0)->setOption('margin-bottom', 0)->setOption('margin-left', 0)
            ->setOption('disable-smart-shrinking', true)->setOption('zoom', '0.78');
        return $pdf->download($file);
    }


    public function getForm(Request $request, $cuestionario_id, $booking_id)
    {
        $booking = Booking::find($booking_id);
        ConfigHelper::areaPermiso($request->user(), $booking->viajero);

        $user = $request->user();
        $ficha = $user->ficha;

        if ($user->roleid != 11 && $user->roleid != 12 && $user->roleid != 13) {
            $fid = Session::get('vcn.manage.area.ficha');
            $d = Session::get('vcn.manage.area.tipo', null);

            if ($d == 'tutor') {
                $ficha = Tutor::find($fid);
            } elseif ($d == 'tutor') {
                $ficha = Viajero::find($fid);
            }

            $user = $ficha ? $ficha->user : $user;
        }

        $form = Cuestionario::findOrFail($cuestionario_id);
        $form_id = $form;

        $respuestas = null;
        $r = null;

        $tuser = "viajero_id";
        if ($user->roleid == 12) {
            $tuser = "tutor_id";
        }

        if (!$user->ficha) {
            $fid = $booking->viajero->id;
        } else {
            $fid = $user->ficha->id;
        }

        $r = CuestionarioRespuesta::where('booking_id', $booking_id)->where($tuser, $fid)->where('cuestionario_id', $cuestionario_id)->orderBy('updated_at', 'desc')->first();

        if ($r) {
            $respuestas = $r->respuestas;
        }

        if ($form->test) {
            //Test: validamos si ya lo ha respondido
            if ($r) {
                if ($r->estado > 0) {
                    return redirect()->route('area.forms.resultado', [$cuestionario_id, $booking_id, $r->id]);
                }
            }
        }

        $view = 'area.forms.' . $form->form;
        return view($view, compact('ficha', 'booking', 'form', 'respuestas'));
    }

    public function postForm(Request $request, $cuestionario_id, $booking_id)
    {
        $booking = Booking::find($booking_id);
        ConfigHelper::areaPermiso($request->user(), $booking->viajero);

        $form = Cuestionario::findOrFail($cuestionario_id);

        $user = $request->user();
        $ficha = $user->ficha;

        $respuestas = $request->input();
        $datos = $respuestas['datos'];

        if (!isset($respuestas['respuesta'])) {
            $datos = $respuestas['datos'];
            $view = 'area.forms.' . $form->form;
            $form_id = $cuestionario_id;
            return view($view, compact('datos', 'booking', 'form'));
        }


        //si no es test de nivel
        if (!$form->test) {
            if ($user->es_tutor) {
                $r = $booking->getCuestionarioRespuestaTutor($cuestionario_id, $ficha->id);
            } else {
                $r = $booking->getCuestionarioRespuesta($cuestionario_id);
            }

            if (!$r) {
                $r = new CuestionarioRespuesta;
            }
        } else {
            $r = new CuestionarioRespuesta;
        }

        $r->cuestionario_id = $cuestionario_id;
        $r->booking_id = $booking_id;

        if ($user->roleid == 11) {
            //viajero_id
            $r->viajero_id = $ficha->id;
        } elseif ($user->roleid == 12) {
            //tutor_id
            $r->tutor_id = $ficha->id;
        } elseif ($user->roleid == 13) {
            //es monitor y asignamos viajero_id
            $r->viajero_id = $booking->viajero->id;
        }
        $r->save();

        if ($form->test == 1 && $form->form == 'testnivelmaxcamps') {
            //Respuestas, Puntuaciones, Resultado, Nivel ???
            $respuesta = $respuestas['respuesta'];
            $datos = unserialize($respuestas['datos']);

            //Correcciones test nivel max camps
            if ($datos['testnumber'] == 1) {
                //Correcciones pregunta 1
                $puntosA = 10;
                $pA01 = $respuesta['pA01'];
                if ($pA01 != "scissors") {
                    $puntosA = $puntosA - 1;
                }
                $pA02 = $respuesta['pA02'];
                if ($pA02 != "geography") {
                    $puntosA = $puntosA - 1;
                }
                $pA03 = $respuesta['pA03'];
                if ($pA03 != "dictionary") {
                    $puntosA = $puntosA - 1;
                }
                $pA04 = $respuesta['pA04'];
                if ($pA04 != "art") {
                    $puntosA = $puntosA - 1;
                }
                $pA05 = $respuesta['pA05'];
                if ($pA05 != "spoon") {
                    $puntosA = $puntosA - 1;
                }
                $pA06 = $respuesta['pA06'];
                if ($pA06 != "postcard") {
                    $puntosA = $puntosA - 1;
                }
                $pA07 = $respuesta['pA07'];
                if ($pA07 != "knife") {
                    $puntosA = $puntosA - 1;
                }
                $pA08 = $respuesta['pA08'];
                if ($pA08 != "university") {
                    $puntosA = $puntosA - 1;
                }
                $pA09 = $respuesta['pA09'];
                if ($pA09 != "envelope") {
                    $puntosA = $puntosA - 1;
                }
                $pA10 = $respuesta['pA10'];
                if ($pA10 != "newspaper") {
                    $puntosA = $puntosA - 1;
                }

                //Correcciones pregunta 2
                $puntosB = 6;
                $pB01 = stripslashes($respuesta["pB01"]);
                if ($pB01 != "Yes, you can.") {
                    $puntosB = $puntosB - 1;
                }
                $pB02 = stripslashes($respuesta["pB02"]);
                if ($pB02 != "Yes, I love it.") {
                    $puntosB = $puntosB - 1;
                }
                $pB03 = stripslashes($respuesta["pB03"]);
                if ($pB03 != "All right.") {
                    $puntosB = $puntosB - 1;
                }
                $pB04 = stripslashes($respuesta["pB04"]);
                if ($pB04 != "Yes, it's easy.") {
                    $puntosB = $puntosB - 1;
                }
                $pB05 = stripslashes($respuesta["pB05"]);
                if ($pB05 != "I like playing football.") {
                    $puntosB = $puntosB - 1;
                }
                $pB06 = stripslashes($respuesta["pB06"]);
                if ($pB06 != "Sorry, I can't.") {
                    $puntosB = $puntosB - 1;
                }

                //Correcciones pregunta 3
                $puntosC = 8;
                $pC01 = stripslashes($respuesta['pC01']);
                if (strtolower($pC01) != "one") {
                    $puntosC = $puntosC - 1;
                }
                $pC02 = stripslashes($respuesta['pC02']);
                if (strtolower($pC02) != "it") {
                    $puntosC = $puntosC - 1;
                }
                $pC03 = stripslashes($respuesta['pC03']);
                if (strtolower($pC03) != "even") {
                    $puntosC = $puntosC - 1;
                }
                $pC04 = stripslashes($respuesta['pC04']);
                if (strtolower($pC04) != "talking") {
                    $puntosC = $puntosC - 1;
                }
                $pC05 = stripslashes($respuesta['pC05']);
                if (strtolower($pC05) != "like") {
                    $puntosC = $puntosC - 1;
                }
                $pC06 = stripslashes($respuesta['pC06']);
                if (strtolower($pC06) != "by") {
                    $puntosC = $puntosC - 1;
                }
                $pC07 = stripslashes($respuesta['pC07']);
                if (strtolower($pC07) != "much") {
                    $puntosC = $puntosC - 1;
                }
                $pC08 = stripslashes($respuesta['pC08']);
                if (strtolower($pC08) != "are") {
                    $puntosC = $puntosC - 1;
                }

                //Totales
                $puntuacion = $puntosA + $puntosB + $puntosC;
                if ($puntuacion <= 7) {
                    $nivel = 1;
                } elseif ($puntuacion >= 8 && $puntuacion <= 13) {
                    $nivel = 2;
                } elseif ($puntuacion >= 14) {
                    $nivel = 3;
                }

                $puntuaciones['puntosA'] = $puntosA;
                $puntuaciones['puntosB'] = $puntosB;
                $puntuaciones['puntosC'] = $puntosC;
                $r->puntuaciones = $puntuaciones;
                $r->resultado = $puntuacion;
                $r->nivel = $nivel;
            } elseif ($datos['testnumber'] == 2) {
                //Correcciones pregunta 1
                $puntosA = 11;
                $sA01 = $respuesta["sA01"];
                if (strtolower($sA01) != "a") {
                    $puntosA = $puntosA - 1;
                }
                $sA02 = $respuesta["sA02"];
                if (strtolower($sA02) != "work") {
                    $puntosA = $puntosA - 1;
                }
                $sA03 = $respuesta["sA03"];
                if (strtolower($sA03) != "there") {
                    $puntosA = $puntosA - 1;
                }
                $sA04 = $respuesta["sA04"];
                if (strtolower($sA04) != "them") {
                    $puntosA = $puntosA - 1;
                }
                $sA05 = $respuesta["sA05"];
                if (strtolower($sA05) != "just") {
                    $puntosA = $puntosA - 1;
                }
                $sA06 = $respuesta["sA06"];
                if (strtolower($sA06) != "stay") {
                    $puntosA = $puntosA - 1;
                }
                $sA07 = $respuesta["sA07"];
                if (strtolower($sA07) != "have") {
                    $puntosA = $puntosA - 1;
                }
                $sA08 = $respuesta["sA08"];
                if (strtolower($sA08) != "because") {
                    $puntosA = $puntosA - 1;
                }
                $sA09 = $respuesta["sA09"];
                if (strtolower($sA09) != "some") {
                    $puntosA = $puntosA - 1;
                }
                $sA10 = $respuesta["sA10"];
                if (strtolower($sA10) != "when") {
                    $puntosA = $puntosA - 1;
                }
                $sA11 = $respuesta["sA11"];
                if (strtolower($sA10) != "who") {
                    $puntosA = $puntosA - 1;
                }

                // Correcciones pregunta 2
                $puntosB = 8;
                $sB01 = $respuesta["sB01"];
                if ($sB01 != "Sometimes") {
                    $puntosB = $puntosB - 1;
                }
                $sB02 = $respuesta["sB02"];
                if ($sB02 != "Twice a month.") {
                    $puntosB = $puntosB - 1;
                }
                $sB03 = $respuesta["sB03"];
                if ($sB03 != "Black trousers and a jumper") {
                    $puntosB = $puntosB - 1;
                }
                $sB04 = stripslashes($respuesta["sB04"]);
                if ($sB04 != "I'm going to Tenerife with my family.") {
                    $puntosB = $puntosB - 1;
                }
                $sB05 = $respuesta["sB05"];
                if ($sB05 != "I used to play with dolls") {
                    $puntosB = $puntosB - 1;
                }
                $sB06 = stripslashes($respuesta["sB06"]);
                if ($sB06 != "I'd ask for their autograph.") {
                    $puntosB = $puntosB - 1;
                }
                $sB07 = $respuesta["sB07"];
                if ($sB07 != "I went to the cinema") {
                    $puntosB = $puntosB - 1;
                }
                $sB08 = $respuesta["sB08"];
                if ($sB08 != "I want to go to university") {
                    $puntosB = $puntosB - 1;
                }

                // Correcciones pregunta 3
                $puntosC = 10;
                $sC01 = $respuesta["sC01"];
                if (strtolower($sC01) != "amount") {
                    $puntosC = $puntosC - 1;
                }
                $sC02 = $respuesta["sC02"];
                if (strtolower($sC02) != "on") {
                    $puntosC = $puntosC - 1;
                }
                $sC03 = $respuesta["sC03"];
                if (strtolower($sC03) != "ought") {
                    $puntosC = $puntosC - 1;
                }
                $sC04 = $respuesta["sC04"];
                if (strtolower($sC04) != "habits") {
                    $puntosC = $puntosC - 1;
                }
                $sC05 = $respuesta["sC05"];
                if (strtolower($sC05) != "enough") {
                    $puntosC = $puntosC - 1;
                }
                $sC06 = $respuesta["sC06"];
                if (strtolower($sC06) != "position") {
                    $puntosC = $puntosC - 1;
                }
                $sC07 = $respuesta["sC07"];
                if (strtolower($sC07) != "plenty") {
                    $puntosC = $puntosC - 1;
                }
                $sC08 = $respuesta["sC08"];
                if (strtolower($sC08) != "although") {
                    $puntosC = $puntosC - 1;
                }
                $sC09 = $respuesta["sC09"];
                if (strtolower($sC09) != "if") {
                    $puntosC = $puntosC - 1;
                }
                $sC10 = $respuesta["sC10"];
                if (strtolower($sC10) != "journey") {
                    $puntosC = $puntosC - 1;
                }

                //Totales
                $puntuacion = $puntosA + $puntosB + $puntosC;
                if ($puntuacion <= 13) {
                    $nivel = 3;
                } elseif ($puntuacion >= 14 && $puntuacion <= 19) {
                    $nivel = 4;
                } elseif ($puntuacion >= 20) {
                    $nivel = 5;
                }

                $puntuaciones['puntosA'] = $puntosA;
                $puntuaciones['puntosB'] = $puntosB;
                $puntuaciones['puntosC'] = $puntosC;
                $r->puntuaciones = $puntuaciones;
                $r->resultado = $puntuacion;
                $r->nivel = $nivel;
            } elseif ($datos['testnumber'] == 3) {
                //Correcciones
                $puntos = 60;
                $tA01 = $respuesta['tA01'];
                if (strtolower($tA01) != "i") {
                    $puntos = $puntos - 1;
                }
                $tA02 = $respuesta['tA02'];
                if (strtolower($tA02) != "m") {
                    $puntos = $puntos - 1;
                }
                $tA03 = $respuesta['tA03'];
                if (strtolower($tA03) != "light") {
                    $puntos = $puntos - 1;
                }
                $tA04 = $respuesta['tA04'];
                if (strtolower($tA04) != 'my') {
                    $puntos = $puntos - 1;
                }
                $tA05 = $respuesta['tA05'];
                if (strtolower($tA05) != 'works') {
                    $puntos = $puntos - 1;
                }
                $tA06 = $respuesta['tA06'];
                if (strtolower($tA06) != 'brother') {
                    $puntos = $puntos - 1;
                }
                $tA07 = $respuesta['tA07'];
                if (strtolower($tA07) != "they") {
                    $puntos = $puntos - 1;
                }
                $tA08 = $respuesta['tA08'];
                if (strtolower($tA08) != 'and') {
                    $puntos = $puntos - 1;
                }
                $tA09 = $respuesta['tA09'];
                if (strtolower($tA09) != 'the') {
                    $puntos = $puntos - 1;
                }
                $tA10 = $respuesta['tA10'];
                if (strtolower($tA10) != 'to') {
                    $puntos = $puntos - 1;
                }
                $tA11 = $respuesta['tA11'];
                if (strtolower($tA11) != 'moved') {
                    $puntos = $puntos - 1;
                }
                $tA12 = $respuesta['tA12'];
                if (strtolower($tA12) != 'in') {
                    $puntos = $puntos - 1;
                }
                $tA13 = $respuesta['tA13'];
                if (strtolower($tA13) != "m") {
                    $puntos = $puntos - 1;
                }
                $tA14 = $respuesta['tA14'];
                if (strtolower($tA14) != 'a') {
                    $puntos = $puntos - 1;
                }
                $tA15 = $respuesta['tA15'];
                if (strtolower($tA15) != "like") {
                    $puntos = $puntos - 1;
                }
                $tA16 = $respuesta['tA16'];
                if (strtolower($tA16) != 'at') {
                    $puntos = $puntos - 1;
                }
                $tA17 = $respuesta['tA17'];
                if (strtolower($tA17) != "theatre") {
                    $puntos = $puntos - 1;
                }
                $tA18 = $respuesta['tA18'];
                if (strtolower($tA18) != 'and') {
                    $puntos = $puntos - 1;
                }
                $tA19 = $respuesta['tA19'];
                if (strtolower($tA19) != "yourself") {
                    $puntos = $puntos - 1;
                }
                $tA20 = $respuesta['tA20'];
                if (strtolower($tA20) != "company") {
                    $puntos = $puntos - 1;
                }
                $tA21 = $respuesta['tA21'];
                if (strtolower($tA21) != "a") {
                    $puntos = $puntos - 1;
                }
                $tA22 = $respuesta['tA22'];
                if (strtolower($tA22) != "although") {
                    $puntos = $puntos - 1;
                }
                $tA23 = $respuesta['tA23'];
                if (strtolower($tA23) != "the") {
                    $puntos = $puntos - 1;
                }
                $tA24 = $respuesta['tA24'];
                if (strtolower($tA24) != 'with') {
                    $puntos = $puntos - 1;
                }
                $tA25 = $respuesta['tA25'];
                if (strtolower($tA25) != 'i') {
                    $puntos = $puntos - 1;
                }
                $tA26 = $respuesta['tA26'];
                if (strtolower($tA26) != "job") {
                    $puntos = $puntos - 1;
                }
                $tA27 = $respuesta['tA27'];
                if (strtolower($tA27) != "found") {
                    $puntos = $puntos - 1;
                }
                $tA28 = $respuesta['tA28'];
                if (strtolower($tA28) != "m") {
                    $puntos = $puntos - 1;
                }
                $tA29 = $respuesta['tA29'];
                if (strtolower($tA29) != 'have') {
                    $puntos = $puntos - 1;
                }
                $tA30 = $respuesta['tA30'];
                if (strtolower($tA30) != "this") {
                    $puntos = $puntos - 1;
                }
                $tA31 = $respuesta['tA31'];
                if (strtolower($tA31) != "was") {
                    $puntos = $puntos - 1;
                }
                $tA32 = $respuesta['tA32'];
                if (strtolower($tA32) != 'people') {
                    $puntos = $puntos - 1;
                }
                $tA33 = $respuesta['tA33'];
                if (strtolower($tA33) != 'ago') {
                    $puntos = $puntos - 1;
                }
                $tA34 = $respuesta['tA34'];
                if (strtolower($tA34) != 'again') {
                    $puntos = $puntos - 1;
                }
                $tA35 = $respuesta['tA35'];
                if (strtolower($tA35) != 'he') {
                    $puntos = $puntos - 1;
                }
                $tA36 = $respuesta['tA36'];
                if (strtolower($tA36) != "told") {
                    $puntos = $puntos - 1;
                }
                $tA37 = $respuesta['tA37'];
                if (strtolower($tA37) != 'i') {
                    $puntos = $puntos - 1;
                }
                $tA38 = $respuesta['tA38'];
                if (strtolower($tA38) != "though") {
                    $puntos = $puntos - 1;
                }
                $tA39 = $respuesta['tA39'];
                if (strtolower($tA39) != 'what') {
                    $puntos = $puntos - 1;
                }
                $tA40 = $respuesta['tA40'];
                if (strtolower($tA40) != 'which') {
                    $puntos = $puntos - 1;
                }
                $tA41 = $respuesta['tA41'];
                if (strtolower($tA41) != 'to') {
                    $puntos = $puntos - 1;
                }
                $tA42 = $respuesta['tA42'];
                if (strtolower($tA42) != "theft") {
                    $puntos = $puntos - 1;
                }
                $tA43 = $respuesta['tA43'];
                if (strtolower($tA43) != 'place') {
                    $puntos = $puntos - 1;
                }
                $tA44 = $respuesta['tA44'];
                if (strtolower($tA44) != "five") {
                    $puntos = $puntos - 1;
                }
                $tA45 = $respuesta['tA45'];
                if (strtolower($tA45) != 'and') {
                    $puntos = $puntos - 1;
                }
                $tA46 = $respuesta['tA46'];
                if (strtolower($tA46) != "three") {
                    $puntos = $puntos - 1;
                }
                $tA47 = $respuesta['tA47'];
                if (strtolower($tA47) != "locks") {
                    $puntos = $puntos - 1;
                }
                $tA48 = $respuesta['tA48'];
                if (strtolower($tA48) != 'of') {
                    $puntos = $puntos - 1;
                }
                $tA49 = $respuesta['tA49'];
                if (strtolower($tA49) != 'by') {
                    $puntos = $puntos - 1;
                }
                $tA50 = $respuesta['tA50'];
                if (strtolower($tA50) != 'exhibition') {
                    $puntos = $puntos - 1;
                }
                $tA51 = $respuesta['tA51'];
                if (strtolower($tA51) != "where") {
                    $puntos = $puntos - 1;
                }
                $tA52 = $respuesta['tA52'];
                if (strtolower($tA52) != "displaying") {
                    $puntos = $puntos - 1;
                }
                $tA53 = $respuesta['tA53'];
                if (strtolower($tA53) != "fact") {
                    $puntos = $puntos - 1;
                }
                $tA54 = $respuesta['tA54'];
                if (strtolower($tA54) != 'is') {
                    $puntos = $puntos - 1;
                }
                $tA55 = $respuesta['tA55'];
                if (strtolower($tA55) != 'of') {
                    $puntos = $puntos - 1;
                }
                $tA56 = $respuesta['tA56'];
                if (strtolower($tA56) != "remove") {
                    $puntos = $puntos - 1;
                }
                $tA57 = $respuesta['tA57'];
                if (strtolower($tA57) != "sound") {
                    $puntos = $puntos - 1;
                }
                $tA58 = $respuesta['tA58'];
                if (strtolower($tA58) != 'will') {
                    $puntos = $puntos - 1;
                }
                $tA59 = $respuesta['tA59'];
                if (strtolower($tA59) != 'with') {
                    $puntos = $puntos - 1;
                }
                $tA60 = $respuesta['tA60'];
                if (strtolower($tA60) != "our") {
                    $puntos = $puntos - 1;
                }

                //Totales
                $puntuacion = $puntos;
                if ($puntuacion <= 29) {
                    $nivel = 3;
                } elseif ($puntuacion >= 30 && $puntuacion <= 39) {
                    $nivel = 4;
                } elseif ($puntuacion >= 40 && $puntuacion <= 49) {
                    $nivel = 5;
                } elseif ($puntuacion >= 50) {
                    $nivel = 6;
                }


                $puntuaciones['puntosA'] = $puntos;
                $r->puntuaciones = $puntuaciones;
                $r->resultado = $puntuacion;
                $r->nivel = $nivel;
            }

            $test['datos'] = $datos;
            $test['respuesta'] = $respuesta;
            $testnivel = json_encode($test);
            $r->respuestas = $testnivel;
            $r->estado = 1;
        }

        if ($form->test == 1 && $form->form == 'testnivelcoloniascic') {
            // Respuestas, Puntuaciones, Resultado, Nivel ???
            $respuesta = $respuestas['respuesta'];
            $datos = unserialize($respuestas['datos']);

            // Correcciones test nivel colonias cic
            // para todos los tests
            $puntos = 0;

            if ($respuesta['p01'] == "b") {
                $puntos++;
            }
            if ($respuesta['p02'] == "d") {
                $puntos++;
            }
            if ($respuesta['p03'] == "b") {
                $puntos++;
            }
            if ($respuesta['p04'] == "c") {
                $puntos++;
            }
            if ($respuesta['p05'] == "d") {
                $puntos++;
            }
            if ($respuesta['p06'] == "b") {
                $puntos++;
            }
            if ($respuesta['p07'] == "a") {
                $puntos++;
            }
            if ($respuesta['p08'] == "a") {
                $puntos++;
            }
            if ($respuesta['p09'] == "b") {
                $puntos++;
            }
            if ($respuesta['p10'] == "c") {
                $puntos++;
            }
            if ($respuesta['p11'] == "d") {
                $puntos++;
            }
            if ($respuesta['p12'] == "a") {
                $puntos++;
            }
            if ($respuesta['p13'] == "d") {
                $puntos++;
            }
            if ($respuesta['p14'] == "d") {
                $puntos++;
            }
            if ($respuesta['p15'] == "c") {
                $puntos++;
            }
            if ($respuesta['p16'] == "d") {
                $puntos++;
            }
            if ($respuesta['p17'] == "a") {
                $puntos++;
            }
            if ($respuesta['p18'] == "d") {
                $puntos++;
            }
            if ($respuesta['p19'] == "a") {
                $puntos++;
            }
            if ($respuesta['p20'] == "d") {
                $puntos++;
            }
            if ($respuesta['p21'] == "b") {
                $puntos++;
            }
            if ($respuesta['p22'] == "a") {
                $puntos++;
            }
            if ($respuesta['p23'] == "b") {
                $puntos++;
            }
            if ($respuesta['p24'] == "a") {
                $puntos++;
            }
            if ($respuesta['p25'] == "c") {
                $puntos++;
            }

            //Totales
            $puntuacion = $puntos;

            if ($datos['testnumber'] == 'B0') {
                if ($puntuacion <= 14) {
                    $nivel = 1;
                } elseif ($puntuacion >= 14 && $puntuacion <= 25) {
                    $nivel = 2;
                }
            }

            if ($datos['testnumber'] == 'B1') {
                if ($puntuacion <= 10) {
                    $nivel = 3;
                } elseif ($puntuacion >= 11 && $puntuacion <= 14) {
                    $nivel = 4;
                } elseif ($puntuacion >= 15 && $puntuacion <= 25) {
                    $nivel = 5;
                }
            }

            if ($datos['testnumber'] == 'B2') {
                if ($puntuacion <= 10) {
                    $nivel = 5;
                } elseif ($puntuacion >= 11 && $puntuacion <= 14) {
                    $nivel = 6;
                } elseif ($puntuacion >= 15 && $puntuacion <= 25) {
                    $nivel = 7;
                }
            }

            if ($datos['testnumber'] == 'B3') {
                if ($puntuacion <= 10) {
                    $nivel = 7;
                } elseif ($puntuacion >= 11 && $puntuacion <= 14) {
                    $nivel = 8;
                } elseif ($puntuacion >= 15 && $puntuacion <= 25) {
                    $nivel = 9;
                }
            }

            $puntuaciones['choicelevel'] = $nivel;
            $puntuaciones['choicepoints'] = $puntuacion;
            $puntuaciones['textlevel'] = 0;
            $r->puntuaciones = $puntuaciones;
            $r->resultado = 0;
            $r->nivel = 0;

            $test['datos'] = $datos;
            $test['respuesta'] = $respuesta;
            $testnivel = json_encode($test);
            $r->respuestas = $testnivel;
            $r->estado = 1;
        }

        if ($form->test == 0) {
            $respuesta = $respuestas['respuesta'];

            $r->respuestas = json_encode($respuesta);
            $r->estado = 1;
        }


        $r->save();
        $test_id = $r->id;

        $m = trans('forms.cuestionarioEnviado');
        Session::flash('mensaje-ok', $m);

        \VCN\Models\Bookings\BookingLog::addLog($booking, "Cuestionario Enviado[$form->name]");

        return redirect()->route('area.forms.resultado', [$cuestionario_id, $booking_id, $test_id]);
    }

    public function getFormResultados($cuestionario_id, $booking_id, $test_id)
    {
        $resultados = CuestionarioRespuesta::find($test_id);
        $estado = $resultados->estado;

        if ($estado == 0) {
            //redireccionar para que vuelvan a rellenar el test/cuestionario
            //eliminar datos mal guardados?
            return redirect()->route('area.forms', [$cuestionario_id, $booking_id]);
        }

        if ($estado > 0) {
            $form = Cuestionario::findOrFail($cuestionario_id);
            $respuestaTest = json_decode($resultados->respuestas);

            if (isset($respuestaTest->datos)) {
                $datos = $respuestaTest->datos;
                $respuesta = $respuestaTest->respuesta;
            } else {
                $datos = '';
                $respuesta = $respuestaTest;
            }

            $view = "area.forms." . $form->form . "-resultados";
            return view($view, compact('respuesta', 'datos', 'estado', 'booking_id', 'form'));
        }
    }

    public function getFamilia(Request $request, $asignacion_id)
    {
        $ficha = BookingFamilia::find($asignacion_id);

        if (!$ficha) {
            abort(404);
        }

        $booking = $ficha->booking;
        ConfigHelper::areaPermiso($request->user(), $booking->viajero);

        $ficha = $ficha->familia;

        $view = 'area.booking-familia';
        return view($view, compact('ficha', 'booking'));
    }

    public function getFamiliaPdf(Request $request, $asignacion_id)
    {
        $ficha = BookingFamilia::find($asignacion_id);

        if (!$ficha) {
            abort(404);
        }

        $booking = $ficha->booking;
        ConfigHelper::areaPermiso($request->user(), $booking->viajero);

        $ficha = $ficha->familia;

        $lang = $booking->idioma_contacto;

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView('area.booking-familia-pdf', compact('ficha', 'booking'));
        $nombre = 'familia-' . str_slug($ficha->name);

        $pdf->setOption('margin-top', 30);
        $pdf->setOption('margin-right', 0);
        $pdf->setOption('margin-bottom', 30);
        $pdf->setOption('margin-left', 0);
        $pdf->setOption('no-print-media-type', false);
        $pdf->setOption('footer-spacing', 0);
        $pdf->setOption('header-font-size', 9);
        $pdf->setOption('header-spacing', 0);
        //$pdf->setOption('javascript-delay',20000);
        $pdf->setOption('header-html', 'https://' . ConfigHelper::config('web') . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo') . 'header.html');
        $pdf->setOption('footer-html', 'https://' . ConfigHelper::config('web') . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo') . 'footer.html');

        //return $pdf->stream("$nombre.pdf");
        return $pdf->download("$nombre.pdf");
    }

    public function getSchool(Request $request, $asignacion_id)
    {
        $ficha = BookingSchool::find($asignacion_id);

        if (!$ficha) {
            abort(404);
        }

        $booking = $ficha->booking;
        ConfigHelper::areaPermiso($request->user(), $booking->viajero);

        $ficha = $ficha->school;

        $view = 'area.booking-school';
        return view($view, compact('ficha', 'booking'));
    }

    public function getSchoolPdf(Request $request, $asignacion_id)
    {
        $idiomaapp = App::getLocale();

        $ficha = BookingSchool::find($asignacion_id);

        if (!$ficha) {
            abort(404);
        }

        $booking = $ficha->booking;
        ConfigHelper::areaPermiso($request->user(), $booking->viajero);

        $ficha = $ficha->school;

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView('area.booking-school-pdf', compact('ficha', 'booking'));
        $nombre = 'school-' . str_slug($ficha->name);

        $pdf->setOption('margin-top', 30);
        $pdf->setOption('margin-right', 0);
        $pdf->setOption('margin-bottom', 30);
        $pdf->setOption('margin-left', 0);
        $pdf->setOption('no-print-media-type', false);
        $pdf->setOption('footer-spacing', 0);
        $pdf->setOption('header-font-size', 9);
        $pdf->setOption('header-spacing', 0);
        //$pdf->setOption('javascript-delay',20000);
        $pdf->setOption('header-html', 'https://' . ConfigHelper::config('web') . '/assets/logos/' . $idiomaapp . '/' . ConfigHelper::config('sufijo') . 'header.html');
        $pdf->setOption('footer-html', 'https://' . ConfigHelper::config('web') . '/assets/logos/' . $idiomaapp . '/' . ConfigHelper::config('sufijo') . 'footer.html');

        // return $pdf->stream("$nombre.pdf");
        return $pdf->download("$nombre.pdf");
    }

    public function getPocketguide(Request $request, $booking_id)
    {
        $booking = Booking::find($booking_id);
        $ficha = $booking->curso;

        ConfigHelper::areaPermiso($request->user(), $booking->viajero);

        $idioma = $booking->viajero->idioma_contacto;

        if (!$ficha) {
            abort(404);
        }


        return view('area.pocketguide', compact('ficha', 'idioma', 'booking'));
    }

    public function getDeseos(Request $request, $curso_id = null)
    {
        $user = ConfigHelper::usuario();

        if ($user && $curso_id) {
            UserDeseo::add($curso_id);
            return redirect()->back();
        }


        return view('area.deseos', compact('user'));
    }

    public function getDeseosBorrar(Request $request, $id)
    {
        UserDeseo::find($id)->delete();

        return redirect()->route('area.deseos');
    }

    public function getFirmar(Request $request, $booking_id)
    {
        $usuario = $request->user();

        $booking = Booking::find($booking_id);

        $pdf = $booking->getArchivoFirmaPendiente();

        return view('area.booking-firma', compact('booking', 'pdf', 'usuario'));
    }

    public function postFirmar(Request $request, $booking_id)
    {
        $user = $request->user();

        $doc_id = (int) $request->get('doc_id');
        $doc = ViajeroArchivo::find($doc_id);
        if (!$doc) {
            Session::flash('mensaje-alert', 'ERROR');
            return redirect()->route('manage.index');
        }

        $booking = Booking::find($booking_id);
        $firmante = $booking->firmante;

        //firma
        $pic = $request->input('signature_b64');

        $dir = storage_path("files/bookings/" . $booking->viajero->id . "/");
        $fichero = $dir . $doc->doc;

        $pdf = new Fpdi\Fpdi();
        // $pdf->AddPage();

        $firmas = $doc->firmas;
        if ($firmas == "pendiente") {
            $firmas = [];
        }

        //Para evitar duplicidad de firmas del mismo firmante
        foreach ($firmas  as $ifirma => $f) {
            if (in_array($firmante, $firmas)) {
                unset($firmas[$ifirma]);
            }
        }

        $firma = [];
        $firma['firmante'] = $firmante;
        $firma['fecha'] = Carbon::now()->format('d/m/Y H:i:s');
        $firma['ip'] = $request->ip();
        $firma['firma'] = $pic;
        $firmas[] = $firma;

        $doc->firmas = $firmas;
        $doc->save();

        //$booking->firmas = $firmas;
        //$booking->save();

        $coord_x = 60;
        if ($firmante == "tutor2") {
            $coord_x = 130;
        }

        switch ($firmante) {
            case 'viajero': {
                    $booking->viajero->firma = $pic;
                    $booking->viajero->save();
                }
                break;

            case 'tutor1': {
                    $booking->viajero->tutor1->firma = $pic;
                    $booking->viajero->tutor1->save();
                }
                break;

            case 'tutor2': {
                    $booking->viajero->tutor2->firma = $pic;
                    $booking->viajero->tutor2->save();
                }
                break;
        }

        $pagesTot = $pdf->setSourceFile($fichero);
        for ($iPage = 1; $iPage <= $pagesTot; $iPage++) {
            // import a page
            $tplId = $pdf->importPage($iPage);

            $pdf->AddPage();
            $pdf->useTemplate($tplId, ['adjustPageSize' => true]);

            if ($iPage == $pagesTot) {
                $pdf->Image($pic, $coord_x, 210, 70, null, 'png');

                $txt = $firma['fecha'] . " por " . $user->ficha->full_name . " [ip::" . $firma['ip'] . "]";
                $txt = iconv('UTF-8', 'windows-1252', $txt);

                $pdf->SetFont('Helvetica', '', 7);
                $pdf->SetXY($coord_x, 265);
                $pdf->Write(0, $txt);
            }
        }
        $pdf->Output("F", $fichero);

        //Ocultamos el resto
        foreach ($booking->pdfs as $p) {
            if ($p->visible) {
                $p->visible = false;
                $p->save();
            }
        }

        $doc->visible = true;
        $doc->save();

        Session::flash('mensaje-ok', trans('area.booking.firmado'));
        return redirect()->route('area.index');
    }


    public function getMonitorBookings(Request $request)
    {
        $user = $request->user();
        $monitor = $user->ficha;

        if ($monitor && !$monitor->es_director) {
            return redirect()->back();
        }

        return view('area.monitores.bookings', compact('monitor'));
    }

    public function postAdjuntar(Request $request, $booking_id)
    {
        $doc_id = (int) $request->get('doc_id');
        $doc_idioma = $request->get('doc_idioma');

        if ($doc_idioma == "0")
        {
            $doc = DocEspecifico::find($doc_id);
        } else {
            // $doc = DocEspecificoLang::find($doc_id);
            $doc = DocEspecificoLang::where('doc_id', $doc_id)->where('idioma', $doc_idioma)->first();
        }

        if (!$doc) {
            Session::flash('mensaje', 'Error doc.');
            return redirect()->route('area.index');
        }

        $booking = Booking::find($booking_id);

        //file
        $file = "";
        if ($request->hasFile('doc_adjunto')) {
            $dir = storage_path("files/viajeros/" . $booking->viajero_id . "/" . $booking_id . "/");

            $file = $request->file('doc_adjunto');
            $file = str_slug($doc->name) . "." . $file->getClientOriginalExtension();

            $request->file('doc_adjunto')->move($dir, $file);
        }
        else
        {
            Session::flash('mensaje', 'Error doc.');
            return redirect()->back();
        }

        //viajeroarchivo
        $va = new ViajeroArchivo;
        $va->viajero_id = $booking->viajero_id;
        $va->booking_id = $booking_id;
        $va->user_id = auth() ? auth()->user()->id : 0;
        $va->fecha = Carbon::now(); //->format('Y-m-d');
        $va->visible = 1;

        $va->doc = "$booking_id/$file";
        $va->notas = "Doc. específico: " . $doc->name;
        $va->doc_especifico_id = $doc_id;
        $va->doc_especifico_status = 0;
        $va->doc_especifico_idioma = $doc_idioma;
        $va->save();

        MailHelper::mailAvisoDocEspecifico($booking, $va->id);

        Session::flash('mensaje-ok', trans('area.doc_ok'));
        return redirect()->route('area.index');
    }

    public function swapLogin(Request $request)
    {
        $user = $request->user();

        $u = $user->user_tutor_viajero;
        if ($u) {
            auth()->login($u);
        }

        return redirect()->route('area.index');
    }

    public function getExams(Request $request)
    {
        $user = $request->user();

        $exams = collect();
        if ($user->es_viajero) {
            $exams = $user->ficha->exam_respuestas;
        } elseif ($user->es_tutor) {
            $vids = $user->ficha->viajeros->pluck('id')->toArray();
            $exams = Respuesta::whereIn('viajero_id', $vids)->get();
        }

        return view('area.exams', compact('user', 'exams'));
    }

    public function getExam(Request $request, Examen $exam, $id = null, $tipo = 'booking')
    {
        $template = "exams." . $exam->template;
        $urlAssets = "/assets/exams/$exam->tema";

        $v = $request->user()->viajero ?: null;
        
        $vid = $id;
        $tipoViajero = ($tipo == "booking") ? false : true;
        if (!$tipoViajero) {
            $b = Booking::find($id);
            $vid = $b ? $b->viajero_id :  0;
        }

        $booking_id = $tipoViajero ? 0 : $id;
        $viajero_id = $vid;

        $respuesta = Examen::getRespuesta($exam, $id, $tipo);
        if ($respuesta && $respuesta->status == -1)
        {
            $respuesta->status = 0;
            $respuesta->save();
        }

        return view($template)->with(compact('exam', 'urlAssets', 'respuesta', 'tipo', 'booking_id', 'viajero_id'));
    }

    public function postExam(Request $request, Examen $exam)
    {
        $res = Examen::postRespuesta($exam, $request);
        // $res->evaluar();

        RespuestaLog::addLog($res, 'Enviada');

        if($res->tipo || !$res->booking_id)
        {
            //viajero
            return redirect()->route('area.exams.ask', [$exam->id, $res->viajero_id, 'viajero']);
        }

        return redirect()->route('area.exams.ask', [$exam->id, $res->booking_id, 'booking']);
    }
}
