<?php

namespace VCN\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;

class MailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  x  $event
     * @return void
     */
    public function handle($event)
    {
        $headers = $event->message->getHeaders();
        $headers->addTextHeader('x-mailgun-native-send', true);
    }
}
