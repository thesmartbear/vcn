<?php namespace VCN\Repositories\Alojamientos;

use VCN\Repositories\LogRepository;

class CuotaRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Alojamientos\AlojamientoCuota';
    }
}