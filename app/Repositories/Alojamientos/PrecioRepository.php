<?php namespace VCN\Repositories\Alojamientos;

use VCN\Repositories\LogRepository;

class PrecioRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Alojamientos\AlojamientoPrecio';
    }
}