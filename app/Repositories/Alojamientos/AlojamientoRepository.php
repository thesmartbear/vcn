<?php namespace VCN\Repositories\Alojamientos;

use VCN\Repositories\LogRepository;

class AlojamientoRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Alojamientos\Alojamiento';
    }
}