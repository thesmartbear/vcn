<?php namespace VCN\Repositories\Leads;

use VCN\Repositories\LogRepository;

class LeadRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Leads\Lead';
    }
}