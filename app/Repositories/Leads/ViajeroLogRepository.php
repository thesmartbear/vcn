<?php namespace VCN\Repositories\Leads;

use Dugajean\Repositories\Eloquent\Repository;
class ViajeroLogRepository extends Repository {

    public function model() {
        return '\VCN\Models\Leads\ViajeroLog';
    }
}