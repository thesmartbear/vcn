<?php namespace VCN\Repositories\Leads;

use VCN\Repositories\LogRepository;

class ViajeroDatosRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Leads\ViajeroDatos';
    }
}