<?php namespace VCN\Repositories\Leads;

use VCN\Repositories\LogRepository;

class ViajeroRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Leads\Viajero';
    }

    public function update(array $data, $id, $attribute = 'id')
    {
        if($id==0)
        {
            return null;
        }

        return parent::update($data,$id,$attribute);
    }
}