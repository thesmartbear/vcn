<?php namespace VCN\Repositories\Leads;

use VCN\Repositories\LogRepository;

class TutorRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Leads\Tutor';
    }
}