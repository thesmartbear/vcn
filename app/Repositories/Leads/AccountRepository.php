<?php namespace VCN\Repositories\Leads;

use VCN\Repositories\LogRepository;

class AccountRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Leads\Account';
    }
}