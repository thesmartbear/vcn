<?php namespace VCN\Repositories\Leads;

use VCN\Repositories\LogRepository;

class ViajeroTareaRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Leads\ViajeroTarea';
    }
}