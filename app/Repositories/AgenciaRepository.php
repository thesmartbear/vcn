<?php namespace VCN\Repositories;

use VCN\Repositories\LogRepository;

class AgenciaRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Agencia';
    }
}