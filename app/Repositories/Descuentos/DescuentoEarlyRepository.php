<?php namespace VCN\Repositories\Descuentos;

use VCN\Repositories\LogRepository;

class DescuentoEarlyRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Descuentos\DescuentoEarly';
    }
}