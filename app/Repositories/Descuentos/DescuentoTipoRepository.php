<?php namespace VCN\Repositories\Descuentos;

use VCN\Repositories\LogRepository;

class DescuentoTipoRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Descuentos\DescuentoTipo';
    }
}