<?php namespace VCN\Repositories\System;

use Dugajean\Repositories\Eloquent\Repository;
class UserRoleRepository extends Repository {

    public function model() {
        return '\VCN\Models\System\UserRole';
    }
}