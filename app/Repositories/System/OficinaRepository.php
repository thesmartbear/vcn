<?php namespace VCN\Repositories\System;

use Dugajean\Repositories\Eloquent\Repository;
class OficinaRepository extends Repository {

    public function model() {
        return '\VCN\Models\System\Oficina';
    }
}