<?php namespace VCN\Repositories\System;

use Dugajean\Repositories\Eloquent\Repository;

class SystemLogRepository extends Repository {

    public function model() {
        return '\VCN\Models\System\SystemLog';
    }
}