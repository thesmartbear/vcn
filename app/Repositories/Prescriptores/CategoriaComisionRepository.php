<?php namespace VCN\Repositories\Prescriptores;

use VCN\Repositories\LogRepository;

class CategoriaComisionRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Prescriptores\CategoriaComision';
    }
}