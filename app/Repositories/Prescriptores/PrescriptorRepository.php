<?php namespace VCN\Repositories\Prescriptores;

use VCN\Repositories\LogRepository;

class PrescriptorRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Prescriptores\Prescriptor';
    }
}