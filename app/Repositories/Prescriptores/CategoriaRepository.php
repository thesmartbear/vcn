<?php namespace VCN\Repositories\Prescriptores;

use VCN\Repositories\LogRepository;

class CategoriaRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Prescriptores\Categoria';
    }
}