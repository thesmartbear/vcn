<?php namespace VCN\Repositories\Criteria;

use Dugajean\Repositories\Criteria\Criteria;
use Dugajean\Repositories\Contracts\RepositoryInterface as Repository;

use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Centros\Centro;
use VCN\Models\Cursos\Curso;
use VCN\Models\Convocatorias\Abierta as ConvocatoriaAbierta;
use VCN\Models\Convocatorias\Cerrada as ConvocatoriaCerrada;

use VCN\Helpers\ConfigHelper;

class FiltroPlataformaConvocatoria extends Criteria {

    protected $abierta = false;
    public function __construct($tipo=false)
    {
        $this->abierta = $tipo;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if( auth()->user()->isFullAdmin() )
            return $model;

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            $proveedores = Proveedor::where('propietario',$filtro)->orWhere('propietario',0)->pluck('id');
            $centros = Centro::whereIn('provider_id',$proveedores)->pluck('id');
            $cursos = Curso::whereIn('center_id',$centros)->pluck('id');

            if($this->abierta)
            {
                $convocatorias = ConvocatoriaAbierta::whereIn('course_id',$cursos)->pluck('id');
            }
            else
            {
                $convocatorias = ConvocatoriaCerrada::whereIn('course_id',$cursos)->pluck('id');
            }


            $model = $model->whereIn('convocatory_id', $convocatorias);
        }

        return $model;
    }
}