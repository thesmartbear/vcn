<?php namespace VCN\Repositories\Criteria;

use Dugajean\Repositories\Criteria\Criteria;
use Dugajean\Repositories\Contracts\RepositoryInterface as Repository;

use VCN\Helpers\ConfigHelper;
use VCN\Models\Leads\Viajero;

class FiltroOficinaViajero extends Criteria {

    private $oficina_id = 0;

    public function __construct($oficina_id)
    {
        $this->oficina_id = $oficina_id;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        // if( auth()->user()->isFullAdmin() )
        //     return $model;

        $filtro = $this->oficina_id;
        if(is_numeric($filtro) && $filtro>0)
        {
            $ofis = Viajero::where('oficina_id',$filtro)->pluck('id');
            $model = $model->whereIn('viajero_id', $ofis);
        }

        return $model;
    }
}