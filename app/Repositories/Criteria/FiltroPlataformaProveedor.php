<?php namespace VCN\Repositories\Criteria;

use Dugajean\Repositories\Criteria\Criteria;
use Dugajean\Repositories\Contracts\RepositoryInterface as Repository;

use VCN\Models\Proveedores\Proveedor;
use VCN\Helpers\ConfigHelper;

class FiltroPlataformaProveedor extends Criteria {

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if( auth()->user()->isFullAdmin() )
            return $model;

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            $proveedores = Proveedor::where('propietario',$filtro)->orWhere('propietario',0)->pluck('id');
            $model = $model->whereIn('provider_id', $proveedores);
        }

        return $model;
    }
}