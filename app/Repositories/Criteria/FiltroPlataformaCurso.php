<?php namespace VCN\Repositories\Criteria;

use Dugajean\Repositories\Criteria\Criteria;
use Dugajean\Repositories\Contracts\RepositoryInterface as Repository;

use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Centros\Centro;
use VCN\Models\Cursos\Curso;

use VCN\Helpers\ConfigHelper;

class FiltroPlataformaCurso extends Criteria {

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if( auth()->user()->isFullAdmin() )
            return $model;

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            $cursos = Curso::where('propietario',$filtro)->orWhere('propietario',0)->pluck('id')->toArray();

            $model = $model->whereIn('course_id', $cursos);
        }

        return $model;
    }
}