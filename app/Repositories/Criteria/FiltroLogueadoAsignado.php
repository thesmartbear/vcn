<?php namespace VCN\Repositories\Criteria;

use Dugajean\Repositories\Criteria\Criteria;
use Dugajean\Repositories\Contracts\RepositoryInterface as Repository;

use VCN\Helpers\ConfigHelper;
use VCN\Models\User;

class FiltroLogueadoAsignado extends Criteria {

    private $user_id = 0;
    private $oficina_id = 0;

    public function __construct($user_id,$oficina_id)
    {
        $this->user_id = $user_id;
        $this->oficina_id = $oficina_id;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        // if( auth()->user()->isFullAdmin() )
        //     return $model;

        $filtro = auth()->user()->id;

        if($this->user_id>0)
        {
            $filtro = $this->user_id;
        }
        elseif($this->user_id === 'null' )
        {
            $model = $model->where('asign_to', null);
            return $model;
        }
        elseif($this->user_id === 'all' )
        {
            if($this->oficina_id>0)
            {
                $ofis = User::asignados()->where('oficina_id',$this->oficina_id)->pluck('id');
                if($ofis)
                {
                    $model = $model->whereIn('asign_to', $ofis);
                }
            }

            return $model;
        }
        elseif($this->user_id === 'all-off' )
        {
            if($this->oficina_id>0)
            {
                $ofis = User::asignadosOff()->where('oficina_id',$this->oficina_id)->pluck('id');
                if($ofis)
                {
                    $model = $model->whereIn('asign_to', $ofis);
                }
            }

            return $model;
        }

        if($filtro)
        {
            $model = $model->where('asign_to', $filtro);
        }

        return $model;
    }
}