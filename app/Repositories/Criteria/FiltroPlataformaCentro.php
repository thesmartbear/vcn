<?php namespace VCN\Repositories\Criteria;

use Dugajean\Repositories\Criteria\Criteria;
use Dugajean\Repositories\Contracts\RepositoryInterface as Repository;

use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Centros\Centro;
use VCN\Helpers\ConfigHelper;

class FiltroPlataformaCentro extends Criteria {

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if( auth()->user()->isFullAdmin() )
            return $model;

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            $proveedores = Proveedor::where('propietario',$filtro)->orWhere('propietario',0)->pluck('id');
            $centros = Centro::whereIn('provider_id',$proveedores)->pluck('id');
            $model = $model->whereIn('center_id', $centros);
        }

        return $model;
    }
}