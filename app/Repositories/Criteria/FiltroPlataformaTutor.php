<?php namespace VCN\Repositories\Criteria;

use Dugajean\Repositories\Criteria\Criteria;
use Dugajean\Repositories\Contracts\RepositoryInterface as Repository;

use VCN\Helpers\ConfigHelper;
// use VCN\Models\User;
// use VCN\Models\Leads\Viajero;
// use VCN\Models\Leads\ViajeroTutor;
// use VCN\Models\Leads\Tutor;

class FiltroPlataformaTutor extends Criteria {

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if( auth()->user()->isFullAdmin() )
            return $model;

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            // $users = User::asignados()->get()->pluck('id');
            // $viajeros = Viajero::whereIn('user_id', $users)->pluck('id');
            // $tutores = ViajeroTutor::whereIn('viajero_id', $viajeros)->pluck('tutor_id');
            // $model = $model->whereIn('id', $tutores);
            $model = $model->where('plataforma',$filtro)->orWhere('plataforma',0);
        }

        return $model;
    }
}