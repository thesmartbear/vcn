<?php namespace VCN\Repositories\Criteria;

use Dugajean\Repositories\Criteria\Criteria;
use Dugajean\Repositories\Contracts\RepositoryInterface as Repository;

use VCN\Models\System\Oficina;
use VCN\Helpers\ConfigHelper;

class FiltroPlataformaOficina extends Criteria {

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if( auth()->user()->isFullAdmin() )
            return $model;

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            $oficinas = Oficina::where('propietario',$filtro)->orWhere('propietario',0)->pluck('id') + [0];
            $model = $model->whereIn('oficina_id', $oficinas);
        }

        return $model;
    }
}