<?php namespace VCN\Repositories\Criteria;

use Dugajean\Repositories\Criteria\Criteria;
use Dugajean\Repositories\Contracts\RepositoryInterface as Repository;

use VCN\Helpers\ConfigHelper;
use VCN\Models\User;

class FiltroPlataformaAsignado extends Criteria {

    private $user_id = 0;

    public function __construct($user_id=0)
    {
        $this->user_id = $user_id;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        // if( auth()->user()->isFullAdmin() )
        //     return $model;

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            if($this->user_id === "all-off")
            {
                $users = User::asignadosOff()->get()->pluck('id');
            }
            else
            {
                $users = User::asignados()->get()->pluck('id');
            }
            
            $model = $model->whereIn('asign_to', $users);
        }

        return $model;
    }
}