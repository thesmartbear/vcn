<?php namespace VCN\Repositories\Criteria;

use Dugajean\Repositories\Criteria\Criteria;
use Dugajean\Repositories\Contracts\RepositoryInterface as Repository;

use VCN\Helpers\ConfigHelper;
// use VCN\Models\User;

class FiltroPlataformaViajero extends Criteria {

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if( auth()->user()->isFullAdmin() )
            return $model;

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            // $users = User::asignados()->get()->pluck('id');
            // $model = $model->whereIn('user_id', $users);
            $model = $model->where('plataforma',$filtro);//->orWhere('plataforma',0);
        }

        return $model;
    }
}