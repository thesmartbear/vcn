<?php namespace VCN\Repositories\Criteria;

use Dugajean\Repositories\Criteria\Criteria;
use Dugajean\Repositories\Contracts\RepositoryInterface as Repository;

use VCN\Helpers\ConfigHelper;
use VCN\Models\Leads\Viajero;

class FiltroPlataformaViajeroRel extends Criteria {

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if( auth()->user()->isFullAdmin() )
            return $model;

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            $viajeros = Viajero::plataforma()->pluck('id');
            $model = $model->whereIn('viajero_id', $viajeros);
        }

        return $model;
    }
}