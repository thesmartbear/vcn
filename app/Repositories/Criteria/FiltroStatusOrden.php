<?php namespace VCN\Repositories\Criteria;

use Dugajean\Repositories\Criteria\Criteria;
use Dugajean\Repositories\Contracts\RepositoryInterface as Repository;

use VCN\Models\Solicitudes\Status;

class FiltroStatusOrden extends Criteria {

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        // if( auth()->user()->isFullAdmin() )
        //     return $model;

        $st = Status::where('orden','>',0)->pluck('id');
        $model = $model->whereIn('status_id', $st);

        return $model;
    }
}