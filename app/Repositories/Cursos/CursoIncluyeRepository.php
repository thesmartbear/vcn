<?php namespace VCN\Repositories\Cursos;

use VCN\Repositories\LogRepository;

class CursoIncluyeRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Cursos\CursoIncluye';
    }
}