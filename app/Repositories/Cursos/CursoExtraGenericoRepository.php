<?php namespace VCN\Repositories\Cursos;

use VCN\Repositories\LogRepository;

class CursoExtraGenericoRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Cursos\CursoExtraGenerico';
    }
}