<?php namespace VCN\Repositories\Cursos;

use VCN\Repositories\LogRepository;

class CursoRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Cursos\Curso';
    }
}