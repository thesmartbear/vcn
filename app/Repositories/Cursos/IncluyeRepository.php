<?php namespace VCN\Repositories\Cursos;

use VCN\Repositories\LogRepository;

class IncluyeRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Cursos\Incluye';
    }
}