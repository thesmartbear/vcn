<?php namespace VCN\Repositories\Cursos;

use VCN\Repositories\LogRepository;

class CursoExtraRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Cursos\CursoExtra';
    }
}