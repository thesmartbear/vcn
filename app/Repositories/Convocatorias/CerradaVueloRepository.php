<?php namespace VCN\Repositories\Convocatorias;

use VCN\Repositories\LogRepository;

class CerradaVueloRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Convocatorias\CerradaVuelo';
    }
}