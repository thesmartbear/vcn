<?php namespace VCN\Repositories\Convocatorias;

use VCN\Repositories\LogRepository;

class ConvocatoriaMultiEspecialidadRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Convocatorias\ConvocatoriaMultiEspecialidad';
    }
}