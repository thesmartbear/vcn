<?php namespace VCN\Repositories\Convocatorias;

use VCN\Repositories\LogRepository;

class ConvocatoriaMultiRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Convocatorias\ConvocatoriaMulti';
    }
}