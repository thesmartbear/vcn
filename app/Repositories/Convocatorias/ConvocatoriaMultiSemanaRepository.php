<?php namespace VCN\Repositories\Convocatorias;

use VCN\Repositories\LogRepository;

class ConvocatoriaMultiSemanaRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Convocatorias\ConvocatoriaMultiSemana';
    }
}