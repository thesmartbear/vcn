<?php namespace VCN\Repositories\Convocatorias;


use VCN\Repositories\LogRepository;

class AbiertaRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Convocatorias\Abierta';
    }
}