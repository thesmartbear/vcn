<?php namespace VCN\Repositories\Convocatorias;

use VCN\Repositories\LogRepository;

class CerradaRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Convocatorias\Cerrada';
    }
}