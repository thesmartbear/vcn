<?php namespace VCN\Repositories\Convocatorias;

use VCN\Repositories\LogRepository;

class PrecioRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Convocatorias\Precio';
    }
}