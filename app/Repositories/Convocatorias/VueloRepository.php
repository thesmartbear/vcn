<?php namespace VCN\Repositories\Convocatorias;

use VCN\Repositories\LogRepository;

class VueloRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Convocatorias\Vuelo';
    }
}