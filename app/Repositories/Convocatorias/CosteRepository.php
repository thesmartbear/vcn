<?php namespace VCN\Repositories\Convocatorias;

use VCN\Repositories\LogRepository;

class CosteRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Convocatorias\Coste';
    }
}