<?php namespace VCN\Repositories\Convocatorias;

use VCN\Repositories\LogRepository;

class OfertaRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Convocatorias\Oferta';
    }
}