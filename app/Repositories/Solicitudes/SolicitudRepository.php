<?php namespace VCN\Repositories\Solicitudes;

use VCN\Repositories\LogRepository;

class SolicitudRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Solicitudes\Solicitud';
    }
}