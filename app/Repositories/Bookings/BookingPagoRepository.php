<?php namespace VCN\Repositories\Bookings;

use VCN\Repositories\LogRepository;

class BookingPagoRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Bookings\BookingPago';
    }
}