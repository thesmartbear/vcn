<?php namespace VCN\Repositories\Bookings;

use VCN\Repositories\LogRepository;

class BookingDescuentoRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Bookings\BookingDescuento';
    }
}