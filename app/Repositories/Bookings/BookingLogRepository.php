<?php namespace VCN\Repositories\Bookings;

use Dugajean\Repositories\Eloquent\Repository;
class BookingLogRepository extends Repository {

    public function model() {
        return '\VCN\Models\Bookings\BookingLog';
    }
}