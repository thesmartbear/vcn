<?php namespace VCN\Repositories\Bookings;

use VCN\Repositories\LogRepository;

class BookingTareaRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Bookings\BookingTarea';
    }
}