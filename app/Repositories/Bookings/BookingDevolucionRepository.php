<?php namespace VCN\Repositories\Bookings;

use VCN\Repositories\LogRepository;

class BookingDevolucionRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Bookings\BookingDevolucion';
    }
}