<?php namespace VCN\Repositories\Bookings;

use VCN\Repositories\LogRepository;

class BookingIncidenciaRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Bookings\BookingIncidencia';
    }
}