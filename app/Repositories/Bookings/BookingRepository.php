<?php namespace VCN\Repositories\Bookings;

use VCN\Repositories\LogRepository;

class BookingRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Bookings\Booking';
    }
}