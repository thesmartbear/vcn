<?php namespace VCN\Repositories\Bookings;

use Dugajean\Repositories\Eloquent\Repository;
class BookingIncidenciaLogRepository extends Repository {

    public function model() {
        return '\VCN\Models\Bookings\BookingIncidenciaLog';
    }
}