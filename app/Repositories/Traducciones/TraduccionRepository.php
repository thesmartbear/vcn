<?php namespace VCN\Repositories\Traducciones;

use Dugajean\Repositories\Eloquent\Repository;

class TraduccionRepository extends Repository {

    public function model() {
        return '\VCN\Models\Traducciones\Traduccion';
    }
}