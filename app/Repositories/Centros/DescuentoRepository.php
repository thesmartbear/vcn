<?php namespace VCN\Repositories\Centros;

use VCN\Repositories\LogRepository;

class DescuentoRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Centros\CentroDescuento';
    }
}