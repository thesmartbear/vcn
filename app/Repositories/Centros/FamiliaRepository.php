<?php namespace VCN\Repositories\Centros;

use VCN\Repositories\LogRepository;

class FamiliaRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Centros\Familia';
    }
}