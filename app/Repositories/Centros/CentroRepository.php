<?php namespace VCN\Repositories\Centros;

use VCN\Repositories\LogRepository;

class CentroRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Centros\Centro';
    }
}