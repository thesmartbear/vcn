<?php namespace VCN\Repositories\Centros;

use VCN\Repositories\LogRepository;

class SchoolRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Centros\School';
    }
}