<?php namespace VCN\Repositories\Centros;

use VCN\Repositories\LogRepository;

class ExtraRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Centros\CentroExtra';
    }
}