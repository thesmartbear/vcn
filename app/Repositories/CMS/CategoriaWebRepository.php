<?php namespace VCN\Repositories\CMS;

use VCN\Repositories\LogRepository;

class CategoriaWebRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\CMS\CategoriaWeb';
    }
}