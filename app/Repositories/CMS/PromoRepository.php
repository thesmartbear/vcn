<?php namespace VCN\Repositories\CMS;

use VCN\Repositories\LogRepository;

class PromoRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\CMS\Promo';
    }
}