<?php namespace VCN\Repositories\CMS;

use VCN\Repositories\LogRepository;

class PaginaRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\CMS\Pagina';
    }
}