<?php namespace VCN\Repositories\CMS;

use VCN\Repositories\LogRepository;

class EspecialidadWebRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\CMS\EspecialidadWeb';
    }
}