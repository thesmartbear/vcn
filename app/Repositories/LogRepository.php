<?php namespace VCN\Repositories;

use VCN\Models\System\SystemLog;
use Dugajean\Repositories\Eloquent\Repository;

abstract class LogRepository extends Repository {

    public function create(array $data)
    {
        $p = parent::create($data);

        SystemLog::addLog($p, "Nuevo");

        return $p;
    }

    public function delete($id)
    {
        $p = $this->find($id);
        SystemLog::addLog($p, "Eliminado");

        parent::delete($id);
    }

    public function update(array $data, $id, $attribute = 'id')
    {
        $p1 = $this->find($id);

        $p = parent::update($data,$id,$attribute);

        $p2 = $this->find($id);

        SystemLog::addLog($p2, "Modificado", $p1);

        return $p;
    }

    public function model(){}
}