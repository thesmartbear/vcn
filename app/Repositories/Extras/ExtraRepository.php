<?php namespace VCN\Repositories\Extras;

use VCN\Repositories\LogRepository;

class ExtraRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Extras\Extra';
    }
}