<?php namespace VCN\Repositories\Proveedores;

use VCN\Repositories\LogRepository;

class ProveedorRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\Proveedores\Proveedor';
    }
}