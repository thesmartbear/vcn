<?php namespace VCN\Repositories;

use VCN\Repositories\LogRepository;

class UserRepository extends LogRepository {

    public function model() {
        return '\VCN\Models\User';
    }
}