<?php

namespace VCN\Models\Monitores;

use Illuminate\Database\Eloquent\Model;

class MonitorRelacion extends Model
{
    protected $table = 'monitor_relaciones';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function monitor()
    {
        return $this->belongsTo('\VCN\Models\Monitores\Monitor', 'monitor_id');
    }

    public static function refrescar($modelo, $modelo_id, $monitores)
    {
        if(!$monitores)
        {
            MonitorRelacion::where('modelo',$modelo)->where('modelo_id',$modelo_id)->delete();
            return;
        }

        foreach($monitores as $id)
        {
            $r = MonitorRelacion::where('modelo',$modelo)->where('modelo_id',$modelo_id)->where('monitor_id',(int)$id)->first();

            if(!$r)
            {
                $data['monitor_id'] = $id;
                $data['modelo'] = $modelo;
                $data['modelo_id'] = $modelo_id;
                MonitorRelacion::create($data);
            }
        }

        //Borramos las desmarcadas:
        MonitorRelacion::where('modelo',$modelo)->where('modelo_id',$modelo_id)->whereNotIn('monitor_id',$monitores)->delete();
    }

    public function getModeloNameAttribute()
    {
        switch($this->modelo)
        {
            case 'Centro':
            {
                $m = \VCN\Models\Centros\Centro::find($this->modelo_id);
            }
            break;

            case 'Curso':
            {
                $m = \VCN\Models\Cursos\Curso::find($this->modelo_id);
            }
            break;

            case 'Abierta':
            {
                $m = \VCN\Models\Convocatorias\Abierta::find($this->modelo_id);
            }
            break;

            case 'Cerrada':
            {
                $m = \VCN\Models\Convocatorias\Cerrada::find($this->modelo_id);
            }
            break;

            case 'ConvocatoriaMulti':
            {
                $m = \VCN\Models\Convocatorias\ConvocatoriaMulti::find($this->modelo_id);
            }
            break;
        }

        return $m->name;
    }

    public function getLinkAttribute()
    {
        switch($this->modelo)
        {
            case 'Centro':
            {
                $ruta = 'manage.centros.ficha';
            }
            break;

            case 'Curso':
            {
                $ruta = 'manage.cursos.ficha';
            }
            break;

            case 'Abierta':
            {
                $ruta = 'manage.convocatorias.abiertas.ficha';
            }
            break;

            case 'Cerrada':
            {
                $ruta = 'manage.convocatorias.cerradas.ficha';
            }
            break;

            case 'ConvocatoriaMulti':
            {
                $ruta = 'manage.convocatorias.multis.ficha';
            }
            break;
        }

        return route($ruta, $this->modelo_id);

    }
}
