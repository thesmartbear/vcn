<?php

namespace VCN\Models\Monitores;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\User;
use VCN\Models\Bookings\Booking;

use ConfigHelper;
use VCN\Helpers\MailHelper;
use Carbon;
use Session;

use VCN\Models\Cursos\Curso;

class Monitor extends Model
{
    protected $table = 'monitores';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function scopePlataforma($query)
    {
        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return $query->where('plataforma',$filtro)->orWhere('plataforma',0);
        }

        return $query;
    }

    public function usuario()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public function relaciones()
    {
        return $this->hasMany('\VCN\Models\Monitores\MonitorRelacion', 'monitor_id');
    }

    public function relaciones_centros()
    {
        return $this->hasMany('\VCN\Models\Monitores\MonitorRelacion', 'monitor_id')->where('modelo','Centro');
    }

    public function relaciones_cursos()
    {
        return $this->hasMany('\VCN\Models\Monitores\MonitorRelacion', 'monitor_id')->where('modelo','Curso');
    }

    public function relaciones_cerradas()
    {
        return $this->hasMany('\VCN\Models\Monitores\MonitorRelacion', 'monitor_id')->where('modelo','Cerrada');
    }

    public function relaciones_abiertas()
    {
        return $this->hasMany('\VCN\Models\Monitores\MonitorRelacion', 'monitor_id')->where('modelo','Abierta');
    }

    public function relaciones_multis()
    {
        return $this->hasMany('\VCN\Models\Monitores\MonitorRelacion', 'monitor_id')->where('modelo','ConvocatoriaMulti');
    }

    public function getBookingsByCentro($id)
    {

        $ret = collect();
        $valores = [];
        $year = Carbon::now()->year;
        // $valores['status'] = ConfigHelper::config('booking_status_prebooking');
        $valores['desdes'] = "01/01/".$year;
        $valores['hastas'] = "31/12/".$year;

        $valores['centros'] = $id;

        $bookings =  Booking::listadoFiltros($valores);

        foreach($bookings->get() as $booking)
        {
            $ret->push($booking);
        }

        $ret = $ret->unique();

        return $ret;
    }

    public function getBookingsByCurso($id)
    {

        $ret = collect();
        $valores = [];
        $year = Carbon::now()->year;
        // $valores['status'] = ConfigHelper::config('booking_status_prebooking');
        $valores['desdes'] = "01/01/".$year;
        $valores['hastas'] = "31/12/".$year;

        $valores['cursos'] = $id;

        $bookings =  Booking::listadoFiltros($valores);

        foreach($bookings->get() as $booking)
        {
            $ret->push($booking);
        }

        $ret = $ret->unique();

        return $ret;
    }

    public function getBookingsByCerrada($id)
    {
        return $this->getBookings()->where('convocatory_close_id',$id);
    }

    public function getBookingsByAbierta($id)
    {
        return $this->getBookings()->where('convocatory_open_id',$id);
    }

    public function getBookingsByMulti($id)
    {
        return $this->getBookings()->where('convocatory_multi_id',$id);
    }


    public function getCursos()
    {
        $cursos = $this->getBookings()->groupBy('curso_id');

        $arr = [];
        foreach($cursos as $c)
        {
            $arr[] = $c->first()->curso_id;
        }

        return Curso::whereIn('id', $arr)->get();
    }

    public function getBookings()
    {
        $ret = collect();

        //Centro, Curso, Convocatoria: Abierta, Cerrada, ConvocatoriaMulti

        $year = Carbon::now()->year;

        foreach($this->relaciones as $rel)
        {
            $valores = [];
            // $valores['status'] = ConfigHelper::config('booking_status_prebooking');
            $valores['desdes'] = "01/01/".$year;
            $valores['hastas'] = "31/12/".$year;

            switch($rel->modelo)
            {
                case 'Centro':
                {
                    $valores['centros'] = $rel->modelo_id;
                }
                break;

                case 'Curso':
                {
                    $valores['cursos'] = $rel->modelo_id;
                }
                break;

                case 'Abierta':
                {
                    $valores['tipoc'] = 3;
                    $valores['convocatorias'] = $rel->modelo_id;
                }
                break;

                case 'Cerrada':
                {
                    $valores['tipoc'] = 5;
                    $valores['convocatorias'] = $rel->modelo_id;
                }
                break;

                case 'ConvocatoriaMulti':
                {
                    $valores['tipoc'] = 4;
                    $valores['convocatorias'] = $rel->modelo_id;
                }
                break;

            }

            $bookings = Booking::listadoFiltros($valores);

            foreach($bookings->get() as $booking)
            {
                $ret->push($booking);
            }
        }

        $ret = $ret->unique();

        return $ret;

    }

    public function getFullNameAttribute()
    {
        return $this->name;
    }

    public function setUsuario($sendMail=true)
    {
        $email = strtolower(trim($this->email));

        if(!$this->usuario && $email) //solo se crea si tienen email
        {
            if($this->plataforma)
            {
                $old = User::where('username',$email)->where('plataforma',$this->plataforma)->first();
            }
            else
            {
                $old = User::where('username',$this->email)->first();
            }

            if($old)
            {
                Session::flash('mensaje-alert', "No se ha podido crear el Usuario. Email en uso.");
                return false;
            }

            if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                return false;
            }

            $ut = new User;
            $ut->fname = $this->name;
            $ut->email = $email;

            $nomuser = $email;
            $passw = str_random(8);

            $ut->email = $nomuser;
            $ut->username = $nomuser;

            $ut->password = bcrypt($passw);
            $ut->roleid = 13;
            $ut->status = 2;
            $ut->plataforma = $this->plataforma;
            $ut->save();

            $this->user_id = $ut->id;
            $this->save();

            if($sendMail)
            {
                MailHelper::mailArea($ut->id,$passw);
            }

            return true;

        }

        return false;
    }
}
