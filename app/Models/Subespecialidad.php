<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;

class Subespecialidad extends Model
{
    protected $table = 'subespecialidades';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function especialidad()
    {
        return $this->belongsTo('\VCN\Models\Especialidad', 'especialidad_id');
    }
}
