<?php

namespace VCN\Models\Alojamientos;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Monedas\Moneda;
use VCN\Models\Extras\ExtraUnidad;

use VCN\Helpers\ConfigHelper;


class AlojamientoCuota extends Model
{
    protected $table = 'alojamiento_cuotas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function alojamiento()
    {
        return $this->belongsTo('\VCN\Models\Alojamientos\Alojamiento', 'accommodation_id');
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda', 'accommodations_quota_currency_id');
    }

    public function getNameAttribute()
    {
        return $this->accommodations_quota_name;
    }

    public function getPrecioAttribute()
    {
        return $this->accommodations_quota_price;
    }

    public function getMonedaAttribute()
    {
        return Moneda::find($this->accommodations_quota_currency_id)->name;
    }

    public function getMonedaIdAttribute()
    {
        return $this->accommodations_quota_currency_id;
    }

    public function getRequeridoAttribute()
    {
        return $this->required;
    }

    public function getTipoUnidadAttribute()
    {
        return $this->cuota_unidad;
    }

    public function getTipoUnidadNameAttribute()
    {
        return ConfigHelper::getTipoUnidad($this->cuota_unidad);
    }

    public function getUnidadAttribute()
    {
        return $this->cuota_unidad_id?ExtraUnidad::find($this->cuota_unidad_id)->name:"-";
    }

    public function getUnidadIdAttribute()
    {
        return $this->cuota_unidad_id?ExtraUnidad::find($this->cuota_unidad_id)->name:"-";
    }
}
