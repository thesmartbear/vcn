<?php

namespace VCN\Models\Alojamientos;

use Illuminate\Database\Eloquent\Model;

use Carbon;
use ConfigHelper;

class AlojamientoPrecioExtra extends Model
{
    protected $table = 'alojamiento_precio_extras';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function alojamiento()
    {
        return $this->belongsTo('\VCN\Models\Alojamientos\Alojamiento', 'alojamiento_id');
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda', 'moneda_id');
    }

    public function getReglaAttribute()
    {
        $ret = "";

        $txt_duracion = ConfigHelper::getPrecioDuracion($this->duracion);

        switch($this->duracion_tipo)
        {

        }

        if($this->duracion>1)
        {
            $ret = $txt_duracion ." por ". ConfigHelper::getPrecioDuracion($this->duracion_fijo);
        }
        else
        {

        }

        return $ret;
    }

    public function getDesdeYearAttribute()
    {
        return Carbon::parse($this->desde)->format('Y');
    }
    public function getDesdeUnitAttribute()
    {
        $du = $this->alojamiento->duracion_unit;

        switch($du)
        {
            case 1: //Semanas
            {
                $s = Carbon::parse($this->desde);

                while($s->isWeekend())
                {
                    $s->addDay();
                }
                $m = $s->format('m');
                $s = $s->format('W');

                if($m==1 && $s>=52) //para dias del principio de año dan semana 52 o 53 del año anterior
                {
                    $s = 1;
                }

                return $s;
            }
            break;

            case 2: //Meses
            {
                return Carbon::parse($this->desde)->format('m');
            }
            break;

            case 3: //Trimestres
            {
                return ceil(Carbon::parse($this->desde)->format('m')/3);
            }
            break;

            case 4: //Semestres
            {
                return ceil(Carbon::parse($this->desde)->format('m')/6);
            }
            break;

            case 5: //Años
            {
                return Carbon::parse($this->desde)->format('Y');
            }
            break;
        }
    }

    public function getHastaYearAttribute()
    {
        return Carbon::parse($this->hasta)->format('Y');
    }
    public function getHastaUnitAttribute()
    {
        $du = $this->alojamiento->duracion_unit;

        switch($du)
        {
            case 1: //Semanas
            {
                $s = (int)Carbon::parse($this->hasta)->format('W');
                if($s<=$this->desde_unit)
                {
                    $m1 = (int)Carbon::parse($this->desde)->format('m');
                    $m2 = (int)Carbon::parse($this->hasta)->format('m');
                    if($m2>$m1)
                    {
                        $s += 52; //a veces la ultima semana de un año da semana 1
                    }
                }

                return $s;
            }
            break;

            case 2: //Meses
            {
                $m = (int)Carbon::parse($this->hasta)->format('m');
                // if($m<$this->desde_unit)
                // {
                //     $m += 12; //siguiente año
                // }

                return $m;
            }
            break;

            case 3: //Trimestres
            {
                $t = (int)ceil(Carbon::parse($this->hasta)->format('m')/3);
                // if($t<$this->desde_unit)
                // {
                //     $t += 12; //siguiente año
                // }

                return $t;
            }
            break;

            case 4: //Semestres
            {
                $s = (int)ceil(Carbon::parse($this->hasta)->format('m')/6);
                // if($s<$this->desde_unit)
                // {
                //     $s += 12; //siguiente año
                // }

                return $s;
            }
            break;

            case 5: //Años
            {
                return (int)Carbon::parse($this->hasta)->format('Y');
            }
            break;
        }
    }

    public function getMonedaNameAttribute()
    {
        return $this->moneda->name;
    }
}
