<?php

namespace VCN\Models\Alojamientos;

use Illuminate\Database\Eloquent\Model;

class AlojamientoTipo extends Model
{
    protected $table = 'alojamiento_tipos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function alojamientos()
    {
        return $this->hasMany('\VCN\Models\Alojamientos\Alojamiento', 'accommodation_type_id');
    }

    public function getNameAttribute()
    {
        return $this->accommodation_type_name;
    }
}
