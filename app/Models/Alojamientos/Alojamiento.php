<?php

namespace VCN\Models\Alojamientos;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Monedas\Moneda;
use VCN\Helpers\ConfigHelper;
use VCN\Models\Alojamientos\AlojamientoPrecio;
use VCN\Models\Alojamientos\AlojamientoPrecioExtra;
use VCN\Models\Bookings\Booking;

use Session;
use Carbon;
use DB;

class Alojamiento extends Model
{
    protected $table = 'alojamientos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function delete()
    {
        if(Booking::where('accommodation_id',$this->id)->count()>0)
        {
            $error = "Imposible eliminar. Existen Bookings con este Alojamiento.";
            Session::flash('mensaje-alert', $error);
            return back();
        }

        parent::delete();
    }

    public function tipo()
    {
        return $this->belongsTo('\VCN\Models\Alojamientos\AlojamientoTipo', 'accommodation_type_id');
    }

    public function centro()
    {
        return $this->belongsTo('\VCN\Models\Centros\Centro', 'center_id');
    }

    public function precios()
    {
        return $this->hasMany('\VCN\Models\Alojamientos\AlojamientoPrecio', 'alojamiento_id');
    }

    public function precio_extras()
    {
        return $this->hasMany('\VCN\Models\Alojamientos\AlojamientoPrecioExtra', 'alojamiento_id');
    }

    public function cuotas()
    {
        return $this->hasMany('\VCN\Models\Alojamientos\AlojamientoCuota', 'accommodation_id');
    }

    public function getCursosAttribute()
    {
        return \VCN\Models\Cursos\Curso::whereRaw(
           'find_in_set(?, course_accommodation)',
           [$this->id]
        )->get();
    }

    public function plaza()
    {
        return $this->hasOne('\VCN\Models\Convocatorias\Plaza', 'alojamiento_id');
    }

    // public function getPlazas($convocatory_id)
    // {
    //     return \VCN\Models\Convocatorias\Plaza::where('alojamiento_id',$this->id)->where('convocatory_id',$convocatory_id)->first();
    // }

    public function getNameAttribute()
    {
        return $this->accommodation_name;
    }

    public function getFullNameAttribute()
    {
        $centro = $this->centro;
        return $this->accommodation_name ." (". $centro->name .")";
    }

    public function getTipoIdAttribute()
    {
        return $this->accommodation_type_id;
    }

    public function getTipoNameAttribute()
    {
        return $this->tipo->accommodation_type_name;
    }

    public function calcularPrecio($desde, $hasta, $duracion, $extras=true)
    {
        $duracion_unit = $this->duracion_unit;

        $costTotal = 0;
        $weeks_numbers = array();

        $moneda = ConfigHelper::config('moneda');
        $moneda_id = Moneda::where('currency_name', $moneda)->first()->id;

        if($this->precios->count()<1)
        {
            return [ 'precio'=> "INCLUÍDO", 'importe'=>0, 'moneda'=> $moneda, 'moneda_id'=> $moneda_id, 'semanas' => 0];
        }

        $start_date = Carbon::createFromFormat('d/m/Y',$desde);
        $end_date = Carbon::createFromFormat('d/m/Y',$hasta);

        $start_date_weeks = Carbon::createFromFormat('d/m/Y',$desde);

        //Array semanas/meses/trimestres/semestres/años
        for ($i = 1; $i < $duracion +1; $i++)
        {
            switch($duracion_unit)
            {
                case 1: //semanas
                {
                    while($start_date_weeks->isWeekend())
                    {
                        $start_date_weeks->addDay();
                    }

                    $start_date_weeks_number['n'] = $start_date_weeks->format('W');
                    $start_date_weeks_number['y'] = $start_date_weeks->format('Y');

                    $start_date_weeks =  $start_date_weeks->addWeek();
                }
                break;

                case 2: //meses
                {
                    $start_date_weeks_number['n'] = $start_date_weeks->format('m');
                    $start_date_weeks_number['y'] = $start_date_weeks->format('Y');

                    $start_date_weeks =  $start_date_weeks->addMonth();
                }
                break;

                case 3: //trimestres
                {
                    $start_date_weeks_number['n'] = ceil($start_date_weeks->format('m')/3);
                    $start_date_weeks_number['y'] = $start_date_weeks->format('Y');

                    $start_date_weeks =  $start_date_weeks->addMonths(3);
                }
                break;

                case 4: //semestres
                {
                    $start_date_weeks_number['n'] = ceil($start_date_weeks->format('m')/6);
                    $start_date_weeks_number['y'] = $start_date_weeks->format('Y');

                    $start_date_weeks =  $start_date_weeks->addMonths(6);
                }
                break;

                case 5: //años
                {
                    $start_date_weeks_number['n'] = $start_date_weeks->format('Y');
                    $start_date_weeks_number['y'] = $start_date_weeks->format('Y');

                    $start_date_weeks =  $start_date_weeks->addYear();
                }
                break;
            }

            array_push($weeks_numbers, $start_date_weeks_number);
        }

        $currentWeekNumber = array();
        $cost = null;
        $bDisponible = false;

        // dd($weeks_numbers);

        $semanas = 0;

        $i = 0;
        foreach($weeks_numbers as $wn)
        {
            $cost = $this->calcularPrecioByDuracion($duracion, $wn);

            if($cost)
            {

                $semanas++;

                $importe = floatval($cost->importe);

                if($cost->duracion==1 && ($cost->rango1 < $duracion)) //monto fijo con duración menor
                {
                    $importe = ceil($duracion/$cost->rango1) * $cost->importe;
                }

                $costTotal += $importe;

                $moneda_id = $cost->moneda_id;
                $moneda = $cost->moneda->name;

                // echo "$i: ". $wn['n'] ."-". $wn['y'] . " + " . $importe . "<br>";

                $bDisponible = true;

                if($cost->duracion==1 && ($cost->rango1 == $duracion))
                {
                    break;
                }
            }

            $i++;
        }

        // dd($cost);

        //Extras
        $cost_extra = null;
        $costTotal_extras = 0;
        $extra_semanas = 0;
        if($bDisponible && $extras)
        {
            foreach($weeks_numbers as $wn)
            {
                $cost_extras = $this->calcularPrecioByDuracion($duracion, $wn, true);

                if($cost_extras)
                {
                    $extra_semanas++;

                    $importe = $cost_extras->importe;

                    if($cost_extras->duracion==1 && ($cost_extras->rango1 < $duracion)) //monto fijo con duración menor
                    {
                        $importe = ceil($duracion/$cost_extras->rango1) * $cost_extras->importe;
                    }

                    $costTotal_extras += $importe;

                    $moneda_id = $cost_extras->moneda_id;
                    $moneda = $cost_extras->moneda->name;

                    // echo $wn['n'] . " + " . $importe . "<br>";

                    if($cost_extras->duracion==1 && ($cost_extras->rango1 == $duracion))
                    {
                        break;
                    }
                }
            }
        }


        if(!$bDisponible)
        {
            $costTotal = 0;
            $costTotal_extras = 0;
        }

        if($cost)
        {
            $values = [ 
                'importe'=> $costTotal + $costTotal_extras, 
                'importe_base'=> $costTotal, 
                'importe_extra'=> $costTotal_extras, 
                'moneda'=> $moneda, 
                'moneda_id'=>$moneda_id, 
                'semanas'=> $semanas, 
                'extra_semanas'=> $extra_semanas, 
                'duracion'=> $this->duracion_name
            ];

            return $values;
        }

        $err = [ 
            'importe'=> 0,
            'importe_base'=> 0,
            'importe_extra'=> 0,
            'moneda'=> $moneda, 
            'moneda_id'=>$moneda_id, 
            'semanas'=> $semanas, 
            'extra_semanas'=> $extra_semanas, 
            'duracion'=> $this->duracion_name
        ];


        return $err;
    }


    private function calcularPrecioByDuracion($duracion, $wn, $extras=false)
    {
        $duracion_unit = $this->duracion_unit;

        //OJO!: se ha cambiado en : $wn['n'] <= $precio->hasta_unit => $wn['n'] < $precio->hasta_unit

        $precios = $extras?$this->precio_extras:$this->precios;

        if($precios->count()<1)
        {
            return null;
        }

        $year = (int)$wn['y'];
        $semana = (int)$wn['n'];

        //Monto fijo (validez + duracion)
        foreach( $precios->where('duracion',1)->where('duracion_fijo',$duracion_unit)->sortByDesc('rango1') as $precio )
        {
            $unit_d = $precio->desde_unit;
            $unit_h = $precio->hasta_unit;
            $year_d = $precio->desde_year;
            $year_h = $precio->hasta_year;

            if($year_d<$year): $unit_d = 1;
            elseif($year_h>$year): $unit_h = 52;
            endif;

            // echo "$tipo (". $precio->id ."): ". $semana ."-". $year ." :: $unit_d ($year_d): $unit_h ($year_h) = $precio->importe: ";
            // echo "if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h ) <br>";

            // if( (($wn['n'] >= $precio->desde_unit)&&($wn['y'] >= $precio->desde_year)) && (($wn['n'] <= $precio->hasta_unit)&&($wn['y'] <= $precio->hasta_year)) )
            if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h )
            {
                // ->where('rango1','=',$duracion)

                if($precio->rango1 == $duracion)
                {
                    return $precio;
                }
            }
        }

        //Monto fijo (validez + < duracion)
        foreach( $precios->where('duracion',1)->where('duracion_fijo',$duracion_unit)->sortByDesc('rango1') as $precio )
        {
            $unit_d = $precio->desde_unit;
            $unit_h = $precio->hasta_unit;
            $year_d = $precio->desde_year;
            $year_h = $precio->hasta_year;

            if($year_d<$year): $unit_d = 1;
            elseif($year_h>$year): $unit_h = 52;
            endif;

            // echo "$tipo (". $precio->id ."): ". $semana ."-". $year ." :: $unit_d ($year_d): $unit_h ($year_h) = $precio->importe: ";
            // echo "if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h ) <br>";

            // if( (($wn['n'] >= $precio->desde_unit)&&($wn['y'] >= $precio->desde_year)) && (($wn['n'] <= $precio->hasta_unit)&&($wn['y'] <= $precio->hasta_year)) )
            if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h )
            {
                // ->where('rango1','=',$duracion)

                if($precio->rango1 < $duracion)
                {
                    return $precio;
                }
            }
        }

        //Rango
        $tipo = 2;
        foreach( $precios->where('duracion_tipo',$tipo)->where('duracion',$duracion_unit+1)->sortByDesc('rango1') as $precio )
        {
            $unit_d = $precio->desde_unit;
            $unit_h = $precio->hasta_unit;
            $year_d = $precio->desde_year;
            $year_h = $precio->hasta_year;

            if($year_d<$year): $unit_d = 1;
            elseif($year_h>$year): $unit_h = 52;
            endif;

            // echo "$tipo (". $precio->id ."): ". $semana ."-". $year ." :: $unit_d ($year_d): $unit_h ($year_h) = $precio->importe: ";
            // echo "if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h ) <br>";

            // if( (($wn['n'] >= $precio->desde_unit)&&($wn['y'] >= $precio->desde_year)) && (($wn['n'] <= $precio->hasta_unit)&&($wn['y'] <= $precio->hasta_year)) )
            if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h )
            {
                // ->where('rango1','<=',$duracion)
                // ->where('rango2','>=',$duracion)
                // ->orderBy('rango1','desc')->first();

                if( ($precio->rango1 <= $duracion) && ($precio->rango2 >= $duracion) )
                {
                    return $precio;
                }
            }
        }

        //A partir de
        $tipo = 1;
        foreach( $precios->where('duracion_tipo',$tipo)->where('duracion',$duracion_unit+1)->sortByDesc('rango1') as $precio )
        {
            $unit_d = $precio->desde_unit;
            $unit_h = $precio->hasta_unit;
            $year_d = $precio->desde_year;
            $year_h = $precio->hasta_year;

            if($year_d<$year): $unit_d = 1;
            elseif($year_h>$year): $unit_h = 52;
            endif;

            // echo "$tipo (". $precio->id ."): ". $semana ."-". $year ." :: $unit_d ($year_d): $unit_h ($year_h) = $precio->importe: ";
            // echo "if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h ) <br>";

            // if( (($wn['n'] >= $precio->desde_unit)&&($wn['y'] >= $precio->desde_year)) && (($wn['n'] <= $precio->hasta_unit)&&($wn['y'] <= $precio->hasta_year)) )
            if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h )
            {
                // ->where('rango1','<',$duracion)
                // ->orderBy('rango1','desc')->first();

                if($precio->rango1 <= $duracion)
                {
                    return $precio;
                }
            }
        }

        //Cualquiera
        $tipo = 0;
        foreach( $precios->where('duracion_tipo',$tipo)->where('duracion',$duracion_unit+1) as $precio )
        {
            $unit_d = $precio->desde_unit;
            $unit_h = $precio->hasta_unit;
            $year_d = $precio->desde_year;
            $year_h = $precio->hasta_year;

            if($year_d<$year): $unit_d = 1;
            elseif($year_h>$year): $unit_h = 52;
            endif;

            // echo "$tipo (". $precio->id ."): ". $semana ."-". $year ." :: $unit_d ($year_d): $unit_h ($year_h) = $precio->importe: ";
            // echo "if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h ) <br>";

            // if( (($wn['n'] >= $precio->desde_unit)&&($wn['y'] >= $precio->desde_year)) && (($wn['n'] <= $precio->hasta_unit)&&($wn['y'] <= $precio->hasta_year)) )
            if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h )
            {
                // echo "$tipo: ". $wn['n'] ."-". $wn['y'] ." :: $precio->desde_unit ($precio->desde_year): $precio->hasta_unit ($precio->hasta_year) = $precio->importe <br>";
                return $precio;
            }
        }

        return null;
    }

    public function getDuracionUnitAttribute()
    {
        $precio = $this->precios()->first();

        if($precio)
        {
            if($precio->duracion == 1)
            {
                return $precio->duracion_fijo;
            }
            else
            {
                return $precio->duracion-1;
            }
        }

        return null;
    }

    public function getDuracionNameAttribute()
    {
        if( is_null($this->duracion_unit) )
        {
            return null;
        }

        return ConfigHelper::getPrecioDuracionUnit($this->duracion_unit);
    }

    public function getDuracionName($units)
    {
        if( is_null($this->duracion_unit) )
        {
            return null;
        }

        return trans_choice("web.curso.".ConfigHelper::getPrecioDuracionUnit($this->duracion_unit), $units);
    }

    public function getExtrasObligatoriosAttribute()
    {
        $ret = collect();
        foreach( $this->cuotas as $extra )
        {
            if($extra->requerido && $extra->tipo_unidad<2)
            {
                $m = $extra->moneda;
                $extra->moneda_txt = ConfigHelper::parseMoneda($extra->precio,$m);
                $ret->push($extra);
            }
        }

        return $ret;
    }

    public function getDivisasAttribute()
    {
        $monedasUsadas = [];

        foreach($this->extras_obligatorios as $extra)
        {
            array_push($monedasUsadas, $extra->moneda_id );
        }
        // array_push($monedasUsadas, $this->accommodation_currency_id);

        $monedasUsadas = array_unique($monedasUsadas);

        return $monedasUsadas;
    }

    public function getDivisasTxtAttribute()
    {
        $monedas2Ret = [];

        $monedas = $this->divisas;

        foreach($monedas as $moneda)
        {
            $m = Moneda::find($moneda);
            if($m)
            {
                $mtxt = $m->name ." : ". ConfigHelper::monedaCambio($moneda,$this->id);

                array_push($monedas2Ret, $mtxt );
            }
        }

        return $monedas2Ret;
    }

}
