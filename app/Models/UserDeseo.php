<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;

use ConfigHelper;
use Session;

class UserDeseo extends Model
{
    protected $table = 'user_deseos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public function curso()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Curso', 'curso_id');
    }

    public static function add($curso_id)
    {
        $user = ConfigHelper::usuario();

        if($user)
        {
            $d = Self::where('curso_id',$curso_id)->first();

            if(!$d)
            {
                $d = array('curso_id'=> $curso_id, 'user_id'=> $user->id);
                Self::create($d);

                Session::flash('mensaje','Curso añadido a favoritos correctamente.');

                return true;
            }
        }

        Session::flash('mensaje','Curso ya estaba añadido a favoritos.');

        return false;
    }
}
