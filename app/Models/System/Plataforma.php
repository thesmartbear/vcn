<?php

namespace VCN\Models\System;

class Plataforma extends \VCN\Models\ModelAuditable
{
    protected $table = 'plataformas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'config' => 'json',
        'status_booking' => 'array',
        'status_solicitud' => 'array',
        'idiomas' => 'array',
        'avisos'    => 'json',
        'tpv'    => 'json',
    ];

    
    public function oficina()
    {
        return $this->belongsTo('\VCN\Models\System\Oficina', 'oficina_id');
    }

    public function catalogos()
    {
        return $this->belongsTo('\VCN\Models\System\Oficina', 'catalogos_oficina_id');
    }

    public function getRazonSocialAttribute()
    {
        return $this->factura_pie;
    }
}
