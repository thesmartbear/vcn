<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Markdown;

use VCN\Helpers\MailHelper;

class SystemMail extends Model
{
    protected $table = 'system_mails';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function idiomas()
    {
        return $this->hasMany('\VCN\Models\System\SystemMailIdioma', 'mail_id');
    }

    public function getLang($lang)
    {
        return $this->idiomas->where('idioma', strtolower($lang))->first();
    }

    public function getDestinoTipoAttribute()
    {
        $arr = ['Sistema', 'Viajero', 'Tutores', 'Viajero y Tutores'];

        $t = $this->destino;

        if (!is_numeric($t))
            return $arr;

        return $arr[$t];
    }

    public function getContenidoAttribute($value)
    {
        if ($value) {
            return $value;
        }

        return $this->blade_contenido;
    }

    public function getBladeAttribute()
    {
        $template = base_path('resources/views/emails/') . $this->blade_file;

        $ret = "";
        foreach (file($template) as $line) {
            $ret .= nl2br($line);
        }

        return $ret;
    }

    public function getBladeFileAttribute()
    {
        return $this->template . ".blade.php";
    }

    /**
     * @return string
     */
    public function getBladeContenidoAttribute()
    {
        $template = base_path('resources/views/emails/') . $this->blade_file;

        $ret = "";
        $bContenido  = false;

        //$content = file_get_contents($template);
        //return $content;

        //markdown
        foreach (file($template) as $line) {
            if (strpos($line, "{{-- Body.ini --}}") !== false) {
                $bContenido  = true;
                continue; //saltamos 1 línea más
            }

            $break = "{{-- Body.end --}}";
            if (strpos($line, $break) !== false) {
                $bContenido  = false;
            }

            if ($bContenido) {
                $ret .= nl2br($line);
            }
        }

        if ($ret != "") {
            return $ret;
        }

        //blade
        foreach (file($template) as $line) {
            if (strpos($line, "@section('contenido')") !== false) {
                $bContenido  = true;
                continue; //saltamos 1 línea más
            }

            if (strpos($line, "@stop") !== false) {
                $bContenido  = false;
            }

            if ($bContenido) {
                $ret .= nl2br($line);
            }
        }

        return $ret;
    }

    public function getBladeRenderAttribute()
    {
        $blade = "emails." . $this->template;

        $blade = "emails.layout_markdown";

        $markdown = new Markdown(view(), config('mail.markdown'));
        $html = $markdown->render($blade);

        return $html;
    }
}
