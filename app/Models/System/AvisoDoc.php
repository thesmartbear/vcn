<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

class AvisoDoc extends Model
{
    protected $table = 'system_aviso_docs';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function avisos()
    {
        return $this->hasMany('\VCN\Models\System\Aviso', 'doc_id');
    }

    public function idiomas()
    {
        return $this->hasMany('\VCN\Models\System\AvisoDocIdioma', 'doc_id');
    }

    public function getLang($lang)
    {
        return $this->idiomas->where('idioma', $lang)->first();
    }

    public function getCampo($campo,$idioma)
    {
        $idioma = strtoupper($idioma);
        return $this->idiomas->where('idioma',$idioma)->first()->$campo;
    }

}

class AvisoDocIdioma extends Model
{
    protected $table = 'system_aviso_doc_idiomas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function doc()
    {
        return $this->belongsTo('\VCN\Models\System\AvisoDoc', 'doc_id');
    }
}
