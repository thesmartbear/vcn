<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

class UserActividad extends Model
{
    protected $table = 'user_actividad';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $dates = ['fecha'];

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }
}
