<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

class DocEspecifico extends Model
{
    protected $table = 'system_doc_especificos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function langs()
    {
        return $this->hasMany('\VCN\Models\System\DocEspecificoLang', 'doc_id');
    }

    public function getLang($idioma)
    {
        return $this->langs->where('idioma', $idioma)->first();
    }

    /**
     *
     */
    public function getModeloClaseAttribute()
    {
        if($this->modelo == "Curso")
        {
            return \VCN\Models\Cursos\Curso::find($this->modelo_id);
        }

        if($this->modelo == "Categoria")
        {
            return \VCN\Models\Categoria::find($this->modelo_id);
        }

        if($this->modelo == "Subcategoria")
        {
            return \VCN\Models\Subcategoria::find($this->modelo_id);
        }

        return null;
    }

    public function getModeloRouteAttribute()
    {
        if($this->modelo == "Curso")
        {
            return "manage.cursos.ficha";
        }

        if($this->modelo == "Categoria")
        {
            return "manage.categorias.ficha";
        }

        if($this->modelo == "Subcategoria")
        {
            return "manage.subcategorias.ficha";
        }

        return null;
    }

    public function booking()
    {
        //por el informe booking_id es un apaño
        return $this->belongsTo('\VCN\Models\Bookings\Booking', 'booking_id');
    }
}
