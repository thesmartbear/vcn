<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\Tutor;
use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Cursos\Curso;
use VCN\Models\Centros\Centro;
use VCN\Models\Convocatorias\Abierta as ConvocatoriaAbierta;
use VCN\Models\Convocatorias\Cerrada as ConvocatoriaCerrada;
use VCN\Models\Convocatorias\ConvocatoriaMulti;

use Auth;
use ConfigHelper;
use Log;

class UserRole extends Model
{
    protected $table = 'user_roles';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public static function permisoPlataforma($permiso, $Modelo, $id)
    {
        if(Auth::user()->isFullAdmin())
            return true;

        if( Auth::user()->plataforma )
        {
            $p = Auth::user()->plataforma;

            switch($permiso)
            {
                case 'viajeros':
                {
                    $o = Viajero::find($id);
                    if($o)
                    {
                        if($o->plataforma == $p)
                            return true;
                    }
                    return false;
                }
                break;

                case 'tutores':
                    {
                        $o = Tutor::find($id);
                        if($o)
                        {
                            if($o->plataforma == $p)
                                return true;
                        }
                        return false;
                    }
                    break;

                case 'bookings':
                {
                    $o = Booking::find($id);
                    if($o)
                    {
                        if($o->plataforma == $p)
                            return true;
                    }
                    return false;
                }
                break;

                case 'precios':
                case 'proveedores':
                {
                    $o = $Modelo::find($id);
                    if($o)
                    {
                        if(!$o->propietario)
                        {
                            return true; //no hay restriccion
                        }

                        if($o->propietario == $p || $o->propietario==0)
                        {
                            return true;
                        }
                    }
                    return false;
                }
                break;



                /*
                case 'cursos':
                {
                    $o = Curso::find($id);
                    if($o)
                    {
                        if($o->proveedor->propietario == $p || $o->proveedor->propietario==0)
                        {
                            return true;
                        }
                    }
                    return false;
                }
                break;

                case 'centros':
                {
                    $o = Centro::find($id);
                    if($o)
                    {
                        if($o->proveedor->propietario == $p || $o->proveedor->propietario==0)
                        {
                            return true;
                        }
                    }
                    return false;
                }
                break;

                case 'convocatoria-cerradas':
                {
                    $o = ConvocatoriaCerrada::find($id);
                    if($o)
                    {
                        if($o->curso->proveedor->propietario == $p || $o->curso->proveedor->propietario==0)
                        {
                            return true;
                        }
                    }
                    return false;
                }
                break;

                case 'convocatoria-abiertas':
                {
                    $o = ConvocatoriaAbierta::find($id);
                    if($o)
                    {
                        if($o->curso->proveedor->propietario == $p || $o->curso->proveedor->propietario==0)
                        {
                            return true;
                        }
                    }
                    return false;
                }
                break;

                case 'convocatoria-multi':
                {
                    $o = ConvocatoriaMulti::find($id);
                    if($o)
                    {
                        if($o->propietario == $p || $o->propietario==0)
                        {
                            return true;
                        }
                    }
                    return false;
                }
                break;
                */

            }
        }

        return false;
    }

    private static function getPermisos()
    {
        $r = Auth::user()->role;

        // $r = \VCN\Models\System\UserRole::find(20);//pruebas
        // $rp = json_decode( $r->permisos );
        // // dd($rp->informes);

        if( $r )
        {
            return json_decode( $r->permisos );
        }

        return false;
    }

    public static function permisoInforme($ruta)
    {
        $user = Auth::user();

        if($user->isFullAdmin())
            return true;

        if($user->es_aislado)
            return false;

        $pos = strpos($ruta, "manage.informes.filtros.ajax");
        if( $pos === 0)
        {
            return true;
        }

        if(strpos($ruta, '.pdf'))
        {
            $ruta = str_replace('.pdf', '', $ruta);
        }

        $pos = strpos($ruta,'manage.informes');
        if( $pos === false)
        {
            return true;
        }

        if($ruta[0] == "#")
        {
            return true;
        }

        $permisos = self::getPermisos();

        $permiso = ConfigHelper::menuInformePermiso($ruta);
        $bloque = substr($permiso, 0, strpos($permiso,'.'));
        $detalle = substr($permiso, strpos($permiso,'.')+1);

        if($permisos && isset($permisos->informes) && $permisos->informes)
        {
            //Todos
            if( (int)$permisos->informes->view )
            {
                return true;
            }

            if( !isset($permisos->informes->bloques) )
            {
                return false;
            }

            if( !isset($permisos->informes->bloques->$bloque) )
            {
                return false;
            }

            //Bloques
            if( (int)$permisos->informes->bloques->$bloque->view )
            {
                return true;
            }

            // Detalle
            if( !isset($permisos->informes->bloques->$bloque->detalles) )
            {
                return false;
            }

            if( !isset($permisos->informes->bloques->$bloque->detalles->$permiso) )
            {
                return false;
            }

            if( (int)$permisos->informes->bloques->$bloque->detalles->$permiso)
            {
                return true;
            }
        }

        return false;
    }

    public static function permisoView($permiso)
    {
        if(Auth::user()->isFullAdmin())
            return true;

        if($permiso=='all')
            return true;

        $permisos = self::getPermisos();

        if(!$permisos)
            return false;

        if( isset($permisos->$permiso) )
        {
            if($permisos->$permiso->view)
            {
                return true;
            }
        }

        return false;
    }

    public static function permisoEdit($permiso)
    {
        if(Auth::user()->isFullAdmin())
            return true;

        $permisos = self::getPermisos();

        if(!$permisos)
            return false;

        if( isset($permisos->$permiso) )
        {
            if($permisos->$permiso->edit)
            {
                return true;
            }
        }

        return false;
    }

    public static function permisoDelete($permiso)
    {
        if(Auth::user()->isFullAdmin())
            return true;

        $permisos = self::getPermisos();

        if(!$permisos)
            return false;

        if( isset($permisos->$permiso) )
        {
            if($permisos->$permiso->delete)
            {
                return true;
            }
        }

        return false;
    }

}
