<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Leads\ViajeroLog;
use VCN\Models\Bookings\BookingLog;

use Auth;

class SystemLog extends Model
{
    protected $table = 'system_logs';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    // protected $casts = [
    //     'notas' => 'array',
    // ];

    public static function addLog( $objeto, $tipo, $viejo=null)
    {
        $modelo = class_basename(get_class($objeto));

        $log = new SystemLog;
        $log->modelo = $modelo;
        $log->modelo_id = $objeto->id;
        $log->tipo = $tipo;
        $log->user_id = Auth::user()->id;

        // $log->notas = json_encode( $objeto->toArray() );

        if($viejo)
        {
            // $log->notas = json_encode( array_diff( $objeto->toArray(), $viejo->toArray() ) );
            $log->notas = json_encode( array_diff( $objeto->attributes, $viejo->attributes ) );
        }
        else
        {
            $log->notas = json_encode( $objeto->toArray() );
        }

        $log->save();

    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

}
