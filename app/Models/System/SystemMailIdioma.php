<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

class SystemMailIdioma extends Model
{
    protected $table = 'system_mail_idiomas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function mail()
    {
        return $this->belongsTo('\VCN\Models\System\SystemMail', 'mail_id');
    }

    public function getContenidoAttribute($value)
    {
        if($value)
        {
            return $value;
        }

        return $this->blade_contenido;
    }

    public function getBladeAttribute()
    {
        $template = base_path('resources/views/emails/') . $this->blade_file;

        $ret = "";
        foreach( file($template) as $line)
        {
            $ret .= nl2br($line);
        }

        return $ret;
    }

    public function getBladeFileAttribute()
    {
        return $this->idioma ."/". $this->mail->template . ".blade.php";
    }

    /**
     * @return string
     */
    public function getBladeContenidoAttribute()
    {
        $template = base_path('resources/views/emails/') . $this->blade_file;

        $ret = "";
        $bContenido  = false;

        //markdown
        foreach( file($template) as $line)
        {
            if( strpos($line, "{{-- Body.ini --}}") !== false )
            {
                $bContenido  = true;
                continue; //saltamos 1 línea más
            }

            $break = "{{-- Body.end --}}";
            if( strpos($line, $break) !== false )
            {
                $bContenido  = false;
            }

            if($bContenido)
            {
                $ret .= nl2br($line);
            }
        }

        if($ret != "")
        {
            return $ret;
        }

        //blade
        foreach( file($template) as $line)
        {
            if( strpos($line, "@section('contenido')") !== false )
            {
                $bContenido  = true;
                continue; //saltamos 1 línea más
            }

            if( strpos($line, "@stop") !== false )
            {
                $bContenido  = false;
            }

            if($bContenido)
            {
                $ret .= nl2br($line);
            }
        }

        return $ret;
    }

    public function getBladeRenderAttribute()
    {
        $blade = "emails.". $this->idioma .".". $this->mail->template;

        $blade = "emails.layout_markdown";

        $markdown = new Markdown(view(), config('mail.markdown'));
        $html = $markdown->render($blade);

        return $html;
    }

}
