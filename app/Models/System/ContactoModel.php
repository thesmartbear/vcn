<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

class ContactoModel extends Model
{
    protected $table = 'system_contacto_models';
    // protected $fillable = [];
    protected $guarded = ['_token'];

}