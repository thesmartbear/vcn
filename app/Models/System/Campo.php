<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

class Campo extends \VCN\Models\ModelAuditable
{
    protected $table = 'system_campos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'traduccion' => 'json',
    ];

}
