<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\System\SystemLog;

use Auth;
use ConfigHelper;

class Documento extends Model
{
    protected $table = 'documentos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public function getPlataformaNameAttribute()
    {
        return ConfigHelper::plataforma($this->plataforma);
    }

}
