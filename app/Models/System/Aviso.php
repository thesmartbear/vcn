<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Status as BookingStatus;
use VCN\Models\Bookings\Booking;

use VCN\Models\System\AvisoLog;
use VCN\Models\Leads\Viajero;

use ConfigHelper;
use Carbon;
use DB;
use Mail;
use NZTim\Mailchimp\Mailchimp;


class Aviso extends \VCN\Models\ModelAuditable
{
    protected $table = 'system_avisos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'filtros' => 'array',
        'filtros_not' => 'array',
    ];


    public function doc()
    {
        return $this->belongsTo('\VCN\Models\System\AvisoDoc', 'doc_id');
    }

    public function logs()
    {
        return $this->hasMany('\VCN\Models\System\AvisoLog', 'aviso_id');
    }

    public function scopePlataforma($query)
    {
        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return $query->where('plataforma',$filtro);//->orWhere('plataforma',0);
        }

        return $query;
    }

    public function getFechaAttribute()
    {
        return $this->cuando_dia ."/". $this->cuando_mes ."/". $this->cuando_any;
    }

    public function getFechaHoraAttribute()
    {
        return $this->cuando_dia ."/". $this->cuando_mes ."/". $this->cuando_any ." ". $this->cuando_hora;
    }

    public function getEjecutableLogAttribute()
    {
        // $log = AvisoLog::where('aviso_id',$this->id)->where( DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"),'=', Carbon::today()->format('Y-m-d') )->first();
        $log = AvisoLog::where('aviso_id',$this->id)->where( DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d %H:%i')"),'=', Carbon::today()->format('Y-m-d H:i') )->first();

        return is_null($log);
    }

    public function getCuandoDetallesAttribute()
    {
        $ret = "";

        switch($this->cuando_tipo)
        {
            case 'trigger':
            {
                $status = BookingStatus::find($this->cuando_trigger);

                $ret .= $this->cuando_tipo?ConfigHelper::getAvisoCuandoTipo($this->cuando_tipo):"-";
                $ret .= ": ". ($status?$status->name:"-");
            }
            break;

            case 'tarea':
            {
                $ret .= ConfigHelper::getAvisoCuandoTipo($this->cuando_tipo);
                $ret .= ": ". BookingStatus::find($this->cuando_trigger)->name;
                $ret .= " :: $this->cuando_dia después a las $this->cuando_hora";
            }
            break;

            case 'puntual':
            {
                $ret .= "Puntual: ". $this->fecha_hora;
            }
            break;

            case 'dia':
            {

            }
            break;

            case 'semana':
            {

            }
            break;

            case 'mes':
            {

            }
            break;
        }

        return $ret;
    }

    public function getEjecutableQuedaAttribute()
    {
        $timezone = config('app.timezone');

        // if(!$this->ejecutable_log) //Ya se ha ejecutado
        // {
        //     return 0;
        // }

        switch($this->cuando_tipo)
        {
            case 'trigger':
            {
                return 0;
            }
            break;

            case 'puntual':
            {
                $fecha = Carbon::createFromFormat('d/m/Y H:i:s',$this->fecha_hora);
                $now = Carbon::now($timezone);

                $queda = $fecha->diffInDays($now, false);
                if($queda>0) //ya ha pasado el día
                {
                    return 0;
                }

                $queda = $fecha->diffInSeconds($now, false);

                if($queda)
                {
                    return $queda;
                }

                return 0;
            }
            break;
        }

        return 0;
    }

    public function getEsCaducadoAttribute()
    {
        return $this->ejecutable_queda>=0;
    }

    public function getEjecutableAttribute()
    {
        $timezone = config('app.timezone');

        if(!$this->ejecutable_log) //Ya se ha ejecutado
        {
            return false;
        }

        switch($this->cuando_tipo)
        {
            case 'trigger':
            {
                return true;
            }
            break;

            case 'puntual':
            {
                $fecha = Carbon::createFromFormat('d/m/Y H:i:s',$this->fecha_hora);
                $now = Carbon::now($timezone);

                $queda = $fecha->diffInSeconds($now,false);

                if($queda>0 && $queda<60)
                {
                    return true;
                }

                return false;
            }
            break;

            /*case 'dia':
            {
                $now = Carbon::now($timezone)->format('H');

                if($now == $this->cuando_hora)
                {
                    return true;
                }

                return false;
            }
            break;

            case 'semana':
            {
                $now = Carbon::now($timezone)->dayOfWeek;

                if($now == $this->cuando_dia)
                {
                    $nowH = Carbon::now($timezone)->format('H');
                    if($nowH == $this->cuando_hora)
                    {
                        return true;
                    }
                }

                return false;
            }
            break;

            case 'mes':
            {
                $now = Carbon::now($timezone)->format('d');

                if($now == $this->cuando_dia)
                {
                    $nowH = Carbon::now($timezone)->format('H');
                    if($nowH == $this->cuando_hora)
                    {
                        return true;
                    }
                }

                return false;
            }
            break;*/
        }

        return false;
    }

    public function procesarLOPD($bookings=null, $test_id)
    {
        ini_set('memory_limit', '500M');
        ini_set('max_execution_time', 10000);

        $iSend = 0;
        $iTot = 0;
        
        $log = new AvisoLog;
        $log->aviso_id = $this->id;
        $log->save();

        //Sin booking
        if($this->doc_id == 4)
        {
            $iTotV = 0;
            $iTotT = 0;

            //Viajeros
            $viajeros = \VCN\Models\Leads\Viajero::where('booking_id',0);
            if($test_id)
            {
                $viajeros = \VCN\Models\Leads\Viajero::where('id',$test_id);
            }

            $total = $viajeros->count();

            $p1 = \VCN\Models\System\Plataforma::find(1);
            $p2 = \VCN\Models\System\Plataforma::find(2);

            $plat1 = $p1->name;
            $plat2 = $p2->name;

            $asunto_p1_es = "¿Seguimos en contacto?";
            $asunto_p1_ca = "Seguim en contacte?";

            $asunto_p2_es = "¿Seguimos en contacto?";
            $asunto_p2_ca = "Seguim en contacte?";

            $pages = round($total/100)+1;
            for ($i=1; $i <= $pages; $i++)
            {
                $todos = $viajeros->forPage($i, 100);
        
                foreach($todos->get() as $viajero)
                {
                    if($viajero->bookings_lopd->count())
                    {
                        continue;
                    }

                    $idioma = $viajero->idioma_contacto;

                    $p = "p". ($viajero->plataforma?:1);
                    $asunto = "asunto_p". ($viajero->plataforma ?:1) ."_".$idioma;

                    $template = "emails.$idioma.lopd_sin_booking";

                    $asunto = $$asunto;

                    $destino = $viajero;
                    // if($destino && !$destino->yaAviso($this->id))
                    {
                        // Viajero
                        if($viajero->id == $test_id)
                        {
                            echo "V:$destino->id ($destino->email) :: ";

                            if(filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
                            {
                                Mail::send($template,['ficha' => $destino, 'plataforma'=> $$p], function ($m) use ($destino, $asunto) {
                                    $p = $destino->plataforma;
                                    if($p)
                                    {
                                        $plataforma = \VCN\Models\System\Plataforma::find($p);
                                        if($plataforma)
                                        {
                                            $m->from($plataforma->email, $plataforma->name);
                                        }
                                    }
                                    $m->to($destino->email, $destino->full_name)->subject($asunto);
                                });

                                echo " OK ";
                            }
                        }
                        else
                        
                        // if(filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
                        {
                            try {
                                // echo "$destino->email,";
                                $job = new \VCN\Jobs\JobMailer($template,['ficha' => $destino, 'plataforma'=> $$p], $destino, $asunto);
                                dispatch($job);
                                $iSend++;

                                \VCN\Models\Leads\ViajeroLog::addLog($destino, "Aviso[$this->id]", $this->name);
                                $iTotV++;

                            }catch(\Exception $e){}

                            $destino->optout = 1;
                            $destino->optout_fecha = Carbon::now();
                            $destino->save();
                        }

                        //Tutor1
                        $destino = $viajero->tutor1;
                        if($destino && !$destino->yaAviso($this->id))
                        {
                            if($viajero->id == $test_id)
                            {
                                echo "T1:$destino->id ($destino->email) :: ";

                                if(filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
                                {
                                    Mail::send($template,['ficha' => $destino, 'plataforma'=> $$p], function ($m) use ($destino, $asunto) {
                                        $p = $destino->plataforma;
                                        if($p)
                                        {
                                            $plataforma = \VCN\Models\System\Plataforma::find($p);
                                            if($plataforma)
                                            {
                                                $m->from($plataforma->email, $plataforma->name);
                                            }
                                        }
                                        $m->to($destino->email, $destino->full_name)->subject($asunto);
                                    });

                                    echo " OK ";
                                }
                            }
                            else
                            
                            // if(filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
                            {
                                try {

                                    // echo "$destino->email,";
                                    $job = new \VCN\Jobs\JobMailer($template,['ficha' => $destino, 'plataforma'=> $$p], $destino, $asunto);
                                    dispatch($job);
                                    $iSend++;

                                    \VCN\Models\Leads\TutorLog::addLog($destino, "Aviso[$this->id]", $this->name);
                                    $iTotT++;

                                }catch(\Exception $e){}

                                $destino->optout = 1;
                                $destino->optout_fecha = Carbon::now();
                                $destino->save();
                            }
                        }

                        //Tutor2
                        $destino = $viajero->tutor2;
                        if($destino && !$destino->yaAviso($this->id))
                        {
                            if($viajero->id == $test_id)
                            {
                                echo "T2:$destino->id ($destino->email) :: ";

                                if(filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
                                {
                                    Mail::send($template,['ficha' => $destino, 'plataforma'=> $$p], function ($m) use ($destino, $asunto) {
                                        $p = $destino->plataforma;
                                        if($p)
                                        {
                                            $plataforma = \VCN\Models\System\Plataforma::find($p);
                                            if($plataforma)
                                            {
                                                $m->from($plataforma->email, $plataforma->name);
                                            }
                                        }
                                        $m->to($destino->email, $destino->full_name)->subject($asunto);
                                    });

                                    echo " OK ";
                                }
                            }
                            else
                            
                            // if(filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
                            {
                                try {

                                    // echo "$destino->email,";
                                    $job = new \VCN\Jobs\JobMailer($template,['ficha' => $destino, 'plataforma'=> $$p], $destino, $asunto);
                                    dispatch($job);
                                    $iSend++;

                                    \VCN\Models\Leads\TutorLog::addLog($destino, "Aviso[$this->id]", $this->name);
                                    $iTotT++;

                                }catch(\Exception $e){}

                                $destino->optout = 1;
                                $destino->optout_fecha = Carbon::now();
                                $destino->save();
                            }
                        }                        
                    }
                }
            }

            if($viajero->id == $test_id)
            {
                echo "FIN: $test_id";
                return;
            }
            
            $notas = "Sin Bookings [$total]: Viajeros: $iTotV :: Tutores: $iTotT";
            $log->notas = $notas;
            $log->enviados = $iSend;
            $log->save();

            return;
        }

        // if($this->doc_id == 3) //Bookings
        if(!$bookings)
        {
            // if($test_id)
            // {
            //     $bookings = \VCN\Models\Bookings\Booking::where('id', $test_id);
            // }
            // else
            {
                $bookings = \VCN\Models\Bookings\Booking::bookingAvisos($this->filtros,$this->filtros_not);

                $st = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id')->toArray();
                $sto = [ConfigHelper::config('booking_status_overbooking')];
                $st = array_merge($st,$sto);

                $bookings = $bookings->whereIn('status_id',$st);
            }
        }
        $bookings->orderBy('course_start_date','DESC');

        $total = $bookings->count();

        $p1 = \VCN\Models\System\Plataforma::find(1);
        $p2 = \VCN\Models\System\Plataforma::find(2);

        $plat1 = $p1->name;
        $plat2 = $p2->name;

        $asunto_p1_es = "¿Seguimos en contacto?";
        $asunto_p1_ca = "Seguim en contacte?";

        $asunto_p2_es = "¿Seguimos en contacto?";
        $asunto_p2_ca = "Seguim en contacte?";
        
        $asunto_booking_es = "IMPORTANTE: necesitamos tu autorización";
        $asunto_booking_ca = "IMPORTANT: ens cal la teva autorització";

        $pages = round($total/100)+1;
        for ($i=1; $i <= $pages; $i++)
        { 
            $todos = $bookings->forPage($i, 100);
        
            foreach($todos->get() as $booking)
            {
                $idioma = $booking->viajero->idioma_contacto;

                $p = "p".$booking->plataforma;
                $asunto = "asunto_p".$booking->plataforma."_".$idioma;

                $template = "emails.$idioma.lopd_sin_booking";
                if($booking->any == 2018)
                {
                    $template = "emails.$idioma.lopd_booking";
                    $asunto = "asunto_booking_".$idioma;
                }

                $asunto = $$asunto;

                $destino = $booking->viajero;
                if($destino && !$destino->yaAviso($this->id))
                {
                    // if($booking->id == $test_id)
                    // {
                    //     if(filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
                    //     {
                    //         Mail::send($template,['ficha' => $destino, 'booking'=> $booking, 'plataforma'=> $$p], function ($m) use ($destino, $asunto) {
                    //             $p = $destino->plataforma;
                    //             if($p)
                    //             {
                    //                 $plataforma = \VCN\Models\System\Plataforma::find($p);
                    //                 if($plataforma)
                    //                 {
                    //                     $m->from($plataforma->email, $plataforma->name);
                    //                 }
                    //             }
                    //             $m->to($destino->email, $destino->full_name)->subject($asunto);
                    //         });
                    //     }
                    // }
                    // else
                    
                    if(filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
                    {
                        try {
                            // echo "$destino->email,";
                            $job = new \VCN\Jobs\JobMailer($template,['ficha' => $destino, 'booking'=> $booking, 'plataforma'=> $$p], $destino, $asunto);
                            dispatch($job);
                            $iSend++;

                            \VCN\Models\Leads\ViajeroLog::addLog($destino, "Aviso[$this->id]", $this->name);
                        
                        }catch(\Exception $e){}

                        $destino->optout = 1;
                        $destino->optout_fecha = Carbon::now();
                        $destino->save();
                    }
                }

                $destino = $booking->viajero->tutor1;
                if($destino && !$destino->yaAviso($this->id))
                {
                    // if($booking->id == $test_id)
                    // {
                    //     if(filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
                    //     {
                    //         Mail::send($template,['ficha' => $destino, 'booking'=> $booking, 'plataforma'=> $$p], function ($m) use ($destino, $asunto) {
                    //             $p = $destino->plataforma;
                    //             if($p)
                    //             {
                    //                 $plataforma = \VCN\Models\System\Plataforma::find($p);
                    //                 if($plataforma)
                    //                 {
                    //                     $m->from($plataforma->email, $plataforma->name);
                    //                 }
                    //             }
                    //             $m->to($destino->email, $destino->full_name)->subject($asunto);
                    //         });
                    //     }
                    // }
                    // else
                    
                    if(filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
                    {
                        try {
                            // echo "$destino->email,";
                            $job = new \VCN\Jobs\JobMailer($template,['ficha' => $destino, 'booking'=> $booking, 'plataforma'=> $$p], $destino, $asunto);
                            dispatch($job);
                            $iSend++;

                            \VCN\Models\Leads\TutorLog::addLog($destino, "Aviso[$this->id]", $this->name);

                        }catch(\Exception $e){}

                        $destino->optout = 1;
                        $destino->optout_fecha = Carbon::now();
                        $destino->save();
                    }

                }

                $destino = $booking->viajero->tutor2;
                if($destino && !$destino->yaAviso($this->id))
                {
                    // if($booking->id == $test_id)
                    // {
                    //     if(filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
                    //     {
                    //         Mail::send($template,['ficha' => $destino, 'booking'=> $booking, 'plataforma'=> $$p], function ($m) use ($destino, $asunto) {
                    //             $p = $destino->plataforma;
                    //             if($p)
                    //             {
                    //                 $plataforma = \VCN\Models\System\Plataforma::find($p);
                    //                 if($plataforma)
                    //                 {
                    //                     $m->from($plataforma->email, $plataforma->name);
                    //                 }
                    //             }
                    //             $m->to($destino->email, $destino->full_name)->subject($asunto);
                    //         });
                    //     }
                    // }
                    // else
                    
                    if(filter_var(($destino->email), FILTER_VALIDATE_EMAIL))
                    {
                        try {
                            // echo "$destino->email,";
                            $job = new \VCN\Jobs\JobMailer($template,['ficha' => $destino, 'booking'=> $booking, 'plataforma'=> $$p], $destino, $asunto);
                            dispatch($job);
                            $iSend++;
                            
                            \VCN\Models\Leads\TutorLog::addLog($destino, "Aviso[$this->id]", $this->name);

                        }catch(\Exception $e){}

                        $destino->optout = 1;
                        $destino->optout_fecha = Carbon::now();
                        $destino->save();
                    }
                    
                }

                // if($booking->id == $test_id)
                // {
                //     dd($test_id);
                // }

                \VCN\Models\Bookings\BookingLog::addLog($booking, "Aviso[$this->id]", $this->name);
                $iTot++;
            }
        }

        $log->notas = "Total Bookings [$total]: $iTot";
        $log->enviados = $iSend;
        $log->save();

        return;
    }

    public function getEmailsCount($nuevos=0)
    {
        $iRet = 0;

        $bookings = Booking::bookingAvisos($this->filtros, $this->filtros_not);
        $destino = $this->destino;
        
        foreach($bookings->get() as $b)
        {
            if($nuevos)
            {
                if($b->yaAviso($this->id))
                {
                    continue;
                }
                // dd($b->id);
            }
            
            $iRet += $b->getEmailsCount($destino);
        }

        return $iRet;
    }

    public static function syncMailchimp($plat=1, $apiMC=null)
    {
        ini_set('memory_limit', '500M');
        ini_set('max_execution_time', 0);

        $apidefault = ConfigHelper::config('mc_api');//"a2d823daac0c2f08cd18257c0a08f93b-us12";
        
        $apikey = $apiMC ?: $apidefault;

        $mc = new Mailchimp($apikey);

        // $lists = $mc->getLists();
        // dd($lists);

        $ep = "/lists";
        $d = [ 'count' => 20 ];
        $lists = $mc->api("get", $ep, $d);
        $lists = $lists['lists'];

        
        $iMembers = 0;
        $iUnsuscribed = 0;
        $iBorrados = 0;
        $iLists = 0;
        $mclog = [
            'cleaned' => [],
            'unsubscribed' => [],
        ];

        // since_last_changed
        $p = ConfigHelper::propietario();
        $log = "p$p";
        $logs = AvisoLog::where('aviso_id', 0)->where('enviados', "mc")->where('leidos', $log)->orderBy('created_at', 'DESC')->first();
        $last = $logs ? $logs->created_at->toIso8601String() : '';

        foreach($lists as $list)
        {
            $idl = $list['id'];
            
            $ep = "lists/$idl/members";
            $bloque = 50;

            // UNSUSCRIBED
            $st = 'unsubscribed';
            $d = [ 'status' => $st ];
            $data = $mc->api("get", $ep, $d);

            if(!$data)
            {
                Session::flash('mensaje', "Mailchimp:  ERROR");
                return redirect()->route('manage.system.avisos.sync');
            }

            $totalMembers = $data['total_items'];
            if($totalMembers > 0)
            {
                for($iTotal=0; $iTotal<=$totalMembers; $iTotal += $bloque )
                {
                    $d = [ 'status' => $st, 'count' => $bloque, 'offset'=> $iTotal, 'since_last_changed' => $last ];
                    $data = $mc->api("get", $ep, $d);

                    foreach($data['members']  as $member)
                    {
                        $iMembers++;

                        $e = $member['email_address'];

                        $iu = Viajero::emailSync($e, true);
                        $iUnsuscribed += $iu;
                        
                        if($iu)
                        {
                            $mclog[$st][] = $e;
                        }
                    }
                }
            }

            // CLEANED (BORRADOS)
            $st = 'cleaned';
            $d = [ 'status' => $st ]; //offset
            $data = $mc->api("get", $ep, $d);

            $totalMembers = $data['total_items'];

            if($totalMembers > 0)
            {
                for($iTotal=0; $iTotal<=$totalMembers; $iTotal += $bloque )
                {
                    $d = [ 'status' => $st, 'count' => $bloque, 'offset'=> $iTotal, 'since_last_changed' => $last ];
                    $data = $mc->api("get", $ep, $d);

                    foreach($data['members']  as $member)
                    {
                        $iMembers++;

                        $e = $member['email_address'];

                        $ib = Viajero::emailSync($e, true);
                        $iBorrados += $ib;
                        
                        if($ib)
                        {
                            $mclog[$st][] = $e;
                        }
                    }
                }
            }

            $iLists++;
        }

        $log = "Mailchimp:  Procesados: $iMembers [$iLists listas] :: Borrados: $iBorrados / Unsuscribed: $iUnsuscribed";
        
        $alog = new AvisoLog;
        $alog->aviso_id = 0;
        $alog->enviados = "mc";
        $alog->leidos = "p$plat";
        $alog->notas = $log;
        $alog->bookings = $mclog;
        $alog->save();

        return $log;
    }
}
