<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Cursos\Curso;
use VCN\Models\Bookings\Booking;


class CuestionarioVinculado extends Model
{
    protected $table = 'cuestionario_vinculados';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function cuestionario()
    {
        return $this->belongsTo('\VCN\Models\System\Cuestionario', 'cuestionario_id');
    }
}
