<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

class CuestionarioRespuesta extends Model
{
    protected $table = 'cuestionario_respuestas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'respuestas' => 'array',
        'puntuaciones' => 'array',
    ];

    public function cuestionario()
    {
        return $this->belongsTo('\VCN\Models\System\Cuestionario', 'cuestionario_id');
    }

    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking', 'booking_id');
    }

    public function viajero()
    {
        return $this->belongsTo('\VCN\Models\Leads\Viajero', 'viajero_id');
    }

    public function tutor()
    {
        return $this->belongsTo('\VCN\Models\Leads\Tutor', 'tutor_id');
    }

    public function vinculado()
    {
        return $this->belongsTo('\VCN\Models\System\CuestionarioVinculado', 'cuestionario_id');
    }

}
