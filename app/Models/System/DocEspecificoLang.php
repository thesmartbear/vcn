<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

class DocEspecificoLang extends Model
{
    protected $table = 'system_doc_especifico_langs';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function parent()
    {
        return $this->belongsTo('\VCN\Models\System\DocEspecifico', 'doc_id');
    }

    public function getModeloAttribute()
    {
        return $this->parent->modelo;
    }

    public function getModeloIdAttribute()
    {
        return $this->parent->modelo_id;
    }

    public function booking()
    {
        //por el informe booking_id es un apaño
        return $this->belongsTo('\VCN\Models\Bookings\Booking', 'booking_id');
    }
}
