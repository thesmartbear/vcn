<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Booking;
use VCN\Models\Cursos\Curso;

// use VCN\Models\System\CuestionarioVinculado;
// use VCN\Models\System\CuestionarioRespuesta;

use ConfigHelper;
use Session;

class Cuestionario extends Model
{
    protected $table = 'cuestionarios';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('name')->get();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return self::where('propietario', 0)->orWhere('propietario',$filtro)->orderBy('name')->get();
        }

        return self::orderBy('name')->get();
    }

    public function delete()
    {
        if(CuestionarioRespuesta::where('cuestionario_id',$this->id)->count()>0)
        {
            $error = "Imposible eliminar. Existen Respuestas a este Cuestionario.";
            Session::flash('mensaje-alert', $error);
            return back();
        }

        parent::delete();
    }

    public function getAreaAttribute()
    {
        $user = auth()->user();

        if(!$user)
        {
            return false;
        }

        if(!$this->pivot)
        {
            return false;
        }

        if(!$this->activo)
        {
            return false;
        }

        if($user->roleid != 11 && $user->roleid != 12 && $user->roleid != 13)
        {
            $d = Session::get('vcn.manage.area.tipo', null );
            if($d && $d == $this->destino)
            {
                return true;
            }
            elseif($d && $d == "tutor" && ($this->destino == "viajero" || $this->destino == "tutores") )
            {
                return true;
            }
            
            return false;
        }

        if($user->roleid == 11 && $this->destino == "tutores")
        {
            return false;
        }

        return true;
    }

    public function respuestas()
    {
        return $this->hasMany('\VCN\Models\System\CuestionarioRespuesta', 'cuestionario_id');
    }

    public function getBookingsAttribute()
    {
        $cids = CuestionarioVinculado::where('cuestionario_id',$this->id);

        $valores['cuestionarios'] = $this->id;
        $valores['plataformas'] = $this->propietario?:ConfigHelper::propietario();
        $bookings1 = Booking::listadoFiltros($valores);

        $res = collect();

        foreach($cids->get() as $cid)
        {
            $bookings = clone $bookings1;
            switch($cid->modelo)
            {
                case 'Cerrada':
                {
                    $res = $res->merge( $bookings->where('convocatory_close_id',$cid->modelo_id)->get() );
                }
                break;

                case 'Abierta':
                {
                    $res = $res->merge( $bookings->where('convocatory_open_id',$cid->modelo_id)->get() );
                }
                break;

                case 'ConvocatoriaMulti':
                {
                    $res = $res->merge( $bookings->where('convocatory_multi_id',$cid->modelo_id)->get() );
                }
                break;

                case 'Curso':
                {
                    $res = $res->merge( $bookings->where('curso_id',$cid->modelo_id)->get() );
                }
                break;

                case 'Centro':
                {
                    $ccursos = Curso::where('center_id',$cid->modelo_id)->pluck('id')->toArray();
                    $res = $res->merge( $bookings->whereIn('curso_id',$ccursos)->get() );

                }
                break;

                case 'Categoria':
                {
                    $ccursos = Curso::where('category_id',$cid->modelo_id)->pluck('id')->toArray();
                    $res = $res->merge( $bookings->whereIn('curso_id',$ccursos)->get() );

                }
                break;

                case 'Subcategoria':
                {
                    $ccursos = Curso::where('subcategory_id',$cid->modelo_id)->pluck('id')->toArray();
                    $res = $res->merge( $bookings->whereIn('curso_id',$ccursos)->get() );

                }
                break;

            }
        }

        return $res;

    }

    public function bookingsFiltros($filtros)
    {
        $cids = CuestionarioVinculado::where('cuestionario_id',$this->id);
        if(isset($filtros['reclamar']))
        {
            $cids = $cids->where('modelo', $filtros['reclamar']);
        }
        // dd($cids->get());

        $bookings1 = Booking::listadoFiltros($filtros);

        $res = collect();

        foreach($cids->get() as $cid)
        {
            $bookings = clone $bookings1;
            switch($cid->modelo)
            {
                case 'Cerrada':
                {
                    $res = $res->merge( $bookings->where('convocatory_close_id',$cid->modelo_id)->get() );
                }
                break;

                case 'Abierta':
                {
                    $res = $res->merge( $bookings->where('convocatory_open_id',$cid->modelo_id)->get() );
                }
                break;

                case 'ConvocatoriaMulti':
                {
                    $res = $res->merge( $bookings->where('convocatory_multi_id',$cid->modelo_id)->get() );
                }
                break;

                case 'Curso':
                {
                    $res = $res->merge( $bookings->where('curso_id',$cid->modelo_id)->get() );
                }
                break;

                case 'Centro':
                {
                    $ccursos = Curso::where('center_id',$cid->modelo_id)->pluck('id')->toArray();
                    $res = $res->merge( $bookings->whereIn('curso_id',$ccursos)->get() );

                }
                break;

                case 'Categoria':
                {
                    $ccursos = Curso::where('category_id',$cid->modelo_id)->pluck('id')->toArray();
                    $res = $res->merge( $bookings->whereIn('curso_id',$ccursos)->get() );
                }
                break;

                case 'Subcategoria':
                {
                    $ccursos = Curso::where('subcategory_id',$cid->modelo_id)->pluck('id')->toArray();
                    $res = $res->merge( $bookings->whereIn('curso_id',$ccursos)->get() );
                }
                break;

            }
        }

        return $res->unique();

    }

    public function total_viajeros($valores)
    {
        $tot = 0;
        if($this->destino!="tutores")
        {
            return $this->bookingsFiltros($valores)->count();
        }

        return $tot;

    }

    public function total_viajeros_estado($valores)
    {
        $tot[0] = 0;
        $tot[1] = 0;
        $tot[2] = 0;
        $tot[3] = 0;

        if($this->destino=="tutores")
        {
            return $tot;
        }

        if($this->destino=="viajero") //Puede q se responda desde el tutor
        {
            foreach( $this->bookingsFiltros($valores) as $booking )
            {
                $tot[1] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('estado',1)->groupBy('booking_id')->count();
                $tot[2] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('estado',2)->groupBy('booking_id')->count();
                $tot[3] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('estado',3)->groupBy('booking_id')->count();
            }
        }
        else
        {
            foreach( $this->bookingsFiltros($valores) as $booking )
            {
                $tot[1] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('tutor_id',null)->where('estado',1)->groupBy('booking_id')->count();
                $tot[2] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('tutor_id',null)->where('estado',2)->groupBy('booking_id')->count();
                $tot[3] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('tutor_id',null)->where('estado',2)->groupBy('booking_id')->count();
            }
        }

        $tot[0] = $this->bookingsFiltros($valores)->count() - ($tot[1] + $tot[2] + $tot[3]);

        return $tot;

    }


    public function total_tutores($valores)
    {
        $tot = 0;
        if($this->destino!="viajero")
        {
            foreach( $this->bookingsFiltros($valores) as $booking )
            {
                $tot += $booking->viajero->tutores->count();
            }
        }

        return $tot;

    }


    public function total_tutores_estado($valores)
    {
        $tot[0] = 0;
        $tot[1] = 0;
        $tot[2] = 0;
        $tot[3] = 0;

        if($this->destino!="viajero")
        {
            foreach( $this->bookingsFiltros($valores) as $booking )
            {
                $tot[1] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('viajero_id',null)->where('estado',1)->count();
                $tot[2] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('viajero_id',null)->where('estado',2)->count();
                $tot[3] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('viajero_id',null)->where('estado',3)->count();
            }
        }

        $tot[0] = $this->bookingsFiltros($valores)->count() - ($tot[1] + $tot[2] + $tot[3]);

        return $tot;

    }

    public function getTotalViajerosAttribute()
    {
        $tot = 0;
        if($this->destino!="tutores")
        {
            return $this->bookings->count();
        }

        return $tot;

    }

    public function getTotalViajerosEstadoAttribute()
    {
        $tot[0] = 0;
        $tot[1] = 0;
        $tot[2] = 0;

        if($this->destino=="tutores")
        {
            return $tot;
        }

        if($this->destino=="viajero") //Puede q se responda desde el tutor
        {
            foreach( $this->bookings as $booking )
            {
                $tot[1] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('estado',1)->count();
                $tot[2] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('estado',2)->count();
            }
        }
        else
        {
            foreach( $this->bookings as $booking )
            {
                $tot[1] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('tutor_id',null)->where('estado',1)->count();
                $tot[2] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('tutor_id',null)->where('estado',2)->count();
            }
        }

        $tot[0] = $this->bookings->count() - ($tot[1] + $tot[2]);

        return $tot;

    }

    public function getTotalTutoresAttribute()
    {
        $tot = 0;
        if($this->destino!="viajero")
        {
            foreach( $this->bookings as $booking )
            {
                $tot += $booking->viajero->tutores->count();
            }
        }

        return $tot;

    }

    public function getTotalTutoresEstadoAttribute()
    {
        $tot[0] = 0;
        $tot[1] = 0;
        $tot[2] = 0;

        if($this->destino!="viajero")
        {
            foreach( $this->bookings as $booking )
            {
                $tot[1] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('viajero_id',null)->where('estado',1)->count();
                $tot[2] += $booking->cuestionarios->where('cuestionario_id',$this->id)->where('viajero_id',null)->where('estado',2)->count();
            }
        }

        $tot[0] = $this->bookings->count() - ($tot[1] + $tot[2]);

        return $tot;

    }
}
