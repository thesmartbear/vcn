<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    protected $table = 'system_contactos';
    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $appends = ['full_name'];

    public function scopeActivos($query)
    {
        return $query->where('activo',1);
    }

    public function getFullNameAttribute()
    {
        return $this->name . " ($this->phone)";
    }
}
