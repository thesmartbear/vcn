<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Booking;

use Auth;
use ConfigHelper;

class Oficina extends Model
{
    protected $table = 'oficinas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public static function scopePlataforma()
    {
        $user = auth()->user();

        if( $user && $user->isFullAdmin() )
        {
            return self::where('activa',true)->orderBy('name')->get();
        }

        if($user && !$user->informe_oficinas)
        {
            return self::where('id',$user->oficina_id)->get();
        }

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return self::where('activa',true)->where('propietario', 0)->orWhere('propietario',$filtro)->orderBy('name')->get();
        }

        return self::orderBy('name')->get();
    }

    public function provincia()
    {
        return $this->belongsTo('\VCN\Models\Provincia', 'provincia_id');
    }

    public function pais()
    {
        return $this->belongsTo('\VCN\Models\Pais', 'pais_id');
    }

    public function reuniones()
    {
        return $this->hasMany('\VCN\Models\Reuniones\Reunion', 'oficina_id');
    }

    public function usuarios()
    {
        return $this->hasMany('\VCN\Models\User', 'oficina_id');
    }

    public function txtIban(Booking $booking)
    {
        $iban = $this->iban;

        if( $this->id==11 && $this->propietario==2 && $booking->curso->category_id==6)
        {
            $iban = "ES64 0182 6035 49 0208000828";
        }

        return $iban;
    }

}
