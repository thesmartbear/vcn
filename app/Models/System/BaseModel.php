<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\System\Plataforma;

use ConfigHelper;
use PDF;
use File;
use Session;

class BaseModel extends \VCN\Models\ModelAuditable
{
    public function getPdfCondiciones($plataforma, $idioma)
    {
        if(isset($this->pdf_condiciones[$plataforma][$idioma]))
        {
            return $this->pdf_condiciones[$plataforma][$idioma];
        }

        return null;
    }

    public function getCondiciones($plataforma, $idioma)
    {
        if(isset($this->condiciones[$plataforma][$idioma]) && $this->condiciones[$plataforma][$idioma] != "")
        {
            return $this->condiciones[$plataforma][$idioma];
        }

        $plataforma = 0;
        if(isset($this->condiciones[$plataforma][$idioma]) && $this->condiciones[$plataforma][$idioma] != "")
        {
            return $this->condiciones[$plataforma][$idioma];
        }

        return null;
    }

    public function pdfCondiciones()
    {
        $name = class_basename($this) ."_". $this->id;
        $dir = "assets/uploads/pdf_condiciones/";

        $i = 0;

        foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)
        {        
            foreach(ConfigHelper::idiomas() as $keyi=>$idioma)
            {
                if(isset($this->condiciones[$keyp][$idioma]))
                {
                    $texto = $this->condiciones[$keyp][$idioma];

                    $file = "assets/uploads/pdf_condiciones/". $name ."_". $keyp ."_". $idioma .".pdf";
                    $file = public_path($file);

                    if(is_file($file))
                    {
                        File::delete($file);
                    }

                    if($texto)
                    {
                        $plataforma = Plataforma::find($keyp);

                        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path()."/tmp");
                        $pdf = PDF::loadView('pdf.condiciones', ['texto'=> $texto]);

                        $pdf->setOption('margin-top',30);
                        $pdf->setOption('margin-right',0);
                        $pdf->setOption('margin-bottom',30);
                        $pdf->setOption('margin-left',0);
                        $pdf->setOption('no-print-media-type',false);
                        $pdf->setOption('footer-spacing',0);
                        $pdf->setOption('header-font-size',9);
                        $pdf->setOption('header-spacing',0);

                        if($plataforma)
                        {
                            $pdf->setOption('header-html', 'https://'.ConfigHelper::config('web').'/assets/logos/'.$idioma.'/'. $plataforma->sufijo .'header.html');
                            $pdf->setOption('footer-html', 'https://'.ConfigHelper::config('web').'/assets/logos/'.$idioma.'/'. $plataforma->sufijo .'footer.html');
                        }

                        $pdf->save($file);
                        $i++;
                    }
                }
            }

        }

        if($i)
        {
            Session::flash('mensaje-ok', "Se han generado $i pdf/s de condiciones.");
        }
    }
}
