<?php

namespace VCN\Models\System;

use Illuminate\Database\Eloquent\Model;

class AvisoLog extends Model
{
    protected $table = 'system_aviso_logs';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'bookings' => 'array',
    ];

    public function aviso()
    {
        return $this->belongsTo('\VCN\Models\System\Aviso', 'aviso_id');
    }
}
