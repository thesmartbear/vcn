<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = 'ciudades';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function pais()
    {
        return $this->belongsTo('\VCN\Models\Pais', 'country_id');
    }

    public function provincia()
    {
        return $this->belongsTo('\VCN\Models\Provincia', 'provincia_id');
    }
}
