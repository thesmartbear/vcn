<?php

namespace VCN\Models;

// use Illuminate\Auth\Authenticatable;
// use Illuminate\Database\Eloquent\Model;
// use Illuminate\Auth\Passwords\CanResetPassword;
// use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
// use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

// use VCN\Models\Informes\Presupuesto;

// use VCN\Helpers\ConfigHelper;
// use Carbon;

// class User extends Model implements AuthenticatableContract, CanResetPasswordContract
// {
//     use Authenticatable, CanResetPassword;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use NotificationChannels\WebPush\HasPushSubscriptions;

use VCN\Models\Informes\Presupuesto;

use VCN\Helpers\ConfigHelper;
use Carbon;
// use Activity;

class User extends Authenticatable
{
    use Notifiable;
    use HasPushSubscriptions;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'fname', 'lname', 'roleid', 'status', 
        'config', 'plataforma', 'oficina_id',
        'es_guest','last_activity'
    ];
    // protected $guarded = ['_token'];

    protected $dates = ['login_last'];

    protected $casts = [
        'compra' => 'json',
        'registro' => 'json',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function scopePlataforma($query)
    {
        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return $query->where('plataforma',$filtro);//->orWhere('plataforma',0);
        }

        return $query;
    }

    public function scopeCrm($query)
    {
        return $query->where('status','>',0)->where('roleid','<>',11)->where('roleid','<>',12)->where('roleid','<>',13);
    }

    public function scopeActivos($query)
    {
        return $query->where('status','>',0);
    }

    public function viajeros()
    {
        return $this->hasMany('\VCN\Models\Leads\Viajero', 'asign_to');
    }

    public function viajero()
    {
        return $this->hasOne('\VCN\Models\Leads\Viajero', 'user_id');
    }

    public function tutor()
    {
        return $this->hasOne('\VCN\Models\Leads\Tutor', 'user_id');
    }

    public function monitor()
    {
        return $this->hasOne('\VCN\Models\Monitores\Monitor', 'user_id');
    }

    public function getUserTutorViajeroAttribute()
    {
        if($this->viajero)
        {
            $u = User::where('username',$this->username)->where('plataforma',$this->plataforma)->where('roleid',12)->first();

            return $u ?: null;
        }

        if($this->tutor)
        {
            $u = User::where('username',$this->username)->where('plataforma',$this->plataforma)->where('roleid',11)->first();

            return $u ?: null;
        }

        return null;
    }

    public function chats()
    {
        return $this->belongsToMany("\VCN\Models\Chat\ChatSoporte", 'mc_conversation_user', 'user_id', 'conversation_id');//->withTimestamps();
    }

    public function getChatAttribute()
    {
        $ret = $this->chat_id ? \VCN\Models\Chat\ChatSoporte::find($this->chat_id) : null;
        if(!$ret)
        {
            $this->chat_id = null;
            $this->save();
        }

        return $ret;
    }

    public function getChatClienteAttribute()
    {
        $chat = $this->chat;
    }

    public function getChatAgenteAttribute()
    {
        $chat = $this->chat;
    }

    public static function getFreeForChat()
    {
        $m = 60;
        $t = Carbon::now()->subMinutes($m)->getTimestamp();

        $free =  User::where('es_guest',1)->where(function($q) {
            $q->where('chat_id', null)->orWhere('chat_id',0);
        })->first();

        if(!$free)
        {
            $free =  User::where('es_guest',1)->where(function($q) use ($t) {
                $q->where('chat_last_activity',0)->orWhere('chat_last_activity', '<', $t);
            })->orderby('chat_last_activity')->first();
        }

        $free->chat_id = null;
        $free->chat_last_activity = null;

        $free->last_activity = Carbon::now()->getTimestamp();
        $free->save();

        return $free;
    }

    public static function getChatCrmDisponibles()
    {
        // self::onlineUsers();

        $m = 10;
        $t = Carbon::now()->subMinutes($m)->getTimestamp();

        $users =  User::where('chat_admin',1)->where('last_activity','>',0)->where('last_activity', '>=', $t)->get();
        
        return $users;
    }

    public function getChatSesionCaducadaAttribute()
    {
        $m = 10;
        $t = Carbon::now()->subMinutes($m)->getTimestamp();

        return $this->chat_last_activity < $t;
    }

    public function getSesionCaducadaAttribute()
    {
        $m = 10;
        $t = Carbon::now()->subMinutes($m)->getTimestamp();

        return $this->last_activity < $t;
    }

    public function actividad()
    {
        return $this->hasMany('\VCN\Models\System\UserActividad', 'user_id');
    }

    public static function online()
    {
        $activities = Activity::users()->get();

        return $activities;
    }

    public static function onlineUsers()
    {
        $roles = User::whereNotIn('roleid',[11,12,13])->pluck('id')->toArray();
        $activities = Activity::users()->whereIn('sessions.user_id', $roles)->get();
        
        return $activities;
    }

    public static function onlineGuests()
    {
        $guests = Activity::guests(5); //5 minutos

        return $guests;
    }

    public static function onlineViajeros()
    {
        $roles = User::where('roleid',11)->pluck('id')->toArray();
        $activities = Activity::users()->whereIn('sessions.user_id', $roles)->get();

        return $activities;
    }

    public static function onlineTutores()
    {
        $roles = User::where('roleid',12)->pluck('id')->toArray();
        $activities = Activity::users()->whereIn('sessions.user_id', $roles)->get();

        return $activities;
    }

    public static function onlineMonitores()
    {
        $roles = User::where('roleid',13)->pluck('id')->toArray();
        $activities = Activity::users()->whereIn('sessions.user_id', $roles)->get();

        return $activities;
    }

    public function getFichaAttribute()
    {
        if($this->es_viajero)
            return $this->viajero;

        if($this->es_tutor)
            return $this->tutor;

        if($this->es_monitor)
            return $this->monitor;

        return null;
    }

    public function getEsAvisoFotoAttribute()
    {
        if($this->es_viajero)
        {
            if($this->ficha->booking)
            {
                return $this->ficha->booking->es_aviso_foto;
            }
        }

        if($this->es_tutor)
        {
            foreach($this->ficha->viajeros as $viajero)
            {
                if(!$viajero->booking)
                {
                    continue;
                }
                
                if($viajero->booking->es_aviso_foto)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public function getIdiomaContactoAttribute()
    {
        return $this->ficha ? $this->ficha->idioma_contacto : "es";
    }

    public function getDatosCampamentoPendienteAttribute()
    {
        $ret = "<span class='badge badge-warning'><i class='fa fa-exclamation-triangle'></i></span>";

        if($this->es_viajero && $this->viajero->booking)
        {
            if($this->viajero->booking->datos_campamento_pendiente)
            {
                return $ret;
            }
        }

        if($this->es_tutor)
        {
            foreach($this->ficha->viajeros as $viajero)
            {
                if(!$viajero->booking)
                {
                    continue;
                }
                
                if($viajero->booking->datos_campamento_pendiente)
                {
                    return $ret;
                }
            }
        }

        return null;
    }

    public function getFotoDirAttribute()
    {
        if($this->es_viajero)
        {
            return "assets/uploads/monitor/";
        } 
        elseif($this->es_tutor)
        {
            return "assets/uploads/tutores/";
        }
        elseif($this->es_monitor)
        {
            return "assets/uploads/viajeros/";
        }

        return "assets/uploads/users/";
    }


    public function solicitudes()
    {
        return $this->hasMany('\VCN\Models\Solicitudes\Solicitud', 'user_id');
    }

    public function solicitudes_betweenFechas( $desde, $hasta)
    {
        $desde = Carbon::createFromFormat('!d/m/Y',$desde);
        $hasta = Carbon::createFromFormat('!d/m/Y',$hasta);
        return $this->hasMany('\VCN\Models\Solicitudes\Solicitud', 'user_id')->where('fecha','>=',$desde)->where('fecha','<=',$hasta)->get();
    }

    public function solicitudes_activas()
    {
        $status = [];
        $status[] = ConfigHelper::config('solicitud_status_futuro');
        $status[] = ConfigHelper::config('solicitud_status_archivado');

        return $this->hasMany('\VCN\Models\Solicitudes\Solicitud', 'user_id')->whereNotIn('status_id',$status);
    }

    public function solicitudes_activas_betweenFechas( $desde, $hasta)
    {
        $status = [];
        $status[] = ConfigHelper::config('solicitud_status_futuro');
        $status[] = ConfigHelper::config('solicitud_status_archivado');

        $desde = Carbon::createFromFormat('!d/m/Y',$desde);
        $hasta = Carbon::createFromFormat('!d/m/Y',$hasta);
        return $this->hasMany('\VCN\Models\Solicitudes\Solicitud', 'user_id')->whereNotIn('status_id',$status)->where('fecha','>=',$desde)->where('fecha','<=',$hasta)->get();
    }

    public function bookings()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'user_id')->where('es_presupuesto',0);
    }

    public function bookings_betweenFechas( $desde, $hasta)
    {
        $desde = Carbon::createFromFormat('!d/m/Y',$desde);
        $hasta = Carbon::createFromFormat('!d/m/Y',$hasta);
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'user_id')->where('es_presupuesto',0)->where('fecha','>=',$desde)->where('fecha','<=',$hasta)->get();
    }

    // public function bookings()
    // {
    //     return $this->hasMany('\VCN\Models\Bookings\Booking', 'user_id')->where('es_presupuesto',0);
    // }

    public function presupuestos()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'user_id')->where('es_presupuesto',1);
    }

    public function presupuestos_betweenFechas( $desde, $hasta)
    {
        $desde = Carbon::createFromFormat('!d/m/Y',$desde);
        $hasta = Carbon::createFromFormat('!d/m/Y',$hasta);

        // $dweek = $desde->format("W");
        // $hweek = $hasta->format("W");

        return Presupuesto::where('user_id',$this->id)->where('fecha','>=',$desde)->where('fecha','<=',$hasta)->get();
    }

    public function incidencias()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingIncidencia', 'asign_to');
    }

    public function incidencia_tareas()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingTarea', 'asign_to')->where('estado',0);
    }


    public function seguimientos()
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroLog', 'user_id')->where('tipo','<>','')->where('tipo','<>','log');
    }

    public function seguimientos_tipo($tipo)
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroLog', 'user_id')->where('tipo',$tipo)->get();
    }

    public function seguimientos_fecha( $fecha, $tipo=null)
    {
        if(!$tipo)
        {
            return \VCN\Models\Leads\ViajeroLog::where('user_id', $this->id)->where('tipo','<>','')->where('tipo','<>','log')->whereDate('created_at','=',$fecha)->get();
        }

        return \VCN\Models\Leads\ViajeroLog::where('user_id', $this->id)->where('tipo',$tipo)->whereDate('created_at','=',$fecha)->get();
    }

    public function seguimientos_fecha_count( $fecha, $tipo=null)
    {
        if(!$tipo)
        {
            return \VCN\Models\Leads\ViajeroLog::where('user_id', $this->id)->where('tipo','<>','')->where('tipo','<>','log')->whereDate('created_at','=',$fecha)->count();
        }
        
        return \VCN\Models\Leads\ViajeroLog::where('user_id', $this->id)->where('tipo',$tipo)->whereDate('created_at','=',$fecha)->count();
    }

    public function seguimientos_betweenFechas( Carbon $desde, Carbon $hasta)
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroLog', 'user_id')->where('tipo','<>','')->where('tipo','<>','log')->where('created_at','>=',$desde->format('Y-m-d'))->where('created_at','<=',$hasta->format('Y-m-d'));
    }


    public function getCompraEnCursoAttribute()
    {
        if($this->compra)
        {
            $bid = (int)$this->compra['booking_id'];
            if($bid)
            {
                return true;
            }

        }
        return false;
    }


    // public static function asignados()
    public function scopeAsignados($query)
    {
        if( auth()->user()->isFullAdmin() )
        {
            return $query->where('id','>',1)->where('status','>',0)->where('roleid','<>',11)->where('roleid','<>',12)->where('roleid','<>',13)->orderBy('fname');
        }

        // $filtro = ConfigHelper::config('propietario');
        $filtro = auth()->user()->plataforma;
        if($filtro)
        {
            $p = $filtro;
            return $query->where('id','>',1)->where('status','>',0)->where('plataforma',$p)->where('roleid','<>',11)->where('roleid','<>',12)->where('roleid','<>',13)->orderBy('fname');
        }

        return $query->where('id','>',1)->where('status','>',0)->where('roleid','<>',11)->where('roleid','<>',12)->where('roleid','<>',13)->orderBy('fname');
    }

    public function scopeAsignadosOff($query)
    {
        if( auth()->user()->isFullAdmin() )
        {
            return $query->where('id','>',1)->where('status',0)->where('roleid','<>',11)->where('roleid','<>',12)->where('roleid','<>',13)->orderBy('fname');
        }

        // $filtro = ConfigHelper::config('propietario');
        $filtro = auth()->user()->plataforma;
        if($filtro)
        {
            $p = $filtro;
            return $query->where('id','>',1)->where('status',0)->where('plataforma',$p)->where('roleid','<>',11)->where('roleid','<>',12)->where('roleid','<>',13)->orderBy('fname');
        }

        return $query->where('id','>',1)->where('status',0)->where('roleid','<>',11)->where('roleid','<>',12)->where('roleid','<>',13)->orderBy('fname');
    }

    public function scopeVisitantes($query)
    {
        return $query->where('es_guest', 1);
    }

    public static function transporte()
    {
        $ret = collect([]);
        $users = User::where('status','>',0)->where('id','>',1)->where('roleid','<>',11)->where('roleid','<>',12)->where('roleid','<>',13)->get();

        foreach($users as $user)
        {
            if($user->isFullAdmin())
            {
                $ret->push($user);
            }
            else
            {
                $permisos = json_decode( $user->role->permisos );
                $p1 = "transporte-plazas";
                $p2 = "convocatoria-plazas";

                if( isset($permisos->$p1) && isset($permisos->$p2) )
                {
                    if($permisos->$p1->edit && $permisos->$p2->edit)
                    {
                        $ret->push($user);
                    }
                }
            }
        }

        return $ret;
    }

    public function getAvatarNameAttribute()
    {
        $a1 = substr($this->fname, 0, 1);
        $a2 = substr($this->lname, 0, 1);
        if($this->lname == "")
        {
            $a2 = substr($this->fname, 1, 1);
        }

        return $a1.$a2;

        // return str_slug($this->full_name);
    }

    public function getFullNameAttribute()
    {
        return "$this->fname $this->lname";
    }

    public function getNameAttribute()
    {
        return "$this->fname";
    }

    public function role()
    {
        return $this->belongsTo('\VCN\Models\System\UserRole', 'roleid');
    }

    public function oficina()
    {
        return $this->belongsTo('\VCN\Models\System\Oficina', 'oficina_id');
    }

    public function getOficinaNameAttribute()
    {
        return $this->oficina?$this->oficina->name:"-";
    }

    public function getFiltroOficinasAttribute()
    {
        if( $this->isFullAdmin() )
            return true;

        if(!$this->oficina)
            return false;

        if($this->oficina->ver_otras)
            return true;
    }

    public function getInformeOficinasAttribute()
    {
        if( $this->isFullAdmin() )
            return true;

        if(!$this->oficina)
            return false;

        if(!$this->role)
            return false;

        if($this->role->ver_oficinas_informes)
            return true;

        return $this->oficina->ver_otras_informes;
    }

    public function getRoleNameAttribute()
    {
        if($this->roleid == 1)
        {
            return "Full-Admin";
        }
        elseif($this->roleid == 11)
        {
            return "Viajero";
        }
        elseif($this->roleid == 12)
        {
            return "Tutor";
        }
        elseif($this->roleid == 13)
        {
            return "Monitor";
        }
        // elseif($this->roleid == 2)
        // {
        //     return "Admin";
        // }

        return $this->role?$this->role->name:"-";

    }

    public function getPlataformaNameAttribute()
    {
        return ConfigHelper::plataforma($this->plataforma);
    }

    public function getEsTutorAttribute()
    {
        return ($this->roleid == 12);
    }

    public function getEsViajeroAttribute()
    {
        return ($this->roleid == 11);
    }

    public function getEsMonitorAttribute()
    {
        return ($this->roleid == 13);
    }

    public function isFullAdmin()
    {
        return ($this->roleid == 1);
    }

    public function getEsFullAdminAttribute()
    {
        return ($this->roleid == 1);
    }

    public function getEsCrmAdminAttribute()
    {
        if(!$this->roleid)
        {
            return false;
        }
        
        if($this->roleid >= 11 || $this->roleid <= 13)
        {
            return true;
        }

        return false;
    }


    public function getEsClienteAttribute()
    {
        return ($this->roleid == 11 || $this->roleid == 12);
    }

    public function getEsAisladoAttribute()
    {
        if($this->es_full_admin)
        {
            return false;
        }

        return $this->role?$this->role->aislado:true;
    }

    public static function permisoBookings($plataforma)
    {
        $ret = collect([]);
        $users = User::where('plataforma',$plataforma)->where('roleid','<>',11)->where('roleid','<>',12)->get();

        foreach($users as $user)
        {
            if($user->isFullAdmin())
            {
                // $ret->push($user);
            }
            else
            {
                $permisos = json_decode( $user->role->permisos );

                if( isset($permisos->bookings) )
                {
                    if($permisos->bookings->edit)
                    {
                        $ret->push($user);
                    }
                }
            }
        }

        return $ret;
    }

    public function visitas()
    {
        return $this->hasMany('\VCN\Models\Informes\WebVisita', 'user_id');
    }

    public function deseos()
    {
        return $this->hasMany('\VCN\Models\UserDeseo', 'user_id');
    }

    public function esDeseo($curso_id)
    {
        return !is_null($this->deseos->where('curso_id',$curso_id)->first());
    }

    public function checkPermisoAislado($model, $id)
    {
        $ficha = null;

        if( $this->isFullAdmin() )
            return true;

        if(!$this->es_aislado)
            return true;

        switch($model)
        {
            case 'Viajero':
            {
                $ficha = \VCN\Models\Leads\Viajero::find($id);
                if($ficha->asign_to == $this->id)
                {
                    return true;
                }
            }
            break;

            case 'Booking':
            {
                $ficha = \VCN\Models\Bookings\Booking::find($id);
                if($ficha->user_id == $this->id)
                {
                    return true;
                }
            }
            break;
        }

        return false;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \VCN\Notifications\MailResetPasswordNotification($token));
    }
}
