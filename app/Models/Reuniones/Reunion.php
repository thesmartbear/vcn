<?php

namespace VCN\Models\Reuniones;

use Illuminate\Database\Eloquent\Model;

class Reunion extends Model
{
    protected $table = 'reuniones';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $dates = ['fecha'];

    public function oficina()
    {
        return $this->belongsTo('\VCN\Models\System\Oficina', 'oficina_id');
    }

    public function curso()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Curso', 'curso_id');
    }

    public function logs()
    {
        return $this->hasMany('\VCN\Models\Reuniones\ReunionLog', 'reunion_id');
    }

    public function convocatoria()
    {
        $tipo = $this->convocatoria_tipo;
        switch($tipo)
        {
            case 1: //cerrada
            {
                return $this->belongsTo('\VCN\Models\Convocatorias\Cerrada', 'convocatoria_id');
            }
            break;

            case 2: //Abierta
            {
                return $this->belongsTo('\VCN\Models\Convocatorias\Abierta', 'convocatoria_id');
            }
            break;

            case 3: //Multi
            {
                return $this->belongsTo('\VCN\Models\Convocatorias\ConvocatoriaMulti', 'convocatoria_id');
            }
            break;
        }
    }

}
