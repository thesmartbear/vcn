<?php

namespace VCN\Models\Reuniones;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Booking;

class ReunionLog extends Model
{
    protected $table = 'reunion_logs';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'bookings' => 'array',
    ];

    public static function add(Booking $booking, $reunion_id=0)
    {
        if(!$booking->reunion)
        {
            return false;
        }

        //todos
        if(!$reunion_id)
        {
            foreach($booking->reuniones as $reunion)
            {
                $reunion_id = $reunion->id;
                Self::addLog($booking, $reunion_id);
            }
        }
        else
        {
            Self::addLog($booking, $reunion_id);
        }
    }

    private static function addLog(Booking $booking, $reunion_id)
    {
        $log = Self::where('reunion_id',$reunion_id)->first();

        if(!$log)
        {
            $log = new Self;
            $log->reunion_id = $reunion_id;
            $log->bookings = [0=>$booking->id];
            $log->save();
        }
        else
        {
            $b = $log->bookings;

            // if(!in_array($booking->id,$b))
            {
                array_push($b,$booking->id);
                $log->bookings = $b;

                $log->save();
            }
        }
    }
}
