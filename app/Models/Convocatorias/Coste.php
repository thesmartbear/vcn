<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

use Session;

class Coste extends Model
{
    protected $table = 'convocatoria_costes';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function convocatoria()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\Abierta', 'convocatory_id');
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda', 'convocatories_open_currency_id');
    }

    public function getMonedaNameAttribute()
    {
        return $this->moneda?$this->moneda->name:Session::get("vcn.moneda");
    }
}
