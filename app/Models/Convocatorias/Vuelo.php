<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Booking;

use ConfigHelper;
use Carbon;
use Session;

class Vuelo extends Model
{
    protected $table = 'vuelos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'billetes_log' => 'array',
    ];

    public function delete()
    {
        if(Booking::where('vuelo_id',$this->id)->count()>0)
        {
            $bookings = Booking::where('vuelo_id',$this->id)->pluck('id')->toArray();
            $bookings = implode(",", $bookings);

            $error = "Imposible eliminar. Existen Bookings con este Vuelo [$bookings]";
            Session::flash('mensaje-alert', $error);
            return back();
        }

        parent::delete();
    }

    public function bookings()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'vuelo_id');
    }

    public function etapas()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\VueloEtapa', 'vuelo_id');
    }

    public function convocatorias()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\CerradaVuelo', 'vuelo_id');
    }

    public function agencia()
    {
        return $this->belongsTo('\VCN\Models\Agencia', 'agencia_id');
    }

    public function getAgenciaNameAttribute()
    {
        return $this->agencia?$this->agencia->name:"-";
    }

    public function getPropietarioAttribute()
    {
        if($this->convocatorias->first())
        {
            if($this->convocatorias->first()->convocatoria)
            {
                return $this->convocatorias->first()->convocatoria->curso->propietario;
            }
        }

        return 0;
    }

    public function getPlazasReservasAttribute()
    {
        // $p = $this->bookings->where('status_id',ConfigHelper::config('booking_status_prebooking'));
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        $p = Booking::where('vuelo_id',$this->id)->whereIn('status_id',$stplazas);

        $po = $this->bookings->where('status_id',ConfigHelper::config('booking_status_overbooking'))->where('ovbkg_pv',0); //overbooking pero no de vuelo

        return $p->count() + $po->count();// + $this->plazas_online;
    }

    public function getPlazasReservasPlataforma($p)
    {
        // $p = $this->bookings->where('status_id',ConfigHelper::config('booking_status_prebooking'));
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        $p = Booking::where('vuelo_id',$this->id)->where('plataforma',$p)->whereIn('status_id',$stplazas);

        $po = $this->bookings->where('plataforma',$p)->where('status_id',ConfigHelper::config('booking_status_overbooking'))->where('ovbkg_pv',0); //overbooking pero no de vuelo

        return $p->count() + $po->count();// + $this->plazas_online;
    }

    public function getPlazasPrereservasAttribute()
    {
        $p = $this->bookings->where('status_id',ConfigHelper::config('booking_status_prereserva'));

        return $p->count();
    }

    public function getPlazasOverbookingAttribute()
    {
        $p = $this->bookings->where('status_id',ConfigHelper::config('booking_status_overbooking'))->where('ovbkg_pv',1);
        return $p->count();
    }

    public function getPlazasOverbookingPlataforma($p)
    {
        $p = $this->bookings->where('plataforma',$p)->where('status_id',ConfigHelper::config('booking_status_overbooking'))->where('ovbkg_pv',1);
        return $p->count();
    }

    public function getPlazasOnlineAttribute()
    {
        $hora = Carbon::now();
        $hora1 = Carbon::now()->subHour();
        $hora2 = Carbon::now()->subHours(96);

        //tpv_plazas : 1hora
        $p1 = Booking::where('vuelo_id', $this->id)->where('es_online',1)->where('status_id',0)
            ->where('tpv_plazas','>=',$hora1)->where('tpv_plazas','<=',$hora);

        //tpv_plazas : 3dias (backend)
        $p2 = Booking::where('vuelo_id', $this->id)->where('es_online',2)->where('status_id',0)
            ->where('tpv_plazas','>=',$hora2)->where('tpv_plazas','<=',$hora);

        return $p1->count() + $p2->count();
    }

    public function getPlazasDisponiblesAttribute()
    {
        return ($this->plazas_totales - $this->plazas_bloqueadas - $this->plazas_monitor - $this->plazas_reservas - $this->plazas_prereservas - $this->plazas_overbooking - $this->plazas_online );
    }

    public function getPlazasDisponiblesOvbkgAttribute()
    {
        return ($this->plazas_totales - $this->plazas_bloqueadas - $this->plazas_monitor - $this->plazas_reservas - $this->plazas_prereservas - $this->plazas_online );
    }

    public function getPlazasVendidasAttribute()
    {
        return ($this->plazas_reservas - $this->plazas_overbooking );
    }

    public function getEsBcnAttribute()
    {
        $etapas = $this->etapas->where('tipo',0);

        if(!$etapas->first())
        {
            return false;
        }

        return strpos($this->aeropuerto, "(BCN)")>0;
    }

    public function getEsMadridAttribute()
    {
        $etapas = $this->etapas->where('tipo',0);

        if(!$etapas->first())
        {
            return false;
        }

        return strpos($this->aeropuerto, "(MAD)")>0;
    }

    public function getEsOtroAttribute()
    {
        return (!$this->es_bcn && !$this->es_madrid);
    }

    public function getFechaSalidaAttribute()
    {
        $etapas = $this->etapas->where('tipo',0);

        if(!$etapas->first())
        {
            return null;
        }

        return Carbon::parse($etapas->first()->salida_fecha)->format('Y-m-d');
    }

    public function getFechaLlegadaAttribute()
    {
        $etapas = $this->etapas->where('tipo',1)->sortByDesc('llegada_fecha');

        if(!$etapas->first())
        {
            return null;
        }

        return Carbon::parse($etapas->first()->llegada_fecha)->format('Y-m-d');
    }

    public function getArrivalAttribute()
    {
        //Llegada del último tramo de ida
        $etapas = $this->etapas->where('tipo',0)->sortByDesc('llegada_fecha')->sortByDesc('llegada_hora');

        return $etapas->first();
    }

    public function getReturnAttribute()
    {
        //Salida del primer tramo de vuelta
        $etapas = $this->etapas->where('tipo',1)->sortBy('salida_hora')->sortBy('salida_fecha');

        return $etapas->first();
    }


    public function getInfoVueloAttribute()
    {
        $ret = "IDA: ";
        $ret .= $this->info_vuelo_ida;
        $ret .= "<hr>VUELTA:";
        $ret .= $this->info_vuelo_vuelta;
        return $ret;
    }

    public function getInfoVueloIdaAttribute()
    {
        $ret = "";

        foreach($this->etapas->where('tipo',0) as $v)
        {
            $ret .= Carbon::parse($v->salida_fecha)->format('d/m/Y') . " $v->company $v->vuelo";
            $ret .= "<br>DEPARTURE $v->salida_aeropuerto ". substr($v->salida_hora,0,5) ."H";
            $ret .= "<br>ARRIVAL $v->llegada_aeropuerto ". substr($v->llegada_hora,0,5) ."H <br>";
        }

        return $ret;
    }

    public function getInfoVueloVueltaAttribute()
    {
        $ret = "";

        foreach($this->etapas->where('tipo',1) as $v)
        {
            $ret .= Carbon::parse($v->salida_fecha)->format('d/m/Y') . " $v->company $v->vuelo";
            $ret .= "<br>DEPARTURE $v->salida_aeropuerto ". substr($v->salida_hora,0,5) ."H";
            $ret .= "<br>ARRIVAL $v->llegada_aeropuerto ". Carbon::parse($v->llegada_fecha)->format('d/m/Y') ." ". substr($v->llegada_hora,0,5) ."H <br>";
        }

        return $ret;
    }

    public function getAeropuertoAttribute()
    {
        $salida = $this->etapas->where('tipo',0)->first();

        return $salida?$salida->salida_aeropuerto:"-";
    }

    public function getCompanyNameAttribute()
    {
        $salida = $this->etapas->where('tipo',0)->first();

        return $salida?$salida->company:"-";
    }

    public function getSalidaAttribute()
    {
        $salida = $this->etapas->where('tipo',0)->first();

        return $salida;
    }

    public static function listadoFiltros($valores)
    {
        $vuelos = collect();
        $filtrado = false;

        //convocatorias
        $f = isset($valores['convocatorias'])?$valores['convocatorias']:0;
        if($f)
        {
            $v = \VCN\Models\Convocatorias\Cerrada::find($f)->vuelos;
            $vuelos = $vuelos->merge($v);

            $filtrado = true;
        }

        //curso
        $f = isset($valores['cursos'])?$valores['cursos']:0;
        if($f && !$filtrado)
        {
            $convos = \VCN\Models\Convocatorias\Cerrada::where('course_id',$f)->get();

            foreach($convos as $c)
            {
                $v = $c->vuelos;
                $vuelos = $vuelos->merge($v);
            }

            $filtrado = true;
        }

        //proveedor
        $f = isset($valores['proveedores'])?$valores['proveedores']:0;
        if($f && !$filtrado)
        {
            $cursos = \VCN\Models\Proveedores\Proveedor::find($f)->cursos->pluck('id');
            $convos = \VCN\Models\Convocatorias\Cerrada::whereIn('course_id',$cursos)->get();

            foreach($convos as $c)
            {
                $v = $c->vuelos;
                $vuelos = $vuelos->merge($v);
            }

            $filtrado = true;
        }

        $vuelos = $vuelos->unique();

        if(!$valores['any'])
        {
            return $vuelos;
        }

        $ret = collect();
        foreach($vuelos as $v)
        {
            if($v->vuelo->any == $valores['any'])
            {
                $ret->push($v);
            }
        }
        return $ret;
    }

    public static function listadoFiltrosTodos($valores)
    {
        $vuelos = collect();

        //convocatorias
        $f = isset($valores['convocatorias'])?$valores['convocatorias']:null;
        if($f)
        {
            $v = \VCN\Models\Convocatorias\Cerrada::find($f)->vuelos;
            $vuelos = $vuelos->merge($v);

            return $vuelos;
        }

        $convos = \VCN\Models\Convocatorias\Cerrada::select();

        //categoria
        $f = isset($valores['categorias'])?$valores['categorias']:null;
        if($f)
        {
            $cursos = \VCN\Models\Cursos\Curso::where('category_id', $f)->pluck('id')->toArray();
            $convos = $convos->whereIn('course_id',$cursos);
        }

        //centro
        $f = isset($valores['centros'])?$valores['centros']:null;
        if($f)
        {
            $cursos = \VCN\Models\Cursos\Curso::where('center_id', $f)->pluck('id')->toArray();
            $convos = $convos->whereIn('course_id',$cursos);
        }

        //curso
        $f = isset($valores['cursos'])?$valores['cursos']:null;
        if($f)
        {
            $convos = $convos->where('course_id',$f);
        }

        //proveedor
        $f = isset($valores['proveedores'])?$valores['proveedores']:null;
        if($f)
        {
            $cursos = \VCN\Models\Proveedores\Proveedor::find($f)->cursos->pluck('id');
            $convos = $convos->whereIn('course_id',$cursos);
        }

        $convos = $convos->get();
        foreach($convos as $c)
        {
            $v = $c->vuelos;
            $vuelos = $vuelos->merge($v);
        }

        $vuelos = $vuelos->unique();

        // $anys = [];
        // for ($i=$valores['anyd']; $i <= $valores['anyh']; $i++)
        // { 
        //     $anys[] = $i;           
        // }

        $ret = collect();
        foreach($vuelos as $v)
        {
            $anyv = $v->vuelo->any;

            // if(in_array($anyv, $anys))
            if($anyv == $valores['any'])
            {
                $ret->push($v);
            }
        }

        return $ret;

    }
}
