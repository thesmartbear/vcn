<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Booking;
use VCN\Models\System\Documento;

use Carbon;
use Session;

use ConfigHelper;

use \VCN\Models\System\BaseModel;

class ConvocatoriaMulti extends BaseModel
{
    protected $table = 'convocatoria_multis';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'condiciones'    => 'json',
    ];

    public function delete()
    {
        if(Booking::where('convocatory_multi_id',$this->id)->count()>0)
        {
            $error = "Imposible eliminar. Existen Bookings con esta Convocatoria.";
            Session::flash('mensaje-alert', $error);
            return back();
        }

        parent::delete();
    }

    public function semanas()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\ConvocatoriaMultiSemana', 'convocatory_id')->orderBy('desde');
    }

    public function getRouteAttribute()
    {
        return route('manage.convocatorias.multi.ficha', $this->id);
    }

    public function getDocumentos($idioma)
    {
        $docs = \VCN\Models\System\Documento::where('modelo','ConvocatoriaMulti')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();

        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::where('modelo','ConvocatoriaMulti')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();
    }
    
    public function getDocumentosArea($idioma, $plataforma=null)
    {
        $filtro = [0, $plataforma ?: ConfigHelper::config('propietario') ];

        $docs = \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','ConvocatoriaMulti')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','ConvocatoriaMulti')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
    }

    public function especialidades()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\ConvocatoriaMultiEspecialidad', 'convocatory_id');
    }

    public function monitores()
    {
        return $this->hasMany('\VCN\Models\Monitores\MonitorRelacion', 'modelo_id')->where('modelo','ConvocatoriaMulti');
    }

    public function bookings()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'convocatory_multi_id');
    }

    public function bookings_completos()
    {
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'convocatory_multi_id')->whereIn('status_id',$stplazas);
    }

    public function bookings_completos_futuro()
    {
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'convocatory_multi_id')->where('course_start_date','>',Carbon::now())->whereIn('status_id',$stplazas);
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda', 'moneda_id');
    }

    public function getDuracionDiasAttribute()
    {
        return 0;
    }

    public function getDuracionNameAttribute()
    {
        $c = $this->cursos->first();

        if($c)
        {
            return $c->duracion_name;
        }

        return "Semanas";
    }

    public function getDuracionName($units)
    {
        return trans_choice("web.curso.".$this->duracion_name,$units);
    }

    public function getCursosAttribute()
    {
        $a = explode(',', $this->cursos_id);

        return \VCN\Models\Cursos\Curso::whereIn('id',$a)->get();
    }

    public function getMonedaNameAttribute()
    {
        return $this->moneda?$this->moneda->name:Session::get('vcn.moneda');
    }

    //Plazas
    public function getPlazas($booking_id=0)
    {
        $semanas = [];

        if($booking_id>0)
        {
            $b = Booking::find($booking_id);
            foreach($this->semanas->sortBy('semana') as $sem)
            {
                $pb = $b->multis->where('semana_id',$sem->id);//->first();
                if($pb)
                {
                    $semanas[$sem->semana] = $sem->plazas_disponibles;
                }
            }
        }
        else
        {
            foreach($this->semanas->sortBy('semana') as $sem)
            {
                $semanas[$sem->semana] = $sem->plazas_disponibles;
            }
        }

        return $semanas;
    }

    public function hayPlazasDisponibles($booking_id)
    {
        $b = Booking::find($booking_id);

        if($b->es_online)
        {
            $hora = Carbon::now();
            $hora1 = Carbon::now()->subHour();
            $hora2 = Carbon::now()->subHours(96);

            $cid = $this->convocatory_multi_id;

            if($cid)
            {
                //tpv_plazas : 1hora
                $p1 = Booking::where('id', $booking_id)->where('convocatory_multi_id',$cid)
                    ->where('course_start_date','<=', $this->desde)
                    ->where('course_end_date','>=', $this->hasta)
                    ->where('es_online',1)->where('tpv_status',false)
                    ->where('tpv_plazas','>=',$hora1)->where('tpv_plazas','<=',$hora);

                //tpv_plazas : 3dias (backend)
                $p2 = Booking::where('id', $booking_id)->where('convocatory_multi_id',$cid)
                    ->where('course_start_date','<=', $this->desde)
                    ->where('course_end_date','>=', $this->hasta)
                    ->where('es_online',2)->where('tpv_status',false)
                    ->where('tpv_plazas','>=',$hora2)->where('tpv_plazas','<=',$hora);

                if($p1->count() || $p2->count())
                {
                    return true;
                }
            }
        }

        $noplazas = [];

        //si alguna semana no tiene plazas => no hay
        foreach($this->semanas->sortBy('semana') as $sem)
        {
            $pb = $b->multis->where('semana_id',$sem->id)->first(); //Solo si la semana está en el booking
            $p = $sem->plazas_disponibles;
            if($p<=0 && $pb)
            {
                return false;
            }
        }

        return true;
    }

    public function getPlazasSemana($semana)
    {
        $s = $this->semanas->where('semana',$semana)->first();

        return $s?$s->plazas_disponibles:0;
    }

    public function getSemanaByDesde($desde)
    {
        $f = $this->semanas->where('desde',$desde);
        if($f->count()>0)
        {
            return $f->first();
        }
        return 0;
    }

    public function calcularPrecio( $nSemanas, $booking_id, $solo=false )
    {
        if(($nSemanas<1) || (!$booking_id))
        {
            return 0;
        }

        $total = ($this->precio * $nSemanas) + ($this->precio_extra * ($nSemanas-1));

        if($solo)
        {
            return $total;
        }

        //Especialidades
        $totE = 0;
        $b = Booking::find($booking_id);
        foreach($b->multis as $m)
        {
            $totE += $m->precio;
        }

        return $total + $totE;
    }

    public function calcularFechasBloque( $desde, $nSemanas )
    {
        $fdesde = $this->semanas->where('id',$desde)->first();

        $fechas = [];

        $fini = null;
        $ffin = null;
        $ffin_txt = "-";
        $bloques = 0;
        $semanas = 0;

        if($fdesde)
        {
            $fini = $fdesde->desde;
            $desde_sem = $fdesde->semana;

            //bloques totales de semanas
            $iSemanas = 0;
            $fhasta = null;
            // Buscamos desde la fecha desde semana a semana contando los bloques
            foreach($this->semanas as $sem)
            {
                if($sem->semana>=$desde_sem)
                {
                    $bloques += $sem->bloque;
                    $iSemanas++;

                    if($bloques>=$nSemanas)
                    {
                        $fhasta = $sem;
                        $semanas = $iSemanas;
                        break;
                    }
                }
            }

            //$fhasta = $this->semanas->where('semana', $sem_fin)->first();
            if($fhasta && $bloques==$nSemanas)
            {
                $ffin = $fhasta->hasta;
                $ffin_txt = Carbon::parse($ffin)->format('d/m/Y');
                $hasta_sem = $fhasta->semana;
            }
        }

        return [
            'fecha_ini'=> $fini,
            'fecha_fin'=> $ffin,
            'fecha_fin_txt'=> $ffin_txt,
            'bloques'=> $bloques,
            'semanas'=> $semanas,
        ];
    }

    public function calcularFechas( $desde, $nSemanas )
    {
        $f = $this->semanas->where('id',$desde)->first();

        $fechas = [];

        $fini = null;
        $ffin = null;
        $ffin_txt = "-";

        if($f->count()>0)
        {
            $fini = $f->desde;

            $desde = $f->semana;
            $hasta = $this->semanas->where('semana', ($desde+$nSemanas-1));
            if($hasta->count()>0)
            {
                $ffin = $hasta->first()->hasta;
                $ffin_txt = Carbon::parse($ffin)->format('d/m/Y');
            }

        }

        return [
            'fecha_ini'=> $fini,
            'fecha_fin'=> $ffin,
            'fecha_fin_txt'=> $ffin_txt,
        ];
    }

    public function especialidadesBySemana($n)
    {
        $ret = \VCN\Models\Convocatorias\ConvocatoriaMultiEspecialidad::where('convocatory_id',$this->id)->whereRaw(
           'find_in_set(?, semanas)',
           [$n]
        )->get();

        $ret = [0=>"Seleccionar..."] + $ret->pluck('especialidad_name','id')->toArray();

        return $ret;
    }

    public function getSemanasListAttribute()
    {
        $msemanas = [];
        $mfdesde = [];
        $mtots = 0;

        foreach($this->semanas as $s)
        {
            $msemanas[$s->semana] = $s->semana;
            $mfdesde[$s->semana] = Carbon::parse($s->desde)->format('d/m/Y');

            $mtots++;
        }

        $multi = [
            'total' => $mtots,
            'nsemanas'=> $msemanas,
            'fsemanas'=> $mfdesde,
        ];

        return $multi;
    }

    public function getEdadesAttribute()
    {
        // De todos los booking => edad de viajeros

        $ret = [];

        foreach($this->bookings_completos as $booking)
        {
            if(!in_array($booking->viajero->edad, $ret))
            {
                $ret[] = $booking->viajero->edad;
            }
        }

        asort($ret);

        return $ret;
    }

    public function getSemanasBloquesAttribute()
    {
        $ret = [];

        foreach($this->bookings_completos as $booking)
        {
            if(!in_array($booking->weeks, $ret))
            {
                $ret[] = $booking->weeks;
            }
        }

        asort($ret);

        return $ret;
    }

    public function getSemanasBloquesDetalle($semanas)
    {
        $ret = [];

        foreach($this->bookings_completos->where('weeks',$semanas) as $booking)
        {
            $multi = $booking->multis->sortBy('n')->first();

            if($multi)
            {
                $n = $multi->semana->semana;
                $n2 = ($semanas+$n)-1;

                $nb= "$n a $n2";
                if($n2==$n)
                {
                    $nb = "$n";
                }

                if(!in_array($nb, $ret))
                {
                    $ret[] = $nb;
                }
            }
        }

        asort($ret);

        return $ret;
    }

    public function getBookingsSemanas($desde, $hasta, $prescriptor_id=false)
    {
        $total = [];
        $totales = [];
        $sexo = [];

        foreach($this->semanas as $s)
        {
            $total[$s->semana] = [];
            $totales[$s->semana] = 0;

            $sexo[1][$s->semana] = []; //Niños
            $sexo[2][$s->semana] = []; //Niñas

            $sexo[11][$s->semana] = 0; //Niños totales x semana
            $sexo[12][$s->semana] = 0; //Niñas totales x semana
        }

        $sexo[21] = []; //Niños totales x especialidad
        $sexo[22] = []; //Niñas totales x especialidad

        $sexo[31] = 0; //Niños totales
        $sexo[32] = 0; //Niñas totales

        $cuenta = 0;
        $total[0] = [];

        $fini = Carbon::createFromFormat('d/m/Y',$desde)->format('Y-m-d');
        $ffin = Carbon::createFromFormat('d/m/Y',$hasta)->format('Y-m-d');
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        $bookings = \VCN\Models\Bookings\Booking::where('convocatory_multi_id', $this->id)->whereIn('status_id',$stplazas);
        if($prescriptor_id)
        {
            $bookings = $bookings->where('prescriptor_id',$prescriptor_id);
        }
        $bookings = $bookings->where('fecha_pago1','>=',$fini)->where('fecha_pago1','<=',$ffin)->get();

        foreach($bookings as $booking)
        {
            foreach($booking->multis as $multi)
            {
                if(!isset($total[$multi->semana->semana][$multi->especialidad_id]))
                {
                    $total[$multi->semana->semana][$multi->especialidad_id] = 0;
                }
                $total[$multi->semana->semana][$multi->especialidad_id]++;

                if(!isset($total[0][$multi->especialidad_id]))
                {
                    $total[0][$multi->especialidad_id] = 0;
                }
                $total[0][$multi->especialidad_id]++;

                if(!isset($sexo[$booking->viajero->sexo][$multi->semana->semana][$multi->especialidad_id]))
                {
                    $sexo[$booking->viajero->sexo][$multi->semana->semana][$multi->especialidad_id] = 0;
                }
                $sexo[$booking->viajero->sexo][$multi->semana->semana][$multi->especialidad_id]++;

                if(!isset($sexo[$booking->viajero->sexo+20][$multi->especialidad_id]))
                {
                    $sexo[$booking->viajero->sexo+20][$multi->especialidad_id] = 0;
                }
                $sexo[$booking->viajero->sexo+20][$multi->especialidad_id]++;

                $totales[$multi->semana->semana]++;
                $sexo[$booking->viajero->sexo + 10][$multi->semana->semana]++;

                $sexo[$booking->viajero->sexo + 30]++;

                $cuenta++;
            }
        }

        $ret = [
            'total'=> $total,
            'totales'=> $totales,
            'sexo'=> $sexo,
            'cuenta'=> $cuenta
        ];

        return $ret;
    }

    public function getBookingsEdades($desde, $hasta, $prescriptor_id=false)
    {
        $total = [];
        $totales = [];
        $sexo = [];

        foreach($this->semanas as $s)
        {
            $sem = Carbon::parse($s->desde)->format('d/m/Y');

            $total[$sem] = [];
            $totales[$sem] = [];

            $sexo[1][$sem] = []; //Niños
            $sexo[2][$sem] = []; //Niñas

            $sexo[11][$sem] = []; //Niños totales
            $sexo[12][$sem] = []; //Niñas totales
        }

        $fini = Carbon::createFromFormat('d/m/Y',$desde)->format('Y-m-d');
        $ffin = Carbon::createFromFormat('d/m/Y',$hasta)->format('Y-m-d');
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        $bookings = \VCN\Models\Bookings\Booking::where('convocatory_multi_id', $this->id)->whereIn('status_id',$stplazas);
        if($prescriptor_id)
        {
            $bookings = $bookings->where('prescriptor_id',$prescriptor_id);
        }
        $bookings = $bookings->where('fecha_pago1','>=',$fini)->where('fecha_pago1','<=',$ffin)->get();

        foreach($bookings as $booking)
        {
            foreach($booking->multis as $multi)
            {
                $sem = Carbon::parse($multi->semana->desde)->format('d/m/Y');

                if(!isset($total[$sem][$multi->especialidad_id]))
                {
                    $total[$sem][$multi->especialidad_id] = [];
                }
                if(!isset($total[$sem][$multi->especialidad_id][$booking->viajero->edad]))
                {
                    $total[$sem][$multi->especialidad_id][$booking->viajero->edad] = 0;
                }
                $total[$sem][$multi->especialidad_id][$booking->viajero->edad]++;

                if(!isset($sexo[$booking->viajero->sexo][$sem][$multi->especialidad_id]))
                {
                    $sexo[$booking->viajero->sexo][$sem][$multi->especialidad_id] = [];
                }
                if(!isset($sexo[$booking->viajero->sexo][$sem][$multi->especialidad_id][$booking->viajero->edad]))
                {
                    $sexo[$booking->viajero->sexo][$sem][$multi->especialidad_id][$booking->viajero->edad] = 0;
                }
                $sexo[$booking->viajero->sexo][$sem][$multi->especialidad_id][$booking->viajero->edad]++;


                if(!isset($totales[$sem][$booking->viajero->edad]))
                {
                    $totales[$sem][$booking->viajero->edad] = 0;
                }
                $totales[$sem][$booking->viajero->edad]++;

                if(!isset($sexo[$booking->viajero->sexo + 10][$sem][$booking->viajero->edad]))
                {
                    $sexo[$booking->viajero->sexo + 10][$sem][$booking->viajero->edad] = 0;
                }
                $sexo[$booking->viajero->sexo + 10][$sem][$booking->viajero->edad]++;
            }
        }

        $ret = [
            'total'=> $total,
            'totales'=> $totales,
            'sexo'=> $sexo,
        ];

        return $ret;
    }

    public function getBookingsBloques($desde, $hasta, $prescriptor_id=false)
    {
        $total = [];
        $sexo = [];

        $fini = Carbon::createFromFormat('d/m/Y',$desde)->format('Y-m-d');
        $ffin = Carbon::createFromFormat('d/m/Y',$hasta)->format('Y-m-d');
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        $bookings = \VCN\Models\Bookings\Booking::where('convocatory_multi_id', $this->id)->whereIn('status_id',$stplazas);
        if($prescriptor_id)
        {
            $bookings = $bookings->where('prescriptor_id',$prescriptor_id);
        }
        $bookings = $bookings->where('fecha_pago1','>=',$fini)->where('fecha_pago1','<=',$ffin)->get();

        foreach($bookings as $booking)
        {
            $sem = $booking->weeks;

            $total[$sem] = [];
            $sexo[1][$sem] = []; //Niños
            $sexo[2][$sem] = []; //Niñas

        }

        ksort($total);

        foreach($total as $sem=>$value)
        {
            foreach($bookings->where('weeks',$sem) as $booking)
            {
                $multi = $booking->multis->sortBy('n')->first();

                if($multi)
                {
                    $n = $multi->semana->semana;
                    $n2 = ($sem+$n)-1;

                    $nb= "$n a $n2";
                    if($n2==$n)
                    {
                        $nb = "$n";
                    }

                    if(!isset($total[$sem][$nb]))
                    {
                        $total[$sem][$nb] = 0;
                    }
                    $total[$sem][$nb]++;

                    if(!isset($sexo[$booking->viajero->sexo][$sem][$nb]))
                    {
                        $sexo[$booking->viajero->sexo][$sem][$nb] = 0;
                    }
                    $sexo[$booking->viajero->sexo][$sem][$nb] ++;
                }
            }
        }

        $ret = [
            'total'=> $total,
            'sexo'=> $sexo,
        ];

        return $ret;
    }

    public function cuestionarios()
    {
        // return $this->hasManyThrough('\VCN\Models\System\Cuestionario', '\VCN\Models\System\CuestionarioVinculado', 'modelo_id', 'id')->where('modelo','ConvocatoriaMulti');
        return $this->belongsToMany('\VCN\Models\System\Cuestionario', 'cuestionario_vinculados','modelo_id')->where('modelo','ConvocatoriaMulti')->withPivot('id');
    }

    public function examenes()
    {
        return $this->belongsToMany(\VCN\Models\Exams\Examen::class, 'examen_vinculados','modelo_id')->where('modelo','ConvocatoriaMulti')->withPivot('id', 'excluye');
    }

    public function getParentsAttribute()
    {
        return ['cursos'];
    }

    public function getEsFacturableAttribute()
    {
        $valores['convocatorias'] = $this->id;
        $valores['tipoc'] = 4;
        $valores['plataformas'] = ConfigHelper::config('propietario');
        $bookings = Booking::listadoFiltros($valores);

        $bFacturas = false;
        if(!$this->no_facturar)
        {
            foreach($bookings->get() as $booking)
            {
                if($booking->es_facturable && !$booking->no_facturar)
                {
                    $bFacturas = true;
                    break;
                }
            }
        }

        return $bFacturas;
    }

    public function getMontoReservaTxtAttribute()
    {
        if($this->reserva_tipo)
        {
            return $this->reserva_valor?$this->reserva_valor ."%":"";
        }

        return ConfigHelper::parseMoneda($this->reserva_valor);
    }

    public function updateContable()
    {
        foreach($this->bookings_completos as $booking)
        {
            $fechaini = Carbon::parse( $booking->course_start_date );
            if($fechaini->isFuture())
            {
                $booking->setContable(true);
            }
        }
    }

    public function getContactoSOS($proveedor, $plataforma=0)
    {
        $m = \VCN\Models\System\ContactoModel::where('modelo','ConvocatoriaMulti')->where('modelo_id',$this->id);
        $c = clone $m;

        if($proveedor)
        {
            return $c->where('es_proveedor',1)->get();
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',$plataforma)->first();
        if($c)
        {
            return $c;
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',0)->first();
        return $c ?: 0;
    }
}
