<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Centros\Centro;
use VCN\Models\Cursos\Curso;
use VCN\Models\Bookings\Booking;
use VCN\Models\System\Documento;
use VCN\Models\Monedas\Moneda;

use VCN\Helpers\ConfigHelper;
use Carbon;
use Session;

use \VCN\Models\System\BaseModel;

class Cerrada extends BaseModel
{
    protected $table = 'convocatoria_cerradas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'condiciones'   => 'json',
        'corporativo_info'   => 'array',
    ];

    public function delete()
    {
        if(Booking::where('convocatory_close_id',$this->id)->count()>0)
        {
            $error = "Imposible eliminar. Existen Bookings con esta Convocatoria.";
            Session::flash('mensaje-alert', $error);
            return back();
        }

        parent::delete();
    }

    public function scopeActivos($query)
    {
        return $query->where('convocatory_close_status','>',0);
    }

    public static function plataforma()
    {
        // if( auth()->user()->isFullAdmin() )
        //     return self::all();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            $proveedores = Proveedor::where('propietario',$filtro)->orWhere('propietario',0)->pluck('id');
            $centros = Centro::whereIn('provider_id',$proveedores)->pluck('id');
            $cursos = Curso::whereIn('center_id',$centros)->pluck('id');
            return self::whereIn('course_id',$cursos)->get();
        }

        return self::all();
    }

    public function getDocumentos($idioma)
    {
        $docs = \VCN\Models\System\Documento::where('modelo','Cerrada')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();

        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::where('modelo','Cerrada')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();
    }
    
    public function getDocumentosArea($idioma, $plataforma=null)
    {
        $filtro = [0, $plataforma ?: ConfigHelper::config('propietario') ];

        $docs = \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','Cerrada')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','Cerrada')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
    }

    public function monitores()
    {
        return $this->hasMany('\VCN\Models\Monitores\MonitorRelacion', 'modelo_id')->where('modelo','Cerrada');
    }

    public function curso()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Curso', 'course_id');
    }

    public function alojamiento()
    {
        return $this->belongsTo('\VCN\Models\Alojamientos\Alojamiento', 'alojamiento_id');
    }

    public function getRouteAttribute()
    {
        return route('manage.convocatorias.cerradas.ficha', $this->id);
    }

    public function bookings()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'convocatory_close_id');
    }

    public function bookings_completos()
    {
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'convocatory_close_id')->whereIn('status_id',$stplazas);
    }

    public function bookings_completos_futuro()
    {
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'convocatory_close_id')->where('course_start_date','>',Carbon::now())->whereIn('status_id',$stplazas);
    }

    public function getBookingsReservaAttribute()
    {
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        return Booking::where('convocatory_close_id',$this->id)->whereIn('status_id',$stplazas)->get();
    }

    public function incluyes()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\CerradaIncluye', 'convocatory_id');
    }

    public function incluyeValor($inc_id)
    {
        $incluye = $this->incluyes->where('incluye_id',$inc_id)->first();

        return $incluye?$incluye->valor:0;
    }

    public function vuelos()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\CerradaVuelo', 'convocatory_id');
    }

    public function getAnyAttribute()
    {
        if(!$this->convocatory_close_start_date)
        {
            return 0;
        }

        return Carbon::parse($this->convocatory_close_start_date)->year;
    }

    public function getVuelosAnyAttribute()
    {
        $any = Carbon::parse($this->convocatory_close_start_date)->year;
        $vuelos = \VCN\Models\Convocatorias\Vuelo::where('any', $any)->get();

        return $vuelos;
    }

    public function plazas()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\Plaza', 'convocatory_id');
    }

    public function getPlazas($alojamiento_id)
    {
        $ret = \VCN\Models\Convocatorias\Plaza::where('alojamiento_id',$alojamiento_id)->where('convocatory_id',$this->id);

        if($ret->count()>0)
        {
            return $ret->first();
        }

        // $pr = $this->bookings->where('accommodation_id',$alojamiento_id)->where('status_id',ConfigHelper::config('booking_status_prebooking'))->count();
        // $ppr = $this->bookings->where('accommodation_id',$alojamiento_id)->where('status_id',ConfigHelper::config('booking_status_prereserva'))->count();

        // $p = new \VCN\Models\Convocatorias\Plaza;

        // $p->plazas_reservas = $pr;
        // $p->plazas_prereservas = $ppr;
        // $p->plazas_overbooking = $this->bookings->where('accommodation_id',$alojamiento_id)->where('status_id',ConfigHelper::config('booking_status_overbooking'))->count();
        // $p->plazas_disponibles = 0 - ($pr+$ppr);

        // return $p;

        return null;
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda', 'convocatory_close_currency_id');
    }

    public function tabla_cambio()
    {
        return $this->hasOne('\VCN\Models\Monedas\CerradaCambio', 'convocatory_id');
    }

    public function precio_auto()
    {
        return $this->hasOne('\VCN\Models\Convocatorias\CerradaPrecio', 'convocatory_id');
    }

    public function getNameAttribute()
    {
        return $this->convocatory_close_name;
    }

    public function getDuracionDiasAttribute()
    {
        return Carbon::parse($this->convocatory_close_start_date)->diffInDays(Carbon::parse($this->convocatory_close_end_date));
    }

    public function getStartDateAttribute()
    {
        return Carbon::parse($this->convocatory_close_start_date)->format('d/m/Y');
    }

    public function getEndDateAttribute()
    {
        return Carbon::parse($this->convocatory_close_end_date)->format('d/m/Y');
    }

    public function getStartDateYmdAttribute()
    {
        return Carbon::parse($this->convocatory_close_start_date)->format('Y-m-d');;
    }

    public function getEndDateYmdAttribute()
    {
        return Carbon::parse($this->convocatory_close_end_date)->format('Y-m-d');;
    }

    public function getContableAttribute()
    {
        return $this->convocatory_close_code;
    }

    public function getDuracionAttribute()
    {
        return $this->convocatory_close_duration_weeks;
    }

    public function getPrecioAttribute()
    {
        return $this->convocatory_close_price;
    }

    public function getMonedaIdAttribute()
    {
        return $this->convocatory_close_currency_id;
    }

    public function getMonedaNameAttribute()
    {
        return $this->moneda?$this->moneda->currency_name:"-";
        // return $this->convocatory_close_currency_id?$this->moneda->currency_name:Session::get('vcn.moneda');
    }

    public function getDuracionNameAttribute()
    {
        return ConfigHelper::getPrecioDuracionUnit($this->duracion_fijo);
        //por defecto semanas
        // return $this->duracion_fijo?ConfigHelper::getPrecioDuracionUnit($this->duracion_fijo):" Semanas";
        // return $this->convocatory_close_currency_id?$this->moneda->currency_name:Session::get('vcn.moneda');
    }

    public function getDuracionName($units)
    {
        return trans_choice("web.curso.".ConfigHelper::getPrecioDuracionUnit($this->duracion_fijo), $units);
    }

    public function getPlazasReservasAttribute()
    {
        return $this->plazas->sum('plazas_reservas') + $this->plazas_online;

        // $p = $this->bookings->where('status_id',ConfigHelper::config('booking_status_prebooking'));
        // return $p->count();
    }

    public function getPlazasPrereservasAttribute()
    {
        return $this->plazas->sum('plazas_prereservas');

        // $p = $this->bookings->where('status_id',ConfigHelper::config('booking_status_prereserva'));
        // return $p->count();
    }

    public function getPlazasOverbookingAttribute()
    {
        return $this->plazas->sum('plazas_overbooking');
    }

    public function getPlazasOnlineAttribute()
    {
        return $this->plazas->sum('plazas_online');
    }

    public function getPlazasDisponiblesAttribute()
    {
        return $this->plazas->sum('plazas_disponibles');
    }

    public function getPlazasDisponiblesOvbkgAttribute()
    {
        return $this->plazas->sum('plazas_disponibles_ovbkg');
    }

    public function cuestionarios()
    {
        // return $this->hasManyThrough('\VCN\Models\System\Cuestionario', '\VCN\Models\System\CuestionarioVinculado', 'modelo_id', 'id')->where('modelo','Cerrada');
        return $this->belongsToMany('\VCN\Models\System\Cuestionario', 'cuestionario_vinculados','modelo_id')->where('modelo','Cerrada')->withPivot('id');
    }

    public function examenes()
    {
        return $this->belongsToMany(\VCN\Models\Exams\Examen::class, 'examen_vinculados','modelo_id')->where('modelo','Cerrada')->withPivot('id', 'excluye');
    }

    public function getCategoriaAttribute()
    {
        return $this->curso->categoria;
    }

    public function getSubcategoriaAttribute()
    {
        return $this->curso->subcategoria;
    }

    public function getSubcategoriaDetalleAttribute()
    {
        return $this->curso->subcategoria_detalle;
    }

    public function getCentroAttribute()
    {
        return $this->curso->centro;
    }

    public function getParentsAttribute()
    {
        return ['subcategoria_detalle', 'subcategoria', 'categoria', 'centro', 'curso'];
    }

    public function getPrecioVueloAttribute()
    {
        if(!$this->vuelos->count())
        {
            return 0;
        }

        $vuelo = $this->vuelos->first()->vuelo;

        // Vuelo: suma de Precio+Tasas+Margen
        return $vuelo->precio + $vuelo->tasas + $vuelo->margen;

    }

    public function updatePrecio($vd=0, $data=null)
    {
        $precio = $this->precio_auto;

        if(!$precio)
        {
            $precio = new CerradaPrecio;
            $precio->convocatory_id = $this->id;
            $precio->moneda_id = isset($data['moneda_id']) ? $data['moneda_id'] : $this->convocatory_close_currency_id;
            $precio->save();
        }

        if($data)
        {
            $precio->update($data);
        }

        $precio->vuelo = $this->precio_vuelo;
        $precio->save();

        //Tabla cambios
        $tc = $this->tabla_cambio;
        if(!$tc)
        {
            $any = Carbon::parse($this->convocatory_close_start_date)->year;

            if($this->curso->categoria)
            {
                $tc = $this->curso->categoria->getTablaCambioAny($any, $this->moneda);
            }
        }

        $total = 0;

        $campos = ['proveedor','vuelo','seguro','gastos','monitor','mb','comision'];
        foreach( $campos as $c)
        {
            $t = $precio->$c ?: 0;

            $campo = $c."_moneda_id";

            $m = (int)$precio->$campo ?: 0;

            if($precio->moneda_id != $m)
            {
                if($tc)
                {
                    $mc = $tc->cambios->where('moneda_id',$m)->first();
                    if($mc)
                    {
                        $rate = $mc->tc_calculo;
                        $t = $t * $rate;
                    }
                    else
                    {
                        $moneda = Moneda::find($m);
                        $t = $t * $moneda->rate;
                    }
                }
                else
                {
                    $moneda = Moneda::find($m);
                    $t = $t * $moneda->rate;
                }
            }

            $total += (float)$t;
        }

        $precio->precio = $total;
        $precio->save();

        $this->convocatory_close_price = $total;
        $this->convocatory_close_currency_id = $precio->moneda_id;
        $this->save();

        $this->setDivisa();
    }

    public function getEsFacturableAttribute()
    {
        $valores['convocatorias'] = $this->id;
        $valores['tipoc'] = 1;
        $valores['plataformas'] = ConfigHelper::config('propietario');
        $bookings = Booking::listadoFiltros($valores);

        $bFacturas = false;
        if(!$this->no_facturar)
        {
            foreach($bookings->get() as $booking)
            {
                if($booking->es_facturable && !$booking->no_facturar)
                {
                    $bFacturas = true;
                    break;
                }
            }
        }

        return $bFacturas;
    }

    public function getMontoReservaTxtAttribute()
    {
        if($this->reserva_tipo)
        {
            return $this->reserva_valor?$this->reserva_valor ."%":"";
        }

        return ConfigHelper::parseMoneda($this->reserva_valor);
    }

    public function updateContable()
    {
        foreach($this->bookings_completos as $booking)
        {
            $fechaini = Carbon::parse( $booking->course_start_date );
            if($fechaini->isFuture())
            {
                $booking->setContable(true);
            }
        }
    }

    public static function listadoFiltros($valores)
    {
        $convos = \VCN\Models\Convocatorias\Cerrada::select();

        //categorias
        $f = isset($valores['categorias'])?$valores['categorias']:0;
        if($f)
        {
            $cursos = \VCN\Models\Cursos\Curso::where('category_id', $f)->pluck('id')->toArray();
            $convos = $convos->whereIn('course_id',$cursos);   
        }

        //subcategorias
        $f = isset($valores['subcategorias'])?$valores['subcategorias']:0;
        if($f)
        {
            $cursos = \VCN\Models\Cursos\Curso::where('subcategory_id', $f)->pluck('id')->toArray();
            $convos = $convos->whereIn('course_id',$cursos);   
        }

        return $convos;
    }

    public function getContactoSOS($proveedor, $plataforma=0)
    {
        $m = \VCN\Models\System\ContactoModel::where('modelo','Cerrada')->where('modelo_id',$this->id);
        $c = clone $m;

        if($proveedor)
        {
            return $c->where('es_proveedor',1)->get();
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',$plataforma)->first();
        if($c)
        {
            return $c;
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',0)->first();
        return $c ?: 0;
    }

    public function setDivisa($valor=null)
    {
        $convocatoria = $this;
        $precio = $convocatoria->precio_auto;

        if(!$precio)
        {
            return false;
        }

        if($valor)
        {
            $vd = $valor;
            $precio->divisa_variacion = $vd;
            $precio->divisa_total = $convocatoria->precio + $vd;
            $precio->divisa_fecha = Carbon::now()->format('Y-m-d');
            $precio->save();

            $msj = 'Variación de divisa actualizada';
            Session::flash('mensaje-ok', $msj);

            return true;
        }

        $m = $convocatoria->moneda;
        $mp = ConfigHelper::default_moneda();

        if( !$m )
        {
            $msj = 'No hay moneda definida para la convocatoria';
            Session::flash('mensaje-alert', $msj);
            return false;
        }

        if( $m->id != $mp->id )
        {
            return false;
        }

        $categoria = $convocatoria->curso->categoria;
        $any = $convocatoria->any;
        $tc = $convocatoria->tabla_cambio;
        $tc = $tc ?: $categoria->getTablaCambioAny($any, $m);

        $vd = 0;
        $campos = ['proveedor','vuelo','seguro','gastos','monitor','mb','comision'];
        $mc = $m->currency_rate ?: 1;
        if($tc && $tc->cambios)
        {
            foreach($campos as $c)
            {
                $ci = $c."_moneda_id";

                if($tc && $tc->cambios)
                {
                    $mi = $tc->cambios->where('moneda_id', $precio->$ci)->first();
                    $mc = (float) ($mi ? $mi->tc_final : $mc);
                    $mf = (float) ($mi ? $mi->tc_folleto : 0);
                    $mc = $mc - $mf;
                }

                $mi = $precio->$ci;

                //solo los q son otra moneda
                if($mc>0 && $mi && $mi != $m->id)
                {
                    $vd += $precio->$c * $mc;
                }
            }
        }

        //vd vs 9.9% of $convocatoria->precio???
        $msj = 'Variación de divisa calculada';
        $vdMax = round(($convocatoria->precio * 9.9 )/100, 2);
        if( $vd > $vdMax )
        {
            $msj = "Variación de divisa calculada ($vd :: 9.9% => $vdMax)";
            $vd = $vdMax;
        }

        $precio->divisa_variacion = $vd;
        $precio->divisa_total = $convocatoria->precio + $vd;
        $precio->divisa_fecha = Carbon::now()->format('Y-m-d');
        $precio->save();

        Session::flash('mensaje-ok', $msj);
        return true;
    }

}
