<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

class ConvocatoriaMultiEspecialidad extends Model
{
    protected $table = 'convocatoria_multi_especialidades';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function convocatoria()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\ConvocatoriaMulti', 'convocatory_id');
    }

    public function especialidad()
    {
        return $this->belongsTo('\VCN\Models\Subespecialidad', 'especialidad_id');
    }

    public function moneda()
    {
        return $this->convocatoria->moneda_id;
    }

    public function getMonedaNameAttribute()
    {
        return $this->convocatoria->moneda->name;
    }

    public function getEspecialidadNameAttribute()
    {
        return $this->especialidad ? $this->especialidad->name : "?";
    }
}
