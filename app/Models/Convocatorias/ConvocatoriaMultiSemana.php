<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Booking;

use ConfigHelper;
use Carbon;

class ConvocatoriaMultiSemana extends Model
{
    protected $table = 'convocatoria_multi_semanas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function convocatoria()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\ConvocatoriaMulti', 'convocatory_id');
    }

    public function getMonedaNameAttribute()
    {
        return $this->convocatoria->moneda->name;
    }

    public function getPlazasReservasAttribute()
    {
        $p = Booking::where('convocatory_multi_id',$this->convocatoria->id)
            ->where('course_start_date','<=', $this->desde)
            ->where('course_end_date','>=', $this->hasta)
            ->where('status_id',ConfigHelper::config('booking_status_prebooking'));

        $po = Booking::where('convocatory_multi_id',$this->convocatoria->id)
            ->where('course_start_date','<=', $this->desde)
            ->where('course_end_date','>=', $this->hasta)
            ->where('status_id',ConfigHelper::config('booking_status_overbooking'))->where('ovbkg_pa',0);

        //->where('accommodation_id',$this->alojamiento_id) ???
        return $p->count() + $po->count();
    }

    public function getPlazasReservasPlataforma($p)
    {
        $p = Booking::where('convocatory_multi_id',$this->convocatoria->id)->where('plataforma',$p)
            ->where('course_start_date','<=', $this->desde)
            ->where('course_end_date','>=', $this->hasta)
            ->where('status_id',ConfigHelper::config('booking_status_prebooking'));

        $po = Booking::where('convocatory_multi_id',$this->convocatoria->id)
            ->where('course_start_date','<=', $this->desde)
            ->where('course_end_date','>=', $this->hasta)
            ->where('status_id',ConfigHelper::config('booking_status_overbooking'))->where('ovbkg_pa',0);

        //->where('accommodation_id',$this->alojamiento_id) ???
        return $p->count() + $po->count() + $this->plazas_online;
    }

    public function getPlazasPrereservasAttribute()
    {
        $p = Booking::where('convocatory_multi_id',$this->convocatoria->id)
            ->where('course_start_date','<=', $this->desde)
            ->where('course_end_date','>=', $this->hasta)
            ->where('status_id',ConfigHelper::config('booking_status_prereserva'));

        //->where('accommodation_id',$this->alojamiento_id) ???
        return $p->count();
    }

    public function getPlazasOverbookingAttribute()
    {
        $p = Booking::where('convocatory_multi_id',$this->convocatoria->id)
            ->where('course_start_date','<=', $this->desde)
            ->where('course_end_date','>=', $this->hasta)
            ->where('status_id',ConfigHelper::config('booking_status_overbooking'));

        //->where('accommodation_id',$this->alojamiento_id) ???
        return $p->count();
    }

    public function getPlazasOverbookingPlataforma($p)
    {
        $p = Booking::where('convocatory_multi_id',$this->convocatoria->id)->where('plataforma',$p)
            ->where('course_start_date','<=', $this->desde)
            ->where('course_end_date','>=', $this->hasta)
            ->where('status_id',ConfigHelper::config('booking_status_overbooking'));

        //->where('accommodation_id',$this->alojamiento_id) ???
        return $p->count();
    }

    public function getPlazasOnlineAttribute()
    {
        $hora = Carbon::now();
        $hora1 = Carbon::now()->subHour();
        $hora2 = Carbon::now()->subHours(96);

        //tpv_plazas : 1hora
        $p1 = Booking::where('convocatory_multi_id',$this->convocatoria->id)
            ->where('course_start_date','<=', $this->desde)
            ->where('course_end_date','>=', $this->hasta)
            ->where('es_online',1)->where('tpv_status',false)
            ->where('tpv_plazas','>=',$hora1)->where('tpv_plazas','<=',$hora);

        //tpv_plazas : 3dias (backend)
        $p2 = Booking::where('convocatory_multi_id',$this->convocatoria->id)
            ->where('course_start_date','<=', $this->desde)
            ->where('course_end_date','>=', $this->hasta)
            ->where('es_online',2)->where('tpv_status',false)
            ->where('tpv_plazas','>=',$hora2)->where('tpv_plazas','<=',$hora);

        return $p1->count() + $p2->count();
    }

    public function getPlazasDisponiblesAttribute()
    {
        return ($this->plazas_totales - $this->plazas_bloqueadas - $this->plazas_reservas - $this->plazas_prereservas - $this->plazas_overbooking );
    }

    public function getPlazasDisponiblesOvbkgAttribute()
    {
        return ($this->plazas_totales - $this->plazas_bloqueadas - $this->plazas_reservas - $this->plazas_prereservas );
    }

    public function getPlazasVendidasAttribute()
    {
        return ($this->plazas_reservas - $this->plazas_overbooking );
    }

    public function getBloqueAttribute()
    {
        $f1 = Carbon::parse($this->desde);
        $f2 = Carbon::parse($this->hasta);

        $ret = $f1->diffInWeeks($f2);
        return $ret?:1;
    }

}
