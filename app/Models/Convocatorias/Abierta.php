<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Monedas\Moneda;
use VCN\Models\Convocatorias\Precio;
use VCN\Models\Bookings\Booking;

use Carbon;
use Session;
use ConfigHelper;

use \VCN\Models\System\BaseModel;

class Abierta extends BaseModel
{
    protected $table = 'convocatoria_abiertas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'condiciones'    => 'json',
    ];

    public function delete()
    {
        if(Booking::where('convocatory_open_id',$this->id)->count()>0)
        {
            $error = "Imposible eliminar. Existen Bookings con esta Convocatoria.";
            Session::flash('mensaje-alert', $error);
            return back();
        }

        parent::delete();
    }

    public function monitores()
    {
        return $this->hasMany('\VCN\Models\Monitores\MonitorRelacion', 'modelo_id')->where('modelo','Abierta');
    }

    public function curso()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Curso', 'course_id');
    }

    public function getRouteAttribute()
    {
        return route('manage.convocatorias.abiertas.ficha', $this->id);
    }

    public function getDocumentos($idioma)
    {
        $docs = \VCN\Models\System\Documento::where('modelo','Abierta')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();

        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::where('modelo','Abierta')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();
    }
    
    public function getDocumentosArea($idioma, $plataforma=null)
    {
        $filtro = [0, $plataforma ?: ConfigHelper::config('propietario') ];

        $docs = \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','Abierta')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','Abierta')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
    }

    public function getPlazas($alojamiento_id)
    {
        return null; //no tiene plazas
    }

    public function bookings()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'convocatory_open_id');
    }

    public function bookings_completos()
    {
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'convocatory_open_id')->whereIn('status_id',$stplazas);
    }

    public function bookings_completos_futuro()
    {
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'convocatory_open_id')->where('course_start_date','>',Carbon::now())->whereIn('status_id',$stplazas);
    }

    public function incluyes()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\AbiertaIncluye', 'convocatory_id');
    }

    public function incluyeValor($inc_id)
    {
        $incluye = $this->incluyes->where('incluye_id',$inc_id)->first();

        return $incluye?$incluye->valor:0;
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda', 'convocatories_open_currency_id');
    }

    public function costes()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\Coste', 'convocatory_id');
    }

    public function precios()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\Precio', 'convocatory_id');
    }

    public function precio_extras()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\PrecioExtra', 'convocatory_id');
    }

    public function ofertas()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\Oferta', 'convocatory_id');
    }

    public function getNameAttribute()
    {
        return $this->convocatory_open_name;
    }

    public function getDesdeAttribute()
    {
        return $this->convocatory_open_valid_start_date;
    }
    public function getHastaAttribute()
    {
        return $this->convocatory_open_valid_end_date;
    }

    public function getDuracionDiasAttribute()
    {
        return Carbon::parse($this->convocatory_open_valid_start_date)->diffInDays(Carbon::parse($this->convocatory_open_valid_end_date));
    }

    public function getStartDayAttribute()
    {
        return $this->convocatory_open_start_day;
    }

    public function getEndDayAttribute()
    {
        return $this->convocatory_open_end_day;
    }

    public function getStartDateAttribute()
    {
        return Carbon::parse($this->convocatory_open_valid_start_date)->format('d/m/Y');;
    }

    public function getEndDateAttribute()
    {
        return Carbon::parse($this->convocatory_open_valid_end_date)->format('d/m/Y');;
    }

    public function getStartDateYmdAttribute()
    {
        return Carbon::parse($this->convocatory_open_valid_start_date)->format('Y-m-d');;
    }

    public function getEndDateYmdAttribute()
    {
        return Carbon::parse($this->convocatory_open_valid_end_date)->format('Y-m-d');;
    }

    public function getMonedaIdAttribute()
    {
        return $this->convocatory_open_currency_id;
    }

    public function getMonedaNameAttribute()
    {
        return $this->moneda->name;
        // return $this->convocatories_open_currency_id?$this->moneda->currency_name:Session::get('vcn.moneda');
    }

    public function getDuracionNameAttribute()
    {
        return $this->curso->duracion_name;
    }

    public function getDuracionName($units)
    {
        return trans_choice("web.curso.".$this->duracion_name,$units);
    }

    public function getContableAttribute()
    {
        return $this->convocatory_open_code;
    }

    public function calcularPrecio($desde, $hasta, $duracion, $extras=true)
    {
        $duracion_unit = $this->duracion_unit;

        $costTotal = 0;
        $weeks_numbers = array();

        $moneda = ConfigHelper::config('moneda');
        $moneda_id = Moneda::where('currency_name', $moneda)->first()->id;

        if(!$desde)
        {
            return [ 'precio'=> "ERROR", 'importe'=>0, 'moneda'=> $moneda, 'moneda_id'=> $moneda_id];
        }

        if($this->precios->count()<1)
        {
            return [ 'precio'=> "INCLUÍDO", 'importe'=>0, 'moneda'=> $moneda, 'moneda_id'=> $moneda_id];
        }

        $start_date = Carbon::createFromFormat('d/m/Y',$desde)->format('Y-m-d');
        $end_date = Carbon::createFromFormat('d/m/Y',$hasta)->format('Y-m-d');

        $start_date_weeks = Carbon::createFromFormat('d/m/Y',$desde);

        //Array semanas/meses/trimestres/semestres/años
        for ($i = 1; $i < $duracion +1; $i++)
        {
            switch($duracion_unit)
            {
                case 1: //semanas
                {
                    while($start_date_weeks->isWeekend())
                    {
                        $start_date_weeks->addDay();
                    }

                    $start_date_weeks_number['n'] = $start_date_weeks->format('W');
                    $start_date_weeks_number['y'] = $start_date_weeks->format('Y');

                    $start_date_weeks =  $start_date_weeks->addWeek();
                }
                break;

                case 2: //meses
                {
                    $start_date_weeks_number['n'] = $start_date_weeks->format('m');
                    $start_date_weeks_number['y'] = $start_date_weeks->format('Y');

                    $start_date_weeks =  $start_date_weeks->addMonth();
                }
                break;

                case 3: //trimestres
                {
                    $start_date_weeks_number['n'] = ceil($start_date_weeks->format('m')/3);
                    $start_date_weeks_number['y'] = $start_date_weeks->format('Y');

                    $start_date_weeks =  $start_date_weeks->addMonths(3);
                }
                break;

                case 4: //semestres
                {
                    $start_date_weeks_number['n'] = ceil($start_date_weeks->format('m')/6);
                    $start_date_weeks_number['y'] = $start_date_weeks->format('Y');

                    $start_date_weeks =  $start_date_weeks->addMonths(6);
                }
                break;

                case 5: //años
                {
                    $start_date_weeks_number['n'] = $start_date_weeks->format('Y');
                    $start_date_weeks_number['y'] = $start_date_weeks->format('Y');

                    $start_date_weeks =  $start_date_weeks->addYear();
                }
                break;
            }

            array_push($weeks_numbers, $start_date_weeks_number);
        }

        $currentWeekNumber = array();
        $cost = null;
        $bDisponible = false;

        // dd($weeks_numbers);

        $semanas = 0; //Necesitamos saber las semanas de las que hay precio
        $bMontoFijo = false;

        foreach($weeks_numbers as $wni=>$wn)
        {
            $cost = $this->calcularPrecioByDuracion($duracion, $wn);

            if($cost)
            {
                $semanas++;

                $importe = floatval($cost->importe);

                if($cost->duracion==1 && ($cost->rango1 < $duracion)) //monto fijo con duración menor
                {
                    $importe = ceil($duracion/$cost->rango1) * $cost->importe;
                }

                $costTotal += $importe;

                $moneda_id = $cost->moneda_id;
                $moneda = $cost->moneda->name;

                // echo $wn['n'] . " + " . $importe . "<br>";

                $bDisponible = true;

                if($cost->duracion==1 && ($cost->rango1 == $duracion))
                {
                    $bMontoFijo = true;
                    break;
                }
            }
        }

        //Extras
        $cost_extra = null;
        $costTotal_extras = 0;
        if($bDisponible && $extras)
        {
            foreach($weeks_numbers as $wn)
            {
                $cost_extras = $this->calcularPrecioByDuracion($duracion, $wn, true);

                if($cost_extras)
                {
                    $importe = $cost_extras->importe;

                    if($cost_extras->duracion==1 && ($cost_extras->rango1 < $duracion)) //monto fijo con duración menor
                    {
                        $importe = ceil($duracion/$cost_extras->rango1) * $cost_extras->importe;
                    }

                    $costTotal_extras += $importe;

                    $moneda_id = $cost_extras->moneda_id;
                    $moneda = $cost_extras->moneda->name;

                    // echo $wn['n'] . " + " . $importe . "<br>";

                    if($cost_extras->duracion==1 && ($cost_extras->rango1 == $duracion))
                    {
                        break;
                    }
                }
            }
        }

        if(!$bDisponible)
        {
            $costTotal = 0;
            $costTotal_extras = 0;
        }

        return $cost?[ 'importe'=> $costTotal + $costTotal_extras, 'importe_base'=> $costTotal, 'importe_extra'=> $costTotal_extras, 'moneda'=> $moneda, 'moneda_id'=>$moneda_id, 'semanas'=> $semanas, 'duracion'=> $this->duracion_name, 'monto_fijo'=> $bMontoFijo]:null;
    }


    private function calcularPrecioByDuracion($duracion, $wn, $extras=false)
    {
        $duracion_unit = $this->duracion_unit;

        //OJO!: se ha cambiado en : $wn['n'] <= $precio->hasta_unit => $wn['n'] < $precio->hasta_unit

        $precios = $extras?$this->precio_extras:$this->precios;

        if($precios->count()<1)
        {
            return null;
        }

        $year = (int)$wn['y'];
        $semana = (int)$wn['n'];

        //Monto fijo (validez + duracion)
        foreach( $precios->where('duracion',1)->where('duracion_fijo',$duracion_unit)->sortByDesc('rango1') as $precio )
        {
            $unit_d = $precio->desde_unit;
            $unit_h = $precio->hasta_unit;
            $year_d = $precio->desde_year;
            $year_h = $precio->hasta_year;

            if($year_d<$year): $unit_d = 1;
            elseif($year_h>$year): $unit_h = 52;
            endif;

            // echo "$tipo (". $precio->id ."): ". $semana ."-". $year ." :: $unit_d ($year_d): $unit_h ($year_h) = $precio->importe: ";
            // echo "if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h ) <br>";

            // if( (($wn['n'] >= $precio->desde_unit)&&($wn['y'] >= $precio->desde_year)) && (($wn['n'] <= $precio->hasta_unit)&&($wn['y'] <= $precio->hasta_year)) )
            if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h )
            {
                // ->where('rango1','=',$duracion)

                if($precio->rango1 == $duracion)
                {
                    return $precio;
                }
            }
        }

        //Monto fijo (validez + < duracion)
        foreach( $precios->where('duracion',1)->where('duracion_fijo',$duracion_unit)->sortByDesc('rango1') as $precio )
        {
            $unit_d = $precio->desde_unit;
            $unit_h = $precio->hasta_unit;
            $year_d = $precio->desde_year;
            $year_h = $precio->hasta_year;

            if($year_d<$year): $unit_d = 1;
            elseif($year_h>$year): $unit_h = 52;
            endif;

            // echo "$tipo (". $precio->id ."): ". $semana ."-". $year ." :: $unit_d ($year_d): $unit_h ($year_h) = $precio->importe: ";
            // echo "if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h ) <br>";

            // if( (($wn['n'] >= $precio->desde_unit)&&($wn['y'] >= $precio->desde_year)) && (($wn['n'] <= $precio->hasta_unit)&&($wn['y'] <= $precio->hasta_year)) )
            if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h )
            {
                // ->where('rango1','=',$duracion)

                if($precio->rango1 < $duracion)
                {
                    return $precio;
                }
            }
        }

        //Rango
        $tipo = 2;
        foreach( $precios->where('duracion_tipo',$tipo)->where('duracion',$duracion_unit+1)->sortByDesc('rango1') as $precio )
        {
            $unit_d = $precio->desde_unit;
            $unit_h = $precio->hasta_unit;
            $year_d = $precio->desde_year;
            $year_h = $precio->hasta_year;

            if($year_d<$year): $unit_d = 1;
            elseif($year_h>$year): $unit_h = 52;
            endif;

            // echo "$tipo (". $precio->id ."): ". $semana ."-". $year ." :: $unit_d ($year_d): $unit_h ($year_h) = $precio->importe: ";
            // echo "if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h ) <br>";

            // if( (($wn['n'] >= $precio->desde_unit)&&($wn['y'] >= $precio->desde_year)) && (($wn['n'] <= $precio->hasta_unit)&&($wn['y'] <= $precio->hasta_year)) )
            if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h )
            {
                // ->where('rango1','<=',$duracion)
                // ->where('rango2','>=',$duracion)
                // ->orderBy('rango1','desc')->first();

                if( ($precio->rango1 <= $duracion) && ($precio->rango2 >= $duracion) )
                {
                    return $precio;
                }
            }
        }

        //A partir de
        $tipo = 1;
        foreach( $precios->where('duracion_tipo',$tipo)->where('duracion',$duracion_unit+1)->sortByDesc('rango1') as $precio )
        {
            $unit_d = $precio->desde_unit;
            $unit_h = $precio->hasta_unit;
            $year_d = $precio->desde_year;
            $year_h = $precio->hasta_year;

            if($year_d<$year): $unit_d = 1;
            elseif($year_h>$year): $unit_h = 52;
            endif;

            // echo "$tipo (". $precio->id ."): ". $semana ."-". $year ." :: $unit_d ($year_d): $unit_h ($year_h) = $precio->importe: ";
            // echo "if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h ) <br>";

            // if( (($wn['n'] >= $precio->desde_unit)&&($wn['y'] >= $precio->desde_year)) && (($wn['n'] <= $precio->hasta_unit)&&($wn['y'] <= $precio->hasta_year)) )
            if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h )
            {
                // ->where('rango1','<',$duracion)
                // ->orderBy('rango1','desc')->first();

                if($precio->rango1 <= $duracion)
                {
                    return $precio;
                }
            }
        }

        //Cualquiera
        $tipo = 0;
        foreach( $precios->where('duracion_tipo',$tipo)->where('duracion',$duracion_unit+1) as $precio )
        {
            $unit_d = $precio->desde_unit;
            $unit_h = $precio->hasta_unit;
            $year_d = $precio->desde_year;
            $year_h = $precio->hasta_year;

            if($year_d<$year): $unit_d = 1;
            elseif($year_h>$year): $unit_h = 52;
            endif;

            // echo "$tipo (". $precio->id ."): ". $semana ."-". $year ." :: $unit_d ($year_d): $unit_h ($year_h) = $precio->importe: ";
            // echo "if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h ) <br>";

            // if( (($wn['n'] >= $precio->desde_unit)&&($wn['y'] >= $precio->desde_year)) && (($wn['n'] <= $precio->hasta_unit)&&($wn['y'] <= $precio->hasta_year)) )
            if($semana>=$unit_d && $semana<=$unit_h && $year>=$year_d && $year<=$year_h )
            {
                // echo "$tipo: ". $wn['n'] ."-". $wn['y'] ." :: $precio->desde_unit ($precio->desde_year): $precio->hasta_unit ($precio->hasta_year) = $precio->importe <br>";
                return $precio;
            }
        }

        return null;
    }

    public function getDuracionUnitAttribute()
    {
        $precio = $this->precios()->first();

        if($precio)
        {
            if($precio->duracion == 1)
            {
                return $precio->duracion_fijo;
            }
            else
            {
                return $precio->duracion-1;
            }
        }

        return 0;
    }

    public function calcularPrecio_OLD($duracion)
    {
        $precio = $this->precios->first();

        if(!$precio)
            return null;

        if($precio->duracion==1) //monto fijo
        {
            return Precio::where('convocatory_id',$this->id)->where('rango1','=',$duracion)->first();
        }
        elseif($precio->duracion_tipo==0)
        {
            return $precio;
        }
        else
        {
            //rango
            $p = Precio::where('convocatory_id',$this->id)->where('duracion_tipo',2)
                    ->where('rango1','<=',$duracion)
                    ->where('rango2','>=',$duracion)
                    ->orderBy('rango1','desc')->first();

            if(!$p)
            {
                //A partir de
                $p = Precio::where('convocatory_id',$this->id)->where('duracion_tipo',1)->where('rango1','<=',$duracion)->orderBy('rango1','desc')->first();
            }

            return $p;
        }

        return null;
    }

    public function cuestionarios()
    {
        // return $this->hasManyThrough('\VCN\Models\System\Cuestionario', '\VCN\Models\System\CuestionarioVinculado', 'modelo_id', 'id')->where('modelo','Abierta');
        return $this->belongsToMany('\VCN\Models\System\Cuestionario', 'cuestionario_vinculados','modelo_id')->where('modelo','Abierta')->withPivot('id');
    }

    public function examenes()
    {
        return $this->belongsToMany(\VCN\Models\Exams\Examen::class, 'examen_vinculados','modelo_id')->where('modelo','Abierta')->withPivot('id', 'excluye');
    }

    public function getCategoriaAttribute()
    {
        return $this->curso->categoria;
    }

    public function getSubcategoriaAttribute()
    {
        return $this->curso->subcategoria;
    }

    public function getSubcategoriaDetalleAttribute()
    {
        return $this->curso->subcategoria_detalle;
    }

    public function getCentroAttribute()
    {
        return $this->curso->centro;
    }

    public function getParentsAttribute()
    {
        return ['subcategoria_detalle', 'subcategoria', 'categoria', 'centro', 'curso'];
    }

    public function getMontoReservaTxtAttribute()
    {
        if($this->reserva_tipo)
        {
            return $this->reserva_valor?$this->reserva_valor ."%":"";
        }

        return ConfigHelper::parseMoneda($this->reserva_valor);
    }

    public function updateContable()
    {
        foreach($this->bookings_completos as $booking)
        {
            $fechaini = Carbon::parse( $booking->course_start_date );
            if($fechaini->isFuture())
            {
                $booking->setContable(true);
            }
        }
    }

    public function getContactoSOS($proveedor, $plataforma=0)
    {
        $m = \VCN\Models\System\ContactoModel::where('modelo','Abierta')->where('modelo_id',$this->id);
        $c = clone $m;

        if($proveedor)
        {
            return $c->where('es_proveedor',1)->get();
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',$plataforma)->first();
        if($c)
        {
            return $c;
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',0)->first();
        return $c ?: 0;
    }
}
