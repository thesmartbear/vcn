<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

class CerradaPrecio extends Model
{
    protected $table = 'convocatoria_cerrada_precios';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function convocatoria()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\Cerrada', 'convocatory_id');
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda', 'moneda_id');
    }
}
