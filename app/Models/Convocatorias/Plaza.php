<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Booking;

use VCN\Helpers\ConfigHelper;
use Carbon;

class Plaza extends Model
{
    protected $table = 'convocatoria_plazas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function convocatoria()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\Cerrada', 'convocatory_id');
    }

    public function alojamiento()
    {
        return $this->belongsTo('\VCN\Models\Alojamientos\Alojamiento', 'alojamiento_id');
    }

    public function getPlazasReservasAttribute()
    {
        // $p = $this->convocatoria->bookings->where('accommodation_id',$this->alojamiento_id)->where('status_id',ConfigHelper::config('booking_status_prebooking'));
        $p = $this->convocatoria->bookings_reserva->where('accommodation_id',$this->alojamiento_id);

        $po = $this->convocatoria->bookings->where('accommodation_id',$this->alojamiento_id)->where('status_id',ConfigHelper::config('booking_status_overbooking'))->where('ovbkg_pa',0); //overbooking pero no de alojamiento

        return $p->count() + $po->count();// + $this->plazas_online;
    }

    public function getPlazasReservasPlataforma($p)
    {
        $p = $this->convocatoria->bookings_reserva->where('accommodation_id',$this->alojamiento_id)->where('plataforma',$p);

        $po = $this->convocatoria->bookings->where('accommodation_id',$this->alojamiento_id)->where('plataforma',$p)->where('status_id',ConfigHelper::config('booking_status_overbooking'))->where('ovbkg_pa',0); //overbooking pero no de alojamiento

        return $p->count() + $po->count();// + $this->plazas_online;
    }

    public function getPlazasPrereservasAttribute()
    {
        $p = $this->convocatoria->bookings->where('accommodation_id',$this->alojamiento_id)->where('status_id',ConfigHelper::config('booking_status_prereserva'));
        return $p->count();
    }

    public function getPlazasOnlineAttribute()
    {
        $hora = Carbon::now();
        $hora1 = Carbon::now()->subHour();
        $hora2 = Carbon::now()->subHours(96);

        $cid = $this->convocatoria->id;

        //tpv_plazas : 1hora
        $p1 = Booking::where('convocatory_close_id', $cid)->where('accommodation_id',$this->alojamiento_id)->where('es_online',1)->where('status_id',0)
            ->where('tpv_plazas','>=',$hora1)->where('tpv_plazas','<=',$hora);

        //tpv_plazas : 3dias (backend)
        $p2 = Booking::where('convocatory_close_id', $cid)->where('accommodation_id',$this->alojamiento_id)->where('es_online',2)->where('status_id',0)
            ->where('tpv_plazas','>=',$hora2)->where('tpv_plazas','<=',$hora);
        
        return $p1->count() + $p2->count();
    }

    public function getPlazasOverbookingAttribute()
    {
        $p = $this->convocatoria->bookings->where('accommodation_id',$this->alojamiento_id)->where('status_id',ConfigHelper::config('booking_status_overbooking'))->where('ovbkg_pa',1);
        return $p->count();
    }

    public function getPlazasOverbookingPlataforma($p)
    {
        $p = $this->convocatoria->bookings->where('accommodation_id',$this->alojamiento_id)->where('plataforma',$p)->where('status_id',ConfigHelper::config('booking_status_overbooking'))->where('ovbkg_pa',1);
        return $p->count();
    }

    public function getPlazasDisponiblesAttribute()
    {
        return ($this->plazas_totales - $this->plazas_bloqueadas - $this->plazas_reservas - $this->plazas_prereservas - $this->plazas_overbooking - $this->plazas_online );
    }

    public function getPlazasDisponiblesOvbkgAttribute()
    {
        return ($this->plazas_totales - $this->plazas_bloqueadas - $this->plazas_reservas - $this->plazas_prereservas - $this->plazas_online );
    }

    public function getPlazasVendidasAttribute()
    {
        return ($this->plazas_reservas - $this->plazas_overbooking );
    }
}
