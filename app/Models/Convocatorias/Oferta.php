<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

class Oferta extends Model
{
    protected $table = 'convocatoria_ofertas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function convocatoria()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\Abierta', 'convocatory_id');
    }
}
