<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Convocatorias\Vuelo;

class CerradaVuelo extends Model
{
    protected $table = 'convocatoria_vuelos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function convocatoria()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\Cerrada', 'convocatory_id');
    }

    public function convocatorias()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\Cerrada', 'convocatory_id');
    }

    public function vuelo()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\Vuelo', 'vuelo_id');
    }

}
