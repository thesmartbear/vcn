<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

class AbiertaIncluye extends Model
{
    protected $table = 'convocatoria_abierta_incluyes';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function convocatoria()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\AbiertaIncluye', 'convocatory_id');
    }

    public function incluye()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Incluye', 'incluye_id');
    }
}
