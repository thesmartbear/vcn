<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

class CerradaIncluye extends Model
{
    protected $table = 'convocatoria_cerrada_incluyes';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function convocatoria()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\CerradaIncluye', 'convocatory_id');
    }

    public function incluye()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Incluye', 'incluye_id');
    }
}
