<?php

namespace VCN\Models\Convocatorias;

use Illuminate\Database\Eloquent\Model;

class VueloEtapa extends Model
{
    protected $table = 'vuelo_etapas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function vuelo()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\Vuelo', 'vuelo_id');
    }

    public function vuelo_parent()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\Vuelo', 'vuelo_id');
    }
}
