<?php

namespace VCN\Models\Monedas;

use Illuminate\Database\Eloquent\Model;

use ConfigHelper;
use Session;

class Cambio extends Model
{
    protected $table = 'cambios';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function delete()
    {
        $error = "Imposible eliminar. Cambio en uso?.";
        Session::flash('mensaje-alert', $error);
        return back();

        // parent::delete();
    }

    public function scopePlataforma($query)
    {
        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return $query->where('plataforma',$filtro)->orWhere('plataforma',0);
        }

        return $query;
    }


    public function cambios()
    {
        return $this->hasMany('\VCN\Models\Monedas\CambioMoneda', 'cambio_id');
    }

    public function monedas()
    {
        // return $this->hasManyThrough('\VCN\Models\Monedas\Moneda', '\VCN\Models\Monedas\CambioMoneda');
        return $this->belongsToMany('\VCN\Models\Monedas\Moneda', 'cambio_monedas');//->withPivot('id');
    }
}
