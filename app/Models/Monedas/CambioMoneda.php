<?php

namespace VCN\Models\Monedas;

use Illuminate\Database\Eloquent\Model;

class CambioMoneda extends Model
{
    protected $table = 'cambio_monedas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function cambio()
    {
        return $this->belongsTo('\VCN\Models\Monedas\Cambio', 'cambio_id');
    }

    public function moneda()
    {
        return $this->belongsTo('\VCN\Models\Monedas\Moneda', 'moneda_id');
    }

    public function getRateAttribute()
    {
        return $this->tc_final ?: $this->tc_folleto;
    }
}
