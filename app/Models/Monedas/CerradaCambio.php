<?php

namespace VCN\Models\Monedas;

use Illuminate\Database\Eloquent\Model;

class CerradaCambio extends Model
{
    protected $table = 'convocatoria_cerrada_cambios';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function delete()
    {
        $error = "Imposible eliminar. Cambio en uso?.";
        Session::flash('mensaje-alert', $error);
        return back();

        // parent::delete();
    }

    public function cambio()
    {
        return $this->belongsTo('\VCN\Models\Monedas\Cambio', 'cambio_id');
    }

    public function cambios()
    {
        return $this->hasMany('\VCN\Models\Monedas\CerradaCambioMoneda', 'cambio_id');
    }

    public function monedas()
    {
        // return $this->hasManyThrough('\VCN\Models\Monedas\Moneda', '\VCN\Models\Monedas\CambioMoneda');
        return $this->belongsToMany('\VCN\Models\Monedas\Moneda', 'cambio_monedas');//->withPivot('id');
    }
}
