<?php

namespace VCN\Models\Monedas;

use Illuminate\Database\Eloquent\Model;

class Moneda extends Model
{
    protected $table = 'monedas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function delete()
    {
        $error = "Imposible eliminar. Moneda en uso.";
        Session::flash('mensaje-alert', $error);
        return back();

        // parent::delete();
    }

    public function getNameAttribute()
    {
        return $this->currency_name;
    }

    public function getRateAttribute()
    {
        return $this->currency_rate;
    }
}
