<?php

namespace VCN\Models\Monedas;

use Illuminate\Database\Eloquent\Model;

class CerradaCambioMoneda extends Model
{
    protected $table = 'convocatoria_cerrada_cambio_monedas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function cambio()
    {
        return $this->belongsTo('\VCN\Models\Monedas\CerradaCambio', 'convocatory_id');
    }

    public function moneda()
    {
        return $this->belongsTo('\VCN\Models\Monedas\Moneda', 'moneda_id');
    }

    public function getRateAttribute()
    {
        return (float) ($this->tc_final>0 ? $this->tc_final : $this->tc_folleto);
    }
}
