<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Class     ModelAuditable
 *
 * @package  VCN
 * @author   carlituxman <carlituxman@informatik.es>
 */

abstract class ModelAuditable extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $auditExclude = ['created_at', 'updated_at'];

    /*public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }*/
}