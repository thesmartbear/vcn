<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\System\Documento;
use VCN\Models\Monedas\Moneda;
use VCN\Models\Monedas\Cambio;

use ConfigHelper;
use Session;
use Carbon;

use \VCN\Models\System\BaseModel;

class Categoria extends BaseModel
{
    protected $table = 'categorias';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'avisos' => 'array',
        'avisos_online' => 'array',
        'avisos_cancel' => 'array',
        'avisos_datos' => 'array',
        'avisos_doc' => 'array',
        'avisos_catalogo' => 'array',
        'avisos_cursosweb' => 'array',
        'promo_cambio_fijo_fechas' => 'array',
        'pdf_condiciones'    => 'json',
        'condiciones'    => 'json',
        'pdf_cancelacion'    => 'json',
    ];

    public function subcategorias()
    {
        return $this->hasMany('\VCN\Models\Subcategoria', 'category_id');
    }

    public function cursos()
    {
        return $this->hasMany('\VCN\Models\Cursos\Curso', 'category_id');
    }

    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('name')->get();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return self::where('propietario', 0)->orWhere('propietario',$filtro)->orderBy('name')->get();
        }

        return self::orderBy('name')->get();
    }

    public static function scopePlataformas($query)
    {
        if( auth()->user()->isFullAdmin() )
        {
            // return self::orderBy('name')->get();
            return $query->orderBy('name');
        }

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return $query->where('propietario', 0)->orWhere('propietario',$filtro)->orderBy('name');
        }

        return $query->orderBy('name');
    }

    public function getDocumentos($idioma)
    {
        $docs =  \VCN\Models\System\Documento::where('modelo','Categoria')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();
        
        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::where('modelo','Categoria')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();
    }
    
    public function getDocumentosEspecificosAttribute()
    {
        return \VCN\Models\System\DocEspecifico::where('modelo','Categoria')->where('modelo_id',$this->id)->get();
    }

    public function getDocumentosArea($idioma, $plataforma=null)
    {
        $filtro = [0, $plataforma ?: ConfigHelper::config('propietario') ];

        $docs = \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','Categoria')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','Categoria')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
    }

    public function cuestionarios()
    {
        return $this->belongsToMany('\VCN\Models\System\Cuestionario', 'cuestionario_vinculados','modelo_id')->where('modelo','Categoria')->withPivot('id');
    }

    public function examenes()
    {
        return $this->belongsToMany(\VCN\Models\Exams\Examen::class, 'examen_vinculados','modelo_id')->where('modelo','Categoria')->withPivot('id', 'excluye');
    }

    public function getParentsAttribute()
    {
        return [];
    }

    public function getTablaCambioAttribute()
    {
        $cambios = Cambio::where('activo',1)->orderBy('any','DESC')->get();

        foreach($cambios as $cambio)
        {
            $cats = explode(',',$cambio->categorias);
            if(in_array($this->id, $cats))
            {
                return $cambio;//->cambios;
            }
        }

        return null;
    }

    public function getTablaCambioAny($any, $moneda=null)
    {
        $mDefault = ConfigHelper::default_moneda();
        if($moneda != $mDefault)
        {
            return null;
        }

        $cambios = Cambio::where('activo',1)->where('any',$any)->orderBy('updated_at')->get();

        foreach($cambios as $cambio)
        {
            $cats = explode(',',$cambio->categorias);
            if(in_array($this->id, $cats))
            {
                return $cambio;//->cambios;
            }
        }

        return null;
    }

    public function getMontoReservaTxtAttribute()
    {
        if($this->reserva_tipo)
        {
            return $this->reserva_valor?$this->reserva_valor ."%":"";
        }

        return ConfigHelper::parseMoneda($this->reserva_valor);

    }

    public function getContactoSOS($proveedor, $plataforma=0)
    {
        $c = \VCN\Models\System\ContactoModel::where('modelo','Categoria')->where('modelo_id',$this->id);

        if($proveedor)
        {
            return $c->where('es_proveedor',1)->get();
        }

        $c = \VCN\Models\System\ContactoModel::where('modelo', 'Categoria')->where('modelo_id',$this->id) ;
        $c = $c->where('es_proveedor',0)->where('plataforma',$plataforma)->first();
        return $c ?: 0;
    }

    public function scriptUpdate()
    {
        $bookings = \VCN\Models\Bookings\Booking::where('course_end_date','>=',Carbon::now())->where('category_id',$this->id);
        
        $i = 0;
        foreach($bookings->get() as $booking)
        {
            $pcfd = $booking->promo_cambio_fijo_default;
            $pcf = $booking->promo_cambio_fijo;

            if($pcf != $pcfd)
            {   
                $booking->promo_cambio_fijo = $pcfd;
                $booking->save();
                
                $i++;
            }
        }

        if($i)
        {
            Session::flash('mensaje', "Se han actualizado $i Bookings");
        }
    }
}
