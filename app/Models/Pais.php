<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = 'paises';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function provincias()
    {
        return $this->hasMany('\VCN\Models\Provincia', 'pais_id');
    }

    public function ciudades()
    {
        return $this->hasMany('\VCN\Models\Ciudad', 'contry_id');
    }

    public function centros()
    {
        return $this->hasMany('\VCN\Models\Centros\Centro', 'country_id');
    }
}
