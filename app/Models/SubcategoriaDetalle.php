<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Cursos\Curso;

use ConfigHelper;

class SubcategoriaDetalle extends Model
{
    protected $table = 'subcategoria_detalles';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function delete()
    {
        if(Curso::where('subcategory_det_id',$this->id)->count()>0)
        {
            $error = "Imposible eliminar. Existen Cursos con esta SubCategoría Detalle.";
            Session::flash('mensaje-alert', $error);
            return back();
        }

        parent::delete();
    }

    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('name')->get();

        // $filtro = ConfigHelper::config('propietario');
        // if($filtro)
        // {
        //     return self::where('propietario', 0)->orWhere('propietario',$filtro)->orderBy('name')->get();
        // }

        return self::orderBy('name')->get();
    }

    public function subcategoria()
    {
        return $this->belongsTo('\VCN\Models\Subcategoria', 'subcategory_id');
    }

    public function examenes()
    {
        return $this->belongsToMany(\VCN\Models\Exams\Examen::class, 'examen_vinculados','modelo_id')->where('modelo','SubcategoriaDetalle')->withPivot('id', 'excluye');
    }

    public function getParentsAttribute()
    {
        return ['subcategoria','categoria'];
    }
}
