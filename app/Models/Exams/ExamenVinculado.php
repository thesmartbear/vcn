<?php

namespace VCN\Models\Exams;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Centros\Centro;
use VCN\Models\Cursos\Curso;
use VCN\Models\Convocatorias\{Cerrada, Abierta, ConvocatoriaMulti};
use VCN\Models\{Categoria, Subcategoria};

class ExamenVinculado extends Model
{
    protected $table = "examen_vinculados";

    protected $guarded = ['_token'];
    
    public function examen()
    {
        return $this->belongsTo(\VCN\Models\Exams\Examen::class, 'examen_id');
    }

    public function getRelacionUrlAttribute()
    {
        switch($this->modelo)
        {
            case "Centro":
            {
                return route('manage.centros.ficha', $this->modelo_id);
            }
            break;

            case "Curso":
            {
                return route('manage.cursos.ficha', $this->modelo_id);
            }
            break;

            case "Categoria":
            {
                return route('manage.categorias.ficha', $this->modelo_id);
            }
            break;

            case "Subcategoria":
            {
                return route('manage.subcategorias.ficha', $this->modelo_id);
            }
            break;

            case "Cerrada":
            {
                return route('manage.convocatorias.cerradas.ficha', $this->modelo_id);
            }
            break;

            case "Abierta":
            {
                return route('manage.convocatorias.abiertas.ficha', $this->modelo_id);
            }
            break;

            case "ConvocatoriaMulti":
            {
                return route('manage.convocatorias.multis.ficha', $this->modelo_id);
            }
            break;
        }

        return null;
    }

    public function getRelacionNameAttribute()
    {
        switch($this->modelo)
        {
            case "Centro":
            {
                return Centro::find($this->modelo_id)->name;
            }
            break;

            case "Curso":
            {
                return Curso::find($this->modelo_id)->name;
            }
            break;

            case "Categoria":
            {
                return Categoria::find($this->modelo_id)->name;
            }
            break;

            case "Subcategoria":
            {
                return Subcategoria::find($this->modelo_id)->name;
            }
            break;

            case "Cerrada":
            {
                return Cerrada::find($this->modelo_id)->name;
            }
            break;

            case "Abierta":
            {
                return Abierta::find($this->modelo_id)->name;
            }
            break;

            case "ConvocatoriaMulti":
            {
                return ConvocatoriaMulti::find($this->modelo_id)->name;
            }
            break;
        }

        return null;
    }
}
