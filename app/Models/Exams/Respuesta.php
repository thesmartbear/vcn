<?php

namespace VCN\Models\Exams;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    protected $table = "examen_respuestas";

    protected $guarded = ['_token'];
    
    protected $casts = [
        'respuestas' => 'array',
    ];

    public function examen()
    {
        return $this->belongsTo(\VCN\Models\Exams\Examen::class, 'examen_id');
    }

    public function viajero()
    {
        return $this->belongsTo(\VCN\Models\Leads\Viajero::class, 'viajero_id');
    }

    public function booking()
    {
        return $this->belongsTo(\VCN\Models\Bookings\Booking::class, 'booking_id');
    }

    public function logs()
    {
        return $this->hasMany(\VCN\Models\Exams\RespuestaLog::class, 'respuesta_id');
    }

    public function getTipoAttribute($value)
    {
        return $value ? 'viajero' : 'booking';
    }

    public function evaluar()
    {
        //aciertos, resultados (ielts)

        $exam = $this->examen;
        $respuestas = $this->respuestas;

        $iOk = 0;
        foreach($exam->preguntas as $p)
        {
            $r = $respuestas[$p->id] ?? 'x';
            if($r == $p->respuesta)
            {
                $iOk++;
            }
        }

        $this->aciertos = $iOk;
        $this->save();
        
        $this->resultado = $this->ielts;
        $this->notas = $this->ielts ." - ". $this->ielts_txt;
        $this->status = 1;
        $this->save();

        RespuestaLog::addLog($this, 'Evaluación');
    }

    public function getIELTSAttribute()
    {
        // [0,0]   => 0,
        // [1,5]   => 0.5,
        // [6,10]  => 1,
        // [11,13] => 1.5,
        // [14,17] => 2,
        // [18,21] => 2.5,
        // [22,24] => 3,
        // [25,28] => 3.5,
        // [29,32] => 4,
        // [33,36] => 4.5,
        // [37,40] => 5,
        // [41,44] => 5.5,
        // [45,50] => 6,
        
        $score = [
            0   => 0,
            1   => 0.5,
            2   => 0.5,
            3   => 0.5,
            4   => 0.5,
            5   => 0.5,
            6   => 1,
            7   => 1,
            8   => 1,
            9   => 1,
            10  => 1,
            11  => 1.5,
            12  => 1.5,
            13  => 1.5,
            14  => 2,
            15  => 2,
            16  => 2,
            17  => 2,
            18  => 2.5,
            19  => 2.5,
            20  => 2.5,
            21  => 2.5,
            22  => 3,
            23  => 3,
            24  => 3,
            25  => 3.5,
            26  => 3.5,
            27  => 3.5,
            28  => 3.5,
            29  => 4,
            30  => 4,
            31  => 4,
            32  => 4,
            33  => 4.5,
            34  => 4.5,
            35  => 4.5,
            36  => 4.5,
            37  => 5,
            38  => 5,
            39  => 5,
            40  => 5,
            41  => 5.5,
            42  => 5.5,
            43  => 5.5,
            44  => 5.5,
            45  => 6,
            46  => 6,
            47  => 6,
            48  => 6,
            49  => 6,
            50  => 6,
        ];
        
        return $score[$this->aciertos ?: 0];
    }

    public function getIELTSTxtAttribute()
    {
        $ielts = [
            0   => 'Absolute Beginner', //Could not attempt
            0.5 => 'Absolute Beginner', //Tried test
            1   => 'Beginner',
            1.5 => 'Upper Beginner',
            2   => 'Lower Elementary',
            2.5 => 'Elementary',
            3   => 'Upper Elementary',
            3.5 => 'Pre Intermediate',
            4   => 'Lower intermediate',
            4.5 => 'Intermediate',
            5   => 'Upper Intermediate',
            5.5 => 'Advanced',
            6   => 'Advanced'
        ];

        return $ielts[$this->ielts ?: 0];
    }

    public function getEstadoAttribute()
    {
        $st = [-1 => 'Avisado', 0 => 'Pendiente', 1 => 'Entregado'];

        return $st[$this->status];
    }
}
