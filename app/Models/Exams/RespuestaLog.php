<?php

namespace VCN\Models\Exams;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Exams\Respuesta;
use Auth;

class RespuestaLog extends Model
{
    protected $table = "examen_respuesta_logs";

    protected $guarded = ['_token'];
    
    public function respuesta()
    {
        return $this->belongsTo(\VCN\Models\Exams\Respuesta::class, 'respuesta_id');
    }

    public function user()
    {
        return $this->belongsTo(\VCN\Models\User::class,'user_id');
    }

    public static function addLog( Respuesta $respuesta, $tipo, $notas="")
    {
        $log = new RespuestaLog;
        $log->respuesta_id = $respuesta->id;
        $log->tipo = $tipo;
        $log->user_id = Auth::user() ? Auth::user()->id : 0;

        $log->notas = $notas;

        $log->save();

    }
}
