<?php

namespace VCN\Models\Exams;

use Illuminate\Database\Eloquent\Model;

use ConfigHelper;
use Illuminate\Http\Request;

use \VCN\Models\Bookings\Booking;

class Examen extends Model
{
    protected $table = "examenes";

    protected $guarded = ['_token'];

    public function preguntas()
    {
        return $this->hasMany(\VCN\Models\Exams\Pregunta::class, 'examen_id');
    }

    public function respuestas()
    {
        return $this->hasMany(\VCN\Models\Exams\Respuesta::class, 'examen_id');
    }

    public function getPreguntasTotalAttribute()
    {
        return $this->preguntas->where('numero', '>', 0)->count();
    }

    public function vinculados()
    {
        return $this->hasMany(\VCN\Models\Exams\ExamenVinculado::class, 'examen_id');
    }

    public function scopeActivo($query)
    {
        return $query->where('activo', 1);
    }

    public function scopePlataforma($query)
    {
        $filtro = ConfigHelper::config('propietario');
        if ($filtro) {
            return $query->where('propietario', $filtro)->orWhere('propietario', 0);
        }

        return $query;
    }

    public static function getRespuesta(Examen $exam, $id, $tipo = "booking", $status=1)
    {
        // $tipo: viajero/booking
        // $id: de viajero/booking

        if ($tipo == "booking") {
            $r = Respuesta::where('examen_id', $exam->id)->where('booking_id', $id)->where('status',$status)->first();
        } elseif ($tipo == "viajero") {
            $r = Respuesta::where('examen_id', $exam->id)->where('viajero_id', $id)->where('booking_id', 0)->where('status',$status)->first();
        } else {
            return null;
        }

        return $r;
    }

    public static function postRespuesta(Examen $exam, Request $request)
    {
        //$exam->id == 1

        $preguntas = $exam->preguntas->where('numero', '>', 0);
        foreach ($preguntas as $pregunta) {
            if ($pregunta->numero == 0) {
                continue;
            }

            $rId = $pregunta->id;
            $r = $rId;
            if ($pregunta->bloque == 3 && $pregunta->numero < 12) {
                // part3-X
                $r = "part3-" . $pregunta->pregunta;
            }

            $respuestas[$rId] = $request->get($r, null);
        }

        $tipo = $request->get('tipo');
        $bid = $request->get('booking_id', 0);
        $b = Booking::find($bid);
        $vid = $b ? $b->viajero_id : 0;
        $vid = $request->get('viajero_id', $vid);

        $data['examen_id'] = $exam->id;
        $data['viajero_id'] = $vid;
        $data['booking_id'] = ($tipo == "viajero") ? 0 : $bid;
        $data['tipo'] = ($tipo == "viajero");
        // $data['status'] = 1;

        $r = Respuesta::firstOrNew($data);
        $r->respuestas = $respuestas;
        $r->save();

        $r->evaluar();

        return $r;
    }

    public function getRespuestaBooking($bid)
    {
        if(!$bid)
        {
            return null;
        }
        
        return Respuesta::where('examen_id', $this->id)->where('booking_id', $bid)->first();
    }

    public function getRespuestaViajero($vid)
    {
        return Respuesta::where('examen_id', $this->id)->where('viajero_id', $vid)->where('booking_id', 0)->first();
    }
}
