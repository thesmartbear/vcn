<?php

namespace VCN\Models\Exams;

use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    protected $table = "examen_preguntas";

    protected $guarded = ['_token'];
    
    protected $casts = [
        'opciones' => 'array',
    ];

    public function examen()
    {
        return $this->belongsTo(\VCN\Models\Exams\Examen::class, 'examen_id');
    }
}
