<?php

namespace VCN\Models\Chat;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\User;
class ChatSoporte extends \Musonza\Chat\Models\Conversation
{
    public function getStatusAttribute()
    {
        $data = $this->data;
        return isset($data['status']) ? $data['status'] : 0;
    }

    public function getStatusNameAttribute()
    {
        return $this->status_text;
    }


    public function getStatusTextAttribute()
    {
        $data = $this->data;
        $status = isset($data['status']) ? $data['status'] : 0;

        switch($status)
        {
            case 1:
            {
                return "Atendido por: ". $this->agente->full_name;
            }
            break;

            case 2:
            {
                return "Esperando a ser atendido";
            }
            break;

            case 20:
            {
                return "No hay agentes disponibles";
            }
            break;

            case 21:
            {
                return "Tiempo de espera agotado";
            }
            break;

            case 22:
            {
                return "Tiempo excedido. No hay agentes";
            }
            break;

            case 11:
            {
                return "Agente ha cerrado la conversación";
            }
            break;

            case 12:
            {
                return "Cliente ha cerrado la conversación";
            }
            break;

            case 13:
            {
                return "Sesión caducada";
            }
            break;

            case 30:
            {
                return "Terminado";
            }
            break;

            default:
            {
                return "¿?";
            }
            break;
        }
    }

    public function getAgenteAttribute()
    {
        $data = $this->data;
        return isset($data['agente_id']) ? User::find($data['agente_id']) : null;
    }

    public function getClienteAttribute()
    {
        $data = $this->data;
        return isset($data['cliente_id']) ? User::find($data['cliente_id']) : null;
    }

    public function getMensajesAttribute()
    {
        $user = auth()->user();

        $messages = $this->messages;

        foreach($messages as $m)
        {
            $m->enviado = $m->user_id == $user->id;
            $m->hora = $m->created_at->format('H:i');
            $m->created = $m->created_at->toIso8601String();
        }

        return $messages;
    }

    public function getMensajesNuevosAttribute()
    {
        $user = auth()->user();

        $messages = $this->messages;

        //created_at vs chat_last_activity

        foreach($messages as $m)
        {
            $m->enviado = $m->user_id == $user->id;
            $m->hora = $m->created_at->format('H:i');
            $m->created = $m->created_at->toIso8601String();
        }

        return $messages;
    }

    public function updateStatus($st=null)
    {
        $chatData = $this->data;

        if($this->cliente->chat_sesion_caducada)
        {
            $st = 13;
            $chatData['status'] = $st;
            $this->update(['data' => $chatData]);

            $cli = $this->cliente;
            $cli->chat_id = 0;
            if($cli->roleid == 0)
            {
                $cli->chat_last_activity = null;
            }
            $cli->save();
            return;
        }

        if($st)
        {
            $chatData['status'] = $st;
            $this->update(['data' => $chatData]);
            
            if($st>10)
            {
                $cli = $this->cliente;
                $cli->chat_id = 0;
                if($cli->roleid == 0)
                {
                    $cli->chat_last_activity = null;
                }
                $cli->save();
                return;
            }
        }

        $status = $st ?: $this->status;
        
        if ( User::getChatCrmDisponibles()->count() < 1)
        {
            $status = 22;
        }

        $chatData['status'] = $status;
        $this->update(['data' => $chatData]);
    }

    public function finalizar()
    {
        // $this->updateStatus(30)

    }

    public static function notificarNuevo($chat)
    {
        $disponibles = User::getChatCrmDisponibles();
        foreach($disponibles as $u)
        {
            $t = "Nuevo Chat";
            $m = "Petición de Chat nueva";
            $a = route('manage.chat.agente', $chat->id);
            $u->notify(new \VCN\Notifications\ChatNotification($t,$m,$a));
        }
    }
}
