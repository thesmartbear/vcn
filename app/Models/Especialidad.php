<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{
    protected $table = 'especialidades';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function subespecialidades()
    {
        return $this->hasMany('\VCN\Models\Especialidad', 'especialidad_id');
    }
}
