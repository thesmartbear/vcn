<?php

namespace VCN\Models\Prescriptores;

use Illuminate\Database\Eloquent\Model;

use ConfigHelper;

class CategoriaComision extends Model
{
    protected $table = 'prescriptor_comisiones_cat';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('name')->get();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return self::where('propietario', 0)->orWhere('propietario',$filtro)->orderBy('name')->get();
        }

        return self::orderBy('name')->get();
    }
}
