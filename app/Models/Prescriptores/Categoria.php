<?php

namespace VCN\Models\Prescriptores;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Cursos\Curso;

use ConfigHelper;

class Categoria extends Model
{
    protected $table = 'prescriptor_categorias';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function delete()
    {
        if(Curso::where('category_id',$this->id)->count()>0)
        {
            $error = "Imposible eliminar. Existen Cursos con esta Categoría.";
            Session::flash('mensaje-alert', $error);
            return back();
        }

        parent::delete();
    }

    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('name')->get();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return self::where('propietario', 0)->orWhere('propietario',$filtro)->orderBy('name')->get();
        }

        return self::orderBy('name')->get();
    }

    public function prescriptores()
    {
        return $this->hasMany('\VCN\Models\Prescriptores\Prescriptor', 'categoria_id');
    }
}
