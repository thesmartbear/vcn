<?php

namespace VCN\Models\Prescriptores;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\System\Oficina;

use ConfigHelper;

class Prescriptor extends Model
{
    protected $table = 'prescriptores';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('name')->get();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            $oficinas = Oficina::where('propietario',$filtro)->orWhere('propietario',0)->pluck('id')->toArray() + [0];
            return self::whereIn('oficina_id', $oficinas)->orWhere('oficina_id', 0)->orderBy('name')->get();
        }

        return self::orderBy('name')->get();
    }

    public function categoria()
    {
        return $this->belongsTo('\VCN\Models\Prescriptores\Categoria', 'categoria_id');
    }

    public function categoria_comision()
    {
        return $this->belongsTo('\VCN\Models\Prescriptores\CategoriaComision', 'comision_cat_id');
    }

    public function oficina()
    {
        return $this->belongsTo('\VCN\Models\System\Oficina', 'oficina_id');
    }
}
