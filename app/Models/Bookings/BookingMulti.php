<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

class BookingMulti extends Model
{
    protected $table = 'booking_multis';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking', 'booking_id');
    }

    public function semana()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\ConvocatoriaMultiSemana', 'semana_id');
    }

    public function especialidad()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\ConvocatoriaMultiEspecialidad', 'especialidad_id');
    }

    public function getEspecialidadNameAttribute()
    {
        if($this->especialidad)
        {
            if($this->especialidad->especialidad)
            {
                return $this->especialidad->especialidad_name;
            }
        }

        return $this->especialidad_id;
    }
}
