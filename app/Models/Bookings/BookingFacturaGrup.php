<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

class BookingFacturaGrup extends Model
{
    protected $table = 'booking_facturas_grup';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $dates = ['fecha'];

    protected $casts = [
        'bookings' => 'array',
    ];

    public function facturas()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingFactura', 'grup_id');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }
}
