<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\App;
use VCN\Models\User;
use VCN\Models\Monedas\Moneda;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Bookings\BookingFamilia;
use VCN\Models\Bookings\BookingDescuento;
use VCN\Models\Bookings\BookingMulti;
use VCN\Models\Bookings\BookingMoneda;
use VCN\Models\Bookings\BookingDato;
use VCN\Models\Convocatorias\ConvocatoriaMultiSemana;
use VCN\Models\Convocatorias\Abierta as ConvocatoriaAbierta;
use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Categoria;
use VCN\Models\Subcategoria;
use VCN\Models\Extras\Extra;
use VCN\Models\Alojamientos\Alojamiento;
use VCN\Models\Exams\{Respuesta, RespuestaLog, Examen};

use VCN\Models\Cursos\Curso;
use VCN\Models\Reuniones\Reunion;
use VCN\Models\Convocatorias\Abierta;
use VCN\Models\Convocatorias\Cerrada;
use VCN\Models\Convocatorias\ConvocatoriaMulti;
use VCN\Models\Prescriptores\Prescriptor;
use VCN\Models\Prescriptores\Categoria as CategoriaPrescriptor;

use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\ViajeroDatos;
use VCN\Models\Leads\ViajeroTarea;
use VCN\Models\Leads\ViajeroLog;
use VCN\Models\Descuentos\DescuentoEarly;
use VCN\Models\Centros\Centro;
use VCN\Models\Centros\Familia;
use VCN\Models\Centros\School;
use VCN\Models\System\Aviso;
use VCN\Models\System\AvisoLog;
use VCN\Models\System\AvisoDoc;
use VCN\Models\Leads\ViajeroArchivo;
use VCN\Models\System\CuestionarioRespuesta;

use VCN\Models\Leads\Origen;
use VCN\Models\Leads\Suborigen;
use VCN\Models\Leads\SuborigenDetalle;
use VCN\Models\Descuentos\DescuentoTipo;
use VCN\Models\System\Oficina;

use VCN\Helpers\MailHelper;
use VCN\Helpers\ConfigHelper;
use Session;
use Carbon;
use PDF;
use File;
use Log;
use Request;
use PdfMerger;

class Booking extends \VCN\Models\ModelAuditable
{
    protected $table = 'bookings';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $dates = ['fecha_reserva', 'fecha_prereserva', 'pagado_fecha', 'fecha_pago1', 'tpv_plazas'];

    protected $casts = [
        // 'facturas' => 'array',
        'alertas' => 'json',
        'online' => 'json',
        'tpv_result' => 'array',
        'importes' => 'json',
        'total' => 'float',
        'datos_campamento' => 'json',
        'es_pdf' => 'boolean',
        'firmas' => 'json',
        'sos_proveedor' => 'array'
    ];

    protected $with = ['viajero'];

    public function delete()
    {
        if ($this->tiene_facturas) {
            Session::flash('mensaje-alert', 'Booking total o parcialmente facturado. Imposible eliminar.');
            return;
        }

        if (auth()->user() && auth()->user()->ficha) {
            if ($this->es_online && $this->pagos->count()) {
                Session::flash('mensaje-alert', trans('area.borrar_error'));
                return;
            }
        }

        $this->decidiendo();

        \VCN\Models\Informes\Venta::remove($this);

        $this->tareaAutoEliminar();

        //Archivos??
        foreach ($this->archivos as $ba) {
            $ba->booking_id = 0;
            $ba->save();
        }

        //Borrar pdfs
        $dir = storage_path("files/bookings/" . $this->viajero->id . "/");
        $files = "ooking_$this->id-";
        $filesp = "_" . $this->id . ".pdf";

        if (File::exists($dir)) {
            foreach (File::files($dir) as $f) {
                if (strpos($f, $files)) {
                    File::delete($f);
                }

                if (strpos($f, $filesp)) {
                    File::delete($f);
                }
            }
        }

        if (!$this->es_presupuesto) {
            ViajeroLog::addLog($this->viajero, "Booking Eliminado [$this->id]");
        }

        parent::delete();
    }

    public function getViajeroFullNameAttribute()
    {
        $ret = "";
        if ($this->viajero) {
            $ret = $this->viajero->full_name;
        }

        return $ret;
    }

    /**
     *
     */
    public function getTipoAttribute()
    {
        if ($this->es_online == 2) {
            return "BACKEND+ONLINE";
        }

        return $this->es_online ? "ONLINE" : "BACKEND";
    }

    public function getDocumentosEspecificosAttribute()
    {
        $idioma = $this->idioma_contacto;

        $ret = collect();

        $docs = $this->curso->documentos_especificos;
        foreach ($docs as $doc) {
            if ($doc->langs->count()) {
                $d = $doc->langs->where('idioma', $idioma)->first();
            } else {
                $d = clone $doc;
            }

            $ret = $ret->push($d);
        }

        $docs = $this->categoria->documentos_especificos;
        foreach ($docs as $doc) {
            if ($doc->langs->count()) {
                $d = $doc->langs->where('idioma', $idioma)->first();
            } else {
                $d = clone $doc;
            }

            $ret = $ret->push($d);
        }

        $docs = $this->subcategoria ? $this->subcategoria->documentos_especificos : [];
        foreach ($docs as $doc) {
            if ($doc->langs->count()) {
                $d = $doc->langs->where('idioma', $idioma)->first();
            } else {
                $d = clone $doc;
            }

            $ret = $ret->push($d);
        }

        return $ret;
    }


    public function getIdiomaContactoAttribute()
    {
        return $this->viajero->idioma_contacto;
    }

    public function getPdfLinkAttribute()
    {
        $dir = storage_path("files/bookings/" . $this->viajero->id . "/");
        $fname = "ooking_" . $this->id . "-";

        if (!File::exists($dir)) {
            return null;
        }

        $pdf = $this->pdf_booking_last;
        if ($pdf) {
            return route('file.booking', [$this->viajero_id, $pdf->doc]);
        }

        $files = collect(File::allFiles($dir))
            ->filter(function ($file) use ($fname) {
                return strpos($file, $fname);
            })
            ->sortBy(function ($file) {
                return $file->getCTime();
            })
            ->map(function ($file) {
                return $file->getBaseName();
            });

        if ($files->count()) {
            $pdf = $files->first();

            if ($pdf) {
                return route('file.booking', [$this->viajero_id, $pdf]);
            }

            return "";
        }

        return "";
    }

    public function getPdfMonitorEnlaceAttribute()
    {
        $href = route('area.booking.pdfmonitor', $this->id);
        return "<a data-label='Descargar PDF' href='$href' target='_blank' class='btn btn-danger btn-xs'><i class='fa fa-file-pdf-o'></i></a>";
    }

    public function getPdfEnlaceAttribute()
    {
        $ret = "";

        $pdff = $this->pdfs_firmas;
        if ($pdff && $pdff->count()) {
            $pdf = $pdff->sortByDesc('updated_at')->first();

            $href = route('file.booking', [$this->viajero_id, $pdf->doc]);
            return "<a data-label='Descargar PDF' href='$href' target='_blank' class='btn btn-danger btn-xs'><i class='fa fa-file-pdf-o'></i></a>";
        }


        $pdfl = $this->pdf_link;
        if ($pdfl) {
            $ret = "<a data-label='Descargar PDF' href='$pdfl' target='_blank' class='btn btn-danger btn-xs'><i class='fa fa-file-pdf-o'></i></a>";
        }

        return $ret;
    }


    public function scopePlataforma($query)
    {
        $filtro = ConfigHelper::config('propietario');
        if ($filtro) {
            return $query->where('plataforma', $filtro); //->orWhere('plataforma',0);
        }

        return $query;
    }

    public function getEsPasadoAttribute()
    {
        return (Carbon::parse($this->course_start_date)->lt(Carbon::now()));
    }

    public function getEsFinalizadoAttribute()
    {
        return (Carbon::parse($this->course_end_date)->lt(Carbon::now()));
    }


    public function cancelar()
    {
        // if($this->tiene_facturas)
        // {
        //     Session::flash('mensaje-alert', 'Booking total o parcialmente facturado. Imposible cancelar.');
        //     return;
        // }

        $nPagos = $this->pagos->count();

        BookingLog::addLog($this, 'Booking cancelado.');

        $st = ConfigHelper::config('booking_status_refund');
        $this->setStatus($st);
        Session::flash('mensaje', 'Booking con pagos pendientes. Status Pendiente Refund.');

        if ($this->solicitud) {
            $this->solicitud->archivar("Booking $this->id", "Cancelado");
        }

        // $viajero = $this->viajero;
        // $viajero->booking_id = 0;
        // $viajero->booking_status_id = 0;
        // $viajero->es_cliente = false;
        // $viajero->save();
    }

    public function status()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Status', 'status_id');
    }

    public function plataforma_config()
    {
        return $this->belongsTo('\VCN\Models\System\Plataforma', 'plataforma');
    }

    public function plataforma_model()
    {
        return $this->belongsTo('\VCN\Models\System\Plataforma', 'plataforma');
    }

    public function getPlataformaWebAttribute()
    {
        $p = $this->plataforma ?: 1;
        return ConfigHelper::config('area_url', $p) ?: ConfigHelper::config('web', $p);
    }

    public function checklists()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingChecklist');
    }

    public function setPagoTPV($importe = null, $conPago = true)
    {
        $ficha = $this;

        //importe: 1:transferencia, 2:tpv

        $notas = $importe ? 'Reserva inicial TPV' : 'Reserva inicial TRANSFERENCIA';
        $tipo = $importe ? 2 : 1;

        // BookingLog::addLog($ficha, $notas);

        $ip = isset($ficha->online['ip']) ? $ficha->online['ip'] : "-";
        $user = auth()->user();
        $log = "[IP: $ip] [USER: " . $user->full_name . "]";

        if ($conPago) {
            $p = new BookingPago;
            $p->booking_id = $ficha->id;
            $p->user_id = 0;
            $p->fecha = Carbon::now()->format('Y-m-d');
            $p->importe_pago = $importe;
            $p->moneda_id = ConfigHelper::default_moneda()->id;
            $p->notas = $notas;
            $p->rate = 0;
            $p->tipo = $tipo;
            $p->importe = $importe;
            $p->save();

            $ficha->fecha_pago1 = $p->fecha;
            $ficha->promo_cambio_fijo = $ficha->promo_cambio_fijo_default;
            $ficha->save();

            $notas = $importe ? "Pre-Brooking + Pago [ONLINE] TPV" : "Pre-Reserva + Pago [ONLINE] TRANSFERENCIA";
        } else {
            //corporativo + sin pago
            $notas = "Pre-Reserva + SIN Pago (Corporativo)";
        }

        $st = ConfigHelper::config('booking_status_prebooking');
        if (!$importe) {
            $st = ConfigHelper::config('booking_status_prereserva');
        }

        //plazas
        if ($ficha->sera_overbooking) {
            $st = ConfigHelper::config('booking_status_overbooking');
        }

        $ficha->setStatus($st);
        if ($ficha->viajero->solicitud) {
            $ficha->viajero->solicitud->setInscrito("booking-pago");
        }

        BookingLog::addLog($ficha, $notas, $log);
    }

    public function setChecklist($checkid, $fecha, $user_id, $seguimiento = false)
    {
        $booking = $this;
        $id = $this->id;

        $s = BookingChecklist::where('booking_id', $id)->where('checklist_id', $checkid)->first();
        if (!$s) {
            $s = new BookingChecklist;
            $s->booking_id = $id;
            $s->checklist_id = $checkid;
        }

        //Si viene de seguimiento no ponemos a off
        if ($seguimiento && $s->estado) {
            return $s;
        }

        $s->user_id = $user_id;
        $s->estado = !$s->estado;
        $s->fecha = Carbon::createFromFormat('d/m/Y', $fecha)->format('Y-m-d');
        $s->save();

        BookingLog::addLogChecklist($booking, $checkid, $s->estado, $fecha);

        $checklist = $booking->status->getChecklistBooking($id);
        $marcados = $checklist->count();
        foreach ($checklist as $check) {
            if ($booking->viajero->getStatusChecklistEstado($check->id)) {
                $marcados--;
            }
        }

        $status_id = 0;
        if ($marcados == 0) {
            $status_id = $booking->status->next->id;
            if (ConfigHelper::config('booking_status_vuelta') != $status_id) {
                // $booking->viajero->setStatus($status_id);
            }
        }

        return $s;
    }

    public function cuestionarios()
    {
        return $this->hasMany('\VCN\Models\System\CuestionarioRespuesta', 'booking_id');
    }

    public function vuelo()
    {
        return $this->belongsTo('\VCN\Models\Convocatorias\Vuelo', 'vuelo_id');
    }

    public function getVueloNameAttribute()
    {
        return $this->vuelo ? $this->vuelo->name : "-";
    }

    public function familias()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingFamilia', 'booking_id');
        // return $this->belongsToMany('\VCN\Models\Centros\Familia','booking_familias');
    }

    public function familias_area()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingFamilia', 'booking_id')->where('area', 1);
        // return $this->belongsToMany('\VCN\Models\Centros\Familia','booking_familias')->where('area',1);
    }

    public function schools()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingSchool', 'booking_id');
        // return $this->belongsToMany('\VCN\Models\Centros\Familia','booking_familias');
    }

    public function schools_area()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingSchool', 'booking_id')->where('area', 1);
        // return $this->belongsToMany('\VCN\Models\Centros\Familia','booking_familias')->where('area',1);
    }

    public function curso()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Curso');
    }

    public function categoria()
    {
        return $this->belongsTo('\VCN\Models\Categoria', 'category_id');
    }

    public function getCategoriaNameAttribute()
    {
        return $this->categoria ? $this->categoria->name : "-";
    }

    public function subcategoria()
    {
        return $this->belongsTo('\VCN\Models\Subcategoria', 'subcategory_id');
    }

    public function getSubcategoriaNameAttribute()
    {
        return $this->subcategoria ? $this->subcategoria->name : "-";
    }

    public function subcategoria_detalle()
    {
        return $this->belongsTo('\VCN\Models\SubcategoriaDetalle', 'subcategory_det_id');
    }

    public function getSubcategoriaDetalleNameAttribute()
    {
        return $this->subcategoria_detalle ? $this->subcategoria_detalle->name : "-";
    }


    public function pagos()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingPago', 'booking_id');
    }

    public function monedas()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingMoneda', 'booking_id');
    }

    public function logs()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingLog', 'booking_id');
    }

    public function oficina()
    {
        return $this->belongsTo('\VCN\Models\System\Oficina', 'oficina_id');
    }

    public function prescriptor()
    {
        return $this->belongsTo('\VCN\Models\Prescriptores\Prescriptor', 'prescriptor_id');
    }

    public function archivos()
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroArchivo', 'booking_id');
    }

    public function pdfs()
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroArchivo', 'booking_id')->where('notas', 'Booking-PDF');
    }

    public function pdfs_firmas()
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroArchivo', 'booking_id')->where('notas', 'Booking-PDF')->whereNotNull('firmas');
    }

    public function archivos_importantes()
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroArchivo', 'booking_id')->where('visible', 1)->whereNull('firmas');
    }

    public function archivos_firmas()
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroArchivo', 'booking_id')->where('visible', 1)->whereNotNull('firmas');
    }

    public function incidencias()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingIncidencia', 'booking_id');
    }

    public function origen()
    {
        return $this->belongsTo('\VCN\Models\Leads\Origen', 'origen_id');
    }

    public function suborigen()
    {
        return $this->belongsTo('\VCN\Models\Leads\Suborigen', 'suborigen_id');
    }

    public function suborigen_det()
    {
        return $this->belongsTo('\VCN\Models\Leads\SuborigenDetalle', 'suborigendet_id');
    }

    public function datos()
    {
        return $this->hasOne('\VCN\Models\Bookings\BookingDatos', 'booking_id');
    }

    public function getFullNameOrigenTxtAttribute()
    {
        $ret = $this->origen ? $this->origen->name : "-";

        if ($this->suborigen) {
            $ret .= " > " . $this->suborigen->name;
        }

        if ($this->suborigen_det) {
            $ret .= " > " . $this->suborigen_det->name;
        }

        if ($this->es_repetidor) {
            $ret = "REPETIDOR ($ret)";
        }

        return $ret;
    }

    public function getFullNameOrigenAttribute()
    {
        if ($this->es_repetidor) {
            return "REPETIDOR";
        }

        $ret = $this->origen ? $this->origen->name : "-";

        if ($this->suborigen) {
            $ret .= " > " . $this->suborigen->name;
        }

        if ($this->suborigen_det) {
            $ret .= " > " . $this->suborigen_det->name;
        }

        return $ret;
    }

    public function getFullNameOrigenIdAttribute()
    {
        if ($this->es_repetidor) {
            return "R";
        }

        $ret = $this->origen_id . "-" . $this->suborigen_id . "-" . $this->suborigendet_id;

        return $ret;
    }

    public function getFullNameOrigenUrlAttribute()
    {
        if ($this->es_repetidor) {
            return "REPETIDOR";
        }

        if (!$this->origen) {
            return "-";
        }

        $ret = "<a target='_blank' href='" . route('manage.viajeros.origenes.ficha', $this->origen_id) . "#ventas'>" . $this->origen->name . "</a>";

        if ($this->suborigen) {
            $ret .= " > " . "<a target='_blank' href='" . route('manage.viajeros.suborigenes.ficha', $this->suborigen_id) . "#ventas'>" . $this->suborigen->name . "</a>";
        }

        if ($this->suborigen_det) {
            $ret .= " > " . "<a target='_blank' href='" . route('manage.viajeros.suborigendetalles.ficha', $this->suborigendet_id) . "#ventas'>" . $this->suborigen_det->name . "</a>";
        }

        return $ret;
    }

    public function getDivisaVariacionAttribute()
    {
        if ($this->promo_cambio_fijo) {
            return false;
        }

        if ($this->pagado) {
            return $this->vdivisa;
        }

        if ($this->es_facturado) {
            return $this->vdivisa;
        }

        //Revisamos
        // $this->promo_cambio_fijo = $this->promo_cambio_fijo ? $this->promo_cambio_fijo : $this->promo_cambio_fijo_default;
        // $this->save();

        // dd($this->promo_cambio_fijo_default);

        if ($this->curso->es_convocatoria_cerrada) {
            $precio = $this->convocatoria ? $this->convocatoria->precio_auto : null;

            $p = $precio ? $precio->divisa_variacion : false;
            $this->vdivisa = $p;
            $this->save();

            return $p;
        }

        return false;
    }

    public function getTotalDivisaAttribute()
    {
        if ($this->pagado) {
            return $this->total;
        }

        if ($this->promo_cambio_fijo) {
            return $this->total;
        }

        if ($this->curso->es_convocatoria_cerrada) {
            $precio = $this->convocatoria ? $this->convocatoria->precio_auto : null;

            if (!$precio) {
                return $this->total;
            }

            if (!$precio->divisa_variacion) {
                return $this->total;
            }

            return $this->total; // + $precio->divisa_variacion;

        }

        return $this->total;
    }

    public function getTotalDivisaTxtAttribute()
    {
        return ConfigHelper::parseMoneda($this->total_divisa);
    }

    public function factura()
    {
        return $this->hasOne('\VCN\Models\Bookings\BookingFactura', 'booking_id');
    }
    public function factura_list()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingFactura', 'booking_id');
    }
    public function facturas()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingFactura', 'booking_id');
    }
    public function getFacturaLastAttribute()
    {
        return $this->factura_list->sortByDesc('numero')->first();
    }

    public function getPdfBookingLastAttribute()
    {
        return $this->pdfs->sortByDesc('id')->first();
    }

    public function getPdfsFirmasLastAttribute()
    {
        return $this->pdfs_firmas->sortByDesc('updated_at')->first();
    }

    public function getAbonoAttribute()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingFactura', 'booking_id')->where('total', '<', 0)->count() > 0;
    }

    public function getIncidenciasPendientesAttribute()
    {
        return $this->incidencias->where('estado', 0);
    }

    public function getTieneIncidenciasPendientesAttribute()
    {
        return $this->incidencias->where('estado', 0)->count() > 0;
    }

    public function getBotonIncidenciaAttribute()
    {
        $color = "btn-warning";
        $label = "Abrir nueva incidencia";

        if ($this->tiene_incidencias_pendientes) {
            $color = "btn-danger";
            $label .= " (hay pendientes)";
        }

        $ret = "<a href='#' data-toggle='modal' data-target='#modalBookingIncidencia' data-label='$label' class='btn btn-xs $color'><i class='fa fa-plus-circle'></i> <i class='fa fa-exclamation-triangle'></i> Incidencia</a>";

        return $ret;
    }


    public function getPlataformaNameAttribute()
    {
        return $this->plataforma_config->name;
    }

    public function getOficinaNameAttribute()
    {
        return $this->oficina ? $this->oficina->name : "-";
    }

    public function getOrigenNameAttribute()
    {
        if ($this->es_repetidor) {
            return "REPETIDOR";
        }

        return $this->origen ? $this->origen->name : "-";
    }

    public function getSuborigenNameAttribute()
    {
        if ($this->es_repetidor) {
            return "";
        }

        return $this->suborigen ? $this->suborigen->name : "-";
    }

    public function getSuborigenDetNameAttribute()
    {
        if ($this->es_repetidor) {
            return "";
        }

        return $this->suborigen_det ? $this->suborigen_det->name : "-";
    }

    public function getEsRepetidorAttribute()
    {
        if ($this->viajero->bookings->count() > 1) {
            $stplazas = \VCN\Models\Bookings\Status::where('plazas', 1)->pluck('id')->toArray();
            $nBookings = self::where('viajero_id', $this->viajero_id)->whereIn('status_id', $stplazas)->orderBy('id');

            $b = $nBookings->first();
            if ($b && ($b->id == $this->id)) //El primero no es
            {
                return false;
            }

            if ($nBookings->where('id', '<>', $this->id)->count() > 0) {
                return true;
            }
        }

        return false;
    }

    public function getConvocatoriaNameAttribute()
    {
        return $this->convocatoria ? $this->convocatoria->name : "?";
    }

    public function getConvocatoriaMonedaAttribute()
    {
        return $this->convocatoria ? $this->convocatoria->moneda_name : Session::get('vcn.moneda');
    }

    public function getSaldoAttribute()
    {
        $saldo = 0;
        return $this->pagos->sum('importe');
        // foreach($this->pagos as $pago)
        // {
        //     $saldo += $pago->importe;
        // }

        // return $saldo;
    }

    public function getSaldoPagadoAttribute()
    {
        return $this->saldo;
    }

    public function getTotalPagadoAttribute()
    {
        return $this->saldo;
    }

    public function getTotalCursoAttribute()
    {
        return $this->course_total_amount;
    }

    public function getTotalCursoNetoAttribute()
    {
        $t = $this->course_total_amount;
        $t = $t - $this->center_discount_amount;

        return $t;
    }

    public function getTotalCursoNetoEnMonedaAttribute()
    {
        $moneda = ConfigHelper::default_moneda();
        $m = Moneda::find($this->course_currency_id);

        $precio = $this->total_curso_neto;

        if ($m && $m->id != $moneda->id) {
            $precio = $precio * $this->getMonedaTasa($m->id);
        }

        return $precio;
    }

    public function getMonedasUsadasAttribute()
    {
        $monedasUsadas = [];

        array_push($monedasUsadas, $this->course_currency_id);

        foreach ($this->extras_curso as $extra) {
            array_push($monedasUsadas, $extra->moneda_id);
        }

        foreach ($this->extras_centro as $extra) {
            array_push($monedasUsadas, $extra->moneda_id);
        }

        foreach ($this->extras_alojamiento as $extra) {
            array_push($monedasUsadas, $extra->moneda_id);
        }

        foreach ($this->extras_generico as $extra) {
            array_push($monedasUsadas, $extra->moneda->id);
        }

        foreach ($this->extras_otros as $extra) {
            array_push($monedasUsadas, $extra->moneda_id);
        }

        array_push($monedasUsadas, $this->accommodation_currency_id);

        //nuevos uso de las monedas
        $tc = $this->convocatoria ? $this->convocatoria->precio_auto : null;
        if ($tc) {
            array_push($monedasUsadas, $tc->proveedor_moneda_id);
            array_push($monedasUsadas, $tc->vuelo_moneda_id);
            array_push($monedasUsadas, $tc->seguro_moneda_id);
            array_push($monedasUsadas, $tc->gastos_moneda_id);
            array_push($monedasUsadas, $tc->monitor_moneda_id);
            array_push($monedasUsadas, $tc->mb_moneda_id);
            array_push($monedasUsadas, $tc->comision_moneda_id);
        }

        $monedasUsadas = array_unique($monedasUsadas);

        return $monedasUsadas;
    }

    public function getMonedasUsadasTxtAttribute()
    {
        $monedas2Ret = [];

        $monedas = $this->monedas_usadas;

        foreach ($monedas as $moneda) {
            $m = Moneda::find($moneda);
            if ($m) {
                $mtxt = $m->name . " : " . ConfigHelper::monedaCambio($moneda, $this->id);

                array_push($monedas2Ret, $mtxt);
            }
        }

        return $monedas2Ret;
    }


    public function getSaldoPendienteAttribute()
    {
        $saldo = $this->saldo;
        $total = $this->total;

        return $total - $saldo;
    }

    public function devoluciones()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingDevolucion');
    }

    public function descuentos()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingDescuento');
    }

    public function descuentos_codigo()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingDescuento')->whereNotNull('codigo');
    }

    public function getEsCodigoAttribute()
    {
        return $this->descuentos_codigo->count() > 0;
    }

    public function descuentos_noearly()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingDescuento')->where('notas', '<>', "Descuento Early Bird");
    }

    public function getPrecioDescuentoSoloAttribute()
    {
        $dto = 0;

        if ($this->importes && $this->es_terminado) {
            return $this->importes['descuentos'];
        }

        $moneda = ConfigHelper::default_moneda();
        foreach ($this->descuentos as $descuento) {
            // $dto += $descuento->importe;
            $dtoi = 0;

            $m = Moneda::find($descuento->currency_id);

            if (!isset($descuentos[$m->currency_name])) {
                $descuentos[$m->currency_name] = 0;
            }
            $descuentos[$m->currency_name] += $descuento->importe;

            if ($m->id == $moneda->id) {
                $dtoi = $descuento->importe;
            } else {
                $dtoi = $descuento->importe * $this->getMonedaTasa($m->id);
            }

            $dto += $dtoi;
        }

        $descuentos_solo = $dto - $this->descuento_early_importe;
        return $descuentos_solo;
    }

    public function getPrecioDescuentoSoloTxtAttribute()
    {
        return ConfigHelper::parseMoneda($this->precio_descuento_solo);
    }

    public function getPrecioSinCancelacionAttribute()
    {
        return $this->total - $this->precio_cancelacion;
    }

    public function getTotalSinCancelacionAttribute()
    {
        return $this->total - $this->precio_cancelacion;
    }

    public function getPrecioCancelacionAttribute()
    {
        $total_cancelacion = 0;

        if ($this->importes && $this->es_terminado) {
            return $this->importes['cancelacion'];
        }

        $seguro = $this->cancelacion ? $this->cancelacion_seguro : null;
        if ($seguro) {
            $moneda = ConfigHelper::default_moneda();

            $iextra = $seguro->precio;

            $m = Moneda::find($seguro->moneda_id);

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }
            $subtotal[$m->currency_name] += $iextra;

            if ($m->id == $moneda->id) {
                $p = $iextra;
            } else {
                $p = $iextra * $this->getMonedaTasa($m->id);
            }

            $total_cancelacion += $p;
        }

        return $total_cancelacion;
    }

    public function getPrecioCancelacionTxtAttribute()
    {
        return ConfigHelper::parseMoneda($this->precio_cancelacion);
    }


    public function centro()
    {
        return $this->curso->centro();
    }

    public function viajero()
    {
        return $this->belongsTo('\VCN\Models\Leads\Viajero');
    }

    public function tutor()
    {
        return $this->belongsTo('\VCN\Models\Leads\Tutor', 'tutor_id');
    }

    public function solicitud()
    {
        return $this->belongsTo('\VCN\Models\Solicitudes\Solicitud', 'solicitud_id');
    }

    public function asignado()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public function getAsignadoNameAttribute()
    {
        return $this->asignado ? $this->asignado->full_name : "-";
    }

    public function autorizado()
    {
        return $this->belongsTo('\VCN\Models\User', 'ovbkg_id');
    }

    public function alojamiento()
    {
        return $this->belongsTo('\VCN\Models\Alojamientos\Alojamiento', 'accommodation_id');
    }

    public function multis()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingMulti', 'booking_id');
    }

    public function especialidades()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingMulti', 'booking_id');
    }

    public function convocatoria()
    {
        if ($this->convocatory_multi_id) {
            return $this->belongsTo('\VCN\Models\Convocatorias\ConvocatoriaMulti', 'convocatory_multi_id');
        }

        if ($this->convocatory_close_id) {
            return $this->belongsTo('\VCN\Models\Convocatorias\Cerrada', 'convocatory_close_id');
        }

        return $this->belongsTo('\VCN\Models\Convocatorias\Abierta', 'convocatory_open_id');
    }

    public function getConvocatoryIdAttribute()
    {
        return $this->convocatoria ? $this->convocatoria->id : 0;
    }

    public function getConvocatoriaIdAttribute()
    {
        if ($this->convocatory_close_id) {
            return "convocatory_close_id";
        }

        if ($this->convocatory_multi_id) {
            return "convocatory_multi_id";
        }

        return "convocatory_open_id";
    }

    public function getConvocatoriaTipoAttribute()
    {
        if ($this->convocatory_close_id) {
            return 1;
        }

        if ($this->convocatory_multi_id) {
            return 3;
        }

        return 2;
    }

    public function getConvocatoriaLinkAttribute()
    {
        $convo_id = 0;
        $convo = $this->convocatoria ? $this->convocatoria->name : "-";
        if ($this->convocatory_close_id) {
            $convo_id = $this->convocatory_close_id;
            return "<a target='_blank' href='" . route('manage.convocatorias.cerradas.ficha', $convo_id) . "'>$convo</a>";
        }

        if ($this->convocatory_multi_id) {
            $convo_id = $this->convocatory_multi_id;
            return "<a target='_blank' href='" . route('manage.convocatorias.multis.ficha', $convo_id) . "'>$convo</a>";
        }

        $convo_id = $this->convocatory_open_id;
        return "<a target='_blank' href='" . route('manage.convocatorias.abiertas.ficha', $convo_id) . "'>$convo</a>";
    }

    public function getVuelosAttribute()
    {
        if ($this->convocatoria) {
            if ($this->convocatoria->vuelos) {
                if ($this->convocatoria->vuelos->count()) {
                    return $this->convocatoria->vuelos;
                }
            }
        }

        return null;
    }

    public function getVuelosWebAttribute()
    {
        $ret = collect();
        if ($this->convocatoria) {
            if ($this->convocatoria->vuelos) {
                if ($this->convocatoria->vuelos->count()) {
                    foreach ($this->convocatoria->vuelos as $v) {
                        if (!$v->vuelo->interno) {
                            $ret->push($v);
                        }
                    }
                }
            }
        }

        return $ret->count() ? $ret : null;
    }

    public function getVuelosListAttribute()
    {
        $ret = [];

        if ($this->convocatory_close_id) {
            $ret[0] = "Sin Vuelo";

            if ($this->vuelos) {
                foreach ($this->vuelos as $v) {
                    $ret[$v->vuelo->id] = $v->vuelo->name;
                }
            }
        }

        $permiso = \VCN\Models\System\UserRole::permisoEdit('bookings-vuelos');
        if ($permiso) {
            $ret += \VCN\Models\Convocatorias\Vuelo::where('any', $this->any)->where('individual', true)->pluck('name', 'id')->toArray();
        }

        return $ret;
    }

    public function convocatoria_class($c_id)
    {
        $class = "";

        if ($this->curso->convocatoria_tipo == 'abierta' && $this->convocatory_open_id == $c_id) {
            if ($this->curso->convocatoriasAbiertas->count() == 1) {
                $class = " obligatorio";
            }

            return "fa fa-check-circle-o" . $class;
        }

        if ($this->curso->convocatoria_tipo == 'cerrada' && $this->convocatory_close_id == $c_id) {
            if ($this->curso->convocatoriasCerradas->count() == 1) {
                $class = " obligatorio";
            }

            return "fa fa-check-circle-o" . $class;
        }

        return "fa fa-circle-thin";
    }

    public function getConvocatoriaPrecioAttribute()
    {
        if ($this->convocatory_close_id) {
            return $this->convocatory_close_price;
        }

        return $this->convocatory_open_price;
    }

    public function getFechaAttribute()
    {
        return $this->fecha_reserva ? $this->fecha_reserva->format('d/m/Y') : $this->created_at->format('d/m/Y');
    }

    public function getFechaInicioAttribute()
    {
        return $this->course_start_date;
    }

    public function getFechaCursoAttribute()
    {
        return Carbon::parse($this->course_start_date);
    }

    public function getAnyAttribute()
    {
        return Carbon::parse($this->course_start_date)->year;
    }

    public function getNameAttribute()
    {
        return $this->viajero->full_name . " " . $this->fecha;
    }

    public function getViajeroNameAttribute()
    {
        return $this->viajero->name;
    }

    /**
     *
     */
    public function getProgramaAttribute()
    {
        $ret = $this->curso->es_convocatoria_multi ? $this->convocatoria : $this->curso;

        return $ret ? $ret->name : null;
    }

    public function getDescuentosImporteAttribute()
    {
        // $moneda = Moneda::where('currency_name', Session::get('vcn.moneda'))->first();
        $moneda = ConfigHelper::default_moneda();

        $dto = 0;
        foreach ($this->descuentos as $descuento) {
            // $dto += $descuento->importe;

            $m = Moneda::find($descuento->currency_id);

            if (!isset($descuentos[$m->currency_name])) {
                $descuentos[$m->currency_name] = 0;
            }
            $descuentos[$m->currency_name] += $descuento->importe;

            if ($m->id == $moneda->id) {
                $dtoi = $descuento->importe;
            } else {
                $dtoi = $descuento->importe * $this->getMonedaTasa($m->id);
            }

            $dto += $dtoi;
        }

        return $dto;
    }

    public function getExtrasAttribute()
    {
        return BookingExtra::where('booking_id', $this->id)->get();
    }

    public function getExtrasImporteAttribute()
    {
        // $moneda = Moneda::where('currency_name', Session::get('vcn.moneda'))->first();
        $moneda = ConfigHelper::default_moneda();

        $total = 0;
        foreach ($this->extras as $extra) {
            $m = Moneda::find($extra->moneda_id);

            if ($m->id == $moneda->id) {
                $total += $extra->unidades * $extra->precio;
            } else {
                $p = $extra->precio * $this->getMonedaTasa($m->id);
                $total += $extra->unidades * $p;
            }
        }

        return $total;
    }

    public function getExtrasCursoAttribute()
    {
        return BookingExtra::where('booking_id', $this->id)->where('tipo', 0)->get();
    }

    public function getExtrasCursoImporteAttribute()
    {
        if ($this->importes && $this->es_terminado) {
            return $this->importes['extras_curso'];
        }

        // $moneda = Moneda::where('currency_name', Session::get('vcn.moneda'))->first();
        $moneda = ConfigHelper::default_moneda();

        $total = 0;
        foreach ($this->extras_curso as $extra) {
            $m = Moneda::find($extra->moneda_id);

            if ($m->id == $moneda->id) {
                $total += $extra->unidades * $extra->precio;
            } else {
                $p = $extra->precio * $this->getMonedaTasa($m->id);
                $total += $extra->unidades * $p;
            }
        }

        return $total;
    }

    public function getExtrasCentroAttribute()
    {
        return BookingExtra::where('booking_id', $this->id)->where('tipo', 1)->get();
    }

    public function getExtrasCentroImporteAttribute()
    {
        if ($this->importes && $this->es_terminado) {
            return $this->importes['extras_centro'];
        }

        // $moneda = Moneda::where('currency_name', Session::get('vcn.moneda'))->first();
        $moneda = ConfigHelper::default_moneda();

        $total = 0;
        foreach ($this->extras_centro as $extra) {
            $m = Moneda::find($extra->moneda_id);

            if ($m->id == $moneda->id) {
                $total += $extra->unidades * $extra->precio;
            } else {
                $p = $extra->precio * $this->getMonedaTasa($m->id);
                $total += $extra->unidades * $p;
            }
        }

        return $total;
    }

    public function getExtrasAlojamientoAttribute()
    {
        return BookingExtra::where('booking_id', $this->id)->where('tipo', 3)->get();
    }

    public function getExtrasAlojamientoImporteAttribute()
    {
        if ($this->importes && $this->es_terminado) {
            return $this->importes['extras_alojamiento'];
        }

        // $moneda = Moneda::where('currency_name', Session::get('vcn.moneda'))->first();
        $moneda = ConfigHelper::default_moneda();

        $total = 0;
        foreach ($this->extras_alojamiento as $extra) {
            $m = Moneda::find($extra->moneda_id);

            if ($m->id == $moneda->id) {
                $total += $extra->unidades * $extra->precio;
            } else {
                $p = $extra->precio * $this->getMonedaTasa($m->id);
                $total += $extra->unidades * $p;
            }
        }

        return $total;
    }

    public function getExtrasGenericoAttribute()
    {
        return BookingExtra::where('booking_id', $this->id)->where('tipo', 2)->get();
    }

    public function getExtrasCancelacionAttribute()
    {
        return BookingExtra::where('booking_id', $this->id)->where('tipo', 5)->get();
    }

    public function getExtrasGenericoImporteAttribute()
    {
        if ($this->importes && $this->es_terminado) {
            return $this->importes['extras_generico'];
        }

        // $moneda = Moneda::where('currency_name', Session::get('vcn.moneda'))->first();
        $moneda = ConfigHelper::default_moneda();

        $total = 0;
        foreach ($this->extras_generico as $extra) {
            if ($extra->es_cancelacion) {
                continue;
            }

            $m = Moneda::find($extra->moneda_id);

            if ($m->id == $moneda->id) {
                $total += $extra->unidades * $extra->precio;
            } else {
                $p = $extra->precio * $this->getMonedaTasa($m->id);
                $total += $extra->unidades * $p;
            }
        }

        return $total;
    }

    public function getExtrasOtrosAttribute()
    {
        return BookingExtra::where('booking_id', $this->id)->where('tipo', 4)->get();
    }

    public function getExtrasOtrosImporteAttribute()
    {
        if ($this->importes && $this->es_terminado) {
            return $this->importes['extras_otros'];
        }

        $moneda = ConfigHelper::default_moneda(); // Moneda::where('currency_name', Session::get('vcn.moneda'))->first();

        $total = 0;
        foreach ($this->extras_otros as $extra) {
            $m = Moneda::find($extra->moneda_id);

            if ($m->id == $moneda->id) {
                $total += $extra->unidades * $extra->precio;
            } else {
                $p = $extra->precio * $this->getMonedaTasa($m->id);
                $total += $extra->unidades * $p;
            }
        }

        return $total;
    }


    public function setStatus($status, $motivo = null, $nota = null)
    {
        if ($this->es_presupuesto) {
            return;
        }

        if ($status == $this->status_id) {
            // OJO. por si el cambio es a su mismo estado.
            return;
        }

        if (!$this->status) {
            $this->setContactosSOS();
            $this->setCorporativo();
        }

        //estado previo
        $status0 = $this->status_id;

        BookingLog::addLogStatus($this, $status);

        //Restamos la venta
        if ($status == ConfigHelper::config('booking_status_refund')) {
            \VCN\Models\Informes\Venta::remove($this);
        } elseif (($status == ConfigHelper::config('booking_status_cancelado')) && ($this->status_id != ConfigHelper::config('booking_status_refund'))) {
            //Pq en el refund ya se hizo
            \VCN\Models\Informes\Venta::remove($this);
        }

        $this->status_id = $status;
        $this->save();

        if ($this->viajero->booking_id == $this->id) {
            $this->viajero->booking_status_id = $status;
            $this->viajero->save();
        }

        if ($status == 0)
        {
            // if($status0>0)
            // {
            //     //no se archiva
            // }
            // else
            {
                $nota .= "[setStatus:0]";
                $this->archivar($motivo, $nota);
            }
        }
        elseif ($status == ConfigHelper::config('booking_status_prebooking')) {
            $this->fecha_reserva = Carbon::now();
            $this->save();
            $this->onlineFin();

            \VCN\Models\Informes\Venta::add($this);

            $this->setContable();
            $this->setExamenes();
            $this->mailAviso();

            if (!$this->es_directo) {
                $this->mail();
                $this->mailReunion();

                $this->viajero->setInscrito(true);
                $this->viajero->setEmail();

                if ($this->notaPago35(false, "(status)")) {
                    $this->notaPago35_mail();
                }
            }

            if ($this->vuelo) {
                if ($this->transporte_requisitos || $this->transporte_otro || $this->transporte_recogida) {
                    MailHelper::mailBookingVuelo($this->id);
                    BookingLog::addLog($this, "Email Aviso aporte en Booking enviado");
                }
            }
        } elseif ($status == ConfigHelper::config('booking_status_prereserva')) {
            if ($this->es_online) {
                $this->mailAviso();
            }

            $this->fecha_prereserva = Carbon::now();
            $this->save();

            $this->viajero->setEmail();

            $this->onlineFin();
        } elseif ($status == ConfigHelper::config('booking_status_cancelado')) {
            $this->decidiendo();

            $this->tareaAutoEliminar();
        } elseif ($status == ConfigHelper::config('booking_status_overbooking')) {
            $this->fecha_reserva = Carbon::now();
            $this->save();
            $this->onlineFin();

            MailHelper::mailOvbkg($this->id);

            $this->setContable();

            $this->viajero->setInscrito(true);
        }


        //Email cancelado
        if (($status == ConfigHelper::config('booking_status_refund')) && ($status0 != ConfigHelper::config('booking_status_refund'))) {
            $this->decidiendo();
            //Si pasa a refund enviamos
            MailHelper::mailBookingCancelar($this->id);
        } elseif (($status == ConfigHelper::config('booking_status_cancelado')) && ($status0 != ConfigHelper::config('booking_status_refund'))) {
            //Si pasa a cancelado pero no venía de refund
            MailHelper::mailBookingCancelar($this->id);
        }

        \VCN\Models\Bookings\BookingLogStatus::addLog($this, $status0);

        $this->trigger();
        $this->triggerTarea();
    }

    public function getStatusNameAttribute()
    {
        if ($this->status_id == ConfigHelper::config('booking_status_overbooking')) {
            $name = $this->status->name . " ";

            if ($this->ovbkg_pa) {
                $name .= "(A)";
            }

            if ($this->ovbkg_pv) {
                $name .= "(V)";
            }

            return $name;
        }

        if ($this->status_id == ConfigHelper::config('booking_status_prereserva')) {
            $name = $this->status->name . " ";

            if($this->ovbkg_id)
            {
                $name .= "OVKG";
            }

            if ($this->ovbkg_pa) {
                $name .= "(A)";
            }

            if ($this->ovbkg_pv) {
                $name .= "(V)";
            }

            return $name;
        }

        if ($this->es_presupuesto) {
            // return "Presupuesto";
        }

        return $this->status ? $this->status->name : "Pendiente";
    }

    public function getStatusAreaAttribute()
    {
        //Inscrito, Overbooking, En el extranjero, De vuelta a casa

        if ($this->status) {
            if ($this->status_id == ConfigHelper::config('booking_status_overbooking')) {
                return $this->status->name;
            }

            if ($this->status->orden > 50) {
                return $this->status_name;
            } else {
                return "Inscrito";
            }
        }


        return "-";
    }

    public function getStatusIconoAttribute()
    {
        $st = $this->status ? $this->status->name : "Pendiente";

        $class = "icon-st icon-st-mini";
        $class .= " icon-status-" . strtolower(snake_case($st));

        return "<div class='$class'></div>";
    }

    public function getMonedaTasa($moneda_id)
    {
        return ConfigHelper::monedaCambio($moneda_id, $this->id);

        // $moneda = $this->monedas->where('moneda_id',$moneda_id)->first();
        // if($moneda)
        // {
        //     return $moneda->rate;
        // }

        // $moneda = Moneda::find($moneda_id);
        // return $moneda?$moneda->rate:1;
    }

    public function getTotalEnMonedaAttribute()
    {
        $moneda = ConfigHelper::default_moneda();
        $m = Moneda::find($this->course_currency_id);

        $precio = $this->total;

        if ($m && $m->id != $moneda->id) {
            $precio = $precio * $this->getMonedaTasa($m->id);
        }

        return $precio;
    }

    public function updateImportesFull()
    {
        $importes = $this->importes;

        $importes['descuentos'] = $this->precio_descuento_solo;
        $importes['cancelacion'] = $this->precio_cancelacion;

        $importes['extras_centro'] = $this->extras_centro_importe;
        $importes['extras_curso'] = $this->extras_curso_importe;
        $importes['extras_alojamiento'] = $this->extras_alojamiento_importe;
        $importes['extras_generico'] = $this->extras_generico_importe;
        $importes['extras_otros'] = $this->extras_otros_importe;

        $p = floatval($this->saldo_pagado);
        $importes['saldo_pagado'] = $p;
        $importes['saldo_pendiente'] = floatval($this->total - $p);

        $this->importes = $importes;
        $this->save();
    }

    public function updateImportesInit()
    {
        $importes['descuentos'] = 0;
        $importes['cancelacion'] = 0;

        $importes['extras_centro'] = 0;
        $importes['extras_curso'] = 0;
        $importes['extras_alojamiento'] = 0;
        $importes['extras_generico'] = 0;
        $importes['extras_otros'] = 0;

        $importes['saldo_pagado'] = 0;
        $importes['saldo_pendiente'] = 0;

        $this->importes = $importes;
        $this->save();
    }

    public function updateImportes()
    {
        //importes: descuentos, cancelacion, extras:centro,curso,alojamiento,generico,otros
        $importes = $this->importes;

        $importes['descuentos'] = $this->precio_descuento_solo;
        $importes['cancelacion'] = $this->precio_cancelacion;

        $importes['extras_centro'] = $this->extras_centro_importe;
        $importes['extras_curso'] = $this->extras_curso_importe;
        $importes['extras_alojamiento'] = $this->extras_alojamiento_importe;
        $importes['extras_generico'] = $this->extras_generico_importe;
        $importes['extras_otros'] = $this->extras_otros_importe;

        $this->importes = $importes;
        $this->save();
    }

    public function updatePagos()
    {
        //importes: pagado, pendiente
        $pagos = $this->importes;

        $p = floatval($this->saldo_pagado);
        $pagos['saldo_pagado'] = $p;
        $pagos['saldo_pendiente'] = floatval($this->total - $p);

        $this->importes = $pagos;
        $this->save();
    }

    public function getPrecioTotalAttribute()
    {
        $subtotal = [];
        $total = 0;

        $total_extras = 0;
        $total_alojamiento = 0;
        $total_extras_descontar = 0;
        $total_cancelacion = 0;

        $importes = [];

        $moneda = ConfigHelper::default_moneda();

        $importes['extras_curso'] = 0;
        $totex = 0;
        foreach ($this->extras_curso as $extra) {
            $m = Moneda::find($extra->moneda_id);

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }

            if ($extra->extra) {
                //bug de obligatorio y unidades 0 pq lo marcan obligatorio después
                if ($extra->extra->requerido && !$extra->unidades) {
                    $extra->unidades = 1;
                    $extra->save();
                }
            }

            $subtotal[$m->currency_name] += $extra->unidades * $extra->precio;

            if ($m->id == $moneda->id) {
                $totex += $extra->unidades * $extra->precio;
            } else {
                $p = $extra->precio * $this->getMonedaTasa($m->id);
                $totex += $extra->unidades * $p;
            }
        }

        $total += $totex;
        $total_extras += $total;
        $importes['extras_curso'] = $totex;


        $importes['extras_centro'] = 0;
        $totex = 0;
        foreach ($this->extras_centro as $extra) {
            $m = Moneda::find($extra->moneda_id);

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }

            if ($extra->extra) {
                //bug de obligatorio y unidades 0 pq lo marcan obligatorio después
                if ($extra->extra->requerido && !$extra->unidades) {
                    $extra->unidades = 1;
                    $extra->save();
                }
            }

            $subtotal[$m->currency_name] += $extra->unidades * $extra->precio;

            if ($m->id == $moneda->id) {
                $totex += $extra->unidades * $extra->precio;
            } else {
                $p = $extra->precio * $this->getMonedaTasa($m->id);
                $totex += $extra->unidades * $p;
            }
        }

        $total += $totex;
        $total_extras += $total;
        $importes['extras_centro'] = $totex;


        $importes['extras_alojamiento'] = 0;
        $totex = 0;
        foreach ($this->extras_alojamiento as $extra) {
            $m = Moneda::find($extra->moneda_id);

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }

            if ($extra->extra) {
                //bug de obligatorio y unidades 0 pq lo marcan obligatorio después
                if ($extra->extra->requerido && !$extra->unidades) {
                    $extra->unidades = 1;
                    $extra->save();
                }
            }

            $subtotal[$m->currency_name] += $extra->unidades * $extra->precio;

            if ($m->id == $moneda->id) {
                $totex += $extra->unidades * $extra->precio;
            } else {
                $p = $extra->precio * $this->getMonedaTasa($m->id);
                $totex += $extra->unidades * $p;
            }
        }

        $total += $totex;
        $total_extras += $total;
        $importes['extras_alojamiento'] = $totex;


        $importes['extras_generico'] = 0;
        $totex = 0;
        foreach ($this->extras_generico as $extra) {
            if ($extra->tipo_unidad < 2) {
                $m = Moneda::find($extra->moneda_id);

                if (!isset($subtotal[$m->currency_name])) {
                    $subtotal[$m->currency_name] = 0;
                }
                $subtotal[$m->currency_name] += $extra->unidades * $extra->precio;

                if ($m->id == $moneda->id) {
                    $p = $extra->unidades * $extra->precio;
                    $totex += $p;
                } else {
                    $p = ($extra->precio * $this->getMonedaTasa($m->id)) * $extra->unidades;
                    $totex += $p;
                }
            }
        }

        $total += $totex;
        $total_extras += $total;
        $total_extras_descontar += $total;
        $importes['extras_generico'] = $totex;


        $importes['extras_otros'] = 0;
        $totex = 0;
        foreach ($this->extras_otros as $extra) {
            $m = Moneda::find($extra->moneda_id);

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }
            $subtotal[$m->currency_name] += $extra->unidades * $extra->precio;

            if ($m->id == $moneda->id) {
                $totex += $extra->unidades * $extra->precio;
            } else {
                $p = $extra->precio * $this->getMonedaTasa($m->id);
                $totex += $extra->unidades * $p;
            }
        }

        $total += $totex;
        $total_extras += $total;
        $importes['extras_otros'] = $totex;

        $total_curso = 0;

        //Convocatoria
        $c = $this->convocatoria;

        if ($c) {
            $m = Moneda::find($this->course_currency_id);
            $precio = (float) $this->course_total_amount;

            if ($this->es_terminado) {
                $precio = (float) $this->course_total_amount; //del curso
            } else {
                if ($this->curso->es_convocatoria_cerrada) {
                    $precio = $c->precio;
                } elseif ($this->curso->es_convocatoria_abierta) {
                    $fechaini = Carbon::parse($this->course_start_date)->format('d/m/Y');
                    $fechafin = Carbon::parse($this->course_end_date)->format('d/m/Y');
                    $duracion = $this->weeks ? $this->weeks : 0;
                    $p = $c->calcularPrecio($fechaini, $fechafin, $duracion);

                    $precio = 0;
                    if ($p) {
                        $importe = $p['importe'];
                        $m = Moneda::find($p['moneda_id']);

                        $precio = $importe;
                    }
                } elseif ($this->curso->es_convocatoria_multi) {
                    $duracion = $this->weeks ? $this->weeks : 0;
                    $precio = $c->calcularPrecio($duracion, $this->id);

                    if ($precio) {
                        $m = Moneda::find($c->moneda_id);
                    }
                }
            }

            $total_curso = $precio;

            $moneda = ConfigHelper::default_moneda();
            if (!$m) {
                $m = $moneda;
            }

            $this->course_total_amount = $precio;
            $this->save();

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }
            $subtotal[$m->currency_name] += $precio;

            if ($m->id == $moneda->id) {
                $total += $precio;
            } else {
                $mt = (float) $this->getMonedaTasa($m->id);
                $p = $precio * $mt;
                $total += $p;

                // $total_curso = $p;
            }
        }
        elseif ($this->es_terminado) //????
        {
            $m = ConfigHelper::default_moneda();
            $total_curso = $this->course_total_amount;
            $total += $total_curso;

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }
            $subtotal[$m->currency_name] += $total_curso;
        }

        //Alojamiento
        $importes['alojamiento'] = 0;
        if ($this->alojamiento) {
            $m = Moneda::find($this->accommodation_currency_id);

            if ($this->es_terminado) {
                $importe = $this->accommodation_total_amount;
            } else {

                $fechaini = Carbon::parse($this->accommodation_start_date)->format('d/m/Y');
                $fechafin = Carbon::parse($this->accommodation_end_date)->format('d/m/Y');
                $precio = $this->alojamiento->calcularPrecio($fechaini, $fechafin, $this->accommodation_weeks);

                $importe = $precio['importe'];
                $mid = $precio['moneda_id'];
                $m = Moneda::find($mid);
                
                $this->accommodation_total_amount = $importe;
                $this->accommodation_currency_id = $mid;
                $this->save();
            }

            $total_alojamiento = $importe;
            $importes['alojamiento'] = $importe;

            if (!$m)
            {
                $m = $moneda;
            }

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }
            $subtotal[$m->currency_name] += $importe;

            if ($m->id == $moneda->id) {
                $total += $importe;
            } else {
                $p = $importe * $this->getMonedaTasa($m->id);
                $total += $p;
            }
        }

        //Extra genérico Porcentaje
        $egp = 0;
        foreach ($this->extras_generico as $extra) {
            if ($extra->tipo_unidad == 2) {
                $iextra = ($extra->precio * $total) / 100;

                $m = Moneda::find($extra->moneda_id);

                if (!isset($subtotal[$m->currency_name])) {
                    $subtotal[$m->currency_name] = 0;
                }
                $subtotal[$m->currency_name] += $iextra;

                if ($m->id == $moneda->id) {
                    $total += $iextra;
                } else {
                    $total += $iextra * $this->getMonedaTasa($m->id);
                }

                $total_extras += $iextra;
                $egp += $iextra;
            }
        }
        $importes['extras_generico'] += $egp;

        //Seguro Cancelacion auto
        $seguro = $this->updateCancelacion($total);
        if ($seguro) {
            $iextra = $seguro->precio;

            $m = Moneda::find($seguro->moneda_id);

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }
            $subtotal[$m->currency_name] += $iextra;

            if ($m->id == $moneda->id) {
                $p = $iextra;
            } else {
                $p = $iextra * $this->getMonedaTasa($m->id);
            }

            $total += $p;
            $total_cancelacion += $p;
        }
        $importes['cancelacion'] = $total_cancelacion;

        //Especialidades Multi
        // if($this->fase==1)
        // {
        //     if($this->curso->es_convocatoria_multi)
        //     {
        //         foreach($this->multis as $multi)
        //         {
        //             $total += $multi->precio;
        //             $subtotal[$moneda->currency_name] += $multi->precio;
        //         }
        //     }
        // }

        // if($this->fase>1)
        // {
        //     $total = $this->total;
        // }

        if ($this->divisa_variacion) {
            $total = $total + $this->divisa_variacion;
        }

        // $total = ceil($total);
        $total_bruto = $total;

        //Descuento Centro y Early
        $dto_pc = $this->aplicarDescuento();

        //BookingDescuentos
        $descuentos = [];
        $dto = 0;
        foreach ($this->descuentos as $descuento) {
            // $dto += $descuento->importe;

            $m = Moneda::find($descuento->currency_id);

            if (!isset($descuentos[$m->currency_name])) {
                $descuentos[$m->currency_name] = 0;
            }
            $descuentos[$m->currency_name] += $descuento->importe;

            if ($m->id == $moneda->id) {
                $dtoi = $descuento->importe;
            } else {
                $dtoi = $descuento->importe * $this->getMonedaTasa($m->id);
            }

            $dto += $dtoi;
        }
        $importes['descuentos'] = $dto;

        //BookingDevoluciones
        $devoluciones = [];
        $devol = 0;
        foreach ($this->devoluciones as $d) {
            // $dto += $descuento->importe;

            $m = Moneda::find($d->currency_id);

            if (!isset($devoluciones[$m->currency_name])) {
                $devoluciones[$m->currency_name] = 0;
            }
            $devoluciones[$m->currency_name] += $d->importe;

            if ($m->id == $moneda->id) {
                $devi = $d->importe;
            } else {
                $devi = $d->importe * $this->getMonedaTasa($m->id);
            }

            $devol += $devi;
        }
        $importes['devoluciones'] = $devol;

        //Moneda dto centro (especial)
        $dto_pc_moneda = $dto_pc;
        $m = $moneda;
        if ($this->course_currency_id) {
            $m = Moneda::find($this->course_currency_id); //->first();
        }

        if ($m && $m->id != $moneda->id) {
            $dto_pc_moneda = $dto_pc * $this->getMonedaTasa($m->id);
        }
        $total = $total - $dto - $dto_pc_moneda - $devol;
        $total_neto = $total;


        //Formato moneda
        $subtotal_txt = [];
        foreach ($subtotal as $stk => $stv) {
            $v = ConfigHelper::parseMoneda($stv, $stk);
            $subtotal_txt[$stk] = $v;
        }

        $descuento_txt = ConfigHelper::parseMoneda($dto);
        $descuento_pc_txt = ConfigHelper::parseMoneda($dto_pc, $this->curso_moneda);

        $this->total = $total;
        $this->importes = $importes;
        // $this->save();

        if ($this->pagado) {
            $pt = (float) $this->pagado_total;
            if ($total != $pt) {
                $this->pagado_total = $total;
                $this->save();
            }

            // $total = $this->pagado_total;
        }

        $total_txt = ConfigHelper::parseMoneda($total);

        $total_descontar = $total_curso + $total_alojamiento + $total_extras_descontar;

        $descuentos_solo = $dto - $this->descuento_early_importe;
        $descuentos_solo_txt = ConfigHelper::parseMoneda($descuentos_solo);

        //subtotales desglose: subtotal,descuentos
        $total_subtotal = [];
        $total_subtotal_txt = $total_txt;
        foreach ($subtotal as $ksub => $vsub) {
            $d = isset($descuentos[$ksub]) ? $descuentos[$ksub] : 0;
            $i = $vsub - $d;

            //dto especial
            if ($this->curso_moneda == $ksub) {
                $i = $i - $dto_pc;
            }

            $total_subtotal[$ksub] = ConfigHelper::parseMoneda($i, $ksub);
        }

        $ret = [
            'subtotal' => $subtotal, 'subtotal_txt' => $subtotal_txt, 'total' => $total, 'total_txt' => $total_txt,
            'total_bruto' => $total_bruto, 'total_neto' => $total_neto,
            'descuento' => $dto, 'descuento_txt' => $descuento_txt, 'descuento_solo_txt' => $descuentos_solo_txt,
            'descuento_especial' => $dto_pc, 'descuento_especial_txt' => $descuento_pc_txt,
            'descuentos' => $descuentos,
            'devolucion' => $devol, 'devoluciones' => $devoluciones,
            'total_extras' => $total_extras, 'total_cancelacion' => $total_cancelacion, 'total_extras_solo' => $total_extras - $total_cancelacion,
            'total_curso' => $total_curso, 'total_descontar' => $total_descontar,
            'total_sin_cancelacion' => $total - $total_cancelacion,
            'total_curso_neto' => ($total_curso - $dto - $dto_pc),
            'total_subtotal' => $total_subtotal,
            'total_subtotal_txt' => $total_subtotal_txt,
        ];

        // dd($ret);

        return $ret;
    }

    public static function web_precio_total($curso_id, $curso_fecha, $curso_weeks, $alojamiento_id, $alojamiento_fecha, $alojamiento_weeks)
    {
        $subtotal = [];
        $total = 0;

        $total_extras = 0;
        $total_alojamiento = 0;
        $total_extras_descontar = 0;
        $total_cancelacion = 0;

        $moneda = ConfigHelper::default_moneda();

        $curso = Curso::find($curso_id);
        $alojamiento = Alojamiento::find($alojamiento_id);

        foreach ($curso->extras_obligatorios as $extra) {
            $m = Moneda::find($extra->moneda_id);

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }

            $subtotal[$m->currency_name] += 1 * $extra->precio;

            if ($m->id == $moneda->id) {
                $total += 1 * $extra->precio;
            } else {
                $rate = $m->rate;
                $p = $extra->precio * $rate;
                $total += 1 * $p;
            }

            $total_extras += $total;
        }

        foreach ($curso->extras_centro_obligatorios as $extra) {
            $m = Moneda::find($extra->moneda_id);

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }

            $subtotal[$m->currency_name] += 1 * $extra->precio;

            if ($m->id == $moneda->id) {
                $total += 1 * $extra->precio;
            } else {
                $rate = $m->rate;
                $p = $extra->precio * $rate;
                $total += 1 * $p;
            }

            $total_extras += $total;
        }

        foreach ($alojamiento->extras_obligatorios as $extra) {
            $m = Moneda::find($extra->moneda_id);

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }

            $subtotal[$m->currency_name] += 1 * $extra->precio;

            if ($m->id == $moneda->id) {
                $total += 1 * $extra->precio;
            } else {
                $rate = $m->rate;
                $p = $extra->precio * $rate;
                $total += 1 * $p;
            }

            $total_extras += $total;
        }

        foreach ($curso->extras_genericos_obligatorios as $extra) {
            if ($extra->tipo_unidad < 2) {
                $m = Moneda::find($extra->moneda_id);

                if (!isset($subtotal[$m->currency_name])) {
                    $subtotal[$m->currency_name] = 0;
                }
                $subtotal[$m->currency_name] += 1 * $extra->precio;

                if ($m->id == $moneda->id) {
                    $p = 1 * $extra->precio;
                    $total += $p;
                } else {
                    $rate = $m->rate;
                    $p = $extra->precio * $rate * 1;
                    $total += $p;
                }

                $total_extras += $total;
            }
        }

        $total_curso = 0;

        //Convocatoria
        $fechaini = Carbon::createFromFormat('d/m/Y', $curso_fecha);
        // $c = ConvocatoriaAbierta::where('course_id', $curso_id)
        //     ->where('convocatory_open_valid_start_date','<=',$fechaini)
        //     ->where('convocatory_open_valid_end_date','>=',$fechaini)
        //     ->first();
        $cas = ConvocatoriaAbierta::where('course_id', $curso_id)->get();
        if ($cas->count() > 1) {
            $c = ConvocatoriaAbierta::where('course_id', $curso_id)
                ->where('convocatory_open_valid_start_date', '<=', $fechaini)
                ->where('convocatory_open_valid_end_date', '>=', $fechaini)
                ->first();
        } else {
            $c = $cas->first();
        }

        if ($c) {
            $m = Moneda::find($c->moneda_id);

            //C.Abierta
            $duracion = $curso_weeks;
            $fechafin = $curso->calcularFechaFin($fechaini, $duracion, $c->start_day, $c->end_day);
            $precio = $c->calcularPrecio($fechaini->format('d/m/Y'), $fechafin->format('d/m/Y'), $duracion);

            if ($precio) {
                $m = Moneda::find($precio['moneda_id']);
                $precio = $precio['importe'];
            }

            $total_curso = $precio;

            $moneda = ConfigHelper::default_moneda();
            if (!$m) {
                $m = $moneda;
            }

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }
            $subtotal[$m->currency_name] += $precio;

            if ($m->id == $moneda->id) {
                $total += $precio;
            } else {
                $rate = $m->rate;
                $p = $precio * $rate;
                $total += $p;
            }
        }

        //Alojamiento
        if ($alojamiento) {
            $fechaini = Carbon::createFromFormat('d/m/Y', $alojamiento_fecha);
            $fechafin = $curso->calcularFechaFin($fechaini, $alojamiento_weeks, $alojamiento->start_day, $alojamiento->end_day);

            $fechaini = $alojamiento_fecha;
            $precio = $alojamiento->calcularPrecio($fechaini, $fechafin->format('d/m/Y'), $alojamiento_weeks);

            $importe = $precio['importe'];
            $m = Moneda::find($precio['moneda_id']);

            $total_alojamiento = $importe;

            if (!$m) {
                $m = $moneda;
            }

            if (!isset($subtotal[$m->currency_name])) {
                $subtotal[$m->currency_name] = 0;
            }
            $subtotal[$m->currency_name] += $importe;

            if ($m->id == $moneda->id) {
                $total += $importe;
            } else {
                $rate = $m->rate;
                $p = $importe * $rate;
                $total += $p;
            }
        }

        $total = ceil($total);
        $total_bruto = $total;

        //Descuento Centro y Early
        // $dto_pc = $this->aplicarDescuento();
        $dto_pc = 0;

        $dto = 0;

        //Moneda dto centro (especial)
        $dto_pc_moneda = $dto_pc;
        $m = $moneda;
        if ($curso->moneda_id) {
            $m = Moneda::find($curso->moneda_id);
        }

        if ($m && $m->id != $moneda->id) {
            $rate = $m->rate;
            $dto_pc_moneda = $dto_pc * $rate;
        }
        $total = $total - $dto - $dto_pc_moneda;
        $total_neto = $total;

        //Formato moneda
        $subtotal_txt = [];
        foreach ($subtotal as $stk => $stv) {
            $v = ConfigHelper::parseMoneda($stv, $stk);
            $subtotal_txt[$stk] = $v;
        }

        // $descuento_txt = ConfigHelper::parseMoneda($dto);
        // $descuento_pc_txt = ConfigHelper::parseMoneda($dto_pc,$curso->moneda);

        $total_txt = ConfigHelper::parseMoneda($total);

        // $total_descontar = $total_curso + $total_alojamiento + $total_extras_descontar;

        // $descuentos_solo = $dto - $this->descuento_early_importe;
        // $descuentos_solo_txt = ConfigHelper::parseMoneda($descuentos_solo);

        $ret = [
            'subtotal' => $subtotal, 'subtotal_txt' => $subtotal_txt, 'total' => $total, 'total_txt' => $total_txt,
            'total_bruto' => $total_bruto, 'total_neto' => $total_neto,
            // 'descuento'=>$dto, 'descuento_txt'=> $descuento_txt, 'descuento_solo_txt'=> $descuentos_solo_txt,
            // 'descuento_especial'=>$dto_pc, 'descuento_especial_txt'=> $descuento_pc_txt,
            // 'descuentos'=> $descuentos,
            'total_extras' => $total_extras, //'total_cancelacion'=> $total_cancelacion, 'total_extras_solo'=> $total_extras - $total_cancelacion,
            'total_curso' => $total_curso, //'total_descontar'=> $total_descontar
        ];

        // dd($ret);

        return $ret;
    }

    public function getDescuentoAttribute()
    {
        return $this->getDescuentos()->first();
    }

    public function getDescuentos()
    {
        $curso = $this->curso;

        $descuentos = array();

        foreach ($this->centro->descuentos as $descuento) {
            //Categoria
            if ($descuento->category_id > 0 && !$descuento->subcategory_id && !$descuento->subcategory_det_id) {
                if ($descuento->category_id == $curso->category_id) {
                    array_push($descuentos, $descuento);
                }
            } elseif ($descuento->category_id > 0 && $descuento->subcategory_id > 0 && !$descuento->subcategory_det_id) {
                //Subcategoria

                if ($descuento->category_id == $curso->category_id && $descuento->subcategory_id == $curso->subcategory_id) {
                    array_push($descuentos, $descuento);
                }
            } elseif ($descuento->category_id > 0 && $descuento->subcategory_id > 0 && $descuento->subcategory_det_id > 0) {
                //SubcategoriaDetalle

                if ($descuento->category_id == $curso->category_id && $descuento->subcategory_id == $curso->subcategory_id && $descuento->subcategory_det_id == $curso->subcategory_det_id) {
                    array_push($descuentos, $descuento);
                }
            }
        }
        rsort($descuentos);

        return collect($descuentos);
    }


    public function setContable($force = false)
    {
        //curso->prefijo correlativo (mirar bookings ordenados por fecha_reserva)
        //ccerrada => convocatoria->contable (si no hay prefijo)
        //cmulti => convocatoria->curso->contable

        if ($this->es_directo) {
            return false;
        }

        if (!$force && $this->contable_code != "") {
            return $this->contable_code;
        }

        //Priorizamos el codigo antes que el prefijo
        if ($this->curso->es_convocatoria_cerrada) {
            if ($this->convocatoria->contable) {
                $this->contable_code = $this->convocatoria->contable;
                $this->save();
                return $this->contable_code;
            }
        } elseif ($this->curso->es_convocatoria_multi) {
            if ($this->contable_multi) {
                $this->contable_code = $this->contable_multi;
                $this->save();
                return $this->contable_code;
            } elseif ($this->curso->contable) {
                $this->contable_code = $this->curso->contable;
                $this->save();
                return $this->contable_code;
            }
        }

        $prefijo = $this->curso->prefijo_contable;

        if ($prefijo) {
            //separar por plataforma
            $plataforma = ConfigHelper::config('propietario');
            if ($plataforma) {
                $bookingLast = self::where('plataforma', $plataforma)->where('contable_code', 'LIKE', "$prefijo%")->orderBy('contable_code', 'DESC')->first();
            } else {
                $bookingLast = self::where('contable_code', 'LIKE', "$prefijo%")->orderBy('contable_code', 'DESC')->first();
            }

            $num = 1;
            if ($bookingLast) {
                $num = intval(substr($bookingLast->contable_code, -4));
                $num++;
            }

            $num = str_pad($num, 4, "0", STR_PAD_LEFT);

            $this->contable_code = $prefijo . $num;
            $this->save();
        }

        return $this->contable_code;
    }

    public function setFechasConvo($convo_id = null)
    {
        $cc = $this->convocatoria;

        if (!$cc) {
            $cc = $this->curso->convocatorias->where('id', $convo_id)->first();
        }

        if (!$cc) {
            return false;
        }

        $this->convocatory_close_id = $cc->id;
        $this->convocatory_close_price = $cc->precio;

        $this->course_start_date = $cc->convocatory_close_start_date;
        $this->course_end_date = $cc->convocatory_close_end_date;
        $this->weeks = $cc->convocatory_close_duration_weeks;
        $this->semanas = $this->semanas_unit;
        $this->duracion_fijo = $cc->duracion_fijo;

        $this->accommodation_start_date = $this->course_start_date;
        $this->accommodation_end_date = $this->course_end_date;
        $this->accommodation_weeks = $this->weeks;

        $this->course_price = $cc->precio;
        $this->course_total_amount = $cc->precio;
        $this->course_currency_id = $cc->convocatory_close_currency_id;

        $this->save();

        return true;
    }

    public function setExtras($soloExtras = false)
    {
        //Creamos los Extras obligatorios
        //curso,centro,genericos

        $booking_id = $this->id;

        foreach ($this->curso->extras as $extra) {
            if ($extra->course_extras_required && $extra->tipo_unidad < 2) {
                BookingExtra::add($extra, $booking_id, 0, $this->semanas);
            }
        }

        foreach ($this->curso->centro->extras as $extra) {
            if ($extra->center_extras_required && $extra->tipo_unidad < 2) {
                BookingExtra::add($extra, $booking_id, 1, $this->semanas);
            }
        }

        if ($this->alojamiento) {
            foreach ($this->alojamiento->cuotas as $extra) {
                if ($extra->required && $extra->tipo_unidad < 2) {
                    BookingExtra::add($extra, $booking_id, 3, $this->semanas);
                }
            }
        }

        foreach ($this->curso->extrasGenericos as $extra) {
            if ($extra->generico->generic_required && $extra->generico->tipo_unidad < 2) {
                BookingExtra::add($extra, $booking_id, 2, $this->semanas);
            }
        }

        if ($soloExtras) {
            return;
        }

        //Convocatorias if 1
        if ($this->curso->es_convocatoria_cerrada) {
            $this->vuelo_id = 0;
            $this->save();

            if ($this->curso->convocatoriasCerradas->count() == 1 || $this->convocatoria) {
                $cc = $this->convocatoria ?: $this->curso->convocatoriasCerradas->first();

                $this->convocatory_close_id = $cc->id;
                $this->convocatory_close_price = $cc->precio;

                $this->course_start_date = $cc->convocatory_close_start_date;
                $this->course_end_date = $cc->convocatory_close_end_date;
                $this->weeks = $cc->convocatory_close_duration_weeks;
                $this->duracion_fijo = $cc->duracion_fijo;

                $this->accommodation_start_date = $this->course_start_date;
                $this->accommodation_end_date = $this->course_end_date;
                $this->accommodation_weeks = $this->weeks;

                $this->save();
                $this->semanas = $this->semanas_unit;

                $this->course_price = $cc->precio;
                $this->course_total_amount = $cc->precio;
                $this->course_currency_id = $cc->convocatory_close_currency_id;
                $this->save();

                //Descuento
                $this->aplicarDescuento();

                //Vuelo
                if ($this->curso->vuelos->count() == 1) {
                    $this->vuelo_id = $this->curso->vuelos->first()->id;
                }

                //Alojamiento
                $this->accommodation_id = $cc->alojamiento_id;
                $this->save();
            }
        } elseif ($this->curso->es_convocatoria_multi) {
            if ($this->curso->convocatorias_multi->count() == 1) {
                $cm = $this->curso->convocatorias_multi->first();

                $this->convocatory_multi_id = $cm->id;
                $this->course_currency_id = $cm->moneda_id;
                $this->save();
            } else {
                // $cm = $this->curso->convocatoriasMulti->where.....

                // $this->convocatory_multi_id = $cm->id;
                // $this->course_currency_id = $cm->moneda_id;
                // $this->save();

            }

            //Descuento
            // $this->aplicarDescuento();

        } elseif ($this->curso->es_convocatoria_abierta) {
            if ($this->curso->convocatoriasAbiertas->count() == 1) {
                $ca = $this->curso->convocatoriasAbiertas->first();

                $this->convocatory_open_id = $ca->id;
                $this->save();

                // $precio = $ca->calcularPrecio(0);
                $fechaini = Carbon::parse($this->course_start_date)->format('d/m/Y');
                $fechafin = Carbon::parse($this->course_end_date)->format('d/m/Y');
                $duracion = $this->weeks ? $this->weeks : 0;
                $precio = $ca->calcularPrecio($fechaini, $fechafin, $duracion);

                if ($precio) {
                    $importe = $precio['importe'];

                    $this->convocatory_open_price = $importe;
                    $this->save();

                    $this->course_price = $importe;
                    $this->course_total_amount = $importe;
                    $this->course_currency_id = $precio['moneda_id'];
                    $this->save();
                }

                //Descuento (pendiente??)
                $this->aplicarDescuento();
            } else {
                //pendiente: La de la temporada segun la fecha de inicio !!
            }
        }

        // Alojamiento if 1
        if ($this->curso->alojamientos->count() == 1) {
            $a = $this->curso->alojamientos->first();
            $this->accommodation_id = $a->id;

            $this->accommodation_start_date = $this->course_start_date;
            $this->accommodation_end_date = $this->course_end_date;
            $this->accommodation_weeks = $this->weeks;
            $this->save();

            $precio = $a->calcularPrecio(Carbon::parse($this->course_start_date)->format('d/m/Y'), Carbon::parse($this->course_end_date)->format('d/m/Y'), $this->weeks);
            if($precio)
            {
                $this->accommodation_price = $precio['importe'];
                $this->accommodation_currency_id = $precio['moneda_id'];
                $this->accommodation_total_amount = $this->accommodation_price * $this->accommodation_weeks;
            }

            $this->save();
        }
    }

    public function getEsTerminadoAttribute()
    {
        if ($this->es_presupuesto) {
            return false;
        }

        if ($this->es_directo && $this->status_id > 0) {
            return true;
        }

        return $this->status_id > 0 && $this->fase > 1;
    }

    public function aplicarDescuento()
    {
        $dto = 0;
        $dtoc = 0;
        $dto_pagar = 0;

        $descuentos = $this->getDescuentos();

        if ($this->es_terminado) //Ya no volvemos a calcular
        {
            $dtoc = $this->center_discount_amount;
        } else {
            // $this->center_discount_amount = 0;
            // $this->save();

            if ($descuentos->count() > 0) {
                $d = $descuentos->first();

                $importeWeek = $this->course_total_amount;
                $weeks = $this->weeks;
                if ($weeks > 0) {
                    $importeWeek = $this->course_total_amount / $weeks;
                }

                if ($this->convocatory_close_id) {
                    $weeks = 1;
                }

                if ($d->tipo) {
                    $dtoc = $d->importe;
                } else {
                    // $dtoc = ( ($importeWeek * $weeks) * ($d->porcentaje / 100) );
                    $dtoc = ($this->course_total_amount * ($d->porcentaje / 100));
                }

                //Dto Centro
                // if($descuentos->count()>0)
                // {
                //     $dc = new BookingDescuento;
                //     $dc->booking_id = $this->id;
                //     $dc->fecha = Carbon::now()->format('Y-m-d');
                //     $dc->user_id = 0;
                //     $dc->importe = $dtoc;
                //     $dc->currency_id = $d->centro->currency_id;
                //     $dc->notas = "Descuento Centro";
                //     $dc->save();
                //     BookingLog::addLog($this, "Descuento Centro auto.");
                // }

                $this->center_discount_amount = $dtoc;
                $this->save();
            }

            //Dto Early => BookingDto
            if ($this->descuento_early) {
                if ($this->curso->es_convocatoria_multi) //La multi tiene q recalcular semanas
                {
                    $this->dto_early = null;
                    $this->save();

                    $this->descuentos()->where('notas', "Descuento Early Bird")->delete();
                }

                if (!$this->dto_early) {
                    $d = new BookingDescuento;
                    $d->booking_id = $this->id;
                    $d->fecha = Carbon::now()->format('Y-m-d');
                    $d->user_id = 0;
                    $d->importe = $this->descuento_early_importe;
                    $d->currency_id = $this->descuento_early->moneda->id;
                    $d->notas = "Descuento Early Bird";
                    $d->save();

                    BookingLog::addLog($this, "Descuento Early Bird auto.");

                    $this->dto_early = $this->descuento_early->id;
                    $this->save();
                }
            }
        }

        //BookingDescuentos
        $dto = 0;
        // foreach($this->descuentos as $descuento)
        // {
        //     $dto += $descuento->importe;
        // }

        return $dto + $dtoc;
    }


    public function deleteExtras()
    {
        $e = BookingExtra::where('booking_id', $this->id)->delete();

        $this->convocatory_close_id = 0;
        $this->convocatory_close_price = 0;

        $this->convocatory_open_id = 0;
        $this->convocatory_open_price = 0;

        $this->accommodation_id = 0;
        $this->accommodation_price = 0;
        $this->accommodation_start_date = null;
        $this->accommodation_end_date = null;
        $this->accommodation_total_amount = 0;
        $this->accommodation_weeks = 0;

        $this->save();
    }

    public function resetExtras()
    {
        BookingExtra::where('booking_id', $this->id)->delete();

        $this->semanas = $this->semanas_unit;
        $this->save();

        $this->setExtras(true);
    }

    public function updateExtras($reset = false)
    {
        //Revisamos si hay extras que han puesto como obligatorios

        $booking_id = $this->id;

        if ($reset) {
            if (!$this->es_terminado) {
                BookingExtra::where('booking_id', $booking_id)->where('required', true)->delete();
            }
        }

        $tipo = 0;
        foreach ($this->curso->extras as $extra) {
            if ($extra->requerido && $extra->tipo_unidad < 2) {
                $yaExtra = $this->extras_curso->where('tipo', $tipo)->where('extra_id', $extra->id)->first();

                if (!$yaExtra) {
                    BookingExtra::add($extra, $booking_id, $tipo, $this->semanas);
                }
            }
        }

        $tipo = 1;
        foreach ($this->curso->centro->extras as $extra) {
            if ($extra->requerido && $extra->tipo_unidad < 2) {
                $yaExtra = $this->extras_centro->where('tipo', $tipo)->where('extra_id', $extra->id)->first();

                if (!$yaExtra) {
                    BookingExtra::add($extra, $booking_id, $tipo, $this->semanas);
                }
            }
        }

        $tipo = 3;
        if ($this->alojamiento) {
            foreach ($this->alojamiento->cuotas as $extra) {
                if ($extra->required && $extra->tipo_unidad < 2) {
                    $yaExtra = $this->extras_alojamiento->where('tipo', $tipo)->where('extra_id', $extra->id)->first();

                    if (!$yaExtra) {
                        BookingExtra::add($extra, $booking_id, $tipo, $this->semanas);
                    }
                }
            }
        }

        $tipo = 2;
        foreach ($this->curso->extrasGenericos as $extra) {
            if ($extra->generico->generic_required && $extra->generico->tipo_unidad < 2) {
                $yaExtra = $this->extras_generico->where('tipo', $tipo)->where('extra_id', $extra->id)->first();

                if (!$yaExtra) {
                    BookingExtra::add($extra, $booking_id, $tipo, $this->semanas);
                }
            }
        }
    }

    public function extra_activo($extra_id, $tipo)
    {
        $e = BookingExtra::where('booking_id', $this->id)->where('tipo', $tipo)->where('extra_id', $extra_id)->first();

        return $e ? true : false;
    }

    public function extra_unidades($extra_id, $tipo)
    {
        $e = BookingExtra::where('booking_id', $this->id)->where('tipo', $tipo)->where('extra_id', $extra_id)->first();

        if ($e) {
            if ($e->extra->tipo_unidad == 1) {
                return $e ? $e->unidades : 0;
            }

            return 1;
        }

        return 0;
    }

    public function extra_class($extra_id, $tipo)
    {
        $e = BookingExtra::where('booking_id', $this->id)->where('tipo', $tipo)->where('extra_id', $extra_id)->first();

        if (!$e) {
            return "fa fa-circle-thin";
        }

        return $e->unidades ? "fa fa-check-circle-o" : "fa fa-circle-thin";
    }

    public function extra_class_curso($extra_id)
    {
        return $this->extra_class($extra_id, 0);
    }

    public function extra_class_centro($extra_id)
    {
        return $this->extra_class($extra_id, 1);
    }

    public function extra_class_alojamiento($extra_id)
    {
        return $this->extra_class($extra_id, 3);
    }

    public function extra_class_generico($extra_id)
    {
        return $this->extra_class($extra_id, 2);
    }

    public function extra_class_otro($extra_id)
    {
        // return $this->extra_class($extra_id,4);
        return "fa fa-check-circle-o";
    }


    public function alojamiento_class($alojamiento_id, $cc_alojamiento_id = 0)
    {
        $class = "";

        if ($alojamiento_id) {
            if ($this->curso->alojamientos->count() == 1) {
                $class = " obligatorio";
            }

            if ($alojamiento_id == $cc_alojamiento_id) {
                $class = " obligatorio";
            }
        }

        if ($this->accommodation_id == $alojamiento_id)
            return "fa fa-check-circle-o" . $class;

        return "fa fa-circle-thin" . $class;
    }

    public function vuelo_class($vuelo_id)
    {
        $class = "";
        if ($this->curso->vuelos->count() == 1) {
            $class = " obligatorio";
        }

        if ($this->vuelo_id == $vuelo_id) {
            return "fa fa-check-circle-o" . $class;
        }

        return "fa fa-circle-thin" . $class;
    }

    public function getAlojamientoMonedaAttribute()
    {
        $m = Moneda::find($this->accommodation_currency_id);

        return $m ? $m->name : Session::get('vcn.moneda');
    }

    public function getAlojamientoIdAttribute()
    {
        return $this->accommodation_id;
    }

    public function getCursoMonedaAttribute()
    {
        $m = Moneda::find($this->course_currency_id);

        return $m ? $m->name : Session::get('vcn.moneda');
    }

    public function getCursoStartDateAttribute()
    {
        return Carbon::parse($this->course_start_date)->format('d/m/Y');
    }

    public function getCursoEndDateAttribute()
    {
        return Carbon::parse($this->course_end_date)->format('d/m/Y');
    }

    public function getCursoFechasAttribute()
    {
        return "Desde " . Carbon::parse($this->course_end_date)->format('d/m/Y') . " hasta " . Carbon::parse($this->course_end_date)->format('d/m/Y');
    }

    public function getAlojamientoStartDateAttribute()
    {
        return Carbon::parse($this->accommodation_start_date)->format('d/m/Y');
    }

    public function getAlojamientoEndDateAttribute()
    {
        return Carbon::parse($this->accommodation_end_date)->format('d/m/Y');
    }

    public function archivar($motivo, $nota = null)
    {
        // $this->setStatus(220, "Archivar");
        $this->archivar_fecha = Carbon::now();
        $this->archivar_motivo = $motivo;
        $this->archivar_motivo_nota = $nota;
        $this->save();

        BookingLog::addLog($this, "Archivar: $motivo [$nota]");
    }

    public function getDuracionAttribute()
    {
        return $this->weeks;
    }

    public function getDuracionNameAttribute()
    {
        $n = "Semanas";

        if ($this->convocatory_close_id) {
            return ConfigHelper::getPrecioDuracionUnit($this->duracion_fijo);
        }

        if ($this->convocatoria) {
            $n = $this->convocatoria->duracion_name;
        }

        return $n;
    }

    public function calcularFechaFin($fechaini, $duracion, $diaini = 0, $diafin = 0, $es_curso = true)
    {
        $fecha = clone $fechaini;

        $dini = $diaini;
        $dfin = $diafin;

        if ($diaini == 7) {
            $diaini = 0;
        };
        if ($diafin == 7) {
            $diafin = 0;
        };

        // $fecha = Carbon::parse($fecha);
        switch ($this->curso->duracion_unit) {
            case 1: //Semanas
                {
                    $ret = $fecha->addWeeks($duracion);
                }
                break;

            case 2: //Meses
                {
                    $ret = $fecha->addMonths($duracion);
                }
                break;

            case 3: //Trimestres
                {
                    $ret = $fecha->addMonths($duracion * 3);
                }
                break;

            case 4: //Semestres
                {
                    $ret = $fecha->addMonths($duracion * 6);
                }
                break;

            case 5: //Meses
                {
                    $ret = $fecha->addYears($duracion);
                }
                break;

            default: //Semanas
                {
                    $ret = $fecha->addWeeks($duracion);
                }
                break;
        }

        if ($diaini != $diafin) {
            if ($es_curso) {
                $ret = $ret->subWeek();
            } else if ($dini > $dfin)  //|| $dini==0
            {
                //por ejemplo inicio sábado(6) y fin domingo(0) no debería restar

                $difd = abs($diaini - $dini); //entre dia inicio y fin hay 1 semana casi.
                if ($difd > 6) {
                    $ret = $ret->subWeek();
                }
            }

            //diafin
            while ($ret->dayOfWeek != $diafin) {
                $ret->addDay();
            }
        }

        return $ret;
    }

    public function presupuesto()
    {
        if (!$this->es_presupuesto) {
            return false;
        }

        $dir = storage_path("files/viajeros/" . $this->viajero->id . "/");
        $name = "presupuesto_" . Carbon::now()->format('Y-m-d') . "_" . $this->id . "_" . str_slug($this->curso->name) . ".pdf";
        $file = $dir . $name;

        //viajeroarchivo
        $va = new ViajeroArchivo;
        $va->viajero_id = $this->viajero->id;
        $va->booking_id = 0; //$this->id;
        $va->user_id = auth() ? auth()->user()->id : 0;
        $va->visible = true;
        $va->doc = $name;
        $va->fecha = Carbon::now(); //->format('Y-m-d');
        $va->save();
        $va->update(['visible' => true]);

        $lang = $this->idioma_contacto;

        // return view('pdf.booking_presupuesto', ['ficha'=> $this]);

        $p = $this->plataforma;
        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView('pdf.booking_presupuesto', ['ficha' => $this]);

        $pdf->setOption('margin-top', 30);
        $pdf->setOption('margin-right', 0);
        $pdf->setOption('margin-bottom', 30);
        $pdf->setOption('margin-left', 0);
        $pdf->setOption('no-print-media-type', false);
        $pdf->setOption('footer-spacing', 0);
        $pdf->setOption('header-font-size', 9);
        $pdf->setOption('header-spacing', 0);

        $header = 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'header.html';
        $footer = 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'footer.html';
        $pdf->setOption('header-html', $header);
        $pdf->setOption('footer-html', $footer);


        //Informe
        \VCN\Models\Informes\Presupuesto::add($this);

        //Borramos el booking
        $this->delete();

        return $pdf->save($file);
    }

    public function directo()
    {
        $this->es_directo = 1;
        $this->save();

        $this->setStatus(ConfigHelper::config('booking_status_prebooking'));
    }

    public function pdf($download = false, $es_job = false, $force = false)
    {
        // if( App::environment('local') )
        // {
        //     return $this->pdf_generar($download, true);
        // }

        if ($download) {
            return $this->pdf_generar($download);
        }

        if ($es_job) {
            $job = new \VCN\Jobs\JobPdfBooking($this, $force);
            return dispatch($job);
        }

        return $this->pdf_generar();
    }

    public function pdf_monitor()
    {
        // app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path()."/tmp");
        // $dir = storage_path("files/bookings/". $this->viajero->id . "/");
        $p = $this->plataforma ?: 1;

        $name = "booking-info_" . $this->id . "-" . Carbon::now()->format('Y-m-d_H-i-s') . ".pdf";
        // $file = $dir . $name;
        $lang = $this->idioma_contacto;

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView('manage.bookings.pdf_monitor', ['ficha' => $this]);

        $pdf->setOption('margin-top', 30);
        $pdf->setOption('margin-right', 0);
        $pdf->setOption('margin-bottom', 30);
        $pdf->setOption('margin-left', 0);
        $pdf->setOption('no-print-media-type', false);
        $pdf->setOption('footer-spacing', 0);
        $pdf->setOption('header-font-size', 9);
        $pdf->setOption('header-spacing', 0);
        $pdf->setOption('header-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'header.html');
        $pdf->setOption('footer-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'footer.html');

        return $pdf->download($name);
    }

    public function pdf_proveedor()
    {
        $p = $this->plataforma ?: 1;
        $lang = $this->idioma_contacto;
        
        $dir = storage_path("files/bookings/" . $this->viajero->id . "/");

        //PDF para proveedor (Categoría Adultos o Jóvenes>Individuales): YYYYMMDD_Nombre_Apellido_Curso
        $filep = $dir . Carbon::now()->format('Ymd_H-i-s') . "_" . str_slug($this->viajero->full_name) . "_" . str_slug($this->curso->name) . "_" . $this->id . ".pdf";

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView('manage.bookings.pdf_proveedor', ['ficha' => $this]);

        $pdf->setOption('margin-top', 30);
        $pdf->setOption('margin-right', 0);
        $pdf->setOption('margin-bottom', 30);
        $pdf->setOption('margin-left', 0);
        $pdf->setOption('no-print-media-type', false);
        $pdf->setOption('footer-spacing', 0);
        $pdf->setOption('header-font-size', 9);
        $pdf->setOption('header-spacing', 0);
        //$pdf->setOption('javascript-delay',20000);
        $pdf->setOption('header-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'header.html');
        $pdf->setOption('footer-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'footer.html');

        $pdf->save($filep);
    }

    public function pdf_generar($download = false, $force = false)
    {
        if ($this->datos_congelados && !$force) {
            // if($download)
            {
                Session::flash('mensaje', "Datos congelados. No puede generar pdf nuevo.");
                return false; //redirect()->back();
            }
            return false;
        }

        if ($this->fase <= 2) {
            return;
        }

        if (!$download) {
            //Cada vez que haya pdf hacemos nota pago. Este update genera log sin detalle
            if ($this->es_pdf) {
                $this->notaPago35update();
                // $this->notaPago35_mail();
            }
        }

        $firma = null;
        if (!$this->es_pdf || $force) {
            $firma = "pendiente";
        }

        $this->es_pdf = true;
        $this->save();

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $dir = storage_path("files/bookings/" . $this->viajero->id . "/");

        $name = "booking_" . $this->id . "-" . Carbon::now()->format('Y-m-d_H-i-s') . ".pdf";
        $file = $dir . $name;
        $lang = $this->idioma_contacto;

        $bCategoria = false;
        if (Categoria::where('name', 'Pathways')->first()) {
            $bCategoria = ($this->curso->category_id == Categoria::where('name', 'Pathways')->first()->id);
        }

        if (Categoria::where('name', 'Adultos')->first()) {
            $bCategoria = ($this->curso->category_id == Categoria::where('name', 'Adultos')->first()->id);
        }

        if (Categoria::where('name', 'Jóvenes')->first()) {
            $bSubCategoria = ($this->curso->category_id == Categoria::where('name', 'Jóvenes')->first()->id);
            if ($bSubCategoria) {
                $bCategoria = ($this->curso->subcategory_id == Subcategoria::where('name', 'Individuales')->first()->id);
            }
        }

        $p = $this->plataforma ?: 1;

        if ($this->curso->es_convocatoria_abierta || $this->convocatoria->convocatory_semiopen || $bCategoria)
        {
            $this->pdf_proveedor();
        }

        app()->setLocale($this->idioma_contacto);
        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView('manage.bookings.pdf', ['ficha' => $this]);

        $pdf->setOption('margin-top', 30);
        $pdf->setOption('margin-right', 0);
        $pdf->setOption('margin-bottom', 30);
        $pdf->setOption('margin-left', 0);
        $pdf->setOption('no-print-media-type', false);
        $pdf->setOption('footer-spacing', 0);
        $pdf->setOption('header-font-size', 9);
        $pdf->setOption('header-spacing', 0);
        //$pdf->setOption('javascript-delay',20000);
        $pdf->setOption('header-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'header.html');
        $pdf->setOption('footer-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'footer.html');

        $pdfbooking = $pdf->save($file);
        $pdf = PdfMerger::init();

        $pdf->addPDF($file);

        //pdf_condiciones
        $condiciones = $this->pdf_condiciones;
        if ($condiciones) {
            $pdf->addPDF($condiciones);
        }

        app()->setLocale("es");
        $pdf->merge();

        if ($download) {
            return $pdf->save($name, 'download');
        } else {

            $doc = ViajeroArchivo::addBooking($this, $name, false, $firma);
            BookingLog::addLog($this, "PDF generado ($p)");

            //Backend pre-reserva no se envía, pero comprobante si
            if ($this->es_online || $this->es_prebooking || $this->es_overbooking || $force) {

                if (!is_null($firma) && !$force) {
                    MailHelper::mailBookingFirma($this);
                    BookingLog::addLog($this, "Solicitud firma (pdf): $doc->doc");
                }
            } else {
                BookingLog::addLog($this, "Solicitud firma NO ($this->status_name)");
            }

            return $pdf->save($file, 'file');
        }
    }

    public function getEsViajeroAttribute()
    {
        $user = auth()->user();
        if ($user && $this->viajero && $this->viajero->user_id == $user->id) {
            return true;
        }

        return false;
    }

    public function getEsTutor1Attribute()
    {
        $user = auth()->user();
        if ($user)
        {
            if($this->viajero->tutor1 && $this->viajero->tutor1->user_id == $user->id)
            {
                return true;
            }            
        }

        return false;
    }

    public function getEsTutor2Attribute()
    {
        $user = auth()->user();
        if ($user)
        {
            if($this->viajero->tutor2 && $this->viajero->tutor2->user_id == $user->id)
            {
                return true;
            }            
        }

        return false;
    }

    public function getFirmanteAttribute()
    {
        $firmante = null;

        if ($this->es_viajero) {
            $firmante = "viajero";
        }

        if ($this->es_menor) {
            if ($this->es_tutor1) {
                $firmante = "tutor1";
            } elseif ($this->es_tutor2) {
                $firmante = "tutor2";
            }
        }

        return $firmante;
    }

    public function getArchivoFirma($file)
    {
        $ret = ViajeroArchivo::where('booking_id', $this->id)->where('doc', $file)->first();

        return $ret;
    }

    public function getArchivoFirmaPendiente()
    {
        $firmante = $this->firmante;

        $ret = ViajeroArchivo::where('booking_id', $this->id)->where('firmas', '"pendiente"')->first();

        if ($ret) {
            return $ret;
        } else {
            $ret = null;

            // visible==1 pq ya ha firmado alguno y solo queremos el último
            $fichero = ViajeroArchivo::where('booking_id', $this->id)->where('notas', 'Booking-PDF')
                ->where('visible', 1)
                ->where('firmas', '<>', '"pendiente"')->orderBy('id', 'DESC')->first();

            if ($fichero && $fichero->firmas)
            {
                $firmado = false;
                $firmas = $fichero->firmas;
                foreach ($firmas as $firma) {
                    if ($firma['firmante'] == $firmante) {
                        return null;
                    }
                }

                return $fichero;
            }
        }

        return $ret;
    }

    public function getArchivosFirmaCountAttribute()
    {
        $ret = ViajeroArchivo::where('booking_id', $this->id)->whereNotNull('firmas')->count();

        return $ret;
    }

    public function setFirmaPendiente(ViajeroArchivo $doc)
    {
        foreach ($this->pdfs as $p) {
            if (!$p->es_firmado) {
                $p->firmas = null;
            }

            $p->visible = false; //el resto se ocultan.
            $p->save();
        }

        $doc->firmas = 'pendiente';
        $doc->save();
    }

    public function getDescuentoEarlyAttribute()
    {
        //Descuento Early
        $dtoEarly = null;

        if ($this->es_terminado) {
            return $this->descuentos->where('notas', 'Descuento Early Bird')->first();
        }

        if (!$this->convocatoria) {
            return $dtoEarly;
        }

        if ($this->convocatoria->dto_early) {
            //Se busca el id y además que tenga validez

            $hoy = Carbon::now();
            $descuento = DescuentoEarly::where('id', $this->convocatoria->dto_early)->where('desde', '<=', $hoy)->where('hasta', '>=', $hoy)->orderBy('hasta', 'DESC')->first();
            return $descuento;
        }

        return $dtoEarly;
    }

    public function getDescuentoEarlyImporteAttribute()
    {
        $importe = $this->descuento_early ? $this->descuento_early->importe : 0;

        if ($this->curso->es_convocatoria_multi) {
            $importe = $importe * $this->weeks;
        }

        return $importe;
    }

    public function setMultiEspecialidades($desde, $semanas)
    {
        BookingMulti::where('booking_id', $this->id)->delete();

        $cms = ConvocatoriaMultiSemana::find($desde);

        $n = 1;
        foreach (ConvocatoriaMultiSemana::where('convocatory_id', $this->convocatory_multi_id)->where('semana', '>=', $cms->semana)->orderBy('semana')->get() as $s) {
            $e = $this->convocatoria->especialidadesBySemana($s->semana);

            $m = new BookingMulti;
            $m->booking_id = $this->id;
            $m->semana_id = $s->id;
            $m->especialidad_id = 0; //($e->count()>0)?$e->first()->id:0;
            $m->n = $n;
            $m->precio = 0; //($e->count()>0)?$e->first()->precio:0;
            $m->save();

            $n++;

            if ($n > $semanas) {
                break;
            }
        }
    }

    public function getEdadAttribute()
    {
        $fsalida = Carbon::parse($this->course_start_date);
        $fnac = Carbon::parse($this->viajero->fechanac);

        return $fnac->diffInYears($fsalida);

        // return $this->viajero->edad;
    }

    public function getSexoAttribute()
    {
        return $this->viajero->sexo;
    }

    public function getSemanasUnitAttribute()
    {
        /* $unit = $this->convocatoria ? $this->convocatoria->duracion_fijo : null;

        if(is_null($unit))
        {
            $unit = $this->curso->duracion_unit ?: 1;
        } */

        $unit = $this->duracion_fijo;
        $semanas = $this->weeks;
        switch ($unit) {
            case 0: //dias
                {
                    $semanas = 1;
                }
                break;

            case 2: //meses
                {
                    $semanas = $semanas * 4;
                }
                break;

            case 3: //trimmestres
                {
                    $semanas = $semanas * 12;
                }
                break;

            case 4: //semestres
                {
                    $semanas = $semanas * 24;
                }
                break;

            case 5: //años
                {
                    $semanas = $semanas * 40;
                }
                break;
        }

        return $semanas;
    }

    public function mail()
    {
        if ($this->es_directo) {
            return false;
        }

        if ($this->mail_confirmacion) {
            //enviar mail a viajero, tutor1 y tutor2
            MailHelper::mailBooking($this->id);
        }
    }

    public function mailAviso()
    {
        MailHelper::mailBookingAviso($this->id);
        return;

        /*if($this->es_convocatoria_abierta)
        {
            MailHelper::mailBookingAviso($this->id);
        }
        elseif($this->convocatoria->convocatory_semiopen)
        {
            MailHelper::mailBookingAviso($this->id);
        }
        else
        {
            //x Categoria
            $cat1 = 1; //Adultos
            $cat2 = 10; //Jóvenes > Individuales
            $cat3 = 7;
            if($this->curso->category_id == $cat1 || $this->curso->subcategory_id == $cat2 || $this->curso->category_id == $cat3 )
            {
                MailHelper::mailBookingAviso($this->id);
            }
        }*/
    }

    public function mailFamilia()
    {
        $asunto = "Actualización datos de Familia";
        $idioma = $this->viajero->idioma_contacto;
        if ($idioma == "ca") {
            $asunto = "Actualització de dades de família amfitriona";
        }

        MailHelper::mailTemplate($this->id, "$idioma.booking_familia", $asunto);

        Session::flash('mensaje-ok', "Emails de aviso info Famlia enviados.");
    }

    public function mailSchool()
    {
        $asunto = "Actualización datos de School";
        $idioma = $this->viajero->idioma_contacto;
        if ($idioma == "ca") {
            $asunto = "Actualització de dades de School";
        }

        MailHelper::mailTemplate($this->id, "$idioma.booking_school", $asunto);

        Session::flash('mensaje-ok', "Emails de aviso info School enviados.");
    }

    // public function tareaAuto()
    // {
    //     //tarea recordatorio:
    //     /*Fecha: Fecha inicio curso + 2 días / Hora: 12:00
    //     Asignado a: user propietario del booking
    //     Tipo: Teléfono
    //     Resumen: Llamar para confirmar que todo bien. Si no contesta, enviar email. */

    //     $t = new ViajeroTarea;
    //     $t->viajero_id = $this->viajero->id;
    //     $t->fecha = Carbon::parse($this->course_start_date)->addDays(2)->format('Y-m-d 12:00:00');
    //     $t->tipo = "Teléfono";
    //     $t->notas = "Llamar para confirmar que todo bien. Si no contesta, enviar email.";
    //     $t->save();
    // }

    public function tareaAutoEliminar()
    {
        $avisos = Aviso::where('tipo', 0)->where('activo', 1)->where('cuando_tipo', 'tarea')->where('cuando_trigger', $this->status_id)->get();

        foreach ($avisos as $aviso) {
            $fecha = Carbon::parse($this->course_start_date)->addDays($aviso->cuando_dia)->format('Y-m-d ' . $aviso->cuando_hora);
            ViajeroTarea::where('viajero_id', $this->viajero->id)->where('fecha', $fecha)->delete();
        }
    }

    public function decidiendo()
    {
        if ($this->es_directo) {
            return;
        }

        if ($this->es_presupuesto) {
            return;
        }

        $booking_id = $this->id;
        $bookings = $this->viajero->bookings->sortByDesc('course_start_date');
        $bookings = $bookings->filter(function ($item) use ($booking_id) {
            return $item->id != $booking_id;
        });

        // if($bookings->count()>0)
        // {
        //     if($this->viajero->booking_id == $this->id) //borra el asignado
        //     {
        //         $booking_id = $bookings->first()->id;

        //         if(!$this->es_presupuesto)
        //         {
        //             $this->viajero->status_id = ConfigHelper::config('solicitud_status_decidiendo');
        //         }
        //         $this->viajero->booking_id = $booking_id;
        //         $this->viajero->booking_status_id = Self::find($booking_id)->status_id;
        //         $this->viajero->save();

        //         ViajeroLog::addLog($this->viajero, "Booking cancelado => Booking [$this->id]=> Booking [". $booking_id ."]");
        //     }
        //     else
        //     {
        //         ViajeroLog::addLog($this->viajero, "Booking cancelado => Booking [$this->id] (otro)");
        //     }
        // }
        // else
        {
            // $solicitud = $this->viajero->solicitudes->sortByDesc('id')->first();
            $solicitud = $this->solicitud;

            if(!$solicitud)
            {
                $solicitud = $this->viajero->solicitudes->sortByDesc('id')->first();
            }
            
            if( $solicitud )
            {
                $solicitud->setStatus(ConfigHelper::config('solicitud_status_decidiendo'));
                $this->viajero->solicitud_id = $solicitud->id;
            }

            $this->viajero->booking_id = 0;
            $this->viajero->booking_status_id = 0;
            $this->viajero->es_cliente = false;
            $this->viajero->save();

            $this->viajero->setStatus(ConfigHelper::config('solicitud_status_decidiendo'));

            ViajeroLog::addLog($this->viajero, "Booking cancelado => Decidiendo [$this->id]=> Solicitud [" . $this->viajero->solicitud_id . "]");
        }

        // if($this->es_presupuesto && !$this->viajero->es_booking)
        // {
        //     $this->viajero->booking_id = 0;
        //     $this->viajero->booking_status_id = 0;
        //     $this->viajero->es_cliente = false;
        //     $this->viajero->save();
        // }
    }

    public function setCaducado()
    {
        $data = [
            'fecha_creacion' => $this->created_at->format('d/m/Y'),
            'fecha_prereserva' => $this->fecha_prereserva->format('d/m/Y'),
            'fecha_caduca' => Carbon::now()->format('d/m/Y'),
            'curso' => $this->curso->name,
            'convocatoria' => $this->convocatoria ? $this->convocatoria->name : "-",
        ];

        $txt = "Booking caducado => Eliminar [$this->id]";
        if ($this->es_online) {
            $txt = "Booking [ONLINE] caducado"; // => Eliminar [$this->id]";
            BookingLog::addLog($this, $txt);

            $this->setStatus(ConfigHelper::config('booking_status_cancelado'));
            $this->archivar('AUTO', 'CADUCADO');
        }

        ViajeroLog::addLogData($this->viajero, $txt, $data);

        if (!$this->es_online) {
            $this->delete();
        }
    }

    public function getAviso1Attribute()
    {
        if ($this->status_id != ConfigHelper::config('booking_status_prereserva')) {
            return null;
        }

        //Aviso viajero y tutores
        $aviso = $this->fecha_prereserva;
        if (!$aviso) {
            return null;
        }

        $iCaducidad = $this->es_online ? 2 : 3;
        $iAviso = 0;
        while ($iAviso < $iCaducidad) {
            $aviso->addDay();

            if (!$aviso->isWeekend()) {
                $iAviso++;
            }
        }

        return $aviso;
    }

    public function getAviso2Attribute()
    {
        if ($this->status_id != ConfigHelper::config('booking_status_prereserva')) {
            return null;
        }

        //Aviso viajero y tutores
        $aviso = $this->fecha_prereserva;
        if (!$aviso) {
            return null;
        }

        $iCaducidad = $this->es_online ? 2 : 4;
        $iAviso = 0;
        while ($iAviso < $iCaducidad) {
            $aviso->addDay();

            if (!$aviso->isWeekend()) {
                $iAviso++;
            }
        }

        return $aviso;
    }

    public function getCaducaAttribute()
    {
        if ($this->status_id != ConfigHelper::config('booking_status_prereserva')) {
            return null;
        }

        //Aviso viajero y tutores
        $caduca = $this->fecha_prereserva;
        if (!$caduca) {
            return null;
        }

        $iCaducidad = $this->es_online ? 4 : 5;

        if ($this->es_corporativo) {
            $corp = $this->corporativo_info;
            if ($corp && isset($corp['caducidad']) && $corp['caducidad']) {
                $iCaducidad = ((int) $corp['caducidad']) ?: $iCaducidad;
            }
        }

        $iCaduca = 0;
        while ($iCaduca < $iCaducidad) {
            $caduca->addDay();

            if (!$caduca->isWeekend()) {
                $iCaduca++;
            }
        }

        return $caduca;
    }

    public function getEsMenorAttribute()
    {
        if ($this->curso->es_menor) {
            return true;
        }

        if ($this->viajero->es_menor) {
            return true;
        }

        return false;
    }

    public function getEsMultiAttribute()
    {
        return $this->convocatory_multi_id > 0;
    }

    public function getEsConvocatoriaMultiAttribute()
    {
        return $this->convocatory_multi_id > 0;
    }

    public function getEsConvocatoriaCerradaAttribute()
    {
        return $this->convocatory_close_id > 0;
    }

    public function getEsConvocatoriaAbiertaAttribute()
    {
        return $this->convocatory_open_id > 0;
    }

    public function getEsVigenteAttribute()
    {
        if($this->status_id == ConfigHelper::config('booking_status_cancelado'))
        {
            return false;
        }

        if($this->status_id == ConfigHelper::config('booking_status_refund'))
        {
            return false;
        }

        return Carbon::parse($this->course_end_date)->addDays(45)->diffInDays(Carbon::now(), false) < 0;
    }

    public function getDatosCongeladosAttribute()
    {
        return Carbon::parse($this->course_end_date)->addDay()->diffInDays(Carbon::now(), false) > 0;
    }

    public function getEsCampamentoCicAttribute()
    {
        return $this->curso->category_id == 6;
    }

    public function getExtrasNameAttribute()
    {
        $ret = "";

        foreach ($this->extras_centro as $e) {
            $ret .= "$e->extra_name : $e->unidades <br>";
        }

        foreach ($this->extras_curso as $e) {
            $ret .= "$e->extra_name : $e->unidades <br>";
        }

        return $ret;
    }

    public function getExtrasNamePdfAttribute()
    {
        $ret = "";

        foreach ($this->extras_centro as $e) {
            $ret .= "$e->extra_name : $e->unidades \r\n";
        }

        foreach ($this->extras_curso as $e) {
            $ret .= "$e->extra_name : $e->unidades \r\n";
        }

        return $ret;
    }

    public static function listadoFiltros($valores, $force = false, $valoresNot = null)
    {
        $bookings = null;
        $user = auth()->user();

        $f = isset($valores['status']) ? $valores['status'] : 0;
        if ($f) {
            if (is_array($f)) {
                $st = \VCN\Models\Bookings\Status::whereIn('id', $f)->pluck('id')->toArray();
            } else {
                $st = \VCN\Models\Bookings\Status::where('id', $f)->pluck('id')->toArray();
            }
        } else {
            $st = \VCN\Models\Bookings\Status::where('plazas', 1)->pluck('id')->toArray();
            $sto = [ConfigHelper::config('booking_status_overbooking')];

            $st = array_merge($st, $sto);
        }

        //Fecha created_at
        $bFechas = false;
        if (isset($valores['desdec']) && isset($valores['hastac'])) {
            if ($valores['desdec'] && $valores['hastac']) {
                $fini = Carbon::createFromFormat('d/m/Y', $valores['desdec'])->format('Y-m-d');
                $ffin = Carbon::createFromFormat('d/m/Y', $valores['hastac'])->format('Y-m-d');

                $bookings = Booking::whereIn('status_id', $st)->where('created_at', '>=', $fini)->where('created_at', '<=', $ffin);

                $bFechas = true;
            } else {
                $bookings = Booking::whereIn('status_id', $st);
            }
        } else {
            //full-admin
            $bookings = Booking::whereIn('status_id', $st);
        }

        //Fechas
        if (!$bFechas) {
            //Fechas PRIMER PAGO
            if (isset($valores['desde']) && isset($valores['hasta'])) {
                if ($valores['desde'] && $valores['hasta']) {
                    $fini = Carbon::createFromFormat('d/m/Y', $valores['desde'])->format('Y-m-d');
                    $ffin = Carbon::createFromFormat('d/m/Y', $valores['hasta'])->format('Y-m-d');

                    $bookings = Booking::whereIn('status_id', $st)->where('fecha_pago1', '>=', $fini)->where('fecha_pago1', '<=', $ffin);

                    $bFechas = true;
                } else {
                    $bookings = Booking::whereIn('status_id', $st);
                }
            } else {
                //full-admin
                $bookings = Booking::whereIn('status_id', $st);
            }
        }

        if (!$bFechas) {
            //Fechas INICIO BOOKING
            if (isset($valores['desdes']) && isset($valores['hastas'])) {
                if ($valores['desdes'] && $valores['hastas']) {
                    $fini = Carbon::createFromFormat('d/m/Y', $valores['desdes'])->format('Y-m-d');
                    $ffin = Carbon::createFromFormat('d/m/Y', $valores['hastas'])->format('Y-m-d');

                    // $bookings = Booking::whereIn('status_id',$st)->where('course_start_date','>=',$fini)->where('course_end_date','<=',$ffin);
                    $bookings = Booking::whereIn('status_id', $st)->where('course_start_date', '>=', $fini)->where('course_start_date', '<=', $ffin);

                    $bFechas = true;
                } else {
                    $bookings = Booking::whereIn('status_id', $st);
                }
            } else {
                //full-admin
                $bookings = Booking::whereIn('status_id', $st);
            }
        }

        //TipoConvocatoria sino la saca del curso
        $tc = 0;
        $f = isset($valores['tipoc']) ? $valores['tipoc'] : $tc;
        $fc = isset($valores['cursos']) ? $valores['cursos'] : 0;
        if(!$f && $fc)
        {
            if (is_array($fc) && count($fc) == 1) {
                if ($fc[0] == 0) {
                    $fc = null;
                }
            }
            if ($fc)
            {
                if (is_array($f))
                {
                    $fc = $f[0];
                }

                $curso = Curso::find($fc);
                $tc = $curso->convocatoria_tipo_num ?? 0;
            }
        }

        $f = isset($valores['tipoc']) ? $valores['tipoc'] : $tc;
        $f = $f ?: $tc;
        if ($f)
        {
            $fc = isset($valores['convocatorias']) ? $valores['convocatorias'] : 0;
            if (!is_array($fc)) {
                $fc = isset($valores['convocatorias']) ? explode(',', $valores['convocatorias']) : 0;
            }

            switch ($f) {
                case 1: {
                        $ftipoc = 'convocatory_close_id';
                        if (!$fc) {
                            $c = Cerrada::where('convocatory_semiopen', 0)->pluck('id')->toArray();
                        }
                    }
                    break;

                case 2: {
                        $ftipoc = 'convocatory_close_id';
                        if (!$fc) {
                            $c = Cerrada::where('convocatory_semiopen', 1)->pluck('id')->toArray();
                        }
                    }
                    break;

                case 3: {
                        $ftipoc = 'convocatory_open_id';
                        if (!$fc) {
                            $c = Abierta::pluck('id')->toArray();
                        }
                    }
                    break;

                case 4: {
                        $ftipoc = 'convocatory_multi_id';
                        if (!$fc) {
                            $c = ConvocatoriaMulti::pluck('id')->toArray();
                        }
                    }
                    break;

                case 5; {
                        $ftipoc = 'convocatory_close_id';
                        if (!$fc) {
                            $c = Cerrada::pluck('id')->toArray();
                        }
                    }
                    break;
            }

            if ($fc) {
                if ($bFechas) {
                    $bookings = $bookings->whereIn($ftipoc, $fc);
                } else {
                    $bookings = Booking::whereIn('status_id', $st)->whereIn($ftipoc, $fc);
                }
            } else {
                if ($bFechas) {
                    $bookings = $bookings->whereIn($ftipoc, $c);
                } else {
                    $bookings = Booking::whereIn('status_id', $st)->whereIn($ftipoc, $c);
                }
            }
        }

        //Filtros:

        //any
        $f = isset($valores['any']) ? $valores['any'] : null;
        if ($f) {
            $any = $valores['any'];
            $fini = "$any-01-01";
            $ffin = "$any-12-31";

            $bookings = $bookings->where('course_start_date', '>=', $fini)->where('course_start_date', '<=', $ffin);
        }

        //Prescriptor
        $f = isset($valores['prescriptores']) ? $valores['prescriptores'] : 0;
        if ($f) {
            $bookings = $bookings->where('prescriptor_id', $f);
        }

        //Plataforma
        $f = isset($valores['plataformas']) ? $valores['plataformas'] : 0;
        if ($f) {
            $bookings = $bookings->where('plataforma', $f);
        }

        //Asignado
        $f = isset($valores['asignados']) ? $valores['asignados'] : 0;
        if ($f) {
            $bookings = $bookings->where('user_id', $f);
        }

        //categoria prescriptor
        $f = isset($valores['categoriasp']) ? $valores['categoriasp'] : 0;
        if ($f) {
            if (is_array($f)) {
                $cp = [];
                foreach ($f as $fi) {
                    if ($fi > 0) {
                        $cp0 = CategoriaPrescriptor::find($fi);
                        if ($cp0) {
                            $cp += $cp0->prescriptores->pluck('id')->toArray();
                        }
                    }
                }
                $cp = array_unique($cp);

                if (count($cp)) {
                    $bookings = $bookings->whereIn('prescriptor_id', $cp);
                }
            } else {
                $cp = CategoriaPrescriptor::find($f)->prescriptores->pluck('id');
                $bookings = $bookings->whereIn('prescriptor_id', $cp);
            }
        }

        //Origen
        $f = isset($valores['origenes']) ? $valores['origenes'] : 0;
        if (is_array($f) && count($f) == 1) {
            if ($f[0] == 0) {
                $f = null;
            }
        }
        if ($f) {
            if (is_array($f)) {
                $bookings = $bookings->whereIn('origen_id', $f);
            } else {
                $bookings = $bookings->where('origen_id', $f);
            }
        }
        //Suborigen
        $f = isset($valores['suborigenes']) ? $valores['suborigenes'] : 0;
        if ($f) {
            $bookings = $bookings->where('suborigen_id', $f);
        }
        //SuborigenDet
        $f = isset($valores['suborigenesdet']) ? $valores['suborigenesdet'] : 0;
        if ($f) {
            $bookings = $bookings->where('suborigendet_id', $f);
        }


        //oficinas
        if (!$user->informe_oficinas && !$force) {
            $valores['oficinas'] = $user->oficina_id;
        }
        $f = isset($valores['oficinas']) ? $valores['oficinas'] : 0;
        if (is_array($f) && count($f) == 1) {
            if ($f[0] == 0) {
                $f = null;
            }
        }
        if ($f) {
            if (is_array($f)) {
                $bookings = $bookings->whereIn('oficina_id', $f);
            } else {
                $bookings = $bookings->where('oficina_id', $f);
            }
        }

        //proveedor
        $f = isset($valores['proveedores']) ? $valores['proveedores'] : 0;
        if ($f) {
            $p = Proveedor::find($f)->cursos->pluck('id');
            $bookings = $bookings->whereIn('curso_id', $p);
        }

        //paises
        $f = isset($valores['paises']) ? $valores['paises'] : 0;
        if ($f) {
            $centros = Centro::where('country_id', $valores['paises'])->pluck('id')->toArray();
            $cursos = Curso::whereIn('center_id', $centros)->pluck('id')->toArray();

            $bookings = $bookings->whereIn('curso_id', $cursos);
        }

        //centros
        $f = isset($valores['centros']) ? $valores['centros'] : 0;
        if ($f) {
            $p = Centro::find($f)->cursos->pluck('id');
            $bookings = $bookings->whereIn('curso_id', $p);
        }

        //cursos
        $f = isset($valores['cursos']) ? $valores['cursos'] : 0;
        if (is_array($f) && count($f) == 1) {
            if ($f[0] == 0) {
                $f = null;
            }
        }
        if ($f) {
            if (is_array($f)) {
                $bookings = $bookings->whereIn('curso_id', $f);
            } else {
                $bookings = $bookings->where('curso_id', $f);
            }
        }

        //categorias
        $f = isset($valores['categorias']) ? $valores['categorias'] : 0;
        if (is_array($f) && count($f) == 1) {
            if ($f[0] == 0) {
                $f = null;
            }
        }
        if ($f) {
            if (is_array($f)) {
                $bookings = $bookings->whereIn('category_id', $f);
            } else {
                $bookings = $bookings->where('category_id', $f);
            }
        }

        //subcategorias
        $f = isset($valores['subcategorias']) ? $valores['subcategorias'] : 0;
        if (is_array($f) && count($f) == 1) {
            if ($f[0] == 0) {
                $f = null;
            }
        }
        if ($f) {
            if (is_array($f)) {
                $bookings = $bookings->whereIn('subcategory_id', $f);
            } else {
                $bookings = $bookings->where('subcategory_id', $f);
            }
        }

        //subcategoriasdet
        $f = isset($valores['subcategoriasdet']) ? $valores['subcategoriasdet'] : 0;
        if (is_array($f) && count($f) == 1) {
            if ($f[0] == 0) {
                $f = null;
            }
        }
        if ($f) {
            if (is_array($f)) {
                $bookings = $bookings->whereIn('subcategory_det_id', $f);
            } else {
                $bookings = $bookings->where('subcategory_det_id', $f);
            }
        }

        $f = isset($valores['vuelos']) ? $valores['vuelos'] : 0;
        if ($f) {
            $bookings = $bookings->where('vuelo_id', $f);
        }

        $f = isset($valores['agencias']) ? $valores['agencias'] : 0;
        if ($f) {
            $v = \VCN\Models\Agencia::find($f)->vuelos->pluck('id');
            $bookings = $bookings->whereIn('vuelo_id', $v);
        }

        $f = isset($valores['directos']) ? $valores['directos'] : 1;
        if ($f == 0) {
            $bookings = $bookings->where('es_directo', 0);
        } elseif ($f == 2) {
            $bookings = $bookings->where('es_directo', 1);
        }


        //### #### ## Filtro excluye ## ### ####
        if ($valoresNot) {
            $f = explode(',', $valoresNot['prescriptores']);
            if ($valoresNot['prescriptores'] != "") {
                if ($f[0] == 0) //Todos
                {
                    $bookings = $bookings->where('prescriptor_id', 0);
                } elseif ($f[0] > 0) {
                    $bookings = $bookings->whereNotIn('prescriptor_id', $f);
                }
            }
        }

        return $bookings;
    }

    public static function bookingGrupos($valores)
    {
        $bookings = null;
        $user = auth()->user();

        $st = \VCN\Models\Bookings\Status::where('plazas', 1)->pluck('id')->toArray();
        $sto = [ConfigHelper::config('booking_status_overbooking')];

        $st = array_merge($st, $sto);

        //Fechas
        $bFechas = false;
        if (isset($valores['desde']) && isset($valores['hasta'])) {
            if ($valores['desde'] && $valores['hasta']) {
                $fini = Carbon::createFromFormat('d/m/Y', $valores['desde'])->format('Y-m-d');
                $ffin = Carbon::createFromFormat('d/m/Y', $valores['hasta'])->format('Y-m-d');

                $bookings = Booking::whereIn('status_id', $st)->where('fecha_pago1', '>=', $fini)->where('fecha_pago1', '<=', $ffin);

                $bFechas = true;
            } else {
                $bookings = Booking::whereIn('status_id', $st);
            }
        } else {
            //full-admin
            $bookings = Booking::whereIn('status_id', $st);
        }

        if (!$bFechas) {
            //Fechas Salida
            if (isset($valores['desdes']) && isset($valores['hastas'])) {
                if ($valores['desdes'] && $valores['hastas']) {
                    $fini = Carbon::createFromFormat('d/m/Y', $valores['desdes'])->format('Y-m-d');
                    $ffin = Carbon::createFromFormat('d/m/Y', $valores['hastas'])->format('Y-m-d');

                    // $bookings = Booking::whereIn('status_id',$st)->where('course_start_date','>=',$fini)->where('course_end_date','<=',$ffin);
                    $bookings = Booking::whereIn('status_id', $st)->where('course_start_date', '>=', $fini)->where('course_start_date', '<=', $ffin);

                    $bFechas = true;
                } else {
                    $bookings = Booking::whereIn('status_id', $st);
                }
            } else {
                //full-admin
                $bookings = Booking::whereIn('status_id', $st);
            }
        }


        //TipoConvocatoria
        $f = isset($valores['tipoc']) ? $valores['tipoc'] : 0;
        if ($f) {
            switch ($f) {
                case 1: {
                        $ftipoc = 'convocatory_close_id';
                        $c = Cerrada::where('convocatory_semiopen', 0)->pluck('id')->toArray();
                    }
                    break;

                case 2: {
                        $ftipoc = 'convocatory_close_id';
                        $c = Cerrada::where('convocatory_semiopen', 1)->pluck('id')->toArray();
                    }
                    break;

                case 3: {
                        $ftipoc = 'convocatory_open_id';
                        $c = Abierta::pluck('id')->toArray();
                    }
                    break;

                case 4: {
                        $ftipoc = 'convocatory_multi_id';
                        $c = ConvocatoriaMulti::pluck('id')->toArray();
                    }
                    break;
            }

            if ($bFechas) {
                $bookings = $bookings->whereIn($ftipoc, $c);
            } else {
                $bookings = Booking::whereIn('status_id', $st)->whereIn($ftipoc, $c);
            }
        }

        //Filtros
        if ($valores['plataformas']) {
            $bookings = $bookings->where('plataforma', $valores['plataformas']);
        }

        //oficinas
        if (!$user->informe_oficinas) {
            $valores['oficinas'] = $user->oficina_id;
        }
        $f = $valores['oficinas'];
        if ($f) {
            $bookings = $bookings->where('oficina_id', $f);
        }

        //proveedor
        $f = $valores['proveedores'];
        if ($f) {
            $p = Proveedor::find($f)->cursos->pluck('id');
            $bookings = $bookings->whereIn('curso_id', $p);
        }

        //centros
        $f = $valores['centros'];
        if ($f) {
            $p = Centro::find($f)->cursos->pluck('id');
            $bookings = $bookings->whereIn('curso_id', $p);
        }

        //cursos
        $f = $valores['cursos'];
        if ($f) {
            $bookings = $bookings->where('curso_id', $f);
        }

        //categorias
        $f = $valores['categorias'];
        if ($f) {
            $c = Curso::where('category_id', $f)->pluck('id');

            $bookings = $bookings->whereIn('curso_id', $c);
        }

        return $bookings;
    }

    public static function bookingAvisos($valores, $valoresNot)
    {
        $st = explode(',', $valores['status']);

        if ($st[0] != "") {
            $bookings = Booking::whereIn('status_id', $st);
        } else {
            $bookings = Booking::select();
        }

        $user = auth()->user();

        //TipoConvocatoria
        $f = isset($valores['tipoc']) ? $valores['tipoc'] : 0;
        if ($f) {
            $fc = isset($valores['convocatorias']) ? $valores['convocatorias'] : 0;

            if ($fc) {
                $fc = explode(',', $fc);
            }

            switch ($f) {
                case 1: {
                        $ftipoc = 'convocatory_close_id';
                        if (!$fc) {
                            $fc = Cerrada::where('convocatory_semiopen', 0)->pluck('id')->toArray();
                        }
                    }
                    break;

                case 2: {
                        $ftipoc = 'convocatory_close_id';
                        if (!$fc) {
                            $fc = Cerrada::where('convocatory_semiopen', 1)->pluck('id')->toArray();
                        }
                    }
                    break;

                case 3: {
                        $ftipoc = 'convocatory_open_id';
                        if (!$fc) {
                            $fc = Abierta::pluck('id')->toArray();
                        }
                    }
                    break;

                case 4: {
                        $ftipoc = 'convocatory_multi_id';
                        if (!$fc) {
                            $fc = ConvocatoriaMulti::pluck('id')->toArray();
                        }
                    }
                    break;

                case 5; {
                        $ftipoc = 'convocatory_close_id';
                        if (!$fc) {
                            $fc = Cerrada::pluck('id')->toArray();
                        }
                    }
                    break;
            }

            if (is_array($fc)) {
                $bookings = $bookings->whereIn($ftipoc, $fc);
            } else {
                if ($fc) {
                    $bookings = $bookings->where($ftipoc, $fc);
                } else {
                    $bookings = $bookings->where($ftipoc, '>', 0);
                }
            }
        }

        //Filtros
        if ($valores['plataformas']) {
            $bookings = $bookings->where('plataforma', $valores['plataformas']);
        }

        //oficinas
        if ($user && !$user->informe_oficinas) {
            $valores['oficinas'] = $user->oficina_id;
        }
        $f = $valores['oficinas'];
        if ($f) {
            $f = explode(',', $f);
            $bookings = $bookings->whereIn('oficina_id', $f);
        }

        //any
        $f = isset($valores['any']) ? $valores['any'] : null;
        if ($f) {
            $any = $valores['any'];
            $fini = "$any-01-01";
            $ffin = "$any-12-31";

            $bookings = $bookings->where('course_start_date', '>=', $fini)->where('course_start_date', '<=', $ffin);
        }

        //proveedor
        $f = isset($valores['proveedores']) ? $valores['proveedores'] : null;
        if ($f) {
            $p = Proveedor::find($f)->cursos->pluck('id');
            $bookings = $bookings->whereIn('curso_id', $p);
        }

        //centros
        $f = isset($valores['centros']) ? $valores['centros'] : null;
        if ($f) {
            $p = Centro::find($f)->cursos->pluck('id');
            $bookings = $bookings->whereIn('curso_id', $p);
        }

        //cursos
        $f = isset($valores['cursos']) ? $valores['cursos'] : null;
        if ($f) {
            $f = explode(',', $f);
            $bookings = $bookings->whereIn('curso_id', $f);
        }

        //categorias
        $f = isset($valores['categorias']) ? $valores['categorias'] : null;
        if ($f) {
            $c = Curso::where('category_id', $f)->pluck('id');

            $bookings = $bookings->whereIn('curso_id', $c);
        }

        //subcategorias
        $f = isset($valores['subcategorias']) ? $valores['subcategorias'] : null;
        if ($f) {
            $c = Curso::where('subcategory_id', $f)->pluck('id');

            $bookings = $bookings->whereIn('curso_id', $c);
        }

        $f = isset($valores['prescriptores']) ? $valores['prescriptores'] : null;
        if ($f) {
            $bookings = $bookings->where('prescriptor_id', $f);
        }

        $f = isset($valores['directos']) ? $valores['directos'] : 1;
        // if($f)
        {
            // [1=> 'Incluir', 0=> 'Excluir', 2=> 'Sólo directos']
            if ($f == 0) {
                $bookings = $bookings->where('es_directo', 0);
            }

            if ($f == 2) {
                $bookings = $bookings->where('es_directo', 1);
            }
        }

        //paises
        $f = isset($valores['paises']) ? $valores['paises'] : null;
        if ($f)
        {
            $f = explode(',', $f);
            $centros = Centro::whereIn('country_id', $f)->pluck('id')->toArray();
            $cursos = Curso::whereIn('center_id', $centros)->pluck('id')->toArray();
            $bookings = $bookings->whereIn('curso_id', $cursos);
        }

        //===== Filtros excluye =====

        //prescriptores
        $f = isset($valoresNot['prescriptores']) ? $valoresNot['prescriptores'] : null;
        if ($f)
        {
            $f = explode(',', $f);

            if ($f[0] == 0) //Todos
            {
                $bookings = $bookings->where('prescriptor_id', 0);
            }
            elseif ($f[0] > 0)
            {
                $bookings = $bookings->whereNotIn('prescriptor_id', $f);
            }
        }

        // paises
        $f = isset($valoresNot['paises']) ? $valoresNot['paises'] : null;
        if ($f)
        {
            $f = explode(',', $f);
            $centros = Centro::whereIn('country_id', $f)->pluck('id')->toArray();
            $cursos = Curso::whereIn('center_id', $centros)->pluck('id')->toArray();
            $bookings = $bookings->whereNotIn('curso_id', $cursos);    
        }

        // cursos
        $f = isset($valoresNot['cursos']) ? $valoresNot['cursos'] : null;
        if ($f)
        {
            $f = explode(',', $f);
            $bookings = $bookings->whereNotIn('curso_id', $f);   
        }

        return $bookings;
    }

    public static function bookingMailing($valores, $valoresNot)
    {
        $st = isset($valores['status']) ? explode(',', $valores['status']) : null;

        if ($st && (int) $st[0] > 0) {
            $bookings = Booking::whereIn('status_id', $st);
        } else {
            $bookings = Booking::select();
        }

        $user = auth()->user();

        //Filtros
        if ($valores['plataformas']) {
            $bookings = $bookings->where('plataforma', $valores['plataformas']);
        }

        $f = isset($valores['any']) ? $valores['any'] : 0;
        if ($f) {
            $any = $valores['any'];
            $fini = "$any-01-01";
            $ffin = "$any-12-31";

            $bookings = $bookings->where('course_start_date', '>=', $fini)->where('course_start_date', '<=', $ffin);
        }

        //oficinas
        if ($user && !$user->informe_oficinas) {
            $valores['oficinas'] = $user->oficina_id;
        }
        $f = $valores['oficinas'];
        if ($f) {
            $f = explode(',', $f);
            $bookings = $bookings->whereIn('oficina_id', $f);
        }

        //curso
        $f = isset($valores['cursos']) ? $valores['cursos'] : 0;
        if ($f) {
            $bookings = $bookings->where('curso_id', $f);
        }

        //Prescriptor
        $f = isset($valores['prescriptores']) ? $valores['prescriptores'] : 0;
        if ($f) {
            $bookings = $bookings->where('prescriptor_id', $f);
        }

        //categorias
        $f = isset($valores['categorias']) ? $valores['categorias'] : null;
        if ($f) {
            $f = explode(',', $f);
            if ((int) $f[0] > 0) {
                $bookings = $bookings->whereIn('category_id', $f);
            }
        }

        //subcategorias
        $f = isset($valores['subcategorias']) ? $valores['subcategorias'] : null;
        if ($f) {
            $f = explode(',', $f);
            if ((int) $f[0] > 0) {
                $bookings = $bookings->whereIn('subcategory_id', $f);
            }
        }

        //paises
        $f = isset($valores['paises']) ? $valores['paises'] : 0;
        if ($f) {
            $centros = Centro::where('country_id', $valores['paises'])->pluck('id')->toArray();
            $cursos = Curso::whereIn('center_id', $centros)->pluck('id')->toArray();

            $bookings = $bookings->whereIn('curso_id', $cursos);
        }

        //Filtro excluye
        $f = explode(',', $valoresNot['prescriptores']);
        if ($valoresNot['prescriptores'] != "") {
            if ($f[0] == 0) //Todos
            {
                $bookings = $bookings->where('prescriptor_id', 0);
            } elseif ($f[0] > 0) {
                $bookings = $bookings->whereNotIn('prescriptor_id', $f);
            }
        }

        return $bookings;
    }

    public function extraVuelo()
    {
        $name = "Aporte Vuelo";

        if (!$this->transporte_recogida || ($this->transporte_recogida == "MADRID" || $this->transporte_recogida == "BCN")) {
            if ($this->extras_otros->where('name', $name)->first()) {
                $this->extras_otros->where('name', $name)->first()->delete();
            }
            return;
        }

        if ($this->extras_otros->where('name', $name)->first()) {
            return;
        }

        //BookingExtra tipo 5
        $e = new BookingExtra;
        $e->tipo = 4;
        $e->booking_id = $this->id;
        $e->extra_id = 0;
        $e->unidades = 1;
        $e->moneda_id = ConfigHelper::default_moneda_id();
        $e->precio = (float) ConfigHelper::config('extra_aporte');
        $e->notas = $this->transporte_recogida;
        $e->name = $name;

        // $e->required = $extra->required;
        // $e->tipo_unidad = $extra->tipo_unidad;

        $e->save();
    }



    public function yaAviso($aviso_id)
    {
        return !is_null($this->logs->where('tipo', "Aviso[$aviso_id]")->first());
    }

    public function trigger()
    {
        $avisos = Aviso::where('tipo', 0)->where('activo', 1)->where('cuando_tipo', 'trigger')->where('cuando_trigger', $this->status_id)->get();

        foreach ($avisos as $aviso) {
            //Verificamos que este booking entre en la regla
            $bookings = Booking::bookingAvisos($aviso->filtros, $aviso->filtros_not);
            $bookings = $bookings->where('id', $this->id);

            if ($bookings->first()) {
                $log = new AvisoLog;
                $log->aviso_id = $aviso->id;
                $log->notas = "Booking[$this->id]";
                $log->enviados = 1;
                $log->save();

                //Pendiente ejecutar aviso
            }
        }
    }

    public function triggerTarea()
    {
        $avisos = Aviso::where('tipo', 0)->where('activo', 1)->where('cuando_tipo', 'tarea')->where('cuando_trigger', $this->status_id)->get();

        foreach ($avisos as $aviso) {
            //Verificamos que este booking entre en la regla
            $bookings = Booking::bookingAvisos($aviso->filtros, $aviso->filtros_not);
            $bookings = $bookings->where('id', $this->id);

            if ($bookings->first()) {
                $log = new AvisoLog;
                $log->aviso_id = $aviso->id;
                $log->notas = "Booking[$this->id]";
                $log->enviados = 1;
                $log->save();

                $viajero = $this->viajero;
                $t = new ViajeroTarea;
                $t->viajero_id = $this->viajero_id;
                $t->fecha = Carbon::parse($this->course_start_date)->addDays($aviso->cuando_dia)->format('Y-m-d ' . $aviso->cuando_hora);
                $t->tipo = $aviso->tarea_tipo;
                $t->notas = $aviso->tarea;
                $t->asign_to = $viajero->asign_to;
                $t->aviso = $aviso->id;
                $t->save();
            }
        }
    }

    public function pagado($estado = true)
    {
        if ($this->pagado == $estado) {
            return false;
        }

        $this->pagado = $estado;
        $this->pagado_fecha = Carbon::now();
        $this->pagado_total = $this->total;
        $this->save();

        if ($estado) {
            BookingLog::addLog($this, "Pagado");
        } else {
            BookingLog::addLog($this, "No Pagado");
        }

        return true;
    }

    public function getEsEmpezadoAttribute()
    {
        $cd = Carbon::parse($this->course_start_date);
        return $cd->diffInDays(Carbon::now(), false) >= 0;
    }

    public function getEsCanceladoAttribute()
    {
        if ($this->status_id == ConfigHelper::config('booking_status_cancelado')) {
            return true;
        }

        return false;
    }

    public function getEsPrebookingAttribute()
    {
        if ($this->status_id == ConfigHelper::config('booking_status_prebooking')) {
            return true;
        }

        return false;
    }

    public function getEsOverbookingAttribute()
    {
        if ($this->status_id == ConfigHelper::config('booking_status_overbooking')) {
            return true;
        }

        return false;
    }

    public function getEsCorporativoOverbookingAttribute()
    {
        if (!$this->es_corporativo) {
            return false;
        }

        $plazasv = $this->vuelo ? $this->vuelo->plazas_disponibles_ovbkg : 1;
        $plazasa = 0;

        $pa = $this->convocatoria ? $this->convocatoria->getPlazas($this->alojamiento ? $this->alojamiento->id : 0) : null;
        if ($pa) {
            $plazasa = $pa->plazas_disponibles_ovbkg;
        }

        if ($plazasv < 1 || $plazasa < 1) {
            return true;
        }

        return false;
    }

    public function getSeraOverbookingAttribute()
    {
        return false; //pendiente
        
        $plazasv = $this->vuelo ? $this->vuelo->plazas_disponibles_ovbkg : 1;
        $plazasa = 0;

        $pa = $this->convocatoria ? $this->convocatoria->getPlazas($this->alojamiento ? $this->alojamiento->id : 0) : null;
        if ($pa) {
            $plazasa = $pa->plazas_disponibles_ovbkg;
        }

        if ($plazasv < 1 || $plazasa < 1) {
            return true;
        }

        return false;
    }

    public function getEsRefundAttribute()
    {
        if ($this->status_id == ConfigHelper::config('booking_status_refund')) {
            return true;
        }

        return false;
    }

    public function getEsVueltaAttribute()
    {
        if ($this->status_id == ConfigHelper::config('booking_status_vuelta')) {
            return true;
        }

        return false;
    }

    public function getEsFueraAttribute()
    {
        if ($this->status_id == ConfigHelper::config('booking_status_fuera')) {
            return true;
        }

        return false;
    }

    public function getEsSeguroCancelacionAttribute()
    {
        return $this->seguro_cancelacion ? true : false;
    }

    public function getSeguroCancelacionAttribute()
    {
        $e = Extra::where('es_cancelacion', 1)->pluck('id')->toArray();

        $extra = BookingExtra::where('booking_id', $this->id)->whereIn('extra_id', $e)->first();

        return $extra;
    }

    public function getSeguroCancelacionEnMonedaAttribute()
    {
        return $this->seguro_cancelacion ? $this->seguro_cancelacion->precio : 0;
    }

    public function getAreaDefaultAttribute()
    {
        if (!$this->convocatoria) {
            return false;
        }

        if (!$this->convocatoria->area) {
            return false;
        }

        if ($this->prescriptor) {
            if (!$this->prescriptor->area) {
                return false;
            }
        }

        return true;
    }

    public function getAreaPagosDefaultAttribute()
    {
        if (!$this->convocatoria) {
            return false;
        }

        if (!$this->convocatoria->area_pagos) {
            return false;
        }

        if ($this->prescriptor) {
            if (!$this->prescriptor->area_pagos) {
                return false;
            }
        }

        return true;
    }

    public function getAreaReunionDefaultAttribute()
    {
        if (!$this->convocatoria) {
            return false;
        }

        if (!$this->convocatoria->area_reunion) {
            return false;
        }

        if ($this->prescriptor) {
            if (!$this->prescriptor->area_reunion) {
                return false;
            }
        }

        return true;
    }

    public function getAreaCuestionariosAttribute()
    {
        $cuestionarios = collect();

        $stplazas = \VCN\Models\Bookings\Status::where('plazas', 1)->pluck('id')->toArray();
        if (!in_array($this->status_id, $stplazas)) {
            return $cuestionarios;
        }

        if (!$this->convocatoria) {
            return $cuestionarios;
        }

        // $filtro = ConfigHelper::config('propietario');

        $forms = [];

        foreach ($this->convocatoria->cuestionarios->where('activo', 1) as $c) {
            if (!in_array($c->id, $forms)) {
                $forms[] = $c->id;
                $cuestionarios = $cuestionarios->push($c);
            }
        }

        foreach ($this->curso->cuestionarios->where('activo', 1) as $c) {
            if (!in_array($c->id, $forms)) {
                $forms[] = $c->id;
                $cuestionarios = $cuestionarios->push($c);
            }
        }

        if ($this->curso->centro) {
            foreach ($this->curso->centro->cuestionarios->where('activo', 1) as $c) {
                if (!in_array($c->id, $forms)) {
                    $forms[] = $c->id;
                    $cuestionarios = $cuestionarios->push($c);
                }
            }
        }

        //Categoria y Subcategoria
        if ($this->curso->categoria) {
            foreach ($this->curso->categoria->cuestionarios->where('activo', 1) as $c) {
                if (!in_array($c->id, $forms)) {
                    $forms[] = $c->id;
                    $cuestionarios = $cuestionarios->push($c);
                }
            }
        }
        if ($this->curso->subcategoria) {
            foreach ($this->curso->subcategoria->cuestionarios->where('activo', 1) as $c) {
                if (!in_array($c->id, $forms)) {
                    $forms[] = $c->id;
                    $cuestionarios = $cuestionarios->push($c);
                }
            }
        }

        $cuestionarios = $cuestionarios->unique();
        // dd($cuestionarios);

        return $cuestionarios;
    }

    public function exam_respuestas()
    {
        return $this->hasMany(\VCN\Models\Exams\Respuesta::class, 'booking_id');
    }

    public function getAreaExamenesAttribute()
    {
        $examenes = collect();
        $examIds = [];
        $excluyeIds = [];

        foreach ($this->exam_respuestas as $r) {
            $c = $r->examen;

            if (!in_array($c->id, $examIds)) {
                $examIds[] = $c->id;
            }
        }

        //===

        if ($this->convocatoria) {
            foreach ($this->convocatoria->examenes->where('activo', 1) as $c) {
                if ($c->pivot->excluye) {
                    $excluyeIds[] = $c->id;
                    continue;
                }

                if (!in_array($c->id, $examIds)) {
                    $examIds[] = $c->id;
                }
            }
        }

        foreach ($this->curso->examenes->where('activo', 1) as $c) {
            if ($c->pivot->excluye) {
                $excluyeIds[] = $c->id;
                continue;
            }

            if (!in_array($c->id, $examIds)) {
                $examIds[] = $c->id;
            }
        }

        if ($this->curso->centro) {
            foreach ($this->curso->centro->examenes->where('activo', 1) as $c) {
                if ($c->pivot->excluye) {
                    $excluyeIds[] = $c->id;
                    continue;
                }

                if (!in_array($c->id, $examIds)) {
                    $examIds[] = $c->id;
                }
            }
        }

        //Categoria y Subcategoria
        if ($this->curso->categoria) {
            foreach ($this->curso->categoria->examenes->where('activo', 1) as $c) {
                if ($c->pivot->excluye) {
                    $excluyeIds[] = $c->id;
                    continue;
                }

                if (!in_array($c->id, $examIds)) {
                    $examIds[] = $c->id;
                }
            }
        }
        if ($this->curso->subcategoria) {
            foreach ($this->curso->subcategoria->examenes->where('activo', 1) as $c) {
                if ($c->pivot->excluye) {
                    $excluyeIds[] = $c->id;
                    continue;
                }

                if (!in_array($c->id, $examIds)) {
                    $examIds[] = $c->id;
                }
            }
        }

        $examenes = Examen::whereIn('id', $examIds)->whereNotIn('id', $excluyeIds)->get();

        return $examenes;
    }

    public function getExamenRespuesta($examId)
    {
        return Respuesta::where('booking_id', $this->id)->where('examen_id', $examId)->first();
    }

    public function getExamenRespuestasAttribute()
    {
        return Respuesta::where('booking_id', $this->id)->get();
    }

    public function getPromoCambioFijoDefaultAttribute()
    {
        if (!$this->curso->categoria) {
            return false;
        }

        $b2ret = false;

        if (!$this->fecha_pago1) {
            return false;
        }

        $cat = $this->curso->categoria;

        if ($cat->promo_cambio_fijo_fechas) {
            $fechas = $cat->promo_cambio_fijo_fechas;

            // dd($fechas['desde']);

            foreach ($fechas['desde'] as $k => $vfecha) {
                if ($fechas['desde'][$k] && $fechas['hasta'][$k]) {
                    $desde = Carbon::createFromFormat('!d/m/Y', $fechas['desde'][$k]);
                    $hasta = Carbon::createFromFormat('!d/m/Y', $fechas['hasta'][$k]);

                    if ($this->fecha_pago1->gte($desde) && $this->fecha_pago1->lte($hasta)) {
                        $b2ret = true;
                        break;
                    }
                }
            }
        }

        if (!$this->curso->subcategoria) {
            return $b2ret;
        }

        $b2ret_sc = false;
        $scat = $this->curso->subcategoria;
        if ($scat->promo_cambio_fijo_fechas) {
            $fechas = $scat->promo_cambio_fijo_fechas;

            // dd($fechas['desde']);

            foreach ($fechas['desde'] as $k => $vfecha) {
                if ($fechas['desde'][$k] && $fechas['hasta'][$k]) {
                    $desde = Carbon::createFromFormat('!d/m/Y', $fechas['desde'][$k]);
                    $hasta = Carbon::createFromFormat('!d/m/Y', $fechas['hasta'][$k]);

                    if ($this->fecha_pago1->gte($desde) && $this->fecha_pago1->lte($hasta)) {
                        $b2ret_sc = true;
                        break;
                    }
                }
            }
        }

        return $b2ret_sc ?: $b2ret; //devuelve la de la categoria si la de scat no lo tiene marcado
    }

    public function getVueloInfoAreaAttribute()
    {
        if (!$this->vuelo) {
            return false;
        }

        if ($this->vuelo->encuentro_lugar) {
            if ($this->vuelo->encuentro_fecha < Carbon::now()->toDateString()) {
                //Ha pasado el día
                return false;
            }
        }

        if ($this->vuelo_salida_area) {
            if ($this->vuelo_salida_area->salida_fecha < Carbon::now()->toDateString()) {
                //Ha pasado el día
                return false;
            }
        }

        return true;
    }

    public function getVueloTituloAreaAttribute()
    {
        if (!$this->vuelo) {
            return null;
        }

        $idas = $this->vuelo->etapas->where('tipo', 0);
        $llegadas = $this->vuelo->etapas->where('tipo', 1);

        if ($idas->first()) {
            if ($idas->first()->salida_fecha > Carbon::now()) {
                return "area.vuelosalida";
            }
        }

        if ($llegadas->first()) {
            if ($llegadas->first()->salida_fecha > Carbon::now()) {
                return "area.vuelollegada";
            }
        }

        return null;
    }

    public function getVueloAreaAttribute()
    {
        if (!$this->vuelo) {
            return null;
        }

        $idas = $this->vuelo->etapas->where('tipo', 0);
        $llegadas = $this->vuelo->etapas->where('tipo', 1);

        if ($idas->first()) {
            if ($idas->first()->salida_fecha > Carbon::now()) {
                //Mostramos Salida
                return $idas;
            }
        }

        if ($llegadas->first()) {
            if ($llegadas->first()->salida_fecha > Carbon::now()) {
                //Mostramos Salida
                return $llegadas;
            }
        }

        return null;
    }

    public function getVueloSalidaAreaAttribute()
    {
        if (!$this->vuelo) {
            return false;
        }

        return $this->vuelo->etapas->where('tipo', 0)->first();
    }

    public function getReunionAttribute()
    {
        if (!$this->reuniones) {
            return false;
        }

        return $this->reuniones->count() > 0;
    }

    public function getReunionesAttribute()
    {
        if(!$this->convocatoria)
        {
            return []; 
        }
        
        $curso_id = $this->curso_id;
        $convo_tipo = $this->convocatoria_tipo;
        $convo_id = $this->convocatoria->id;

        if ($this->convocatoria->reunion_no) {
            return [];
        }

        if (!$this->fecha_reserva) {
            return [];
        }

        //Bookings pasados
        if ($this->es_pasado) {
            return [];
        }

        //Primero convocatoria y luego curso y si no pues null

        $reuniones = Reunion::where('oficina_id', $this->oficina_id)->where('convocatoria_tipo', $convo_tipo)->where('convocatoria_id', $convo_id)->where('fecha', '>=', $this->fecha_reserva)->where('fecha', '>=', Carbon::today())->get();

        if (!$reuniones->count()) {
            $reuniones = Reunion::where('oficina_id', $this->oficina_id)->where('curso_id', $curso_id)->where('fecha', '>=', $this->fecha_reserva)->where('fecha', '>=', Carbon::today())->get();
        }

        if (!$reuniones->count()) {
            return [];
        }

        // if($reunion)
        // {
        //     if( $reunion->fecha->lt($this->fecha_reserva) )
        //     {
        //         return null;
        //     }

        //     if( $reunion->fecha->lt( Carbon::now() ) )
        //     {
        //         return null;
        //     }
        // }

        return $reuniones;
    }

    public function mailReunion($todos = false)
    {
        $r = 0;

        if ($this->es_directo) {
            return false;
        }

        if (!$this->reunion) {
            return false;
        }

        if ($this->es_pasado) {
            return false;
        }

        if ($todos) {
            $r = MailHelper::mailBookingReunion($this->id);
            if ($r > 0) {
                $this->mail_reunion = true;
                $this->save();

                BookingLog::addLogReunion($this);
            }
            return $r;
        }

        if ($this->reunion && !$this->mail_reunion) {
            $r = MailHelper::mailBookingReunion($this->id);
            if ($r > 0) {
                $this->mail_reunion = true;
                $this->save();

                BookingLog::addLogReunion($this);
            }
        }

        return $r;
    }

    public function getEsNotapago35GeneradaAttribute()
    {
        $name = "Nota_Pago_Final_" . $this->id;

        $va = ViajeroArchivo::where('booking_id', $this->id)->where('doc', 'LIKE', "$name%")->first();
        if ($va) {
            return true;
        }

        return false;
    }

    public function getEsNotapago35EnviadaAttribute()
    {
        $ret = false;

        $name = "Nota_Pago_Final_" . $this->id;
        $nps = ViajeroArchivo::where('booking_id', $this->id)->where('doc', 'LIKE', "$name%");

        foreach ($nps->get() as $np) {
            if ($np->notapago_enviado) {
                $ret = true;
                break;
            }
        }

        return $ret;
    }

    public function getEsNotapago35LastEnviadaAttribute()
    {
        $n = $this->notapago35_last;

        if (!$n) {
            return false;
        }

        return $n->notapago_enviado;
    }

    public function getNotapago35CountAttribute()
    {
        $name = "Nota_Pago_Final_" . $this->id;

        $ret = ViajeroArchivo::where('booking_id', $this->id)->where('doc', 'LIKE', "$name%")->count();

        if ($ret > 0) {
            //Ocultamos las otras
            $vas = ViajeroArchivo::where('booking_id', $this->id)->where('doc', 'LIKE', "$name%")->get();
            foreach ($vas as $va) {
                $va->update(['visible' => false]);
            }
        }

        return $ret;
    }

    public function getNotapago35LastAttribute()
    {
        $name = "Nota_Pago_Final_" . $this->id;

        return ViajeroArchivo::where('booking_id', $this->id)->where('doc', 'LIKE', "$name%")->orderBy('created_at', 'DESC')->first();
    }

    public function isNotaPagoPosible($force = false)
    {
        if (!$this->area_pagos) {
            Session::flash('mensaje-alert', 'No se puede generar Nota de pago. No tiene activo el Area de pagos.');
            return false;
        }

        if ($this->saldo_pendiente <= 0) {
            Session::flash('mensaje-alert', 'No se puede generar Nota de pago. No tiene saldo pendinete');
            return false;
        }

        if ($this->status_id != ConfigHelper::config('booking_status_prebooking') && $this->status_id != ConfigHelper::config('booking_status_presalida') && $this->status_id != ConfigHelper::config('booking_status_fuera')) {
            Session::flash('mensaje-alert', 'No se puede generar Nota de pago. No tiene Estado compatible.');
            return false;
        }

        if ($this->es_notapago35_enviada && !$force) {
            // return false;
        }

        $auto = $this->curso->categoria->notapago_auto;

        //No es auto y no es forzado
        if (!$auto && !$force) {
            return false;
            //manual pero ya tiene generada, entonces si hay q generarla
            // if(!$this->es_notapago35_generada)
            // {
            //     return false;
            // }
        }

        //No la mandamos 2 veces si es auto
        if ($auto && $this->es_notapago35_generada && !$force) {
            return false;
        }

        $npDias = $this->curso->categoria->notapago;

        $fsalida35 = Carbon::parse($this->course_start_date)->subDays($npDias);
        $fnow = Carbon::now();
        $bFechaEnviar = $fnow->gte($fsalida35); //Comprobacion $npDias

        //No es la fecha y no es forzado
        if (!$bFechaEnviar && !$force) {
            return false;
        }

        return true;
    }

    public function notaPago35($force = false, $log = "")
    {
        if ($this->es_directo) {
            return false;
        }

        if (!$this->isNotaPagoPosible($force)) {
            //Session::flash('mensaje-alert', 'No se puede generar Nota de pago.');
            return false;
        }

        //Ojo pdf lo llama con force y es auto (por categoria): pero puede q no le toque la fecha y no se ha generado aun
        // if($auto && $force && !$bFechaEnviar && !$this->es_notapago35_generada )
        // {
        //     Session::flash('mensaje-alert', 'No se puede generar Nota de pago. Es automática y no ha sido generada aún.');
        //     return false;
        // }

        //21/06/2018: si tienen TC hay q respetarlo
        $tc = $this->categoria->getTablaCambioAny($this->any, $this->convocatoria->moneda);
        if (!$tc) {
            //31/05/2017: establecemos su propia tabla de cambio
            $this->setMonedasCambio();
        }


        // Nota_Pago_Final_%ID booking%_%DDMMYYYY
        $this->notaPago35pdf($log);

        return true;
    }

    public function notaPago35update($log = "(update)")
    {
        if (!$this->area_pagos) {
            return false;
        }

        $this->precio_total; //Para q se actualicen los saldos

        if ($this->saldo_pendiente <= 0) {
            return false;
        }

        if ($this->pagado) {
            return false;
        }

        if ($this->status_id != ConfigHelper::config('booking_status_prebooking') && $this->status_id != ConfigHelper::config('booking_status_presalida') && $this->status_id != ConfigHelper::config('booking_status_fuera')) {
            return false;
        }

        $auto = $this->curso->categoria->notapago_auto;

        if (!$auto) {
            return false;
        }

        $npDias = $this->curso->categoria->notapago;

        $fsalida35 = Carbon::parse($this->course_start_date)->subDays($npDias);
        $fnow = Carbon::now();
        $bFechaEnviar = $fnow->gte($fsalida35); //Comprobacion $npDias

        //No es la fecha y no es forzado
        if (!$bFechaEnviar) {
            return false;
        }

        // Nota_Pago_Final_%ID booking%_%DDMMYYYY
        $this->notaPago35pdf($log);

        return true;
    }

    public function notaPago35pdf($log = "")
    {
        $dir = storage_path("files/viajeros/" . $this->viajero->id . "/");
        $name = "Nota_Pago_Final_" . $this->id . "_" . Carbon::now()->format('dmY');

        //Versiones
        if ($this->notapago35_last) {
            $v = $this->notapago35_count + 1;
            $name = $name . "_v$v";

            Session::flash('notapago-new', "Al actualizar el Booking, se ha actualizado la Nota de pago.");
        }

        $file = $name . ".pdf";

        //generamos pdf
        $booking = $this;
        $idioma = $this->viajero->idioma_contacto;
        $view = 'pdf.' . strtolower($idioma) . '.recibo35';

        $lang = $this->idioma_contacto;

        // return view($view, compact('booking','idioma'));

        /*
        $pdf = PDF::loadView($view, compact('booking','idioma'))
            ->setOption('margin-top', 30)->setOption('margin-right', 0)->setOption('margin-bottom', 20)->setOption('margin-left', 0)
            ->setOption('disable-smart-shrinking',true)->setOption('zoom','0.78');
        */

        $p = $this->plataforma;

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView($view, compact('booking', 'idioma'));
        $pdf->setOption('margin-top', 30);
        $pdf->setOption('margin-right', 0);
        $pdf->setOption('margin-bottom', 30);
        $pdf->setOption('margin-left', 0);
        $pdf->setOption('no-print-media-type', false);
        $pdf->setOption('footer-spacing', 0);
        $pdf->setOption('header-font-size', 9);
        $pdf->setOption('header-spacing', 0);
        $pdf->setOption('header-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'header.html');
        $pdf->setOption('footer-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'footer.html');

        $pdf->save($dir . $file);

        //viajeroarchivo
        $va = new ViajeroArchivo;
        $va->viajero_id = $this->viajero->id;
        $va->booking_id = $this->id;
        $va->user_id = 0; //auth()?auth()->user()->id:1;
        $va->visible = true;
        $va->doc = $file;
        $va->fecha = Carbon::now(); //->format('Y-m-d');
        $va->save();

        $va->update(['visible' => true]);

        BookingLog::addLog($this, "Nota Pago generada. $log", $va->doc);
    }

    public function notaPago35_mail()
    {
        if ($this->es_directo) {
            return false;
        }

        $np = $this->notapago35_last;
        if (!$np) {
            Session::flash('mensaje-alert', "Error. No hay nota de pago que enviar.");
            return false;
        }

        if (MailHelper::mailBookingNotaPago35($this->id)) {
            $np->notapago_enviado = true;
            $np->save();

            BookingLog::addLog($this, "Nota Pago enviada.", $np->doc);
        } else {
            BookingLog::addLog($this, "Nota Pago NO enviada (error email).", $np->doc);
        }
    }

    public function familiaOcupadaCheck()
    {
        foreach ($this->familias as $familia) {
            $familia_id = $familia->familia_id;
            $desde = $familia->desde;
            $hasta = $familia->hasta;

            $bfs = BookingFamilia::where('familia_id', $familia_id)->where('booking_id', '<>', $this->id);
            foreach ($bfs->get() as $bf) {
                if ($bf->desde->gte($desde) && $bf->hasta->lte($hasta)) {
                    $famtxt = $bf->familia->name;
                    Session::flash('mensaje', "Atención. La familia <strong>$famtxt</strong> tiene otro viajero en estas fechas.");
                    break;
                }
            }
        }
    }

    public function familiaFechasCheck(Carbon $desde, Carbon $hasta, $asignacion_id = 0)
    {
        foreach ($this->familias as $familia) {
            if ($familia->id == $asignacion_id) {
                continue;
            }

            $desdeF = $familia->desde;
            $hastaF = $familia->hasta;

            if ($desde->eq($desdeF) || $hasta->eq($hastaF)) {
                return false;
            }

            if ($desde->lt($hastaF) && $hasta->lt($hastaF)) {
                if ($desde->gt($desdeF) && $hasta->gt($desdeF)) {
                    return false;
                }
            }

            if ($desde->lt($hastaF) && $hasta->gt($hastaF)) {
                return false;
            }

            if ($desde->lt($desdeF) && $hasta->gt($desdeF)) {
                return false;
            }
        }

        return true;
    }

    public function familiaFechasAlojamientoCheck(Carbon $desde, Carbon $hasta)
    {
        $desdeA = Carbon::createFromFormat('!d/m/Y', $this->alojamiento_start_date);
        $hastaA = Carbon::createFromFormat('!d/m/Y', $this->alojamiento_end_date);

        if ($desde->lt($desdeA)) {
            return false;
        }

        if ($hasta->gt($hastaA)) {
            return false;
        }


        return true;
    }


    public function schoolFechasCheck(Carbon $desde, Carbon $hasta, $asignacion_id = 0)
    {
        foreach ($this->schools as $school) {
            if ($school->id == $asignacion_id) {
                continue;
            }

            $desdeF = $school->desde;
            $hastaF = $school->hasta;

            if ($desde->eq($desdeF) || $hasta->eq($hastaF)) {
                return false;
            }

            if ($desde->lt($hastaF) && $hasta->lt($hastaF)) {
                if ($desde->gt($desdeF) && $hasta->gt($desdeF)) {
                    return false;
                }
            }

            if ($desde->lt($hastaF) && $hasta->gt($hastaF)) {
                return false;
            }

            if ($desde->lt($desdeF) && $hasta->gt($desdeF)) {
                return false;
            }
        }

        return true;
    }

    public function schoolFechasAlojamientoCheck(Carbon $desde, Carbon $hasta)
    {
        $desdeA = Carbon::createFromFormat('!d/m/Y', $this->alojamiento_start_date);
        $hastaA = Carbon::createFromFormat('!d/m/Y', $this->alojamiento_end_date);

        if ($desde->lt($desdeA)) {
            return false;
        }

        if ($hasta->gt($hastaA)) {
            return false;
        }


        return true;
    }

    public function getCuestionarioRespuesta($cuestionario_id)
    {
        //Viajero
        $r = CuestionarioRespuesta::where('booking_id', $this->id)->where('cuestionario_id', $cuestionario_id)->first();

        if (!$r) {
            return null;
        }

        $rv = $r->cuestionario;

        if ($rv->destino == "tutores") {
            return null;
        } elseif ($rv->destino == "viajero") {
            //Puede haberla respondido el tutor
            $r = CuestionarioRespuesta::where('booking_id', $this->id)->where('cuestionario_id', $cuestionario_id)->first();
        } else {
            $r = CuestionarioRespuesta::where('booking_id', $this->id)->where('cuestionario_id', $cuestionario_id)->where("viajero_id", $this->viajero_id)->first();
        }

        return $r;
    }

    public function getCuestionarioRespuestaTutor($cuestionario_id, $tutor_id)
    {
        $r = CuestionarioRespuesta::where('booking_id', $this->id)->where('cuestionario_id', $cuestionario_id)->first();

        if (!$r) {
            return null;
        }

        $rv = $this->area_cuestionarios->where('id', $r->cuestionario_id)->first();
        if (!$rv) {
            return null;
        }

        //Si es de viajero al tutor se le muestra el del viajero
        if ($rv->destino != "viajero") {
            $r = CuestionarioRespuesta::where('booking_id', $this->id)->where('cuestionario_id', $cuestionario_id)->where("tutor_id", $tutor_id)->first();
        }

        return $r;
    }


    public function getCuestionarioEstadoViajero($cuestionario_id)
    {
        $re = $this->getCuestionarioRespuesta($cuestionario_id);

        return $re ? $re->estado : 0;
    }
    public function getCuestionarioEstadoTutor($cuestionario_id, $tutor_id)
    {
        $re = $this->getCuestionarioRespuestaTutor($cuestionario_id, $tutor_id);

        return $re ? $re->estado : 0;
    }

    public function getAreaCuestionarioEstado($cuestionario_id)
    {
        $user = auth()->user();
        if (!$user) {
            return 0;
        }

        //Viajero o tutor?
        if ($user->roleid == 11) {
            //Viajero
            $re = $this->getCuestionarioRespuesta($cuestionario_id);
        } elseif ($user->roleid == 12) {
            $re = $this->getCuestionarioRespuestaTutor($cuestionario_id, $user->ficha->id);
        } else {
            //admin
            $re = $this->getCuestionarioRespuesta($cuestionario_id);
        }

        return $re ? $re->estado : 0;
    }

    public function getPrescriptorNoFacturarAttribute()
    {
        if (!$this->prescriptor) {
            return $this->no_facturar;
        }

        if ($this->prescriptor->no_facturar && !$this->no_facturar) {
            $this->no_facturar = 1;
            $this->save();
        }

        return $this->prescriptor->no_facturar;
    }

    public function getFacturaGrupAttribute()
    {
        if (!$this->factura) {
            return false;
        }

        return $this->factura->grup;
    }

    public function getFacturasGrupAttribute()
    {
        if (!$this->factura) {
            return false;
        }

        return $this->facturas()->where('grup_id', '>', 0)->get();
    }

    public function getTieneFacturasAttribute()
    {
        if ($this->facturas->count() > 0) {
            return true;
        }

        return false;
    }

    public function getEsFacturadoAttribute()
    {
        return !$this->es_facturable;
    }

    public function getFacturableAttribute()
    {
        $f = $this->facturas->where('grup_id', null)->sum('total');

        $fgrup = 0;
        $grup = $this->facturas()->where('grup_id', '>', 0)->get();
        foreach ($grup as $fact) {
            $factg = $fact->grup;
            $totgrup = count($factg->bookings);

            if ($totgrup > 0) {
                $fgrup += $this->facturas->where('grup_id', $factg->id)->sum('total') / $totgrup;
            }
        }

        // dd($this->total);
        // dd("($this->total - $f - $fgrup)");

        return round(($this->total - $f - $fgrup), 2);
    }

    public function getEsFacturableAttribute()
    {
        if (!$this->factura) {
            return true;
        }

        // if( $this->factura->grup )
        // {
        //     return false;
        // }

        return $this->facturable > 0;
    }

    public function getEsFacturableAutoAttribute()
    {
        $booking = $this;

        $p = $booking->plataforma;
        $facturacion = ConfigHelper::config('facturas', $p);

        //Comprobación "NO FACTURAR POR SISTEMA"
        if ($booking->prescriptor) {
            if ($booking->prescriptor->no_facturar) {
                $facturacion = false;
            }
        }

        if ($booking->convocatoria->no_facturar) {
            $facturacion = false;
        }

        if ($booking->curso->categoria) {
            if ($booking->curso->categoria->no_facturar) {
                $facturacion = false;

                if ($booking->curso->subcategoria) {
                    if ($booking->curso->subcategoria->no_facturar) {
                        $facturacion = false;
                    }
                }
            }
        }

        return $facturacion;
    }

    public function facturar_mail()
    {
        return $this->facturar(null, 0, true);
    }


    public function facturar($parcial = null, $percent = 0, $force = false)
    {
        $dir = storage_path("files/bookings/" . $this->viajero->id . "/");

        if ($this->no_facturar == 1) {
            return false;
        }

        // if($this->datos_congelados)
        // {
        //     Session::flash('mensaje',"Datos congelados. No puede generar pdf nuevo.");
        //     return false;//redirect()->back();
        // }

        if ($this->factura) {
            if ($this->factura->grup) {
                Session::flash('mensaje-alert', 'Booking ya facturado como parte de una factura agrupando varios bookings.');
                return redirect()->back();
            }
        }

        $vd = (float) $this->divisa_variacion ?: 0;

        //Ha variado el importe
        if ($this->facturas->count() > 0 && $this->es_facturable) {
            if ($this->facturable != $this->total && !$parcial) {
                $parcial = $this->facturable;
                $percent = 0;
            }

            if ($vd) {
                foreach ($this->facturas as $f) {
                    if ($f->divisa_variacion) {
                        $vd = 0;
                        break;
                    }
                }
            }
        }

        // FACTURA NUEVA
        {
            //ABONO
            // if($this->factura && !$parcial) //si ya había otra es un abono
            // {
            //     $this->facturarAbono();
            // }
            // else
            // {
            // }

            //31/05/2017: establecemos su propia tabla de cambio
            $this->setMonedasCambio();

            $cif = $this->factura_cif;
            $cp = $this->datos->fact_cp ? $this->datos->fact_cp : $this->datos->cp;

            $plat = ConfigHelper::config('propietario');

            // $any = (int)Carbon::parse($this->fecha_inicio)->format('Y');
            $any = (int) Carbon::now()->format('Y');
            $num = ConfigHelper::facturaNumero($any);

            $total = $this->facturable;

            $factura = new BookingFactura;
            $factura->booking_id = $this->id;

            $factura->user_id = auth()->user() ? auth()->user()->id : 0;
            $factura->numero = $num;
            $factura->total = $parcial ?: $total;
            $factura->fecha = Carbon::now();
            $factura->plataforma = $this->plataforma;
            $factura->nif = $cif;
            $factura->cp = $cp;

            $direccion = ($this->datos->fact_domicilio ? $this->datos->fact_domicilio : $this->datos->direccion) . ", " . ($this->datos->fact_cp ? $this->datos->fact_cp : $this->datos->cp) . " " . ($this->datos->fact_poblacion ? $this->datos->fact_poblacion : $this->datos->ciudad);
            $datos = [
                'razonsocial'   => $this->factura_titular,
                'direccion'     => $direccion,
                'cif'           => $cif,
                'cp'            => $cp,
                'concepto1'     => $this->datos->fact_concepto ? $this->datos->fact_concepto : $this->curso->name,
                'concepto2'     => $this->datos->fact_concepto ? "" : ConfigHelper::parseMoneda($this->course_total_amount, $this->curso_moneda),
            ];
            $factura->datos = $datos;
            $factura->precio_total = $this->precio_total;
            $factura->divisa_variacion = $vd;
            $factura->total_divisa_txt = $this->total_divisa_txt;

            $monedas = "";
            foreach ($this->monedas_usadas_txt as $mu) {
                $monedas .= "<small>$mu</small><br>";
            }
            $factura->monedas = $monedas;

            if ($parcial) {
                $factura->parcial = true;
                $factura->parcial_importe = $parcial;
                $factura->parcial_porcentaje = $percent;
            }

            $factura->save();

            $factura->pdf($force);

            return $factura;
        }

        return false;
    }

    public function facturarAbono($factura_id = 0)
    {
        $dir = storage_path("files/bookings/" . $this->viajero->id . "/");

        $plat = ConfigHelper::config('propietario');

        if ($factura_id) {
            $abono_factura = $this->factura_list->find($factura_id);
        } else {
            $abono_factura = $this->factura_last;
        }

        // $any = (int)Carbon::parse($this->fecha_inicio)->format('Y');
        // $any = $abono_factura->fecha->year;
        $any = (int) Carbon::now()->format('Y');
        $num = ConfigHelper::facturaNumero($any);

        $iTotAbono = 0;
        if ($abono_factura->grup) {
            $abono_total = $abono_factura->grup->total * -1;

            $convo = $abono_factura->booking->convocatoria;
            $abono_factura = $abono_factura->grup;

            $grup_total = $abono_total;

            $grup = new BookingFacturaGrup;
            $grup->user_id = auth()->user()->id;
            $grup->numero = $num;
            $grup->total = $abono_total;
            $grup->fecha = Carbon::now();
            $grup->plataforma = $plat;
            $grup->nif = $abono_factura->nif;
            $grup->cp = $abono_factura->cp;
            $grup->razonsocial = $abono_factura->razonsocial;
            $grup->direccion = $abono_factura->direccion;
            $grup->bookings = $abono_factura->bookings;
            $grup->save();

            $grup_id = $grup->id;

            $dir = storage_path("files/convocatorias/facturas/");

            foreach ($abono_factura->facturas as $abonof) {
                $factura = new BookingFactura;
                $factura->booking_id = $abonof->booking_id;
                $factura->user_id = auth()->user()->id;
                $factura->numero = $num;
                $factura->total = $abono_total;
                $factura->fecha = Carbon::now();
                $factura->plataforma = $plat;
                $factura->grup_id = $grup_id;
                $factura->nif = $abonof->nif;
                $factura->cp = $abonof->cp;
                $factura->save();

                $iTotAbono++;
            }

            //Pdf
            $name = "factura_" . $num . ".pdf";
            $file = $dir . $name;
            $lang = $this->idioma_contacto;

            $info = ConfigHelper::config_plataforma();

            $template = 'manage.bookings.factura_grup';

            // return view($template, ['ficha'=> $this, 'factura'=> $grup, 'info'=> $info, 'datos'=> $datos, 'total'=> $grup_total, 'convocatoria'=> $convo]);

            $p = $this->plataforma;
            app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
            $pdf = PDF::loadView($template, ['ficha' => $this, 'factura' => $grup, 'info' => $info, 'total' => $grup_total, 'convocatoria' => $convo]);

            $pdf->setOption('margin-top', 30);
            $pdf->setOption('margin-right', 0);
            $pdf->setOption('margin-bottom', 30);
            $pdf->setOption('margin-left', 0);
            $pdf->setOption('no-print-media-type', false);
            $pdf->setOption('footer-spacing', 0);
            $pdf->setOption('header-font-size', 9);
            $pdf->setOption('header-spacing', 0);

            $pdf->setOption('header-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'header.html');
            $pdf->setOption('footer-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'footer.html');

            $pdf->save($file);
        } else {
            $iTotAbono = 1;

            $abono_total = $abono_factura->total;
            $abono_total = $abono_total * -1;

            $this->no_facturar = 0;
            $this->save();

            $name = "factura_" . $num . ".pdf";
            $file = $dir . $name;

            $factura = new BookingFactura;
            $factura->booking_id = $this->id;
            $factura->user_id = auth()->user()->id;
            $factura->numero = $num;
            $factura->total = $abono_total;
            $factura->fecha = Carbon::now();
            $factura->plataforma = $this->plataforma;
            $factura->nif = $abono_factura->nif;
            $factura->cp = $abono_factura->cp;
            $factura->save();

            $factura->parcial = $abono_factura->parcial;
            $factura->parcial_importe = $abono_factura->parcial_importe * -1;
            $factura->parcial_porcentaje = $abono_factura->parcial_porcentaje;

            $abono_ptotal = $abono_factura->precio_total;
            if ($abono_ptotal['subtotal']) {
                foreach ($abono_ptotal['subtotal'] as $k => $v) {
                    $abono_ptotal['subtotal'][$k] = "-" . $v;
                }

                foreach ($abono_ptotal['subtotal_txt'] as $k => $v) {
                    $abono_ptotal['subtotal_txt'][$k] = "-" . $v;
                }

                $abono_ptotal['total'] = "-" . $abono_ptotal['total'];
                $abono_ptotal['total_txt'] = "-" . $abono_ptotal['total_txt'];
                $abono_ptotal['total_bruto'] *= -1;
                $abono_ptotal['total_neto'] *= -1;
            }

            $factura->datos = $abono_factura->datos;
            $factura->precio_total = $abono_ptotal;
            $factura->monedas = $abono_factura->monedas;
            $factura->divisa_variacion = $abono_factura->divisa_variacion * -1;
            $factura->total_divisa_txt = "-" . $abono_factura->total_divisa_txt;
            $factura->save();

            //Generamos pdf factura
            $name = "factura_" . $num . ".pdf";
            $file = $dir . $name;
            $lang = $this->idioma_contacto;

            $info = ConfigHelper::config_plataforma();

            $template = 'manage.bookings.factura_abono';

            // return view($template, ['ficha'=> $this, 'factura'=> $factura, 'info'=> $info, 'total'=> $parcial]);
            // $factura->pdf_generar_abono($file);
            $job = new \VCN\Jobs\JobPdfFacturaAbono($factura, $file);
            app('Illuminate\Contracts\Bus\Dispatcher')->dispatch($job);
            
        }

        return $iTotAbono;
    }

    public function getTablaCambioAttribute()
    {
        //TC de la convocatoria
        $tc = $this->convocatoria ? $this->convocatoria->tabla_cambio : null;
        if ($tc) {
            return $tc;
        } else {
            //Si no tiene la convocatoria TC => curso
            $m = $this->convocatoria ? $this->convocatoria->moneda : null;
            $tc = $this->curso->categoria ? $this->curso->categoria->getTablaCambioAny($this->any, $m) : null;
            if ($tc) {
                return $tc;
            }
        }

        return null;
    }

    /**
     * @return bool
     */
    public function setMonedasCambio($force = false)
    {
        if ($this->monedas->count() && !$force) {
            // return false;
        }

        if ($this->pagado && !$force) {
            return false;
        }

        $tc = $this->tabla_cambio;

        // if(!$this->pagado)
        {
            foreach ($this->monedas_usadas as $m) {
                if ($m) {
                    $bmTot = $this->monedas->where('moneda_id', $m)->count();
                    if ($bmTot > 1) {
                        foreach ($this->monedas->where('moneda_id', $m) as $bm) {
                            $bm->delete();
                        }
                    }

                    $bm = $this->monedas->where('moneda_id', $m)->first();
                    if (!$bm) {
                        $moneda = Moneda::find($m);
                        $monedaTC = null;
                        if ($tc) {
                            $monedaTC = $tc->cambios->where('moneda_id', $m)->first();
                        }

                        $moneda = $monedaTC ?: $moneda;

                        $bm = new BookingMoneda;
                        $bm->booking_id = $this->id;
                        $bm->moneda_id = $m;
                        $bm->rate = (float) $moneda->rate ?: 1;
                        $bm->save();
                    }
                }
            }

            BookingLog::addLog($this, "Monedas Booking");

            return true;
        }

        return false;
    }

    public function setCancelacion($total = 0)
    {
        if (!ConfigHelper::config('seguro')) {
            return null;
        }

        if (!$total) {
            if (!$this->precio_total) {
                return null;
            }

            $total = $this->precio_total['total'];
        }

        $extra = Extra::where('es_cancelacion', true)->where('cancelacion_rango1', '<=', $total)->where('cancelacion_rango2', '>=', $total)->first();

        if ($this->cancelacion) {
            //Ya estaba añadido
            return $this->cancelacion_seguro;
        }

        if (!$extra) {
            return null;
        }

        //Si es el mismo no actualiza
        $extra1 = $this->cancelacion_seguro;

        if ($extra1) {
            if ($extra1->id == $extra->id) {
                return $extra1;
            }
        }

        //Borramos los otros de cancelación
        $extras = Extra::where('es_cancelacion', true)->pluck('id')->toArray();
        foreach ($this->extras_generico as $e) {
            if (in_array($e->extra_id, $extras)) {
                $e->delete();
            }
        }

        $e = BookingExtra::add($extra, $this->id, 5, 1);

        $this->cancelacion = true;
        $this->save();

        return $e;
    }

    public function getCancelacionSeguroAttribute()
    {
        $extras = Extra::where('es_cancelacion', true)->pluck('id')->toArray();
        foreach ($this->extras_cancelacion as $e) {
            if (in_array($e->extra_id, $extras)) {
                return $e;
            }
        }

        return null;
    }

    public function getSeguroMedicoAttribute()
    {
        $extras = Extra::where('es_medico', true)->pluck('id')->toArray();
        foreach ($this->extras_generico as $e) {
            if (in_array($e->extra_id, $extras)) {
                return $e;
            }
        }

        return null;
    }

    public function updateCancelacion($total)
    {
        if ($this->cancelacion && $total) {
            return $this->setCancelacion($total);
        }

        return $this->cancelacion_seguro;
        // return null;
    }

    public function quitCancelacion()
    {
        //Borramos los otros de cancelación
        $extras = Extra::where('es_cancelacion', true)->pluck('id')->toArray();
        foreach ($this->extras_cancelacion as $e) {
            if (in_array($e->extra_id, $extras)) {
                $e->delete();
            }
        }

        $this->cancelacion = false;
        $this->save();
    }

    public function getDatosCampamentoPendienteAttribute()
    {
        if ($this->categoria && $this->categoria->es_info_campamento) {
            $log = $this->logs->where('tipo', "DatosCampamento")->sortByDesc('created_at')->first();
            if (!$log) {
                return true;
            }
        }

        return false;
    }

    public function updateDatosCampamento($request)
    {
        $bools = [
            //'campamento_enfermedad','campamento_alergia_1','campamento_alergia_2','campamento_alergia_3','campamento_medicacion',
            'campamento_enfermizo', 'campamento_operaciones', 'campamento_discapacidad',
            'campamento_aprendizaje', 'campamento_familiar', 'campamento_miedo'
        ];

        $data = $request->except('_token');
        if ($this->curso->categoria && $this->curso->categoria->es_info_campamento) {
            $datos_campamento = [];
            foreach ($data as $campo => $valor) {
                if (strpos($campo, "campamento_") !== false) {
                    if (!strpos($campo, "_bool")) {
                        if (in_array($campo, $bools)) {
                            $datos_campamento[$campo] = $valor;
                        }
                    } else {
                        $c = substr($campo, 0, strpos($campo, "_bool"));
                        $datos_campamento[$c] = $data[$campo] == 1 ? 1 : 0;
                    }

                    // unset($data[$campo]);
                }
            }

            $this->datos_campamento = $datos_campamento;
            $this->save();

            $fecha = Carbon::now()->format('d/m/Y - H:i');
            $ip = $request->ip();
            $notas = $request->user()->username . " ($fecha)[$ip]";

            BookingLog::addLog($this, "DatosCampamento", $notas);
        }
    }

    public function updateDatos($force = false, $vid = 0)
    {
        if ($this->datos && $this->datos_congelados && !$force) {
            return false;
        }

        if (!$this->viajero) {
            return false;
        }

        if (!$this->datos) {
            $datos = new BookingDatos;
            $datos->booking_id = $this->id;
            $datos->save();
        } else {
            $datos = $this->datos;
        }

        $viajero_id = $this->viajero_id;
        if ($force && $vid) {
            $viajero_id = $vid;
        }

        $viajero = Viajero::find($viajero_id);
        if (!$viajero->datos) {
            $vd = new ViajeroDatos;
            $vd->viajero_id = $viajero_id;
            $vd->save();
        }

        //mailBookingVueloBilletes
        $datos1 = clone $datos;
        $booking1 = clone $this;

        //origen_id,suborigen_id,suborigendet_id
        if (!$this->es_codigo) {
            $this->origen_id = $viajero->origen_id;
            $this->suborigen_id = $viajero->suborigen_id;
            $this->suborigendet_id = $viajero->suborigendet_id;
            $this->save();
        }

        if ($this->status_id) {
            if (($booking1->origen_id != $this->origen_id) || ($booking1->suborigen_id != $this->suborigen_id) || ($booking1->suborigendet_id != $this->suborigendet_id)) {
                \VCN\Models\Informes\Venta::updateBooking($this);
            }
        }

        if ($this->status) {
            $this->setContable();
        }

        $vdatos = $viajero->datos;

        $datos->name = $viajero->name;
        $datos->lastname = $viajero->lastname;
        $datos->lastname2 = $viajero->lastname2;
        $datos->full_name = $viajero->full_name;

        $datos->email = $viajero->email;
        $datos->phone = $viajero->phone;
        $datos->movil = $viajero->movil;
        $datos->fechanac = $viajero->fechanac;
        $datos->paisnac = $viajero->paisnac;
        $datos->sexo = $viajero->sexo;
        $datos->es_adulto = $viajero->es_adulto;
        $datos->emergencia_contacto = $viajero->emergencia_contacto;
        $datos->emergencia_telefono = $viajero->emergencia_telefono;
        $datos->save();

        if ($vdatos) {
            $datos->nacionalidad = $vdatos->nacionalidad;
            $datos->documento = $vdatos->documento;
            $datos->tipodoc = $vdatos->tipodoc;
            $datos->pasaporte = $vdatos->pasaporte;
            $datos->pasaporte_pais = $vdatos->pasaporte_pais;
            $datos->pasaporte_emision = $vdatos->pasaporte_emision;
            $datos->pasaporte_caduca = $vdatos->pasaporte_caduca;
            $datos->tipovia = $vdatos->tipovia;
            $datos->direccion = $vdatos->direccion;
            $datos->ciudad = $vdatos->ciudad;
            $datos->provincia = $vdatos->provincia;
            $datos->pais = $vdatos->pais;
            $datos->cp = $vdatos->cp;
            $datos->idioma = $vdatos->idioma;
            $datos->idioma_nivel = $vdatos->idioma_nivel;
            $datos->titulacion = $vdatos->titulacion;

            $datos->alergias = $vdatos->alergias;
            $datos->medicacion = $vdatos->medicacion;
            $datos->enfermedad = $vdatos->enfermedad;
            $datos->tratamiento = $vdatos->tratamiento;
            $datos->dieta = $vdatos->dieta;

            $datos->alergias2 = $vdatos->alergias2;
            $datos->medicacion2 = $vdatos->medicacion2;
            $datos->enfermedad2 = $vdatos->enfermedad2;
            $datos->tratamiento2 = $vdatos->tratamiento2;
            $datos->dieta2 = $vdatos->dieta2;

            $datos->hobby = $vdatos->hobby;
            $datos->animales = $vdatos->animales;
            $datos->fumador = $vdatos->fumador;
            $datos->profesion = $vdatos->profesion;
            $datos->empresa = $vdatos->empresa;
            $datos->empresa_phone = $vdatos->empresa_phone;
            $datos->curso_anterior = $vdatos->curso_anterior;

            $datos->provincia = $vdatos->provincia ? $vdatos->provincia : "-";
            $datos->pais = $vdatos->pais ? $vdatos->pais : "-";

            $datos->provincia_id = $vdatos->provincia_id;
            $datos->pais_id = $vdatos->pais_id;

            $datos->escuela = $vdatos->escuela;
            $datos->escuela_curso = $vdatos->escuela_curso;
            $datos->grado_ext = $vdatos->grado_ext;
            $datos->ingles = $vdatos->ingles;
            $datos->ingles_academia = $vdatos->ingles_academia;
            $datos->hermanos = $vdatos->hermanos;
            $datos->telefonos = $vdatos->telefonos;
            $datos->idioma_contacto = $vdatos->idioma_contacto;

            $datos->h1_nom = $vdatos->h1_nom;
            $datos->h1_fnac = $vdatos->h1_fnac;
            $datos->h2_nom = $vdatos->h2_nom;
            $datos->h2_fnac = $vdatos->h2_fnac;
            $datos->h3_nom = $vdatos->h3_nom;
            $datos->h3_fnac = $vdatos->h3_fnac;
            $datos->h4_nom = $vdatos->h4_nom;
            $datos->h4_fnac = $vdatos->h4_fnac;

            $datos->fact_nif = $datos->fact_nif ?: $vdatos->fact_nif;
            $datos->fact_razonsocial = $datos->fact_razonsocial ?: $vdatos->fact_razonsocial;
            $datos->fact_domicilio = $datos->fact_domicilio ?: $vdatos->fact_domicilio;
            $datos->fact_cp = $datos->fact_cp ?: $vdatos->fact_cp;
            $datos->fact_poblacion = $datos->fact_poblacion ?: $vdatos->fact_poblacion;
            $datos->fact_concepto = $datos->fact_concepto ?: $vdatos->fact_concepto;

            $datos->cic = $vdatos->cic;
            $datos->cic_nivel = $vdatos->cic_nivel;
            $datos->cic_thau = $vdatos->cic_thau;
            $datos->trinity_exam = $vdatos->trinity_exam;
            $datos->trinity_any = $vdatos->trinity_any;
            $datos->trinity_nivel = $vdatos->trinity_nivel;
            $datos->save();
        }

        $datos->notas = $this->notas;
        if($viajero->notas)
        {
            $datos->notas .= "<br><br>". $viajero->notas;
        }

        $datos->tutores = $viajero->tutores->toArray();

        $datos->save();

        //mailBookingVueloBilletes
        $campos = ['name', 'lastname', 'lastname2', 'fechanac', 'sexo', 'nacionalidad', 'pasaporte', 'pasaporte_pais', 'pasaporte_emision', 'pasaporte_caduca'];

        $datos2 = clone $datos;

        if ($this->vuelo) {
            if ($this->vuelo->billetes) {
                $bMailBilletes = false;
                $cambios = [];
                $inicial = [];

                foreach ($campos as $campo) {
                    if ($datos2->$campo != $datos1->$campo) {
                        $inicial[$campo] = $datos1->$campo;
                        $cambios[$campo] = $datos2->$campo;
                        $bMailBilletes = true;
                    }
                }

                if ($bMailBilletes) {
                    MailHelper::mailBookingVueloBilletes($this, $cambios, $inicial);
                }
            }
        }
    }

    public function facturaManual($input)
    {
        $plat = ConfigHelper::config('propietario');

        $total = $this->facturable;

        $fecha = Carbon::createFromFormat('d/m/Y', $input['fecha']);
        $num = $input['numero'];

        $parcial = null;
        if ($input['importe']) {
            $parcial = $input['importe'];
        }

        $razonsocial = $input['razonsocial'];
        $cif = $input['cif'];

        $factura = new BookingFactura;
        $factura->booking_id = $this->id;
        $factura->user_id = auth()->user()->id;
        $factura->numero = $num;
        $factura->total = $parcial ?: $total;
        $factura->fecha = $fecha;
        $factura->plataforma = $this->plataforma;
        $factura->nif = $cif;
        $factura->cp = $input['cp'];
        $factura->manual = true;

        $direccion = ($this->datos->fact_domicilio ? $this->datos->fact_domicilio : $this->datos->direccion) . ", " . ($this->datos->fact_cp ? $this->datos->fact_cp : $this->datos->cp) . " " . ($this->datos->fact_poblacion ? $this->datos->fact_poblacion : $this->datos->ciudad);
        $datos = [
            'razonsocial'   => $razonsocial,
            'direccion'     => $direccion,
            'cif'           => $cif,
            'cp'            => $input['cp'],
            'concepto1'     => $this->datos->fact_concepto ? $this->datos->fact_concepto : $this->curso->name,
            'concepto2'     => $this->datos->fact_concepto ? "" : ConfigHelper::parseMoneda($this->course_total_amount, $this->curso_moneda),
        ];
        $factura->datos = $datos;
        $factura->precio_total = $this->precio_total;
        $factura->divisa_variacion = $this->divisa_variacion ?: 0;
        $factura->total_divisa_txt = $this->total_divisa_txt;

        $monedas = "";
        foreach ($this->monedas_usadas_txt as $mu) {
            $monedas .= "<small>$mu</small><br>";
        }
        $factura->monedas = $monedas;

        $factura->save();
    }

    public function facturaPDFManual($num)
    {
        $dir = storage_path("files/bookings/" . $this->viajero->id . "/");

        //Pdf
        $name = "factura_" . $num . ".pdf";
        $file = $dir . $name;
        $lang = $this->idioma_contacto;

        $info = ConfigHelper::config_plataforma();
        $parcial = 0;

        $template = 'manage.bookings.factura';

        $p = $this->plataforma;
        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView($template, ['ficha' => $this, 'factura' => $this->factura, 'info' => $info, 'parcial' => $parcial]);

        $pdf->setOption('margin-top', 30);
        $pdf->setOption('margin-right', 0);
        $pdf->setOption('margin-bottom', 30);
        $pdf->setOption('margin-left', 0);
        $pdf->setOption('no-print-media-type', false);
        $pdf->setOption('footer-spacing', 0);
        $pdf->setOption('header-font-size', 9);
        $pdf->setOption('header-spacing', 0);

        $pdf->setOption('header-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'header.html');
        $pdf->setOption('footer-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'footer.html');

        $pdf->save($file);
        // dd("factura $num");
    }

    public function facturaPDFupdate()
    {
        if (!$this->factura) {
            return;
        }

        $dir = storage_path("files/bookings/" . $this->viajero->id . "/");
        $num = $this->factura->numero;

        //Pdf
        $name = "factura_" . $num . ".pdf";
        $file = $dir . $name;
        $lang = $this->idioma_contacto;

        //borramos
        if (File::exists($file)) {
            File::delete($file);
        }

        $info = ConfigHelper::config_plataforma();
        $parcial = $this->factura->parcial_importe;

        $template = 'manage.bookings.factura';

        $p = $this->plataforma;
        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView($template, ['ficha' => $this, 'factura' => $this->factura, 'info' => $info, 'parcial' => $parcial]);

        $pdf->setOption('margin-top', 30);
        $pdf->setOption('margin-right', 0);
        $pdf->setOption('margin-bottom', 30);
        $pdf->setOption('margin-left', 0);
        $pdf->setOption('no-print-media-type', false);
        $pdf->setOption('footer-spacing', 0);
        $pdf->setOption('header-font-size', 9);
        $pdf->setOption('header-spacing', 0);

        $pdf->setOption('header-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'header.html');
        $pdf->setOption('footer-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'footer.html');

        $pdf->save($file);
    }

    public function getContableMultiAttribute()
    {
        $contable = null;
        $especialidades = $this->especialidades;

        foreach ($especialidades as $e) {
            if ($e->especialidad && $e->especialidad->contable) {
                $sem = $e->n;
                $c = explode(',', $e->especialidad->contable);
                if (isset($c[$sem])) {
                    $contable .= trim($c[$sem]);
                    $contable .= ",";
                }
            }
        }

        //quitamos la ultima coma
        return substr($contable, 0, -1);
    }

    public static function online_crear($curso_id, Viajero $viajero = null, $tutor_id = null, $convo_id = 0)
    {
        if (!$viajero) {
            $viajero = Viajero::find(0);
            if (!$viajero) {
                $viajero = new Viajero;
                $viajero->name = null;
                $viajero->save();
                $viajero->id = 0;
                $viajero->save();

                $vd = new ViajeroDatos;
                $vd->viajero_id = 0;
                $vd->save();
            }
        }

        $asign_to = ConfigHelper::config('tpv_asign_to');

        $ip = Request::ip();
        $user = auth()->user();

        $online = [
            'ip' => $ip,
            'creador_id' => $user->id,
        ];

        if ($viajero->id && $viajero->asign_to) {
            $asign_to = $viajero->asign_to;
        }

        $curso = Curso::find($curso_id);

        $booking = new self; //creamos new booking;
        $booking->viajero_id = $viajero->id;
        $booking->curso_id = $curso_id;
        $booking->category_id = $curso->category_id;
        $booking->subcategory_id = $curso->subcategory_id;
        $booking->subcategory_det_id = $curso->subcategory_det_id;
        $booking->user_id = $asign_to;
        $booking->plataforma = ConfigHelper::propietario();
        $booking->es_online = 1;
        $booking->online = $online;
        $booking->area = true;
        $booking->area_pagos = true;
        $booking->area_reunion = true;
        $booking->fase = 1;
        $booking->es_pdf = false;
        $booking->oficina_id = $viajero->oficina_asignada;
        $booking->status_id = 0;
        $booking->mail_confirmacion = true;

        //Curso corporativo
        if ($convo_id) {
            $campo = $curso->convocatoria_campo;
            $booking->$campo = (int) $convo_id;
        }

        if (!$viajero->id) {
            $booking->tutor_id = $tutor_id;
            $booking->prescriptor_id = 0;
        }

        $booking->promo_cambio_fijo = $booking->promo_cambio_fijo_default;
        $booking->tpv_plazas = Carbon::now();
        $booking->save();

        $booking_id = $booking->id;

        Session::put('vcn.booking_id', $booking_id);

        $booking->setExtras();
        $booking->prescriptor_no_facturar;

        $booking->updateDatos(true);

        $viajero_id = $booking->viajero_id;
        if ($viajero_id) {
            BookingLog::addLog($booking, "Nuevo - Curso [$curso_id] - VIAJERO [$viajero_id] - ONLINE [$ip]");
            ViajeroLog::addLog($booking->viajero, "Nuevo Booking ONLINE [$booking->id]");
        } else {
            BookingLog::addLog($booking, "Nuevo - Curso [$curso_id] - TUTOR - ONLINE [$ip]");
        }

        Session::forget('vcn.booking_user');

        //     $booking->cancelacion = false;
        //     $booking->save();

        return $booking;
    }

    public function route_getUpdate()
    {
        $ficha = $this;
        $user = auth()->user();

        if (!$ficha->datos) {
            $ficha->updateDatos();
        }

        if ($ficha->viajero->setBooking()) {
            // $ficha = $this->booking->find($id);
        }

        if (!$ficha->es_presupuesto && !$ficha->viajero->es_cliente) {
            $ficha->viajero->setInscrito(true);

            $ficha->viajero->booking_status_id = 0;
            $ficha->viajero->save();

            //Archivamos solicitud
            $solicitud = $ficha->viajero->solicitud;
            if ($solicitud && $solicitud->id == $ficha->solicitud_id) {
                $solicitud->setInscrito("booking-update");
            }
        }

        $ficha->prescriptor_no_facturar;

        if (!$ficha->viajero->datos) {
            $data['viajero_id'] = $ficha->viajero_id;
            $this->datos->create($data);
        }

        $monedas = Moneda::pluck('currency_name', 'id');
        $descuentos = DescuentoTipo::plataforma()->pluck('full_name', 'id');

        $monedas_cambio = Moneda::pluck('currency_rate', 'id')->toArray();

        $total = $ficha->precio_total;

        $prescriptores = [0 => "Sin Prescriptor"] + Prescriptor::plataforma()->pluck('name', 'id')->toArray();
        $oficinas = Oficina::plataforma()->pluck('name', 'id')->toArray();

        // ====== BOOKING COMPLETO => FICHA ======
        if ($ficha->es_terminado) //Completo
        {
            $familias = null;
            if ($ficha->alojamiento) {
                if ($ficha->alojamiento->tipo->es_familia) {
                    $familias = Familia::where('center_id', $ficha->curso->centro->id)->where('activo', 1)->orderBy('name')->pluck('name', 'id');
                }
            }
            $familias = Familia::where('center_id', $ficha->curso->center_id)->where('activo', 1)->pluck('name', 'id');

            $schools = School::where('center_id', $ficha->curso->center_id)->where('activo', 1)->pluck('name', 'id');

            $ficha->familiaOcupadaCheck();

            if (count($ficha->monedas_usadas) > 0) {
                if ($ficha->facturas->count() || $ficha->es_notapago35_generada) {
                    $ficha->setMonedasCambio();
                }
            }

            $template = 'manage.bookings.ficha';
            if ($user->es_cliente) {
                $template = 'comprar.booking';
            }
            $booking = $ficha;
            return view($template, compact('ficha', 'booking', 'prescriptores', 'oficinas', 'descuentos', 'monedas', 'monedas_cambio', 'total', 'familias', 'schools'));
        }
        // ====== BOOKING COMPLETO => FICHA ======

        //si no está completo
        $ficha->updateExtras(true);

        $semanas = ConfigHelper::getSemanas($ficha->duracion_name);

        //descuentos
        $descuentos = $ficha->getDescuentos();
        $descuento = null;
        if ($descuentos->count() > 0) {
            $descuento = $descuentos->first();
        }

        $conocidos = ["" => ""] + Origen::plataforma()->pluck('name', 'id')->toArray();
        $subconocidos = Suborigen::where('origen_id', $ficha->origen_id)->pluck('name', 'id');
        $subconocidosdet = SuborigenDetalle::where('suborigen_id', $ficha->suborigen_id)->pluck('name', 'id');

        $tutores = []; //Tutor::plataforma()->pluck('full_name', 'id');
        $monedas = Moneda::all()->pluck('currency_name', 'id');

        $descuentos = DescuentoTipo::plataforma()->pluck('full_name', 'id');

        //convo multi: fechas y semanas
        $multi = [];
        if ($ficha->curso->es_convocatoria_multi) {
            if ($ficha->curso->convocatorias_multi->count() > 1) //hay varias cmulti
            {
                $semanas = collect();
                foreach ($ficha->curso->convocatorias_multi as $cm) {
                    foreach ($cm->semanas as $s) {
                        $semanas->push($s);
                    }
                }

                $msemanas = [];
                $mfdesde = [];

                $msemanas[0] = 0;

                $mtots = 0;
                foreach ($semanas as $s) {
                    $msemanas[$s->semana] = $s->semana;
                    $mfdesde[$s->id] = Carbon::parse($s->desde)->format('d/m/Y') . " [" . $s->convocatoria->name . "]";

                    $mtots++;
                }

                if ($ficha->convocatoria) {
                    $desde_id = $ficha->convocatoria->semanas->where('desde', $ficha->course_start_date)->first();
                    $desde_id = $desde_id ? $desde_id->id : $semanas->first()->id;
                } else {
                    $desde_id = $semanas->first()->id;
                }
            } else {
                $desde_id = null;
                if ($ficha->convocatoria) {
                    if ($ficha->convocatoria->semanas) {
                        $desde_id = $ficha->convocatoria ? $ficha->convocatoria->semanas->where('desde', $ficha->course_start_date)->first() : null;
                        $desde_id = $desde_id ? $desde_id->id : $ficha->convocatoria->semanas->first()->id;
                    }
                }

                $msemanas = [];
                $mfdesde = [];

                $msemanas[0] = 0;

                $mtots = 0;

                if ($ficha->convocatoria) {
                    foreach ($ficha->convocatoria->semanas as $s) {
                        //$s->id ??
                        //$msemanas[$s->semana] = $s->semana;
                        $mfdesde[$s->id] = Carbon::parse($s->desde)->format('d/m/Y') . " [" . $s->bloque . " " . str_plural('Semana', $s->bloque) . "]";

                        $mtots += $s->bloque;
                    }

                    for ($i = 0; $i <= $mtots; $i++) {
                        $msemanas[$i] = $i;
                    }
                }
            }

            $multi = [
                'mtotsemanas' => $mtots,
                'mfdesde_id' => $desde_id,
                'msemanas' => $msemanas,
                'mfdesde' => $mfdesde,
            ];

            // dd($multi);
        }

        $template = 'manage.bookings.new';
        if ($user->es_cliente) {
            $template = 'comprar.booking';
        }
        $booking = $ficha;

        return view($template, compact('ficha', 'booking', 'semanas', 'multi', 'descuento', 'conocidos', 'subconocidos', 'subconocidosdet', 'tutores', 'monedas', 'monedas_cambio', 'monedas_cambio', 'descuentos', 'prescriptores', 'oficinas', 'total'));
    }

    public function getEsOnlineTpvAttribute()
    {
        if (!$this->es_online) {
            return false;
        }

        if (isset($this->online['tpv'])) {
            return (bool) $this->online['tpv'];
        }

        return true;
    }

    public function getEsPlazaOnlineAlojamiento($convo_id = 0, $alojamiento_id = 0)
    {
        if (!$this->es_online) {
            return false;
        }

        if (!$convo_id) {
            $convo_id = $this->convocatory_close_id;
        }

        if (!$alojamiento_id) {
            $alojamiento_id = $this->alojamiento_id;
        }

        if ($this->convocatory_close_id != $convo_id) {
            return false;
        }

        if ($this->alojamiento_id != $alojamiento_id) {
            return false;
        }

        if (!$convo_id) {
            return false;
        }

        $hora = Carbon::now();
        $hora1 = Carbon::now()->subHour();
        $hora2 = Carbon::now()->subHours(96);

        //tpv_plazas : 1hora
        $p1 = Booking::where('id', $this->id)->where('convocatory_close_id', $convo_id)->where('accommodation_id', $alojamiento_id)
            ->where('es_online', 1)->where('status_id', 0)->where('tpv_plazas', '>=', $hora1)->where('tpv_plazas', '<=', $hora);

        //tpv_plazas : 3dias (backend)
        $p2 = Booking::where('id', $this->id)->where('convocatory_close_id', $convo_id)->where('accommodation_id', $alojamiento_id)
            ->where('es_online', 2)->where('status_id', 0)->where('tpv_plazas', '>=', $hora2)->where('tpv_plazas', '<=', $hora);

        return ($p1->count() || $p2->count());
    }

    public function getEsPlazaOnlineVuelo($vuelo_id = 0)
    {
        if (!$this->es_online) {
            return false;
        }

        if (!$vuelo_id) {
            $vuelo_id = $this->vuelo_id;
        }

        if ($this->vuelo_id != $vuelo_id) {
            return false;
        }

        if (!$vuelo_id) {
            return false;
        }

        $hora = Carbon::now();
        $hora1 = Carbon::now()->subHour();
        $hora2 = Carbon::now()->subHours(96);

        //tpv_plazas : 1hora
        $p1 = Booking::where('id', $this->id)->where('vuelo_id', $vuelo_id)->where('es_online', 1)->where('status_id', 0)
            ->where('tpv_plazas', '>=', $hora1)->where('tpv_plazas', '<=', $hora);

        //tpv_plazas : 3dias (backend)
        $p2 = Booking::where('id', $this->id)->where('vuelo_id', $vuelo_id)->where('es_online', 2)->where('status_id', 0)
            ->where('tpv_plazas', '>=', $hora2)->where('tpv_plazas', '<=', $hora);

        return ($p1->count() || $p2->count());
    }

    public function getEsOnlineComprobanteAttribute()
    {
        if ($this->es_online_tpv) {
            return false;
        }

        if (isset($this->online['comprobante'])) {
            return $this->online['comprobante'];
        }

        return false;
    }

    public function getOnlineReservaAttribute()
    {
        $reserva = 0;
        $total = $this->total_sin_cancelacion;
        // convocatoria > curso > subcategoria > categoria

        if (!$reserva) {
            if ($this->convocatoria && $this->convocatoria->reserva_valor) {
                if ($this->convocatoria->reserva_tipo) {
                    $reserva = round((($this->convocatoria->reserva_valor * $total) / 100), 2) ?: null;
                } else {
                    $reserva = round($this->convocatoria->reserva_valor, 2) ?: null;
                }
            }
        }

        if (!$reserva) {
            if ($this->curso->reserva_valor) {
                if ($this->curso->reserva_tipo) {
                    $reserva = round((($this->curso->reserva_valor * $total) / 100), 2) ?: null;
                } else {
                    $reserva = round($this->curso->reserva_valor, 2) ?: null;
                }
            }
        }

        if (!$reserva) {
            if ($this->curso->subcategoria->reserva_valor) {
                if ($this->curso->subcategoria->reserva_tipo) {
                    $reserva = round((($this->curso->subcategoria->reserva_valor * $total) / 100), 2) ?: null;
                } else {
                    $reserva = round($this->curso->subcategoria->reserva_valor, 2) ?: null;
                }
            }
        }

        if (!$reserva) {
            if ($this->curso->categoria->reserva_valor) {
                if ($this->curso->categoria->reserva_tipo) {
                    $reserva = round((($this->curso->categoria->reserva_valor * $total) / 100), 2) ?: null;
                } else {
                    $reserva = round($this->curso->categoria->reserva_valor, 2) ?: null;
                }
            }
        }

        //Seguro cancelacion
        $seguro = $this->cancelacion_seguro;
        $seguroi = 0;
        if ($seguro) {
            // $m = Moneda::find($seguro->moneda_id);
            $seguroi = $seguro->precio;
        }

        return $reserva + $seguroi;
    }

    public function online_delete()
    {
        //Borrar pdfs
        $dir = storage_path("files/bookings/" . $this->viajero->id . "/");
        $files = "ooking_$this->id-";
        $filesp = "_" . $this->id . ".pdf";

        if (file_exists($dir)) {
            foreach (File::files($dir) as $f) {
                if (strpos($f, $files)) {
                    File::delete($f);
                }

                if (strpos($f, $filesp)) {
                    File::delete($f);
                }
            }
        }

        ViajeroLog::addLog($this->viajero, "Booking Eliminado [$this->id] [ONLINE]");

        parent::delete();
    }

    public function getTpvTiempoAttribute()
    {
        if (!$this->tpv_plazas) {
            $this->tpv_plazas = Carbon::now();
            $this->save();
        }
        $time = clone $this->tpv_plazas;

        $abs = App::environment('local');

        return Carbon::now()->diffInMinutes($time->addHours(96), $abs);

        if ($this->online_es_backend) {
            return Carbon::now()->diffInMinutes($time->addHours(96), $abs);
        }

        return Carbon::now()->diffInMinutes($time->addMinutes(30), $abs);
    }

    public function getTpvTiempoAgotadoAttribute()
    {
        return $this->tpv_tiempo <= 0;
    }

    public function getOnlinePlazasAttribute()
    {
        //tpv_plazas : 1hora
        $hora = Carbon::now();
        $hora1 = Carbon::now()->subHour();

        if ($this->online_es_backend) {
            $hora1 = Carbon::now()->subHours(72); //3 días
        }

        // ->where('tpv_plazas','>=',$hora1)->where('tpv_plazas','<=',$hora);

        if ($this->curso->es_convocatoria_cerrada) {
            $plazas_vuelo = $this->vuelo ? $this->vuelo->plazas_disponibles : 1;
            if (!$this->alojamiento) {
                $plazas_a = null;
                $plazas_alojamiento = 1;
            } else {
                $plazas_a = $this->convocatoria->getPlazas($this->alojamiento->id);
                $plazas_alojamiento = $plazas_a ? $plazas_a->plazas_disponibles : 0;
            }

            if ($plazas_vuelo && $plazas_alojamiento) {
                return true;
            }

            //Si no  hay plazas pero las suyas estaban reservadas
            return ($this->tpv_plazas->gte($hora1) && $this->tpv_plazas->lte($hora));
        }

        if ($this->curso->es_convocatoria_multi) {
            $plazas = $this->convocatoria->hayPlazasDisponibles($this->id);

            if ($plazas) {
                return true;
            }

            //Si no  hay plazas pero las suyas estaban reservadas
            return ($this->tpv_plazas->gte($hora1) && $this->tpv_plazas->lte($hora));
        }

        return true;
    }

    public function online_aeropuertos()
    {
        if (!$this->vuelo) {
            return false;
        }

        $tpv_aeropuertos = ConfigHelper::config('tpv_aeropuertos');
        if (!$tpv_aeropuertos) {
            $this->extraVuelo();
        } else {
            //BCN/MAD/OTRO
            if ($this->transporte_recogida != "BCN" && $this->transporte_recogida != "MADRID") {
                $this->extraVuelo();
            }
        }
    }

    public function mailAvisoCambios()
    {
        if (!$this->facturas->count()) {
            return false;
        }

        BookingLog::addLog($this, "Mail Cambio Importes Booking.");
        MailHelper::mailCambioImportes($this->id);
    }


    public function mb($en_divisa = false)
    {
        $mid = ConfigHelper::default_moneda_id();
        // $mRate = $this->monedas->where('moneda_id',$mid)->first();
        // $mRate = (float)($mRate?$mRate->rate:1);
        $mRate = 1;

        if ($this->es_convocatoria_cerrada) {
            $p = $this->convocatoria->precio_auto;
            if ($p) {
                $v = (float) $p->mb;
                if ($v) {
                    if ($p->mb_moneda_id && $p->mb_moneda_id != $mid && !$en_divisa) {
                        // $mRate = $this->monedas->where('moneda_id', $p->mb_moneda_id)->first();
                        // $mRate = (float)($mRate?$mRate->rate:1);

                        $mRate = (float) ConfigHelper::monedaCambio($p->mb_moneda_id, $this->id);
                    }

                    return round(($p->mb * $mRate), 0);
                }
                // else

                $v = (float) $p->proveedor;
                if ($v) {
                    if ($p->proveedor_moneda_id && $p->proveedor_moneda_id != $mid && !$en_divisa) {
                        // $mRate = $this->monedas->where('moneda_id', $p->proveedor_moneda_id)->first();
                        // $mRate = (float)($mRate?$mRate->rate:1);

                        $mRate = (float) ConfigHelper::monedaCambio($p->proveedor_moneda_id, $this->id);
                    }

                    $c = ($p->proveedor * $this->curso->commission);

                    return round(($c * $mRate) / 100, 0);
                }
            }

            return 0;
        }

        if ($this->es_convocatoria_abierta) {
            $t = $this->total_curso_neto;
            $c = ($this->total_curso * $this->curso->commission);

            if (!$en_divisa) {
                $mRate = (float) ConfigHelper::monedaCambio($this->course_currency_id, $this->id);
            }

            return round(($c * $mRate) / 100, 0);
        }

        return 0;
    }


    public function onlineFin()
    {
        if (!$this->es_online) {
            return;
        }

        $u = $this->viajero->user;
        if ($u && $u->compra) {
            if ($u->compra['booking_id'] == $this->id) {
                $u->compra = null;
                $u->save();
            }
        }

        $viajero = $this->viajero;
        if ($viajero->tutor1 && $viajero->tutor1->user) {
            $u = $viajero->tutor1->user;
            if ($u && $u->compra)
            {
                if ($u->compra['booking_id'] == $this->id) {
                    $u->compra = null;
                    $u->save();
                }
            }
        }

        if ($viajero->tutor2 && $viajero->tutor2->user) {
            $u = $viajero->tutor2->user;
            if ($u && $u->compra)
            {
                if ($u->compra['booking_id'] == $this->id) {
                    $u->compra = null;
                    $u->save();
                }
            }
        }
    }

    public function getOnlineTutorAttribute()
    {
        if ($this->es_online) {
            $b = '%"booking_id":' . $this->id . '%';
            $u = User::where('compra', 'LIKE', $b)->first();
            return $u;
        }

        return null;
    }

    public function getOnlineCreadorAttribute()
    {
        $ret = "-";
        $creado = null;

        if ($this->es_online) {
            if ($this->tutor_id) {
                $creado = $this->tutor ? $this->tutor->user : null;
            } else {
                if (isset($this->online['creador_id'])) {
                    $creado = User::find($this->online['creador_id']);
                }
                // else
                // {
                //     $b = '%"booking_id":'. $this->id .'%';
                //     $creado = User::where('compra', 'LIKE', $b)->first();
                // }
            }

            if (!$creado) {
                return "BACKEND";
            }
        }

        if ($creado && $creado->ficha) {
            $ret = $creado->full_name . "($creado->role_name:" . $creado->ficha->id . ")";
            if ($this->tutor_id) {
                $ret = "<a target='_blank' href='" . route('manage.tutores.ficha', $this->tutor_id) . "'>$ret</a>";
            }
        }

        return $ret;
    }

    public function getOnlineEsBackendAttribute()
    {
        if ($this->es_online == 2) {
            return true;
        }

        if ($this->es_online) {
            if (isset($this->online['es_backend'])) {
                return (bool) $this->online['es_backend'];
            }
        }

        return false;
    }

    public function getOnlineBackendUserAttribute()
    {
        if ($this->es_online) {
            if (isset($this->online['backend_user_id'])) {
                return User::find((int) $this->online['backend_user_id']);
            }
        }

        return null;
    }

    public function getOnlineDestinatariosAttribute()
    {
        $ret = [];

        if ($this->viajero) {
            $viajero = $this->viajero;

            $user = $viajero->user;

            if ($user) {
                $nom = "Viajero: " . $user->ficha->full_name;
                if ($user->compra) {
                    $nom .= " [Tiene booking]";
                }
                $ret[$user->id] = $nom;
            } elseif ($viajero->email) {
                $nom = "Viajero: " . $viajero->full_name;
                $ret["v"] = $nom;
            }

            if ($viajero->tutor1) {
                $user = $viajero->tutor1->user;

                if ($user) {
                    $nom = "Tutor1: " . $user->ficha->full_name;
                    if ($user->compra) {
                        $nom .= " [Tiene booking]";
                    }
                    $ret[$user->id] = $nom;
                } elseif ($viajero->tutor1->email) {
                    $nom = "Tutor1: " . $viajero->tutor1->full_name;
                    $ret["t1"] = $nom;
                }
            }

            if ($viajero->tutor2) {
                $user = $viajero->tutor2->user;

                if ($user) {
                    $nom = "Tutor2: " . $user->ficha->full_name;
                    if ($user->compra) {
                        $nom .= " [Tiene booking]";
                    }
                    $ret[$user->id] = $nom;
                } elseif ($viajero->tutor2->email) {
                    $nom = "Tutor2: " . $viajero->tutor2->full_name;
                    $ret["t2"] = $nom;
                }
            }
        }

        return $ret;
    }

    public function setPdfCondiciones($file)
    {
        $dir = storage_path("files/bookings/" . $this->viajero->id);
        $pdf = $dir . "/" . $this->id . "_pdf_condiciones.pdf";

        if (file_exists($file)) {
            if (!file_exists($dir)) {
                File::makeDirectory($dir, 0775, true);
            }

            File::copy($file, $pdf);
        }
    }

    public function getPdfCondicionesAttribute()
    {
        //Convo > Curso > SCat > Cat

        $pdf = storage_path("files/bookings/" . $this->viajero->id . "/") . $this->id . "_pdf_condiciones.pdf";
        if (file_exists($pdf)) {
            return $pdf;
        }

        $plataforma = $this->plataforma;
        $idioma = $this->idioma_contacto;

        $ret = null;

        $dir = "assets/uploads/pdf_condiciones/";
        $dir = public_path($dir);

        if ($this->convocatoria && $this->convocatoria->getCondiciones($plataforma, $idioma)) {
            $name = class_basename($this->convocatoria) . "_" . $this->convocatoria->id;
            $file = $name . "_" . $plataforma . "_" . $idioma . ".pdf";

            if (file_exists($dir . $file)) {
                $ret = $dir . $file;
            } else {
                $file = $name . "_" . 0 . "_" . $idioma . ".pdf";
                if (file_exists($dir . $file)) {
                    $ret = $dir . $file;
                }
            }

            if ($ret) {
                $this->setPdfCondiciones($ret);
                return $ret;
            }
        }

        if ($this->curso && $this->curso->getCondiciones($plataforma, $idioma)) {
            $name = class_basename($this->curso) . "_" . $this->curso->id;
            $file = $name . "_" . $plataforma . "_" . $idioma . ".pdf";

            if (file_exists($dir . $file)) {
                $ret = $dir . $file;
            } else {
                $file = $name . "_" . 0 . "_" . $idioma . ".pdf";
                if (file_exists($dir . $file)) {
                    $ret = $dir . $file;
                }
            }

            if ($ret) {
                $this->setPdfCondiciones($ret);
                return $ret;
            }
        }

        if ($this->subcategoria && $this->subcategoria->getCondiciones($plataforma, $idioma)) {
            $name = class_basename($this->subcategoria) . "_" . $this->subcategoria->id;
            $file = $name . "_" . $plataforma . "_" . $idioma . ".pdf";

            if (file_exists($dir . $file)) {
                $ret = $dir . $file;
            } else {
                $file = $name . "_" . 0 . "_" . $idioma . ".pdf";
                if (file_exists($dir . $file)) {
                    $ret = $dir . $file;
                }
            }

            if ($ret) {
                $this->setPdfCondiciones($ret);
                return $ret;
            }
        }

        if ($this->categoria && $this->categoria->getCondiciones($plataforma, $idioma)) {
            $name = class_basename($this->categoria) . "_" . $this->categoria->id;
            $file = $name . "_" . $plataforma . "_" . $idioma . ".pdf";

            if (file_exists($dir . $file)) {
                $ret = $dir . $file;
            } else {
                $file = $name . "_" . 0 . "_" . $idioma . ".pdf";
                if (file_exists($dir . $file)) {
                    $ret = $dir . $file;
                }
            }

            if ($ret) {
                $this->setPdfCondiciones($ret);
                return $ret;
            }
        }

        return null;
    }

    public function getTxtCondicionesAttribute()
    {
        //Convo > Curso > SCat > Cat

        $plataforma = $this->plataforma;
        $idioma = $this->idioma_contacto;

        $ret = null;

        if ($this->convocatoria) {
            $cond = $this->convocatoria->getCondiciones($plataforma, $idioma);

            if ($cond) {
                return $cond;
            }
        }

        if ($this->curso) {
            $cond = $this->curso->getCondiciones($plataforma, $idioma);

            if ($cond) {
                return $cond;
            }
        }

        if ($this->subcategoria) {
            $cond = $this->subcategoria->getCondiciones($plataforma, $idioma);

            if ($cond) {
                return $cond;
            }
        }

        if ($this->categoria) {
            $cond = $this->categoria->getCondiciones($plataforma, $idioma);

            if ($cond) {
                return $cond;
            }
        }

        return null;
    }

    public function getBancoEmailAttribute()
    {
        if ($this->es_corporativo) {
            $corp = $this->corporativo_info;
            if ($corp && isset($corp['email']) && $corp['email']) {
                return $corp['email'];
            }
        }

        if ($this->oficina && $this->oficina->email) {
            return $this->oficina->email;
        }

        return ConfigHelper::config('email');
    }

    public function getBancoCccAttribute()
    {
        $plataforma = \VCN\Models\System\Plataforma::find($this->plataforma);
        $titular = $plataforma->factura_pie;
        $titular = $titular ? "[Titular: $titular]" : "";

        if ($this->es_corporativo) {
            $corp = $this->corporativo_info;
            if ($corp && isset($corp['banco']) && $corp['banco'])
            {
                if (isset($corp['titular']) && $corp['titular'])
                {
                    $titular = $corp['titular'];
                    $titular = "[Titular: $titular]";
                }
                
                return $corp['banco'] . ": " . $corp['iban'] . " $titular";
            }
        }

        if ($this->categoria) {
            $cat = $this->categoria;
            if ($cat->banco) {
                return $cat->banco . ": " . $cat->iban . " $titular";
            }
        }

        if ($this->oficina && $this->oficina->banco) {
            $ofi = $this->oficina;
            return $ofi->banco . ": " . $ofi->iban . " $titular";
        }

        $viajero = $this->viajero;

        if ($viajero->oficina && $viajero->oficina->banco) {
            $ofi = $viajero->oficina;
            return $ofi->banco . ": " . $ofi->iban . " $titular";
        }

        if (!$viajero->provincia) {
            return null;
        }

        if ($viajero->provincia->oficina) {
            $oficina = $viajero->provincia->oficina;
            if ($oficina->banco) {
                return $oficina->banco . ": " . $oficina->iban . " $titular";
            }
        }

        $oficina = $plataforma->oficina;
        return $oficina->banco . ": " . $oficina->iban . " $titular";
    }

    public function getEsCorporativoAttribute()
    {
        if (!$this->convocatoria) {
            return Session::get('vcn.booking_corporativo') ? true : false;
        }

        return $this->convocatoria->activo_corporativo;
    }

    public function getCorporativoInfoAttribute()
    {
        if (!$this->convocatoria) {
            return null;
        }

        return $this->convocatoria->corporativo_info;
    }

    /**
     * @return bool
     */
    public function getEsPagoTpvAttribute()
    {
        if ($this->es_corporativo) {
            $corp = $this->corporativo_info;
            if (isset($corp['pago'])) {
                if ($corp['pago'] == 2) {
                    return true;
                }

                return false;
            }
        }

        return ConfigHelper::es_booking_online_tpv();
    }

    public function getEsCorporativoPagoAttribute()
    {
        $ret = true; //si no es corporativo devolverá true

        if ($this->es_corporativo) {
            $corp = $this->corporativo_info;
            $ret = isset($corp['pago']) ? $corp['pago'] : false; //por defecto true para los anteriores a lo nuevo
        }

        return $ret;
    }

    /**
     * @return bool
     */
    public function setCorporativo()
    {
        if (!$this->es_corporativo) {
            return false;
        }

        $corp = $this->corporativo_info;

        if ($corp) {
            if (isset($corp['oficina'])) {
                $this->oficina_id = (int) $corp['oficina'];
            }

            if (isset($corp['user_id'])) {
                $uid = (int) $corp['user_id'];
                $this->user_id = $uid;
                $this->viajero->asign_to = $uid;
                $this->viajero->save();
            }

            $pid = isset($corp['prescriptor_id']) ? $corp['prescriptor_id'] : $this->prescriptor_id;
            $this->prescriptor_id = $pid;
            $oid = isset($corp['origen_id']) ? $corp['origen_id'] : $this->origen_id;
            $this->origen_id = $oid;
            $soid = isset($corp['suborigen_id']) ? $corp['suborigen_id'] : $this->suborigen_id;
            $this->suborigen_id = $soid;
            $sodid = isset($corp['suborigendet_id']) ? $corp['suborigendet_id'] : $this->suborigendet_id;
            $this->suborigendet_id = $sodid;

            $this->save();

            $this->viajero->setBooking($this->id);
            $viajero = $this->viajero;

            $viajero->prescriptor_id = $pid ?: $viajero->prescriptor_id;
            $viajero->origen_id = $viajero->origen_id ?: $oid;
            $viajero->suborigen_id = $viajero->suborigen_id ?: $soid;
            $viajero->suborigendet_id = $viajero->suborigendet_id ?: $sodid;
            $viajero->save();
        }
    }

    /**
     *
     */
    public function getCancelacionPdfAttribute()
    {
        $seguro = $this->cancelacion_seguro;
        if ($seguro) {
            $idioma = $this->idioma_contacto;
            $plataforma = $this->plataforma;

            //curso > subcategoria > categoria > extra (plataforma > todas)
            $arr = ['curso', 'subcategoria', 'categoria'];
            foreach ($arr as $m) {
                if (!$this->$m) {
                    continue;
                }

                $pdf = $this->$m->pdf_cancelacion;
                if ($pdf) {
                    if (isset($pdf[$plataforma][$idioma])) {
                        $pdf = $pdf[$plataforma][$idioma];
                        if ($pdf) {
                            return $pdf;
                        }
                    }
                }
            }

            $plataforma = 0;
            foreach ($arr as $m) {
                $pdf = $this->$m->pdf_cancelacion;
                if ($pdf) {
                    if (isset($pdf[$plataforma][$idioma])) {
                        $pdf = $pdf[$plataforma][$idioma];
                        if ($pdf) {
                            return $pdf;
                        }
                    }
                }
            }

            $extra = $seguro->extra;
            $pdf = \VCN\Helpers\Traductor::getWeb($idioma, 'Extra', 'pdf_condiciones', $extra->id, $extra->pdf_condiciones);
            return $pdf;

            //$seguro_name = $pdf ? "<a href='$pdf' target='_blank'>$seguro_name</a>" : $seguro_name;
        }
    }

    public function setViajero(int $viajero_id, $request = null)
    {
        $viajero = Viajero::find($viajero_id);

        $this->viajero_id = $viajero_id;
        $this->user_id = $viajero->asign_to ?: ConfigHelper::config('tpv_asign_to');
        $this->oficina_id = $viajero->oficina_asignada;

        if (!$this->es_codigo) {
            $this->prescriptor_id = $viajero->prescriptor_id;
        }

        $this->save();

        $viajero->oficina_id = $this->oficina_id;
        $viajero->save();

        if ($request) {
            $data = $request->input();
            $data['fechanac'] = $request->has('fechanac') ? Carbon::createFromFormat('d/m/Y', $data['fechanac'])->format('Y-m-d') : null;
            $viajero->update($data);

            $data = $request->except('_token');
            $data['viajero_id'] = $viajero->id;
            $data['pasaporte_emision'] = $request->get('pasaporte_emision') ? Carbon::createFromFormat('d/m/Y', $request->get('pasaporte_emision'))->format('Y-m-d') : "";
            $data['pasaporte_caduca'] = $request->get('pasaporte_caduca') ? Carbon::createFromFormat('d/m/Y', $request->get('pasaporte_caduca'))->format('Y-m-d') : "";
            $data['ingles'] = $request->get('ingles_academia_bool');
            $viajero->datos->update($data);
        }

        $online = $this->es_online ? "ONLINE" : "";
        ViajeroLog::addLog($viajero, "Nuevo Booking $online [$this->id] Tutor");

        $viajero->setBooking($this->id);
        $this->updateDatos(true, $viajero_id);
    }

    public function getImporteNotaPago($doc)
    {
        $importe = $this->precio_sin_cancelacion;

        if ($doc->notapago_pagar_tipo == 2)
        {
            $importe = $this->saldo_pendiente;
        }
        elseif ($doc->notapago_pagar_tipo == 1)
        {
            //% sobre el total
            $importe = number_format((($importe * $doc->notapago_pagar) / 100), 2);
        }
        elseif ($doc->notapago_pagar_tipo == 9)
        {
            //% sobre el restante
            $importe = $this->saldo_pendiente;
            $importe = number_format((($importe * $doc->notapago_pagar) / 100), 2);
        }
        else
        {
            $importe = $doc->notapago_pagar;
        }

        $importe = ConfigHelper::parseMoneda($importe);

        return $importe;
    }

    /**
     * @param $id
     */
    public function setDescuento($descuento_id, $codigo = null)
    {
        $booking_id = $this->id;

        $descuento = DescuentoTipo::find($descuento_id);

        if (!$descuento) {
            return false;
        }

        $dtoValor = null;
        $dtoMulti = "";
        if ($descuento->es_multi) {
            if (!$this->es_convocatoria_multi) {
                return false;
            }

            $oneTime = $descuento->tipo_multi;
            $semanas = [];
            $dtoValor = 0;
            $iSemanas = $this->multis->count();

            foreach ($this->multis as $esp) {
                $eid = $esp->especialidad->especialidad_id;

                if ($oneTime) {
                    if (in_array($eid, $semanas)) {
                        continue;
                    }
                }

                if (in_array($eid, $descuento->especialidad_id)) {
                    $semanas[] = $eid;

                    $is = array_search($eid, $descuento->especialidad_id);
                    $dtoValor += $descuento->especialidad_importe[$is];
                }
            }

            $dtoMulti = " [$iSemanas semanas]";
        }

        $d = new BookingDescuento;
        $d->booking_id = $this->id;
        $d->fecha = Carbon::now()->format('Y-m-d');
        $d->user_id = auth()->user()->id;

        $precio = $this->precio_total;
        $ptotal = $precio['total'];
        $pcurso = $precio['total_curso'];
        $pextras = $precio['total_extras'];

        //si no es multi coge el del dto.
        $dValor = $dtoValor ?: $descuento->valor;

        if ($descuento->tipo == 0) {
            $d->currency_id = ConfigHelper::default_moneda_id();

            //Porcentaje
            switch ($descuento->tipo_pc) {
                case 0: //total
                    {
                        $importe = ($ptotal * $dValor) / 100;
                        $notas = "Descuento " . $descuento->name . " (" . $dValor . "% Total): $importe";
                    }
                    break;

                case 1: //curso
                    {
                        $importe = ($pcurso * $dValor) / 100;
                        $notas = "Descuento " . $descuento->name . " (" . $dValor . "% Curso): $importe";

                        $d->currency_id = $this->convocatoria->moneda_id;
                    }
                    break;

                case 2: //total menos extras
                    {
                        $importe = (($ptotal - $pextras) * $dValor) / 100;
                        $notas = "Descuento " . $descuento->name . " (" . $dValor . "% Total-Extras): $importe";
                    }
                    break;
            }
        } else {
            //Importe
            $importe = $dValor;
            $notas = "Descuento " . $descuento->name . " (Importe)";

            $d->currency_id = $descuento->moneda_id;
        }

        $notas .= " :: [$codigo]";

        $d->importe = $importe;
        $d->notas = $notas . $dtoMulti;
        $d->codigo = $codigo;
        $d->save();

        return $d;
    }

    /**
     * @param DescuentoTipo $descuento
     */
    public function setDescuentoCodigo(DescuentoTipo $descuento)
    {
        if (!$descuento) {
            return false;
        }

        if ($descuento->codigo_origen) {
            $this->origen_id = $descuento->codigo_origen;
        }

        if ($descuento->codigo_suborigen) {
            $this->suborigen_id = $descuento->codigo_suborigen;
        }

        if ($descuento->codigo_suborigendet) {
            $this->suborigendet_id = $descuento->codigo_suborigendet;
        }

        if ($descuento->codigo_prescriptor) {
            $this->prescriptor_id = $descuento->codigo_prescriptor;
        }

        $this->save();

        //Viajero
        if (!$this->viajero_id) {
            return false;
        }

        $viajero = $this->viajero;
        if (!$viajero) {
            return false;
        }

        if ($descuento->codigo_origen) {
            $viajero->origen_id = $viajero->origen_id ?: $descuento->codigo_origen;
        }

        if ($descuento->codigo_suborigen) {
            $viajero->suborigen_id = $viajero->suborigen_id ?: $descuento->codigo_suborigen;
        }

        if ($descuento->codigo_suborigendet) {
            $viajero->suborigendet_id = $viajero->suborigendet_id ?: $descuento->codigo_suborigendet;
        }

        if ($descuento->codigo_prescriptor) {
            $viajero->prescriptor_id = $descuento->codigo_prescriptor;
        }

        $viajero->save();

        return true;
    }

    public function setContactosSOS()
    {
        $this->sos_proveedor = null;
        $this->sos_plataforma = null;
        $this->sos_proveedor_notas = null;
        $this->sos_plataforma_notas = null;
        $this->save();

        $arr = [];
        $notas = [];
        //sos_proveedor: convocatoria, curso, centro, proveedor
        if ($this->convocatoria) {
            $c = $this->convocatoria->getContactoSOS(1);

            $arr += $c->pluck('contacto_id')->toArray();
            $notas += $c->pluck('notas')->toArray();
        }

        if (!$this->sos_proveedor && $this->curso) {
            $c = $this->curso->getContactoSOS(1);

            $arr += $c->pluck('contacto_id')->toArray();
            $notas += $c->pluck('notas')->toArray();
        }

        if (!$this->sos_proveedor && $this->centro) {
            $c = $this->centro->getContactoSOS(1);

            $arr += $c->pluck('contacto_id')->toArray();
            $notas += $c->pluck('notas')->toArray();
        }

        if (!$this->sos_proveedor && $this->proveedor) {
            $c = $this->proveedor->getContactoSOS(1);

            $arr += $c->pluck('contacto_id')->toArray();
            $notas += $c->pluck('notas')->toArray();
        }

        $arr = array_unique($arr);
        $this->sos_proveedor = $arr;
        $this->sos_proveedor_notas = implode($notas);

        //sos_plataforma: convocatoria, curso, centro, subcategoria, categoria 
        $p = $this->plataforma;
        if ($this->convocatoria) {
            $c = $this->convocatoria->getContactoSOS(0, $p);
            $this->sos_plataforma = $c ? $c->contacto_id : 0;
            $this->sos_plataforma_notas = $c ? $c->notas : "";
        }

        if (!$this->sos_plataforma && $this->curso) {
            $c = $this->curso->getContactoSOS(0, $p);

            $this->sos_plataforma = $c ? $c->contacto_id : 0;
            $this->sos_plataforma_notas = $c ? $c->notas : "";
        }

        if (!$this->sos_plataforma && $this->centro) {
            $c = $this->centro->getContactoSOS(0, $p);

            $this->sos_plataforma = $c ? $c->contacto_id : 0;
            $this->sos_plataforma_notas = $c ? $c->notas : "";
        }

        if (!$this->sos_plataforma && $this->subcategoria) {
            $c = $this->subcategoria->getContactoSOS(0, $p);

            $this->sos_plataforma = $c ? $c->contacto_id : 0;
            $this->sos_plataforma_notas = $c ? $c->notas : "";
        }

        if (!$this->sos_plataforma && $this->categoria) {
            $c = $this->categoria->getContactoSOS(0, $p);

            $this->sos_plataforma = $c ? $c->contacto_id : 0;
            $this->sos_plataforma_notas = $c ? $c->notas : "";
        }

        $this->save();
    }

    public function getContactosSOS($proveedor = false)
    {
        if ($proveedor) {
            $c = $this->sos_proveedor ? \VCN\Models\System\Contacto::whereIn('id', $this->sos_proveedor)->get() : null;
            return $c;
        }

        if ($this->sos_plataforma) {
            $c = \VCN\Models\System\Contacto::find($this->sos_plataforma);
            return $c;
        }

        return null;
    }

    public function getContactoSosProveedorAttribute()
    {
        return $this->getContactosSOS(1);
    }

    public function getContactoSosAttribute()
    {
        return $this->getContactosSOS();
    }

    public function getSosProveedorTxtAttribute()
    {
        $ret = "";
        if ($this->sos_proveedor && is_array($this->sos_proveedor)) {
            foreach ($this->sos_proveedor as $c) {
                $c = \VCN\Models\System\Contacto::find($c);
                $ret .= $c->name . " [" . $c->phone . "], ";
            }
        }

        return $ret;
    }

    public function getSosPlataformaTxtAttribute()
    {
        $ret = "";
        if ($this->sos_plataforma) {
            $c = \VCN\Models\System\Contacto::find($this->sos_plataforma);
            $ret = $c->name;
        }

        return $ret;
    }

    public function getEsAvisoFotoAttribute()
    {
        if ($this->viajero->foto) {
            return false;
        }

        $aviso = $this->curso->es_aviso_foto;
        $aviso = is_null($aviso) ? null : $aviso;

        if (is_null($aviso)) {
            $aviso = $this->categoria->es_aviso_foto;
            $aviso = is_null($aviso) ? null : $aviso;
        }

        if (is_null($aviso)) {
            $aviso = $this->subcategoria->es_aviso_foto;
            $aviso = is_null($aviso) ? null : $aviso;
        }

        if (is_null($aviso)) {
            $aviso = $this->subcategoria_detalle->es_aviso_foto;
            $aviso = is_null($aviso) ? null : $aviso;
        }

        return is_null($aviso) ? false : $aviso;
    }

    public function getEstadoPlazasAttribute()
    {
        $booking = $this;

        $ret = [];

        $hayPlazas = true;
        $plazas_alojamiento = 1;
        $plazas_vuelo = 1;

        if ($booking->curso->es_convocatoria_cerrada)
        {
            $hayPlazas = false;

            $plazas_vuelo = $booking->vuelo ? $booking->vuelo->plazas_disponibles : 1;

            /*
            if (!$booking->alojamiento) {
                $plazas_a = null;
                $plazas_alojamiento = 1;
            } else {
                if ($booking->convocatoria) {
                    $plazas_a = $booking->convocatoria->getPlazas($booking->alojamiento->id);
                    $plazas_alojamiento = $plazas_a ? $plazas_a->plazas_disponibles : 0;
                }
            }
            */

            $plazas_alojamiento = 0;
            if($booking->convocatoria)
            {
                if ($booking->alojamiento)
                {
                    $plazas_a = $booking->convocatoria->getPlazas($booking->alojamiento->id);
                    $plazas_alojamiento = $plazas_a ? $plazas_a->plazas_disponibles : 0;
                }
            }

            // if ($plazas_alojamiento>0 && $plazas_vuelo>0) {
            //     $hayPlazas = true;
            // }
        }

        $hayPlazas = true;
        if ($plazas_alojamiento < 1 || $plazas_vuelo < 1)
        {
            $hayPlazas = false;
        }

        $ret['hayPlazas'] = $hayPlazas;
        $ret['alojamiento'] = $plazas_alojamiento;
        $ret['vuelo'] = $plazas_vuelo;

        return $ret;
    }

    public function getOvbkgAuthAttribute()
    {
        if ($this->ovbkg_id) {
            $destino = User::find($this->ovbkg_id);
            return $destino ? $destino->full_name : "?";
        }

        return "-";
    }

    public function getFacturaQuienAttribute()
    {
        $ret = $this->datos->fact_nif ?: null;
        if ($ret) {
            return "Empresa";
        }

        $ret = $this->datos->documento ?: null;
        if ($ret) {
            return "Viajero";
        }

        $viajero = $this->viajero;

        $ret = $viajero->datos->fact_nif ?: null;
        if ($ret) {
            return "Empresa";
        }

        $ret = $viajero->datos->documento ?: null;
        if ($ret) {
            return "Viajero";
        }

        $tutor = $viajero->tutor1;
        $ret = $tutor ? $tutor->nif : null;
        if ($ret) {
            return "Tutor1";
        }

        $tutor = $viajero->tutor2;
        $ret = $tutor ? $tutor->nif : null;
        if ($ret) {
            return "Tutor2";
        }

        return "-";
    }

    public function getFacturaCifAttribute()
    {
        $ret = $this->datos->fact_nif ? $this->datos->fact_nif : ($this->datos->documento ?: null);
        if ($ret) {
            return $ret;
        }

        $viajero = $this->viajero;

        $ret = $viajero->datos->fact_nif ?: ($viajero->datos->documento ?: null);
        if ($ret) {
            return $ret;
        }

        $tutor = $viajero->tutor1;
        $ret = $tutor ? $tutor->nif : null;
        if ($ret) {
            return $ret;
        }

        $tutor = $viajero->tutor2;
        $ret = $tutor ? $tutor->nif : null;
        if ($ret) {
            return $ret;
        }

        return null;
    }

    public function getFacturaTitularAttribute()
    {
        if ($this->datos->fact_razonsocial) {
            return $this->datos->fact_razonsocial;
        }

        $viajero = $this->viajero;
        $ret = $viajero->datos->fact_nif ?: ($viajero->datos->documento ?: null);
        if ($ret) {
            return $this->datos->full_name;
        }

        $tutor = $viajero->tutor1;
        $ret = $tutor ? $tutor->nif : null;
        if ($ret) {
            return $tutor->full_name;
        }

        $tutor = $viajero->tutor2;
        $ret = $tutor ? $tutor->nif : null;
        if ($ret) {
            return $tutor->full_name;
        }

        return $this->datos->fact_razonsocial ? $this->datos->fact_razonsocial : $this->datos->full_name;
    }

    public function getNotasTxtAttribute()
    {
        if ($this->notas && !$this->notas2) {
            return "Obs: -";
        }

        return $this->notas2;
    }

    public function getEmailsCount($tipo = 'ambos')
    {
        $viajero = $this->viajero;

        $iSend = 0;
        switch ($tipo) {
            case 'viajero': {
                    $iSend = MailHelper::validar($viajero);
                }
                break;

            case 'tutores': {
                    $destino = $viajero->tutor1;
                    if ($destino) {
                        $iSend += MailHelper::validar($destino);
                    }

                    $destino = $viajero->tutor2;
                    if ($destino) {
                        $iSend += MailHelper::validar($destino);
                    }
                }
                break;

            case 'ambos': {
                    $destino = $viajero;
                    $iSend += MailHelper::validar($destino);

                    $destino = $viajero->tutor1;
                    if ($destino) {
                        $iSend += MailHelper::validar($destino);
                    }

                    $destino = $viajero->tutor2;
                    if ($destino) {
                        $iSend += MailHelper::validar($destino);
                    }
                }
                break;

            case 'user': {
                    $destino = $this->asignado;
                    if ($destino) {
                        $iSend += MailHelper::validar($destino);
                    }
                }
                break;

            case 'prescriptor': {
                    $destino = $this->prescriptor;
                    if ($destino) {
                        $iSend += MailHelper::validar($destino);
                    }
                }
                break;

                // case 'email':
        }

        return $iSend;
    }

    public function setExamenes()
    {
        $respuestas = Respuesta::where('viajero_id', $this->viajero_id)->where('booking_id', 0);

        foreach ($respuestas->get() as $r) {
            $r->booking_id = $this->id;
            $r->save();

            RespuestaLog::addLog($r, 'Booking');
            BookingLog::addLog($this, "Test $r->examen_id");
        }
    }

    public function getEsGradoExtAttribute()
    {
        return $this->categoria->es_grado_ext ?? false;
    }
}
