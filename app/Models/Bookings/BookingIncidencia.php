<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

class BookingIncidencia extends Model
{
    protected $table = 'booking_incidencias';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public function asignado()
    {
        return $this->belongsTo('\VCN\Models\User', 'asign_to');
    }

    public function tareas()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingTarea', 'incidencia_id');
    }

    public function seguimientos()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingTarea', 'incidencia_id')->where('tipo','<>','')->where('tipo','<>','log');
    }

    public function logs()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingIncidenciaLog', 'incidencia_id');
    }
}
