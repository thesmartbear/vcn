<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

use Carbon;

class BookingDevolucion extends Model
{
    protected $table = 'booking_devoluciones';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking', 'booking_id');
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda', 'currency_id');
    }

    public function getFechaDmyAttribute()
    {
        return Carbon::parse($this->fecha)->format('d/m/Y');
    }
}
