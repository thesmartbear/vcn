<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Monedas\Moneda;
use VCN\Models\Extras\ExtraUnidad;

use ConfigHelper;

class BookingExtra extends Model
{
    protected $table = 'booking_extras';

    // protected $fillable = [];
    protected $guarded = ['_token'];


    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking');
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda','moneda_id');
    }

    public function getMonedaNameAttribute()
    {
        $m = Moneda::find($this->moneda_id);
        return $m?$m->name:'-';
    }

    public function extra()
    {
        switch($this->tipo)
        {
            case 0:
            {
                return $this->belongsTo('\VCN\Models\Cursos\CursoExtra', 'extra_id');
            }
            break;

            case 1:
            {
                return $this->belongsTo('\VCN\Models\Centros\CentroExtra', 'extra_id');
            }
            break;

            case 2:
            {
                return $this->belongsTo('\VCN\Models\Cursos\CursoExtraGenerico', 'extra_id');
            }
            break;

            case 3:
            {
                return $this->belongsTo('\VCN\Models\Alojamientos\AlojamientoCuota', 'extra_id');
            }
            break;

            case 4:
            {
                return $this->notas;
            }
            break;

            case 5:
            {
                return $this->belongsTo('\VCN\Models\Extras\Extra', 'extra_id');
            }
            break;
        }

    }

    public function getExtraNameAttribute()
    {
        // return $this->extra?$this->extra->name:'[Extra eliminado]';
        return $this->name;
    }

    public function getTipoUnidadNameAttribute()
    {
        // return $this->extra?$this->extra->tipo_unidad_name:'?';
        return ConfigHelper::getTipoUnidad($this->tipo_unidad);
    }

    public function getUnidadNameAttribute()
    {
        return $this->unidad_id?ExtraUnidad::find($this->unidad_id)->name:"-";
    }

    public function getFullNameAttribute()
    {
        if($this->tipo_unidad == 2)
        {
            if($this->extra)
            {
                $pc = $this->extra->precio;
                return $this->name . " [$pc%] = ". ConfigHelper::parseMoneda($this->precio,$this->moneda_name);
            }

            return $this->name . " [%] = ". ConfigHelper::parseMoneda($this->precio,$this->moneda_name);
        }

        if($this->unidades>1)
        {
            return $this->name . " x " . $this->unidades ." (" . $this->unidad_name .") = ". ConfigHelper::parseMoneda($this->precio,$this->moneda_name);
        }

        return $this->name . " = ". ConfigHelper::parseMoneda($this->precio,$this->moneda_name);
    }

    public function getPorcentajeAttribute()
    {
        if($this->tipo_unidad == 2)
        {
            if($this->extra)
            {
                $pc = $this->extra->precio;
                return $pc;
            }

            return $this->precio;
        }

        return 0;
    }

    public static function add($extra, $booking_id, $tipo,$unidades=0)
    {
        if($extra->generics_id == 12) //extra dippy
        {
            if($extra->name == "Gastos de gestión cursos Dippy")
            {
                $unidades = ($unidades>3) ? 3 : $unidades;
            }
        }
        
        $e = new self;
        $e->tipo = $tipo;
        $e->booking_id = $booking_id;
        $e->extra_id = $extra->id;
        $e->moneda_id = $extra->moneda_id;

        $e->required = $extra->required;
        $e->name = $extra->name;
        $e->tipo_unidad = $extra->tipo_unidad;
        $e->unidad_id = $extra->unidad_id;

        $e->precio = $extra->precio;
        $e->unidades = 1;
        if($extra->tipo_unidad==1) //por unidad
        {
            $e->unidades = $unidades ?: 0;
            // $e->precio = $extra->precio * $unidades;
        }
        elseif($extra->tipo_unidad == 2) //Porcentaje
        {
            $booking = Booking::find($booking_id);
            $precio = $booking->total_curso_neto_en_moneda; //$booking->precio_total['total_curso'];
            $precio = ($extra->precio * $precio) / 100;

            $e->precio = $precio;
        }

        $e->save();

        return $e;
    }

    public function getEsCancelacionAttribute()
    {
        if($this->tipo != 2)
        {
            return false;
        }

        if(!$this->extra)
        {
            return false;
        }

        if($this->extra->generico)
        {
            return $this->extra->generico->es_cancelacion;
        }

        return false;
    }

}
