<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

class BookingSchool extends Model
{
    protected $table = 'booking_schools';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $dates = ['desde','hasta'];

    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking', 'booking_id');
    }

    public function school()
    {
        return $this->belongsTo('\VCN\Models\Centros\School', 'school_id');
    }

    public function getCambioAvisadoAttribute()
    {
        $logMail = $this->booking->logs->where('tipo',"Asignación School email [$this->school_id]")->sortByDesc('created_at')->first();
        $logCambio = $this->booking->logs->where('tipo',"Asignación School modificado [$this->school_id]")->sortByDesc('created_at')->first();

        if(!$logMail)
        {
            return false;
        }

        if(!$logCambio)
        {
            $logCambio = $this->booking->logs->where('tipo',"Asignación School creado [$this->school_id]")->sortByDesc('created_at')->first();
        }

        if($logMail->created_at->gte($logCambio->created_at))
        {
            return true;
        }

        return false;
    }
}
