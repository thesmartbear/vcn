<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

use Carbon;

class Status extends Model
{
    protected $table = 'statuses_booking';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function checklist()
    {
        $cats = $this->booking->curso->categoria;

        return $this->hasMany('\VCN\Models\Bookings\StatusChecklist')->where('category_id', $cats);
    }

    public function getNextAttribute()
    {
        return Status::where('orden','>',$this->orden)->orderBy('orden')->first();
    }

    public function getChecklistFecha( $booking_id, $checkid )
    {
        $c = BookingChecklist::where(['booking_id'=> $booking_id, 'checklist_id'=> $checkid])->first();

        return $c?("[". Carbon::parse($c->fecha)->format('d/m/Y') ."] (". ($c->user?$c->user->full_name:".") .")"):"-";
    }


    public function getChecklistBooking( $booking_id, $status_id=null )
    {
        $ficha = Booking::find($booking_id);

        $ret = collect();

        if(!$ficha)
        {
            return $ret;
        }

        $curso = $ficha->curso;
        $centro = $curso->centro;

        $status = $status_id?:$this->id;

        $statuses = \VCN\Models\Bookings\Status::orderBy('orden');
        foreach( $statuses->get() as $st )
        {
            $sts = \VCN\Models\Bookings\StatusChecklist::where('status_id', $st->id)->orderBy('orden');
            if( $sts->count()>0 )
            {
                foreach( $sts->get() as $st )
                {
                    $bSt = false;
                    // $ret->push($st);
                    // if($st->status_id == $status) //Ahora se muestran todas
                    {
                        if( $st->category_id==0 && $st->subcategory_id==0 && $st->subcategory_det_id==0 )
                        {
                            if(!$bSt)
                            {
                                $ret->push($st);
                                $bSt = true;
                            }
                        }

                        if( $st->category_id==$curso->category_id && $st->subcategory_id==0 && $st->subcategory_det_id==0 )
                        {
                            if(!$bSt)
                            {
                                $ret->push($st);
                                $bSt = true;
                            }
                        }

                        if( $st->category_id==$curso->category_id && $st->subcategory_id==$curso->subcategory_id && $st->subcategory_det_id==0 )
                        {
                            if(!$bSt)
                            {
                                $ret->push($st);
                                $bSt = true;
                            }
                        }

                        if( $st->category_id==$curso->category_id && $st->subcategory_id==$curso->subcategory_id && $st->subcategory_det_id==$curso->subcategory_det_id )
                        {
                            if(!$bSt)
                            {
                                $ret->push($st);
                                $bSt = true;
                            }
                        }

                        $arr = explode(',', $st->curso_id);
                        if( in_array($curso->id, $arr) )
                        {
                            if(!$bSt)
                            {
                                $ret->push($st);
                                $bSt = true;
                            }
                        }

                        $arr = explode(',', $st->centro_id);
                        if( in_array($centro->id, $arr) )
                        {
                            if(!$bSt)
                            {
                                $ret->push($st);
                                $bSt = true;
                            }
                        }
                    }
                }

                // $ret = $this->hasMany('\VCN\Models\Bookings\StatusChecklist')
                //     ->orWhere(function ($query) use ($status) {
                //         $query->where('status_id',$status)
                //             ->where('category_id', 0)
                //             ->where('subcategory_id', 0)
                //             ->where('subcategory_det_id', 0);
                //     })
                //     ->orWhere(function ($query) use ($curso, $status) {
                //         $query->where('status_id',$status)
                //             ->where('category_id', $curso->category_id)
                //             ->where('subcategory_id', null)
                //             ->where('subcategory_det_id', null);
                //     })
                //     ->orWhere(function ($query) use ($curso, $status) {
                //         $query->where('status_id',$status)
                //             ->where('category_id', $curso->category_id)
                //             ->where('subcategory_id', $curso->subcategory_id)
                //             ->where('subcategory_det_id', null);
                //     })
                //     ->orWhere(function ($query) use ($curso, $status) {
                //         $query->where('status_id',$status)
                //             ->where('category_id', $curso->category_id)
                //             ->where('subcategory_id', $curso->subcategory_id)
                //             ->where('subcategory_det_id', $curso->subcategory_det_id);
                //     })
                //     ->get();
            }
        }

        return $ret;
    }
}
