<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Status;
use VCN\Models\Bookings\StatusChecklist;

use Auth;

class BookingLog extends Model
{
    protected $table = 'booking_logs';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'data' => 'json',
    ];


    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking');
    }

    public function usuario()
    {
        return $this->belongsTo('\VCN\Models\User','user_id');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User','user_id');
    }

    public static function addLog( Booking $booking, $tipo, $notas="")
    {
        //booking: BookingLog

        $log = new BookingLog;
        $log->booking_id = $booking->id;
        $log->tipo = $tipo;
        $log->user_id = Auth::user()?Auth::user()->id:0;

        $log->notas = $notas;

        $log->save();

    }

    public static function addLogReunion( Booking $booking )
    {
        if($booking && $booking->reunion)
        {
            $txt = "Info Reunión ";

            foreach($booking->reuniones as $r)
            {
                $rid = $r->id;
                $txt .= "[$rid]";
            }

            self::addLog($booking, 'Reunion enviada',$txt);
        }
    }

    public static function addLogDatos( $booking, $data, $fase=0 )
    {
        if(!$booking)
        {
            return;
        }
        
        $dataBooking = $booking->toArray();
        foreach($dataBooking as $idbk => $dbk)
        {
            if(is_array($dbk))
            {
                unset($dataBooking[$idbk]);        
            }
        }
        // unset($dataBooking['pagos']);
        // unset($dataBooking['alertas']);
        // unset($dataBooking['importes']);
        // unset($dataBooking['online']);
        // unset($dataBooking['datos_campamento']);
        // unset($dataBooking['firmas']);
        // unset($dataBooking['tpv_result']);
        // unset($dataBooking['sos_proveedor']);

        $notas = null;
        if(is_array($data))
        {
            $notas = json_encode( array_diff( $data, $dataBooking ) );
        }

        if($notas)
        {
            $tipo = "Modificado" . $fase?" (Fase:$fase)":"";
            self::addLog($booking, $tipo, $notas);
        }
    }

    public static function addLogStatus( Booking $booking, $status )
    {
        $status1 = Status::find($booking->status_id);
        $status1 = $status1 ? $status1->name : 0;

        $st = Status::find($status)->name ?? $status;
        $status = $status > 0 ? $st : 0;

        $tipo = "Cambio de Estado [$status1 -> $status]";
        self::addLog($booking, $tipo);
    }

    public static function addLogStatusAuto( Booking $booking, $status )
    {
        if($status == $booking->status_id)
        {
            return;
        }
        
        $status1 = Status::find($booking->status_id);
        $status1 = $status1?$status1->name:0;
        $status = $status>0?Status::find($status)->name:0;

        $tipo = "Cambio de Estado AUTO [$status1 -> $status]";
        self::addLog($booking, $tipo);
    }

    public static function addLogChecklist( Booking $booking, $checkid, $estado, $fecha=null )
    {
        $check = StatusChecklist::find($checkid);

        // $estado = $booking->checklists->where('checklist_id',$checkid)->first();
        // $estado = $estado?$estado->estado:0;

        $tipo = "Checklist (". $check->name .") [". ($estado?"On":"Off") ."] [$fecha]";
        self::addLog($booking, $tipo);
    }
}
