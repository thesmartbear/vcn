<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

use Auth;
use ConfigHelper;

class BookingIncidenciaLog extends Model
{
    protected $table = 'booking_incidencia_logs';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function incidencia()
    {
        return $this->belongsTo('\VCN\Models\Bookings\BookingIncidencia', 'incidencia_id');
    }

    public function usuario()
    {
        return $this->belongsTo('\VCN\Models\User','user_id');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User','user_id');
    }

    public static function addLog( BookingIncidencia $incidencia, $notas="", $tipo="log")
    {
        $log = new Self;
        $log->incidencia_id = $incidencia->id;
        $log->tipo = $tipo;
        $log->user_id = Auth::user()?Auth::user()->id:0;

        $log->notas = $notas;

        $log->save();

    }
}
