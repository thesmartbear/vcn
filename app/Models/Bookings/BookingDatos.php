<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

class BookingDatos extends \VCN\Models\ModelAuditable
{
    protected $table = 'booking_datos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'tutores' => 'json',
    ];


    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking','booking_id');
    }

    public function getAlergiasTxtAttribute()
    {
        if(!$this->alergias)
        {
            return "";
        }

        if($this->alergias && !$this->alergias2)
        {
            return "Allergies: -";
        }

        return $this->alergias2 ? "Allergies: ". $this->alergias2 : "";
    }

    public function getEnfermedadTxtAttribute()
    {
        if(!$this->enfermedad)
        {
            return "";
        }

        if($this->enfermedad && !$this->enfermedad2)
        {
            return "Illnes: -";
        }

        return $this->enfermedad2 ? "Illnes: ". $this->enfermedad2 : "";
    }

    public function getMedicacionTxtAttribute()
    {
        if(!$this->medicacion)
        {
            return "";
        }

        if($this->medicacion && !$this->medicacion2)
        {
            return "Medication: -";
        }

        return $this->medicacion2 ? "Medication: ". $this->medicacion2 : "";
    }

    public function getTratamientoTxtAttribute()
    {
        if(!$this->tratamiento)
        {
            return "";
        }

        if($this->tratamiento && !$this->tratamiento2)
        {
            return "Treatment: -";
        }

        return $this->tratamiento2 ? "Treatment: ". $this->tratamiento2 : "";
    }

    public function getDietaTxtAttribute()
    {
        if(!$this->dieta)
        {
            return "";
        }

        if($this->dieta && !$this->dieta2)
        {
            return "Diet: -";
        }

        return $this->dieta2 ? "Diet: ". $this->dieta2 : "";
    }
}
