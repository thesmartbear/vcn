<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Booking;

use Auth;

class BookingLogStatus extends Model
{
    protected $table = 'booking_log_status';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User','user_id');
    }

    public static function addLog( Booking $booking, $st1, $notas="")
    {
        $log = new self;
        $log->booking_id = $booking->id;
        $log->user_id = Auth::user()?Auth::user()->id:0;

        $log->status1 = $st1;
        $log->status2 = $booking->status_id;

        $log->notas = $notas;

        $log->save();

    }
}
