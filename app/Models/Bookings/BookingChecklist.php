<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

class BookingChecklist extends Model
{
    protected $table = 'booking_checklists';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function booking()
    {
        return $this->hasOne('\VCN\Models\Bookings\Booking');
    }

    public function checklist()
    {
        return $this->belongsTo('\VCN\Models\Bookings\StatusChecklist', 'checklist_id');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }
}
