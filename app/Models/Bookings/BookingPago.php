<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

use Carbon;
use ConfigHelper;
use PDF;
use Illuminate\Support\Facades\App;

class BookingPago extends Model
{
    protected $table = 'booking_pagos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    
    public function save(array $options = [])
    {
        parent::save();
        
        $booking = $this->booking;
        if(!$booking->pagado && $booking->saldo_pendiente<=0 )
        {
            $this->booking->pagado();
        }
    }


    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking');
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda','moneda_id');
    }

    public function getFechaDmyAttribute()
    {
        return Carbon::parse($this->fecha)->format('d/m/Y');
    }

    public function getTipoPagoAttribute()
    {
        return ConfigHelper::getTipoPago($this->tipo);
    }

    public function getImporteMonedaAttribute()
    {
        return ConfigHelper::parseMoneda($this->importe, $this->moneda ? $this->moneda->name : null );
    }

    public function getImportePagoMonedaAttribute()
    {
        if($this->rate != 1)
        {
            return ConfigHelper::parseMoneda($this->importe_pago, $this->moneda ? $this->moneda->name : null) . " [$this->rate]";
        }

        return $this->importe_moneda;
    }

    public function getEsReservaInicialAttribute()
    {
        return strpos($this->notas, "Reserva inicial") !== false;

    }

    public function getPdfNameAttribute()
    {
        $dir = storage_path("files/bookings/". $this->booking->viajero_id . "/");
        $name = "r_" . $this->id .".pdf";
        $file = $dir . $name;

        return $file;
    }

    public function pdf()
    {
        $ficha = $this;
        $booking = $this->booking;

        $idioma = $booking->viajero->idioma_contacto;

        $view = 'pdf.'. strtolower($idioma) .'.recibo';

        $file = $this->pdf_name;

        if (file_exists($file))
        {
            @unlink($file);
        }

        $p = $booking->plataforma;        

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path()."/tmp");
        $pdf = PDF::loadView($view, compact('ficha','booking','idioma'));
        $pdf->setOption('margin-top',30);
        $pdf->setOption('margin-right',0);
        $pdf->setOption('margin-bottom',30);
        $pdf->setOption('margin-left',0);
        $pdf->setOption('no-print-media-type',false);
        $pdf->setOption('footer-spacing',0);
        $pdf->setOption('header-font-size',9);
        $pdf->setOption('header-spacing',0);
        $pdf->setOption('header-html', 'https://'.ConfigHelper::config('web',$p).'/assets/logos/'.$idioma.'/'.ConfigHelper::config('sufijo',$p).'header.html');
        $pdf->setOption('footer-html', 'https://'.ConfigHelper::config('web',$p).'/assets/logos/'.$idioma.'/'.ConfigHelper::config('sufijo',$p).'footer.html');

        return $pdf->save($file);
    }

    // public function getImporteAttribute()
    // {
    //     return $this->importe_pago * $this->rate;
    // }
}
