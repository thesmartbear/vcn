<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\App;
use File;
use PDF;
use ConfigHelper;

class BookingFactura extends Model
{
    protected $table = 'booking_facturas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $dates = ['fecha'];

    protected $casts = [
        'datos' => 'json',
        'precio_total' => 'json',
    ];

    public function delete()
    {
        $dir = storage_path("files/bookings/". $this->booking->viajero->id . "/");
        $num = $this->numero;

        //Pdf
        $name = "factura_". $num .".pdf";
        $file = $dir . $name;

        //borramos
        if(File::exists($file))
        {
            File::delete($file);
        }

        parent::delete();
    }


    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking', 'booking_id');
    }

    public function getViajeroAttribute()
    {
        return $this->booking->viajero;
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public function grup()
    {
        return $this->belongsTo('\VCN\Models\Bookings\BookingFacturaGrup', 'grup_id');
    }

    public function getTrimestreAttribute()
    {
        return intval(($this->fecha->month-1) / 3)+1;
    }

    public function pdf($force=false)
    {
        if($force)
        {
            $booking = $this->booking;
            $booking->updateDatos(true);

            $cif = $booking->factura_cif;

            $direccion = ($booking->datos->fact_domicilio?$booking->datos->fact_domicilio:$booking->datos->direccion) .", ". ($booking->datos->fact_cp?$booking->datos->fact_cp:$booking->datos->cp) ." ". ($booking->datos->fact_poblacion?$booking->datos->fact_poblacion:$booking->datos->ciudad);
            $datos = [
                    'razonsocial'   => $booking->factura_titular,
                    'direccion'     => $direccion,
                    'cif'           => $cif,
                    'cp'            => $booking->datos->fact_cp?$booking->datos->fact_cp:$booking->datos->cp,
                    'concepto1'     => $booking->datos->fact_concepto?$booking->datos->fact_concepto:$booking->curso->name,
                    'concepto2'     => $booking->datos->fact_concepto?"":ConfigHelper::parseMoneda($booking->course_total_amount, $booking->curso_moneda),
                ];

            $this->nif = $cif;
            $this->datos = $datos;
            $this->save();
        }

        if($force)
        {
            return $this->pdf_generar();
        }

        $job = new \VCN\Jobs\JobPdfFactura($this);
        app('Illuminate\Contracts\Bus\Dispatcher')->dispatch($job);
    }

    public function pdf_generar()
    {
        $factura = $this;
        $booking = $this->booking;

        $dir = storage_path("files/bookings/". $booking->viajero->id . "/");

        //Generamos pdf factura
        $name = "factura_". $factura->numero .".pdf";
        $file = $dir . $name;
        $lang = $booking->idioma_contacto;

        $info = ConfigHelper::config_plataforma($booking->plataforma);
        $parcial = $factura->parcial_importe;
        
        $template = 'manage.bookings.factura';
        if($parcial)
        {
            $template = 'manage.bookings.factura_parcial';
        }

        //abono
        if($factura->total<0)
        {
            $parcial = $factura->total;
        }

        //borramos
        if(File::exists($file))
        {
            File::delete($file);
        }

        // return view($template, ['ficha'=> $this, 'factura'=> $factura, 'info'=> $info, 'total'=> $parcial]);

        $p = $booking->plataforma;

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path()."/tmp");
        $pdf = PDF::loadView($template, ['ficha'=> $booking, 'factura'=> $factura, 'info'=> $info, 'parcial'=> $parcial]);

        $pdf->setOption('margin-top',30);
        $pdf->setOption('margin-right',0);
        $pdf->setOption('margin-bottom',30);
        $pdf->setOption('margin-left',0);
        $pdf->setOption('no-print-media-type',false);
        $pdf->setOption('footer-spacing',0);
        $pdf->setOption('header-font-size',9);
        $pdf->setOption('header-spacing',0);

        $pdf->setOption('header-html', 'https://'.ConfigHelper::config('web',$p).'/assets/logos/'.$lang.'/'.ConfigHelper::config('sufijo',$p).'header.html');
        $pdf->setOption('footer-html', 'https://'.ConfigHelper::config('web',$p).'/assets/logos/'.$lang.'/'.ConfigHelper::config('sufijo',$p).'footer.html');

        $pdf->save($file);

        // MailHelper::mailFactura($this->id);
    }

    public function pdf_generar_abono($file)
    {
        $template = 'manage.bookings.factura_abono';

        $p = $this->plataforma;
        $info = ConfigHelper::config_plataforma($p);

        $booking = $this->booking;
        $lang = $booking->idioma_contacto;

        app()->make('snappy.pdf.wrapper')->snappy()->setTemporaryFolder(storage_path() . "/tmp");
        $pdf = PDF::loadView($template, ['ficha' => $this, 'factura' => $this, 'info' => $info]);

        $pdf->setOption('margin-top', 30);
        $pdf->setOption('margin-right', 0);
        $pdf->setOption('margin-bottom', 30);
        $pdf->setOption('margin-left', 0);
        $pdf->setOption('no-print-media-type', false);
        $pdf->setOption('footer-spacing', 0);
        $pdf->setOption('header-font-size', 9);
        $pdf->setOption('header-spacing', 0);

        $pdf->setOption('header-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'header.html');
        $pdf->setOption('footer-html', 'https://' . ConfigHelper::config('web', $p) . '/assets/logos/' . $lang . '/' . ConfigHelper::config('sufijo', $p) . 'footer.html');

        $pdf->save($file);
    }
}
