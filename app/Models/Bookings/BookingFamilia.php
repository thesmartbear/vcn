<?php

namespace VCN\Models\Bookings;

use Illuminate\Database\Eloquent\Model;

class BookingFamilia extends Model
{
    protected $table = 'booking_familias';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $dates = ['desde','hasta'];

    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking', 'booking_id');
    }

    public function familia()
    {
        return $this->belongsTo('\VCN\Models\Centros\Familia', 'familia_id');
    }

    public function getCambioAvisadoAttribute()
    {
        $logMail = $this->booking->logs->where('tipo',"Asignación Familia email [$this->familia_id]")->sortByDesc('created_at')->first();
        $logCambio = $this->booking->logs->where('tipo',"Asignación Familia modificado [$this->familia_id]")->sortByDesc('created_at')->first();

        if(!$logMail)
        {
            return false;
        }

        if(!$logCambio)
        {
            $logCambio = $this->booking->logs->where('tipo',"Asignación Familia creado [$this->familia_id]")->sortByDesc('created_at')->first();
        }

        if($logMail->created_at->gte($logCambio->created_at))
        {
            return true;
        }

        return false;
    }
}
