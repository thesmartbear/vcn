<?php

namespace VCN\Models\Solicitudes;

use Illuminate\Database\Eloquent\Model;

class SolicitudChecklist extends Model
{
    protected $table = 'solicitud_checklists';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function solicitud()
    {
        return $this->hasOne('\VCN\Models\Solicitudes\Solicitud');
    }

    public function checklist()
    {
        return $this->belongsTo('\VCN\Models\Solicitudes\SolicitudChecklist', 'checklist_id');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }
}
