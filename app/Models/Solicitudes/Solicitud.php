<?php

namespace VCN\Models\Solicitudes;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\User;
use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\ViajeroLog;
use VCN\Helpers\ConfigHelper;
use Carbon;

class Solicitud extends Model
{
    protected $table = 'solicitudes';

    // protected $fillable = [];
    protected $guarded = ['_token'];


    public function viajero()
    {
        return $this->belongsTo('\VCN\Models\Leads\Viajero');
    }

    public function status()
    {
        return $this->belongsTo('\VCN\Models\Solicitudes\Status', 'status_id');
    }

    public function checklists()
    {
        return $this->hasMany('\VCN\Models\Solicitudes\SolicitudChecklist');
    }

    public function logs()
    {
        return $this->hasMany('\VCN\Models\Solicitudes\SolicitudLogStatus');
    }

    public function origen()
    {
        return $this->belongsTo('\VCN\Models\Leads\Origen', 'origen_id');
    }

    public function suborigen()
    {
        return $this->belongsTo('\VCN\Models\Leads\Suborigen', 'suborigen_id');
    }

    public function suborigen_det()
    {
        return $this->belongsTo('\VCN\Models\Leads\SuborigenDetalle', 'suborigendet_id');
    }

    public function getViajeroFullNameAttribute()
    {
        $ret = "";
        if($this->viajero)
        {
            $ret = $this->viajero->full_name;
        }

        return $ret;
    }

    public function getOrigenFullNameAttribute()
    {
        $ret = $this->origen?$this->origen->name:"-";

        if($this->suborigen)
        {
            $ret .= " > ". $this->suborigen->name;
        }

        if($this->suborigen_det)
        {
            $ret .= " > ". $this->suborigen_det->name;
        }

        return $ret;
    }

    public function setChecklist($checkid, $fecha, $user_id, $seguimiento=false)
    {
        $solicitud = $this;
        $id = $this->id;

        $s = SolicitudChecklist::where('solicitud_id',$id)->where('checklist_id',$checkid)->first();
        if(!$s)
        {
            $s = new SolicitudChecklist;
            $s->solicitud_id = $id;
            $s->checklist_id = $checkid;

            ViajeroLog::addLog($solicitud->viajero, "Solicitud Checklist [$s->fecha] [Ok]");
        }
        else
        {
            ViajeroLog::addLog($solicitud->viajero, "Solicitud Checklist [$s->fecha] [$s->estado -> !$s->estado]");
        }

        //Si viene de seguimiento no ponemos a off
        if($seguimiento && $s->estado)
        {
            return $s;
        }

        $s->user_id = $user_id;
        $s->estado = !$s->estado;
        $s->fecha = Carbon::createFromFormat('d/m/Y',$fecha)->format('Y-m-d');
        $s->save();

        //pasar a siguiente status cuando todas se marcar
        $checklist = $solicitud->status->getChecklistSolicitud($id);
        $marcados = $checklist->count();
        foreach($checklist as $check)
        {
            if( $solicitud->viajero->getStatusChecklistEstado($check->id) )
            {
                $marcados--;
            }
        }

        $status_id = 0;
        if($marcados==0)
        {
            $status_id = $solicitud->status->next->id;
            $solicitud->viajero->setStatus($status_id);
        }

        return $s;
    }

    public function asignado()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public function categoria()
    {
        return $this->belongsTo('\VCN\Models\Categoria', 'category_id');
    }

    public function getCategoriaNameAttribute()
    {
        return $this->categoria?$this->categoria->name:"-";
    }

    public function subcategoria()
    {
        return $this->belongsTo('\VCN\Models\Subcategoria', 'subcategory_id');
    }

    public function getTareasAttribute()
    {
        return $this->viajero->tareas;
    }

    public function getNameAttribute()
    {
        return $this->viajero->full_name ." ". Carbon::parse($this->fecha)->format('d/m/Y');
    }

    public function getEsInscritoAttribute()
    {
        return ($this->status_id == ConfigHelper::config('solicitud_status_inscrito'));
    }

    public function getStatusAnteriorAttribute()
    {
        $last = $this->logs->sortByDesc('created_at')->first();

        return $last ? $last->status1 : ConfigHelper::config('solicitud_status_inscrito');
    }


    public function archivar($motivo, $nota=null)
    {
        // if($this->status_id != ConfigHelper::config('solicitud_status_archivado'))
        {
            $this->viajero->solicitud_id = 0;
            $this->viajero->save();

            $this->viajero->setStatus(0);

            $this->setStatus(ConfigHelper::config('solicitud_status_archivado'));
            
            $this->archivar_fecha = Carbon::now();
            $this->archivar_motivo = $motivo;
            $this->archivar_motivo_nota = $nota;
            $this->save();

            ViajeroLog::addLog($this->viajero, "Solicitud [$this->id] Archivada [$motivo]:[$nota]");
        }
    }

    public function setStatus($status_id, $notas="")
    {
        if($this->status_id == $status_id)
        {
            return;
        }
        
        $st = $this->status_id;

        // if($this->status_id != ConfigHelper::config('solicitud_status_inscrito'))
        {
            $this->status_id = $status_id;
            $this->save();
        }

        if($status_id == ConfigHelper::config('solicitud_status_inscrito'))
        {
            $this->viajero->setStatus( $status_id );
            $this->save();
        }

        \VCN\Models\Solicitudes\SolicitudLogStatus::addLog($this, $st, $notas);
    }

    public function setInscrito($log="")
    {
        // if($this->status_id != ConfigHelper::config('solicitud_status_inscrito'))
        {
            $this->viajero->solicitud_id = 0;
            $this->viajero->save();

            $st = ConfigHelper::config('solicitud_status_inscrito');
            $this->status_id = $st;
            $this->save();

            \VCN\Models\Solicitudes\SolicitudLogStatus::addLog($this, $st, $log);
            ViajeroLog::addLog($this->viajero, "Solicitud [$this->id] => Inscrito ($log)");
        }
    }

    public function scopePlataforma($query)
    {
        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            $viajeros = Viajero::plataforma()->pluck('id');
            return $query->whereIn('viajero_id', $viajeros);
        }

        return $query;
    }

    public static function listadoFiltros($valores)
    {
        $solicitudes = null;

        $f = isset($valores['status'])?$valores['status']:0;
        if($f)
        {
            if(is_array($f))
            {
                $st = \VCN\Models\Solicitudes\Status::whereIn('id',$f)->pluck('id')->toArray();
            }
            else
            {
                $st = \VCN\Models\Solicitudes\Status::where('id',$f)->pluck('id')->toArray();
            }
        }
        else
        {
            $st = [];
            $st[] = ConfigHelper::config('solicitud_status_lead');
            $st[] = ConfigHelper::config('solicitud_status_contactado');
            $st[] = ConfigHelper::config('solicitud_status_futuro');
            $st[] = ConfigHelper::config('solicitud_status_decidiendo');
            $st[] = ConfigHelper::config('solicitud_status_archivado');
            $st[] = ConfigHelper::config('solicitud_status_inscrito');
        }

        //Fechas
        if(isset($valores['desdec']) && isset($valores['hastac']))
        {
            if($valores['desdec'] && $valores['hastac'])
            {
                $fini = Carbon::createFromFormat('d/m/Y',$valores['desdec'])->format('Y-m-d');
                $ffin = Carbon::createFromFormat('d/m/Y',$valores['hastac'])->format('Y-m-d');

                $solicitudes = Solicitud::whereIn('status_id',$st)->where('fecha','>=',$fini)->where('fecha','<=',$ffin);
            }
            else
            {
                $solicitudes = Solicitud::whereIn('status_id',$st);
            }
        }
        else
        {
            //full-admin
            $solicitudes = Solicitud::whereIn('status_id',$st);
        }

        //oficinas
        $f = isset($valores['oficinas'])?$valores['oficinas']:0;
        if(is_array($f) && count($f)==1)
        {
            if($f[0]==0)
            {
                $f = null;
            }
        }
        if($f)
        {
            if(is_array($f))
            {
                $solicitudes = $solicitudes->whereIn('oficina_id',$f);
            }
            else
            {
                $solicitudes = $solicitudes->where('oficina_id',$f);
            }
        }

        //Asignado
        $f = isset($valores['asignados']) ? $valores['asignados'] : 0;
        if(is_array($f) && count($f)==1)
        {
            if($f[0]==0)
            {
                $f = null;
            }
        }
        if($f)
        {
            if(is_array($f))
            {
                $solicitudes = $solicitudes->whereIn('user_id',$f);
            }
            else
            {
                $solicitudes = $solicitudes->where('user_id',$f);
            }
        }

        //categorias
        $f = isset($valores['categorias'])?$valores['categorias']:0;
        if(is_array($f) && count($f)==1)
        {
            if($f[0]==0)
            {
                $f = null;
            }
        }
        if($f)
        {
            if(is_array($f))
            {
                $solicitudes = $solicitudes->whereIn('category_id',$f);
            }
            else
            {
                $solicitudes = $solicitudes->where('category_id',$f);
            }
        }

        //subcategorias
        $f = isset($valores['subcategorias'])?$valores['subcategorias']:0;
        if(is_array($f) && count($f)==1)
        {
            if($f[0]==0)
            {
                $f = null;
            }
        }
        if($f)
        {
            if(is_array($f))
            {
                $solicitudes = $solicitudes->whereIn('subcategory_id',$f);
            }
            else
            {
                $solicitudes = $solicitudes->where('subcategory_id',$f);
            }
        }

        //subcategoriasdet
        $f = isset($valores['subcategoriasdet'])?$valores['subcategoriasdet']:0;
        if(is_array($f) && count($f)==1)
        {
            if($f[0]==0)
            {
                $f = null;
            }
        }
        if($f)
        {
            if(is_array($f))
            {
                $solicitudes = $solicitudes->whereIn('subcategory_det_id',$f);
            }
            else
            {
                $solicitudes = $solicitudes->where('subcategory_det_id',$f);
            }
        }

        return $solicitudes;

    }

    public function setCatalogo($user, $catenviado=1)
    {
        $fichaOld = clone $this;

        $viajero = $this->viajero;

        if($fichaOld->catalogo_enviado != $catenviado)
        {
            $log = "Catálogo enviado [". ($catenviado ? 'ON':'OFF') ."]";
            ViajeroLog::addLog($viajero, $log);

            $this->catalogo_enviado_log = "";
            $this->catalogo_enviado = false;
            
            if($catenviado)
            {
                $fecha = Carbon::now()->format('d/m/Y H:i');
                $log = "[$user->full_name : $fecha ]";
                $this->catalogo_enviado = true;
                $this->catalogo_enviado_log = $log;
            }
            $this->save();
        }
    }
}
