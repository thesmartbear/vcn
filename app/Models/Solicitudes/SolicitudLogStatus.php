<?php

namespace VCN\Models\Solicitudes;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Solicitudes\Solicitud;

use Auth;
use Session;
use ConfigHelper;

class SolicitudLogStatus extends Model
{
    protected $table = 'solicitud_log_status';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function solicitud()
    {
        return $this->belongsTo('\VCN\Models\Solicitudes\Solicitud');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User','user_id');
    }

    public static function addLog( Solicitud $solicitud, $st1, $notas="")
    {
        $user = Auth::user();
        if($user && $user->id == 1)
        {
            $user = null;
        }

        $log = new self;
        $log->solicitud_id = $solicitud->id;
        $log->user_id =  $user ? $user->id : ($solicitud->user_id ?: 0);

        $log->status1 = $st1;
        $log->status2 = $solicitud->status_id;

        $log->notas = $notas;

        $log->save();

    }

    public static function addLogBug( Solicitud $solicitud)
    {
        $user_id = $solicitud->user_id ?: 0;
        $st0 = ConfigHelper::config('solicitud_status_lead');
        $notas = "BUG";

        //si no está inscrito tb creamos el LEAD
        if($solicitud->status_id != $st0)
        {
            $log = new self;
            $log->solicitud_id = $solicitud->id;
            $log->user_id =  $user_id;
            $log->status1 = 0;
            $log->status2 = $st0;
            $log->notas = $notas;
            
            $log->created_at = $solicitud->created_at;
            $log->updated_at = $solicitud->created_at;
            $log->save();
        }
        
        $log = new self;
        $log->solicitud_id = $solicitud->id;
        $log->user_id =  $user_id;

        $log->status1 = $st0;
        $log->status2 = $solicitud->status_id;

        $log->notas = $notas;

        $log->created_at = $solicitud->updated_at;
        $log->updated_at = $solicitud->updated_at;

        $log->save();

    }
}
