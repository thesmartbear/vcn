<?php

namespace VCN\Models\Solicitudes;

use Illuminate\Database\Eloquent\Model;

class StatusChecklist extends Model
{
    protected $table = 'status_checklists';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'seguimiento' => 'array',
    ];

    public function status()
    {
        return $this->belongsTo('\VCN\Models\Solicitudes\Status');
    }

    public function categoria()
    {
        return $this->belongsTo('\VCN\Models\Categoria', 'category_id');
    }

    public function subcategoria()
    {
        return $this->belongsTo('\VCN\Models\Subcategoria', 'subcategory_id');
    }

    public function subcategoria_detalle()
    {
        return $this->belongsTo('\VCN\Models\SubcategoriaDetalle', 'subcategory_det_id');
    }

}
