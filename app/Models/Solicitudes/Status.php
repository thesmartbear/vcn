<?php

namespace VCN\Models\Solicitudes;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'statuses';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function checklist()
    {
        return $this->hasMany('\VCN\Models\Solicitudes\StatusChecklist');
    }

    public function getNextAttribute()
    {
        return Status::where('orden','>',$this->orden)->orderBy('orden')->first();
    }

    public function getChecklistSolicitud( $solicitud_id )
    {
        $ficha = Solicitud::find($solicitud_id);

        $ret = collect();

        if(!$ficha)
        {
            return $ret;
        }

        $status = $this->id;

        $statuses = \VCN\Models\Solicitudes\Status::orderBy('orden');
        foreach( $statuses->get() as $st )
        {
            $sts = \VCN\Models\Solicitudes\StatusChecklist::where('status_id', $st->id)->orderBy('orden');
            if( $sts->count()>0 )
            {
                foreach( $sts->get() as $st )
                {
                    $bSt = false;
                    // if($st->status_id == $status) //Ahora se muestran todas
                    {
                        if( $st->category_id==0 && $st->subcategory_id==0 && $st->subcategory_det_id==0 )
                        {
                            if(!$bSt)
                            {
                                $ret->push($st);
                                $bSt = true;
                            }
                        }

                        if( $st->category_id==$ficha->category_id && $st->subcategory_id==0 && $st->subcategory_det_id==0 )
                        {
                            if(!$bSt)
                            {
                                $ret->push($st);
                                $bSt = true;
                            }
                        }

                        if( $st->category_id==$ficha->category_id && $st->subcategory_id==$ficha->subcategory_id && $st->subcategory_det_id==0 )
                        {
                            if(!$bSt)
                            {
                                $ret->push($st);
                                $bSt = true;
                            }
                        }

                        if( $st->category_id==$ficha->category_id && $st->subcategory_id==$ficha->subcategory_id && $st->subcategory_det_id==$ficha->subcategory_det_id )
                        {
                            if(!$bSt)
                            {
                                $ret->push($st);
                                $bSt = true;
                            }
                        }
                    }
                }

                // $ret = $this->hasMany('\VCN\Models\Solicitudes\StatusChecklist')
                //     ->orWhere(function ($query) use ($status) {
                //         $query->where('status_id',$status)
                //             ->where('category_id', 0)
                //             ->where('subcategory_id', 0)
                //             ->where('subcategory_det_id', 0);
                //     })
                //     ->orWhere(function ($query) use ($ficha, $status) {
                //         $query->where('status_id',$status)
                //             ->where('category_id', $ficha->category_id)
                //             ->where('subcategory_id', null)
                //             ->where('subcategory_det_id', null);
                //     })
                //     ->orWhere(function ($query) use ($ficha, $status) {
                //         $query->where('status_id',$status)
                //             ->where('category_id', $ficha->category_id)
                //             ->where('subcategory_id', $ficha->subcategory_id)
                //             ->where('subcategory_det_id', null);
                //     })
                //     ->orWhere(function ($query) use ($ficha, $status) {
                //         $query->where('status_id',$status)
                //             ->where('category_id', $ficha->category_id)
                //             ->where('subcategory_id', $ficha->subcategory_id)
                //             ->where('subcategory_det_id', $ficha->subcategory_det_id);
                //     })->get();

            }
        }

        return $ret;
    }
}
