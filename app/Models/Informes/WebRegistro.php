<?php

namespace VCN\Models\Informes;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\User;
use VCN\Models\Leads\ViajeroLog;
use VCN\Helpers\MailHelper;

use ConfigHelper;
use Carbon;

class WebRegistro extends Model
{
    protected $table = 'informe_web_registros';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $dates = ['fecha'];

    protected $casts = [
        'registros_home_viajeros' => 'array',
        'registros_home_tutores' => 'array',
        'registros_info_viajeros' => 'array',
        'registros_info_tutores' => 'array',
    ];

    public function curso()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Curso', 'curso_id');
    }

    public static function add(User $user)
    {
        //Contabilizamos
        $curso_id = $user->registro['curso_id'];

        $plataforma = ConfigHelper::config('propietario');
        $fecha = Carbon::now();

        $visita = Self::where('plataforma',$plataforma)->where('fecha',$fecha->format('Y-m-d'))->where('curso_id',$curso_id)->first();
        if(!$visita)
        {
            $visita = new Self;
            $visita->plataforma = $plataforma;
            $visita->curso_id = $curso_id;
            $visita->fecha = $fecha;
            $visita->any = $fecha->year;
            $visita->semana = $fecha->weekOfYear;
        }

        if($curso_id)
        {
            $visita->registros_info++;
        }
        else
        {
            $visita->registros_home++;
        }

        if($user->es_viajero)
        {
            if($curso_id)
            {
                $arr = $visita->registros_info_viajeros?:[];
                array_push($arr,$user->id);
                $visita->registros_info_viajeros = $arr;

                ViajeroLog::addLog($user->viajero, "Registro Curso [$curso_id]");
            }
            else
            {
                $arr = $visita->registros_home_viajeros?:[];
                array_push($arr,$user->id);
                $visita->registros_home_viajeros = $arr;

                ViajeroLog::addLog($user->viajero, "Registro Web Home");
            }

        }
        elseif($user->es_tutor)
        {
            if($curso_id)
            {
                $arr = $visita->registros_info_tutores?:[];
                array_push($arr,$user->id);
                $visita->registros_info_tutores = $arr;
            }
            else
            {
                $arr = $visita->registros_home_tutores?:[];
                array_push($arr,$user->id);
                $visita->registros_home_tutores = $arr;
            }
        }

        $visita->save();

        $user->registro = null;
        $user->save();

        MailHelper::mailWebRegistro($user->id);
    }
}
