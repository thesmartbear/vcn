<?php

namespace VCN\Models\Informes;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\User;
use VCN\Models\Leads\ViajeroLog;

use Carbon;
use ConfigHelper;

class WebVisita extends Model
{
    protected $table = 'informe_web_visitas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $dates = ['fecha'];

    public function curso()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Curso', 'curso_id');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public function viajero()
    {
        return $this->belongsTo('\VCN\Models\Leads\Viajero', 'viajero_id');
    }

    public function tutor()
    {
        return $this->belongsTo('\VCN\Models\Leads\Tutor', 'tutor_id');
    }

    private static function buscar($curso_id, $user_id)
    {
        $plataforma = ConfigHelper::config('propietario');
        $fecha = Carbon::now();

        $visita = Self::where('plataforma',$plataforma)->where('fecha',$fecha->format('Y-m-d'))->where('curso_id',$curso_id)->where('user_id',$user_id)->first();
        if(!$visita)
        {
            $visita = new Self;
            $visita->plataforma = $plataforma;
            $visita->curso_id = $curso_id;
            $visita->fecha = $fecha;
            $visita->any = $fecha->year;
            $visita->semana = $fecha->weekOfYear;
            $visita->user_id = $user_id;
            $visita->save();
        }

        return $visita;
    }

    public static function add($curso_id, $user_id)
    {
        $user = User::find($user_id);
        if($user && !$user->es_viajero && !$user->es_tutor)
        {
            return;
        }

        $plataforma = ConfigHelper::config('propietario');
        $fecha = Carbon::now();

        $visita = Self::buscar($curso_id, $user_id);

        $visita->visitas++;
        $visita->save();

        if($user && $user->es_viajero)
        {
            ViajeroLog::addLog($user->viajero, "Visita Curso [$curso_id]");
        }
    }

    public static function addProveedor($curso_id, $user_id)
    {
        $user = User::find($user_id);
        if($user && !$user->es_viajero && !$user->es_tutor)
        {
            return;
        }

        $visita = Self::buscar($curso_id, $user_id);

        $visita->visitas_proveedor++;
        $visita->save();

        if($user && $user->es_viajero)
        {
            ViajeroLog::addLog($user->viajero, "Visita Proveedor Curso [$curso_id]");
        }
    }
}
