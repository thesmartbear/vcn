<?php

namespace VCN\Models\Informes;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Booking;

class Presupuesto extends Model
{
    protected $table = 'informe_presupuestos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    // 'any','semana','oficina_id','category_id','subcategory_id','subcategory_det_id','presupuestos','semanas','bookings'
    // 'pais_id', 'proveedor_id', 'prescriptor_id', 'origen_id', 'suborigen_id', 'suborigendet_id', 'prescriptor_id', 'pais_id', 'proveedor_id'

    protected $dates = ['fecha'];

    protected $casts = [
        'bookings' => 'array',
    ];


    public static function add(Booking $booking)
    {
        $user_id = $booking->user_id;

        $bInsert = true;
        if($bInsert)
        {
            //fecha = created_at
            $fecha = $booking->created_at;
            $curso = $booking->curso;
            $centro = $booking->centro;
            $viajero = $booking->viajero;
            $oficina_id = $booking->oficina_id?:0;

            $semana = $fecha->weekOfYear;
            $semanas = $booking->semanas ?: 0;

            $venta = Self::where('user_id',$user_id)->where('fecha',$fecha->format('Y-m-d'))
                ->where('oficina_id',$oficina_id)
                ->where('category_id',$curso->category_id?:0)
                ->where('subcategory_id',$curso->subcategory_id?:0)
                ->where('proveedor_id',$centro->provider_id?:0)
                ->where('pais_id',$centro->pais_id?:0)
                ->where('curso_id',$curso->id?:0)
                ->first();

            if(!$venta)
            {
                $venta = new Self;
                $venta->fecha = $fecha;

                $venta->oficina_id = $oficina_id;
                $venta->category_id = $curso->category_id?$curso->category_id:0;
                $venta->subcategory_id = $curso->subcategory_id?$curso->subcategory_id:0;

                $venta->proveedor_id = $centro->provider_id?:0;
                $venta->pais_id = $centro->pais_id?:0;
                $venta->curso_id = $curso->id?:0;

                $venta->presupuestos = 1;
                $venta->semanas = $semanas;
                $venta->bookings = [0=>$booking->id];
                $venta->user_id = $user_id;

                $venta->save();
            }
            else
            {
                $b = $venta->bookings;

                if(!in_array($booking->id,$b))
                {
                    $venta->semanas += $semanas;
                    $venta->presupuestos += 1;

                    array_push($b,$booking->id);
                    $venta->bookings = $b;

                    $venta->save();
                }
            }
        }
    }

    public static function remove(Booking $booking)
    {
        $user_id = $booking->user_id;

        //fecha = created_at
        $fecha = $booking->created_at;
        $curso = $booking->curso;
        $centro = $booking->centro;
        $viajero = $booking->viajero;
        $oficina_id = $booking->oficina_id;

        $semana = $fecha->weekOfYear;
        $semanas = $booking->semanas;

        $ventas = Self::where('user_id',$user_id)->where('fecha',$fecha->format('Y-m-d'))
                ->where('oficina_id',$oficina_id)
                ->where('category_id',$curso->category_id?$curso->category_id:0)
                ->where('subcategory_id',$curso->subcategory_id?$curso->subcategory_id:0)
                ->where('proveedor_id',$centro->provider_id?:0)
                ->where('pais_id',$centro->pais_id?:0)
                ->where('curso_id',$curso->id?:0);

        // dd($ventas->get());

        if($ventas->count()>0)
        {
            foreach($ventas->get() as $venta)
            {
                $b = $venta->bookings;
                if(in_array($booking->id,$b))
                {
                    $venta->presupuestos -= 1;
                    $venta->semanas -= $semanas;

                    $b = array_diff($b, array($booking->id));
                    $venta->bookings = $b;

                    $venta->save();
                    break;
                }
            }
        }

        return;
    }
}
