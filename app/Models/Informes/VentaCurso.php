<?php

namespace VCN\Models\Informes;

use Illuminate\Database\Eloquent\Model;

class VentaCurso extends Venta
{
    protected $table = 'informe_ventas_curso';
}