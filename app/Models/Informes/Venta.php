<?php

namespace VCN\Models\Informes;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Booking;

use ConfigHelper;
use Carbon;

class Venta extends Model
{
    protected $table = 'informe_ventas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    // 'any','semana','oficina_id','category_id','subcategory_id','subcategory_det_id','inscripciones','semanas','bookings'
    // 'pais_id', 'proveedor_id', 'prescriptor_id', 'origen_id', 'suborigen_id', 'suborigendet_id', 'prescriptor_id', 'pais_id', 'proveedor_id'

    protected $casts = [
        'bookings' => 'array',
    ];

    /**
     * Funcionaminto:
     *     Venta +1 : booking_status_prebooking
     *     Venta -1 : booking eliminado
     */

    public function oficina()
    {
        return $this->belongsTo('\VCN\Models\System\Oficina', 'oficina_id');
    }

    public function pais()
    {
        return $this->belongsTo('\VCN\Models\Pais', 'pais_id');
    }

    public function proveedor()
    {
        return $this->belongsTo('\VCN\Models\Proveedores\Proveedor', 'proveedor_id');
    }

    public function prescriptor()
    {
        return $this->belongsTo('VCN\Models\Prescriptores\Prescriptor', 'prescriptor_id');
    }

    public function categoria()
    {
        return $this->belongsTo('\VCN\Models\Categoria', 'category_id');
    }

    public function subcategoria()
    {
        return $this->belongsTo('\VCN\Models\Subcategoria', 'subcategory_id');
    }

    public function subcategoria_det()
    {
        return $this->belongsTo('\VCN\Models\SubcategoriaDetalle', 'subcategory_det_id');
    }

    public function origen()
    {
        return $this->belongsTo('\VCN\Models\Leads\Origen', 'origen_id');
    }

    public function suborigen()
    {
        return $this->belongsTo('\VCN\Models\Leads\Suborigen', 'suborigen_id');
    }

    public function suborigen_det()
    {
        return $this->belongsTo('\VCN\Models\Leads\SuborigenDetalle', 'suborigendet_id');
    }

    public function getFullNameCategoriaAttribute()
    {
        $ret = $this->categoria?$this->categoria->name:"-";

        if($this->subcategoria)
        {
            $ret .= " > ". $this->subcategoria->name;
        }

        if($this->subcategoria_det)
        {
            $ret .= " > ". $this->subcategoria_det->name;
        }

        return $ret;
    }

    public function getFullNameCategoriaIdAttribute()
    {
        $ret = $this->categoria?$this->categoria->id:0;

        if($this->subcategoria)
        {
            $ret .= "-". $this->subcategoria->id;
        }

        if($this->subcategoria_det)
        {
            $ret .= "-". $this->subcategoria_det->id;
        }

        return $ret;
    }

    public function getFullNameOrigenAttribute()
    {
        $ret = $this->origen?$this->origen->name:"-";

        if($this->suborigen)
        {
            $ret .= " > ". $this->suborigen->name;
        }

        if($this->suborigen_det)
        {
            $ret .= " > ". $this->suborigen_det->name;
        }

        return $ret;
    }

    public static function updateBooking(Booking $booking, $force=true)
    {
        self::remove($booking);
        self::add($booking, $force);
    }

    public static function add(Booking $booking, $force=false)
    {
        $booking->semanas = $booking->semanas_unit;
        $booking->save();

        self::addVenta($booking, $force);
        self::addVenta($booking, $force, true);

        $booking->curso->updateDataWebPlazas();
    }

    private static function addVenta(Booking $booking, $force=false, $esCurso=false)
    {
        $oficina_id = $booking->oficina_id;
        $user_id = $booking->user_id;

        if(!$oficina_id)
        {
            $booking->oficina_id = $booking->viajero->oficina_asignada;
            $booking->save();

            $oficina_id = $booking->oficina_id;
        }

        // $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');

        $bInsert = false;
        if( $booking->status_id == ConfigHelper::config('booking_status_prebooking') && $oficina_id>0)
        {
            $bInsert = true;
        }

        if($force)
        {
            $bInsert = true;
        }

        if($bInsert)
        {
            // $fecha = $booking->fecha_pago1?:($booking->fecha_reserva?:$booking->created_at);
            $fecha = $booking->fecha_reserva ?: $booking->created_at;
            $fecha_curso = null;
            $semana_curso = 0;

            if($esCurso)
            {
                $Clase = new VentaCurso;
                $fecha_curso = $booking->fecha_curso ?: $booking->created_at; //course_start_date
                // $semana_curso = $fecha_curso->weekOfYear;
                $semana_curso = $fecha->weekOfYear;
                
                $mes_curso = $fecha_curso->month;
                if( $mes_curso==1 && $semana_curso>=52)
                {
                    $semana_curso = 1;
                }

                $anyBooking = $fecha->year;
                $anyCurso = $fecha_curso->year;
                if( $anyBooking < $anyCurso )
                {
                    $semana_curso = 1;
                }

            }
            else
            {
                $Clase = new Venta;
                // $fecha = $booking->fecha_pago1?:($booking->fecha_reserva?:$booking->created_at);
            }

            $curso = $booking->curso;
            $centro = $booking->centro;
            $viajero = $booking->viajero;

            $semana = $fecha->weekOfYear;
            $mes = $fecha->month;
            if( $mes==1 && $semana>=52)
            {
                $semana = 1;
            }

            $semanas = $booking->semanas_unit ?: 0;

            $venta = $Clase::where('user_id',$user_id)->where('oficina_id',$oficina_id);
            $venta = $venta->where('any',$fecha->format('Y'))->where('semana',$semana);

            if($esCurso)
            {
                $venta = $venta->where('curso_any',$fecha_curso->format('Y'))->where('curso_semana',$semana_curso);
            }

            $origen_id = $viajero->origen_id?:0;
            $suborigen_id = $viajero->suborigen_id?:0;
            $suborigendet_id = $viajero->suborigendet_id?:0;

            $catId = $booking->category_id ?: ($curso->category_id?:0);
            $scatId = $booking->subcategory_id ?: ($curso->subcategory_id?:0);
            $scatdetId = $booking->subcategory_det_id ?: ($curso->subcategory_det_id?:0);

            //cambios a futuro
            $f = $booking->fecha_curso ?: $booking->created_at;
            if( $f->gt( Carbon::now() ) )
            {
                if($catId != $curso->category_id)
                {
                    $booking->category_id = $curso->category_id;
                }

                if($scatId != $curso->subcategory_id)
                {
                    $booking->subcategory_id = $curso->subcategory_id;
                }

                if($scatdetId != $curso->subcategory_det_id)
                {
                    $booking->subcategory_det_id = $curso->subcategory_det_id;
                }

                $booking->save();
                
            }

            $venta = $venta->where('category_id', $catId)
                ->where('subcategory_id', $scatId)
                ->where('subcategory_det_id', $scatdetId)
                ->where('origen_id',$origen_id)
                ->where('suborigen_id',$suborigen_id)
                ->where('suborigendet_id',$suborigendet_id)
                ->where('proveedor_id',$centro->provider_id?:0)
                ->where('pais_id',$centro->pais_id?:0)
                ->where('prescriptor_id',$booking->prescriptor_id?:0)
                ->where('es_directo', $booking->es_directo)
                ->first();

            $booking->origen_id = $origen_id;
            $booking->suborigen_id = $suborigen_id;
            $booking->suborigendet_id = $suborigendet_id;
            $booking->proveedor_id = $centro->provider_id?:0;
            $booking->pais_id = $centro->pais_id?:0;
            $booking->save();

            $moneda = ConfigHelper::default_moneda();

            $total = $booking->course_total_amount;
            $m_id = $booking->course_currency_id;
            if($m_id != $moneda->id)
            {
                $total = $total * $booking->getMonedaTasa($m_id);
            }

            if(!$venta)
            {
                $venta = new $Clase;
                $venta->oficina_id = $oficina_id?:0;

                $venta->any = $fecha->year;
                $venta->semana = $semana;

                if($esCurso)
                {
                    $venta->curso_any = $fecha_curso->year;
                    $venta->curso_semana = $semana_curso;
                }

                $venta->category_id = $catId;
                $venta->subcategory_id = $scatId;
                $venta->subcategory_det_id = $scatdetId;

                $venta->origen_id = $origen_id;
                $venta->suborigen_id = $suborigen_id;
                $venta->suborigendet_id = $suborigendet_id;

                $venta->proveedor_id = $centro->provider_id?:0;
                $venta->pais_id = $centro->pais_id?:0;
                $venta->prescriptor_id = $booking->prescriptor_id?:0;

                $venta->inscripciones = 1;
                $venta->semanas = $semanas;
                $venta->bookings = [0=>$booking->id];

                $venta->total_curso = $total;
                $venta->total = $booking->total;

                $venta->user_id = $user_id;
                $venta->es_directo = $booking->es_directo;

                $venta->save();

            }
            else
            {
                $b = $venta->bookings;

                if(!in_array($booking->id,$b))
                {
                    $venta->semanas += $semanas;
                    $venta->inscripciones += 1;

                    $venta->total_curso += $total;
                    $venta->total += $booking->total;

                    array_push($b,$booking->id);
                    $venta->bookings = $b;

                    $venta->save();
                }
            }

            // 'any','semana','oficina_id','category_id','subcategory_id','subcategory_det_id','origen_id','suborigen_id','suborigendet_id','inscripciones','semanas','bookings'
        }
    }

    public static function remove(Booking $booking)
    {
        self::removeVenta($booking);
        self::removeVenta($booking, true);

        $booking->curso->updateDataWebPlazas();
    }

    private static function removeVenta(Booking $booking, $esCurso = false)
    {
        if($booking->fase<4)
        {
            return;
        }

        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        $borrar = Booking::where('id',$booking->id)->whereIn('status_id',$stplazas)->first();
        if(!$borrar)
        {
            return;
        }

        // $fecha = $booking->fecha_pago1?:($booking->fecha_reserva?:$booking->created_at);
        $fecha = $booking->fecha_reserva?:$booking->created_at;
        $fecha_curso = null;
        $semana_curso = 0;

        if($esCurso)
        {
            $Clase = new VentaCurso;
            $fecha_curso = $booking->fecha_curso?:$booking->created_at;
            // $semana_curso = $fecha_curso->weekOfYear;
            $semana_curso = $fecha->weekOfYear;
            
            $mes_curso = $fecha_curso->month;
            if( $mes_curso==1 && $semana_curso>=52)
            {
                $semana_curso = 1;
            }

            $anyBooking = $fecha->year;
            $anyCurso = $fecha_curso->year;
            if( $anyBooking < $anyCurso )
            {
                $semana_curso = 1;
            }
        }
        else
        {
            $Clase = new Venta;
            // $fecha = $booking->fecha_pago1?:($booking->fecha_reserva?:$booking->created_at);
        }

        $user_id = $booking->user_id;

        $curso = $booking->curso;
        $viajero = $booking->viajero;
        $oficina_id = $booking->oficina_id;
        $semana = $fecha->weekOfYear;
        $mes = $fecha->month;
        if( $mes==1 && $semana>=52)
        {
            $semana = 1;
        }
            
        $semanas = $booking->semanas_unit;

        $catId = $booking->category_id ?: ($curso->category_id?:0);
        $scatId = $booking->subcategory_id ?: ($curso->subcategory_id?:0);
        $scatdetId = $booking->subcategory_det_id ?: ($curso->subcategory_det_id?:0);

        $ventas = $Clase::where('user_id',$user_id)->where('oficina_id',$oficina_id);
        $ventas = $ventas->where('any',$fecha->format('Y'))->where('semana',$semana);

        if($esCurso)
        {
            $ventas = $ventas->where('curso_any',$fecha_curso->format('Y'))->where('curso_semana',$semana_curso);
        }

        $ventas = $ventas->where('category_id', $catId)
                ->where('subcategory_id', $scatId)
                ->where('subcategory_det_id', $scatdetId);
                // ->where('origen_id',$viajero->origen_id?$viajero->origen_id:0)
                // ->where('suborigen_id',$viajero->suborigen_id?$viajero->suborigen_id:0)
                // ->where('suborigendet_id',$viajero->suborigendet_id?$viajero->suborigendet_id:0)
                // ->where('proveedor_id',$centro->provider_id?:0)
                // ->where('pais_id',$centro->pais_id?:0)
                // ->where('prescriptor_id',$booking->prescriptor_id?:0)
                // ->first();

        // dd($ventas->get());

        if($ventas->count()>0)
        {
            foreach($ventas->get() as $venta)
            {
                $b = $venta->bookings;
                if(in_array($booking->id,$b))
                {
                    $moneda = ConfigHelper::default_moneda();

                    $total = $booking->course_total_amount;
                    $m_id = $booking->course_currency_id;
                    if($m_id != $moneda->id)
                    {
                        $total = $total * $booking->getMonedaTasa($m_id);
                    }

                    $venta->inscripciones -= 1;
                    $venta->semanas -= $semanas;
                    $venta->total_curso -= $total;
                    $venta->total -= $booking->total;

                    $b = array_diff($b, array($booking->id));
                    $venta->bookings = $b;

                    $venta->save();
                    break;
                }
            }
        }

        return;
    }

    public static function procesarBookings($any=null)
    {
        ini_set('memory_limit', '600M');
        set_time_limit(60000);

        $iRet = 0;

        $ventas = Venta::where('bookings','<>','');
        $ventasc = VentaCurso::where('bookings','<>','');

        if($any)
        {
            $ventas = $ventas->where('any',$any);
            $ventasc = $ventasc->where('any',$any);
        }

        //borramos ventas:
        $ventas->delete();
        $ventasc->delete();

        //pasamos los bookings actuales a ventas
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        
        $bookings = Booking::whereIn('status_id',$stplazas);

        if($any)
        {
            $desde = "$any-01-01";
            $hasta = "$any-12-31";
            
            $bookings = $bookings->whereBetween('created_at', [$desde,$hasta]);
        }
        
        // $bookings = $bookings->groupBy('id')->get();
        echo "Bookings ". $bookings->count();
        $bookings->chunk(200, function($bookings) use ($iRet)
        {
            echo ".";
            foreach ($bookings as $b)
            {
                self::add($b, true);
                $iRet++;    
            }
        });
        
        return $iRet;
    }
}