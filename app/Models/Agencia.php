<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;

class Agencia extends Model
{
    protected $table = 'agencias';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function vuelos()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\Vuelo', 'agencia_id');
    }
}
