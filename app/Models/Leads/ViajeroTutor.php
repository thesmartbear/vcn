<?php

namespace VCN\Models\Leads;

use Illuminate\Database\Eloquent\Model;


class ViajeroTutor extends Model
{
    protected $table = 'viajero_tutores';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function viajero()
    {
        return $this->belongsTo('\VCN\Models\Leads\Viajero');
    }

    public function tutor()
    {
        return $this->belongsTo('\VCN\Models\Leads\Tutor');
    }

}
