<?php

namespace VCN\Models\Leads;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Informes\Venta;

use ConfigHelper;
use DB;

class Origen extends Model
{
    protected $table = 'viajero_origenes';

    // protected $fillable = [];
    protected $guarded = ['_token'];


    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('name')->get();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return self::where('propietario',$filtro)->orWhere('propietario',0)->orderBy('name')->get();
        }

        return self::orderBy('name')->get();
    }


    public function suborigenes()
    {
        return $this->hasMany('\VCN\Models\Leads\Suborigen', 'origen_id');
    }

    public function getVentas($any, $oficina_id=0)
    {
        $id = $this->id;

        //cambiado 11-05-2018 : qieren q se acumule

        if($oficina_id)
        {
            // $ventas = Venta::where('any',$any)->where('oficina_id',$oficina_id)->where('origen_id',$id)->where('suborigen_id',0)->where('suborigendet_id',0)->groupBy('category_id')->select('*', DB::raw('sum(inscripciones) as total_bookings'), DB::raw('sum(semanas) as total_semanas'),DB::raw('sum(total_curso) as total_curso'),DB::raw('sum(total) as total_total'));
            
            $ventas = Venta::where('any',$any)->where('oficina_id',$oficina_id)->where('origen_id',$id)->groupBy('category_id')->select('*', DB::raw('sum(inscripciones) as total_bookings'), DB::raw('sum(semanas) as total_semanas'),DB::raw('sum(total_curso) as total_curso'),DB::raw('sum(total) as total_total'));
        }
        else
        {
            // $ventas = Venta::where('any',$any)->where('origen_id',$id)->where('suborigen_id',0)->where('suborigendet_id',0)->groupBy('category_id')->select('*', DB::raw('sum(inscripciones) as total_bookings'), DB::raw('sum(semanas) as total_semanas'),DB::raw('sum(total_curso) as total_curso'),DB::raw('sum(total) as total_total'));
            
            $ventas = Venta::where('any',$any)->where('origen_id',$id)->groupBy('category_id')->select('*', DB::raw('sum(inscripciones) as total_bookings'), DB::raw('sum(semanas) as total_semanas'),DB::raw('sum(total_curso) as total_curso'),DB::raw('sum(total) as total_total'));
        }

        return $ventas->get();
    }

    public function getHayVentasAttribute()
    {
        return Venta::where('origen_id',$this->id)->count();
    }
}
