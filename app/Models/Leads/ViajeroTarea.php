<?php

namespace VCN\Models\Leads;

use Illuminate\Database\Eloquent\Model;

use Carbon;

class ViajeroTarea extends Model
{
    protected $table = 'viajero_tareas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function viajero()
    {
        return $this->belongsTo('\VCN\Models\Leads\Viajero', 'viajero_id');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public function asignado()
    {
        return $this->belongsTo('\VCN\Models\User', 'asign_to');
    }

    public function getOficinaIdAttribute()
    {
        return $this->asignado->oficina_id;
    }

    public function scopeVencidas($query)
    {

        $now = Carbon::now();
        $now1 = Carbon::now()->subMinute();

        return $query->where('estado',0)->where('fecha','>',$now1)->where('fecha','<',$now);
    }

    public function getEsVencidaAttribute()
    {
        $fecha = Carbon::parse($this->fecha);

        return $fecha->isPast();
    }
}
