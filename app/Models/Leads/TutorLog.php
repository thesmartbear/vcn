<?php

namespace VCN\Models\Leads;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Leads\Tutor;
use VCN\Models\Bookings\Booking;

use VCN\Models\User;

use Auth;

class TutorLog extends Model
{
    protected $table = 'tutor_logs';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'data'=> 'array',
    ];

    public function tutor()
    {
        return $this->belongsTo('\VCN\Models\Leads\Tutor');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public static function addLog( $tutor, $txt, $tipo="log", $data=null )
    {
        if(!$tutor)
        {
            return;
        }

        $log = new self;
        $log->tutor_id = $tutor->id;
        $log->tipo = $tipo;
        $log->user_id = Auth::user()?Auth::user()->id:0;

        $log->notas = $txt;
        $log->data = $data;

        $log->save();
    }

    public static function addLogData( self $tutor, $txt, $data )
    {
        self::addLog($tutor, $txt, "log", $data);
    }

    public static function addLogCreado( self $tutor )
    {
        $log = new self;
        $log->tutor_id = $tutor->id;
        $log->tipo = "Creación";
        $log->user_id = Auth::user()->id;

        $log->notas = "Creación Tutor";

        $log->save();
    }

    public static function addLogBooking( self $tutor, Booking $booking )
    {
        $log = new self;
        $log->tutor_id = $tutor->id;
        $log->tipo = "Booking";
        $log->user_id = Auth::user()->id;

        $log->notas = "Nuevo Booking ". $booking->id;

        $log->save();
    }
}
