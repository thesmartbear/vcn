<?php

namespace VCN\Models\Leads;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Leads\Viajero;
use VCN\Models\Prescriptores\Prescriptor;

use VCN\Models\Solicitudes\Status;
use VCN\Models\Solicitudes\StatusChecklist;

use VCN\Models\User;
use VCN\Models\System\Oficina;

use Auth;

class ViajeroLog extends Model
{
    protected $table = 'viajero_logs';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'data'=> 'array',
    ];

    public function viajero()
    {
        return $this->belongsTo('\VCN\Models\Leads\Viajero');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public function asignado()
    {
        return $this->belongsTo('\VCN\Models\User', 'asign_to');
    }

    public static function addLog( Viajero $viajero, $txt, $tipo="log", $data=null )
    {
        //viajero: ViajeroLog
        //solicitud: ViajeroLog

        $log = new ViajeroLog;
        $log->viajero_id = $viajero->id;
        $log->tipo = $tipo;
        $log->user_id = Auth::user()?Auth::user()->id:0;

        $log->notas = $txt;
        $log->data = $data;

        $log->save();
    }

    public static function addLogData( Viajero $viajero, $txt, $data )
    {
        self::addLog($viajero, $txt, "log", $data);
    }

    public static function addLogDatos( Viajero $viajero, $txt )
    {
        //viajero: ViajeroLog
        //solicitud: ViajeroLog

        $log = new ViajeroLog;
        $log->viajero_id = $viajero->id;
        $log->tipo = 'Datos';
        $log->user_id = Auth::user()?Auth::user()->id:0;

        $log->notas = $txt;

        $log->save();
    }

    public static function addLogCreado( Viajero $viajero )
    {
        //viajero: ViajeroLog
        //solicitud: ViajeroLog

        $log = new ViajeroLog;
        $log->viajero_id = $viajero->id;
        $log->tipo = "Creación";
        $log->user_id = Auth::user()->id;

        $log->notas = "Creación Lead";

        $log->save();
    }

    public static function addLogStatus( Viajero $viajero, $status )
    {
        $status1 = Status::find($viajero->status_id);
        $status1 = $status1?$status1->name:0;
        $status = $status>0?Status::find($status)->name:0;

        $txt = "Cambio de Estado [$status1 -> $status]";
        self::addLog($viajero, $txt);
    }

    public static function addLogStatusAuto( Viajero $viajero, $status )
    {
        if($status == $viajero->status_id)
        {
            return;
        }

        $status1 = Status::find($viajero->status_id);
        $status1 = $status1?$status1->name:0;
        $status = $status>0?Status::find($status)->name:0;

        $txt = "Cambio de Estado AUTO [$status1 -> $status]";
        self::addLog($viajero, $txt);
    }

    public static function addLogAsignado( Viajero $viajero, $asign )
    {
        $asign1 = User::find($viajero->asign_to);
        $asign1 = $asign1?$asign1->full_name:"-";
        $asign = $asign>0?User::find($asign)->full_name:"-";

        $txt = "Cambio de Asignado [$asign1 -> $asign]";
        self::addLog($viajero, $txt);
    }

    public static function addLogPrescriptor( Viajero $viajero, $p )
    {
        $p1 = Prescriptor::find($viajero->prescriptor_id);
        $p1 = $p1?$p1->name:"-";
        $p = $p>0?Prescriptor::find($p)->name:"-";

        $txt = "Cambio de Prescriptor [$p1 -> $p]";
        self::addLog($viajero, $txt);
    }

    public static function addLogOficina( Viajero $viajero, $oficina )
    {
        // $oficina = Oficina::find($o);

        $txt = "Cambio Oficina ". ($viajero->oficina?$viajero->oficina->name:"-") ." => ". ($oficina?$oficina->name:"-");
        self::addLog($viajero, $txt, 'oficina');
    }

}
