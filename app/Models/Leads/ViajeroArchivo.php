<?php

namespace VCN\Models\Leads;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\Booking;

use Carbon;
use File;

class ViajeroArchivo extends Model
{
    protected $table = 'viajero_archivos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $fillable = ['viajero_id', 'booking_id', 'user_id', 'visible', 'doc', 'fecha'];
    protected $dates = ['fecha'];
    protected $casts = [
        'visible' => 'boolean',
        'notapago_enviado' => 'boolean',
        'firmas' => 'json'
    ];

    public function delete()
    {
        $dir = storage_path("files/bookings/" . $this->viajero_id . "/");
        $f = $dir . $this->doc;
        if (file_exists($f)) {
            File::delete($f);
        }

        $dir = storage_path("files/viajeros/" . $this->viajero_id . "/");
        $f = $dir . $this->doc;
        if (file_exists($f)) {
            File::delete($f);
        }

        parent::delete();
    }

    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking', 'booking_id');
    }

    public function viajero()
    {
        return $this->belongsTo('\VCN\Models\Leads\Viajero', 'viajero_id');
    }

    public function getEsFirmadoAttribute()
    {
        $b2ret = $this->firmas != null && $this->firmas != "pendiente";

        if (!$b2ret && $this->firmas != "pendiente") {
            $bug = $this->getAttributes()['firmas'];

            if ($bug != null) {
                $f = $bug . '"}]';
                $this->firmas = json_decode($f);
                $this->save();

                return true;
            }
        }

        return $b2ret;
    }

    public function getEsFirmaSolicitadaAttribute()
    {
        return $this->firmas == "pendiente";
    }

    public function getEsBookingAttribute()
    {
        return $this->notas == "Booking-PDF";
    }

    public static function addBooking(Booking $booking, $doc, $ocultar = false, $firma = null)
    {
        $viajero = $booking->viajero;
        $booking_id = $booking->id;

        $va = new self;
        $va->viajero_id = $viajero->id;
        $va->booking_id = $booking_id;
        $va->user_id = 0;
        $va->doc = $doc;
        $va->notas = "Booking-PDF";
        $va->fecha = Carbon::now();
        $va->firmas = $firma;
        $va->save();

        $bVisible = $firma ? 1 : 0;

        if($ocultar)
        {
            foreach ($booking->pdfs as $p)
            {
                $p->update(['visible' => 0]);
            }
        }

        if ($bVisible)
        {
            foreach ($booking->pdfs as $p)
            {
                $p->update(['visible' => 0]);
            }

            //si esta es de firma se queda visible este
            $va->update(['visible' => 1]);
        } else {

            $p = $booking->pdfs_firmas_last;
            if ($p) {
                $p->update(['visible' => 1]);
            } else {
                $p = $booking->pdf_booking_last;
                if ($p) {
                    $p->update(['visible' => 1]);
                }
            }

            if (!$p) {
                $va->update(['visible' => 1]);
            }
        }

        return $va;
    }

    public function getEnlaceAttribute()
    {
        $link = "/files/bookings/" . $this->viajero->id . "/" . $this->doc;
        $ret = "<a target='_blank' href='$link'>" . $this->doc . "</a>";

        return $ret;
    }

    // public function getDocAttribute($value)
    // {
    //     if($value[0]=="/")
    //     {
    //         $b = strlen($this->booking_id);
    //         return substr($value, $b+2);
    //     }

    //     return $value;
    // }
}
