<?php

namespace VCN\Models\Leads;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\User;
use VCN\Models\Leads\Viajero;
use VCN\Models\Leads\ViajeroTutor;

use ConfigHelper;
use VCN\Helpers\MailHelper;
use Carbon;
use Session;

class Tutor extends \VCN\Models\ModelAuditable
{
    protected $table = 'tutores';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $dates = ['optout_fecha'];


    public function scopePlataforma($query)
    {
        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return $query->where('plataforma',$filtro)->orWhere('plataforma',0);
        }

        return $query;
    }

    public static function plataformaXXX()
    {
        // if( auth()->user()->isFullAdmin() )
        //     return self::all();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            // $users = User::asignados()->get()->pluck('id');
            // $viajeros = Viajero::whereIn('user_id', $users)->pluck('id');
            // $tutores = ViajeroTutor::whereIn('viajero_id', $viajeros)->pluck('tutor_id');
            // return self::whereIn('id', $tutores)->get();
            return self::where('plataforma',$filtro)->orWhere('plataforma',0)->get();
        }

        return self::all();
    }


    public static function checkEmail($email)
    {
        if($email == "")
        {
            return false;
        }

        $email = self::where('email', $email)->first();

        if(!$email)
        {
            $plataforma = ConfigHelper::config('propietario');
            $email2 = User::where('username',$email)->where('plataforma',$plataforma)->first();
            if(!$email2)
            {
                return false;
            }
            else
            {
                return $email2->ficha;
            }
        }

        return $email;
    }

    public function viajeros()
    {
        return $this->belongsToMany('\VCN\Models\Leads\Viajero', 'viajero_tutores')->withPivot('id','viajero_id', 'tutor_id','relacion','relacion_otro');
    }

    public function bookings()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'tutor_id');
    }

    public function getBookingsViajerosAttribute()
    {
        $ret = collect();

        foreach($this->viajeros as $v)
        {
            foreach($v->bookings_terminados as $b)
            {
                $ret->push($b);
            }
        }

        return $ret;
    }

    public function getTipoAttribute()
    {
        return "Tutor";
    }

    public function logs()
    {
        return $this->hasMany('\VCN\Models\Leads\TutorLog', 'tutor_id');
    }

    public function yaAviso($aviso_id)
    {
        return !is_null( $this->logs->where('notas',"Aviso[$aviso_id]")->first() );
    }

    public function usuario()
    {
        return $this->belongsTo('\VCN\Models\User','user_id');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User','user_id');
    }

    public function getFullNameAttribute()
    {
        return "$this->name $this->lastname";
    }

    public function plataformaConfig()
    {
        return $this->belongsTo('\VCN\Models\System\Plataforma','plataforma');
    }

    public function getPlataformaNameAttribute()
    {
        if( $this->plataformaConfig )
        {
            return $this->plataformaConfig->name;
        }

        return ConfigHelper::config_plataforma()->name;
    }

    public function getOficinasAttribute()
    {
        $ret = collect();

        foreach($this->viajeros as $v)
        {
            $ret->push($v->oficina);
        }

        $ret->unique();

        return $ret;
    }


    public static function buscar($query,$campo)
    {
        $filtros = [];

        if($campo=='nombre')
        {
            $qterms = explode(",", $query);

            $qnom = isset($qterms[0])?str_replace(" ","%",trim($qterms[0])):null;
            $qape1 = isset($qterms[1])?str_replace(" ","%",trim($qterms[1])):null;
            $qape2 = isset($qterms[2])?str_replace(" ","%",trim($qterms[2])):null;

            $qape = trim("$qape1 $qape2");

            // if($qnom)
            // {
            //     $filtros[] = ['name', 'LIKE' ,"%$qnom%"];
            // }

            // if($qape)
            // {
            //     $filtros[] = ['lastname', 'LIKE' ,"%$qape%"];
            // }
        }

        // if($campo=='email')
        // {
        //     $q = Self::where('email','LIKE',"%$query%");
        // }
        if($campo=='email')
        {
            $q = self::where( function($q) use ($query) {
               return $q
                ->where('email', 'LIKE', "%$query%")
                ->orWhere('phone', 'LIKE', "%$query%")
                ->orWhere('movil', 'LIKE', "%$query%");
            });
        }
        elseif($campo=='nombre')
        {
            $q = Self::select();

            if(isset($qnom))
            {
                $q = $q->where('name','LIKE',"%$qnom%");
            }

            if(isset($qape))
            {
                $q = $q->where('lastname','LIKE',"%$qape%");
            }
        }

        $plat = ConfigHelper::config('propietario');
        if($plat)
        {
            $q = $q->where('plataforma',$plat);
        }

        $col = $q->get();
        return $col;
    }

    public function getIdiomaContactoAttribute()
    {
        $idioma = "es";

        if($this->viajeros->count()>0)
        {
            $viajero = $this->viajeros->first();
            $idioma = $viajero->idioma_contacto;
            if(!$idioma)
            {
                $idioma = $viajero->idioma_contacto?:"es";
            }
        }

        return strtolower($idioma);
    }

    public function setUsuario($sendMail=true, $compra=false)
    {
        $email = strtolower(trim($this->email));

        if(!$this->usuario && $email) //solo se crea si tienen email
        {
            // $old = User::where('username',$this->email)->first();
            $old = User::where('username',$email)->where('plataforma',$this->plataforma)->first();

            if($old)
            {
                //esta repetido
                // $c = User::where('email',$u->email)->count()+1;
                // $nomuser = $nomuser . $c;
                // $u->username = $nomuser;
                Session::flash('mensaje-alert', 'E-mail tutor repetido.');
                return false;
            }

            if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                Session::flash('mensaje-alert', 'E-mail tutor erróneo.');
                return false;
            }

            $ut = new User;
            $ut->fname = $this->name;
            $ut->lname = $this->lastname;
            $ut->email = $email;

            // $nomuser = str_slug($this->name . $this->lastname);
            // $nomuser = str_replace('-','',str_slug($nomuser));
            $nomuser = $email;
            $passw = str_random(8);

            $ut->email = $nomuser;
            $ut->username = $nomuser;

            $ut->password = bcrypt($passw);
            // $ut->password = bcrypt($nomuser);
            $ut->roleid = 12;
            $ut->status = 2;
            $ut->plataforma = $this->plataforma;
            $ut->save();

            if($ut->id<100)
            {
                $ut->id = $ut->id+100;
                $ut->save();
            }

            $this->user_id = $ut->id;
            $this->save();

            if($compra)
            {
                MailHelper::mailAreaCompra($ut->id,$passw);
            }
            else
            {
                if($sendMail)
                {
                    MailHelper::mailArea($ut->id,$passw);
                }
            }

            return true;

        }

        return false;
    }

    public function getBookingsAreaPendientesAttribute()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'tutor_id')->where('es_online','>',0)->where('status_id',0)->where('viajero_id',0)->orderBy('course_start_date', 'DESC')->get();
    }

    public function getBookingsAreaAttribute()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'tutor_id')->where('es_directo',0)->where('status_id','>',0)->orderBy('course_start_date', 'DESC')->get();
    }

    public function getEsViajeroAttribute()
    {
        return false;
    }

    public function getLastBookingAttribute()
    {
        $b = $this->hasMany('\VCN\Models\Bookings\Booking', 'tutor_id')->where('es_directo',0)->where('status_id','>',0)->orderBy('course_start_date','DESC')->get();
        return $b->first();
    }

    public function getLastBookingAnyAttribute()
    {
        if(!$this->bookings_area->count())
        {
            return false;
        }

        return Carbon::parse($this->bookings_area->sortByDesc('course_start_date')->first()->course_start_date)->year;
    }

    public function getLopdBookingAnyAttribute()
    {
        foreach($this->bookings_viajeros as $b)
        {
            if($b->any == 2018)
            {
                return 2018;
            }
        }

        return 0;
    }
}
