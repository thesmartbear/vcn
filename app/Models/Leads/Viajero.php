<?php

namespace VCN\Models\Leads;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\User;
use VCN\Models\Solicitudes\Solicitud;

use VCN\Models\Solicitudes\SolicitudChecklist;
use VCN\Models\Bookings\BookingChecklist;
use VCN\Models\Bookings\BookingFactura;

use VCN\Models\Bookings\Booking;
use VCN\Models\Bookings\BookingLog;
use VCN\Models\Leads\ViajeroLog;
use VCN\Models\Leads\Tutor;
use VCN\Models\Leads\TutorLog;

use VCN\Models\Provincia;

use VCN\Helpers\ConfigHelper;
use VCN\Helpers\MailHelper;
use Carbon;
use Session;


class Viajero extends \VCN\Models\ModelAuditable
{
    protected $table = 'viajeros';

    protected $fillable = ["name","lastname","lastname2","email","phone","movil","fechanac","notas","sexo", 'rating',
        "origen_id","suborigen_id","suborigendet_id","es_adulto","paisnac","emergencia_contacto","emergencia_telefono"];
    // protected $guarded = ['_token'];

    protected $dates = ['optout_fecha'];


    public function delete()
    {
        //Pendiente: borrar archivos

        /*
        //if(booking) => Error
        if($this->bookings->count())
        {
            Session::flash('mensaje-alert','Viajero con Bookings. No se puede eliminar.');
            return false;
        }
        //if(tutores) => Error
        if($this->tutores->count())
        {
            Session::flash('mensaje-alert','Viajero con Tutores. No se puede eliminar.');
            return false;
        }
        */

        parent::delete();
    }

    public function update(array $attributes = [], array $options = [])
    {
        if($this->id == 0)
        {
            \Log::info("Bug Viajero0");
            return null;    
        }

        parent::update($attributes, $options);
    
    }

    // public function setFechanacAttribute($value)
    // {
    //     $this->attributes['fechanac'] = Carbon::parse($value);
    // }

    public function scopePlataforma($query)
    {
        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            // $users = User::asignados()->get()->pluck('id');
            // return $query->whereIn('asign_to', $users);
            return $query->where('plataforma',$filtro);//->orWhere('plataforma',0);
        }

        return $query;
    }

    public static function checkEmail($email)
    {
        if($email == "")
        {
            return false;
        }

        $email = self::where('email', $email)->first();

        if(!$email)
        {
            $plataforma = ConfigHelper::config('propietario');
            $email2 = User::where('username',$email)->where('plataforma',$plataforma)->first();
            if(!$email2)
            {
                return false;
            }
            else
            {
                return $email2->ficha;
            }
        }

        return $email;
    }

    public function tutores()
    {
        return $this->belongsToMany('\VCN\Models\Leads\Tutor', 'viajero_tutores')->withPivot('id','viajero_id', 'tutor_id','relacion','relacion_otro');
    }

    public function prescriptor()
    {
        return $this->belongsTo('\VCN\Models\Prescriptores\Prescriptor','prescriptor_id');
    }

    public function getTipoAttribute()
    {
        return "Viajero";
    }


    public function plataformaConfig()
    {
        return $this->belongsTo('\VCN\Models\System\Plataforma','plataforma');
    }

    public function getPlataformaNameAttribute()
    {
        if( $this->plataformaConfig )
        {
            return $this->plataformaConfig->name;
        }

        return ConfigHelper::config_plataforma()->name;
    }

    public function origen()
    {
        return $this->belongsTo('\VCN\Models\Leads\Origen', 'origen_id');
    }

    public function suborigen()
    {
        return $this->belongsTo('\VCN\Models\Leads\Suborigen', 'suborigen_id');
    }

    public function suborigen_det()
    {
        return $this->belongsTo('\VCN\Models\Leads\SuborigenDetalle', 'suborigendet_id');
    }

    public function getSexoNameAttribute()
    {
        return $this->sexo?($this->sexo==1?'Hombre':'Mujer'):'-';
    }
    public function getSexoNameEnAttribute()
    {
        return $this->sexo?($this->sexo==1?'Male':'Female'):'-';
    }

    public function getOficinaAsignadaAttribute()
    {
        if($this->oficina)
        {
            $oficina = $this->oficina;
            return $oficina ? $oficina->id : 0;
        }

        $oficina = null;
        $plataforma = \VCN\Models\System\Plataforma::find($this->plataforma);
        if($plataforma && $plataforma->oficina)
        {
            $oficina = $plataforma->oficina;
        }

        $oficina = $oficina ?: \VCN\Models\System\Oficina::where('propietario', $this->plataforma)->where('activa',true)->first();

        if(!$this->provincia)
        {
            return $oficina ? $oficina->id : 0;
        }

        if($this->provincia->oficina)
        {
            $oficina = $this->provincia->oficina;
        }

        return $oficina ? $oficina->id : 0;
    }

    public function getFechanacDmyAttribute()
    {
        return $this->fechanac?Carbon::parse($this->fechanac)->format('d/m/Y'):'-';
    }

    public function getTutor1Attribute()
    {
        if(!$this->tutores->count())
            return null;

        return $this->tutores->sortBy('id')->get(0);
    }

    public function getTutor2Attribute()
    {
        if(!$this->tutores->count())
            return null;

        return $this->tutores->sortBy('id')->get(1);
    }

    public function getTutor1NameAttribute()
    {
        $t = $this->tutor1;
        return $t?$t->full_name:'-';
    }

    public function getTutor2NameAttribute()
    {
        $t = $this->tutor2;
        return $t?$t->full_name:'-';
    }

    public function tareas()
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroTarea', 'viajero_id');
    }

    public function getTareasLastAttribute()
    {
        return $this->tareas->sortByDesc('fecha')->where('estado',0)->first();
    }

    public function logs()
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroLog', 'viajero_id');
    }

    public function yaAviso($aviso_id)
    {
        return !is_null( $this->logs->where('notas',"Aviso[$aviso_id]")->first() );
    }

    public function seguimientos()
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroLog', 'viajero_id')->where('tipo','<>','')->where('tipo','<>','log');
    }

    public function seguimientos_enviados()
    {
        $tipos = ConfigHelper::getTipoSeguimientoEnviado();
        return $this->hasMany('\VCN\Models\Leads\ViajeroLog', 'viajero_id')->whereIn('tipo',$tipos);
    }

    public function seguimientos_recibidos()
    {
        $tipos = ConfigHelper::getTipoSeguimientoRecibido();
        return $this->hasMany('\VCN\Models\Leads\ViajeroLog', 'viajero_id')->whereIn('tipo',$tipos);
    }

    public function bookings()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking');
    }

    public function bookings_terminados()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking')->where('status_id','>',0);
    }

    public function getBookingsLopdAttribute()
    {
        $st = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id')->toArray();
        $sto = [ConfigHelper::config('booking_status_overbooking')];
        $st = array_merge($st,$sto);

        return $this->hasMany('\VCN\Models\Bookings\Booking')->whereIn('status_id',$st)->get();
    }

    public function getBookingsPlazaAttribute()
    {
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id')->toArray();
        return $this->hasMany('\VCN\Models\Bookings\Booking')->whereIn('status_id',$stplazas)->get();
    }

    public function getBookingsPlazaLastAttribute()
    {
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id')->toArray();
        $b = $this->hasMany('\VCN\Models\Bookings\Booking')->whereIn('status_id',$stplazas)->orderBy('id','DESC')->get();
        return $b->first();
    }

    public function getLastBookingAttribute()
    {
        $b = $this->hasMany('\VCN\Models\Bookings\Booking')->where('status_id','>',0)->orderBy('course_start_date','DESC')->get();
        return $b->first();
    }

    public function getLopdBookingAnyAttribute()
    {
        $b = $this->last_booking;

        return $b ? $b->any : 0;
    }

    public function getFacturasAttribute()
    {
        $bookings = $this->bookings->pluck('id')->toArray();

        return BookingFactura::whereIn('booking_id',$bookings)->get();
    }

    public function getBookingsAreaAttribute()
    {
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id')->toArray();
        array_push($stplazas, ConfigHelper::config('booking_status_overbooking'));
        array_push($stplazas, ConfigHelper::config('booking_status_prereserva'));
        
        //covid19
        array_push($stplazas, ConfigHelper::config('booking_status_cancelado'));
        array_push($stplazas, ConfigHelper::config('booking_status_refund'));

        return $this->hasMany('\VCN\Models\Bookings\Booking')->where('es_directo',0)->where('area',1)->whereIn('status_id',$stplazas)->orderBy('course_start_date', 'DESC')->get();
    }

    public function getBookingsAreaPendientesAttribute()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking')->where('es_online','>',0)->where('status_id',0)->orderBy('course_start_date', 'DESC')->get();
    }

    public function booking()
    {
        return $this->belongsTo('\VCN\Models\Bookings\Booking', 'booking_id');
    }

    public function solicitudes()
    {
        return $this->hasMany('\VCN\Models\Solicitudes\Solicitud');
    }

    public function solicitud()
    {
        return $this->belongsTo('\VCN\Models\Solicitudes\Solicitud', 'solicitud_id');
    }

    public function getSolicitudActivaAttribute()
    {
        if(!$this->solicitud)
        {
            return null;
        }

        $sol = $this->solicitud;
        if( $sol->status_id == ConfigHelper::config('solicitud_status_inscrito') )
        {
            $this->solicitud_id = 0;
            // $this->save();

            return null;
        }
        elseif( $sol->status_id == ConfigHelper::config('solicitud_status_archivado') )
        {
            $this->solicitud_id = 0;
            // $this->save();

            return null;
        }

        return $sol;
    }

    public function usuario()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public function user()
    {
        return $this->belongsTo('\VCN\Models\User', 'user_id');
    }

    public function archivos()
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroArchivo', 'viajero_id');
    }

    public function archivos_importantes()
    {
        return $this->hasMany('\VCN\Models\Leads\ViajeroArchivo', 'viajero_id')->where('booking_id',0)->where('visible',1);
    }

    public function oficina()
    {
        return $this->belongsTo('\VCN\Models\System\Oficina', 'oficina_id');
    }

    public function status()
    {
        // if($this->es_cliente)
        //     return $this->belongsTo('\VCN\Models\Bookings\Status', 'booking_status_id');

        if( $this->es_booking && !$this->booking->es_presupuesto )
        {
            return $this->belongsTo('\VCN\Models\Bookings\Status', 'booking_status_id');
        }

        return $this->belongsTo('\VCN\Models\Solicitudes\Status', 'status_id');
    }

    public function getStatusChecklist($checkid)
    {
        //categoria y subcategoria

        if($this->es_booking)
        {
            // $c = BookingChecklist::where('booking_id',$this->booking_id)->where('checklist_id',$checkid)->first();
            $c = BookingChecklist::where(['booking_id'=> $this->booking_id, 'checklist_id'=> $checkid]);
        }
        else
        {
            $c = SolicitudChecklist::where('solicitud_id',$this->solicitud_id)->where('checklist_id',$checkid);
        }

        return $c->first();
    }

    public function getStatusChecklistClass($checkid)
    {
        $c = $this->getStatusChecklist($checkid);
        return $c?($c->estado?"fa fa fa-check-circle-o":"fa fa-circle-thin"):"fa fa-circle-thin";
    }

    public function getStatusChecklistFecha($checkid)
    {
        $c = $this->getStatusChecklist($checkid);

        if(!$c)
        {
            return "";
        }

        if(!$c->estado)
        {
            return "";
        }

        return "[". Carbon::parse($c->fecha)->format('d/m/Y') ."] (". ($c->user?$c->user->full_name:".") .")";
    }

    public function getStatusChecklistEstado($checkid)
    {
        $c = $this->getStatusChecklist($checkid);
        return $c?$c->estado:false;
    }

    public function asignado()
    {
        return $this->belongsTo('\VCN\Models\User', 'asign_to');
    }

    public function datos()
    {
        return $this->hasOne('\VCN\Models\Leads\ViajeroDatos');
    }

    public function getFullNameAttribute()
    {
        return "$this->name $this->lastname $this->lastname2";
    }

    public function getFullApellidosAttribute()
    {
        return "$this->lastname $this->lastname2, $this->name";
    }

    public function getViajeroNameAttribute()
    {
        return $this->name;
    }

    public function getViajeroFullNameAttribute()
    {
        return $this->full_name;
    }

    public function getAvatarAttribute()
    {
        if($this->foto)
        {
            return $this->foto;
        }

        if($this->user)
        {
            if($this->user->avatar)
            {
                return $this->user->avatar;
            }
        }

        $avatar = \Avatar::create($this->full_name)->toBase64();
        $this->foto = $avatar;
        $this->save();

        if($this->user)
        {
            $this->user->avatar = $avatar;
            $this->user->save();
        }

        return $avatar;
    }

    public function getFueClienteAttribute()
    {
        return ($this->bookings->count()>0);
    }

    public function getExClienteAttribute()
    {
        if($this->user_id && !$this->es_cliente)
            return true;

        return false;
    }

    public function getEsMenorAttribute()
    {
        $edad = Carbon::parse($this->fechanac)->diff(Carbon::now())->format('%y');
        return intval($edad)<18;
    }

    public function getEdadAttribute()
    {
        $edad = Carbon::parse($this->fechanac)->diff(Carbon::now())->format('%y');

        return intval($edad);
    }

    public function getEsCumple()
    {
        return $this->fechanac->format('d/m') == Carbon::now()->format('d/m');
    }

    public function getEsCicAttribute()
    {
        // $plataformas = config('vcn.plataformas');

        return $this->plataforma == 2;
    }

    public function getCategoriaNameAttribute()
    {
        if($this->booking)
        {
            return $this->booking?$this->booking->curso->categoria_name . "(Booking)":"-";
        }

        if($this->solicitud)
        {
            return $this->solicitud->categoria?$this->solicitud->categoria->name:"-";
        }

        return "-";
    }


    public function setStatus($status, $motivo=null, $nota=null)
    {
        if($this->es_cliente)
        {
            $this->booking_status_id = $status;
            $this->save();

            // $this->booking->setStatus($status);

            // if( $this->booking )
            // {
            //     $this->booking->setStatus( $status );

            //     if( $status == ConfigHelper::config('booking_status_archivado') )
            //     {
            //         $this->booking->archivar($motivo,$nota);
            //     }
            // }

            if($this->booking)
            {
                BookingLog::addLogStatus($this->booking, $status);
            }
        }
        else
        {
            $this->status_id = $status;
            if( $this->solicitud )
            {
                $this->solicitud->setStatus( $status );
                
                if( $status == 0 )
                {
                    $this->solicitud->archivar($motivo,$nota);
                }
            }

            ViajeroLog::addLogStatus($this, $status);
        }

        $this->save();
    }



    //====== Traspaso =====

    private static function createFromLeadViajero( Lead $lead )
    {
        $stages = ['Leads'=>1,'Archivados'=>2,'Potenciales'=>3,'Prioritarios'=>4,'Futuros'=>5];

        $v = new Viajero;
        $v->name = $lead->lead_name;
        $v->lastname = $lead->lead_lastname;
        $v->email = $lead->lead_email;
        $v->phone = $lead->lead_phone;
        $v->movil = $lead->lead_movil;
        $v->fechanac = $lead->lead_traveler_birthday;
        $v->notas = $lead->lead_additional_information;
        $v->asign_to = $lead->lead_asign_to;

        $v->origen_id = $lead->lead_category;
        $v->suborigen_id = $lead->lead_subcategory;

        $v->user_id = 0;
        $v->status_id = $stages[$lead->lead_stage];

        $v->save();
        $v->created_at = $lead->created_at;
        $v->save();

        $s = new Solicitud;
        $s->viajero_id = $v->id;
        $s->user_id = $v->asign_to;
        // $s->category_id = $lead->lead_category;
        // $s->subcategory_id = $lead->lead_subcategory;
        $s->status_id = $stages[$lead->lead_stage];
        $s->fecha = $lead->created_at;
        $s->save();

        $v->solicitud_id = $s->id;
        $v->save();

        //Seguimiento (follow_up)
        $logs = LeadLog::where('lead_id',$lead->id)->get();
        foreach($logs as $log)
        {
            $vl = new ViajeroLog;
            $vl->viajero_id = $v->id;
            $vl->user_id = $log->lead_updated_by?$log->lead_updated_by:($log->lead_assigned_to?$log->lead_assigned_to:1);
            $vl->status_id = $stages[$log->lead_stage];
            $vl->asign_to = $log->lead_assigned_to;
            $vl->tipo = $log->lead_contact_type;
            $vl->notas = $log->lead_note;
            $vl->created_at = $log->created_at;
            $vl->save();
        }

        //Tareas (next_action)
        $tareas = LeadTarea::where('lead_id',$lead->id)->get();
        foreach($tareas as $t)
        {
            $vt = new ViajeroTarea;
            $vt->viajero_id = $v->id;
            $vt->fecha = $t->lead_next_action_date;
            $vt->user_id = $t->created_by;
            $vt->asign_to = $t->created_by;
            $vt->tipo = $t->lead_next_action_contact_type;
            $vt->notas = $t->lead_next_action_note;
            $vt->created_at = $t->created_at;
            $vt->estado = $t->lead_next_action_done=='Yes'?1:0;
            $vt->save();
        }

        return $v;
    }


    private static function createFromLeadTutor( Lead $lead )
    {
        $t = new Tutor;
        $t->name = $lead->lead_name;
        $t->lastname = $lead->lead_lastname;
        $t->email = $lead->lead_email;
        $t->phone = $lead->lead_phone;
        $t->movil = $lead->lead_movil;
        $t->user_id = 0;

        $t->save();
        $t->created_at = $lead->created_at;
        $t->save();

        $v = new Viajero;
        $v->name = $lead->lead_traveler_name;
        $v->lastname = $lead->lead_traveler_lastname;

        $a = Account::where(['name'=> $lead->lead_traveler_name, 'lname'=> $lead->lead_traveler_lastname])->first();
        if($a)
        {
            $v->email = $a->lead_email;
        }
        else
        {
            $v->email = $lead->lead_email;
        }

        $v->phone = $lead->lead_phone;
        $v->movil = $lead->lead_movil;
        $v->fechanac = $lead->lead_traveler_birthday;
        $v->notas = $lead->lead_additional_information;
        $v->asign_to = $lead->lead_asign_to;
        $v->user_id = 0;

        $stages = ['Leads'=>1,'Archivados'=>2,'Potenciales'=>3,'Prioritarios'=>4,'Futuros'=>5];
        $v->status_id = $stages[$lead->lead_stage];

        $v->save();
        $v->created_at = $lead->created_at;
        $v->save();

        $s = new Solicitud;
        $s->viajero_id = $v->id;
        $s->user_id = $v->asign_to;
        $s->category_id = $lead->lead_category;
        $s->subcategory_id = $lead->lead_subcategory;
        $s->status_id = $stages[$lead->lead_stage];
        $s->fecha = $lead->created_at;
        $s->save();

        $v->solicitud_id = $s->id;
        $v->save();

        //ViajeroTutor
        $vt = new ViajeroTutor;
        $vt->tutor_id = $t->id;
        $vt->viajero_id = $v->id;
        $vt->relacion = 'Tutor';
        $vt->save();

        //Seguimiento (follow_up)
        $logs = LeadLog::where('lead_id',$lead->id)->get();
        foreach($logs as $log)
        {
            $vl = new ViajeroLog;
            $vl->viajero_id = $v->id;
            $vl->user_id = $log->lead_updated_by?$log->lead_updated_by:($log->lead_assigned_to?$log->lead_assigned_to:1);
            $vl->status_id = $stages[$log->lead_stage];
            $vl->asign_to = $log->lead_assigned_to;
            $vl->tipo = $log->lead_contact_type;
            $vl->notas = $log->lead_note;
            $vl->created_at = $log->created_at;
            $vl->save();
        }

        //Tareas (next_action)
        $tareas = LeadTarea::where('lead_id',$lead->id)->get();
        foreach($tareas as $t)
        {
            $vt = new ViajeroTarea;
            $vt->viajero_id = $v->id;
            $vt->fecha = $t->lead_next_action_date;
            $vt->user_id = $t->created_by;
            $vt->asign_to = $t->created_by;
            $vt->tipo = $t->lead_next_action_contact_type;
            $vt->notas = $t->lead_next_action_note;
            $vt->created_at = $t->created_at;
            $vt->estado = $t->lead_next_action_done=='Yes'?1:0;
            $vt->save();
        }

        return $v;
    }


    public static function traspasoFromLead( Lead $lead )
    {
        if($lead->lead_type == 2)
        {
            $v = self::createFromLeadViajero($lead);
        }
        elseif($lead->lead_type == 1)
        {
            $v = self::createFromLeadTutor($lead);
        }
        else
        {
            $v = self::createFromLeadViajero($lead);
        }

        // if($lead->lead_is_client)
        // {
        //     // $v->setInscrito();
        // }
    }

    //=============

    public function setInscrito($estado=true)
    {
        $this->es_cliente = $estado;
        $this->save();

        if(!$estado)
        {
            return;
        }

        $this->setUsuarios();
    }

    public function setEmail()
    {
        foreach($this->tutores as $tutor)
        {
            if($tutor->email == $this->email)
            {
                $this->email = null;
                $this->save();

                if($this->user)
                {
                    $this->user->delete();
                }

                return;
            }
        }
    }

    public static function emailSync($email, $cleaned=false)
    {
        $i2ret = 0;

        $viajeros = self::where('email', $email)->get();
        foreach($viajeros as $viajero)
        {
            $viajero->email = null;
            $viajero->save();

            $i2ret++;

            if($cleaned && $viajero->user)
            {
                $viajero->user->delete();
            }

            $log = "Sync MAILCHIMP [$email]";
            ViajeroLog::addLog($viajero, $log);
        }

        $tutores = Tutor::where('email', $email)->get();
        foreach($tutores as $tutor)
        {
            $tutor->email = null;
            $tutor->save();

            $i2ret++;

            if($cleaned && $tutor->user)
            {
                $tutor->user->delete();
            }

            $log = "Sync MAILCHIMP [$email]";
            TutorLog::addLog($tutor, $log);
        }

        return $i2ret;
    }

    public function setUsuarios($sendMail=true, $compra=false)
    {
        if(!$compra)
        {
            if(!ConfigHelper::config('area'))
            {
                return false;
            }

            if(!$this->booking)
            {
                return false;
            }

            if($this->booking && $this->booking->es_presupuesto)
            {
                return false;
            }
        }

        //Regla de AREA NO:


        if(!$compra)
        {
            if($this->booking)
            {
                if(!$this->booking->area)
                {
                    //No lo creamos
                    return false;
                }
            }
        }

        return $this->setUsuariosForce($sendMail, $compra);
    }

    public function setUsuariosForce($sendMail=true, $compra=false)
    {
        $iRet = 0;

        //Usuario para el y su/s tutores

        //PENDIENTE: Si es un viajero q ya es tutor ???
        // $vt = User::where('username',$this->email)->where('plataforma',$this->plataforma)->first();
        // if($vt)
        // {
        //     if($vt->roleid==12)
        //     {
        //         //Viajero y tutor (pendiente)
        //     }
        // }

        //1 - Tutores
        foreach($this->tutores as $tutor)
        {
            $iRet += $tutor->setUsuario($sendMail,$compra) ? 1 : 0;
        }

        //Viajero
        $email = strtolower(trim($this->email));
        if(!$this->usuario && $email) //solo se crea si tienen email
        {
            // $old = User::where('username',$this->email)->first();
            $oldV = User::where('username',$email)->where('plataforma',$this->plataforma)->where('roleid',11)->first();
            $oldT = User::where('username',$email)->where('plataforma',$this->plataforma)->where('roleid',12)->first();

            if($oldV)
            {
                //esta repetido
                $this->email = null;
                $this->save();

                Session::flash('mensaje-alert', 'E-mail viajero repetido.');
                return false;
            }

            if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                $this->email = null;
                $this->save();

                Session::flash('mensaje-alert', 'Dirección de E-mail incorrecta.');
                return false;
            }

            $u = new User;
            $u->fname = $this->name;
            $u->lname = $this->lastname;

            // $nomuser = str_slug($this->name . $this->lastname);
            // $nomuser = str_replace('-','',str_slug($nomuser));
            $nomuser = $email;
            $passw = str_random(8);

            $u->email = $nomuser;
            $u->username = $nomuser;

            $u->password = bcrypt($passw);
            // $u->password = bcrypt($nomuser);
            $u->roleid = 11;
            $u->status = 2;
            $u->plataforma = $this->plataforma;
            $u->save();

            if($u->id<100)
            {
                $u->id = $u->id+100;
                $u->save();
            }

            $this->user_id = $u->id;
            $this->save();

            if($compra)
            {
                MailHelper::mailAreaCompra($u->id,$passw);
            }
            else
            {
                if($sendMail)
                {
                    MailHelper::mailArea($u->id,$passw);
                }
            }

            $iRet++;
        }

        return $iRet;
    }

    public function getOrigenFullNameAttribute()
    {
        $ret = $this->origen?$this->origen->name:"-";

        if($this->suborigen)
        {
            $ret .= " > ". $this->suborigen->name;
        }

        if($this->suborigen_det)
        {
            $ret .= " > ". $this->suborigen_det->name;
        }

        return $ret;
    }

    public function setBooking($booking_id=0)
    {
        $ret = false;

        if($booking_id)
        {
            $this->booking_id = $booking_id;
            $this->booking_status_id = 0;
            $this->es_cliente = true;
            $this->save();
        }

        // if(!$this->es_cliente && !$this->booking)
        if(!$this->booking)
        {
            $booking = $this->bookings->sortByDesc('fecha_reserva')->first();

            if($booking && !$booking->es_presupuesto)
            {
                if($booking->es_vigente)
                {
                    if($booking->status_id != ConfigHelper::config('booking_status_vuelta'))
                    {
                        $this->booking_id = $booking->id;
                        $this->booking_status_id = $booking->status_id;
                        $this->es_cliente = true;
                        $this->save();
                    }
                }
                else
                {
                    $this->booking_id = 0;
                    $this->booking_status_id = 0;
                    $this->es_cliente = false;
                    if(!$this->solicitud_id)
                    {
                        $this->setStatus( ConfigHelper::config('solicitud_status_lead') );
                    }
                    $this->save();
                }

                $ret = true;
            }
        }

        if($this->booking && $this->booking->es_prebooking && $this->solicitud)
        {
            //Puede haber una solicitud nueva y q el viajero sea booking pero no hay q pasarla a inscrito
            if($this->booking->solicitud_id == $this->solicitud_id)
            {
                $this->solicitud->setInscrito();
            }
        }

        if(!$this->es_cliente && $this->booking && !$this->booking->es_presupuesto)
        {
            $this->es_cliente = true;
            $this->save();

            return true;
        }

        return $ret;
    }

    public function getStatusNameAttribute()
    {
        if($this->es_booking)
        {
            if(!$this->booking->es_presupuesto)
            {
                return $this->booking->status?$this->booking->status->name : "Pendiente";
            }
        }

        if($this->status)
        {
            return $this->status->name;
        }

        if($this->booking && $this->booking->es_presupuesto)
        {
            return "Presupuesto";
        }

        if($this->solicitud)
        {
            return $this->solicitud->status->name;
        }

        // if($this->es_booking)
        
        //     return "Pendiente";
        

        return "Sin Estado";
    }

    public function getStatusAreaAttribute()
    {
        //Inscrito, Overbooking, En el extranjero, De vuelta a casa

        if($this->es_booking)
        {
            return $this->booking->status_area;
        }

        return "-";
    }

    public function getStatusIconoAttribute()
    {
        $class = "icon-st";
        $st = strtolower(snake_case($this->status_name));
        
        if(!$this->booking)
        {
            $st = $this->solicitud ? "lead" : "null";
        }

        $class .= " icon-status-". $st;

        return "<span style='float:right;' class='$class'></span>";
    }

    public function getStatusTipoAttribute()
    {
        return $this->es_booking ? "Booking" : "Lead";

    }

    public function getEsEmailTutorAttribute()
    {
        if(!$this->email)
        {
            return false;
        }

        if(!filter_var($this->email, FILTER_VALIDATE_EMAIL))
        {
            return false;
        }

        // $t = Tutor::where('email',$this->email)->first();
        foreach($this->tutores as $t)
        {
            if($t->plataforma == 0 || $t->plataforma == $this->plataforma)
            {
                if($t->email == $this->email)
                {
                    return $t;
                }
            }
        }

        return null;
    }

    public function getEsBookingAreaAttribute()
    {
        if(!$this->es_booking)
        {
            return false;
        }

        if($this->booking && $this->booking->es_presupuesto)
        {
            return false;
        }

        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id')->toArray();
        if( in_array($this->booking->status_id, $stplazas) )
        {
            if($this->booking->status_id == ConfigHelper::config('booking_status_vuelta'))
            {
                return false;
            }

            return true;
        }

        return false;
    }

    public function getEsBookingAttribute()
    {
        $b2Ret = false;

        // if(!$this->booking)
        // {
        //     // if($this->bookings->count()>0)
        //     // {
        //     //     $bookings = $this->bookings->sortByDesc('course_start_date');

        //     //     $booking = $bookings->first();
        //     //     $booking_id = $booking->id;

        //     //     if($booking->status_id != ConfigHelper::config('booking_status_vuelta'))
        //     //     {
        //     //         $this->booking_id = $booking_id;
        //     //         $this->booking_status_id = Booking::find($booking_id)->status_id;
        //     //         $this->save();
        //     //     }
        //     // }
        //     // else
        //     {
        //         $this->booking_id = 0;
        //         $this->booking_status_id = 0;
        //         $this->es_cliente = false;
        //         //$this->status_id = 1; //Lead
        //         $this->save();
        //     }
        // }

        $booking = $this->booking;

        if($booking && ($booking->es_cancelado || $booking->es_refund))
        {
            $this->booking_id = 0;
            $this->booking_status_id = 0;
            $this->es_cliente = false;
            $this->save();

            return false;
        }

        if($booking && $this->booking->es_presupuesto)
        {
            $this->booking_id = 0;
            $this->booking_status_id = 0;
            $this->es_cliente = false;
            $this->save();

            return false;
        }

        if($this->es_cliente)
        {
            $b2Ret = true;

            if(!$this->booking)
            {
                $this->booking_id = 0;
                $this->booking_status_id = 0;
                $this->es_cliente = false;
                $this->save();

                $b2Ret = false;
            }
        }

        if($this->solicitud && $this->booking)
        {
            if( $this->booking->es_vigente  )
            {
                $this->booking_status_id = $this->booking->status_id;
                $this->es_cliente = true;
                $this->save();

                $b2Ret = true;
            }
        }

        if($b2Ret && !$this->usuario)
        {
            // $this->setInscrito();
        }

        return $b2Ret;

    }

    public function getIdiomaContactoAttribute()
    {
        $idioma = "es";

        if($this->datos)
        {
            $idioma = $this->datos->idioma_contacto;
            if(!$idioma)
            {
                $idioma = "es";
                if($this->datos->provincia)
                {
                    $provincia = Provincia::where('name',$this->datos->provincia)->first();
                    if($provincia)
                    {
                        $idioma = $provincia->idioma_contacto?:"es";
                    }
                }
            }
        }

        return strtolower($idioma);
    }

    public function getAlertaBookingAttribute()
    {
        $ret = null;

        if($this->es_booking)
        {
            if($this->booking)
            {
                $st = [];
                $st[] = ConfigHelper::config('booking_status_vuelta');
                $st[] = ConfigHelper::config('booking_status_cancelado');
                $st[] = ConfigHelper::config('booking_status_refund');

                if( Booking::where('viajero_id',$this->id)->whereNotIn('status_id',$st)->count() > 0 )
                {
                    return Booking::where('viajero_id',$this->id)->whereNotIn('status_id',$st)->orderBy('id','DESC')->first();
                }
            }
        }

        return $ret;
    }

    public function getAlertaBookingTotalAttribute()
    {
        $ret = 0;

        if($this->es_booking)
        {
            if($this->booking)
            {
                $st = [];
                $st[] = ConfigHelper::config('booking_status_vuelta');
                $st[] = ConfigHelper::config('booking_status_cancelado');
                $st[] = ConfigHelper::config('booking_status_refund');

                return Booking::where('viajero_id',$this->id)->whereNotIn('status_id',$st)->count();
            }
        }

        return $ret;
    }

    public function getLastBookingAnyAttribute()
    {
        if(!$this->bookings_area->count())
        {
            return false;
        }

        return Carbon::parse($this->bookings->sortByDesc('course_start_date')->first()->course_start_date)->year;
    }

    public function getEsViajeroAttribute()
    {
        return true;
    }

    public function exam_respuestas()
    {
        return $this->hasMany(\VCN\Models\Exams\Respuesta::class, 'viajero_id');
    }

    public function getDestinatariosAttribute()
    {
        $ret = [];

        $viajero = $this;

        $user = $viajero->user;

        if($user)
        {
            $nom = "Viajero: ". $user->ficha->full_name;
            $ret[$user->id] = $nom;
        }
        elseif($viajero->email)
        {
            $nom = "Viajero: ". $viajero->full_name;
            $ret["v"] = $nom;
        }

        if($viajero->tutor1)
        {
            $user = $viajero->tutor1->user;

            if($user)
            {
                $nom = "Tutor1: ". $user->ficha->full_name;
                $ret[$user->id] = $nom;
            }
            elseif($viajero->tutor1->email)
            {
                $nom = "Tutor1: ". $viajero->tutor1->full_name;
                $ret["t1"] = $nom;
            }
        }

        if($viajero->tutor2)
        {
            $user = $viajero->tutor2->user;

            if($user)
            {
                $nom = "Tutor2: ". $user->ficha->full_name;
                $ret[$user->id] = $nom;
            }
            elseif($viajero->tutor2->email)
            {
                $nom = "Tutor2: ". $viajero->tutor2->full_name;
                $ret["t2"] = $nom;
            }
        }

        return $ret;
    }

    public function getPlataformaWebAttribute()
    {
        $p = $this->plataforma ?: 1;
        return ConfigHelper::config('area_url',$p) ?: ConfigHelper::config('web',$p);
    }
}
