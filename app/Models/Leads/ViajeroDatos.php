<?php

namespace VCN\Models\Leads;

use Illuminate\Database\Eloquent\Model;

use ConfigHelper;

class ViajeroDatos extends Model
{
    protected $table = 'viajero_datos';

    protected $fillable = ['viajero_id','nacionalidad','tipodoc','documento','pasaporte','pasaporte_pais','pasaporte_emision',
        'pasaporte_caduca','tipovia','direccion','ciudad','provincia','pais','cp','idioma','idioma_nivel','titulacion',

        'alergias','enfermedad','tratamiento','dieta','medicacion',
        'alergias2','enfermedad2','tratamiento2','dieta2','medicacion2',

        'hobby','fumador','profesion','empresa','empresa_phone','curso_anterior','animales',
        'h1_nom','h1_fnac','h2_nom','h2_fnac','h3_nom','h3_fnac','h4_nom','h4_fnac', 'idioma_contacto',
        'escuela','escuela_curso','ingles','ingles_academia',
        'fact_nif','fact_razonsocial','fact_domicilio','fact_cp','fact_poblacion','fact_concepto',
        'trinity_exam','trinity_any','trinity_nivel','cic_thau','cic','cic_nivel',
        'telefonos','rs_instagram',
        ];

    // protected $guarded = ['_token'];

    // protected $dates = ['h1_fnac','h2_fnac','h3_fnac','h4_fnac'];

    public function update(array $attributes = [], array $options = [])
    {
        if($this->viajero_id == 0)
        {
            \Log::info("Bug Viajero0: VD");
            return null;    
        }
        
        parent::update($attributes, $options);
    }

    public function viajero()
    {
        return $this->belongsTo('\VCN\Models\Leads\Viajero','viajero_id');
    }

    public function provincia_model()
    {
        return $this->belongsTo('\VCN\Models\Provincia', 'provincia_id');
    }

    public function pais_model()
    {
        return $this->belongsTo('\VCN\Models\Pais', 'pais_id');
    }

    public function getPaisNameAttribute()
    {
        return $this->pais_model?$this->pais_model->name:"-";
    }

    public function getTipoViaNameAttribute()
    {
        return ConfigHelper::getTipoVia($this->tipovia?:0);
    }

    public function getProvinciaNameAttribute()
    {
        return $this->provincia;
    }

    // private function getFecha($timestamp)
    // {
    //     return ( ! starts_with($timestamp, '0000')) ? $this->asDateTime($timestamp) : null;
    // }

    // public function getH1FnacAttribute($timestamp)
    // {
    //     return $this->getFecha($timestamp);
    // }

    // public function getH2FnacAttribute($timestamp)
    // {
    //     return $this->getFecha($timestamp);
    // }

    // public function getH3FnacAttribute($timestamp)
    // {
    //     return $this->getFecha($timestamp);
    // }

    // public function getH4FnacAttribute($timestamp)
    // {
    //     return $this->getFecha($timestamp);
    // }
}
