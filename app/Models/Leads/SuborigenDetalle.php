<?php

namespace VCN\Models\Leads;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Informes\Venta;

use ConfigHelper;
use DB;

class SuborigenDetalle extends Model
{
    protected $table = 'viajero_suborigen_detalles';

    // protected $fillable = [];
    protected $guarded = ['_token'];


    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('name')->get();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            $o = Suborigen::plataforma()->pluck('id')->toArray();
            return self::whereIn('suborigen_id',$o)->orderBy('name')->get();
        }

        return self::orderBy('name')->get();
    }

    public function getFullNameAttribute()
    {
        $ret = "";

        $ret = $this->suborigen->origen->name ." > ". $this->suborigen->name ." > <strong>". $this->name . "</strong>";

        return $ret;
    }

    public function suborigen()
    {
        return $this->belongsTo('\VCN\Models\Leads\Suborigen', 'suborigen_id');
    }

    public function getVentas($any,$oficina_id=0)
    {
        $id = $this->id;

        if($oficina_id)
        {
            $ventas = Venta::where('any',$any)->where('oficina_id',$oficina_id)->where('origen_id',$this->suborigen->origen_id)->where('suborigen_id',$this->suborigen_id)->where('suborigendet_id',$id)->groupBy('category_id')->select('*', DB::raw('sum(inscripciones) as total_bookings'), DB::raw('sum(semanas) as total_semanas'),DB::raw('sum(total_curso) as total_curso'),DB::raw('sum(total) as total_total'));
        }
        else
        {
            $ventas = Venta::where('any',$any)->where('origen_id',$this->suborigen->origen_id)->where('suborigen_id',$this->suborigen_id)->where('suborigendet_id',$id)->groupBy('category_id')->select('*', DB::raw('sum(inscripciones) as total_bookings'), DB::raw('sum(semanas) as total_semanas'),DB::raw('sum(total_curso) as total_curso'),DB::raw('sum(total) as total_total'));
        }

        return $ventas->get();
    }

    public function getHayVentasAttribute()
    {
        return Venta::where('origen_id',$this->suborigen->origen_id)->where('suborigen_id',$this->suborigen_id)->where('suborigendet_id',$this->id)->count();
    }
}
