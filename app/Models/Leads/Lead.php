<?php

namespace VCN\Models\Leads;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $table = 'leads';

    // protected $fillable = [];
    protected $guarded = ['_token'];
}

class Account extends Model
{
    protected $table = 'accounts';

    // protected $fillable = [];
    protected $guarded = ['_token'];
}


class LeadLog extends Model
{
    protected $table = 'leads_follow_up';

    // protected $fillable = [];
    protected $guarded = ['_token'];
}

class LeadTarea extends Model
{
    protected $table = 'lead_next_action';

    // protected $fillable = [];
    protected $guarded = ['_token'];
}

