<?php

namespace VCN\Models\Leads;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Informes\Venta;

use ConfigHelper;
use DB;

class Suborigen extends Model
{
    protected $table = 'viajero_suborigenes';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('name')->get();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            $o = Origen::plataforma()->pluck('id')->toArray();
            return self::whereIn('origen_id',$o)->orderBy('name')->get();
        }

        return self::orderBy('name')->get();
    }

    public function getFullNameAttribute()
    {
        $ret = "";

        $ret = $this->origen->name ." > <strong>". $this->name . "</strong>";

        return $ret;
    }

    public function origen()
    {
        return $this->belongsTo('\VCN\Models\Leads\Origen', 'origen_id');
    }

    public function suborigenesdet()
    {
        return $this->hasMany('\VCN\Models\Leads\SuborigenDetalle', 'suborigen_id');
    }

    public function getVentas($any,$oficina_id=0)
    {
        $id = $this->id;

        //cambiado 11-05-2018 : qieren q se acumule

        if($oficina_id)
        {
            // $ventas = Venta::where('any',$any)->where('oficina_id',$oficina_id)->where('origen_id',$this->origen_id)->where('suborigen_id',$id)->where('suborigendet_id',0)->groupBy('category_id')->select('*', DB::raw('sum(inscripciones) as total_bookings'), DB::raw('sum(semanas) as total_semanas'),DB::raw('sum(total_curso) as total_curso'),DB::raw('sum(total) as total_total'));
            
            $ventas = Venta::where('any',$any)->where('oficina_id',$oficina_id)->where('origen_id',$this->origen_id)->where('suborigen_id',$id)->groupBy('category_id')->select('*', DB::raw('sum(inscripciones) as total_bookings'), DB::raw('sum(semanas) as total_semanas'),DB::raw('sum(total_curso) as total_curso'),DB::raw('sum(total) as total_total'));
        }
        else
        {
            // $ventas = Venta::where('any',$any)->where('origen_id',$this->origen_id)->where('suborigen_id',$id)->where('suborigendet_id',0)->groupBy('category_id')->select('*', DB::raw('sum(inscripciones) as total_bookings'), DB::raw('sum(semanas) as total_semanas'),DB::raw('sum(total_curso) as total_curso'),DB::raw('sum(total) as total_total'));
            
            $ventas = Venta::where('any',$any)->where('origen_id',$this->origen_id)->where('suborigen_id',$id)->groupBy('category_id')->select('*', DB::raw('sum(inscripciones) as total_bookings'), DB::raw('sum(semanas) as total_semanas'),DB::raw('sum(total_curso) as total_curso'),DB::raw('sum(total) as total_total'));
        }

        return $ventas->get();
    }

    public function getHayVentasAttribute()
    {
       return Venta::where('origen_id',$this->suborigen->origen_id)->where('suborigen_id',$this->id)->count();
    }
}
