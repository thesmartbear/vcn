<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;

class Nacionalidad extends Model
{
    protected $table = 'nacionalidades';

    // protected $fillable = [];
    protected $guarded = ['_token'];
}
