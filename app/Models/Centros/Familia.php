<?php

namespace VCN\Models\Centros;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\BookingFamilia;

class Familia extends Model
{
    protected $table = 'familias';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'hijos' => 'array',
        'hijos_fuera' => 'array',
        'familia' => 'array',
        'adultos' => 'array',
    ];

    public function delete()
    {
        if(BookingFamilia::where('familia_id',$this->id)->count()>0)
        {
            $error = "Imposible eliminar. Existen Bookings con esta Familia.";
            Session::flash('mensaje-alert', $error);
            return back();
        }

        parent::delete();
    }

    public function centro()
    {
        return $this->belongsTo('\VCN\Models\Centros\Centro', 'center_id');
    }

    public function bookings()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingFamilia', 'familia_id');
    }

    public function pais()
    {
        return $this->belongsTo('\VCN\Models\Pais', 'pais_id');
    }

    public function getPaisNameAttribute()
    {
        return $this->pais?$this->pais->name:"";
    }

    public function getDireccionCompletaAttribute()
    {
        return strip_tags($this->direccion). ",". strip_tags($this->poblacion) .",". strip_tags($this->cp) .",". strip_tags($this->pais_name);
    }
}
