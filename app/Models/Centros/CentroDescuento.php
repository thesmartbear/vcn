<?php

namespace VCN\Models\Centros;

use Illuminate\Database\Eloquent\Model;

class CentroDescuento extends Model
{
    protected $table = 'centro_descuentos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function centro()
    {
        return $this->belongsTo('\VCN\Models\Centros\Centro', 'center_id');
    }

    public function categoria()
    {
        return $this->belongsTo('\VCN\Models\Categoria', 'category_id');
    }

    public function subcategoria()
    {
        return $this->belongsTo('\VCN\Models\Subcategoria', 'subcategory_id');
    }

    public function detalle()
    {
        return $this->belongsTo('\VCN\Models\SubcategoriaDetalle', 'subcategory_det_id');
    }

    public function getImporteNameAttribute()
    {
        return $this->importe ." ". $this->centro->moneda_name;
    }

    public function getPercentNameAttribute()
    {
        return $this->center_discount_percent."%";
    }

    public function getPorcentajeAttribute()
    {
        return $this->center_discount_percent;
    }

    public function getNameAttribute()
    {
        if($this->tipo)
        {
            return $this->importe_name;
        }

        return $this->center_discount_percent." %";
    }
}
