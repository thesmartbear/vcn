<?php

namespace VCN\Models\Centros;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Proveedores\Proveedor;
use VCN\Models\Cursos\Curso;
use VCN\Models\Convocatorias\Cerrada;
use VCN\Models\Convocatorias\Abierta;
use VCN\Models\Convocatorias\ConvocatoriaMulti;

use VCN\Models\System\Documento;

use VCN\Helpers\ConfigHelper;

class Centro extends Model
{
    protected $table = 'centros';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function scopeConvocatoria($query,$tipo=0)
    {
        switch($tipo)
        {
            case 1: //Cerrada solo
            {
                $c = Cerrada::where('convocatory_semiopen',0)->pluck('course_id');
            }
            break;

            case 2: //SemiCerrada
            {
                $c = Cerrada::where('convocatory_semiopen',1)->pluck('course_id');
            }
            break;

            case 3: //Abierta
            {
                $c = Abierta::pluck('course_id');
            }
            break;

            case 4: //Multi
            {
                $cursos = Curso::where('es_convocatoria_multi',1)->pluck('center_id');
                return $query->whereIn('id',$cursos);
            }
            break;

            default:
            {
                return null;
            }
            break;
        }

        $cursos = Curso::whereIn('id',$c)->pluck('center_id');
        return $query->whereIn('id',$cursos);
    }

    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('name')->get();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            $proveedores = Proveedor::where('propietario',$filtro)->orWhere('propietario',0)->pluck('id');
            return self::whereIn('provider_id', $proveedores)->orderBy('name')->get();
        }

        return self::orderBy('name')->get();
    }

    public function monitores()
    {
        return $this->hasMany('\VCN\Models\Monitores\MonitorRelacion', 'modelo_id')->where('modelo','Centro');
    }

    public function proveedor()
    {
        return $this->belongsTo('\VCN\Models\Proveedores\Proveedor', 'provider_id');
    }

    public function pais()
    {
        return $this->belongsTo('\VCN\Models\Pais', 'country_id');
    }

    public function ciudad()
    {
        return $this->belongsTo('\VCN\Models\Ciudad', 'city_id');
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda', 'currency_id');
    }

    public function descuentos()
    {
        return $this->hasMany('\VCN\Models\Centros\CentroDescuento', 'center_id');
    }

    public function extras()
    {
        return $this->hasMany('\VCN\Models\Centros\CentroExtra', 'center_id');
    }

    public function alojamientos()
    {
        return $this->hasMany('\VCN\Models\Alojamientos\Alojamiento', 'center_id');
    }

    public function cursos()
    {
        return $this->hasMany('\VCN\Models\Cursos\Curso', 'center_id');
    }

    public function familias()
    {
        return $this->hasMany('\VCN\Models\Centros\Familia', 'center_id');
    }

    public function schools()
    {
        return $this->hasMany('\VCN\Models\Centros\School', 'center_id');
    }

    public function getAddressMapAttribute()
    {
        return str_replace("#", '', $this->address);
    }

    public function getDocumentos($idioma)
    {
        $docs = \VCN\Models\System\Documento::where('modelo','Centro')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();

        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::where('modelo','Centro')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();
    }
    
    public function getDocumentosArea($idioma, $plataforma=null)
    {
        $filtro = [0, $plataforma ?: ConfigHelper::config('propietario') ];

        $docs = \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','Centro')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','Centro')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
    }

    public function getMonedaIdAttribute()
    {
        return $this->currency_id;
    }

    public function getPaisNameAttribute()
    {
        return $this->pais?$this->pais->name:"-";
    }

    public function getPaisIdAttribute()
    {
        return $this->country_id;
    }

    public function getMonedaNameAttribute()
    {
        return $this->moneda->name;
    }

    public function getDepositosAttribute()
    {
        return $this->comment;
    }

    public function cuestionarios()
    {
        // return $this->hasManyThrough('\VCN\Models\System\Cuestionario', '\VCN\Models\System\CuestionarioVinculado', 'modelo_id', 'id')->where('modelo','Centro');
        return $this->belongsToMany('\VCN\Models\System\Cuestionario', 'cuestionario_vinculados','modelo_id')->where('modelo','Centro')->withPivot('id');
    }

    public function examenes()
    {
        return $this->belongsToMany(\VCN\Models\Exams\Examen::class, 'examen_vinculados','modelo_id')->where('modelo','Centro')->withPivot('id', 'excluye');
    }

    public function getParentsAttribute()
    {
        return [];
    }

    public function getExtrasObligatoriosAttribute()
    {
        $ret = collect();
        foreach( $this->extras as $extra )
        {
            if($extra->requerido && $extra->tipo_unidad<2)
            {
                $ret->push($extra);
            }
        }

        return $ret;
    }

    public function getContactoSOS($proveedor, $plataforma=0)
    {
        $m = \VCN\Models\System\ContactoModel::where('modelo','Centro')->where('modelo_id',$this->id);
        $c = clone $m;

        if($proveedor)
        {
            return $c->where('es_proveedor',1)->get();
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',$plataforma)->first();
        if($c)
        {
            return $c;
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',0)->first();
        return $c ?: 0;
    }
}
