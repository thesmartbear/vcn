<?php

namespace VCN\Models\Centros;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Bookings\BookingSchool;

use Session;

class School extends Model
{
    protected $table = 'centro_schools';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function delete()
    {
        if(BookingSchool::where('school_id',$this->id)->count()>0)
        {
            $error = "Imposible eliminar. Existen Bookings con este School.";
            Session::flash('mensaje-alert', $error);
            return back();
        }

        parent::delete();
    }

    public function centro()
    {
        return $this->belongsTo('\VCN\Models\Centros\Centro', 'center_id');
    }

    public function bookings()
    {
        return $this->hasMany('\VCN\Models\Bookings\BookingSchool', 'school_id');
    }

    public function pais()
    {
        return $this->belongsTo('\VCN\Models\Pais', 'pais_id');
    }

    public function getPaisNameAttribute()
    {
        return $this->pais?$this->pais->name:"";
    }

    public function getDireccionCompletaAttribute()
    {
        return strip_tags($this->direccion). ",". strip_tags($this->poblacion) .",". strip_tags($this->cp) .",". strip_tags($this->pais_name);
    }
}
