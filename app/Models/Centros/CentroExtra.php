<?php

namespace VCN\Models\Centros;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Monedas\Moneda;
use VCN\Models\Extras\ExtraUnidad;

use VCN\Helpers\ConfigHelper;

class CentroExtra extends Model
{
    protected $table = 'centro_extras';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function centro()
    {
        return $this->belongsTo('\VCN\Models\Centros\Centro', 'center_id');
    }

    public function tipo()
    {
        return $this->belongsTo('\VCN\Models\Extras\ExtraUnidad', 'center_extras_unit_id');
    }

    public function getNameAttribute()
    {
        return $this->center_extras_name;
    }

    public function getPrecioAttribute()
    {
        return $this->center_extras_price;
    }

    public function getMonedaAttribute()
    {
        return Moneda::find($this->center_extras_currency_id)->currency_name;
    }

    public function getMonedaIdAttribute()
    {
        return $this->center_extras_currency_id;
    }

    public function getTipoUnidadAttribute()
    {
        return $this->center_extras_unit;
    }

    public function getRequeridoAttribute()
    {
        return $this->center_extras_required;
    }
    public function getRequiredAttribute()
    {
        return $this->center_extras_required;
    }

    public function getTipoUnidadNameAttribute()
    {
        return ConfigHelper::getTipoUnidad($this->center_extras_unit);
    }

    public function getUnidadNameAttribute()
    {
        return $this->center_extras_unit_id?ExtraUnidad::find($this->center_extras_unit_id)->name:"-";
    }

    public function getUnidadIdAttribute()
    {
        return $this->center_extras_unit_id;
    }
}
