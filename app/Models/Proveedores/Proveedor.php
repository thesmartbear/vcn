<?php

namespace VCN\Models\Proveedores;

use Illuminate\Database\Eloquent\Model;

use VCN\Helpers\ConfigHelper;

class Proveedor extends Model
{
    protected $table = 'proveedores';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('name')->get();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return self::where('propietario', 0)->orWhere('propietario',$filtro)->orderBy('name')->get();
        }

        return self::orderBy('name')->get();
    }

    public function centros()
    {
        return $this->hasMany('\VCN\Models\Centros\Centro', 'provider_id');
    }

    public function alojamientos()
    {
        return $this->hasManyThrough('VCN\Models\Alojamientos\Alojamiento', 'VCN\Models\Centros\Centro', 'provider_id', 'center_id');
    }

    public function cursos()
    {
        return $this->hasManyThrough('VCN\Models\Cursos\Curso', 'VCN\Models\Centros\Centro', 'provider_id', 'center_id');
    }

    public function getContactoSOS($proveedor, $plataforma=0)
    {
        $m = \VCN\Models\System\ContactoModel::where('modelo','Proveedor')->where('modelo_id',$this->id);
        $c = clone $m;

        if($proveedor)
        {
            return $c->where('es_proveedor',1)->get();
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',$plataforma)->first();
        if($c)
        {
            return $c->contacto_id;
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',0)->first();
        return $c ?: 0;
    }
}
