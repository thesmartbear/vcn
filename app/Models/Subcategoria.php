<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Cursos\Curso;

use ConfigHelper;
use Session;
use Carbon;

use \VCN\Models\System\BaseModel;

class Subcategoria extends BaseModel
{
    protected $table = 'subcategorias';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'avisos' => 'array',
        'avisos_online' => 'array',
        'avisos_datos' => 'array',
        'avisos_doc' => 'array',
        'avisos_catalogo' => 'array',
        'avisos_cursosweb' => 'array',
        'promo_cambio_fijo_fechas' => 'array',
        'pdf_condiciones'    => 'json',
        'condiciones'    => 'json',
        'pdf_cancelacion'    => 'json',
    ];

    public function delete()
    {
        if(Curso::where('subcategory_id',$this->id)->count()>0)
        {
            $error = "Imposible eliminar. Existen Cursos con esta SubCategoría.";
            Session::flash('mensaje-alert', $error);
            return back();
        }

        parent::delete();
    }


    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('name')->get();

        // $filtro = ConfigHelper::config('propietario');
        // if($filtro)
        // {
        //     return self::where('propietario', 0)->orWhere('propietario',$filtro)->orderBy('name')->get();
        // }

        return self::orderBy('name')->get();
    }

    public function categoria()
    {
        return $this->belongsTo('\VCN\Models\Categoria', 'category_id');
    }

    public function subcategoriasdet()
    {
        return $this->hasMany('\VCN\Models\SubcategoriaDetalle', 'subcategory_id');
    }

    public function cursos()
    {
        return $this->hasMany('\VCN\Models\Cursos\Curso', 'subcategory_id');
    }

    public function getDocumentos($idioma)
    {
        $docs = \VCN\Models\System\Documento::where('modelo','Subcategoria')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();

        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::where('modelo','Subcategoria')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();
    }
    
    public function getDocumentosEspecificosAttribute()
    {
        return \VCN\Models\System\DocEspecifico::where('modelo','Subcategoría')->where('modelo_id',$this->id)->get();
    }

    public function getDocumentosArea($idioma, $plataforma=null)
    {
        $filtro = [0, $plataforma ?: ConfigHelper::config('propietario') ];

        $docs = \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','Subcategoria')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','Subcategoria')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
    }

    public function cuestionarios()
    {
        return $this->belongsToMany('\VCN\Models\System\Cuestionario', 'cuestionario_vinculados','modelo_id')->where('modelo','Subcategoria')->withPivot('id');
    }

    public function examenes()
    {
        return $this->belongsToMany(\VCN\Models\Exams\Examen::class, 'examen_vinculados','modelo_id')->where('modelo','Subcategoria')->withPivot('id', 'excluye');
    }

    public function getParentsAttribute()
    {
        return ['categoria'];
    }

    public function getMontoReservaTxtAttribute()
    {
        if($this->reserva_tipo)
        {
            return $this->reserva_valor?$this->reserva_valor ."%":"";
        }

        return ConfigHelper::parseMoneda($this->reserva_valor);
    }
    
    public function getContactoSOS($proveedor, $plataforma=0)
    {
        $m = \VCN\Models\System\ContactoModel::where('modelo','Subcategoria')->where('modelo_id',$this->id);
        $c = clone $m;

        if($proveedor)
        {
            return $c->where('es_proveedor',1)->get();
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',$plataforma)->first();
        if($c)
        {
            return $c->contacto_id;
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',0)->first();
        return $c ?: 0;
    }

    public function scriptUpdate()
    {
        $bookings = \VCN\Models\Bookings\Booking::where('course_end_date','>=',Carbon::now())->where('subcategory_id',$this->id);

        $i = 0;
        foreach($bookings->get() as $booking)
        {
            $pcfd = $booking->promo_cambio_fijo_default;
            $pcf = $booking->promo_cambio_fijo;

            if($pcf != $pcfd)
            {   
                $booking->promo_cambio_fijo = $pcfd;
                $booking->save();
                
                $i++;
            }
        }

        if($i)
        {
            Session::flash('mensaje', "Se han actualizado $i Bookings");
        }
    }
}
