<?php

namespace VCN\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class LandingPrograma extends Model
{
    protected $table = 'cms_landing_programas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

}
