<?php

namespace VCN\Models\CMS;


use VCN\Models\Cursos\Curso;
use VCN\Models\Traducciones\Traduccion;
use VCN\Helpers\Traductor;

use ConfigHelper;
use App;

class CategoriaWeb extends \VCN\Models\ModelAuditable
{
    protected $table = 'cms_categorias';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'campos' => 'array',
    ];


    public function scopePlataforma($query)
    {
        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return $query->where('propietario',$filtro)->orWhere('propietario',0);
        }

        return $query;
    }

    public function scopeMenuPrincipal($query)
    {
        return $query->where('menu_principal',true);
    }

    public function scopeMenuSecundario($query)
    {
        return $query->where('menu_secundario',true);
    }

    public function getHomeLinkAttribute()
    {
        $wc = $this;

        // if($subcat['es_link'] == 1){
        //     $url = (App::getLocale() != ConfigHelper::config('idioma') ? '/'.App::getLocale().'/' : '/') .Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'link', $subcat['id'], $subcat['link']);
        // }else{
        //     $url = (App::getLocale() != ConfigHelper::config('idioma') ? '/'.App::getLocale().'/' : '/') .Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $menucat['id'], $menucat['seo_url']).'/'.Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $subcat['id'], $subcat['seo_url']);
        // }

        $locale = App::getLocale();
        $idioma = ConfigHelper::config('idioma');

        if ($wc->es_link == 1)
        {
            $tlink = Traductor::trans('CategoriaWeb', 'link', $wc);
            if( strpos($tlink,'://') !== false)
            {
                return $tlink;
            }
            $url = ($locale != $idioma ? '/'.$locale.'/' : '/') .$tlink;
        }
        else
        {
            // 3 niveles

            $menucat = $wc->padre;
            if($menucat)
            {
                $menucat3 = $menucat->padre;
                if($menucat3)
                {
                    $url = ($locale != $idioma ? ('/'.$locale.'/') : '/') .Traductor::trans('CategoriaWeb', 'seo_url', $menucat3) .'/'. Traductor::trans('CategoriaWeb', 'seo_url', $menucat) .'/'. Traductor::trans('CategoriaWeb', 'seo_url', $wc);
                }
                else
                {
                    $url = ($locale != $idioma ? ('/'.$locale.'/') : '/') .Traductor::trans('CategoriaWeb', 'seo_url', $menucat) .'/'. Traductor::trans('CategoriaWeb', 'seo_url', $wc);
                }
            }
            else
            {
                // $url = Traductor::trans('CategoriaWeb', 'seo_url', $wc);
                $url = ($locale != $idioma ? ('/'.$locale.'/') : '/') .Traductor::trans('CategoriaWeb', 'seo_url', $wc);
            }
        }

        return $url;
    }

    public function getHomeEnlaceAttribute()
    {
        $url = $this->home_link;

        if ($this->link_blank == 1)
        {
            $link = 'href="'. $url .'" target="_blank"';
        }
        else
        {
            $link = 'href="'. $url .'"';
        }

        return $link;
    }
    
    public function getHomeEnlaceTxtAttribute()
    {
        $campo = "home_titulo_link";

        $ret = __('web.masinfo');
        if($this->home_titulo_link)
        {
            $ret = Traductor::trans('CategoriaWeb', $campo, $this);
        }

        return $ret;
    }

    public function getTranslate($campo, $alter=null)
    {
        // return Traductor::trans('CategoriaWeb', $campo, $this);

        if($alter == "namito")
        {
            return Traductor::getWeb(App::getLocale(), 'CategoriaWeb', $campo, $this->id, $this->$alter, true);    
        }
        
        $alter = $alter ?: $campo;
        return Traductor::getWeb(App::getLocale(), 'CategoriaWeb', $campo, $this->id, $this->$alter);
    }

    public static function buscar301Categoria($slug, $estructura=2)
    {
        $ret = null;

        if(!$slug)
        {
            return $ret;
        }

        $idioma = App::getLocale();
        $p = ConfigHelper::config('propietario');

        if($idioma == "es")
        {
            $list = Self::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('estructura', $estructura);
            $list = $list->where('seo_url',$slug);
            $list = $list->orderBy('orden', 'DESC');

            $ret = $list->first();
        }
        else
        {
            $t = Traduccion::where(['modelo'=> 'CategoriaWeb', 'idioma'=> $idioma, 'campo'=> 'seo_url', 'traduccion'=> $slug])->first();

            if($t)
            {
                $list = Self::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('estructura', $estructura);
                $list = $list->where('id', $t->modelo_id);

                $ret = $list->first();
            }
        }

        return $ret;

    }

    public static function buscar301Curso($slug, $estructura=2)
    {
        $ret = null;

        if(!$slug)
        {
            return $ret;
        }

        $idioma = App::getLocale();
        $p = ConfigHelper::config('propietario');

        $list = Self::whereIn('propietario', [$p, '0'])->where('activo', 1);
        $list = $list->where('estructura', $estructura)->orderBy('orden', 'DESC');

        foreach( $list->get() as $cat )
        {
            // ->hijos_activos->count() : seguimos profundizando
            // ->nivel??

            if($cat->hijos_activos->count())
            {
                // continue;
            }

            foreach($cat->cursos as $c)
            {
                if($idioma == "es")
                {
                    if($c->course_slug == $slug)
                    {
                        $ret = [
                            'Curso'=> $c,
                            'CategoriaWeb'=> $cat,
                        ];

                        break;
                    }
                }
                else
                {
                    $t = Traduccion::where(['modelo'=> 'Curso', 'modelo_id'=> $c->id])->first();
                    if($t)
                    {
                        if($t->traduccion == $slug)
                        {
                            $ret = [
                                'Curso'=> $c,
                                'CategoriaWeb'=> $cat,
                            ];

                            break;
                        }
                    }
                }

            }
        }

        return $ret;
    }

    public function padre()
    {
        return $this->belongsTo('\VCN\Models\CMS\CategoriaWeb', 'category_id');
    }

    public function hijos()
    {
        $p = $this->propietario;
        return $this->hasMany('\VCN\Models\CMS\CategoriaWeb', 'category_id')->whereIn('propietario',[$p,0]);
    }

    public function hijos_activos()
    {
        $p = $this->propietario;
        return $this->hasMany('\VCN\Models\CMS\CategoriaWeb', 'category_id')->whereIn('propietario',[$p,0])->where('activo',1);
    }

    public function pais()
    {
        return $this->belongsTo('\VCN\Models\Pais', 'pais_id');
    }

    public function getPaisesAttribute()
    {
        $paises = [];
        foreach($this->cursos as $c)
        { 
            foreach(\VCN\Models\Centros\Centro::distinct()->where('id', $c->centro->id)->get() as $p)
            {
                $paises[] = \VCN\Models\Pais::distinct('name')->where('id', $p->pais->id)->first()->name;
            }
        }

        $paises = array_unique($paises);
        return $paises;
    }

    public function getImagenHomeAttribute()
    {
        if($this->imagen)
        {
            if(file_exists( public_path($this->imagen) ))
            {
                return true;
            }
        }

        return false;
    }

    public function getImagenWebpAttribute()
    {
        $webp = $this->imagen . ".webp";

        if(file_exists( public_path($webp) ))
        {
            return $webp;
        }

        return $this->imagen;
    }

    public function getHomeImagenWebpAttribute()
    {
        $img = $this->home_imagen ?: $this->imagen;
        $webp = $img . ".webp";

        if(file_exists( public_path($webp) ))
        {
            return $webp;
        }

        return $img;
    }

    public function getSeoUrlTraduccionAttribute()
    {
        $idioma = App::getLocale();

        if($idioma != "es")
        {
            $t = Traduccion::where(['modelo'=> 'CategoriaWeb', 'idioma'=> $idioma, 'campo'=> 'seo_url', 'modelo_id'=> $this->id])->first();

            return $t?$t->traduccion:$this->seo_url;
        }

        return $this->seo_url;
    }

    public function getSlugAttribute()
    {
        $cat = $this;
        $ret = $cat->seo_url;

        $cat = $this;
        $ret = $cat->seo_url_traduccion;

        while($cat->padre)
        {
            $cat = $cat->padre;
            $ret = $cat->seo_url_traduccion."/".$ret;
        }

        $ret = "$ret";
        return $ret;
    }

    public function getSlugIdioma($lang=null)
    {
        $idioma = $lang ?: App::getLocale();

        if($idioma != "es")
        {
            $t = Traduccion::where(['modelo'=> 'CategoriaWeb', 'idioma'=> $idioma, 'campo'=> 'seo_url', 'modelo_id'=> $this->id])->first();

            return $t?$t->traduccion:$this->seo_url;
        }

        return $this->seo_url;
    }

    public function getNivelAttribute()
    {
        if(!$this->padre)
        {
            return 1;
        }

        $nivel = 1;
        $p = $this;

        do {
            $p = $p->padre;
            $nivel ++;

            if(!$p->padre)
            {
                break;
            }
            if($nivel>4)
            {
                return "?¿";
            }
        } while ($nivel>0);

        return $nivel;
    }

    public function getCategoriasListAttribute()
    {
        ($this->excluye_categorias) ? $excluye_categorias = 'whereNotIn' : $excluye_categorias = 'whereIn';

        $array = explode(',', $this->categorias);
        return \VCN\Models\Categoria::$excluye_categorias('id',$array)->get();
    }

    public function getSubcategoriasListAttribute()
    {
        ($this->excluye_subcategorias) ? $excluye_subcategorias = 'whereNotIn': $excluye_subcategorias = 'whereIn';

        $array = explode(',', $this->subcategorias);
        return \VCN\Models\Subcategoria::$excluye_subcategorias('id',$array)->get();
    }

    public function getSubcategoriasDetListAttribute()
    {
        ($this->excluye_subcategoriasdet) ? $excluye_subcategoriasdet = 'whereNotIn' :  $excluye_subcategoriasdet = 'whereIn';

        $array = explode(',', $this->subcategoriasdet);
        return \VCN\Models\SubcategoriaDetalle::$excluye_subcategoriasdet('id',$array)->get();
    }

    public function getCursosAttribute()
    {
        $paginate = 100000;

        $c = $this->categorias?explode(',', $this->categorias):[];
        $sc = $this->subcategorias?explode(',', $this->subcategorias):[];
        $scd = $this->subcategoriasdet?explode(',', $this->subcategoriasdet):[];
        $idiomas = $this->idioma?explode(',', $this->idioma):[];

        $pais_id = $this->pais ? $this->pais_id : null;        
        if(!$c && !$pais_id)
        {
            //No nay categorias
            return self::where('id',0)->paginate(0);
        }

        $list = null;

        ($this->excluye_categorias) ? $excluye_categorias = 'whereNotIn' : $excluye_categorias = 'whereIn';
        ($this->excluye_subcategorias) ? $excluye_subcategorias = 'whereNotIn': $excluye_subcategorias = 'whereIn';
        ($this->excluye_subcategoriasdet) ? $excluye_subcategoriasdet = 'whereNotIn' :  $excluye_subcategoriasdet = 'whereIn';

        $p = ConfigHelper::config('propietario');

        if(!$sc)
        {
            $list = Curso::whereIn('propietario',[$p,0])->$excluye_categorias('category_id',$c)->where('activo_web',1);
        }
        elseif($sc && !$scd) //tiene subcat
        {
            $list = Curso::whereIn('propietario',[$p,0])->$excluye_categorias('category_id',$c)->$excluye_subcategorias('subcategory_id',$sc)->where('activo_web',1);
        }
        elseif($c)
        {
            $list = Curso::whereIn('propietario',[$p,0])->$excluye_categorias('category_id',$c)->$excluye_subcategorias('subcategory_id',$sc)->$excluye_subcategoriasdet('subcategory_det_id',$scd)->where('activo_web',1);
        }

        if($pais_id)
        {
            $centros = \VCN\Models\Centros\Centro::where('country_id', $pais_id)->pluck('id')->toArray();
            $list = $list->whereIn('center_id',$centros);
        }

        $cursos = collect();
        if(!$idiomas)
        {
            return $list->paginate($paginate);
        }
        else
        {
            $list = $list->get();

            foreach($list as $curso)
            {
                foreach($idiomas as $i)
                {
                    if($curso->idiomas[0] == '')
                    {
                        $cursos->push($curso);
                    }
                    elseif( in_array($i,$curso->idiomas) && !$this->idioma_excluye )
                    {
                        $cursos->push($curso);
                    }
                    elseif( !in_array($i,$curso->idiomas) && $this->idioma_excluye )
                    {
                        $cursos->push($curso);
                    }
                    elseif( in_array($i,$curso->idiomas) && count($curso->idiomas) > 1 )
                    {
                        $cursos->push($curso);
                    }
                }
            }
        }

        $cids = $cursos->pluck('id')->toArray();
        $cursos = Curso::whereIn('id', $cids)->paginate($paginate);
        // $cursos = Curso::whereIn('id', $cids)->with('centro','convocatoriasCerradas','convocatoriasAbiertas')->paginate($paginate);
        // $cursos = Curso::whereIn('id', $cids)->paginate($paginate)->load('centro','convocatoriasCerradas','convocatoriasAbiertas');
        
        return $cursos;
    }


    public function getNumcursosAttribute()
    {

        if($this->nivel != 1)
        {
            return $this->cursos->total();
        }
        else
        {
            $numcursos = 0;
            if(count($this->hijos_activos))
            {
                foreach ($this->hijos_activos as $h1)
                {
                    if(count($h1->hijos_activos))
                    {
                        foreach ($h1->hijos_activos as $h2)
                        {
                            $numcursos += $h2->cursos->total();
                        }
                    }
                    else
                    {
                        $numcursos += $h1->cursos->total();
                    }
                }

                return $numcursos;

            }
            else
            {
                return $this->cursos->total();
            }
        }
    }

    public static function getPadresAttribute()
    {
        //Ejemplo con 3 niveles, si hay más es añadir bucles, se podría hacer recursivo pero es demasiado lio para lo que es.
        $arbol = array();
        $p = ConfigHelper::config('propietario');
        $e = ConfigHelper::config('web_estructura');

        $padres = self::where('estructura', $e)->whereIn('propietario',[0,$p])->where('activo',1)->where('category_id',0)->orderBy('orden')->get();

        return $padres;
    }

    public static function arbol($posicion=null, $offset=0, $limit=0, $estructura=1)
    {
        //Ejemplo con 3 niveles, si hay más es añadir bucles, se podría hacer recursivo pero es demasiado lio para lo que es.
        $arbol = array();
        $padres = self::arbolPadres($posicion, $offset, $limit, $estructura);

        foreach($padres as $item)
        {
            $arbol_subcats = null;
            foreach($item->hijos_activos->sortBy('orden') as $h1)
            {
                if($posicion && !$h1->$posicion)
                {
                    continue;
                }

                $arbol_subcatsdet = null;
                foreach($h1->hijos_activos->sortBy('orden') as $h2)
                {
                    if($posicion && !$h2->$posicion)
                    {
                        continue;
                    }
                    
                    if ($h2->numcursos || $h2->es_link == 1 || strpos($h2->seo_url, 'pdf') !== false)
                    {
                        $arbol_subcatsdet[] = [
                            'id' => $h2->id,
                            'name' => $h2->name,
                            'titulo' => $h2->titulo, 'seo_url' => $h2->seo_url, 'numcursos' => $h2->numcursos, 'es_link' => $h2->es_link, 'link' => $h2->link, 'link_blank' => $h2->link_blank,
                            'enlace'=> $h2->home_enlace,
                            'home_titulo' => $h2->getTranslate('home_titulo2', 'titulo')
                        ];
                    }
                }

                //menu_paises???

                if ($h1->numcursos || $h1->es_link == 1 || strpos($h1->seo_url, 'pdf') !== false)
                {
                    $arbol_subcats[] = [
                        'id' => $h1->id,
                        'name' => $h1->name,
                        'titulo' => $h1->titulo,
                        'seo_url' => $h1->seo_url,
                        'numcursos' => $h1->numcursos,
                        'subcategoriasdetalle' => $arbol_subcatsdet,
                        'es_link' => $h1->es_link,
                        'link' => $h1->link,
                        'link_blank' => $h1->link_blank,
                        'enlace'=> $h1->home_enlace,
                        'home_titulo' => $h1->getTranslate('home_titulo2', 'titulo')
                    ];
                }
                
            }

            // $enlace = "";
            // if($item->es_link == 1){
            //     $url = (App::getLocale() != ConfigHelper::config('idioma') ? '/'.App::getLocale().'/' : '/') .Traductor::trans('CategoriaWeb', 'link', $item);
            // }else{
            //     $url = (App::getLocale() != ConfigHelper::config('idioma') ? '/'.App::getLocale().'/' : '/') .Traductor::trans('CategoriaWeb', 'seo_url', $item);
            // }
            // if ($item->link_blank == 1){
            //     $enlace = 'href="'.$url.'" target="_blank"';
            // }else{
            //     $enlace = 'href="'.$url.'"';
            // }

            $arbol[] = [
                'id' => $item->id,
                'name' => $item->name,
                'titulo' => $item->titulo,
                'seo_url' => $item->seo_url,
                'numcursos' => $item->numcursos,
                'subcategorias' => $arbol_subcats,
                'es_link' => $item->es_link,
                'link' => $item->link,
                'link_blank' => $item->link_blank,
                'enlace'=> $item->home_enlace,
                'home_titulo' => $item->getTranslate('home_titulo2', 'titulo')
            ];
        }

        return $arbol;
    }

    public static function arbolPadres($posicion=null, $offset=0, $limit=0, $estructura=1, $p=null)
    {
        //Ejemplo con 3 niveles, si hay más es añadir bucles, se podría hacer recursivo pero es demasiado lio para lo que es.
        $arbol = array();
        $p = $p ?: ConfigHelper::config('propietario');
        $e = $estructura ?: ConfigHelper::config('web_estructura');

        // //position: 1: menu_principal, 2: menu_secundario ?!?!?!

        $padres = self::where('estructura',$e)->whereIn('propietario',[0,$p])->where('activo',1)->where('category_id',0)->orderBy('orden');
        
        if($posicion)
        {   
            $padres = $padres->where($posicion,1);
        }

        if($offset)
        {
            $padres = $padres->offset($offset);
        }

        if($limit)
        {
            $padres = $padres->limit($limit);
        }

        $padres = $padres->get();

        return $padres;
    }

    public function getHijosFiltroAttribute()
    {
        $p = ConfigHelper::config('propietario');

        $fcats = [];
        // $filCats = "";
        foreach($this->hijos_activos as $h)
        {
            $cat = Traductor::transLang(App::getLocale(), 'CategoriaWeb', 'name', $h);
            $c = "cat-$p-". str_slug($cat);
            $fcats[$c] = $cat;
        }

        // $filCats = implode(' ', array_keys($fcats));
        return $fcats;        
    }
}
