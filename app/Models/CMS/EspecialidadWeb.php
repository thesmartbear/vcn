<?php

namespace VCN\Models\CMS;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Cursos\Curso;
use VCN\Models\Cursos\CursoEspecialidad;
use VCN\Models\Especialidad;
use VCN\Models\Subespecialidad;

use ConfigHelper;

class EspecialidadWeb extends \VCN\Models\ModelAuditable
{
    protected $table = 'cms_especialidades';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'campos' => 'array',
    ];

    
    public function ScopePlataforma($query)
    {
        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return $query->where('propietario',$filtro)->orWhere('propietario',0);
        }

        return $query;
    }

    public function padre()
    {
        return $this->belongsTo('\VCN\Models\CMS\EspecialidadWeb', 'especialidad_id');
    }

    public function hijos()
    {
        $p = $this->propietario;
        return $this->hasMany('\VCN\Models\CMS\EspecialidadWeb', 'especialidad_id')->whereIn('propietario',[$p,0]);
    }

    public function hijos_activos()
    {
        $p = $this->propietario;
        return $this->hasMany('\VCN\Models\CMS\EspecialidadWeb', 'especialidad_id')->whereIn('propietario',[$p,0])->where('activo',1);
    }

    public function getNivelAttribute()
    {
        if(!$this->padre)
        {
            return 1;
        }

        $nivel = 1;
        $p = $this;

        do {
            $p = $p->padre;
            $nivel ++;

            if(!$p->padre)
            {
                break;
            }
            if($nivel>4)
            {
                return "?¿";
            }
        } while ($nivel>0);

        return $nivel;
    }

    public function getEspecialidadesListAttribute()
    {
        $array = explode(',', $this->especialidades);
        return \VCN\Models\Especialidad::whereIn('id',$array)->get();
    }

    public function getSubespecialidadesListAttribute()
    {
        $array = explode(',', $this->subespecialidades);
        return \VCN\Models\Subespecialidad::whereIn('id',$array)->get();
    }

    public function getCursosAttribute()
    {
        $cursos = collect();

        $e = $this->especialidades?explode(',', $this->especialidades):null;
        $se = $this->subespecialidades?explode(',', $this->subespecialidades):null;
        $idiomas = $this->idioma?explode(',', $this->idioma):null;

        if(!$e)
        {
            //No nay especialidades
            return $cursos;
        }

        $list = null;

        ($this->excluye_especialidades) ? $excluye_especialidades = 'whereNotIn' : $excluye_especialidades = 'whereIn';
        ($this->excluye_subespecialidades) ? $excluye_subespecialidades = 'whereNotIn': $excluye_subespecialidades = 'whereIn';

        $p = ConfigHelper::config('propietario');

        if(!$se)
        {
            $curso_e = CursoEspecialidad::$excluye_especialidades('especialidad_id',$e)->pluck('curso_id')->toArray();
            $list = Curso::whereIn('id',$curso_e)->whereIn('propietario',[$p,0])->where('activo_web',1)->get();
        }
        else //tiene subesp
        {
            $subesp = SubEspecialidad::$excluye_subespecialidades('id',$se)->pluck('id')->toArray();

            $curso_se = [];

            foreach($subesp as $k=>$valor)
            {
                $list = CursoEspecialidad::whereRaw(
                   'find_in_set(?, subespecialidad_id)',
                   [$valor]
                )->get()->pluck('curso_id')->toArray();

                foreach($list as $k=>$c)
                {
                    if(!in_array($c,$curso_se))
                    {
                        array_push($curso_se,$c);
                    }
                }
            }

            $list = Curso::whereIn('id',$curso_se)->whereIn('propietario',[$p,0])->where('activo_web',1)->get();
        }

        if(!$idiomas)
        {
            return $cursos;
        }
        else
        {
            foreach($list as $curso)
            {
                foreach($idiomas as $i)
                {
                    if($curso->idiomas[0] == '')
                    {
                        $cursos->push($curso);
                    }
                    elseif( in_array($i,$curso->idiomas) && !$this->idioma_excluye )
                    {
                        $cursos->push($curso);
                    }
                    elseif( !in_array($i,$curso->idiomas) && $this->idioma_excluye )
                    {
                        $cursos->push($curso);
                    }

                }
            }
        }

        return $cursos;
    }

    public function getNumcursosAttribute()
    {
        if($this->nivel != 1)
        {
            return count($this->cursos);
        }else{
            $numcursos = 0;
            if(count($this->hijos_activos)) {
                foreach ($this->hijos_activos as $h1) {
                    if(count($h1->hijos_activos)) {
                        foreach ($h1->hijos_activos as $h2) {
                            $numcursos += count($h2->cursos);
                        }
                    }else {
                        $numcursos += count($h1->cursos);
                    }
                }
                return $numcursos;
            }else{
                return count($this->cursos);
            }
        }
    }

    public static function arbol()
    {
        //Ejemplo con 3 niveles, si hay más es añadir bucles, se podría hacer recursivo pero es demasiado lio para lo que es.
        $arbol = array();
        $p = ConfigHelper::config('propietario');

        $padres = Self::whereIn('propietario',[0,$p])->where('activo',1)->where('especialidad_id',0)->orderBy('orden')->get();


        foreach($padres as $item){
            $arbol_subesp = null;

            foreach($item->hijos_activos->sortBy('orden') as $h1)
            {
                // foreach($h1->hijos_activos->sortBy('orden') as $h2)
                // {
                //     if ($h2->numcursos > 0 || $h2->es_link == 1 || strpos($h2->seo_url, 'pdf') !== false)
                //     {
                //         $arbol_subcatsdet[] = ['id' => $h2->id, 'name' => $h2->name, 'titulo' => $h2->titulo, 'seo_url' => $h2->seo_url, 'numcursos' => $h2->numcursos, 'es_link' => $h2->es_link, 'link' => $h2->link, 'link_blank' => $h2->link_blank];
                //     }
                // }

                if ($h1->numcursos > 0 || $h1->es_link == 1 || strpos($h1->seo_url, 'pdf') !== false)
                {
                    $arbol_subesp[] = ['id' => $h1->id, 'name' => $h1->name, 'titulo' => $h1->titulo, 'seo_url' => $h1->seo_url, 'numcursos' => $h1->numcursos, 'es_link' => $h1->es_link, 'link' => $h1->link, 'link_blank' => $h1->link_blank];
                }
            }
            $arbol[]= ['id' => $item->id, 'name' => $item->name, 'titulo' => $item->titulo, 'seo_url' => $item->seo_url, 'numcursos' => $item->numcursos, 'subespecialidades' => $arbol_subesp, 'es_link' => $item->es_link, 'link' => $item->link, 'link_blank' => $item->link_blank];
        }

        return $arbol;
    }

}
