<?php

namespace VCN\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class LandingTestimonio extends \VCN\Models\ModelAuditable
{
    protected $table = 'cms_landing_testimonios';

    // protected $fillable = [];
    protected $guarded = ['_token'];
    
    protected $casts = [
        'data' => 'array',
        'info' => 'array',
    ];

}
