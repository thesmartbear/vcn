<?php

namespace VCN\Models\CMS;

use Illuminate\Database\Eloquent\Model;

use VCN\Helpers\ConfigHelper;

use ReflectionClass;

// use VCN\Repositories\Cursos\CursoRepository as Curso;
use VCN\Models\Cursos\Curso;

class Pagina extends \VCN\Models\ModelAuditable
{
    protected $table = 'cms_paginas';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    
    public function getTipoNameAttribute()
    {
        return ConfigHelper::getCMSTipo($this->tipo);
    }


    public static function cursos($filtro=null)
    {
        $m = new Curso;

        if($filtro)
        {
            return self::findWhere($m, $filtro);
        }

        return $m->all();
    }

    public static function ficha($modelo, $id)
    {

    }

    public function padre()
    {
        return $this->belongsTo('\VCN\Models\CMS\Pagina', 'page_id');
    }

    public function hijos()
    {
        $p = $this->propietario;
        return $this->hasMany('\VCN\Models\CMS\Pagina', 'page_id')->whereIn('propietario',[$p,0]);
    }

    public function hijos_activos()
    {
        $p = $this->propietario;
        return $this->hasMany('\VCN\Models\CMS\Pagina', 'page_id')->whereIn('propietario',[$p,0])->where('activo',1);
    }

    public function getNivelAttribute()
    {
        if(!$this->padre)
        {
            return 1;
        }

        $nivel = 1;
        $p = $this;

        do {
            $p = $p->padre;
            $nivel ++;

            if(!$p->padre)
            {
                break;
            }
            if($nivel>4)
            {
                return "?¿";
            }
        } while ($nivel>0);

        return $nivel;
    }

    public static function findWhere($model, $where, $columns = ['*'], $or = false)
    {
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = (! $or)
                    ? $model->where($value)
                    : $model->orWhere($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = (! $or)
                        ? $model->where($field, $operator, $search)
                        : $model->orWhere($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = (! $or)
                        ? $model->where($field, '=', $search)
                        : $model->orWhere($field, '=', $search);
                }
            } else {
                $model = (! $or)
                    ? $model->where($field, '=', $value)
                    : $model->orWhere($field, '=', $value);
            }
        }
        return $model->get($columns);
    }

}
