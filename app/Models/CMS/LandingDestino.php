<?php

namespace VCN\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class LandingDestino extends Model
{
    protected $table = 'cms_landing_destinos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

}
