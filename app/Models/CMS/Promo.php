<?php

namespace VCN\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class Promo extends \VCN\Models\ModelAuditable
{
    protected $table = 'cms_promos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    
    public function seccion()
    {
        return $this->belongsTo('\VCN\Models\CMS\CategoriaWeb', 'seccion_id');
    }

    public function getSeccionAttribute(){
        if($this->seccion_id != 0){
            return \VCN\Models\CMS\CategoriaWeb::where('id',$this->seccion_id)->first();
        }
    }
}
