<?php

namespace VCN\Models\CMS;

use Illuminate\Database\Eloquent\Model;

use Carbon;

class LandingForm extends Model
{
    protected $table = 'cms_landing_forms';

    // protected $fillable = [];
    protected $guarded = ['_token'];
    
    protected $casts = [
        'data' => 'array',
        'info' => 'array',
    ];

    public function landing()
    {
        return $this->belongsTo(\VCN\Models\CMS\Landing::class, 'landing_id');
    }

    public function getFechanacDmyAttribute()
    {
        return $this->fechanac ? Carbon::parse($this->fechanac)->format("d/m/Y") : "";
    }

    public function getInfoIpAttribute()
    {
        return $this->info['ip'];
    }

    public function getInfoGeoAttribute()
    {
        $geo = $this->info['geo'];
        return $geo['iso_code'] ." [". $geo['city'] ."] ". $geo['postal_code'];
    }

    public function getInfoMapaAttribute()
    {
        $geo = $this->info['geo'];
        $url_map = "https://www.google.es/maps/@". $geo['lat'] .",". $geo['lon'] .",15z";

        $ret = "<a target='_blank' href='$url_map'>(". $geo['lat'] .",". $geo['lon'] .")</a>";
        
        return $ret;
    }

    public function getUtmSourceAttribute()
    {
        return $this->info['utm_source'] ?? "";
    }

    public function getUtmMediumAttribute()
    {
        return $this->info['utm_medium'] ?? "";
    }

    public function getUtmCampaignAttribute()
    {
        return $this->info['utm_campaign'] ?? "";
    }

    public function getUtmAttribute()
    {
        if(!$this->utm_source == "") return "";
        if($this->utm_source == "") return "";

        return "$this->utm_source - $this->utm_medium - $this->utm_campaign";
    }
}
