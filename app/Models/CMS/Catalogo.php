<?php

namespace VCN\Models\CMS;

use Illuminate\Database\Eloquent\Model;

use File;

class Catalogo extends \VCN\Models\ModelAuditable
{
    protected $table = 'cms_catalogos';

    // protected $fillable = [];
    protected $guarded = ['_token'];



    public function delete()
    {
        //Pendiente: borrar archivos
        File::delete(public_path($this->pdf));
        File::delete(public_path($this->imagen));

        parent::delete();
    }

    public function scopeActivos($query)
    {
        return $query->where('activo',1);
    }

    public function plataforma()
    {
        return $this->belongsTo('\VCN\Models\System\Plataforma', 'plataforma_id');
    }
}
