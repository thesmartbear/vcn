<?php

namespace VCN\Models\CMS;

use Illuminate\Database\Eloquent\Model;

use ConfigHelper;
use DB;
use Traductor;

class Landing extends \VCN\Models\ModelAuditable
{
    protected $table = 'cms_landings';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'testimonios' => 'array',
        'destinos' => 'array',
        'programas' => 'array',
        'avisos'    => 'array',
        'avisos_catalogo'    => 'array',
    ];

    public function forms()
    {
        return $this->hasMany(\VCN\Models\CMS\LandingForm::class, 'landing_id');
    }

    public function formsGrup()
    {
        return $this->hasMany(\VCN\Models\CMS\LandingForm::class, 'landing_id')->groupBy(DB::raw('Date(created_at)'), 'landing_id', 'nombre', 'email', 'telefono');
    }

    public function getTemaNameAttribute()
    {
        return ConfigHelper::config($this->template);
    }

    public function getTemplateAttribute()
    {
        return "landings." . $this->tema;
    }

    public function getSubtitleChecksAttribute()
    {
        return explode("\r\n", $this->subtitle2);
    }

    public function getCatalogoAttribute()
    {
        if(!$this->catalogo_id)
        {
            return null;
        }

        
        $cid = Traductor::trans("Landing", 'catalogo_id', $this);

        return \VCN\Models\CMS\Catalogo::find($cid);

    }

    public function getTestimoniosListAttribute()
    {
        // return LandingTestimonio::whereIn('id', $this->testimonios ?? [])->get();
        $ret = collect();
        if (!$this->testimonios) {
            return $ret;
        }

        foreach ($this->testimonios as $d) {
            $item = LandingTestimonio::find($d);
            if ($item) {
                $ret->push($item);
            }
        }

        return $ret;
    }

    public function getTestimoniosIdsAttribute()
    {
        $ret = "";

        if (!$this->testimonios) {
            return $ret;
        }

        $arr = $this->testimonios ?? [];
        foreach ($arr as $d) {
            if (!$d) {
                continue;
            }

            $ret .= $d;

            if ($d !== end($arr)) {
                $ret .= ",";
            }
        }

        return $ret;
    }

    public function getDestinosListAttribute()
    {
        // return LandingDestino::whereIn('id', $this->destinos ?? [])->get();
        $ret = collect();

        if (!$this->destinos) {
            return $ret;
        }

        foreach ($this->destinos as $d) {
            $item = LandingDestino::find($d);
            if ($item) {
                $ret->push($item);
            }
        }

        return $ret;
    }

    public function getDestinosIdsAttribute()
    {
        $ret = "";

        if (!$this->destinos) {
            return $ret;
        }

        $arr = $this->destinos ?? [];
        foreach ($arr as $d) {
            if (!$d) {
                continue;
            }

            $ret .= $d;

            if ($d !== end($arr)) {
                $ret .= ",";
            }
        }

        return $ret;
    }

    public function getProgramasListAttribute()
    {
        // return LandingPrograma::whereIn('id', $this->programas ?? [])->get();
        $ret = collect();

        if (!$this->programas) {
            return $ret;
        }

        foreach ($this->programas as $d) {
            $item = LandingPrograma::find($d);
            if ($item) {
                $ret->push($item);
            }
        }

        return $ret;
    }

    public function getProgramasIdsAttribute()
    {
        $ret = "";

        if (!$this->programas) {
            return $ret;
        }

        $arr = $this->programas ?? [];
        foreach ($arr as $d) {
            if (!$d) {
                continue;
            }

            $ret .= $d;

            if ($d !== end($arr)) {
                $ret .= ",";
            }
        }

        return $ret;
    }

    public function getSeccionListAttribute()
    {
        return $this->es_general ? $this->destinos_list : $this->programas_list;
    }

    public function getSeccionModelAttribute()
    {
        return $this->es_general ? "LandingDestino" : "LandingPrograma";
    }
}
