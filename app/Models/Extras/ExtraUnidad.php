<?php

namespace VCN\Models\Extras;

use Illuminate\Database\Eloquent\Model;

class ExtraUnidad extends Model
{
    protected $table = 'extra_unidades';

    // protected $fillable = [];
    protected $guarded = ['_token'];
}
