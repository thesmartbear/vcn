<?php

namespace VCN\Models\Extras;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Categoria;

class Extra extends Model
{
    protected $table = 'extras';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function tipo()
    {
        return $this->belongsTo('\VCN\Models\Extras\ExtraUnidad', 'generic_unit_id');
    }

    public function getTipoNameAttribute()
    {
        if(!$this->tipo)
        {
            return "";
        }
        return $this->tipo->name;
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda', 'generic_currency_id');
    }

    public function getNameAttribute()
    {
        return $this->generic_name;
    }

    public function getPrecioAttribute()
    {
        return $this->generic_price;
    }

    public function getMonedaIdAttribute()
    {
        return $this->generic_currency_id;
    }

    public function getUnidadIdAttribute()
    {
        return $this->generic_unit_id;
    }

    public function getTipoUnidadAttribute()
    {
        return $this->generic_unit;
    }
}
