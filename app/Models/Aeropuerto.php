<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;

class Aeropuerto extends Model
{
    protected $table = 'airports';

    public function getNameDetalleAttribute()
    {
        return "$this->name ($this->iata), $this->city";
    }
}
