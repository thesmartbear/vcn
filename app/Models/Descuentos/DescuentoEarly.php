<?php

namespace VCN\Models\Descuentos;

use Illuminate\Database\Eloquent\Model;

class DescuentoEarly extends Model
{
    protected $table = 'descuentos_early';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda', 'moneda_id');
    }

    public function getDetalleAttribute()
    {
        return $this->name . " [". $this->importe ." ". $this->moneda->name ."]";
    }
}
