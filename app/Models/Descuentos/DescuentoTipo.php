<?php

namespace VCN\Models\Descuentos;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Monedas\Moneda;
use ConfigHelper;


class DescuentoTipo extends Model
{
    protected $table = 'descuento_tipos';

    // protected $fillable = [];
    protected $guarded = ['_token'];
    protected $casts = [
        'especialidad_id' => 'array',
        'especialidad_importe' => 'array',
    ];

    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::where('activo',1)->get();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return self::where('activo',1)->whereIn('propietario', [0, $filtro])->get();
        }

        return self::where('activo',1)->get();
    }

    public function moneda()
    {
        return $this->belongsTo('VCN\Models\Monedas\Moneda', 'moneda_id');
    }

    public function getTipoNameAttribute()
    {
        if($this->tipo==1)
        {
            return "Importe";
        }

        switch($this->tipo_pc)
        {
            case 0:
            {
                $ret = $this->valor ."% del Total";
            }
            break;

            case 1:
            {
                $ret = $this->valor ."% del Total";
            }
            break;

            case 2:
            {
                $ret = $this->valor ."% del Total";
            }
            break;
        }

        return $ret;
    }

    public function getDetallesAttribute()
    {
        $ret = "";

        if($this->tipo==1)
        {
            return "Importe [$this->valor $this->moneda_name]";
        }
        else
        {
            return $this->tipo_name;
        }

        return $ret;
    }

    public function getFullNameAttribute()
    {
        return $this->name ." [". $this->detalles ."]";
    }

    public function getMonedaNameAttribute()
    {
        return $this->moneda?$this->moneda->name:'';
    }
}
