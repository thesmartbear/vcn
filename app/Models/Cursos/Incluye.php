<?php

namespace VCN\Models\Cursos;

use Illuminate\Database\Eloquent\Model;

class Incluye extends Model
{
    protected $table = 'incluyes';

    // protected $fillable = [];
    protected $guarded = ['_token'];
}
