<?php

namespace VCN\Models\Cursos;

use Illuminate\Database\Eloquent\Model;

class CursoSlug extends Model
{
    protected $table = 'curso_slugs';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function curso()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Curso', 'course_id');
    }
}
