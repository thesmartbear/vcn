<?php

namespace VCN\Models\Cursos;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Subespecialidad;
use VCN\Helpers\Traductor;
use App;
class CursoEspecialidad extends Model
{
    protected $table = 'curso_especialidades';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function curso()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Curso', 'curso_id');
    }

    public function especialidad()
    {
        return $this->belongsTo('\VCN\Models\Especialidad', 'especialidad_id');
    }

    public function getSubespecialidadesAttribute()
    {
        return explode(',', $this->subespecialidad_id);
        //return $this->belongsTo('\VCN\Models\Subespecialidad', 'subespecialidad_id');
    }

    public function getSubespecialidadesNameAttribute()
    {
        $cats = explode(',', $this->subespecialidad_id);

        $ret = "";

        $itot = 0;
        $tot = count($cats);
        foreach($cats as $c)
        {
            $cat = Subespecialidad::find($c);

            if($cat)
            {
                $ret .= $cat->name;

                if(++$itot != $tot)
                {
                    $ret .= ", ";
                }
            }
        }

        return $ret;
    }

    public function getSubespecialidadesNameLangAttribute()
    {
        $cats = explode(',', $this->subespecialidad_id);

        $ret = "";

        $itot = 0;
        $tot = count($cats);
        foreach($cats as $c)
        {
            $cat = Subespecialidad::find($c);

            if($cat)
            {
                $cn = Traductor::getWeb(App::getLocale(), 'Subespecialidad', 'name', $cat->id, $cat->name);
                $ret .= $cn;

                if(++$itot != $tot)
                {
                    $ret .= ", ";
                }
            }
        }

        return $ret;
    }
}
