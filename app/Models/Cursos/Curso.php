<?php

namespace VCN\Models\Cursos;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Categoria;
use VCN\Models\Alojamientos\Alojamiento;
use VCN\Models\Convocatorias\Vuelo;
use VCN\Models\Convocatorias\Cerrada;
use VCN\Models\Convocatorias\Abierta;
use VCN\Models\Convocatorias\ConvocatoriaMulti;
use VCN\Models\Bookings\Booking;
use VCN\Models\System\Documento;
use VCN\Models\Monedas\Moneda;
use VCN\Models\CMS\CategoriaWeb;
// use Vinkla\Instagram\Instagram;

use VCN\Models\System\Plataforma;
use VCN\Helpers\Traductor;
use VCN\Helpers\ConfigHelper;
use Session;
use Carbon;

use \VCN\Models\System\BaseModel;

class Curso extends BaseModel
{
    protected $table = 'cursos';

    protected $auditInclude = [
        'frase','course_summary',
    ];


    // protected $fillable = [];
    protected $guarded = ['_token'];

    protected $casts = [
        'condiciones'       => 'json',
        'pdf_cancelacion'   => 'json',
        'dataweb'           => 'object'
    ];

    public function delete()
    {
        if(Booking::where('curso_id',$this->id)->count()>0)
        {
            $error = "Imposible eliminar. Existen Bookings con este Curso.";
            Session::flash('mensaje-alert', $error);
            return back();
        }

        parent::delete();
    }

    public function scopeActivos($query)
    {
        return $query->where('course_active',1);
    }

    public function scopeActivoWeb($query)
    {
        return $query->where('activo_web',1);
    }

    public function scopeConvocatoria($query,$tipo=0)
    {
        switch($tipo)
        {
            case 1: //Cerrada solo
            {
                $c = Cerrada::where('convocatory_semiopen',0)->pluck('course_id');
            }
            break;

            case 2: //SemiCerrada
            {
                $c = Cerrada::where('convocatory_semiopen',1)->pluck('course_id');
            }
            break;

            case 3: //Abierta
            {
                $c = Abierta::pluck('course_id');
            }
            break;

            case 4: //Multi
            {
                return $query->where('es_convocatoria_multi',1);
            }
            break;

            default:
            {
                return null;
            }
            break;
        }

        return $query->whereIn('id',$c);
    }

    public static function plataforma()
    {
        if( auth()->user()->isFullAdmin() )
            return self::orderBy('course_name')->get();

        $filtro = ConfigHelper::config('propietario');
        if($filtro)
        {
            return self::where('propietario', $filtro)->orWhere('propietario', 0)->get()->sortBy('course_name');
        }

        return self::orderBy('course_name')->get();
    }

    public function getSlugAttribute()
    {
        return $this->course_slug;
    }

    public function setSlug($slug=null)
    {
        $slug0 = $this->course_slug;
        $slug1 = $slug ?: str_slug($this->course_name);

        if($slug0 != $slug1)
        {
            $cs = new CursoSlug;
            $cs->course_id = $this->id;
            $cs->slug = $slug0;
            $cs->save();
        }

        $c = self::where('course_slug', $slug)->get();
        if($c->count())
        {
            // $slug = "$slug-2";
            Session::flash("mensaje", "Revise el slug del Curso (SEO). Está duplicado");
        }
        //Traducción tb:
        $t = \VCN\Models\Traducciones\Traduccion::where(['modelo'=> 'Curso', 'idioma'=> "ca", 'campo'=> 'course_slug', 'traduccion'=> $slug]);
        if($t->count()>1)
        {
            Session::flash("mensaje", "Revise la traducción del slug del Curso (SEO). Está duplicado ". $t->count() ." veces");
        }

        $this->course_slug = $slug1;
        $this->save();

    }

    public function monitores()
    {
        return $this->hasMany('\VCN\Models\Monitores\MonitorRelacion', 'modelo_id')->where('modelo','Curso');
    }

    public function getMonitorAttribute()
    {
        if(!$this->monitor_web)
        {
            return null;
        }

        $m = null;

        $monitores = $this->centro->monitores;
        if($monitores->count() && !$m)
        {
            $m = $monitores->first();
        }

        $monitores = $this->monitores;
        if($monitores->count() && !$m)
        {
            $m = $monitores->first();
        }

        if(!$m)
        {
            foreach($this->convocatorias as $c)
            {
                if($c->monitores->count())
                {
                    $m = $c->monitores->first();
                    break;
                }

            }
        }

        if($m)
        {
            return $m->monitor?$m->monitor:null;
        }

        return null;
    }

    public function getMonitorAsignacionAttribute()
    {
        if(!$this->monitor_web)
        {
            return null;
        }

        $m = null;

        $monitores = $this->centro->monitores;
        if($monitores->count() && !$m)
        {
            $m = "Centro";
        }

        $monitores = $this->monitores;
        if($monitores->count() && !$m)
        {
            $m = "Curso";
        }

        if(!$m)
        {
            foreach($this->convocatorias as $c)
            {
                if($c->monitores->count())
                {
                    $m = "Convocatoria ". $this->convocatoria_tipo .":";
                    $m .= "<a href='". $c->route ."'> (". $c->id .") ". $c->name ."</a>";
                    break;
                }

            }
        }

        return $m;
    }

    public function getConvocatoriasAttribute()
    {
        if($this->es_convocatoria_nula)
        {
            return collect();
        }

        if( $this->es_convocatoria_abierta )
        {
            return $this->convocatoriasAbiertas;
        }

        if( $this->es_convocatoria_multi )
        {
            return $this->convocatoriasMulti;
        }

        if( $this->es_convocatoria_cerrada )
        {
            return $this->convocatoriasCerradas;
        }

        return collect();
    }

    public function getConvocatoriasAllAttribute()
    {
        if( $this->es_convocatoria_abierta )
        {
            return $this->convocatoriasAbiertasAll;
        }

        if( $this->es_convocatoria_multi )
        {
            return $this->convocatoriasMultiAll;
        }

        if( $this->es_convocatoria_cerrada )
        {
            return $this->convocatoriasCerradasAll;
        }

        return collect();
    }

    public function slugs()
    {
        return $this->hasMany('\VCN\Models\Cursos\CursoSlug', 'course_id');
    }

    public function centro()
    {
        return $this->belongsTo('\VCN\Models\Centros\Centro', 'center_id');
    }

    public function getIdiomasAttribute()
    {
        return explode(',', $this->course_language);
    }

    public function getPaisAttribute()
    {
        return $this->centro->pais;
    }

    public function getPaisNameAttribute()
    {
        return $this->centro->pais_name;
    }

    public function getDocumentos($idioma)
    {
        $docs = \VCN\Models\System\Documento::where('modelo','Curso')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();

        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::where('modelo','Curso')->where('modelo_id',$this->id)->where('idioma',$idioma)->get();
    }

    public function getDocumentosEspecificosAttribute()
    {
        return \VCN\Models\System\DocEspecifico::where('modelo','Curso')->where('modelo_id',$this->id)->get();
    }

    public function getDocumentosArea($idioma, $plataforma=null)
    {
        $filtro = [0, $plataforma ?: ConfigHelper::config('propietario') ];

        $docs = \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','Curso')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
        foreach($docs as $doc)
        {
            $dir = public_path("assets/uploads/documentos/". $doc->idioma ."/". $doc->modelo ."/". $doc->modelo_id . "/");
            if(!\File::exists($dir.$doc->doc))
            {
                $doc->delete();
            }
        }

        return \VCN\Models\System\Documento::whereIn('plataforma',$filtro)->where('modelo','Curso')->where('modelo_id',$this->id)->where('visible',1)->where('idioma',$idioma)->get();
    }

    public function reuniones()
    {
        return $this->hasMany('\VCN\Models\Reuniones\Reunion', 'curso_id');
    }

    public function bookings()
    {
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'curso_id');
    }

    public function bookings_completos()
    {
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'curso_id')->whereIn('status_id',$stplazas); //estaba comentado el filtro???
    }

    public function bookings_completos_futuro()
    {
        $stplazas = \VCN\Models\Bookings\Status::where('plazas',1)->pluck('id');
        return $this->hasMany('\VCN\Models\Bookings\Booking', 'curso_id')->where('course_start_date','>',Carbon::now())->whereIn('status_id',$stplazas);
    }

    public function categoria()
    {
        return $this->belongsTo('\VCN\Models\Categoria', 'category_id');
    }

    public function getCategoriaNameAttribute()
    {
        return $this->categoria?$this->categoria->name:"-";
    }

    public function subcategoria()
    {
        return $this->belongsTo('\VCN\Models\Subcategoria', 'subcategory_id');
    }

    public function getSubcategoriaNameAttribute()
    {
        return $this->subcategoria?$this->subcategoria->name:"-";
    }

    public function subcategoria_detalle()
    {
        return $this->belongsTo('\VCN\Models\SubcategoriaDetalle', 'subcategory_det_id');
    }

    public function getSubcategoriaDetalleNameAttribute()
    {
        return $this->subcategoria_detalle?$this->subcategoria_detalle->name:"-";
    }

    public function getHayPlazasAttribute()
    {
        $ret = true;

        if($this->es_convocatoria_cerrada)
        {
            foreach($this->convocatoriasCerradasSolo->where('activo_web',1) as $cc)
            {
                $plazas_alojamiento = 1;
                if($cc->alojamiento)
                {
                    $pa = $cc->getPlazas($cc->alojamiento->id);
                    if($pa)
                    {
                       $plazas_alojamiento = $pa->plazas_disponibles;
                    }
                }

                if($plazas_alojamiento>0)
                {
                    return true;
                }
            }

            foreach($this->convocatoriasSemiCerradas->where('activo_web',1) as $cc)
            {
                $ret = false;

                $plazas_alojamiento = 1;
                if($cc->alojamiento)
                {
                    $pa = $cc->getPlazas($cc->alojamiento->id);
                    if($pa)
                    {
                       $plazas_alojamiento = $pa->plazas_disponibles;
                    }
                }

                if($plazas_alojamiento>0)
                {
                    return true;
                }
            }
        }

        return $ret;
    }

    public function getCategoriaFullNameAttribute()
    {
        $ret = $this->categoria?$this->categoria->name:"-";

        if($this->subcategoria)
        {
            $ret .= " > ". $this->subcategoria->name;
        }

        if($this->subcategoria_det)
        {
            $ret .= " > ". $this->subcategoria_det->name;
        }

        return $ret;
    }

    public function getCategoriaWebAttribute(){

        $this->catweb = '';
        $catcurso = $this->categoria?$this->categoria->id:0;
        $p = ConfigHelper::config('propietario');

        if (ConfigHelper::config('tema') == 'britishsummerv3') {
            $catsweb = CategoriaWeb::whereIn('propietario', [$p, '0'])->where('activo', 1)->where('category_id', 0)->whereRaw('FIND_IN_SET(' . $catcurso . ',categorias)')->orderBy('orden')->get();
            foreach ($catsweb as $cw) {
                $arrayCursosCat = $cw->cursos->pluck('id')->toArray();

                if (in_array($this->id, $arrayCursosCat)) {
                    $this->catweb = $cw;
                }
                if (count($cw->hijos_activos) > 0) {
                    $hijoscatweb = $cw->hijos_activos;
                    foreach ($hijoscatweb as $hcw) {
                        if(count($hcw->cursos) > 0) {
                            $arrayCursosSub = $hcw->cursos->pluck('id')->toArray();
                            //dd($arrayCursosSub);
                            if (in_array($this->id, $arrayCursosSub)) {
                                $this->catweb = $cw;
                                $this->subcatweb = $hcw;
                                $this->cerrado = $hcw->cerrado;
                            }
                        }
                    }
                }
            }
            if ($this->subcatweb != '') {
                if (count($this->subcatweb->hijos_activos) > 0) {
                    $hijossub = $this->subcatweb->hijos_activos;
                    foreach ($hijossub as $hs) {
                        $arrayCursos = $hs->cursos->pluck('id')->toArray();
                        if (in_array($this->id, $arrayCursos)) {
                            $this->subcatwebdet = $hs;
                            $this->cerrado = $hs->cerrado;
                        }
                    }

                }
            }
        }

        return $this->catweb;

    }

    public function getProveedorAttribute()
    {
        return $this->centro->proveedor;
    }

    public function extras()
    {
        return $this->hasMany('\VCN\Models\Cursos\CursoExtra', 'course_id');
    }

    public function extrasGenericos()
    {
        return $this->hasMany('\VCN\Models\Cursos\CursoExtraGenerico', 'course_id');
    }

    public function especialidades()
    {
        return $this->hasMany('\VCN\Models\Cursos\CursoEspecialidad', 'curso_id');
    }

    public function precios()
    {
        $precios = collect([]);
        if( $this->convocatoriasAbiertas->count()>0 )
        {
            foreach($this->convocatoriasAbiertas as $convo)
            {
                $precios = $precios->merge($convo->precios);
            }

            return $precios;
        }

        return $precios;
    }

    public function getEdadesAttribute()
    {
        return explode(',', $this->course_age);
    }

    public function getAlojamientosAttribute()
    {
        $a = explode(',', $this->course_accommodation);

        return Alojamiento::whereIn('id',$a)->get();
    }

    public function getVuelosAttribute()
    {
        $vuelos = collect([]);
        return $vuelos; //da problemas de convo-vuelo

        foreach($this->convocatoriasCerradas as $c)
        {
            $vuelos = $vuelos->merge($c->vuelos);
        }

        return $vuelos;
    }

    // public function getCategoriasAttribute()
    // {
    //     return explode(',', $this->course_category);
    // }

    // public function getSubcategoriasAttribute()
    // {
    //     return explode(',', $this->course_subcategory);
    // }

    // public function getCategoriasNameAttribute()
    // {
    //     $cats = explode(',', $this->course_category);

    //     $ret = "";

    //     $itot = 0;
    //     $tot = count($cats);
    //     foreach($cats as $c)
    //     {
    //         $cat = Categoria::find($c);

    //         if($cat)
    //         {
    //             $ret .= $cat->name;

    //             if(++$itot != $tot)
    //             {
    //                 $ret .= ", ";
    //             }
    //         }
    //     }

    //     return $ret;
    // }

    public function convocatoriasCerradas()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\Cerrada', 'course_id')->where('convocatory_close_status',1);
    }

    public function convocatoriasCerradasSolo()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\Cerrada', 'course_id')->where('convocatory_close_status',1)->where('convocatory_semiopen',0);
    }
    public function convocatoriasSemiCerradas()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\Cerrada', 'course_id')->where('convocatory_close_status',1)->where('convocatory_semiopen',1);
    }

    public function convocatoriasCerradasAll()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\Cerrada', 'course_id');
    }

    public function convocatoriasAbiertas()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\Abierta', 'course_id')->where('convocatory_open_status',1);
    }

    public function convocatoriasAbiertasAll()
    {
        return $this->hasMany('\VCN\Models\Convocatorias\Abierta', 'course_id');
    }

    public function getConvocatoriasMultiAttribute ()
    {
        return \VCN\Models\Convocatorias\ConvocatoriaMulti::whereRaw(
           'find_in_set(?, cursos_id)',
           [$this->id]
        )->where('activa',1)->get();
    }

    public function getConvocatoriasMultiAllAttribute ()
    {
        return \VCN\Models\Convocatorias\ConvocatoriaMulti::whereRaw(
           'find_in_set(?, cursos_id)',
           [$this->id]
        )->get();
    }

    public function getEsMenorAttribute()
    {
        return $this->categoria ? $this->categoria->es_menor : 0;
    }

    public function getEsConvocatoriaAbiertaAttribute()
    {
        if( $this->convocatoriasAbiertasAll->count()>0 )
            return true;

        return false;
    }

    public function getEsConvocatoriaCerradaAttribute()
    {
        if( $this->convocatoriasCerradasAll->count()>0 )
            return true;

        return false;
    }

    public function getEsConvocatoriaNulaAttribute()
    {
        if( !$this->convocatoriasCerradasAll->count() && !$this->convocatoriasAbiertasAll->count() && !$this->convocatoriasMulti->count() )
            return true;

        return false;
    }

    public function getEsConvocatoriaSemicerradaAttribute()
    {
        return ( $this->convocatoriasSemiCerradas->count()>0 );
    }

    public function getEsConvocatoriaSemicerradaSoloAttribute()
    {
        if( !$this->convocatoriasCerradasSolo->count() && $this->es_convocatoria_semicerrada )
        {
            return true;
        }

        return false;

        // return !$this->es_convocatoria_cerrada_solo && $this->es_convocatoria_semicerrada;
    }

    public function getEsConvocatoriaCerradaSoloAttribute()
    {
        return $this->es_convocatoria_cerrada && !$this->es_convocatoria_semicerrada;
    }

    public function getConvocatoriaTipoAttribute()
    {
        if($this->es_convocatoria_nula)
        {
            return "sin definir";
        }

        $ret = 0;
        if( $this->es_convocatoria_abierta )
        {
            $ret = "abierta";
        }

        if( $this->es_convocatoria_multi )
        {
            $ret = "multi";
        }

        if( $this->es_convocatoria_cerrada )
        {
            if($ret!=0)
            {
                $ret .= "cerrada";
                // $ret .= " y cerrada";
            }
            else
            {
                $ret = "cerrada";
            }
        }

        return $ret;
    }

    public function getConvocatoriaCampoAttribute()
    {
        if($this->es_convocatoria_nula)
        {
            return null;
        }

        $ret = 0;
        if( $this->es_convocatoria_abierta )
        {
            $ret = "convocatory_open_id";
        }

        if( $this->es_convocatoria_multi )
        {
            $ret = "convocatory_multi_id";
        }

        if( $this->es_convocatoria_cerrada )
        {
            $ret = "convocatory_close_id";
        }

        return $ret;
    }

    public function getConvocatoriaTipoNumAttribute()
    {
        if($this->es_convocatoria_nula)
        {
            return 0;
        }

        $ret = 0;
        if( $this->es_convocatoria_abierta )
        {
            return 3;
        }

        if( $this->es_convocatoria_multi )
        {
            return 4;
        }

        if( $this->es_convocatoria_cerrada )
        {
            return 1;
        }

        return $ret;
    }

    public function getConvocatoriaName($id)
    {
        if($this->es_convocatoria_nula)
        {
            return null;
        }

        $ret = 0;
        if( $this->es_convocatoria_abierta )
        {
            $c = Abierta::find($id);
        }

        if( $this->es_convocatoria_multi )
        {
            $c = ConvocatoriaMulti::find($id);
        }

        if( $this->es_convocatoria_cerrada )
        {
            $c = Cerrada::find($id);
        }

        return $c?$c->name:"-";
    }

    public function getNameAttribute()
    {
        return $this->course_name;
    }

    public function getDuracionUnitAttribute()
    {
        $ret = 1; //default semanas

        if( $this->es_convocatoria_abierta )
        {
            $precio = $this->precios()->first();

            if($precio)
            {
                if($precio->duracion == 1)
                {
                    return $precio->duracion_fijo;
                }
                else
                {
                    return $precio->duracion-1;
                }
            }

            // return 1;
        }
        elseif( $this->es_convocatoria_cerrada )
        {
            $cc = $this->convocatoriasCerradas();
            if($cc->count()>0)
            {
                $cc1 = $cc->first();
                return $cc1?$cc1->duracion_fijo:1;
            }
        }

        return $ret;

    }

    public function getDuracionName($units)
    {
        return trans_choice("web.curso.".$this->duracion_name,$units);
    }

    public function getDuracionNameAttribute()
    {
        if( $this->es_convocatoria_abierta )
        {
            $precio = $this->precios()->first();

            if($precio)
            {
                if($precio->duracion == 1)
                {
                    return ConfigHelper::getPrecioDuracionUnit($precio->duracion_fijo);
                }
                else
                {
                    return ConfigHelper::getPrecioDuracionUnit($precio->duracion-1);
                }
            }

            return "Semanas";
        }
        else
        {
            $cc = $this->convocatoriasCerradas();
            if($cc->count()>0)
            {
                $cc1 = $cc->first();
                return $cc1?$cc1->duracion_name:"Semanas";
            }

            return $this->convocatoria?$this->convocatoria->duracion_name:"Semanas";
        }


        return $ret;

    }

    public function getDuracionNameEngAttribute()
    {
        // $arr = ['Para...','Semanas','Meses','Trimestres','Semestres','Años'];

        switch($this->duracion_name)
        {
            case 'Semanas':
            {
                return "weeks";
            }
            break;

            case 'Semana':
            {
                return "week";
            }
            break;

            case 'Meses':
            {
                return "months";
            }
            break;

            case 'Mes':
            {
                return "month";
            }
            break;

            case 'Trimestres':
            {
                return "quarters";
            }
            break;

            case 'Trimestre':
            {
                return "quarter";
            }
            break;

            case 'Semestres':
            {
                return "semesters";
            }
            break;

            case 'Semestre':
            {
                return "semester";
            }
            break;

            case 'Años':
            {
                return 'years';
            }
            break;

            case 'Año':
            {
                return 'year';
            }
            break;
        }
    }

    public function calcularFechaFin($fechaini, $duracion, $diaini=0, $diafin=0)
    {
        $fecha = clone $fechaini;

        if($diaini == 7){$diaini = 0;};
        if($diafin == 7){$diafin = 0;};

        switch($this->duracion_unit)
        {
            case 1: //Semanas
            {
                $ret = $fecha->addWeeks($duracion);
            }
            break;

            case 2: //Meses
            {
                $ret = $fecha->addMonths($duracion);
            }
            break;

            case 3: //Trimestres
            {
                $ret = $fecha->addMonths($duracion*3);
            }
            break;

            case 4: //Semestres
            {
                $ret = $fecha->addMonths($duracion*6);
            }
            break;

            case 5: //Meses
            {
                $ret = $fecha->addYears($duracion);
            }
            break;

            default: //Semanas
            {
                $ret = $fecha->addWeeks($duracion);
            }
            break;
        }

        if($diaini!=$diafin)
        {
            $ret = $ret->subWeek();

            while($ret->dayOfWeek!=$diafin)
            {
                $ret->addDay();
            }
        }

        return $ret;
    }

    public function getPrefijoContableAttribute()
    {
        //Prioridad: subcategory_det_id => subcategory_id => category_id

        if(!$this->categoria)
        {
            return null;
        }

        if($this->subcategoria_detalle)
        {
            $contable = $this->subcategoria_detalle->contable;
            if($contable)
            {
                return $contable;
            }
        }

        if($this->subcategoria)
        {
            $contable = $this->subcategoria->contable;
            if($contable)
            {
                return $contable;
            }
        }

        if($this->categoria->contable)
        {
            $contable = $this->categoria->contable;
            return $this->categoria->contable;
        }

        return null;
    }

    public function cuestionarios()
    {
        // return $this->hasManyThrough('\VCN\Models\System\Cuestionario', '\VCN\Models\System\CuestionarioVinculado', 'modelo_id', 'id')->where('modelo','Curso');
        return $this->belongsToMany('\VCN\Models\System\Cuestionario', 'cuestionario_vinculados','modelo_id')->where('modelo','Curso')->withPivot('id');
    }

    public function examenes()
    {
        return $this->belongsToMany(\VCN\Models\Exams\Examen::class, 'examen_vinculados','modelo_id')->where('modelo','Curso')->withPivot('id', 'excluye');
    }

    public function getParentsAttribute()
    {
        return ['subcategoria_detalle', 'subcategoria', 'categoria', 'centro'];
    }

    public function getExtrasObligatoriosAttribute()
    {
        $ret = collect();
        foreach( $this->extras as $extra )
        {
            if($extra->requerido && $extra->tipo_unidad<2)
            {
                $ret->push($extra);
            }
        }

        return $ret;
    }

    public function getExtrasGenericosObligatoriosAttribute()
    {
        $ret = collect();
        foreach( $this->extrasGenericos as $extra )
        {
            if($extra->generico->generic_required && $extra->generico->tipo_unidad<2)
            {
                $ret->push($extra);
            }
        }

        return $ret;
    }

    public function getExtrasCentroObligatoriosAttribute()
    {
        $ret = collect();
        foreach( $this->centro->extras as $extra )
        {
            if($extra->requerido && $extra->tipo_unidad<2)
            {
                $ret->push($extra);
            }
        }

        return $ret;
    }

    public function getDivisasAttribute()
    {
        $monedasUsadas = [];

        $moneda = null;
        if($this->es_convocatoria_abierta)
        {
            $convo = $this->convocatoriasAbiertas->first();
            if($convo)
            {
                $moneda = $convo->moneda;
                if($moneda)
                {
                    array_push($monedasUsadas, $moneda->id );
                }

                //precios
                foreach($convo->precios as $p)
                {
                    $moneda = $p->moneda;
                    if($moneda)
                    {
                        array_push($monedasUsadas, $moneda->id );
                    }
                }
            }
        }

        if($this->es_convocatoria_cerrada)
        {
            $convo = $this->convocatoriasCerradas->first();
            if($convo)
            {
                $moneda = $convo->moneda;
                if($moneda)
                {
                    array_push($monedasUsadas, $moneda->id );
                }

                //Precio auto
                if($convo->precio_auto)
                {
                    $precio = $convo->precio_auto;
                    array_push($monedasUsadas, $precio->proveedor_moneda_id );
                    array_push($monedasUsadas, $precio->vuelo_moneda_id );
                    array_push($monedasUsadas, $precio->seguro_moneda_id );
                    array_push($monedasUsadas, $precio->gastos_moneda_id );
                    array_push($monedasUsadas, $precio->monitor_moneda_id );
                    array_push($monedasUsadas, $precio->mb_moneda_id );
                    array_push($monedasUsadas, $precio->comision_moneda_id );
                }
            }
        }

        if($this->es_convocatoria_multi)
        {
            $convo = $this->convocatoriasMulti->first();
            if($convo)
            {
                $moneda = $convo->moneda;
                if($moneda)
                {
                    array_push($monedasUsadas, $moneda->id );
                }
            }
        }

        //Centro
        $moneda = $this->centro->moneda;
        if($moneda)
        {
            array_push($monedasUsadas, $moneda->id );
        }

        foreach($this->extras_obligatorios as $extra)
        {
            array_push($monedasUsadas, $extra->moneda_id );
        }

        foreach($this->extras_genericos_obligatorios as $extra)
        {
            array_push($monedasUsadas, $extra->moneda_id );
        }

        foreach($this->extras_centro_obligatorios as $extra)
        {
            array_push($monedasUsadas, $extra->moneda_id );
        }

        $monedasUsadas = array_unique($monedasUsadas);

        return $monedasUsadas;
    }

    public function getDivisasTxtWebAttribute()
    {
        $monedas2Ret = [];

        $monedas = $this->divisas;

        $mid = ConfigHelper::default_moneda_id();

        foreach($monedas as $moneda)
        {
            $m = Moneda::find($moneda);
            if($m && $m->id != $mid)
            {
                $mtxt = $m->name ." = ". ConfigHelper::monedaCambioCursoWeb($moneda,$this->id);

                array_push($monedas2Ret, $mtxt );
            }
        }

        return $monedas2Ret;

    }

    public function getDivisasTxtAttribute()
    {
        $monedas2Ret = [];

        $monedas = $this->divisas;

        $mid = ConfigHelper::default_moneda_id();

        foreach($monedas as $moneda)
        {
            $m = Moneda::find($moneda);
            if($m && $m->id != $mid)
            {
                $mtxt = $m->name ." = ". ConfigHelper::monedaCambioCurso($moneda,$this->id);

                array_push($monedas2Ret, $mtxt );
            }
        }

        return $monedas2Ret;
    }

    public function getMontoReservaTxtAttribute()
    {
        if($this->reserva_tipo)
        {
            return $this->reserva_valor?$this->reserva_valor ."%":"";
        }

        return ConfigHelper::parseMoneda($this->reserva_valor);
    }


    public function getFotosCentroAttribute()
    {
        $curso = $this;
        $fotos = null;

        $path = public_path() ."/assets/uploads/center/" . $curso->centro->center_images;
        $folder = "/assets/uploads/center/" . $curso->centro->center_images;

        if (is_dir($path)) {
            $results = scandir($path);
            foreach ($results as $result)
            {
                if ($result === '.' or $result === '..' or $result[0] === '.') continue;

                $result_thumb = $result;
                if(strpos($result, ".webp"))
                {
                    $result_thumb = substr($result, 0, -5);
                }
                
                $file = $path . '/' . $result;
                $img = $folder . '/' . $result;
                $filethumb = $folder . '/thumb/' . $result_thumb;

                if (is_file($file))
                {
                    $foto['img'] = $img;
                    $foto['thumb'] = $filethumb;
                    $foto['name'] = $result;
                    $fotos[] = $foto;
                }
            }
        }
        return $fotos;
    }

    public function getFotosCursoAttribute()
    {
        $curso = $this;
        $fotos = null;

        $path = public_path() ."/assets/uploads/course/" . $curso->course_images;
        $folder = "/assets/uploads/course/" . $curso->course_images;

        if (is_dir($path)) {
            $results = scandir($path);
            foreach ($results as $result)
            {
                if ($result === '.' or $result === '..' or $result[0] === '.') continue;

                $result_thumb = $result;
                if(strpos($result, ".webp"))
                {
                    $result_thumb = substr($result, 0, -5);
                }
                
                $file = $path . '/' . $result;
                $img = $folder . '/' . $result;
                $filethumb = $folder . '/thumb/' . $result_thumb;

                if (is_file($file))
                {
                    $foto['img'] = $img;
                    $foto['thumb'] = $filethumb;
                    $foto['name'] = $result;
                    $fotos[] = $foto;
                }
            }
        }
        return $fotos;
    }

    public function getWebStyleBgAttribute()
    {

        if($this->image_portada)
        {
            if(is_file(public_path() ."/assets/uploads/course/" . $this->course_images . "/" .$this->image_portada))
            {
                $ret = "background-image: url('/assets/uploads/course/". $this->course_images ."/thumb/". $this->image_portada. "')";
                return $ret;
            }
        }
        
        if($this->centro->center_image_portada)
        {
            if(is_file(public_path() ."/assets/uploads/center/" . $this->centro->center_image . "/" .$this->centro->center_image_portada))
            {
                $ret = "background-image: url('/assets/uploads/course/". $this->centro->center_image ."/thumb/". $this->centro->center_image_portada. "')";
                return $ret;
            }
        }

        $fotos = $this->fotos_curso;
        if($fotos)
        {
            $ret = "/assets/uploads/course/". $this->course_images ."/thumb/";
            $f = $fotos[rand(0,count($fotos)-1)]['thumb'];
            $ret = $f;

            $ret = "background-image: url('". $ret ."'); background-size: cover; background-position: center center;";
            return $ret;
        }
     
        $fotos = $this->fotos_centro;
        if($fotos)
        {
            $ret = "/assets/uploads/center/". $this->centro->center_images ."/thumb/";
            $f = $fotos[rand(0,count($fotos)-1)]['thumb'];
            $ret = $f;

            $ret = "background-image: url('". $ret ."'); background-size: cover;";// background-position: center center;";
            return $ret;
        }

        $ret = "background: #E5E5E5;";
        return $ret;
    }

    public function getEsPocketAttribute()
    {
        if($this->pocket)
        {
            return true;
        }

        if($this->subcategoria)
        {
            if($this->subcategoria->pocket)
            {
                return true;
            }
        }

        if($this->categoria)
        {
            if($this->categoria->pocket)
            {
                return true;
            }
        }

        return false;
    }

    public function getWebPieAttribute()
    {
        if($this->subcategoria)
        {
            if($this->subcategoria->web_pie)
            {
                return true;
            }
        }

        if($this->categoria)
        {
            if($this->categoria->web_pie)
            {
                return true;
            }
        }

        return false;
    }

    public function getWebPieTxtAttribute()
    {
        if($this->subcategoria)
        {
            if($this->subcategoria->web_pie)
            {
                return $this->subcategoria->web_pie_txt;
            }
        }

        if($this->categoria)
        {
            if($this->categoria->web_pie)
            {
                return $this->categoria->web_pie_txt;
            }
        }

        return null;
    }

    public function getContactoSOS($proveedor, $plataforma=0)
    {
        $m = \VCN\Models\System\ContactoModel::where('modelo','Curso')->where('modelo_id',$this->id);
        $c = clone $m;

        if($proveedor)
        {
            return $c->where('es_proveedor',1)->get();
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',$plataforma)->first();
        if($c)
        {
            return $c;
        }

        $c = clone $m;
        $c = $c->where('es_proveedor',0)->where('plataforma',0)->first();
        return $c ?: 0;
    }

    public function getFotosInstagramAttribute()
    {
        return null;

        $token = $this->instagram_token;
        // $token = "42532811.1677ed0.e6997b88efb447d5942a94ad68ed406c";

        $fotos = null;

        if(!$token)
        {
            return $fotos;
        }

        try {
            $instagram = new Instagram($token);

            foreach ($instagram->media() as $ig)
            {
                $href = $ig->link;
                $img = $ig->images->standard_resolution->url;
                $thumb = $ig->images->thumbnail->url;
                $name = $ig->caption->text;

                $foto['img'] = $img;
                $foto['thumb'] = $thumb;
                $foto['name'] = $name;
                $fotos[] = $foto;
            }

        }catch(\Exception $e){ return $fotos; }

        // //??
        // $fotoscurso .= '<div class="col-md-4 col-sm-6 col-sx-12 fotos"><div class="foto"><a rel="group" href="' . $href . '"><img class="img-responsive" src="' . $img . '" alt=""></a></div></div>';
        // $fotoscursoname[] = $name;

        return $fotos;
    }

    public function updateDataWeb($plazas=false)
    {
        ini_set('memory_limit', '600M');
        set_time_limit(0);

        //dataweb(json): centro, pais, especialidades, alojamientos listado fotos_centro, listado fotos_curso, centro->ciudad, style bg

        $dataweb = [];
        
        $p = ConfigHelper::config('propietario') ?: 1;
        $plataforma = Plataforma::find($p);
        $idiomas = explode(',',$plataforma->idiomas);

        foreach($idiomas as $i)
        {
            $espes = array();
            $especialidades = array();
            $subespecialidades = array();
            foreach($this->especialidades->sortBy('name') as $e)
            {
                $eid = $e->especialidad->id;
                $esp = Traductor::transLang($i, 'Especialidad', 'name', $e->especialidad);

                $espes[] = str_slug($esp);
                $especialidades[$eid] = $esp;
                $subespecialidades[$eid] = Traductor::getWeb($i, 'Subespecialidad', 'name', $e->subespecialidad_id, $e->SubespecialidadesName);
            }

            $alojascurso = array();
            foreach($this->alojamientos as $alojamiento)
            {
                $alojascurso[] = str_slug(Traductor::transLang($i, 'AlojamientoTipo', 'accommodation_type_name', $alojamiento->tipo));
            }

            $alojas = array();
            foreach($this->alojamientos as $alojamiento)
            {
                $alojas[] = Traductor::transLang($i, 'AlojamientoTipo', 'accommodation_type_name', $alojamiento->tipo);
            }

            $edades = array();
            if($this->es_menor == 1)
            {
                foreach(explode(',',$this->course_age) as $edad)
                {
                    $edades[] = trans('web.edad').'-'.trim($edad);
                }
            }
            else
            {
                $edades = array_map('trim',$edades);
            }

            $pais = $this->centro->pais;
            $paisColor = $pais->color;
            // $pais = $pais->name;
            // $ciudad = $this->centro->ciudad->city_name;

            $pais = Traductor::transLang($i, 'Pais', 'name', $pais);

            $ciudad = $this->centro->ciudad;
            $ciudad = Traductor::transLang($i, 'Ciudad', 'city_name', $ciudad);

            $catsweb = [];

            // $idplats = [1,2];
            $idplats = ConfigHelper::plataformas(false);
            foreach($idplats as $idplat => $plat)
            {
                $menuPrincipal = \VCN\Models\CMS\CategoriaWeb::arbolPadres('menu_principal', 0, 0, 1, $idplat);

                foreach($menuPrincipal as $cat)
                {
                    $cids = $cat->cursos->pluck('id')->toArray();
                    if(in_array($this->id, $cids))
                    {
                        $catname = Traductor::transLang($i, 'CategoriaWeb', 'name', $cat);
                        $c = "cat-$idplat-". str_slug($catname);
                        $catsweb[$c] = $catname;
                    }

                    foreach($cat->hijos_activos as $scat)
                    {
                        $cids = $scat->cursos->pluck('id')->toArray();
                        if(in_array($this->id, $cids))
                        {
                            $catname = Traductor::transLang($i, 'CategoriaWeb', 'name', $scat);
                            $c = "cat-$idplat-". str_slug($catname);
                            $catsweb[$c] = $catname;
                        }

                        foreach($scat->hijos_activos as $scatd)
                        {
                            $cids = $scatd->cursos->pluck('id')->toArray();
                            if(in_array($this->id, $cids))
                            {
                                $catname = Traductor::transLang($i, 'CategoriaWeb', 'name', $scatd);
                                $c = "cat-$idplat-". str_slug($catname);
                                $catsweb[$c] = $catname;
                            }
                        }
                    }
                }
            }

            $dataweb[$i] = [
                'id'            => $this->id,
                'name'          => Traductor::getWeb($i, 'Curso', 'name', $this->id, $this->course_name),
                'especialidades'=> $especialidades,
                'subespecialidades'=> $subespecialidades,
                'espes'         => implode(' ', array_unique($espes)),
                'alojascurso'   => implode(' ', array_unique($alojascurso)),
                'alojas'        => implode(', ', array_unique($alojas)),
                'edades'        => implode(' ', $edades),
                'catsweb'       => implode(' ', array_keys($catsweb)),
                'centro'        => $this->centro->name,
                'pais'          => $pais,
                'ciudad'        => $ciudad,
                'mix'           => ($pais != 'España') ? str_slug($pais) : str_slug($ciudad),
                'web_style_bg'  => $this->web_style_bg,
                'fotos_curso'   => $this->fotos_curso,
                'fotos_centro'  => $this->fotos_centro,
                // 'cursolink'     => '/'.Traductor::transLang($i, 'CategoriaWeb', 'seo_url', $this->categoria).'/'.Traductor::trans('Curso', 'course_slug', $this).'.html',
                'cursolink'     => Traductor::trans('Curso', 'course_slug', $this).'.html',
                'course_age_range'  => Traductor::transLang($i, 'Curso', 'course_age_range', $this),
                'pais_color'    => $paisColor,
            ];
        }
        
        $this->dataweb = $dataweb;
        $this->save();

        if($plazas)
        {
            $this->updateDataWebPlazas();
        }
    }

    public function updateDataWebPlazas()
    {
        $dataweb = $this->dataweb;
        if(!$dataweb)
        {
            $this->updateDataWeb();
            $dataweb = $this->dataweb;
        }

        $plazasrestantes = 0;

        $convocatorias = $this->convocatoriasCerradas;
        if( $convocatorias->contains('convocatory_semiopen', 0) )
        {
            foreach($convocatorias->sortBy('convocatory_close_start_date')->sortBy('convocatory_close_duration_weeks') as $cc)
            {
                if($cc->activo_web == 1)
                {
                    if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 0)
                    {
                        $plazasrestantes += $cc->plazas_disponibles;
                    }
                }
            }
        }

        $dataweb->plazas = $plazasrestantes;

        $this->dataweb = $dataweb;
        $this->save();
    }

    public function updateDataWebTag($tag, $value)
    {
        $dataweb = $this->dataweb;
        if(!$dataweb)
        {
            $this->updateDataWeb();
            $dataweb = $this->dataweb;
        }

        $dataweb->$tag = $value;

        $this->dataweb = $dataweb;
        $this->save();
    }
}
