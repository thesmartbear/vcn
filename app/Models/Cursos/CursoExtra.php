<?php

namespace VCN\Models\Cursos;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Monedas\Moneda;
use VCN\Models\Extras\ExtraUnidad;

use VCN\Helpers\ConfigHelper;

class CursoExtra extends Model
{
    protected $table = 'curso_extras';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function curso()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Curso', 'course_id');
    }

    public function tipo()
    {
        return $this->belongsTo('\VCN\Models\Extras\ExtraUnidad', 'course_extras_unit_id');
    }

    public function getTipoNameAttribute()
    {
        if(!$this->tipo)
        {
            return "";
        }
        return $this->tipo->name;
    }

    public function getNameAttribute()
    {
        return $this->course_extras_name;
    }

    public function getPrecioAttribute()
    {
        return $this->course_extras_price;
    }

    public function getMonedaAttribute()
    {
        return Moneda::find($this->course_extras_currency_id)->currency_name;
    }

    public function getMonedaIdAttribute()
    {
        return $this->course_extras_currency_id;
    }

    public function getRequeridoAttribute()
    {
        return $this->course_extras_required;
    }
    public function getRequiredAttribute()
    {
        return $this->course_extras_required;
    }

    public function getTipoUnidadAttribute()
    {
        return $this->course_extras_unit;
    }

    public function getTipoUnidadNameAttribute()
    {
        return ConfigHelper::getTipoUnidad($this->course_extras_unit);
    }

    public function getUnidadNameAttribute()
    {
        return $this->course_extras_unit_id?ExtraUnidad::find($this->course_extras_unit_id)->name:"-";
    }

    public function getUnidadIdAttribute()
    {
        return $this->course_extras_unit_id;
    }
}
