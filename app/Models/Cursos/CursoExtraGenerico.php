<?php

namespace VCN\Models\Cursos;

use Illuminate\Database\Eloquent\Model;

use VCN\Models\Monedas\Moneda;
use VCN\Models\Extras\ExtraUnidad;

use VCN\Helpers\ConfigHelper;

class CursoExtraGenerico extends Model
{
    protected $table = 'curso_extra_genericos';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function curso()
    {
        return $this->belongsTo('\VCN\Models\Cursos\Curso', 'course_id');
    }

    public function generico()
    {
        return $this->belongsTo('\VCN\Models\Extras\Extra', 'generics_id');
    }

    public function getNameAttribute()
    {
        return $this->generico->generic_name;
    }

    public function getPrecioAttribute()
    {
        return $this->generico->generic_price;
    }

    public function getMonedaAttribute()
    {
        return Moneda::find($this->generico->generic_currency_id)->currency_name;
    }

    public function getMonedaIdAttribute()
    {
        return $this->generico->generic_currency_id;
    }

    public function getMonedaNameAttribute()
    {
        return $this->moneda;
    }

    public function getRequeridoAttribute()
    {
        return $this->generico->generic_required;
    }
    public function getRequiredAttribute()
    {
        return $this->generico->generic_required;
    }

    public function getTipoUnidadAttribute()
    {
        return $this->generico->generic_unit;
    }

    public function getTipoUnidadNameAttribute()
    {
        return ConfigHelper::getTipoUnidad($this->generico->generic_unit);
    }

    public function getUnidadNameAttribute()
    {
        return $this->generico->generic_unit_id?ExtraUnidad::find($this->generico->generic_unit_id)->name:"-";
    }

    public function getUnidadIdAttribute()
    {
        return $this->generico->generic_unit_id;
    }
}
