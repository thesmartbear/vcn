<?php

namespace VCN\Models;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    protected $table = 'provincias';

    // protected $fillable = [];
    protected $guarded = ['_token'];

    public function pais()
    {
        return $this->belongsTo('\VCN\Models\Pais', 'pais_id');
    }

    public function ciudades()
    {
        return $this->hasMany('\VCN\Models\Ciudad', 'provincia_id');
    }

    public function oficina()
    {
        return $this->belongsTo('\VCN\Models\System\Oficina', 'oficina_id');
    }
}
