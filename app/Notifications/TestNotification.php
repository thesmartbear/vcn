<?php

namespace VCN\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;

use Carbon;

class TestNotification extends Notification
{
    use Queueable;

    protected $titulo;
    protected $mensaje;
    protected $action;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($titulo=null, $mensaje=null, $action=null)
    {
        $this->titulo = $titulo;
        $this->mensaje = $mensaje;
        $this->action = $action;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        // return ['mail'];
        return ['database', 'broadcast', WebPushChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $t = config('app.name') ." ". ($this->titulo ?? '');
        $m = $this->mensaje ?? 'WebPush';
        $action = $this->action ?? config('app.url');

        return [
            'title' => $t,
            'body' => $m,
            'action_url' => $action,
            'created' => Carbon::now()->toIso8601String()
        ];
    }

    public function toWebPush($notifiable, $notification)
    {
        $t = config('app.name') ." ". ($this->titulo ?? '');
        $m = $this->mensaje ?? 'WebPush';
        $action = $this->action ?? config('app.url');

        return (new WebPushMessage)
            ->title($t)
            ->icon('/images/webpush.jpg')
            ->body($m)
            ->action('Notificación', $action)
            ->data(['id' => $notification->id]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMailOFF($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArrayOFF($notifiable)
    {
        return [
            //
        ];
    }
}
