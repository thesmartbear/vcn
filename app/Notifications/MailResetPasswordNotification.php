<?php

namespace VCN\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use VCN\Helpers\ConfigHelper;
use VCN\Helpers\MailHelper;

class MailResetPasswordNotification extends Notification
{
    use Queueable;

    protected $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // $link = url( "/auth/password/reset/" . $this->token );
        // $link = url( config('app.url') . route('password.reset', ['token'=> $this->token], false));
        $link = route('password.reset', ['token' => $this->token]);

        $template = "system.password";
        $ficha = \VCN\Models\System\SystemMail::where('name', $template)->first();

        $data = [];
        $m = MailHelper::enviarCompile($ficha, $data);

        config(['app.name' => ConfigHelper::plataformaApp()]);
        config(['app.url' => ConfigHelper::config('web')]);

        $txt1 = "Si no puede hacer clic en este enlace, simplemente cópielo y péguelo en su navegador web:<br>";
        $txt2 = "<br><br>Si no ha solicitado restablecer la constraseña, puede hacer caso omiso de este mensaje.";

        $p = ConfigHelper::config('propietario');
        $plataforma = \VCN\Models\System\Plataforma::find($p);

        return (new MailMessage)
            // ->view('reset.emailer')
            ->from( $plataforma->email, $plataforma->name)
            ->subject($ficha->asunto)
            ->greeting('¡Hola!')
            ->line($data['body'])
            ->action('Restablecer', $link)
            ->line($txt1)
            ->line($link)
            ->line($txt2)

            ->salutation('¡Gracias!');

        /* return (new MailMessage)
           ->subject(Lang::getFromJson('Reset Password Notification'))
           ->line(Lang::getFromJson('You are receiving this email because we received a password reset request for your account.'))
           ->action(Lang::getFromJson('Reset Password'), $link)
           ->line(Lang::getFromJson('If you did not request a password reset, no further action is required.')); */
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
