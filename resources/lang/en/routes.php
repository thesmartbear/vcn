<?php

return [
      "about"       =>  "about",
      "view"        =>  "view/{id}", //we add a route parameter

      // other translated routes
      "pais"        => "/country/{pais?}",
      "pais_curso"  => "/country/{pais?}/{course_slug?}.html",

      "buscar"        => "/search",
    ];