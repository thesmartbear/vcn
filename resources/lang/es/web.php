<?php

return array (

    'gracias' => "Gracias",

    'chatweb' => array(
        'formulario' => "No hay agentes disponibles. Puede dejarnos un mensaje",
        'gracias' => '<strong>Tu mensaje ha sido enviado.</strong><br>En breve nos pondremos en contacto contigo.<br>Gracias por confiar en nosotros.',
        'nombre'=> 'Nombre',
        'email'=> 'Email',
        'enviar'=> 'Enviar'
    ),

    'home' => array(
        'menu' => 'menú',
    ),

    'test' =>
        array (
        'test1' => 'Texto normal',
        'test2' => 'Texto con <strong>negrita</strong> y saltos de línea <br>salto1 <br><br> salto doble',
        'test3' => 'Test con texto y lista:<br>
            <ul>
            <li>uno</li>
            <li>dos</li>
            </ul>',
    ),
    'ups' => '¡Ups!',
    'error404' => 'Parece que hay un problema con esta página.<br />Inténtalo más tarde por favor.',
    'error404rich' => array(
        'title' => 'Parece que no podemos encontrar la página que estás buscando',
        'code' => 'Código de error: 404',
        'links' => 'A lo mejor alguno de estos enlaces te puede ayudar:',
    ),
    'error500' => '',
    'error419' => 'Página expirada',
    'about' => 'Acerca de',
    'quienes-somos' => 'Quiénes somos',
    'que-ofrecemos' => 'Qué ofrecemos',
    'blog' => 'Blog',
    'partners' => 'Colaboradores',
    'inscripcion' => 'Hojas de inscripción',
    'inscripcion-slug' => 'inscripcion.html',
    'inscripcion-txt-cic' => '<p>Toda solicitud de inscripción en cualquiera de nuestros programas deberá ir acompañada de un depósito del importe indicado en cada hoja de inscripción más el importe del seguro de cancelación, en caso de contratarse. El abono de esta cantidad, descontando el seguro de cancelación si se ha contratado, se considera en concepto de pago a cuenta de los gastos iniciales de tramitación, y se puede realizar mediante tarjeta de crédito o transferencia bancaria, haciendo constar el nombre del participante y el programa.</p>
                       <p>Envíenos el comprobante de la transferencia junto con la hoja de inscripción por correo electrónico (<a href="mailto:viatges@iccic.edu">viatges@iccic.edu</a>) o fax. </p>

                        <h4>N&uacute;mero de cuenta</h4>
                          <p>
                              <strong>Extranjero (J&oacute;venes y Adultos)</strong><br />
								BBVA. IBAN: ES45 0182 6035 46 0201504839
                       </p>
                       <p>
                              <strong>Summer Camps (colonias)</strong><br />
								BBVA. IBAN: ES64 0182 6035 49 0208000828
                       </p>',
    'inscripcion-txt-bs' => '<p>Las inscripciones se considera v&aacute;lida &uacute;nicamente al recibirlas juntamente con el pago indicado en cada una de ellas, seg&uacute;n la modalidad del curso, m&aacute;s el importe del seguro de cancelaci&oacute;n en caso de contratarlo, como dep&oacute;sito a nombre de British Summer Experiences S.L.</p>
              			<p>El hecho de enviar y de tomar parte en uno de estos cursos implica la total aceptaci&oacute;n por parte del firmante de las condiciones generales descritas en la hoja de inscripci&oacute;n.</p>
              			<p>Rellena la hoja de inscripci&oacute;n y env&iacute;anosla por:<br />
                        - email<br />
                        - correo ordinario<br />
                        - personalmente&nbsp;en cualquiera de nuestas oficinas.<br /><br /></p>

                        <h4>N&uacute;meros de cuenta</h4>
                          <p>
                              <strong>Barcelona:</strong> LA CAIXA. IBAN ES70 2100 7332 0902 0014 7281<br />
                                <strong>Girona:</strong> LA CAIXA. IBAN ES25 2100 7332 0002 0015 4255<br />
                                <strong>Madrid: </strong>BBVA. IBAN ES26 0182 4853 5102 0156 8789<br />
                                <strong>Andorra:</strong> MORA BANC. IBAN AD07 0004 0019 0001 3016 9014
                       </p>                ',
    'buscar-slug' => 'buscar.html',
    'buscar' => 'Buscar...',
    'hasbuscado' => 'Has buscado:',
    'cursos-en-promocion' => 'cursos-en-promocion.html',
    'cursos-en-promocion-titulo' => 'Cursos en promoción',
    'financiacion' => '<b>Financiación sin intereses<sup>1</sup>.</b><br />Entre 600 € y 5.000 € a 3, 6 meses directamente en nuestras oficinas.<br />Crédito por medio de entidad bancaria.<br /><small><sup>1</sup> aplicables descuentos ni precio Early Bird</small>',
    'solicitainfo' => 'Solicita información',
    'chat' => 'Chat',
    'catalogo-slug' => 'catalogo.html',
    'catalogo' => 'catálogo',
    'aclientes' => 'Área de Clientes',
    'login' => 'Login',
    'admin' => 'admin',
    'salir' => 'salir',
    'volver' => 'Volver',
    'masinfo' => 'Más info',
    'trabajo' => 'trabaja con nosotros',
    'contacto-slug' => 'contacto.html',
    'contacto' => 'Contacto',
    'oficinas' => 'Oficinas',
    'oficina' => 'Oficina',
    'horario' => 'Horario',
    'nombre' => 'Nombre',
    'telefono' => 'Teléfono',
    'movil' => 'Movil|Celular (incluir lada)',
    'email' => 'Email',
    'mensaje' => 'Mensaje',
    'obligatorio' => 'Campos obligatorios',
    'enviarmensaje' => 'Enviar mensaje',
    'mensajegracias' => '<strong>Tu mensaje ha sido enviado.</strong><br>En breve nos pondremos en contacto contigo.<br>Gracias por confiar en nosotros.',
    'aviso-legal' => 'Aviso legal',
    'politica-de-cookies' => 'Política de cookies',
    'trabaja-con-nosotros' => 'Trabaja con nosotros',
    'edad' => 'edad',
    'todos' => 'todos',
    'todas' => 'todas',
    'alojamiento' => 'alojamiento',
    'pais' => 'país',
    'ciudad' => 'Ciudad',
    'especialidad' => 'especialidad',
    'idioma' => 'idioma',
    'sinresultados' => 'Ningún programa coicide con tu búsqueda',
    'subcategorias' => 'subcategorias',

    'Calle' => 'Calle',
    'Avenida' => 'Avenida',
    'Vía' => 'Vía',
    'Paseo' => 'Paseo',
    'Pasaje' => 'Pasaje',
    'Plaza' => 'Plaza',
    'Vuelo' => 'Vuelo',
    'Autobús' => 'Autobús',
    'Barco' => 'Barco',
    'Tren' => 'Tren',
    'Inglés' => 'Inglés',
    'Español' => 'Español',
    'Francés' => 'Francés',
    'Alemán' => 'Alemán',
    'Chino' => 'Chino',
    'Italiano' => 'Italiano',
    'Principiante' => 'Principiante',
    'Pre-intermedio' => 'Pre-intermedio',
    'Intermedio' => 'Intermedio',
    'Intermedio Alto' => 'Intermedio Alto',
    'Avanzado' => 'Avanzado',
    'grupocerrado' => 'GRUPO CERRADO',
    'ultimasplazas' => 'ÚLTIMAS PLAZAS',
    'plazasdisponiblesnum' => '{1} plaza disponible|[2,Inf]plazas disponibles',
    'plazasdisponibles' => 'plazas disponibles',
    'whatsapp' => 'No se atienden llamadas, solamente mensajes.',
    'lopd' => "Protección de Datos de Carácter Personal",

    'catalogo_info' => [
        'titulo'=> "Solicitar información",
        'texto' => "Facilítanos los siguientes datos y accederás a nuestro catálogo digital.",
        'nombre'=> "Nombre",
        'telefono'=> "Teléfono",
        'email'=> "E-mail",
    ],

    'seo-bs' =>
        array(
            'keywords' => 'british, summer, ocio, max camps, singulares, experiencias, estudiar fuera, cursos, idiomas, extranjero, jóvenes, adultos, familia, programas, aprender, estudiar, verano, monitor, grupo, colonias, campamentos, camps, año escolar, año académico, trimestre, plan b',
            'description' => 'Cursos de idiomas en el extranjero para jóvenes, adultos y toda la familia. Campamentos en inglés, Año Escolar y cursos especializados.',
            'subject' => 'Experiencias lingüísticas',
        ),
    'seo-cic' =>
        array(
            'keywords' => 'cic, ICCIC, ocio, singulares, experiencias, estudiar fuera, cursos, idiomas, extranjero, jóvenes, adultos, familia, programas, aprender, estudiar, verano, monitor, grupo, colonias, campamentos, camps, año escolar, año académico, trimestre, plan b',
            'description' => 'Cursos de idiomas en el extranjero para jóvenes, adultos y toda la familia. Campamentos en inglés, Año Escolar y cursos especializados.',
            'subject' => 'Experiencias lingüísticas',
        ),
    'seo-ps' =>
        array(
            'keywords' => 'pepperscope, ocio, estudiar fuera, cursos, idiomas, extranjero, jóvenes, adultos, familia, programas, aprender, estudiar, verano, monitor, grupo, campamentos, camps',
            'description' => 'Bienvenid@ a Pepperscope, la web de búsqueda de programas en el extranjero para gente inquieta y diferente. Enjoy!',
            'subject' => 'Programas diferentes en el extranjero',
        ),
    'seo-sf' =>
        array(
            'keywords' => 'studyfuera, ocio, estudiar fuera, cursos, idiomas, extranjero, jóvenes, adultos, familia, programas, aprender, estudiar, verano, monitor, grupo, campamentos, camps',
            'description' => 'Somos una agencia mexicana de estudios en el extranjero. En Studyfuera ofrecemos High School, Summer Camps, Licenciaturas, Inglés, Francés, Alemán...',
            'subject' => 'Estudios en el extranjero con un Plus',
        ),
    'seo-dp' =>
        array(
            'keywords' => 'dippy, life, dippy.life, ocio, experiencias, activiades, mundo, viajar, estudiar fuera, cursos, idiomas, extranjero, jóvenes, adultos, programas, aprender, estudiar, verano, monitor, grupo, campamentos, camps',
            'description' => 'Inmersión lingüística total practicando lo que más te gusta',
            'subject' => 'Inmersión lingüística total practicando lo que más te gusta',
        ),

    'formulario' => [
        'titulo' => '¿Te interesa este programa pero tienes preguntas?',
        'subtitulo' => 'Rellena este formulario y te ayudaremos.',
        'pregunta'=> '¿Eres el que va a viajar?',
        'gracias' => "<strong>Gracias por enviarnos tus datos. <br> Te contactaremos enseguida</strong><br><br>",
        'fechanac' => "Fecha de nacimiento del participante",
        'lopd'=> "Sí, autorizo el uso de mis datos personales de acuerdo con la Política de Privacidad, y me gustaría recibir las últimas noticias, ofertas e información de los programas de :plataforma por correo electrónico o a través del teléfono móvil.",
        'lopd1' => "Acepto la <a href='/proteccion-de-datos.html'>política de privacidad</a>",
        'lopd2' => 'Acepto recibir información sobre los programas de :plataforma',
        
        'error' => [
            'email' => "el formato del email es incorrecto",
        ],
    ],

    'masinfobs' =>
        array(
            'plusinfotext' => 'Si lo prefieres<br/>NOSOTROS TE LLAMAMOS',
            'titulo' => "LLÁMANOS AL <b>93 200 88 88</b> O AL <b>91 345 95 65</b><br />O DÉJANOS UN TELÉFONO DE CONTACTO",
            'txt' => "TE AYUDAREMOS A TOMAR LA MEJOR DECISIÓN",
            'nombre' => 'nombre',
            'nombrecampo' => 'nombre',
            'telefono' => 'teléfono',
            'telefonocampo' => 'teléfono de contacto',
            'email' => 'email',
            'emailcampo' => 'email de contacto',
            'enviar' => 'enviar',
            'protecciondatos' => 'De acuerdo con lo establecido en la Ley Orgánica 15/1999, de Protección de Datos de Carácter Personal, le informamos de que los datos personales que nos facilite a través del presente formulario, se incorporarán a los ficheros de BRITISH SUMMER EXPERIENCES S.L. con la única finalidad de contactar con Ud tal y como nos solicita. Podrá acceder, rectificar y cancelar sus datos, así como oponerse al tratamiento de los mismos, dirigiéndose por escrito y adjuntando copia del DNI a BRITISH SUMMER EXPERIENCES S.L., Via Augusta 33, Entlo 2ª, 08006 Barcelona.',
        ),
    'masinfocic' =>
        array(
            'plusinfotext' => 'Si lo prefieres<br/>NOSOTROS TE LLAMAMOS',
            'titulo' => "LLÁMANOS AL 93 200 11 33<br />O DÉJANOS UN TELÉFONO DE CONTACTO",
            'txt' => "TE AYUDAREMOS A TOMAR LA MEJOR DECISIÓN",
            'nombre' => 'nombre',
            'nombrecampo' => 'nombre',
            'telefono' => 'teléfono',
            'telefonocampo' => 'teléfono de contacto',
            'email' => 'email',
            'emailcampo' => 'email de contacto',
            'enviar' => 'enviar',
            'protecciondatos' => 'De acuerdo con lo establecido en la Ley Orgánica 15/1999, de Protección de Datos de Carácter Personal, le informamos de que los datos personales que nos facilite a través del presente formulario, se incorporarán a los ficheros de la Institució Cultural del CIC con la única finalidad de contactar con Ud tal y como nos solicita. Podrá acceder, rectificar y cancelar sus datos, así como oponerse al tratamiento de los mismos, dirigiéndose por escrito y adjuntando copia del DNI a la Institució Cultural del CIC, Via Augusta 205, 08021 Barcelona.',
        ),
    'masinfosf' =>
        array(
            'plusinfotext' => 'Si lo prefieres<br/>NOSOTROS TE LLAMAMOS',
            // 'titulo' => "LLÁMANOS AL (55) 6830 8285<br />O DÉJANOS UN TELÉFONO DE CONTACTO",
            'titulo' => "Llama o Whatsapp al 55 4538 2090",
            'txt' => "TE AYUDAREMOS A TOMAR LA MEJOR DECISIÓN",
            'nombre' => 'nombre',
            'nombrecampo' => 'nombre',
            'telefono' => 'teléfono',
            'telefonocampo' => 'teléfono de contacto',
            'email' => 'email',
            'emailcampo' => 'email de contacto',
            'enviar' => 'enviar',
            'protecciondatos' => 'Aviso de privacidad',
        ),
    'masinfodp' =>
        array(
            'plusinfotext' => '¿Tienes dudas?',
            'titulo' => "DÉJANOS UN TELÉFONO DE CONTACTO Y NOSOTROS TE LLAMAMOS",
            'txt' => "TE AYUDAREMOS A TOMAR LA MEJOR DECISIÓN",
            'nombre' => 'nombre',
            'nombrecampo' => 'nombre',
            'telefono' => 'teléfono',
            'telefonocampo' => 'teléfono de contacto',
            'email' => 'email',
            'emailcampo' => 'email de contacto',
            'enviar' => 'enviar',
            'protecciondatos' => '',
        ),
    'idiomas' => array(
        'Inglés' => 'Inglés',
        'Español' => 'Español',
        'Francés' => 'Francés',
        'Alemán' => 'Alemán',
        'Chino' => 'Chino',
        'Italiano' => 'Italiano',
        'Portugués' => 'Portugés',
        'Japonés' => 'Japonés',
    ),
    'curso' =>
        array (
            'programa' => 'Programa',
            'idiomarequerido' => 'Idioma requerido',
            'sesiones' => 'Número de sesiones de idioma',
            'requisitos' => 'Requisitos especiales',
            'nivel' => 'Nivel requerido',
            'centro' => 'Centro',
            'alojamiento' => 'Alojamiento',
            'actividades' => 'Actividades',
            'excursiones' => 'Excursiones',
            'transporte' => 'Transporte',
            'instalaciones' => 'Instalaciones',
            'comidas' => 'Comidas',
            'monitor' => 'Monitor',
            'coordinador-tab' => 'coordinacion',
            'coordinador' => 'Coordinación',
            'fechas-precios' => 'fechas-precios',
            'fechasyprecios' => 'Fechas y Precios',
            'fotos' => 'Fotos',
            'videos' => 'Vídeos',
            'fotosvideo' => 'Fotos y Vídeos',
            'fotos-videos' => 'fotos-videos',
            'info-basica' => 'Info básica',
            'internet-disponible' => 'Internet disponible',
            'inscripcion' => 'Inscripción',
            'catalogo' => 'Catálogo',
            'faq' => 'F.A.Q.',
            'descargar' => 'Descargar',
            'solicitarplaza' => 'Más información',
            'comprar' => 'Inscripción',
            'comprar-corporativo' => 'Inscripción Curso corporativo, bla, bla, bla',
            'deseo' => 'Favorito',
            'reservar' => 'Reservar',
            'timetable' => 'Timetable',
            'masinfoproveedor1' => 'Más información en la',
            'masinfoproveedor2' => 'web de nuestro proveedor.',
            'incluido' => 'Incluido en el precio',
            'precioincluye' => 'Qué incluye el precio',
            'incluyeclasesidioma' => 'Horario clases de idioma:',
            'al' => 'al',
            'entre' => 'entre el',
            'yel' => 'y el',
            'planb' => 'Plan B',
            'planbfrase' => 'Si no te van bien las fechas de grupo puedes irte en otras fechas de forma individual…',
            'extras' => 'Extras',
            'curso' => 'Duración del curso',
            'finicio' => 'Fecha inicio',
            'ffin' => 'Fecha fin',
            'duracion' => 'Duración',
            'precio' => 'Precio',

            'Días' => 'Día|Días',
            'Semana' => 'Semana',
            'semana' => 'Semana',
            'Semanas' => 'Semana|Semanas',
            'Meses' => 'Mes|Meses',
            'Trimestres' => 'Trimestre|Trimestres',
            'Semestres' => 'Semestre|Semestres',
            'Años' => 'Años',
            'Trayecto' => 'Trayecto',
            
            'Excursión' => 'Excursión',
            'Noche' => 'Noche',
            'Clase' => 'Clase',
            'Per afternoon' => 'Per afternoon',
            'Por 2 semanas' => 'Por 2 semanas',
            'Term' => 'Term',

            'presupuesto' => 'Pida presupuesto personalizado',
            'subtotal' => 'Subtotal',
            'total' => 'Total',
            'alojamientoextrasobligatorios' => 'Extras obligatorios alojamiento',
            'extrasobligatorios' => 'Extras obligatorios',
            'divisas' => 'Cambio aplicado',
            'selecciona' => 'Selecciona la duración del curso y alojamiento para ver el total',
        ),
    'categorias' => array(
            'jovenes' => 'Jóvenes',
            'adultos' => 'Adultos',
            'aescolar' => 'Año Escolar',
            'camps' => 'Campamentos',
            'colonias' => 'Colonias',
            'verprogramas' => 'VER PROGRAMAS',
            'vertodos' => 'Ver todos',
            'ver' => 'ver',
            'eligecurso' => 'Elige tu curso y empieza una nueva experiencia',
            'buscar' => 'Buscar cursos',
            'buscar-resultados' => 'Has buscado:',
            'ingles-slug' => 'aprender-ingles-en-el-extranjero',
            'ingles' => 'Inglés en el extranjero',
            'ingles-desc' => 'Todas las edades',
            'ingles-intro' => '<p>Para que nos vamos a engañar, el inglés es el idioma que todo el mundo quiere aprender y por supuesto saber. Por eso en esta sección sólo hay cursos de inglés, para que nadie se pierda en la búsqueda y encuentre su curso idóneo.</p><p>Inglés para niños, inglés para adolescentes, inglés para adultos, inglés para familias enteras, inglés para profesionales desesperados o no tanto. “English for everybody and everywhere”.</p>',
            'idiomas-slug' => 'aprender-otros-idiomas-en-el-extranjero',
            'idiomas' => 'Otros idiomas en el extranjero',
            'idiomas-desc' => 'Alemán, Chino, Francés, Italiano, Japonés, Ruso...',
            'idiomas-intro' => '<p>No sólo de inglés vive el hombre. ¡Imagínate en un mercado de Shanghái hablado sobre la textura de los dim-sum con su cocinero! O negociando en francés para tu negocio de moda. O debatiendo en alemán sobre la nueva arquitectura de la ciudad de Berlín.</p><p>Chino, japonés, francés, alemán, ruso, italiano, árabe, portugués... en la variedad está el buen gusto ¿No es así?</p>',
            'jovenes-desc' => 'de 8 a 18 años',
            'jovenes-intro' => '',
            'adultos-profesionales-desc' => 'de 18 a 99 años',
            'adultos-profesionales-intro' => '',
            'toda-la-familia-desc' => 'Programas para toda la familia',
            'toda-la-familia-intro' => '',
            'curso-escolar-en-el-extranjero-desc' => 'Primaria, secundaria y bachillerato',
            'curso-escolar-desc' => 'desde 1984',
            'curso-escolar-en-el-extranjero-intro' => '<p>Cuando una familia decide que su hijo o hija realice un curso de secundaria o bachillerato en algún país del mundo distinto al suyo, quiere saber que durante todo ese proceso contarán con el mejor equipo de profesionales.</p><p>Desde luego un año escolar en el extranjero es la apuesta más relevante que se puede hacer a nivel formativo para un adolescente pero también la de mayor impacto a nivel de aprendizaje vital, lingüístico y académico.</p><p>La decisión de realizar un año escolar en un país extranjero es, sin duda, la más relevante que una familia puede tomar en relación a la vida académica-personal de un hijo o hija.</p><p>Y en ese camino, que se inicia con las preguntas a conocidos que han realizado un programa parecido, para seguir con la comparación entre diversas empresas del sector, es necesario decidirse por los mejores expertos y tener la tranquilidad de que durante todo el proceso estaremos asistidos por personas que escuchan nuestras solicitudes y dudas de la forma más personal y a la vez las resuelven de la forma más profesional.</p>',
            'curso-escolar-en-el-extranjero-sidebar' => '<h5 class="head">Nuestros 10 puntos fuertes<span></span></h5>
                                <h5><small>¿Por qué escoger a <strong>'.ConfigHelper::config('nombre').'</strong>?</small></h5>
                                <ol>
                                  <li>Número reducido de alumnos, lo que nos permite atender las
                                    necesidades individuales que se plantean.</li>
                                  <li>Entrevistas personales con los padres y los participantes conjuntamente y por separado.</li>
                                  <li>El mejor seguro médico de accidentes y responsabilidad civil.</li>
                                  <li>Test de nivel de idioma.</li>
                                  <li> Asesoramiento sobre el programa más indicado.</li>
                                  <li>Organización antes de la salida de un seminario de orientación y preparación para los estudiantes y sus familias.</li>
                                  <li>Seguimiento minucioso de la adaptación y resultado de cada estudiante.</li>
                                  <li>Gestión completa de la convalidación.</li>
                                  <li>Servicio de atención 24 horas.</li>
                                  <li>Y como siempre, la garantía del organizador de cursos de idiomas en el extranjero con más prestigio del país.</li>
                                </ol>',
            'nuestrasclases' => 'Nuestras colonias',
            'campamentos-verano-ingles-desc' => '<b>Max Camps</b>. De 7 a 17 años',
            'campamentos-verano-ingles-intro' => "<div class='row'><video style='border: 1px solid #E5E5E5; margin-bottom: 30px;display: block;' class='col-sm-8' controls='' preload='none' poster='/assets/britishsummer/home/maxcamps2015.jpg'>
<source src='/assets/britishsummer/home/maxcamps2015.mp4' type='video/mp4'>
  </video></div>
                        <h2 id='clases' class='anchor'>Las clases</h2>
                    	<p>Distinguimos entre los participantes más jóvenes (entre los 7 y los 10/11 años) y los mayores (10/11 a 17) ya que las dinámicas de aprendizaje son muy diferentes.</p>
                        <p><strong>Ratio media por clase 1/12.</strong></p>
                        <p><strong>Profesores nativos y titulados.</strong></p>
                        <p><strong>Niveles:</strong> 6 niveles diferentes entre elemental y avanzado (pre-advanced).</p>
                        <p><strong>Ejes temáticos:</strong> Las clases tendrán un eje vertebrador semanal: naturaleza y medio ambiente; multiculturalidad; innovación y tecnología; arte, cultura y entretenimiento.</p>
                        <h4 class='anchor' id='sesiones'>SESIONES</h4>
                        <p><strong>Organización de las tres sesiones diarias de clase.<br>
                        15 sesiones semanales. 1 sesión = 50 minutos</strong></p>
                        <ul>
                          <li>1ª sesión 10h o 16h. CLASSWORK: Lengua y vocabulario.</li>
                          <li>2ª sesión 11h o 17h. ACTIVE PRACTICE: Práctica del idioma a través de juegos, teatro, experimentos, trabajos manuales, role-playing…</li>
                          <li>3ª sesión 12h o 18h. SHOWTIME: Los estudiantes trabajan tutorizados por un profesor/a en un proyecto común: canciones, programa de radio, anuncios, etc. En esta sesión se mezclan estudiantes de diferentes niveles.</li>
                        </ul>
                       <p>Los participantes que realicen el Trinity Exam sustituirán cuatro días de esta tercera sesión para una preparación específica de este examen.</p>

                       <h2 class='anchor' id='vintageday'>Vintage Day</h2>
                       <p>La tecnología gana terreno día a día: Ipad, iPhone, IMac, tablet, Playstation, Kindle... ¡y nos encanta!, pero durante un día queremos recuperar algunas de aquellas actividades que perduran en el tiempo, de las que hemos disfrutado tantas veces y que son tan divertidas.</p>

                        <h2 class='anchor' id='nightactivity'>Performance &amp; night activity</h2>
                       <p>¡La noche es nuestra! Las Night Activity son actividades que organizamos siempre después de cenar y que tienen como objetivo fomentar la participación y cohesión de los grupos utilizando el inglés de una manera divertida y relajada.</p>
                    	<h2 class='anchor' id='pooltime'>Pool time</h2>
                       <p>Cada día, organizados por grupos, los participantes podrán disfrutar de la magnífica piscina de que dispone el Vilar Rural de Cardona y los apartamentos de Asturias y Andalucía.</p>

                       <h2 class='anchor' id='trinityexam'>Trinity Exam<br><small>(sólo Vilar Rural de Cardona)</small></h2>
                       <p>Los participantes que hagan un mínimo de dos semanas de campamento con inicio el primero turno o el tercero, tendrán incluido el examen del Trinity College de Londres, siempre y cuando haya más de veinte participantes inscritos.</p>
                       <p><strong>Está dividido en 12 niveles:</strong></p>
                       <ul>
                         <li>Inicial – Niveles 1 a 3</li>
                         <li>Elemental– Niveles 4 a 6</li>
                         <li>Intermedio – Niveles 7 a 9</li>
                         <li>Avanzado – Niveles 10 a 12</li>
                       </ul>
                       <p>El Examen GESE del Trinity College evalúa la capacidad de escuchar y hablar de los estudiantes, y hace mayor hincapié en la habilidad práctica en el uso de la lengua que en el conocimiento teórico.</p>
                       <p>Para realizar los exámenes GESE del Trinity los examinadores viajan desde el Reino Unido hasta nuestros campamentos. Estos examinadores tienen una alta preparación y mucha experiencia.</p>
                       ",
        'campamentos-verano-ingles-sidebar' => "
            <ul>
                <li><a href='#clases'>Las Clases</a></li>
                <li><a href='#sesiones'>Sesiones</a></li>
                <li><a href='#vintageday'>Vintage Day</a></li>
                <li><a href='#nightactivity'>Performance & night activity</a></li>
                <li><a href='#pooltime'>Pool time</a></li>
                <li><a href='#trinityexam'>Trinity Exam</a></li>
            </ul>",
        'summer-camps-ingles-desc' => 'De 6 a 15 años',
        'summer-camps-ingles-intro' => "
                <div class='row'>
                	<div class='col-sm-2 col-xs-2'>
                		<ul class='colonies' id='colonies'>
                        	<li><span>C</span>oneix</li>
                            <li><span>O</span>bre't</li>
                            <li><span>L</span>lança't</li>
                            <li><span>O</span>bserva</li>
                            <li><span>N</span>odreix-te</li>
                            <li><span>I</span>l·lusiona't</li>
                            <li><span>E</span>xperimenta</li>
                            <li><span>S</span>orprèn-te</li>
                        </ul>
                    </div>
                    <div class='col-sm-9 col-sm-offset-1 col-xs-9 col-sx-offset-1'>


                		<h2>Què és una colònia CIC a Catalunya?</h2>
                    	<h4>Nuestras colonias, con la experiencia acumulada de más de 30 años, forman parte del proyecto pedagógico de CIC Escuela de idiomas, garantizando una buena integración del rigor académico en la vertiente más lúdica de aprender inglés.
</h4>

                       <h4 class='addmargintop30'><span>1.</span> PREPARACIÓN PREVIA</h4>
                       <ul>
                           <li><strong>Proyecto académico diseñado para CIC Escuela de Idiomas:</strong> programación y material.</li>
                           <li><strong>Planificación de las actividades deportivas y de ocio</strong> con la ayuda de especialistas en el sector.</li>
                           <li><strong>Grupos reducidos</strong> distribuidos por edades.</li>
                           <li><strong>Prueba de nivel</strong> (a partir de 5º EP).</li>
                           <li><strong>Asesoría individualizada.</strong></li>
                           <li><strong>Reunión informativa para las familias.</strong></li>
                       </ul>

                    	<h4 class='addmargintop30'><span>2.</span> DURANTE LA ESTANCIA</h4>
                       <ul>
                           <li><strong>Inmersión en el inglés desde el minuto cero</strong>, trabajando: reading, speaking, listening y writing.</li>
                           <li><strong>Proporción alta de monitores para participantes</strong>, garantizando una atención cercana y personalizada.</li>
                           <li><strong>Actividades organizadas por grupos de edad, habilidades y afinidades.</strong></li>
                           <li><strong>Trabajo por proyectos</strong>, centro de interés y / o preparación para el Trinity College Exam. Siempre de manera dinámica y lúdica.</li>
                           <li><strong>Dietas equilibradas.</strong> Cocina de elaboración propia.</li>
                           <li><strong>Menús especiales</strong> para alergias e intolerancias alimentarias.</li>
                           <li><strong>Contacto diario con los coordinadores</strong> y, en caso necesario, con las familias.</li>
                           <li><strong>Blog online</strong> para cada colonia, actualizado con comentarios y fotografías.</li>
                           <li><strong>Energía de los equipos al 110%</strong></li>
                       </ul>


                       <h4 class='addmargintop30'><span>3.</span> DESPUÉS DE LA ESTANCIA</h4>
                       <ul>
                           <li><strong>Certificado de participación.</strong></li>
                           <li><strong>Informe</strong> de la actitud y las aptitudes del participante.</li>
                           <li><strong>Servicio de seguimiento</strong> del inglés por parte de CIC Escuela de Idiomas.</li>
                           <li><strong>Encuestas</strong> de los participantes, familias y propuestas de mejora.</li>
                           <li><strong>Evaluación</strong> de los resultados y <strong>programación</strong> de la nueva temporada.</li>
                       </ul>
                    </div>
                </div>",
        'colectivos-y-escuelas-desc' => '',
        'colectivos-y-escuelas-intro' => 'Cada vez más las escuelas se plantean aprovechar una semana de su calendario escolar o algún paréntesis del mismo, para integrarse en la cultura de un país y practicar el idioma que llevan tiempo estudiando. En British Summer ofrecemos gran variedad de programas que permiten realizar unas clases del idioma elegido en distintos niveles y con participantes internacionales, alojarse en familia o apartamentos y disponer de tiempo libre para compartir en conjunto los increíbles atractivos de algunas de las ciudades europeas más cosmopolitas.',
    ),
    'landedslogan' => 'El blog del verano',
    'academicopromo' => '<div class="sloganitem"><h2>-400€ en Año escolar en USA J1</h2><a class="btn btn-white btn-outlined btn-promo" href="curso-escolar-en-el-extranjero/ano-escolar-en-usa-familia-voluntaria-j1.html">ver</a></div>
                        <div class="sloganitem"><h2>-500€ en Año escolar en Irlanda</h2><a class="btn btn-white btn-outlined btn-promo" href="curso-escolar-en-el-extranjero/ano-escolar-en-escuela-publica-irlanda.html">ver</a></div>
                        <div class="sloganitem"><h2>-300€ en Año escolar en Langley, Canadá + ¡Esquiada gratis!</h2><a class="btn btn-white btn-outlined btn-promo" href="curso-escolar-en-el-extranjero/ano-escolar-en-langley-canada.html">ver</a></div>',
);
