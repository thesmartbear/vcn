<?php

return array (
    'failed' => 'Las credenciales no coinciden con ningún usuario.',
    'throttle' => 'Demasiados intentos. Por favor inténtalo dentro de :seconds seconds.',
    'user' => 'Usuario',
    'email' => 'E-mail',
    'nombre' => 'Nombre',
    'apellidos' => 'Apellidos',
    'telefono' => 'Teléfono',
    'movil' => 'Móvil',
    'password' => 'Contraseña',
    'password_confirmation' => 'Repetir Contraseña',
    'recordarpass' => "¿Has olvidado tu contraseña?",
    'recordar' => 'Recordar',
    'entrar' => 'Entrar',
    'registrar' => 'Registrar',

    'compra-p_email' => 'Si ya has viajado con nosotros o nos has pedido información en el pasado, es importante que nos des el mismo email que en esta ocasión. Así no tendrás que volver a entrar todos tus datos :)',
    'compra-p_login_viajero' => 'GOOD, ya existe tu área personal de Viajero. Entra tu contraseña para acceder a tus datos',
    'compra-p_login_tutor' => 'GOOD, ya existe tu área personal de Tutor. Entra tu contraseña para acceder a tus datos',
    'compra-p_cuenta' => 'Haz clic aquí para crear tu cuenta online y poder seguir',
    'compra-p_activacion' => 'Para poder seguir tenenemos que activar tu cuenta online. Te enviaremos un email con tu contraseña. Por favor, ve a tu email para poder acceder a tu área de cliente',
    'compra-p_activacion_pwd' => 'Introduce la contraseña que te acabamos de enviar por email para seguir con el proceso de inscripción (si no ves ningún email nuestro en tu bandeja de entrada, mira en tus correos no deseados).',
    'compra-p_registro' =>  'Introduce los datos siguientes para crear tu cuenta',
    'compra-p_viajero' => 'Soy el/la que va a viajar',
    'compra-p_tutor' => 'No soy el/la que va a viajar (soy padre, madre, tutor,...)',
);
