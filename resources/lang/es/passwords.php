<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben tener al menos seis caracteres y coincidir con la confirmación.',
    'reset' => 'Tu contraseña ha sido restablecida',
    'sent' => 'Hemos enviado un email con un enlace para recuperar la contraseña',
    'token' => "Este enlace ya ha sido utilizado una vez. Si quiere recuperar la contraseña, tendrá que volver a pedir un nuevo enlace.",
    'user' => "No existe ningún usuario con ese e-mail.",

];
