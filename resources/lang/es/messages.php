<?php

return array (
  'about' => 'Acerca de',
  'hola' => 'Hola',
  'prueba1' => 'Marc Márquez ha emitido esta mañana un comunicado para referirse al incidente que protagonizó ayer con dos periodistas italianos cuando estos últimos se personaron en su domicilio y, según la nota de prensa, "profirieron insultos" y "acciones humillantes y ridículas hacia el piloto llegando a empujar y agredir a sus familiares". A continuación reproducimos el comunicado de forma íntegra:
<br>
"En el día de ayer se produjeron unos desafortunados acontecimientos en Cervera. Un grupo de personas se personaron en la vivienda del piloto y profirieron una serie de insultos, realizado determinadas acciones humillantes y ridículas hacia el propio piloto e incluso llegando a empujar y agredir a sus familiares más cercanos. Dada la gravedad de la acción, dichos actos han sido denunciados y seguirán el curso normal de una acción pena frente a dichas personas.',
);
