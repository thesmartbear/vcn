<?php

return array(
  'Nombre'        => 'Nombre',
  'Teléfono'      => 'Teléfono',
  'E-mail'        => 'E-mail',
  'viajero'       => '¿Eres tú el que viaja?',
  'Sí'            => 'Sí',
  'No'            => 'No',
  'Padres/Tutor'  => 'Padres/Tutor',
  'fechanac'      => "Fecha de nacimiento del/la estudiante",

  'destino'       => "¿En qué destino estás interesado/a?",
  'destino_tipo'  => "Tipo de programa",
  'destino_tiempo' => "¿Por cuánto tiempo?",
  'Solicitar'     => "Solicitar",

  'Edad'          => 'Edad',
  'Precio'        => 'Precio',
  'Descripción'   => 'Descripción',

  'porque'    => '¿Por qué <br> :plataforma?',
  'sellos'    => 'Sellos de calidad',
  'oficinas'  => 'Oficinas',

  'porque_1' => "Un equipo consolidado de expertos para atender todas las necesidades.",
  'porque_2' => "Test de nivel de idioma.",
  'porque_3' => "Seminarios de orientación y preparación antes de la salida.",
  'porque_4' => "Entrevistas personales con las familias y los participantes.",
  'porque_5' => "Consolidadas relaciones con las mejores escuelas a nivel mundial.",
  'porque_6' => "Seguimiento minucioso de la adaptación y resultado de cada estudiante.",
);
