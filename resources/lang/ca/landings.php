<?php

return array(
  'Nombre'        => 'Nom',
  'Teléfono'      => 'Telèfon',
  'E-mail'        => 'E-mail',
  'viajero'       => 'Ets tu qui viatja?',
  'Sí'            => 'Sí',
  'No'            => 'No',
  'Padres/Tutor'  => 'Pares/Tutor',
  'fechanac'      => "Data de naixement de l'alumne / a",

  'destino'       => "En quin destí estàs interessat / a?",
  'destino_tipo'  => "Tipus de programa",
  'destino_tiempo' => "Per quant de temps?",
  'Solicitar'     => "Solicitar",

  'Edad'          => 'Edat',
  'Precio'        => 'Preu',
  'Descripción'   => 'Descripció',

  'porque'    => 'Per què <br> :plataforma?',
  'sellos'    => 'Segells de qualitat',
  'oficinas'  => 'Oficines',

  'porque_1' => "Un consolidat equip d'experts per a atendre totes les necessitats.",
  'porque_2' => "Test de nivell d'idioma.",
  'porque_3' => "Seminaris d'orientació i preparació abans de la sortida.",
  'porque_4' => "Entrevistes personals amb les famílies i els participants.",
  'porque_5' => "Consolidades relacions amb les millors escoles a nivell mundial.",
  'porque_6' => "Seguiment minuciós de l'adaptació i resultat de cada estudiant.",

);
