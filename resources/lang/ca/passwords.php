<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Les contrasenyes han de tenir almenys sis caràcters i coincidir amb la confirmació.',
    'reset' => 'La contrasenya ha estat restablerta',
    'sent' => 'Enllaç de recuperació de contrasenya enviat per email',
    'token' => 'Aquest enllaç ja ha estat utilitzat una vegada. Si vol recuperar la contrasenya, haurà de tornar a demanar un nou enllaç.',
    'user' => "No existeix cap usuari amb aquest correu electrònic.",

];
