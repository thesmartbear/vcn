<?php

return [
      "about"       =>  "sobre",
      "view"        =>  "{id}", //we add a route parameter

      // other translated routes
      "pais"        => "/pais/{pais?}",
      "pais_curso"  => "/pais/{pais?}/{course_slug?}.html",

      "buscar"        => "/cercar",
    ];