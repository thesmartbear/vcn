<?php

return array (
    'failed' => 'Les credencials no coincideixen amb cap usuari.',
    'throttle' => 'Massa intents. Si us plau, intenteu dins de :seconds segons.',
    'user' => 'Usuari',
    'nombre' => 'Nom',
    'apellidos' => 'Cognoms',
    'telefono' => 'Telèfon',
    'movil' => 'Mòbil',
    'email' => 'E-mail',
    'password' => 'Contrasenya',
    'password_confirmation' => 'Repetir Contrasenya',
    'recordarpass' => "¿Has oblidat la teva contrasenya?",
    'recordar' => 'Recordar',
    'entrar' => 'Entrar',
    'registrar' => 'Registrar',

    'compra-p_email' => 'Si ja has viatjat amb nosaltres o vas demanar informació per e-mail en el passat, es important que ens facilitis el mateix email. Així no hauràs de tornar a entrar totes les teves dades :)',
    'compra-p_login_viajero' => 'GOOD, ja existeix la teva àrea personal de Viatjer. Entra amb la teva contrasenya para accedir a les teves dades',
    'compra-p_login_tutor' => 'GOOD, ja existeix la teva àrea personal de Tutor. Entra amb la teva contrasenya para accedir a les teves dades',
    'compra-p_cuenta' => 'Fes clic aquí pre crear el teu compte online i poder seguir',
    'compra-p_activacion' => "Per poder continuar hem d'activar tu cuenta online. Te enviaremos un email con tu contraseña. Por favor, ve a tu email para poder acceder a tu área de cliente",
    'compra-p_activacion_pwd' => "Introdueix la contrasenya que t'acabem d'enviar per email per seguir amb el procés d'inscripció (si no veus cap email nostre a la teva safata d'entrada, mira dins el teu correu brossa).",
    'compra-p_registro' =>  'Introdueix les següents dades per crear el teu compte',
    'compra-p_viajero' => 'Soc el/la que viatjaré',
    'compra-p_tutor' => 'No soc el/la que viatjaré (soc pare, mare, tutor,...)',
);
