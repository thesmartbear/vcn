<?php

return array (

    'gracias' => "Gràcies",
    
    'chat' => array(
        'formulario' => "No hi ha agents disponibles. Pot deixar-nos un missatge",
        'gracias' => "<strong>El teu missatge ha estat enviat.</strong><br />En breu ens posarem en contacte amb tu.<br /> Gràcies per confiar en nosaltres.",
        'nombre'=> 'Nom',
        'email'=> 'Email',
        'enviar'=> 'Enviar'
    ),

    'home' => array(
        'menu' => 'menu',
    ),

    'test' =>
        array (
            'test1' => 'Text normal',
            'test2' => 'Text amb <strong>negreta</strong> y salts de línea <br>
                        salt1 <br><br> salt doble',
            'test3' => 'Test amb text i llista:<br>
                <ul>
                <li>un</li>
                <li>dos</li>
                </ul>',
    ),
        
    'ups' => 'Ups!',
        'error404' => "Sembla ser que hi ha problema amb aquesta pàgina.<br />Proveu més tard si us plau.",
        'error404rich' => array(
            'title' => 'Sembla que no podem trobar la pàgina que estàs buscant',
            'code' => "Codi d'error: 404",
            'links' => "Potser algun d'aquests enllaços et pot ajudar:",
        ),
        'error500' => '',
        'error419' => 'Pàgina ha caducat',
        'about' => 'Sobre',
        'quienes-somos' => 'Qui som',
        'partners' => 'Col·laboradors',
        'inscripcion' => "Fulls d'inscripció",
        'inscripcion-slug' => 'inscripcio.html',
        'inscripcion-txt-cic' => "<p>Tota sol·licitud d’inscripció a qualsevol dels nostres programes haurà d’anar acompanyada d’un dipòsit de l'import indicat a cada full d'inscripció més l’import de l’assegurança de cancel·lació, en cas de contractar-la. L’abonament d’aquesta quantitat,descomptant l’assegurança de cancel·lació si s’ha contractat,es considera en concepte de pagament a compte de les despeses inicials de tramitació, i es pot realitzar mitjançant tarjeta de crèdit o transferència bancària, fent constar el nom del participant i el programa.</p>
                       <p>Feu-nos arribar el comprovant de la transferència juntament amb el full d’inscripció per correu electrònic (<a href='mailto:viatges@iccic.edu'>viatges@iccic.edu</a>) o fax. </p>

                        <h4>N&uacute;meros de compte</h4>
                          <p>
                              <strong>Estranger (Joves i Adults)</strong><br />
								BBVA. IBAN: ES45 0182 6035 46 0201504839
                       </p>
                       <p>
                              <strong>Summer Camps (colònies)</strong><br />
								BBVA. IBAN: ES64 0182 6035 49 0208000828
                       </p>",
        'inscripcion-txt-bs' => "<p>Les inscripcions es considera vàlida únicament al rebre juntament amb el pagament indicat en cadascuna d'elles, segons la modalitat del curs, més l'import de l'assegurança de cancel·lació en cas de contractar-lo, com a dipòsit a nom de British Summer Experiences S.L.</p>
              			<p>El fet d'enviar i de prendre part en un d'aquests cursos implica la total acceptació per part del signant de les condicions generals descrites al full d'inscripció.</p>
              			<p>Omple el full d'inscripció i envia'ns-la per:<br />
                        - email<br />
                        - correu ordinari<br />
                        - Personalment en qualsevol de les nostres oficines.<br /><br /></p>

                        <h4>NÚMEROS DE COMPTE</h4>
                          <p>
                              <strong>Barcelona:</strong> LA CAIXA. IBAN ES70 2100 7332 0902 0014 7281<br />
                                <strong>Girona:</strong> LA CAIXA. IBAN ES25 2100 7332 0002 0015 4255<br />
                                <strong>Madrid: </strong>BBVA. IBAN ES26 0182 4853 5102 0156 8789<br />
                                <strong>Andorra:</strong> MORA BANC. IBAN AD07 0004 0019 0001 3016 9014
                       </p>",
        'buscar-slug' => 'cercar.html',
        'buscar' => 'Cercar...',
        'hasbuscado' => 'Has cercat:',
        'cursos-en-promocion' => 'cursos-en-promocio.html',
        'cursos-en-promocion-titulo' => 'Cursos en promoció',
        'financiacion' => "<b>Finançament sense interessos<sup>1</sup></b>.<br />Entre 600€ i 5.000€ a 3 o 6 mesos directament a les nostres oficines.<br />Crèdit per mitjà d'entitat bancària.<br /><sup>1</sup>aplicables descomptes ni preu Early Bird",
        'solicitainfo' => 'Sol·licita informació',
        'chat' => 'xat',
        'catalogo-slug' => 'cataleg.html',
        'catalogo' => 'catàleg',
        'trabajo' => 'treballa amb nosaltres',
        'aclientes' => 'Àrea de Clients',
        'login' => 'Login',
        'admin' => 'admin',
        'salir' => 'sortir',
        'volver' => 'tornar',
        'masinfo' => 'Més info',
        'contacto-slug' => 'contacte.html',
        'contacto' => 'Contacte',
        'oficinas' => 'Oficines',
        'oficina' => 'Oficina',
        'horario' => 'Horari',
        'nombre' => 'Nom',
        'telefono' => 'Telèfon',
        'movil' => 'Mòbil',
        'email' => 'Email',
        'mensaje' => 'Missatge',
        'obligatorio' => 'Camps obligatoris',
        'enviarmensaje' => 'Enviar missatge',
        'mensajegracias' => "<strong>El teu missatge ha estat enviat.</strong><br />En breu ens posarem en contacte amb tu.<br /> Gràcies per confiar en nosaltres.",
        'aviso-legal' => 'Avís legal',
        'politica-de-cookies' => 'Política de cookies',
        'trabaja-con-nosotros' => 'Treballa amb nosaltres',
        'edad' => 'edat',
        'todos' => 'tots',
        'todas' => 'totes',
        'alojamiento' => 'allotjament',
        'pais' => 'país',
        'ciudad' => 'ciutat',
        'especialidad' => 'especialitat',
        'idioma' => 'idioma',
        'sinresultados' => 'Cap programa coincideix amb la teva cerca',
        'subcategorias' => 'subcategories',

    //arrays

        'Calle' => 'Carrer',
        'Avenida' => 'Avinguda',
        'Vía' => 'Via',
        'Paseo' => 'Passeig',
        'Pasaje' => 'Passatge',
        'Plaza' => 'Plaça',
        'Vuelo' => 'Vol',
        'Autobús' => 'Autobús',
        'Barco' => 'Vaixell',
        'Tren' => 'Tren',
        'Inglés' => 'Anglès',
        'Español' => 'Espanyol',
        'Francés' => 'Francès',
        'Alemán' => 'Alemany',
        'Chino' => 'Xinès',
        'Italiano' => 'Italià',
        'Principiante' => 'Principiant',
        'Pre-intermedio' => 'Pre-intermig',
        'Intermedio' => 'Intermig',
        'Intermedio Alto' => 'Intermig Alt',
        'Avanzado' => 'Avançat',
        'grupocerrado' => 'GRUP TANCAT',
        'ultimasplazas' => 'DARRERES PLACES',
        'plazasdisponiblesnum' => '{1} plaça disponible|[2,Inf]places disponibles',
        'plazasdisponibles' => 'places disponibles',
        'whatsapp' => "No s'atenen trucades, només missatges.",
        'lopd' => "Protecció de Dades de Caràcter Personal",

        'catalogo_info' => [
            'titulo'=> "Sol·licitar informació",
            'texto' => "Facilita'ns les següents dades i accediràs al nostre catàleg digital.",
            'nombre'=> "Nom",
            'telefono'=> "Telèfon",
            'email'=> "E-mail",
        ],

  'seo-bs' =>
    array(
        'keywords' => 'british, summer, oci, max camps, singulars, experiències, estudiar fora, cursos, idiomes, estanger, joves, adults, família, programes, aprendre, estudiar, estiu, monitor, grup, colònies, campaments, camps, any escolar, any académico, trimestre, pla b',
        'description' => "Cursos d'idiomes a l'estranger per a joves, adults i tota la família. Campaments en anglès, Any Escolar y cursos especialitzats.",
        'subject' => 'Experiències lingüístiques',
    ),
  'seo-cic' =>
        array(
            'keywords' => 'cic, ICCIC, oci, singulars, experiències, estudiar fora, cursos, idiomes, estanger, joves, adults, família, programes, aprendre, estudiar, estiu, monitor, grup, colònies, campaments, camps, any escolar, any académico, trimestre, pla b',
            'description' => "Cursos d'idiomes a l'estranger per a joves, adults i tota la família. Campaments en anglès, Any Escolar y cursos especialitzats.",
            'subject' => 'Experiències lingüístiques',
        ),
  'seo-ps' =>
        array(
            'keywords' => 'pepperscope, oci, estudiar fora, cursos, idiomes, estranger, joves, adults, família, programes, aprendre, estudiar, estiu, monitor, grup, campaments, camps',
            'description' => "Benvingut a Pepperscope, el web de recerca de programes a l'estranger per a gent inquieta i diferent. Enjoy!",
            'subject' => "Programes diferents a l'estranger",
        ),
    'seo-sf' =>
        array(
            'keywords' => 'studyfuera, oci, estudiar fora, cursos, idiomes, estranger, joves, adults, família, programes, aprendre, estudiar, estiu, monitor, grup, campaments, camps',
            'description' => "Som una agència mexicana d'estudis a l'estranger. En Studyfuera oferim High School, Summer Camps, llicenciatures, Anglès, Francès, Alemany...",
            'subject' => "Estudis a l'estranger amb un Plus",
        ),


    'formulario' => [
        'titulo' => "T'interessa aquest programa però tens preguntes?",
        'subtitulo' => "Omple aquest formulari i t'ajudarem.",
        'pregunta'=> 'Ets tu el que viatjaràs?',
        'gracias' => "<strong>Gràcies per enviar-nos les teves dades. <br> Et contactarem de seguida.</strong><br><br>",
        'fechanac' => "Data de naixement del participant",
        'lopd'=> "Sí, autoritzo l'ús de les meves dades personals d'acord amb la Política de Privacitat, i m'agradaria rebre les últimes notícies, ofertes i informació dels programes de :plataforma per correu electrònic o mitjançant el telèfon mòbil.",
        'lopd1' => "Accepto la <a href='/proteccion-de-datos.html'>política de privacitat</a>",
        'lopd2' => 'Accepto rebre informació sobre els programes de :plataforma',

        'error' => [
            'email' => "el format del email és incorrecte",
        ],
    ],

    'masinfobs' =>
        array(
            'plusinfotext' => 'Si ho prefereixes<br/>NOSALTRES ET TRUQUEM',
            'titulo' => "TRUCA'NS AL 93 200 88 88<br />O DEIXA'NS UN TELÈFON DE CONTACTE",
            'txt' => "T'AJUDAREM A PRENDRE LA MILLOR DECISIÓ",
            'nombre' => 'nom',
            'nombrecampo' => 'nom',
            'telefono' => 'telèfon',
            'telefonocampo' => 'telèfon de contacte',
            'email' => 'email',
            'emailcampo' => 'email de contacte',
            'enviar' => 'enviar',
            'protecciondatos' => "D'acord amb el que estableix la Llei Orgànica 15/1999, de Protecció de Dades de Caràcter Personal, l'informem que les dades personals que ens faciliti a través del present formulari, s'incorporaran als fitxers de BRITISH SUMMER EXPERIENCES S.L. amb l'única finalitat de contactar amb vostè tal com ens demana. Podrà accedir, rectificar i cancel·lar les seves dades, així com oposar-se al tractament dels mateixos, dirigint-se per escrit i adjuntant còpia del DNI a BRITISH SUMMER EXPERIENCES S.L., Via Augusta 33, Entlo 2ª, 08006 Barcelona.",
        ),
  'masinfocic' =>
        array(
            'plusinfotext' => 'Si ho prefereixes<br/>NOSALTRES ET TRUQUEM',
            'titulo' => "TRUCA'NS AL 93 200 11 33<br />O DEIXA'NS UN TELÈFON DE CONTACTE",
            'txt' => "T'AJUDAREM A PRENDRE LA MILLOR DECISIÓ",
            'nombre' => 'nom',
            'nombrecampo' => 'nom',
            'telefono' => 'telèfon',
            'telefonocampo' => 'telèfon de contacte',
            'email' => 'email',
            'emailcampo' => 'email de contacte',
            'enviar' => 'enviar',
            'protecciondatos' => "D'acord amb el que estableix la Llei Orgànica 15/1999, de Protecció de Dades de Caràcter Personal, l'informem que les dades personals que ens faciliti a través del present formulari, s'incorporaran als fitxers de la Institució Cultural del CIC amb l'única finalitat de contactar amb vostè tal com ens demana. Podrà accedir, rectificar i cancel·lar les seves dades, així com oposar-se al tractament dels mateixos, dirigint-se per escrit i adjuntant còpia del DNI a la Institució Cultural del CIC, Via Augusta 205, 08021 Barcelona.",
        ),
    'idiomas' => array(
        'Inglés' => 'Anglès',
        'Español' => 'Espanyol',
        'Francés' => 'Francès',
        'Alemán' => 'Alemany',
        'Chino' => 'Xinès',
        'Italiano' => 'Italià',
        'Portugués' => 'Portugès',
        'Japonés' => 'Japonès',
    ),
  'curso' =>
        array (
            'programa' => 'Programa',
            'idiomarequerido' => 'Idioma requerit',
            'sesiones' => "Número de sessions d'idioma",
            'requisitos' => 'Requisits especials',
            'nivel' => 'Nivell requerit',
            'centro' => 'Centre',
            'alojamiento' => 'Allotjament',
            'actividades' => 'Activitats',
            'excursiones' => 'Excursions',
            'transporte' => 'Transport',
            'instalaciones' => 'Instal·lacions',
            'comidas' => 'Àpats',
            'monitor' => 'Monitor',
            'coordinador-tab' => 'coordinacio',
            'coordinador' => 'Coordinació',
            'fechas-precios' => 'dates-preus',
            'fechasyprecios' => 'Dates i Preus',
            'fotos' => 'Fotos',
            'videos' => 'Vídeos',
            'fotosvideo' => 'Fotos i Vídeos',
            'fotos-videos' => 'fotos-videos',
            'info-basica' => 'Info bàsica',
            'internet-disponible' => 'Internet disponible',
            'inscripcion' => 'Inscripció',
            'catalogo' => 'Catàleg',
            'faq' => 'F.A.Q.',
            'descargar' => 'Descarregar',
            'solicitarplaza' => 'Més informació',
            'comprar' => 'Inscripció',
            'comprar-corporativo' => 'Inscripció Curs corporatiu, bla, bla, bla',
            'deseo' => 'Favorit',
            'reservar' => 'Reservar',
            'timetable' => 'Timetable',
            'masinfoproveedor1' => 'Més informació a la',
            'masinfoproveedor2' => 'web del nostre proveïdor.',
            'incluido' => 'Inclòs al preu',
            'precioincluye' => 'El preu inclou:',
            'incluyeclasesidioma' => "Horari de classes d'idiomes:",
            'al' => 'al',
            'entre' => 'entre el',
            'yel' => 'i el',
            'planb' => 'Pla B',
            'planbfrase' => 'Si no us van bé les dates proposades per viatjar en grup, podeu començar el programa de forma individual en un altre moment...',
            'extras' => 'Extres',
            'curso' => 'Duració del curs',
            'finicio' => 'Data inici',
            'ffin' => 'Data fi',
            'duracion' => 'Duració',
            'precio' => 'Preu',

            'Días' => 'Dia|Dies',
            'Semana' => 'Setmana',
            'semana' => 'Setmana',
            'Semanas' => 'Setmana|Setmanes',
            'Meses' => 'Mes|Mesos',
            'Trimestres' => 'Trimestre|Trimestres',
            'Semestres' => 'Semestre|Semestres',
            'Años' => 'Anys',
            'Trayecto' => 'Trajecte',

            'Excursión' => 'Excursió',
            'Noche' => 'Nit',
            'Clase' => 'Classe',
            'Per afternoon' => 'Per afternoon',
            'Por 2 semanas' => 'Per 2 setmanes',
            'Term' => 'Term',

            'presupuesto' => 'Demai presupost personalitzat',
            'subtotal' => 'Subtotal',
            'total' => 'Total',
            'alojamientoextrasobligatorios' => 'Extres obligatoris allotjament',
            'extrasobligatorios' => 'Extres obligatoris',
            'divisas' => 'Canvi aplicat',
            'selecciona' => 'Selecciona la durada del curs i allotjament per veure el total',
        ),
    'categorias' => array(
        'jovenes' => 'Joves',
        'adultos' => 'Adults',
        'aescolar' => 'Any Escolar',
        'camps' => 'Campaments',
        'colonias' => 'Colònies',
        'verprogramas' => 'VEURE PROGRAMES',
        'vertodos' => 'Veure tots',
        'ver' => 'veure',
        'eligecurso' => 'Tria el teu curs i comença una nova experiència',
        'buscar' => 'Cercar cursos',
        'buscar-resultados' => 'Has cercat:',
        'ingles-slug' => 'aprendre-angles-a-l-estranger',
        'ingles' => "Anglès a l'estranger",
        'ingles-desc' => 'Totes les edats',
        'ingles-intro' => "<p>Perquè ens anem a enganyar, l'anglès és l'idioma que tothom vol aprendre i per suposat saber. Per això en aquesta secció només hi ha cursos d'anglès, perquè ningú es perdi en la recerca i trobi el seu curs idoni.</p><p>Anglès per a nens, anglès per a adolescents, anglès per a adults, anglès per a famílies senceres, anglès per a professionals desesperats o no tant. “English for everybody and everywhere”.</p>",
        'idiomas-slug' => 'aprendre-altres-idiomes-a-l-estranger',
        'idiomas' => "Altres idiomes a l'estranger",
        'idiomas-desc' => 'Alemany, Xinès, Francès, Italià, Japonès, Rus...',
        'idiomas-intro' => "<p>No només d'anglès viu l'home. Imagina't en un mercat de Xangai parlat sobre la textura dels dim-sum amb el seu cuiner! O negociant en francès per al teu negoci de moda. O debatent en alemany sobre la nova arquitectura de la ciutat de Berlín.</p><p>Xinès, japonès, francès, alemany, rus, italià, àrab, portuguès ... en la varietat està el bon gust No és així?</p>",
        'jovenes-desc' => 'de 8 a 18 anys',
        'jovenes-intro' => '',
        'adultos-profesionales-desc' => 'de 18 a 99 anys',
        'adultos-profesionales-intro' => '',
        'toda-la-familia-desc' => 'Programes per a tota la família',
        'toda-la-familia-intro' => '',
        'curso-escolar-en-el-extranjero-desc' => 'Primària, secundària i batxillerat',
        'curso-escolar-desc' => 'desde 1984',
        'curso-escolar-en-el-extranjero-intro' => "<p>Quan una família decideix que el seu fill o filla realitzi un curs de secundària o batxillerat en algun país del món diferent al seu, vol saber que durant tot aquest procés comptaran amb el millor equip de professionals.</p><p>Per descomptat un any escolar a l'estranger és l'aposta més rellevant que es pot fer a nivell formatiu per a un adolescent però també la de major impacte a nivell d'aprenentatge vital, lingüístic i acadèmic.</p><p>La decisió de realitzar un any escolar en un país estranger és, sens dubte, la més rellevant que una família pot prendre en relació a la vida acadèmica-personal d'un fill o filla.</p><p>I en aquest camí, que s'inicia amb les preguntes a coneguts que han realitzat un programa semblant, per seguir amb la comparació entre diverses empreses del sector, cal decidir-se pels millors experts i tenir la tranquil·litat que durant tot el procés estarem assistits per persones que escolten les nostres sol·licituds i dubtes de la forma més personal i alhora les resolen de la manera més professional.</p>",
        'curso-escolar-en-el-extranjero-sidebar' => "<h5 class='head'>Els nostres 10 punts forts<span></span></h5>
                                <h5><small>Per què escollir a <strong>".ConfigHelper::config('nombre')."</strong>?</small></h5>
                                <ol>
                                  <li>Nombre reduït d'alumnes, el que ens permet atendre les necessitats individuals que es plantegen.</li>
                                  <li>Entrevistes personals amb els pares i els participants conjuntament i per separat.</li>
                                  <li>El millor assegurança mèdica d'accidents i responsabilitat civil.</li>
                                  <li>Test de nivell d'idioma.</li>
                                  <li>Assessorament sobre el programa més indicat.</li>
                                  <li>Organització abans de la sortida d'un seminari d'orientació i preparació per als estudiants i les seves famílies.</li>
                                  <li>Seguiment minuciós de l'adaptació i resultat de cada estudiant.</li>
                                  <li>Gestió completa de la convalidació.</li>
                                  <li>Servei d'atenció 24 hores.</li>
                                  <li>I com sempre, la garantia de l'organitzador de cursos d'idiomes a l'estranger amb més prestigi del país.</li>
                                </ol>",
        'nuestrasclases' => 'Les nostres colònies',
        'campamentos-verano-ingles-desc' => '<b>Max Camps</b>. De 7 a 17 anys',
        'campamentos-verano-ingles-intro' => "
  <div class='row'><video style='border: 1px solid #E5E5E5; margin-bottom: 30px;display: block;' class='col-sm-8' controls='' preload='none' poster='/assets/britishsummer/home/maxcamps2015.jpg'>
<source src='/assets/britishsummer/home/maxcamps2015.mp4' type='video/mp4'>
  </video></div>
                        <h2>Les classes</h2>
                    	<p>Distingim entre els participants més joves (entre els 7 i els 10/11 anys) i els més grans (10/11 a 17) ja que les dinàmiques d’aprenentatge són molt diferents.</p>
                        <p><strong>Ràtio mitjana per classe 1/12.</strong></p>
                        <p><strong>Professors nadius i titulats.</strong></p>
                        <p><strong>Nivells:</strong> 6 nivells diferents entre elemental i avançat (pre-advanced).</p>
                        <p><strong>Eixos temàtics:</strong> Les classes tindran un eix vertebrador setmanal: natura i medi ambient; multiculturalitat; innovació i tecnologia; i art, cultura i entreteniment</p>
                        <h4 class='addmargintop30'>SESSIONS</h4>
                        <p><strong>Organització de les tres sessions diàries de classe.<br>
15 sessions setmanals. 1 sessió = 50 minuts</strong></p>
                        <ul>
                          <li>1ª sessió 10h o 16h. CLASSWORK: Llengua i vocabulari.</li>
                          <li>2ª sessió 11h o 17h. ACTIVE PRACTICE: Pràctica de l’idioma a través de jocs, teatre, experiments, treballs manuals, role-playing…</li>
                          <li>3ª sessió 12h o 18h. SHOWTIME: Els estudiants treballen tutoritzats pel professor/a en un projecte comú: cançons, programa de ràdio, anuncis, etc. En aquesta sessió es barregen estudiants de diferents nivells.</li>
                        </ul>
                       <p>Els participants que facin el Trinity Exam substituiran quatre dies d’aquesta tercera sessió per una preparació específica d’aquest examen.</p>

                       <h2 class='addmargintop30'>Vintage Day</h2>
                       <p>La tecnologia guanya terreny dia a dia: Ipad, iPhone, IMac, tablet, Playstation, Kindle… i ens encanta! però durant un dia volem recuperar algunes d’aquelles activitats que perduren en el temps, que hem gaudit tantes vegades i que són tan divertides.</p>

                        <h2 class='addmargintop30'>Performance &amp; night activity</h2>
                       <p>La nit és nostra! Les Night Activity són activitats que organitzem sempre després de sopar i que tenen com a objectiu fomentar la participació i cohessió dels grups utilitzant l’anglès d’una manera divertida i relaxada.</p>
                    	<h2 class='addmargintop30'>Pool time</h2>
                       <p>Cada dia, organitzats per grups, els participants podran gaudir de la magnífica piscina de què disposa el Vilar Rural de Cardona i els apartaments d’Astúries i Andalusia.</p>

                       <h2 class='addmargintop30'>Trinity Exam<br><small>(només Vilar Rural de Cardona)</small></h2>
                       <p>Els participants que facin un mínim de dues setmanes de campament començant el primer torn o el tercer tenen inclòs l’examen del Trinity College de Londres sempre que hi hagi més de vint participants inscrits.</p>
                       <p><strong>Està dividit en 12 nivells:</strong></p>
                       <ul>
                         <li>Inicial – Nivells 1 a 3</li>
                         <li>Elemental– Nivells 4 a 6</li>
                         <li>Intermedi – Nivells 7 a 9</li>
                         <li>Avançat – Nivells 10 a 12</li>
                       </ul>
                       <p>L’Examen GESE del Trinity College avalua la capacitat d’escoltar i parlar dels estudiants, i posa èmfasi en l’habilitat pràctica en l’ús de la llengua més que en el coneixement teòric.</p>
                       <p>Per a realitzar els exàmens GESE del Trinity els examinadors viatgen des del Regne Unit fins els nostres campaments. Aquests examinadors tenen una alta preparació i molta experiència.</p>

",
        'summer-camps-ingles-desc' => 'De 6 a 15 anys',
        'summer-camps-ingles-intro' => "
                <div class='row'>
                	<div class='col-sm-2 col-xs-2'>
                		<ul class='colonies' id='colonies'>
                        	<li><span>C</span>oneix</li>
                            <li><span>O</span>bre't</li>
                            <li><span>L</span>lança't</li>
                            <li><span>O</span>bserva</li>
                            <li><span>N</span>odreix-te</li>
                            <li><span>I</span>l·lusiona't</li>
                            <li><span>E</span>xperimenta</li>
                            <li><span>S</span>orprèn-te</li>
                        </ul>
                    </div>
                    <div class='col-sm-9 col-sm-offset-1 col-xs-9 col-sx-offset-1'>


                		<h2>Què és una colònia CIC a Catalunya?</h2>
                    	<h4>Les nostres colònies, amb l’experiència acumulada de més de 30 anys, formen part del projecte pedagògic de CIC Escola d’idiomes. Garantim una bona integració del rigor acadèmic en l’aprenentatge de l’anglès de forma lúdica.</h4>

                       <h4 class='addmargintop30'><span>1.</span> PREPARACIÓ PRÈVIA</h4>
                       <ul>
                           <li><strong>Projecte acadèmic dissenyat per CIC Escola d’Idiomes:</strong> programació imaterial.</li>
                           <li><strong>Planificació de les activitats esportives i de lleure</strong> amb l’ajuda d’especialistes en el sector.</li>
                           <li><strong>Grups reduïts</strong> distribuïts per edats.</li>
                           <li><strong>Prova de nivell</strong> (a partir de 5è EP).</li>
                           <li><strong>Assessoria individualitzada</strong></li>
                           <li><strong>Reunió informativa per a les famílies.</strong></li>
                       </ul>

                    	<h4 class='addmargintop30'><span>2.</span> DURANT L'ESTADA</h4>
                       <ul>
                           <li><strong>Immersió en l’anglès des del minut zero</strong>, tot treballant: reading, speaking, listening i writing.</li>
                           <li><strong>Proporció alta de monitors per participants</strong>, garantint una atenció propera i personalitzada.</li>
                           <li><strong>Activitats organitzades per grups d’edat, habilitats i afinitats.</strong></li>
                           <li><strong>Treball per projectes</strong>, centre d’interès i/o preparació per al Trinity College Exam. Sempre de manera dinàmica i lúdica.</li>
                           <li><strong>Dietes equilibrades.</strong> Cuina d’elaboració pròpia. </li>
                           <li><strong>Menús especials</strong> per al·lèrgies i intoleràncies alimentàries.</li>
                           <li><strong>Contacte diari amb els coordinadors</strong> i, en cas necessari, amb les famílies.</li>
                           <li><strong>Blog online</strong> per a cada colònia, actualitzat amb comentaris i fotografies.</li>
                           <li><strong>Energia dels equips al 110%.</strong></li>
                       </ul>


                       <h4 class='addmargintop30'><span>3.</span> DESPRÉS DE L'ESTADA</h4>
                       <ul>
                           <li><strong>Certificat de participació.</strong> (Oficial en el cas del Trinity Exam, a Els Pins).</li>
                           <li><strong>Informe</strong> de l’actitud i les aptituds del participant.</li>
                           <li><strong>Servei de seguiment</strong> de l’anglès per part de CIC Escola d’Idiomes.</li>
                           <li><strong>Enquestes</strong> dels participants, famílies i propostes de millora.</li>
                           <li><strong>Avaluació</strong> dels resultats i <strong>programació</strong> de la nova temporada.</li>
                       </ul>
                    </div>
                </div>",
        'colectivos-y-escuelas-desc' => '',
        'colectivos-y-escuelas-intro' => "Cada vegada més les escoles es plantegen aprofitar una setmana del seu calendari escolar o algun parèntesi del mateix, per integrar-se en la cultura d'un país i practicar l'idioma que porten temps estudiant. A British Summer oferim gran varietat de programes que permeten realitzar unes classes de l'idioma triat en diferents nivells i amb participants internacionals, allotjar-se en família o apartaments i disposar de temps lliure per compartir en conjunt els increïbles atractius d'algunes de les ciutats europees més cosmopolites.",
    ),
    'landedslogan' => "El bloc de l'estiu",
    'academicopromo' => '<div class="sloganitem"><h2>-400€ a Any escolar a EUA J1</h2><a class="btn btn-white btn-outlined btn-promo" href="curs-escolar-a-l-estranger/ano-escolar-en-usa-familia-voluntaria-j1.html">veure</a></div>
                        <div class="sloganitem"><h2>-500€ a Any escolar a Irlanda</h2><a class="btn btn-white btn-outlined btn-promo" href="curs-escolar-a-l-estranger/ano-escolar-en-escuela-publica-irlanda.html">veure</a></div>
                        <div class="sloganitem"><h2>-300€ a Any escolar a Langley, Canadà + ¡Esquiada gratis!</h2><a class="btn btn-white btn-outlined btn-promo" href="curs-escolar-a-l-estranger/ano-escolar-en-langley-canada.html">veure</a></div>',
);
