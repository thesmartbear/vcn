@if(isset($categoria) && $categoria->tabla_cambio)

    <?php

        if(isset($any))
        {
            $tc = $categoria->getTablaCambioAny($any, $ficha->moneda);
        }
        else
        {
            $tc = $ficha->tabla_cambio;
        }
    ?>

    @if($tc)

        <table class="table table-bordered table-hover">
            <caption>Tabla de Tipos de cambio Activa (Categoría): [<a target="_blank" href="{{ route('manage.monedas.cambios.ficha', $tc->id) }}">{{$tc->name}}</a>]</caption>
            <thead>
                <tr>
                    <th>Moneda</th>
                    <th>TC Cálculo</th>
                    <th>TC Folleto</th>
                    <th>TC Final</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tc->cambios as $cambio)
                <tr>
                    <td>{{\VCN\Models\Monedas\Moneda::find($cambio->moneda_id)->name}}</td>
                    <td>{{$cambio->tc_calculo}}</td>
                    <td>{{$cambio->tc_folleto}}</td>
                    <td>{{$cambio->tc_final}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

    @else

        <div class="note note-info">
            <p>No hay Tabla de Tipo de cambio activa para esta Categoría y Año.</p>
        </div>

    @endif

@else
    <div class="note note-info">
        <p>No hay Tabla de Tipo de cambio activa para esta Categoría.</p>
    </div>

@endif