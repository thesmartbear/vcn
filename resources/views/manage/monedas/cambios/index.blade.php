@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-money fa-fw"></i> Tablas Tipo de Cambio
                <span class="pull-right"><a href="{{ route('manage.monedas.cambios.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Tabla</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'        => 'Nombre',
                      'categorias'  => 'Categorias',
                      'activo'      => 'Activo',
                      'plataforma'  => 'Plataforma',
                      'options'     => ''
                    ])
                    ->setUrl(route('manage.monedas.cambios.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [4] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop