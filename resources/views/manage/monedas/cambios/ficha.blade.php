@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-table fa-fw"></i> Tabla Cambio :: {{$ficha->name}}
    </div>
    <div class="panel-body">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Tabla</a></li>
            <li role="presentation"><a href="#monedas" aria-controls="monedas" role="tab" data-toggle="tab"><i class="fa fa-money fa-fw"></i> Monedas</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">
                {!! Form::model($ficha, array('route' => array('manage.monedas.cambios.ficha', $ficha->id))) !!}

                <div class="form-group row">
                    <div class="col-md-10">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activa'])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        @include('includes.form_select', [ 'campo'=> 'any', 'texto'=> 'Año inicio curso', 'select'=> ConfigHelper::getAnys()])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_select_multi', [ 'campo'=> 'categorias', 'texto'=> 'Categorías', 'valor'=> explode(',',$ficha->categorias), 'select'=> $categorias ])
                    </div>
                </div>

                <div class="form-group">
                    @include('includes.form_plataforma', ['campo'=> 'plataforma', 'todas'=> true])
                </div>

                @include('includes.form_submit', [ 'permiso'=> 'monedas-cambio', 'texto'=> 'Guardar'])

                {!! Form::close() !!}
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="monedas">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.monedas.cambios.monedas.ficha',$ficha->id), 'role' => 'form', 'class' => '')) !!}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-plus-circle fa-fw"></i> Cambios
                        <a id="cambios_add" href="#cambios_add" class='btn btn-xs btn-warning' data-label="Añadir"><i class="fa fa-plus-square"></i></a>
                    </div>
                    <div class="panel-body">

                        <div id="cambios_div">

                            @if(count($ficha->cambios))
                                @foreach($ficha->cambios as $cambios)

                                    <div class="cambios_form">

                                        <div class="row">
                                            <div class="col-md-2">
                                                @include('includes.form_select', [ 'campo'=> 'cambios[moneda_id][]', 'texto'=> 'Moneda', 'select'=> \VCN\Models\Monedas\Moneda::all()->pluck('name','id')->toArray(), 'valor'=> $cambios->moneda_id ])
                                            </div>

                                            <div class="col-md-2">
                                                @include('includes.form_input_number', [ 'step'=> '0.0001', 'campo'=> 'cambios[tc_calculo][]', 'texto'=> 'TC Cálculo', 'valor'=> $cambios->tc_calculo])
                                            </div>
                                            <div class="col-md-2">
                                                @include('includes.form_input_number', [ 'step'=> '0.0001', 'campo'=> 'cambios[tc_folleto][]', 'texto'=> 'TC Folleto', 'valor'=> $cambios->tc_folleto])
                                            </div>
                                            <div class="col-md-2">
                                                @include('includes.form_input_number', [ 'step'=> '0.0001', 'campo'=> 'cambios[tc_final][]', 'texto'=> 'TC Final', 'valor'=> $cambios->tc_final])
                                            </div>
                                        </div>

                                    </div>

                                @endforeach

                                <hr>

                            @else

                                <div class="cambios_form">

                                    <div class="row">
                                        <div class="col-md-2">
                                            @include('includes.form_select', [ 'campo'=> 'cambios[moneda_id][]', 'texto'=> 'Moneda', 'select'=> \VCN\Models\Monedas\Moneda::all()->pluck('name','id')->toArray() ])
                                        </div>

                                        <div class="col-md-2">
                                            @include('includes.form_input_number', [ 'step'=> '0.0001', 'campo'=> 'cambios[tc_calculo][]', 'texto'=> 'TC Cálculo'])
                                        </div>
                                        <div class="col-md-2">
                                            @include('includes.form_input_number', [ 'step'=> '0.0001', 'campo'=> 'cambios[tc_folleto][]', 'texto'=> 'TC Folleto'])
                                        </div>
                                        <div class="col-md-2">
                                            @include('includes.form_input_number', [ 'step'=> '0.0001', 'campo'=> 'cambios[tc_final][]', 'texto'=> 'TC Final'])
                                        </div>
                                    </div>

                                </div>

                            @endif

                        </div>

                        <div class="clearfix"></div>

                        @include('includes.form_submit', [ 'permiso'=> 'monedas-cambio', 'texto'=> 'Guardar'])

                    </div>
                </div>
                {!! Form::close() !!}

            </div>

        </div>

    </div>
</div>

@include('includes.script_form_add', ['campo'=> 'cambios' ])
@stop