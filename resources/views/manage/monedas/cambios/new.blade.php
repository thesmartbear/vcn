@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-truck fa-fw"></i> Nueva Tabla de cambios
            </div>
            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.monedas.cambios.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group row">
                        <div class="col-md-10">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activa'])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_select', [ 'campo'=> 'any', 'texto'=> 'Año inicio curso', 'select'=> ConfigHelper::getAnys()])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select_multi', [ 'campo'=> 'categorias', 'texto'=> 'Categorías', 'select'=> $categorias ])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_plataforma', ['campo'=> 'plataforma'])
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'monedas-cambio', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop