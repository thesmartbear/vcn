@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-money fa-fw"></i> Monedas
                <span class="pull-right"><a href="{{ route('manage.monedas.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Moneda</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'currency_name'   => 'Moneda',
                      'currency_rate'   => 'Tasa'
                    ])
                    ->setUrl(route('manage.monedas.index'))
                    ->render() !!}

            </div>
        </div>

@stop