@extends('layouts.manage')


@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.index.contactos') !!} --}}
@stop


@section('content')

<?php
$user = auth()->user();
?>

<ul class="nav nav-tabs" role="tablist">
    @if(ConfigHelper::canView('Resumen'))
    <li role="presentation"><a href="{{route('manage.index.resumen')}}">Resumen</a></li>
    @endif
    @if(ConfigHelper::canView('Tareas'))
    <li role="presentation"><a href="{{route('manage.index')}}">Tareas</a></li>
    @endif
    @if(ConfigHelper::canView('Ventas'))
    <li role="presentation"><a href="{{route('manage.index.ventas')}}">Ventas</a></li>
    @endif
    @if(ConfigHelper::canView('Incidencias'))
    <li role="presentation"><a href="{{route('manage.index')}}#incidencias">Incidencias ({{$user->incidencia_tareas->count()}})</a></li>
    @endif
    <li role="presentation" class="active"><a href="#contactos" aria-controls="contactos" role="tab" data-toggle="tab">Contactos</a></li>
</ul>

<div class="tab-content">

    <div role="tabpanel" class="tab-pane fade in active" id="contactos">

        <div class="portlet box grey">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-bar-chart"></i> Contactos diarios
            </div>
            <div class="tools">
                <a href="" class="collapse"></a>
                <a href="" class="fullscreen"></a>
            </div>

        </div>
        <div class="portlet-body">

            @if(!$resultados)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        No hay resultados
                    </div>
                </div>
            @else

                <div id="chart_c1"></div>
                @columnchart('Chart-Contactos', 'chart_c1')

                <hr>
            
                <div class="table-responsive">
                <table class="table table-bordered table-striped table-condensed">

                    <caption class="text-uppercase">Contactos diarios [{{$valores['desde']}} - {{$valores['hasta']}}]</caption>
                    <thead>
                        <tr>
                            <th></th>
                            @foreach($usuarios as $u)
                                <th>{{$u->full_name}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($fechas as $f)
                        <tr>
                            <td>{{$f}}</td>
                            @foreach($usuarios as $u)
                                <td>{{$contactos[$f][$u->id]}}</td>
                            @endforeach
                        </tr>
                        @endforeach
                    </tbody>
                
                </table>
                </div>

            @endif

        </div>
        </div>

    </div>


</div>
{{-- @endif --}}

@stop