@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tag fa-fw"></i> Sub-Especialidades
                <span class="pull-right"><a href="{{ route('manage.subespecialidades.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Sub-Especialidad</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Subespecialidad',
                      'especialidad'    => 'Especialidad',
                      'es_multi'        => 'Multi',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.subespecialidades.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [3] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop