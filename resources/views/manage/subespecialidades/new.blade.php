@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Nueva Sub-Especialidad
            </div>
            <div class="panel-body">


                {!! Form::open(array('method' => 'POST', 'url' => route('manage.subespecialidades.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'especialidad_id', 'texto'=> 'Especialidad', 'valor'=> 0, 'select'=> $especialidades])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'SubEspecialidad'])
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'configuracion', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop