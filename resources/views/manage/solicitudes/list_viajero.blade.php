<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-list-alt"></i> Solicitudes anteriores

        </div>
        <div class="panel-body">

          {!! Datatable::table()
              ->addColumn([
                'fecha'     => 'Fecha',
                'categoria' => 'Categoría',
                'status'    => 'Status',
                'rating'    => 'Rating',
                'asignado'  => 'Asignado',
                'destinos'  => 'País',
                'ciudad'    => 'Ciudad',
                'cursos'    => 'Cursos',
                'notas'     => 'Notas'

              ])
              ->setUrl( route('manage.solicitudes.index.viajero', $viajero_id) )
              ->setOptions(
                  "aoColumnDefs", array(
                    //[ "bSortable" => false, "aTargets" => [7] ]
                    [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
              ->render() !!}

        </div>
    </div>

</div>