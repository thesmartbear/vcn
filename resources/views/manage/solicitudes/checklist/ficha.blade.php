@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> CheckList Status Solicitud :: {{$ficha->name}}
            </div>
            <div class="panel-body">


                {!! Form::model($ficha, array('route' => array('manage.solicitudes.checklist.ficha', $ficha->id))) !!}

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'status_id', 'texto'=> 'Status', 'valor'=> $ficha->status_id, 'select'=> $statuses])
                </div>

                <div class="form-group">
                    @include('includes.form_input_number', [ 'campo'=> 'orden', 'texto'=> 'Orden', 'required'=>true])
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_select', [ 'campo'=> 'category_id', 'texto'=> 'Categoría', 'select'=> $categorias])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_select', [ 'campo'=> 'subcategory_id', 'texto'=> 'Subcategoría', 'select'=> $subcategorias])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_select', [ 'campo'=> 'subcategory_det_id', 'texto'=> 'Detalle Subcategoría', 'select'=> $subcategorias_det])
                    </div>
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'CheckList'])
                </div>

                <div class="form-group">
                    @include('includes.form_select_multi', [ 'campo'=> 'seguimiento', 'texto'=> 'Seguimiento Tipo', 'select'=> ConfigHelper::getTipoSeguimiento() ])
                </div>

                <div class="form-group pull-right">
                    @include('includes.form_submit', [ 'id'=>'btn-add', 'permiso'=> 'checklist-solicitudes', 'texto'=> 'Guardar'])
                </div>

                {!! Form::close() !!}

            </div>
        </div>

@include('includes.script_categoria')

@stop