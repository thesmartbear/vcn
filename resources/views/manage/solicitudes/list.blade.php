
    <div class="portlet light tabs">

        <div class="portlet-body">

            <?php
              $params = [$status_id,$user_id,$oficina_id, 'any'=> $any];
              if(isset($valores))
              {
                $params = array_merge($params, $valores);
              }
            ?>

            {!! Datatable::table()
                ->addColumn([
                  'fecha'       => 'Fecha',
                  'viajero'     => 'Lead',
                  'categoria'   => 'Categoría',
                  'origen'      => 'Orígen',
                  'status'      => 'Status',
                  'rating'      => 'Rating',
                  'asignado'    => 'Asignado',
                  'ultimo'      => 'Últ. contacto',
                  'tarea'       => 'Tarea',
                  'options'     => ''

                ])
                ->setUrl( route('manage.solicitudes.indexby', $params) )
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "columnDefs", array(
                    [ "sortable" => false, "targets" => [6,8,9] ],
                    [ "targets" => [0,7], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                    //[ "orderData" => 0, "targets" => 1 ],
                    //[ "visible" => false, "targets" => [0] ]
                  )
                )
                ->render() !!}
        </div>
    </div>

