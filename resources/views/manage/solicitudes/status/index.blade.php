@extends('layouts.manage')


@section('container')
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-tag fa-fw"></i> Status Solicitud
        <span class="pull-right"><a href="{{ route('manage.solicitudes.status.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Status Solicitud</a></span>
    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
              'orden'      => 'Orden',
              'name'    => 'Status',
              'options' => ''
            ])
            ->setUrl(route('manage.solicitudes.status.index'))
            ->setOptions(
              "aoColumnDefs", array(
                [ "bSortable" => false, "aTargets" => [2] ]
              )
            )
            ->render() !!}

    </div>
</div>
@stop