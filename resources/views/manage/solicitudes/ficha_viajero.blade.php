{!! Form::model($ficha, array('route' => array('manage.solicitudes.ficha', $ficha->id))) !!}

    {!! Form::hidden('rating',$ficha->rating, array('class' => 'rating')) !!}

    <div class="form-group">
        @include('includes.form_input_datetime', [ 'campo'=> 'fecha', 'valor'=> Carbon::parse($ficha->fecha)->format('d/m/Y'),'texto'=> 'Fecha'])
    </div>

    <div class="form-group row">
        <div class="col-md-12">
            <h4 class="form-section">Tipo de curso buscado</h4>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_select', [ 'campo'=> 'category_id', 'texto'=> 'Categoría', 'select'=> $categorias])
        </div>
        <div class="col-md-4">
            @include('includes.form_select', [ 'campo'=> 'subcategory_id', 'texto'=> 'Sub-Categoría','select'=> $subcategorias])
        </div>
        <div class="col-md-4">
            @include('includes.form_select', [ 'campo'=> 'subcategory_det_id', 'texto'=> 'Detalle','select'=> $subcategorias_det])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12">
            <h4 class="form-section">Solicitud</h4>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_select', [ 'campo'=> 'destinos', 'texto'=> "País", 'select'=> [""=>""] + \VCN\Models\Pais::orderBy('name')->pluck('name','name')->toArray()])
        </div>
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'ciudad', 'texto'=> 'Ciudad'])
        </div>
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'cursos', 'texto'=> 'Cursos'])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12">
            @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas'])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-3">
            @include('includes.form_checkbox', [ 'campo'=> 'catalogo_envio', 'texto'=> 'Envío de catálogo'])
            @if($ficha->catalogo_envio)
                <br>
                <span>{{$ficha->catalogo_envio_log}}</span>
            @endif  
        </div>
        <div class="col-md-3" id="catalogo_enviado_div" style="display:none;">
            @include('includes.form_checkbox', [ 'campo'=> 'catalogo_enviado', 'texto'=> 'Catálogo enviado'])
            @if($ficha->catalogo_enviado)
                <br>
                <span>{{$ficha->catalogo_envio_log}}</span>
            @endif  
        </div>
    </div>


    <div class="form-group pull-right">
        {!! Form::submit('Guardar', array( 'class' => 'btn btn-success')) !!}
        <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
    </div>

{!! Form::close() !!}

@push('scripts')
<script>
    function changeBool()
    {
        if( $('#catalogo_envio').is(':checked') )
        {
            $('#catalogo_enviado_div').show();
        }
        else
        {
            $('#catalogo_enviado_div').hide();
        }
    }

    $('#catalogo_envio').change( function(e) {
        changeBool();
    });

    changeBool();
</script>
@endpush