@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.solicitudes.index') !!}
@stop

@section('titulo')
    <i class="fa fa-pencil-square"></i> Solicitudes
@stop

@section('container')

    <div class="row">
        <div class="col-md-1">
            {!! Form::select('any', $anys, $any, array('class'=>'select2', 'data-style'=>'blue', 'id'=>'select-any-filtro'))  !!}
            @include('includes.script_filtro_any')
        </div>
        <div class="col-md-5"></div>

        @include('includes.select_asignados', ['route'=> 'manage.solicitudes.index'])

    </div>

    <hr>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs arrows" role="tablist">
        <li role="presentation" class="active all"><a href="#todos" aria-controls="todos" role="tab" data-toggle="tab">Todos [{{$statuses_total[0]}}]</a></li>

        @foreach($statuses as $status)
        <li role="presentation" class="status-{{$status->orden}}">
            <a href="#status-{{$status->id}}" aria-controls="status-{{$status->id}}" role="tab" data-toggle="tab"><span>{{$status->name}} [{{$statuses_total[$status->id]}}]</span></a>
        </li>
        @endforeach

    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <div role="tabpanel" class="tab-pane fade in active" id="todos">
            @include('manage.solicitudes.list', ['status_id'=> $status_id, 'user_id'=> $user_id, 'oficina_id'=> $oficina_id, 'status_name' => 'Todos'])
        </div>

        @foreach($statuses as $status)
        <div role="tabpanel" class="tab-pane fade in" id="status-{{$status->id}}">
            @include('manage.solicitudes.list', ['status_id'=> $status->id, 'user_id'=> $user_id, 'oficina_id'=> $oficina_id, 'status_name' => $status->name])
        </div>
        @endforeach

    </div>

@include('includes.script_checklist_status', ['booking'=> false])

@stop