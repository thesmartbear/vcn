@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.bookings.index') !!} --}}
@stop

@section('titulo')
    <i class="fa fa-bell"></i> Notificaciones
@stop


@section('container')


<div class="row">

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-bell"></i> Notificaciones
    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
                'fecha_in'    => 'Enviada',
                'nombre'        => 'Nombre',
                'texto'         => 'Notificación',
                'link'          => 'Enlace',
                'read_at'       => 'Leído',
                // 'options' => ''

            ])
            ->setUrl( route('notifications.list') )
            ->setOptions(
                "aoColumnDefs", array(
                )
            )
            ->render() !!}

    </div>
</div>

</div>

@stop