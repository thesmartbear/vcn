@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Nuevo Detalle Sub-Categoría
            </div>
            <div class="panel-body">


                {!! Form::open(array('method' => 'POST', 'url' => route('manage.subcategoria-detalles.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'subcategory_id', 'texto'=> 'SubCategoría', 'valor'=> 0, 'select'=> $subcategorias])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'SubCategoría'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name_web', 'texto'=> 'Nombre web'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'slug', 'texto'=> 'Url SEO'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'descripcion', 'texto'=> 'Descripción'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'contable', 'texto'=> 'Prefijo contable'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select', [ 'campo'=> 'es_aviso_foto', 'texto'=> 'Aviso foto', 'select'=> [null=>'',0=>'No',1=>'Si']])
                        </div>
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'system', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop