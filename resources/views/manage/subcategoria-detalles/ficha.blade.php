@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Detalle Sub-Categoria :: {{$ficha->name}} [Sub-Categoría :: <a href="{{ route('manage.subcategorias.ficha', $ficha->subcategory_id) }}">{{$ficha->subcategoria->name }}</a>]
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Detalle Sub-Categoría</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">
                        {!! Form::model($ficha, array('route' => array('manage.subcategoria-detalles.ficha', $ficha->id))) !!}

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'subcategory_id', 'texto'=> 'Categoría', 'valor'=> $ficha->subcategory_id, 'select'=> $subcategorias])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Subcategoría'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name_web', 'texto'=> 'Nombre web'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'slug', 'texto'=> 'Url SEO'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'descripcion', 'texto'=> 'Descripción'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_input_text', [ 'campo'=> 'contable', 'texto'=> 'Prefijo contable'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_select', [ 'campo'=> 'es_aviso_foto', 'texto'=> 'Aviso foto', 'select'=> [null=>'',0=>'No',1=>'Si']])
                            </div>
                        </div>

                        @include('includes.form_submit', [ 'permiso'=> 'system', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'SubcategoriaDetalle',
                                'campos_text'=> [
                                    ['name'=> 'Nombre'],
                                    ['name_web'=> 'Nombre web'],
                                    ['slug'=> 'Url SEO'],
                                ],
                                'campos_textarea'=> [
                                    ['descripcion'=> 'Descripcion'],
                                ]
                            ])

                    </div>

                </div>

            </div>
        </div>

@stop