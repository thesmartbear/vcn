@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tag fa-fw"></i> Detalles Sub-Categoría
                <span class="pull-right"><a href="{{ route('manage.subcategoria-detalles.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Detalle Sub-Categoría</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Detalle',
                      'subcategoria'    => 'SubCategoría',
                      'categoria'       => 'Categoría',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.subcategoria-detalles.index'))
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [3] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop