@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.proveedores.index') !!}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-truck fa-fw"></i> Proveedores
                <span class="pull-right"><a href="{{ route('manage.proveedores.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Proveedor</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    //->addColumn('Nombre Proveedor','Persona Contacto', 'Email Contacto', 'Número Contaco', 'Móvil Contacto', 'Comisión', '')
                    ->addColumn([
                      'name'            => 'Nombre Proveedor',
                      'contact_name'    => 'Persona Contacto',
                      'contact_email'   => 'Email Contacto',
                      'contact_number'  => 'Número Contacto',
                      'contact_mobil'   => 'Móvil Contacto',
                      'commission'       => 'Comisión',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.proveedores.index'))
                    ->setOptions('iDisplayLength',100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [6] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop