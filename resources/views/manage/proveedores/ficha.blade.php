@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.proveedores.ficha', $ficha) !!}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-truck fa-fw"></i> Proveedor :: {{$ficha->name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Proveedor</a></li>
                    <li role="presentation"><a href="#centros" aria-controls="centros" role="tab" data-toggle="tab">Centros</a></li>
                    <li role="presentation"><a href="#alojamientos" aria-controls="alojamientos" role="tab" data-toggle="tab">Alojamientos</a></li>
                    <li role="presentation"><a href="#cursos" aria-controls="cursos" role="tab" data-toggle="tab">Cursos</a></li>

                    @if(ConfigHelper::canEdit('informes'))
                    <li role="presentation"><a href="#ventas" aria-controls="ventas" role="tab" data-toggle="tab">Ventas</a></li>
                    @endif

                    <li role="presentation"><a href="#contactos_sos" aria-controls="contactos_sos" role="tab" data-toggle="tab">Contactos SOS</a></li>

                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('route' => array('manage.proveedores.ficha', $ficha->id))) !!}

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'contact_name', 'texto'=> 'Nombre contacto'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'contact_email', 'texto'=> 'Email contacto'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'contact_number', 'texto'=> 'Número contacto'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'contact_mobil', 'texto'=> 'Móvil contacto'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'commission', 'texto'=> 'Comisión'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea', [ 'campo'=> 'plazos', 'texto'=> 'Plazos de Pago y comisiones'])
                        </div>
                        

                        @include('includes.form_plataforma', ['campo'=> 'propietario', 'todas'=> true])

                        @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="contactos_sos">
                        @include('includes.system_contactos-tab', ['modelo'=> 'Proveedor', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="centros">
                        @include('manage.centros.list', ['proveedor_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="alojamientos">
                        @include('manage.alojamientos.list_proveedor', ['proveedor_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="cursos">
                        @include('manage.cursos.list_proveedor', ['proveedor_id'=> $ficha->id])
                    </div>

                    @if(ConfigHelper::canEdit('informes'))
                    <div role="tabpanel" class="tab-pane fade in" id="ventas">
                        <div class="row">
                            <div class="col-md-12">

                                <?php
                                    $anyd = 2015;
                                    $anyh = intval(Carbon::now()->format('Y'));
                                ?>

                                <ul class="nav nav-tabs" role="tablist">
                                @for ($i = $anyd; $i <= $anyh; $i++)
                                    @if($totales[$i]['total_num']>0)
                                        <li role="presentation" class="{{($i === $anyh)?'active':''}}"><a href="#any-{{$i}}" aria-controls="any-{{$i}}" role="tab" data-toggle="tab">{{$i}}</a></li>
                                    @endif
                                @endfor
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    @for ($i = $anyd; $i <= $anyh; $i++)
                                        @if($totales[$i]['total_num']>0)
                                        <div role="tabpanel" id="any-{{$i}}" class="tab-pane fade in {{($i === $anyh)?'active':''}}">

                                            {!! Datatable::table()
                                                ->addColumn([
                                                    'categoria'     => 'Categoria',
                                                    'num'           => 'Nº Inscrip.',
                                                    'total_sem'     => 'Semanas',
                                                    'total_curso'   => "Imp.Curso <i class='fa fa-info-circle' data-label='Importe del curso, sin extras ni descuentos, ni alojamiento'></i>",
                                                    'total'         => "Imp.Total <i class='fa fa-info-circle' data-label='Importe total a pagar por el cliente'></i>",
                                                ])
                                                ->setUrl( route('manage.proveedores.ventas',[$ficha->id,$i]) )
                                                ->setOptions('iDisplayLength', 100)
                                                ->render()
                                            !!}

                                            <hr>

                                            <table class="table table-bordered table-hover">
                                                <caption>Totales</caption>
                                                <thead>
                                                    <tr>
                                                        <th>Total Inscrip.</th>
                                                        <th>Total Semanas</th>
                                                        <th>Total Curso</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>{{$totales[$i]['total_num']}}</td>
                                                        <td>{{$totales[$i]['total_sem']}}</td>
                                                        <td>{{ConfigHelper::parseMoneda($totales[$i]['total_curso'])}}</td>
                                                        <td>{{ConfigHelper::parseMoneda($totales[$i]['total'])}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <hr>
                                            @foreach($ficha->centros as $centro)

                                                <div class="portlet box blue">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-pie-chart"></i>
                                                            <span class="caption-subject bold">Centro {{$centro->name}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        {!! Datatable::table()
                                                            ->addColumn([
                                                                'categoria'     => 'Categoria',
                                                                'num'           => 'Nº Inscrip.',
                                                                'total_sem'     => 'Semanas',
                                                                'total_curso'   => "Imp.Curso <i class='fa fa-info-circle' data-label='Importe del curso, sin extras ni descuentos, ni alojamiento'></i>",
                                                                'total'         => "Imp.Total <i class='fa fa-info-circle' data-label='Importe total a pagar por el cliente'></i>",
                                                            ])
                                                            ->setUrl( route('manage.proveedores.ventas',[$ficha->id,$i,$centro->id]) )
                                                            ->setOptions('iDisplayLength', 100)
                                                            ->render()
                                                        !!}

                                                        <hr>

                                                        <table class="table table-bordered table-hover">
                                                            <caption>Totales ({{$centro->name}})</caption>
                                                            <thead>
                                                                <tr>
                                                                    <th>Total Inscrip.</th>
                                                                    <th>Total Semanas</th>
                                                                    <th>Total Curso</th>
                                                                    <th>Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>{{$totales_centro[$centro->id][$i]['total_num']}}</td>
                                                                    <td>{{$totales_centro[$centro->id][$i]['total_sem']}}</td>
                                                                    <td>{{ConfigHelper::parseMoneda($totales_centro[$centro->id][$i]['total_curso'])}}</td>
                                                                    <td>{{ConfigHelper::parseMoneda($totales_centro[$centro->id][$i]['total'])}}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>

                                            @endforeach

                                        </div>
                                        @endif
                                    @endfor

                                </div>

                            </div>
                        </div>
                    </div>
                    @endif

                    <div role="tabpanel" class="tab-pane fade in" id="contactos_sos">
                        @include('includes.system_contactos-tab', ['modelo'=> 'Proveedor', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'Proveedor',
                                'campos_text'=> [ ['name'=> 'Nombre'], ],
                                'campos_textarea'=> []
                            ])

                    </div>

                </div>

            </div>
        </div>

@stop