@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.proveedores.nuevo') !!}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-truck fa-fw"></i> Nuevo Proveedor
            </div>
            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.proveedores.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'contact_name', 'texto'=> 'Nombre contacto'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'contact_email', 'texto'=> 'Email contacto'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'contact_number', 'texto'=> 'Número contacto'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'contact_mobil', 'texto'=> 'Móvil contacto'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'commission', 'texto'=> 'Comisión'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea', [ 'campo'=> 'plazos', 'texto'=> 'Plazos de Pago y comisiones'])
                    </div>

                    @include('includes.form_plataforma', ['campo'=> 'propietario', 'todas'=> true])

                    @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop