@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.index.resumen') !!}
@stop

@section('content')

<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#resumen" aria-controls="resumen" role="tab" data-toggle="tab">Resumen</a></li>
    @if(ConfigHelper::canView('Tareas'))
    <li role="presentation"><a href="{{route('manage.index')}}">Tareas</a></li>
    @endif
    @if(ConfigHelper::canView('Ventas'))
    <li role="presentation"><a href="{{route('manage.index.ventas')}}">Bookings</a></li>
    @endif
    @if(ConfigHelper::canView('Incidencias'))
    <li role="presentation"><a href="{{route('manage.index')}}#incidencias">Incidencias ({{$user->incidencia_tareas->count()}})</a></li>
    @endif
    @if(ConfigHelper::canView('Contactos'))
    <li role="presentation"><a href="{{route('manage.index.contactos')}}">Contactos</a></li>
    @endif
</ul>

<div class="tab-content">
<div role="tabpanel" class="tab-pane fade in active" id="resumen">

    <div class="row">
        <div class="col-md-6"></div>

        <div class="col-md-3">
            @foreach($oficinas as $a)
                <?php $o[$a->id] = $a->name; ?>
            @endforeach
            <?php $o['all'] = 'TODAS'; ?>
            <?php $o1 = ($oresumen?$oresumen:'all'); ?>

            @if( auth()->user()->filtro_oficinas )
                {!! Form::select('oficinas', $o, $o1, array('class'=>'select2 show-tick pull-right', 'data-style'=>'blue', 'id'=>'select-resumen-oficina'))  !!}
            @endif
        </div>

        <div class="col-md-3">
            @foreach($asignados as $a)
                <?php $u[$a->id] = $a->full_name; ?>
            @endforeach
            <?php $u['all'] = 'TODOS'; ?>
            <?php $u['all-off'] = 'NO ACTIVOS'; ?>
            <?php $u1 = ($uresumen?$uresumen:'all'); ?>
            {!! Form::select('asignados', $u, $u1, array('class'=>'select2 show-tick pull-right', 'data-style'=>'green', 'id'=>'select-resumen-filtro'))  !!}
        </div>


        <?php
        /*
            $asignadosOff = \VCN\Models\User::asignadosOff()->get();
            if((int)$oresumen>0)
            {
                $asignadosOff = $asignadosOff->where('oficina_id', (int)$oresumen);
            }
        
            <script type="text/javascript">

                    $(document).ready( function(){

                        <?php
                            if($asignadosOff->count())
                            {
                                ?>
                                $('#select-resumen-filtro').append('<optgroup label="NO ACTIVOS:"></optgroup>');
                                <?php
                            }

                            foreach($asignadosOff as $a)
                            {
                                $u = $a->full_name;

                                $opt = "<option value='". $a->id ."'>". $u ."</option>";
                                ?>
                                $('#select-resumen-filtro').append("{!! $opt !!}");
                                <?php
                            }
                        ?>
                    
                    });
            </script>
        */
        ?>

        @include('includes.script_filtro_asignados')

    </div>
    </div>

    <br>

    @if(!isset($error))

    <?php
    $grafico5d = [];
    $grafico10d = [];
    $grafico15d = [];
    $grafico30d = [];
    $grafico1m = [];
    ?>

    <div class="portlet box blue-ebonyclay">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-bar-chart fa-fw"></i> INFORME DE SEGUIMIENTO DE LEADS POR Origen
            </div>
            <div class="tools">
                <a href="" class="collapse"></a>
                <a href="" class="fullscreen"></a>
            </div>

        </div>

        <div class="portlet-body flip-scroll">

            <table class="table table-bordered table-striped table-condensed flip-content">
                <caption></caption>
                <thead class="flip-content">
                    <tr>
                        <th></th>
                        <th>TOTAL</th>
                        @foreach($status as $st)
                            <th>{{$st->name}}</th>
                            <th>%</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>

                    <?php $total = $viajeros['total']; ?>
                    <tr>
                        <td><strong>TOTAL</strong></td>
                        <td><strong>{{$total}}</strong></td>
                        @foreach($status as $st)
                            <?php $tot = $viajeros['status'][$st->id]; ?>
                            <td>{{$tot}}</td>
                            <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                        @endforeach
                    </tr>

                    @foreach($origenes as $origen)
                        <?php $total = $viajeros['origen'][$origen->id]['status'][0]; ?>
                        @if($total>0)
                        <tr>
                            <td><strong>{{$origen->name}}</strong></td>
                            <td><strong>{{$total}}</strong></td>
                            @foreach($status as $st)
                                <?php $tot = $viajeros['origen'][$origen->id]['status'][$st->id]; ?>
                                <td>{{$tot}}</td>
                                <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                            @endforeach
                        </tr>
                        @foreach($origen->suborigenes as $suborigen)
                            <?php $total = $viajeros['suborigen'][$suborigen->id]['status'][0]; ?>
                            @if($total>0)
                            <tr>
                                <td>&nbsp;> {{$suborigen->name}}</td>
                                <td>{{$total}}</td>
                                @foreach($status as $st)
                                    <?php $tot = $viajeros['suborigen'][$suborigen->id]['status'][$st->id]; ?>
                                    <td>{{$tot}}</td>
                                    <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                                @endforeach
                            </tr>
                            @foreach($suborigen->suborigenesdet as $suborigendet)
                                <?php $total = $viajeros['suborigendet'][$suborigendet->id]['status'][0]; ?>
                                @if($total>0)
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;>> {{$suborigendet->name}}</td>
                                    <td>{{$total}}</td>
                                    @foreach($status as $st)
                                        <?php $tot = $viajeros['suborigendet'][$suborigendet->id]['status'][$st->id]; ?>
                                        <td>{{$tot}}</td>
                                        <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                                    @endforeach
                                </tr>
                                @endif
                            @endforeach
                            @endif
                        @endforeach
                        @endif
                    @endforeach

                </tbody>
            </table>

        </div>

    </div>


    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-bar-chart fa-fw"></i> INFORME DE SEGUIMIENTO DE LEADS POR Categoría (Solicitudes)
            </div>
            <div class="tools">
                <a href="" class="collapse"></a>
                <a href="" class="fullscreen"></a>
            </div>

        </div>

        <div class="portlet-body flip-scroll">

            <table class="table table-bordered table-striped table-condensed flip-content">
                <caption></caption>
                <thead class="flip-content">
                    <tr>
                        <th></th>
                        <th>TOTAL</th>
                        @foreach($status as $st)
                            <th>{{$st->name}}</th>
                            <th>%</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>

                    <?php $total = $solicitudes['total']; ?>
                    <tr>
                        <td><strong>TOTAL</strong></td>
                        <td><strong>{{$total}}</strong></td>
                        @foreach($status as $st)
                            <?php $tot = $solicitudes['status'][$st->id]; ?>
                            <td>{{$tot}}</td>
                            <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                        @endforeach
                    </tr>

                    @foreach($categorias as $categoria)
                        <?php $total = $solicitudes['category'][$categoria->id]['status'][0]; ?>
                        @if($total>0)
                        <tr>
                            <td><strong>{{$categoria->name}}</strong></td>
                            <td><strong>{{$total}}</strong></td>
                            @foreach($status as $st)
                                <?php $tot = $solicitudes['category'][$categoria->id]['status'][$st->id]; ?>
                                <td>{{$tot}}</td>
                                <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                            @endforeach
                        </tr>
                        @foreach($categoria->subcategorias as $subcategoria)
                            <?php $total = $solicitudes['subcategory'][$subcategoria->id]['status'][0]; ?>
                            @if($total>0)
                            <tr>
                                <td>&nbsp;> {{$subcategoria->name}}</td>
                                <td>{{$total}}</td>
                                @foreach($status as $st)
                                    <?php $tot = $solicitudes['subcategory'][$subcategoria->id]['status'][$st->id]; ?>
                                    <td>{{$tot}}</td>
                                    <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                                @endforeach
                            </tr>
                            @foreach($subcategoria->subcategoriasdet as $subcategoriadet)
                                <?php $total = $solicitudes['subcategory_det'][$subcategoriadet->id]['status'][0]; ?>
                                @if($total>0)
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;>> {{$subcategoriadet->name}}</td>
                                    <td>{{$total}}</td>
                                    @foreach($status as $st)
                                        <?php $tot = $solicitudes['subcategory_det'][$subcategoriadet->id]['status'][$st->id]; ?>
                                        <td>{{$tot}}</td>
                                        <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                                    @endforeach
                                </tr>
                                @endif
                            @endforeach
                            @endif
                        @endforeach
                        @endif
                    @endforeach

                </tbody>
            </table>

        </div>

    </div>

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-bar-chart fa-fw"></i> INFORME de Fecha último contacto
            </div>
            <div class="tools">
                <a href="" class="collapse"></a>
                <a href="" class="fullscreen"></a>
            </div>

        </div>

        <div class="portlet-body flip-scroll">

            <table class="table table-bordered table-striped table-condensed flip-content">
                <caption></caption>
                <thead class="flip-content">
                    <tr>
                        <th></th>
                        <th>TOTAL</th>
                        @foreach($status as $st)
                            @if($st->id != 0)
                            <th>{{$st->name}}</th>
                            <th>%</th>
                            @endif
                        @endforeach
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <?php $total = $seguimientos['5d']->count(); ?>
                        <th>Últimos 5 días</th>
                        <th>{{$total}}</th>
                        @foreach($status as $st)
                            @if($st->id != 0)
                            <?php $tot = $seguimientos['5d']->where('status_id',$st->id)->count(); ?>
                            <?php $grafico5d[$st->name] = $tot; ?>
                            <td>{{$tot}}</td>
                            <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                            @endif
                        @endforeach
                    </tr>

                    <tr>
                        <?php $total = $seguimientos['10d']->count(); ?>
                        <th>Entre 5 y 10 días</th>
                        <th>{{$total}}</th>
                        @foreach($status as $st)
                            @if($st->id != 0)
                            <?php $tot = $seguimientos['10d']->where('status_id',$st->id)->count(); ?>
                            <?php $grafico10d[$st->name] = $tot; ?>
                            <td>{{$tot}}</td>
                            <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                            @endif
                        @endforeach
                    </tr>

                    <tr>
                        <?php $total = $seguimientos['15d']->count(); ?>
                        <th>Entre 10 y 15 días</th>
                        <th>{{$total}}</th>
                        @foreach($status as $st)
                            @if($st->id != 0)
                            <?php $tot = $seguimientos['15d']->where('status_id',$st->id)->count(); ?>
                            <?php $grafico15d[$st->name] = $tot; ?>
                            <td>{{$tot}}</td>
                            <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                            @endif
                        @endforeach
                    </tr>

                    <tr>
                        <?php $total = $seguimientos['30d']->count(); ?>
                        <th>Entre 15 días y 1 mes</th>
                        <th>{{$total}}</th>
                        @foreach($status as $st)
                            @if($st->id != 0)
                            <?php $tot = $seguimientos['30d']->where('status_id',$st->id)->count(); ?>
                            <?php $grafico30d[$st->name] = $tot; ?>
                            <td>{{$tot}}</td>
                            <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                            @endif
                        @endforeach
                    </tr>

                    <tr>
                        <?php $total = $seguimientos['1m']->count(); ?>
                        <th>Más de 1 mes</th>
                        <th>{{$total}}</th>
                        @foreach($status as $st)
                            @if($st->id != 0)
                            <?php $tot = $seguimientos['1m']->where('status_id',$st->id)->count(); ?>
                            <?php $grafico1m[$st->name] = $tot; ?>
                            <td>{{$tot}}</td>
                            <td>{{($total>0)?round(($tot/$total),2)*100:0}}%</td>
                            @endif
                        @endforeach
                    </tr>

                </tbody>
            </table>

            <center>
                <div id="grafico1" style="width:440px;height:342px; border:1px solid black;"></div>
            </center>

        </div>

    </div>

</div>
</div>

<script type="text/javascript">

    var $status = [];
    @foreach($status as $st)
        $status.push("{{$st->name}}");
    @endforeach

    var $data5d = [];
    @foreach( $grafico5d as $gk=>$gv)
        $data5d.push({{$gv}});
    @endforeach

    var $data10d = [];
    @foreach( $grafico10d as $gk=>$gv)
        $data10d.push({{$gv}});
    @endforeach

    var $data15d = [];
    @foreach( $grafico15d as $gk=>$gv)
        $data15d.push({{$gv}});
    @endforeach

    var $data30d = [];
    @foreach( $grafico30d as $gk=>$gv)
        $data30d.push({{$gv}});
    @endforeach

    var $data1m = [];
    @foreach( $grafico1m as $gk=>$gv)
        $data1m.push({{$gv}});
    @endforeach

    Morris.Bar({
      element: 'grafico1',
      data: [
        { y: 'Últimos 5 días',      a:$data5d[0], b:$data5d[1], c:$data5d[2], d:$data5d[3], e:$data5d[4] },
        { y: 'Entre 5 y 10 días',   a:$data10d[0], b:$data10d[1], c:$data10d[2], d:$data10d[3], e:$data10d[4] },
        { y: 'Entre 10 y 15 días',  a:$data15d[0], b:$data15d[1], c:$data15d[2], d:$data15d[3], e:$data15d[4] },
        { y: 'Entre 15 y 30 días',  a:$data30d[0], b:$data30d[1], c:$data30d[2], d:$data30d[3], e:$data30d[4] },
        { y: 'Más de 1 mes',        a:$data1m[0], b:$data1m[1], c:$data1m[2], d:$data1m[3], e:$data1m[4] },
      ],
      xkey: 'y',
      ykeys: ['a', 'b', 'c', 'd', 'e'],
      labels: [ $status[0], $status[1],$status[2], $status[3],$status[4] ],
      resize: true,
      axes: false,
    });


$(document).ready(function() {

    // $.plot("#grafico1", [ $data5d, $data10d, $data15d, $data30d, $data1m ], {
    //     series: {
    //         stack: true,
    //         bars: {
    //             show: true,
    //             barWidth: 0.6,
    //             align: "center"
    //         }
    //     },
    //     xaxis: {
    //         mode: "categories",
    //         tickLength: 0
    //     }
    // });



});
</script>

@endif

@stop

