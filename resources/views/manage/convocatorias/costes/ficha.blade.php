@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-credit-card fa-fw"></i>
                    <a href="{{route('manage.convocatorias.abiertas.ficha',$ficha->convocatory_id)}}#costes">
                    Convocatoria Abierta: ({{$ficha->convocatoria->convocatory_open_name}})
                    </a>
                    Coste por Temporada :: {{$ficha->convocatory_open_cost_name}}
            </div>
            <div class="panel-body">

                {!! Form::model($ficha, array('route' => array('manage.convocatorias.costes.ficha', $ficha->id))) !!}

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'convocatory_id', 'texto'=> 'Convocatoria', 'valor'=> $ficha->convocatory_id, 'select'=> $convocatorias])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'convocatory_open_cost_name', 'texto'=> 'Nombre'])
                    </div>

                    {{--
                    {!! Form::label('Validez') !!}
                    <div class="form-group">
                        <div class="col-md-6">
                            @include('includes.form_input_datetime', [ 'campo'=> 'convocatory_open_cost_start_date', 'texto'=> 'Fecha Inicio', 'valor'=> Carbon::parse($ficha->convocatory_open_cost_start_date)])
                        </div>
                        <div class="col-md-6">
                            @include('includes.form_input_datetime', [ 'campo'=> 'convocatory_open_cost_end_date', 'texto'=> 'Fecha Final', 'valor'=> Carbon::parse($ficha->convocatory_open_cost_end_date)])
                        </div>
                    </div>
                    --}}

                    {!! Form::label('Precio por Rango Semanas') !!}
                    <div class="form-group">
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'convocatory_open_cost_first_week_range', 'texto'=> 'Desde'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'convocatory_open_cost_second_week_range', 'texto'=> 'Hasta'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'convocatory_open_cost_price_per_range', 'texto'=> 'Precio'])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'convocatory_open_deals_commision', 'texto'=> 'Comisión'])
                    </div>

                    <div class="form-group pull-right">
                        {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                        <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                    </div>

                {!! Form::close() !!}


            </div>
        </div>

@stop