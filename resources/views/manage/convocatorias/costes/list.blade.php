<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-credit-card"></i> Costes Temporada

            <span class="pull-right"><a href="{{ route('manage.convocatorias.costes.nuevo', $convocatoria_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Coste Por Temporada Convocatoria Cerrada</a></span>

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'convocatoria' => 'Convocatoria',
                  'name'        => 'Coste',
                  'periodo'      => 'Periodo',
                  'rango'      => 'Rango',
                  'precio' => 'Precio',
                  'options' => ''

                ])
                ->setUrl( route('manage.convocatorias.costes.index', $convocatoria_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [5] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>