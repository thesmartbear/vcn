@extends('layouts.manage')


@section('container')

    @include('manage.convocatorias.costes.list', ['convocatoria_id'=> $convocatoria_id])

@stop