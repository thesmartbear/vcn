@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bed fa-fw"></i>
                    @if($curso)
                        <a href="{{route('manage.cursos.ficha',$curso->id)}}#convocatorias-abiertas">
                            Curso: ({{$curso->name}})
                        </a>
                    @else
                        Curso: (Seleccionar)
                    @endif
                    Convocatoria Abierta :: Nueva
            </div>
            <div class="panel-body">

                {!! Form::open(array('route' => array('manage.convocatorias.abiertas.ficha', 0))) !!}

                    @if($curso)
                        {!! Form::hidden('course_id', $curso->id) !!}
                    @else
                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'course_id', 'texto'=> 'Curso', 'valor'=> 0, 'select'=> $cursos])
                        </div>
                    @endif

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'convocatory_open_name', 'texto'=> 'Nombre convocatoria'])
                    </div>

                    <div class="form-group">
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'convocatory_open_start_day', 'texto'=> 'Día Inicio', 'valor'=> 0, 'select'=> $dias])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'convocatory_open_end_day', 'texto'=> 'Día Fin', 'valor'=> 0, 'select'=> $dias])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_datetime', [ 'campo'=> 'convocatory_open_valid_start_date', 'texto'=> 'Fecha Inicio'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_datetime', [ 'campo'=> 'convocatory_open_valid_end_date', 'texto'=> 'Fecha Final'])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'convocatory_open_currency_id', 'texto'=> 'Moneda', 'valor'=> ($curso?$curso->centro->moneda_id:0), 'select'=> $monedas])
                    </div>

                    <div class="form-group row col-md-12">

                        <div class="portlet box blue-ebonyclay">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-tasks"></i> Precio incluye:
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"></a>
                                    <a href="" class="fullscreen"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                @include('manage.cursos.incluye_list', ['ficha'=> null, 'listado'=> $incluyes ])
                            </div>
                        </div>

                        @include('includes.form_input_text', [ 'campo'=> 'incluye_horario', 'texto'=> 'Horario idiomas'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'convocatory_open_price_include', 'texto'=> 'Precio incluye (otras opciones)'])
                    </div>

                    @include('includes.form_booking_reserva')

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'convocatory_open_code', 'texto'=> 'Código Contable'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select_multi', [ 'campo'=> 'monitores', 'texto'=> 'Monitores', 'valor'=> [], 'select'=> \VCN\Models\Monitores\Monitor::plataforma()->pluck('name','id')->toArray() ])
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'convocatory_open_status', 'texto'=> 'Activa'])
                    </div>

                    <hr>
                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'area', 'texto'=> 'Área Cliente', 'valor'=>1 ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'area_pagos', 'texto'=> 'Área Cliente (Pagos)', 'valor'=>1 ])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_checkbox', [ 'campo'=> 'area_reunion', 'texto'=> 'Área Cliente (Reunión)', 'valor'=> 1 ])
                        </div>
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'precios', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop