@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.convocatorias.abiertas.ficha', $ficha) !!}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-book fa-fw"></i>
                    <a href="{{route('manage.cursos.ficha',$ficha->course_id)}}#convocatorias-cerradas">
                    Curso: ({{$ficha->curso->course_name}})
                    </a>
                    Convocatoria Abierta :: {{$ficha->convocatory_open_name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Convocatoria</a></li>
                    {{-- <li role="presentation"><a href="#costes" aria-controls="costes" role="tab" data-toggle="tab">Costes Temporada</a></li> --}}

                    @if( ConfigHelper::canEdit('precios-cabierta')  )
                    <li role="presentation"><a href="#precios" aria-controls="precios" role="tab" data-toggle="tab">Precios</a></li>
                    <li role="presentation"><a href="#precio-extras" aria-controls="precios" role="tab" data-toggle="tab">Precios Extra temporada</a></li>
                    @endif

                    <li role="presentation"><a href="#ofertas" aria-controls="ofertas" role="tab" data-toggle="tab">Ofertas</a></li>

                    @if(!$ficha->reunion_no)
                    <li role="presentation"><a data-label="Reuniones" href="#reuniones" aria-controls="reuniones" role="tab" data-toggle="tab"><i class="fa fa-comments"></i></a></li>
                    @endif

                    <li role="presentation"><a data-label="Documentos" href="#docs" aria-controls="docs" role="tab" data-toggle="tab"><i class="fa fa-paperclip fa-fw"></i></a></li>

                    <li role="presentation"><a data-label="Cuestionarios" href="#cuestionarios" aria-controls="cuestionarios" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>
                    <li role="presentation"><a data-label="Tests" href="#tests" aria-controls="tests" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>

                    <li role="presentation"><a href="#condiciones" aria-controls="condiciones" role="tab" data-toggle="tab"> Condiciones</a></li>
                    <li role="presentation"><a href="#contactos_sos" aria-controls="contactos_sos" role="tab" data-toggle="tab"> Contactos SOS</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                    {!! Form::model($ficha, array('route' => array('manage.convocatorias.abiertas.ficha', $ficha->id))) !!}
                        <div class="well bg-success">
                            @include('includes.form_checkbox', [ 'campo'=> 'convocatory_open_status', 'texto'=> 'Activa'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'course_id', 'texto'=> 'Curso', 'valor'=> $ficha->course_id, 'select'=> $cursos])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'convocatory_open_name', 'texto'=> 'Nombre convocatoria'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                @include('includes.form_select', [ 'campo'=> 'convocatory_open_start_day', 'texto'=> 'Día Inicio', 'valor'=> $ficha->convocatory_open_start_day, 'select'=> $dias])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_select', [ 'campo'=> 'convocatory_open_end_day', 'texto'=> 'Día Fin', 'valor'=> $ficha->convocatory_open_end_day, 'select'=> $dias])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_input_datetime', [ 'campo'=> 'convocatory_open_valid_start_date', 'texto'=> 'Fecha Inicio', 'valor'=> ($ficha->convocatory_open_valid_start_date?Carbon::parse($ficha->convocatory_open_valid_start_date)->format('d/m/Y'):'')])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_input_datetime', [ 'campo'=> 'convocatory_open_valid_end_date', 'texto'=> 'Fecha Final', 'valor'=> ($ficha->convocatory_open_valid_end_date?Carbon::parse($ficha->convocatory_open_valid_end_date)->format('d/m/Y'):'')])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'convocatory_open_currency_id', 'texto'=> 'Moneda', 'valor'=> $ficha->convocatory_open_currency_id, 'select'=> $monedas])
                        </div>

                        <div class="form-group row col-md-12">

                            <div class="portlet box blue-ebonyclay">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-tasks"></i> Precio incluye:
                                    </div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a>
                                        <a href="" class="fullscreen"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    @include('manage.cursos.incluye_list', ['ficha'=> $ficha, 'listado'=> $incluyes ])
                                </div>
                            </div>

                            @include('includes.form_input_text', [ 'campo'=> 'incluye_horario', 'texto'=> 'Horario idiomas'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'convocatory_open_price_include', 'texto'=> 'Precio incluye (otras opciones)'])
                        </div>

                        @include('includes.form_booking_reserva')

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'convocatory_open_code', 'texto'=> 'Código Contable'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'reunion_no', 'texto'=> 'Sin Reunión informativa'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select_multi', [ 'campo'=> 'monitores', 'texto'=> 'Monitores', 'valor'=> $ficha->monitores->pluck('monitor_id')->toArray(), 'select'=> \VCN\Models\Monitores\Monitor::plataforma()->pluck('name','id')->toArray() ])
                        </div>

                        <hr>
                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'area', 'texto'=> 'Área Cliente' ])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'area_pagos', 'texto'=> 'Área Cliente (Pagos)' ])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_checkbox', [ 'campo'=> 'area_reunion', 'texto'=> 'Área Cliente (Reunión)' ])
                            </div>
                        </div>

                        @include('includes.form_submit', [ 'permiso'=> 'precios', 'texto'=> 'Guardar'])

                    {!! Form::close() !!}

                    </div>

                    {{-- <div role="tabpanel" class="tab-pane fade in" id="costes">
                        @include('manage.convocatorias.costes.list', ['convocatoria_id'=> $ficha->id])
                    </div> --}}

                    @if( ConfigHelper::canEdit('precios-cabierta')  )
                    <div role="tabpanel" class="tab-pane fade in" id="precios">
                        @include('manage.convocatorias.precios.list', ['convocatoria_id'=> $ficha->id, 'extras'=> false])
                    </div>
                    <div role="tabpanel" class="tab-pane fade in" id="precio-extras">
                        @include('manage.convocatorias.precios.list', ['convocatoria_id'=> $ficha->id, 'extras'=> true])
                    </div>
                    @endif

                    <div role="tabpanel" class="tab-pane fade in" id="ofertas">
                        @include('manage.convocatorias.ofertas.list', ['convocatoria_id'=> $ficha->id])
                    </div>

                    @if(!$ficha->reunion_no)
                    <div role="tabpanel" class="tab-pane fade" id="reuniones">
                        <div class="col-md-12">
                            @include('manage.reuniones.list_convocatoria_curso', ['curso_id'=> $ficha->curso->id])
                        </div>
                        <div class="col-md-12">
                            @include('manage.reuniones.list_convocatoria', ['tipo'=>2, 'convocatoria_id'=> $ficha->id])
                        </div>
                        <div class="col-md-12">
                            @include('manage.reuniones.new', ['tipo'=>2, 'convocatoria_id'=> $ficha->id])
                        </div>
                    </div>
                    @endif

                    <div role="tabpanel" class="tab-pane fade in" id="docs">
                        @include('includes.documentos', ['modelo'=> 'Abierta', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="cuestionarios">
                        @include('manage.system.cuestionarios.vinculado', ['modelo'=> 'Abierta', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="tests">
                        @include('manage.exams.vinculado', ['modelo'=> 'Abierta', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="condiciones">
                        {!! Form::model($ficha, array('route' => array('manage.convocatorias.abiertas.ficha', $ficha->id)) )!!}

                        {!! Form::hidden('condiciones', 'condiciones') !!}

                        @foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)

                            @foreach(ConfigHelper::idiomas() as $keyi=>$idioma)

                                <?php

                                    $dir = "assets/uploads/pdf_condiciones/";
                                    $name = class_basename($ficha) ."_". $ficha->id;
                                    $file = $dir. $name ."_". $keyp ."_". $idioma .".pdf";

                                    $valor = null;

                                    if(isset($ficha->condiciones[$keyp][$idioma]))
                                    {
                                        $valor = $ficha->condiciones[$keyp][$idioma];
                                    }
                                ?>

                                <div class="form-group">
                                    @include('includes.form_textarea_tinymce', [ 'campo'=> "condiciones_$keyp-$idioma", 'texto'=> "Condiciones ($plataforma : $idioma)", 'valor'=> $valor])
                                    
                                    @if($valor)
                                        Ver: <a target="_blank" href="/{{$file}}">PDF</a>
                                    @endif

                                </div>

                            @endforeach

                        @endforeach

                        @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="contactos_sos">
                        @include('includes.system_contactos-tab', ['modelo'=> 'Abierta', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'Abierta',
                                'campos_text'=> [
                                    ['convocatory_open_name'=> 'Nombre convocatoria'],
                                    ['incluye_horario'=> 'Horario idiomas'],
                                ],
                                'campos_textarea'=> [
                                    ['convocatory_open_price_include'=> 'Precio incluye (otras opciones)'],
                                ]
                            ])

                    </div>

                </div>


            </div>
        </div>

@stop