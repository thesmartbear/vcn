<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-book"></i> Convocatorias Cerradas

            @if($curso_id)
              <span class="pull-right"><a href="{{ route('manage.convocatorias.cerradas.nuevo', $curso_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Convocatoria Cerrada</a></span>
            @endif

        </div>
        <div class="panel-body">

        @if(isset($anys))

            <?php
                $categorias = [0=>'Todas'] + \VCN\Models\Categoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
                $subcategorias = [0=>'Todas'] + \VCN\Models\Subcategoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
                // $subcategoriasdet = [0=>'Todas'] + SubcategoriaDetalle::plataforma()->sortBy('name')->pluck('name','id')->toArray();
                // $cursos = [0=>'Todos'] + Curso::plataforma()->where('activo_web',1)->sortBy('name')->pluck('name','id')->toArray();
            ?>

            {!! Form::open(['route' => array('manage.convocatorias.cerradas.index'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-1">
                        {!! Form::label('any', 'Año') !!}
                        <br>
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any'))  !!}
                    </div>

                    <div class="col-md-4">
                        {!! Form::label('categorias', 'Categoría') !!}
                        @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                        <br>
                        {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                        @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                        </div>

                        <div class="col-md-4">
                        {!! Form::label('subcategorias', 'SubCategoría') !!}
                        @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                        <br>
                        {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
                    </div>

                    <div class="col-md-1">
                        {!! Form::label('&nbsp;') !!}
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>
                </div>

                @if(ConfigHelper::canEdit("desactivacion-bloque"))
                <hr>
                  {!! Form::submit('Desactivar Activas', array( 'name'=> 'desactivar', 'class' => 'btn btn-warning')) !!}
                  {!! Form::submit('Desactivar Activas Web', array( 'name'=> 'desactivar-web', 'class' => 'btn btn-warning')) !!}
                @endif

            {!! Form::close() !!}
            <hr>


            @if(!$listado)
                <br>
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                {!! Datatable::table()
                    ->addColumn([
                      'nombre'      => 'Nombre',
                      'activo'      => 'Activo',
                      'activo_web'  => 'Activo Web',
                      'curso'      => 'Curso',
                      'duracion_unit' => 'Ud. duración',
                      'convocatory_close_code' => 'Cod.Contable',
                      'options' => ''

                    ])
                    ->setUrl( route('manage.convocatorias.cerradas.index', $valores) )
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [5] ]
                      )
                    )
                    ->render() !!}

            @endif

        @else

            {!! Datatable::table()
                    ->addColumn([
                      'nombre'      => 'Nombre',
                      'activo'      => 'Activo',
                      'activo_web'  => 'Activo Web',
                      'curso'      => 'Curso',
                      'duracion_unit' => 'Ud. duración',
                      'convocatory_close_code' => 'Cod.Contable',
                      'options' => ''

                    ])
                    ->setUrl( route('manage.convocatorias.cerradas.index', $curso_id) )
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [5] ]
                      )
                    )
                    ->render() !!}

        @endif

        </div>
    </div>

</div>