@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.convocatorias.cerradas.ficha', $ficha) !!}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-book fa-fw"></i>
                    <a href="{{route('manage.cursos.ficha',$ficha->course_id)}}#convocatorias-cerradas">
                    Curso: ({{$ficha->curso->course_name}})
                    </a>
                    Convocatoria Cerrada :: {{$ficha->convocatory_close_name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Convocatoria</a></li>
                    <li role="presentation"><a href="#vuelos" aria-controls="vuelos" role="tab" data-toggle="tab"><i class="fa fa-plane"></i> Vuelos</a></li>
                    <li role="presentation"><a href="#plazas" aria-controls="plazas" role="tab" data-toggle="tab">Plazas</a></li>

                    @if( ConfigHelper::canEdit('precios-ccerrada')  )
                    <li role="presentation"><a href="#precio" aria-controls="precio" role="tab" data-toggle="tab">Precio</a></li>
                    @endif

                    @if(!$ficha->reunion_no)
                    <li role="presentation"><a data-label="Reuniones" href="#reuniones" aria-controls="reuniones" role="tab" data-toggle="tab"><i class="fa fa-comments"></i></a></li>
                    @endif

                    <li role="presentation"><a data-label="Documentos" href="#docs" aria-controls="docs" role="tab" data-toggle="tab"><i class="fa fa-paperclip fa-fw"></i></a></li>

                    <li role="presentation"><a data-label="Cuestionarios" href="#cuestionarios" aria-controls="cuestionarios" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>
                    <li role="presentation"><a data-label="Tests" href="#tests" aria-controls="tests" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>

                    @if( ConfigHelper::config('facturas') && ConfigHelper::canEdit('facturas')  )
                    <li role="presentation"><a data-label="Facturas" href="#facturas" aria-controls="facturas" role="tab" data-toggle="tab"><i class="fa fa-ticket fa-fw"></i></a></li>
                    @endif
                    
                    <li role="presentation"><a href="#condiciones" aria-controls="condiciones" role="tab" data-toggle="tab"> Condiciones</a></li>
                    <li role="presentation"><a href="#contactos_sos" aria-controls="contactos_sos" role="tab" data-toggle="tab"> Contactos SOS</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                    {!! Form::model($ficha, array('route' => array('manage.convocatorias.cerradas.ficha', $ficha->id))) !!}

                        <div class="well bg-success">
                            <div class="row">
                                <div class="col-md-3">
                                    @include('includes.form_checkbox', [ 'campo'=> 'convocatory_close_status', 'texto'=> 'Activa'])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_checkbox', [ 'campo'=> 'activo_web', 'texto'=> 'Activo Web'])
                                </div>
                            </div>
                        </div>

                        <div class="well">
                            @include('includes.form_checkbox', [ 'campo'=> 'activo_corporativo', 'texto'=> 'Convocatoria corporativa'])

                            <div id="corporativa" class="form-group" style="display:none;">
                                [{{ConfigHelper::config('area_url')?:ConfigHelper::config('web')}}/booking-corporativo/{{$ficha->curso->id}}/{{$ficha->id}}]
                                <br><br>
                                <div class="row">
                                    <div class="col-md-1">
                                        @include('includes.form_input_number', [ 'campo'=> "corporativo_info[caducidad]", 'texto'=> 'Caducidad', 'valor'=> (isset($ficha->corporativo_info['caducidad']) ? $ficha->corporativo_info['caducidad'] : "") ])
                                    </div>
                                    <div class="col-md-4">
                                        @include('includes.form_select', [ 'campo'=> "corporativo_info[oficina]", 'texto'=> 'Oficina', 'select'=> $oficinas , 'valor'=> (isset($ficha->corporativo_info['oficina']) ? $ficha->corporativo_info['oficina'] : "") ])
                                    </div>
                                    <div class="col-md-4">
                                        @include('includes.form_select', [ 'campo'=> "corporativo_info[user_id]", 'texto'=> 'Asignado', 'select'=> $asignados , 'valor'=> (isset($ficha->corporativo_info['user_id']) ? $ficha->corporativo_info['user_id'] : "") ])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_select', [ 'campo'=> "corporativo_info[pago]", 'texto'=> 'Pago', 'select'=> [0=>"Sin Pago", 1=>"Transferencia", 2=>"Transf. + TPV"] , 'valor'=> (isset($ficha->corporativo_info['pago']) ? $ficha->corporativo_info['pago'] : "") ])
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        @include('includes.form_input_text', [ 'campo'=> "corporativo_info[banco]", 'texto'=> 'Banco', 'valor'=> (isset($ficha->corporativo_info['banco']) ? $ficha->corporativo_info['banco'] : "") ])
                                    </div>
                                    <div class="col-md-3">
                                        @include('includes.form_input_text', [ 'campo'=> "corporativo_info[iban]", 'texto'=> 'IBAN', 'valor'=> (isset($ficha->corporativo_info['iban']) ? $ficha->corporativo_info['iban'] : "") ])
                                    </div>
                                    <div class="col-md-3">
                                        @include('includes.form_input_text', [ 'campo'=> "corporativo_info[titular]", 'texto'=> 'Titular', 'valor'=> (isset($ficha->corporativo_info['titular']) ? $ficha->corporativo_info['titular'] : "") ])
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        @include('includes.form_select2', [ 'campo'=> 'corporativo_info[prescriptor_id]', 'texto'=> 'Prescriptor', 'select'=> $prescriptores])
                                    </div>
                                    <div class="col-md-3">
                                        @include('includes.form_select2', [ 'campo'=> 'origen_id', 'texto'=> 'Origen', 'select'=> $conocidos, 'valor'=> (isset($ficha->corporativo_info['origen_id']) ? $ficha->corporativo_info['origen_id'] : "")])
                                    </div>
                                    <div class="col-md-3">
                                        @include('includes.form_select2', [ 'campo'=> 'suborigen_id', 'texto'=> 'Sub-Origen', 'select'=> $conocidosSub, 'valor'=> (isset($ficha->corporativo_info['suborigen_id']) ? $ficha->corporativo_info['suborigen_id'] : "")])
                                    </div>
                                    <div class="col-md-3">
                                        @include('includes.form_select2', [ 'campo'=> 'suborigendet_id', 'texto'=> 'Sub-Origen Detalle', 'select'=> $conocidosSubD, 'valor'=> (isset($ficha->corporativo_info['suborigendet_id']) ? $ficha->corporativo_info['suborigendet_id'] : "")])
                                    </div>
                                </div>
                                @include('includes.script_conocido')
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'course_id', 'texto'=> 'Curso', 'valor'=> $ficha->course_id, 'select'=> $cursos])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'convocatory_close_name', 'texto'=> 'Nombre convocatoria'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'alojamiento_id', 'texto'=> 'Alojamiento', 'help'=> 'Importante: Depende del curso. Si éste cambia revise este campo.', 'select'=> $alojamientos])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                @include('includes.form_input_datetime', [ 'campo'=> 'convocatory_close_start_date', 'texto'=> 'Fecha Inicio', 'valor'=> Carbon::parse($ficha->convocatory_close_start_date)->format('d/m/Y')])
                            </div>
                            <div class="col-md-6">
                                @include('includes.form_input_datetime', [ 'campo'=> 'convocatory_close_end_date', 'texto'=> 'Fecha Final', 'valor'=> Carbon::parse($ficha->convocatory_close_end_date)->format('d/m/Y')])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'convocatory_semiopen', 'texto'=> 'Convocatoria Semicerrada'])
                        </div>

                        <div id="semicerrada" class="form-group row" style="display:none;">
                            <div class="col-md-6">
                                @include('includes.form_select', [ 'campo'=> 'convocatory_semiopen_start_day', 'texto'=> 'Empieza el', 'valor'=> $ficha->convocatory_semiopen_start_day, 'select'=> $dias])
                            </div>
                            <div class="col-md-6">
                                @include('includes.form_select', [ 'campo'=> 'convocatory_semiopen_end_day', 'texto'=> 'Acaba el', 'valor'=> $ficha->convocatory_semiopen_end_day, 'select'=> $dias])
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                @include('includes.form_input_number', [ 'campo'=> 'convocatory_close_duration_weeks', 'texto'=> 'Duración'])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_select', [ 'campo'=> 'duracion_fijo', 'texto'=> 'Unidad duración',
                                    'select'=> ConfigHelper::getPrecioDuracionUnitCerrada()])
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-money"></i> Precio:
                                        </div>
                                    </div>
                                    <div class="portlet-body">

                                        {{ConfigHelper::parseMoneda($ficha->precio, $ficha->moneda_name)}}

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                @include('includes.form_select', [ 'campo'=> 'dto_early', 'texto'=> 'Descuento Early Bird',
                                    'select'=> $descuentos_early])
                            </div>
                        </div>

                        {{-- <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_select', [ 'campo'=> 'convocatory_close_currency_id', 'texto'=> 'Moneda', 'valor'=> $ficha->convocatory_close_currency_id, 'select'=> $monedas])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'convocatory_close_price', 'texto'=> 'Precio'])
                            </div>
                        </div> --}}

                        {{-- <div class="form-group row">
                            <div class="col-md-6">
                                @include('includes.form_input_number', [ 'campo'=> 'plazas', 'texto'=> 'Plazas'])
                            </div>
                            <div class="col-md-6">
                                @include('includes.form_input_number', [ 'campo'=> 'plazas_reservadas', 'texto'=> 'Plazas reservadas'])
                            </div>
                        </div> --}}

                        <div class="form-group row col-md-12">

                            <div class="portlet box blue-ebonyclay">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-tasks"></i> Precio incluye:
                                    </div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a>
                                        <a href="" class="fullscreen"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    @include('manage.cursos.incluye_list', ['ficha'=> $ficha, 'listado'=> $incluyes ])
                                </div>
                            </div>

                            @include('includes.form_input_text', [ 'campo'=> 'incluye_horario', 'texto'=> 'Horario idiomas'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'convocatory_close_price_include', 'texto'=> 'Precio incluye (otras opciones)'])
                        </div>

                        @include('includes.form_booking_reserva')

                        <div class="form-group row">
                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'convocatory_close_code', 'texto'=> 'Código Contable'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'no_facturar', 'texto'=> 'No facturar por sistema'])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'reunion_no', 'texto'=> 'Sin Reunión informativa'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select_multi', [ 'campo'=> 'monitores', 'texto'=> 'Monitores', 'valor'=> $ficha->monitores->pluck('monitor_id')->toArray(), 'select'=> \VCN\Models\Monitores\Monitor::plataforma()->pluck('name','id')->toArray() ])
                        </div>

                        <hr>
                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'area', 'texto'=> 'Área Cliente' ])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'area_pagos', 'texto'=> 'Área Cliente (Pagos)' ])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_checkbox', [ 'campo'=> 'area_reunion', 'texto'=> 'Área Cliente (Reunión)' ])
                            </div>
                        </div>


                    @include('includes.form_submit', [ 'permiso'=> 'precios', 'texto'=> 'Guardar'])

                    {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="vuelos">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-plane"></i> Asignar Vuelo:
                            </div>
                            <div class="panel-body">

                                {!! Form::open(array('route' => array('manage.convocatorias.vuelos.asignar'))) !!}

                                {!! Form::hidden('convocatory_id', $ficha->id) !!}
                                {!! Form::hidden('tab', 'vuelos') !!}

                                <div class="form-group">
                                    <div class="col-md-10">
                                        @include('includes.form_select2', [ 'campo'=> 'vuelo_id', 'texto'=> null, 'select'=> $vuelos])
                                    </div>
                                    <div class="col-md-2">
                                        {!! Form::submit('Añadir', array('class' => 'btn btn-success')) !!}
                                    </div>
                                </div>

                                {!! Form::close() !!}

                            </div>

                        </div>

                        <hr>
                        @include('manage.convocatorias.vuelos.list', ['convocatoria_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="plazas">
                        @include('manage.convocatorias.cerradas.ficha-plazas', ['convocatoria'=> $ficha])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="precio">
                        @include('manage.convocatorias.cerradas.ficha-precio', ['convocatoria'=> $ficha])
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="reuniones">
                        <div class="col-md-12">
                            @include('manage.reuniones.list_convocatoria', ['tipo'=>1, 'convocatoria_id'=> $ficha->id])
                        </div>
                        <div class="col-md-12">
                            @include('manage.reuniones.new', ['tipo'=>1, 'convocatoria_id'=> $ficha->id])
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="docs">
                        @include('includes.documentos', ['modelo'=> 'Cerrada', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="cuestionarios">
                        @include('manage.system.cuestionarios.vinculado', ['modelo'=> 'Cerrada', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="tests">
                        @include('manage.exams.vinculado', ['modelo'=> 'Cerrada', 'modelo_id'=> $ficha->id])
                    </div>

                    @if( ConfigHelper::config('facturas') && ConfigHelper::canEdit('facturas')  )
                    <div role="tabpanel" class="tab-pane fade in" id="facturas">
                        @include('manage.convocatorias.facturas', ['modelo'=> 'Cerrada', 'modelo_id'=> $ficha->id])
                    </div>
                    @endif

                    <div role="tabpanel" class="tab-pane fade in" id="condiciones">
                        {!! Form::model($ficha, array('route' => array('manage.convocatorias.cerradas.ficha', $ficha->id)) )!!}

                        {!! Form::hidden('condiciones', 'condiciones') !!}

                        @foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)

                            @foreach(ConfigHelper::idiomas() as $keyi=>$idioma)

                                <?php

                                    $dir = "assets/uploads/pdf_condiciones/";
                                    $name = class_basename($ficha) ."_". $ficha->id;
                                    $file = $dir. $name ."_". $keyp ."_". $idioma .".pdf";

                                    $valor = null;

                                    if(isset($ficha->condiciones[$keyp][$idioma]))
                                    {
                                        $valor = $ficha->condiciones[$keyp][$idioma];
                                    }
                                ?>

                                <div class="form-group">
                                    @include('includes.form_textarea_tinymce', [ 'campo'=> "condiciones_$keyp-$idioma", 'texto'=> "Condiciones ($plataforma : $idioma)", 'valor'=> $valor])
                                    
                                    @if($valor)
                                        Ver: <a target="_blank" href="/{{$file}}">PDF</a>
                                    @endif

                                </div>

                            @endforeach

                        @endforeach

                        @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="contactos_sos">
                        @include('includes.system_contactos-tab', ['modelo'=> 'Cerrada', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'Cerrada',
                                'campos_text'=> [
                                    ['convocatory_close_name'=> 'Nombre convocatoria'],
                                    ['incluye_horario'=> 'Horario idiomas'],
                                ],
                                'campos_textarea'=> [
                                    ['convocatory_close_price_include'=> 'Precio incluye (otras opciones)'],
                                ]
                            ])

                    </div>

                </div>


            </div>
        </div>

<script type="text/javascript">
$(document).ready(function() {

    @if($ficha->convocatory_semiopen)
        $('#semicerrada').show();
    @endif

    $("#convocatory_semiopen").click(function(){
        if( $(this).is(':checked') )
        {
            $('#semicerrada').show();
        }
        else
        {
            $('#semicerrada').hide();
        }
        // $("#semicerrada").slideToggle();
    });

    @if($ficha->activo_corporativo)
        $('#corporativa').show();
    @endif

    $("#activo_corporativo").click(function(){
        if( $(this).is(':checked') )
        {
            $('#corporativa').show();
        }
        else
        {
            $('#corporativa').hide();
        }
    });

});
</script>

@stop