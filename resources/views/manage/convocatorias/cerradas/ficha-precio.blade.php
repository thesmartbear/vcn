{!! Form::open(array('method'=> 'post', 'url' => route('manage.convocatorias.cerradas.precio', $convocatoria->id))) !!}

    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_select', [ 'campo'=> 'moneda_id', 'texto'=> 'Moneda', 'valor'=> $convocatoria->convocatory_close_currency_id, 'select'=> $monedas])
        </div>
        <div class="col-md-6">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-money"></i> Precio:
                    </div>
                </div>
                <div class="portlet-body">

                    {{ConfigHelper::parseMoneda($convocatoria->precio, $convocatoria->moneda_name)}}

                </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-2">
            <br><br>
            <label>Coste Proveedor:</label>
        </div>
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'proveedor', 'texto'=> 'Precio', 'ficha'=> $convocatoria->precio_auto])
        </div>
        <div class="col-md-3">
            @include('includes.form_select', [ 'campo'=> 'proveedor_moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ($convocatoria->precio_auto?$convocatoria->precio_auto->proveedor_moneda_id:$convocatoria->curso->centro->moneda_id)])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-2">
            <br>
            <label>Vuelo:</label>
            <span class="badge badge-help">Suma de Precio+Tasas+Margen del Vuelo</span>
        </div>
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'vuelo', 'texto'=> 'Precio', 'valor'=> ($convocatoria->precio_auto?$convocatoria->precio_auto->vuelo:$convocatoria->precio_vuelo), 'disabled'=> true])
        </div>
        <div class="col-md-3">
            @include('includes.form_select', [ 'campo'=> 'vuelo_moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ($convocatoria->precio_auto?$convocatoria->precio_auto->vuelo_moneda_id:ConfigHelper::default_moneda_id())])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-2">
            <br><br>
            <label>Seguro:</label>
        </div>
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'seguro', 'texto'=> 'Precio', 'ficha'=> $convocatoria->precio_auto])
        </div>
        <div class="col-md-3">
            @include('includes.form_select', [ 'campo'=> 'seguro_moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ($convocatoria->precio_auto?$convocatoria->precio_auto->seguro_moneda_id:ConfigHelper::default_moneda_id())])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-2">
            <br><br>
            <label>Gastos generales:</label>
        </div>
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'gastos', 'texto'=> 'Precio', 'ficha'=> $convocatoria->precio_auto])
        </div>
        <div class="col-md-3">
            @include('includes.form_select', [ 'campo'=> 'gastos_moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ($convocatoria->precio_auto?$convocatoria->precio_auto->gastos_moneda_id:ConfigHelper::default_moneda_id())])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-2">
            <br><br>
            <label>Monitor:</label>
        </div>
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'monitor', 'texto'=> 'Precio', 'ficha'=> $convocatoria->precio_auto])
        </div>
        <div class="col-md-3">
            @include('includes.form_select', [ 'campo'=> 'monitor_moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ($convocatoria->precio_auto?$convocatoria->precio_auto->monitor_moneda_id:ConfigHelper::default_moneda_id())])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-2">
            <br><br>
            <label>MB:</label>
        </div>
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'mb', 'texto'=> 'Precio', 'ficha'=> $convocatoria->precio_auto])
        </div>
        <div class="col-md-3">
            @include('includes.form_select', [ 'campo'=> 'mb_moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ($convocatoria->precio_auto?$convocatoria->precio_auto->mb_moneda_id:ConfigHelper::default_moneda_id())])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-2">
            <br><br>
            <label>Comisión Terceros:</label>
        </div>
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'comision', 'texto'=> 'Precio', 'ficha'=> $convocatoria->precio_auto])
        </div>
        <div class="col-md-3">
            @include('includes.form_select', [ 'campo'=> 'comision_moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ($convocatoria->precio_auto?$convocatoria->precio_auto->comision_moneda_id:ConfigHelper::default_moneda_id())])
        </div>
    </div>

    <hr>
    <div class="form-group row">
        <div class="col-md-2">
            <label>Variación Divisa:</label>
            <br>{{ConfigHelper::default_moneda()->name}}
        </div>
        <div class="col-md-2">
            @include('includes.form_input_text', [ 'campo'=> 'divisa_variacion', 'texto'=> 'Importe', 'ficha'=> $convocatoria->precio_auto])
        </div>
        <div class="col-md-2">
            @include('includes.form_input_text', [ 'campo'=> 'divisa_total', 'texto'=> 'Total', 'ficha'=> $convocatoria->precio_auto, 'disabled'=> true])
        </div>
        <div class="col-md-1">
            <label>&nbsp;</label><br>
            {!! Form::submit('Establecer', array('name'=> 'submit_divisa', 'class' => 'btn btn-success')) !!}
        </div>
    </div>

    <hr>
    @include('includes.form_submit', [ 'permiso'=> 'precios-ccerrada', 'texto'=> 'Guardar'])

    <div class="clearfix"></div>

{!! Form::close() !!}

<hr>
@if(!$convocatoria->tabla_cambio)

    @include('manage.monedas.cambios.tabla', ['categoria'=> $convocatoria->curso->categoria, 'any'=> $convocatoria->any])
    <hr>

    <div class="note note-warning">
        Modificar Tipos de Cambios SÓLO PARA ESTA CONVOCATORIA. Primero elegir la tabla de TC que servirá de base para luego modificar los TC.
    </div>

    <div class="note note-danger">
        <strong>Nota:</strong> si se quiere modificar un TC y que se refleje en todas los precios de una misma categoría de curso, hay que ir en el menú a ‘Tablas Tipo de Cambio’. <strong>NO hacerlo desde aquí.</strong>
    </div>

    @if($convocatoria->curso->categoria)
    {!! Form::open(array('method'=> 'post', 'url' => route('manage.convocatorias.cerradas.cambio', $convocatoria->id))) !!}
    {!! Form::hidden('importar',1) !!}
    <div class="form-group row">
        <div class="col-md-8">
            @include('includes.form_select', [ 'campo'=> 'cambio_id', 'valor'=> $convocatoria->curso->categoria->tabla_cambio?$convocatoria->curso->categoria->tabla_cambio->id:0, 'texto'=> 'Modificar TC', 'select'=> \VCN\Models\Monedas\Cambio::where('activo',1)->pluck('name','id') ])
        </div>
        <div class="col-md-2">
            <label>&nbsp;</label><br>
            @include('includes.form_submit', [ 'permiso'=> 'monedas-cambio', 'texto'=> 'Seleccionar'])
        </div>
    </div>
    {!! Form::close() !!}
    @endif

@else

    {!! Form::open(array('method' => 'POST', 'url' => route('manage.convocatorias.cerradas.cambio',$convocatoria->id), 'role' => 'form', 'class' => '')) !!}
    {!! Form::hidden('cambio_id',$convocatoria->tabla_cambio->id) !!}
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-plus-circle fa-fw"></i> Cambios
            <a id="cambios_add" href="#cambios_add" class='btn btn-xs btn-warning' data-label="Añadir"><i class="fa fa-plus-square"></i></a>

            @if($convocatoria->tabla_cambio->cambio)
                (Importados de tabla: [{{$convocatoria->tabla_cambio->cambio->name}}])
            @endif

        </div>
        <div class="panel-body">

            <div id="cambios_div">

                @if(count($convocatoria->tabla_cambio->cambios))
                    @foreach($convocatoria->tabla_cambio->cambios as $cambios)

                        <div class="cambios_form">

                            <div class="row">
                                <div class="col-md-2">
                                    @include('includes.form_select', [ 'campo'=> 'cambios[moneda_id][]', 'texto'=> 'Moneda', 'select'=> \VCN\Models\Monedas\Moneda::all()->pluck('name','id'), 'valor'=> $cambios->moneda_id ])
                                </div>

                                <div class="col-md-2">
                                    @include('includes.form_input_number', [ 'step'=> '0.0001', 'campo'=> 'cambios[tc_calculo][]', 'texto'=> 'TC Cálculo', 'valor'=> $cambios->tc_calculo])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_input_number', [ 'step'=> '0.0001', 'campo'=> 'cambios[tc_folleto][]', 'texto'=> 'TC Folleto', 'valor'=> $cambios->tc_folleto])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_input_number', [ 'step'=> '0.0001', 'campo'=> 'cambios[tc_final][]', 'texto'=> 'TC Final', 'valor'=> $cambios->tc_final])
                                </div>
                            </div>

                        </div>

                    @endforeach

                    @include('includes.script_form_add', ['campo'=> 'cambios' ])

                    <hr>

                @else

                    <div class="cambios_form">

                        <div class="row">
                            <div class="col-md-2">
                                @include('includes.form_select', [ 'campo'=> 'cambios[moneda_id][]', 'texto'=> 'Moneda', 'select'=> \VCN\Models\Monedas\Moneda::all()->pluck('name','id') ])
                            </div>

                            <div class="col-md-2">
                                @include('includes.form_input_number', [ 'step'=> '0.0001', 'campo'=> 'cambios[tc_calculo][]', 'texto'=> 'TC Cálculo'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_input_number', [ 'step'=> '0.0001', 'campo'=> 'cambios[tc_folleto][]', 'texto'=> 'TC Folleto'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_input_number', [ 'step'=> '0.0001', 'campo'=> 'cambios[tc_final][]', 'texto'=> 'TC Final'])
                            </div>
                        </div>

                    </div>

                @endif

            </div>

            <div class="clearfix"></div>

            @include('includes.form_submit', [ 'permiso'=> 'monedas-cambio', 'texto'=> 'Guardar'])

        </div>
    </div>
    {!! Form::close() !!}
@endif