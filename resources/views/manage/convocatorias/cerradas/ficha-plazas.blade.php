<div class="col-md-12">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-puzzle-piece fa-fw"></i> Plazas
        </div>
        <div class="panel-body">

            @if(!$convocatoria->curso)
                <div class="container">
                    <div class="content">
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            Convocatoria sin Curso/s asignado/s
                        </div>
                    </div>
                </div>
            @else

                {!! Form::model($ficha, array('route' => array('manage.convocatorias.plazas.ficha', $ficha->id))) !!}
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead>
                        <tr>
                            <th>Alojamiento</th>
                            <th class="col-md-1">Plazas</th>
                            <th class="col-md-1">Reserva</th>
                            <th class="col-md-1">Pre-reserva</th>
                            <th class="col-md-1">Overbooking</th>
                            <th class="col-md-1">Disponible</th>
                            <th class="col-md-1">Bloqueadas</th>
                            <th class="col-md-1">Proveedor</th>
                            <th class="col-md-1">Online</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $convocatoria->curso->alojamientos as $alojamiento )

                            <?php
                                $plazas = $ficha->getPlazas($alojamiento->id);
                            ?>
                            <tr>
                                <th>{{$alojamiento->name}}</th>
                                <td> @include('includes.form_input_number', [ 'campo'=> "plazas_totales[$alojamiento->id]", 'texto'=> null, 'valor'=> $plazas?$plazas->plazas_totales:0])</td>
                                <td>{{$plazas ? $plazas->plazas_reservas : "?"}}</td>
                                <td>{{$plazas ? $plazas->plazas_prereservas : "?"}}</td>
                                <td>{{$plazas ? $plazas->plazas_overbooking : "?"}}</td>
                                <td>{{$plazas ? $plazas->plazas_disponibles : "?"}}</td>
                                <td> @include('includes.form_input_number', [ 'campo'=> "plazas_bloqueadas[$alojamiento->id]", 'texto'=> null, 'valor'=> $plazas?$plazas->plazas_bloqueadas:0])</td>
                                <td> @include('includes.form_input_number', [ 'campo'=> "plazas_proveedor[$alojamiento->id]", 'texto'=> null, 'valor'=> $plazas?$plazas->plazas_proveedor:0])</td>
                                <td>{{$plazas ? $plazas->plazas_online : "?"}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @if(ConfigHelper::canEdit('plazas-privado'))

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'plazas_txt', 'texto'=> 'Comentarios'])
                    </div>

                    @if($ficha->curso->propietario == ConfigHelper::config('propietario'))
                        @include('includes.form_submit', [ 'permiso'=> 'plazas-privado', 'texto'=> 'Guardar'])
                    @else
                        @include('includes.form_submit', [ 'permiso'=> 'convocatoria-plazas', 'texto'=> 'Guardar'])
                    @endif
                @else
                    @include('includes.form_submit', [ 'permiso'=> 'convocatoria-plazas', 'texto'=> 'Guardar'])
                @endif


                {!! Form::close() !!}

            @endif

        </div>
    </div>
</div>

@foreach( $convocatoria->curso->alojamientos as $alojamiento )
<div class="col-md-12">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-book fa-fw"></i> Bookings en Alojamiento {{$alojamiento->name}}
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                    'viajero'   => 'Viajero',
                    'fecha'     => 'Fecha Inicio',
                    'status'    => 'Estado',
                    'oficina'   => 'Oficina',
                ])
                ->setUrl( route('manage.bookings.index.alojamiento', [$ficha->id, $alojamiento->id]) )
                ->setOptions('iDisplayLength', 100)
                ->render() !!}

        </div>

    </div>

</div>
@endforeach