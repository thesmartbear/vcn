@extends('layouts.manage')


@section('container')

    @include('manage.convocatorias.cerradas.list', ['curso_id'=> $curso_id])

    @include('manage.convocatorias.cerradas.modal_info')

@stop