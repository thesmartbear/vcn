@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bed fa-fw"></i>
                    @if($curso)
                        <a href="{{route('manage.cursos.ficha',$curso->id)}}#convocatorias-cerradas">
                            Curso: ({{$curso->name}})
                        </a>
                    @else
                        Curso: (Seleccionar)
                    @endif
                    Convocatoria Cerrada :: Nueva
            </div>
            <div class="panel-body">

                {!! Form::open(array('route' => array('manage.convocatorias.cerradas.ficha', 0))) !!}

                    @if($curso)
                        {!! Form::hidden('course_id', $curso->id) !!}
                    @else
                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'course_id', 'texto'=> 'Curso', 'valor'=> 0, 'select'=> $cursos])
                        </div>
                    @endif

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'convocatory_close_name', 'texto'=> 'Nombre convocatoria'])
                    </div>

                    @if($alojamientos)
                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'alojamiento_id', 'texto'=> 'Alojamiento', 'select'=> $alojamientos])
                    </div>
                    @endif

                    <div class="form-group row">
                        <div class="col-md-6">
                            @include('includes.form_input_datetime', [ 'campo'=> 'convocatory_close_start_date', 'texto'=> 'Fecha Inicio'])
                        </div>
                        <div class="col-md-6">
                            @include('includes.form_input_datetime', [ 'campo'=> 'convocatory_close_end_date', 'texto'=> 'Fecha Final'])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'convocatory_semiopen', 'texto'=> 'Convocatoria Semicerrada'])
                    </div>

                    <div id="semicerrada" class="form-group row" style="display:none;">
                        <div class="col-md-6">
                            @include('includes.form_select', [ 'campo'=> 'convocatory_semiopen_start_day', 'texto'=> 'Empieza el', 'valor'=>0, 'select'=> $dias])
                        </div>
                        <div class="col-md-6">
                            @include('includes.form_select', [ 'campo'=> 'convocatory_semiopen_end_day', 'texto'=> 'Acaba el', 'valor'=>0, 'select'=> $dias])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_input_number', [ 'campo'=> 'convocatory_close_duration_weeks', 'texto'=> 'Duración'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'duracion_fijo', 'texto'=> 'Unidad duración',
                                'select'=> ConfigHelper::getPrecioDuracionUnitCerrada()])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-money"></i> Precio:
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <span class="badge badge-help">Una vez creada, se calcula desde la pestaña de precio</span>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            @include('includes.form_select', [ 'campo'=> 'dto_early', 'texto'=> 'Descuento Early Bird',
                                'select'=> $descuentos_early])
                        </div>
                    </div>

                    {{-- <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_select', [ 'campo'=> 'convocatory_close_currency_id', 'texto'=> 'Moneda', 'valor'=> ($curso?$curso->centro->moneda_id:0), 'select'=> $monedas])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'convocatory_close_price', 'texto'=> 'Precio'])
                        </div>
                    </div> --}}

                    {{-- <div class="form-group row">
                        <div class="col-md-6">
                            @include('includes.form_input_number', [ 'campo'=> 'plazas', 'texto'=> 'Plazas'])
                        </div>
                        <div class="col-md-6">
                            @include('includes.form_input_number', [ 'campo'=> 'plazas_reservadas', 'texto'=> 'Plazas reservadas'])
                        </div>
                    </div> --}}

                    <div class="form-group row col-md-12">

                        <div class="portlet box blue-ebonyclay">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-tasks"></i> Precio incluye:
                                </div>
                                <div class="tools">
                                    <a href="" class="collapse"></a>
                                    <a href="" class="fullscreen"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                @include('manage.cursos.incluye_list', ['ficha'=> null, 'listado'=> $incluyes ])
                            </div>
                        </div>

                        @include('includes.form_input_text', [ 'campo'=> 'incluye_horario', 'texto'=> 'Horario idiomas'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'convocatory_close_price_include', 'texto'=> 'Precio incluye (otras opciones)'])
                    </div>

                    @include('includes.form_booking_reserva')

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'convocatory_close_code', 'texto'=> 'Código Contable'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'no_facturar', 'texto'=> 'No facturar por sistema'])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_select_multi', [ 'campo'=> 'monitores', 'texto'=> 'Monitores', 'valor'=> [], 'select'=> \VCN\Models\Monitores\Monitor::plataforma()->pluck('name','id')->toArray() ])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'convocatory_close_status', 'texto'=> 'Activa'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'activo_web', 'texto'=> 'Activo Web'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_checkbox', [ 'campo'=> 'activo_corporativo', 'texto'=> 'Convocatoria corporativa'])
                        </div>
                    </div>

                    <hr>
                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'area', 'texto'=> 'Área Cliente', 'valor'=>1 ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'area_pagos', 'texto'=> 'Área Cliente (Pagos)', 'valor'=>1 ])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_checkbox', [ 'campo'=> 'area_reunion', 'texto'=> 'Área Cliente (Reunión)', 'valor'=>1 ])
                        </div>
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'precios', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

<script type="text/javascript">
$(document).ready(function() {

    $("#convocatory_semiopen").click(function(){
        if( $(this).is(':checked') )
        {
            $('#semicerrada').show();
        }
        else
        {
            $('#semicerrada').hide();
        }
        // $("#semicerrada").slideToggle();
    });

});
</script>
@stop