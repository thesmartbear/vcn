@extends('layouts.manage')


@section('container')

<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-book"></i> Info Convocatorias Cerradas
        </div>
        <div class="panel-body">

            <?php
                $categorias = [0=>'Todas'] + \VCN\Models\Categoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
                $subcategorias = [0=>'Todas'] + \VCN\Models\Subcategoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
                // $subcategoriasdet = [0=>'Todas'] + SubcategoriaDetalle::plataforma()->sortBy('name')->pluck('name','id')->toArray();
                // $cursos = [0=>'Todos'] + Curso::plataforma()->where('activo_web',1)->sortBy('name')->pluck('name','id')->toArray();
            ?>

            {!! Form::open(['route' => array('manage.convocatorias.cerradas.index.info'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-1">
                        {!! Form::label('any', 'Año') !!}
                        <br>
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any'))  !!}
                    </div>

                    <div class="col-md-4">
                        {!! Form::label('paises', 'País') !!}
                        <br>
                        {!! Form::select('paises', $paises, $valores['paises'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-paises'))  !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('semanas', 'Semanas') !!}
                        <br>
                        {!! Form::select('semanas', ConfigHelper::getSemanas(), $valores['semanas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-semanas'))  !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('semi', 'Semi-Cerradas') !!}
                        <br>
                        {!! Form::select('semi', [0=>'No', 1=>'Si', 2=>'Sólo semi'], $valores['semi'], array('class'=>'select2', 'id'=>'filtro-semi'))  !!}
                    </div>

                    <div class="col-md-3">
                        @include('includes.form_checkbox', [ 'campo'=> 'solo_plazas', 'texto'=> 'Solo Plazas disponibles', 'valor'=> $valores['solo_plazas'] ])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        {!! Form::label('categorias', 'Categoría') !!}
                        @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                        <br>
                        {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                        @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                        </div>

                        <div class="col-md-4">
                        {!! Form::label('subcategorias', 'SubCategoría') !!}
                        @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                        <br>
                        {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
                    </div>

                    <div class="col-md-1">
                        {!! Form::label('&nbsp;') !!}
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>
                </div>

            {!! Form::close() !!}
            <hr>

            @if(!$listado)
                <br>
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                {!! Datatable::table()
                    ->addColumn([
                      'name'        => 'Nombre',
                      'activo'      => 'Activo',
                      'tipo'        => 'Tipo',
                      'curso'        => 'Curso',
                      'pais'        => 'País',
                      'alojamiento'      => 'Alojamiento',
                      'plazas'      => 'Plazas Disp.',
                      'plazasv'      => 'Vuelos Disp.',
                      'reservas'    => 'Reservas',
                      'vuelos'    => 'Vuelos',
                      'semanas'     => 'N.Semanas',
                      'options' => 'Edades'

                      ])
                    ->setUrl( route('manage.convocatorias.cerradas.index.info', $valores) )
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                            [ "bSortable" => false, "aTargets" => [8,9,10] ]
                        )
                      )
                      ->render() !!}

            @endif

        </div>
    </div>

</div>

@include('manage.convocatorias.cerradas.modal_info')

@stop