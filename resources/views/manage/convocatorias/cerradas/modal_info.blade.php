<div class="modal fade" id="modalInfoPlazas">
    <div class="modal-wide">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Información Convocatoria :: <span id="modalInfo-Label"></span></h4>
            </div>

            <div class="modal-body">

                <table id="modalInfo-alojamiento" class="table table-bordered table-striped table-condensed flip-content">
                    <caption>Plazas por Alojamiento</caption>
                    <thead>
                        <tr>
                            <th>Alojamiento</th>
                            <th class="col-md-1">Plazas</th>
                            <th class="col-md-1">Reserva</th>
                            <th class="col-md-1">Pre-reserva</th>
                            <th class="col-md-1">Overbooking</th>
                            <th class="col-md-1">Disponible</th>
                            <th class="col-md-1">Proveedor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr><td colspan="7"><i class="fa fa-spin fa-spinner"></i></td></tr>
                    </tbody>
                </table>

                <table id="modalInfo-edades" class="table table-bordered table-striped table-condensed flip-content">
                    <caption>Edades</caption>
                    <thead>
                        <tr>
                            <th class="col-md-3">Edad</th>
                            <th>Chicas</th>
                            <th>Chicos</th>
                            <th>nulo</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr><td colspan="4"><i class="fa fa-spin fa-spinner"></i></td></tr>
                    </tbody>
                </table>

                <table id="modalInfo-transporte" class="table table-bordered table-striped table-condensed flip-content">
                    <caption>Plazas por Transporte</caption>
                    <thead>
                        <tr>
                            <th>Transporte</th>
                            <th class="col-md-1">Plazas</th>
                            <th class="col-md-1">Reserva</th>
                            <th class="col-md-1">Pre-reserva</th>
                            <th class="col-md-1">Overbooking</th>
                            <th class="col-md-1">Disponible</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr><td colspan="8"><i class="fa fa-spin fa-spinner"></i></td></tr>
                    </tbody>
                </table>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        //convo-cerrada info
        $('table.dataTable').on('click', 'a.cc-info',function(e) {
            e.preventDefault();

            var label = $(this).data('titulo');
            $('#modalInfo-Label').html(label);

            // $('#modalInfo').modal('show');

            var $id = $(this).data('id');

            var $url = $(this).data('ajax');
            var $data = { 'info': 'info', 'id': $id };

            $("#modalInfo-transporte tbody").html('<tr><td colspan="8"><i class="fa fa-spin fa-spinner"></i></td></tr>');
            $("#modalInfo-alojamiento tbody").html('<tr><td colspan="7"><i class="fa fa-spin fa-spinner"></i></td></tr>');
            $("#modalInfo-edades tbody").html('<tr><td colspan="4"><i class="fa fa-spin fa-spinner"></i></td></tr>');

            $.ajax({
                url: $url,
                type: 'GET',
                dataType : 'json',
                data: $data,
                success: function(data) { console.log(data);

                    // $("#modalInfo-transporte").html(data.info_transporte);
                    // $("#modalInfo-alojamiento").html(data.info_alojamiento);
                    // $("#modalInfo-edades").html(data.info_edades);

                    $('#modalInfo-alojamiento tbody').empty();
                    $.each(data.info_alojamiento, function(i, item) {

                        $('#modalInfo-alojamiento tbody').append('<tr><td>'+ item.name +'</td><td>'+ item.plazas_totales +'</td><td>'+ item.plazas_reservas +'</td><td>'+ item.plazas_prereservas +'</td><td>'+ item.plazas_overbooking +'</td><td>'+ item.plazas_disponibles +'</td><td>'+ item.plazas_proveedor +'</td></tr>');

                    });

                    $('#modalInfo-transporte tbody').empty();console.log(data.info_transporte);
                    $.each(data.info_transporte, function(i, item) {

                        $('#modalInfo-transporte tbody').append('<tr><td>'+ item.name +'</td><td>'+ item.plazas_totales +'</td><td>'+ item.plazas_reservas +'</td><td>'+ item.plazas_prereservas +'</td><td>'+ item.plazas_overbooking +'</td><td>'+ item.plazas_disponibles +'</td></tr>');

                    });

                    $('#modalInfo-edades tbody').empty();
                    $.each(data.info_edades, function(i, item) {

                        $('#modalInfo-edades tbody').append('<tr><td>'+ i +'</td><td>'+ item.chicas +'</td><td>'+ item.chicos +'</td><td>'+ item.nulo +'</td></tr>');

                    });

                },
                error: function(xhr, desc, err) {
                  console.log(xhr.responseText);
                  console.log("Details: " + desc + "\nError:" + err);
                }
              }); // end ajax call

        });
    });
</script>