<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-calendar"></i> Ofertas Convocatorias Abiertas

            <span class="pull-right"><a href="{{ route('manage.convocatorias.ofertas.nuevo', $convocatoria_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Oferta Convocatoria Abierta</a></span>

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'convocatoria' => 'Convocatoria',
                  'name'        => 'Oferta',
                  'periodo'      => 'Periodo',
                  'rango'      => 'Rango',
                  'convocatory_open_deals_price_per_range' => 'Precio',
                  'options' => ''

                ])
                ->setUrl( route('manage.convocatorias.ofertas.index', $convocatoria_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [5] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>