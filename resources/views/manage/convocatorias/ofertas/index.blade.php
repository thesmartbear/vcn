@extends('layouts.manage')


@section('container')

    @include('manage.convocatorias.ofertas.list', ['convocatoria_id'=> $convocatoria_id])

@stop