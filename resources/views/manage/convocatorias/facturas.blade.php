<div class="col-md-12">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-ticket fa-fw"></i> Facturas
        </div>
        <div class="panel-body">

            <?php
                $valores['convocatorias'] = $ficha->id;
                $valores['tipoc'] = 1;
                $bookings = \VCN\Models\Bookings\Booking::listadoFiltros($valores);

                $parcial = 0;
                foreach($bookings->get() as $b)
                {
                    $parcial += $b->facturable;
                }
                $parcial_txt = ConfigHelper::parseMoneda($parcial);

            ?>


            @if($bFacturas)

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-ticket fa-fw"></i> Emitir Facturas
                    </div>
                    <div class="panel-body">

                        Clicar en este botón para emitir una factura por inscripción, para todas las inscripciones de la convocatoria:
                        <a href="{{ route('manage.bookings.facturar',[$modelo,$ficha->id]) }}" class='btn btn-warning btn-xs'><i class='fa fa-ticket'></i> Emitir Facturas</a>

                    </div>
                </div>

            @else
                @if($ficha->no_facturar)
                    <div class="note note-danger">
                        <h4 class="block">Alerta</h4>
                        <p>
                            La convocatoria no es facturable por sistema.
                        </p>
                    </div>
                @else
                    <div class="note note-danger">
                        <h4 class="block">Alerta</h4>
                        <p>
                            Ya se han emitido las facturas para esta convocatoria o la convocatoria.
                        </p>
                    </div>
                @endif
            @endif

            @if($parcial)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-ticket fa-fw"></i>
                    @if($bFacturas)
                        Emitir Factura agrupada
                    @else
                        Emitir Abono y Factura agrupada nueva
                    @endif
                </div>
                <div class="panel-body">

                    {!! Form::open(array('method'=> 'post', 'url' => route('manage.bookings.facturar.bymodel', [$modelo,$modelo_id]) )) !!}

                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'razonsocial', 'texto'=> 'Razón Social'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'direccion', 'texto'=> 'Dirección'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'nif', 'texto'=> 'DNI'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'cp', 'texto'=> 'CP'])
                        </div>
                        <div class="col-md-1">
                            <label>Agrupada</label>
                            {!! Form::submit('Emitir',['class' => 'btn btn-success']) !!}
                        </div>

                        {{-- @if(!$bFacturas) --}}
                            {{-- <div class="col-md-1">
                                <label>Agrupada</label>
                                {!! Form::submit('Abono y emitir',['name'=> 'btn-abono','class' => 'btn btn-danger']) !!}
                            </div> --}}
                        {{-- @endif --}}
                    </div>

                    {{-- @if($bFacturas) --}}
                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'parcial', 'texto'=> 'Importe parcial', 'help'=> "En blanco: importe pendiente de facturar ($parcial_txt)"])
                        </div>
                        <div class="col-md-1">
                            <label>Parcial</label>
                            {!! Form::submit('Emitir Parcial',[ 'name'=> 'btn-parcial', 'class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                    {{-- @endif --}}

                    {!! Form::close() !!}

                </div>
            </div>
            @endif

            <hr>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-ticket"></i> Factura manual
                </div>
                <div class="panel-body">
                    {!! Form::open(array('method'=> 'post', 'url' => route('manage.bookings.facturar.bymodel', [$modelo,$modelo_id]) )) !!}
                        {!! Form::hidden('manual', true) !!}
                        <div id="facturas_div" class="form-group">
                            <div class="facturas_form row">
                                <div class="col-md-1">
                                    @include('includes.form_input_text', [ 'campo'=> "manual_numero", 'texto'=> 'Número'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_input_datetime', [ 'campo'=> "manual_fecha", 'texto'=> 'Fecha' ])
                                </div>
                                <div class="col-md-1">
                                    @include('includes.form_input_text', [ 'campo'=> "manual_cp", 'texto'=> 'CP'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_input_text', [ 'campo'=> "manual_importe", 'texto'=> 'Importe', 'help'=> "En blanco: Total"])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_input_text', [ 'campo'=> "manual_razonsocial", 'texto'=> 'Razón Social'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_input_text', [ 'campo'=> "manual_cif", 'texto'=> 'CIF'])
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                {!! Form::submit('Guardar', array('class'=> 'btn btn-success')) !!}
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>

            <hr>

            {!! Datatable::table()
                ->addColumn([
                  'fecha'   => 'Fecha',
                  'numero'  => 'Numero',
                  'booking_id'  => 'Booking',
                  'viajero' => 'Viajero',
                  'total'   => 'Total',
                  'grup'    => 'Agrupada',
                  'manual'  => 'Manual',
                  'pdf'     => 'Ver',
                  'options' => ''

                ])
                ->setUrl( route('manage.bookings.facturas.index.bymodel', [$modelo, $modelo_id]) )
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "columnDefs", array(
                    [ "sortable" => false, "targets" => [7,8] ],
                    [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
                ->render() !!}

                <script type="text/javascript">
                $(document).ready(function() {

                    $('table.dataTable').on('click', '.btn-abonar-grup',function(e)
                    {
                        e.preventDefault();

                        var ruta = $(this).data('ruta');
                        bootbox.confirm({

                            message: "Es una factura agrupada. ¿Está seguro de abonar la totalidad de la factura agrupada?",
                            buttons: {
                                confirm: {
                                    label: 'Abonar',
                                    className: 'btn-danger'
                                },
                            },
                            callback: function(result) {
                                if(result)
                                {
                                    window.location = ruta;
                                }
                            }
                        });

                    });

                });
                </script>

        </div>
    </div>

</div>