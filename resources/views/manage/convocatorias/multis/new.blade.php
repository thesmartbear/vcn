@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bed fa-fw"></i>
                    Convocatoria Multi :: Nueva
            </div>
            <div class="panel-body">

                {!! Form::open(array('route' => array('manage.convocatorias.multis.ficha', 0))) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre convocatoria'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_select', [ 'campo'=> 'moneda_id', 'texto'=> 'Moneda', 'valor'=> 0, 'select'=> $monedas])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'precio', 'texto'=> 'Precio Base x Semana'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'precio_extra', 'texto'=> 'Precio Noche Extra',
                                'help'=> 'Duración semana menos de 7 días'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'dto_early', 'texto'=> 'Descuento Early Bird',
                                'select'=> $descuentos_early])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_select_multi_chosen', [ 'campo'=> 'cursos_id', 'texto'=> 'Cursos', 'select'=> $cursos, 'valor'=> $curso_id ])
                    </div>

                    @include('includes.form_booking_reserva')

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'no_facturar', 'texto'=> 'No facturar por sistema'])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_select_multi', [ 'campo'=> 'monitores', 'texto'=> 'Monitores', 'valor'=> [], 'select'=> \VCN\Models\Monitores\Monitor::plataforma()->pluck('name','id')->toArray() ])
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'activa', 'texto'=> 'Activa'])
                    </div>

                    <hr>
                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'area', 'texto'=> 'Área Cliente', 'valor'=>1 ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'area_pagos', 'texto'=> 'Área Cliente (Pagos)', 'valor'=>1 ])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_checkbox', [ 'campo'=> 'area_reunion', 'texto'=> 'Área Cliente (Reunión)', 'valor'=>1 ])
                        </div>
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'precios', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop