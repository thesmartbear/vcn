@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-book fa-fw"></i>
                    Convocatoria Multi :: {{$ficha->name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Convocatoria</a></li>
                    <li role="presentation"><a href="#semanas" aria-controls="semanas" role="tab" data-toggle="tab">Semanas</a></li>
                    <li role="presentation"><a href="#especialidades" aria-controls="especialidades" role="tab" data-toggle="tab">Especialidades</a></li>

                    @if(!$ficha->reunion_no)
                    <li role="presentation"><a data-label="Reuniones" href="#reuniones" aria-controls="reuniones" role="tab" data-toggle="tab"><i class="fa fa-comments"></i></a></li>
                    @endif

                    <li role="presentation"><a data-label="Documentos" href="#docs" aria-controls="docs" role="tab" data-toggle="tab"><i class="fa fa-paperclip fa-fw"></i></a></li>

                    <li role="presentation"><a data-label="Cuestionarios" href="#cuestionarios" aria-controls="cuestionarios" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>
                    <li role="presentation"><a data-label="Tests" href="#tests" aria-controls="tests" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>

                    @if( ConfigHelper::config('facturas') && ConfigHelper::canEdit('facturas')  )
                    <li role="presentation"><a data-label="Facturas" href="#facturas" aria-controls="facturas" role="tab" data-toggle="tab"><i class="fa fa-ticket fa-fw"></i></a></li>
                    @endif

                    <li role="presentation"><a href="#condiciones" aria-controls="condiciones" role="tab" data-toggle="tab"> Condiciones</a></li>
                    <li role="presentation"><a href="#contactos_sos" aria-controls="contactos_sos" role="tab" data-toggle="tab"> Contactos SOS</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                    {!! Form::model($ficha, array('route' => array('manage.convocatorias.multis.ficha', $ficha->id))) !!}

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre convocatoria'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_select', [ 'campo'=> 'moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas])
                            </div>

                            @if( ConfigHelper::canEdit('precios-cmulti')  )
                                <div class="col-md-2">
                                    @include('includes.form_input_text', [ 'campo'=> 'precio', 'texto'=> 'Precio Base x Semana'])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_input_text', [ 'campo'=> 'precio_extra', 'texto'=> 'Precio Noche Extra',
                                        'help'=> 'Duración semana menos de 7 días'])
                                </div>
                            @else
                                <div class="col-md-2">
                                    {!! Form::label('precio', 'Precio Base x Semana') !!}
                                    {{$ficha->precio}}
                                </div>
                                <div class="col-md-4">
                                    <span class="badge badge-help">Duración semana menos de 7 días</span>
                                    {!! Form::label('precio_extra', 'Precio Noche Extra') !!}
                                    {{$ficha->precio_extra}}
                                </div>
                            @endif

                            <div class="col-md-4">
                                @include('includes.form_select', [ 'campo'=> 'dto_early', 'texto'=> 'Descuento Early Bird',
                                    'select'=> $descuentos_early])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_select2_multi', [ 'campo'=> 'cursos_id[]', 'texto'=> 'Cursos',
                                'valor'=> explode(',',$ficha->cursos_id), 'select'=> $cursos ])
                        </div>

                        <div class="form-group">
                            [{{ConfigHelper::config('area_url')?:ConfigHelper::config('web')}}/booking-multi/{{$ficha->id}}]
                        </div>

                        @include('includes.form_booking_reserva')

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'activa', 'texto'=> 'Activa'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'no_facturar', 'texto'=> 'No facturar por sistema'])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'reunion_no', 'texto'=> 'Sin Reunión informativa'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select_multi', [ 'campo'=> 'monitores', 'texto'=> 'Monitores', 'valor'=> $ficha->monitores->pluck('monitor_id')->toArray(), 'select'=> \VCN\Models\Monitores\Monitor::plataforma()->pluck('name','id')->toArray() ])
                        </div>

                        <hr>
                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'area', 'texto'=> 'Área Cliente' ])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'area_pagos', 'texto'=> 'Área Cliente (Pagos)' ])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_checkbox', [ 'campo'=> 'area_reunion', 'texto'=> 'Área Cliente (Reunión)' ])
                            </div>
                        </div>

                        @include('includes.form_submit', [ 'permiso'=> 'precios', 'texto'=> 'Guardar'])

                    {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="semanas">

                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <caption>Semanas</caption>
                            <thead>
                                <tr>
                                    <th class="col-md-1">Semana</th>
                                    <th class="col-md-2">Desde</th>
                                    <th class="col-md-2">Hasta</th>
                                    <th class="col-md-1">Plazas</th>
                                    <th>P.Reserva</th>
                                    <th>P.Pre-Reserva</th>
                                    <th>P.Overbooking</th>
                                    <th>P.Bloqueadas</th>
                                    <th>P.Online</th>
                                    <th>P.Disponibles</th>
                                    <th class="col-md-3"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($ficha->semanas->sortBy('semana') as $semana)
                                <tr>
                                    {!! Form::open(array('method' => 'POST', 'url' => route('manage.convocatorias.multis.semanas.post',$ficha->id), 'role'=> 'form', 'class' => 'form-inline')) !!}
                                    {!! Form::hidden('edit_semana_id', $semana->id) !!}

                                    <td>
                                        @include('includes.form_input_number', [ 'campo'=> 'semana', 'texto'=> null, 'ficha'=> $semana])
                                        {{-- {{$semana->bloque}} {{str_plural('Semana',$semana->bloque)}} --}}
                                    </td>
                                    <td>
                                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'ficha'=> $semana,
                                            'valor'=> Carbon::parse($semana->desde)->format('d/m/Y')])
                                    </td>
                                    <td>
                                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'ficha'=> $semana,
                                            'valor'=> Carbon::parse($semana->hasta)->format('d/m/Y')])
                                    </td>
                                    <td>
                                        @include('includes.form_input_number', [ 'campo'=> 'plazas_totales', 'texto'=> null, 'ficha'=> $semana])
                                    </td>
                                    <td>{{$semana->plazas_reservas}}</td>
                                    <td>{{$semana->plazas_prereservas}}</td>
                                    <td>{{$semana->plazas_overbooking}}</td>
                                    <td>
                                        @include('includes.form_input_number', [ 'campo'=> 'plazas_bloqueadas', 'texto'=> null, 'ficha'=> $semana])
                                    </td>
                                    <td>{{$semana->plazas_online}}</td>
                                    <td>{{$semana->plazas_disponibles}}</td>
                                    <td>

                                        @include('includes.form_submit', [ 'permiso'=> 'precios', 'texto'=> 'Actualizar'])

                                        <a href='#destroy'
                                            data-label='Borrar'
                                            data-model='Semana Convocatoria Multi'
                                            data-action='{{route( 'manage.convocatorias.multis.semanas.delete', $semana->id)}}'
                                            data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>
                                    </td>

                                    {!! Form::close() !!}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @if( ConfigHelper::canEdit('precios-cmulti')  )
                        <hr>
                        <div id="semana-add" class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-plus-circle fa-fw"></i> Añadir:
                            </div>
                            <div class="panel-body">

                                {!! Form::open(array('method' => 'POST', 'url' => route('manage.convocatorias.multis.semanas.post',$ficha->id), 'role' => 'form-inline', 'class' => '')) !!}

                                <div class="form-group">
                                    <div class="col-md-2">
                                        @include('includes.form_input_number', [ 'campo'=> 'semana', 'texto'=> 'N.Semana', 'required'=> 'required'])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> 'Desde', 'required'=> 'required'])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> 'Hasta', 'required'=> 'required'])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_input_number', [ 'campo'=> 'plazas_totales', 'texto'=> 'Plazas Totales', 'required'=> 'required'])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_input_number', [ 'campo'=> 'plazas_bloqueadas', 'texto'=> 'Plazas Bloqueadas'])
                                    </div>
                                </div>

                                @include('includes.form_submit', [ 'permiso'=> 'precios', 'texto'=> 'Añadir'])
                                {!! Form::close() !!}

                            </div>
                        </div>
                        @endif

                    </div>



                    <div role="tabpanel" class="tab-pane fade in" id="especialidades">

                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <caption>Especialidades</caption>
                            <thead>
                                <tr>
                                    <th class="col-md-4">Especialidad</th>
                                    <th class="col-md-1">Precio</th>
                                    <th class="col-md-3">Semanas</th>
                                    <th class="col-md-2">Cod. Contable</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($ficha->especialidades as $especialidad)

                                <?php
                                $semanas_list = explode(',',$especialidad->semanas);
                                if(!$semanas_list[0])
                                {
                                    $semanas_list = null;
                                }
                                ?>

                                <tr>
                                    {!! Form::open(array('method' => 'POST', 'url' => route('manage.convocatorias.multis.especialidades.post',$ficha->id), 'role'=> 'form', 'class' => 'form-inline')) !!}
                                    {!! Form::hidden('edit_especialidad_id', $especialidad->id) !!}

                                    <td>
                                        {{-- @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> null, 'ficha'=> $especialidad]) --}}
                                        {{$especialidad->especialidad_name}}
                                    </td>
                                    <td>
                                        @include('includes.form_input_number', [ 'campo'=> 'precio', 'texto'=> null, 'ficha'=> $especialidad])
                                    </td>
                                    <td>
                                        {{$especialidad->semanas}}
                                        <div class="pull-right">
                                            {!! Form::select('semanas[]', $semanas, $semanas_list, array('multiple', 'class'=> 'select2 col-md-12')) !!}
                                        </div>
                                    </td>
                                    <td>
                                        @if($semanas_list)
                                        <?php
                                            $contables = explode(',',$especialidad->contable);
                                        ?>
                                        @foreach($semanas_list as $ic => $scontable)
                                            <?php
                                                $sc = isset($contables[$ic])?$contables[$ic]:"";
                                            ?>
                                            Semana {{$semanas_list[$ic]}}:
                                            @include('includes.form_input_text', [ 'campo'=> "contable[$ic]", 'texto'=> null, 'ficha'=> $especialidad, 'valor'=> $sc])
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>

                                        @include('includes.form_submit', [ 'permiso'=> 'precios-cmulti', 'texto'=> 'Actualizar'])
                                        <a href='#destroy'
                                            data-label='Borrar'
                                            data-model='Especialidad Convocatoria Multi'
                                            data-action='{{route( 'manage.convocatorias.multis.especialidades.delete', $especialidad->id)}}'
                                            data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>
                                    </td>

                                    {!! Form::close() !!}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @if( ConfigHelper::canEdit('precios-cmulti')  )
                        <hr>
                        <div id="especialidad-add" class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-plus-circle fa-fw"></i> Añadir:
                            </div>
                            <div class="panel-body">

                                {!! Form::open(array('method' => 'POST', 'url' => route('manage.convocatorias.multis.especialidades.post',$ficha->id), 'role' => 'form-inline', 'class' => '')) !!}

                                <div class="form-group">
                                    <div class="col-md-4">
                                        {{-- @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Especialidad', 'valor'=>'', 'required'=> 'required']) --}}
                                        @include('includes.form_select', [ 'campo'=> 'especialidad_id', 'texto'=> 'Especialidad',
                                            'select'=> $especialidades])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_input_number', [ 'campo'=> 'precio', 'texto'=> 'Precio', 'valor'=>'', 'required'=> 'required'])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_select_multi', [ 'campo'=> 'semanas', 'texto'=> 'Semanas', 'valor'=> [], 'select'=> $semanas ])
                                    </div>
                                    {{-- <div class="col-md-2">
                                        @include('includes.form_input_text', [ 'campo'=> 'contable', 'texto'=> 'Cod.Contable', 'valor'=>''])
                                    </div> --}}
                                </div>

                                @include('includes.form_submit', [ 'permiso'=> 'precios', 'texto'=> 'Añadir'])
                                {!! Form::close() !!}

                            </div>
                        </div>
                        @endif

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="reuniones">
                        <div class="col-md-12">
                            @include('manage.reuniones.list_convocatoria', ['tipo'=>3, 'convocatoria_id'=> $ficha->id])
                        </div>
                        <div class="col-md-12">
                            @include('manage.reuniones.new', ['tipo'=>3, 'convocatoria_id'=> $ficha->id])
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="docs">
                        @include('includes.documentos', ['modelo'=> 'ConvocatoriaMulti', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="cuestionarios">
                        @include('manage.system.cuestionarios.vinculado', ['modelo'=> 'ConvocatoriaMulti', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="tests">
                        @include('manage.exams.vinculado', ['modelo'=> 'ConvocatoriaMulti', 'modelo_id'=> $ficha->id])
                    </div>

                    @if( ConfigHelper::config('facturas') && ConfigHelper::canEdit('facturas')  )
                    <div role="tabpanel" class="tab-pane fade in" id="facturas">
                        @include('manage.convocatorias.multis.facturas', ['modelo'=> 'ConvocatoriaMulti', 'modelo_id'=> $ficha->id])
                        {{-- <a href='{{ route('manage.bookings.facturar',['ConvocatoriaMulti',$ficha->id]) }}' class='btn btn-success'><i class='fa fa-ticket'></i> Facturar</a> --}}
                    </div>
                    @endif

                    <div role="tabpanel" class="tab-pane fade in" id="condiciones">
                        {!! Form::model($ficha, array('route' => array('manage.convocatorias.multis.ficha', $ficha->id)) )!!}

                        {!! Form::hidden('condiciones', 'condiciones') !!}

                        @foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)

                            @foreach(ConfigHelper::idiomas() as $keyi=>$idioma)

                                <?php

                                    $dir = "assets/uploads/pdf_condiciones/";
                                    $name = class_basename($ficha) ."_". $ficha->id;
                                    $file = $dir. $name ."_". $keyp ."_". $idioma .".pdf";

                                    $valor = null;

                                    if(isset($ficha->condiciones[$keyp][$idioma]))
                                    {
                                        $valor = $ficha->condiciones[$keyp][$idioma];
                                    }
                                ?>

                                <div class="form-group">
                                    @include('includes.form_textarea_tinymce', [ 'campo'=> "condiciones_$keyp-$idioma", 'texto'=> "Condiciones ($plataforma : $idioma)", 'valor'=> $valor])
                                    
                                    @if($valor)
                                        Ver: <a target="_blank" href="/{{$file}}">PDF</a>
                                    @endif

                                </div>

                            @endforeach

                        @endforeach

                        @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="contactos_sos">
                        @include('includes.system_contactos-tab', ['modelo'=> 'ConvocatoriaMulti', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'ConvocatoriaMulti',
                                'campos_text'=> [
                                    ['name'=> 'Nombre convocatoria'],
                                ],
                                'campos_textarea'=> []
                            ])

                    </div>

                </div>


            </div>
        </div>

<script type="text/javascript">
$(document).ready(function() {

    @if($ficha->convocatory_semiopen)
        $('#semicerrada').show();
    @endif

    $("#convocatory_semiopen").click(function(){
        if( $(this).is(':checked') )
        {
            $('#semicerrada').show();
        }
        else
        {
            $('#semicerrada').hide();
        }
        // $("#semicerrada").slideToggle();
    });

});
</script>

@stop