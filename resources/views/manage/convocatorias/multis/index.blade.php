@extends('layouts.manage')


@section('container')

    @include('manage.convocatorias.multis.list', ['curso_id'=> $curso_id])

@stop