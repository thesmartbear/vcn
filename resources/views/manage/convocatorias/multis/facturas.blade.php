<div class="col-md-12">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-ticket fa-fw"></i> Facturas
        </div>
        <div class="panel-body">

            @if($bFacturas)

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-ticket fa-fw"></i> Emitir Facturas
                </div>
                <div class="panel-body">

                    Clicar en este botón para emitir una factura por inscripción, para todas las inscripciones de la convocatoria:
                    <a href="{{ route('manage.bookings.facturar',[$modelo,$ficha->id]) }}" class='btn btn-warning btn-xs'><i class='fa fa-ticket'></i> Emitir Facturas</a>

                </div>
            </div>

            @else
                @if($ficha->no_facturar)
                    <div class="note note-danger">
                        <h4 class="block">Alerta</h4>
                        <p>
                            La convocatoria no es facturable por sistema.
                        </p>
                    </div>
                @else
                    <div class="note note-danger">
                        <h4 class="block">Alerta</h4>
                        <p>
                            Ya se han emitido las facturas para esta convocatoria o la convocatoria.
                        </p>
                    </div>
                @endif
            @endif

            <hr>

            {!! Datatable::table()
                ->addColumn([
                  'fecha'   => 'Fecha',
                  'numero'  => 'Numero',
                  'booking_id'  => 'Booking',
                  'viajero' => 'Viajero',
                  'total'   => 'Total',
                  'grup'    => 'Agrupada',
                  'manual'  => 'Manual',
                  'pdf'     => 'Ver',
                  'options' => ''

                ])
                ->setUrl( route('manage.bookings.facturas.index.bymodel', [$modelo, $modelo_id]) )
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "columnDefs", array(
                    [ "sortable" => false, "targets" => [7,8] ],
                    [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>