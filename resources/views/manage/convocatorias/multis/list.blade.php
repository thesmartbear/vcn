<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-book"></i> Convocatorias Multi

            <span class="pull-right"><a href="{{ route('manage.convocatorias.multis.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Convocatoria Multi</a></span>

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'        => 'Nombre',
                  'activo'      => 'Activo',
                  'cursos'      => 'Cursos',
                  'options' => ''

                ])
                ->setUrl( route('manage.convocatorias.multis.index', $curso_id) )
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [3] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>