
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-money"></i>

            @if($extras)
              Convocatoria Abierta Precios Extra temporada
            @else
              Convocatoria Abierta Precios
            @endif

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'        => 'Nombre',
                  'precio'      => 'Precio',
                  'regla'       => 'Regla',
                  'periodo'     => 'Validez',
                  'options'     => ''
                ])
                ->setUrl( route('manage.convocatorias.precios.index', [$convocatoria_id,$extras]) )
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [4] ]
                  )
                )
                ->render() !!}

        </div>
    </div>


<hr>

@if(ConfigHelper::canEdit('precios'))
@include('includes.script_precios',
  [ 'route'=> route('manage.convocatorias.precios.ficha', [0,$extras]), 'extras'=> $extras,
    'campo'=> 'convocatory_id', 'campo_id'=> $convocatoria_id, 'moneda_id'=>$ficha->curso->centro->moneda_id, 'fechas'=> true])
@endif
