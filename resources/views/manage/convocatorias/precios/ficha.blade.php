@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-credit-card fa-fw"></i>
                    <a href="{{route('manage.convocatorias.abiertas.ficha',$ficha->convocatory_id)}}">
                    Convocatoria Abierta: ({{$ficha->convocatoria->convocatory_open_name}})
                    </a>
                    Precio :: {{$ficha->name}}
            </div>
            <div class="panel-body">

                {!! Form::model($ficha, array('route' => array('manage.convocatorias.precios.ficha', $ficha->id))) !!}

                {!! Form::hidden('convocatory_id', $ficha->convocatory_id) !!}


                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre', 'required'=> 'required'])
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas, 'required'=> 'required'])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'importe', 'texto'=> 'Precio', 'required'=> 'required'])
                </div>

                <div class="form-group">
                    <strong>Regla: {{ConfigHelper::getPrecioRegla($ficha)}}</strong>
                </div>

                <div id="rango_div" class="form-group collapse">
                    <span id="rango_titulo"></span>
                </div>


                @include('includes.form_submit', [ 'permiso'=> 'precios', 'texto'=> 'Guardar'])

                {!! Form::close() !!}


            </div>
        </div>

@stop