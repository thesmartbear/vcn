<div class="col-md-12">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-certificate fa-fw"></i> Convocatorias Cerradas
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'              => 'Nombre',
                  'options'           => ''

                ])
                ->setUrl( route('manage.convocatorias.vuelos.index.convocatorias', $vuelo_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [0] ]
                  )
                )
                ->render() !!}

            <hr>

            Asignar a Convocatoria:

            {!! Form::open(array('route' => array('manage.convocatorias.vuelos.asignar'))) !!}

                {!! Form::hidden('vuelo_id', $vuelo_id) !!}
                {!! Form::hidden('tab', 'convocatorias') !!}

                <div class="form-group">
                    <div class="col-md-10">
                        @include('includes.form_select2_multi', [ 'campo'=> 'convocatory_id', 'texto'=> null, 'select'=> $convocatorias])
                    </div>
                    <div class="col-md-2">
                        {!! Form::submit('Añadir', array('class' => 'btn btn-success')) !!}
                    </div>
                </div>

            {!! Form::close() !!}

        </div>
    </div>

</div>