@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.convocatorias.vuelos.ficha', $ficha) !!}
@stop

@section('extra_head')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script>
  $(function() {

    $( "input[name='salida_aeropuerto']" ).autocomplete({
        source: "{{ route('manage.autocomplete', 'aeropuertos') }}",
        minLength: 2,
        create: function() {
            $(this).data('ui-autocomplete')._renderItem = function( ul, item ) {
                return $( "<li>" )
                    .append( "<a>" + item.name + " ("+ item.iata +"), " + item.city + "</a>" )
                    .appendTo( ul );
            };
        },
        select: function( event, ui ) {
            $(this).val( ui.item.name + " ("+ ui.item.iata +"), " + ui.item.city );
            return false;
        },
    });

    $( "input[name='llegada_aeropuerto']" ).autocomplete({
        source: "{{ route('manage.autocomplete', 'aeropuertos') }}",
        minLength: 2,
        create: function() {
            $(this).data('ui-autocomplete')._renderItem = function( ul, item ) {
                return $( "<li>" )
                    .append( "<a>" + item.name + " ("+ item.iata +"), " + item.city + "</a>" )
                    .appendTo( ul );
            };
        },
        select: function( event, ui ) {
            $(this).val( ui.item.name + " ("+ ui.item.iata +"), " + ui.item.city );
            return false;
        },
    });

    $( "input[name='company']" ).autocomplete({
      source: "{{ route('manage.autocomplete', 'aerolineas') }}",
      minLength: 2,
    });
  });
</script>
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-plane fa-fw"></i>
                    Vuelo :: {{$ficha->name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Datos Generales</a></li>
                    <li role="presentation"><a href="#encuentro" aria-controls="encuentro" role="tab" data-toggle="tab">Datos Encuentro</a></li>
                    <li role="presentation"><a href="#plazas" aria-controls="plazas" role="tab" data-toggle="tab">Plazas</a></li>
                    <li role="presentation"><a href="#economico" aria-controls="economico" role="tab" data-toggle="tab">Datos Económicos</a></li>
                    <li role="presentation"><a href="#resumen" aria-controls="resumen" role="tab" data-toggle="tab">Info Vuelo</a></li>
                    <li role="presentation"><a href="#etapas" aria-controls="etapas" role="tab" data-toggle="tab">Etapas</a></li>

                    <li role="presentation"><a href="#convocatorias" aria-controls="convocatorias" role="tab" data-toggle="tab">Convocatorias</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('route' => array('manage.convocatorias.vuelos.ficha', $ficha->id))) !!}

                            <div class="form-group row">
                                <div class="col-md-10">
                                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Vuelo'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_select', [ 'campo'=> 'any', 'texto'=> 'Año', 'select'=> ConfigHelper::getAnys()])
                                </div>
                            </div>

                            <div class="form-group">
                                @include('includes.form_textarea_tinymce', [ 'campo'=> 'info', 'texto'=> 'Información'])
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    @include('includes.form_select', [ 'campo'=> 'transporte', 'texto'=> 'Tipo Transporte', 'select'=> ConfigHelper::getTipoTransporte()])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_select', [ 'campo'=> 'agencia_id', 'texto'=> 'Agencia', 'select'=> $agencias])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_input_text', [ 'campo'=> 'localizador', 'texto'=> 'Localizador'])
                                </div>
                            </div>

                            <div class="form-group">
                                @include('includes.form_checkbox', [ 'campo'=> 'individual', 'texto'=> 'Individual'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_checkbox', [ 'campo'=> 'interno', 'texto'=> 'Interno (no web)'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_checkbox', [ 'campo'=> 'billetes', 'texto'=> 'Datos pasaporte enviados para emisión billetes' ])
                                @if($ficha->billetes)
                                    [{{$ficha->billetes_log['user']}} : {{$ficha->billetes_log['datetime']}}]
                                @endif

                            </div>

                            @include('includes.form_submit', [ 'permiso'=> 'vuelos', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="encuentro">

                        {!! Form::model($ficha, array('route' => array('manage.convocatorias.vuelos.ficha', $ficha->id))) !!}

                            {!! Form::hidden('tab','#encuentro') !!}

                            <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    @include('includes.form_input_datetime', [ 'campo'=> 'encuentro_fecha', 'texto'=> 'Día',
                                        'valor'=> $ficha->encuentro_fecha?Carbon::parse($ficha->encuentro_fecha)->format('d/m/Y'):''])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'encuentro_hora', 'texto'=> 'Hora' ])
                                </div>
                            </div>
                            </div>

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'encuentro_lugar', 'texto'=> 'Lugar'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'encuentro_terminal', 'texto'=> 'Terminal'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'encuentro_punto', 'texto'=> 'Punto (Mostradores facturación / Mostradors facturació)'])
                            </div>

                            @include('includes.form_submit', [ 'permiso'=> 'transporte-info', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>


                    <div role="tabpanel" class="tab-pane fade in" id="plazas">
                        <div class="col-md-12">
                        {!! Form::model($ficha, array('route' => array('manage.convocatorias.vuelos.ficha', $ficha->id))) !!}

                            {!! Form::hidden('tab','#plazas') !!}

                            <div class="form-group row">
                                <div class="col-md-3">
                                    @include('includes.form_input_number', [ 'campo'=> 'plazas_totales', 'texto'=> 'Plazas Totales'])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_input_number', [ 'campo'=> 'plazas_umbral', 'texto'=> 'Plazas Umbral'])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_input_number', [ 'campo'=> 'plazas_bloqueadas', 'texto'=> 'Plazas Bloqueadas'])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_input_number', [ 'campo'=> 'plazas_monitor', 'texto'=> 'Plazas Monitor'])
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-2">
                                P.Reservas: {{$ficha->plazas_reservas}}
                                </div>
                                <div class="col-md-2">
                                P.Pre-Reservas: {{$ficha->plazas_prereservas}}
                                </div>
                                <div class="col-md-2">
                                P.Overbooking:  {{$ficha->plazas_overbooking}}
                                </div>
                                <div class="col-md-2">
                                P.Disponibles: {{$ficha->plazas_disponibles}}
                                </div>
                                <div class="col-md-2">
                                P.Online: {{$ficha->plazas_online}}
                                </div>
                                <div class="col-md-2">
                                <strong>P.Totales: {{$ficha->plazas_totales}}</strong>
                                </div>
                            </div>

                            @if(ConfigHelper::canEdit('plazas-privado'))

                                <div class="form-group">
                                    @include('includes.form_textarea_tinymce', [ 'campo'=> 'plazas_txt', 'texto'=> 'Comentarios'])
                                </div>

                                @if($ficha->propietario == ConfigHelper::config('propietario'))
                                    @include('includes.form_submit', [ 'permiso'=> 'plazas-privado', 'texto'=> 'Guardar'])
                                @else
                                    @include('includes.form_submit', [ 'permiso'=> 'transporte-plazas', 'texto'=> 'Guardar'])
                                @endif
                            @else
                                @include('includes.form_submit', [ 'permiso'=> 'transporte-plazas', 'texto'=> 'Guardar'])
                            @endif

                        {!! Form::close() !!}
                        </div>

                        <div class="col-md-12">
                            <hr>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-edit fa-fw"></i> Bookings
                                </div>
                                <div class="panel-body">

                                    {!! Datatable::table()
                                        ->addColumn([
                                          'name'            => 'Viajero',
                                          'convocatoria'    => 'Convocatoria',
                                          'oficina'         => 'Oficina',
                                          'options'         => ''
                                        ])
                                        ->setUrl(route('manage.convocatorias.vuelos.bookings', $ficha->id))
                                        ->setOptions('iDisplayLength', 100)
                                        ->setOptions(
                                          "aoColumnDefs", array(
                                            [ "bSortable" => false, "aTargets" => [3] ]
                                          )
                                        )
                                        ->render() !!}

                                </div>
                            </div>

                        </div>
                    </div>


                    <div role="tabpanel" class="tab-pane fade in" id="economico">
                        {!! Form::model($ficha, array('route' => array('manage.convocatorias.vuelos.ficha', $ficha->id))) !!}

                            {!! Form::hidden('tab','#economico') !!}

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'precio', 'texto'=> 'Precio'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'tasas', 'texto'=> 'Tasas'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'margen', 'texto'=> 'Margen'])
                            </div>

                            <div class="form-group">

                            </div>

                            @include('includes.form_submit', [ 'permiso'=> 'precios-vuelo', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>


                    <div role="tabpanel" class="tab-pane fade in" id="resumen">

                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <caption></caption>
                            <thead>
                            <tr>
                                <th class="col-md-6">IDA</th>
                                <th class="col-md-6">VUELTA</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        @foreach($ficha->etapas->where('tipo',0) as $v)
                                            {{Carbon::parse($v->salida_fecha)->format('d/m/Y')}} {{$v->company}} {{$v->vuelo}}
                                            <br>DEPARTURE <br>{{$v->salida_aeropuerto}} {{substr($v->salida_hora,0,5)}}H
                                            <br>ARRIVAL <br>{{$v->llegada_aeropuerto}} {{Carbon::parse($v->llegada_fecha)->format('d/m/Y')}} {{substr($v->llegada_hora,0,5)}}H
                                            <br>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($ficha->etapas->where('tipo',1) as $v)
                                            {{Carbon::parse($v->salida_fecha)->format('d/m/Y')}} {{$v->company}} {{$v->vuelo}}
                                            <br>DEPARTURE <br>{{$v->salida_aeropuerto}} {{substr($v->salida_hora,0,5)}}H
                                            <br>ARRIVAL <br>{{$v->llegada_aeropuerto}} {{Carbon::parse($v->llegada_fecha)->format('d/m/Y')}} {{substr($v->llegada_hora,0,5)}}H
                                            <br>
                                        @endforeach
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>


                    <div role="tabpanel" class="tab-pane fade in" id="etapas">

                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <caption>Etapas</caption>
                            <thead>
                                <tr>
                                    <th class="col-md-1">Tipo</th>
                                    <th class="col-md-3">Compañia</th>
                                    <th class="col-md-1">Nº Vuelo</th>
                                    <th class="col-md-3">Salida</th>
                                    <th class="col-md-3">Llegada</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($ficha->etapas as $etapa)
                                <tr>
                                {!! Form::open(array('method' => 'POST', 'url' => route('manage.convocatorias.vuelos.etapas.ficha',$ficha->id), 'role' => 'form', 'class' => 'form-inline')) !!}
                                    {!! Form::hidden('etapa_id', $etapa->id) !!}

                                    <td>{{$etapa->tipo?'Vuelta':'Ida'}}</td>
                                    <td>
                                        @include('includes.form_input_text', [ 'campo'=> 'company', 'texto'=> null, 'required'=>true, 'valor'=> $etapa->company])
                                    </td>
                                    <td>
                                        @include('includes.form_input_text', [ 'campo'=> 'vuelo', 'texto'=> null, 'required'=>true, 'valor'=> $etapa->vuelo])
                                    </td>
                                    <td>
                                        @include('includes.form_input_text', [ 'campo'=> 'salida_aeropuerto', 'texto'=> null, 'required'=>true, 'valor'=> $etapa->salida_aeropuerto])
                                        @include('includes.form_input_datetime', [ 'campo'=> 'salida_fecha', 'texto'=> null, 'required'=>true,
                                            'valor'=> Carbon::parse($etapa->salida_fecha)->format('d/m/Y')])
                                        @include('includes.form_input_datetime_hora', [ 'campo'=> 'salida_hora', 'texto'=> null, 'required'=>true,
                                            'valor'=> $etapa->salida_hora])
                                    </td>
                                    <td>
                                        @include('includes.form_input_text', [ 'campo'=> 'llegada_aeropuerto', 'texto'=> null, 'required'=>true, 'valor'=> $etapa->llegada_aeropuerto])
                                        @include('includes.form_input_datetime', [ 'campo'=> 'llegada_fecha', 'texto'=> null, 'required'=>true,
                                            'valor'=> Carbon::parse($etapa->llegada_fecha)->format('d/m/Y')])
                                        @include('includes.form_input_datetime_hora', [ 'campo'=> 'llegada_hora', 'texto'=> null, 'required'=>true,
                                            'valor'=> $etapa->llegada_hora])
                                    </td>
                                    <td>
                                        @include('includes.form_submit', [ 'permiso'=> 'vuelos', 'texto'=> 'Actualizar'])


                                        <a href='#destroy'
                                            data-label='Borrar'
                                            data-model='Etapa'
                                            data-action='{{route( 'manage.convocatorias.vuelos.etapas.delete', $etapa->id)}}'
                                            data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>
                                    </td>
                                {!! Form::close() !!}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <hr>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-plus-circle fa-fw"></i> Añadir:
                            </div>
                            <div id="vuelo-add" class="panel-body">

                                {!! Form::model($ficha, array('route' => array('manage.convocatorias.vuelos.etapas.ficha', $ficha->id))) !!}

                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Ida/Vuelta', 'select'=> ['Ida','Vuelta']])
                                        </div>
                                        <div class="col-md-5">
                                            @include('includes.form_input_text', [ 'campo'=> 'company', 'texto'=> 'Compañia', 'required'=>true])
                                        </div>
                                        <div class="col-md-5">
                                            @include('includes.form_input_text', [ 'campo'=> 'vuelo', 'texto'=> 'Nº Vuelo', 'required'=>true])
                                        </div>
                                    </div>

                                    <h5>SALIDA</h5>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            @include('includes.form_input_text', [ 'campo'=> 'salida_aeropuerto', 'texto'=> 'Aeropuerto', 'required'=>true])
                                        </div>
                                        <div class="col-md-4">
                                            @include('includes.form_input_datetime', [ 'campo'=> 'salida_fecha', 'texto'=> 'Día', 'required'=>true])
                                        </div>
                                        <div class="col-md-2">
                                            @include('includes.form_input_datetime_hora', [ 'campo'=> 'salida_hora', 'texto'=> 'Hora', 'required'=>true])
                                        </div>
                                    </div>

                                    <h5>LLEGADA</h5>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            @include('includes.form_input_text', [ 'campo'=> 'llegada_aeropuerto', 'texto'=> 'Aeropuerto', 'required'=>true])
                                        </div>
                                        <div class="col-md-4">
                                            @include('includes.form_input_datetime', [ 'campo'=> 'llegada_fecha', 'texto'=> 'Día', 'required'=>true])
                                        </div>
                                        <div class="col-md-2">
                                            @include('includes.form_input_datetime_hora', [ 'campo'=> 'llegada_hora', 'texto'=> 'Hora', 'required'=>true])
                                        </div>
                                    </div>

                                    @include('includes.form_submit', [ 'permiso'=> 'vuelos', 'texto'=> 'Añadir'])

                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>


                    <div role="tabpanel" class="tab-pane fade in" id="convocatorias">
                        @include('manage.convocatorias.vuelos.list-convocatorias', ['vuelo_id'=> $ficha->id])
                    </div>

                </div>


            </div>
        </div>

@stop