@extends('layouts.manage')

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-plane fa-fw"></i>
                    Vuelo :: Nuevo
            </div>
            <div class="panel-body">

                {!! Form::open(array('route' => array('manage.convocatorias.vuelos.ficha', 0))) !!}

                    {!! Form::hidden('convocatoria_id', $convo_id) !!}

                    <div class="form-group row">
                        <div class="col-md-10">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Vuelo'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select', [ 'campo'=> 'any', 'texto'=> 'Año', 'select'=> ConfigHelper::getAnys()])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'info', 'texto'=> 'Información'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'transporte', 'texto'=> 'Tipo Transporte', 'select'=> ConfigHelper::getTipoTransporte()])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'agencia_id', 'texto'=> 'Agencia', 'select'=> $agencias])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'localizador', 'texto'=> 'Localizador'])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'individual', 'texto'=> 'Individual'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'interno', 'texto'=> 'Interno (no web)'])
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'vuelos', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

<script type="text/javascript">
$(document).ready(function() {
    if ($('convocatory_semiopen').is(':checked'))
    {
        $('#semicerrada').show();
    }

    $("#convocatory_semiopen").click(function(){
        $("#semicerrada").slideToggle();
    });
});
</script>
@stop