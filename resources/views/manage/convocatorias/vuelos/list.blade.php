
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-plane"></i> Vuelos

            <span class="pull-right"><a href="{{ route('manage.convocatorias.vuelos.nuevo', (isset($ficha) ? $ficha->id : 0) ) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Vuelo</a></span>

        </div>
        <div class="panel-body">

        @if(isset($anys))

            {!! Form::open(['route' => array('manage.convocatorias.vuelos.index'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-1">
                        {!! Form::label('any', 'Año') !!}
                        <br>
                        {!! Form::select('any', $anys, $any, array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any'))  !!}
                    </div>

                    <div class="col-md-1">
                        {!! Form::label('&nbsp;') !!}
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>
                </div>

            {!! Form::close() !!}
            <hr>

            @if(!$listado)
                <br>
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                {!! Datatable::table()
                  ->addColumn([
                    'name'              => 'Nombre',
                    'plazas_totales'    => 'Plazas Totales',
                    'plazas_umbral'     => 'P.Umbral',
                    'plazas_bloqueadas' => "P.Bloqueadas",
                    'plazas_monitor'    => "P.Monitor",
                    'plazas_reservas'   => 'P.Reservas',
                    'plazas_prereservas'=> 'P.Pre-Reservas',
                    'plazas_overbooking'=> 'P.Overbooking',
                    'plazas_disponibles'=> 'P.Disponibles',
                    'options'           => ''

                  ])
                  ->setUrl( route('manage.convocatorias.vuelos.index', [$convocatoria_id, 'any'=>$any]) )
                  ->setOptions('iDisplayLength', 100)
                  ->setOptions(
                    "aoColumnDefs", array(
                      [ "bSortable" => false, "aTargets" => [0] ]
                    )
                  )
                  ->render() !!}

            @endif

        @else

            {!! Datatable::table()
                ->addColumn([
                  'name'              => 'Nombre',
                  'plazas_totales'    => 'Plazas Totales',
                  'plazas_umbral'     => 'P.Umbral',
                  'plazas_bloqueadas' => "P.Bloqueadas",
                  'plazas_reservas'   => 'P.Reservas',
                  'plazas_prereservas'=> 'P.Pre-Reservas',
                  'plazas_overbooking'=> 'P.Overbooking',
                  'plazas_disponibles'=> 'P.Disponibles',
                  'options'           => ''

                ])
                ->setUrl( route('manage.convocatorias.vuelos.index', $convocatoria_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [0] ]
                  )
                )
                ->render() !!}

        @endif

        </div>
    </div>
