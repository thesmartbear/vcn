@extends('layouts.manage')


@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.nuevo') !!} --}}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-building fa-fw"></i> Nueva Oficina
            </div>
            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.system.oficinas.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'direccion', 'texto'=> 'Dirección'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'cp', 'texto'=> 'CP'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'poblacion', 'texto'=> 'Población'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'provincia_id', 'texto'=> 'Provincia', 'select'=> $provincias])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'pais_id', 'texto'=> 'País', 'select'=> $paises])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'persona', 'texto'=> 'Persona contacto'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'telefono', 'texto'=> 'Teléfono'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'E-mail'])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'banco', 'texto'=> 'Banco'])
                        </div>
                        <div class="col-md-8">
                            @include('includes.form_input_text', [ 'campo'=> 'iban', 'texto'=> 'IBAN'])
                        </div>
                    </div>

                    @include('includes.form_plataforma', ['campo'=> 'propietario'])

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'activa', 'texto'=> 'Activa'])
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'oficinas', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop