@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.index') !!} --}}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-building fa-fw"></i> Oficinas
                <span class="pull-right"><a href="{{ route('manage.system.oficinas.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Oficina</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Oficina',
                      'plataforma'      => 'Plataforma',
                      'activa'          => 'Activa',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.system.oficinas.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [2] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop