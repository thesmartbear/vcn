@extends('layouts.manage')

@section('breadcrumb')
     {!! Breadcrumbs::render('manage.system.documentos.ficha', $ficha) !!}
@stop


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-file fa-fw"></i> Documento :: {{$ficha->name}}
    </div>
    <div class="panel-body">

        {!! Form::model( $ficha, array('route' => array('manage.system.docs.ficha', $ficha->id), 'files'=>true) ) !!}

            @php
                $dir = "/assets/uploads/doc_especificos/";
                $dir = $dir . $ficha->modelo ."/". $ficha->modelo_id ."/0/";
            @endphp

            {!! Form::hidden('modelo', $ficha->modelo) !!}
            {!! Form::hidden('modelo_id', $ficha->modelo_id) !!}
            {!! Form::hidden('tipo', 1) !!}

            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'doc_name', 'texto'=> 'Nombre', 'valor'=> $ficha->name])
                </div>
                <div class="col-md-5">
                    @include('includes.form_input_text', [ 'campo'=> 'doc_notas', 'texto'=> 'Notas', 'valor'=> $ficha->notas])
                </div>
                <div class="col-md-3">
                    @include('includes.form_input_file', [ 'campo'=> "doc_adjunto", 'texto'=> 'Documento (descargable)'])
                    @if($ficha && $ficha->doc)
                        {!! ConfigHelper::iframe($ficha->doc, public_path($dir), $dir) !!}
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])
                </div>
            </div>

        {!! Form::close() !!}
        
        <hr>
    
        {!! Form::model( $ficha, array('route' => array('manage.system.docs.ficha.langs', $ficha->id), 'files'=>true) ) !!}

            @foreach(ConfigHelper::idiomas() as $key=>$idioma)

            <?php
                $fLang = $ficha->getLang($idioma);
                $dir = "/assets/uploads/doc_especificos/";
                $dir = $dir . $ficha->modelo ."/". $ficha->modelo_id . "/$idioma/";
            ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-file fa-fw"></i> Documento :: {{$ficha->name}} :: {{$idioma}}
                </div>
                <div class="panel-body">

                    <div class="form-group row">
                        <div class="col-md-7">
                            @include('includes.form_input_text', [ 'campo'=> "name_$idioma", 'texto'=> 'Nombre', 'valor'=> ($fLang ? $fLang->name : ""), 'required'=> true])
                        </div>
                        <div class="col-md-5">
                            @include('includes.form_input_file', [ 'campo'=> "doc_$idioma", 'texto'=> 'Documento (descargable)'])
                            @if($fLang && $fLang->doc)
                                {!! ConfigHelper::iframe($fLang->doc, public_path($dir), $dir) !!}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> "notas_$idioma", 'texto'=> 'Notas', 'valor'=> ($fLang ? $fLang->notas : "")])
                    </div>

                </div>
            </div>

            <hr>

            @endforeach


            @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

@stop