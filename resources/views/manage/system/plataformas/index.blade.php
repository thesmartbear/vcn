@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.plataformas.index') !!}
@stop

@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-cog fa-fw"></i> Plataformas
    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
                'id'=>  'ID',
                'name'    => 'Plataforma',
                'web'     => 'Web',
                'email'   => 'E-mail',
                'tema'      => 'Plantilla',
                'web_estructura'      => 'Estructura',
                'ganalytics' => 'G.Analytics'
            ])
            ->setUrl(route('manage.system.plataformas.index'))
            ->setOptions(
                "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [2,3,4] ]
                )
            )
            ->render() !!}

        <hr>
        <blockquote>
            <p>Las plataformas nuevas se dan de alta por el SuperAdmin.</p>
        </blockquote>

    </div>
</div>

@stop