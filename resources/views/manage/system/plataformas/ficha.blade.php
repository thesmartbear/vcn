@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.plataformas.ficha', $ficha) !!}
@stop


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-user-secret fa-fw"></i> Plataforma :: {{$ficha->name}}
    </div>
    <div class="panel-body">


        {!! Form::open(array('method' => 'POST', 'files'=> true, 'url' => route('manage.system.plataformas.ficha',$ficha->id), 'role' => 'form', 'class' => '')) !!}

            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'web', 'texto'=> 'Web'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'E-mail'])
                </div>
            </div>

            <hr>

            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_select', [ 'campo'=> 'tema', 'texto'=> 'Plantilla/Tema', 'select'=> ConfigHelper::config('temas')])
                </div>
                <div class="col-md-2">
                    @include('includes.form_select', [ 'campo'=> 'ruta_raiz', 'texto'=> 'Ruta raiz web (web/?)', 'select'=> ConfigHelper::getTipoRaiz()])
                </div>
                <div class="col-md-1">
                    @include('includes.form_select', [ 'campo'=> 'web_estructura', 'texto'=> 'Estructura', 'select'=> [1=> "v.1", 2=> "v.2"]])
                </div>
                <div class="col-md-3">
                    @include('includes.form_input_text', [ 'campo'=> 'logo', 'texto'=> 'Logo' ])
                </div>
                <div class="col-md-3">
                    @include('includes.form_input_text', [ 'campo'=> 'logoweb', 'texto'=> 'Logo Web' ])
                </div>
                <div class="col-md-1">
                    @include('includes.form_input_text', [ 'campo'=> 'sufijo', 'texto'=> 'Sufijo' ])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'area', 'texto'=> 'Área de cliente' ])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'area_url', 'texto'=> 'Área de cliente URL' ])
                </div>
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'web_registro', 'texto'=> 'Registro Web (dippy)' ])
                </div>
                <div class="col-md-1">
                    @include('includes.form_checkbox', [ 'campo'=> 'es_rgpd', 'texto'=> 'RGPD' ])
                </div>
                <div class="col-md-3">
                    @include('includes.form_select', [ 'campo'=> 'catalogos_oficina_id', 'texto'=> 'Datos Catálogo', 'select'=> [0=> "Inactivo"] + $oficinas ])
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'facturas', 'texto'=> 'Facturas' ])
                </div>
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'seguro', 'texto'=> 'Seguro' ])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_number', [ 'campo'=> 'extra_aporte', 'texto'=> 'Extra Aporte' ])
                </div>
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'es_notificaciones', 'texto'=> 'Notificaciones' ])
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'chat_smartsupp', 'texto'=> 'Chat widget' ])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'whatsapp', 'texto'=> 'WhatsApp' ])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'whatsapp_pre', 'texto'=> 'Prefijo para WhatsApp' ])
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    @include('includes.form_textarea', [ 'campo'=> 'simplybook', 'texto'=> 'SimplyBook widget', 'help' => "Sólo la parte de var widget = new... hasta el </script>" ])
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    @include('includes.form_textarea', [ 'campo'=> 'code_header', 'texto'=> 'Código HEAD', 'help' => "Código HTML, js, ..." ])
                </div>
                <div class="col-md-6">
                    @include('includes.form_textarea', [ 'campo'=> 'code_footer', 'texto'=> 'Código FOOTER', 'help' => "Código HTML, js, ..." ])
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'factura_pie', 'texto'=> 'Razón Social' ])
                </div>
                <div class="col-md-8">
                    @include('includes.form_input_text', [ 'campo'=> 'factura_iva_pie', 'texto'=> 'Pie' ])
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-tag fa-fw"></i> Moneda
                </div>
                <div class="panel-body">

                    <div class="form-group row">

                        <div class="col-md-2">
                            @include('includes.form_select', [ 'campo'=> 'moneda', 'texto'=> 'Nombre', 'select'=> $monedas])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'moneda_format', 'texto'=> 'Formato moneda', 'help'=> "%s %s"])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'moneda_locale', 'texto'=> 'Locale', 'help'=> "es_ES, es_CA, ..."])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select', [ 'campo'=> 'moneda_miles', 'texto'=> 'Miles', 'select'=> ['.'=>'. (punto)',','=>', (coma)'] ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select', [ 'campo'=> 'moneda_decimales', 'texto'=> 'Decimales', 'select'=> ['.'=>'. (punto)',','=>', (coma)'] ])
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_select', [ 'campo'=> 'idioma', 'texto'=> 'Idioma por defecto', 'select'=> ConfigHelper::getIdiomaWeb() ])
                </div>
                <div class="col-md-3">
                    @include('includes.form_select_multi', [ 'class'=> 'multiselect-basic', 'campo'=> 'idiomas', 'texto'=> 'Idiomas WEB', 'select'=> ConfigHelper::getIdiomaWeb(), 'valor'=> explode(',', $ficha->idiomas) ])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'ganalytics', 'texto'=> 'Google Analytics' ])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'gtm', 'texto'=> 'Google Tag Manager' ])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'gads', 'texto'=> 'Google Ads' ])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'facebook', 'texto'=> 'Facebook Pixel Code' ])
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-envelope fa-fw"></i> Avisos * (Para Booking nuevos y cancelados, se configuran desde <a href="{{route('manage.categorias.index')}}">Categorías</a>)
                </div>
                <div class="panel-body">

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_select_multi', [ 'campo'=> 'avisos[vuelo]', 'texto'=> 'Avisos Vuelo', 'valor'=> $ficha->avisos['vuelo'], 'select'=> $asignados ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select_multi', [ 'campo'=> 'avisos[overbooking]', 'texto'=> 'Avisos Overbooking', 'valor'=> $ficha->avisos['overbooking'], 'select'=> $asignados ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select_multi', [ 'campo'=> 'avisos[informes]', 'texto'=> 'Informes (Menores12)', 'valor'=> isset($ficha->avisos['informes'])?$ficha->avisos['informes']:[], 'select'=> $asignados ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select_multi', [ 'campo'=> 'avisos[informe-pagos]', 'texto'=> 'Informes Pagos', 'valor'=> isset($ficha->avisos['informe-pagos'])?$ficha->avisos['informe-pagos']:[], 'select'=> $asignados ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select_multi', [ 'campo'=> 'avisos[cambio-importes]', 'texto'=> 'Cambio importes', 'valor'=> isset($ficha->avisos['cambio-importes'])?$ficha->avisos['cambio-importes']:[], 'select'=> $asignados ])
                        </div>
                    </div>

                    {{-- <hr>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-clock-o fa-fw"></i> CronJobs
                        </div>
                        <div class="panel-body">

                        </div>
                    </div> --}}

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-tag fa-fw"></i> Status Booking *
                </div>
                <div class="panel-body">

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_booking[prereserva]', 'valor'=> $ficha->status_booking['prereserva'], 'texto'=> 'Pre-Reserva', 'select'=> $status_booking ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_booking[prebooking]', 'valor'=> $ficha->status_booking['prebooking'], 'texto'=> 'Pre-Booking/Reserva', 'select'=> $status_booking ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_booking[presalida]', 'valor'=> $ficha->status_booking['presalida'], 'texto'=> 'Pre-Salida', 'select'=> $status_booking ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_booking[overbooking]', 'valor'=> $ficha->status_booking['overbooking'], 'texto'=> 'Overbooking', 'select'=> $status_booking ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_booking[archivado]', 'valor'=> $ficha->status_booking['archivado'], 'texto'=> 'Archivado', 'select'=> $status_booking ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_booking[cancelado]', 'valor'=> $ficha->status_booking['cancelado'], 'texto'=> 'Cancelado', 'select'=> $status_booking ])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_booking[refund]', 'valor'=> $ficha->status_booking['refund'], 'texto'=> 'Refund', 'select'=> $status_booking ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_booking[vuelta]', 'valor'=> $ficha->status_booking['vuelta'], 'texto'=> 'Vuelta', 'select'=> $status_booking ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_booking[fuera]', 'valor'=> $ficha->status_booking['fuera'], 'texto'=> 'Fuera de Casa', 'select'=> $status_booking ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_booking[listo]', 'valor'=> $ficha->status_booking['listo'], 'texto'=> 'Listo para Viajar', 'select'=> $status_booking ])
                        </div>
                        <div class="col-md-2">
                            <?php
                                $st = isset($ficha->status_booking['borrador'])?$ficha->status_booking['borrador']:0;
                            ?>
                            @include('includes.form_select',
                                [ 'campo'=> 'status_booking[borrador]', 'valor'=> $st, 'texto'=> 'Borrador', 'select'=> $status_booking ])
                        </div>
                    </div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-tag fa-fw"></i> Status Solicitud *
                </div>
                <div class="panel-body">

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_solicitud[lead]', 'valor'=> $ficha->status_solicitud['lead'], 'texto'=> 'Lead', 'select'=> $status_solicitud ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_solicitud[contactado]', 'valor'=> $ficha->status_solicitud['contactado'], 'texto'=> 'Contactado', 'select'=> $status_solicitud ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_solicitud[futuro]', 'valor'=> $ficha->status_solicitud['futuro'], 'texto'=> 'Futuro', 'select'=> $status_solicitud ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_solicitud[decidiendo]', 'valor'=> $ficha->status_solicitud['decidiendo'], 'texto'=> 'Decidiendo', 'select'=> $status_solicitud ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_solicitud[archivado]', 'valor'=> $ficha->status_solicitud['archivado'], 'texto'=> 'Archivado/Descartado', 'select'=> $status_solicitud ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select',
                                [ 'campo'=> 'status_solicitud[inscrito]', 'valor'=> $ficha->status_solicitud['inscrito'], 'texto'=> 'Inscrito', 'select'=> $status_solicitud ])
                        </div>
                    </div>

                </div>
            </div>

            <hr>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-credit-card fa-fw"></i> Booking Online
                </div>
                <div class="panel-body">
                    <div class="col-md-1">
                        @include('includes.form_checkbox', [ 'campo'=> 'tpv_activo', 'texto'=> 'TPV' ])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_checkbox', [ 'campo'=> 'tpv_activo_banco', 'texto'=> 'Transferencia' ])
                    </div>

                    <div class="col-md-3">
                        @include('includes.form_select', [ 'campo'=> 'tpv_asign_to', 'texto'=> 'Asignados a', 'select'=> $asignados ])
                    </div>

                    <div class="col-md-3">
                        @include('includes.form_checkbox', [ 'campo'=> 'tpv_aeropuertos', 'texto'=> 'Gestión Aeropuertos BCN/MAD/OTRO' ])
                    </div>

                    <div class="col-md-2">
                        @include('includes.form_select', [ 'campo'=> 'oficina_id', 'texto'=> 'Oficina', 'select'=> $oficinas ])
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-credit-card fa-fw"></i> Configuración TPV
                </div>
                <div class="panel-body">

                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_input_text',
                                [ 'campo'=> 'tpv[comercio]', 'valor'=> $ficha->tpv['comercio'], 'texto'=> 'Comercio' ])
                        </div>
                        <div class="col-md-1">
                            @include('includes.form_input_text',
                                [ 'campo'=> 'tpv[terminal]', 'valor'=> $ficha->tpv['terminal'], 'texto'=> 'Terminal' ])
                        </div>
                        <div class="col-md-1">
                            @include('includes.form_input_text',
                                [ 'campo'=> 'tpv[moneda]', 'valor'=> $ficha->tpv['moneda'], 'texto'=> 'Moneda' ])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text',
                                [ 'campo'=> 'tpv[clave]', 'valor'=> $ficha->tpv['clave'], 'texto'=> 'Clave' ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'tpv[real]', 'texto'=> 'Entorno real', 'valor'=> isset($ficha->tpv['real'])?$ficha->tpv['real']:0  ])
                        </div>
                    </div>

                </div>

            </div>


            <hr>

            <div class="panel panel-red">
                <div class="panel-heading">
                    <i class="fa fa-info fa-fw"></i> Notas
                </div>
                <div class="panel-body">
                    Los parámetros: <strong>propietario, backend, frontend</strong>
                    <br>
                    deben estar configurados en config/vcn.php:<br><br>
                    [url-de-la-web] //=http://url.de.la.web<br>
                    'propietario' => 1/2/...,<br>
                    'backend' => 0/1,<br>
                    'frontend' => 0/1,<br>
                </div>
            </div>

            @include('includes.form_submit', [ 'permiso'=> 'full-admin', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

@stop