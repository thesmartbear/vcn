@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.roles.ficha', $ficha) !!}
@stop


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-user-secret fa-fw"></i> Permisos :: {{$ficha->name}}
    </div>
    <div class="panel-body">


        {!! Form::open(array('method' => 'POST', 'url' => route('manage.system.roles.ficha',$ficha->id), 'role' => 'form', 'class' => '')) !!}

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
            </div>

            <div class="form-group">

                {!! Form::label('Permisos') !!}:

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    @foreach($permisos as $p1k=>$p1v)
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading-{{$p1k}}">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$p1k}}" aria-expanded="true" aria-controls="collapse-{{$p1k}}">
                            {{$p1k}}
                            </a>
                          </h4>
                        </div>
                        <div id="collapse-{{$p1k}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{$p1k}}">
                          <div class="panel-body">

                            <table class="table table-bordered permisos">
                                <thead>
                                    <tr>
                                        <th>Permisos</th>
                                        <th>Ver</th>
                                        <th>Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($p1v as $permiso)
                                    <tr>
                                        <td>{{$permiso}}</td>
                                        <td>
                                            {!! Form::checkbox("view[$permiso]", true, (isset($permisos_user->$permiso)?$permisos_user->$permiso->view:0), ['data-permiso'=>$permiso]) !!}
                                        </td>
                                        <td>
                                            {!! Form::checkbox("edit[$permiso]", true, (isset($permisos_user->$permiso)?$permisos_user->$permiso->edit:0), ['data-permiso'=>$permiso]) !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                          </div>
                        </div>
                    </div>
                    @endforeach

                    <hr>
                    <div class="form-group">
                        {{-- informes: bloques y detalle --}}
                        <table class="table table-bordered permisos">
                            <caption><h4>Permiso Informes</h4></caption>
                            <thead>
                                <tr>
                                    <th>Bloque</th>
                                    <th>Permiso</th>
                                    <th>Detalle</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($informes_bloques as $kb=>$bloque)
                                    <tr>
                                        <td>{{$bloque}} <br>
                                            <?php
                                                $permiso = 'informes';
                                                $val = (isset($permisos_user->$permiso->bloques->$bloque->view)?$permisos_user->$permiso->bloques->$bloque->view:0)
                                            ?>
                                        </td>
                                        <td>
                                            {!! Form::checkbox("informes[$bloque][view]", true, $val) !!}
                                        </td>

                                        <td>

                                            <table class="table table-bordered permisos">
                                            <thead>
                                                <tr>
                                                    <th>Informe</th>
                                                    <th>Permiso</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach( $informes_menu[$kb]['submenu'] as $menu )

                                                    @if($menu['nom'] != '-')

                                                        <?php
                                                            $permiso = 'informes';
                                                            // $detalle = $menu['permiso'];
                                                            $ruta = $menu['route'];
                                                            $detalle = ConfigHelper::menuInformePermiso($ruta);

                                                            $val = (isset($permisos_user->$permiso->bloques->$bloque)?$permisos_user->$permiso->bloques->$bloque:0);

                                                            if($val)
                                                            {
                                                                $val = (isset($permisos_user->$permiso->bloques->$bloque->detalles)?$permisos_user->$permiso->bloques->$bloque->detalles:0);

                                                                if($val)
                                                                {
                                                                    $val = (isset($permisos_user->$permiso->bloques->$bloque->detalles->$detalle)?$permisos_user->$permiso->bloques->$bloque->detalles->$detalle:0);
                                                                }
                                                            }
                                                        ?>

                                                        <tr>
                                                            <td>{{$menu['nom']}}</td>
                                                            <td>{!! Form::checkbox("informes[$bloque][detalles][$detalle]", true, $val) !!}</td>
                                                        </tr>

                                                    @endif

                                                @endforeach
                                            </tbody>
                                            </table>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>

                <div class="form-group">
                    @include('includes.form_checkbox', [ 'campo'=> 'aislado', 'texto'=> 'Aislado (No visualizar datos de otros usuarios)'])
                </div>
                <div class="form-group">
                    @include('includes.form_checkbox', [ 'campo'=> 'ver_oficinas_informes', 'texto'=> 'Ver informes de otras oficinas'])
                </div>

            </div>

            @include('includes.form_submit', [ 'permiso'=> 'full-admin', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {

    $("input[name^='edit']").click( function(e) {
        // e.preventDefault();

        var $checked = $(this).prop('checked');
        var $p = $(this).data('permiso');

        if($checked)
        {
            $("input[name='view["+$p+"]']").prop('checked',true);
            $("input[name='view["+$p+"]']").parent().addClass('checked');
        }

    });
});
</script>

@stop