@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.roles.index') !!}
@stop

@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-user-secret fa-fw"></i> Permisos
        <span class="pull-right"><a href="{{ route('manage.system.roles.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Rol </a></span>
    </div>
    <div class="panel-body">

        <strong>Full-Admin es básico del sistema.</strong> <hr>

        {!! Datatable::table()
            ->addColumn([
              'name'      => 'Rol',
              'options'   => ''
            ])
            ->setUrl(route('manage.system.roles.index'))
            ->setOptions(
              "aoColumnDefs", array(
                [ "bSortable" => false, "aTargets" => [1] ]
              )
            )
            ->render() !!}

    </div>
</div>

@stop