@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.roles.nuevo') !!}
@stop


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-user-secret fa-fw"></i> Nuevo Rol
    </div>
    <div class="panel-body">

        {!! Form::open(array('method' => 'POST', 'url' => route('manage.system.roles.ficha',0), 'role' => 'form', 'class' => '')) !!}

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
            </div>

            <div class="form-group">

                {!! Form::label('Permisos') !!}:

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    @foreach($permisos as $p1k=>$p1v)
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading-{{$p1k}}">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$p1k}}" aria-expanded="true" aria-controls="collapse-{{$p1k}}">
                            {{$p1k}}
                            </a>
                          </h4>
                        </div>
                        <div id="collapse-{{$p1k}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{$p1k}}">
                          <div class="panel-body">

                            <table class="table table-bordered permisos">
                                <thead>
                                    <tr>
                                        <th>Permisos</th>
                                        <th>Ver</th>
                                        <th>Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($p1v as $permiso)
                                    <tr>
                                        <td>{{$permiso}}</td>
                                        <td>
                                            {!! Form::checkbox("view[$permiso]", true) !!}
                                        </td>
                                        <td>
                                            {!! Form::checkbox("edit[$permiso]", true) !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                          </div>
                        </div>
                    </div>
                    @endforeach

                </div>

                <div class="form-group">
                    @include('includes.form_checkbox', [ 'campo'=> 'aislado', 'texto'=> 'Aislado (No visualizar datos de otros usuarios)'])
                </div>
                <div class="form-group">
                    @include('includes.form_checkbox', [ 'campo'=> 'ver_oficinas_informes', 'texto'=> 'Ver informes de otras oficinas'])
                </div>

            </div>

            @include('includes.form_submit', [ 'permiso'=> 'full-admin', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

@stop