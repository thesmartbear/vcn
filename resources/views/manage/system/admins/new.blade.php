@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.admins.nuevo') !!}
@stop


@section('container')
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-user-secret fa-fw"></i> Nuevo Admin
    </div>
    <div class="panel-body">

        {!! Form::open(array('method' => 'POST', 'url' => route('manage.system.admins.ficha',0), 'role' => 'form', 'class' => '')) !!}

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'fname', 'texto'=> 'Nombre'])
            </div>

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'lname', 'texto'=> 'Apellidos'])
            </div>

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'E-mail'])
            </div>

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'username', 'texto'=> 'Usuario'])
            </div>

            <div class="form-group">
                @include('includes.form_select', [ 'campo'=> 'roleid', 'texto'=> 'Rol', 'select'=> $roles])
            </div>

            <div class="form-group">
                @include('includes.form_plataforma', ['campo'=> 'plataforma', 'todas'=> false])
            </div>

            <div class="form-group">
                {{-- @include('includes.form_input_text', [ 'campo'=> 'oficina', 'texto'=> 'Oficina']) --}}
                @include('includes.form_select', [ 'campo'=> 'oficina_id', 'texto'=> 'Oficina', 'select'=> $oficinas])
            </div>

            <div class="form-group row">
                <div class="col-md-1">
                    @include('includes.form_checkbox', [ 'campo'=> 'status', 'texto'=> 'Activo'])
                </div>
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'chat_admin', 'texto'=> 'Disponible para chat'])
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6">
                    @include('includes.form_input_password', [ 'campo'=> 'password1', 'texto'=> 'Contraseña'])
                </div>
                <div class="col-md-6">
                    @include('includes.form_input_password', [ 'campo'=> 'password2', 'texto'=> 'Repite Contraseña'])
                </div>
            </div>

            @include('includes.form_submit', [ 'permiso'=> 'full-admin', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

@stop