@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.admins.index') !!}
@stop

@section('container')
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-user-secret fa-fw"></i> Administradores
        <span class="pull-right"><a href="{{ route('manage.system.admins.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Admin</a></span>
    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
              'name'      => 'Nombre',
              'username'  => 'Usuario',
              'role'      => 'Rol',
              'oficina'   => 'Oficina',
              'estado'    => 'Estado',
              'options'   => ''
            ])
            ->setUrl(route('manage.system.admins.index'))
            ->setOptions(
              "aoColumnDefs", array(
                [ "bSortable" => false, "aTargets" => [4] ]
              )
            )
            ->render() !!}

    </div>
</div>
@stop