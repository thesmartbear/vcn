@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.admins.ficha', $ficha) !!}
@stop


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-user-secret fa-fw"></i> Admin :: {{$ficha->full_name}}
    </div>
    <div class="panel-body">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Ficha</a></li>
            <li role="presentation"><a href="#actividad" aria-controls="actividad" role="tab" data-toggle="tab">Actividad</a></li>
            @if($ficha->chat_admin)
            <li role="presentation"><a href="#chats" aria-controls="chats" role="tab" data-toggle="tab">Chats</a></li>
            @endif 
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">


                {!! Form::model( $ficha, array('route' => array('manage.system.admins.ficha', $ficha->id), 'files'=>true) ) !!}

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'fname', 'texto'=> 'Nombre'])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'lname', 'texto'=> 'Apellidos'])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'E-mail'])
                </div>

                <div class="form-group row">
                    <div class="col-md-8">
                        @include('includes.form_input_text', [ 'campo'=> 'username', 'texto'=> 'Usuario', 'disabled'=>true])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_file', [ 'campo'=> 'foto', 'texto'=> 'Foto'])
                    </div>
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'roleid', 'texto'=> 'Rol', 'select'=> $roles])
                </div>

                <div class="form-group">
                    @include('includes.form_plataforma', ['campo'=> 'plataforma', 'todas'=> false])
                </div>

                <div class="form-group">
                    {{-- @include('includes.form_input_text', [ 'campo'=> 'oficina', 'texto'=> 'Oficina']) --}}
                    @include('includes.form_select', [ 'campo'=> 'oficina_id', 'texto'=> 'Oficina', 'select'=> $oficinas])
                </div>

                <div class="form-group row">
                    <div class="col-md-1">
                        @include('includes.form_checkbox', [ 'campo'=> 'status', 'texto'=> 'Activo'])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_checkbox', [ 'campo'=> 'chat_admin', 'texto'=> 'Disponible para chat'])
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6">
                        @include('includes.form_input_password', [ 'campo'=> 'password1', 'texto'=> 'Contraseña'])
                    </div>
                    <div class="col-md-6">
                        @include('includes.form_input_password', [ 'campo'=> 'password2', 'texto'=> 'Repite Contraseña'])
                    </div>
                </div>

                @include('includes.form_submit', [ 'permiso'=> 'full-admin', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="actividad">

                {!! Datatable::table()
                    ->addColumn([
                      'fecha'       => 'Fecha',
                      'hora_ini'       => 'Inicio',
                      'hora_fin'         => 'Fin',
                      'desktop_hora_ini'   => 'Desk. Ini',
                      'desktop_hora_fin'     => 'Desk. Fin',
                      'inactividad'     => 'Inactividad [max/total] (minutos)',
                      'mobile_hora_ini'     => 'Mov. Ini',
                      'mobile_hora_fin'     => 'Mov. Fin',
                      'tablet_hora_ini'     => 'Tablet Ini',
                      'tablet_hora_fin'     => 'Tablet Fin',
                    ])
                    ->setUrl(route('manage.system.admins.actividad', $ficha->id))
                    ->setOptions("order", [[ 0, "desc" ]])
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [1,2,3,4,5,6,7,8] ],
                        [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                      )
                    )
                    ->render() !!}

            </div>

            @if($ficha->chat_admin)
            <div role="tabpanel" class="tab-pane fade in" id="chats">

                {!! Datatable::table()
                    ->addColumn([
                      'fecha'       => 'Fecha',
                      'cliente'     => 'Cliente',
                      'status'      => 'Estado'
                    ])
                    ->setUrl(route('manage.chat.index', $ficha->id))
                    ->setOptions("order", [[ 0, "desc" ]])
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "targets" => [0],
                        "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                      )
                    )
                    ->render() !!}

            </div>
            @endif


        </div>

    </div>
</div>

@include('includes.script_plataforma_oficina')

@stop