@extends('layouts.manage')


@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.nuevo') !!} --}}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-phone fa-fw"></i> Nuevo Contacto SOS
            </div>
            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.system.contactos.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'contacto_name', 'texto'=> 'Nombre'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'contacto_phone', 'texto'=> 'Teléfono'])
                    </div>

                    <div class="col-md-2">
                        @include('includes.form_checkbox', [ 'campo'=> 'es_proveedor', 'texto'=> 'Proveedor'])
                    </div>
                    <div class="col-md-1">
                        @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activo'])
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'sos', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop