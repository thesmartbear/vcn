@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.index') !!} --}}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-phone fa-fw"></i> Contactos SOS {{$esProveedor ? "Proveedor" : "Plataforma"}}
                <span class="pull-right"><a href="{{ route('manage.system.contactos.ficha', 0) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Contacto</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Nombre',
                      'phone'           => 'Teléfono',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.system.contactos.index', $esProveedor))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [2] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop