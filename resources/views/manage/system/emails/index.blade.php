@extends('layouts.manage')

@section('breadcrumb')
{{--    {!! Breadcrumbs::render('manage.system.mails.index') !!}--}}
@stop

@section('container')
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-envelope-o fa-fw"></i> Emails de Sistema
    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
              'name'        => 'Nombre',
              'trigger'     => 'Trigger',
              'tipo'        => 'Tipo',
              'idiomas'        => 'Idiomas',
              'destino_notas'     => 'Destino',
              'options'   => ''
            ])
            ->setUrl(route('manage.system.emails.index'))
            ->setOptions(
              "aoColumnDefs", array(
                [ "bSortable" => false, "aTargets" => [5] ]
              )
            )
            ->render() !!}

    </div>
</div>
@stop