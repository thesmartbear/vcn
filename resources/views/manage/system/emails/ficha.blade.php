@extends('layouts.manage')

@section('breadcrumb')
{{--    {!! Breadcrumbs::render('manage.system.mails.ficha', $ficha) !!}--}}
@stop


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-envelope fa-fw"></i> SystemMail :: {{$ficha->name}} [{{$ficha->destino_tipo}}]
    </div>
    <div class="panel-body">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Ficha</a></li>

            @if(!$ficha->asunto)
                @foreach(ConfigHelper::idiomas() as $key=>$idioma)
                    <li role="presentation"><a href="#idiomas-{{$idioma}}" aria-controls="idiomas-{{$idioma}}" role="tab" data-toggle="tab">Idiomas [{{$idioma}}]</a></li>
                @endforeach
            @endif
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                <div class="form-group">
                    Destino: {{$ficha->destino_tipo}} :: {{$ficha->destino_notas}}
                    <br>
                    Trigger: {{$ficha->trigger}}
                </div>

                {!! Form::model( $ficha, array('route' => array('manage.system.emails.ficha', $ficha->id), 'files'=>true) ) !!}
                {!! Form::hidden('idioma', null) !!}

                <div class="form-group">
                    @if($ficha->asunto)
                        @include('includes.form_input_text', [ 'campo'=> 'asunto', 'texto'=> 'Asunto'])
                    @endif
                </div>

                <hr>
                @if($ficha->asunto)
                    <div class="form-group">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    {{$ficha->template}}
                                </div>
                                <div class="panel-body">

                                    {{--{!! $ficha->blade_contenido !!}--}}
                                    {{--<hr>--}}

                                    @include('includes.form_textarea_tinymce', [ 'campo'=> "contenido", 'texto'=> 'Body', 'tags'=> true])
                                </div>
                            </div>
                    </div>

                        @include('includes.form_submit', [ 'permiso'=> 'full-admin', 'texto'=> 'Guardar'])

                    {!! Form::close() !!}
                    <div class="clearfix"></div>
                    <hr>

                    <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Plantilla: {{$ficha->template}}
                                    <div class="pull-right"><a target="_blank" href="{{route('manage.system.emails.preview', [$ficha->id])}}" class="btn btn-info btn-xs">Preview</a></div>
                                </div>
                                <div class="panel-body">
                                    <iframe src="{{route('manage.system.emails.render', $ficha->id)}}" frameborder="0" width="100%" height="400px"></iframe>
                                </div>
                            </div>

                            <hr>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Plantilla: {{$ficha->blade_file}}
                                </div>
                                <div class="panel-body">
                                    {!! $ficha->blade !!}
                                </div>
                            </div>
                    </div>
                @else
                    Template: {{$ficha->template}}
                @endif

            </div>

            @if(!$ficha->asunto)
                @foreach(ConfigHelper::idiomas() as $key=>$idioma)

                    <div role="tabpanel" class="tab-pane fade in" id="idiomas-{{$idioma}}">

                        <?php
                        $lang = $ficha->getLang($idioma);
                        ?>

                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <i class="fa fa-file fa-fw"></i> SystemMailIdioma :: {{$ficha->name}} :: {{$idioma}}
                            </div>
                            <div class="panel-body">

                                {!! Form::model( $ficha, array('route' => array('manage.system.emails.ficha', $ficha->id), 'files'=>true) ) !!}

                                {!! Form::hidden('idioma', $idioma) !!}

                                <div class="form-group">
                                    @include('includes.form_input_text', [ 'campo'=> "asunto_$idioma", 'texto'=> 'Body', 'valor'=> ($lang ? $lang->asunto : "")])
                                </div>

                                <hr>
                                <div class="form-group">

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            {{$ficha->template}}
                                        </div>
                                        <div class="panel-body">

                                            {{--{!! $lang->blade_contenido !!}--}}
                                            {{--<hr>--}}

                                            @include('includes.form_textarea_tinymce', [ 'campo'=> "contenido_$idioma", 'texto'=> 'Nombre', 'valor'=> ($lang ? $lang->contenido : ""), 'tags'=> true])

                                        </div>
                                    </div>

                                </div>

                                @include('includes.form_submit', [ 'permiso'=> 'full-admin', 'texto'=> 'Guardar'])

                                {!! Form::close() !!}
                                <div class="clearfix"></div>

                                <hr>
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Plantilla: {{$idioma}}/{{$ficha->template}}
                                            <div class="pull-right"><a target="_blank" href="{{route('manage.system.emails.preview', [$ficha->id])}}" class="btn btn-info btn-xs">Preview</a></div>
                                        </div>
                                        <div class="panel-body">
                                            <iframe src="{{route('manage.system.emails.render', [$ficha->id, $idioma])}}" frameborder="0" width="100%" height="400px"></iframe>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Plantilla: {{$lang->blade_file}}
                                        </div>
                                        <div class="panel-body">
                                            {!! $lang->blade !!}
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                @endforeach
            @endif

        </div>

    </div>
</div>

@stop