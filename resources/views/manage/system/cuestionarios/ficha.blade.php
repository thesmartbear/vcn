@extends('layouts.manage')


@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.ficha', $ficha) !!} --}}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-list-alt fa-fw"></i> Cuestionario :: {{$ficha->name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Cuestionario</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::open(array('method' => 'POST', 'url' => route('manage.system.cuestionarios.ficha',$ficha->id), 'role' => 'form', 'class' => '')) !!}

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_select', [ 'campo'=> 'form', 'texto'=> 'Formulario', 'select'=> $forms])
                            </div>

                            <div class="form-group">
                                @include('includes.form_select', ['campo'=> 'destino', 'texto'=> 'Destino', 'select'=> ConfigHelper::getCuestionarioDestinatario()])
                            </div>

                            <div class="form-group">
                                @include('includes.form_checkbox', [ 'campo'=> 'test', 'texto'=> 'Test de nivel'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activo'])
                            </div>

                            @include('includes.form_plataforma', ['campo'=> 'propietario', 'todas'=> true])

                            @include('includes.form_submit', [ 'permiso'=> 'cuestionarios', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'Cuestionario',
                                'campos_text'=> [
                                    ['name'=> 'Nombre'],
                                ],
                                'campos_textarea'=> [
                                ]
                            ])

                    </div>

            </div>
        </div>

@stop