<table class="table table-bordered table-hover">
    <caption>Cuestionarios</caption>
    <thead>
        <tr>
            <th>Cuestionario</th>
            <th>Destino</th>
            <th>Estado Viajero</th>
            <th>Estado</th>
            <td>Puntuación</td>
            <td>Nivel</td>
            @foreach($booking->viajero->tutores as $t)
                <th>Tutor {{$t->full_name}}</th>
                <td>Resultado</td>
                <td>Nivel</td>
            @endforeach
        </tr>
    </thead>
    <tbody>

        @foreach($booking->area_cuestionarios as $c)
        <?php //$c = $cu->cuestionario; ?>
        <tr>
            <?php
                $respuesta = $booking->getCuestionarioRespuesta($c->id);
                $e = ConfigHelper::areaFormEstado($respuesta ? $respuesta->estado : 0);
            ?>
            <td>{{$c->name}} [{{$c->test?"Test":"Cuestionario"}}]</td>
            <td>{{$c->destino}}</td>
            <td>
                @if($respuesta && $respuesta->estado)
                    <a target='_blank' data-label='Ver Cuestionario' href='{{ route('manage.system.cuestionarios.respuesta', $respuesta->id)}}'>{{$e}}</a>
                @else
                    {{$e}}
                @endif
            </td>
            <td>{{$respuesta?$respuesta->resultado:"-"}}</td>
            <td>{{$respuesta?$respuesta->nivel:"-"}}</td>
            <td>
                @if($c->destino!="tutores")
                    {{ConfigHelper::areaFormEstado($booking->cuestionario_estado_viajero)}}
                @else
                    Tutores
                @endif
            </td>
            @foreach($booking->viajero->tutores as $t)
                <?php
                    $respuesta = $booking->getCuestionarioRespuestaTutor($c->id,$t->id);
                    $e = ConfigHelper::areaFormEstado($respuesta ? $respuesta->estado : 0);
                ?>
                <td>{{$e}}</td>
                <td>{{$respuesta?$respuesta->resultado:"-"}}</td>
                <td>{{$respuesta?$respuesta->nivel:"-"}}</td>
            @endforeach
        </tr>
        @endforeach

    </tbody>
</table>

<?php
$tRespuestas = [
    'r01'               => 'Residencia: Habitacion',
    'r02'               => 'Residencia: Aseos/Duchas',
    'r03'               => 'Residencia: Comedor',
    'r04'               => 'Residencia: Zonas comunes',
    'r05'               => 'Residencia: Lavandería',
    'r06'               => 'Residencia: Instalaciones deportivas',
    'r07'               => 'Residencia: Cantidad comida',
    'r08'               => 'Residencia: Calidad comida',
    'f01'               => 'Familia: Habitación',
    'f02'               => 'Familia: Aseos/Duchas',
    'f03'               => 'Familia: Comedor',
    'f04'               => 'Familia: Cocina',
    'f05'               => 'Familia: Cantidad comida',
    'f06'               => 'Familia: Calidad comida',
    'f07'               => 'Familia: ¿Te han lavado la ropa?',
    'f08'               => 'Familia: Comunicación',
    'f09'               => 'Familia: Hospitalidad',
    'p01'               => 'Escuela: Aulas',
    'p02'               => 'Escuela: Profesor/a',
    'p03'               => 'Escuela: Material didáctico',
    'p04'               => 'Escuela: Nivel asignado',
    'p05'               => 'Escuela: ¿Has mejorado tu nivel?',
    'p06'               => 'Actividades: Deportivas',
    'p07'               => 'Actividades: Culturales',
    'p08'               => 'Actividades: Ocio',
    'p09'               => 'Actividades: Talleres',
    'p10'               => 'Actividades: Excursiones',
    'p11'               => 'Personal: Monitor español',
    'p12'               => 'Personal: Personal del colegio',
    'p13'               => 'Personal: Personal del lugar de inscripción en España',
    'p14'               => 'Aspectos Generales: Valoración general del curso',
    'p15'               => 'Aspectos Generales: Viaje al país de destino',
    'p16'               => 'Aspectos Generales: Reunión informativa previa',
    'f10'               => 'Familia: ¿Recomendarías a la familia?',
    'f11'               => 'Familia: Transporte utilizado casa-escuela',
    'f12'               => 'Familia: Tiempo en trasladarte casa-escuela en minutos',
    'f13'               => 'Familia: ¿Cuántos transportes utilizabas casa-escuela?',
    'p17'               => 'Otros: Resume en unas cuantas palabras como ha sido tu experiencia',
    'p18'               => 'Otros: ¿Qué crees que tenemos que mejorar en nuestro programa?',
    'p19'               => 'Otros: Actividades y excursiones que añadirías al programa',
    'p20'               => 'Otros: ¿Qué nuevo programa te gustaría ver en nuestro catálogo?',
    'p21'               => 'Otros: ¿Por qué te has apuntado a éste programa?',
    'p22'               => 'Comentarios',
];

?>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
@foreach($booking->cuestionarios as $cu)
    <?php $c = $cu->cuestionario; ?>
    <li role="presentation">
        <a href="#c-{{$c->id}}-respuestas" aria-controls="c-{{$c->id}}-respuestas" role="tab" data-toggle="tab">Respuestas {{$c->name}} [{{$c->destino}}]</a>
    </li>

@endforeach
</ul>

<!-- Tab panes -->
<div class="tab-content">
@foreach($booking->cuestionarios as $cu)
<?php $c = $cu->cuestionario; ?>
<div role="tabpanel" class="tab-pane fade in" id="c-{{$c->id}}-respuestas">

<div class="panel panel-default">
    <div class="panel-heading">
        Respuestas {{$c->name}} [{{$c->destino}}] [{{$c->test?"Test":"Cuestionario"}}]:
    </div>

    @if($c->test)

        <div class="panel-body">
            <?php
                
                $r = $booking->getCuestionarioRespuesta($c->id);

                if(!$r || $r->estado == 0)
                {
                    $ret = "- Sin Resultados -";
                }
                else
                {
                    $estado = ConfigHelper::areaFormEstado($r->estado);
                    if($r->estado < 3)
                    {
                        $ret = "<a target='_blank' href='" . route('manage.system.cuestionarios.respuesta', $r->id) . "'>$estado</a>";
                    }
                    else
                    {
                        $ret = '<span class="text-warning">'.$estado.'</span>';
                    }
                }
                
                $booking_id = $booking->id;
            ?>

            {!! $ret !!}

        </div>

    @else

        <div class="panel-body">

        @if($c->destino == "tutores")

            @foreach($booking->viajero->tutores as $t)

                <?php
                $r = $booking->getCuestionarioRespuestaTutor($c->id, $t->id);

                if($r && $r->respuestas)
                {
                    $rRespuestas = json_decode($r->respuestas, true);
                }
                ?>

                <strong>Tutor :: {{ $t->full_name }}:</strong>

                @if($r && $r->respuestas)

                    @if($c->form == "padresjovenes")

                        @include('area.forms.padresjovenes-resultados-include', ['respuesta'=> json_decode($r->respuestas)])

                    @elseif($c->form == "padresmaxcamp")

                        @include('area.forms.padresmaxcamp-resultados-include', ['respuesta'=> json_decode($r->respuestas)])
                        
                    @else

                        <?php
                        for( $i=1; $i <= 8 ; $i++ )
                        {
                            $column = 'r0'.$i;
                            if (isset($rRespuestas[$column]))
                            {
                                $rR = nl2br($rRespuestas[$column]);
                            }

                            echo "<br>$tRespuestas[$column]: ";
                            echo isset($rR) ? $rR : '-';
                        }

                        for( $i=1; $i <= 9 ; $i++ )
                        {
                            $column = 'f0'.$i;
                            if (isset($rRespuestas[$column]))
                            {
                                $rR = nl2br($rRespuestas[$column]);
                            }

                            echo "<br>$tRespuestas[$column]: ";
                            echo isset($rR) ? $rR : '-';
                        }

                        for( $i=1; $i <= 16 ; $i++ )
                        {
                            if($i < 10) {
                                $column = 'p' . str_pad($i, 2, "0", STR_PAD_LEFT);
                            }else{
                                $column = 'p' . $i;
                            }

                            if (isset($rRespuestas[$column]))
                            {
                                $rR = nl2br($rRespuestas[$column]);
                            }

                            echo "<br>$tRespuestas[$column]: ";
                            echo isset($rR) ? $rR : '-';
                        }

                        for( $i=10; $i <= 13 ; $i++ )
                        {
                            $column = 'f' . $i;
                            if (isset($rRespuestas[$column]))
                            {
                                $rR = nl2br($rRespuestas[$column]);
                            }

                            echo "<br>$tRespuestas[$column]: ";
                            echo isset($rR) ? $rR : '-';
                        }

                        for( $i=17; $i <= 22 ; $i++ )
                        {
                            $column = 'p' . $i;
                            if (isset($rRespuestas[$column]))
                            {
                                $rR = nl2br($rRespuestas[$column]);
                            }

                            echo "<br>$tRespuestas[$column]: ";
                            echo isset($rR) ? $rR : '-';
                        }

                        ?>

                    @endif
                
                @else

                    - Sin Respuestas -

                @endif

                <hr>

            @endforeach

        @else

            <?php
                $r = $booking->getCuestionarioRespuesta($c->id);

                if($r && $r->respuestas)
                {
                    $rRespuestas = json_decode($r->respuestas, true);
                }
            ?>

            @if($r && $r->respuestas)

                <?php
                for( $i=1; $i <= 8 ; $i++ )
                {
                    $column = 'r0'.$i;
                    if (isset($rRespuestas[$column]))
                    {
                        $rR = nl2br($rRespuestas[$column]);
                    }

                    echo "<br>$tRespuestas[$column]: ";
                    echo isset($rR) ? $rR : '-';
                }

                for( $i=1; $i <= 9 ; $i++ )
                {
                    $column = 'f0'.$i;
                    if (isset($rRespuestas[$column]))
                    {
                        $rR = nl2br($rRespuestas[$column]);
                    }

                    echo "<br>$tRespuestas[$column]: ";
                    echo isset($rR) ? $rR : '-';
                }

                for( $i=1; $i <= 16 ; $i++ )
                {
                    if($i < 10) {
                        $column = 'p' . str_pad($i, 2, "0", STR_PAD_LEFT);
                    }else{
                        $column = 'p' . $i;
                    }

                    if (isset($rRespuestas[$column]))
                    {
                        $rR = nl2br($rRespuestas[$column]);
                    }

                    echo "<br>$tRespuestas[$column]: ";
                    echo isset($rR) ? $rR : '-';
                }

                for( $i=10; $i <= 13 ; $i++ )
                {
                    $column = 'f' . $i;
                    if (isset($rRespuestas[$column]))
                    {
                        $rR = nl2br($rRespuestas[$column]);
                    }

                    echo "<br>$tRespuestas[$column]: ";
                    echo isset($rR) ? $rR : '-';
                }

                for( $i=17; $i <= 22 ; $i++ )
                {
                    $column = 'p' . $i;
                    if (isset($rRespuestas[$column]))
                    {
                        $rR = nl2br($rRespuestas[$column]);
                    }

                    echo "<br>$tRespuestas[$column]: ";
                    echo isset($rR) ? $rR : '-';
                }

                ?>

            @else

                - Sin Respuestas -

            @endif

        @endif

    </div>

    @endif
</div>

</div>

@endforeach
</div>