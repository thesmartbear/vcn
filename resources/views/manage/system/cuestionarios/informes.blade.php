@extends('layouts.manage')

@section('breadcrumb')

@stop

@section('titulo')
    <i class="fa fa-comments fa-fw"></i> Informes
@stop

@section('extra_head')
    <style>
        td.details-control {
            cursor: pointer;
        }
        tr.details td.details-control {
        }
        .amcharts-main-div{
            min-height: 300px;
        }
    </style>
@stop

@section('container')
    <?php $scripts = ''; ?>
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros</span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.system.cuestionarios.informes'), 'method'=> 'GET', 'class' => 'form']) !!}

                 <div class="form-group row">

                    <div class="col-md-5">
                    {!! Form::label('cuestionarios', 'Cuestionario') !!}
                    @include('includes.form_input_cargando',['id'=> 'cuestionarios-cargando'])
                    <br>
                    {!! Form::select('cuestionarios', $cuestionarios, $valores['cuestionarios'], array('class'=>'select2 col-md-5', 'data-style'=>'red', 'id'=>'filtro-cuestionarios'))  !!}
                    </div>

                    {{--
                    <div class="col-md-2">
                    {!! Form::label('estados', 'Estado') !!}
                    @include('includes.form_input_cargando',['id'=> 'estados-cargando'])
                    <br>
                    {!! Form::select('estado', $estados, $valores['estado'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-estado'))  !!}
                    </div>
                    --}}

                </div>


                <div class="form-group row">
                    <div class="col-md-6">
                    {!! Form::label('cursos', 'Curso') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])

                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
                    </div>

                </div>

                <hr>


                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-3">
                        {!! Form::label('desdes','Desde (fecha salida)') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desdes', 'texto'=> null, 'valor'=> $valores['desdes']])
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('hastas','Hasta (fecha salida)') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hastas', 'texto'=> null, 'valor'=> $valores['hastas']])
                    </div>

                    <div class="col-md-1 col-md-offset-1">
                        {!! Form::label('&nbsp;') !!}
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}

        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    @if($totales)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-list-alt fa-fw"></i> Resumen :: <strong>Cuestionario {{$cuestionario->name}} {{$cuestionario->test?"[Test]":""}} :: Destino: {{$cuestionario->destino}}</strong>
                            <span class="pull-right"><a href="{{route('manage.system.cuestionarios.reclamar',$cuestionario->id)}}" data-label="Reclamar pendientes" class="btn btn-warning btn-xs"><i class="fa fa-bullhorn"></i></a></span>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-12">

                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Total Bookings</th>
                                                <th>Viajeros</th>
                                                <th>Pendientes</th>
                                                <th>Entregados</th>
                                                <th>Aceptados</th>
                                                @if($cuestionario->form == 'testnivelcoloniascic')
                                                <th>Offline</th>
                                                @endif
                                                <th>Tutores</th>
                                                <th>Pendientes</th>
                                                <th>Entregados</th>
                                                <th>Aceptados</th>
                                                @if($cuestionario->form == 'testnivelcoloniascic')
                                                <th>Offline</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{$totales['bookings']}}</td>
                                                <td>{{$totales['totalv']}}</td>
                                                <td>{{$totales['totalv_estados'][0]}}</td>
                                                <td>{{$totales['totalv_estados'][1]}}</td>
                                                <td>{{$totales['totalv_estados'][2]}}</td>
                                                @if($cuestionario->form == 'testnivelcoloniascic')
                                                <td>{{$totales['totalv_estados'][3]}}</td>
                                                @endif
                                                <td>{{$totales['totalt']}}</td>
                                                <td>{{$totales['totalt_estados'][0]}}</td>
                                                <td>{{$totales['totalt_estados'][1]}}</td>
                                                <td>{{$totales['totalt_estados'][2]}}</td>
                                                @if($cuestionario->form == 'testnivelcoloniascic')
                                                <td>{{$totales['totalt_estados'][3]}}</td>
                                                @endif
                                            </tr>
                                        </tbody>
                                    </table>

                                    <p>

                                        @if ($valores['cursos'] == 0)
                                            Cursos:
                                            <ul>
                                            @foreach($totales['cursos'] as $ck => $c)
                                                @if($ck != 0)
                                                    <li><b>{{$c}}</b></li>
                                                @endif
                                            @endforeach
                                            </ul>
                                        @else
                                            Curso: <b>{{$totales['cursos'][$valores['cursos']]}}</b>
                                        @endif

                                    </p>

                                </div>

                            </div>
                        </div>
                    </div>
                    @else
                        <div class="alert alert-info" role="alert">
                            <p>Totales solo si el filtro es por Cuestionario</p>
                        </div>
                    @endif

                    @if($cuestionario->form == 'padresjovenes')
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-globe fa-fw"></i> Informe de Resultados
                            </div>
                            <div class="panel-body">

                                @foreach($totalesRespuestas as $k => $v)

                                    <?php
                                    $v = array_replace($v,array_fill_keys(array_keys($v, null),''));
                                    $total = array_count_values($v);
                                    ?>

                                    @if($k == 'r01' || $k == 'r02' || $k == 'r03' || $k == 'r04' || $k == 'r05')
                                        <?php
                                            $scripts .= '
                                            var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                "type": "pie",
                                                "startDuration": 0,
                                                "theme": "light",
                                                "addClassNames": true,
                                                "legend":{
                                                    "position":"right",
                                                    "marginRight":100,
                                                    "autoMargins":false
                                                },
                                                "innerRadius": "30%",
                                                "defs": {
                                                    "filter": [{
                                                        "id": "shadow",
                                                        "width": "200%",
                                                        "height": "200%",
                                                        "feOffset": {
                                                            "result": "offOut",
                                                            "in": "SourceAlpha",
                                                            "dx": 0,
                                                            "dy": 0
                                                        },
                                                        "feGaussianBlur": {
                                                            "result": "blurOut",
                                                            "in": "offOut",
                                                            "stdDeviation": 5
                                                        },
                                                        "feBlend": {
                                                            "in": "SourceGraphic",
                                                            "in2": "blurOut",
                                                            "mode": "normal"
                                                        }
                                                    }]
                                                },
                                                "dataProvider": [
                                                    {
                                                        "respuesta": "'.trans('forms.muydeacuerdo').'",
                                                        "numero": ';
                                                                  if(isset($total['muydeacuerdo'])){
                                                                    $scripts .= $total['muydeacuerdo'];
                                                                  }else{
                                                                      $scripts .= '0';
                                                                  }
                                                    $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.deacuerdo').'",
                                                        "numero": ';
                                                                   if(isset($total['deacuerdo'])){
                                                                         $scripts .= $total['deacuerdo'];
                                                                   }else{
                                                                         $scripts .= '0';
                                                                   }
                                                    $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.endesacuerdo').'",
                                                        "numero": ';
                                                                                if(isset($total['endesacuerdo'])){
                                                                                    $scripts .= $total['endesacuerdo'];
                                                                                }else{
                                                                                    $scripts .= '0';
                                                                                }
                                                                                $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.muyendesacuerdo').'",
                                                        "numero": ';
                                                                                if(isset($total['muyendesacuerdo'])){
                                                                                    $scripts .= $total['muyendesacuerdo'];
                                                                                }else{
                                                                                    $scripts .= '0';
                                                                                }
                                                                                $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "No entregado",
                                                        "numero": '.$totales['totalt_estados'][0].'
                                                    }
                                                    ],
                                                    "valueField": "numero",
                                                            "titleField": "respuesta",
                                                            "export": {
                                                        "enabled": true
                                                    }
                                            });
                                            ';
                                        ?>
                                    @endif

                                    {{-- 2 preguntas nuevas --}}
                                    @if($k == 'r07' || $k == 'r08')

                                        <?php
                                            $scripts .= '
                                            var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                "type": "pie",
                                                "startDuration": 0,
                                                "theme": "light",
                                                "addClassNames": true,
                                                "ocultar": false,
                                                "legend":{
                                                    "position":"right",
                                                    "marginRight":100,
                                                    "autoMargins":false
                                                },
                                                "innerRadius": "30%",
                                                "defs": {
                                                    "filter": [{
                                                        "id": "shadow",
                                                        "width": "200%",
                                                        "height": "200%",
                                                        "feOffset": {
                                                            "result": "offOut",
                                                            "in": "SourceAlpha",
                                                            "dx": 0,
                                                            "dy": 0
                                                        },
                                                        "feGaussianBlur": {
                                                            "result": "blurOut",
                                                            "in": "offOut",
                                                            "stdDeviation": 5
                                                        },
                                                        "feBlend": {
                                                            "in": "SourceGraphic",
                                                            "in2": "blurOut",
                                                            "mode": "normal"
                                                        }
                                                    }]
                                                },
                                                "dataProvider": [
                                                    {
                                                        "respuesta": "'.trans('forms.mal').'",
                                                        "numero": ';
                                                                  if(isset($total['mal'])){
                                                                    $scripts .= $total['mal'];
                                                                  }else{
                                                                      $scripts .= '0';
                                                                  }
                                                    $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.regular').'",
                                                        "numero": ';
                                                                   if(isset($total['regular'])){
                                                                         $scripts .= $total['regular'];
                                                                   }else{
                                                                         $scripts .= '0';
                                                                   }
                                                    $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.bien').'",
                                                        "numero": ';
                                                                                if(isset($total['bien'])){
                                                                                    $scripts .= $total['bien'];
                                                                                }else{
                                                                                    $scripts .= '0';
                                                                                }
                                                                                $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.muybien').'",
                                                        "numero": ';
                                                                                if(isset($total['muybien'])){
                                                                                    $scripts .= $total['muybien'];
                                                                                }else{
                                                                                    $scripts .= '0';
                                                                                }
                                                                                $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.excelente').'",
                                                        "numero": ';
                                                                                if(isset($total['excelente'])){
                                                                                    $scripts .= $total['excelente'];
                                                                                }else{
                                                                                    $scripts .= '0';
                                                                                }
                                                                                $scripts .= '
                                                    },
                                                    ],
                                                        "valueField": "numero",
                                                        "titleField": "respuesta",
                                                        "export": {
                                                        "enabled": true
                                                    }
                                            });
                                            ';
                                        ?>

                                    @endif

                                    @if($k != 'r01' && $k != 'r02' && $k != 'r03' && $k != 'r04' && $k != 'r05' && $k != 'r06' && $k != 'r07' && $k != 'r08')
                                        <?php
                                        $scripts .= '
                                        var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                            "type": "pie",
                                            "startDuration": 0,
                                            "theme": "light",
                                            "addClassNames": true,
                                            "legend":{
                                                "position":"right",
                                                "marginRight":100,
                                                "autoMargins":false
                                            },
                                            "innerRadius": "30%",
                                            "defs": {
                                                "filter": [{
                                                    "id": "shadow",
                                                    "width": "200%",
                                                    "height": "200%",
                                                    "feOffset": {
                                                        "result": "offOut",
                                                        "in": "SourceAlpha",
                                                        "dx": 0,
                                                        "dy": 0
                                                    },
                                                    "feGaussianBlur": {
                                                        "result": "blurOut",
                                                        "in": "offOut",
                                                        "stdDeviation": 5
                                                    },
                                                    "feBlend": {
                                                        "in": "SourceGraphic",
                                                        "in2": "blurOut",
                                                        "mode": "normal"
                                                    }
                                                }]
                                            },
                                            "dataProvider": [';

                                        for($i=1; $i <11; $i++){
                                            $scripts .= '
                                                    {
                                                    "respuesta": "'.$i.'",
                                                    "numero": ';
                                            if(isset($total[$i])){
                                                $scripts .= $total[$i].'
                                                },';
                                            }else{
                                                $scripts .= '0
                                                },';
                                            }
                                        }


                                        $scripts .= '

                                                {
                                                    "respuesta": "No entregado",
                                                    "numero": '.$totales['totalt_estados'][0].'
                                                }

                                                ],
                                                "valueField": "numero",
                                                        "titleField": "respuesta",
                                                        "export": {
                                                    "enabled": true
                                                }
                                        });
                                        ';
                                        ?>

                                    @endif

                                @endforeach


                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p01')}}</b></h4>
                                <div id="chartdiv-r01" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p02')}}</b></h4>
                                <div id="chartdiv-r02" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p03')}}</b></h4>
                                <div id="chartdiv-r03" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p04')}}</b></h4>
                                <div id="chartdiv-r04" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p05')}}</b></h4>
                                <div id="chartdiv-r05" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p07')}}</b></h4>
                                <div id="chartdiv-r07" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p08')}}</b></h4>
                                <div id="chartdiv-r08" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p06')}}</b></h4>
                                @foreach($totalesRespuestas['r06'] as $r06)
                                    <p>{!! nl2br($r06) !!}</p>
                                    <hr>
                                @endforeach

                                </div>
                            </div>



                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-globe fa-fw"></i> Resultados por viajeros
                            </div>
                            <div class="panel-body">

                                {!! Datatable::table()
                                    ->addColumn([
                                        'viajero'           => 'Viajero',
                                        'curso'             => 'Curso',
                                        'tutor1_resultado'    => 'Tutor 1',
                                        'tutor2_resultado'    => 'Tutor 2'
                                    ])
                                    ->setUrl( route('manage.system.cuestionarios.informes', $valores) )
                                    ->setOptions('iDisplayLength', 100)
                                    ->setOptions(
                                      "columnDefs", array(
                                        [ "bSortable" => false, "aTargets" => [3] ]
                                        // [ "targets" => [5,6], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                      )
                                    )
                                    ->render() !!}

                            </div>
                        </div>

                    @endif


                    @if($cuestionario->form == 'padrescoloniascic' || $cuestionario->form == 'padresmaxcamps')
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-globe fa-fw"></i> Informe de Resultados
                            </div>
                            <div class="panel-body">


                                @foreach($totalesRespuestas as $k => $v)

                                    <?php
                                    $v = array_replace($v,array_fill_keys(array_keys($v, null),''));
                                    $total = array_count_values($v);
                                    ?>

                                    @if($k == 'r01' || $k == 'r02' || $k == 'r03' || $k == 'r04' || $k == 'r05' || $k == 'r06')
                                        <?php
                                            $scripts .= '
                                            var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                "type": "pie",
                                                "startDuration": 0,
                                                "theme": "light",
                                                "addClassNames": true,
                                                "legend":{
                                                    "position":"right",
                                                    "marginRight":100,
                                                    "autoMargins":false
                                                },
                                                "innerRadius": "30%",
                                                "defs": {
                                                    "filter": [{
                                                        "id": "shadow",
                                                        "width": "200%",
                                                        "height": "200%",
                                                        "feOffset": {
                                                            "result": "offOut",
                                                            "in": "SourceAlpha",
                                                            "dx": 0,
                                                            "dy": 0
                                                        },
                                                        "feGaussianBlur": {
                                                            "result": "blurOut",
                                                            "in": "offOut",
                                                            "stdDeviation": 5
                                                        },
                                                        "feBlend": {
                                                            "in": "SourceGraphic",
                                                            "in2": "blurOut",
                                                            "mode": "normal"
                                                        }
                                                    }]
                                                },
                                                "dataProvider": [
                                                    {
                                                        "respuesta": "'.trans('forms.muydeacuerdo').'",
                                                        "numero": ';
                                                                  if(isset($total['muydeacuerdo'])){
                                                                    $scripts .= $total['muydeacuerdo'];
                                                                  }else{
                                                                      $scripts .= '0';
                                                                  }
                                                    $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.deacuerdo').'",
                                                        "numero": ';
                                                                   if(isset($total['deacuerdo'])){
                                                                         $scripts .= $total['deacuerdo'];
                                                                   }else{
                                                                         $scripts .= '0';
                                                                   }
                                                    $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.endesacuerdo').'",
                                                        "numero": ';
                                                                                if(isset($total['endesacuerdo'])){
                                                                                    $scripts .= $total['endesacuerdo'];
                                                                                }else{
                                                                                    $scripts .= '0';
                                                                                }
                                                                                $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.muyendesacuerdo').'",
                                                        "numero": ';
                                                                                if(isset($total['muyendesacuerdo'])){
                                                                                    $scripts .= $total['muyendesacuerdo'];
                                                                                }else{
                                                                                    $scripts .= '0';
                                                                                }
                                                                                $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "No entregado",
                                                        "numero": '.$totales['totalt_estados'][0].'
                                                    }
                                                    ],
                                                    "valueField": "numero",
                                                            "titleField": "respuesta",
                                                            "export": {
                                                        "enabled": true
                                                    }
                                            });
                                            ';
                                        ?>
                                    @endif

                                    {{-- 2 preguntas nuevas --}}
                                    @if($k == 'r08')

                                        <?php
                                            $scripts .= '
                                            var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                "type": "pie",
                                                "startDuration": 0,
                                                "theme": "light",
                                                "addClassNames": true,
                                                "ocultar": false,
                                                "legend":{
                                                    "position":"right",
                                                    "marginRight":100,
                                                    "autoMargins":false
                                                },
                                                "innerRadius": "30%",
                                                "defs": {
                                                    "filter": [{
                                                        "id": "shadow",
                                                        "width": "200%",
                                                        "height": "200%",
                                                        "feOffset": {
                                                            "result": "offOut",
                                                            "in": "SourceAlpha",
                                                            "dx": 0,
                                                            "dy": 0
                                                        },
                                                        "feGaussianBlur": {
                                                            "result": "blurOut",
                                                            "in": "offOut",
                                                            "stdDeviation": 5
                                                        },
                                                        "feBlend": {
                                                            "in": "SourceGraphic",
                                                            "in2": "blurOut",
                                                            "mode": "normal"
                                                        }
                                                    }]
                                                },
                                                "dataProvider": [
                                                    {
                                                        "respuesta": "'.trans('forms.indispensable').'",
                                                        "numero": ';
                                                                  if(isset($total['indispensable'])){
                                                                    $scripts .= $total['indispensable'];
                                                                  }else{
                                                                      $scripts .= '0';
                                                                  }
                                                    $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.muyutil').'",
                                                        "numero": ';
                                                                   if(isset($total['muyutil'])){
                                                                         $scripts .= $total['muyutil'];
                                                                   }else{
                                                                         $scripts .= '0';
                                                                   }
                                                    $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.indiferente').'",
                                                        "numero": ';
                                                                                if(isset($total['indiferente'])){
                                                                                    $scripts .= $total['indiferente'];
                                                                                }else{
                                                                                    $scripts .= '0';
                                                                                }
                                                                                $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.innecesario').'",
                                                        "numero": ';
                                                                                if(isset($total['innecesario'])){
                                                                                    $scripts .= $total['innecesario'];
                                                                                }else{
                                                                                    $scripts .= '0';
                                                                                }
                                                                                $scripts .= '
                                                    },
                                                    ],
                                                    "valueField": "numero",
                                                            "titleField": "respuesta",
                                                            "export": {
                                                        "enabled": true
                                                    }
                                            });
                                            ';
                                        ?>

                                    @endif

                                    @if($k == 'r09')

                                        <?php
                                            $scripts .= '
                                            var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                "type": "pie",
                                                "startDuration": 0,
                                                "theme": "light",
                                                "addClassNames": true,
                                                "ocultar": false,
                                                "legend":{
                                                    "position":"right",
                                                    "marginRight":100,
                                                    "autoMargins":false
                                                },
                                                "innerRadius": "30%",
                                                "defs": {
                                                    "filter": [{
                                                        "id": "shadow",
                                                        "width": "200%",
                                                        "height": "200%",
                                                        "feOffset": {
                                                            "result": "offOut",
                                                            "in": "SourceAlpha",
                                                            "dx": 0,
                                                            "dy": 0
                                                        },
                                                        "feGaussianBlur": {
                                                            "result": "blurOut",
                                                            "in": "offOut",
                                                            "stdDeviation": 5
                                                        },
                                                        "feBlend": {
                                                            "in": "SourceGraphic",
                                                            "in2": "blurOut",
                                                            "mode": "normal"
                                                        }
                                                    }]
                                                },
                                                "dataProvider": [
                                                    {
                                                        "respuesta": "'.trans('forms.sabado').'",
                                                        "numero": ';
                                                                  if(isset($total['sabado'])){
                                                                    $scripts .= $total['sabado'];
                                                                  }else{
                                                                      $scripts .= '0';
                                                                  }
                                                    $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.domingo').'",
                                                        "numero": ';
                                                                   if(isset($total['domingo'])){
                                                                         $scripts .= $total['domingo'];
                                                                   }else{
                                                                         $scripts .= '0';
                                                                   }
                                                    $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.lunes').'",
                                                        "numero": ';
                                                                                if(isset($total['lunes'])){
                                                                                    $scripts .= $total['lunes'];
                                                                                }else{
                                                                                    $scripts .= '0';
                                                                                }
                                                                                $scripts .= '
                                                    },
                                                    {
                                                        "respuesta": "'.trans('forms.indiferente').'",
                                                        "numero": ';
                                                                                if(isset($total['indiferente'])){
                                                                                    $scripts .= $total['indiferente'];
                                                                                }else{
                                                                                    $scripts .= '0';
                                                                                }
                                                                                $scripts .= '
                                                    },
                                                    ],
                                                    "valueField": "numero",
                                                            "titleField": "respuesta",
                                                            "export": {
                                                        "enabled": true
                                                    }
                                            });
                                            ';
                                        ?>

                                    @endif

                                    @if($k != 'r01' && $k != 'r02' && $k != 'r03' && $k != 'r04' && $k != 'r05' && $k != 'r06' && $k != 'r07' && $k != 'r08' && $k != 'r09')
                                        <?php
                                        $scripts .= '
                                        var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                            "type": "pie",
                                            "startDuration": 0,
                                            "theme": "light",
                                            "addClassNames": true,
                                            "legend":{
                                                "position":"right",
                                                "marginRight":100,
                                                "autoMargins":false
                                            },
                                            "innerRadius": "30%",
                                            "defs": {
                                                "filter": [{
                                                    "id": "shadow",
                                                    "width": "200%",
                                                    "height": "200%",
                                                    "feOffset": {
                                                        "result": "offOut",
                                                        "in": "SourceAlpha",
                                                        "dx": 0,
                                                        "dy": 0
                                                    },
                                                    "feGaussianBlur": {
                                                        "result": "blurOut",
                                                        "in": "offOut",
                                                        "stdDeviation": 5
                                                    },
                                                    "feBlend": {
                                                        "in": "SourceGraphic",
                                                        "in2": "blurOut",
                                                        "mode": "normal"
                                                    }
                                                }]
                                            },
                                            "dataProvider": [';

                                        for($i=1; $i <11; $i++){
                                            $scripts .= '
                                                    {
                                                    "respuesta": "'.$i.'",
                                                    "numero": ';
                                            if(isset($total[$i])){
                                                $scripts .= $total[$i].'
                                                },';
                                            }else{
                                                $scripts .= '0
                                                },';
                                            }
                                        }


                                        $scripts .= '

                                                {
                                                    "respuesta": "No entregado",
                                                    "numero": '.$totales['totalt_estados'][0].'
                                                }

                                                ],
                                                "valueField": "numero",
                                                        "titleField": "respuesta",
                                                        "export": {
                                                    "enabled": true
                                                }
                                        });
                                        ';
                                        ?>

                                    @endif

                                @endforeach


                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p01')}}</b></h4>
                                <div id="chartdiv-r01" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p02')}}</b></h4>
                                <div id="chartdiv-r02" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p03')}}</b></h4>
                                <div id="chartdiv-r03" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p04')}}</b></h4>
                                <div id="chartdiv-r04" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p05')}}</b></h4>
                                <div id="chartdiv-r05" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p06')}}</b></h4>
                                <div id="chartdiv-r06" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p08')}}</b></h4>
                                <div id="chartdiv-r08" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p09')}}</b></h4>
                                <div id="chartdiv-r09" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.web')}}</b></h4>
                                <div id="chartdiv-web" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.catalogo')}}</b></h4>
                                <div id="chartdiv-catalogo" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.blog')}}</b></h4>
                                <div id="chartdiv-blog" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.autocares')}}</b></h4>
                                <div id="chartdiv-autocares" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.lavado')}}</b></h4>
                                <div id="chartdiv-lavado" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.trinity')}}</b></h4>
                                <div id="chartdiv-trinity" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.trato')}}</b></h4>
                                <div id="chartdiv-trato" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.informe')}}</b></h4>
                                <div id="chartdiv-informe" class="chartdiv"></div>
                                <hr>

                                <h4><b>{{trans('forms.'.$cuestionario->form.'.p07')}}</b></h4>
                                @foreach($totalesRespuestas['r07'] as $r07)
                                    <p>{!! nl2br($r07) !!}</p>
                                    <hr>
                                @endforeach

                                </div>
                            </div>



                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-globe fa-fw"></i> Resultados por viajeros
                            </div>
                            <div class="panel-body">

                                {!! Datatable::table()
                                    ->addColumn([
                                        'viajero'           => 'Viajero',
                                        'curso'             => 'Curso',
                                        'tutor1_resultado'    => 'Tutor 1',
                                        'tutor2_resultado'    => 'Tutor 2'
                                    ])
                                    ->setUrl( route('manage.system.cuestionarios.informes', $valores) )
                                    ->setOptions('iDisplayLength', 100)
                                    ->setOptions(
                                      "columnDefs", array(
                                        [ "bSortable" => false, "aTargets" => [3] ]
                                        // [ "targets" => [5,6], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                      )
                                    )
                                    ->render() !!}

                            </div>
                        </div>

                    @endif

                    @if($cuestionario->form == 'testnivelmaxcamps')

                            <?php
                            $scripts .= '
                                var chartMax = AmCharts.makeChart("chartdiv-max", {
                                    "type": "serial",
                                    "theme": "none",  "marginRight": 70,
                                    "dataProvider": [';

                            foreach($totales['cursos'] as $ck => $c){
                                if($ck != 0){
                                    $scripts .= '{
                                        "curso": "'.$c.'",
                                        "nivel": 3025,
                                        "color": "#FF0F00"
                                    },';
                                }
                            }

                            $scripts .= '],
                                    "valueAxes": [{
                                        "axisAlpha": 0,
                                        "position": "left",
                                        "title": "Nivel"
                                    }],
                                    "startDuration": 1,
                                    "graphs": [{
                                        "balloonText": "<b>[[category]]: [[value]]</b>",
                                        "fillColorsField": "color",
                                        "fillAlphas": 0.9,
                                        "lineAlpha": 0.2,
                                        "type": "column",
                                        "valueField": "nivel"
                                    }],
                                    "chartCursor": {
                                        "categoryBalloonEnabled": false,
                                        "cursorAlpha": 0,
                                        "zoomable": false
                                    },
                                    "categoryField": "curso",
                                    "categoryAxis": {
                                        "gridPosition": "start",
                                        "labelRotation": 50
                                    },
                                    "export": {
                                        "enabled": true
                                    }

                                });';

                                ?>

                                <div id="chartdiv-max" class="chartdiv"></div>
                                <hr>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-globe fa-fw"></i> Resultados Test de Nivel Max Camps
                                </div>
                                <div class="panel-body">

                                    {!! Datatable::table()
                                        ->addColumn([
                                            'viajero'       => 'Viajero',
                                            'curso'         => 'Curso',
                                            'resultado'     => 'Resultado',
                                            'nivel'         => 'Nivel'
                                        ])
                                        ->setUrl( route('manage.system.cuestionarios.informes', $valores) )
                                        ->setOptions('iDisplayLength', 100)
                                        ->setOptions(
                                          "columnDefs", array(
                                            [ "bSortable" => false, "aTargets" => [3] ]
                                            // [ "targets" => [5,6], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                          )
                                        )
                                        ->render() !!}

                                </div>
                            </div>

                    @endif



                        @if($cuestionario->form == 'testnivelcoloniascic')


                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-globe fa-fw"></i> Resultados {{$cuestionario->name}}
                                </div>
                                <div class="panel-body testnivelcoloniascic">

                                    {!! Datatable::table()
                                        ->addColumn([
                                            'viajerolastname'       => 'Apellidos',
                                            'viajeroname'       => 'Nombre',
                                            'sexo'          => 'Sexe',
                                            'fechanac'      => 'DOB',
                                            'curso'         => 'Curso',
                                            'convocatoria'       => 'Convocatoria',
                                            
                                            'escuela_curso' => 'Academic Year',
                                            'escuela'       => 'Escola',
                                            'nivel_cic'     => 'Nivell CIC',
                                            'editar'        => 'Asignar nivel',
                                            'nivel_trinity' => 'Trinity Assolit',
                                            'trinity_any'   => 'Any Trinity',

                                            'testname'      => 'Test Name',
                                            'choicelevel'   => 'MC Level',
                                            'ptxt'          => 'WT',
                                            'textlevel'     => 'WT Level',
                                            'oralresult'    => 'Oral Result',
                                            'nivel'         => 'Nivell CIC camp',
                                            'trinity'       => 'Trinity Camp',
                                        ])
                                        ->setUrl( route('manage.system.cuestionarios.informes', $valores) )
                                        ->setOptions('iDisplayLength', 100)
                                        //->setOptions('responsive', false)
                                        ->setOptions(
                                          "columnDefs", array(
                                            //['bSortable' => false, 'sClass' => 'details-control', "aTargets" => [0] ],
                                            [ "bSortable" => false, "aTargets" => [16] ],
                                            // [ "targets" => [5,6], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],

                                          )
                                        )
                                       // ->setOptions("order", [ [1, 'asc'] ])
                                        ->setId('testcoloniascictable')
                                        ->render() !!}

                                </div>
                            </div>

                        @endif



                        @if($cuestionario->form == 'opinionjovenes')
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-globe fa-fw"></i> Informe de Resultados
                                </div>
                                <div class="panel-body">


                                    @foreach($totalesRespuestas as $k => $v)
                                        @if(strpos($k, 'r') !== false)

                                            <?php
                                            $v = array_replace($v,array_fill_keys(array_keys($v, null),''));
                                            $total = array_count_values($v);
                                            ?>

                                            <?php
                                            $scripts .= '
                                                var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                    "type": "pie",
                                                    "startDuration": 0,
                                                    "theme": "light",
                                                    "addClassNames": true,
                                                    "legend":{
                                                        "position":"right",
                                                        "marginRight":100,
                                                        "autoMargins":false
                                                    },
                                                    "innerRadius": "30%",
                                                    "defs": {
                                                        "filter": [{
                                                            "id": "shadow",
                                                            "width": "200%",
                                                            "height": "200%",
                                                            "feOffset": {
                                                                "result": "offOut",
                                                                "in": "SourceAlpha",
                                                                "dx": 0,
                                                                "dy": 0
                                                            },
                                                            "feGaussianBlur": {
                                                                "result": "blurOut",
                                                                "in": "offOut",
                                                                "stdDeviation": 5
                                                            },
                                                            "feBlend": {
                                                                "in": "SourceGraphic",
                                                                "in2": "blurOut",
                                                                "mode": "normal"
                                                            }
                                                        }]
                                                    },
                                                    "dataProvider": [
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.1').'",
                                                            "numero": ';
                                            if(isset($total['1'])){
                                                $scripts .= $total['1'];
                                            }else{
                                                $scripts .= '0';
                                            }
                                            $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.2').'",
                                                            "numero": ';
                                            if(isset($total['2'])){
                                                $scripts .= $total['2'];
                                            }else{
                                                $scripts .= '0';
                                            }
                                            $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.3').'",
                                                            "numero": ';
                                            if(isset($total['3'])){
                                                $scripts .= $total['3'];
                                            }else{
                                                $scripts .= '0';
                                            }
                                            $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.4').'",
                                                            "numero": ';
                                            if(isset($total['4'])){
                                                $scripts .= $total['4'];
                                            }else{
                                                $scripts .= '0';
                                            }
                                            $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.5').'",
                                                            "numero": ';
                                                if(isset($total['5'])){
                                                    $scripts .= $total['5'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "No entregado",
                                                            "numero": '.$totales['totalt_estados'][0].'
                                                        }
                                                        ],
                                                        "valueField": "numero",
                                                                "titleField": "respuesta",
                                                                "export": {
                                                            "enabled": true
                                                        }
                                                });
                                                ';
                                            ?>
                                        @endif
                                    @endforeach


                                        @foreach($totalesRespuestas as $k => $v)

                                            @if((strpos($k, 'f') !== false))
                                                <?php
                                                $v = array_replace($v,array_fill_keys(array_keys($v, null),''));
                                                $total = array_count_values($v);
                                                ?>
                                                <?php
                                                $scripts .= '
                                                var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                    "type": "pie",
                                                    "startDuration": 0,
                                                    "theme": "light",
                                                    "addClassNames": true,
                                                    "legend":{
                                                        "position":"right",
                                                        "marginRight":100,
                                                        "autoMargins":false
                                                    },
                                                    "innerRadius": "30%",
                                                    "defs": {
                                                        "filter": [{
                                                            "id": "shadow",
                                                            "width": "200%",
                                                            "height": "200%",
                                                            "feOffset": {
                                                                "result": "offOut",
                                                                "in": "SourceAlpha",
                                                                "dx": 0,
                                                                "dy": 0
                                                            },
                                                            "feGaussianBlur": {
                                                                "result": "blurOut",
                                                                "in": "offOut",
                                                                "stdDeviation": 5
                                                            },
                                                            "feBlend": {
                                                                "in": "SourceGraphic",
                                                                "in2": "blurOut",
                                                                "mode": "normal"
                                                            }
                                                        }]
                                                    },
                                                    "dataProvider": [
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.1').'",
                                                            "numero": ';
                                                if(isset($total['1'])){
                                                    $scripts .= $total['1'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.2').'",
                                                            "numero": ';
                                                if(isset($total['2'])){
                                                    $scripts .= $total['2'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.3').'",
                                                            "numero": ';
                                                if(isset($total['3'])){
                                                    $scripts .= $total['3'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.4').'",
                                                            "numero": ';
                                                if(isset($total['4'])){
                                                    $scripts .= $total['4'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.5').'",
                                                            "numero": ';
                                                if(isset($total['5'])){
                                                    $scripts .= $total['5'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "No entregado",
                                                            "numero": '.$totales['totalt_estados'][0].'
                                                        }
                                                        ],
                                                        "valueField": "numero",
                                                                "titleField": "respuesta",
                                                                "export": {
                                                            "enabled": true
                                                        }
                                                });
                                                ';
                                                ?>
                                            @endif



                                    @endforeach


                                        @foreach($totalesRespuestas as $k => $v)

                                            @if((strpos($k, 'p') !== false))

                                                <?php
                                                    $v = array_replace($v,array_fill_keys(array_keys($v, null),''));
                                                    $total = array_count_values($v);
                                                ?>

                                                <?php
                                                $scripts .= '
                                                var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                    "type": "pie",
                                                    "startDuration": 0,
                                                    "theme": "light",
                                                    "addClassNames": true,
                                                    "legend":{
                                                        "position":"right",
                                                        "marginRight":100,
                                                        "autoMargins":false
                                                    },
                                                    "innerRadius": "30%",
                                                    "defs": {
                                                        "filter": [{
                                                            "id": "shadow",
                                                            "width": "200%",
                                                            "height": "200%",
                                                            "feOffset": {
                                                                "result": "offOut",
                                                                "in": "SourceAlpha",
                                                                "dx": 0,
                                                                "dy": 0
                                                            },
                                                            "feGaussianBlur": {
                                                                "result": "blurOut",
                                                                "in": "offOut",
                                                                "stdDeviation": 5
                                                            },
                                                            "feBlend": {
                                                                "in": "SourceGraphic",
                                                                "in2": "blurOut",
                                                                "mode": "normal"
                                                            }
                                                        }]
                                                    },
                                                    "dataProvider": [
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.1').'",
                                                            "numero": ';
                                                if(isset($total['1'])){
                                                    $scripts .= $total['1'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.2').'",
                                                            "numero": ';
                                                if(isset($total['2'])){
                                                    $scripts .= $total['2'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.3').'",
                                                            "numero": ';
                                                if(isset($total['3'])){
                                                    $scripts .= $total['3'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.4').'",
                                                            "numero": ';
                                                if(isset($total['4'])){
                                                    $scripts .= $total['4'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.5').'",
                                                            "numero": ';
                                                if(isset($total['5'])){
                                                    $scripts .= $total['5'];
                                                }else{
                                                    $scripts .= '0';
                                                }

                                                if($k == 'p16'){
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "'.trans('forms.opinionjovenes.0').'",
                                                            "numero": ';
                                                    if(isset($total['0'])){
                                                        $scripts .= $total['0'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }

                                                }

                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "No entregado",
                                                            "numero": '.$totales['totalt_estados'][0].'
                                                        }
                                                        ],
                                                        "valueField": "numero",
                                                                "titleField": "respuesta",
                                                                "export": {
                                                            "enabled": true
                                                        }
                                                });
                                                ';
                                                ?>
                                            @endif



                                        @endforeach



                                        @foreach($totalesRespuestas as $k => $v)

                                            @if($k == 'p21')
                                                <?php
                                                $v = array_replace($v,array_fill_keys(array_keys($v, null),''));
                                                $total = array_count_values($v);
                                                ?>
                                                <?php
                                                $scripts .= '
                                                var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                    "type": "pie",
                                                    "startDuration": 0,
                                                    "theme": "light",
                                                    "addClassNames": true,
                                                    "legend":{
                                                        "position":"right",
                                                        "marginRight":100,
                                                        "autoMargins":false
                                                    },
                                                    "innerRadius": "30%",
                                                    "defs": {
                                                        "filter": [{
                                                            "id": "shadow",
                                                            "width": "200%",
                                                            "height": "200%",
                                                            "feOffset": {
                                                                "result": "offOut",
                                                                "in": "SourceAlpha",
                                                                "dx": 0,
                                                                "dy": 0
                                                            },
                                                            "feGaussianBlur": {
                                                                "result": "blurOut",
                                                                "in": "offOut",
                                                                "stdDeviation": 5
                                                            },
                                                            "feBlend": {
                                                                "in": "SourceGraphic",
                                                                "in2": "blurOut",
                                                                "mode": "normal"
                                                            }
                                                        }]
                                                    },
                                                    "dataProvider": [
                                                        {
                                                            "respuesta": "Lo han decidido mis padres",
                                                            "numero": ';
                                                if(isset($total['decididospadres'])){
                                                    $scripts .= $total['decididospadres'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "Lo he dicidido yo",
                                                            "numero": ';
                                                if(isset($total['decididoyo'])){
                                                    $scripts .= $total['decididoyo'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "No entregado",
                                                            "numero": '.$totales['totalt_estados'][0].'
                                                        }
                                                        ],
                                                        "valueField": "numero",
                                                                "titleField": "respuesta",
                                                                "export": {
                                                            "enabled": true
                                                        }
                                                });
                                                ';
                                                ?>
                                            @endif

                                            @if($k == 'f10')
                                                    <?php
                                                    $v = array_replace($v,array_fill_keys(array_keys($v, null),''));
                                                    $total = array_count_values($v);
                                                    ?>
                                                <?php
                                                $scripts .= '
                                                var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                    "type": "pie",
                                                    "startDuration": 0,
                                                    "theme": "light",
                                                    "addClassNames": true,
                                                    "legend":{
                                                        "position":"right",
                                                        "marginRight":100,
                                                        "autoMargins":false
                                                    },
                                                    "innerRadius": "30%",
                                                    "defs": {
                                                        "filter": [{
                                                            "id": "shadow",
                                                            "width": "200%",
                                                            "height": "200%",
                                                            "feOffset": {
                                                                "result": "offOut",
                                                                "in": "SourceAlpha",
                                                                "dx": 0,
                                                                "dy": 0
                                                            },
                                                            "feGaussianBlur": {
                                                                "result": "blurOut",
                                                                "in": "offOut",
                                                                "stdDeviation": 5
                                                            },
                                                            "feBlend": {
                                                                "in": "SourceGraphic",
                                                                "in2": "blurOut",
                                                                "mode": "normal"
                                                            }
                                                        }]
                                                    },
                                                    "dataProvider": [
                                                        {
                                                            "respuesta": "Sí",
                                                            "numero": ';
                                                if(isset($total['1'])){
                                                    $scripts .= $total['1'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "No",
                                                            "numero": ';
                                                if(isset($total['0'])){
                                                    $scripts .= $total['0'];
                                                }else{
                                                    $scripts .= '0';
                                                }
                                                $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "No entregado",
                                                            "numero": '.$totales['totalt_estados'][0].'
                                                        }
                                                        ],
                                                        "valueField": "numero",
                                                                "titleField": "respuesta",
                                                                "export": {
                                                            "enabled": true
                                                        }
                                                });
                                                ';
                                                ?>
                                            @endif


                                                @if($k == 'f11')
                                                    <?php
                                                    $v = array_replace($v,array_fill_keys(array_keys($v, null),''));
                                                    $total = array_count_values($v);
                                                    ?>
                                                    <?php
                                                    $scripts .= '
                                                var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                    "type": "pie",
                                                    "startDuration": 0,
                                                    "theme": "light",
                                                    "addClassNames": true,
                                                    "legend":{
                                                        "position":"right",
                                                        "marginRight":100,
                                                        "autoMargins":false
                                                    },
                                                    "innerRadius": "30%",
                                                    "defs": {
                                                        "filter": [{
                                                            "id": "shadow",
                                                            "width": "200%",
                                                            "height": "200%",
                                                            "feOffset": {
                                                                "result": "offOut",
                                                                "in": "SourceAlpha",
                                                                "dx": 0,
                                                                "dy": 0
                                                            },
                                                            "feGaussianBlur": {
                                                                "result": "blurOut",
                                                                "in": "offOut",
                                                                "stdDeviation": 5
                                                            },
                                                            "feBlend": {
                                                                "in": "SourceGraphic",
                                                                "in2": "blurOut",
                                                                "mode": "normal"
                                                            }
                                                        }]
                                                    },
                                                    "dataProvider": [
                                                        {
                                                            "respuesta": "Caminando",
                                                            "numero": ';
                                                    if(isset($total['caminando'])){
                                                        $scripts .= $total['caminando'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "Bus",
                                                            "numero": ';
                                                    if(isset($total['bus'])){
                                                        $scripts .= $total['bus'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "Metro",
                                                            "numero": ';
                                                    if(isset($total['metro'])){
                                                        $scripts .= $total['metro'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "Tren",
                                                            "numero": ';
                                                    if(isset($total['tren'])){
                                                        $scripts .= $total['tren'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "Coche",
                                                            "numero": ';
                                                    if(isset($total['coche'])){
                                                        $scripts .= $total['coche'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "Otro",
                                                            "numero": ';
                                                    if(isset($total['otro'])){
                                                        $scripts .= $total['otro'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "No entregado",
                                                            "numero": '.$totales['totalt_estados'][0].'
                                                        }
                                                        ],
                                                        "valueField": "numero",
                                                                "titleField": "respuesta",
                                                                "export": {
                                                            "enabled": true
                                                        }
                                                });
                                                ';
                                                    ?>
                                                @endif

                                                @if($k == 'f12')
                                                    <?php
                                                    $v = array_replace($v,array_fill_keys(array_keys($v, null),''));
                                                    $total = array_count_values($v);
                                                    ?>
                                                    <?php
                                                    $scripts .= '
                                                var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                    "type": "pie",
                                                    "startDuration": 0,
                                                    "theme": "light",
                                                    "addClassNames": true,
                                                    "legend":{
                                                        "position":"right",
                                                        "marginRight":100,
                                                        "autoMargins":false
                                                    },
                                                    "innerRadius": "30%",
                                                    "defs": {
                                                        "filter": [{
                                                            "id": "shadow",
                                                            "width": "200%",
                                                            "height": "200%",
                                                            "feOffset": {
                                                                "result": "offOut",
                                                                "in": "SourceAlpha",
                                                                "dx": 0,
                                                                "dy": 0
                                                            },
                                                            "feGaussianBlur": {
                                                                "result": "blurOut",
                                                                "in": "offOut",
                                                                "stdDeviation": 5
                                                            },
                                                            "feBlend": {
                                                                "in": "SourceGraphic",
                                                                "in2": "blurOut",
                                                                "mode": "normal"
                                                            }
                                                        }]
                                                    },
                                                    "dataProvider": [
                                                        {
                                                            "respuesta": "Menos de 10 minutos",
                                                            "numero": ';
                                                    if(isset($total['menos10min'])){
                                                        $scripts .= $total['menos10min'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "Entre 10 y 20 minutos",
                                                            "numero": ';
                                                    if(isset($total['entre10y20'])){
                                                        $scripts .= $total['entre10y20'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "Entre 20 y 40 minutos",
                                                            "numero": ';
                                                    if(isset($total['entre20y40'])){
                                                        $scripts .= $total['entre20y40'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "Entre 40 y 60 minutos",
                                                            "numero": ';
                                                    if(isset($total['entre40y60'])){
                                                        $scripts .= $total['entre40y60'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "Más de 1 hora",
                                                            "numero": ';
                                                    if(isset($total['mas1h'])){
                                                        $scripts .= $total['mas1h'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }

                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "No entregado",
                                                            "numero": '.$totales['totalt_estados'][0].'
                                                        }
                                                        ],
                                                        "valueField": "numero",
                                                                "titleField": "respuesta",
                                                                "export": {
                                                            "enabled": true
                                                        }
                                                });
                                                ';
                                                    ?>
                                                @endif

                                                @if($k == 'f13')
                                                    <?php
                                                    $v = array_replace($v,array_fill_keys(array_keys($v, null),''));
                                                    $total = array_count_values($v);
                                                    ?>
                                                    <?php
                                                    $scripts .= '
                                                var chart_'.$k.' = AmCharts.makeChart("chartdiv-'.$k.'", {
                                                    "type": "pie",
                                                    "startDuration": 0,
                                                    "theme": "light",
                                                    "addClassNames": true,
                                                    "legend":{
                                                        "position":"right",
                                                        "marginRight":100,
                                                        "autoMargins":false
                                                    },
                                                    "innerRadius": "30%",
                                                    "defs": {
                                                        "filter": [{
                                                            "id": "shadow",
                                                            "width": "200%",
                                                            "height": "200%",
                                                            "feOffset": {
                                                                "result": "offOut",
                                                                "in": "SourceAlpha",
                                                                "dx": 0,
                                                                "dy": 0
                                                            },
                                                            "feGaussianBlur": {
                                                                "result": "blurOut",
                                                                "in": "offOut",
                                                                "stdDeviation": 5
                                                            },
                                                            "feBlend": {
                                                                "in": "SourceGraphic",
                                                                "in2": "blurOut",
                                                                "mode": "normal"
                                                            }
                                                        }]
                                                    },
                                                    "dataProvider": [
                                                        {';
                                                    $scripts .= '
                                                            "respuesta": "0",
                                                            "numero": ';
                                                    if(isset($total['0'])){
                                                        $scripts .= $total['0'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                    },
                                                        {
                                                            "respuesta": "1",
                                                            "numero": ';
                                                    if(isset($total['1'])){
                                                        $scripts .= $total['1'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "2",
                                                            "numero": ';
                                                    if(isset($total['2'])){
                                                        $scripts .= $total['2'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "3",
                                                            "numero": ';
                                                    if(isset($total['3'])){
                                                        $scripts .= $total['3'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "4",
                                                            "numero": ';
                                                    if(isset($total['4'])){
                                                        $scripts .= $total['4'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }
                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "5",
                                                            "numero": ';
                                                    if(isset($total['5'])){
                                                        $scripts .= $total['5'];
                                                    }else{
                                                        $scripts .= '0';
                                                    }


                                                    $scripts .= '
                                                        },
                                                        {
                                                            "respuesta": "No entregado",
                                                            "numero": '.$totales['totalt_estados'][0].'
                                                        }
                                                        ],
                                                        "valueField": "numero",
                                                                "titleField": "respuesta",
                                                                "export": {
                                                            "enabled": true
                                                        }
                                                });
                                                ';
                                                    ?>
                                                @endif



                                        @endforeach




                                    <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <h2 class="text-primary">Alojamiento en residencia</h2>
                                        <h4><b>Habitación:</b></h4>
                                        <div id="chartdiv-r01" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Aseos/Duchas: </b></h4>
                                        <div id="chartdiv-r02" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Comedor: </b></h4>
                                        <div id="chartdiv-r03" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Zonas comunes: </b></h4>
                                        <div id="chartdiv-r04" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Lavandería: </b></h4>
                                        <div id="chartdiv-r05" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Instalaciones deportivas: </b></h4>
                                        <div id="chartdiv-r06" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Cantidad comida: </b></h4>
                                        <div id="chartdiv-r07" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Calidad comida: </b></h4>
                                        <div id="chartdiv-r08" class="chartdiv"></div>
                                        <hr>



                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <h2 class="text-primary">Alojamiento en familia</h2>
                                        <h4><b>Habitación:</b></h4>
                                        <div id="chartdiv-f01" class="chartdiv"></div>
                                        <hr>

                                        <h4><b>Aseos/Duchas:</b></h4>
                                        <div id="chartdiv-f02" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Comedor:</b></h4>
                                        <div id="chartdiv-f03" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Cocina:</b></h4>
                                        <div id="chartdiv-f04" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Cantidad comida:</b></h4>
                                        <div id="chartdiv-f05" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Calidad comida:</b></h4>
                                        <div id="chartdiv-f06" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>¿Te han lavado la ropa?:</b></h4>
                                        <div id="chartdiv-f07" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Comunicación:</b></h4>
                                        <div id="chartdiv-f08" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Hospitalidad:</b></h4>
                                        <div id="chartdiv-f09" class="chartdiv"></div>
                                        <hr>



                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <h2 class="text-primary">Escuela</h2>
                                        <h4><b>Aulas:</b></h4>
                                        <div id="chartdiv-p01" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Profesor/a:</b></h4>
                                        <div id="chartdiv-p02" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Material didáctico:</b></h4>
                                        <div id="chartdiv-p03" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Nivel asignado:</b></h4>
                                        <div id="chartdiv-p04" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>¿Has mejorado tu nivel?:</b></h4>
                                        <div id="chartdiv-p05" class="chartdiv"></div>
                                        <hr>

                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <h2 class="text-primary">Actividades</h2>
                                        <h4><b>Deportivas:</b></h4>
                                        <div id="chartdiv-p06" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Culturales:</b></h4>
                                        <div id="chartdiv-p07" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Ocio:</b></h4>
                                        <div id="chartdiv-p08" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Talleres:</b></h4>
                                        <div id="chartdiv-p09" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Excursiones:</b></h4>
                                        <div id="chartdiv-p10" class="chartdiv"></div>
                                        <hr>

                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <h2 class="text-primary">Personal</h2>
                                        <h4><b>Monitor español:</b></h4>
                                        <div id="chartdiv-p11" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Personal del colegio:</b></h4>
                                        <div id="chartdiv-p12" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Personal del lugar de inscripción en España:</b></h4>
                                        <div id="chartdiv-p13" class="chartdiv"></div>
                                        <hr>


                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <h2 class="text-primary">Aspectos Generales</h2>
                                        <h4><b>Valoración general del curso:</b></h4>
                                        <div id="chartdiv-p14" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Viaje al país de destino:</b></h4>
                                        <div id="chartdiv-p15" class="chartdiv"></div>
                                        <hr>
                                        <h4><b>Reunión informativa previa:</b></h4>
                                        <div id="chartdiv-p16" class="chartdiv"></div>
                                        <hr>


                                        <h2 class="text-primary">Familia</h2>
                                        <h4><b>¿Recomendarías a la familia?</b></h4>
                                        <div id="chartdiv-f10" class="chartdiv"></div>
                                        <hr>

                                        <p>&nbsp;</p>

                                        <h2>Transporte</h2>
                                        <p>&nbsp;</p>
                                        <h4><b>Transporte utilizado casa-escuela</b></h4>
                                        <div id="chartdiv-f11" class="chartdiv"></div>
                                        <hr>

                                        <h4><b>Tiempo en trasladarte casa-escuela en minutos</b></h4>
                                        <div id="chartdiv-f12" class="chartdiv"></div>
                                        <hr>

                                        <h4><b>¿Cuántos transportes utilizabas casa-escuela?</b></h4>
                                        <div id="chartdiv-f13" class="chartdiv"></div>
                                        <hr>



                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>

                                        <h2 class="text-primary">Otros</h2>
                                        <p>&nbsp;</p>

                                        <h4>¿Por qué te has apuntado a éste programa?</h4>
                                        <div id="chartdiv-p21" class="chartdiv"></div>
                                        {{-- @foreach($totalesRespuestas as $k => $v)
                                            @if($k == 'p21')
                                                @foreach($v as $valor)
                                                <p>{!! nl2br($valor) !!}</p>
                                                <hr>
                                                @endforeach
                                            @endif
                                        @endforeach --}}
                                        <p>&nbsp;</p>

                                        <h4>Resume en unas cuantas palabras como ha sido tu experiencia</h4>
                                        @foreach($totalesRespuestas as $k => $v)
                                            @if($k == 'p17')
                                                @foreach($v as $valor)
                                                <p>{!! nl2br($valor) !!}</p>
                                                <hr>
                                                @endforeach
                                            @endif
                                        @endforeach

                                        <p>&nbsp;</p>
                                        <h4>¿Qué crees que tenemos que mejorar en nuestro programa?</h4>
                                        @foreach($totalesRespuestas as $k => $v)
                                            @if($k == 'p18')
                                                @foreach($v as $valor)
                                                <p>{!! nl2br($valor) !!}</p>
                                                <hr>
                                                @endforeach
                                            @endif
                                        @endforeach
                                        <p>&nbsp;</p>

                                        <h4>Actividades y excursiones que añadirías al programa</h4>
                                        @foreach($totalesRespuestas as $k => $v)
                                            @if($k == 'p19')
                                                @foreach($v as $valor)
                                                <p>{!! nl2br($valor) !!}</p>
                                                <hr>
                                                @endforeach
                                            @endif
                                        @endforeach
                                        <p>&nbsp;</p>

                                        <h4>¿Qué nuevo programa te gustaría ver en nuestro catálogo?</h4>
                                        @foreach($totalesRespuestas as $k => $v)
                                            @if($k == 'p20')
                                                @foreach($v as $valor)
                                                <p>{!! nl2br($valor) !!}</p>
                                                <hr>
                                                @endforeach
                                            @endif
                                        @endforeach
                                        <p>&nbsp;</p>

                                        <h4>Comentarios</h4>
                                        @foreach($totalesRespuestas as $k => $v)
                                            @if($k == 'p22')
                                                @foreach($v as $valor)
                                                <p>{!! nl2br($valor) !!}</p>
                                                <hr>
                                                @endforeach
                                            @endif
                                        @endforeach
                                        <p>&nbsp;</p>


                                </div>
                            </div>



                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-globe fa-fw"></i> Resultados por viajeros
                                </div>
                                <div class="panel-body">

                                    {!! Datatable::table()
                                        ->addColumn([
                                            'viajero'           => 'Viajero',
                                            'curso'             => 'Curso',
                                            'convocatoria'      => 'Convocatoria',
                                            'alojamiento'       => 'Alojamiento',
                                            'r01'               => 'Residencia: Habitacion',
                                            'r02'               => 'Residencia: Aseos/Duchas',
                                            'r03'               => 'Residencia: Comedor',
                                            'r04'               => 'Residencia: Zonas comunes',
                                            'r05'               => 'Residencia: Lavandería',
                                            'r06'               => 'Residencia: Instalaciones deportivas',
                                            'r07'               => 'Residencia: Cantidad comida',
                                            'r08'               => 'Residencia: Calidad comida',
                                            'f01'               => 'Familia: Habitación',
                                            'f02'               => 'Familia: Aseos/Duchas',
                                            'f03'               => 'Familia: Comedor',
                                            'f04'               => 'Familia: Cocina',
                                            'f05'               => 'Familia: Cantidad comida',
                                            'f06'               => 'Familia: Calidad comida',
                                            'f07'               => 'Familia: ¿Te han lavado la ropa?',
                                            'f08'               => 'Familia: Comunicación',
                                            'f09'               => 'Familia: Hospitalidad',
                                            'p01'               => 'Escuela: Aulas',
                                            'p02'               => 'Escuela: Profesor/a',
                                            'p03'               => 'Escuela: Material didáctico',
                                            'p04'               => 'Escuela: Nivel asignado',
                                            'p05'               => 'Escuela: ¿Has mejorado tu nivel?',
                                            'p06'               => 'Actividades: Deportivas',
                                            'p07'               => 'Actividades: Culturales',
                                            'p08'               => 'Actividades: Ocio',
                                            'p09'               => 'Actividades: Talleres',
                                            'p10'               => 'Actividades: Excursiones',
                                            'p11'               => 'Personal: Monitor español',
                                            'p12'               => 'Personal: Personal del colegio',
                                            'p13'               => 'Personal: Personal del lugar de inscripción en España',
                                            'p14'               => 'Aspectos Generales: Valoración general del curso',
                                            'p15'               => 'Aspectos Generales: Viaje al país de destino',
                                            'p16'               => 'Aspectos Generales: Reunión informativa previa',
                                            'f10'               => 'Familia: ¿Recomendarías a la familia?',
                                            'f11'               => 'Familia: Transporte utilizado casa-escuela',
                                            'f12'               => 'Familia: Tiempo en trasladarte casa-escuela en minutos',
                                            'f13'               => 'Familia: ¿Cuántos transportes utilizabas casa-escuela?',
                                            'p17'               => 'Otros: Resume en unas cuantas palabras como ha sido tu experiencia',
                                            'p18'               => 'Otros: ¿Qué crees que tenemos que mejorar en nuestro programa?',
                                            'p19'               => 'Otros: Actividades y excursiones que añadirías al programa',
                                            'p20'               => 'Otros: ¿Qué nuevo programa te gustaría ver en nuestro catálogo?',
                                            'p21'               => 'Otros: ¿Por qué te has apuntado a éste programa?',
                                            'p22'               => 'Comentarios',

                                        ])
                                        ->setUrl( route('manage.system.cuestionarios.informes', $valores) )
                                        ->setOptions('iDisplayLength', 100)
                                        ->setOptions(
                                          "columnDefs", array(
                                            [ "bSortable" => false, "aTargets" => [2] ]
                                            // [ "targets" => [5,6], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                          )
                                        )
                                        ->render() !!}

                                </div>
                            </div>

                        @endif
                @endif

           @endif


        </div>

    </div>



    <!-- Modal -->
    <div class="modal fade" id="editor" tabindex="-1" role="dialog" aria-labelledby="editor" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div id="viajeroname"><h4></h4></div>
                </div>

                <div class="modal-body">

                    <div id="online">
                        <div class="test">
                            <p><b>Test Name: </b><span></span></p>
                        </div>
                        <div class="multiplechoice">
                            <p><b>MC Level: </b><span></span></p>
                        </div>
                        <div class="ptxt">
                            <p><b>WT: </b></p>
                            <div></div>
                        </div>
                        {!! Form::open(array('id'=>'testnivel')) !!}
                        <div class="row addmargintop30">
                            <div class="col-sm-12">
                                {!! Form::label('textlevel', 'WT Level') !!}
                                {!! Form::select('textlevel',ConfigHelper::getCICNivelTestColonias(),
                                '',
                                array(
                                        'id'=> 'textlevel',
                                        'name'=> 'textlevel',
                                        'class' => "selectpicker form-control textlevel",
                                        'title' => "...",
                                        'required'
                                    )
                                )
                                !!}
                            </div>
                        </div>
                        <div class="row addmargintop30">
                            <div class="col-sm-12">
                                {!! Form::label('nivel', 'Final CIC Level') !!}
                                {!! Form::select('nivel',ConfigHelper::getCICNivelTestColonias(),
                                '',
                                array(
                                        'id'=> 'nivel',
                                        'name'=> 'nivel',
                                        'class' => "selectpicker form-control nivel",
                                        'title' => "...",
                                        'required'
                                    )
                                )
                                !!}
                            </div>
                        </div>

                        <div class="row addmargintop30">
                            <div class="col-sm-12">
                                {!! Form::label('oralresult', 'Oral Result') !!}
                                {!! Form::text('oralresult', '', ['class'=> 'form-control']) !!}
                            </div>
                        </div>

                        <div class="row addmargintop30">
                            <div class="col-sm-12">
                                {!! Form::label('trinity', 'Trinity Grade') !!}
                                {!! Form::text('trinity', '', ['class'=> 'form-control']) !!}
                            </div>
                        </div>

                        {!! Form::hidden('booking_id', '', array('id' => 'booking_id', 'class' => 'booking')) !!}
                        {!! Form::close() !!}

                        <div class="msj"></div>
                    </div>

                    <div id="offline">
                        {!! Form::open(array('id'=>'testnivel')) !!}
                        <div class="row addmargintop30">
                            <div class="col-sm-12">
                                {!! Form::label('escuela_curso', 'Academic Year') !!}
                                {!! Form::select('escuela_curso',ConfigHelper::getCICAcademicYearTestColonias(),
                                '',
                                array(
                                        'id'=> 'escuela_curso',
                                        'name'=> 'escuela_curso',
                                        'class' => "selectpicker form-control escuela_curso",
                                        'title' => "...",
                                        'required'
                                    )
                                )
                                !!}
                            </div>
                        </div>
                        <div class="row addmargintop30">
                            <div class="col-sm-6">
                                {!! Form::label('testnumber', 'Test Name') !!}
                                {!! Form::select('testnumber',
                                array(
                                    0 => '',
                                    'B0' => 'B0',
                                    'B1' => 'B1',
                                    'B2' => 'B2',
                                    'B3' => 'B3',
                                ),
                                '',
                                array(
                                        'id'=> 'testnumber',
                                        'name'=> 'testnumber',
                                        'class' => "selectpicker form-control testnumber",
                                        'title' => "...",
                                        'required'
                                    )
                                )
                                !!}
                            </div>

                            <div class="col-sm-6">
                                {!! Form::label('englishlevel', 'English Level') !!}
                                {!! Form::select('englishlevel',
                                array(
                                    0 => '',
                                    1 => 'Low',
                                    2 => 'High'
                                ),
                                '',
                                array(
                                        'id'=> 'englishlevel',
                                        'name'=> 'englishlevel',
                                        'class' => "selectpicker form-control englishlevel",
                                        'title' => "...",
                                        'required'
                                    )
                                )
                                !!}
                            </div>
                        </div>
                        <div class="row addmargintop30">
                            <div class="col-sm-4">
                                {!! Form::label('choicepoints', 'MC Level') !!}
                                {!! Form::select('choicepoints',array(
                                    0 => '',
                                    1 => '1',
                                    2 => '2',
                                    3 => '3',
                                    4 => '4',
                                    5 => '5',
                                    6 => '6',
                                    7 => '7',
                                    8 => '8',
                                    9 => '9',
                                    10 => '10',
                                    11 => '11',
                                    12 => '12',
                                    13 => '13',
                                    14 => '14',
                                    15 => '15',
                                    16 => '16',
                                    17 => '17',
                                    18 => '18',
                                    19 => '19',
                                    20 => '20',
                                    21 => '21',
                                    22 => '22',
                                    23 => '23',
                                    24 => '24',
                                    25 => '25'
                                ),
                                '',
                                array(
                                        'id'=> 'choicepoints',
                                        'name'=> 'choicepoints',
                                        'class' => "selectpicker form-control choicelevel",
                                        'title' => "...",
                                        'required'
                                    )
                                )
                                !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::label('choicelevel', 'MC Level') !!}
                                {!! Form::select('choicelevel',ConfigHelper::getCICNivelTestColonias(),
                                '',
                                array(
                                        'id'=> 'choicelevel',
                                        'name'=> 'choicelevel',
                                        'class' => "selectpicker form-control choicelevel",
                                        'title' => "...",
                                        'required'
                                    )
                                )
                                !!}
                            </div>
                        </div>

                        <div class="row addmargintop30">
                            <div class="col-sm-12">
                                {!! Form::label('textlevel', 'WT Level') !!}
                                {!! Form::select('textlevel',ConfigHelper::getCICNivelTestColonias(),
                                '',
                                array(
                                        'id'=> 'textlevel',
                                        'name'=> 'textlevel',
                                        'class' => "selectpicker form-control textlevel",
                                        'title' => "...",
                                        'required'
                                    )
                                )
                                !!}
                            </div>
                        </div>
                        <div class="row addmargintop30">
                            <div class="col-sm-12">
                                {!! Form::label('nivel', 'Final CIC Level') !!}
                                {!! Form::select('nivel',ConfigHelper::getCICNivelTestColonias(),
                                '',
                                array(
                                        'id'=> 'nivel',
                                        'name'=> 'nivel',
                                        'class' => "selectpicker form-control nivel",
                                        'title' => "...",
                                        'required'
                                    )
                                )
                                !!}
                            </div>
                        </div>

                        <div class="row addmargintop30">
                            <div class="col-sm-12">
                                {!! Form::label('oralresult', 'Oral Result') !!}
                                {!! Form::text('oralresult', '', ['class'=> 'form-control']) !!}
                            </div>
                        </div>

                        <div class="row addmargintop30">
                            <div class="col-sm-12">
                                {!! Form::label('trinity', 'Trinity Grade') !!}
                                {!! Form::text('trinity', '', ['class'=> 'form-control']) !!}
                            </div>
                        </div>
                        
                        {!! Form::hidden('booking_id', '', array('id' => 'booking_id', 'class' => 'booking')) !!}
                        {!! Form::close() !!}

                        <div class="msj"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary enviar" id="editorsave" type="button">Guardar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@stop

@section('extra_footer')
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

    <script>
        {!! $scripts !!}


@if($results)

        var allCharts = AmCharts.charts;
        for (var i = 0; i < allCharts.length; i++) {
            if( typeof allCharts[i].ocultar === 'undefined' )
            {
                allCharts[i].addListener('init',function(e) {
                    var ultimo = e.chart.legend.legendData.length-1;
                    e.chart.hideSlice(ultimo);
                });
            }
        }

   @if($cuestionario->form == 'testnivelcoloniascic')


        $(document).ready(function() {
            var dt = $('#testcoloniascictable').DataTable();

            // On each draw, loop over the `detailRows` array and show any child rows
            dt.on( 'draw', function () {

                $('.editorbutton').on('click', function () {
                    console.log($(this).data('booking'));

                    $.ajax({
                        type: "GET",
                        url: "{{route('manage.system.cuestionarios.editor')}}?booking_id="+$(this).data('booking')+'&cuestionario_id={{$cuestionario->id}}',
                        //data: post_data,
                        success: function (respuesta) {
                            console.log(respuesta);
                            $('#viajeroname h4').html(respuesta.viajero);
                            console.log(respuesta);
                            if(respuesta.estado != 3) {
                                $('.ptxt div').html(respuesta.ptxt);
                                $('.test span').html(respuesta.test);
                                $('.multiplechoice span').html(respuesta.choicelevel + ' (' + respuesta.choicepoints + '/25)');
                                $('#online #textlevel').selectpicker('val', respuesta.textlevel);
                                $('#online #nivel').selectpicker('val', respuesta.nivel);
                                $('#online #oralresult').val(respuesta.oralresult);
                                $('#online #trinity').val(respuesta.trinity);
                                $('#online .booking').val(respuesta.booking_id);
                                $('.msj').html('');
                                $('#online').show();
                                $('#offline').hide();
                                $('#editor').modal();
                            }else{
                                $('#offline #escuela_curso').selectpicker('val', respuesta.escuela_curso);
                                $('#offline #testnumber').selectpicker('val', respuesta.testnumber);
                                $('#offline #choicepoints').selectpicker('val',respuesta.choicepoints);
                                $('#offline #choicelevel').selectpicker('val', respuesta.choicelevelnumber);
                                $('#offline #englishlevel').selectpicker('val', respuesta.englishlevel);
                                $('#offline #textlevel').selectpicker('val', respuesta.textlevel);
                                $('#offline #nivel').selectpicker('val', respuesta.nivel);
                                $('#offline #oralresult').val(respuesta.oralresult);
                                $('#offline #trinity').val(respuesta.trinity);
                                $('#offline .booking').val(respuesta.booking_id);
                                $('#online').hide();
                                $('#offline').show();
                                $('#editor').modal();
                            }
                        },
                        error: function (msg) {
                            console.log(msg);
                        }
                    });
                })
            } );

               $('#editor').on('hidden.bs.modal', function () {
                   $('.ptxt div').html('');
                   $('.test span').html('');
                   $('.multiplechoice span').html('');
                   $('#online #textlevel').selectpicker('val', '');
                   $('#online #nivel').selectpicker('val', '');
                   $('#online #oralresult').val('');
                   $('#online #trinity').val('');
                   $('#online .booking_id').val('');

                   $('#offline #escuela_curso').selectpicker('val', '');
                   $('#offline #testnumber').selectpicker('val', '');
                   $('#offline #choicepoints').selectpicker('val', '');
                   $('#offline #choicelevel').selectpicker('val', '');
                   $('#offline #englishlevel').selectpicker('val', '');
                   $('#offline #textlevel').selectpicker('val', '');
                   $('#offline #nivel').selectpicker('val', '');
                   $('#offline #oralresult').val('');
                   $('#offline #trinity').val('');
                   $('#offline .booking').val('');

                   $('.msj').html('');
               });

            $('#editorsave').on('click', function(){

                if($('#online').is(":visible")){
                    var url = "{{route('manage.system.cuestionarios.editorsave')}}?booking_id="+$('#online .booking ').val()+'&cuestionario_id={{$cuestionario->id}}&textlevel='+$('#online #textlevel').val()+'&nivel='+$('#online #nivel').val()+'&oralresult='+$('#online #oralresult').val()+'&trinity='+$('#online #trinity').val();
                }
                if($('#offline').is(":visible")){
                    var url = "{{route('manage.system.cuestionarios.editorsaveoffline')}}?booking_id="+$('#offline .booking').val()+'&cuestionario_id={{$cuestionario->id}}&escuela_curso='+$('#offline #escuela_curso').val()+'&testnumber='+$('#offline #testnumber').val()+'&englishlevel='+$('#offline #englishlevel').val()+'&choicepoints='+$('#offline #choicepoints').val()+'&choicelevel='+$('#offline #choicelevel').val()+'&textlevel='+$('#offline #textlevel').val()+'&nivel='+$('#offline #nivel').val()+'&oralresult='+$('#offline #oralresult').val()+'&trinity='+$('#offline #trinity').val();
                }

                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (respuesta) {
                        console.log(respuesta);
                        $('.msj').html('');
                        dt.ajax.reload();
                        $('#editor').modal('hide');
                    },
                    error: function (msg) {
                        console.log(msg);
                        $('.msj').html('<span class="text-danger">¡Error al guardar los datos!</span>');
                    }
                });
            });


        } );

    @endif
@endif

    </script>
@stop
