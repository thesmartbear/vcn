@if(count($ficha->cuestionarios))
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list-alt fa-fw"></i> Cuestionarios vinculados
    </div>
    <div class="panel-body">

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Cuestionario</th>
                    <th class='col-md-1'>Formulario</th>
                    <th>Destino</th>
                    <th>Viajeros</th>
                    <th>Pendientes</th>
                    <th>Entregados</th>
                    <th>Aceptados</th>
                    <th>Tutores</th>
                    <th>Pendientes</th>
                    <th>Entregados</th>
                    <th>Aceptados</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($ficha->cuestionarios as $form)
                    <?php
                        $totv = $form->total_viajeros_estado;
                        $tott = $form->total_tutores_estado;
                    ?>
                    <tr>
                        <td>{{$form->name}}</td>
                        <td>{{$form->form}}</td>
                        <td>{{$form->destino}}</td>
                        <td>{{$form->total_viajeros}}</td>
                        <td>{{$totv[0]}}</td>
                        <td>{{$totv[1]}}</td>
                        <td>{{$totv[2]}}</td>
                        <td>{{$form->total_tutores}}</td>
                        <td>{{$tott[0]}}</td>
                        <td>{{$tott[1]}}</td>
                        <td>{{$tott[2]}}</td>
                        <td>
                            <a href="{{route('manage.system.cuestionarios.reclamar',[$form->id, $modelo, $modelo_id])}}" data-label="Reclamar pendientes" class="btn btn-warning btn-xs"><i class="fa fa-bullhorn"></i></a>

                            <a href='#destroy' data-label='Borrar' data-model='Vinculación de Cuestionario' data-action=" {{route( 'manage.system.cuestionarios.vinculados.delete', $form->pivot->id)}}" data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@endif

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list-alt fa-fw"></i> Vincular cuestionario
    </div>
    <div class="panel-body">
        {!! Form::open(array('method' => 'POST', 'url' => route('manage.system.cuestionarios.vinculados') , 'role' => 'form', 'class' => '')) !!}

            {!! Form::hidden('modelo',$modelo) !!}
            {!! Form::hidden('modelo_id',$modelo_id) !!}

            <div class="form-group">
                @include('includes.form_select', ['campo'=> 'cuestionario_id', 'texto'=> 'Cuestionario', 'select'=> $cuestionarios])
            </div>

            <div class="form-group">
                @include('includes.form_submit', [ 'permiso'=> 'cuestionarios', 'texto'=> 'Guardar'])
            </div>

        {!! Form::close() !!}
    </div>
</div>