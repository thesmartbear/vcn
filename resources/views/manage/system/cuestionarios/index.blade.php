@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.index') !!} --}}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-list-alt fa-fw"></i> Cuestionarios
                <span class="pull-right"><a href="{{ route('manage.system.cuestionarios.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo cuestionario</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Cuestionario',
                      'form'            => 'Formulario',
                      'destino'         => 'Destino',
                      'test'            => 'Test de Nivel',
                      'activo'          => 'Activo',
                      'plataforma'      => 'Plataforma',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.system.cuestionarios.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [6] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop