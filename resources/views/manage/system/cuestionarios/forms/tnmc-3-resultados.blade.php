<?php
$tA01 = $respuesta->tA01;
if (strtolower($tA01) != "i") {
    $tA01 = "<span class='text-danger'>".$tA01."</span>";
}else{
    $tA01 = "<span class='text-success'>".$tA01."</span>";
}

$tA02 = $respuesta->tA02;
if (strtolower($tA02) != "m") {
    $tA02 = "<span class='text-danger'>".$tA02."</span>";
}else{
    $tA02 = "<span class='text-success'>".$tA02."</span>";
}

$tA03 = $respuesta->tA03;
if (strtolower($tA03) != "light") {
    $tA03 = "<span class='text-danger'>".$tA03."</span>";
}else{
    $tA03 = "<span class='text-success'>".$tA03."</span>";
}

$tA04 = $respuesta->tA04;
if ( strtolower($tA04) != 'my' ) {
    $tA04 = "<span class='text-danger'>".$tA04."</span>";
}else{
    $tA04 = "<span class='text-success'>".$tA04."</span>";
}

$tA05 = $respuesta->tA05;
if ( strtolower($tA05) != 'works' ) {
    $tA05 = "<span class='text-danger'>".$tA05."</span>";
}else{
    $tA05 = "<span class='text-success'>".$tA05."</span>";
}

$tA06 = $respuesta->tA06;
if ( strtolower($tA06) != 'brother' ) {
    $tA06 = "<span class='text-danger'>".$tA06."</span>";
}else{
    $tA06 = "<span class='text-success'>".$tA06."</span>";
}

$tA07 = $respuesta->tA07;
if (strtolower($tA07) != "they") {
    $tA07 = "<span class='text-danger'>".$tA07."</span>";
}else{
    $tA07 = "<span class='text-success'>".$tA07."</span>";
}

$tA08 = $respuesta->tA08;
if ( strtolower($tA08) != 'and' ) {
    $tA08 = "<span class='text-danger'>".$tA08."</span>";
}else{
    $tA08 = "<span class='text-success'>".$tA08."</span>";
}

$tA09 = $respuesta->tA09;
if ( strtolower($tA09) != 'the' ) {
    $tA09 = "<span class='text-danger'>".$tA09."</span>";
}else{
    $tA09 = "<span class='text-success'>".$tA09."</span>";
}

$tA10 = $respuesta->tA10;
if ( strtolower($tA10) != 'to' ) {
    $tA10 = "<span class='text-danger'>".$tA10."</span>";
}else{
    $tA10 = "<span class='text-success'>".$tA10."</span>";
}

$tA11 = $respuesta->tA11;
if ( strtolower($tA11) != 'moved' ) {
    $tA11 = "<span class='text-danger'>".$tA11."</span>";
}else{
    $tA11 = "<span class='text-success'>".$tA11."</span>";
}

$tA12 = $respuesta->tA12;
if ( strtolower($tA12) != 'in' ) {
    $tA12 = "<span class='text-danger'>".$tA12."</span>";
}else{
    $tA12 = "<span class='text-success'>".$tA12."</span>";
}

$tA13 = $respuesta->tA13;
if (strtolower($tA13) != "m") {
    $tA13 = "<span class='text-danger'>".$tA13."</span>";
}else{
    $tA13 = "<span class='text-success'>".$tA13."</span>";
}

$tA14 = $respuesta->tA14;
if ( strtolower($tA14) != 'a' ) {
    $tA14 = "<span class='text-danger'>".$tA14."</span>";
}else{
    $tA14 = "<span class='text-success'>".$tA14."</span>";
}

$tA15 = $respuesta->tA15;
if (strtolower($tA15) != "like") {
    $tA15 = "<span class='text-danger'>".$tA15."</span>";
}else{
    $tA15 = "<span class='text-success'>".$tA15."</span>";
}

$tA16 = $respuesta->tA16;
if ( strtolower($tA16) != 'at' ) {
    $tA16 = "<span class='text-danger'>".$tA16."</span>";
}else{
    $tA16 = "<span class='text-success'>".$tA16."</span>";
}

$tA17 = $respuesta->tA17;
if (strtolower($tA17) != "theatre") {
    $tA17 = "<span class='text-danger'>".$tA17."</span>";
}else{
    $tA17 = "<span class='text-success'>".$tA17."</span>";
}

$tA18 = $respuesta->tA18;
if ( strtolower($tA18) != 'and' ) {
    $tA18 = "<span class='text-danger'>".$tA18."</span>";
}else{
    $tA18 = "<span class='text-success'>".$tA18."</span>";
}

$tA19 = $respuesta->tA19;
if (strtolower($tA19) != "yourself") {
    $tA19 = "<span class='text-danger'>".$tA19."</span>";
}else{
    $tA19 = "<span class='text-success'>".$tA19."</span>";
}

$tA20 = $respuesta->tA20;
if (strtolower($tA20) != "company") {
    $tA20 = "<span class='text-danger'>".$tA20."</span>";
}else{
    $tA20 = "<span class='text-success'>".$tA20."</span>";
}

$tA21 = $respuesta->tA21;
if (strtolower($tA21) != "a") {
    $tA21 = "<span class='text-danger'>".$tA21."</span>";
}else{
    $tA21 = "<span class='text-success'>".$tA21."</span>";
}

$tA22 = $respuesta->tA22;
if (strtolower($tA22) != "although") {
    $tA22 = "<span class='text-danger'>".$tA22."</span>";
}else{
    $tA22 = "<span class='text-success'>".$tA22."</span>";
}

$tA23 = $respuesta->tA23;
if (strtolower($tA23) != "the") {
    $tA23 = "<span class='text-danger'>".$tA23."</span>";
}else{
    $tA23 = "<span class='text-success'>".$tA23."</span>";
}

$tA24 = $respuesta->tA24;
if ( strtolower($tA24) != 'with' ) {
    $tA24 = "<span class='text-danger'>".$tA24."</span>";
}else{
    $tA24 = "<span class='text-success'>".$tA24."</span>";
}

$tA25 = $respuesta->tA25;
if ( strtolower($tA25) != 'i' ) {
    $tA25 = "<span class='text-danger'>".$tA25."</span>";
}else{
    $tA25 = "<span class='text-success'>".$tA25."</span>";
}

$tA26 = $respuesta->tA26;
if (strtolower($tA26) != "job") {
    $tA26 = "<span class='text-danger'>".$tA26."</span>";
}else{
    $tA26 = "<span class='text-success'>".$tA26."</span>";
}

$tA27 = $respuesta->tA27;
if (strtolower($tA27) != "found") {
    $tA27 = "<span class='text-danger'>".$tA27."</span>";
}else{
    $tA27 = "<span class='text-success'>".$tA27."</span>";
}

$tA28 = $respuesta->tA28;
if (strtolower($tA28) != "m") {
    $tA28 = "<span class='text-danger'>".$tA28."</span>";
}else{
    $tA28 = "<span class='text-success'>".$tA28."</span>";
}

$tA29 = $respuesta->tA29;
if ( strtolower($tA29) != 'have' ) {
    $tA29 = "<span class='text-danger'>".$tA29."</span>";
}else{
    $tA29 = "<span class='text-success'>".$tA29."</span>";
}

$tA30 = $respuesta->tA30;
if (strtolower($tA30) != "this") {
    $tA30 = "<span class='text-danger'>".$tA30."</span>";
}else{
    $tA30 = "<span class='text-success'>".$tA30."</span>";
}

$tA31 = $respuesta->tA31;
if (strtolower($tA31) != "was") {
    $tA31 = "<span class='text-danger'>".$tA31."</span>";
}else{
    $tA31 = "<span class='text-success'>".$tA31."</span>";
}

$tA32 = $respuesta->tA32;
if ( strtolower($tA32) != 'people' ) {
    $tA32 = "<span class='text-danger'>".$tA32."</span>";
}else{
    $tA32 = "<span class='text-success'>".$tA32."</span>";
}

$tA33 = $respuesta->tA33;
if ( strtolower($tA33) != 'ago' ) {
    $tA33 = "<span class='text-danger'>".$tA33."</span>";
}else{
    $tA33 = "<span class='text-success'>".$tA33."</span>";
}

$tA34 = $respuesta->tA34;
if ( strtolower($tA34) != 'again' ) {
    $tA34 = "<span class='text-danger'>".$tA34."</span>";
}else{
    $tA34 = "<span class='text-success'>".$tA34."</span>";
}

$tA35 = $respuesta->tA35;
if ( strtolower($tA35) != 'he' ) {
    $tA35 = "<span class='text-danger'>".$tA35."</span>";
}else{
    $tA35 = "<span class='text-success'>".$tA35."</span>";
}

$tA36 = $respuesta->tA36;
if (strtolower($tA36) != "told") {
    $tA36 = "<span class='text-danger'>".$tA36."</span>";
}else{
    $tA36 = "<span class='text-success'>".$tA36."</span>";
}

$tA37 = $respuesta->tA37;
if ( strtolower($tA37) != 'i' ) {
    $tA37 = "<span class='text-danger'>".$tA37."</span>";
}else{
    $tA37 = "<span class='text-success'>".$tA37."</span>";
}

$tA38 = $respuesta->tA38;
if (strtolower($tA38) != "though") {
    $tA38 = "<span class='text-danger'>".$tA38."</span>";
}else{
    $tA38 = "<span class='text-success'>".$tA38."</span>";
}

$tA39 = $respuesta->tA39;
if ( strtolower($tA39) != 'what' ) {
    $tA39 = "<span class='text-danger'>".$tA39."</span>";
}else{
    $tA39 = "<span class='text-success'>".$tA39."</span>";
}

$tA40 = $respuesta->tA40;
if ( strtolower($tA40) != 'which' ) {
    $tA40 = "<span class='text-danger'>".$tA40."</span>";
}else{
    $tA40 = "<span class='text-success'>".$tA40."</span>";
}

$tA41 = $respuesta->tA41;
if ( strtolower($tA41) != 'to' ) {
    $tA41 = "<span class='text-danger'>".$tA41."</span>";
}else{
    $tA41 = "<span class='text-success'>".$tA41."</span>";
}

$tA42 = $respuesta->tA42;
if (strtolower($tA42) != "theft") {
    $tA42 = "<span class='text-danger'>".$tA42."</span>";
}else{
    $tA42 = "<span class='text-success'>".$tA42."</span>";
}

$tA43 = $respuesta->tA43;
if ( strtolower($tA43) != 'place' ) {
    $tA43 = "<span class='text-danger'>".$tA43."</span>";
}else{
    $tA43 = "<span class='text-success'>".$tA43."</span>";
}

$tA44 = $respuesta->tA44;
if (strtolower($tA44) != "five") {
    $tA44 = "<span class='text-danger'>".$tA44."</span>";
}else{
    $tA44 = "<span class='text-success'>".$tA44."</span>";
}

$tA45 = $respuesta->tA45;
if ( strtolower($tA45) != 'and' ) {
    $tA45 = "<span class='text-danger'>".$tA45."</span>";
}else{
    $tA45 = "<span class='text-success'>".$tA45."</span>";
}

$tA46 = $respuesta->tA46;
if (strtolower($tA46) != "three") {
    $tA46 = "<span class='text-danger'>".$tA46."</span>";
}else{
    $tA46 = "<span class='text-success'>".$tA46."</span>";
}

$tA47 = $respuesta->tA47;
if (strtolower($tA47) != "locks") {
    $tA47 = "<span class='text-danger'>".$tA47."</span>";
}else{
    $tA47 = "<span class='text-success'>".$tA47."</span>";
}

$tA48 = $respuesta->tA48;
if ( strtolower($tA48) != 'of' ) {
    $tA48 = "<span class='text-danger'>".$tA48."</span>";
}else{
    $tA48 = "<span class='text-success'>".$tA48."</span>";
}

$tA49 = $respuesta->tA49;
if ( strtolower($tA49) != 'by' ) {
    $tA49 = "<span class='text-danger'>".$tA49."</span>";
}else{
    $tA49 = "<span class='text-success'>".$tA49."</span>";
}

$tA50 = $respuesta->tA50;
if ( strtolower($tA50) != 'exhibition' ) {
    $tA50 = "<span class='text-danger'>".$tA50."</span>";
}else{
    $tA50 = "<span class='text-success'>".$tA50."</span>";
}

$tA51 = $respuesta->tA51;
if (strtolower($tA51) != "where") {
    $tA51 = "<span class='text-danger'>".$tA51."</span>";
}else{
    $tA51 = "<span class='text-success'>".$tA51."</span>";
}

$tA52 = $respuesta->tA52;
if (strtolower($tA52) != "displaying") {
    $tA52 = "<span class='text-danger'>".$tA52."</span>";
}else{
    $tA52 = "<span class='text-success'>".$tA52."</span>";
}

$tA53 = $respuesta->tA53;
if (strtolower($tA53) != "fact") {
    $tA53 = "<span class='text-danger'>".$tA53."</span>";
}else{
    $tA53 = "<span class='text-success'>".$tA53."</span>";
}

$tA54 = $respuesta->tA54;
if ( strtolower($tA54) != 'is' ) {
    $tA54 = "<span class='text-danger'>".$tA54."</span>";
}else{
    $tA54 = "<span class='text-success'>".$tA54."</span>";
}

$tA55 = $respuesta->tA55;
if ( strtolower($tA55) != 'of' ) {
    $tA55 = "<span class='text-danger'>".$tA55."</span>";
}else{
    $tA55 = "<span class='text-success'>".$tA55."</span>";
}

$tA56 = $respuesta->tA56;
if (strtolower($tA56) != "remove") {
    $tA56 = "<span class='text-danger'>".$tA56."</span>";
}else{
    $tA56 = "<span class='text-success'>".$tA56."</span>";
}

$tA57 = $respuesta->tA57;
if (strtolower($tA57) != "sound") {
    $tA57 = "<span class='text-danger'>".$tA57."</span>";
}else{
    $tA57 = "<span class='text-success'>".$tA57."</span>";
}

$tA58 = $respuesta->tA58;
if ( strtolower($tA58) != 'will' ) {
    $tA58 = "<span class='text-danger'>".$tA58."</span>";
}else{
    $tA58 = "<span class='text-success'>".$tA58."</span>";
}

$tA59 = $respuesta->tA59;
if ( strtolower($tA59) != 'with' ) {
    $tA59 = "<span class='text-danger'>".$tA59."</span>";
}else{
    $tA59 = "<span class='text-success'>".$tA59."</span>";
}

$tA60 = $respuesta->tA60;
if (strtolower($tA60) != "our") {
    $tA60 = "<span class='text-danger'>".$tA60."</span>";
}else{
    $tA60 = "<span class='text-success'>".$tA60."</span>";
}
?>
<div class="row">
    <div class="col-md-10">
        <legend>Read the tests below and choose an appropriate word in each space.</legend>
        <h2>Text A</h2>
        <blockquote>
            <p>
                Dear Jose,
            </p>
            <p align='justify'>Here is a photograph of me. <b>{!!$tA01!!}</b>'m afraid it isn't very good! I'<b>{!!$tA02!!}</b> tall and very thin, and I've got <b>{!!$tA03!!}</b> brown hair and grey eyes.  I look like <b>{!!$tA04!!}</b> mother - she's a social worker.  My father <b>{!!$tA05!!}</b> for a chemical company.  I've got a <b>{!!$tA06!!}</b> called Jack, and two sisters, Emma and Rose. <b>{!!$tA07!!}</b>'re all younger than me.<br />
                <br />
                I'm 18, <b>{!!$tA08!!}</b> I was born in Northwich, a town in <b>{!!$tA09!!}</b> north-west of England, near Manchester.  I went <b>{!!$tA10!!}</b> school there for seven years.  Then my parents <b>{!!$tA11!!}</b> to London, so I went to secondary school <b>{!!$tA12!!}</b> London.  I left school last year.  Now I'<b>{!!$tA13!!}</b> a student and I'm going to be <b>{!!$tA14!!}</b> doctor.  Its very hard work, but I <b>{!!$tA15!!}</b> it!<br />
                <br />
                I don't have much free time <b>{!!$tA16!!}</b> the moment, but I like going to the <b>{!!$tA17!!}</b> and going to concerts.  I also like football <b>{!!$tA18!!}</b> sailing.<br />
                <br />
                Please write and tell me all about <b>{!!$tA19!!}</b>.</p>
            <p>Pat </p>
        </blockquote>
        <p>&nbsp;</p>
        <h2>Text B</h2>
        <blockquote>
            <p>
                Dear Mary,
            </p>
            <p align='justify'>I  worked for a small <b>{!!$tA20!!}</b> for ten years before I took on <b>{!!$tA21!!}</b> new job with a much larger firm. <b>{!!$tA22!!}</b> I didn't earn as much in <b>{!!$tA23!!}</b> old job, I got on much better <b>{!!$tA24!!}</b> my boss and the other employees  than <b>{!!$tA25!!}</b> do now.</p>
            <p align='justify'>I've  had the new <b>{!!$tA26!!}</b> for more than a year and have <b>{!!$tA27!!}</b> that my  personal life has suffered.&nbsp; I <b>{!!$tA28!!}</b> expected to travel a lot and never <b>{!!$tA29!!}</b> any time for my family.&nbsp; I find <b>{!!$tA30!!}</b> job less interesting  than the old one <b>{!!$tA31!!}</b>. There isn't the same contact with  <b>{!!$tA32!!}</b> I used to have.</p>
            <p align='justify'>A  few days  <b>{!!$tA33!!}</b> I happened to see my old boss <b>{!!$tA34!!}</b>. When I told him how I felt, <b>{!!$tA35!!}</b> offered me my old job back. I <b>{!!$tA36!!}</b> him I would think about it. If <b>{!!$tA37!!}</b> take the offer, I will be happier <b>{!!$tA38!!}</b> my salary won't be as good.</p>
            <p><b>{!!$tA39!!}</b> would you advise me to do?</p>
            <p><br />
                M.L. Hamilton </p>
        </blockquote>
        <p>&nbsp;</p>
        <h2>Text C</h2>
        <blockquote>
            <p align='justify'>Hello and welcome to the programme <b>{!!$tA40!!}</b> today features the latest devices designed <b>{!!$tA41!!}</b> protect you and your property from <b>{!!$tA42!!}</b>.</p>
            <p align='justify'>Statistics  show that a burglary takes <b>{!!$tA43!!}</b> every 90 seconds night and day: <b>{!!$tA44!!}</b> out of ten break-ins are spontaneous <b>{!!$tA45!!}</b> take less than ten minutes: and <b>{!!$tA46!!}</b> out of ten are through insecure <b>{!!$tA47!!}</b> and windows.</p>
            <p align='justify'>Tomorrow  the first exhibition <b>{!!$tA48!!}</b> it's kind ever to be seen <b>{!!$tA49!!}</b> the public, the National Crime Prevention <b>{!!$tA50!!}</b> will open at London's Barbican Centre <b>{!!$tA51!!}</b>more than sixty companies will be <b>{!!$tA52!!}</b> their household security  systems.</p>
            <p align='justify'>The depressing <b>{!!$tA53!!}</b> is that household and personal security <b>{!!$tA54!!}</b> now a boom business. The kind  <b>{!!$tA55!!}</b> devices which you can fit and <b>{!!$tA56!!}</b> yourself and which emit an ear-shattering <b>{!!$tA57!!}</b> are becoming increasingly popular and these <b>{!!$tA58!!}</b> be represented at the exhibition along <b>{!!$tA59!!}</b> the more conventional  alarms and locks.</p>
            <p align='left'><b>{!!$tA60!!}</b> reporter, Nick Bell went along to a preview of the exhibition and here's his  report...</p>
        </blockquote>
        <p>&nbsp;</p>
        <h4><strong>Puntuación:</strong> {{$resultados->puntuaciones['puntosA']}}</h4>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>
</div>