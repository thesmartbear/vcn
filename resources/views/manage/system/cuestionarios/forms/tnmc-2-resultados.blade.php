<?php
// pregunta 1
$sA01 = $respuesta->sA01;
if (strtolower($sA01) != "a") {
    $sA01 = "<span class='text-danger'>".$sA01."</span>";
}else{
    $sA01 = "<span class='text-success'>".$sA01."</span>";
}

$sA02 = $respuesta->sA02;
if (strtolower($sA02) != "work") {
    $sA02 = "<span class='text-danger'>".$sA02."</span>";
}else{
    $sA02 = "<span class='text-success'>".$sA02."</span>";
}

$sA03 = $respuesta->sA03;
if (strtolower($sA03) != "there") {
    $sA03 = "<span class='text-danger'>".$sA03."</span>";
}else{
    $sA03 = "<span class='text-success'>".$sA03."</span>";
}

$sA04 = $respuesta->sA04;
if (strtolower($sA04) != "them") {
    $sA04 = "<span class='text-danger'>".$sA04."</span>";
}else{
    $sA04 = "<span class='text-success'>".$sA04."</span>";
}

$sA05 = $respuesta->sA05;
if (strtolower($sA05) != "just") {
    $sA05 = "<span class='text-danger'>".$sA05."</span>";
}else{
    $sA05 = "<span class='text-success'>".$sA05."</span>";
}

$sA06 = $respuesta->sA06;
if (strtolower($sA06) != "stay") {
    $sA06 = "<span class='text-danger'>".$sA06."</span>";
}else{
    $sA06 = "<span class='text-success'>".$sA06."</span>";
}

$sA07 = $respuesta->sA07;
if (strtolower($sA07) != "have") {
    $sA07 = "<span class='text-danger'>".$sA07."</span>";
}else{
    $sA07 = "<span class='text-success'>".$sA07."</span>";
}

$sA08 = $respuesta->sA08;
if (strtolower($sA08) != "because") {
    $sA08 = "<span class='text-danger'>".$sA08."</span>";
}else{
    $sA08 = "<span class='text-success'>".$sA08."</span>";
}

$sA09 = $respuesta->sA09;
if (strtolower($sA09) != "some") {
    $sA09 = "<span class='text-danger'>".$sA09."</span>";
}else{
    $sA09 = "<span class='text-success'>".$sA09."</span>";
}

$sA10 = $respuesta->sA10;
if (strtolower($sA10) != "when") {
    $sA10 = "<span class='text-danger'>".$sA10."</span>";
}else{
    $sA10 = "<span class='text-success'>".$sA10."</span>";
}

$sA11 = $respuesta->sA11;
if (strtolower($sA10) != "who") {
    $sA11 = "<span class='text-danger'>".$sA11."</span>";
}else{
    $sA11 = "<span class='text-success'>".$sA11."</span>";
}
// pregunta 2
$sB01 = $respuesta->sB01;
if ($sB01 != "Sometimes") {
    $sB01 = "<span class='text-danger'>".$sB01."</span>";
}else{
    $sB01 = "<span class='text-success'>".$sB01."</span>";
}

$sB02 = $respuesta->sB02;
if ($sB02 != "Twice a month.") {
    $sB02 = "<span class='text-danger'>".$sB02."</span>";
}else{
    $sB02 = "<span class='text-success'>".$sB02."</span>";
}

$sB03 = $respuesta->sB03;
if ($sB03 != "Black trousers and a jumper") {
    $sB03 = "<span class='text-danger'>".$sB03."</span>";
}else{
    $sB03 = "<span class='text-success'>".$sB03."</span>";
}
$sB04 = $respuesta->sB04;
if ($sB04 != "I'm going to Tenerife with my family.") {
    $sB04 = "<span class='text-danger'>".$sB04."</span>";
}else{
    $sB04 = "<span class='text-success'>".$sB04."</span>";
}

$sB05 = $respuesta->sB05;
if ($sB05 != "I used to play with dolls") {
    $sB05 = "<span class='text-danger'>".$sB05."</span>";
}else{
    $sB05 = "<span class='text-success'>".$sB05."</span>";
}

$sB06 = $respuesta->sB06;
if ($sB06 != "I'd ask for their autograph.") {
    $sB06 = "<span class='text-danger'>".$sB06."</span>";
}else{
    $sB06 = "<span class='text-success'>".$sB06."</span>";
}

$sB07 = $respuesta->sB07;
if ($sB07 != "I went to the cinema") {
    $sB07 = "<span class='text-danger'>".$sB07."</span>";
}else{
    $sB07 = "<span class='text-success'>".$sB07."</span>";
}

$sB08 = $respuesta->sB08;
if ($sB08 != "I want to go to university") {
    $sB08 = "<span class='text-danger'>".$sB08."</span>";
}else{
    $sB08 = "<span class='text-success'>".$sB08."</span>";
}

// pregunta 3
$sC01 = $respuesta->sC01;
if (strtolower($sC01) != "amount") {
    $sC01 = "<span class='text-danger'>".$sC01."</span>";
}else{
    $sC01 = "<span class='text-success'>".$sC01."</span>";
}

$sC02 = $respuesta->sC02;
if (strtolower($sC02) != "on") {
    $sC02 = "<span class='text-danger'>".$sC02."</span>";
}else{
    $sC02 = "<span class='text-success'>".$sC02."</span>";
}

$sC03 = $respuesta->sC03;
if (strtolower($sC03) != "ought") {
    $sC03 = "<span class='text-danger'>".$sC03."</span>";
}else{
    $sC03 = "<span class='text-success'>".$sC03."</span>";
}

$sC04 = $respuesta->sC04;
if (strtolower($sC04) != "habits") {
    $sC04 = "<span class='text-danger'>".$sC04."</span>";
}else{
    $sC04 = "<span class='text-success'>".$sC04."</span>";
}

$sC05 = $respuesta->sC05;
if (strtolower($sC05) != "enough") {
    $sC05 = "<span class='text-danger'>".$sC05."</span>";
}else{
    $sC05 = "<span class='text-success'>".$sC05."</span>";
}

$sC06 = $respuesta->sC06;
if (strtolower($sC06) != "position") {
    $sC06 = "<span class='text-danger'>".$sC06."</span>";
}else{
    $sC06 = "<span class='text-success'>".$sC06."</span>";
}

$sC07 = $respuesta->sC07;
if (strtolower($sC07) != "plenty") {
    $sC07 = "<span class='text-danger'>".$sC07."</span>";
}else{
    $sC07 = "<span class='text-success'>".$sC07."</span>";
}

$sC08 = $respuesta->sC08;
if (strtolower($sC08) != "although") {
    $sC08 = "<span class='text-danger'>".$sC08."</span>";
}else{
    $sC08 = "<span class='text-success'>".$sC08."</span>";
}

$sC09 = $respuesta->sC09;
if (strtolower($sC09) != "if") {
    $sC09 = "<span class='text-danger'>".$sC09."</span>";
}else{
    $sC09 = "<span class='text-success'>".$sC09."</span>";
}

$sC10 = $respuesta->sC10;
if (strtolower($sC10) != "journey") {
    $sC10 = "<span class='text-danger'>".$sC10."</span>";
}else{
    $sC10 = "<span class='text-success'>".$sC10."</span>";
}
?>

<div class="row">
    <div class="col-md-10">
        <legend>Read the text. Choose the correct words</legend>
        <h3>Hospitals</h3>
        <p>When we think  of <strong>{!!$sA01!!}</strong> hospital, perhaps we only think of doctors and nurses there, but other people &nbsp;<strong>{!!$sA02!!}</strong> there too. They all do important jobs.  <strong>{!!$sA03!!}</strong> are secretaries, cooks and engineers. &nbsp;In hospitals with a lot  of children, they have teachers who give<strong>{!!$sA04!!}</strong>&nbsp;lessons when they can&rsquo;t go to  school. Some people go to hospital <strong>{!!$sA05!!}</strong> for one day, but other people need to <strong>{!!$sA06!!}</strong> there for a longer time.&nbsp; If you go to hospital, sometimes you&nbsp;<strong>{!!$sA07!!}</strong>&nbsp; to wait a long time before you see the doctor&nbsp;&nbsp;<strong>{!!$sA08!!}</strong>&nbsp; doctors have a lot of work to do.&nbsp; If you are in hospital for a long time, you  need to take clothes and&nbsp;&nbsp;<strong>{!!$sA09!!}</strong>  books  ?? comics to read.&nbsp; Often  your friends and family send you cards and flowers&nbsp;<strong>{!!$sA10!!}</strong> they visit you. Hospitals are full of people <strong>{!!$sA11!!}</strong> want to help you, but most of us still want to go home quickly.</p>
        <p>&nbsp;</p>
        <h4><strong>Puntuación:</strong> {{$resultados->puntuaciones['puntosA']}}</h4>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <legend>Read  the text and choose the best answer for each gap</legend>
        <p>&nbsp;</p>
        <p><strong>Do you like cooking?</strong> {!!$sB01!!}</p>
        <p><br />
            <strong>How often do you go to the cinema? </strong>{!!$sB02!!}</p>
        <p><br />
            <strong>What are you wearing?</strong><strong> </strong>{!!$sB03!!}</p>
        <p><br />
            <strong>What are you holiday plans?</strong><strong> </strong>{!!$sB04!!}</p>
        <p><br />
            <strong>What were your hobbies when you were younger?</strong><strong> </strong>{!!$sB05!!}</p>
        <p><br />
            <strong>What would you do if you met a famous person?</strong><strong> </strong>{!!$sB06!!}</p>
        <p><br />
            <strong>What did you do last weekend?</strong><strong> </strong>{!!$sB07!!}</p>
        <p><br />
            <strong>What will you do when you leave school?</strong><strong> </strong>{!!$sB08!!}</p>
        <p>&nbsp;</p>
        <h4><strong>Puntuación:</strong> {{$resultados->puntuaciones['puntosB']}}</h4>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <legend>Read the text. Choose the correct words</legend>
        <h3>Deep sleep</h3>
        <p>Deep sleep is important for everyone. The actual <strong>{!!$sC01!!}</strong> of sleep you need depends <strong> {!!$sC02!!}</strong> your age. A young child <strong>{!!$sC03!!}</strong> to sleep ten to twelve hours, and a teenager about nine hours. Adults differ a lot in their sleeping <strong>{!!$sC04!!}.</strong>&nbsp; For most of them, seven to eight hours a  night is <strong>{!!$sC05!!}</strong>, but some sleep longer, while others manage with only four hours.</p>
        <p>For a good  night, having a comfortable <strong>{!!$sC06!!}</strong> to sleep is very important. Also, there  should be <strong>{!!$sC07!!}</strong> of fresh air in the room. A warm drink sometimes helps people to sleep,<strong> {!!$sC08!!}</strong> it is not a good idea to drink coffee before going to bed.</p>
        <p><strong>{!!$sC09!!}</strong> you have to travel a long distance, try to go to bed earlier than usual the day before the <strong>{!!$sC10!!}</strong>.&nbsp; This will help you feel more rested when you  arrive.</p>
        <p>&nbsp;</p>
        <h4><strong>Puntuación:</strong> {{$resultados->puntuaciones['puntosC']}}</h4>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>
</div>