@extends('layouts.manage')

@section('breadcrumb')

@stop

@section('titulo')
    <i class="fa fa-comments fa-fw"></i> Resultados: {{$resultados->cuestionario->name}}
@stop

@section('container')
    <div class="row">
        <div class="col-md-8">
            <h3 class="text-primary">Viajero: {{$viajero->name}} {{$viajero->lastname}} {{$viajero->lastname2}}</h3>
            <h4>Curso: {{$viajero->booking->curso->name}} <small>({{$viajero->booking->course_start_date}} al {{$viajero->booking->course_end_date}})</small></h4>
        </div>
        <div class="col-md-3">
            <div class="btn-group pull-right" role="group">
                <a class="btn btn-primary btn-group" role="group" href="{{route('manage.bookings.ficha',$viajero->booking->id)}}"><i class="fa fa-pencil-square"></i> BOOKING</a>
                <a class="btn btn-success btn-group" role="group" href="{{route('manage.viajeros.ficha',$viajero->id)}}"><i class="fa fa-suitcase"></i> FICHA VIAJERO</a>
            </div>
        </div>
        <div class="col-md-1">
            <a class="btn btn-danger btn-group pull-right" role="group" href="{{URL::previous()}}"><i class="fa fa-chevron-left"></i> VOLVER</a>
        </div>
    </div>

    @if(isset($respuesta))

        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-speech"></i>
                    <span class="caption-subject bold uppercase">Tutor: {{$tutor->name}} {{$tutor->lastname}} {{$tutor->lastname2}}</span>
                    <span class="caption-helper"></span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body respuestas">
                <h4>{{trans('forms.padrescoloniascic.p01')}}</h4><p>{{trans('forms.'.$respuesta->r01)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r01t) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.p02')}}</h4><p>{{trans('forms.'.$respuesta->r02)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r02t) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.p03')}}</h4><p>{{trans('forms.'.$respuesta->r03)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r03t) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.p04')}}</h4><p>{{trans('forms.'.$respuesta->r04)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r04t) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.p05')}}</h4><p>{{trans('forms.'.$respuesta->r05)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r05t) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.p06')}}</h4><p>{{trans('forms.'.$respuesta->r06)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r06t) !!}</p>
                
                {{-- <h4>{{trans('forms.padrescoloniascic.p08')}}</h4><p>{{trans('forms.'.$respuesta->r08)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r08t) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.p09')}}</h4><p>{{trans('forms.'.$respuesta->r09)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r09t) !!}</p> --}}

                <h4>{{trans('forms.padrescoloniascic.p07')}}</h4><p>{!! nl2br($respuesta->r07) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.web')}}</h4><p>{{$respuesta->web}}</p>
                <h4>{{trans('forms.padrescoloniascic.catalogo')}}</h4><p>{{$respuesta->catalogo}}</p>
                <h4>{{trans('forms.padrescoloniascic.blog')}}</h4><p>{{$respuesta->blog}}</p>
                {{-- <h4>{{trans('forms.padrescoloniascic.autocares')}}</h4><p>{{$respuesta->autocares}}</p> --}}
                <h4>{{trans('forms.padrescoloniascic.lavado')}}</h4><p>{{$respuesta->lavado}}</p>
                {{-- @if($respuesta->trinity != '')
                    <h4>{{trans('forms.padrescoloniascic.trinity')}}</h4><p>{{$respuesta->trinity}}</p>
                @endif --}}
                @if($respuesta->trato != '')
                    <h4>{{trans('forms.padrescoloniascic.trato')}}</h4><p>{{$respuesta->trato}}</p>
                @endif
                <h4>{{trans('forms.padrescoloniascic.informe')}}</h4><p>{{$respuesta->informe}}</p>
            </div>
        </div>

    @endif

@stop