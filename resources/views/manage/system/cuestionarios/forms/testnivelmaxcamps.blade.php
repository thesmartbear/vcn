@extends('layouts.manage')

@section('breadcrumb')

@stop

@section('titulo')
    <i class="fa fa-comments fa-fw"></i> Resultados {{trans('forms.testnivelmaxcamps.testnivelmaxcamps')}}
@stop

@section('container')
    <div class="row">
        <div class="col-md-8">
            <h3 class="text-primary"> {{$viajero->name}} {{$viajero->lastname}} {{$viajero->lastname2}}</h3>
            <h4>Curso: {{$viajero->booking->curso->name}} <small>({{$viajero->booking->course_start_date}} al {{$viajero->booking->course_end_date}})</small></h4>
        </div>
        <div class="col-md-3">
            <div class="btn-group pull-right" role="group">
                <a class="btn btn-primary btn-group" role="group" href="{{route('manage.bookings.ficha',$viajero->booking->id)}}"><i class="fa fa-pencil-square"></i> BOOKING</a>
                <a class="btn btn-success btn-group" role="group" href="{{route('manage.viajeros.ficha',$viajero->id)}}"><i class="fa fa-suitcase"></i> FICHA</a>
            </div>
        </div>
        <div class="col-md-1">
            <a class="btn btn-danger btn-group pull-right" role="group" href="{{URL::previous()}}"><i class="fa fa-chevron-left"></i> VOLVER</a>
        </div>
    </div>

    @if( isset($respuesta) && isset($datos))
        <p>
            <b>{{trans('area.cursoactual')}}:</b> {{ConfigHelper::getEscuelaCurso($datos->escuela_curso)}}<br />
            <b>{{trans('area.escuela')}}:</b> {{$datos->escuela}}<br />
            <b>{{trans('forms.testnivelmaxcamps.tienestitulo')}}:</b> {{$datos->tienestitulo?'Si':'No'}}<br />
            @if($datos->tienestitulo == 1)
                <b>{{trans('forms.testnivelmaxcamps.titulooficial')}}:</b> {{$datos->titulooficial}}
            @endif
        </p>
        <hr>
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-speech"></i>
                    <span class="caption-subject bold uppercase"> LEVEL L{{$resultados->nivel}}</span>
                    <span class="caption-helper">[Total {{$resultados->resultado}} points]</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                @include('manage.system.cuestionarios.forms.tnmc-'.$datos->testnumber.'-resultados')
            </div>
        </div>


    @endif

@stop