<?php
// pregunta 1
$pA01 = $respuesta->pA01;
if ($pA01 != "scissors") {
    $pA01 = "<span class='text-danger'>".$pA01."</span>";
}else{
    $pA01 = "<span class='text-success'>".$pA01."</span>";
}

$pA02 = $respuesta->pA02;
if ($pA02 != "geography") {
    $pA02 = "<span class='text-danger'>".$pA02."</span>";
}else{
    $pA02 = "<span class='text-success'>".$pA02."</span>";
}

$pA03 = $respuesta->pA03;
if ($pA03 != "dictionary") {
    $pA03 = "<span class='text-danger'>".$pA03."</span>";
}else{
    $pA03 = "<span class='text-success'>".$pA03."</span>";
}

$pA04 = $respuesta->pA04;
if ($pA04 != "art") {
    $pA04 = "<span class='text-danger'>".$pA04."</span>";
}else{
    $pA04 = "<span class='text-success'>".$pA04."</span>";
}

$pA05 = $respuesta->pA05;
if ($pA05 != "spoon") {
    $pA05 = "<span class='text-danger'>".$pA05."</span>";
}else{
    $pA05 = "<span class='text-success'>".$pA05."</span>";
}

$pA06 = $respuesta->pA06;
if ($pA06 != "postcard") {
    $pA06 = "<span class='text-danger'>".$pA06."</span>";
}else{
    $pA06 = "<span class='text-success'>".$pA06."</span>";
}

$pA07 = $respuesta->pA07;
if ($pA07 != "knife") {
    $pA07 = "<span class='text-danger'>".$pA07."</span>";
}else{
    $pA07 = "<span class='text-success'>".$pA07."</span>";
}

$pA08 = $respuesta->pA08;
if ($pA08 != "university") {
    $pA08 = "<span class='text-danger'>".$pA08."</span>";
}else{
    $pA08 = "<span class='text-success'>".$pA08."</span>";
}

$pA09 = $respuesta->pA09;
if ($pA09 != "envelope") {
    $pA09 = "<span class='text-danger'>".$pA09."</span>";
}else{
    $pA09 = "<span class='text-success'>".$pA09."</span>";
}

$pA10 = $respuesta->pA10;
if ($pA10 != "newspaper") {
    $pA10 = "<span class='text-danger'>".$pA10."</span>";
}else{
    $pA10 = "<span class='text-success'>".$pA10."</span>";
}
// pregunta 2
$sB01 = $respuesta->pB01;
if ($sB01 != "Yes, you can.") {
    $sB01 = "<span class='text-danger'>".$sB01."</span>";
}else{
    $sB01 = "<span class='text-success'>".$sB01."</span>";
}

$sB02 = $respuesta->pB02;
if ($sB02 != "Yes, I love it.") {
    $sB02 = "<span class='text-danger'>".$sB02."</span>";
}else{
    $sB02 = "<span class='text-success'>".$sB02."</span>";
}

$sB03 = $respuesta->pB03;
if ($sB03 != "All right.") {
    $sB03 = "<span class='text-danger'>".$sB03."</span>";
}else{
    $sB03 = "<span class='text-success'>".$sB03."</span>";
}

$sB04 = $respuesta->pB04;
if ($sB04 != "Yes, it's easy.") {
    $sB04 = "<span class='text-danger'>".$sB04."</span>";
}else{
    $sB04 = "<span class='text-success'>".$sB04."</span>";
}

$sB05 = $respuesta->pB05;
if ($sB05 != "I like playing football.") {
    $sB05 = "<span class='text-danger'>".$sB05."</span>";
}else{
    $sB05 = "<span class='text-success'>".$sB05."</span>";
}

$sB06 = $respuesta->pB06;
if ($sB06 != "Sorry, I can't.") {
    $sB06 = "<span class='text-danger'>".$sB06."</span>";
}else{
    $sB06 = "<span class='text-success'>".$sB06."</span>";
}
// pregunta 3
$sC01 = $respuesta->pC01;
if (strtolower($sC01) != "one") {
    $sC01 = "<span class='text-danger'>".$sC01."</span>";
}else{
    $sC01 = "<span class='text-success'>".$sC01."</span>";
}

$sC02 = $respuesta->pC02;
if (strtolower($sC02) != "it") {
    $sC02 = "<span class='text-danger'>".$sC02."</span>";
}else{
    $sC02 = "<span class='text-success'>".$sC02."</span>";
}

$sC03 = $respuesta->pC03;
if (strtolower($sC03) != "even") {
    $sC03 = "<span class='text-danger'>".$sC03."</span>";
}else{
    $sC03 = "<span class='text-success'>".$sC03."</span>";
}

$sC04 = $respuesta->pC04;
if (strtolower($sC04) != "talking") {
    $sC04 = "<span class='text-danger'>".$sC04."</span>";
}else{
    $sC04 = "<span class='text-success'>".$sC04."</span>";
}

$sC05 = $respuesta->pC05;
if (strtolower($sC05) != "like") {
    $sC05 = "<span class='text-danger'>".$sC05."</span>";
}else{
    $sC05 = "<span class='text-success'>".$sC05."</span>";
}

$sC06 = $respuesta->pC06;
if (strtolower($sC06) != "by") {
    $sC06 = "<span class='text-danger'>".$sC06."</span>";
}else{
    $sC06 = "<span class='text-success'>".$sC06."</span>";
}

$sC07 = $respuesta->pC07;
if (strtolower($sC07) != "much") {
    $sC07 = "<span class='text-danger'>".$sC07."</span>";
}else{
    $sC07 = "<span class='text-success'>".$sC07."</span>";
}

$sC08 = $respuesta->pC08;
if (strtolower($sC08) != "are") {
    $sC08 = "<span class='text-danger'>".$sC08."</span>";
}else{
    $sC08 = "<span class='text-success'>".$sC08."</span>";
}
?>


<div class="row">
    <div class="col-md-10">
        <legend>Choose the right word.</legend>
        <p><strong>1. You use these to cut paper. </strong><br />{!!$pA01!!}
            <br />
            <br />
            <strong>2. When you study this subject you learn about  islands and jungles.</strong><br />{!!$pA02!!}<br />
            <br />
            <strong>3. If you can&rsquo;t spell a word you can use this to  help you.</strong><br />{!!$pA03!!}<br />
            <br />
            <strong>4. If you like drawing and painting you should  study this subject.</strong><br />{!!$pA04!!}<br />
            <br />
            <strong>5. We need one of these to eat soup  ?? fruit salad.</strong><br />{!!$pA05!!}<br />
            <br />
            <strong>6. When you are on holiday you send this to your  friends.</strong><br />{!!$pA06!!}<br />
            <br />
            <strong>7. You use this to cut bread, cheese and meat.</strong><br />{!!$pA07!!}<br />
            <br />
            <strong>8. You can study here after you leave school.</strong><br />{!!$pA08!!}<br />
            <br />
            <strong>9. We put a letter  ?? a card in this before we post it.</strong><br />{!!$pA09!!}<br />
            <br />
            <strong>10. You can buy this every day and read about things which have happened.</strong><br />{!!$pA10!!}<br />
        </p>
        <p>&nbsp;</p>
        <h4><strong>Puntuación:</strong> {{$resultados->puntuaciones['puntosA']}}</h4>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <legend>Read  the text and choose the best answer for each gap </legend>
        <table width='650' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'><strong>1.</strong></td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td width='185' height='25' align='left' valign='bottom'>Can I come with you?</td>
                <td width='185' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='185' height='25' align='left' valign='bottom'>&nbsp;</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td height='25' colspan='3' align='left' valign='bottom'>
                    {!!$sB01!!}</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'><strong>2.</strong></td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td width='185' height='60' align='left' valign='bottom'>Do you like skating?</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td colspan='3' align='left' valign='bottom'><label>{!!$sB02!!}</label></td>
            </tr>
            <tr>
                <td width='35' height='60' align='left' valign='bottom'><strong>3.</strong></td>
                <td width='60' height='60' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td height='60' colspan='3' align='left' valign='bottom'>We can skate in the  park. Shall we go there now?</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td colspan='3' align='left' valign='bottom'><label>{!!$sB03!!}</label></td>
            </tr>
            <tr>
                <td width='35' height='60' align='left' valign='bottom'><strong>4.</strong></td>
                <td width='60' height='60' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td width='185' height='60' align='left' valign='bottom'>Can you skate well?</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td colspan='3' align='left' valign='bottom'><label>{!!$sB04!!}</label></td>
            </tr>
            <tr>
                <td width='35' height='60' align='left' valign='bottom'><strong>5.</strong></td>
                <td width='60' height='60' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td width='185' height='60' align='left' valign='bottom'>What sports do you like?</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td colspan='3' align='left' valign='bottom'>{!!$sB05!!}</td>
            </tr>
            <tr>
                <td width='35' height='60' align='left' valign='bottom'><strong>6.</strong></td>
                <td width='60' height='60' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td height='60' colspan='3' align='left' valign='bottom'>I often play  football at the beach. Would you like  to come with me on Saturday?</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td colspan='3' align='left' valign='bottom'>{!!$sB06!!}</td>
            </tr>
            <tr>
                <td width='35' height='60' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td width='185' height='60' align='left' valign='bottom'>How about Sunday?</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
            </tr>
            <tr>
                <td width='35' height='30' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='30' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td width='185' height='30' align='left' valign='bottom'>Great! </td>
                <td width='185' height='30' align='left' valign='bottom'>&nbsp;</td>
                <td width='185' height='30' align='left' valign='bottom'>&nbsp;</td>
            </tr>
        </table>
        <p>&nbsp;</p>
        <h4><strong>Puntuación:</strong> {{$resultados->puntuaciones['puntosB']}}</h4>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <legend>Read  the text and choose the best answer for each gap </legend>
        <h3><strong><br />
                Dolphins</strong>  </h3>
        <p>People love dolphins because they are beautiful to watch and friendly. Dolphins are also <strong>{!!$sC01!!}</strong> of the  cleverest animals and are just as clever as dogs.&nbsp;<strong>{!!$sC02!!}</strong> is possible to teach them in the  same way we teach monkeys and dogs.&nbsp; Some  people <strong>{!!$sC03!!}</strong> believe that dolphins have a special way of <strong>{!!$sC04!!}</strong> to each  other.<br />
            <strong>{!!$sC05!!}</strong> many other sea animals  and fish, dolphins are in danger. Many dolphins are caught <strong>{!!$sC06!!}</strong> mistake in fishing  nets, but a <strong>{!!$sC07!!}</strong> greater problem is that thousands of dolphins <strong>{!!$sC08!!}</strong> dying because the sea is no longer clean enough. </p>
        <p>&nbsp;</p>
        <h4><strong>Puntuación:</strong> {{$resultados->puntuaciones['puntosC']}}</h4>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>
</div>