@extends('layouts.manage')


@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.nuevo') !!} --}}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-list-alt fa-fw"></i> Nuevo Cuestionario
            </div>
            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.system.cuestionarios.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'form', 'texto'=> 'Formulario', 'select'=> $forms])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', ['campo'=> 'destino', 'texto'=> 'Destino', 'select'=> ConfigHelper::getCuestionarioDestinatario()])
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'test', 'texto'=> 'Test de nivel'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activo'])
                    </div>

                    @include('includes.form_plataforma', ['campo'=> 'propietario', 'todas'=> true])

                    @include('includes.form_submit', [ 'permiso'=> 'cuestionarios', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop