@extends('layouts.manage')

@section('breadcrumb')

@stop

@section('titulo')
    <i class="fa fa-comments fa-fw"></i> Resultados
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros</span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.system.cuestionarios.resultados'), 'method'=> 'GET', 'class' => 'form']) !!}

                 <div class="form-group row">

                    <div class="col-md-5">
                    {!! Form::label('cuestionarios', 'Cuestionario') !!}
                    @include('includes.form_input_cargando',['id'=> 'cuestionarios-cargando'])
                    <br>
                    {!! Form::select('cuestionarios', $cuestionarios, $valores['cuestionarios'], array('class'=>'select2 col-md-5', 'data-style'=>'red', 'id'=>'filtro-cuestionarios'))  !!}
                    </div>

                    @if($plataformas)
                    <div class="col-md-2">
                        {!! Form::label('plataformas', 'Plataforma') !!}
                        <br>
                        {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    </div>
                    @endif

                    {{--
                    <div class="col-md-2">
                    {!! Form::label('estados', 'Estado') !!}
                    @include('includes.form_input_cargando',['id'=> 'estados-cargando'])
                    <br>
                    {!! Form::select('estado', $estados, $valores['estado'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-estado'))  !!}
                    </div>
                    --}}

                </div>

                {{--
                <div class="form-group row">

                    <div class="col-md-6">
                    {!! Form::label('cursos', 'Curso') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])

                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
                    </div>

                </div>

                <hr>
                --}}

                <div class="form-group row">

                    <div class="col-md-3">
                        {!! Form::label('desdes','Desde (fecha salida)') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desdes', 'texto'=> null, 'valor'=> $valores['desdes']])
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('hastas','Hasta (fecha salida)') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hastas', 'texto'=> null, 'valor'=> $valores['hastas']])
                    </div>

                    <div class="col-md-1 col-md-offset-1">
                        {!! Form::label('&nbsp;') !!}
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}

        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    @if($totales)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-list-alt fa-fw"></i> Resumen :: <strong>Cuestionario {{$cuestionario->name}} {{$cuestionario->test?"[Test]":""}} :: Destino: {{$cuestionario->destino}}</strong>
                            <span class="pull-right"><a href="{{route('manage.system.cuestionarios.reclamar',$cuestionario->id)}}" data-label="Reclamar pendientes" class="btn btn-warning btn-xs"><i class="fa fa-bullhorn"></i></a></span>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-12">

                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>Total Bookings</th>
                                            <th>Viajeros</th>
                                            <th>Pendientes</th>
                                            <th>Entregados</th>
                                            <th>Aceptados</th>
                                            @if($cuestionario->form == 'testnivelcoloniascic')
                                            <th>Offline</th>
                                            @endif
                                            <th>Tutores</th>
                                            <th>Pendientes</th>
                                            <th>Entregados</th>
                                            <th>Aceptados</th>
                                            @if($cuestionario->form == 'testnivelcoloniascic')
                                            <th>Offline</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{$totales['bookings']}}</td>
                                            <td>{{$totales['totalv']}}</td>
                                            <td>{{$totales['totalv_estados'][0]}}</td>
                                            <td>{{$totales['totalv_estados'][1]}}</td>
                                            <td>{{$totales['totalv_estados'][2]}}</td>
                                            @if($cuestionario->form == 'testnivelcoloniascic')
                                            <td>{{$totales['totalv_estados'][3]}}</td>
                                            @endif
                                            <td>{{$totales['totalt']}}</td>
                                            <td>{{$totales['totalt_estados'][0]}}</td>
                                            <td>{{$totales['totalt_estados'][1]}}</td>
                                            <td>{{$totales['totalt_estados'][2]}}</td>
                                            @if($cuestionario->form == 'testnivelcoloniascic')
                                            <td>{{$totales['totalt_estados'][3]}}</td>
                                            @endif
                                        </tr>
                                        </tbody>
                                    </table>

                                    <p>
                                        Cursos:
                                        <ul>
                                        @foreach($totales['cursos'] as $c)
                                            <li>{{$c}}</li>
                                        @endforeach
                                        </ul>
                                    </p>

                                </div>

                            </div>
                        </div>
                    </div>
                    @else
                        <div class="alert alert-info" role="alert">
                            <p>Totales solo si el filtro es por Cuestionario</p>
                        </div>
                    @endif

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-globe fa-fw"></i> Listado
                        </div>
                        <div class="panel-body">
                            @if($cuestionario->form == 'testnivelcoloniascic')

                            {!! Datatable::table()
                                ->addColumn([
                                    'viajerolastname'       => 'Viajero Apellidos',
                                    'viajeroname'       => 'Viajero Nombre',
                                    'curso'             => 'Curso',
                                    'convocatoria'      => 'Convocatoria',
                                    'viajero_estado'    => 'Viajero',
                                    'viajero_resultado'    => 'Resultado',
                                    'viajero_nivel'    => 'Nivel',
                                    'tutor1_estado'      => 'Estado Tutor1',
                                    'tutor1_resultado'    => 'Resultado',
                                    'tutor1_nivel'    => 'Nivel',
                                    'tutor2_estado'      => 'Estado Tutor2',
                                    'tutor2_resultado'    => 'Resultado',
                                    'tutor2_nivel'    => 'Nivel',
                                    'escola_thau'       => 'Escola Thau',
                                    'cic_idiomas'       => 'CIC Idiomes',
                                    'options'           => ''
                                ])

                                ->setUrl( route('manage.system.cuestionarios.resultados', $valores) )
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                  "columnDefs", array(
                                    [ "bSortable" => false, "aTargets" => [9] ]
                                    // [ "targets" => [5,6], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                  )
                                )
                                 ->setId('resumentable')
                                ->render() !!}

                            @else
                                {!! Datatable::table()
                                ->addColumn([
                                    'viajerolastname'       => 'Viajero Apellidos',
                                    'viajeroname'       => 'Viajero Nombre',
                                    'curso'             => 'Curso',
                                    'convocatoria'      => 'Convocatoria',
                                    'viajero_estado'    => 'Viajero',
                                    'viajero_resultado'    => 'Resultado',
                                    'viajero_nivel'    => 'Nivel',
                                    'tutor1_estado'      => 'Estado Tutor1',
                                    'tutor1_resultado'    => 'Resultado',
                                    'tutor1_nivel'    => 'Nivel',
                                    'tutor2_estado'      => 'Estado Tutor2',
                                    'tutor2_resultado'    => 'Resultado',
                                    'tutor2_nivel'    => 'Nivel',
                                    //'options'           => ''
                                ])

                                ->setUrl( route('manage.system.cuestionarios.resultados', $valores) )
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                  "columnDefs", array(
                                    [ "bSortable" => false, "aTargets" => [9] ]
                                    // [ "targets" => [5,6], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                  )
                                )
                                 ->setId('resumentable')
                                ->render() !!}

                            @endif

                        </div>
                    </div>

                @endif

           @endif


        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalestado" tabindex="-1" role="dialog" aria-labelledby="modalestado" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div id="viajeroname"><h4></h4></div>
                </div>
                <div class="modal-body">
                    {!! Form::open(array('id'=>'cambiarestado')) !!}
                    <div class="row addmargintop30">
                        <div class="col-sm-12">
                            <b>Estado actual:</b> <span class="estadoactual"></span>
                        </div>
                    </div>
                    <div class="row addmargintop30">
                        <div class="col-sm-12 ">
                            <b>{!! Form::label('estado', 'Nuevo Estado') !!}</b>
                            {!! Form::select('estado',ConfigHelper::areaFormEstado(),
                            '',
                            array(
                                    'class' => "selectpicker form-control estado",
                                    'title' => "...",
                                    'required'
                                )
                            )
                            !!}
                        </div>
                    </div>
                    {!! Form::hidden('booking_id', '', array('id' => 'booking_id', 'class' => 'booking')) !!}
                    {!! Form::close() !!}

                    <div class="msj"></div>
                </div>
                <div class="modal-footer">
                    <div id="confirm"><span class="text-danger">Esta acción eliminará el cuestionario entregado:</span> <button class="btn btn-success" id="estadosaveconfirm" type="button">Confirmar</button> <button class="btn btn-danger" id="estadosavecancel">Cancelar</button></div>
                    <div id="save"><button class="btn btn-primary enviar" id="estadosave" type="button">Guardar</button></div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@stop

@section('extra_footer')
    <script>
        $(document).ready(function() {

            $('#confirm').hide();

            var dt = $('#resumentable').DataTable();

            // On each draw, loop over the `detailRows` array and show any child rows
            dt.on( 'draw', function () {
                console.log('draw');

                $('.cambiarestadobutton').on('click', function () {

                    $.ajax({
                        type: "GET",
                        url: "{{route('manage.system.cuestionarios.cambiarestado')}}?booking_id="+$(this).data('booking')+'&cuestionario_id='+$('#filtro-cuestionarios').val(),
                        //data: post_data,
                        success: function (respuesta) {
                            console.log(respuesta);

                            $('#viajeroname h4').html(respuesta.viajero);
                            $('#estado').selectpicker('val',respuesta.estado);
                            $('.estadoactual').html(respuesta.estadoactual);
                            if(respuesta.estado == 0){
                                $('#estado').find('[value=1]').prop('disabled', true);
                                $('#estado').find('[value=2]').prop('disabled', true);
                                $('#estado').selectpicker('refresh');
                            }
                            if(respuesta.estado == 1 || respuesta.estado == 2){
                                $('#estado').find('[value=1]').prop('disabled', true);
                                $('#estado').find('[value=2]').prop('disabled', true);
                                $('#estado').find('[value=3]').prop('disabled', true);
                                $('#estado').selectpicker('refresh');
                            }
                            $('#booking_id').val(respuesta.booking_id);
                            $('.msj').html('');
                            $('#modalestado').modal();

                        },
                        error: function (msg) {
                            console.log(msg);
                        }
                    });
                });
            });

            $('#estadosaveconfirm').on('click', function() {
                guardar();
            });
            $('#estadosavecancel').on('click', function() {
                console.log('cancel');
                $('#confirm').hide();
                $('#save').show()
            });

            $('#estadosave').on('click', function() {
                console.log('estado save');
                if ($('#estado').val() == '') {
                    $('.msj').html('<span class="text-danger">Selecciona un estado</span>');
                    setTimeout(function () {
                        $('.msj').html('');
                    }, 1500);
                    return false;
                }
                if ($('#estado').val() == 0) {
                    console.log('estado 0');
                    $('#save').hide();
                    $('#confirm').show();
                    //return false;
                } else {
                    guardar();
                }
            });



            function guardar(){
                $.ajax({
                    type: "GET",
                    url: "{{route('manage.system.cuestionarios.cambiarestadosave')}}?booking_id="+$('#booking_id').val()+'&cuestionario_id='+$('#filtro-cuestionarios').val()+'&estado='+$('#estado').val(),
                    success: function (respuesta) {
                        console.log(respuesta);
                        $('.msj').html('');
                        dt.ajax.reload();
                        $('#modalestado').modal('hide');
                    },
                    error: function (msg) {
                        console.log(msg);
                        $('.msj').html('<span class="text-danger">¡Error al guardar los datos!</span>');
                    }
                });
            }


            $('#estado').on('change',function(){
                console.log('change estado');
                if($(this).val != 0){

                }
            });

        });
    </script>
@stop