@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.avisos.index') !!}
@stop

@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-file fa-fw"></i> Avisos - Docs
        <span class="pull-right">
            <a href="{{ route('manage.system.avisos.docs.nuevo',0) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Doc Aviso</a>
        </span>
    </div>
    <div class="panel-body">


            {!! Datatable::table()
                ->addColumn([
                  'name'      => 'Doc',
                  'activo'    => 'Activo',
                  'tipo'      => 'Tipo',
                  'options'   => ''
                  ])
                ->setUrl(route('manage.system.avisos.docs.index'))
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "aoColumnDefs", array(
                      [ "bSortable" => false, "aTargets" => [1,3] ]
                    )
                  )
                  ->render() !!}
    </div>
</div>

@stop