@extends('layouts.email')


@section('contenido')

<?php
    $asunto = $ficha->getCampo('titulo','es');
    $contenido = $ficha->getCampo('contenido','es');
?>

Asunto: {!! $asunto !!}
<hr>

{!! $contenido !!}


@if( $ficha->tipo == 1 )
{{-- NOTA DE PAGO --}}

<br><br>
Importe: {{$ficha->notapago_pagar_tipo?$ficha->notapago_pagar. "%":ConfigHelper::parseMoneda($ficha->notapago_pagar)}}
<br><br>
El pago debe hacerse vía transferencia bancaria al número de cuenta %NUM.CUENTA%. de %BANCO%.
<br><br>
Recordad enviarnos el comprobante del ingreso con el nombre del participante y nombre del programa al correo %EMAIL OFICINA%.

<br><br>
Nombre y apellido: XXXXX
<br>
Programa: XXXXX
<br>
Fecha de inicio: XXXX
<br>
Fecha final: XXXX

<br><br><br>
¡Saludos!

@endif

@stop