@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.avisos.docs.nuevo') !!}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-file fa-fw"></i> Nuevo Doc Aviso

            </div>
            <div class="panel-body">

                {!! Form::open(['id'=>'frm_aviso','route' => array('manage.system.avisos.docs.ficha',0), 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-6">
                        @include('includes.form_input_text', [ 'campo' => "name", 'texto'=> 'Nombre'])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activo'])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> "Tipo", 'select'=> ConfigHelper::getAvisoDocTipo()])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_checkbox', [ 'campo'=> 'reclamar_total_bool', 'texto'=> 'Reclamar total pendiente'])
                    </div>

                    <div id="reclamar_total_div">
                        <div class="col-md-2">
                            @include('includes.form_input_number', [ 'campo' => "notapago_porcentaje", 'texto'=> 'Pagado < % del total'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_number', [ 'campo' => "notapago_pagar", 'texto'=> 'Nota de Pago Pagar'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select', [ 'campo' => "notapago_pagar_tipo", 'texto'=> 'Tipo Pagar','select'=> $tipos ])
                        </div>
                    </div>
                </div>

                {{-- <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> "Tipo", 'select'=> ConfigHelper::getAvisoDoc()])
                </div> --}}

                <ul class="nav nav-tabs" role="tablist">
                @foreach(ConfigHelper::getIdiomaContacto() as $key=>$idioma)

                    @if($key!="")
                    <li role="presentation" class="{{$key=='ES'?'active':''}}"><a href="#doc-{{$idioma}}" aria-controls="ficha" role="doc-{{$idioma}}" data-toggle="tab">{{$idioma}}</a></li>
                    @endif

                @endforeach
                </ul>

                <div class="tab-content">
                @foreach(ConfigHelper::getIdiomaContacto() as $key=>$idioma)

                    @if($key!="")
                    <div role="tabpanel" class="tab-pane fade in {{$key=='ES'?'active':''}}" id="doc-{{$idioma}}">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-globe fa-fw"></i> Doc :: {{$idioma}}
                            </div>
                            <div class="panel-body">

                                <div class="form-group">
                                    @include('includes.form_input_text', ['valor'=> '', 'campo' => "titulo[$key]", 'texto'=> 'Título/Asunto'])
                                </div>

                                <div class="form-group">
                                    @include('includes.form_textarea_tinymce', ['valor'=> '', 'campo' => "contenido[$key]", 'texto'=> 'Contenido'])
                                </div>

                            </div>
                        </div>

                    </div>
                    @endif

                @endforeach
                </div>

                <div class="form-group pull-right">
                    {!! Form::submit('Crear', array( 'class'=> 'btn btn-danger')) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>

@stop

@push('scripts')
@include('includes.script_boolean', ['campo'=> 'reclamar_total', 'checkbox'=>1, 'checked'=>0 ])
@endpush