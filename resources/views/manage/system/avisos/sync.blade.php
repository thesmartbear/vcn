@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.avisos.index', 'Sincronizar Mailchimp', 'manage.system.avisos.sync') !!}
@stop

@section('titulo')
    <i class="fa fa-list-alt fa-fw"></i> Sincronizar
@stop

@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list-alt fa-fw"></i> Mailchimp
    </div>
    <div class="panel-body">

        {!! Form::open(array('method' => 'POST', 'url' => route('manage.system.avisos.sync'), 'role' => 'form', 'class' => '')) !!}

            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'mc_api', 'texto'=> 'API KEY', 'help'=> "En blanco coge la configurada (BS)"])
                </div>
            </div>

            @include('includes.form_submit', [ 'permiso'=> 'avisos', 'texto'=> 'Sincronizar'])
        {!! Form::close() !!}

    </div>

</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-history fa-fw"></i> Historial
    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
                'fecha'     => 'Fecha',
                'notas'     => 'Log',
                'cleaned'     => 'Cleaned',
                'unsubscribed'     => 'Unsuscribed'
            ])
            ->setUrl(route('manage.system.avisos.sync.logs'))
            ->setOptions("order", [[ 0, "desc" ]])
            // ->setOptions(
            //     "aoColumnDefs", array(
            //         [ "targets" => [0],
            //             "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
            //     )
            // )
            ->render() !!}

    </div>

</div>
@stop