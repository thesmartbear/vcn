@extends('layouts.email')

<?php

$idioma = $booking->viajero->idioma_contacto;

$asunto = "---";
$contenido = "no hay doc (Sistema antiguo)";
$pc = 0.30;
if($aviso->doc)
{
    $asunto = $aviso->doc->getCampo('titulo',$idioma);
    $contenido = $aviso->doc->getCampo('contenido',$idioma);
    // $pc = floatval($aviso->doc->notapago_porcentaje) /100;

    $doc = $aviso->doc;

    $importe = $booking->precio_sin_cancelacion;
    if($doc->notapago_pagar_tipo==2)
    {
        $importe = $booking->saldo_pendiente;
    }
    elseif($doc->notapago_pagar_tipo==1)
    {
        $importe = number_format( (($importe * $doc->notapago_pagar) / 100), 2);
    }
    else
    {
        $importe = $doc->notapago_pagar;
    }

    $importe = ConfigHelper::parseMoneda($importe);
}

?>

@section('contenido')

Asunto: {!! $asunto !!}
<hr>

@if($tipo_doc==1)

    @include($template, ['intro'=>$contenido, 'booking' => $booking, 'asunto'=> $asunto, 'importe'=> $importe])

@else
    {!! $contenido !!}
@endif

@stop