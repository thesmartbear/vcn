@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.avisos.ficha', $ficha) !!}
@stop

@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-bell fa-fw"></i> Aviso :: {{$ficha->name}}
    </div>
    <div class="panel-body">

        <div class="form-group">
            Cuando: {{ConfigHelper::getAvisoCuandoTipo($ficha->cuando_tipo)}} :: {{$ficha->cuando_detalles}}
        </div>

    Filtro:{{print_r($ficha->filtros,true)}}
    <br>
    Filtro Excluye:{{print_r($ficha->filtros_not,true)}}

    @if($ficha->doc && $ficha->doc->tipo==1)
      <hr>
      @if($ficha->doc->notapago_pagar_tipo==2)
        Aviso Nota de Pago: El envío solo se hacen a los que tienen saldo pendiente.
      @else
        Aviso Nota de Pago: El envío solo se hacen a los que tienen el <b>{{$ficha->doc->notapago_porcentaje}}%</b> pendiente.
      @endif
    @endif    

    </div>
</div>

<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#simulacion-0" aria-controls="simulacion-0" role="tab" data-toggle="tab">Simulación (Todos)</a></li>
    <li role="presentation"><a href="#simulacion-1" aria-controls="simulacion-1" role="tab" data-toggle="tab">Simulación (Nuevos)</a></li>
</ul>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="simulacion-0">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-list fa-fw"></i> Simulación (Todos) [Envíos ({{$ficha->destino}}): {{$ficha->getEmailsCount()}}]
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'booking'       => 'Booking',
                      'status'        => 'Status',
                      'fecha_reserva' => 'Fecha Reserva',
                      'fecha_curso'   => 'Fecha Curso',
                      'convocatoria'  => 'Convocatoria',
                      'categoria'     => 'Categoría',
                      'viajero'       => 'Viajero',
                      'prescriptor'   => 'Prescriptor',
                      'plataforma'    => 'Plataforma',
                      'envios'        => 'Envíos',
                      'preview'       => 'Vista Previa',
                    ])
                    ->setUrl(route('manage.system.avisos.simulacion', [$ficha->id,0]))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                        "aoColumnDefs", array(
                                [ "bSortable" => false, "aTargets" => [9] ],
                                [ "targets" => [2,3], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                            )
                        )
                    ->render() !!}
            </div>
        </div>

    </div>

    <div role="tabpanel" class="tab-pane fade in" id="simulacion-1">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-list fa-fw"></i> Simulación (Nuevos) [Envíos ({{$ficha->destino}}): {{$ficha->getEmailsCount(true)}}]
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'booking'       => 'Booking',
                      'status'        => 'Status',
                      'fecha_reserva' => 'Fecha Reserva',
                      'fecha_curso'   => 'Fecha Curso',
                      'convocatoria'  => 'Convocatoria',
                      'categoria'     => 'Categoría',
                      'viajero'       => 'Viajero',
                      'prescriptor'   => 'Prescriptor',
                      'plataforma'    => 'Plataforma',
                      'envios'        => 'Envíos',
                      'preview'       => 'Vista Previa',
                      ])
                    ->setUrl(route('manage.system.avisos.simulacion', [$ficha->id,1]))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                        "aoColumnDefs", array(
                                [ "bSortable" => false, "aTargets" => [9] ],
                                [ "targets" => [2,3], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                            )
                        )
                    ->render() !!}
            </div>
        </div>

    </div>

</div>

@stop