@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.avisos.index') !!}
@stop

@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-bell fa-fw"></i> Avisos
        <span class="pull-right">
            <a href="{{ route('manage.system.avisos.nuevo',0) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Aviso Bookings</a>
        </span>
    </div>
    <div class="panel-body">


            {!! Datatable::table()
                ->addColumn([
                  'name'            => 'Aviso',
                  'activo'          => 'Activo',
                  'tipo'            => 'Para',
                  'cuando'          => 'Cuando',
                  'doc'             => 'Doc/Tarea',
                  'options'         => ''
                  ])
                ->setUrl(route('manage.system.avisos.index'))
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [5] ]
                    )
                  )
                  ->render() !!}
    </div>
</div>

@stop