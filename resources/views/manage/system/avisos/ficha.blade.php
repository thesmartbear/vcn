@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.avisos.ficha',$ficha) !!}
@stop


@section('container')

<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Aviso</a></li>
    <li role="presentation"><a href="#log" aria-controls="log" role="tab" data-toggle="tab">Historial</a></li>
</ul>

<div class="tab-content">

    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bell fa-fw"></i> Aviso :: {{ConfigHelper::getAvisoTipo($tipo)}} :: {{$ficha->name}}

                <div class="pull-right">
                    <a class="btn btn-sm btn-info" data-label='Listado simulación' href='{{route('manage.system.avisos.simulacion',$ficha->id)}}'><i class='fa fa-list-alt'></i> Simulación</a>
                </div>

            </div>
            <div class="panel-body">

                {!! Form::open(['id'=>'frm_aviso','route' => array('manage.system.avisos.ficha',$ficha->id), 'class' => 'form']) !!}

                {!! Form::hidden('tipo', $tipo) !!}

                <div class="form-group row">
                    <div class="col-md-8">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Aviso', 'required'=> true])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activo'])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_plataforma', ['campo'=> 'plataforma'])
                    </div>

                </div>

                <div class="form-group">
                    @include('includes.form_checkbox', [ 'campo'=> 'filtros_bool', 'texto'=> 'Sin Filtro (Todos)'])
                </div>

                <div id="filtros_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-filter fa-fw"></i> Filtros
                        </div>
                        <div class="panel-body">
                            @include('includes.filtros_multi', ['filtro_fechas'=>false])
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-filter fa-fw"></i> Filtros excepto
                        </div>
                        <div class="panel-body">
                            @include('includes.filtros_not_multi')
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-filter fa-fw"></i> Booking Status (Sólo CronJob)
                        </div>
                        <div class="panel-body">

                            <div class="row">
                            <div class="col-md-2">
                            {!! Form::label('status', 'Status') !!}
                            @include('includes.form_input_cargando',['id'=> 'status-cargando'])
                            <br>
                            {!! Form::select('status', $statuses, explode(',',$valores['status']),
                                array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'data-style'=>'red', 'id'=>'filtro-status', 'name'=> 'status[]'))  !!}
                            </div>
                            </div>

                        </div>
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <label class="radio-inline">
                        {!! Form::radio('cuando_tipo', 'trigger', ($ficha->cuando_tipo=='trigger') ) !!} Trigger
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('cuando_tipo', 'dia', ($ficha->cuando_tipo=='dia') ) !!} CronJob Diario
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('cuando_tipo', 'semana', ($ficha->cuando_tipo=='semana') ) !!} CronJob Semanal
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('cuando_tipo', 'mes', ($ficha->cuando_tipo=='mes')) !!} CronJob Mensual
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('cuando_tipo', 'puntual', ($ficha->cuando_tipo=='puntual')) !!} CronJob Puntual
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('cuando_tipo', 'tarea', ($ficha->cuando_tipo=='tarea')) !!} Trigger Tarea
                    </label>
                </div>

                <div id="cuando_trigger" style="display:none;" class="trigger_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Trigger
                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                @include('includes.form_select', [ 'campo'=> 'cuando_trigger[trigger]', 'valor'=> $ficha->cuando_trigger, 'texto'=> 'Status Booking', 'select'=> $statuses])
                            </div>

                        </div>
                    </div>
                </div>

                <div id="cuando_dia" style="display:none;" class="trigger_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-calendar fa-fw"></i> CronJob Diario
                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <div class="col-md-2">
                                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'cuando_hora[dia]', 'valor'=> $ficha->cuando_hora, 'texto'=> 'Hora'])
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="cuando_semana" style="display:none;" class="trigger_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-calendar fa-fw"></i> CronJob Semanal
                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <div class="col-md-2">
                                    @include('includes.form_select', [ 'campo'=> 'cuando_dia[semana]', 'valor'=> $ficha->cuando_dia, 'texto'=> 'Día de la Semana', 'select'=> ConfigHelper::getDia()])
                                </div>

                                <div class="col-md-2">
                                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'cuando_hora[semana]', 'valor'=> $ficha->cuando_hora, 'texto'=> 'Hora'])
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="cuando_mes" style="display:none;" class="trigger_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-calendar fa-fw"></i> CronJob Mensual

                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <div class="col-md-2">
                                    *cambiar a día del mes
                                    @include('includes.form_select', [ 'campo'=> 'cuando_dia[mes]', 'valor'=> $ficha->cuando_dia, 'texto'=> 'Día', 'select'=> ConfigHelper::getDia()])
                                </div>

                                <div class="col-md-2">
                                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'cuando_hora[mes]', 'valor'=> $ficha->cuando_hora, 'texto'=> 'Hora'])
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="cuando_puntual" style="display:none;" class="trigger_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-clock-o fa-fw"></i> CronJob Puntual
                        </div>
                        <div class="panel-body">

                            <div class="form-group row">
                                <div class="col-md-2">
                                    @include('includes.form_input_datetime', [ 'campo'=> 'cuando_fecha', 'texto'=> 'Fecha', 'valor'=> $ficha->fecha])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'cuando_hora[puntual]', 'valor'=> $ficha->cuando_hora, 'texto'=> 'Hora'])
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="cuando_tarea" style="display:none;" class="trigger_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-clock-o fa-fw"></i> Trigger Tarea
                        </div>
                        <div class="panel-body">

                            <div class="form-group row">
                                <div class="col-md-2">
                                    @include('includes.form_select', [ 'campo'=> 'cuando_trigger[tarea]', 'valor'=> $ficha->cuando_trigger, 'texto'=> 'Status Booking', 'select'=> $statuses]
                                    )
                                </div>

                                <div class="col-md-2">
                                    @include('includes.form_input_number', [ 'campo'=> 'cuando_dia[tarea]', 'valor'=> $ficha->cuando_dia, 'texto'=> 'Días después'])
                                </div>

                                <div class="col-md-2">
                                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'cuando_hora[tarea]', 'valor'=> $ficha->cuando_hora, 'texto'=> 'Hora'])
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-3">
                                    @include('includes.form_select', [ 'campo'=> 'tarea_tipo', 'texto'=> 'Tipo', 'select'=> ConfigHelper::getTipoTarea() ])
                                </div>
                                <div class="col-md-8">
                                    @include('includes.form_input_text', [ 'campo'=> 'tarea', 'texto'=> 'Tarea']
                                    )
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div id='aviso_doc' class="form-group row">
                    <div class="col-md-3">
                        @include('includes.form_select2', ['campo'=> 'doc_id', 'texto'=> 'Documento', 'select'=> $documentos])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_select', ['campo'=> 'destino', 'texto'=> 'Destino', 'select'=> ConfigHelper::getAvisoDestinatario()])
                    </div>
                    <div id="destinos_div" class="col-md-7" style="display:none;">
                        @include('includes.form_input_text', [ 'campo'=> 'destinos', 'texto'=> 'Destinos', 'help'=> 'Direcciones separados por comas'])
                    </div>
                </div>

                <div class="form-group pull-right">
                    {!! Form::submit('Guardar', array( 'id'=> 'aviso_crear', 'class'=> 'btn btn-info')) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>

    <div role="tabpanel" class="tab-pane fade in" id="log">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-history fa-fw"></i> Historial

            </div>
            <div class="panel-body">
                {!! Datatable::table()
                    ->addColumn([
                      'fecha'           => 'Fecha',
                      'hora'            => 'Hora',
                      'duracion'        => 'Duracion',
                      'notas'           => 'Notas',
                      'enviados'        => 'Enviados',
                      'leidos'          => 'Leídos',
                      'bookings'        => 'Bookings',
                      ])
                    ->setUrl(route('manage.system.avisos.log',$ficha->id))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                            [ "bSortable" => false, "aTargets" => [1,2,3,4] ],
                            [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                        )
                      )
                    ->render() !!}
            </div>

        </div>

    </div>

</div>

<script type="text/javascript">
$(document).ready( function() {

    $('#filtros_bool').change( function(e) {
        $('#filtros_div').toggle();
    });

    $("input[name='cuando_tipo']").change( function(e) {
        var $val = $(this).val();

        $(".trigger_div").hide();
        $("#cuando_"+$val).show();

        if($val=="trigger" || $val=="tarea")
        {
            $('#aviso_doc').hide();
        }
        else
        {
            $('#aviso_doc').show();
        }
    });

    $('#cuando_{{$ficha->cuando_tipo}}').show();

    $tipo = "{{$ficha->cuando_tipo}}";
    if($tipo=="trigger" || $tipo=="tarea")
    {
        $('#aviso_doc').hide();
    }

    $('#destino').change( function(e) {
        var $val = $(this).val();
        if($val==='email')
        {
            $('#destinos_div').fadeIn();
        }
        else
        {
            $('#destinos_div').hide();
        }
    });

    // $('#aviso_crear').click( function(e) {
    //     e.preventDefault();

    //     var cuando_tipo = $("input[name='cuando_tipo']").val();
    //     // $("#frm_aviso").submit();
    // });
});
</script>

@stop