@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.avisos.index', 'Listado Comunicaciones', 'manage.system.avisos.listado') !!}
@stop

@section('titulo')
    <i class="fa fa-bell fa-fw"></i> Listado Comunicaciones
@stop

@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

                {!! Form::open(['route' => array('manage.system.avisos.listado'), 'method'=> 'GET', 'class' => 'form']) !!}

                {!! Form::hidden('listado', true) !!}

                <div id="filtros_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-filter fa-fw"></i> Filtros
                        </div>
                        <div class="panel-body">

                            <div class="form-group row">

                                <div class="col-md-2">
                                    {!! Form::label('plataformas', 'Plataforma') !!}
                                    <br>
                                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                                    @include('includes.script_filtros_multi', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'prescriptores'])
                                </div>

                                <div class="col-md-3">
                                    {!! Form::label('oficinas', 'Oficinas') !!}
                                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])
                                    <br>
                                    {{-- {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'id'=>'filtro-oficinas'))  !!} --}}
                                    {!! Form::select('oficinas', $oficinas, ($valores['oficinas']!=""?explode(',',$valores['oficinas']):[]), array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-oficinas', 'name'=> 'oficinas[]'))  !!}

                                    {{-- @include('includes.script_filtros', ['filtro'=> 'oficinas', 'destino'=> 'prescriptores']) --}}
                                </div>

                                <div class="col-md-3">
                                    {!! Form::label('prescriptores', 'Prescriptor') !!}
                                    @include('includes.form_input_cargando',['id'=> 'prescriptores-cargando'])
                                    <br>
                                    {!! Form::select('prescriptores', $prescriptores, $valores['prescriptores'], array('class'=>'form-control', 'data-style'=>'red', 'id'=>'filtro-prescriptores'))  !!}
                                </div>

                                <div class="col-md-1">
                                    {!! Form::label('idioma', 'Idioma') !!}
                                    <br>
                                    {!! Form::select('idioma', ConfigHelper::getIdiomaContacto(), $valores['idiomas'], array('class'=> 'form-control', 'name'=> 'idiomas'))  !!}
                                </div>

                                <div class="col-md-2">
                                    {!! Form::label('categorias', 'Categoría') !!}
                                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                                    <br>
                                    {!! Form::select('categorias', $categorias, ($valores['categorias']!=""?explode(',',$valores['categorias']):[]), array('class'=>'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-categorias', 'name'=> 'categorias[]'))  !!}
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-md-2">
                                    {!! Form::label('fndesde','Desde (F.Nac)') !!}
                                    @include('includes.form_input_datetime', [ 'campo'=> 'fndesde', 'texto'=> null, 'valor'=>''])
                                </div>
                                <div class="col-md-2">
                                    {!! Form::label('fnhasta','Hasta (F.Nac)') !!}
                                    @include('includes.form_input_datetime', [ 'campo'=> 'fnhasta', 'texto'=> null, 'valor'=>''])
                                </div>

                                <div class="col-md-2">
                                    {!! Form::label('any', 'Año (Último booking)') !!}
                                    <br>
                                    {!! Form::select('any', $anys, $valores['anys'], array('class'=>'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-anys', 'name'=> 'anys[]'))  !!}
                                </div>

                                <div class="col-md-1">
                                    {!! Form::label('email', 'Email') !!}<br>
                                    {!! Form::checkbox("email", true, $valores['email']) !!}
                                </div>


                                <div class="col-md-2">
                                    {!! Form::label('booking', 'Filtro') !!}<br>
                                    {{-- {!! Form::checkbox("booking", true, $valores['booking']) !!} --}}
                                    {!! Form::select('booking', [0=>'Solicitudes', 1=> 'Bookings'], $valores['booking'], array('class'=>'form-control', 'data-style'=>'red', 'id'=>'filtro-bookings'))  !!}
                                </div>

                                <div class="col-md-1">
                                {!! Form::label('any', 'Año') !!}
                                <br>
                                {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any'))  !!}
                                </div>

                                <div class="col-md-2">
                                    {!! Form::label('sinbooking', 'Sin Booking') !!}<br>
                                    {!! Form::checkbox("sinbooking", true, $valores['sinbooking']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-2">
                                    {!! Form::label('status', 'Status Booking') !!}
                                    @include('includes.form_input_cargando',['id'=> 'status-cargando'])
                                    <br>
                                    {!! Form::select('status', $statuses, explode(',',$valores['status']),
                                        array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'data-style'=>'red', 'id'=>'filtro-status', 'name'=> 'status[]'))  !!}
                                </div>

                                <div class="col-md-2">
                                    {!! Form::label('status', 'Status Solicitud') !!}
                                    @include('includes.form_input_cargando',['id'=> 'statuss-cargando'])
                                    <br>
                                    {!! Form::select('statuss', $statusess, explode(',',$valores['statuss']),
                                        array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'data-style'=>'red', 'id'=>'filtro-statuss', 'name'=> 'statuss[]'))  !!}
                                </div>

                                {{-- descartado? --}}
                                <div class="col-md-2">
                                    {!! Form::label('motivos', 'Motivo') !!}
                                    @include('includes.form_input_cargando',['id'=> 'motivos-cargando'])
                                    <br>
                                    {!! Form::select('motivos', ConfigHelper::getArchivarMotivo(true), $valores['motivos'], array('class'=> 'form-control', 'id'=>'filtro-motivos', 'name'=> 'motivos'))  !!}
                                </div>

                                <div class="col-md-2">
                                    {!! Form::label('optout', 'Optout') !!}
                                    <br>
                                    {!! Form::select('optout', $optouts, $valores['optout'], array('class'=>'form-control', 'id'=>'filtro-optout', 'name'=> 'optout'))  !!}
                                </div>

                            </div>

                            <hr>

                            <div class="form-group row">
                                <div class="col-md-3">
                                    {!! Form::label('origenes', 'Orígen') !!}
                                    @include('includes.form_input_cargando',['id'=> 'origenes-cargando'])
                                    <br>
                                    {!! Form::select('origenes', $origenes, ($valores['origenes']!=""?explode(',',$valores['origenes']):[]), array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-origenes', 'name'=> 'origenes[]'))  !!}
                                </div>

                                <div class="col-md-3">
                                    {!! Form::label('suborigenes', 'SubOrígen') !!}
                                    @include('includes.form_input_cargando',['id'=> 'suborigenes-cargando'])
                                    <br>
                                    {!! Form::select('suborigenes', $suborigenes, ($valores['suborigenes']!=""?explode(',',$valores['suborigenes']):[]), array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-suborigenes', 'name'=> 'suborigenes[]'))  !!}
                                </div>

                                <div class="col-md-3">
                                    {!! Form::label('suborigenesdet', 'SubOrígen Detalle') !!}
                                    @include('includes.form_input_cargando',['id'=> 'suborigenesdet-cargando'])
                                    <br>
                                    {!! Form::select('suborigenesdet', $suborigenesdet, ($valores['suborigenesdet']!=""?explode(',',$valores['suborigenesdet']):[]), array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-suborigenesdet', 'name'=> 'suborigenesdet[]'))  !!}
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-filter fa-fw"></i> Filtros excepto
                        </div>
                        <div class="panel-body">

                            <div class="form-group row">
                                <div class="col-md-4">
                                    {!! Form::label('not-prescriptores', 'Prescriptor') !!}
                                    @include('includes.form_input_cargando',['id'=> 'not-prescriptores-cargando'])
                                    <br>
                                    {!! Form::select('not-prescriptores', $prescriptores,
                                        ($valoresNot['prescriptores']?explode(',',$valoresNot['prescriptores']):[]),
                                        array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'data-style'=>'red', 'id'=>'filtro-not-prescriptores', 'name'=> 'not-prescriptores[]'))  !!}

                                    @include('includes.script_filtros_multi', ['filtro'=> 'plataformas', 'destino'=> 'not-prescriptores'])
                                    @include('includes.script_filtros_multi', ['filtro'=> 'oficinas', 'destino'=> 'not-prescriptores'])
                                </div>

                                <div class="col-md-2">
                                    {!! Form::label('any', 'Año (Último booking)') !!}
                                    <br>
                                    {!! Form::select('not-any', [0=> "-"] + $anys, $valoresNot['any'], array('class'=>'form-control', 'id'=>'filtro-not-any', 'name'=> 'not-any'))  !!}
                                </div>
                            </div>

                        </div>
                    </div>

                </div>


                <div class="form-group pull-right">
                    {!! Form::submit('Generar', array( 'class'=> 'btn btn-info' ) ) !!}
                </div>

                {!! Form::close() !!}

                <div class="clearfix"></div>

            </div>
        </div>


        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>
                    <span class="caption-subject bold">Listado</span>

                </div>
            </div>
            <div class="portlet-body">
                @if(!$listado)
                    <div class="content">
                        <div class="alert alert-info" role="alert">
                            Seleccione los filtros correspondientes
                        </div>
                    </div>
                @else

                    <div class="content">

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#viajeros" aria-controls="viajeros" role="tab" data-toggle="tab">Viajeros</a></li>
                            <li role="presentation"><a href="#tutores" aria-controls="tutores" role="tab" data-toggle="tab">Tutores</a></li>
                        </ul>

                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane fade in active" id="viajeros">
                                {{-- viajeros: Email / Nombre / Apellidos (1+2) / Edad / Idioma / Móvil --}}

                                <?php
                                    $filtros['list'] = "viajeros";
                                ?>

                                {!! Datatable::table()
                                    ->addColumn([
                                      'email'   => 'Email',
                                      'name'  => 'Nombre',
                                      'lastname'    => 'Apellido1',
                                      'lastname2'   => 'Apellido2',
                                      'edad'    => 'Edad',
                                      'idioma'  => 'Idioma',
                                      'movil'   => 'Móvil',
                                      'optout'  => 'Optout',
                                      'oficina' => 'Oficina'
                                      ])
                                    ->setUrl(route('manage.system.avisos.listado', $filtros))
                                    ->setOptions('iDisplayLength', 100)
                                    ->setOptions(
                                      "aoColumnDefs", array(
                                            //[ "bSortable" => false, "aTargets" => [0] ],
                                        )
                                      )
                                    ->render() !!}
                            </div>

                            <div role="tabpanel" class="tab-pane fade in" id="tutores">
                                {{-- tutores: Email (tutor) / Nombre (tutor) / Apellidos (tutor) / Móvil (tutor) / Edad (viajero) / Idioma / Nombre viajero --}}

                                <?php
                                    $filtros['list'] = "tutores";
                                ?>

                                {!! Datatable::table()
                                    ->addColumn([
                                      'email'   => 'Email',
                                      'name'  => 'Nombre',
                                      'lastname'   => 'Apellidos',
                                      'movil'   => 'Móvil',
                                      'edad'    => 'Edad',
                                      'idioma'  => 'Idioma',
                                      'viajero_nom' => 'Viajero Nombre',
                                      'viajero_ape' => 'Viajero Apellidos',
                                      'optout'  => 'Optout',
                                      'oficina' => 'Oficina'
                                      ])
                                    ->setUrl(route('manage.system.avisos.listado', $filtros))
                                    ->setOptions('iDisplayLength', 100)
                                    ->setOptions(
                                      "aoColumnDefs", array(
                                            //[ "bSortable" => false, "aTargets" => [0] ],
                                        )
                                      )
                                    ->render() !!}
                            </div>

                        </div>

                    </div>

                @endif

            </div>
        </div>

@stop