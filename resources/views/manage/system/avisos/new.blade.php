@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.avisos.nuevo') !!}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bell fa-fw"></i> Nuevo Aviso :: {{ConfigHelper::getAvisoTipo($tipo)}}

            </div>
            <div class="panel-body">

                {!! Form::open(['id'=>'frm_aviso','route' => array('manage.system.avisos.ficha',0), 'class' => 'form']) !!}

                {!! Form::hidden('tipo', $tipo) !!}

                <div class="form-group row">
                    <div class="col-md-8">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Aviso', 'required'=> true])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activo'])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_plataforma', ['campo'=> 'plataforma', 'valor'=> 1])
                    </div>

                </div>

                <div class="form-group">
                    @include('includes.form_checkbox', [ 'campo'=> 'filtros_bool', 'texto'=> 'Sin Filtro (Todos)'])
                </div>

                <div id="filtros_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-filter fa-fw"></i> Filtros
                        </div>
                        <div class="panel-body">
                            @include('includes.filtros_multi', ['filtro_fechas'=>false])
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-filter fa-fw"></i> Filtros excepto
                        </div>
                        <div class="panel-body">
                            @include('includes.filtros_not_multi')
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-filter fa-fw"></i> Booking Status (Sólo CronJob)
                        </div>
                        <div class="panel-body">

                            <div class="row">
                            <div class="col-md-2">
                            {!! Form::label('status', 'Status') !!}
                            @include('includes.form_input_cargando',['id'=> 'status-cargando'])
                            <br>
                            {!! Form::select('status', $statuses, [],
                                array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'data-style'=>'red', 'id'=>'filtro-status', 'name'=> 'status[]'))  !!}
                            </div>
                            </div>

                        </div>
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <label class="radio-inline">
                        {!! Form::radio('cuando_tipo', 'trigger', true) !!} Trigger
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('cuando_tipo', 'dia', false) !!} CronJob Diario
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('cuando_tipo', 'semana', false) !!} CronJob Semanal
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('cuando_tipo', 'mes', false) !!} CronJob Mensual
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('cuando_tipo', 'puntual', false) !!} CronJob Puntual
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('cuando_tipo', 'tarea', false) !!} Trigger Tarea
                    </label>
                </div>

                <div id="cuando_trigger" class="trigger_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Trigger
                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                @include('includes.form_select', [ 'campo'=> 'cuando_trigger[trigger]', 'texto'=> 'Status Booking', 'select'=> $statuses])
                            </div>

                        </div>
                    </div>
                </div>

                <div id="cuando_dia" style="display:none;" class="trigger_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-calendar fa-fw"></i> CronJob Diario
                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <div class="col-md-2">
                                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'cuando_hora[dia]', 'texto'=> 'Status Booking', 'select'=> $statuses])
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="cuando_semana" style="display:none;" class="trigger_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-calendar fa-fw"></i> CronJob Semanal
                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <div class="col-md-2">
                                    @include('includes.form_select', [ 'campo'=> 'cuando_dia[semana]', 'texto'=> 'Status Booking', 'select'=> $statuses])
                                </div>

                                <div class="col-md-2">
                                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'cuando_hora[semana]', 'texto'=> 'Hora', 'valor'=>''])
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="cuando_mes" style="display:none;" class="trigger_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-calendar fa-fw"></i> CronJob Mensual

                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <div class="col-md-2">
                                    @include('includes.form_select', [ 'campo'=> 'cuando_dia[mes]', 'texto'=> 'Día', 'valor'=>'', 'select'=> ConfigHelper::getDia()])
                                </div>

                                <div class="col-md-2">
                                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'cuando_hora[mes]', 'texto'=> 'Status Booking', 'select'=> $statuses])
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="cuando_puntual" style="display:none;" class="trigger_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-clock-o fa-fw"></i> CronJob Puntual
                        </div>
                        <div class="panel-body">

                            <div class="form-group row">
                                <div class="col-md-2">
                                    @include('includes.form_input_datetime', [ 'campo'=> 'cuando_fecha', 'texto'=> 'Fecha', 'valor'=>''])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'cuando_hora[puntual]', 'texto'=> 'Hora', 'select'=> $statuses])
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="cuando_tarea" style="display:none;" class="trigger_div">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-clock-o fa-fw"></i> Trigger Tarea
                        </div>
                        <div class="panel-body">

                            <div class="form-group row">
                                <div class="col-md-2">
                                    @include('includes.form_select', [ 'campo'=> 'cuando_trigger[tarea]', 'texto'=> 'Status Booking', 'select'=> $statuses])
                                </div>

                                <div class="col-md-2">
                                    @include('includes.form_input_number', [ 'campo'=> 'cuando_dia[tarea]', 'texto'=> 'Días después', 'select'=> $statuses])
                                </div>

                                <div class="col-md-2">
                                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'cuando_hora[tarea]', 'texto'=> 'Hora', 'select'=> $statuses])
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-3">
                                    @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'valor'=> 0, 'select'=> ConfigHelper::getTipoTarea() ])
                                </div>
                                <div class="col-md-8">
                                    @include('includes.form_input_text', [ 'campo'=> 'tarea', 'texto'=> 'Tarea'])
                                </div>
                            </div>

                        </div>
                    </div>
                </div>



                <div id="aviso_doc" class="form-group row">
                    <div class="col-md-3">
                        @include('includes.form_select2', ['campo'=> 'doc_id', 'texto'=> 'Documento', 'select'=> $documentos])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_select', ['campo'=> 'destino', 'texto'=> 'Destino', 'select'=> ConfigHelper::getAvisoDestinatario()])
                    </div>
                    <div id="destinos_div" class="col-md-7" style="display:none;">
                        @include('includes.form_input_text', [ 'campo'=> 'destinos', 'texto'=> 'Destinos', 'help'=> 'Direcciones separados por comas'])
                    </div>
                </div>

                <div class="form-group pull-right">
                    {!! Form::submit('Crear', array( 'id'=> 'aviso_crear', 'class'=> 'btn btn-danger')) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>

<script type="text/javascript">
$(document).ready( function() {

    $('#filtros_bool').change( function(e) {
        $('#filtros_div').toggle();
    });

    $("input[name='cuando_tipo']").change( function(e) {
        var $val = $(this).val();

        $(".trigger_div").hide();
        $("#cuando_"+$val).show();

        if($val=="trigger" || $val=="tarea")
        {
            $('#aviso_doc').hide();
        }
        else
        {
            $('#aviso_doc').show();
        }
    });

    $('#destino').change( function(e) {
        var $val = $(this).val();
        if($val==='email')
        {
            $('#destinos_div').fadeIn();
        }
        else
        {
            $('#destinos_div').hide();
        }
    });

    // $('#aviso_crear').click( function(e) {
    //     e.preventDefault();

    //     var cuando_tipo = $("input[name='cuando_tipo']").val();
    //     // $("#frm_aviso").submit();
    // });
});
</script>

@stop