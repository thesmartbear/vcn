@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.campos.ficha', $ficha) !!}
@stop


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-user-secret fa-fw"></i> Campo :: {{$ficha->name}}
    </div>
    <div class="panel-body">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Campo</a></li>
            @if($ficha->traducible)
                <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
            @endif
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                {!! Form::open(array('method' => 'POST', 'files'=> true, 'url' => route('manage.system.campos.ficha',$ficha->id), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre', 'disabled'=> true])
                        </div>
                        <div class="col-md-6">
                            @include('includes.form_input_text', [ 'campo'=> 'modelo', 'texto'=> 'Modelo', 'disabled'=> true])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'select'=> ConfigHelper::getTipoCampo(), 'disabled'=> true])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'nombre', 'texto'=> 'Título'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'descripcion', 'texto'=> 'Descripcion'])
                    </div>

                    @if($ficha->tipo)
                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'default', 'texto'=> 'Valor por Defecto'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'textarea', 'texto'=> 'Input', 'select'=> [0=>'Texto', 1=> 'Textarea']])
                        </div>
                    </div>
                    @endif

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'traducible', 'texto'=> "Traducible"])
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'full-admin', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>

            @if($ficha->traducible)
            <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                @include('includes.traduccion-tab',
                        ['modelo'=> 'Campo',
                        'campos_text'=> [
                            ['nombre'=> 'Título']
                        ],
                        'campos_textarea'=> [
                            ['descripcion'=> 'Descripcion'],
                        ]
                    ])

            </div>
            @endif

        </div>

    </div>

</div>

@stop