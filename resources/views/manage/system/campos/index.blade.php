@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.system.campos.index') !!}
@stop

@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-tags fa-fw"></i> Campos
    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
                'name'      => 'Campo',
                'nombre'    => 'Título',
                'modelo'    => 'Modelo',
                'tipo'      => 'Tipo',
                'input'     => 'Input'
            ])
            ->setUrl(route('manage.system.campos.index'))
            ->setOptions(
                "aoColumnDefs", array(
                    //[ "bSortable" => false, "aTargets" => [] ]
                )
            )
            ->render() !!}

        <hr>
        <blockquote>
            <p>Los Campos nuevos se dan de alta por el SuperAdmin.</p>
        </blockquote>

    </div>
</div>

@stop