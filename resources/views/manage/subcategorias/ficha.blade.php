@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Sub-Categoria :: {{$ficha->name}} [Categoría :: <a href="{{ route('manage.categorias.ficha', $ficha->category_id) }}">{{$ficha->categoria->name }}</a>]
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Sub-Categoría</a></li>
                    <li role="presentation"><a data-label="Documentos" href="#docs" aria-controls="docs" role="tab" data-toggle="tab"><i class="fa fa-paperclip fa-fw"></i></a></li>
                    <li role="presentation"><a data-label="Cuestionarios" href="#cuestionarios" aria-controls="cuestionarios" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>
                    <li role="presentation"><a data-label="Tests" href="#tests" aria-controls="tests" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>
                    <li role="presentation"><a href="#condiciones" aria-controls="condiciones" role="tab" data-toggle="tab">Condiciones</a></li>
                    <li role="presentation"><a href="#pdf_cancelacion" aria-controls="pdf_cancelacion" role="tab" data-toggle="tab"> Cond. Cancelación</a></li>
                    <li role="presentation"><a href="#doc_especificos" aria-controls="doc_especificos" role="tab" data-toggle="tab"> Doc. Específicos</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">
                        {!! Form::model($ficha, array('route' => array('manage.subcategorias.ficha', $ficha->id)) )!!}

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'category_id', 'texto'=> 'Categoría', 'valor'=> $ficha->category_id, 'select'=> $categorias])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Subcategoría'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name_web', 'texto'=> 'Nombre web'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'slug', 'texto'=> 'Url SEO'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'descripcion', 'texto'=> 'Descripción'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                @include('includes.form_checkbox', [ 'campo'=> 'web_pie', 'texto'=> 'Nota pie de curso frontend'])
                            </div>
                            <div class="col-md-8">
                                @include('includes.form_textarea_tinymce', [ 'campo'=> "web_pie_txt", 'texto'=> 'Texto pie'])
                            </div>
                        </div>

                        @include('includes.form_booking_reserva')

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'contable', 'texto'=> 'Prefijo contable'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'no_facturar', 'texto'=> 'No facturar por sistema'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'pocket', 'texto'=> 'Pocket Guide'])
                            </div>
                            <div class="col-md-2">
                                @if($ficha->categoria)
                                    (Categoría: {{$ficha->categoria->pocket?"SI":"NO"}})
                                @endif
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_select', [ 'campo'=> 'es_aviso_foto', 'texto'=> 'Aviso foto', 'select'=> [null=>'',0=>'No',1=>'Si']])
                            </div>
                        </div>

                        <hr>

                        @include('includes.promo_cambio_fijo', ['ficha'=> $ficha ])

                        <hr>
                        <div class="form-group row">
                            <div class="col-md-4">
                                {!! Form::label('avisos', 'Avisos de nuevos Bookings') !!}
                                <br>
                                {!! Form::select('avisos', $asignados, $ficha->avisos, array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'avisos[]'))  !!}
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('avisos_online', 'Avisos de nuevos Bookings Online') !!}
                                <br>
                                {!! Form::select('avisos_online', $asignados,$ficha->avisos_online, array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'avisos_online[]'))  !!}
                            </div>

                            <div class="col-md-4">
                                {!! Form::label('avisos_datos', 'Avisos de Datos área') !!}
                                <br>
                                {!! Form::select('avisos_datos', $asignados, $ficha->avisos_datos, array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'avisos_datos[]'))  !!}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                {!! Form::label('avisos_doc', 'Avisos de Documentos área') !!}
                                <br>
                                {!! Form::select('avisos_doc', $asignados, $ficha->avisos_doc, array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'avisos_doc[]'))  !!}
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('avisos_cursosweb', 'Aviso nuevos leads ficha curso') !!}
                                <br>
                                {!! Form::select('avisos_cursosweb', $asignados, $ficha->avisos_cursosweb, array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'avisos_cursosweb[]'))  !!}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                {!! Form::label('avisos_catalogo', 'Avisos envío catálogo') !!}
                                <br>
                                {!! Form::select('avisos_catalogo', $asignados, $ficha->avisos_catalogo, array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'avisos_catalogo[]'))  !!}
                            </div>
                        </div>

                        <hr>

                        @include('includes.form_submit', [ 'permiso'=> 'system', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="docs">
                        @include('includes.documentos', ['modelo'=> 'Subcategoria', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="cuestionarios">
                        @include('manage.system.cuestionarios.vinculado', ['modelo'=> 'Subcategoria', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="tests">
                        @include('manage.exams.vinculado', ['modelo'=> 'Subcategoria', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="condiciones">
                        {!! Form::model($ficha, array('route' => array('manage.subcategorias.ficha', $ficha->id)) )!!}

                        {!! Form::hidden('condiciones', 'condiciones') !!}

                        @foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)

                            @foreach(ConfigHelper::idiomas() as $keyi=>$idioma)

                                <?php

                                    $dir = "assets/uploads/pdf_condiciones/";
                                    $name = class_basename($ficha) ."_". $ficha->id;
                                    $file = $dir. $name ."_". $keyp ."_". $idioma .".pdf";

                                    $valor = null;

                                    if(isset($ficha->condiciones[$keyp][$idioma]))
                                    {
                                        $valor = $ficha->condiciones[$keyp][$idioma];
                                    }
                                ?>

                                <div class="form-group">
                                    @include('includes.form_textarea_tinymce', [ 'campo'=> "condiciones_$keyp-$idioma", 'texto'=> "Condiciones ($plataforma : $idioma)", 'valor'=> $valor])
                                    
                                    @if($valor)
                                        Ver: <a target="_blank" href="/{{$file}}">PDF</a>
                                    @endif

                                </div>

                            @endforeach

                        @endforeach

                        @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="pdf_cancelacion">
                        {!! Form::model($ficha, array('route' => array('manage.subcategorias.ficha', $ficha->id), 'files'=> true ))!!}

                        {!! Form::hidden('pdf_cancelacion', true) !!}

                        @foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)

                            @foreach(ConfigHelper::idiomas() as $keyi=>$idioma)

                                <?php

                                $valor = null;
                                if(isset($ficha->pdf_cancelacion[$keyp][$idioma]))
                                {
                                    $valor = $ficha->pdf_cancelacion[$keyp][$idioma];
                                }
                                ?>

                                <div class="form-group">
                                    @include('includes.form_input_file', [ 'campo'=> "pdf_$keyp-$idioma", 'texto'=> "PDF ($plataforma : $idioma)", 'valor'=> $valor])

                                    @if($valor)
                                        Ver: <a target="_blank" href="/{{$valor}}">PDF</a>
                                    @endif

                                </div>

                            @endforeach

                        @endforeach

                        @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="doc_especificos">
                        @include('includes.system_doc_especificos', ['modelo'=> 'Subcategoria', 'modelo_id'=> $ficha->id, 'noProveedor'=> true])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'Subcategoria',
                                'campos_text'=> [
                                    ['name'=> 'Nombre'],
                                    ['name_web'=> 'Nombre web'],
                                    ['slug'=> 'Url SEO'],
                                ],
                                'campos_textarea'=> [
                                    ['descripcion'=> 'Descripcion'],
                                ]
                            ])

                    </div>

                </div>

            </div>
        </div>

@stop