@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tag fa-fw"></i> Sub-Categorias
                <span class="pull-right"><a href="{{ route('manage.subcategorias.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Sub-Categoría</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Subcategoría',
                      'categoria'       => 'Categoría',
                      'avisos'  => 'Aviso Bookings',
                      'avisos_online'  => 'Aviso Bookings Online',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.subcategorias.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [4] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop