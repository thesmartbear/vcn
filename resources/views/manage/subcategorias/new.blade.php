@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Nueva Sub-Categoría
            </div>
            <div class="panel-body">


                {!! Form::open(array('method' => 'POST', 'url' => route('manage.subcategorias.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'category_id', 'texto'=> 'Categoría', 'valor'=> 0, 'select'=> $categorias])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'SubCategoría'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name_web', 'texto'=> 'Nombre web'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'slug', 'texto'=> 'Url SEO'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'descripcion', 'texto'=> 'Descripción'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_checkbox', [ 'campo'=> 'web_pie', 'texto'=> 'Nota pie de curso frontend'])
                        </div>
                        <div class="col-md-8">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> "web_pie_txt", 'texto'=> 'Texto pie'])
                        </div>
                    </div>

                    @include('includes.form_booking_reserva')

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'contable', 'texto'=> 'Prefijo contable'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'no_facturar', 'texto'=> 'No facturar por sistema'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'pocket', 'texto'=> 'Pocket Guide'])
                        </div>
                    </div>

                    <hr>
                    <div class="form-group row">
                        <div class="col-md-4">
                            {!! Form::label('avisos', 'Avisos de nuevos Bookings') !!}
                            <br>
                            {!! Form::select('avisos', $asignados, [], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'avisos[]'))  !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('avisos_online', 'Avisos de nuevos Bookings Online') !!}
                            <br>
                            {!! Form::select('avisos_online', $asignados, [], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'avisos_online[]'))  !!}
                        </div>

                        <div class="col-md-4">
                            {!! Form::label('avisos_datos', 'Avisos de Datos área') !!}
                            <br>
                            {!! Form::select('avisos_datos', $asignados, [], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'avisos_datos[]'))  !!}
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            {!! Form::label('avisos_doc', 'Avisos de Documentos área') !!}
                            <br>
                            {!! Form::select('avisos_doc', $asignados, [], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'avisos_doc[]'))  !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('avisos_cursosweb', 'Aviso nuevos leads ficha curso') !!}
                            <br>
                            {!! Form::select('avisos_cursosweb', $asignados, [], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'avisos_cursosweb[]'))  !!}
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            {!! Form::label('avisos_catalogo', 'Avisos envío catálogo') !!}
                            <br>
                            {!! Form::select('avisos_catalogo', $asignados, [], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'avisos_catalogo[]'))  !!}
                        </div>
                    </div>

                    <hr>

                    @include('includes.form_submit', [ 'permiso'=> 'system', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop