@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-book fa-fw"></i>
                    <a href="{{route('manage.cursos.ficha',$ficha->course_id)}}#extras">
                    Curso: ({{$ficha->curso->course_name}})
                    </a>
                    Extra :: {{$ficha->course_extras_name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Extra</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('route' => array('manage.cursos.extras.ficha', $ficha->id))) !!}

                        {!! Form::hidden('course_id', $ficha->course_id) !!}

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'course_extras_name', 'texto'=> 'Nombre extra'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea', [ 'campo'=> 'course_extras_description', 'texto'=> 'Descripción'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'course_extras_unit', 'texto'=> 'Unidad', 'valor'=> $ficha->course_extras_unit, 'select'=> $unidades_tipo])
                        </div>

                        <div id="course_extras_unit_div" class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'course_extras_unit_id', 'texto'=> 'Tipo Unidad', 'valor'=> $ficha->course_extras_unit_id, 'select'=> $unidades])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'course_extras_currency_id', 'texto'=> 'Moneda', 'valor'=> $ficha->course_extras_currency_id, 'select'=> $monedas])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'course_extras_price', 'texto'=> 'Precio'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'course_extras_required', 'texto'=> 'Obligatorio'])
                        </div>

                        @include('includes.form_submit', [ 'permiso'=> 'extras', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'CursoExtra',
                                'campos_text'=> [ ['course_extras_name'=> 'Nombre'], ],
                                'campos_textarea'=> [ ['course_extras_description'=> 'Descripción'], ]
                            ])

                    </div>

                </div>

            </div>
        </div>

@include('includes.script_unidades', ['div'=>'#course_extras_unit' ])

@stop