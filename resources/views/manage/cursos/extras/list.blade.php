<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-book"></i> Curso Extras

            <span class="pull-right"><a href="{{ route('manage.cursos.extras.nuevo', $curso_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Curso Extra</a></span>

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'        => 'Nombre',
                  'precio'      => 'Precio',
                  'unidad'      => 'Unidad',
                  'requerido'      => 'Obligatorio',
                  'curso'      => 'Curso',
                  'options' => ''

                ])
                ->setUrl( route('manage.cursos.extras.index', $curso_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [5] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>