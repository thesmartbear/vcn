@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bed fa-fw"></i>
                    @if($curso)
                        <a href="{{route('manage.cursos.ficha',$curso->id)}}#extras">
                            Curso: ({{$curso->name}})
                        </a>
                    @else
                        Curso: (Seleccionar)
                    @endif
                    Extra :: Nuevo
            </div>
            <div class="panel-body">

                {!! Form::open(array('route' => array('manage.cursos.extras.ficha', 0))) !!}

                    @if($curso)
                        {!! Form::hidden('course_id', $curso->id) !!}
                    @else
                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'course_id', 'texto'=> 'Curso', 'valor'=> 0, 'select'=> $cursos])
                        </div>
                    @endif

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'course_extras_name', 'texto'=> 'Nombre extra'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea', [ 'campo'=> 'course_extras_description', 'texto'=> 'Descripción'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'course_extras_unit', 'texto'=> 'Unidad', 'valor'=> 0, 'select'=> $unidades_tipo])
                    </div>

                    <div id="course_extras_unit_div" class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'course_extras_unit_id', 'texto'=> 'Tipo Unidad', 'valor'=> 0, 'select'=> $unidades])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'course_extras_currency_id', 'texto'=> 'Moneda', 'valor'=> ($curso?$curso->centro->moneda_id:0), 'select'=> $monedas])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'course_extras_price', 'texto'=> 'Precio'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'course_extras_required', 'texto'=> 'Obligatorio'])
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'extras', 'texto'=> 'Guardar'])

                    <div class="clearfix"></div>

                {!! Form::close() !!}


            </div>
        </div>

@include('includes.script_unidades', ['div'=>'#course_extras_unit' ])

@stop