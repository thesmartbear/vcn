@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.cursos.nuevo', $centro) !!}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-truck fa-fw"></i> Nuevo Curso
            </div>
            <div class="panel-body">

                {!! Form::open(array('files'=> true, 'method' => 'POST', 'url' => route('manage.cursos.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="well bg-success">
                        <div class="row">
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'course_active', 'texto'=> 'Curso Activo'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'activo_web', 'texto'=> 'Activo Web'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'course_highlight', 'texto'=> 'Curso Destacado'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'activo_directo', 'texto'=> 'Booking directo'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_select', [ 'campo'=> 'es_aviso_foto', 'texto'=> 'Aviso foto', 'select'=> [null=>'',0=>'No',1=>'Si']])
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                @include('includes.form_input_number', [ 'campo'=> 'duracion_web_min', 'texto'=> 'Duración web semanas (mín)'])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_input_number', [ 'campo'=> 'duracion_web_max', 'texto'=> 'Duración web semanas (máx)'])
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'center_id', 'texto'=> 'Centro', 'valor'=> $centro_id, 'select'=> $centros])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'course_name', 'texto'=> 'Nombre Curso'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-8">
                            @include('includes.form_input_text', [ 'campo'=> 'subtitulo', 'texto'=> 'Subtítulo'])
                        </div>
                        <div class="col-md-4">
                        @include('includes.form_input_file', [ 'campo'=> 'icono', 'texto'=> 'Icono'])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_select_multi', [ 'campo'=> 'course_accommodation', 'texto'=> 'Alojamientos', 'valor'=> [], 'select'=> $alojamientos])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'category_id', 'texto'=> 'Categoría', 'select'=> $categorias, 'required'=> true])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'subcategory_id', 'texto'=> 'Subcategoría', 'select'=> $subcategorias, 'required'=> true])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'subcategory_det_id', 'texto'=> 'Detalle Subcategoría', 'select'=> $subcategorias_det])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_select_multi', [ 'campo'=> 'course_language', 'texto'=> 'Idiomas Requeridos para este curso *', 'valor'=> [], 'select'=> $idiomas, 'help'=> '', 'required'=> true])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'course_minimun_language', 'texto'=> 'Nivel Mínimo de Idioma', 'valor'=> 0, 'select'=> $idioma_niveles])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_number', [ 'campo'=> 'course_language_sessions', 'texto'=> 'Número de Sesiones de Idioma'])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'course_age', 'texto'=> 'Edad Números',
                        'help'=> "(entra las edades en numeros separadas por coma. No puede haber un caracter que no sea un numero o una coma.)"])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'course_age_range', 'texto'=> 'Edad Rangos',
                        'help'=> "(entra los numeros como quieres que los vea el cliente. Por ejemplo +18 o 12-16)"])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'commission', 'texto'=> 'Comisión', 'help'=> "En blanco cogerá la del centro"])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'course_summary', 'texto'=> 'Resumen',
                        'help'=> "(este texto es lo que aparece como highlight del curso en el listado de cursos. Tiene que ser algo atractivo y catchy para el cliente)"])
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'es_convocatoria_multi', 'texto'=> 'Multiconvocatoria'])
                    </div>

                    <div class="form-group preciosyfechas">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'preciosyfechas', 'texto'=> 'Precios y Fechas (Multiconvocatoria)'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'course_promo', 'texto'=> 'Curso Promoción'])
                    </div>

                    <div class="form-group promo_texto">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'promo_texto', 'texto'=> 'Texto Promo'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'frase', 'texto'=> 'Frase Destacada'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'course_content', 'texto'=> 'Contenido', 'help'=> 'Las imágenes no deben superar los 500px de ancho'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'course_activities', 'texto'=> 'Actividades',
                            'help'=> 'Entrar información SÓLO si las actividades son diferentes a las entradas en Centro. Sino dejar vacío.'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'course_excursions', 'texto'=> 'Excursiones',
                            'help'=> 'Entrar información SÓLO si las excursiones son diferentes a las entradas en Centro. Sino dejar vacío.'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'course_provider_url', 'texto'=> 'URL Proveedor'])
                    </div>

                    @include('includes.form_booking_reserva')

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'course_video', 'texto'=> 'Vídeo', 'help'=> "poner la url completa del video en Youtube, la que empieza por https://www.youtube.com/watch?v="])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'rs_pinterest', 'texto'=> 'Pinterest'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'ganalytics', 'texto'=> 'Google Analytics (Track)'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'instagram_token', 'texto'=> 'IG Token'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'instagram_user', 'texto'=> 'IG user'])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'requisitos', 'texto'=> 'Requisitos especiales' ])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea', [ 'campo'=> 'video', 'texto'=> 'Embed video' ])
                    </div>

                    <hr>

                    {{-- <div class="form-group row">
                        <div class="col-md-8">
                            @include('includes.form_input_text', [ 'campo'=> 'monitor_name', 'texto'=> 'Monitor'])
                        </div>
                        <div class="col-md-4">
                        @include('includes.form_input_file', [ 'campo'=> 'monitor_foto', 'texto'=> 'Monitor foto'])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'monitor_desc', 'texto'=> 'Descripción Monitor' ])
                    </div> --}}

                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_select_multi', [ 'campo'=> 'monitores', 'texto'=> 'Monitores', 'valor'=> [], 'select'=> \VCN\Models\Monitores\Monitor::plataforma()->pluck('name','id')->toArray() ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'monitor_web', 'texto'=> 'Mostrar en Web'])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'pocket', 'texto'=> 'Pocket Guide'])
                        </div>
                    </div>

                    @include('includes.form_plataforma', ['campo'=> 'propietario', 'todas'=> true])

                    {{--'course_testimonials_parents', 'course_testimonials_campers' --}}

                    @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@if($centro_id>0)
<script type="text/javascript">
$(function() {
    $("#center_id").attr('readonly',true);
});
</script>
@endif


<script type="text/javascript">
    $(function() {

        $('#es_convocatoria_multi').on('change', function(){
             console.log('convocatoria_multi');
            $('.preciosyfechas').slideToggle();
        });

        if($('#es_convocatoria_multi').prop('checked') == true){
            $('.preciosyfechas').show();
        }else{
            $('.preciosyfechas').hide();
        }

        $('#course_promo').on('change', function(){
            console.log('promo');
            $('.promo_texto').slideToggle();
        });

        if($('#course_promo').prop('checked') == true){
            $('.promo_texto').show();
        }else{
            $('.promo_texto').hide();
        }
    });
</script>

{{-- @include('includes.script_categoria_multi', ['url'=> route('manage.subcategorias.json.multi')] ) --}}
@include('includes.script_categoria')
@include('includes.script_alojamientos_curso', ['url'=> route('manage.alojamientos.json')] )
@include('includes.script_especialidad')

@stop