<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-book fa-fw"></i> Cursos
            <span class="pull-right"><a href="{{ route('manage.cursos.nuevo', $centro_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Curso</a></span>
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'            => 'Nombre',
                  'proveedor'       => 'Proveedor',
                  'centro'          => 'Centro',
                  'convocatoria'          => 'Convocatoria',
                  'options'         => ''
                ])
                ->setUrl(route('manage.cursos.index', $centro_id))
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [4] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>
