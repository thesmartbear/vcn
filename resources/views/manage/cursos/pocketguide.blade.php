<div class="row" style="max-width: 21cm;">
    <div class="col-xs-12 pull-right text-right hidden-print">
        @if(isset($lang))
            <?php App::setLocale($lang); ?>
        @else
            <?php $lang = App::getLocale(); ?>
        @endif

        @if(!isset($pdf))
            <a target="_blank" class="btn btn-danger btn-xs hidden-print pull-right" href="{{ route('manage.cursos.pocket-guide', [$ficha->id, $lang]) }}"> <i class="fa fa-file-pdf-o"></i> PDF <span class="text-uppercase">{{$lang}}</span> </a>
            <br />
            <hr>
            <br />
        @endif
    </div>

    <div class="">
        <div class="col-xs-6">
            <img style="margin-left: -5px; margin-bottom: 40px; display: block; width: auto; height: 2.5cm;" src="https://{{ConfigHelper::config('web')}}/assets/logos/pocketguide-logo.jpg" />
        </div>
        <div class="col-xs-6">
            <img style="width: 4.5cm; height: auto; margin-top: 50px;" class="pull-right" src="https://{{ConfigHelper::config('web')}}/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" />
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-5">

                <h3 class="text-success">
                    {{Traductor::getWeb(App::getLocale(), 'Curso', 'name', $ficha->id, $ficha->name)}}<br />
                    <small>@if($ficha->centro->pais->name != 'España') {{Traductor::getWeb(App::getLocale(), 'Pais', 'name', $ficha->centro->pais->id, $ficha->centro->pais->name)}} @else {{Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $ficha->centro->ciudad->id, $ficha->centro->ciudad->city_name)}} @endif</small>
                </h3>

                <div id="details">
                    <ul style="list-style: none; margin:0; padding: 0;">
                        <li>
                            <i class="fa fa-user"></i> {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_age_range', $ficha->id, $ficha->course_age_range)!!}
                        </li>
                        <li><i class="fa fa-globe"></i> {{trans('web.'.$ficha->course_language)}}</li>
                        <?php $alojas = array(); ?>
                        @if(count($ficha->alojamientos))
                            @foreach($ficha->alojamientos as $alojamiento)
                                <?php $alojas[] = Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $alojamiento->tipo->id, $alojamiento->tipo->accommodation_type_name); ?>
                            @endforeach

                            @if(count($alojas))
                                @foreach(array_unique($alojas) as $alojatipo)
                                    <li><i class="fa fa-bed"></i> {!!$alojatipo!!}</li>
                                @endforeach
                            @endif
                        @endif
                    </ul>
                </div>
                <hr>
                <p>
                    <b class="titular">{{trans('area.centro')}}</b><br />
                    {!!Traductor::getWeb(App::getLocale(), 'Centro', 'name', $ficha->centro->id, $ficha->centro->name)!!}<br />
                    {!!Traductor::getWeb(App::getLocale(), 'Centro', 'address', $ficha->centro->id, $ficha->centro->address)!!}<br /><br />
                </p>


            </div>
            <div class="col-xs-7">
                @if($ficha->centro->address)
                    <?php
                        $address = str_replace("-", "+", str_slug(html_entity_decode(strip_tags($ficha->centro->name)))).'+'.str_replace(",", "+", str_slug(html_entity_decode(strip_tags($ficha->centro->address))));
                        $address = str_replace("-", "+", $address);
                        $address = str_replace("++", "+", $address);

                        try {

                            $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=".$address."&sensor=false");
                            $json = json_decode($json);

                            if(isset($json->{'results'}[0]))
                            {

                                $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
                                $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
                                $coordinates = $lat.','.$long;
                            }

                        } catch (Exception $e) {

                        }

                    ?>
                    @if(isset($coordinates))
                        <img class="img-responsive" style="vertical-align: top;" src="https://maps.googleapis.com/maps/api/staticmap?center={{$address}}&zoom=8&size=640x450&markers=color:red|{{$coordinates}}&key=AIzaSyCNPWo91Kd3D656Z5R2U6K1vFlx65KxpE8" />
                    @endif
                @endif
            </div>
            <div class="col-xs-12">
                @if($ficha->centro->transport != '')
                    <p><b>{{trans("area.Transporte")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Centro', 'transport', $ficha->centro->id, $ficha->centro->transport) !!}</p>
                @elseif(!isset($pdf))
                    <p class="text-danger"><b class="titular">{{trans("area.Transporte")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Centro', 'transport', $ficha->centro->id, $ficha->centro->transport) !!}</p>
                @endif
            </div>
        </div>

        <hr>
        <h4 class="text-info">{{trans("area.Información práctica")}}</h4>

        @if($ficha->course_minimun_language != '')
            <p><b class="titular">{{trans("area.Nivel mínimo de idioma")}}:</b> {{trans('web.'.$ficha->course_minimun_language)}}</p>
        @elseif(!isset($pdf))
            <p class="text-danger"><b class="titular">{{trans("area.Nivel mínimo de idioma")}}:</b> {{trans('web.'.$ficha->course_minimun_language)}}</p>
        @endif

        @if($ficha->course_language_sessions != '')
            <p><b class="titular">{{trans("area.Nº de sesiones de idioma")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_language_sessions', $ficha->id, $ficha->course_language_sessions) !!}</p>
        @elseif(!isset($pdf))
            <p class="text-danger"><b class="titular">{{trans("area.Nº de sesiones de idioma")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_language_sessions', $ficha->id, $ficha->course_language_sessions) !!}</p>
        @endif

        @if($ficha->requisitos != '')
            <p><b class="titular">{{trans("area.Requisitos especiales")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Curso', 'requisitos', $ficha->id, $ficha->requisitos) !!}</p>
        @elseif(!isset($pdf))
            <p class="text-danger"><b class="titular">{{trans("area.Requisitos especiales")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Curso', 'requisitos', $ficha->id, $ficha->requisitos) !!}</p>
        @endif

        @if($ficha->centro->internet_uso != '')
            <p><b class="titular">{{trans("area.Uso de internet")}}</b>: {!! Traductor::getWeb(App::getLocale(), 'Centro', 'internet_uso', $ficha->centro->id, $ficha->centro->internet_uso) !!}</p>
        @elseif(!isset($pdf))
            <p class="text-danger"><b class="titular">{{trans("area.Uso de internet")}}</b>: {!! Traductor::getWeb(App::getLocale(), 'Centro', 'internet_uso', $ficha->centro->id, $ficha->centro->internet_uso) !!}</p>
        @endif

        @if($ficha->centro->lavanderia != '')
            <p><b class="titular">{{trans("area.Lavandería")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Centro', 'lavanderia', $ficha->centro->id, $ficha->centro->lavanderia) !!}</p>
        @elseif(!isset($pdf))
            <p class="text-danger"><b class="titular">{{trans("area.Lavandería")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Centro', 'lavanderia', $ficha->centro->id, $ficha->centro->lavanderia) !!}</p>
        @endif

        @if($ficha->centro->foods != '')
            <p><b class="titular">{{trans("area.Comidas")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Centro', 'food', $ficha->centro->id, $ficha->centro->foods) !!}</p>
        @elseif(!isset($pdf))
            <p class="text-danger"><b class="titular">{{trans("area.Comidas")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Centro', 'food', $ficha->centro->id, $ficha->centro->foods) !!}</p>
        @endif


        @if($ficha->alojamientos)
            <hr>
            <h4 class="text-info">{{trans("area.alojamiento")}}</h4>
            @foreach($ficha->alojamientos as $alojamiento)
                <h5 class="text-success"><b>{!! Traductor::getWeb(App::getLocale(), 'Alojamiento', 'accommodation_name', $alojamiento->id, $alojamiento->accommodation_name) !!}</b></h5>
                {!! Traductor::getWeb(App::getLocale(), 'Alojamiento', 'accommodation_description', $alojamiento->id, $alojamiento->accommodation_description) !!}
                @if($alojamiento->requisitos != '')
                    <p><b>{{trans("area.Requisitos especiales")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Alojamiento', 'requisitos', $alojamiento->id, $alojamiento->requisitos) !!}</p>
                @elseif(!isset($pdf))
                    <p class="text-danger"><b>{{trans("area.Requisitos especiales")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Alojamiento', 'requisitos', $alojamiento->id, $alojamiento->requisitos) !!}</p>
                @endif
                @if($alojamiento->tipo->notas != '')
                    <p><b>{{trans("area.notas")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Alojamiento', 'notas', $alojamiento->tipo->id, $alojamiento->tipo->notas) !!}</p>
                @elseif(!isset($pdf))
                    <p class="text-danger"><b>{{trans("area.notas")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Alojamiento', 'notas', $alojamiento->tipo->id, $alojamiento->tipo->notas) !!}</p>
                @endif
            @endforeach
        @endif


        <hr>
        <h4 class="text-info">{{trans("area.El día a día")}}</h4>

        @if($ficha->centro->comment != '')
            <p><b class="titular">{{trans("area.Depósitos")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Centro', 'comment', $ficha->centro->id, $ficha->centro->comment) !!}</p>
        @elseif(!isset($pdf))
            <p class="text-danger"><b class="titular">{{trans("area.Depósitos")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Centro', 'comment', $ficha->centro->id, $ficha->centro->comment) !!}</p>
        @endif


        @if($ficha->pocket_dinero_check)
            <?php
            $v = ConfigHelper::campoValor('pocket_dinero');
            ?>
            <p><b class="titular">{!! Traductor::getWeb(App::getLocale(), 'Campo', 'nombre', $v->id, $v->nombre) !!}:</b> @if($ficha->pocket_dinero != '')<b>{{ ConfigHelper::parseMoneda($ficha->pocket_dinero) }} / {{trans("area.semana")}}</b>@endif</p>
            <p>{!! Traductor::getWeb(App::getLocale(), 'Campo', 'descripcion', $v->id, $v->descripcion) !!}</p>
        @endif

        @if($ficha->pocket_prueba_nivel_check)
            <?php
            $v = ConfigHelper::campoValor('pocket_prueba_nivel');
            ?>
            <p><b class="titular"> {!! Traductor::getWeb(App::getLocale(), 'Campo', 'nombre', $v->id, $v->nombre) !!}: </b> {!! Traductor::getWeb(App::getLocale(), 'Campo', 'descripcion', $v->id, $v->descripcion) !!}</p>
        @endif

        <p>
            <b class="titular">{{trans("area.Tipo cambio día")}}:</b> {{$ficha->centro->moneda->rate}} {{$ficha->centro->moneda_name}}<br />
            <span class="badge badge-help">{{trans("area.Tipo de cambio informativo a día de")}} {{Carbon::now()->format('d/m/Y')}}</span>
        </p>

        @if($ficha->pocket_monitores_check)
            <?php
            $v = ConfigHelper::campoValor('pocket_monitores');
            ?>
            <p><b class="titular">{!! Traductor::getWeb(App::getLocale(), 'Campo', 'nombre', $v->id, $v->nombre) !!}</b>
            <br>
            @if($ficha->pocket_monitores != "")
                {!! Traductor::getWeb(App::getLocale(), 'Curso', 'pocket_monitores', $ficha->id, $ficha->pocket_monitores) !!}</p>
            @else
                {!! Traductor::getWeb(App::getLocale(), 'Campo', 'descripcion', $v->id, $v->descripcion) !!}<br />
            @endif
        @endif

        @if($ficha->material_check)
            <?php
            $v = ConfigHelper::campoValor('material');
            ?>
            <p><b class="titular">{!! Traductor::getWeb(App::getLocale(), 'Campo', 'nombre', $v->id, $v->nombre) !!}</b>
            <br>
            @if($ficha->material != "")
                {!! Traductor::getWeb(App::getLocale(), 'Curso', 'material', $ficha->id, $ficha->material) !!}
            @else
                {!! Traductor::getWeb(App::getLocale(), 'Campo', 'descripcion', $v->id, $v->descripcion) !!}<br />
            @endif
            </p>
        @endif


        @if($ficha->centro->normas != '')
            <p><b class="titular">{{trans("area.Normas de la escuela")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Centro', 'normas', $ficha->centro->id, $ficha->centro->normas) !!}</p>
        @elseif(!isset($pdf))
            <p class="text-danger"><b class="titular">{{trans("area.Normas de la escuela")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Centro', 'normas', $ficha->centro->id, $ficha->centro->normas) !!}</p>
        @endif


        @if($ficha->pocket_normas_check)
            <?php
            $v = ConfigHelper::campoValor('pocket_normas');
            ?>
            <p><b class="titular">{!! Traductor::getWeb(App::getLocale(), 'Campo', 'nombre', $v->id, $v->nombre) !!}:</b> {!! Traductor::getWeb(App::getLocale(), 'Campo', 'descripcion', $v->id, $v->descripcion) !!}</p>
        @endif


        @if($ficha->anterior_participantes_check)
            <?php
            $v = ConfigHelper::campoValor('anterior_participantes');
            ?>
            <p><b class="titular">{!! Traductor::getWeb(App::getLocale(), 'Campo', 'nombre', $v->id, $v->nombre) !!}:</b>  {!! Traductor::getWeb(App::getLocale(), 'Campo', 'descripcion', $v->id, $v->descripcion) !!}</p>
        @endif

        @if($ficha->anterior_nacionalidades_check)
            <?php
            $v = ConfigHelper::campoValor('anterior_nacionalidades');
            ?>
            <p><b class="titular">{!! Traductor::getWeb(App::getLocale(), 'Campo', 'nombre', $v->id, $v->nombre) !!}:</b>  {!! Traductor::getWeb(App::getLocale(), 'Campo', 'descripcion', $v->id, $v->descripcion) !!}</p>
        @endif



        <hr>
        <h4 class="text-info">{{trans("area.Otras cosas que ya sabes")}}</h4>

        @if($ficha->centro->settingup)
            <p><b class="titular">{{trans("area.Instalaciones")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Centro', 'settingup', $ficha->centro->id, $ficha->centro->settingup) !!}</p>
        @elseif(!isset($pdf))
            <p class="text-danger"><b class="titular">{{trans("area.Instalaciones")}}:</b> {!! Traductor::getWeb(App::getLocale(), 'Centro', 'settingup', $ficha->centro->id, $ficha->centro->settingup) !!}</p>
        @endif
        <br />


        @if($ficha->course_activities || $ficha->centro->center_activities)
            <p><b class="titular">{{trans("area.Actividades")}}:</b>
                @if($ficha->course_activities)
                    {!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_activities', $ficha->id, $ficha->course_activities) !!}
                @else
                    {!! Traductor::getWeb(App::getLocale(), 'Centro', 'center_activities', $ficha->centro->id, $ficha->centro->center_activities) !!}
                @endif
            </p>
        @elseif(!isset($pdf))
            <p class="text-danger"><b class="titular">{{trans("area.Actividades")}}:</b>
                @if($ficha->course_activities)
                    {!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_activities', $ficha->id, $ficha->course_activities) !!}
                @else
                    {!! Traductor::getWeb(App::getLocale(), 'Centro', 'center_activities', $ficha->centro->id, $ficha->centro->center_activities) !!}
                @endif
            </p>
        @endif
        <br />

        @if($ficha->course_excursions || $ficha->centro->center_excursions)
            <p><b class="titular">{{trans("area.Excursiones")}}:</b>
                @if($ficha->course_excursions)
                    {!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_excursions', $ficha->id, $ficha->course_excursions) !!}
                @else
                    {!! Traductor::getWeb(App::getLocale(), 'Centro', 'center_excursions', $ficha->centro->id, $ficha->centro->center_excursions) !!}
                @endif</p>
        @elseif(!isset($pdf))
            <p class="text-danger"><b class="titular">{{trans("area.Excursiones")}}:</b>
                @if($ficha->course_excursions)
                    {!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_excursions', $ficha->id, $ficha->course_excursions) !!}
                @else
                    {!! Traductor::getWeb(App::getLocale(), 'Centro', 'center_excursions', $ficha->centro->id, $ficha->centro->center_excursions) !!}
                @endif</p>
        @endif


        @if($ficha->categoria_web)
            <br />
            <hr>
            <h4 class="text-info">{{trans("area.Más información sobre tu programa")}}</h4>
            @if($lang!=ConfigHelper::config('idioma'))
                <p><a href="//{{ConfigHelper::config('web')}}/{{$lang}}/{{Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $ficha->categoria_web->id, $ficha->categoria_web->seo_url)}}/{{$ficha->course_slug}}.html" target="_blank">https://{{ConfigHelper::config('web')}}/{{$lang}}/{{Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $ficha->categoria_web->id, $ficha->categoria_web->seo_url)}}/{{$ficha->course_slug}}.html</a></p>
            @else
                <p><a href="//{{ConfigHelper::config('web')}}/{{Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $ficha->categoria_web->id, $ficha->categoria_web->seo_url)}}/{{$ficha->course_slug}}.html" target="_blank">https://{{ConfigHelper::config('web')}}/{{Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $ficha->categoria_web->id, $ficha->categoria_web->seo_url)}}/{{$ficha->course_slug}}.html</a></p>
            @endif
        @elseif(!isset($pdf))
            -NO HAY CATEGORIAS WEB PARA LA URL-
        @endif

    </div>
</div>