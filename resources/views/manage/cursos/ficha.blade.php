@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.cursos.ficha', $ficha) !!}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-book fa-fw"></i> Curso :: {{$ficha->course_name}}

                @if(ConfigHelper::canEdit('booking-firmas'))
                <div class="col-md-2 pull-right">
                    <a href='{{ route('manage.cursos.firmas', $ficha->id) }}' class='btn btn-xs btn-warning'><i class='fa fa-envelope'></i> Firmas pendientes</a>
                </<div>
                @endif

            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Curso</a></li>
                    <li role="presentation"><a href="#especialidades" aria-controls="especialidades" role="tab" data-toggle="tab">Espec.</a></li>
                    <li role="presentation"><a href="#alojamientos" aria-controls="alojamientos" role="tab" data-toggle="tab">Alojamientos</a></li>

                    <li role="presentation"><a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
                    <li role="presentation"><a href="#imagenes" aria-controls="imagenes" role="tab" data-toggle="tab">Imágenes</a></li>
                    <li role="presentation"><a href="#timetables" aria-controls="timetables" role="tab" data-toggle="tab">Timetables</a></li>
                    <li role="presentation"><a href="#extras" aria-controls="extras" role="tab" data-toggle="tab">Extras</a></li>
                    <li role="presentation"><a href="#extras-genericos" aria-controls="extras-genericos" role="tab" data-toggle="tab">Extras Gen.</a></li>

                    @if($ficha->es_convocatoria_nula)
                        <li role="presentation"><a href="#convocatorias-precio" aria-controls="convocatorias-precio" role="tab" data-toggle="tab">Precios</a></li>
                    @else

                        @if($ficha->es_convocatoria_cerrada)
                        <li role="presentation"><a href="#convocatorias-cerradas" aria-controls="convocatorias-cerradas" role="tab" data-toggle="tab">C.Cerradas</a></li>
                        @endif

                        @if($ficha->es_convocatoria_abierta)
                        <li role="presentation"><a href="#convocatorias-abiertas" aria-controls="convocatorias-abiertas" role="tab" data-toggle="tab">C.Abiertas</a></li>
                        @endif

                        @if($ficha->es_convocatoria_multi)
                        <li role="presentation"><a href="#convocatorias-multi" aria-controls="convocatorias-multi" role="tab" data-toggle="tab">C.Multi</a></li>
                        @endif

                    @endif

                    <li role="presentation"><a data-label="Reuniones" href="#reuniones" aria-controls="reuniones" role="tab" data-toggle="tab"><i class="fa fa-comments"></i></a></li>

                    <li role="presentation"><a data-label="Documentos" href="#docs" aria-controls="docs" role="tab" data-toggle="tab"><i class="fa fa-paperclip fa-fw"></i></a></li>

                    <li role="presentation"><a data-label="Cuestionarios" href="#cuestionarios" aria-controls="cuestionarios" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>
                    <li role="presentation"><a data-label="Tests" href="#tests" aria-controls="tests" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>
                    <li role="presentation"><a data-label="Pocket Guide" href="#pocket_tab" aria-controls="pocket_tab" role="tab" data-toggle="tab"><i class="fa fa-book fa-fw"></i></a></li>

                    <li role="presentation"><a href="#condiciones" aria-controls="condiciones" role="tab" data-toggle="tab"> Condiciones</a></li>
                    <li role="presentation"><a href="#pdf_cancelacion" aria-controls="pdf_cancelacion" role="tab" data-toggle="tab"> Cond. Cancelación</a></li>
                    <li role="presentation"><a href="#doc_especificos" aria-controls="doc_especificos" role="tab" data-toggle="tab"> Doc. Específicos</a></li>

                    <li role="presentation"><a href="#contactos_sos" aria-controls="contactos_sos" role="tab" data-toggle="tab"> Contactos SOS</a></li>

                    <li role="presentation"><a data-label="Traducciones" href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i></a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('files'=>true,'route' => array('manage.cursos.ficha', $ficha->id))) !!}

                        <div class="well bg-success">
                            <div class="row">
                                <div class="col-md-2">
                                    @include('includes.form_checkbox', [ 'campo'=> 'course_active', 'texto'=> 'Curso Activo'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_checkbox', [ 'campo'=> 'activo_web', 'texto'=> 'Activo Web'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_checkbox', [ 'campo'=> 'course_highlight', 'texto'=> 'Curso Destacado'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_checkbox', [ 'campo'=> 'activo_directo', 'texto'=> 'Booking directo'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_select', [ 'campo'=> 'es_aviso_foto', 'texto'=> 'Aviso foto', 'select'=> [null=>'',0=>'No',1=>'Si']])
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    @include('includes.form_input_number', [ 'campo'=> 'duracion_web_min', 'texto'=> 'Duración web semanas (mín)'])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_input_number', [ 'campo'=> 'duracion_web_max', 'texto'=> 'Duración web semanas (máx)'])
                                </div>
                            </div>
                        </div>



                        <div class="form-group">
                            {!! Form::label('centro', 'Centro') !!}:
                            {!! Form::text('centro', $ficha->centro->name, array('class' => 'form-control', 'disabled'=> 'disabled')) !!}
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'course_name', 'texto'=> 'Nombre Curso'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8">
                                @include('includes.form_input_text', [ 'campo'=> 'subtitulo', 'texto'=> 'Subtítulo'])
                            </div>
                            <div class="col-md-4">
                            @include('includes.form_input_file', [ 'campo'=> 'icono', 'texto'=> 'Icono'])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_select_multi_chosen', [ 'campo'=> 'course_accommodation', 'texto'=> 'Alojamientos', 'valor'=> explode(',',$ficha->course_accommodation), 'select'=> $alojamientos])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                @include('includes.form_select', [ 'campo'=> 'category_id', 'texto'=> 'Categoría', 'select'=> $categorias, 'required'=> true])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_select', [ 'campo'=> 'subcategory_id', 'texto'=> 'Subcategoría', 'select'=> $subcategorias, 'required'=> true])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_select', [ 'campo'=> 'subcategory_det_id', 'texto'=> 'Detalle Subcategoría', 'select'=> $subcategorias_det])
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                @include('includes.form_select_multi', [ 'campo'=> 'course_language', 'texto'=> 'Idiomas Requeridos para este curso', 'valor'=> explode(',',$ficha->course_language), 'select'=> $idiomas, 'help'=> $ficha->course_language])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_select', [ 'campo'=> 'course_minimun_language', 'texto'=> 'Nivel Mínimo de Idioma', 'valor'=> $ficha->course_minimun_language, 'select'=> $idioma_niveles])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_input_number', [ 'campo'=> 'course_language_sessions', 'texto'=> 'Número de Sesiones de Idioma'])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'course_age', 'texto'=> 'Edad Números',
                            'help'=> "(entra las edades en numeros separadas por coma. No puede haber un caracter que no sea un numero o una coma.)"])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'course_age_range', 'texto'=> 'Edad Rangos',
                            'help'=> "(entra los numeros como quieres que los vea el cliente. Por ejemplo +18 o 12-16)"])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                @include('includes.form_input_text', [ 'campo'=> 'commission', 'texto'=> 'Comisión', 'help'=> "En blanco cogerá la del centro"])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'course_summary', 'texto'=> 'Resumen',
                            'help'=> "(este texto es lo que aparece como highlight del curso en el listado de cursos. Tiene que ser algo atractivo y catchy para el cliente)"])
                        </div>

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'es_convocatoria_multi', 'texto'=> 'Multiconvocatoria'])
                        </div>

                        <div class="form-group preciosyfechas">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'preciosyfechas', 'texto'=> 'Precios y Fechas (Multiconvocatoria)'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'course_promo', 'texto'=> 'Curso Promoción'])
                        </div>

                        <div class="form-group promo_texto">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'promo_texto', 'texto'=> 'Texto Promo'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'frase', 'texto'=> 'Frase Destacada'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'course_content', 'texto'=> 'Contenido', 'help'=> 'Las imágenes no deben superar los 500px de ancho'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'course_activities', 'texto'=> 'Actividades',
                                'help'=> 'Entrar información SÓLO si las actividades son diferentes a las entradas en Centro. Sino dejar vacío.'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'course_excursions', 'texto'=> 'Excursiones',
                                'help'=> 'Entrar información SÓLO si las excursiones son diferentes a las entradas en Centro. Sino dejar vacío.'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'course_provider_url', 'texto'=> 'URL Proveedor'])
                        </div>

                        @include('includes.form_booking_reserva')

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'course_video', 'texto'=> 'Vídeo', 'help'=> "poner la url completa del video en Youtube, la que empieza por https://www.youtube.com/watch?v="])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_input_text', [ 'campo'=> 'rs_pinterest', 'texto'=> 'Pinterest'])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_input_text', [ 'campo'=> 'ganalytics', 'texto'=> 'Google Analytics (Track)'])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'instagram_token', 'texto'=> 'IG Token'])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_input_text', [ 'campo'=> 'instagram_user', 'texto'=> 'IG user'])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'requisitos', 'texto'=> 'Requisitos especiales' ])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea', [ 'campo'=> 'video', 'texto'=> 'Embed video' ])
                        </div>

                        <hr>

                        {{-- <div class="form-group row">
                            <div class="col-md-8">
                                @include('includes.form_input_text', [ 'campo'=> 'monitor_name', 'texto'=> 'Monitor'])
                            </div>
                            <div class="col-md-4">
                            @include('includes.form_input_file', [ 'campo'=> 'monitor_foto', 'texto'=> 'Monitor foto'])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'monitor_desc', 'texto'=> 'Descripción Monitor' ])
                        </div> --}}

                        <div class="form-group row">
                            <div class="col-md-3">
                                @include('includes.form_select_multi', [ 'campo'=> 'monitores', 'texto'=> 'Monitores', 'valor'=> $ficha->monitores->pluck('monitor_id')->toArray(), 'select'=> \VCN\Models\Monitores\Monitor::plataforma()->pluck('name','id')->toArray() ])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'monitor_web', 'texto'=> 'Mostrar en Web'])
                            </div>
                            <div class="col-md-7">
                                Monitor Web:
                                @if($ficha->monitor)
                                    <a href="{{route('manage.monitores.ficha', $ficha->monitor->id)}}">{{$ficha->monitor->name}}</a> ({!! $ficha->monitor_asignacion !!})
                                @else
                                    NO
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'pocket', 'texto'=> 'Pocket Guide'])
                            </div>
                            <div class="col-md-3">
                                @if($ficha->categoria)
                                    (Categoría: {{$ficha->categoria->pocket?"SI":"NO"}})
                                    @if($ficha->subcategoria)
                                    > (SubCategoría: {{$ficha->subcategoria->pocket?"SI":"NO"}})
                                    @else
                                    > (SubCategoría: (NO TIENE))
                                    @endif
                                @endif
                            </div>
                        </div>

                        @include('includes.form_plataforma', ['campo'=> 'propietario', 'todas'=> true])

                        @if($ficha->es_convocatoria_multi)
                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'contable', 'texto'=> 'Código Contable (Multiconvocatoria)'])
                        </div>
                        @endif

                        {{--'course_testimonials_parents', 'course_testimonials_campers' --}}

                        @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="especialidades">

                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <caption>Especialidades</caption>
                            <thead>
                                <tr>
                                    <th>Especialidad</th>
                                    <th>Subespecialidad/es</th>
                                    <th><i>Editar</i></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($ficha->especialidades as $especialidad)
                                <tr>
                                    <td>{{$especialidad->especialidad?$especialidad->especialidad->name:"-"}}</td>
                                    <td>
                                        {{$especialidad->subespecialidades_name}}
                                    </td>
                                    <td>
                                        {!! Form::open(array('method' => 'POST', 'url' => route('manage.cursos.especialidades.post',$ficha->id), 'role' => 'form', 'class' => 'form-inline')) !!}

                                            {!! Form::hidden('edit_especialidad_id', $especialidad->id) !!}

                                            @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Actualizar'])

                                            <div class="form-group pull-right">
                                            @include('includes.form_select_multi_chosen', [ 'campo'=> 'edit_subespecialidad_id',
                                            'texto'=> null, 'valor'=> explode(',',$especialidad->subespecialidad_id),
                                            'select'=> \VCN\Models\Subespecialidad::where('especialidad_id',$especialidad->especialidad ? $especialidad->especialidad->id : 0)->pluck('name','id')])
                                            </div>

                                        {!! Form::close() !!}
                                    </td>
                                    <td>
                                        <a href='#destroy'
                                            data-label='Borrar'
                                            data-model='Curso Especialidad'
                                            data-action='{{route( 'manage.cursos.especialidades.delete', $especialidad->id)}}'
                                            data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <hr>

                        <div id="especialidad-add" class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-plus-circle fa-fw"></i> Añadir:
                            </div>
                            <div class="panel-body">

                                {!! Form::open(array('method' => 'POST', 'url' => route('manage.cursos.especialidades.post',$ficha->id), 'role' => 'form', 'class' => '')) !!}

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        @include('includes.form_select', [ 'campo'=> 'especialidad_id', 'texto'=> 'Especialidad',
                                            'select'=> $especialidades])
                                    </div>
                                    <div class="col-md-6">
                                        @include('includes.form_select_multi', [ 'campo'=> 'subespecialidad_id', 'texto'=> 'Subespecialidad', 'valor'=> [], 'select'=> $subespecialidades])
                                    </div>
                                </div>

                                @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Añadir'])
                                {!! Form::close() !!}

                            </div>
                        </div>

                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="alojamientos">
                        @include('manage.alojamientos.list_curso', ['curso_id'=> $ficha->id])
                    </div>



                    <div role="tabpanel" class="tab-pane fade in" id="seo">

                        {!! Form::model($ficha, array('route' => array('manage.cursos.ficha', $ficha->id))) !!}

                            {!! Form::hidden('center_id',$ficha->center_id) !!}
                            {!! Form::hidden('_seo',true) !!}

                            {{-- 'course_tags','course_seo_url') --}}

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'course_slug', 'texto'=> 'SEO URL',
                                'help'=> '(verificar que no falta ningun caracter y no sobran guiones. No puede llevar acentos ni caracteres especiales como ( ) o &)'])
                            </div>

                            <div class="form-group row">
                                <div class="col-md-8">
                                    @include('includes.form_input_text', [ 'campo'=> 'course_seo_title', 'texto'=> 'Título SEO',
                                        'help'=> '(Max 60 caracteres - el titulo debe contener el keyword exacto elegido para la pagina)'])
                                </div>
                                <div class="col-md-1"><br>
                                    <div id="course_seo_title_contador"></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-10">
                                @include('includes.form_input_text', [ 'campo'=> 'course_seo_description', 'texto'=> 'Descripción SEO',
                                    'help'=> '(Max 156 caracteres - debe contener el keyword exacto elegido para la pagina)'])
                                </div>
                                <div class="col-md-1"><br>
                                    <div id="course_seo_description_contador"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'course_seo_tags', 'texto'=> 'Etiquetas SEO',
                                'help'=> 'Etiquetas SEO - Separadas por coma'])
                            </div>

                            <div class="form-group pull-right">
                                {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                                <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                            </div>

                            <div class="clearfix"></div>
                        {!! Form::close() !!}

                        <hr>

                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <caption>Historial Slugs</caption>
                            <thead>
                                <tr>
                                    <th>Slug</th>
                                    <th>Cambiado</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($ficha->slugs as $slug)
                                <tr>
                                    <td>{{$slug->slug}}</td>
                                    <td>{{$slug->created_at->format('d/m/Y H:i')}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="imagenes">

                        <div class="row">
                        <?php
                        $path = public_path() ."/assets/uploads/course/" . $ficha->course_images;
                        $folder = "/assets/uploads/course/" . $ficha->course_images;

                        if(is_dir($path))
                        {
                            $results = scandir($path);
                            foreach ($results as $result) {
                              if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0]==='.') continue;

                              $file = $path . '/' . $result;

                              $btn_class = "btn-xs btn-warning";
                              if($result==$ficha->image_portada)
                              {
                                $btn_class = "btn-success";
                              }

                              if( is_file($file) )
                              {
                                echo '
                                <div class="col-md-3">
                                <div class="thumbnail">
                                <img src="'.$folder . '/' . $result.'" alt="">
                                <div class="caption">
                                <p>
                                <a href="'. route('manage.cursos.delete.file', ['course',$ficha->id,$result]).'" class="btn btn-danger btn-xs" role="button">Borrar</a>
                                <a href="'. route('manage.cursos.foto.portada', [$ficha->id,$result]).'" class="btn '. $btn_class .' pull-right" role="button">Portada</a>
                                </p>
                                </div>
                                </div>
                                </div>';
                              }
                            }
                        }
                        ?>
                        </div>

                        <div class="row">
                            <hr>
                            <div class="form-group">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h4>Añadir imágenes [{{$ficha->course_images}}]</h4>
                                    </div>
                                    {{--
                                    <form id="myDropzone" class="dropzone" action="{{route('manage.centros.upload', [$ficha->id])}}" method="POST" enctype="multipart/form-data"></form>
                                    --}}

                                    <div class="dropzone" id="myDropzone"></div>

                                </div>
                            </div>
                        </div>

                        @include('includes.script_dropzone', ['name'=> 'myDropzone', 'url'=> route('manage.cursos.upload', ['course',$ficha->id])])

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="timetables">

                        <div class="row">
                        <?php
                        $path = public_path() ."/assets/uploads/pdf/" . $ficha->course_images;
                        $folder = "/assets/uploads/pdf/" . $ficha->course_images;

                        if(is_dir($path))
                        {
                            $results = scandir($path);
                            foreach ($results as $result) {
                                  if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0]==='.') continue;

                                  $file = $path . '/' . $result;

                                  if( is_file($file) )
                                  {
                                      echo '
                                      <div class="col-md-2">
                                      <div class="thumbnail">
                                      <small>'.$result.'</small>
                                      <hr>
                                      <a class="btn btn-success" href="'.$folder . '/' . $result.'" target="_blank">
                                      <i class="fa fa-download"></i> Download</a>
                                      <p><a href="'. route('manage.cursos.delete.file', ['pdf',$ficha->id,$result]).'" class="btn btn-danger btn-xs" role="button">Borrar</a></p>
                                      </div>
                                      </div>';
                                }
                            }
                        }
                        ?>
                        </div>

                        <div class="row">
                            <hr>
                            <div class="form-group">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h4>Añadir pdf [{{$ficha->course_images}}]</h4>
                                    </div>
                                    {{--
                                    <form id="myDropzone" class="dropzone" action="{{route('manage.centros.upload', [$ficha->id])}}" method="POST" enctype="multipart/form-data"></form>
                                    --}}

                                    <div class="dropzone" id="myDropzone2"></div>

                                </div>
                            </div>
                        </div>

                        @include('includes.script_dropzone', ['name'=> 'myDropzone2', 'url'=> route('manage.cursos.upload', ['pdf',$ficha->id]) ])

                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="extras">
                        @include('manage.cursos.extras.list', ['curso_id'=> $ficha->id])
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="extras-genericos">
                        @include('manage.cursos.genericos.list', ['curso_id'=> $ficha->id])

                        <hr>
                        {!! Form::open(array('route' => array('manage.cursos.genericos.add', $ficha->id))) !!}

                            {!! Form::hidden('course_id',$ficha->id) !!}

                            <div class="form-group">
                                @include('includes.form_select2', [ 'campo'=> 'generics_id', 'texto'=> 'Extra Genérico', 'valor'=> 0, 'select'=> $extra_genericos])
                            </div>

                            @include('includes.form_submit', [ 'permiso'=> 'extras', 'texto'=> 'Añadir'])

                        {!! Form::close() !!}
                    </div>

                    @if($ficha->es_convocatoria_nula)
                    <div role="tabpanel" class="tab-pane fade" id="convocatorias-precio">
                        <br><br>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="pull-right"><a href="{{ route('manage.convocatorias.cerradas.nuevo', $ficha->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Convocatoria Cerrada</a></span>
                            </div>
                            <div class="col-md-3">
                                <span class="pull-right"><a href="{{ route('manage.convocatorias.abiertas.nuevo', $ficha->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Convocatoria Abierta</a></span>
                            </div>
                            <div class="col-md-3">
                                <span class="pull-right"><a href="{{ route('manage.convocatorias.multis.nuevo', $ficha->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Convocatoria Multi</a></span>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if($ficha->es_convocatoria_cerrada)
                    <div role="tabpanel" class="tab-pane fade" id="convocatorias-cerradas">
                        <div class="col-md-12">
                            @include('manage.convocatorias.cerradas.list', ['curso_id'=> $ficha->id])
                        </div>
                    </div>
                    @endif

                    @if($ficha->es_convocatoria_abierta)
                    <div role="tabpanel" class="tab-pane fade" id="convocatorias-abiertas">
                        <div class="col-md-12">
                            @include('manage.convocatorias.abiertas.list', ['curso_id'=> $ficha->id])
                        </div>
                    </div>
                    @endif

                    @if($ficha->es_convocatoria_multi)
                    <div role="tabpanel" class="tab-pane fade" id="convocatorias-multi">
                        <div class="col-md-12">
                            @if(ConfigHelper::canView('precios'))
                                @include('manage.convocatorias.multis.list', ['curso_id'=> $ficha->id])
                            @endif
                        </div>
                    </div>
                    @endif

                    <div role="tabpanel" class="tab-pane fade" id="reuniones">
                        <div class="col-md-12">
                            @include('manage.reuniones.list_curso', ['curso_id'=> $ficha->id])
                        </div>
                        <div class="col-md-12">
                            @include('manage.reuniones.new', ['curso_id'=> $ficha->id])
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="docs">
                        @include('includes.documentos', ['modelo'=> 'Curso', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="cuestionarios">
                        @include('manage.system.cuestionarios.vinculado', ['modelo'=> 'Curso', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="tests">
                        @include('manage.exams.vinculado', ['modelo'=> 'Curso', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="pocket_tab">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#pocket-ficha" aria-controls="ficha" role="tab" data-toggle="tab">Campos</a></li>
                            <li role="presentation"><a href="#pocket-vista" aria-controls="pocket-vista" role="tab" data-toggle="tab">Preview</a></li>
                            <li role="presentation"><a href="#pocket-traduccion" aria-controls="pocket-traduccion" role="pocket-traduccion" data-toggle="tab"><i class="fa fa-globe fa-fw"></i></a></li>
                            @foreach(ConfigHelper::idiomas() as $key=>$idioma)
                                @if($idioma!=ConfigHelper::idiomaCRM())
                                    <li role="presentation"><a href="#pocket-traduccion-vista-{{$idioma}}" aria-controls="pocket-traduccion-vista" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Preview <span class="text-uppercase">{{$idioma}}</span></a></li>
                                @endif
                            @endforeach

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane fade in active" id="pocket-ficha">

                                {!! Form::model($ficha, array('route' => array('manage.cursos.ficha', $ficha->id))) !!}
                                    {!! Form::hidden('_pocket',1) !!}

                                    <div class="form-group">
                                        @include('includes.form_campo', [ 'campo'=> 'pocket_dinero'])
                                    </div>

                                    <div class="form-group">
                                        @include('includes.form_campo', [ 'campo'=> 'pocket_prueba_nivel'])
                                    </div>

                                    <div class="form-group">
                                        @include('includes.form_campo', [ 'campo'=> 'pocket_monitores', 'helpc'=> "Si contenido vacío se muestra el texto de la configuración del campo"])
                                    </div>

                                    <div class="form-group">
                                        @include('includes.form_campo', [ 'campo'=> 'pocket_normas'])
                                    </div>

                                    <div class="form-group">
                                        {{-- @include('includes.form_textarea_tinymce', [ 'campo'=> 'material', 'texto'=> 'Material/Ropa para actividades específicas' ]) --}}
                                        @include('includes.form_campo', [ 'campo'=> 'material', 'helpc'=> "Si contenido vacío se muestra el texto de la configuración del campo"])
                                    </div>

                                    <div class="form-group">
                                        {{-- @include('includes.form_input_number', [ 'campo'=> 'anterior_participantes', 'texto'=> 'Nº participantes totales del año pasado']) --}}
                                        @include('includes.form_campo', [ 'campo'=> 'anterior_participantes'])
                                    </div>

                                    <div class="form-group">
                                        {{-- @include('includes.form_textarea_tinymce', [ 'campo'=> 'anterior_nacionalidades', 'texto'=> 'Nacionalidades año pasado']) --}}
                                        @include('includes.form_campo', [ 'campo'=> 'anterior_nacionalidades'])
                                    </div>

                                    @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                                {!! Form::close() !!}

                            </div>

                            <div role="tabpanel" class="tab-pane fade in" id="pocket-vista">
                                @include('manage.cursos.pocketguide', ['curso'=> $ficha, 'lang'=> ConfigHelper::idiomaCRM()])
                            </div>



                            <div role="tabpanel" class="tab-pane fade in" id="pocket-traduccion">
                                <?php
                                    $pocket_text = \VCN\Models\System\Campo::where('modelo',"Curso")->where('traducible',1)->where('tipo','>',0)->where('textarea',0)->pluck('nombre','name')->toArray();
                                    $pocket_textarea = \VCN\Models\System\Campo::where('modelo',"Curso")->where('traducible',1)->where('tipo','>',0)->where('textarea',1)->pluck('nombre','name')->toArray();
                                ?>

                                @include('includes.traduccion-tab',
                                           [
                                               'modelo'=> 'Curso',
                                               'campos_text'=> [
                                                   $pocket_text
                                               ],
                                               'campos_textarea'=> [
                                                   $pocket_textarea,
                                                   //['material'=> 'Material/Ropa'],
                                                   //['anterior_nacionalidades'=> 'Nacionalidades año pasado'],
                                               ]
                                           ])
                            </div>

                            @foreach(ConfigHelper::idiomas() as $key=>$idioma)
                                @if($idioma!=ConfigHelper::idiomaCRM())
                                    <div role="tabpanel" class="tab-pane fade in" id="pocket-traduccion-vista-{{$idioma}}">
                                        @include('manage.cursos.pocketguide', ['curso'=> $ficha, 'lang' => $idioma])
                                    </div>
                                @endif
                            @endforeach

                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="condiciones">
                        {!! Form::model($ficha, array('route' => array('manage.cursos.ficha', $ficha->id)) )!!}

                        {!! Form::hidden('condiciones', 'condiciones') !!}

                        @foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)

                            @foreach(ConfigHelper::idiomas() as $keyi=>$idioma)

                                <?php

                                    $dir = "assets/uploads/pdf_condiciones/";
                                    $name = class_basename($ficha) ."_". $ficha->id;
                                    $file = $dir. $name ."_". $keyp ."_". $idioma .".pdf";

                                    $valor = null;

                                    if(isset($ficha->condiciones[$keyp][$idioma]))
                                    {
                                        $valor = $ficha->condiciones[$keyp][$idioma];
                                    }
                                ?>

                                <div class="form-group">
                                    @include('includes.form_textarea_tinymce', [ 'campo'=> "condiciones_$keyp-$idioma", 'texto'=> "Condiciones ($plataforma : $idioma)", 'valor'=> $valor])
                                    
                                    @if($valor)
                                        Ver: <a target="_blank" href="/{{$file}}">PDF</a>
                                    @endif

                                </div>

                            @endforeach

                        @endforeach

                        @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="pdf_cancelacion">
                        {!! Form::model($ficha, array('route' => array('manage.cursos.ficha', $ficha->id), 'files'=> true ))!!}

                        {!! Form::hidden('pdf_cancelacion', true) !!}

                        @foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)

                            @foreach(ConfigHelper::idiomas() as $keyi=>$idioma)

                                <?php

                                $valor = null;
                                if(isset($ficha->pdf_cancelacion[$keyp][$idioma]))
                                {
                                    $valor = $ficha->pdf_cancelacion[$keyp][$idioma];
                                }
                                ?>

                                <div class="form-group">
                                    @include('includes.form_input_file', [ 'campo'=> "pdf_$keyp-$idioma", 'texto'=> "PDF ($plataforma : $idioma)", 'valor'=> $valor])

                                    @if($valor)
                                        Ver: <a target="_blank" href="/{{$valor}}">PDF</a>
                                    @endif

                                </div>

                            @endforeach

                        @endforeach

                        @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="doc_especificos">
                        @include('includes.system_doc_especificos', ['modelo'=> 'Curso', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="contactos_sos">
                        @include('includes.system_contactos-tab', ['modelo'=> 'Curso', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                            [
                                'modelo'=> 'Curso',
                                'campos_text'=> [
                                    ['name'=> 'Nombre'],
                                    ['course_age_range'=> 'Edad Rangos'],
                                    ['subtitulo'=> 'Subtítulo'],
                                    ['course_slug'=> 'SEO URL'],
                                    ['course_seo_title'=> 'Título SEO'],
                                    ['course_seo_description'=> 'Descripción SEO'],
                                    ['course_seo_tags'=> 'Etiquetas SEO'],
                                ],
                                'campos_textarea'=> [
                                    ['course_summary'=> 'Resumen'],
                                    ['preciosyfechas' => 'Precios y Fechas (MultiConvocatoria)'],
                                    ['promo_texto' => 'Texto promo'],
                                    ['frase' => 'Frase destacada'],
                                    ['course_content'=> 'Contenido'],
                                    ['course_activities'=> 'Actividades'],
                                    ['course_excursions'=> 'Excursiones'],
                                    ['requisitos'=> 'Requisitos especiales'],
                                    ['video'=> 'Embed video'],
                                    ['monitor_desc'=> 'Descripción Monitor'],
                                ]
                            ])

                    </div>

                </div>

            </div>
        </div>

{{-- @include('includes.script_categoria_multi', ['url'=> route('manage.subcategorias.json.multi')] ) --}}
@include('includes.script_categoria')
@include('includes.script_especialidad')

<script type="text/javascript">
$(document).ready(function() {

    $('.especialidad-edit').click( function(e) {
        e.preventDefault();

        var $sid = $(this).data('id');
    });

    contadorText_init("course_seo_title","course_seo_title_contador", 60);
    contadorText_init("course_seo_description","course_seo_description_contador", 156);


    $('#es_convocatoria_multi').on('change', function(){
        $('.preciosyfechas').slideToggle();
    });

    if($('#es_convocatoria_multi').prop('checked') == true){
        $('.preciosyfechas').show();
    }else{
        $('.preciosyfechas').hide();
    }


    $('#course_promo').on('change', function(){
        $('.promo_texto').slideToggle();
    });

    if($('#course_promo').prop('checked') == true){
        $('.promo_texto').show();
    }else{
        $('.promo_texto').hide();
    }


});
</script>

@stop