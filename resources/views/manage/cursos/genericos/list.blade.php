<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-book"></i> Curso Extras Genéricos
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'generico'        => 'Nombre',
                  'precio'          => 'Precio',
                  'curso'           => 'Curso',
                  'options' => ''

                ])
                ->setUrl( route('manage.cursos.genericos.index', $curso_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [2] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>