@foreach($listado as $inc)
    <div class="row">
    @if($inc->tipo)
        <div class="col-md-1">
            @include('includes.form_input_number', [ 'campo'=> 'incluye['. $inc->id .']', 'texto'=> null, 'valor'=> isset($ficha)?$ficha->incluyeValor($inc->id):'' ])
        </div>
        <div class="col-md-11">{{$inc->name}}</div>
    @else
        <div class="col-md-1">
            @include('includes.form_checkbox', [ 'campo'=> 'incluye['. $inc->id .']', 'texto'=> null, 'valor'=> isset($ficha)?$ficha->incluyeValor($inc->id):0 ])
        </div>
        <div class="col-md-11">{{$inc->name}}</div>
    @endif
    </div>
@endforeach