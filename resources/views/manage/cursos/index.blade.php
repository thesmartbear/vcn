@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.cursos.index') !!}
@stop

@section('container')

    @include('manage.cursos.list', ['centro_id'=> $centro_id])

@stop