<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-book fa-fw"></i> Cursos
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'            => 'Nombre',
                  'proveedor'       => 'Proveedor',
                  'centro'          => 'Centro',
                  'options'         => ''
                ])
                ->setUrl(route('manage.cursos.index.proveedor', $proveedor_id))
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [3] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>