@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bed fa-fw"></i>
                    Incluye :: Nuevo
            </div>
            <div class="panel-body">

                {!! Form::open(array('route' => array('manage.cursos.incluyes.ficha', 0))) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'tipo', 'texto'=> 'Valor numérico', 'help'=> 'Por defecto checkbox'])
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                    <div class="clearfix"></div>

                {!! Form::close() !!}


            </div>
        </div>

@include('includes.script_unidades', ['div'=>'#course_extras_unit' ])

@stop