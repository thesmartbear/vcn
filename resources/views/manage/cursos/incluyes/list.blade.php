<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-book"></i> Incluye Curso

            <span class="pull-right"><a href="{{ route('manage.cursos.incluyes.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Incluye Curso</a></span>

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'        => 'Nombre',
                  'tipo'        => 'Tipo',
                  'options'     => ''

                ])
                ->setUrl( route('manage.cursos.incluyes.index') )
                ->setOptions(
                  "columnDefs", array(
                    [ "sortable" => false, "targets" => [2] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>