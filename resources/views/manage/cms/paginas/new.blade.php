@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-file-text-o fa-fw"></i> Nueva Página
    </div>
    <div class="panel-body">


        {!! Form::open(array('method' => 'POST', 'url' => route('manage.cms.paginas.ficha',0), 'role' => 'form', 'class' => '')) !!}

            <div id="url" class="form-group row">
                <div class="col-md-8">
                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre',
                        'help'=> 'Nombre de la vista en la plantilla, (si usa carpetas: carpeta.vista). Si no existe cogerá PLANTILLA.pagina'])
                </div>
                <div class="col-md-2">
                    @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'select'=> ConfigHelper::getCMSTipo()])
                </div>
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activa' ])
                </div>
            </div>

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'titulo', 'texto'=> 'Título'])
            </div>

            <div id="url" class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'url', 'texto'=> 'URL / Slug',
                        'help'=> '(En config menu.web: nom_trans ).'])
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    @include('includes.form_select', [ 'campo'=> 'page_id', 'texto'=> 'Página Web Padre', 'select'=> $padres])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_number', [ 'campo'=> 'orden', 'texto'=> 'Orden' ])
                </div>
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'menu', 'texto'=> 'Menú principal' ])
                </div>
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'menu_secundario', 'texto'=> 'Menú secundario' ])
                </div>
            </div>

            <div id="contenido" class="form-group">
                @include('includes.form_textarea_tinymce', [ 'campo'=> 'contenido', 'texto'=> 'Contenido'])
            </div>

            @include('includes.form_plataforma', ['campo'=> 'propietario'])

            @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {

    $('#tipo').change( function() {

        if($(this).val()==1)
        {
            $('#contenido').hide();
        }
        else
        {
            $('#contenido').show();
        }

    });
});
</script>

@stop
