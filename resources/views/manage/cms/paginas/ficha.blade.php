@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-page fa-fw"></i> Página :: {{$ficha->name}}
    </div>
    <div class="panel-body">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Página</a></li>
            <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.cms.paginas.ficha',$ficha->id), 'role' => 'form', 'class' => '')) !!}

                    <div id="url" class="form-group row">
                        <div class="col-md-8">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre',
                                'help'=> 'Nombre de la vista en la plantilla, (si usa carpetas: carpeta.vista). Si no existe cogerá PLANTILLA.pagina'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'select'=> ConfigHelper::getCMSTipo()])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activa' ])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'titulo', 'texto'=> 'Título'])
                    </div>

                    <div id="url" class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'url', 'texto'=> 'URL / Slug',
                            'help'=> '(En config menu.web: nom_trans ).'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'page_id', 'texto'=> 'Página Web Padre', 'select'=> $padres])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_number', [ 'campo'=> 'orden', 'texto'=> 'Orden' ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'menu', 'texto'=> 'Menú principal' ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'menu_secundario', 'texto'=> 'Menú secundario' ])
                        </div>
                    </div>

                    <div id="contenido" class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'contenido', 'texto'=> 'Contenido'])
                    </div>

                    @include('includes.form_plataforma', ['campo'=> 'propietario'])

                    @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                @include('includes.traduccion-tab',
                        ['modelo'=> 'Pagina',
                        'campos_text'=> [ ['name'=> 'Nombre'], ['titulo'=> 'Título'], ['url'=> 'URL / Slug'], ],
                        'campos_textarea'=> [ ['contenido'=> 'Contenido'], ]
                    ])

            </div>

        </div>

    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {

    $('#tipo').change( function() {

        if($(this).val()==1)
        {
            $('#contenido').hide();
        }
        else
        {
            $('#contenido').show();
        }

    });
});
</script>

@stop