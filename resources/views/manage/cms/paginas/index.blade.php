@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.cms.paginas.index') !!}
@stop

@section('titulo')
    <i class="fa fa-file-text-o fa-fw"></i> Páginas
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-file-text-o fa-fw"></i> Páginas
                <span class="pull-right"><a href="{{ route('manage.cms.paginas.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Página</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                        'orden'=> 'Orden',
                      'name' => 'Nombre',
                      'titulo' => 'Título',
                      'padre'   => 'Cat.Padre',
                      'nivel'   => 'Nivel',
                      'tipo' => 'Tipo',
                      'url' => 'URL / Slug',
                      'activo' => 'Activa',
                      'options' => ''
                    ])
                    ->setUrl(route('manage.cms.paginas.index'))
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [5,6,7] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop