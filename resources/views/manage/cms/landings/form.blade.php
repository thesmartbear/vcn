<div class="form-group row">
    <div class="col-md-4">
        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre', ])
    </div>
    <div class="col-md-4">
        @include('includes.form_input_text', [ 'campo'=> 'slug', 'texto'=> 'Slug', 'required'=> true])
    </div>
    <div class="col-md-4">
        @include('includes.form_select2_multi', [ 'campo'=> 'avisos[]', 'texto'=> 'Avisos', 'select'=> $parametros->asignados, 'valor'=> $ficha->avisos ?? null])
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3">
        @include('includes.form_select', [ 'campo'=> 'tema', 'texto'=> 'Plantilla', 'select'=> ConfigHelper::config('landings')])
    </div>
    <div class="col-md-2">
        @include('includes.form_select', [ 'campo'=> 'es_general', 'texto'=> 'Tipo', 'select'=> [1=> 'Destino', 0 => 'Programa']])
    </div>
    <div class="col-md-2">
        @include('includes.form_input_text', [ 'campo'=> 'phone', 'texto'=> 'Whatsapp', 'required'=> true])
    </div>
    <div class="col-md-4">
        @include('includes.form_input_file', [ 'campo'=> 'imagen', 'texto'=> 'Fondo'])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-6">
        @include('includes.form_input_text', [ 'campo'=> 'title', 'texto'=> 'Título', 'required'=> true])
    </div>
    <div class="col-md-6">
        @include('includes.form_input_text', [ 'campo'=> 'subtitle', 'texto'=> 'Subtítulo'])
    </div>
</div>
<div class="form-group">
    @include('includes.form_textarea', [ 'campo'=> 'subtitle2', 'texto'=> 'Subtítulo checks', 'help'=> 'Uno por línea'])
</div>

<div class="form-group row">
    <div class="col-md-2">
        @include('includes.form_checkboxh', [ 'campo'=> 'es_form', 'texto'=> 'Mostrar Formulario'])
    </div>
    <div class="col-md-6">
        @include('includes.form_input_text', [ 'campo'=> 'form_title', 'texto'=> 'Título formulario'])
    </div>
    {{-- <div class="col-md-6">
        @include('includes.form_textarea', [ 'campo'=> 'form_subtitle', 'texto'=> 'Subtítulo formulario'])
    </div> --}}
</div>
<div class="form-group row">
    <div class="col-md-6">
        @include('includes.form_textarea_tinymce', [ 'campo'=> 'form_pre', 'texto'=> 'Antes del formulario'])
    </div>
    <div class="col-md-6">
        @include('includes.form_textarea_tinymce', [ 'campo'=> 'form_after', 'texto'=> 'Debajo del formulario'])
    </div>
</div>

<div class="form-group">
    @include('includes.form_textarea', [ 'campo'=> 'gracias', 'texto'=> 'Gracias formulario'])
</div>

<div class="form-group row">
    <div class="col-md-6">
        @include('includes.form_input_text', [ 'campo'=> 'form_options1', 'texto'=> 'Opciones Destino/Programa', 'help'=> 'Separado por comas'])
    </div>
    <div class="col-md-6">
        @include('includes.form_input_text', [ 'campo'=> 'form_options2', 'texto'=> 'Opciones Duración', 'help'=> 'Separado por comas'])
    </div>
</div>

<hr>
<div class="form-group row">
    <div class="col-md-4">
        @include('includes.form_input_text', [ 'campo'=> 'catalogo_form_title', 'texto'=> 'Catálogo Título formulario'])
    </div>
    <div class="col-md-4">
        @include('includes.form_textarea', [ 'campo'=> 'catalogo_form_subtitle', 'texto'=> 'Catálogo Subtítulo formulario'])
    </div>
    <div class="col-md-4">
        @include('includes.form_textarea', [ 'campo'=> 'catalogo_form_texto', 'texto'=> 'Catálogo Texto formulario'])
    </div>
</div>
<div class="form-group row">
    <div class="col-md-6">
        @include('includes.form_textarea', [ 'campo'=> 'catalogo_texto', 'texto'=> 'Catálogo texto'])
    </div>
    <div class="col-md-6">
        @include('includes.form_textarea', [ 'campo'=> 'catalogo_gracias', 'texto'=> 'Catálogo gracias'])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-4">
        @include('includes.form_select2_multi', [ 'campo'=> 'avisos_catalogo[]', 'texto'=> 'Avisos Catálogo', 'select'=> $parametros->asignados, 'valor'=> $ficha->avisos_catalogo ?? null])
    </div>
    <div class="col-md-4">
        @include('includes.form_select', [ 'campo'=> 'catalogo_id', 'texto'=> 'Catálogo', 'select'=> $parametros->catalogos ])
    </div>
</div>

<hr>
<div class="form-group row">
    <div class="col-md-6">
        @include('includes.form_textarea', [ 'campo'=> 'porque', 'texto'=> '¿Por qué BS?', 'help'=> 'Uno por línea'])
    </div>
    <div class="col-md-6">
        @include('includes.form_textarea_tinymce', [ 'campo'=> 'porque_pre', 'texto'=> 'Encima de ¿Por qué BS?'])
    </div>
</div>

<div class="form-group">
    @include('includes.form_input_text', [ 'campo'=> 'video', 'texto'=> 'Url vídeo'])
</div>

<div class="form-group row">
    <div class="col-md-3">
        @include('includes.form_input_text', [ 'campo'=> 'testimonios_title', 'texto'=> 'Título testimonios'])
    </div>
    <div class="col-md-4">
        @include('includes.form_textarea', [ 'campo'=> 'testimonios_subtitle', 'texto'=> 'Subtítulo testimonios'])
    </div>
    <div class="col-md-5">
        @include('includes.form_textarea_tinymce', [ 'campo'=> 'testimonios_pre', 'texto'=> 'Encima de testimonios'])
    </div>
</div>

<div class="form-group">
    @include('includes.form_textarea_tinymce', [ 'campo'=> 'sellos_pre', 'texto'=> 'Encima de Sellos de calidad'])
</div>

<div class="form-group row">
    <div class="col-md-3">
        @include('includes.form_input_text', [ 'campo'=> 'nosotros_title', 'texto'=> 'Título nosotros'])
    </div>
    <div class="col-md-4">
        @include('includes.form_textarea', [ 'campo'=> 'nosotros_subtitle', 'texto'=> 'Subtítulo nosotros'])
    </div>
    <div class="col-md-5">
        @include('includes.form_textarea_tinymce', [ 'campo'=> 'nosotros_pre', 'texto'=> 'Encima de nosotros'])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3">
        @include('includes.form_input_text', [ 'campo'=> 'seccion_title', 'texto'=> 'Título Destinos/Programas'])
    </div>
    <div class="col-md-4">
        @include('includes.form_textarea', [ 'campo'=> 'seccion_subtitle', 'texto'=> 'Subtítulo Destinos/Programas'])
    </div>
    <div class="col-md-5">
        @include('includes.form_textarea_tinymce', [ 'campo'=> 'lista_after', 'texto'=> 'Debajo de Destinos/Programas'])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-4"id="testimonios_sortable">
        @include('includes.form_select2_multi', [ 'campo'=> 'testimonios[]', 'sortable'=> 'testimonios_sortable', 'sortable_ids'=> null, 'texto'=> 'Testimonios', 'select'=> $parametros->testimonios, 'valor'=> $ficha->testimonios ?? null ])
    </div>
    <div class="col-md-4" id="destinos_sortable">
        @include('includes.form_select2_multi', [ 'campo'=> 'destinos[]', 'sortable'=> 'destinos_sortable', 'sortable_ids'=> null, 'texto'=> 'Destinos', 'select'=> $parametros->destinos, 'valor'=> $ficha->destinos ?? null])
    </div>
    <div class="col-md-4" id="programas_sortable">
        @include('includes.form_select2_multi', [ 'campo'=> 'programas[]', 'sortable'=> 'programas_sortable', 'sortable_ids'=> null, 'texto'=> 'Programas', 'select'=> $parametros->programas, 'valor'=> $ficha->programas ?? null] )
    </div>
</div>

<div class="form-group row">
    <div class="col-md-4">
        @include('includes.form_checkboxh', [ 'campo'=> 'bloque_testimonios', 'texto'=> 'Mostrar Testimonios'])
    </div>
    <div class="col-md-4">
        @include('includes.form_checkboxh', [ 'campo'=> 'bloque_catalogo', 'texto'=> 'Mostrar catálogo'])
    </div>
    <div class="col-md-4">
        @include('includes.form_checkboxh', [ 'campo'=> 'bloque_porque', 'texto'=> 'Mostrar Por qué'])
    </div>
</div>


<hr>
<div class="form-group row">
    <div class="col-md-6">
        @include('includes.form_textarea', [ 'campo'=> 'gtm_head', 'texto'=> 'Scripts Head'])
    </div>
    <div class="col-md-6">
        @include('includes.form_textarea', [ 'campo'=> 'gtm_body', 'texto'=> 'Scripts Body'])
    </div>
</div>

<hr>
<div class="form-group row">
    <div class="col-md-4">
        @include('includes.form_plataforma', ['campo'=> 'plataforma_id', 'todas'=> false])
    </div>
    <div class="col-md-4">
        @include('includes.form_checkboxh', [ 'campo'=> 'activo', 'texto'=> 'Activo'])
    </div>
</div>