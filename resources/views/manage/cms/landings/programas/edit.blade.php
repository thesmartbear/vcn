@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-globe fa-fw"></i> Programa :: {{$ficha->name}}
    </div>
    <div class="panel-body">

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Destino</a></li>
            <li role="presentation"><a data-label="Traducciones" href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                {!! Form::open(array('method' => 'PUT', 'files'=> true, 'url' => route('manage.cms.landings.programas.update',$ficha->id), 'role' => 'form', 'class' => '')) !!}

                    @include('manage.cms.landings.programas.form')

                    @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                @include('includes.traduccion-tab',
                        ['modelo'=> 'LandingPrograma',
                        'campos_text'=> [
                            ['title'=> 'Título'],
                            ['subtitle'=> 'Subtítulo'],
                            ['edad'=> 'Edad'],
                            ['precio'=> 'Precio'],
                            ['precio_duracion'=> 'Duración'],
                        ],
                        'campos_textarea_basic'=> [
                            ['texto'=> 'Texto']
                        ]
                    ])

            </div>
            
        </div>

    </div>
</div>

@stop