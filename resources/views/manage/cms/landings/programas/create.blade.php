@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-globe fa-fw"></i> Nuevo Programa
    </div>
    <div class="panel-body">

        {!! Form::open(array('method' => 'POST', 'files'=> true, 'url' => route('manage.cms.landings.programas.store'), 'role' => 'form', 'class' => '')) !!}

            @include('manage.cms.landings.programas.form')

            @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

@stop
