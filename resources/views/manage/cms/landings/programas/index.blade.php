@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.cms.landings.index') !!} --}}
@stop

@section('titulo')
    <i class="fa fa-globe fa-fw"></i> Programas
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-globe fa-fw"></i> Programas
            <span class="pull-right"><a href="{{ route('manage.cms.landings.programas.create') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Programa</a></span>
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name' => 'Nombre',
                  'options' => ''
                ])
                ->setUrl(route('manage.cms.landings.programas.index'))
                ->setOptions(
                  "aoColumnDefs", array(
                    //[ "bSortable" => false, "aTargets" => [2,3,4] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

@stop