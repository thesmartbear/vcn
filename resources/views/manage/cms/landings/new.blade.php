@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-file fa-fw"></i> Nueva Landing
    </div>
    <div class="panel-body">


        {!! Form::open(array('method' => 'POST', 'files'=> true, 'url' => route('manage.cms.landings.ficha',0), 'role' => 'form', 'class' => '')) !!}

            @include('manage.cms.landings.form')

            @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

@stop
