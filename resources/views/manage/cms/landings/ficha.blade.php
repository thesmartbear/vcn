@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-file fa-fw"></i> Landing :: {{$ficha->name}}
    </div>
    <div class="panel-body">

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Landing</a></li>
            <li role="presentation"><a data-label="Formularios" href="#forms" aria-controls="forms" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>
            <li role="presentation"><a data-label="Traducciones" href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                {!! Form::open(array('method' => 'POST', 'files'=> true, 'url' => route('manage.cms.landings.ficha',$ficha->id), 'role' => 'form', 'class' => '')) !!}

                    @include('manage.cms.landings.form')

                    @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="forms">
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-clipboard-list fa-fw"></i> Formularios
                    </div>
                    <div class="panel-body">
            
                        {!! Datatable::table()
                            ->addColumn([
                                'recibido' => 'Recibido',
                                'nombre' => 'Nombre',
                                'es_viajero' => "Viajero",
                                'destino' => "Destino",
                                'duracion' => "Duración",
                                'origen' => "Origen",
                                'utm' => "utm",
                                'options' => ''
                            ])
                            ->setUrl(route('manage.cms.landings.informe', $ficha->id))
                            ->setOptions("order", [ [0, 'desc'] ])
                            ->setOptions(
                                "columnDefs", array(
                                    // [ "sortable" => false, "targets" => [0] ],
                                    [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY H:mm'):'-';}" ],
                                )
                            )
                            ->render() !!}
            
                    </div>
                </div>

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                @include('includes.traduccion-tab',
                        ['modelo'=> 'Landing',
                        'campos_text'=> [
                            ['title'=> 'Título'],
                            ['subtitle'=> 'Subtítulo'],
                            
                            ['form_title'=> 'Título formulario'],
                            ['testimonios_title'=> 'Título testimonios'],
                            ['nosotros_title'=> 'Título nosotros'],
                            ['seccion_title'=> 'Título lista'],

                            ['form_options1'=> 'Opciones Destino/Programa'],
                            ['form_options2'=> 'Opciones Duración'],
                            
                            ['video'=> 'Vídeo']
                        ],
                        // 'campos_textarea'=>[],
                        'campos_textarea_basic'=> [
                            ['subtitle2'=> 'Subtítulo checks'],
                            ['form_subtitle'=> 'Subtítulo formulario'],
                            ['gracias'=> 'Gracias formulario'],
                            ['catalogo_texto'=> 'Catálogo texto'],
                            ['porque'=> '¿Por qué  BS?'],
                            ['testimonios_subtitle'=> 'Subtítulo testimonios'],
                            ['nosotros_subtitle'=> 'Subtítulo nosotros'],
                            ['seccion_subtitle'=> 'Subtítulo lista'],
                            ['form_pre' => 'Antes del formulario'],
                            ['form_after' => 'Debajo del formulario'],
                            ['lista_after' => 'Debajo de Destinos/Programas'],
                            ['nosotros_pre' => 'Encima de nosotros'],
                            ['porque_pre' => 'Encima de ¿Por qué BS?'],
                            ['testimonios_pre' => 'Encima de testimonios'],
                            ['sellos_pre' => 'Encima de Sellos de calidad'],
                        ],
                        'campos_select'=>[
                            ['catalogo_id' => ['title'=> 'Catálogo', 'select' => $parametros->catalogos]],
                        ]
                    ])

            </div>
            
        </div>

    </div>
</div>

@stop