@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.cms.landings.index') !!} --}}
@stop

@section('titulo')
    <i class="fa fa-file fa-fw"></i> Contactos
@stop

@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-file fa-fw"></i> Contacto Landing :: {{$ficha->created_at->format("d/m/Y H:i")}}
    </div>
    <div class="panel-body">

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th width="20%">Campo</th>
                    <th>Valor</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Nombre</td>
                    <td>{{$ficha->nombre}}</td>
                </tr>
                <tr>
                    <td>Teléfono</td>
                    <td>{{$ficha->telefono}}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{{$ficha->email}}</td>
                </tr>
                <tr>
                    <td>F.Nacimiento</td>
                    <td>{{$ficha->fechanac_dmy}}</td>
                </tr>
                <tr>
                    <td>Viajero</td>
                    <td>{{$ficha->es_viajero ? "SI" : "NO"}}</td>
                </tr>
                @if($ficha->es_viajero)
                <tr>
                    <td>Tutor</td>
                    <td>{{$ficha->tutor_nombre}} [{{$ficha->tutor_telefono}}]</td>
                </tr>
                @endif
                <tr>
                    <td>Destino</td>
                    <td>{{$ficha->destino}}</td>
                </tr>
                <tr>
                    <td>Duración</td>
                    <td>{{$ficha->duracion}}</td>
                </tr>

                <tr>
                    <td>+INFO</td>
                    <td>{{$ficha->info_ip}}</td>
                </tr>
                <tr>
                    <td>GEO</td>
                    <td>{{$ficha->info_geo}} :: {!! $ficha->info_mapa !!}</td>
                </tr>
                <tr>
                    <td>UTM</td>
                    <td>{{$ficha->utm}}</td>
                </tr>

            </tbody>
        </table>

    </div>
</div>

@stop