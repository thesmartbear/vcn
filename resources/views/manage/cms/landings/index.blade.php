@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.cms.landings.index') !!} --}}
@stop

@section('titulo')
    <i class="fa fa-file fa-fw"></i> Landings
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-file fa-fw"></i> Landings
            <span class="pull-right"><a href="{{ route('manage.cms.landings.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Landing</a></span>
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name' => 'Nombre',
                  'slug' => 'Slug',
                  'plataforma' => 'Plataforma',
                  'tema' => 'Tema',
                  'general' => 'General',
                  'form' => 'Formulario',
                  'informe' => 'Forms',
                  'activo' => 'Activo',
                  'options' => ''
                ])
                ->setUrl(route('manage.cms.landings.index'))
                ->setOptions(
                  "aoColumnDefs", array(
                    //[ "bSortable" => false, "aTargets" => [2,3,4] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

@stop