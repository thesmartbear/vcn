@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.cms.landings.index') !!} --}}
@stop

@section('titulo')
    <i class="fa fa-plane fa-fw"></i> Destinos
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-plane fa-fw"></i> Destinos
            <span class="pull-right"><a href="{{ route('manage.cms.landings.destinos.create') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Destino</a></span>
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name' => 'Nombre',
                  'options' => ''
                ])
                ->setUrl(route('manage.cms.landings.destinos.index'))
                ->setOptions(
                  "aoColumnDefs", array(
                    //[ "bSortable" => false, "aTargets" => [2,3,4] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

@stop