<div class="form-group row">
    <div class="col-md-4">
        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
    </div>
    <div class="col-md-2">
        @include('includes.form_input_file', [ 'campo'=> 'foto', 'texto'=> 'Foto', 'noborrar'=> true])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-6">
        @include('includes.form_input_text', [ 'campo'=> 'title', 'texto'=> 'Título'])
    </div>
    <div class="col-md-6">
        @include('includes.form_input_text', [ 'campo'=> 'subtitle', 'texto'=> 'Subtítulo'])
    </div>
</div>

<div class="form-group">
    @include('includes.form_textarea', [ 'campo'=> 'texto', 'texto'=> 'Texto'])
</div>