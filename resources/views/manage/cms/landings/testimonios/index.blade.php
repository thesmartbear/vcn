@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.cms.landings.index') !!} --}}
@stop

@section('titulo')
    <i class="fa fa-users fa-fw"></i> Testimonios
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-users fa-fw"></i> Testimonios
            <span class="pull-right"><a href="{{ route('manage.cms.landings.testimonios.create') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Testimonio</a></span>
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name' => 'Nombre',
                  'firma' => 'Firma',
                  'es_principal' => 'Principal',
                  'options' => ''
                ])
                ->setUrl(route('manage.cms.landings.testimonios.index'))
                ->setOptions(
                  "aoColumnDefs", array(
                    //[ "bSortable" => false, "aTargets" => [2,3,4] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

@stop