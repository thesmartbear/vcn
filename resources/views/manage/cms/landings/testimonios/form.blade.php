<div id="frmCheck">

    <div class="form-group row">
    <div class="col-md-4">
        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre', 'required'=> true])
    </div>
    <div class="col-md-2">
        {!! Form::label('es_principal', 'Principal') !!}
        <input name="es_principal" type="hidden" value="0">
        {!! Form::checkbox("es_principal", 1, (isset($ficha) ? $ficha->es_principal : false), ['v-model'=> 'es_principal']) !!}
    </div>
    <div class="col-md-2">
        @include('includes.form_input_file', [ 'campo'=> 'foto', 'texto'=> 'Foto', 'noborrar'=> true])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-8">
        @include('includes.form_textarea', [ 'campo'=> 'texto', 'texto'=> 'Texto'])
    </div>
    <div class="col-md-4">
        @include('includes.form_input_text', [ 'campo'=> 'firma', 'texto'=> 'Firma'])
    </div>
</div>

<div class="form-group row" v-if="es_principal">
    <div class="col-md-8">
        @include('includes.form_textarea', [ 'campo'=> 'texto2', 'texto'=> 'Texto2'])
    </div>
    <div class="col-md-4">
        @include('includes.form_input_text', [ 'campo'=> 'firma2', 'texto'=> 'Firma2'])
    </div>
</div>

</div>

@push('scripts')
<script>
new Vue({
    el: '#frmCheck',
    data: {
        es_principal: false,
    },
    mounted: function() {
        
    },
});
</script>
@endpush