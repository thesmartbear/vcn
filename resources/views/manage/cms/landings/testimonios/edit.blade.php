@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-users fa-fw"></i> Testimonio :: {{$ficha->name}}
    </div>
    <div class="panel-body">

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Testimonio</a></li>
            <li role="presentation"><a data-label="Traducciones" href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                {!! Form::open(array('method' => 'PUT', 'files'=> true, 'url' => route('manage.cms.landings.testimonios.update',$ficha->id), 'role' => 'form', 'class' => '')) !!}

                    @include('manage.cms.landings.testimonios.form')

                    @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                @include('includes.traduccion-tab',
                        ['modelo'=> 'LandingTestimonio',
                        'campos_text'=> [
                            ['firma'=> 'Firma'],
                            ['firma2'=> 'Firma2']
                        ],
                        'campos_textarea_basic'=> [
                            ['texto'=> 'Texto'],
                            ['texto2'=> 'Texto2']
                        ]
                    ])

            </div>
            
        </div>

    </div>
</div>

@stop