<ul class="tree">
    @foreach( $ficha->cursos as $c)
        <li>
            Curso: <a target='_blank' href="{{ route('manage.cursos.ficha', $c->id) }}">{{$c->name}}</a>
            [<a target='_blank' href="{{route('web.wn', $ficha->slug)}}/{{$c->course_slug}}.html">{{route('web.wn', $ficha->slug)}}/{{$c->course_slug}}.html </a>]

        </li>
    @endforeach
</ul>