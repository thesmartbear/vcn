@if( $categoria->hijos_activos->count() )
    @foreach($categoria->hijos_activos->sortBy('orden') as $menu)
    <ul class="tree">
        <li>

            <a target='_blank' href="{{ route('manage.cms.categorias.ficha', $menu->id) }}">{{$menu->titulo}}</a> [{{$menu->cursos->count()}} cursos]

            <?php
                if($menu->es_link == 1)
                {
                    $url = (App::getLocale() != ConfigHelper::config('idioma') ? '/'.App::getLocale().'/' : '/') .Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'link', $menu->id, $menu->link);
                }
                else
                {
                    $url = route('web.wn', $menu->slug);
                }

                if($menu->link_blank == 1)
                {
                    $link = 'href="'.$url.'" target="_blank"';
                }
                else
                {
                    $link = 'href="'.$url.'"';
                }
            ?>

            :: <a {!! $link !!}>{{$menu->slug}}</a>

            @include('manage.cms.categorias._sitemap_categorias', ['categoria'=> $menu])
        </li>
    </ul>
    @endforeach
@endif