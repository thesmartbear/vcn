@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.cms.categorias.index') !!}
@stop

@section('titulo')
    <i class="fa fa-tag fa-fw"></i> Categorías Web
@stop

@section('container')

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#v1" aria-controls="v1" role="tab" data-toggle="tab">v.1</a></li>
        {{-- <li role="presentation"><a href="#v2" aria-controls="v2" role="tab" data-toggle="tab">v.2</a></li>
        <li role="presentation"><a href="#301" aria-controls="301" role="tab" data-toggle="tab">301</a></li> --}}

        <li role="presentation"><a href="#sitemap" aria-controls="sitemap" role="tab" data-toggle="tab">Sitemap</a></li>
    </ul>

    <div class="tab-content">

        {{-- <div role="tabpanel" class="tab-pane fade in ficha" id="v2">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-tag fa-fw"></i> Categorías Web v.2
                    <span class="pull-right"><a href="{{ route('manage.cms.categorias.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Categorías Web</a></span>
                </div>
                <div class="panel-body">

                    {!! Datatable::table()
                        ->addColumn([
                          'orden'   => 'Orden',
                          'name'    => 'Nombre',
                          'padre'   => 'Cat.Padre',
                          'nivel'   => 'Nivel',
                          'titulo'  => 'Título',
                          'tipo'    => 'Tipo',
                          'estructura'    => 'v',
                          'idioma'  => 'Idioma',
                          'plataforma'  => 'Plataforma',
                          'tipo_enlace'  => 'Tipo Enlace',
                          'menu_principal'  => 'M.Pral.',
                          'menu_secundario'  => 'M.Sec.',
                          'activo'  => 'Activo',
                          'options' => ''
                        ])
                        ->setUrl(route('manage.cms.categorias.index', ['estructura'=> 2]))
                        ->setOptions(
                          "aoColumnDefs", array(
                            [ "bSortable" => false, "aTargets" => [13] ],
                          )
                        )
                        ->render() !!}

                </div>
            </div>

        </div> --}}


        <div role="tabpanel" class="tab-pane fade in ficha active" id="v1">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-tag fa-fw"></i> Categorías Web v.1
                    <span class="pull-right"><a href="{{ route('manage.cms.categorias.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Categorías Web</a></span>
                </div>
                <div class="panel-body">

                    {!! Datatable::table()
                        ->addColumn([
                          'orden'   => 'Orden',
                          'name'    => 'Nombre',
                          'padre'   => 'Cat.Padre',
                          'nivel'   => 'Nivel',
                          'titulo'  => 'Título',
                          'tipo'    => 'Tipo',
                          'estructura'    => 'v',
                          'idioma'  => 'Idioma',
                          'plataforma'  => 'Plataforma',
                          'tipo_enlace'  => 'Tipo Enlace',
                          'activo'  => 'Activo',
                          'options' => ''
                        ])
                        ->setUrl(route('manage.cms.categorias.index', ['estructura'=> 1]))
                        ->setOptions(
                          "aoColumnDefs", array(
                            [ "bSortable" => false, "aTargets" => [11] ],
                          )
                        )
                        ->render() !!}

                </div>
            </div>

        </div>

        {{-- <div role="tabpanel" class="tab-pane fade in ficha" id="301">

            <ul class="nav nav-tabs" role="tablist">
                @foreach(ConfigHelper::idiomas() as $key=>$idioma)

                    <li role="presentation" class="{{$key==1?'active':''}}"><a href="#trans-{{$idioma}}" aria-controls="ficha" role="trans-{{$idioma}}" data-toggle="tab">{{$idioma}}</a></li>

                @endforeach
                </ul>

                <div class="tab-content">
                @foreach(ConfigHelper::idiomas() as $key=>$idioma)

                    <div role="tabpanel" class="tab-pane fade in {{$key==1?'active':''}}" id="trans-{{$idioma}}">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-globe fa-fw"></i> 301 :: {{$idioma}}
                            </div>
                            <div class="panel-body">

                                {!! Datatable::table()
                                    ->addColumn([
                                      'orden'   => 'Orden',
                                      'name'    => 'Nombre',
                                      'nivel'   => 'Nivel',
                                      'titulo'  => 'Título',
                                      'slug'    => 'slug',
                                      '301'     => '301',
                                    ])
                                    ->setUrl(route('manage.cms.categorias.index301', ['lang'=> $idioma]))
                                    ->setOptions('iDisplayLength', 999)
                                    ->setOptions(
                                      "aoColumnDefs", array(
                                        //[ "bSortable" => false, "aTargets" => [9] ],
                                      )
                                    )
                                    ->render() !!}

                            </div>

                        </div>

                    </div>

                @endforeach
                </div>

        </div> --}}


        <div role="tabpanel" class="tab-pane fade in ficha" id="sitemap">

            <ul class="nav nav-tabs" role="tablist">
                @foreach(ConfigHelper::plataformas(false) as $key=>$plataforma)

                    <li role="presentation" class="{{$key==1?'active':''}}"><a href="#plat-{{$key}}" aria-controls="ficha" role="plat-{{$key}}" data-toggle="tab">{{$plataforma}}</a></li>

                @endforeach
                </ul>

                <div class="tab-content">
                @foreach(ConfigHelper::plataformas() as $key=>$plataforma)

                    <div role="tabpanel" class="tab-pane fade in {{$key==1?'active':''}}" id="plat-{{$key}}">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-sitemap fa-fw"></i> Sitemap :: {{$plataforma}}
                            </div>
                            <div class="panel-body">

                                <?php
                                    $estructura = ConfigHelper::config('web_estructura');
                                    $p = $key;
                                ?>

                                {{-- TODO --}}
                                <?php
                                    $padres = VCN\Models\CMS\CategoriaWeb::where('estructura',$estructura)->whereIn('propietario',[0,$p])->where('activo',1)->where('category_id',0)->orderBy('orden')->get();
                                ?>
                                <strong>HOME</strong>
                                <ul class="tree">
                                @foreach($padres as $categoria)
                                    
                                    <li>
                                    <strong>{{$categoria->name}} ({{$categoria->seo_url}}) [{{$categoria->cursos->count()}} cursos]</strong>
                                    :: <a target='_blank' href="{{route('web.wn', $categoria->slug)}}">{{$categoria->slug}}</a>

                                    @include('manage.cms.categorias._sitemap_categorias', ['categoria'=> $categoria])
                                    </li>
                                
                                @endforeach
                                </ul>


                                <hr>
                                {{-- menu_principal --}}
                                <?php
                                    $padres = VCN\Models\CMS\CategoriaWeb::where('estructura',$estructura)->whereIn('propietario',[0,$p])->where('activo',1)->where('category_id',0)->where('menu_principal',1)->orderBy('orden')->get();
                                ?>
                                <strong>MENÚ PRINCIPAL/SUPERIOR</strong>
                                <ul class="tree">
                                @foreach($padres as $categoria)
                                    
                                    <li>
                                    <strong>{{$categoria->name}} ({{$categoria->seo_url}}) [{{$categoria->cursos->count()}} cursos]</strong>
                                    :: <a target='_blank' href="{{route('web.wn', $categoria->slug)}}">{{$categoria->slug}}</a>

                                    @include('manage.cms.categorias._sitemap_categorias', ['categoria'=> $categoria])
                                    </li>
                                
                                @endforeach
                                </ul>
                                

                                <hr>
                                {{-- menu_secundario --}}
                                <?php
                                    $padres = VCN\Models\CMS\CategoriaWeb::where('estructura',$estructura)->whereIn('propietario',[0,$p])->where('activo',1)->where('category_id',0)->where('menu_secundario',1)->orderBy('orden')->get();
                                ?>
                                <strong>MENÚ SECUNDARIO/FOOTER</strong>
                                <ul class="tree">
                                @foreach($padres as $categoria)
                                    
                                    <li>
                                    <strong>{{$categoria->name}} ({{$categoria->seo_url}}) [{{$categoria->cursos->count()}} cursos]</strong>
                                    :: <a target='_blank' href="{{route('web.wn', $categoria->slug)}}">{{$categoria->slug}}</a>

                                    @include('manage.cms.categorias._sitemap_categorias', ['categoria'=> $categoria])
                                    </li>
                                
                                @endforeach
                                </ul>
                                

                            </div>

                        </div>

                    </div>

                @endforeach
                </div>

        </div>

    </div>


@stop