@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-tag fa-fw"></i> Nueva Categoría Web
    </div>
    <div class="panel-body">


        {!! Form::open(array('method' => 'POST', 'files'=>true, 'url' => route('manage.cms.categorias.ficha',0), 'role' => 'form', 'class' => '')) !!}

            @include('manage.cms.categorias.form', ['ficha'=> new \VCN\Models\CMS\CategoriaWeb])

            @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

<script type="text/javascript">
$(document).ready(function($){
    $('#color').iris();
    $('#color_fondo').iris();
    $('#color_texto').iris();
    $('#color_menu').iris();
});
</script>

@stop
