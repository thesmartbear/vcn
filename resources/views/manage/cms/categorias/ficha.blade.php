@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-money fa-fw"></i> Categoría Web :: {{$ficha->name}} [Nivel: {{$ficha->nivel}}]
        @if($ficha->padre)
            [Padre: {{$ficha->padre->name}}]
        @endif
    </div>
    <div class="panel-body">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Categoría Web</a></li>
            <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
            <li role="presentation"><a href="#filtros" aria-controls="filtros" role="tab" data-toggle="tab"><i class="fa fa-sitemap fa-fw"></i> Sitemap Categorías</a></li>
            <li role="presentation"><a href="#cursos" aria-controls="cursos" role="tab" data-toggle="tab"><i class="fa fa-book fa-fw"></i> Sitemap Cursos</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                {!! Form::open(array('method' => 'POST', 'files'=> true, 'url' => route('manage.cms.categorias.ficha',$ficha->id), 'role' => 'form', 'id' => 'cms-categorias')) !!}

                    @include('manage.cms.categorias.form', ['ficha'=> $ficha])

                    @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                @include('includes.traduccion-tab',
                        ['modelo'=> 'CategoriaWeb',
                        'campos_text'=> [
                            ['name'=> 'Nombre'],
                            ['titulo'=> 'Título'],
                            ['seo_url'=> 'Url SEO'],
                            ['seo_titulo'=> 'Título SEO'],
                            ['seo_keywords'=> 'Keywords SEO'],
                            ['home_titulo' => 'Título Bloque intro (Home)'],
                            ['home_titulo_link' => 'URL Título (Home)'],
                            ['home_boton'=> 'Botón (Home)'],
                            ['link'=> 'URL Enlace (Home)'],
                        ],
                        'campos_textarea'=> [
                            ['seo_descripcion'=> 'Descripción SEO'],
                            ['descripcion'=> 'Descripción'],
                            ['desc_corta'=> 'Descripción corta'],
                            ['desc_lateral'=> 'Descripción lateral'],
                            ['desc_cursos'=> 'Descripción debajo cursos'],
                            ['home_titulo2' => 'Descripción corta (Home)'],
                        ]
                    ])

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="filtros">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-sitemap fa-fw"></i> Sitemap {{-- ({{$ficha->num_cursos}}) --}}
                    </div>
                    <div class="panel-body">

                        <strong>{{$ficha->name}} ({{$ficha->seo_url}}) [{{$ficha->cursos->count()}} cursos]</strong>

                        @include('manage.cms.categorias._sitemap_categorias', ['categoria'=> $ficha])

                        {{-- <script type="text/javascript">
                             $(document).ready(function () {
                               $('ul.tree li:last-child').addClass('last');
                           });
                        </script> --}}


                    </div>
                </div>

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="cursos">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-book fa-fw"></i> Cursos
                    </div>
                    <div class="panel-body">

                        <?php $slug = route('web.wn', $ficha->slug); ?>

                        <strong>{{$ficha->name}} ({{$ficha->seo_url}}) [<a target='_blank' href="{{$slug}}">{{$slug}}</a>]</strong>
                        @include('manage.cms.categorias._sitemap_cursos', ['ficha'=> $ficha])


                        <ul class="tree">
                        @foreach($ficha->hijos_activos->sortBy('orden') as $h1)
                            <?php $slug1 = "$slug/$h1->seo_url"; ?>
                            <li>
                                <a target='_blank' href="{{ route('manage.cms.categorias.ficha', $h1->id) }}">{{$h1->name}}</a> ({{$h1->seo_url}}) [<a target='_blank' href="{{$slug1}}">{{$slug1}}</a>]
                                @include('manage.cms.categorias._sitemap_cursos', ['ficha'=> $h1])

                                @if( $h1->hijos_activos->count() )
                                    @foreach($h1->hijos_activos->sortBy('orden') as $h2)
                                    <?php $slug2 = "$slug1/$h2->seo_url"; ?>
                                    <ul>
                                        <li>
                                            <a target='_blank' href="{{ route('manage.cms.categorias.ficha', $h2->id) }}">{{$h2->name}}</a> ({{$h2->seo_url}}) [<a target='_blank' href="{{$slug2}}">{{$slug2}}</a>]
                                            @include('manage.cms.categorias._sitemap_cursos', ['ficha'=> $h2])

                                            @if( $h2->hijos_activos->count() )
                                                @foreach($h2->hijos_activos->sortBy('orden') as $h3)
                                                <?php $slug3 = "$slug2/$h3->seo_url"; ?>
                                                <ul>
                                                    <li>
                                                        <a target='_blank' href="{{ route('manage.cms.categorias.ficha', $h3->id) }}">{{$h3->name}}</a> ({{$h3->seo_url}}) [<a target='_blank' href="{{$slug3}}">{{$slug3}}</a>]
                                                        @include('manage.cms.categorias._sitemap_cursos', ['ficha'=> $h3])

                                                        @if( $h3->hijos_activos->count() )
                                                            @foreach($h3->hijos_activos->sortBy('orden') as $h4)
                                                            <?php $slug4 = "$slug3/$h4->seo_url"; ?>
                                                            <ul>
                                                                <li>
                                                                    <a target='_blank' href="{{ route('manage.cms.categorias.ficha', $h4->id) }}">{{$h4->name}}</a> ({{$h4->seo_url}}) [<a target='_blank' href="{{$slug4}}">{{$slug4}}</a>]
                                                                    @include('manage.cms.categorias._sitemap_cursos', ['ficha'=> $h4])
                                                                </li>
                                                            </ul>
                                                            @endforeach
                                                        @endif

                                                    </li>
                                                </ul>
                                                @endforeach
                                            @endif

                                        </li>
                                    </ul>
                                    @endforeach
                                @endif
                            </li>
                        @endforeach
                        </ul>



                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
@stop

@section('extra_footer')
<script type="text/javascript">
$(document).ready(function($){
    $('#color').iris();
    $('#color_fondo').iris();
    $('#color_texto').iris();
    $('#color_menu').iris();

    if('{{$ficha->categorias}}' == '0'){
        $('#filtro-categoriasg').multiselect('selectAll', false);
        $('#filtro-categoriasg').multiselect('updateButtonText');
    }
    if('{{$ficha->subcategorias}}' == '0'){
        $('#filtro-subcategoriasg').multiselect('selectAll', false);
        $('#filtro-subcategoriasg').multiselect('updateButtonText');
    }
    if('{{$ficha->subcategoriasdet}}' == '0'){
        $('#filtro-subcategoriasdetg').multiselect('selectAll', false);
        $('#filtro-subcategoriasdetg').multiselect('updateButtonText');
    }

    //Recargamos los combos para dejar solo las opciones de las categorías seleccionadas
    $('#filtro-subcategoriasg').trigger('change');
    //$('#filtro-categoriasg').trigger('change');

    var subcategoriasdetselected =new Array(<?php echo implode(',', explode(',',$ficha->subcategoriasdet)); ?>);
    $('#filtro-subcategoriasdetg').multiselect('select', ["3", "7", "5", "2", "4", "8", "6", "30", "26", "25"]);


});

</script>

@stop