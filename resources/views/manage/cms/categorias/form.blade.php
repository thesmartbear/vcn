<div class="form-group row">
    <div class="col-md-3">
        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
    </div>
    <div class="col-md-4">
        @include('includes.form_input_text', [ 'campo'=> 'titulo', 'texto'=> 'Título Menú'])
    </div>
    <div class="col-md-1">
        @include('includes.form_select', [ 'campo'=> 'estructura', 'texto'=> 'Estructura', 'select'=> [1=> "v.1", 2=> "v.2"]])
    </div>
    <div class="col-md-1">
        @include('includes.form_input_number', [ 'campo'=> 'orden', 'texto'=> 'Orden'])
    </div>
    <div class="col-md-1">
        @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activa'])
    </div>

    <div class="col-md-2">
        @include('includes.form_checkbox', [ 'campo'=> 'menu_principal', 'texto'=> 'Menú principal'])
        @include('includes.form_checkbox', [ 'campo'=> 'menu_secundario', 'texto'=> 'Menú secundario'])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-4">
        @include('includes.form_select', [ 'campo'=> 'category_id', 'texto'=> 'Categoría Web Padre', 'select'=> $padres])
    </div>
    {{-- <div class="col-md-2">
        @include('includes.form_checkbox', [ 'campo'=> 'menu_paises', 'texto'=> 'Menú paises'])
    </div> --}}
</div>

<hr>

<div class="form-group row">
    <div class="col-md-4">
        @include('includes.form_input_text', [ 'campo'=> 'seo_url', 'texto'=> 'Url SEO'])
    </div>
    <div class="col-md-8">
        @include('includes.form_input_text', [ 'campo'=> 'seo_titulo', 'texto'=> 'Título SEO'])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-8">
        @include('includes.form_textarea', [ 'campo'=> 'seo_descripcion', 'texto'=> 'Descripción SEO'])
    </div>
    <div class="col-md-4">
        @include('includes.form_input_text', [ 'campo'=> 'seo_keywords', 'texto'=> 'Keywords SEO'])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-12">
        @include('includes.form_textarea_tinymce', [ 'campo'=> 'descripcion', 'texto'=> 'Descripción antes de cursos'])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-6">
        @include('includes.form_textarea_tinymce', [ 'campo'=> 'desc_corta', 'texto'=> 'Descripción corta'])
    </div>
    <div class="col-md-6">
        @include('includes.form_textarea_tinymce', [ 'campo'=> 'desc_lateral', 'texto'=> 'Descripción lateral'])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-12">
        @include('includes.form_textarea_tinymce', [ 'campo'=> 'desc_cursos', 'texto'=> 'Descripción debajo cursos'])
    </div>
</div>

<hr>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-home fa-fw"></i> Home

        <div class="pull-right">
            @include('includes.form_checkbox', [ 'campo'=> 'es_home', 'texto'=> 'Activa Home'])
        </div>
    </div>
    <div class="panel-body">

        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'home_titulo', 'texto'=> 'Título Bloque intro'])
            </div>
            <div class="col-md-3">
                @include('includes.form_input_text', [ 'campo'=> 'home_titulo_link', 'texto'=> 'URL Título'])
            </div>
            <div class="col-md-2">
                {{-- @include('includes.form_select', [ 'campo'=> 'home_titulo_pos', 'texto'=> 'Posición Título', 'select'=> [0=> 'Centro', 1=> 'Izquierda', 2=> 'Derecha']]) --}}
                @include('includes.form_select', [ 'campo'=> 'home_bloque', 'texto'=> 'Tipo Bloque', 'select'=> ConfigHelper::getTipoBloqueHome()])
            </div>
            <div class="col-md-2">
                {!! Form::label('home_bloque_height', 'Altura Bloque') !!}: (???)
                {{-- @include('includes.form_select', [ 'campo'=> 'home_bloque_height', 'texto'=> 'Altura Bloque', 'select'=> ConfigHelper::getTipoBloqueHomeHeight()]) --}}
            </div>
            <div class="col-md-1">
                @include('includes.form_checkbox', [ 'campo'=> 'home_bloque_slide', 'texto'=> 'Slide'])
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-3">
                @include('includes.form_input_text', [ 'campo'=> 'color_fondo', 'texto'=> 'Color Fondo (#)'])
            </div>
            <div class="col-md-3">
                @include('includes.form_input_text', [ 'campo'=> 'color_texto', 'texto'=> 'Color Texto (#)'])
            </div>
            <div class="col-md-3">
                @include('includes.form_input_text', [ 'campo'=> 'color_menu', 'texto'=> 'Color Menú (#)'])
            </div>
        </div>

        <div class="form-group">
            @include('includes.form_textarea_tinymce', [ 'campo'=> 'home_titulo2', 'texto'=> 'Descripción corta'])
        </div>

        <div class="form-group row">
            <div class="col-md-2">
                @include('includes.form_input_text', [ 'campo'=> 'home_boton', 'texto'=> 'Botón'])
            </div>
            <div class="col-md-2">
                @include('includes.form_checkbox', [ 'campo'=> 'home_boton_activo', 'texto'=> 'Botón activo'])
            </div>
            <div class="col-md-2">
                @include('includes.form_checkbox', [ 'campo'=> 'es_link', 'texto'=> 'Tipo enlace'])
            </div>
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'link', 'texto'=> 'URL enlace'])
            </div>
            <div class="col-md-2">
                @include('includes.form_checkbox', [ 'campo'=> 'link_blank', 'texto'=> 'Nueva ventana'])
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-3">
                @include('includes.form_input_file', [ 'campo'=> 'imagen', 'texto'=> 'Imagen'])
            </div>
            <div class="col-md-3">
                @include('includes.form_input_file', [ 'campo'=> 'home_imagen', 'texto'=> 'Imagen Home intro'])
            </div>
            <div class="col-md-3">
                @include('includes.form_input_file', [ 'campo'=> 'video', 'texto'=> 'Vídeo'])
                [{{$ficha->video_url ?: "no-video"}}]
            </div>
        </div>

        <div class="form-group row">
            {{-- <div class="col-md-2">
                @include('includes.form_select', [ 'campo'=> 'home_imagen_pos', 'texto'=> 'Posición', 'select'=> [0=> 'Fondo', 1=> 'Izquierda', 2=> 'Derecha']])
            </div> --}}
            <div class="col-md-3">
                @include('includes.form_select', [ 'campo'=> 'plantilla', 'texto'=> 'Plantilla', 'select'=> ConfigHelper::getCategoriaPlantilla()])
            </div>
            <div class="col-md-2">
                @include('includes.form_input_text', [ 'campo'=> 'color', 'texto'=> 'Color (#)'])
            </div>
        </div>

        {{-- <div class="form-group row">
            <div class="col-md-6">
                @include('includes.form_input_text', [ 'campo'=> 'video_url', 'texto'=> 'Vídeo URL (local)'])
            </div>
        </div> --}}

        <div class="form-group row">
            <div class="col-md-2">
                @include('includes.form_plataforma', ['campo'=> 'home_propietario', 'todas'=> true])
            </div>
            <div class="col-md-6">
                @include('includes.form_checkbox', [ 'campo'=> 'cerrado', 'texto'=> 'Ocultar Grupo Cerrado'])
            </div>
        </div>


    </div>
</div>


<hr>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-filter fa-fw"></i> Filtros
    </div>
    <div class="panel-body">

        <div class="form-group row">
            <div class="col-md-2">
                {!! Form::label('propietario', 'Plataforma') !!}
                <br>
                {!! Form::select('propietario', $plataformas, $ficha->propietario, array('class'=>'select2', 'id'=>'filtro-plataformas'))  !!}
                @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categorias'])
            </div>
            <div class="col-md-2">
                @include('includes.form_select', [ 'campo'=> 'tipoc', 'texto'=> 'Tipo Convocatoria', 'select'=> ConfigHelper::getConvocatoriaTipo()])
            </div>
            <div class="col-md-2">
                {!! Form::label('idioma', 'Idioma*') !!}
                <br>
                {!! Form::select('idioma', ConfigHelper::getIdioma(), explode(',',$ficha->idioma), array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'idioma[]'))  !!}
                <span class="help-block">{{ $errors->first('idioma') }}</span>
            </div>
            <div class="col-md-3">
                <label for='excluye'>Incluye/Excluye Idioma:</label>
                <div class="form-radio">
                    <label class="radio-inline">
                        {!! Form::radio('idioma_excluye', 0, ($ficha->idioma_excluye==0)) !!} Incluye
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('idioma_excluye', 1, ($ficha->idioma_excluye==1)) !!} Excluye
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-2">
                @include('includes.form_select', [ 'campo'=> 'pais_id', 'texto'=> 'País', 'select'=> [''=>''] + \VCN\Models\Pais::orderBy('name')->pluck('name','id')->toArray()])
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-4">
                {!! Form::label('categoriasg', 'Categoría') !!}
                @include('includes.form_input_cargando',['id'=> 'categoriasg-cargando'])
                <br>
                {!! Form::select('categoriasg', $categorias, explode(',',$ficha->categorias), array('class'=> 'form-control filtro-cms', 'multiple'=>'multiple', 'id'=>'filtro-categoriasg', 'name'=> 'categorias[]'))  !!}
                @include('includes.script_filtros_multi_cms', ['filtro'=> 'categoriasg', 'destino'=> 'subcategoriasg'])
                @include('includes.script_filtros_multi_cms', ['filtro'=> 'categoriasg', 'destino'=> 'subcategoriasdetg'])
            </div>
            <div class="col-md-3">
                <label for='excluye_categorias'>Incluye/Excluye:</label>
                <div class="form-radio">
                    <label class="radio-inline">
                        {!! Form::radio('excluye_categorias', 0, ($ficha->excluye_categorias==0)) !!} Incluye
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('excluye_categorias', 1, ($ficha->excluye_categorias==1)) !!} Excluye
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-4">
                {!! Form::label('subcategoriasg', 'SubCategoría') !!}
                @include('includes.form_input_cargando',['id'=> 'subcategoriasg-cargando'])
                <br>
                {!! Form::select('subcategoriasg', $subcategorias, explode(',',$ficha->subcategorias), array('class'=> 'form-control filtro-cms', 'multiple'=>'multiple', 'id'=>'filtro-subcategoriasg', 'name'=> 'subcategorias[]'))  !!}
                @include('includes.script_filtros_multi_cms', ['filtro'=> 'subcategoriasg', 'destino'=> 'subcategoriasdetg'])
            </div>
            <div class="col-md-3">
                <label for='excluye_subcategorias'>Incluye/Excluye:</label>
                <div class="form-radio">
                    <label class="radio-inline">
                        {!! Form::radio('excluye_subcategorias', 0, ($ficha->excluye_subcategorias==0)) !!} Incluye
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('excluye_subcategorias', 1, ($ficha->excluye_subcategorias==1)) !!} Excluye
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4">
                {!! Form::label('subcategoriasdetg', 'SubCategoría Detalle') !!}
                @include('includes.form_input_cargando',['id'=> 'subcategoriasdetg-cargando'])
                <br>
                {!! Form::select('subcategoriasdetg', $subcategoriasdet, explode(',',$ficha->subcategoriasdet), array('class'=> 'form-control filtro-cms', 'multiple'=>'multiple', 'id'=>'filtro-subcategoriasdetg', 'name'=> 'subcategoriasdet[]'))  !!}
            </div>
            <div class="col-md-3">
                <label for='excluye_subcategoriasdet'>Incluye/Excluye:</label>
                <div class="form-radio">
                    <label class="radio-inline">
                        {!! Form::radio('excluye_subcategoriasdet', 0, ($ficha->excluye_subcategoriasdet==0)) !!} Incluye
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('excluye_subcategoriasdet', 1, ($ficha->excluye_subcategoriasdet==1)) !!} Excluye
                    </label>
                </div>
            </div>
        </div>

    </div>
</div>