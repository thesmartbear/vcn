@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.cms.catalogos.index') !!} --}}
@stop

@section('titulo')
    <i class="fa fa-file-pdf-o fa-fw"></i> Catálogos
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-file-pdf-o fa-fw"></i> Catálogos
            <span class="pull-right"><a href="{{ route('manage.cms.catalogos.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Catálogo</a></span>
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'grupo' => 'Grupo',
                  'name' => 'Nombre',
                  'plataforma' => 'Plataforma',
                  'idioma' => 'Idioma',
                  'activo' => 'Activo',
                  'options' => ''
                ])
                ->setUrl(route('manage.cms.catalogos.index'))
                ->setOptions(
                  "aoColumnDefs", array(
                    //[ "bSortable" => false, "aTargets" => [2,3,4] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

@stop