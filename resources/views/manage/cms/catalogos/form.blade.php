<div class="form-group row">
    <div class="col-md-4">
        @include('includes.form_input_text', [ 'campo'=> 'grupo', 'texto'=> 'Grupo', 'help'=> "Se agrupan por grupo"])
    </div>
    <div class="col-md-6">
        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
    </div>
    <div class="col-md-2">
        @include('includes.form_select', [ 'campo'=> 'idioma', 'texto'=> 'Idioma', 'select'=> ConfigHelper::getIdiomaContacto()])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-4">
        @include('includes.form_input_file', [ 'campo'=> "pdf", 'texto'=> 'PDF'])
    </div>
    <div class="col-md-4">
        @include('includes.form_input_file', [ 'campo'=> "imagen", 'texto'=> 'Preview'])
    </div>
</div>

{{-- <div id="contenido" class="form-group">
    @include('includes.form_textarea_tinymce', [ 'campo'=> 'notas', 'texto'=> 'Notas'])
</div> --}}

<div class="form-group row">
    <div class="col-md-4">
        @include('includes.form_plataforma', ['campo'=> 'plataforma_id', 'todas'=> false])
    </div>
    <div class="col-md-4">
        @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activo'])
    </div>
</div>