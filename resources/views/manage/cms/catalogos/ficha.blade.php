@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-money fa-fw"></i> Catálogo :: {{$ficha->name}}
    </div>
    <div class="panel-body">

        {!! Form::open(array('method' => 'POST', 'files'=> true, 'url' => route('manage.cms.catalogos.ficha',$ficha->id), 'role' => 'form', 'class' => '')) !!}

            @include('manage.cms.catalogos.form')

            @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

@stop