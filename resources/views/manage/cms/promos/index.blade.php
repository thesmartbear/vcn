@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.cms.promos.index') !!}
@stop

@section('titulo')
    <i class="fa fa-gift fa-fw"></i> Promos
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-gift fa-fw"></i> Promos
                <span class="pull-right"><a href="{{ route('manage.cms.promos.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Promo</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name' => 'Nombre',
                      'seccion' => 'Seccion',
                      'orden'  => 'Orden',
                      'activo'  => 'Activo',
                      'options' => ''
                    ])
                    ->setUrl(route('manage.cms.promos.index'))
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [3] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop