@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-gift fa-fw"></i> Nueva Promo
    </div>
    <div class="panel-body">


        {!! Form::open(array('method' => 'POST', 'files'=>true, 'url' => route('manage.cms.promos.ficha',0), 'role' => 'form', 'class' => '')) !!}

            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_select', [ 'campo'=> 'seccion_id', 'texto'=> 'Sección', 'select'=> $categorias])
                </div>
                <div class="col-md-3">
                    @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activa'])
                </div>
                <div class="col-md-1">
                    @include('includes.form_input_text', [ 'campo'=> 'orden', 'texto'=> 'Orden'])
                </div>
            </div>

            <hr>

            <div class="form-group row">
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'promo1_titulo', 'texto'=> 'Promo1 Título'])
                </div>
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'promo1_url', 'texto'=> 'Promo1 URL'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'promo2_titulo', 'texto'=> 'Promo2 Título'])
                </div>
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'promo2_url', 'texto'=> 'Promo2 URL'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'promo3_titulo', 'texto'=> 'Promo3 Título'])
                </div>
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'promo3_url', 'texto'=> 'Promo3 URL'])
                </div>
            </div>


            <div id="panel_div" class="panel panel-default">
                <div class="panel-heading">Panel Promos (Home)</div>
                <div class="panel-body">
                    <div class="row msj">
                        <div class="col-md-12">
                            <h4 class="text-danger">Los valores de color, imagen y/o video se mostrarán en el panel de promos en lugar de los valores por defecto de la categoría seleccionada.</h4>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            @include('includes.form_input_text', [ 'campo'=> 'panel_titulo', 'texto'=> 'Título'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'panel_color', 'texto'=> 'Color (#)'])
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 panel_url">
                            @include('includes.form_input_text', [ 'campo'=> 'panel_url', 'texto'=> 'URL'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_checkbox', [ 'campo'=> 'targetblank_panel', 'texto'=> 'Nueva ventana'])
                        </div>
                        <div class="col-md-3 promo_todas">
                            @include('includes.form_checkbox', [ 'campo'=> 'promo_todas', 'texto'=> 'Link a todas las promos'])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_file', [ 'campo'=> 'panel_imagen', 'texto'=> 'Imagen'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'video_url', 'texto'=> 'Vídeo Url'])
                    </div>

                </div>
            </div>


            @include('includes.form_plataforma', ['campo'=> 'propietario', 'todas'=> true])

            @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {

    $('#promo_todas').change( function() {
        var $v = $(this).is(':checked');
        if($v)
        {
            $('#panel_url').val('cursos-en-promocion.html');
            $('#panel_url').attr('readonly','readonly');
            $('#ca_panel_url').val('cursos-en-promocio.html');
            $('#ca_panel_url').attr('readonly','readonly');
        }
        else
        {
            $('#panel_url').val('');
            $('#panel_url').removeAttr('readonly');

            $('#ca_panel_url').val('');
            $('#ca_panel_url').removeAttr('readonly');
        }
    });

    $('#seccion_id').change( function() {

        if($(this).val()!=0)
        {
            $('#panel_titulo').val($('#seccion_id').children(':selected').text());
            $('#panel_titulo').attr('readonly','readonly');
            $('#panel_url').val('');
            $('#panel_url').attr('readonly','readonly');
            $('.panel_url').hide();
            $(".promo_todas").hide();
            $('.msj').show();
        }
        else
        {
            $('#panel_titulo').val('');
            $('#panel_titulo').removeAttr('readonly');
            $('#panel_url').removeAttr('readonly');
            $('.panel_url').show();
            $(".promo_todas").show();
            $('.msj').hide();
        }

    });

    $('#seccion_id').trigger('change');

    $('#panel_color').iris();
});
</script>

@stop
