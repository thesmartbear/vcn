@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-money fa-fw"></i> Especialidad Web :: {{$ficha->name}} [Nivel: {{$ficha->nivel}}]
    </div>
    <div class="panel-body">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Especialidad Web</a></li>
            <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                {!! Form::open(array('method' => 'POST', 'files'=> true, 'url' => route('manage.cms.especialidades.ficha',$ficha->id), 'role' => 'form', 'id' => 'cms-especialidades')) !!}

                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'titulo', 'texto'=> 'Título'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_number', [ 'campo'=> 'orden', 'texto'=> 'Orden'])
                        </div>

                        <div class="col-md-3">
                            @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activa'])
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'seo_url', 'texto'=> 'Url SEO'])
                        </div>
                        <div class="col-md-8">
                            @include('includes.form_input_text', [ 'campo'=> 'seo_titulo', 'texto'=> 'Título SEO'])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-8">
                            @include('includes.form_textarea', [ 'campo'=> 'seo_descripcion', 'texto'=> 'Descripción SEO'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'seo_keywords', 'texto'=> 'Keywords SEO'])
                        </div>
                    </div>

                <hr>

                <div class="form-group row">
                    <div class="col-md-6 row">
                        <div class="col-md-6">
                            @include('includes.form_select', [ 'campo'=> 'inscripcion', 'texto'=> 'Hoja de Inscripción / Condiciones', 'select'=> ConfigHelper::getHojaInscripcion()])
                        </div>
                        <div class="col-md-6">
                            @include('includes.form_checkbox', [ 'campo'=> 'cerrado', 'texto'=> 'Ocultar Grupo Cerrado'])
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-8">
                            @include('includes.form_select', [ 'campo'=> 'plantilla', 'texto'=> 'Plantilla', 'select'=> ConfigHelper::getCategoriaPlantilla()])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'color', 'texto'=> 'Color (#)'])
                        </div>
                        <div class="col-md-12">
                            @include('includes.form_input_text', [ 'campo'=> 'video_url', 'texto'=> 'Vídeo URL (local)'])
                        </div>

                    </div>
                    <div class="col-md-6 row">
                        <div class="col-md-6">
                            @if($ficha->imagen)
                                <img src="{{$ficha->imagen}}" class="img-responsive thumbnail">
                            @endif
                            <br />
                            @include('includes.form_input_file', [ 'campo'=> 'imagen', 'texto'=> 'Imagen'])
                        </div>

                    </div>
                </div>

                <hr>

                    <div class="form-group row">
                        <div class="col-md-12">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'descripcion', 'texto'=> 'Descripción'])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'desc_corta', 'texto'=> 'Descripción corta'])
                        </div>
                        <div class="col-md-6">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'desc_lateral', 'texto'=> 'Descripción lateral'])
                        </div>
                    </div>

                    <hr>


                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'especialidad_id', 'texto'=> 'Especialidad Web Padre', 'select'=> $padres])
                    </div>

                    <hr>
                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'es_link', 'texto'=> 'Tipo enlace'])
                        </div>
                        <div class="col-md-8">
                            @include('includes.form_input_text', [ 'campo'=> 'link', 'texto'=> 'URL enlace'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'link_blank', 'texto'=> 'Nueva ventana'])
                        </div>
                    </div>
                    <hr>

                    <div class="form-group row">
                        <div class="col-md-2">
                            {!! Form::label('propietario', 'Plataforma') !!}
                            <br>
                            {!! Form::select('propietario', $plataformas, $ficha->propietario, array('class'=>'select2', 'id'=>'filtro-plataformas'))  !!}
                            @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categorias'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select', [ 'campo'=> 'tipoc', 'texto'=> 'Tipo Convocatoria', 'select'=> ConfigHelper::getConvocatoriaTipo()])
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('idioma', 'Idioma*') !!}
                            <br>
                            {!! Form::select('idioma', ConfigHelper::getIdioma(), explode(',',$ficha->idioma), array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'idioma[]'))  !!}
                        </div>
                        <div class="col-md-3">
                            <label for='excluye'>Incluye/Excluye Idioma:</label>
                            <div class="form-radio">
                                <label class="radio-inline">
                                    {!! Form::radio('idioma_excluye', 0, ($ficha->idioma_excluye==0)) !!} Incluye
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('idioma_excluye', 1, ($ficha->idioma_excluye==1)) !!} Excluye
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            {!! Form::label('especialidadesg', 'Especialidad') !!}
                            @include('includes.form_input_cargando',['id'=> 'especialidadesg-cargando'])
                            <br>
                            {!! Form::select('especialidadesg', $especialidades, explode(',',$ficha->especialidades), array('class'=> 'form-control filtro-cms', 'multiple'=>'multiple', 'id'=>'filtro-especialidadesg', 'name'=> 'especialidades[]'))  !!}
                            @include('includes.script_filtros_multi_cms', ['filtro'=> 'especialidadesg', 'destino'=> 'subespecialidadesg'])
                        </div>
                        <div class="col-md-3">
                            <label for='excluye_especialidades'>Incluye/Excluye:</label>
                            <div class="form-radio">
                                <label class="radio-inline">
                                    {!! Form::radio('excluye_especialidades', 0, true) !!} Incluye
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('excluye_especialidades', 1, false) !!} Excluye
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            {!! Form::label('subespecialidadesg', 'SubEspecialidad') !!}
                            @include('includes.form_input_cargando',['id'=> 'subespecialidadesg-cargando'])
                            <br>
                            {!! Form::select('subespecialidadesg', $subespecialidades, explode(',',$ficha->subespecialidades), array('class'=> 'form-control filtro-cms', 'multiple'=>'multiple', 'id'=>'filtro-subespecialidadesg', 'name'=> 'subespecialidades[]'))  !!}
                        </div>
                        <div class="col-md-3">
                            <label for='excluye_subespecialidades'>Incluye/Excluye:</label>
                            <div class="form-radio">
                                <label class="radio-inline">
                                    {!! Form::radio('excluye_subespecialidades', 0, true) !!} Incluye
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('excluye_subespecialidades', 1, false) !!} Excluye
                                </label>
                            </div>
                        </div>
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                @include('includes.traduccion-tab',
                        ['modelo'=> 'EspecialidadWeb',
                        'campos_text'=> [
                            ['name'=> 'Nombre'],
                            ['titulo'=> 'Título'],
                            ['seo_url'=> 'Url SEO'],
                            ['seo_titulo'=> 'Título SEO'],
                            ['seo_keywords'=> 'Keywords SEO'],
                            ['link'=> 'URL Enlace'],
                        ],
                        'campos_textarea'=> [
                            ['seo_descripcion'=> 'Descripción SEO'],
                            ['descripcion'=> 'Descripción'],
                            ['desc_corta'=> 'Descripción corta'], ['desc_lateral'=> 'Descripción lateral'],
                        ]
                    ])

            </div>

        </div>

    </div>
</div>
@stop

@section('extra_footer')
<script type="text/javascript">
$(document).ready(function($){
    $('#color').iris();

    if('{{$ficha->categorias}}' == '0'){
        $('#filtro-categoriasg').multiselect('selectAll', false);
        $('#filtro-categoriasg').multiselect('updateButtonText');
    }
    if('{{$ficha->subcategorias}}' == '0'){
        $('#filtro-subcategoriasg').multiselect('selectAll', false);
        $('#filtro-subcategoriasg').multiselect('updateButtonText');
    }
    if('{{$ficha->subcategoriasdet}}' == '0'){
        $('#filtro-subcategoriasdetg').multiselect('selectAll', false);
        $('#filtro-subcategoriasdetg').multiselect('updateButtonText');
    }

    //Recargamos los combos para dejar solo las opciones de las categorías seleccionadas
    $('#filtro-subcategoriasg').trigger('change');
    //$('#filtro-categoriasg').trigger('change');

    var subcategoriasdetselected =new Array(<?php echo implode(',', explode(',',$ficha->subcategoriasdet)); ?>);
    $('#filtro-subcategoriasdetg').multiselect('select', ["3", "7", "5", "2", "4", "8", "6", "30", "26", "25"]);


});

</script>

@stop