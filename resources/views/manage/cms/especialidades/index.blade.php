@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.cms.especialidades.index') !!}
@stop

@section('titulo')
    <i class="fa fa-tag fa-fw"></i> Especialidades Web
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tag fa-fw"></i> Especialidades Web
                <span class="pull-right"><a href="{{ route('manage.cms.especialidades.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Especialidad Web</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'orden'   => 'Orden',
                      'name'    => 'Nombre',
                      'padre'   => 'Espec.Padre',
                      'nivel'   => 'Nivel',
                      'titulo'  => 'Título',
                      'idioma'  => 'Idioma',
                      'plataforma'  => 'Plataforma',
                      'tipo_enlace'  => 'Tipo Enlace',
                      'activo'  => 'Activo',
                      'options' => ''
                    ])
                    ->setUrl(route('manage.cms.especialidades.index'))
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [9] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop