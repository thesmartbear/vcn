@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-tag fa-fw"></i> Nueva Especialidad Web
    </div>
    <div class="panel-body">


        {!! Form::open(array('method' => 'POST', 'files'=>true, 'url' => route('manage.cms.especialidades.ficha',0), 'role' => 'form', 'class' => '')) !!}

            <div class="form-group row">
                <div class="col-md-3">
                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'titulo', 'texto'=> 'Título'])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_number', [ 'campo'=> 'orden', 'texto'=> 'Orden'])
                </div>
                <div class="col-md-3">
                    @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activa'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'seo_url', 'texto'=> 'Url SEO'])
                </div>
                <div class="col-md-8">
                    @include('includes.form_input_text', [ 'campo'=> 'seo_titulo', 'texto'=> 'Título SEO'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-8">
                    @include('includes.form_textarea', [ 'campo'=> 'seo_descripcion', 'texto'=> 'Descripción SEO'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'seo_keywords', 'texto'=> 'Keywords SEO'])
                </div>
            </div>

        <hr>

        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_select', [ 'campo'=> 'inscripcion', 'texto'=> 'Hoja de Inscripción / Condiciones', 'select'=> ConfigHelper::getHojaInscripcion()])
            </div>
            <div class="col-md-4">
                @include('includes.form_checkbox', [ 'campo'=> 'cerrado', 'texto'=> 'Ocultar Grupo Cerrado'])
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <div class="col-md-6 row">

                <div class="col-md-8">
                    @include('includes.form_select', [ 'campo'=> 'plantilla', 'texto'=> 'Plantilla', 'select'=> ConfigHelper::getCategoriaPlantilla()])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'color', 'texto'=> 'Color (#)'])
                </div>
                <div class="col-md-12">
                    @include('includes.form_input_text', [ 'campo'=> 'video_url', 'texto'=> 'Vídeo URL (local)'])
                </div>
            </div>


            <div class="col-md-6 row">
                <div class="col-md-12">
                    @include('includes.form_input_file', [ 'campo'=> 'imagen', 'texto'=> 'Imagen'])
                </div>
            </div>
        </div>

        <hr>

        <div class="form-group row">
                <div class="col-md-12">
                    @include('includes.form_textarea_tinymce', [ 'campo'=> 'descripcion', 'texto'=> 'Descripción'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    @include('includes.form_textarea_tinymce', [ 'campo'=> 'desc_corta', 'texto'=> 'Descripción corta'])
                </div>
                <div class="col-md-6">
                    @include('includes.form_textarea_tinymce', [ 'campo'=> 'desc_lateral', 'texto'=> 'Descripción lateral'])
                </div>
            </div>


            <hr>

            <div class="form-group">
                @include('includes.form_select', [ 'campo'=> 'especialidad_id', 'texto'=> 'Especialidad Web Padre', 'select'=> $padres])
            </div>

            <hr>
            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'es_link', 'texto'=> 'Tipo enlace'])
                </div>
                <div class="col-md-8">
                    @include('includes.form_input_text', [ 'campo'=> 'link', 'texto'=> 'URL enlace'])
                </div>
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'link_blank', 'texto'=> 'Nueva ventana'])
                </div>
            </div>
            <hr>

            <div class="form-group row">
                <div class="col-md-2">
                    {!! Form::label('propietario', 'Plataforma') !!}
                    <br>
                    {!! Form::select('propietario', $plataformas, $plataforma, array('class'=>'select2', 'id'=>'filtro-plataformas'))  !!}
                    {{-- @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categorias']) --}}
                </div>
                <div class="col-md-2">
                    @include('includes.form_select', [ 'campo'=> 'tipoc', 'texto'=> 'Tipo Convocatoria', 'select'=> ConfigHelper::getConvocatoriaTipo()])
                </div>
                <div class="col-md-2">
                    {!! Form::label('idioma', 'Idioma*') !!}
                    <br>
                    {!! Form::select('idioma', ConfigHelper::getIdioma(), '', array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'name'=> 'idioma[]'))  !!}
                </div>
                <div class="col-md-3">
                    <label for='excluye'>Incluye/Excluye Idioma:</label>
                    <div class="form-radio">
                        <label class="radio-inline">
                            {!! Form::radio('idioma_excluye', 0, true) !!} Incluye
                        </label>
                        <label class="radio-inline">
                            {!! Form::radio('idioma_excluye', 1, false) !!} Excluye
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    {!! Form::label('especialidadesg', 'Especialidad') !!}
                    @include('includes.form_input_cargando',['id'=> 'especialidadesg-cargando'])
                    <br>
                    {!! Form::select('especialidadesg', $especialidades, '', array('class'=> 'form-control filtro-cms', 'multiple'=>'multiple', 'id'=>'filtro-especialidadesg', 'name'=> 'especialidades[]'))  !!}
                    @include('includes.script_filtros_multi_cms', ['filtro'=> 'especialidadesg', 'destino'=> 'subespecialidadesg'])
                </div>
                <div class="col-md-3">
                    <label for='excluye_especialidades'>Incluye/Excluye:</label>
                    <div class="form-radio">
                        <label class="radio-inline">
                            {!! Form::radio('excluye_especialidades', 0, true) !!} Incluye
                        </label>
                        <label class="radio-inline">
                            {!! Form::radio('excluye_especialidades', 1, false) !!} Excluye
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    {!! Form::label('subespecialidadesg', 'SubEspecialidad') !!}
                    @include('includes.form_input_cargando',['id'=> 'subespecialidadesg-cargando'])
                    <br>
                    {!! Form::select('subespecialidadesg', $subespecialidades, '', array('class'=> 'form-control filtro-cms', 'multiple'=>'multiple', 'id'=>'filtro-subespecialidadesg', 'name'=> 'subespecialidades[]'))  !!}
                </div>
                <div class="col-md-3">
                    <label for='excluye_subespecialidades'>Incluye/Excluye:</label>
                    <div class="form-radio">
                        <label class="radio-inline">
                            {!! Form::radio('excluye_subespecialidades', 0, true) !!} Incluye
                        </label>
                        <label class="radio-inline">
                            {!! Form::radio('excluye_subespecialidades', 1, false) !!} Excluye
                        </label>
                    </div>
                </div>
            </div>

            @include('includes.form_submit', [ 'permiso'=> 'cms', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

<script type="text/javascript">
$(document).ready(function($){
    $('#color').iris();
});
</script>

@stop
