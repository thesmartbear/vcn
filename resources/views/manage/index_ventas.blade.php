@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.index.ventas') !!}
@stop


@section('content')

{{-- @if(!ConfigHelper::canView('informes'))
    - NO TIENE PERMISO -
@else --}}

<?php
$user = auth()->user();
?>

<ul class="nav nav-tabs" role="tablist">
    @if(ConfigHelper::canView('Resumen'))
    <li role="presentation"><a href="{{route('manage.index.resumen')}}">Resumen</a></li>
    @endif
    @if(ConfigHelper::canView('Tareas'))
    <li role="presentation"><a href="{{route('manage.index')}}">Tareas</a></li>
    @endif
    <li role="presentation" class="active"><a href="#ventas" aria-controls="ventas" role="tab" data-toggle="tab">Ventas</a></li>
    @if(ConfigHelper::canView('Incidencias'))
    <li role="presentation"><a href="{{route('manage.index')}}#incidencias">Incidencias ({{$user->incidencia_tareas->count()}})</a></li>
    @endif
    @if(ConfigHelper::canView('Contactos'))
    <li role="presentation"><a href="{{route('manage.index.contactos')}}">Contactos</a></li>
    @endif
</ul>

<div class="tab-content">

    <div role="tabpanel" class="tab-pane fade in active" id="ventas">

        <div class="portlet box grey">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-bar-chart"></i> Ventas
            </div>
            <div class="tools">
                <a href="" class="collapse"></a>
                <a href="" class="fullscreen"></a>
            </div>

        </div>
        <div class="portlet-body">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-filter"></i> Filtros <strong class="pull-right">(Estamos en la semana {{Carbon::now()->format('W')}})</strong> </div>
                <div class="panel-body">

                    {!! Form::open(['route' => array('manage.index.ventas'), 'method'=> 'GET', 'class' => 'form']) !!}

                        <div class="form-group row">
                            <div class="col-md-1">
                            {!! Form::label('any', 'Año') !!}
                            <br>
                            {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any'))  !!}
                            </div>

                            <div class="col-md-1">
                            {!! Form::label('semana', 'Semana') !!}
                            <br>
                            {!! Form::select('semana', $semanas, ($valores['semana']?:Carbon::now()->format('W')), array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-semana'))  !!}
                            </div>

                            @if(auth()->user()->isFullAdmin())
                            <div class="col-md-2">
                            {!! Form::label('plataformas', 'Plataforma') !!}
                            <br>
                            {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                            @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                            @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categoriasp'])
                            </div>
                            @endif

                            @if( auth()->user()->filtro_oficinas )
                            <div class="col-md-3">
                            {!! Form::label('oficinas', 'Oficina') !!}
                            <br>
                            @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                            {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'multiple'=>'multiple', 'id'=>'filtro-oficinas', 'name'=> 'oficinas[]'))  !!}

                            {{-- {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-oficinas'))  !!} --}}
                            </div>
                            @endif

                        </div>

                        <div class="form-group row">

                            <div class="col-md-2">
                            {!! Form::label('categorias', 'Categoría') !!}
                            @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                            <br>
                            {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'id'=>'filtro-categorias'))  !!}
                            @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                            </div>

                            <div class="col-md-4">
                            {!! Form::label('subcategorias', 'SubCategoría') !!}
                            @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                            <br>
                            {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'id'=>'filtro-subcategorias'))  !!}
                            </div>

                            <div class="col-md-1 col-md-offset-3">
                                {!! Form::label('&nbsp;') !!}
                                {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                            </div>

                        </div>

                    {!! Form::close() !!}

                </div>
            </div>

            <hr>

            @if(!$resultados)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        No hay datos que mostrar.
                    </div>
                </div>
            @else

                <div class="panel panel-default">
                    <div class="panel-heading">Bookings x Categoría (acumulado hasta semana seleccionada) -por fecha de inicio booking</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div id="chart_6"></div>
                                @columnchart('Chart-Bookings-Categoria', 'chart_6')
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Bookings x Mes</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div id="chart_9"></div>
                                @columnchart('Chart-Bookings-Mes', 'chart_9')
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Solicitudes x Categoría (acumulado hasta semana seleccionada)</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div id="chart_8"></div>
                                @columnchart('Chart-Solicitudes-Categoria', 'chart_8')
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Semanas x Categoría (acumulado hasta semana seleccionada) -por fecha de inicio booking</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div id="chart_7"></div>
                                @columnchart('Chart-Semanas-Categoria', 'chart_7')
                            </div>
                        </div>

                    </div>
                </div>

                <hr>

                <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Evolución semanal (Bookings)</div>
                        <div class="panel-body">
                            <div id="chart_4"></div>
                            @linechart('Chart-Bookings-Evolucion', 'chart_4')
                        </div>
                    </div>
                </div>
                </div>

                <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Evolución semanal (Solicitudes)</div>
                        <div class="panel-body">
                            <div id="chart_5"></div>
                            @linechart('Chart-Solicitudes-Evolucion', 'chart_5')
                        </div>
                    </div>
                </div>
                </div>

                <hr>

                <div class="panel panel-default">
                    <div class="panel-heading">Bookings y Semanas (para semanas seleccionadas)</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div id="chart_1"></div>
                                {!! Lava::render('ColumnChart', 'Chart-Bookings-1', 'chart_1') !!}
                                {{-- @columnchart('Chart-Bookings-1', 'chart_1') --}}
                            </div>
                            <div class="col-md-6">
                                <div id="chart_2"></div>
                                @columnchart('Chart-Bookings-2', 'chart_2')
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Bookings x Oficina (acumulado hasta semana seleccionada) - por fecha de inicio booking</div>
                    <div class="panel-body">
                        <div id="chart_3"></div>
                        @barchart('Chart-Bookings-Oficinas', 'chart_3')
                    </div>
                </div>

            @endif

        </div>
        </div>

    </div>

</div>
{{-- @endif --}}

@stop