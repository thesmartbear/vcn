@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tag fa-fw"></i> Especialidades
                <span class="pull-right"><a href="{{ route('manage.especialidades.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Especialidad</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Especialidad',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.especialidades.index'))
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [1] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop