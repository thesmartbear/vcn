@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.index') !!}
@stop


@section('content')

    <ul class="nav nav-tabs" role="tablist">
        
        @if(ConfigHelper::canView('Resumen'))
        <li role="presentation"><a href="{{route('manage.index.resumen')}}">Resumen</a></li>
        @endif
        @if(ConfigHelper::canView('Tareas'))
        <li role="presentation" class="active"><a href="#tasks" aria-controls="tasks" role="tab" data-toggle="tab">Tareas</a></li>
        @endif
        @if(ConfigHelper::canView('Ventas'))
        <li role="presentation"><a href="{{route('manage.index.ventas')}}">Bookings</a></li>
        @endif
        @if(ConfigHelper::canView('Incidencias'))
        <li role="presentation"><a href="#incidencias" aria-controls="incidencias" role="tab" data-toggle="tab">Incidencias ({{$user->incidencia_tareas->count()}})</a></li>
        @endif
        @if(ConfigHelper::canView('Contactos'))
        <li role="presentation"><a href="{{route('manage.index.contactos')}}">Contactos</a></li>
        @endif

    </ul>

    <div class="tab-content">

        @if(ConfigHelper::canView('Tareas'))
        <div role="tabpanel" class="tab-pane fade in active" id="tasks">

            <?php
                $o['all'] = 'TODAS';
                $o1 = ($user->oficina_id?$user->oficina_id:'all');
            ?>

            @if( !auth()->user()->es_aislado )
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-3">

                    @foreach($oficinas as $a)
                        <?php $o[$a->id] = $a->name; ?>
                    @endforeach

                    @if( auth()->user()->filtro_oficinas )
                        {!! Form::select('oficinas', $o, $o1, array('class'=>'select2 show-tick pull-right', 'data-style'=>'blue', 'id'=>'select-tareas-oficina'))  !!}
                    @endif

                </div>

                <div class="col-md-3">

                    <?php
                    foreach($users as $a)
                    {
                        $u[$a->id] = $a->full_name;
                    }
                    
                    $u['all'] = 'TODOS';

                    ?>
                    {{-- {!! Form::select('asignados', $u, $user->id, array('class'=>'selectpicker show-tick pull-right', 'data-style'=>'green', 'id'=>'select-tareas-filtro'))  !!} --}}

                    <select class="select2 show-tick pull-right" data-style="green" id="select-tareas-filtro" name="asignados">
                        @foreach($users as $a)
                            <option data-ofi='{{$a->oficina_id}}' value="{{$a->id}}">{{$a->full_name}}</option>
                        @endforeach
                        <option data-ofi='0' value="all">TODOS</option>
                        <option data-ofi='0' value="all-off">NO ACTIVOS</option>

                        <?php /*
                        //$asignadosOff = \VCN\Models\User::asignadosOff()->get();
                        
                        {{-- @if($asignadosOff->count())
                            <optgroup label="NO ACTIVOS:"></optgroup>
                            @foreach($asignadosOff as $a)
                                <option data-ofi='{{$a->oficina_id}}' value="{{$a->id}}">{{$a->full_name}}</option>
                            @endforeach
                        @endif --}}
                        */ ?>

                    </select>

                    @include('includes.script_filtro_tareas_users')

                </div>

            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <hr>
                    @include('manage.viajeros.tareas.list_resumen', ['filtro'=> 1, 'oficina_id'=>($o1 ?? 0), 'filtro_name'=> 'Caducadas', 'color_estado'=>'red'])
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    @include('manage.viajeros.tareas.list_resumen', ['filtro'=> 2, 'oficina_id'=>($o1 ?? 0), 'filtro_name'=> 'HOY', 'color_estado'=>'yellow'])
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    @include('manage.viajeros.tareas.list_resumen', ['filtro'=> 3, 'oficina_id'=>($o1 ?? 0), 'filtro_name'=> 'Esta semana', 'color_estado'=>'green'])
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    @include('manage.viajeros.tareas.list_resumen', ['filtro'=> 4, 'oficina_id'=>($o1 ?? 0), 'filtro_name'=> 'Más adelante', 'color_estado'=>'blue'])
                </div>
            </div>

            @include('manage.viajeros.tareas.edit')

        </div>
        @endif

        @if(ConfigHelper::canView('Incidencias'))
        <div role="tabpanel" class="tab-pane fade in" id="incidencias">

            <div class="row">
                <div class="col-md-12">
                    <hr>
                    @include('manage.bookings.incidencias.list_resumen', ['oficina_id'=>($o1 ?? 0), 'filtro_name'=> 'Tareas :: INCIDENCIAS', 'color_estado'=>'red-haze'])
                </div>
            </div>

        </div>
        @endif

    </div>

@stop