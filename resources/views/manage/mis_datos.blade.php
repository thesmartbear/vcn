@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.index.misdatos') !!}
@stop


@section('content')

    mis datos :: pendiente de ampliar...

    <hr>

<div class="portlet light bordered info">
    <div class="portlet-title">
        <div class="caption font-green-sharp">
            <i class="fa fa-user fa-fw"></i>
            <span class="caption-subject bold uppercase">{{$ficha->full_name}} <small>[{{$ficha->username}}]</small></span>
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
            <a href="" class="fullscreen"></a>
        </div>

    </div>

    <div class="portlet-body flip-scroll">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Mis Datos</a></li>
            <li role="presentation"><a href="#avatar" aria-controls="avatar" role="tab" data-toggle="tab">Foto</a></li>
        </ul>
    
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active ficha" id="ficha">
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="avatar">

                {!! Form::open( array('id'=> 'frmAvatar', 'method'=> 'post', 'files'=> true, 'url' => route('manage.index.misdatos')) ) !!}
                  
                    <div class="form-group row">  
                        <div class="col-md-4">
                            @include('includes.form_input_file', [ 'campo'=> 'avatar', 'texto'=> 'Foto'])
                        </div>
                        <div class="col-md-8">
                            @include('includes.webcam')
                        </div>
                    </div>

                    <div class="form-group pull-right">
                        {!! Form::submit("Borrar foto", array('name'=> 'submit_avatar_reset', 'class' => 'btn btn-warning')) !!}
                        {!! Form::submit(trans('area.actualizar'), array('name'=> 'submit_avatar', 'class' => 'btn btn-success')) !!}
                    </div>


                    <div class="clearfix"></div>

                {!! Form::close() !!}

            </div>

        </div>

    </div>
</div>


@stop