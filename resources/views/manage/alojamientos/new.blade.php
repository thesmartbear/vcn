@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.alojamientos.nuevo', $centro) !!}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-truck fa-fw"></i> Nuevo Alojamiento
            </div>
            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.alojamientos.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'center_id', 'texto'=> 'Centro', 'valor'=> $centro_id, 'select'=> $centros])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'accommodation_name', 'texto'=> 'Nombre'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'accommodation_type_id', 'texto'=> 'Tipo', 'valor'=> 0, 'select'=> $tipos])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'start_day', 'texto'=> 'Día Inicio', 'select'=> ConfigHelper::getDia()])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'end_day', 'texto'=> 'Día Fin', 'select'=> ConfigHelper::getDia()])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'accommodation_description', 'texto'=> 'Descripción'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'requisitos', 'texto'=> 'Requisitos especiales'])
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@if($centro_id>0)
<script type="text/javascript">
$(function() {
    $("#center_id").attr('readonly',true);
});
</script>
@endif


@stop