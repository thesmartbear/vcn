@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.alojamientos.index') !!}
@stop

@section('container')
    @include('manage.alojamientos.list', ['centro_id'=> $centro_id])
@stop