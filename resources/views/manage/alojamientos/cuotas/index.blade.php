@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.alojamientos.nuevo') !!}
@stop


@section('container')

        @include('manage.alojamientos.cuotas.list', ['alojamiento_id'=> $alojamiento_id])

@stop