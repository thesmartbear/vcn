@extends('layouts.manage')

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bed fa-fw"></i>
                    <a href="{{route('manage.alojamientos.ficha',$ficha->accommodation_id)}}">
                    Alojamiento: ({{$ficha->alojamiento->name}})
                    </a>
                    Precio :: {{$ficha->accommodations_quota_name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Extra</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('route' => array('manage.alojamientos.cuotas.ficha', $ficha->id))) !!}

                        {!! Form::hidden('accommodation_id', $ficha->accommodation_id) !!}

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'accommodations_quota_name', 'texto'=> 'Nombre'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'accommodations_quota_currency_id', 'texto'=> 'Moneda', 'select'=> $monedas])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'cuota_unidad', 'texto'=> 'Unidad', 'select'=> $unidades_tipo])
                        </div>

                        <div id="cuota_unidad_div" class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'cuota_unidad_id', 'texto'=> 'Tipo Unidad', 'select'=> $unidades])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'accommodations_quota_price', 'texto'=> 'Precio'])
                        </div>

                        <div class="form-group">
                            {!! Form::label('required', 'Obligatorio') !!}
                            {!! Form::checkbox("required", $ficha->required) !!}
                        </div>

                        <div class="form-group pull-right">
                            {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                            <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                        </div>

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'AlojamientoCuota',
                                'campos_text'=> [ ['accommodations_quota_name'=> 'Nombre'], ],
                                'campos_textarea'=> []
                            ])

                    </div>

                </div>

            </div>
        </div>

@include('includes.script_unidades', ['div'=>'#cuota_unidad' ])

@stop