@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bed fa-fw"></i>
                    <a href="{{route('manage.alojamientos.ficha',$alojamiento->id)}}">
                    Alojamiento: ({{$alojamiento->name}})
                    </a>
                    Cuota :: Nueva
            </div>
            <div class="panel-body">

                {!! Form::open( array('route' => array('manage.alojamientos.cuotas.ficha', 0))) !!}

                {!! Form::hidden('accommodation_id', $alojamiento->id) !!}

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'accommodations_quota_name', 'texto'=> 'Nombre'])
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'cuota_unidad', 'texto'=> 'Unidad', 'valor'=> 0, 'select'=> $unidades_tipo])
                </div>

                <div id="cuota_unidad_div" class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'cuota_unidad_id', 'texto'=> 'Tipo Unidad', 'valor'=> 0, 'select'=> $unidades])
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'accommodations_quota_currency_id', 'texto'=> 'Moneda', 'valor'=> $alojamiento->centro->moneda_id, 'select'=> $monedas])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'accommodations_quota_price', 'texto'=> 'Precio'])
                </div>

                <div class="form-group">
                    {!! Form::label('required', 'Obligatorio') !!}
                    {!! Form::checkbox("required") !!}
                </div>

                <div class="form-group pull-right">
                    {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                    <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                </div>

                {!! Form::close() !!}

            </div>
        </div>

@include('includes.script_unidades', ['div'=>'#cuota_unidad' ])

@stop