<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-money"></i> Cuotas

            <span class="pull-right"><a href="{{ route('manage.alojamientos.cuotas.nuevo', $alojamiento_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Cuota Alojamiento</a></span>

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'       => 'Nombre',
                  'precio'      => 'Precio',
                  'unidades'      => 'Unidades',
                  'requerido'   => 'Obligatorio',
                  'options'     => ''
                ])
                ->setUrl( route('manage.alojamientos.cuotas.index', $alojamiento_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [2] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>