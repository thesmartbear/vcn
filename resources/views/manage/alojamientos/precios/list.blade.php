
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-money"></i>

            @if($extras)
              Alojamiento Precios Extra temporada
            @else
              Alojamiento Precios
            @endif

        </div>
        <div class="panel-body">

          @if(ConfigHelper::canView('precios'))
            {!! Datatable::table()
                ->addColumn([
                  'name'        => 'Nombre',
                  'precio'      => 'Precio',
                  'regla'       => 'Regla',
                  'periodo'     => 'Validez',
                  'options'     => ''
                ])
                ->setUrl( route('manage.alojamientos.precios.index', [$alojamiento_id,$extras]) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [4] ]
                  )
                )
                ->render() !!}
          @else
            <div class="content">
            <div class="alert alert-warning" role="alert">
                No tiene permisos
            </div>
            </div>
          @endif

        </div>
    </div>


<hr>

@if(ConfigHelper::canEdit('precios'))
@include('includes.script_precios',
  [ 'route'=> route('manage.alojamientos.precios.ficha', [0,$extras]), 'extras'=> $extras,
    'campo'=> 'alojamiento_id', 'campo_id'=> $alojamiento_id, 'moneda_id'=> isset($ficha)?$ficha->centro->moneda_id:'', 'fechas'=> true])
@endif


