
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-bed fa-fw"></i> Alojamientos
            <span class="pull-right"><a href="{{ route('manage.alojamientos.nuevo', $centro_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Alojamiento</a></span>
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'            => 'Nombre',
                  'tipo'            => 'Tipo',
                  'centro'          => 'Centro',
                  'options'         => ''
                ])
                ->setUrl(route('manage.alojamientos.index', $centro_id))
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [3] ]
                  )
                )
                ->render() !!}

        </div>
    </div>
