
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-bed fa-fw"></i> Alojamientos
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'            => 'Nombre',
                  'tipo'            => 'Tipo',
                  'centro'          => 'Centro',
                  'options'         => ''
                ])
                ->setUrl(route('manage.alojamientos.index.curso', $curso_id))
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [3] ]
                  )
                )
                ->render() !!}

        </div>
    </div>
