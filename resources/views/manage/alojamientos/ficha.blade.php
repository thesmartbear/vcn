@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.alojamientos.ficha', $ficha) !!}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-truck fa-fw"></i> Alojamiento :: {{$ficha->accommodation_name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Alojamiento</a></li>
                    <li role="presentation"><a href="#precios" aria-controls="precios" role="tab" data-toggle="tab">Precios</a></li>
                    <li role="presentation"><a href="#precio-extras" aria-controls="precios" role="tab" data-toggle="tab">Precios Extra temporada</a></li>
                    <li role="presentation"><a href="#cuotas" aria-controls="cuotas" role="tab" data-toggle="tab">Cuotas</a></li>

                    <li role="presentation"><a href="#imagenes" aria-controls="imagenes" role="tab" data-toggle="tab">Imágenes</a></li>

                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panels -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('route' => array('manage.alojamientos.ficha', $ficha->id))) !!}

                        <div class="form-group">
                            {!! Form::label('center_id', 'Centro') !!}:
                            {!! Form::text('center_id', $ficha->centro->name, array('class' => 'form-control', 'disabled'=> 'disabled')) !!}
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'accommodation_name', 'texto'=> 'Nombre'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                @include('includes.form_select', [ 'campo'=> 'accommodation_type_id', 'texto'=> 'Tipo', 'valor'=> $ficha->accommodation_type_id, 'select'=> $tipos])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_select', [ 'campo'=> 'start_day', 'texto'=> 'Día Inicio', 'select'=> ConfigHelper::getDia()])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_select', [ 'campo'=> 'end_day', 'texto'=> 'Día Fin', 'select'=> ConfigHelper::getDia()])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'accommodation_description', 'texto'=> 'Descripción'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'requisitos', 'texto'=> 'Requisitos especiales'])
                        </div>

                        @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="precios">
                        @include('manage.alojamientos.precios.list', ['alojamiento_id'=> $ficha->id, 'extras'=> false])
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="precio-extras">
                        @include('manage.alojamientos.precios.list', ['alojamiento_id'=> $ficha->id, 'extras'=> true])
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="cuotas">
                        @include('manage.alojamientos.cuotas.list', ['alojamiento_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="imagenes">

                        <div class="row">
                        <?php
                        $path = public_path() ."/assets/uploads/alojamiento/" . $ficha->image_dir;
                        $folder = "/assets/uploads/alojamiento/" . $ficha->image_dir;

                        if(is_dir($path))
                        {
                            $results = scandir($path);
                            foreach ($results as $result) {
                              if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0]==='.') continue;

                              $file = $path . '/' . $result;

                              $btn_class = "btn-xs btn-warning";
                              if($result==$ficha->image_portada)
                              {
                                $btn_class = "btn-success";
                              }

                              if( is_file($file) )
                              {
                                echo '
                                <div class="col-md-3">
                                <div class="thumbnail">
                                <img src="'.$folder . '/' . $result.'" alt="">
                                <div class="caption">
                                <p>
                                <a href="'. route('manage.alojamientos.foto.delete', [$ficha->id,$result]).'" class="btn btn-danger btn-xs" role="button">Borrar</a>
                                <a href="'. route('manage.alojamientos.foto.portada', [$ficha->id,$result]).'" class="btn '. $btn_class .' pull-right" role="button">Portada</a>
                                </p>
                                </div>
                                </div>
                                </div>';
                              }
                            }
                        }
                        ?>
                        </div>

                        <div class="row">
                            <hr>
                            <div class="form-group">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h4>Añadir imágenes [{{$ficha->image_dir}}]</h4>
                                    </div>

                                    <div class="dropzone" id="myDropzone"></div>

                                </div>
                            </div>
                        </div>

                        @include('includes.script_dropzone', ['name'=> 'myDropzone', 'url'=> route('manage.alojamientos.upload', $ficha->id)])

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'Alojamiento',
                                'campos_text'=> [ ['name'=> 'Nombre'], ],
                                'campos_textarea'=> [
                                    ['accommodation_description'=> 'Descripción'],
                                    ['requisitos'=> 'Requisitos especiales'],
                                ]
                            ])

                    </div>

                </div>

            </div>
        </div>

@stop