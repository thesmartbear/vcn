@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bed fa-fw"></i> Tipos Alojamientos
                <span class="pull-right"><a href="{{ route('manage.alojamientos.tipos.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Tipo Alojamiento</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Tipo',
                      'familia'         => 'Familia',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.alojamientos.tipos.index'))
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [2] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop