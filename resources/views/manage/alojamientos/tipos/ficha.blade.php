@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Tipo Alojamiento :: {{$ficha->accommodation_type_name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Tipo Alojamiento</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">
                        {!! Form::open(array('method' => 'POST', 'url' => route('manage.alojamientos.tipos.ficha',$ficha->id), 'role' => 'form', 'class' => '')) !!}

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'accommodation_type_name', 'texto'=> 'Tipo'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_checkbox', [ 'campo'=> 'es_familia', 'texto'=> 'Marcar si se quiere poder entrar datos específicos de una familia para cada viajero.'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_textarea_tinymce', [ 'campo'=> 'notas', 'texto'=> 'Notas Pocket Guide'])
                            </div>

                            @include('includes.form_submit', [ 'permiso'=> 'configuracion', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'AlojamientoTipo',
                                'campos_text'=> [
                                    ['accommodation_type_name'=> 'Nombre']
                                ],
                                'campos_textarea'=> [
                                    ['notas'=> 'Notas']
                                ]
                            ])

                    </div>

                </div>

            </div>

            </div>
        </div>

@stop