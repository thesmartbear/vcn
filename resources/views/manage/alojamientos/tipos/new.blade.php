@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Nuevo Tipo Alojamiento
            </div>
            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.alojamientos.tipos.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'accommodation_type_name', 'texto'=> 'Tipo'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'es_familia', 'texto'=> 'Marcar si se quiere poder entrar datos específicos de una familia para cada viajero.'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'notas', 'texto'=> 'Notas Pocket Guide'])
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'configuracion', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop