@extends('layouts.manage')

@section('extra_footer')
    {{--{!! Html::script('assets/plugins/webcamjs/webcam.min.js') !!}--}}
@stop

@section('content')

    <script language="JavaScript">
        $(document).ready(function() {

            Webcam.set({
                width: 320,
                height: 240,
                image_format: 'jpeg',
                jpeg_quality: 90
            });
            // Webcam.attach( '#my_camera' );

        });

        function setup() {
            Webcam.reset();
            Webcam.attach( '#my_camera' );
        }

        function take_snapshot() {
            // take snapshot and get image data
            Webcam.snap( function(data_uri) {
                // display results in page
                document.getElementById('results').innerHTML =
                    '<h2>Here is your image:</h2>' +
                    '<img src="'+data_uri+'"/>';

                $('#webcam64').val(data_uri);
            } );
        }


    </script>

    {!! Form::open( array('method'=> 'post', 'files'=> true, 'url' => route('manage.test.post')) ) !!}

    <div class="row">
        <div class="col-md-4">
            <div id="my_camera"></div>
        </div>

        <div class="col-md-4">
            <div id="results"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <input type="button" value="Access Camera" onClick="setup(); $(this).hide().next().show();">
        </div>
        <div class="col-md-2">
            <input type=button value="Take Snapshot" onClick="take_snapshot()">
        </div>
        <div class="col-md-2">
            {!! Form::hidden('webcam64', null, array('id'=> 'webcam64')) !!}
            {!! Form::submit("Enviar", array('class' => 'btn btn-success pull-right')) !!}
        </div>
    </div>

    {!! Form::close() !!}

@stop


{{-- xxx --}}

@section('extra_head-xxx')
    <style type="text/css" media="screen">
        #signature_container {
            color:darkblue;
            /*background-color:lightgrey;*/
            padding: 8px;

            /*position: absolute;
            top: 100px;
            right: 200px;
            z-index: 9999;*/
        }

        .signature_box {
            padding: 0 0 0 0;
            margin: .2em 0 0 .3em;
            border: 2px dotted #000;
            width: 340px;
        }
        .signature_data {
            height: 120px;
            width: 340px;
            border: 1px solid #ccc;
            /*padding: 20px;*/
        }

        .signature_iframe {
            /*width: 800px;*/
            min-height: 600px;
            margin-bottom: 20px;
        }
    </style>
@stop


@section('content-xxx')

    <?php
    $fichero = "/assets/uploads/categorias/2/1/es/condiciones-jovenes-bs-2018.pdf";
    ?>

    {!! Form::open( array('method'=> 'post', 'url' => route('manage.test.post')) ) !!}

    <div class="form-group row">
        <iframe class="signature_iframe col-md-10" src="{{$fichero}}" frameborder="0"> </iframe>
    </div>

    <div class="form-group row">

        <div class="col-md-4">
            <div id='signature_container'>
                Firma:
                <div id="signature" class="signature_box"></div>
            </div>
        </div>

        <div class="col-md-4">

            <button id="sign_reset" class="btn btn-warning" disabled>Limpiar</button>
            <button id="sign_export" class="btn btn-success" disabled>Aceptar</button>

            <div id="signature_img1" class='signature_data'></div>

        </div>

        <div class="col-md-2">
            {!! Form::submit("Firmar", array('class' => 'btn btn-success pull-right')) !!}
        </div>
    </div>


    {!! Form::hidden('signature_b64_1', null, array('id'=> 'signature_b64_1')) !!}

{!! Form::close() !!}


<!--[if lt IE 9]>
    <script type="text/javascript" src="libs/flashcanvas.js"></script>
    <![endif]-->
    {!! Html::script('assets/plugins/jSignature/jSignature.min.js') !!}
    <div id="signature"></div>
    <script>
        $(document).ready(function() {

                    {{--
                    @if( isset($booking) && $booking->firmas && isset($booking->firmas['viajero']) )

                      var i = new Image()
                      i.src = "{{$booking->firmas['viajero']}}"
                      $(i).appendTo($("#signature_img1"))

                    @endif
                    --}}

            var options = {
                    // defaultAction: 'drawIt',
                    // penColour: '#2c3e50',
                    // lineWidth: 0,
                    'decor-color': 'transparent',
                }

            $sigdiv = $("#signature");
            $sigdiv.jSignature(options);
            $sigdiv.jSignature("reset");

            $sigdiv.bind('change', function(e){
                $('#sign_reset').prop('disabled', false);
                $('#sign_export').prop('disabled', false);
            });

            $('#sign_reset').click( function(e) {
                e.preventDefault();
                $sigdiv.jSignature("reset");

                $(this).prop('disabled', true);
                $("#signature_img1").html("");
            });

            $('#sign_export').click( function(e) {
                e.preventDefault();

                var datapair = $sigdiv.jSignature("getData", 'image')
                var i = new Image()
                var b64 = "data:" + datapair[0] + "," + datapair[1];
                // console.log(b64)

                i.src = b64
                $(i).appendTo($("#signature_img1"))
                $('#signature_b64_1').val(b64);

            });
        })
    </script>

@stop