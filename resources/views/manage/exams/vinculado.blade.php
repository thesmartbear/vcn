@foreach($ficha->parents as $p)
    
    @if($p == "cursos")
        @foreach($ficha->cursos as $c)
            <hr>
            Curso: {{$c->name}}
            @foreach($c->parents as $p)
                @include('manage.exams.vinculado_list', ['ficha'=> $c, 'title'=> $p, 'subtitle' => $c->name])
            @endforeach
        @endforeach
    @else
        @include('manage.exams.vinculado_list', ['ficha'=> $ficha->$p, 'title'=> $p])
    @endif
    
    <hr>
@endforeach

@include('manage.exams.vinculado_list', ['ficha'=> $ficha, 'title'=> null])
<hr>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list-alt fa-fw"></i> Vincular Tests
    </div>
    <div class="panel-body">

        @php
            $examenes = \VCN\Models\Exams\Examen::activo()->pluck('name','id')->toArray();
        @endphp

        {!! Form::open(array('method' => 'POST', 'url' => route('manage.exams.vinculados') , 'role' => 'form', 'class' => '')) !!}

            {!! Form::hidden('modelo',$modelo) !!}
            {!! Form::hidden('modelo_id',$modelo_id) !!}

            <div class="form-group row">
                <div class="col-md-3">
                    @include('includes.form_select', ['campo'=> 'examen_id', 'texto'=> 'Test', 'select'=> $examenes])
                </div>
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'excluye', 'texto'=> 'Excluir' ])
                </div>
            </div>

            <div class="form-group">
                @include('includes.form_submit', [ 'permiso'=> 'cuestionarios', 'texto'=> 'Guardar'])
            </div>

        {!! Form::close() !!}
    </div>
</div>

<hr>
{{-- {{$modelo}} - {{$modelo_id}} --}}
@include('manage.exams._respuestas', ['route'=> route('manage.exams.asks', [0, $modelo => $modelo_id])])