@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list-alt fa-fw"></i> Nuevo Test
    </div>
    <div class="panel-body">

        {!! Form::open(array('method' => 'POST', 'files'=> true, 'url' => route('manage.exams.store'), 'role' => 'form', 'class' => '')) !!}

            @include('manage.exams.form')

            @include('includes.form_submit', [ 'permiso'=> 'cuestionarios', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

@stop
