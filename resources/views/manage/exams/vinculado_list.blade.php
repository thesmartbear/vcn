<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list-alt fa-fw"></i> Tests vinculados @if($title) [@lang("manage.".$title)] @endif
        @if(isset($subtitle)) [{{$subtitle}}] @endif
    </div>

    @if($ficha)
    <div class="panel-body">

        @if(count($ficha->examenes))
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Test</th>
                    <th>Excluir</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($ficha->examenes as $test)
                    <tr>
                        <td>{{$test->name}}</td>
                        <td>{{$test->pivot->excluye ? "SI" : "NO"}}</td>
                        <td>
                            @if(!$title)
                            <a href="{{route('manage.exams.reclamar',[$test->id, $modelo, $modelo_id])}}" data-label="Reclamar" class="btn btn-warning btn-xs"><i class="fa fa-bullhorn"></i></a>

                            <a href='#destroy' data-label='Borrar' data-model='Vinculación de Test' data-action=" {{route( 'manage.exams.vinculados.delete', $test->pivot->id)}}" data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
            - Ninguno -
        @endif

    </div>
    @endif

</div>