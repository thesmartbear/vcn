@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.cms.landings.index') !!} --}}
@stop

@section('titulo')
    <i class="fa fa-file fa-fw"></i> Tests
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-list-alt fa-fw"></i> Tests
            {{-- <span class="pull-right"><a href="{{ route('manage.exams.create') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Test</a></span> --}}
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'    => 'Nombre',
                  'tema'    => 'Tema',
                  'bloques'    => 'Bloques',
                  'preguntas'    => 'Preguntas',
                  'respuestas'  => 'Respuestas',
                  'preview' => 'Probar',
                  'status'  => 'Activo',
                  'options' => ''
                ])
                ->setUrl(route('manage.exams.index'))
                ->setOptions(
                  "aoColumnDefs", array(
                    //[ "bSortable" => false, "aTargets" => [2,3,4] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

@stop