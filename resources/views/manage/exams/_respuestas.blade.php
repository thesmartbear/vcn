<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-clipboard fa-fw"></i> Respuestas
    </div>
    <div class="panel-body">

        @php
          $url = isset($route) ? $route : route('manage.exams.asks', $ficha->id);
        @endphp

        {!! Datatable::table()
            ->addColumn([
              'tipo'    => 'Tipo',
              'ver'     => 'Ver',
              'booking'  => 'Booking',
              'viajero'  => 'Viajero',
              'aciertos' => 'Aciertos',
              'estado'  => 'Estado',
              'resultado' => 'Resultado',
              'notas' => 'IELTS',
              'options' => ''
            ])
            ->setUrl($url)
            ->setOptions(
              "aoColumnDefs", array(
                //[ "bSortable" => false, "aTargets" => [2,3,4] ]
              )
            )
            ->render() !!}

    </div>
</div>