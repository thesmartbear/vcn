<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list-alt fa-fw"></i> Vinculaciones
    </div>
    <div class="panel-body">

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Relación</th>
                    <th>Modelo</th>
                    <th>Excluye</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($ficha->vinculados as $v)
                    <tr>
                        <td><a target="_blank" href="{{$v->relacion_url}}">{{$v->modelo}}</a></td>
                        <td>{{$v->relacion_name}}</td>
                        <td>{{$v->excluye ? "SI" : "NO"}}</td>
                        <td>
                            @if(!$v->excluye)
                                <a href="{{route('manage.exams.reclamar',[$v->examen, $v->modelo, $v->modelo_id])}}" data-label="Reclamar" class="btn btn-warning btn-xs"><i class="fa fa-bullhorn"></i></a>
                            @endif

                            <a href='#destroy' data-label='Borrar' data-model='Vinculación de Test' data-action=" {{route( 'manage.exams.vinculados.delete', $v->id)}}" data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>