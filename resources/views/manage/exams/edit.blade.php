@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-list-alt fa-fw"></i> Test :: {{$ficha->name}}
    </div>
    <div class="panel-body">

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Test</a></li>
            <li role="presentation"><a data-label="Vinculados" href="#vinculados" aria-controls="vinculados" role="tab" data-toggle="tab">Vinculados</a></li>
            <li role="presentation"><a data-label="Respuestas" href="#asks" aria-controls="asks" role="tab" data-toggle="tab">Respuestas</a></li>
            {{-- <li role="presentation"><a data-label="Traducciones" href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i></a></li> --}}
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                {!! Form::open(array('method' => 'PUT', 'files'=> true, 'url' => route('manage.exams.update', $ficha), 'role' => 'form', 'class' => '')) !!}

                    @include('manage.exams.form')

                    @include('includes.form_submit', [ 'permiso'=> 'cuestionarios', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="vinculados">
                @include('manage.exams._vinculados')
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="asks">
                @include('manage.exams._respuestas')
            </div>

            {{-- <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                @include('includes.traduccion-tab',
                        ['modelo'=> 'Examen',
                        'campos_text'=> [
                        ],
                        'campos_textarea_basic'=> [
                        ]
                    ])

            </div> --}}
            
        </div>

    </div>
</div>

@stop