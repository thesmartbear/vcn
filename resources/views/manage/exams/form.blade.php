<div class="form-group row">
    <div class="col-md-6">
        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre', 'required'=> true])
    </div>
    <div class="col-md-3">
        @include('includes.form_select', [ 'campo'=> 'template', 'texto'=> 'Plantilla', 'select'=> $parametros->temas])
    </div>
    <div class="col-md-1">
        @include('includes.form_input_number', [ 'campo'=> 'bloques', 'texto'=> 'Nº.Bloques'])
    </div>
    <div class="col-md-2">
        @include('includes.form_checkboxh', [ 'campo'=> 'activo', 'texto'=> 'Activo'])
    </div>
    
</div>
<div class="form-group">
    @include('includes.form_input_text', [ 'campo'=> 'title', 'texto'=> 'Título', 'required'=> true])
</div>
<div class="form-group row">
    <div class="col-md-6">
        @include('includes.form_textarea_tinymce', [ 'campo'=> 'texto', 'texto'=> 'Instrucciones', 'required'=> true])
    </div>
    <div class="col-md-6">
        @include('includes.form_textarea_tinymce', [ 'campo'=> 'gracias', 'texto'=> 'Gracias', 'required'=> true])
    </div>
</div>