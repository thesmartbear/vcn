@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-truck fa-fw"></i> País :: {{$ficha->name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">País</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">
                        {!! Form::model($ficha, array('route' => array('manage.paises.ficha', $ficha->id))) !!}

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'descripcion', 'texto'=> 'Descripción'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_file', [ 'campo'=> 'imagen', 'texto'=> 'Imagen'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'seo_url', 'texto'=> 'Url SEO'])
                            </div>
                            <div class="col-md-6">
                                @include('includes.form_input_text', [ 'campo'=> 'seo_titulo', 'texto'=> 'Título SEO'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_input_text', [ 'campo'=> 'color', 'texto'=> 'Color web (#)'])
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8">
                                @include('includes.form_textarea', [ 'campo'=> 'seo_descripcion', 'texto'=> 'Descripción SEO'])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'seo_keywords', 'texto'=> 'Keywords SEO'])
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8">
                                @include('includes.form_input_text', [ 'campo'=> 'slug', 'texto'=> 'Slug'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'activo_web', 'texto'=> 'Activo Web'])
                            </div>
                        </div>

                        @include('includes.form_submit', [ 'permiso'=> 'tablas', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'Pais',
                                'campos_text'=> [
                                    ['name'=> 'Nombre'],
                                    ['seo_url'=> 'Url SEO'],
                                    ['seo_titulo'=> 'Título SEO'],
                                    ['seo_keywords'=> 'Keywords SEO'],
                                ],
                                'campos_textarea'=> [
                                    ['descripcion'=> 'Descripción'],
                                    ['seo_descripcion'=> 'Descripción SEO'],
                                ]
                            ])

                    </div>

                </div>

            </div>
        </div>

@stop

@push('scripts')
<script type="text/javascript">
$(document).ready(function($){
    $('#color').iris();
});
</script>
@endpush