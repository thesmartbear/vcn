@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-truck fa-fw"></i> Nuevo País
            </div>
            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.paises.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'descripcion', 'texto'=> 'Descripción'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_file', [ 'campo'=> 'imagen', 'texto'=> 'Imagen'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'seo_url', 'texto'=> 'Url SEO'])
                        </div>
                        <div class="col-md-6">
                            @include('includes.form_input_text', [ 'campo'=> 'seo_titulo', 'texto'=> 'Título SEO'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'color', 'texto'=> 'Color web (#)'])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-8">
                            @include('includes.form_textarea', [ 'campo'=> 'seo_descripcion', 'texto'=> 'Descripción SEO'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'seo_keywords', 'texto'=> 'Keywords SEO'])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-8">
                            @include('includes.form_input_text', [ 'campo'=> 'slug', 'texto'=> 'Slug'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'activo_web', 'texto'=> 'Activo Web'])
                        </div>
                    </div>


                    @include('includes.form_submit', [ 'permiso'=> 'tablas', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop

@push('scripts')
<script type="text/javascript">
$(document).ready(function($){
    $('#color').iris();
});
</script>
@endpush