@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.index') !!} --}}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tag fa-fw"></i> Categorias Prescriptor
                <span class="pull-right"><a href="{{ route('manage.prescriptores.categorias.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Categoria</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Categoria',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.prescriptores.categorias.index'))
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [1] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop