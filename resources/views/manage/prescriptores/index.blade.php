@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.index') !!} --}}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-suitcase fa-fw"></i> Prescriptores
                <span class="pull-right"><a href="{{ route('manage.prescriptores.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Prescriptor</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Prescriptor',
                      'categoria'       => 'Categoría',
                      'categoria_com'   => 'Categoría Comisión',
                      'oficina'      => 'Oficina',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.prescriptores.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [4] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop