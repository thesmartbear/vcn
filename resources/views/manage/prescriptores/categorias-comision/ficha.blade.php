@extends('layouts.manage')


@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.ficha', $ficha) !!} --}}
@stop


@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-tag fa-fw"></i> Categoría Comisión :: {{$ficha->name}}
        </div>
        <div class="panel-body">

            {!! Form::model($ficha, array('route' => array('manage.prescriptores.categorias-comision.ficha', $ficha->id))) !!}

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
            </div>

            @include('includes.form_plataforma', ['campo'=> 'propietario'])

            @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

            {!! Form::close() !!}

        </div>

    </div>

@stop