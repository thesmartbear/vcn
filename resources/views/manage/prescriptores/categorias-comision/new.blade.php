@extends('layouts.manage')


@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.nuevo') !!} --}}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tag fa-fw"></i> Nueva Categoría Comisión
            </div>
            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.prescriptores.categorias-comision.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                    </div>

                    @include('includes.form_plataforma', ['campo'=> 'propietario'])

                    @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop