@extends('layouts.manage')


@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.ficha', $ficha) !!} --}}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-suitcase fa-fw"></i> Prescriptor :: {{$ficha->name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Datos Generales</a></li>

                    <li role="presentation"><a href="#contacto" aria-controls="contacto" role="tab" data-toggle="tab">Datos Contacto</a></li>
                    <li role="presentation"><a href="#fiscales" aria-controls="fiscales" role="tab" data-toggle="tab">Datos Fiscales</a></li>
                    <li role="presentation"><a href="#comisiones" aria-controls="comisiones" role="tab" data-toggle="tab">Comisiones</a></li>

                    @if(ConfigHelper::canEdit('prescriptores'))
                    <li role="presentation"><a href="#ventas" aria-controls="ventas" role="tab" data-toggle="tab">Ventas</a></li>
                    <li role="presentation"><a href="#ventasdet" aria-controls="ventasdet" role="tab" data-toggle="tab">Ventas detalle</a></li>
                    <li role="presentation"><a href="#solicitudes" aria-controls="solicitudes" role="tab" data-toggle="tab">Solicitudes</a></li>
                    @endif

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('route' => array('manage.prescriptores.ficha', $ficha->id))) !!}

                            {!! Form::hidden('tab','#ficha') !!}

                            <div class="form-group row">
                                <div class="col-md-4">
                                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_input_text', [ 'campo'=> 'sucursal', 'texto'=> 'Sucursal'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_select', [ 'campo'=> 'idioma', 'texto'=> 'Idioma', 'select'=> ['es'=>'Español', 'ca'=>'Català']])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_select', [ 'campo'=> 'pais_id', 'texto'=> 'País', 'select'=>[""=>""]+ \VCN\Models\Pais::orderBy('name')->pluck('name','id')->toArray()])
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    @include('includes.form_select', [ 'campo'=> 'categoria_id', 'texto'=> 'Categoría', 'select'=> $categorias])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_select', [ 'campo'=> 'comision_cat_id', 'texto'=> 'Categoría Comisión', 'select'=> $categoriascom])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_select', [ 'campo'=> 'oficina_id', 'texto'=> 'Oficina', 'select'=> $oficinas])
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-3">
                                    @include('includes.form_input_text', [ 'campo'=> 'direccion', 'texto'=> 'Dirección'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_input_text', [ 'campo'=> 'cp', 'texto'=> 'CP'])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_input_text', [ 'campo'=> 'poblacion', 'texto'=> 'Población'])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_select', [ 'campo'=> 'provincia_id', 'texto'=> 'Provincia', 'select'=>[""=>""]+ \VCN\Models\Provincia::pluck('name','id')->toArray()])
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-3">
                                    @include('includes.form_input_text', [ 'campo'=> 'persona', 'texto'=> 'Persona contacto'])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_input_text', [ 'campo'=> 'telefono1', 'texto'=> 'Teléfono1'])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_input_text', [ 'campo'=> 'telefono2', 'texto'=> 'Teléfono2'])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'E-mail'])
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <div class="col-md-2">
                                    @include('includes.form_input_text', [ 'campo'=> 'nif', 'texto'=> 'Nif/Cif'])
                                </div>
                                <div class="col-md-5">
                                    @include('includes.form_input_text', [ 'campo'=> 'razon_social', 'texto'=> 'Razón Social'])
                                </div>
                                <div class="col-md-5">
                                    @include('includes.form_input_text', [ 'campo'=> 'domicilio_social', 'texto'=> 'Domicilio Social'])
                                </div>
                            </div>

                            <div class="form-group">
                                 @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas' ])
                            </div>

                            <hr>

                            <div class="form-group row">
                                <div class="col-md-2">
                                    @include('includes.form_checkbox', [ 'campo'=> 'area', 'texto'=> 'Área Cliente' ])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_checkbox', [ 'campo'=> 'area_pagos', 'texto'=> 'Área Cliente (Pagos)' ])
                                </div>
                                <div class="col-md-1">
                                    @include('includes.form_checkbox', [ 'campo'=> 'area_reunion', 'texto'=> 'Área Cliente (Reunión)' ])
                                </div>
                                <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'no_facturar', 'texto'=> 'No facturar por sistema'])
                        </div>
                            </div>

                        @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="contacto">

                        {!! Form::model($ficha, array('route' => array('manage.prescriptores.ficha', $ficha->id))) !!}

                            {!! Form::hidden('tab','#contacto') !!}

                            <div class="form-group row">
                                <div class="col-md-3">
                                    @include('includes.form_input_text', [ 'campo'=> 'persona', 'texto'=> 'Persona de Contacto'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_input_text', [ 'campo'=> 'telefono1', 'texto'=> 'Teléfono1'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_input_text', [ 'campo'=> 'telefono2', 'texto'=> 'Teléfono2'])
                                </div>
                                <div class="col-md-5">
                                    @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'E-mail'])
                                </div>
                            </div>

                            <div class="form-group">
                                @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Observaciones'])
                            </div>

                            @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>


                    <div role="tabpanel" class="tab-pane fade in" id="fiscales">

                        {!! Form::model($ficha, array('route' => array('manage.prescriptores.ficha', $ficha->id))) !!}

                            {!! Form::hidden('tab','#fiscales') !!}

                            <div class="form-group row">
                                <div class="col-md-4">
                                    @include('includes.form_input_text', [ 'campo'=> 'nif', 'texto'=> 'NIF'])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_input_text', [ 'campo'=> 'razon_social', 'texto'=> 'Razón Social'])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_input_text', [ 'campo'=> 'domicilio_social', 'texto'=> 'Domicilio Social'])
                                </div>
                            </div>

                            @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>


                    <div role="tabpanel" class="tab-pane fade in" id="comisiones">
                    ...
                    </div>

                    @if(ConfigHelper::canEdit('prescriptores'))
                    <div role="tabpanel" class="tab-pane fade in" id="ventas">
                        <div class="row">
                            <div class="col-md-12">

                                <?php
                                    $anyd = 2015;
                                    $anyh = intval(Carbon::now()->format('Y'));
                                ?>

                                <ul class="nav nav-tabs" role="tablist">
                                @for ($i = $anyd; $i <= $anyh; $i++)
                                    {{-- @if($totales[$i]['total_num']>0) --}}
                                        <li role="presentation" class="{{($i === $anyh)?'active':''}}"><a href="#any-{{$i}}" aria-controls="any-{{$i}}" role="tab" data-toggle="tab">{{$i}}</a></li>
                                    {{-- @endif --}}
                                @endfor
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    @for ($i = $anyd; $i <= $anyh; $i++)

                                        <div role="tabpanel" id="any-{{$i}}" class="tab-pane fade in {{($i === $anyh)?'active':''}}">

                                            @if($totales[$i]['total_num']>0)
                                            {!! Datatable::table()
                                                ->addColumn([
                                                    'categoria'     => 'Categoria',
                                                    'num'           => 'Nº Inscrip.',
                                                    'total_sem'     => 'Semanas',
                                                    'total_curso'   => "Imp.Curso <i class='fa fa-info-circle' data-label='Importe del curso, sin extras ni descuentos, ni alojamiento'></i>",
                                                    'total'         => "Imp.Total <i class='fa fa-info-circle' data-label='Importe total a pagar por el cliente'></i>",
                                                ])
                                                ->setUrl( route('manage.prescriptores.ventas',[$ficha->id,$i]) )
                                                ->setOptions('iDisplayLength', 100)
                                                ->render()
                                            !!}

                                            <hr>

                                            <table class="table table-bordered table-hover">
                                                <caption>Totales</caption>
                                                <thead>
                                                    <tr>
                                                        <th>Total Inscrip.</th>
                                                        <th>Total Semanas</th>
                                                        <th>Total Curso</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>{{$totales[$i]['total_num']}}</td>
                                                        <td>{{$totales[$i]['total_sem']}}</td>
                                                        <td>{{ConfigHelper::parseMoneda($totales[$i]['total_curso'])}}</td>
                                                        <td>{{ConfigHelper::parseMoneda($totales[$i]['total'])}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            @else
                                                -- No hay bookings --
                                            @endif

                                        </div>
                                    @endfor

                                </div>

                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="ventasdet">

                        <div class="row">
                            <div class="col-md-12">

                                <?php
                                    $anyd = 2015;
                                    $anyh = intval(Carbon::now()->format('Y'));
                                ?>

                                <ul class="nav nav-tabs" role="tablist">
                                @for ($i = $anyd; $i <= $anyh; $i++)
                                    {{-- @if($totales[$i]['total_num']>0) --}}
                                        <li role="presentation" class="{{($i === $anyh)?'active':''}}"><a href="#anyd-{{$i}}" aria-controls="anyd-{{$i}}" role="tab" data-toggle="tab">{{$i}}</a></li>
                                    {{-- @endif --}}
                                @endfor
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    @for ($i = $anyd; $i <= $anyh; $i++)

                                        <div role="tabpanel" id="anyd-{{$i}}" class="tab-pane fade in {{($i === $anyh)?'active':''}}">

                                            @if($totales[$i]['total_num']>0)

                                            {!! Datatable::table()
                                                ->addColumn([
                                                    'viajero'   => 'Viajero',
                                                    'curso'     => 'Curso',
                                                    'convocatoria'  => 'Convocatoria',
                                                    'semanas'   => 'NºSemanas',
                                                    'curso_total'   => 'Imp. Curso',
                                                    'fecha'     => 'Fecha Booking',
                                                    'total'     => 'Importe total',
                                                    'options'   => ''

                                                ])
                                                ->setUrl( route('manage.prescriptores.ventas-detalle',[$ficha->id,$i]) )
                                                ->setOptions('iDisplayLength', 100)
                                                ->setOptions(
                                                  "columnDefs", array(
                                                    [ "sortable" => false, "targets" => [7] ],
                                                    [ "targets" => [5], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                                  )
                                                )
                                                ->render()
                                            !!}

                                            @else
                                                -- No hay bookings --
                                            @endif

                                        </div>

                                    @endfor

                                </div>

                            </div>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="solicitudes">

                        <div class="row">
                            <div class="col-md-12">

                                <?php
                                    $anyd = 2015;
                                    $anyh = intval(Carbon::now()->format('Y'));

                                    $valores['prescriptor_id'] = $ficha->id;
                                ?>

                                <ul class="nav nav-tabs" role="tablist">
                                @for ($i = $anyd; $i <= $anyh; $i++)
                                    <li role="presentation" class="{{($i === $anyh)?'active':''}}"><a href="#anys-{{$i}}" aria-controls="anys-{{$i}}" role="tab" data-toggle="tab">{{$i}}</a></li>
                                @endfor
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    @for ($i = $anyd; $i <= $anyh; $i++)
                                        <div role="tabpanel" id="anys-{{$i}}" class="tab-pane fade in {{($i === $anyh)?'active':''}}">

                                            @include('manage.solicitudes.list', ['status_id'=> 0, 'user_id'=> 'all', 'oficina_id'=> 'all', 'status_name' => 0, 'any'=> $i, 'valores'=> $valores])

                                        </div>
                                    @endfor

                                </div>

                            </div>
                        </div>

                    </div>

                    @endif

                </div>

            </div>
        </div>

@stop