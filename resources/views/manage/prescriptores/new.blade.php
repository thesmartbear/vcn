@extends('layouts.manage')


@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.nuevo') !!} --}}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-suitcase fa-fw"></i> Nuevo Prescriptor
            </div>
            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.prescriptores.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    {!! Form::hidden('tab','#ficha') !!}

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'sucursal', 'texto'=> 'Sucursal'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_select', [ 'campo'=> 'idioma', 'texto'=> 'Idioma', 'select'=> ['es'=>'Español', 'ca'=>'Català']])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'provincia_id', 'texto'=> 'País', 'select'=> \VCN\Models\Pais::orderBy('name')->pluck('name','id')])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'categoria_id', 'texto'=> 'Categoría', 'select'=> $categorias])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'comision_cat_id', 'texto'=> 'Categoría Comisión', 'select'=> $categoriascom])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'oficina_id', 'texto'=> 'Oficina', 'select'=> $oficinas])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'direccion', 'texto'=> 'Dirección'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'cp', 'texto'=> 'CP'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'poblacion', 'texto'=> 'Población'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'provincia_id', 'texto'=> 'Provincia', 'select'=>[""=>""]+ \VCN\Models\Provincia::pluck('name','id')->toArray()])
                        </div>
                    </div>

                    <hr>

                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'persona', 'texto'=> 'Persona contacto'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'telefono1', 'texto'=> 'Teléfono1'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'telefono2', 'texto'=> 'Teléfono2'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'E-mail'])
                        </div>
                    </div>

                    <hr>

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'nif', 'texto'=> 'Nif/Cif'])
                        </div>
                        <div class="col-md-5">
                            @include('includes.form_input_text', [ 'campo'=> 'razon_social', 'texto'=> 'Razón Social'])
                        </div>
                        <div class="col-md-5">
                            @include('includes.form_input_text', [ 'campo'=> 'domicilio_social', 'texto'=> 'Domicilio Social'])
                        </div>
                    </div>

                    <div class="form-group">
                         @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas' ])
                    </div>

                    <hr>
                    <div class="form-group row">
                        <div class="col-md-1">
                            @include('includes.form_checkbox', [ 'campo'=> 'area', 'texto'=> 'Área Cliente', 'valor'=>1 ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'area_pagos', 'texto'=> 'Área Cliente (Pagos)', 'valor'=>1 ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'area_reunion', 'texto'=> 'Área Cliente (Reunión)', 'valor'=>1 ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'no_facturar', 'texto'=> 'No facturar por sistema'])
                        </div>
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'prescriptores', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop