@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.centros.schools.ficha', $ficha) !!}
@stop


@section('container')

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Shcool</a></li>
        <li role="presentation"><a href="#bookings" aria-controls="bookings" role="tab" data-toggle="tab">Historial</a></li>
        <li role="presentation"><a href="#area" aria-controls="area" role="tab" data-toggle="tab">Área</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <div role="tabpanel" class="tab-pane fade in active" id="ficha">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bed fa-fw"></i>
                        <a href="{{route('manage.centros.ficha',$centro->id)}}#schools">
                        Centro: ({{$centro->name}})
                        </a> :: School {{$ficha->name}}
                </div>
                <div class="panel-body">

                    {!! Form::open(array('files'=> true,'route' => array('manage.centros.schools.ficha', $ficha->id))) !!}

                        {!! Form::hidden('center_id', $centro->id) !!}

                        <div class="form-group row">
                            <div class="col-md-8">
                                @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'School'])
                            </div>
                            <div class="col-md-4">
                            @include('includes.form_input_file', [ 'campo'=> 'foto', 'texto'=> 'Foto'])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'direccion', 'texto'=> 'Dirección'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_input_text', [ 'campo'=> 'cp', 'texto'=> 'CP'])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'poblacion', 'texto'=> 'Población'])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_input_text', [ 'campo'=> 'estado', 'texto'=> 'Estado'])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_select', [ 'campo'=> 'pais_id', 'texto'=> 'País', 'select'=>[""=>""]+ \VCN\Models\Pais::pluck('name','id')->toArray()])
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_input_text', [ 'campo'=> 'telefono', 'texto'=> 'Teléfono'])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'Email'])
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8">
                                @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Comentarios'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_input_number', [ 'campo'=> 'alumnos', 'texto'=> 'N.Alumnos'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activa'])
                            </div>
                        </div>


                        @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                        <div class="clearfix"></div>

                    {!! Form::close() !!}


                </div>
            </div>

        </div>

        <div role="tabpanel" class="tab-pane fade in" id="bookings">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bed fa-fw"></i>
                        <a href="{{route('manage.centros.ficha',$centro->id)}}#schools">
                        Centro: ({{$centro->name}})
                        </a> :: School {{$ficha->name}}
                </div>
                <div class="panel-body">

                    {!! Datatable::table()
                        ->addColumn([
                          'booking'     => 'Booking',
                          'viajero'     => 'Viajero',
                          'fecha_ini'   => 'Fecha inicio',
                          'fecha_fin'   => 'Fecha fin',
                          'fecha_asignacion'    => 'Fecha asignación',
                          'user_asignacion'     => 'Usuario asignación',
                          'fecha_activacion'    => 'Fecha activación',
                          'user_activacion'     => 'Usuario activación',
                          'fecha_cambio'        => 'Fecha último cambio',
                          'user_cambio'         => 'Usuario último cambio'
                        ])
                        ->setUrl( route('manage.centros.schools.historial', $ficha->id) )
                        ->setOptions(
                          "aoColumnDefs", array(
                            //[ "bSortable" => false, "aTargets" => [3] ]
                            [ "targets" => [2,3], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                            //[ "targets" => [4,6,8], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY H:mm'):'-';}" ],
                          )
                        )
                        ->render() !!}

                </div>
            </div>

        </div>

        <div role="tabpanel" class="tab-pane fade in" id="area">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-graduation-cap fa-fw"></i>

                </div>
                <div class="panel-body">

                    @include('manage.centros.schools.area')

                </div>

            </div>
        </div>

    </div>

@stop