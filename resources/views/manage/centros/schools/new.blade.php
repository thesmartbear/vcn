@extends('layouts.manage')

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-bed fa-fw"></i>
                <a href="{{route('manage.centros.ficha',$centro->id)}}#schools">
                Centro: ({{$centro->name}})
                </a> :: School Nueva
        </div>
        <div class="panel-body">

            {!! Form::open(array('files'=> true,'route' => array('manage.centros.schools.ficha', 0))) !!}

                {!! Form::hidden('center_id', $centro->id) !!}

                <div class="form-group row">
                    <div class="col-md-8">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'School'])
                    </div>
                    <div class="col-md-4">
                    @include('includes.form_input_file', [ 'campo'=> 'foto', 'texto'=> 'Foto'])
                    </div>
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'direccion', 'texto'=> 'Dirección'])
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        @include('includes.form_input_text', [ 'campo'=> 'cp', 'texto'=> 'CP'])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'poblacion', 'texto'=> 'Población'])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'estado', 'texto'=> 'Estado'])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_select', [ 'campo'=> 'pais_id', 'texto'=> 'País', 'select'=>[""=>""]+ \VCN\Models\Pais::pluck('name','id')->toArray()])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        @include('includes.form_input_text', [ 'campo'=> 'telefono', 'texto'=> 'Teléfono'])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'Email'])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-8">
                        @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Comentarios'])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_input_number', [ 'campo'=> 'alumnos', 'texto'=> 'N.Alumnos'])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activa'])
                    </div>
                </div>


                @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                <div class="clearfix"></div>

            {!! Form::close() !!}


        </div>
    </div>

@stop