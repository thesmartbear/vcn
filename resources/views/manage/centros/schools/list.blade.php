
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-graduation-cap"></i> Schools

            @if($centro_id)
              <span class="pull-right"><a href="{{ route('manage.centros.schools.nuevo', $centro_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva School</a></span>
            @endif

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'        => 'Nombre',
                  'activo'      => 'Activa',
                  'bookings'    => 'Asignados',
                  'options'     => ''
                ])
                ->setUrl( route('manage.centros.schools.index', $centro_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [3] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

