    <div class="row">
        <div class="col-md-10">
            <div class="caption font-green-sharp">
                <i class="fa fa-plane fa-2x"></i>
                <span class="caption-subject bold uppercase">
                    CURSO XXXX
                </span>
                <span class="caption-helper separator"> | </span>
                <span class="caption-helper out"><i class="fa fa-arrow-circle-right fa-lg text-success"></i> FECHA DESDE </span>
                <span class="caption-helper in"><i class="fa fa-arrow-circle-left fa-lg text-danger"></i> FECHA HASTA </span>
                <h4 class="text-muted"><i class="fa fa-male fa-fw text-muted"></i> VIAJERO </h4>
            </div>
        </div>
        <div class="col-md-2">
            <a class="btn btn-block btn-success" href="{{route('manage.centros.schools.pdf', $ficha->id)}}"><i class="fa fa-download"></i> {!! trans('area.descargar') !!}</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <hr>
            <h3 class="page-title font-green-sharp">{{$ficha->name}}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4  col-xs-12">
            @if($ficha->foto)
                <img class="img-responsive img-thumbnail" src="{{$ficha->foto}}" />
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.datoscontacto')}}</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="well">
                @if($ficha->direccion)
                    <strong>{{trans('area.direccion')}}:</strong> {{$ficha->direccion}}<br />
                @endif

                @if($ficha->cp)
                    <strong>{{trans('area.cp')}}:</strong> {{$ficha->cp}}<br />
                @endif

                @if($ficha->poblacion)
                    <strong>{{trans('area.poblacion')}}:</strong> {{$ficha->poblacion}}<br />
                @endif

                @if($ficha->pais)
                    <strong>{{trans('area.pais')}}:</strong> {{$ficha->pais->name}}
                @endif

                <hr>

                @if($ficha->telefono)
                    <strong>{{trans('area.telefono')}}:</strong> {{$ficha->telefono}}<br />
                @endif

                @if($ficha->email)
                    <strong>{{trans('area.email')}}:</strong> {{$ficha->email}}<br />
                @endif

                @if($ficha->notas)
                    <strong>{{trans('area.comentarios')}}:</strong> {{$ficha->notas}}
                @endif

            </div>
        </div>

        @if(strip_tags($ficha->direccion) != '')
            <div class="col-md-6">
                <iframe
                        width="100%"
                        height="300"
                        frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCNPWo91Kd3D656Z5R2U6K1vFlx65KxpE8
    &q={{strip_tags($ficha->direccion)}}, {{strip_tags($ficha->poblacion)}}, {{strip_tags($ficha->cp)}}, {{strip_tags($ficha->pais_name)}}&language={{App::getLocale()}}&zoom=13" allowfullscreen>
                </iframe>
            </div>
        @endif
    </div>


    @if(strip_tags($ficha->direccion) != '' && strip_tags($ficha->centro->address) != '')
        <div class="row">
            <div class="col-md-12">
                <h4 class="form-section text-primary">{{trans('area.trayectoschool')}}</h4>
                <iframe
                        width="100%"
                        height="350"
                        frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyCNPWo91Kd3D656Z5R2U6K1vFlx65KxpE8&origin={{strip_tags($ficha->direccion)}}, {{strip_tags($ficha->poblacion)}}, {{strip_tags($ficha->cp)}}, {{strip_tags($ficha->pais_name)}}&destination={{strip_tags($ficha->centro->address)}}&mode=transit" allowfullscreen>
                </iframe>
            </div>
        </div>
    @endif