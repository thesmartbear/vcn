<html>
<head>
    <title>CURSO - School {{$ficha->name}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    <!-- Bootstrap -->
    {!! Html::style('assets/css/bootstrap.css') !!}
            <!-- font awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    {!! Html::style('assets/css/area.css') !!}
    {!! Html::style('assets/css/components.css') !!}


    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            background: transparent;
            text-shadow: none;
        }
        body{
            font-size: 1.2em;
            margin-left: 2cm !important;
            margin-right: 2cm !important;
        }

        .page{
        }

        #contenido{
            padding-top: 0;
            margin-top: 0;
        }

        .container{
            padding: 0 2cm;
        }

        h2{
            margin-top: 2cm;
            font-size: 1.8em;
            font-weight: bold;
            @if(ConfigHelper::config('propietario') == 1)
                color: #f1c40f;
            @elseif(ConfigHelper::config('propietario') == 2)
                color: #3B6990;
            @endif
        }
        h2:first-child{
            margin-top: 1cm;
        }
        h4{
            margin-top: 30px;
            page-break-inside: avoid;
            text-transform: capitalize;
        }

        .logo{
            margin-bottom: 40px;
        }

    </style>

</head>

<body class="page">
    <div class="row logo">
        <div class="col-xs-12"><img style="width: 4.5cm; height: auto; margin-top: 30px;" class="pull-right" src="https://{{ConfigHelper::config('web')}}/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" /></div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="caption font-green-sharp">
                <i class="fa fa-plane fa-2x"></i>
                <span class="caption-subject bold uppercase">
                    CURSO XXX
                </span>
                <span class="caption-helper separator"> | </span>
                <span class="caption-helper out"><i class="fa fa-arrow-circle-right fa-lg text-success"></i> FECHA DESDE</span>
                <span class="caption-helper in"><i class="fa fa-arrow-circle-left fa-lg text-danger"></i> FECHA HASTA </span>
                <h4 class="text-muted"><i class="fa fa-male fa-fw text-muted"></i> VIAJERO </h4>
            </div>

            <hr>

            <h3 class="page-title font-green-sharp">{{$ficha->name}}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.datoscontacto')}}</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="well">
                @if($ficha->direccion)
                    <strong>{{trans('area.direccion')}}:</strong> {{$ficha->direccion}}<br />
                @endif

                @if($ficha->cp)
                    <strong>{{trans('area.cp')}}:</strong> {{$ficha->cp}}<br />
                @endif

                @if($ficha->poblacion)
                    <strong>{{trans('area.poblacion')}}:</strong> {{$ficha->poblacion}}<br />
                @endif

                @if($ficha->pais)
                    <strong>{{trans('area.pais')}}:</strong> {{$ficha->pais->name}}
                @endif

                <hr>

                @if($ficha->telefono)
                    <strong>{{trans('area.telefono')}}:</strong> {{$ficha->telefono}}<br />
                @endif

                @if($ficha->email)
                    <strong>{{trans('area.email')}}:</strong> {{$ficha->email}}<br />
                @endif

                @if($ficha->notas)
                    <strong>{{trans('area.comentarios')}}:</strong> {{$ficha->notas}}
                @endif

            </div>
        </div>

        @if(strip_tags($ficha->direccion) != '')
            <div class="col-md-6">
                <img src="https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=625x250&center={{strip_tags($ficha->direccion)}}, {{strip_tags($ficha->poblacion)}}, {{strip_tags($ficha->cp)}}, {{strip_tags($ficha->pais_name)}}&markers=clor:red|{{strip_tags($ficha->direccion)}}, {{strip_tags($ficha->poblacion)}}, {{strip_tags($ficha->cp)}}, {{strip_tags($ficha->pais_name)}}&key=AIzaSyCNPWo91Kd3D656Z5R2U6K1vFlx65KxpE8" />
            </div>
        @endif
    </div>

</body>
</html>