@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.centros.nuevo', $proveedor) !!}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-truck fa-fw"></i> Centro :: Nuevo
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Centro</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::open(array('route' => array('manage.centros.ficha', 0))) !!}

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'provider_id', 'texto'=> 'Proveedor', 'valor'=> $proveedor_id, 'select'=> $proveedores])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'country_id', 'texto'=> 'Pais', 'valor'=> 0, 'select'=> $paises])
                        </div>
                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'city_id', 'texto'=> 'Ciudad', 'valor'=> 0, 'select'=> $ciudades])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'address', 'texto'=> 'Dirección'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'currency_id', 'texto'=> 'Divisa', 'valor'=> 0, 'select'=> $monedas])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'commission', 'texto'=> 'Comisión',
                                'valor'=> isset($proveedor)?$proveedor->commission:''])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'description', 'texto'=> 'Descripción', 'help'=> 'Las imágenes no deben superar los 500px de ancho'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'settingup', 'texto'=> 'Instalaciones'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'foods', 'texto'=> 'Comidas'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'transport', 'texto'=> 'Transporte'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'center_activities', 'texto'=> 'Actividades',
                                'help'=> 'Comunes a todos los cursos de este centro'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'center_excursions', 'texto'=> 'Excursiones',
                                'help'=> 'Comunes a todos los cursos de este centro'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'internet_bool', 'texto'=> 'Internet'])
                            </div>
                            <div id="internet_div" class="col-md-4">
                                @include('includes.form_textarea_tinymce', [ 'campo'=> 'internet_uso', 'texto'=> 'Uso de internet'])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'lavanderia', 'texto'=> 'Lavandería'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'normas', 'texto'=> 'Normas de la escuela'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'comment', 'texto'=> 'Depósitos'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'center_video', 'texto'=> 'Vídeo'])
                            <span class="badge" style="color: #3C763D;background-color: #DFF0D8;">poner la url completa del video en Youtube, la que empieza por https://www.youtube.com/watch?v=</span>
                        </div>

                        <div class="form-group">
                            @include('includes.form_select_multi', [ 'campo'=> 'monitores', 'texto'=> 'Monitores', 'valor'=> [], 'select'=> \VCN\Models\Monitores\Monitor::plataforma()->pluck('name','id')->toArray() ])
                        </div>

                        @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                </div>

            </div>
        </div>


@if($proveedor_id>0)
<script type="text/javascript">
$(function() {
    $("#provider_id").attr('readonly',true);
});
</script>
@endif

<script type="text/javascript">
$(document).ready(function() {

    $("#country_id").change(function() {

        $("#city_id > option").each(function(){
            $(this).remove();
        });

        $("#city_id").prop('disabled', true);

        var country_id = $("#country_id").val();
        var c_id, city_name;

        $.ajax({
          url: "{{route('manage.ciudades.index')}}",
          type: 'GET',
          dataType : 'json',
          data: {'country_id': country_id},
          success: function(data) {

            $.each(data, function(i, item) {

                $("#city_id").append($('<option>', {
                  value: i,
                  text: item
                }));

            });

            $("#city_id").prop('disabled', false);
            $('#city_id').selectpicker('refresh');
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        }); // end ajax call
    });
});
</script>

@include('includes.script_boolean', ['campo'=> 'internet', 'checkbox'=> 1 ])

@stop