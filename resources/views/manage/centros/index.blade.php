@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.centros.index') !!}
@stop

@section('container')

    @include('manage.centros.list', ['proveedor_id'=> $proveedor_id])

@stop