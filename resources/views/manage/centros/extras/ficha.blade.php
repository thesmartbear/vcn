@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bed fa-fw"></i>
                    <a href="{{route('manage.centros.ficha',$ficha->center_id)}}#extras">
                    Centro: ({{$ficha->centro->name}})
                    </a>
                    Extra :: {{$ficha->center_extras_name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Extra</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('route' => array('manage.centros.extras.ficha', $ficha->id))) !!}

                        {!! Form::hidden('center_id', $ficha->center_id) !!}

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'center_extras_name', 'texto'=> 'Nombre extra'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea', [ 'campo'=> 'center_extras_description', 'texto'=> 'Descripción'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'center_extras_unit', 'texto'=> 'Unidad', 'valor'=> $ficha->center_extras_unit, 'select'=> $unidades_tipo])
                        </div>

                        <div id="center_extras_unit_div" class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'center_extras_unit_id', 'texto'=> 'Tipo Unidad', 'valor'=> $ficha->center_extras_unit_id, 'select'=> $unidades])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'center_extras_currency_id', 'texto'=> 'Moneda', 'valor'=> $ficha->center_extras_currency_id, 'select'=> $monedas])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'center_extras_price', 'texto'=> 'Precio'])
                        </div>

                        <div class="form-group">
                            {!! Form::label('center_extras_required', 'Obligatorio') !!}
                            {!! Form::checkbox("center_extras_required", $ficha->center_extras_required) !!}
                        </div>

                        @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'CentroExtra',
                                'campos_text'=> [ ['center_extras_name'=> 'Nombre'], ],
                                'campos_textarea'=> [ ['center_extras_description'=> 'Descripción'], ]
                            ])

                    </div>

                </div>

            </div>
        </div>

@include('includes.script_unidades', ['div'=>'#center_extras_unit' ])

@stop