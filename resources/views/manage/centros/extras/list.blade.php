
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-book"></i> Extras

            @if($centro_id)
              <span class="pull-right"><a href="{{ route('manage.centros.extras.nuevo', $centro_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Extra</a></span>
            @endif

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'        => 'Nombre',
                  'precio'   => 'Precio',
                  'unidades'    => 'Unidades',
                  'requerido'    => 'Obligatorio',
                  'options'     => ''
                ])
                ->setUrl( route('manage.centros.extras.index', $centro_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [4] ]
                  )
                )
                ->render() !!}

        </div>
    </div>
