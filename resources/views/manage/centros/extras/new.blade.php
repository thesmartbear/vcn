@extends('layouts.manage')

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bed fa-fw"></i>
                    <a href="{{route('manage.centros.ficha',$centro->id)}}#extras">
                    Centro: ({{$centro->name}})
                    </a>
                    Extra :: Nuevo
            </div>
            <div class="panel-body">

                {{$errors}}

                {!! Form::open(array('route' => array('manage.centros.extras.ficha', 0))) !!}

                    {!! Form::hidden('center_id', $centro->id) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'center_extras_name', 'texto'=> 'Nombre extra'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea', [ 'campo'=> 'center_extras_description', 'texto'=> 'Descripción'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'center_extras_unit', 'texto'=> 'Unidad', 'valor'=> 0, 'select'=> $unidades_tipo])
                    </div>

                    <div id="center_extras_unit_div" class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'center_extras_unit_id', 'texto'=> 'Tipo Unidad', 'valor'=> 0, 'select'=> $unidades])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'center_extras_currency_id', 'texto'=> 'Moneda', 'valor'=> $centro->moneda_id, 'select'=> $monedas])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'center_extras_price', 'texto'=> 'Precio'])
                    </div>

                    <div class="form-group">
                        {!! Form::label('center_extras_required', 'Obligatorio') !!}
                        {!! Form::checkbox("center_extras_required") !!}
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                    <div class="clearfix"></div>

                {!! Form::close() !!}


            </div>
        </div>

@include('includes.script_unidades', ['div'=>'#center_extras_unit' ])

@stop