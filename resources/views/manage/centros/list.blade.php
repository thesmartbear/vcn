<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-home"></i> Centros
            <span class="pull-right"><a href="{{ route('manage.centros.nuevo', $proveedor_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Centro</a></span>
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'      => 'Nombre',
                  'pais'      => 'País',
                  'ciudad'    => 'Ciudad',
                  'moneda'    => 'Moneda',
                  'options'         => ''
                ])
                ->setUrl( route('manage.centros.index', $proveedor_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [4] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>