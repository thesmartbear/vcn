@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.centros.ficha', $ficha) !!}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-truck fa-fw"></i> Centro :: {{$ficha->name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Centro</a></li>
                    <li role="presentation"><a href="#imagenes" aria-controls="imagenes" role="tab" data-toggle="tab">Imágenes</a></li>
                    <li role="presentation"><a href="#descuentos" aria-controls="descuentos" role="tab" data-toggle="tab">Descuentos</a></li>
                    <li role="presentation"><a href="#extras" aria-controls="extras" role="tab" data-toggle="tab">Extras</a></li>
                    <li role="presentation"><a href="#alojamientos" aria-controls="alojamientos" role="tab" data-toggle="tab">Alojamientos</a></li>
                    <li role="presentation"><a href="#cursos" aria-controls="cursos" role="tab" data-toggle="tab">Cursos</a></li>
                    <li role="presentation"><a href="#familias" aria-controls="familias" role="tab" data-toggle="tab">Familias</a></li>
                    <li role="presentation"><a href="#schools" aria-controls="schools" role="tab" data-toggle="tab">Schools</a></li>
                    <li role="presentation"><a data-label="Documentos" href="#docs" aria-controls="docs" role="tab" data-toggle="tab"><i class="fa fa-paperclip fa-fw"></i></a></li>
                    <li role="presentation"><a data-label="Cuestionarios" href="#cuestionarios" aria-controls="cuestionarios" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>
                    <li role="presentation"><a data-label="Tests" href="#tests" aria-controls="tests" role="tab" data-toggle="tab"><i class="fa fa-list-alt fa-fw"></i></a></li>
                    <li role="presentation"><a href="#contactos_sos" aria-controls="contactos_sos" role="tab" data-toggle="tab"> Contactos SOS</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('route' => array('manage.centros.ficha', $ficha->id))) !!}

                        <div class="form-group">
                            {!! Form::label('provider_id', 'Proveedor') !!}:
                            {!! Form::text('provider_id', $ficha->proveedor->name, array('class' => 'form-control', 'disabled'=> 'disabled')) !!}
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'country_id', 'texto'=> 'Pais', 'valor'=> $ficha->country_id, 'select'=> $paises])
                        </div>
                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'city_id', 'texto'=> 'Ciudad', 'valor'=> $ficha->city_id, 'select'=> $ciudades])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'address', 'texto'=> 'Dirección'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'currency_id', 'texto'=> 'Divisa', 'valor'=> $ficha->currency_id, 'select'=> $monedas])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'commission', 'texto'=> 'Comisión'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'description', 'texto'=> 'Descripción', 'help'=> 'Las imágenes no deben superar los 500px de ancho'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'settingup', 'texto'=> 'Instalaciones'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'foods', 'texto'=> 'Comidas'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'transport', 'texto'=> 'Transporte'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'center_activities', 'texto'=> 'Actividades',
                                'help'=> 'Comunes a todos los cursos de este centro'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'center_excursions', 'texto'=> 'Excursiones',
                                'help'=> 'Comunes a todos los cursos de este centro'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'internet_bool', 'texto'=> 'Internet', 'valor'=> $ficha->internet])
                            </div>
                            <div id="internet_div" class="col-md-4">
                                @include('includes.form_textarea_tinymce', [ 'campo'=> 'internet_uso', 'texto'=> 'Uso de internet'])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'lavanderia', 'texto'=> 'Lavandería'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'normas', 'texto'=> 'Normas de la escuela'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea_tinymce', [ 'campo'=> 'comment', 'texto'=> 'Depósitos'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'center_video', 'texto'=> 'Vídeo',
                            'help'=> "poner la url completa del video en Youtube, la que empieza por https://www.youtube.com/watch?v="])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select_multi', [ 'campo'=> 'monitores', 'texto'=> 'Monitores', 'valor'=> $ficha->monitores->pluck('monitor_id')->toArray(), 'select'=> \VCN\Models\Monitores\Monitor::plataforma()->pluck('name','id')->toArray() ])
                        </div>

                        @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="imagenes">

                        <div class="row">
                        <?php
                        $path = public_path() ."/assets/uploads/center/" . $ficha->center_images;
                        $folder = "/assets/uploads/center/" . $ficha->center_images;

                        if(is_dir($path))
                        {
                            $results = scandir($path);
                            foreach ($results as $result) {
                              if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0]==='.') continue;

                              $file = $path . '/' . $result;

                              $btn_class = "btn-xs btn-warning";
                              if($result==$ficha->center_image_portada)
                              {
                                $btn_class = "btn-success";
                              }

                              if( is_file($file) )
                              {
                                echo '
                                <div class="col-md-3">
                                <div class="thumbnail">
                                <img src="'.$folder . '/' . $result.'" alt="">
                                <div class="caption">
                                <p>
                                <a href="'. route('manage.centros.foto.delete', [$ficha->id,$result]).'" class="btn btn-danger btn-xs" role="button">Borrar</a>
                                <a href="'. route('manage.centros.foto.portada', [$ficha->id,$result]).'" class="btn '. $btn_class .' pull-right" role="button">Portada</a>
                                </p>
                                </div>
                                </div>
                                </div>';
                              }
                            }
                        }
                        ?>
                        </div>

                        <div class="row">
                            <hr>
                            <div class="form-group">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h4>Añadir imágenes [{{$ficha->center_images}}]</h4>
                                    </div>
                                    {{--
                                    <form id="myDropzone" class="dropzone" action="{{route('manage.centros.upload', [$ficha->id])}}" method="POST" enctype="multipart/form-data"></form>
                                    --}}

                                    <div class="dropzone" id="myDropzone"></div>

                                </div>
                            </div>
                        </div>

                        @include('includes.script_dropzone', ['name'=> 'myDropzone', 'url'=> route('manage.centros.upload', $ficha->id)])

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="descuentos">

                        @include('manage.centros.descuentos.list', ['centro_id'=> $ficha->id])

                    </div>


                    <div role="tabpanel" class="tab-pane fade in" id="extras">
                        @if(ConfigHelper::canView('extras'))
                            @include('manage.centros.extras.list', ['centro_id'=> $ficha->id])
                        @endif
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="cursos">
                        @include('manage.cursos.list', ['centro_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="alojamientos">
                        @include('manage.alojamientos.list', ['centro_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="familias">
                        @include('manage.centros.familias.list', ['centro_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="schools">
                        @include('manage.centros.schools.list', ['centro_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="docs">
                        @include('includes.documentos', ['modelo'=> 'Centro', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="cuestionarios">
                        @include('manage.system.cuestionarios.vinculado', ['modelo'=> 'Centro', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="tests">
                        @include('manage.exams.vinculado', ['modelo'=> 'Centro', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="contactos_sos">
                        @include('includes.system_contactos-tab', ['modelo'=> 'Centro', 'modelo_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'Centro',
                                'campos_text'=> [
                                    ['name'=> 'Nombre'],
                                ],
                                'campos_textarea_basic'=> [
                                ],
                                'campos_textarea'=> [
                                    ['address'=> 'Dirección'],
                                    ['description'=> 'Descripción'],
                                    ['settingup'=> 'Instalaciones'],
                                    ['food'=> 'Comidas'],
                                    ['transport'=> 'Transporte'],
                                    ['center_activities'=> 'Actividades'],
                                    ['center_excursions'=> 'Excursiones'],
                                    ['comment'=> 'Depósitos'],
                                    ['internet_uso'=> 'Uso de internet'],
                                    ['lavanderia'=> 'Lavandería'],
                                    ['normas'=> 'Normas de la escuela'],
                                ]
                            ])

                    </div>

                </div>

            </div>
        </div>

@include('includes.script_pais')
@include('includes.script_categoria')

@include('includes.script_boolean', ['campo'=> 'internet', 'checkbox'=> 1 ])

@stop