<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-tag"></i> Descuentos
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'categoria'       => 'Categoria',
                  'subcategoria'    => 'Subcategoria',
                  'detalle'         => 'Detalle Subcat.',
                  'descuento'       => 'Descuento',
                  'options'         => ''
                ])
                ->setUrl( route('manage.centros.descuentos.index', $centro_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [4] ]
                  )
                )
                ->render() !!}


            <hr>

            <h4>Añadir:</h4>

            {!! Form::open(array('route' => array('manage.centros.descuentos.add', $ficha->id))) !!}

                {!! Form::hidden('center_id', $ficha->id) !!}

                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_select', [ 'campo'=> 'category_id', 'texto'=> 'Categoría', 'select'=> $categorias])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_select', [ 'campo'=> 'subcategory_id', 'texto'=> 'Subcategoría', 'select'=> null])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_select', [ 'campo'=> 'subcategory_det_id', 'texto'=> 'Detalle Subcategoría', 'select'=> null])
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-4">
                        @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'select'=> [0=>'Porcentaje',1=>'Importe'], 'valor'=> 0])
                    </div>
                    <div id="tipo_0" class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'center_discount_percent', 'texto'=> 'Porcentaje'])
                    </div>
                    <div id="tipo_1" class="col-md-3" style="display:none;">
                        @include('includes.form_input_text', [ 'campo'=> 'importe', 'texto'=> "Importe ($ficha->moneda_name)" ])
                    </div>
                </div>

                <div class="form-group pull-right">
                    @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])
                </div>

                <div class="clearfix"></div>

            {!! Form::close() !!}

        </div>
    </div>

</div>

<script type="text/javascript">
$(document).ready( function() {
    $('#tipo').change( function() {
        // var t = $(this).val();
        $("#tipo_0").toggle();
        $("#tipo_1").toggle()
    });
});
</script>