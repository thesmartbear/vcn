@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.centros.familias.ficha', $ficha) !!}
@stop


@section('container')

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Familia</a></li>
        <li role="presentation"><a href="#bookings" aria-controls="bookings" role="tab" data-toggle="tab">Historial</a></li>
        <li role="presentation"><a href="#area" aria-controls="area" role="tab" data-toggle="tab">Área</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <div role="tabpanel" class="tab-pane fade in active" id="ficha">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bed fa-fw"></i>
                        <a href="{{route('manage.centros.ficha',$centro->id)}}#familias">
                        Centro: ({{$centro->name}})
                        </a> :: Familia {{$ficha->name}}
                </div>
                <div class="panel-body">

                    {!! Form::open(array('files'=> true,'route' => array('manage.centros.familias.ficha', $ficha->id))) !!}

                        {!! Form::hidden('center_id', $centro->id) !!}

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre', 'required'=> 'required'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'direccion', 'texto'=> 'Dirección'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_input_text', [ 'campo'=> 'cp', 'texto'=> 'CP'])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'poblacion', 'texto'=> 'Población'])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_input_text', [ 'campo'=> 'estado', 'texto'=> 'Estado'])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_select', [ 'campo'=> 'pais_id', 'texto'=> 'País', 'select'=>[""=>""]+ \VCN\Models\Pais::pluck('name','id')->toArray()])
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_input_text', [ 'campo'=> 'telefono', 'texto'=> 'Teléfono'])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'Email'])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_input_text', [ 'campo'=> 'skype', 'texto'=> 'Skype'])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_textarea', [ 'campo'=> 'rrss', 'texto'=> 'Redes Sociales'])
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                @include('includes.form_input_text', [ 'campo'=> 'distancia', 'texto'=> 'Distancia del centro enseñanza/centro actividad'])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_textarea', [ 'campo'=> 'transporte', 'texto'=> 'Transporte público'])
                            </div>
                            <div class="col-md-4">
                                <label for="animales">Animales domésticos</label>
                                <div class="form-radio">
                                    <label class="radio-inline">
                                        {!! Form::radio('animales_bool', '1', ($ficha->animales!="") ) !!} SI
                                    </label>
                                    <label class="radio-inline">
                                        {!! Form::radio('animales_bool', '0', ($ficha->animales=="")) !!} NO
                                    </label>
                                </div>

                                <div id="animales_div" style="display:none;">
                                    <br>
                                    @include('includes.form_textarea', [ 'campo'=> 'animales' ])
                                </div>
                                <span class="help-block">{{ $errors->first('animales') }}</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                @include('includes.form_textarea', [ 'campo'=> 'aficiones', 'texto'=> 'Aficiones familiares'])
                            </div>
                            <div class="col-md-6">
                                @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Comentarios'])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activa'])
                            </div>
                        </div>

                        <div class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption font-blue-sharp">Habitación</div>
                                <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
                            </div>
                            <div class="portlet-body flip-scroll">

                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label for="habitacion_compartida_bool">Habitación compartida</label>
                                        <div class="form-radio">
                                            <label class="radio-inline">
                                                {!! Form::radio('habitacion_compartida_bool', 1, ($ficha->habitacion_compartida==1)) !!} SI
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::radio('habitacion_compartida_bool', 0, ($ficha->habitacion_compartida==0)) !!} NO
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::radio('habitacion_compartida_bool', 2, ($ficha->habitacion_compartida==2)) !!} No mostrar
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div id="habitacion_compartida_div" style="display:none;">
                                            <br>
                                            @include('includes.form_textarea', [ 'campo'=> 'habitacion_compartida_notas', 'texto'=> 'Notas' ])
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption font-blue-sharp">Fotos</div>
                                <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
                            </div>
                            <div class="portlet-body flip-scroll">

                                <div class="row">
                                <?php
                                $path = public_path() ."/assets/uploads/familia/" . $ficha->id;
                                $folder = "/assets/uploads/familia/" . $ficha->id;

                                if(is_dir($path))
                                {
                                    $results = scandir($path);
                                    foreach ($results as $result) {
                                      if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0]==='.') continue;

                                      $file = $path . '/' . $result;

                                      $btn_class = "btn-xs btn-warning";
                                      if($result==$ficha->image_portada)
                                      {
                                        $btn_class = "btn-success";
                                      }

                                      if( is_file($file) )
                                      {
                                        echo '
                                        <div class="col-md-3">
                                        <div class="thumbnail">
                                        <img src="'.$folder . '/' . $result.'" alt="">
                                        <div class="caption">
                                        <p>
                                        <a href="'. route('manage.centros.familias.foto.delete', [$ficha->id,$result]).'" class="btn btn-danger btn-xs" role="button">Borrar</a>
                                        <a href="'. route('manage.centros.familias.foto.portada', [$ficha->id,$result]).'" class="btn '. $btn_class .' pull-right" role="button">Portada</a>
                                        </p>
                                        </div>
                                        </div>
                                        </div>';
                                      }
                                    }
                                }
                                ?>
                                </div>

                                <div class="row">
                                    <hr>
                                    <div class="form-group">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4>Añadir imágenes: </h4>
                                            </div>
                                            {{--
                                            <form id="myDropzone" class="dropzone" action="{{route('manage.centros.upload', [$ficha->id])}}" method="POST" enctype="multipart/form-data"></form>
                                            --}}

                                            <div class="dropzone" id="myDropzone"></div>

                                        </div>
                                    </div>
                                </div>

                                @include('includes.script_dropzone', ['name'=> 'myDropzone', 'url'=> route('manage.centros.familias.upload', $ficha->id)])

                            </div>

                        </div>

                        <div class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption font-blue-sharp">Hijos en casa
                                    <a id="hijos_add" href="#hijos_add" class='btn btn-xs btn-warning' data-label="Añadir" style="display:none;"><i class="fa fa-plus-square"></i></a>
                                </div>
                                <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
                            </div>
                            <div class="portlet-body flip-scroll">

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <div class="form-radio">
                                            <label class="radio-inline">
                                                {!! Form::radio('hijos_bool', '1', is_array($ficha->hijos)) !!} SI
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::radio('hijos_bool', '0', !is_array($ficha->hijos)) !!} NO
                                            </label>
                                        </div>

                                        <div id="hijos_div" style="display:none;" class="row">

                                            @if(is_array($ficha->hijos))

                                                @foreach($ficha->hijos['nombre'] as $kform=>$vform)

                                                    <div class="form-group row hijos_form">
                                                        <div class="col-md-3">
                                                            @include('includes.form_input_text', [ 'campo'=> "hijos[nombre][$kform]", 'texto'=> 'Nombre', 'valor'=> $ficha->hijos['nombre'][$kform] ])
                                                        </div>
                                                        <div class="col-md-2">
                                                            @include('includes.form_input_datetime', [ 'campo'=> "hijos[fechanac][$kform]", 'texto'=> 'Fecha Nac.', 'valor'=> $ficha->hijos['fechanac'][$kform] ])
                                                        </div>
                                                        <div class="col-md-1">
                                                            @include('includes.form_input_number', [ 'campo'=> "hijos[edad][$kform]", 'texto'=> 'Edad', 'valor'=> $ficha->hijos['edad'][$kform] ])
                                                        </div>
                                                        <div class="col-md-3">
                                                            @include('includes.form_input_text', [ 'campo'=> "hijos[aficiones][$kform]", 'texto'=> 'Aficiones', 'valor'=> $ficha->hijos['aficiones'][$kform] ])
                                                        </div>
                                                        <div class="col-md-1">
                                                            {!! Form::label("hijos[sexo][$kform]", "Sexo") !!}
                                                            {!! Form::select("hijos[sexo][$kform]", [0=>""]+[1=>'Hombre']+[2=>'Mujer'], isset($ficha->hijos['sexo'])?(isset($ficha->hijos['sexo'][$kform])?$ficha->hijos['sexo'][$kform]:0):0) !!}
                                                            <span class="help-block">{{ $errors->first("hijos[sexo][$kform]") }}</span>
                                                        </div>
                                                        <div class="col-md-1">
                                                            {!! Form::label("Borrar") !!}<br>
                                                            <a href="#hijos_div" class='btn btn-xs btn-danger hijos_del' data-label="Borrar" data-id='{{$kform}}'><i class="fa fa-minus-square"></i></a>
                                                        </div>
                                                    </div>

                                                @endforeach

                                            @else

                                                <div class="form-group row hijos_form">
                                                    <div class="col-md-3">
                                                        @include('includes.form_input_text', [ 'campo'=> 'hijos[nombre][]', 'texto'=> 'Nombre', 'valor'=>''])
                                                    </div>
                                                    <div class="col-md-2">
                                                        @include('includes.form_input_datetime', [ 'campo'=> 'hijos[fechanac][]', 'texto'=> 'Fecha Nac.', 'valor'=>''])
                                                    </div>
                                                    <div class="col-md-1">
                                                        @include('includes.form_input_number', [ 'campo'=> 'hijos[edad][]', 'texto'=> 'Edad', 'valor'=>''])
                                                    </div>
                                                    <div class="col-md-4">
                                                        @include('includes.form_input_text', [ 'campo'=> 'hijos[aficiones][]', 'texto'=> 'Aficiones', 'valor'=>''])
                                                    </div>
                                                </div>

                                            @endif

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption font-blue-sharp">Hijos viviendo fuera casa
                                    <a id="hijos_fuera_add" href="#hijos_fuera_add" class='btn btn-xs btn-warning' data-label="Añadir" style="display:none;"><i class="fa fa-plus-square"></i></a>
                                </div>
                                <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
                            </div>
                            <div class="portlet-body flip-scroll">

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <div class="form-radio">
                                            <label class="radio-inline">
                                                {!! Form::radio('hijos_fuera_bool', '1', is_array($ficha->hijos_fuera)) !!} SI
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::radio('hijos_fuera_bool', '0', !is_array($ficha->hijos_fuera)) !!} NO
                                            </label>
                                        </div>

                                        <div id="hijos_fuera_div" style="display:none;">

                                            @if(is_array($ficha->hijos_fuera))

                                                @foreach($ficha->hijos_fuera['nombre'] as $kform=>$vform)

                                                    <div class="form-group row hijos_fuera_form">

                                                        <div class="col-md-4">
                                                            @include('includes.form_input_text', [ 'campo'=> 'hijos_fuera[nombre][]', 'texto'=> 'Nombre', 'valor'=> $ficha->hijos_fuera['nombre'][$kform] ])
                                                        </div>
                                                        <div class="col-md-1">
                                                            @include('includes.form_input_number', [ 'campo'=> 'hijos_fuera[edad][]', 'texto'=> 'Edad', 'valor'=> $ficha->hijos_fuera['edad'][$kform] ])
                                                        </div>
                                                        <div class="col-md-1">
                                                            {!! Form::label("hijos_fuera[sexo][]", "Sexo") !!}
                                                            {!! Form::select("hijos_fuera[sexo][]", [0=>""]+[1=>'Hombre']+[2=>'Mujer'], isset($ficha->hijos_fuera['sexo'])?$ficha->hijos_fuera['sexo'][$kform]:0) !!}
                                                            <span class="help-block">{{ $errors->first("hijos_fuera[sexo][$kform]") }}</span>
                                                        </div>
                                                        <div class="col-md-1">
                                                            {!! Form::label("Borrar") !!}<br>
                                                            <a href="#hijos_fuera_div" class='btn btn-xs btn-danger hijos_fuera_del' data-label="Borrar" data-id='{{$kform}}'><i class="fa fa-minus-square"></i></a>
                                                        </div>
                                                    </div>

                                                @endforeach

                                            @else

                                                <div class="form-group row hijos_fuera_form">
                                                    <div class="col-md-4">
                                                        @include('includes.form_input_text', [ 'campo'=> 'hijos_fuera[nombre][]', 'texto'=> 'Nombre'])
                                                    </div>
                                                    <div class="col-md-1">
                                                        @include('includes.form_input_number', [ 'campo'=> 'hijos_fuera[edad][]', 'texto'=> 'Edad'])
                                                    </div>
                                                    <div class="col-md-1">
                                                        {!! Form::label("Sexo") !!}
                                                        {!! Form::select('hijos_fuera[sexo][]', [0=>""]+[1=>'Hombre']+[2=>'Mujer']) !!}
                                                    </div>
                                                </div>

                                            @endif

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption font-blue-sharp">Otros miembros de la familia
                                    <a id="familia_add" href="#familia_add" class='btn btn-xs btn-warning' data-label="Añadir" style="display:none;"><i class="fa fa-plus-square"></i></a>
                                </div>
                                <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
                            </div>
                            <div class="portlet-body flip-scroll">

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <div class="form-radio">
                                            <label class="radio-inline">
                                                {!! Form::radio('familia_bool', '1', is_array($ficha->familia)) !!} SI
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::radio('familia_bool', '0', !is_array($ficha->familia)) !!} NO
                                            </label>
                                        </div>

                                        <div id="familia_div" style="display:none;">

                                            @if(is_array($ficha->familia))

                                                @foreach($ficha->familia['nombre'] as $kform=>$vform)

                                                    <div class="familia_form">

                                                        <div class="col-md-3">
                                                            @include('includes.form_input_text', [ 'campo'=> 'familia[nombre][]', 'texto'=> 'Nombre', 'valor'=> $ficha->familia['nombre'][$kform] ])
                                                        </div>
                                                        <div class="col-md-3">
                                                            @include('includes.form_input_text', [ 'campo'=> 'familia[parentesco][]', 'texto'=> 'Parentesco', 'valor'=> $ficha->familia['parentesco'][$kform] ])
                                                        </div>
                                                        <div class="col-md-1">
                                                            @include('includes.form_input_number', [ 'campo'=> 'familia[edad][]', 'texto'=> 'Edad', 'valor'=> $ficha->familia['edad'][$kform] ])
                                                        </div>
                                                        <div class="col-md-1">
                                                            {!! Form::label("Borrar") !!}<br>
                                                            <a href="#familia_div" class='btn btn-xs btn-danger familia_del' data-label="Borrar" data-id='{{$kform}}'><i class="fa fa-minus-square"></i></a>
                                                        </div>
                                                    </div>

                                                @endforeach

                                            @else

                                                <div class="familia_form">
                                                    <div class="col-md-3">
                                                        @include('includes.form_input_text', [ 'campo'=> 'familia[nombre][]', 'texto'=> 'Nombre'])
                                                    </div>
                                                    <div class="col-md-3">
                                                        @include('includes.form_input_text', [ 'campo'=> 'familia[parentesco][]', 'texto'=> 'Parentesco'])
                                                    </div>
                                                    <div class="col-md-1">
                                                        @include('includes.form_input_number', [ 'campo'=> 'familia[edad][]', 'texto'=> 'Edad'])
                                                    </div>
                                                </div>

                                            @endif

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption font-blue-sharp">Adultos
                                    <a id="adultos_add" href="#adultos_add" class='btn btn-xs btn-warning' data-label="Añadir"><i class="fa fa-plus-square"></i></a>
                                </div>
                                <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
                            </div>
                            <div class="portlet-body flip-scroll">


                                <div id="adultos_div">

                                    @if(is_array($ficha->adultos))

                                        @foreach($ficha->adultos['nombre'] as $kform=>$vform)

                                            <div class="adultos_form">

                                                <div class="row">
                                                    <div class="col-md-2">
                                                        @include('includes.form_input_text', [ 'campo'=> 'adultos[nombre][]', 'texto'=> 'Nombre', 'valor'=> $ficha->adultos['nombre'][$kform] ])
                                                    </div>
                                                    <div class="col-md-1">
                                                        @include('includes.form_input_number', [ 'campo'=> 'adultos[edad][]', 'texto'=> 'Edad', 'valor'=> $ficha->adultos['edad'][$kform] ])
                                                    </div>
                                                    <div class="col-md-2">
                                                        @include('includes.form_input_text', [ 'campo'=> 'adultos[edades][]', 'texto'=> 'Horquilla edades', 'valor'=> $ficha->adultos['edades'][$kform] ])
                                                    </div>
                                                    <div class="col-md-1">
                                                        {!! Form::label("adultos[sexo][]", "Sexo") !!}
                                                        {!! Form::select("adultos[sexo][]", [0=>""]+[1=>'Hombre']+[2=>'Mujer'], $ficha->adultos['sexo'][$kform]) !!}
                                                        <span class="help-block">{{ $errors->first("adultos[sexo][$kform]") }}</span>
                                                    </div>

                                                    <div class="col-md-3">
                                                        @include('includes.form_input_text', [ 'campo'=> 'adultos[ocupacion][]', 'texto'=> 'Ocupación', 'valor'=> $ficha->adultos['ocupacion'][$kform] ])
                                                    </div>
                                                    <div class="col-md-3">
                                                        @include('includes.form_input_text', [ 'campo'=> 'adultos[aficiones][]', 'texto'=> 'Aficiones', 'valor'=> $ficha->adultos['aficiones'][$kform] ])
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-2">
                                                        @include('includes.form_input_text', [ 'campo'=> 'adultos[movil][]', 'texto'=> 'Móvil', 'valor'=> $ficha->adultos['movil'][$kform] ])
                                                    </div>

                                                    <div class="col-md-3">
                                                        @include('includes.form_input_text', [ 'campo'=> 'adultos[email][]', 'texto'=> 'E-mail', 'valor'=> $ficha->adultos['email'][$kform] ])
                                                    </div>

                                                    <div class="col-md-3">
                                                    @if(isset($ficha->adultos['foto']))
                                                        @if(isset($ficha->adultos['foto'][$kform]))
                                                            <div class="col-sm-4 col-xs-12 frm-img">
                                                                <img class="img-responsive img-thumbnail" src="{{$ficha->adultos['foto'][$kform]}}" />
                                                                {!! Form::hidden("adultos_foto[$kform]", $ficha->adultos['foto'][$kform]) !!}
                                                            </div>
                                                        @endif
                                                    @endif

                                                    @include('includes.form_input_file', [ 'campo'=> "adultos[foto][$kform]", 'texto'=> 'Foto'])
                                                    </div>


                                                    <div class="col-md-1">
                                                        {!! Form::label("Borrar") !!}<br>
                                                        <a href="#adultos_div" class='btn btn-xs btn-danger adultos_del' data-label="Borrar" data-id='{{$kform}}'><i class="fa fa-minus-square"></i></a>
                                                    </div>

                                                </div>

                                            </div>

                                            <hr>

                                        @endforeach

                                    @else

                                        <div class="adultos_form">

                                            <div class="row">
                                                <div class="col-md-2">
                                                    @include('includes.form_input_text', [ 'campo'=> 'adultos[nombre][]', 'texto'=> 'Nombre'])
                                                </div>
                                                <div class="col-md-1">
                                                    @include('includes.form_input_number', [ 'campo'=> 'adultos[edad][]', 'texto'=> 'Edad'])
                                                </div>
                                                <div class="col-md-2">
                                                    @include('includes.form_input_text', [ 'campo'=> 'adultos[edades][]', 'texto'=> 'Horquilla edades'])
                                                </div>
                                                <div class="col-md-1">
                                                    {!! Form::label("adultos[sexo][]", "Sexo") !!}
                                                    {!! Form::select('adultos[sexo][]', [0=>""]+[1=>'Hombre']+[2=>'Mujer'], 0) !!}
                                                    <span class="help-block">{{ $errors->first('adultos[sexo][]') }}</span>
                                                </div>

                                                <div class="col-md-3">
                                                    @include('includes.form_input_text', [ 'campo'=> 'adultos[ocupacion][]', 'texto'=> 'Ocupación'])
                                                </div>
                                                <div class="col-md-3">
                                                    @include('includes.form_input_text', [ 'campo'=> 'adultos[aficiones][]', 'texto'=> 'Aficiones'])
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-2">
                                                    @include('includes.form_input_text', [ 'campo'=> 'adultos[movil][]', 'texto'=> 'Móvil'])
                                                </div>

                                                <div class="col-md-3">
                                                    @include('includes.form_input_text', [ 'campo'=> 'adultos[email][]', 'texto'=> 'E-mail'])
                                                </div>

                                                <div class="col-md-3">
                                                    @include('includes.form_input_file', [ 'campo'=> 'adultos[foto][]', 'texto'=> 'Foto'])
                                                </div>
                                            </div>

                                        </div>

                                    @endif

                                </div>

                                <div class="clearfix"></div>

                            </div>
                        </div>

                        @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                        <div class="clearfix"></div>

                    {!! Form::close() !!}


                </div>
            </div>

        </div>

        <div role="tabpanel" class="tab-pane fade in" id="bookings">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bed fa-fw"></i>
                        <a href="{{route('manage.centros.ficha',$centro->id)}}#familias">
                        Centro: ({{$centro->name}})
                        </a> :: Familia {{$ficha->name}}
                </div>
                <div class="panel-body">

                    {!! Datatable::table()
                        ->addColumn([
                          'booking'     => 'Booking',
                          'viajero'     => 'Viajero',
                          'fecha_ini'   => 'Fecha inicio',
                          'fecha_fin'   => 'Fecha fin',
                          'fecha_asignacion'    => 'Fecha asignación',
                          'user_asignacion'     => 'Usuario asignación',
                          'fecha_activacion'    => 'Fecha activación',
                          'user_activacion'     => 'Usuario activación',
                          'fecha_cambio'        => 'Fecha último cambio',
                          'user_cambio'         => 'Usuario último cambio'
                        ])
                        ->setUrl( route('manage.centros.familias.historial', $ficha->id) )
                        ->setOptions(
                          "aoColumnDefs", array(
                            //[ "bSortable" => false, "aTargets" => [3] ]
                            [ "targets" => [2,3], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                            //[ "targets" => [4,6,8], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY H:mm'):'-';}" ],
                          )
                        )
                        ->render() !!}

                </div>
            </div>

        </div>

        <div role="tabpanel" class="tab-pane fade in" id="area">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bed fa-fw"></i>

                </div>
                <div class="panel-body">

                    @include('manage.centros.familias.area')

                </div>

            </div>
        </div>

    </div>

@include('includes.script_boolean', ['campo'=> 'animales' ])
@include('includes.script_boolean', ['campo'=> 'habitacion_compartida' ])
@include('includes.script_boolean', ['campo'=> 'hijos' ])
@include('includes.script_boolean', ['campo'=> 'hijos_fuera' ])
@include('includes.script_boolean', ['campo'=> 'familia' ])

@include('includes.script_form_add', ['campo'=> 'hijos' ])
@include('includes.script_form_add', ['campo'=> 'hijos_fuera' ])
@include('includes.script_form_add', ['campo'=> 'familia' ])
@include('includes.script_form_add', ['campo'=> 'adultos' ])

<script type="text/javascript">
// $(document).ready(function() {

//     $(div +" div"+ form).find("input").each( function(){
//         var n = $(this).attr('name');
//         n = n.slice(0,-3);
//         $(this).attr('name', n+"[]");
//         $(this).attr('id', n+"[]");
//     });

// });
</script>

@stop