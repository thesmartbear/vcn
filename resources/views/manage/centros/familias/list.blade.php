
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-users"></i> Familias

            @if($centro_id)
              <span class="pull-right"><a href="{{ route('manage.centros.familias.nuevo', $centro_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Familia</a></span>
            @endif

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'        => 'Nombre',
                  'activo'      => 'Activa',
                  'bookings'    => 'Asignados',
                  'options'     => ''
                ])
                ->setUrl( route('manage.centros.familias.index', $centro_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [3] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

