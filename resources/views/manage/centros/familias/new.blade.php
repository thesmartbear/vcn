@extends('layouts.manage')

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-bed fa-fw"></i>
                <a href="{{route('manage.centros.ficha',$centro->id)}}#familias">
                Centro: ({{$centro->name}})
                </a> :: Familia Nueva
        </div>
        <div class="panel-body">

            {!! Form::open(array('files'=> true,'route' => array('manage.centros.familias.ficha', 0))) !!}

                {!! Form::hidden('center_id', $centro->id) !!}

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre', 'required'=> 'required'])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'direccion', 'texto'=> 'Dirección'])
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        @include('includes.form_input_text', [ 'campo'=> 'cp', 'texto'=> 'CP'])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'poblacion', 'texto'=> 'Población'])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'estado', 'texto'=> 'Estado'])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_select', [ 'campo'=> 'pais_id', 'texto'=> 'País', 'select'=>[""=>""]+ \VCN\Models\Pais::pluck('name','id')->toArray(), 'valor'=> $centro->country_id])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        @include('includes.form_input_text', [ 'campo'=> 'telefono', 'texto'=> 'Teléfono'])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'Email'])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'skype', 'texto'=> 'Skype'])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_textarea', [ 'campo'=> 'rrss', 'texto'=> 'Redes Sociales'])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'distancia', 'texto'=> 'Distancia del centro enseñanza/centro actividad'])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_textarea', [ 'campo'=> 'transporte', 'texto'=> 'Transporte público'])
                    </div>
                    <div class="col-md-4">
                        <label for="animales">Animales domésticos</label>
                        <div class="form-radio">
                            <label class="radio-inline">
                                {!! Form::radio('animales_bool', '1', false) !!} SI
                            </label>
                            <label class="radio-inline">
                                {!! Form::radio('animales_bool', '0', true) !!} NO
                            </label>
                        </div>

                        <div id="animales_div" style="display:none;">
                            <br>
                            @include('includes.form_textarea', [ 'campo'=> 'animales' ])
                        </div>
                        <span class="help-block">{{ $errors->first('animales') }}</span>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-3">
                        @include('includes.form_textarea', [ 'campo'=> 'aficiones', 'texto'=> 'Aficiones familiares'])
                    </div>
                    <div class="col-md-6">
                        @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Comentarios'])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activa'])
                    </div>
                </div>


                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption font-blue-sharp">Habitación</div>
                        <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
                    </div>
                    <div class="portlet-body flip-scroll">

                        <div class="form-group row">
                            <div class="col-md-2">
                                <label for="habitacion_compartida_bool">Habitación compartida</label>
                                <div class="form-radio">
                                    <label class="radio-inline">
                                        {!! Form::radio('habitacion_compartida_bool', '1', false) !!} SI
                                    </label>
                                    <label class="radio-inline">
                                        {!! Form::radio('habitacion_compartida_bool', '0', true) !!} NO
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div id="habitacion_compartida_div" style="display:none;">
                                    <br>
                                    @include('includes.form_textarea', [ 'campo'=> 'habitacion_compartida_notas', 'texto'=> 'Notas' ])
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption font-blue-sharp">Hijos en casa
                            <a id="hijos_add" href="#hijos_add" class='btn btn-xs btn-warning' data-label="Añadir" style="display:none;"><i class="fa fa-plus-square"></i></a>
                        </div>
                        <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
                    </div>
                    <div class="portlet-body flip-scroll">

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-radio">
                                    <label class="radio-inline">
                                        {!! Form::radio('hijos_bool', '1', false) !!} SI
                                    </label>
                                    <label class="radio-inline">
                                        {!! Form::radio('hijos_bool', '0', true) !!} NO
                                    </label>
                                </div>

                                <div id="hijos_div" style="display:none;" class="row">

                                    <div class="form-group hijos_form">
                                        <div class="col-md-3">
                                            @include('includes.form_input_text', [ 'campo'=> 'hijos[nombre][]', 'texto'=> 'Nombre'])
                                        </div>
                                        <div class="col-md-3">
                                            @include('includes.form_input_datetime', [ 'campo'=> 'hijos[fechanac][]', 'texto'=> 'Fecha Nac.'])
                                        </div>
                                        <div class="col-md-1">
                                            @include('includes.form_input_number', [ 'campo'=> 'hijos[edad][]', 'texto'=> 'Edad'])
                                        </div>
                                        <div class="col-md-4">
                                            @include('includes.form_input_text', [ 'campo'=> 'hijos[aficiones][]', 'texto'=> 'Aficiones'])
                                        </div>
                                        <div class="col-md-1">
                                            {!! Form::label("hijos[sexo][]", "Sexo") !!}
                                            {!! Form::select('hijos[sexo][]', [0=>""]+[1=>'Hombre']+[2=>'Mujer']) !!}
                                            <span class="help-block">{{ $errors->first("hijos[sexo][]") }}</span>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>


                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption font-blue-sharp">Hijos viviendo fuera casa
                            <a id="hijos_fuera_add" href="#hijos_fuera_add" class='btn btn-xs btn-warning' data-label="Añadir" style="display:none;"><i class="fa fa-plus-square"></i></a>
                        </div>
                        <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
                    </div>
                    <div class="portlet-body flip-scroll">

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-radio">
                                    <label class="radio-inline">
                                        {!! Form::radio('hijos_fuera_bool', '1', false) !!} SI
                                    </label>
                                    <label class="radio-inline">
                                        {!! Form::radio('hijos_fuera_bool', '0', true) !!} NO
                                    </label>
                                </div>

                                <div id="hijos_fuera_div" style="display:none;">

                                    <div class="form-group hijos_fuera_form">
                                        <div class="col-md-3">
                                            @include('includes.form_input_text', [ 'campo'=> 'hijos_fuera[nombre][]', 'texto'=> 'Nombre'])
                                        </div>
                                        <div class="col-md-1">
                                            @include('includes.form_input_number', [ 'campo'=> 'hijos_fuera[edad][]', 'texto'=> 'Edad'])
                                        </div>
                                        <div class="col-md-1">
                                            {!! Form::label("hijos_fuera[sexo][]", "Sexo") !!}
                                            {!! Form::select('hijos_fuera[sexo][]', [0=>""]+[1=>'Hombre']+[2=>'Mujer']) !!}
                                            <span class="help-block">{{ $errors->first("hijos_fuera[sexo][]") }}</span>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption font-blue-sharp">Otros miembros de la familia
                            <a id="familia_add" href="#familia_add" class='btn btn-xs btn-warning' data-label="Añadir" style="display:none;"><i class="fa fa-plus-square"></i></a>
                        </div>
                        <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
                    </div>
                    <div class="portlet-body flip-scroll">

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-radio">
                                    <label class="radio-inline">
                                        {!! Form::radio('familia_bool', '1', false) !!} SI
                                    </label>
                                    <label class="radio-inline">
                                        {!! Form::radio('familia_bool', '0', true) !!} NO
                                    </label>
                                </div>

                                <div id="familia_div" style="display:none;">

                                    <div class="familia_form">
                                        <div class="col-md-3">
                                            @include('includes.form_input_text', [ 'campo'=> 'familia[nombre][]', 'texto'=> 'Nombre'])
                                        </div>
                                        <div class="col-md-3">
                                            @include('includes.form_input_text', [ 'campo'=> 'familia[parentesco][]', 'texto'=> 'Parentesco'])
                                        </div>
                                        <div class="col-md-1">
                                            @include('includes.form_input_number', [ 'campo'=> 'familia[edad][]', 'texto'=> 'Edad'])
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption font-blue-sharp">Adultos
                            <a id="adultos_add" href="#adultos_add" class='btn btn-xs btn-warning' data-label="Añadir"><i class="fa fa-plus-square"></i></a>
                        </div>
                        <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
                    </div>
                    <div class="portlet-body flip-scroll">


                        <div id="adultos_div">

                            <div class="adultos_form">

                                <div class="row">
                                    <div class="col-md-2">
                                        @include('includes.form_input_text', [ 'campo'=> 'adultos[nombre][]', 'texto'=> 'Nombre'])
                                    </div>
                                    <div class="col-md-1">
                                        @include('includes.form_input_number', [ 'campo'=> 'adultos[edad][]', 'texto'=> 'Edad'])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_input_text', [ 'campo'=> 'adultos[edades][]', 'texto'=> 'Horquilla edades'])
                                    </div>
                                    <div class="col-md-1">
                                        {!! Form::label("adultos[sexo][]", "Sexo") !!}
                                        {!! Form::select('adultos[sexo][]', [0=>""]+[1=>'Hombre']+[2=>'Mujer']) !!}
                                        <span class="help-block">{{ $errors->first("adultos[sexo][]") }}</span>
                                    </div>

                                    <div class="col-md-3">
                                        @include('includes.form_input_text', [ 'campo'=> 'adultos[ocupacion][]', 'texto'=> 'Ocupación'])
                                    </div>
                                    <div class="col-md-3">
                                        @include('includes.form_input_text', [ 'campo'=> 'adultos[aficiones][]', 'texto'=> 'Aficiones'])
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2">
                                        @include('includes.form_input_text', [ 'campo'=> 'adultos[movil][]', 'texto'=> 'Móvil'])
                                    </div>

                                    <div class="col-md-3">
                                        @include('includes.form_input_text', [ 'campo'=> 'adultos[email][]', 'texto'=> 'E-mail'])
                                    </div>

                                    <div class="col-md-3">
                                        @include('includes.form_input_file', [ 'campo'=> 'adultos[foto][]', 'texto'=> 'Foto'])
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="clearfix"></div>

                    </div>
                </div>

                @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])

                <div class="clearfix"></div>

            {!! Form::close() !!}


        </div>
    </div>

@include('includes.script_boolean', ['campo'=> 'animales' ])
@include('includes.script_boolean', ['campo'=> 'habitacion_compartida' ])
@include('includes.script_boolean', ['campo'=> 'hijos' ])
@include('includes.script_boolean', ['campo'=> 'hijos_fuera' ])
@include('includes.script_boolean', ['campo'=> 'familia' ])

@include('includes.script_form_add', ['campo'=> 'hijos' ])
@include('includes.script_form_add', ['campo'=> 'hijos_fuera' ])
@include('includes.script_form_add', ['campo'=> 'familia' ])
@include('includes.script_form_add', ['campo'=> 'adultos' ])

@stop