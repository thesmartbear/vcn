<html>
<head>
    <title>CURSO - Familia {{$ficha->name}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    <!-- Bootstrap -->
    {!! Html::style('assets/css/bootstrap.css') !!}
            <!-- font awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    {!! Html::style('assets/css/area.css') !!}
    {!! Html::style('assets/css/components.css') !!}


    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            background: transparent;
            text-shadow: none;
        }
        body{
            font-size: 1.2em;
            margin-left: 2cm !important;
            margin-right: 2cm !important;
        }

        .page{
        }

        #contenido{
            padding-top: 0;
            margin-top: 0;
        }

        .container{
            padding: 0 2cm;
        }

        h2{
            margin-top: 2cm;
            font-size: 1.8em;
            font-weight: bold;
            @if(ConfigHelper::config('propietario') == 1)
                color: #f1c40f;
            @elseif(ConfigHelper::config('propietario') == 2)
                color: #3B6990;
            @endif
        }
        h2:first-child{
            margin-top: 1cm;
        }
        h4{
            margin-top: 30px;
            page-break-inside: avoid;
            text-transform: capitalize;
        }

        .logo{
            margin-bottom: 40px;
        }

    </style>

</head>

<body class="page">
    <div class="row logo">
        <div class="col-xs-12"><img style="width: 4.5cm; height: auto; margin-top: 30px;" class="pull-right" src="https://{{ConfigHelper::config('web')}}/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" /></div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="caption font-green-sharp">
                <i class="fa fa-plane fa-2x"></i>
                <span class="caption-subject bold uppercase">
                    CURSO XXX
                </span>
                <span class="caption-helper separator"> | </span>
                <span class="caption-helper out"><i class="fa fa-arrow-circle-right fa-lg text-success"></i> FECHA DESDE</span>
                <span class="caption-helper in"><i class="fa fa-arrow-circle-left fa-lg text-danger"></i> FECHA HASTA </span>
                <h4 class="text-muted"><i class="fa fa-male fa-fw text-muted"></i> VIAJERO </h4>
            </div>

            <hr>

            <h3 class="page-title font-green-sharp">Familia {{$ficha->name}} <small> [ALOJAMIENTO]</small></h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.datoscontacto')}}</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="well">
                @if($ficha->direccion)
                    <strong>{{trans('area.direccion')}}:</strong> {{$ficha->direccion}}<br />
                @endif

                @if($ficha->cp)
                    <strong>{{trans('area.cp')}}:</strong> {{$ficha->cp}}<br />
                @endif

                @if($ficha->poblacion)
                    <strong>{{trans('area.poblacion')}}:</strong> {{$ficha->poblacion}}<br />
                @endif

                @if($ficha->pais)
                    <strong>{{trans('area.pais')}}:</strong> {{$ficha->pais->name}}
                @endif

                <hr>

                @if($ficha->telefono)
                    <strong>{{trans('area.telefono')}}:</strong> {{$ficha->telefono}}<br />
                @endif

                @if($ficha->email)
                    <strong>{{trans('area.email')}}:</strong> {{$ficha->email}}<br />
                @endif

                @if($ficha->skype)
                    <strong><i class='fa fa-skype'></i>:</strong> {{$ficha->skype}}<br />
                @endif

                @if($ficha->rrss)
                    <strong>{{trans('area.redessociales')}}:</strong> {{$ficha->rrss}}<br />
                @endif


                @if($ficha->distancia)
                    <strong>{{trans('area.distanciacentro')}}:</strong> {{$ficha->distancia}}<br />
                @endif

                @if($ficha->transporte)
                    <strong>{{trans('area.transportepublico')}}:</strong> {{$ficha->transporte}}<br />
                @endif

                @if($ficha->animales)
                    <strong>{{trans('area.animalesdomesticos')}}:</strong> {{$ficha->animales}}<br />
                @endif

                @if($ficha->aficiones)
                    <strong>{{trans('area.aficionesfamiliares')}}:</strong> {{$ficha->aficiones}}<br />
                @endif

                @if($ficha->notas)
                    <strong>{{trans('area.comentarios')}}:</strong> {{$ficha->notas}}
                @endif

            </div>
        </div>

        @if(strip_tags($ficha->direccion) != '')
            <div class="col-md-6">
                <img src="https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=625x250&center={{strip_tags($ficha->direccion)}}, {{strip_tags($ficha->poblacion)}}, {{strip_tags($ficha->cp)}}, {{strip_tags($ficha->pais->name)}}&markers=clor:red|{{strip_tags($ficha->direccion)}}, {{strip_tags($ficha->poblacion)}}, {{strip_tags($ficha->cp)}}, {{strip_tags($ficha->pais->name)}}&key=AIzaSyCNPWo91Kd3D656Z5R2U6K1vFlx65KxpE8" />
            </div>
        @endif
    </div>


    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.adultos')}}</h4>
        </div>

        @if(is_array($ficha->adultos))
            @foreach($ficha->adultos['nombre'] as $kform=>$vform)
                <div class="row">
                    <div class="col-sm-6">
                        @if(isset($ficha->adultos['foto']))
                            @if(isset($ficha->adultos['foto'][$kform]))
                                <div class="col-xs-4">
                                    <img class="img-responsive img-thumbnail" src="https://{{ConfigHelper::config('web')}}{{$ficha->adultos['foto'][$kform]}}" />
                                </div>
                            @endif
                        @endif

                        <div class="col-xs-8">
                            <strong>{{trans('area.nombre')}}:</strong> {{$ficha->adultos['nombre'][$kform]}}<br />

                            @if($ficha->adultos['edad'][$kform])
                                <strong>{{trans('area.edad')}}:</strong> {{$ficha->adultos['edad'][$kform]}}<br />
                            @endif

                            @if(!$ficha->adultos['edades'][$kform] && $ficha->adultos['edades'][$kform])
                                <strong>{{trans('area.edad')}}:</strong> {{$ficha->adultos['edades'][$kform]}}<br />
                            @endif

                            @if($ficha->adultos['sexo'][$kform])
                                <strong>{{trans('area.sexo')}}:</strong> {{$ficha->adultos['sexo'][$kform]==1?trans('area.hombre'):trans('area.mujer')}}<br />
                            @endif

                            @if($ficha->adultos['ocupacion'][$kform])
                                <strong>{{trans('area.ocupacion')}}:</strong> {{$ficha->adultos['ocupacion'][$kform]}}<br />
                            @endif

                            @if($ficha->adultos['aficiones'][$kform])
                                <strong>{{trans('area.aficiones')}}:</strong> {{$ficha->adultos['aficiones'][$kform]}}<br />
                            @endif

                            @if($ficha->adultos['movil'][$kform])
                                <strong>{{trans('area.movil')}}:</strong> {{$ficha->adultos['movil'][$kform]}}<br />
                            @endif

                            @if($ficha->adultos['email'][$kform])
                                <strong>{{trans('area.email')}}:</strong> {{$ficha->adultos['email'][$kform]}}<br />
                            @endif
                        </div>

                    </div>
                </div>
            @endforeach
        @endif
    </div>

    @if(is_array($ficha->hijos))
    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.hijosencasa')}}</h4>
        </div>
            @foreach($ficha->hijos['nombre'] as $kform=>$vform)

                <div class="col-md-3">
                    <strong>{{trans('area.nombre')}}:</strong> {{$ficha->hijos['nombre'][$kform]}}<br />

                    @if($ficha->hijos['fechanac'][$kform])
                        <strong>{{trans('area.fnacimiento')}}:</strong> {{$ficha->hijos['fechanac'][$kform]}}<br />
                    @endif


                    <strong>{{trans('area.edad')}}:</strong>{{$ficha->hijos['edad'][$kform]?$ficha->hijos['edad'][$kform]:"-"}}<br />


                    @if($ficha->hijos['aficiones'][$kform])
                        <strong>{{trans('area.aficiones')}}:</strong> {{$ficha->hijos['aficiones'][$kform]}}
                    @endif

                </div>
            @endforeach
    </div>
    @endif

    @if(is_array($ficha->hijos_fuera))
    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.hijosfuera')}}</h4>
        </div>
            @foreach($ficha->hijos_fuera['nombre'] as $kform=>$vform)
                <div class="col-md-3">
                    <strong>{{trans('area.nombre')}}:</strong> {{$ficha->hijos_fuera['nombre'][$kform]}}<br />
                    <strong>{{trans('area.edad')}}:</strong> {{$ficha->hijos_fuera['edad'][$kform]?$ficha->hijos_fuera['edad'][$kform]:"-"}}
                </div>
            @endforeach
    </div>
    @endif

    @if(is_array($ficha->familia))
    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.otrosmiembros')}}</h4>
        </div>


            @foreach($ficha->familia['nombre'] as $kform=>$vform)
                <div class="col-md-3">
                    <strong>{{trans('area.nombre')}}:</strong> {{$ficha->familia['nombre'][$kform]}}<br />

                    @if($ficha->familia['parentesco'][$kform])
                        <strong>{{trans('area.parentesco')}}:</strong> {{$ficha->familia['parentesco'][$kform]}}<br />
                    @endif

                    @if($ficha->familia['edad'][$kform])
                        <strong>{{trans('area.edad')}}:</strong> {{$ficha->familia['edad'][$kform]}}<br />
                    @endif
                </div>
            @endforeach

    </div>
    @endif


    @if($ficha->habitacion_compartida == 1)
        <div class="row">
            <div class="col-md-12">
                <h4 class="form-section text-primary">{{trans('area.habitacion')}}</h4>
            </div>
            <div class="col-md-12">
                <p><strong>{{trans('area.tipohabitacion')}}:</strong> {{$ficha->habitacion_compartida?trans('area.compartida'):trans('area.individual')}}</p>
                @if($ficha->habitacion_compartida_notas)
                    <p><strong>{{trans('area.notas')}}:</strong> {{$ficha->habitacion_compartida_notas}}</p>
                @endif
            </div>
        </div>
    @endif


    <?php
    $folder = "/assets/uploads/familia/" . $ficha->id;
    $path = public_path() . $folder;
    $fotos = '';

    if (is_dir($path)) {
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

            $file = $path . '/' . $result;

            if (is_file($file)) {
                $fotos .= '
                            <div class="col-xs-4">
                                <a rel="group" href="https://'. ConfigHelper::config('web'). $folder . '/' . $result . '"><div style="position: relative; overflow: hidden; pading-bottom: 100%;"><img style="margin-bottom: 10px;" class="img-responsive full-width img-thumbnail" src="//'. ConfigHelper::config('web') . $folder . '/' . $result . '" alt=""></div></a>
                            </div>';

            }
        }
    }
    ?>
    @if ($fotos != '')
        <div class="row">
            <div class="col-md-12">
                <h4 class="form-section text-primary">{{trans('area.fotos')}}</h4>
                <div id="fotos">
                    {!! $fotos !!}
                </div>
            </div>
        </div>
    @endif



</body>
</html>