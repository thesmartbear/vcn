@extends('layouts.manage')


@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.nuevo') !!} --}}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-gift fa-fw"></i> Nuevo Descuento Early Bird
            </div>
            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.descuentos.early-bird.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'importe', 'texto'=> 'Importe'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas,
                                'valor'=> ConfigHelper::default_moneda_id()])
                        </div>

                        <div class="col-md-3">
                            @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> 'Desde', 'valor'=> '', 'required'=> 'required'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> 'Hasta', 'valor'=> '', 'required'=> 'required'])
                        </div>
                    </div>


                    @include('includes.form_submit', [ 'permiso'=> 'descuentos', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop