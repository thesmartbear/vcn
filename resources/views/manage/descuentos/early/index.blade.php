@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.index') !!} --}}
@stop

@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-gift fa-fw"></i> Descuentos Early Bird
                <span class="pull-right"><a href="{{ route('manage.descuentos.early-bird.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Descuento Early Bird</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Descuento',
                      'importe'         => 'Importe',
                      'moneda'          => 'Moneda',
                      'desde'           => 'Desde',
                      'hasta'           =>  'Hasta',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.descuentos.early-bird.index'))
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [1] ],
                        [ "targets" => [3,4], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop