@extends('layouts.manage')


@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.agencias.ficha', $ficha) !!} --}}
@stop


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-gift fa-fw"></i> Descuento Early Bird :: {{$ficha->name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Status</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panels -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('route' => array('manage.descuentos.early-bird.ficha', $ficha->id))) !!}

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                @include('includes.form_input_text', [ 'campo'=> 'importe', 'texto'=> 'Importe'])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_select', [ 'campo'=> 'moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas ])
                            </div>

                            <div class="col-md-3">
                            @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> 'Desde', 'valor'=> Carbon::parse($ficha->desde)->format('d/m/Y'), 'required'=> 'required'])
                            </div>
                            <div class="col-md-3">
                            @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> 'Hasta', 'valor'=> Carbon::parse($ficha->hasta)->format('d/m/Y'), 'required'=> 'required'])
                            </div>
                        </div>

                        @include('includes.form_submit', [ 'permiso'=> 'descuentos', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                            ['modelo'=> 'DescuentoEarly',
                                'campos_text'=> [ ['name'=> 'Nombre'], ],
                            ])

                    </div>

                </div>

            </div>
        </div>

@stop