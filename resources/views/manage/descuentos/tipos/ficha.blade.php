@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-gift fa-fw"></i> Descuento cliente :: {{$ficha->name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Status</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panels -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('route' => array('manage.descuentos.tipos.ficha', $ficha->id))) !!}

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Descuento'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'select'=> [0=>'Porcentaje',1=>'Importe']])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_input_text', [ 'campo'=> 'valor', 'texto'=> 'Valor'])
                            </div>

                            <div id="div_moneda" class="col-md-2" style="display:none;">
                                @include('includes.form_select', [ 'campo'=> 'moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas ])
                            </div>

                            <div id="div_tipo_pc" class="col-md-3" style="display:none;">
                                @include('includes.form_select', [ 'campo'=> 'tipo_pc', 'texto'=> 'Tipo %', 'select'=> [0=>'Total',1=>'Curso',2=>'Todo menos extras']])
                            </div>

                            <div class="col-md-2">
                                @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activo'])
                            </div>
                        </div>

                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <i class="fa fa-flag fa-tag"></i> Código
                            </div>
                            <div class="panel-body">

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        @include('includes.form_input_text', [ 'campo'=> 'codigo', 'texto'=> 'Código'])
                                    </div>
                                    <div class="col-md-3">
                                        @include('includes.form_select2', [ 'campo'=> 'prescriptor_id', 'valor'=> $ficha->codigo_prescriptor, 'texto'=> 'Prescriptor', 'select'=> $prescriptores])
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-4">
                                        @include('includes.form_select2', [ 'campo'=> 'origen_id', 'valor'=> $ficha->codigo_origen, 'texto'=> 'Origen', 'select'=> $conocidos])
                                    </div>
                                    <div class="col-md-4">
                                        @include('includes.form_select2', [ 'campo'=> 'suborigen_id', 'valor'=> $ficha->codigo_suborigen, 'texto'=> 'Sub-Origen', 'select'=> $conocidosSub])
                                    </div>
                                    <div class="col-md-4">
                                        @include('includes.form_select2', [ 'campo'=> 'suborigendet_id', 'valor'=> $ficha->codigo_suborigendet, 'texto'=> 'Sub-Origen Detalle', 'select'=> $conocidosSubD])
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-book fa-tag"></i> Especialidad
                            </div>
                            <div class="panel-body">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        @include('includes.form_select', [ 'campo'=> 'tipo_multi', 'texto'=> 'Tipo', 'select'=> [0=>'xSemana',1=>'One-time']])
                                    </div>
                                    <div class="col-md-8">

                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <td width='60%'>Especialidad</td>
                                                    <td>Importe</td>
                                                    <td width='15%'></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($ficha->especialidad_id ?? [] as $ie=> $e)
                                                <tr class="tr_clone">
                                                    <td>
                                                        {!! Form::select('especialidad_id[]', $especialidades, $e, ['class'=> 'form-control'])  !!}
                                                    </td>
                                                    <td>
                                                        @include('includes.form_input_text', [ 'campo'=> 'especialidad_importe[]', 'texto'=> null, 'valor'=> $ficha->especialidad_importe[$ie] ?: 0])
                                                    </td>
                                                    <td>
                                                        <input type="button" name="remove" value="-" class="btn btn-danger btn-sm tr_clone_remove">
                                                    </td>
                                                </tr>
                                                @endforeach

                                                <tr class="tr_clone">
                                                    <td>
                                                        {!! Form::select('especialidad_id[]', $especialidades, 0, ['class'=> 'form-control'])  !!}
                                                    </td>
                                                    <td>
                                                        @include('includes.form_input_text', [ 'campo'=> 'especialidad_importe[]', 'texto'=> null, 'valor'=> ''])
                                                    </td>
                                                    <td>
                                                        <input type="button" name="add" value="+" class="btn btn-warning btn-sm tr_clone_add">
                                                        <input type="button" name="remove" value="-" class="btn btn-danger btn-sm tr_clone_remove">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <script>
                                            $("table").on('click', '.tr_clone_add', function() {
                                                var $tr    = $(this).closest('.tr_clone');
                                                var $clone = $tr.clone();
                                                $clone.find(':text').val('');
                                                $clone.find('select').val(0);
                                                $tr.after($clone);
                                            });
                                            $("table").on('click', '.tr_clone_remove', function() {
                                                $(this).closest('.tr_clone').remove();
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @include('includes.form_plataforma', ['campo'=> 'propietario', 'todas'=> true])

                        @include('includes.form_submit', [ 'permiso'=> 'descuentos', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                            ['modelo'=> 'DescuentoTipo',
                                'campos_text'=> [ ['name'=> 'Nombre'], ],
                            ])

                    </div>

                </div>

            </div>
        </div>

@include('includes.script_conocido')

<script type="text/javascript">
function tipo(t)
{
    if(t>0)
    {
        $('#div_tipo_pc').hide();
        $('#div_moneda').show();
    }
    else
    {
        $('#div_tipo_pc').show();
        $('#div_moneda').hide();
    }
}
$(document).ready(function(){

    tipo($('#tipo').val());

    $('#tipo').change( function(e) {

        tipo($(this).val());

    });
});
</script>

@stop