<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-plus-circle fa-fw"></i> Añadir Nuevo
    </div>
    <div class="panel-body">

        {!! Form::open(array('route' => array('manage.descuentos.tipos.crear'))) !!}

            {!! Form::hidden('booking_id', $booking_id) !!}

            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_select', [ 'campo'=> 'dto_tipo', 'texto'=> 'Tipo', 'select'=> [0=>'Porcentaje',1=>'Importe']])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'dto_valor', 'texto'=> 'Valor'])
                </div>

                <div id="div_dto_moneda" class="col-md-2" style="display:none;">
                    @include('includes.form_select', [ 'campo'=> 'dto_moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas,
                        'valor'=> ConfigHelper::default_moneda_id()])
                </div>

                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'dto_notas', 'texto'=> 'Notas'])
                </div>

                <div id="div_dto_tipo_pc" class="col-md-2">
                    @include('includes.form_select', [ 'campo'=> 'dto_tipo_pc', 'texto'=> 'Tipo %', 'select'=> [0=>'Total',1=>'Curso',2=>'Todo menos extras']])
                </div>

                <div class="col-md-2">
                    {!! Form::label('Añadir') !!}<br>
                    {!! Form::submit('Crear', array('class' => 'btn btn-success')) !!}
                </div>

            </div>

        {!! Form::close() !!}
    </div>
</div>

<hr>

<div class="panel panel-default" style="padding-bottom: 240px;">
    <div class="panel-heading">
        <i class="fa fa-plus-circle fa-fw"></i> Importar Descuento Tipo
    </div>
    <div class="panel-body">

        {!! Form::open(array('route' => array('manage.descuentos.tipos.aplicar'))) !!}

            {!! Form::hidden('booking_id', $booking_id) !!}

            <div class="form-group">
                <div class="col-md-10">
                    @include('includes.form_select2', [ 'campo'=> 'descuento_id', 'texto'=> null, 'select'=> $descuentos])
                </div>
                <div class="col-md-2">
                    {!! Form::submit('Importar', array('class' => 'btn btn-success')) !!}
                </div>
            </div>

        {!! Form::close() !!}
    </div>

</div>

@include('includes.script_descuento')