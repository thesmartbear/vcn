@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-gift fa-fw"></i> Descuentos Cliente
                <span class="pull-right"><a href="{{ route('manage.descuentos.tipos.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Descuento Cliente</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'      => 'Nombre',
                      'valor'     => 'Valor',
                      'unidad'    => 'Unidad',
                      'sobre'     => 'Sobre...',
                      'codigo'    => 'Código',
                      'propietario'   => 'Empresa',
                      'activo'    => 'Activo',
                      'options'   => ''
                    ])
                    ->setUrl(route('manage.descuentos.tipos.index'))
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [3] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop