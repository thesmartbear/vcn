<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-users fa-fw"></i> Reuniones

            <div class="pull-right">
                <a href="{{route('manage.reuniones.enviar',[$convocatoria_id,$tipo,1])}}" data-label="Reenviar a Todos" class="btn btn-warning btn-xs"><i class="fa fa-paper-plane fa-fw"></i> Renviar a Todos</a>

                <a href="{{route('manage.reuniones.enviar',[$convocatoria_id,$tipo,0])}}" data-label="Enviar" class="btn btn-success btn-xs"><i class="fa fa-envelope fa-fw"></i> Enviar</a>
            </div>

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'oficina' => 'Oficina',
                  'fecha'   => 'Fecha',
                  'hora'    => 'Hora',
                  'lugar'   => 'Lugar',
                  'notas'   => 'Notas',
                  'options' => ''
                ])
                ->setUrl(route('manage.reuniones.index.convocatoria', [$convocatoria_id,$tipo]))
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [4] ],
                    [ "targets" => [1], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
                ->render() !!}
        </div>
    </div>

</div>
