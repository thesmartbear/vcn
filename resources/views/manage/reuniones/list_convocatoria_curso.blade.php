<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-users fa-fw"></i> Reuniones Curso
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>

    </div>

    <div class="portlet-body flip-scroll">

        {!! Datatable::table()
            ->addColumn([
              'oficina' => 'Oficina',
              'fecha'   => 'Fecha',
              'hora'    => 'Hora',
              'lugar'   => 'Lugar',
              'notas'   => 'Notas',
              'options' => ''
            ])
            ->setUrl(route('manage.reuniones.index.curso', $curso_id))
            ->setOptions('iDisplayLength', 100)
            ->setOptions(
              "aoColumnDefs", array(
                [ "bSortable" => false, "aTargets" => [4] ],
                [ "targets" => [1], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
              )
            )
            ->render() !!}
    </div>
</div>