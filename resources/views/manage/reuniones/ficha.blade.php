@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Reunion :: {{$ficha->lugar}} : Oficina {{$ficha->oficina->name}}
            </div>
            <div class="panel-body">

                @if($ficha->curso_id)
                    {!! Form::open(array('method'=> 'post', 'url' => route('manage.reuniones.ficha.curso', $ficha->id), 'class'=> 'form' )) !!}
                @else
                    {!! Form::open(array('method'=> 'post', 'url' => route('manage.reuniones.ficha.convocatoria', $ficha->id))) !!}
                @endif

                <div class="form-group row">
                    <div class="col-md-3">
                        @include('includes.form_input_datetime', [ 'campo'=> 'fecha', 'texto'=> 'Fecha'])
                    </div>

                    <div class="col-md-2">
                        @include('includes.form_input_datetime_hora', [ 'campo'=> 'hora', 'texto'=> 'Hora'])
                    </div>

                    <div class="col-md-7">
                        @include('includes.form_input_text', [ 'campo'=> 'lugar', 'texto'=> 'Lugar'])
                    </div>
                </div>

                <div class="form-group">
                    @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Nota previa'])
                </div>

                <div class="form-group pull-right">
                    @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Guardar'])
                </div>


                {!! Form::close() !!}

            </div>
        </div>

        <hr>

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-list fa-fw"></i> Logs
            </div>
            <div class="panel-body">

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td>Fecha</td>
                            <td>Bookings</td>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($ficha->logs as $log)

                            <tr>
                                <td>{{$log->created_at->format('d/m/Y')}}</td>
                                <td>
                                    @foreach($log->bookings as $b)
                                        <a href="{{route('manage.bookings.ficha',$b)}}">{{$b}}</a>, 
                                    @endforeach
                                </td>
                            </tr>

                        @endforeach

                    </tbody>

                </table>

            </div>

        </div>

@stop