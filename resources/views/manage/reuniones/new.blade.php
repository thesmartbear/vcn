<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-users fa-fw"></i> Añadir Reunión
        </div>
        <div class="panel-body">

            @if(isset($curso_id))
                {!! Form::open(array('method'=> 'post', 'url' => route('manage.reuniones.ficha.curso', [0]), 'class'=> 'form' )) !!}
                    {!! Form::hidden('curso_id',$curso_id) !!}
            @else
                {!! Form::open(array('method'=> 'post', 'url' => route('manage.reuniones.ficha.convocatoria', [0]))) !!}
                    {!! Form::hidden('convocatoria_tipo',$tipo) !!}
                    {!! Form::hidden('convocatoria_id',$convocatoria_id) !!}
            @endif

            <div class="form-group">
                @include('includes.form_select_multi', [ 'campo'=> 'oficinas', 'texto'=> 'Oficinas', 'select'=> \VCN\Models\System\Oficina::plataforma()->pluck('name','id'), 'valor'=> 0] )
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    @include('includes.form_input_datetime', [ 'campo'=> 'fecha', 'texto'=> 'Fecha', 'valor'=>''])
                </div>

                <div class="col-md-2">
                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'hora', 'texto'=> 'Hora', 'valor'=>''])
                </div>

                <div class="col-md-7">
                    @include('includes.form_input_text', [ 'campo'=> 'lugar', 'texto'=> 'Lugar', 'valor'=>''])
                </div>
            </div>

            <div class="form-group">
                @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Nota previa', 'valor'=>''])
            </div>

            <div class="form-group pull-right">
                {!! Form::submit('Añadir',array('class'=> 'btn btn-success')) !!}
            </div>


            {!! Form::close() !!}

        </div>
    </div>

</div>
