    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-history"></i> Historial
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'fecha'    => 'Fecha',
                  'tipo'     => 'Tipo',
                  'usuario'  => 'Actualizado',
                  'notas'    => 'Notas',
                  'options' => ''

                ])
                ->setUrl( route('manage.tutores.logs.index', [$tutor_id, $todos]) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [4] ]
                  )
                )
                ->render() !!}

        </div>
    </div>