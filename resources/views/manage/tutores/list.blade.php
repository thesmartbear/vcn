
<div class="portlet light">

    <div class="portlet-body">

        {!! Datatable::table()
            ->setId('DttTutores')
            ->addColumn([
              'name'        => 'Nombre',
              'lastname'    => 'Apellidos',
              'email'       => 'Email',
              'viajeros'    => 'Leads/Relación',
              'phone'       => 'Teléfono',
              'movil'       => 'Movil',
              'empresa'       => 'Empresa',
              'options' => ''

            ])
            //->addColumn('Nombre','Email','Viajeros','')
            ->setUrl( route('manage.tutores.index', $viajero_id) )
            ->setOptions('iDisplayLength',100)
            ->setOptions(
              "aoColumnDefs", array(
                [ "bSortable" => false, "aTargets" => [6] ]
              )
            )
            ->render() !!}

    </div>
</div>