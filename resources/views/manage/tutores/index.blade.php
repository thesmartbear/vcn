@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.tutores.index') !!}
@stop

@section('titulo')
    <i class="fa fa-shield"></i> Tutores
@stop

@section('container')
    @include('manage.tutores.list', ['viajero_id'=> $viajero_id])
@stop