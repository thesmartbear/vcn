@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-truck fa-fw"></i> Tutor :: {{$ficha->full_name}}

        @if($ficha->user && ConfigHelper::canEdit('superlogin'))
            <a class='pull-right' href='{{ route('manage.system.admins.login', [$ficha->user_id, 'url'=> Request::url()]) }}'><i class='fa fa-sign-in'></i></a>
        @endif
        
    </div>
    <div class="panel-body">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Tutor</a></li>

            @if(!$viajero_id)
                <li role="presentation"><a href="#viajeros" aria-controls="viajeros" role="tab" data-toggle="tab">Viajeros</a></li>
            @endif

            @if($ficha->usuario)
                <li role="presentation"><a data-label="Visitas" href="#visitas" aria-controls="visitas" role="tab" data-toggle="tab"><i class="fa fa-globe"></i></a></li>
            @endif

            <li role="presentation"><a data-label="Historial" href="#historial" aria-controls="historial" role="tab" data-toggle="tab"><i class="fa fa-history"></i></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                {!! Form::model($ficha, array( 'id'=> 'frmValidar', 'route' => array('manage.tutores.ficha', $ficha->id))) !!}

                {!! Form::hidden('viajero_id',$viajero_id) !!}

                <div class="form-group">
                    @if(ConfigHelper::config('es_rgpd'))
                        @include('includes.form_checkbox', [ 'campo'=> 'lopd_check2', 'texto'=> trans('area.booking.lopd_check2', ['plataforma'=> ConfigHelper::plataformaApp()]), 'html'=> true ])
                    @else
                        @include('includes.form_checkbox', [ 'campo'=> 'optout', 'texto'=> trans('area.optout'), 'html'=> true ])
                    @endif

                    @if($ficha->optout && $ficha->optout_fecha)
                        (Desde el {{$ficha->optout_fecha->format('d/m/Y')}})
                    @endif
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'lastname', 'texto'=> 'Apellidos'])
                </div>

                @if(ConfigHelper::config('nif'))
                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_select', [ 'campo'=> 'tipodoc', 'texto'=> "Tipo", 'select'=> ConfigHelper::getTipoDoc() ])
                    </div>
                    <div class="col-md-8">
                        @include('includes.form_input_text', [ 'campo'=> 'nif', 'valor'=> $ficha->nif, 'texto'=> 'DNI'])
                    </div>
                </div>
                @endif

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'E-mail'])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'phone', 'texto'=> 'Teléfono'])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'movil', 'texto'=> 'Movil'])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'profesion', 'texto'=> 'Profesión'])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'empresa', 'texto'=> 'Empresa'])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'empresa_phone', 'texto'=> 'Tlf. Empresa'])
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        @include('includes.form_input_text', [ 'campo'=> 'emergencia_contacto', 'texto'=> 'Contacto Emergencia'])
                    </div>
                    <div class="col-md-6">
                        @include('includes.form_input_text', [ 'campo'=> 'emergencia_telefono', 'texto'=> 'Teléfono Emergencia'])
                    </div>
                </div>

                @if($viajero_id)
                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'relacion', 'texto'=> 'Relación', 'valor'=> 0, 'select'=> $relaciones])
                    </div>
                @endif

                <div class="form-group pull-right">
                    {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                    <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                </div>

                {!! Form::close() !!}

                @if($ficha->usuario)
                [Username: {{$ficha->usuario->username}}]
                (Último login: {{$ficha->usuario->login_attempts>0?Carbon::parse($ficha->usuario->login_last)->format('d/m/Y H:i:s'):""}} de {{$ficha->usuario->login_attempts}})
                @endif

            </div>

            @if(!$viajero_id)
                <div role="tabpanel" class="tab-pane fade" id="viajeros">
                    @include('manage.viajeros.list_tutor', ['tutor_id'=> $ficha->id])
                </div>
            @endif

            @if($ficha->usuario)
            <div role="tabpanel" class="tab-pane fade" id="visitas">

                @if($ficha->usuario->visitas->count())

                    {!! Datatable::table()
                        ->addColumn([
                          'fecha'   => 'Fecha',
                          'curso'   => 'Curso',
                          'visitas' => 'Visitas',
                        ])
                        ->setUrl(route('manage.viajeros.trafico', $ficha->id))
                        ->setOptions('iDisplayLength', 100)
                        ->setOptions(
                          "columnDefs", array(
                            // [ "sortable" => false, "targets" => [10] ],
                            [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                          )
                        )
                        ->render()
                    !!}

                @else
                    - No hay visitas contabilizadas -
                @endif

            </div>
            @endif

            <div role="tabpanel" class="tab-pane fade" id="historial">
                @include('manage.tutores.logs.list', ['tutor_id'=> $ficha->id, 'todos'=> true])
            </div>

        </div>


    </div>
</div>

<script type="text/javascript">
$(function() {

    //rules nif/nie
    function tipodoc_nif()
    {
        return $('#tipodoc').val()==0;
    }

    function tipodoc_nie()
    {
        return $('#tipodoc').val()==1;
    }

    $("#tipodoc").change( function(){

        $("#nif").rules( "remove" );
        $("#nif").rules("add",{
            nifES: tipodoc_nif(),
            nieES: tipodoc_nie(),
        });

    });

    $("#frmValidar").validate({
        rules:
        {
            nif: {
                nifES: tipodoc_nif(),
                nieES: tipodoc_nie(),
            },
        }
    });
});
</script>

@stop