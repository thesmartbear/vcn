@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Nuevo Tutor :: Viajero {{$viajero_name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#nuevo" aria-controls="nuevo" role="tab" data-toggle="tab">Nuevo</a></li>

                    <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab">Tutor existente</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="nuevo">

                        {!! Form::open(array('method' => 'POST', 'url' => route('manage.tutores.ficha',0), 'role' => 'form', 'class' => '', 'id'=> 'frmValidar')) !!}

                            {!! Form::hidden('viajero_id',$viajero_id) !!}

                            {{-- <div class="form-group">
                                @include('includes.form_select', [ 'campo'=> 'viajero_id', 'texto'=> 'Viajero', 'valor'=> $viajero_id, 'select'=> $viajeros])
                            </div> --}}

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'lastname', 'texto'=> 'Apellidos'])
                            </div>

                            @if(ConfigHelper::config('nif'))
                            <div class="form-group row">
                                <div class="col-md-4">
                                    @include('includes.form_select', [ 'campo'=> 'tipodoc', 'texto'=> "Tipo", 'select'=> ConfigHelper::getTipoDoc() ])
                                </div>
                                <div class="col-md-8">
                                    @include('includes.form_input_text', [ 'campo'=> 'nif', 'texto'=> 'DNI'])
                                </div>
                            </div>
                            @endif

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'E-mail'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'phone', 'texto'=> 'Teléfono'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'movil', 'texto'=> 'Movil'])
                            </div>

                            {{--
                            <div class="form-group row">
                                <div class="col-md-6">
                                    @include('includes.form_input_text', [ 'campo'=> 'emergencia_contacto', 'texto'=> 'Contacto Emergencia'])
                                </div>
                                <div class="col-md-6">
                                    @include('includes.form_input_text', [ 'campo'=> 'emergencia_telefono', 'texto'=> 'Teléfono Emergencia'])
                                </div>
                            </div>
                            --}}

                            <div class="form-group">
                                @include('includes.form_select', [ 'campo'=> 'relacion', 'texto'=> 'Relación', 'valor'=> 0, 'select'=> $relaciones])
                            </div>

                            <div class="form-group pull-right">
                                {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                                <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                            </div>

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="add">
                        {!! Form::open(array('method' => 'POST', 'url' => route('manage.tutores.ficha.asignar',$viajero_id), 'role' => 'form', 'class' => '')) !!}

                            <div class="form-group">
                                {!! Form::label('tutor_id', 'Elegir Tutor') !!}:
                                {!! Form::select('tutor_id', $tutores, 0, array( 'id'=> 'tutor_id', 'class' => 'form-control chosen-select-tab')) !!}
                                <span class="help-block">{{ $errors->first('tutor_id') }}</span>
                            </div>

                            <div class="form-group">
                                @include('includes.form_select', [ 'campo'=> 'relacion', 'texto'=> 'Relación', 'valor'=> 0, 'select'=> $relaciones])
                            </div>

                            <div class="form-group pull-right">
                                {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                                <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                            </div>

                        {!! Form::close() !!}
                    </div>

                </div>

            </div>
        </div>

@if($viajero_id)
<script>
$(function() {
    $("#viajero_id").attr('disabled',true);
});
</script>

<script type="text/javascript">
$(function() {

    //rules nif/nie
    function tipodoc_nif()
    {
        return $('#tipodoc').val()==0;
    }

    function tipodoc_nie()
    {
        return $('#tipodoc').val()==1;
    }

    $("#tipodoc").change( function(){

        $("#nif").rules( "remove" );
        $("#nif").rules("add",{
            nifES: tipodoc_nif(),
            nieES: tipodoc_nie(),
        });

    });

    $("#frmValidar").validate({
        rules:
        {
            nif: {
                nifES: tipodoc_nif(),
                nieES: tipodoc_nie(),
            },
        }
    });
});
</script>
@endif

@stop