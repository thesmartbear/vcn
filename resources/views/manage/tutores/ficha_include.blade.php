<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-shield fa-fw"></i> Tutor :: {{$ficha->name}}
            ({{ConfigHelper::getTutorRelacion($ficha->pivot->relacion)}})

        <span class="pull-right">
            <a href="{{ route('manage.tutores.ficha', $ficha->id) }}" target="_blank" class="btn btn-warning btn-xs"><i class="fa fa-plus-circle"></i> Editar</a>

            <a class="btn btn-danger btn-xs" href='#destroy'
                data-label='Borrar Relación'
                data-model='Relación'
                data-action="{{route( 'manage.tutores.delete.relacion', $ficha->pivot->id)}}"
                data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i> Eliminar</a>
        </span>

    </div>
    <div class="panel-body tutor-edit">

        @if($ficha->usuario)
        [Username: {{$ficha->usuario->username}}]
        <a target='_blank' href='{{ route('manage.tutores.area', $ficha->id)}}' class='btn btn-success btn-xs'><i class='fa fa-sign-in'></i> Area</a>
        <br>
        (Último login: {{$ficha->usuario->login_attempts>0?Carbon::parse($ficha->usuario->login_last)->format('d/m/Y H:i:s'):""}} de {{$ficha->usuario->login_attempts}})

            @if($ficha->user && ConfigHelper::canEdit('superlogin'))
                <a class='pull-right' href='{{ route('manage.system.admins.login', [$ficha->user_id, 'url'=> Request::url()]) }}'><i class='fa fa-sign-in'></i></a>
            @endif

        @endif

        <div class="form-group">
            @include('includes.form_input_text', [ 'campo'=> 'tutor_name', 'valor'=> $ficha->name, 'texto'=> 'Nombre'])
        </div>

        <div class="form-group">
            @include('includes.form_input_text', [ 'campo'=> 'tutor_lastname', 'valor'=> $ficha->lastname, 'texto'=> 'Apellidos'])
        </div>

        @if(ConfigHelper::config('nif'))
        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_select', [ 'campo'=> 'tutor_tipodoc', 'texto'=> "Tipo", 'select'=> ConfigHelper::getTipoDoc() ])
            </div>
            <div class="col-md-8">
                @include('includes.form_input_text', [ 'campo'=> 'tutor_nif', 'valor'=> $ficha->nif, 'texto'=> 'DNI'])
            </div>
        </div>
        @endif

        <div class="form-group">
            @include('includes.form_input_text', [ 'campo'=> 'tutor_email', 'valor'=> $ficha->email, 'texto'=> 'E-mail'])
        </div>

        <div class="form-group">
            @include('includes.form_input_text', [ 'campo'=> 'tutor_phone', 'valor'=> $ficha->phone, 'texto'=> 'Teléfono'])
        </div>

        <div class="form-group">
            @include('includes.form_input_text', [ 'campo'=> 'tutor_movil', 'valor'=> $ficha->movil, 'texto'=> 'Movil'])
        </div>

        <div class="form-group">
            @include('includes.form_input_text', [ 'campo'=> 'tutor_profesion', 'valor'=> $ficha->profesion, 'texto'=> 'Profesión'])
        </div>

        <div class="form-group">
            @include('includes.form_input_text', [ 'campo'=> 'tutor_empresa', 'valor'=> $ficha->empresa, 'texto'=> 'Empresa'])
        </div>

        <div class="form-group">
            @include('includes.form_input_text', [ 'campo'=> 'tutor_empresa_phone', 'valor'=> $ficha->empresa_phone, 'texto'=> 'Tlf. Empresa'])
        </div>


    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.tutor-edit input').attr('readonly', 'readonly');
    $('.tutor-edit select').attr("disabled", true);

    //rules nif/nie
    function tipodoc_nif()
    {
        return $('#tutor_tipodoc').val()==0;
    }

    function tipodoc_nie()
    {
        return $('#tutor_tipodoc').val()==1;
    }

    $("#tutor_tipodoc").change( function(){

        $("#tutor_nif").rules( "remove" );
        $("#tutor_nif").rules("add",{
            nifES: tipodoc_nif(),
            nieES: tipodoc_nie(),
        });

    });

    $("#frmValidar").validate({
        rules:
        {
            tutor_nif: {
                nifES: tipodoc_nif(),
                nieES: tipodoc_nie(),
            },
        }
    });
});
</script>