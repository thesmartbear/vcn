<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Booking {{$ficha->id}}</title>

        {!! Html::style('assets/css/pdf.css') !!}
        {!! Html::style('assets/css/bootstrap.css') !!}
    
        <?php
            $p = $ficha->plataforma ?: 1;
            $sufijo = ConfigHelper::config('sufijo', $p);
            $web = ConfigHelper::config('web',$p);
        ?>

        <style>
            caption{
                @if($sufijo == 'bs')
                    color: #f1c40f;
                @elseif($sufijo == 'cic')
                    color: #3B6990;
                @elseif($sufijo == 'sf')
                    color: #24aab6;
                @else
                    color: #000000;
                @endif
            }
            h1{
                @if($sufijo == 'bs')
                    border-bottom: 1px solid #f1c40f;
                @elseif($sufijo == 'cic')
                    border-bottom: 1px solid #3B6990;
                @elseif($sufijo == 'sf')
                    border-bottom: 1px solid #24aab6;
                @else
                   border-bottom: 1px solid #CCCCCC;
                @endif
            }
            table.total thead td {
                @if($sufijo == 'bs')
                    color: #f1c40f;
                @elseif($sufijo == 'cic')
                    color: #3B6990;
                @elseif($sufijo == 'sf')
                    color: #24aab6;
                @else
                    color: #000000;
                @endif
            }
        </style>

    </head>
    <body>
        <div class="row">
            <div class="col-xs-12"><img style="width: 4.5cm; height: auto; margin-top: 4px;" class="pull-right" src="https://{{$web}}/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb', $p)}}" /></div>
        </div>

        <div class="row" style="margin-top: 1cm;">
            <div class="col-xs-12"><h1>Booking {{$ficha->id}}</h1></div>
        </div>

        <?php $datos = $ficha->datos; ?>
        <?php $viajero = $ficha->viajero; ?>
        <?php $datosv = $viajero->datos; ?>

        <div class="row">
            <div class="col-xs-12">

                <table class="table datosviajero">
                    <tbody>
                        <tr>
                            <td><img height='160px' src="{{$viajero->avatar ?: $viajero->foto}}" class="form-foto-mini"></td>
                            <td>{{$datos->full_name}}</td>
                        </tr>
                    </tbody>
                </table>    

            </div>
        </div>

        <br><br><br>

        <div class="row">
            <div class="col-xs-12">

                <table class="table nombrecurso">
                    <tr>
                        <td>Nombre del Curso</td>
                        <td>{{ $ficha->curso->es_convocatoria_multi?$ficha->convocatoria->name:$ficha->curso->name}}</td>
                    </tr>
                    <tr>
                        <td>Centro</td>
                        <td>{{$ficha->centro->name}} ({{$ficha->centro->ciudad->city_name}}. {{$ficha->centro->pais->name}})</td>
                    </tr>
                </table>

                <table class="table table-condensed">
                    <caption>Convocatoria</caption>
                    <tbody>
                        <tr>
                            <td width="60%">
                                Del {{$ficha->curso_start_date}} al {{$ficha->curso_end_date}}
                                <span id="booking-cabierta-alerta" class='booking-alerta'>{{isset($ficha->alertas['cabierta'])?$ficha->alertas['cabierta']:""}}</span>
                            </td>
                            <td width="25%" align='center'>{{$ficha->weeks}} {{$ficha->convocatoria->getDuracionName($ficha->weeks)}}</td>
                        </tr>

                        {{-- DESCUENTOS --}}
                        <?php
                        $descuentos = $ficha->getDescuentos();
                        $descuento = null;
                        if($descuentos->count()>0)
                        {
                            $descuento = $descuentos->first();
                        }
                        ?>

                        @if($ficha->curso->es_convocatoria_cerrada)
                            <tr>
                                <td colspan="3">
                                    @if($ficha->vuelo)
                                        {{$ficha->vuelo->name}}
                                    @else
                                        @if($ficha->transporte_no)
                                            NO necesito transporte
                                        @elseif($ficha->transporte_otro)
                                            Otro transporte: {{$ficha->transporte_detalles}}
                                        @else
                                            -
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endif

                    </tbody>
                </table>

                @if($ficha->curso->es_convocatoria_multi)
                {{-- Especialidades --}}
                <table class='table'>
                    <caption>Especialidades</caption>
                    <tbody id='booking-multi-especialidades-table'>
                        @foreach($ficha->multis as $esp)
                        <tr>
                            <td class='col-md-1'>Semana {{$esp->n}} [{{$esp->semana->semana}}]</td>
                            <td>{{$esp->especialidad?$esp->especialidad->name:'-'}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif

                @if($ficha->alojamiento)
                <table class="table">
                    <caption>Alojamiento</caption>
                    <tbody>
                    <tr>
                        <td width="60%"><b>{{$ficha->alojamiento?$ficha->alojamiento->name:"-"}}</b> ::
                            Del {{Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y')}}
                            al {{Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y')}}
                            <span id="booking-alojamiento-alerta" class='booking-alerta'>{{isset($ficha->alertas['alojamiento'])?$ficha->alertas['alojamiento']:""}}</span>
                        </td>
                        <td width="25%" align='center'>
                            <?php $u = $ficha->accommodation_weeks; ?>
                            {{$ficha->accommodation_weeks}} {{$ficha->alojamiento?$ficha->alojamiento->getDuracionName($u):$ficha->convocatoria->getDuracionName($u)}}
                        </td>
                    </tr>
                    </tbody>
                </table>
                @endif

                <table class="table datosviajero">
                    <caption>Datos</caption>
                    <tbody>
                        <tr>
                            <td>DNI</td>
                            <td>{{$datos->documento}}</td>
                        </tr>

                        <tr>
                            <td>F.Nacimiento</td>
                            <td>{{Carbon::parse($datos->fechanac)->format('d/m/Y')}}</td>
                        </tr>

                        <tr>
                            <td>Sexo</td>
                            <td>{{$ficha->datos->sexo==1?'Hombre':'Mujer'}}</td>
                        </tr>

                        <tr>
                            <td>Nacionalidad</td>
                            <td>{{$ficha->datos->nacionalidad}}</td>
                        </tr>

                        <tr>
                            <td>Teléfono</td>
                            <td>{{$ficha->datos->phone}}</td>
                        </tr>

                        <tr>
                            <td>Móvil</td>
                            <td>{{$ficha->datos->movil}}</td>
                        </tr>

                        <tr>
                            <td>E-mail</td>
                            <td>{{$ficha->datos->email}}</td>
                        </tr>

                        @if(!$ficha->curso->es_menor)

                            <tr class="separador">
                                <td>Fumador</td>
                                <td>{{$ficha->datos->fumador?'SI':'NO'}}</td>
                            </tr>

                            <tr>
                                <td>Profesión</td>
                                <td>{{$ficha->datos->profesion}}</td>
                            </tr>

                            <tr>
                                <td>Empresa</td>
                                <td>{{$ficha->datos->empresa}}</td>
                            </tr>

                        @endif

                        <tr class="separador">
                            <td>Pasaporte</td>
                            <td>
                                {{$ficha->datos->pasaporte}} ({{$ficha->datos->pasaporte_pais}})
                                Del {{$ficha->datos->pasaporte_emision!='0000-00-00'?Carbon::parse($ficha->datos->pasaporte_emision)->format('d/m/Y'):""}} al {{$ficha->datos->pasaporte_caduca!='0000-00-00'?Carbon::parse($ficha->datos->pasaporte_caduca)->format('d/m/Y'):""}}
                            </td>
                        </tr>

                        <tr>
                            <td>Dirección</td>
                            <td>{{$ficha->datos->tipovia_name}} {{$ficha->datos->direccion}}, {{$ficha->datos->ciudad}} {{$ficha->datos->cp}} ({{$ficha->datos->provincia}}) [{{$ficha->datos->pais}}]</td>
                        </tr>
                    </tbody>
                </table>

                <table class="table datosviajero">
                    <caption>Datos académicos</caption>

                    <tbody>
                        <tr>
                            <td>Idioma</td>
                            <td>{{$datos->idioma}}</td>
                        </tr>
                        <tr>
                            <td>Nivel Idioma</td>
                            <td>{{$datos->idioma_nivel}}</td>
                        </tr>
                        <tr>
                            <td>Titulación/Estudios</td>
                            <td>{{$datos->titulacion}}</td>
                        </tr>
                        <tr>
                            <td>Escuela</td>
                            <td>{{$datos->escuela}}</td>
                        </tr>
                        <tr>
                            <td>Curso académico actual</td>
                            <td>{{ConfigHelper::getEscuelaCurso($datos->escuela_curso)}}</td>
                        </tr>
                        <tr>
                            <td>¿Estudias inglés en alguna academia?</td>
                            <td>{{($ficha->datos->ingles_academia=="")?"NO":$ficha->datos->ingles_academia}}</td>
                        </tr>

                        <tr>
                            <td>¿Has realizado algún curso de idiomas en el extranjero con anterioridad?</td>
                            <td>{{($ficha->datos->curso_anterior=="")?"NO":$ficha->datos->curso_anterior}}</td>
                        </tr>

                        <tr>
                            <td>¿Tienes algún título oficial?</td>
                            <td>{{($ficha->datos->titulacion=="")?"NO":$ficha->datos->titulacion}}</td>
                        </tr>

                        <tr>
                            <td>¿Has hecho el Trinity Exam?</td>
                            <td>
                                @if($datos->trinity_exam)
                                    Año: {{$datos->trinity_any}} - {{ConfigHelper::getTrinityNivel($datos->trinity_nivel)}}
                                @else
                                    NO
                                @endif
                            </td>
                        </tr>

                        @if($ficha->es_cic)
                        <tr>
                            <td>¿Estudia en un centro de idiomas del CIC?</td>
                            <td>{{($datos->cic=="")?"NO":$datos->cic_nivel}}</td>
                        </tr>
                        <tr>
                            <td>¿Vas a la escuela Thau Barcelona o Sant Cugat?</td>
                            <td>{{ConfigHelper::getCicThau($datos->cic_thau)}}</td>
                        </tr>
                        @endif

                        <tr>
                            <td>Aficiones especiales, hobbies y deportes</td>
                            <td>{{$ficha->datos->hobby}}</td>
                        </tr>

                        <tr>
                            <td>Observaciones (viajero)</td>
                            <td>{{$ficha->datos->notas}}</td>
                        </tr>
                    </tbody>
                </table>

                <table class="table table-responsive tutores">
                    <caption>TUTOR/ES</caption>
                    <thead>
                        <tr>
                            <th>Relación</th>
                            <th>Nombre</th>
                            <th>E-mail</th>
                            <th>Teléfono</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ficha->datos_congelados?$ficha->datos->tutores:$ficha->viajero->tutores as $tutor)
                        <tr>
                            <td>{{ConfigHelper::getTutorRelacion($ficha->datos_congelados?$tutor['pivot']['relacion']:$tutor->pivot->relacion)}}</td>
                            <td>{{$ficha->datos_congelados?($tutor['name']." ".$tutor['lastname']):$tutor->full_name}}</td>
                            <td>{{$ficha->datos_congelados?$tutor['email']:$tutor->email}}</td>
                            <td>{{$ficha->datos_congelados?$tutor['phone']:$tutor->phone}} {{$ficha->datos_congelados?$tutor['movil']:$tutor->movil?" / ".$ficha->datos_congelados?$tutor['movil']:$tutor->movil:''}}</td>
                            <td>
                                {{$ficha->datos_congelados?$tutor['phone']:$tutor->phone}}&nbsp;
                                {{ $ficha->datos_congelados ? $tutor['movil'] : ($tutor->movil ? (" / ".$ficha->datos_congelados ? $tutor['movil'] : $tutor->movil) : '') }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <?php
                    $bscd32 = false;
                    if( $ficha->curso->subcategoria_detalle )
                    {
                        if($ficha->curso->subcategoria_detalle->id == 32)
                        {
                            $bscd32 = true;
                        }
                    }
                ?>

                {{-- DATOS MEDICOS --}}
                <table class="table datosmedicos">
                    <caption>@lang('area.datosmedicos')</caption>
                    <tbody>
                        
                        <tr>
                            <td>@lang('area.alergia')</td>
                            <td>{{($ficha->datos->alergias=="")?"NO":$ficha->datos->alergias}}</td>
                        </tr>
                        <tr>
                            <td>@lang('area.enfermedad')</td>
                            <td>{{($ficha->datos->enfermedad=="")?"NO":$ficha->datos->enfermedad}}</td>
                        </tr>
                        <tr>
                            <td>@lang('area.medicacion')</td>
                            <td>{{($ficha->datos->medicacion=="")?"NO":$ficha->datos->medicacion}}</td>
                        </tr>
                        <tr>
                            <td>@lang('area.tratamiento')</td>
                            <td>{{($ficha->datos->tratamiento=="")?"NO":$ficha->datos->tratamiento}}</td>
                        </tr>
                        <tr>
                            <td>@lang('area.dieta')</td>
                            <td>{{($ficha->datos->dieta=="")?"NO":$ficha->datos->dieta}}</td>
                        </tr>
                        <tr>
                            <td>@lang('area.animales')</td>
                            <td>{{($ficha->datos->animales=="")?"NO":$ficha->datos->animales}}</td>
                        </tr>

                    </tbody>
                </table>

                {{-- DATOS CAMPAMENTO --}}
                <?php
                    $datos_campamento = ['campamento_nadar','campamento_enuresis','campamento_comer','campamento_bici',
                        'campamento_mareos','campamento_insomnio','campamento_cansado',
                        //'campamento_enfermedad','campamento_alergia','campamento_alergia_1','campamento_alergia_2','campamento_alergia_3','campamento_medicacion',
                        'campamento_enfermizo','campamento_operaciones','campamento_discapacidad',
                        'campamento_aprendizaje','campamento_familiar','campamento_miedo'
                    ];
                ?>
                @if($ficha->curso->categoria && $ficha->curso->categoria->es_info_campamento)
                    <table class="table datoscampamento">
                        <caption>@lang('area.datoscampamento')</caption>
                        <tbody>
                            @foreach($datos_campamento as $campo)
                                <?php
                                    $nom = substr($campo, 11);
                                    $nom = trans("area.campamento.".$nom);
                                ?>

                                <tr>
                                    <td>{{$nom}}</td>
                                    <td>
                                        
                                        <?php
                                            $valor = "NO";
                                            if(isset($ficha->datos_campamento[$campo]))
                                            {
                                                $valor = $ficha->datos_campamento[$campo];
                                                if( $valor === 1)
                                                {
                                                    $valor = "SI";
                                                }
                                                elseif( $valor === 0)
                                                {
                                                    $valor = "NO";
                                                }
                                                elseif( $valor === "")
                                                {
                                                    $valor = "NO";
                                                }
                                            }
                                        ?>

                                        {{$valor}}

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif

            </div>
        </div>
    </body>
</html>