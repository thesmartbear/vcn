@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tag fa-fw"></i> Nuevo Status Booking
            </div>
            <div class="panel-body">


                {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.status.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_input_number', [ 'campo'=> 'orden', 'texto'=> 'Orden', 'required'=>true])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Status'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_checkbox', [ 'campo'=> 'manual', 'texto'=> 'Permite cambio manual'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_checkbox', [ 'campo'=> 'plazas', 'texto'=> 'Cuenta para plazas'])
                        </div>
                    </div>

                    <div class="form-group pull-right">
                        {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                        <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                    </div>

                {!! Form::close() !!}

            </div>
        </div>

@stop