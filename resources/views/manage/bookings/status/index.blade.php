@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tag fa-fw"></i> Status Booking
                <span class="pull-right"><a href="{{ route('manage.bookings.status.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Status Booking</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'orden'      => 'Orden',
                      'name'    => 'Status',
                      'manual'    => 'Manual',
                      'plazas'    => 'Plazas',
                      'options' => ''
                    ])
                    ->setUrl(route('manage.bookings.status.index'))
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [4] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop