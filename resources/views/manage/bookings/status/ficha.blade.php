@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-group fa-fw"></i> Status Booking :: {{$ficha->name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Status</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panels -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('route' => array('manage.bookings.status.ficha', $ficha->id))) !!}

                        <div class="form-group">
                            @include('includes.form_input_number', [ 'campo'=> 'orden', 'texto'=> 'Orden', 'required'=>true])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Status'])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                @include('includes.form_checkbox', [ 'campo'=> 'manual', 'texto'=> 'Permite cambio manual'])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_checkbox', [ 'campo'=> 'plazas', 'texto'=> 'Cuenta para plazas'])
                            </div>
                        </div>

                        <div class="form-group pull-right">
                            {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                            <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                        </div>

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'VCN\Models\Bookings\Status',
                                'campos_text'=> [ ['name'=> 'Status'], ],
                                'campos_textarea'=> []
                            ])

                    </div>

                </div>

            </div>
        </div>

@stop