@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-money fa-fw"></i> Moneda :: {{$ficha->currency_name}}
            </div>
            <div class="panel-body">


                {!! Form::open(array('method' => 'POST', 'url' => route('manage.monedas.ficha',$ficha->id), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'currency_name', 'texto'=> 'Moneda'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'currency_rate', 'texto'=> 'Tasa'])
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'monedas', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop