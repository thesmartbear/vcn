<div class="col-md-12"><br>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-money fa-fw"></i> Tipo de Cambio
        </div>
        <div class="panel-body">

            <table class="table table-bordered table-striped table-condensed flip-content">
                            <caption>Monedas</caption>
                            <thead>
                                <tr>
                                    <th>Moneda</th>
                                    <th>Tasa</th>
                                    <th><i>Actualizar</i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($ficha->monedas as $moneda)
                                    {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.monedas',$booking_id), 'role' => 'form', 'class' => 'form-inline')) !!}
                                    <tr>
                                        <td>{{$moneda->moneda->name}}</td>
                                        <td class="col-md-1">
                                            @include('includes.form_input_text', [ 'campo'=> 'rate', 'texto'=> null, 'valor'=> $moneda->rate])
                                        </td>
                                        <td>
                                            {!! Form::hidden('moneda_id', $moneda->id) !!}
                                            {!! Form::submit('Actualizar', ['class'=> 'btn btn-success']) !!}
                                        </td>
                                    </tr>
                                    {!! Form::close() !!}
                                @endforeach
                            </tbody>
                        </table>

        </div>
    </div>
</div>