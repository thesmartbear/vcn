<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-cog fa-fw"></i> Asigaciones
    </div>
    <div class="panel-body" id="info-datos">
        {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.ficha', $ficha->id) , 'role' => 'form', 'class' => '')) !!}

        @if($ficha->datos_congelados)
            <div class="note note-info">
                <h4 class="block">Datos congelados</h4>
                <p>No se puede modificar</p>
            </div>
            
            <script type="text/javascript">
                $(document).ready(function() {
                    $("#ficha input[type!='hidden']").prop('disabled', true);
                    $("#ficha :radio").prop('disabled', true);
                    $("#ficha select").prop('disabled', true);
                    $('#ficha .selectpicker').prop('disabled', true);
                    $('#ficha .selectpicker').selectpicker('refresh');
                    $("#ficha input[type='checkbox']").prop('disabled', false);
                    $("#ficha input[name='submit_asignar_info']").prop('disabled', false);
                });
            </script>
           
        @endif


        <div class="form-group row">
            <div class="col-md-3">
                @include('includes.form_select2', ['campo'=> 'prescriptor_id', 'texto'=> 'Prescriptor', 'select'=> $prescriptores, 'valor'=> $ficha->prescriptor_id])
            </div>
            <div class="col-md-3">
                @include('includes.form_select2', ['campo'=> 'oficina_id', 'texto'=> 'Oficina', 'select'=> $oficinas, 'valor'=> $ficha->oficina_id])
            </div>
            <div class="col-md-3">
                @include('includes.form_select2', ['campo'=> 'user_id', 'texto'=> 'Asignado', 'select'=> $asignados = \VCN\Models\User::asignados()->get()->pluck('full_name', 'id')->toArray(), 'valor'=> $ficha->user_id])
            </div>
            @if($ficha->categoria->es_grado_ext)
            <div class="col-md-3">
                @include('includes.form_select2', ['campo'=> 'grado_ext', 'texto'=> 'Grado extranjero', 'select'=> $grados = ConfigHelper::getGradoExt(), 'valor'=> $ficha->grado_ext])
            </div>
            @endif
        </div>

        <hr>

        <div class="form-group row">
            <div class="col-md-2">
                @include('includes.form_checkbox', [ 'campo'=> 'area', 'texto'=> 'Área Cliente' ])
            </div>
            <div class="col-md-2">
                @include('includes.form_checkbox', [ 'campo'=> 'area_pagos', 'texto'=> 'Área Cliente (Pagos)' ])
            </div>
            <div class="col-md-3">
                @include('includes.form_checkbox', [ 'campo'=> 'area_reunion', 'texto'=> 'Área Cliente (Reunión)' ])
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit('Cambiar', array('name'=> 'submit_asignar_info', 'class' => 'btn btn-success')) !!}
        </div>

        {!! Form::close() !!}
    </div>
</div>

<div class="row">
<div class="{{$ficha->es_directo ? 'col-md-12' : 'col-md-6'}}">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-book fa-fw"></i> Curso :: {{$ficha->curso->name}}
        </div>
        <div class="panel-body">
            <table class="table">
                <tr>
                    <td>Nombre del Curso</td>
                    <td>
                        <a href="{{ route('manage.cursos.ficha', $ficha->curso_id) }}">
                            {{ $ficha->curso->es_convocatoria_multi?$ficha->convocatoria->name:$ficha->curso->name}}
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>Convocatoria</td>
                    <td>
                        {{-- {{$ficha->curso->convocatoria_tipo}} --}}
                        {{-- <a href="{{ route('manage.convocatorias.'.$ficha->curso->convocatoria_tipo.'s.ficha', $ficha->convocatoria->id) }}"> --}}
                            {{$ficha->convocatoria?$ficha->convocatoria->name:"-"}}
                        {{-- </a> --}}
                    </td>
                </tr>
                <tr>
                    <td>Categorías</td>
                    <td>{{$ficha->curso->categoria_name}}</td>
                </tr>
                <tr>
                    <td>Centro</td>
                    <td>{{$ficha->centro->name}}</td>
                </tr>
                <tr>
                    <td>Pais</td>
                    <td>{{$ficha->centro->pais->name}}</td>
                </tr>
                <tr>
                    <td>Ciudad</td>
                    <td>{{$ficha->centro->ciudad->city_name}}</td>
                </tr>
                <tr>
                    <td>Tipo Convocatoria</td>
                    <td>{{$ficha->curso->convocatoria_tipo}}</td>
                </tr>
                <tr>
                    <td>Duración</td>
                    <td>{{$ficha->weeks}} {{$ficha->convocatoria->duracion_name}} ({{$ficha->semanas}})</td>
                </tr>
                <tr>
                    <td>Moneda</td>
                    <td>{{Session::get('vcn.moneda')}}</td>
                </tr>
                <tr>
                    <td>Idioma contacto</td>
                    <td>{{ConfigHelper::getIdiomaContacto($ficha->idioma_contacto)}}</td>
                </tr>
                <tr>
                    <td>Código Contable</td>
                    <td>{{$ficha->contable_code}}</td>
                </tr>
                <tr>
                    <td>Prescriptor</td>
                    <td>{{$ficha->prescriptor?$ficha->prescriptor->name:"-"}}</td>
                </tr>
                <tr>
                    <td>Oficina</td>
                    <td>{{$ficha->oficina_name}} [{{$ficha->asignado_name}}]</td>
                </tr>

                {{-- @if($booking->es_online) --}}
                <tr>
                    <td>GeoIP</td>
                    <td>
                    <?php
                        $online = $booking->online;
                        $ip = isset($online['ip'])?$online['ip']:null;
                        if($ip)
                        {
                            $geoip = geoip($ip);
                            $url_map = "https://www.google.es/maps/@". $geoip['lat'] .",". $geoip['lon'] .",15z";
                        }
                    ?>
                    
                    @if($ip)
                        [{{$ip}}] :: {{$geoip['iso_code']}} [{{$geoip['city']}}] - {{$geoip['postal_code']}}

                        <a target="_blank" href="{{$url_map}}">({{$geoip['lat']}} , {{$geoip['lon']}})</a>
                    @else
                        - N/D -
                    @endif
                    </td>

                {{-- @endif --}}
                </tr>

                <tr>
                    <td>Condiciones</td>
                    <td>
                    <?php $condiciones = $ficha->pdf_condiciones; ?>
                    @if($condiciones)
                        <a target="_blank" href='{{ route('file.booking', [$ficha->viajero_id, $ficha->id."_pdf_condiciones.pdf"]) }}'>Ver pdf</a>
                    @else
                        -- NO TIENE -- 
                    @endif
                    </td>
                </tr>

                @if(ConfigHelper::config('es_rgpd'))
                <tr>
                    <td>LOPD</td>
                    <td>
                        <?php
                        $lopd1 = $ficha->logs->where('tipo', 'lopd_check1')->first();
                        $lopd2 = $ficha->logs->where('tipo', 'lopd_check2')->first();
                        $lopd3 = $ficha->logs->where('tipo', 'lopd_check3')->first();
                        ?>
                        
                        {{$lopd1 ? $lopd1->notas : "-"}}
                        <br>
                        {{$lopd2 ? $lopd2->notas : ""}}
                        <br>
                        {{$lopd3 ? $lopd3->notas : ""}}

                    </td>
                </tr>
                @endif

                @if(!$ficha->factura_cif))
                <tr>
                    <?php
                        $dnino = $ficha->logs->where('tipo', 'booking-nif-no')->first();
                    ?>
                    <td>DNI / NIE</td>
                    <td>{{$dnino ? $dnino->notas : "-"}}</td>
                </tr>
                @endif

                <tr>
                    <td>Origen / SubOrigen / SubOrigen Det.</td>
                    <td>{{$ficha->full_name_origen_txt}}</td>
                </tr>

                
            </table>

            <table class="table">
                <caption>Convocatoria</caption>
                <thead>
                    <tr class="thead">
                        <td class="col-md-5">Fecha</td>
                        <td>Duración</td>
                        <td align="right">Precio</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            Del {{$ficha->curso_start_date}} al {{$ficha->curso_end_date}}
                            <span id="booking-cabierta-alerta" class='booking-alerta'>{{isset($ficha->alertas['cabierta'])?$ficha->alertas['cabierta']:""}}</span>
                            <span id="booking-cabierta-precio-detalle" class='booking-precio'>
                                @if($ficha->curso_precio_extra>0)
                                    {{"Precio base: ". ConfigHelper::parseMoneda( ($ficha->course_price), $ficha->curso_moneda)}}
                                    {{" + Extra temporada: ". ConfigHelper::parseMoneda( ($ficha->curso_precio_extra), $ficha->curso_moneda)}}
                                @endif
                            </span>
                        </td>
                        <td>{{$ficha->weeks}} {{$ficha->convocatoria->getDuracionName($ficha->weeks)}}</td>
                        <td align="right">
                            {{ConfigHelper::parseMoneda($ficha->course_total_amount, $ficha->curso_moneda)}}
                        </td>
                    </tr>

                    @if(ConfigHelper::canEdit('booking-fechas-alojamiento'))
                        {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.fechas', [$ficha->id,'curso']), 'role' => 'form', 'class' => '')) !!}
                        <tr>
                            <td>
                                @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> 'Desde', 'valor'=>$ficha->curso_start_date])
                                @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> 'Hasta', 'valor'=>$ficha->curso_end_date])
                            </td>
                            <td colspan='2'>
                                @include('includes.form_submit', [ 'permiso'=> 'booking-fechas-alojamiento', 'texto'=> 'Cambiar'])
                            </td>
                        </tr>

                        {!! Form::close() !!}
                    @endif

                </tbody>
            </table>

            @if($ficha->curso->es_convocatoria_multi)

                {{-- Especialidades --}}
                @if($ficha->pagado)

                    <table class='table'>
                        <caption>Especialidades</caption>
                        <thead>
                            <tr>
                                <td>Semana</td>
                                <td>Especialidad</td>
                                <td align='right'>Precio</td>
                            </tr>
                        </thead>
                        <tbody id='booking-multi-especialidades-table'>
                            @foreach($ficha->multis as $esp)
                            <tr>
                                <td class='col-md-1'>Semana {{$esp->n}} [{{$esp->semana->semana}}]</td>
                                <td>
                                    {{$esp->especialidad_name}}
                                </td>
                                <td align='right' id="booking-multi-especialidad-precio-{{$esp->id}}">{{ ConfigHelper::parseMoneda($esp->precio, $ficha->convocatoria_moneda) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                @else

                    <table class='table'>
                        <caption>Especialidades</caption>
                        <thead>
                            <tr>
                                <th>Semana</th>
                                <th>Especialidad</th>
                                <td align='right'>Precio</td>
                            </tr>
                        </thead>
                        <tbody id='booking-multi-especialidades-table'>
                            @foreach($ficha->multis as $esp)
                            <tr>
                                <td class='col-md-1'>
                                    Semana {{$esp->n}} [{{$esp->semana->semana}}]
                                </td>
                                <td>
                                    {!! Form::select('booking-multi-especialidad-'.$esp->id, $ficha->convocatoria->especialidadesBySemana($esp->semana->semana), $esp->especialidad_id, array( 'id'=> 'booking-multi-especialidad-'.$esp->id, 'class' => 'booking-multi-especialidad form-control', 'data-id'=> $esp->id, 'data-semana'=> $esp->n )) !!}
                                </td>
                                <td align='right' id="booking-multi-especialidad-precio-{{$esp->id}}">{{ ConfigHelper::parseMoneda($esp->precio, $ficha->convocatoria->moneda_name) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                @endif

            @endif

            <table class="table">
                <caption>Notas</caption>
                <tbody>
                    <tr>
                        <td>Notas internas</td>
                        <td>{{$ficha->viajero->datos->notas}}</td>
                    </tr>

                    <tr>
                        <td>Observaciones (viajero)</td>
                        <td>{{$ficha->notas}}</td>
                    </tr>

                    <tr>
                        <td>Notas Booking</td>
                        <td>{{$ficha->notas2}}</td>
                    </tr>
                </tbody>
            </table>

            @include('manage.bookings.ficha-total')

        </div>
    </div>
</div>

@if(!$ficha->es_directo)
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-bed fa-fw"></i> Alojamiento :: {{$ficha->alojamiento?$ficha->alojamiento->name:"Sin Alojamiento"}}
        </div>
        <div class="panel-body">

            <table class="table">
                <thead>
                    <tr class="thead">
                        <td>Fechas</td>
                        <td>Duración</td>
                        <td align="right">Precio</td>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        {{$ficha->alojamiento?$ficha->alojamiento->name:"Sin Alojamiento"}} ::
                        Del {{Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y')}}
                        al {{Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y')}}
                        <span id="booking-alojamiento-alerta" class='booking-alerta'>{{isset($ficha->alertas['alojamiento'])?$ficha->alertas['alojamiento']:""}}</span>
                        <span id="booking-alojamiento-precio-detalle" class='booking-precio'>
                            @if($ficha->alojamiento_precio_extra>0)
                                {{"Precio base: ". ConfigHelper::parseMoneda( ($ficha->accommodation_price), $ficha->alojamiento_moneda)}}
                                {{" + Extra temporada: ". ConfigHelper::parseMoneda( ($ficha->alojamiento_precio_extra), $ficha->alojamiento_moneda)}}
                            @endif
                        </span>
                    </td>
                    <td>
                        <?php $u = $ficha->accommodation_weeks; ?>
                        @if($ficha->curso->es_convocatoria_cerrada)
                            {{$ficha->accommodation_weeks}} {{$ficha->convocatoria->getDuracionName($u)}}
                        @else
                            {{$ficha->accommodation_weeks}} {{$ficha->alojamiento?($ficha->alojamiento->duracion_name?:$ficha->convocatoria->getDuracionName($u)):$ficha->convocatoria->getDuracionName($u)}}
                        @endif
                    </td>
                    <td align='right'>
                        {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
                    </td>
                </tr>
                </tbody>
            </table>

            @if(ConfigHelper::canEdit('booking-fechas-alojamiento'))
                {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.fechas', [$ficha->id,'alojamiento']), 'role' => 'form', 'class' => '')) !!}
                <tr>
                    <td>
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> 'Desde', 'valor'=>$ficha->alojamiento_start_date])
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> 'Hasta', 'valor'=>$ficha->alojamiento_end_date])
                    </td>
                    <td colspan='2'>
                        @include('includes.form_submit', [ 'permiso'=> 'booking-fechas-alojamiento', 'texto'=> 'Cambiar'])
                    </td>
                </tr>

                {!! Form::close() !!}

                @if( $ficha->curso->es_convocatoria_abierta && !$ficha->pagado )
                    <br><br>
                    <hr>

                    {{-- ALOJAMIENTOS --}}
                    <table class="table">
                        <thead>
                        <tr>
                            <th colspan="5">Cambiar Alojamiento</th>
                        </tr>
                        <tr class="thead">
                            <td>Alojamiento</td>
                            <td>Fecha Inicio</td>
                            <td>Fecha Fin</td>
                            <td>Duración</td>
                            <td class="col-md-2" align="right">Precio</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                {!! Form::select('booking-cabierta-alojamiento', [""=>"Sin Alojamiento"] + $ficha->curso->alojamientos->pluck('name','id')->toArray(), $ficha->accommodation_id,
                                        array(
                                            'id'=> 'booking-cabierta-alojamiento',
                                            'class' => 'form-control',
                                            'data-day' => ($ficha->alojamiento?$ficha->alojamiento->start_day:null),
                                        )
                                    )
                                !!}
                                <span class="help-block">{{ $errors->first('booking-alojamiento') }}</span>
                            </td>
                            <td>
                                {!! Form::text('booking-alojamiento-fecha_ini', $ficha->accommodation_start_date?Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y'):"", array( 'id'=>'booking-alojamiento-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('booking-alojamiento-fecha_ini') }}</span>
                            </td>
                            <td>
                                {!! Form::text('booking-alojamiento-fecha_fin', $ficha->accommodation_end_date?Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y'):"", array( 'id'=>'booking-alojamiento-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
                            </td>
                            <td>
                                {!! Form::select('booking-alojamiento-semanas', ConfigHelper::getSemanas($ficha->duracion_name), $ficha->accommodation_weeks, array( 'id'=> 'booking-alojamiento-semanas', 'class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('booking-alojamiento-semanas') }}</span>
                            </td>
                            <td align='right'>
                                <span class="booking-alojamiento-precio" id="booking-alojamiento-precio">
                                    {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
                                </span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                @endif

            @endif

        </div>
    </div>

    @if($ficha->curso->es_convocatoria_cerrada)
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-plane fa-fw"></i> Vuelo
        </div>
        <div class="panel-body">
            <table class="table">
                <thead>
                    <tr class="thead">
                        <td>Nombre</td>
                        {{-- <td align="right">Precio</td> --}}
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        @if($ficha->vuelo)
                            {{$ficha->vuelo->aeropuerto}} [{{$ficha->vuelo->name}}]
                            <br><br>
                            Requisitos especiales: {{$ficha->transporte_requisitos}}
                            <br>
                            Aporte desde/a: {{$ficha->transporte_recogida}}
                        @else
                            @if($ficha->transporte_no)
                                NO necesito transporte
                            @elseif($ficha->transporte_otro)
                                Otro transporte: {{$ficha->transporte_detalles}}
                            @else
                                -
                            @endif
                        @endif
                    </td>
                    {{-- <td align='right'></td> --}}
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    @endif

    <hr>

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-phone fa-fw"></i> Contactos SOS
        </div>
        <div class="panel-body">

            {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.ficha', [$ficha->id]), 'role' => 'form', 'class' => '')) !!}
                <div class="form-group">
                    <?php
                        $select = \VCN\Models\System\Contacto::activos()->where('es_proveedor',1)->get()->pluck('full_name','id')->toArray();
                    ?>
                    @include('includes.form_select2_multi', ['campo'=> "sos_proveedor[]", 'texto'=> 'Proveedor', 'select'=> $select, 'valor'=> $ficha->sos_proveedor ?: [] ])

                    <br>
                    {!! $ficha->sos_proveedor_notas !!}
                </div>
                
                <div class="form-group row">
                    <div class="col-md-8">
                    <?php
                        $select = \VCN\Models\System\Contacto::activos()->where('es_proveedor',0)->get()->pluck('full_name','id')->toArray();
                    ?>
                    @include('includes.form_select2', ['campo'=> "sos_plataforma", 'texto'=> 'Plataforma', 'select'=> $select ])

                    <br>
                    {!! $ficha->sos_plataforma_notas !!}
                    </div>
                    <div class="col-md-2">
                        &nbsp;<br>
                        {!! Form::submit("Actualizar", array('name'=> 'submit_contactos_sos', 'class' => 'btn btn-success')) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

    <hr>

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-star-half-o fa-fw"></i> Extras
        </div>
        <div class="panel-body">
            <table class="table">
                <thead>
                    <tr class="thead">
                        <td>Extra</td>
                        <td>Unidad</td>
                        <td>Precio</td>
                    </tr>
                </thead>
                <tbody>

                    @foreach($ficha->extrasCurso as $extra)
                        @include('includes.bookings_tr_extra', ['extra'=> $extra])
                    @endforeach

                    @foreach($ficha->extrasCentro as $extra)
                        @include('includes.bookings_tr_extra', ['extra'=> $extra])
                    @endforeach

                    @foreach($ficha->extras_alojamiento as $extra)
                        @include('includes.bookings_tr_extra', ['extra'=> $extra])
                    @endforeach

                    @foreach($ficha->extrasGenerico as $extra)
                        @include('includes.bookings_tr_extra', ['extra'=> $extra])
                    @endforeach

                    @foreach($ficha->extras_cancelacion as $extra)
                        @include('includes.bookings_tr_extra', ['extra'=> $extra])
                    @endforeach

                    @foreach($ficha->extrasOtros as $extra)
                    <tr>
                        <td>{{$extra->notas}}</td>
                        <td>
                            <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
                            x{{$extra->unidades}}
                        </td>
                        <td align="right">{{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda_name) }}</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

    <hr>

    @include('manage.bookings.descuentos.list', ['booking_id'=> $ficha->id])

</div>
@endif
</div>

@if(!$ficha->es_directo)
*LOPD: {!! trans('area.booking.lopd_check1', ['responsable'=> ConfigHelper::config('name')]) !!}
<br>
*LOPD C.2: {!! trans('area.booking.lopd_check2', ['plataforma'=> ConfigHelper::plataformaApp()]) !!}
<br>
*LOPD C.3: {!! trans('area.booking.lopd_check3') !!}
@endif