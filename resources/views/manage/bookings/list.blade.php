<div class="portlet light tabs">

    <div class="portlet-body">

            {!! Datatable::table()
                ->addColumn([
                  'fecha_reserva'   => 'Fecha Booking',
                  'name'            => 'Viajero',
                  'curso'           => 'Convocatoria/Curso',
                  'course_start_date' => 'Fecha Inicio',
                  'duracion'        => 'Ud. duración',
                  'status'          => 'Status',
                  'asignado'        => 'Asignado',
                  'online'          => 'Online',
                  'options' => ''

                ])
                ->setUrl( route('manage.bookings.index', [$status_id,$user_id,$oficina_id, "any=$any"]) )
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "columnDefs", array(
                    [ "sortable" => false, "targets" => [2,4,5,6,8] ],
                    [ "targets" => [0,3], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
                ->setOptions(
                    "rowCallback", "function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                        if(aData['status'].indexOf('badge-incidencias')>0) { $('td', nRow).addClass('td-st-incidencias'); }
                        if(aData['status'].indexOf('fa-exclamation-triangle')>0) { $('td', nRow).addClass('td-st-incidencia'); }
                     }"
                )
                ->render() !!}

    </div>

</div>