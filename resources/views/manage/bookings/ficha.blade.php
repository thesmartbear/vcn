@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.bookings.ficha', $ficha) !!}
@stop


@section('content')

<div id="booking-user">
    <div class="panel panel-default">
        <div class="panel-heading">
        <div class="row">
            <div class="col-md-7">
                <i class="fa fa-user fa-fw"></i> Booking :: {{$ficha->fecha}} :: Viajero :: <a href="{{route('manage.viajeros.ficha', $ficha->viajero->id)}}">{{$ficha->viajero->full_name}}</a> ({{$ficha->status_name}})

                @if($ficha->es_cancelado || $ficha->es_refund || $ficha->pagado)
                    <i class='fa fa-lock'></i>
                @endif

                @if($ficha->es_online)
                    <span class="caption">[ONLINE :: {!! $ficha->online_creador !!}] </span>
                @endif
            </div>
            <div class="col-md-1 pull-right">{!! $ficha->status_icono !!}</div>
            <div class="col-md-2 pull-right">
                @if(!$ficha->es_cancelado)
                    @php
                        $pregunta = "Vas a cancelar una inscripción y liberar una plaza. Esta inscripción se seguirá mostrando en el área de cliente. Para que no aparezca, tienes que quitar el check 'Área cliente' en la ficha booking.";
                        if($ficha->tiene_facturas)
                        {
                            $pregunta .= " [TIENE FACTURAS]";
                        }

                        $data = " data-label='Cancelar' data-pregunta='$pregunta' data-action='" . route('manage.bookings.cancelar', $ficha->id) . "'";
                    @endphp 
                    <a {!! $data !!} href='#confirma' class='btn btn-danger btn-xs'><i class='fa fa-ban'></i> Cancelar</a>
                @endif
            </div>
            <div class="col-md-3 pull-right">
                {!! $ficha->boton_incidencia !!}

                @if($booking->area_examenes->count())
                    <a class="btn btn-info btn-xs" href="{{route('manage.bookings.examenes.mail', $ficha->id)}}"><i class='fa fa-envelope'></i> Enviar Tests</a>
                @endif

                @if($ficha->tiene_incidencias_pendientes)
                    <i class="fa fa-exclamation-triangle icon-st-incidencia"></i>
                @endif

                <a href="#" data-toggle='modal' data-target='#modalViajeroLog' class="btn btn-warning btn-xs">
                    <i class="fa fa-plus-circle"></i> Seguimiento
                </a>

                <?php

                if ($ficha->status_id == ConfigHelper::config('booking_status_overbooking'))
                {
                        if ($ficha->curso->es_convocatoria_cerrada) {
                            $plazasv = $ficha->vuelo ? $ficha->vuelo->plazas_disponibles_ovbkg : 1;
                            $plazasa = 0;

                            if($ficha->alojamiento)
                            {   
                                $pa = $ficha->convocatoria ? $ficha->convocatoria->getPlazas($ficha->alojamiento->id) : null;
                                if ($pa) {
                                    $plazasa = $pa->plazas_disponibles_ovbkg;
                                }
                            }

                            $plazas = "P.Vuelo: $plazasv , P.Aloj.: $plazasa";
                            echo " <a data-label='$plazas' href='" . route('manage.bookings.overbooking', [$ficha->id]) . "' class='btn btn-warning btn-xs'><i class='fa fa-fighter-jet'></i> OVBKG</a>";
                        } elseif ($ficha->curso->es_convocatoria_multi) {
                            
                        }
                    }

                ?>
            </div>
        </div>
        </div>

        <div class="panel-body">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a data-label="Información" href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab"><i class="fa fa-info"></i></a></li>

                @if(!$ficha->es_cancelado && !$ficha->es_refund)
                    <li role="presentation"><a href="#datos" aria-controls="datos" role="tab" data-toggle="tab">Datos</a></li>
                    <li role="presentation"><a href="#extras" aria-controls="extras" role="tab" data-toggle="tab">Extras</a></li>

                    @if( $ficha->curso->es_convocatoria_cerrada )
                    <li role="presentation"><a data-label="Vuelo" href="#vuelo" aria-controls="vuelo" role="tab" data-toggle="tab"><i class="fa fa-plane"></i></a></li>
                    @endif
                @endif

                <li role="presentation"><a data-label="Checklist" href="#checklist-tab" aria-controls="checklist-tab" role="tab" data-toggle="tab"><i class="fa fa-tag"></i></a></li>

                @if($ficha->alojamiento)
                    @if($ficha->alojamiento->tipo->es_familia)
                        <li role="presentation"><a data-label="Familias" href="#familia" aria-controls="familia" role="tab" data-toggle="tab"><i class="fa fa-users"></i></a></li>
                    @endif
                @endif

                <li role="presentation"><a data-label="Schools" href="#school" aria-controls="school" role="tab" data-toggle="tab"><i class="fa fa-graduation-cap"></i></a></li>

                @if($ficha->es_campamento_cic)
                    <li role="presentation"><a href="#niveles" aria-controls="niveles" role="tab" data-toggle="tab">Niveles</a></li>
                @endif

                <li role="presentation"><a data-label="Pagos" href="#pagos" aria-controls="pagos" role="tab" data-toggle="tab"><i class="fa fa-money"></i></a></li>

                @if(count($ficha->incidencias))
                    <li role="presentation"><a data-label="Incidencias" href="#incidencias" aria-controls="incidencias" role="tab" data-toggle="tab"><i class="fa fa-exclamation-triangle"></i></a></li>
                @endif

                @if(!$ficha->pagado)
                    @if($ficha->monedas->count()>0)
                        <li role="presentation"><a href="#monedas" aria-controls="monedas" role="tab" data-toggle="tab">T.Cambio</a></li>
                    @endif
                @endif

                @if(!$ficha->es_cancelado && !$ficha->es_refund)
                <li role="presentation"><a href="#descuentos" aria-controls="descuentos" role="tab" data-toggle="tab">Dtos</a></li>
                @endif

                @if($ficha->es_cancelado || $ficha->es_prebooking || $ficha->es_refund || $ficha->es_vuelta || $ficha->es_fuera )
                    <li role="presentation"><a href="#devoluciones" aria-controls="devoluciones" role="tab" data-toggle="tab">Devoluc.</a></li>
                @endif

                <li role="presentation"><a data-label="Historial" href="#historial" aria-controls="historial" role="tab" data-toggle="tab"><i class="fa fa-history"></i></a></li>
                <li role="presentation"><a data-label="PDF y PDF Proveedor" href="#pdf" aria-controls="pdf" role="tab" data-toggle="tab"> <i class="fa fa-file-pdf-o"></i></a></li>

                @if( ConfigHelper::config('facturas') && ConfigHelper::canEdit('facturas')  )
                <li role="presentation"><a data-label="Facturas" href="#facturas" aria-controls="facturas" role="tab" data-toggle="tab"> <i class="fa fa-ticket"></i></a></li>
                @endif

                @if(!$ficha->es_cancelado && !$ficha->es_refund)
                    @if($ficha->vuelo)
                        <li role="presentation"><a href="#vuelo-info" aria-controls="vuelo-info" role="tab" data-toggle="tab"><i class="fa fa-info-circle"></i> <i class="fa fa-plane"></i></a></li>
                    @endif
                @endif

                @if( $ficha->cuestionarios->count() )
                    <li role="presentation"><a data-label="Cuestionarios" href="#cuestionarios" aria-controls="cuestionarios" role="tab" data-toggle="tab"><i class="fa fa-list-alt"></i></a></li>
                @endif

                <li role="presentation"><a data-label="Archivos" href="#archivos" aria-controls="archivos" role="tab" data-toggle="tab"><i class="fa fa-paperclip"></i></a></li>

                <li role="presentation"><a data-label="Documentos Específicos" href="#documentos_e" aria-controls="documentos_e" role="tab" data-toggle="tab"><i class="fa fa-folder"></i></a></li>

                <li role="presentation"><a data-label="Generar Documentos" href="#documentos" aria-controls="documentos" role="tab" data-toggle="tab"><i class="fa fa-file-word-o"></i></a></li>
                <li role="presentation"><a href="#seguimiento" aria-controls="seguimiento" role="tab" data-toggle="tab">Seguimiento</a></li>

            </ul>


            <!-- Tab panes -->
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane fade in active" id="ficha">
                    <div class="col-md-12"><br>
                        @include('manage.bookings.ficha-info')
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="seguimiento">
                    @include('manage.viajeros.logs.list', ['viajero_id'=> $ficha->viajero_id, 'todos'=> 0])
                </div>

                @if(!$ficha->es_cancelado && !$ficha->es_refund)
                <div role="tabpanel" class="tab-pane fade in" id="datos">
                    <div class="col-md-12"><br>

                        {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.ficha', $ficha->id) , 'role' => 'form', 'class' => '')) !!}

                            @if($ficha->datos_congelados)
                                <div class="note note-info">
                                    <h4 class="block">Datos congelados</h4>
                                    <p>No se puede modificar</p>
                                </div>
                            @endif

                            {{-- <div class="form-group row">
                                <div class="col-md-4">
                                    @include('includes.form_select2', ['campo'=> 'prescriptor_id', 'texto'=> 'Prescriptor', 'select'=> $prescriptores, 'valor'=> $ficha->prescriptor_id])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_select2', ['campo'=> 'oficina_id', 'texto'=> 'Oficina', 'select'=> $oficinas, 'valor'=> $ficha->oficina_id])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_select2', ['campo'=> 'user_id', 'texto'=> 'Asignado', 'select'=> $asignados = \VCN\Models\User::asignados()->get()->pluck('full_name', 'id')->toArray(), 'valor'=> $ficha->user_id])
                                </div>

                                <div class="col-md-1">
                                    <br>
                                    {!! Form::submit('Cambiar', array('name'=> 'submit_asignar','class' => 'btn btn-success')) !!}
                                </div>
                            </div>

                            <hr> --}}

                            @include('manage.bookings.ficha-viajero')

                            {{-- <hr>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    @include('includes.form_checkbox', [ 'campo'=> 'area', 'texto'=> 'Área Cliente' ])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_checkbox', [ 'campo'=> 'area_pagos', 'texto'=> 'Área Cliente (Pagos)' ])
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_checkbox', [ 'campo'=> 'area_reunion', 'texto'=> 'Área Cliente (Reunión)' ])
                                </div>
                            </div> --}}
                            
                            <hr>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    @include('includes.form_checkbox', [ 'campo'=> 'promo_cambio_fijo', 'texto'=> 'Promoción Tipo de Cambio Fijo' ])
                                </div>
                            </div>


                            @if(!$ficha->datos_congelados)
                            <hr>
                            <div class="form-group">
                                {!! Form::submit('Actualizar', array('class' => 'btn btn-success')) !!}
                            </div>
                            @else
                                <div class="note note-info">
                                    <h4 class="block">Datos congelados</h4>
                                    <p>No se puede modificar</p>
                                </div>
                            @endif

                        {!! Form::close() !!}
                    </div>
                </div>


                <div role="tabpanel" class="tab-pane fade in" id="extras">

                    <div class="booking col-md-12"><br>
                    <div id="booking-form1" class="item">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-graduation-cap fa-fw"></i> Extras
                            </div>
                            <div class="panel-body">

                                @include('manage.bookings.ficha-curso-extras')

                            </div>
                        </div>
                    </div>
                    </div>


                    <div id="booking-extra-add" class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-gift fa-fw"></i> Añadir Extra
                            </div>
                            <div class="panel-body">
                                {!! Form::open(array('route' => array('manage.bookings.extra.add', $ficha->id))) !!}

                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            @include('includes.form_input_text', [ 'campo'=> 'extra_importe', 'texto'=> 'Importe'])
                                        </div>

                                        <div class="col-md-2">
                                            @include('includes.form_select', [ 'campo'=> 'moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas])
                                        </div>

                                        <div class="col-md-3">
                                            @include('includes.form_input_text', [ 'campo'=> 'extra_name', 'texto'=> 'Nombre'])
                                        </div>

                                        <div class="col-md-4">
                                            @include('includes.form_input_text', [ 'campo'=> 'extra_notas', 'texto'=> 'Notas'])
                                        </div>

                                        <div class="col-md-1">
                                            {!! Form::label('Añadir') !!}<br>
                                            {!! Form::submit('Crear', array('class' => 'btn btn-success')) !!}
                                        </div>

                                    </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                </div>

                <div role="tabpanel" class="tab-pane fade in" id="checklist-tab">
                    <div class="col-md-12"><br>
                        @include('includes.checklist', ['ficha'=> $ficha->viajero, 'booking_id'=> $ficha->id])

                        <hr>

                        @include('manage.bookings.logs.list', ['booking_id'=> $ficha->id, 'filtro'=> 'Checklist'])

                    </div>
                </div>

                @if( $ficha->curso->es_convocatoria_cerrada || $ficha->curso->es_convocatoria_abierta)
                <div role="tabpanel" class="tab-pane fade in" id="vuelo">
                    <div class="col-md-12"><br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-plane fa-fw"></i> Aeropuerto de Salida
                                @if($ficha->vuelo)
                                    :: <a href="{{route('manage.convocatorias.vuelos.ficha',$ficha->vuelo->id)}}" title="">Vuelo {{$ficha->vuelo->name}}</a>
                                @endif
                        </div>
                        <div class="panel-body">

                            {!! Form::model($ficha, array( 'id'=> 'frm-vuelo', 'route' => array('manage.bookings.vuelo', $ficha->id))) !!}

                            <div id="booking-vuelo-table"
                                @if($ficha->transporte_no)
                                    style="display:none;"
                                @endif
                            >

                                <div class="form-group">
                                    @include('includes.form_select', ['campo'=> 'vuelo_id', 'texto'=> 'Vuelo', 'select'=> $ficha->vuelos_list])
                                </div>

                                <div class="form-group" id="div_transporte_requisitos">
                                    @include('includes.form_input_text', [ 'campo'=> 'transporte_requisitos', 'texto'=> 'Requisitos especiales para este vuelo (dieta, cambio de vuelo,...)'])
                                </div>

                                <div class="form-group">
                                    @include('includes.form_checkbox', [ 'campo'=> 'transporte_recogida_bool',
                                        'texto'=> '¿Quieres que tramitemos un vuelo desde el aeropuerto más cercano a tu ciudad hasta el aeropuerto de salida de nuestro grupo?', 'valor'=>$ficha->transporte_recogida ])
                                    <div id="div_transporte_recogida_bool"
                                        @if(!$ficha->transporte_recogida)
                                            style="display:none;"
                                        @endif
                                    >

                                        @include('includes.form_input_text', [ 'campo'=> 'transporte_recogida',
                                            'texto'=> 'Indica el aeropuerto de salida y regreso más cercano a tu ciudad'])
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                @include('includes.form_checkbox', [ 'campo'=> 'transporte_otro', 'texto'=> 'Necesito otro transporte' ])
                                <div id="div_transporte_otro"
                                    @if(!$ficha->transporte_otro)
                                        style="display:none;"
                                    @endif
                                >
                                    @include('includes.form_input_text', [ 'campo'=> 'transporte_detalles', 'texto'=> '¿Necesitas que te ayudemos a buscar tu vuelo?'])
                                </div>
                            </div>

                            <div class="form-group">
                                @include('includes.form_checkbox', [ 'campo'=> 'transporte_no', 'texto'=> 'NO necesito transporte' ])
                            </div>

                            <hr>

                            <div class="form-group pull-right">
                                {!! Form::submit('Actualizar', array('id'=> 'btn-vuelo', 'class' => 'btn btn-success')) !!}
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                    </div>
                </div>
                @endif
                @endif

                <div role="tabpanel" class="tab-pane fade in" id="pagos">
                    @include('manage.bookings.pagos.list', ['booking_id'=> $ficha->id])
                </div>

                @if(count($ficha->incidencias))
                    <div role="tabpanel" class="tab-pane fade in" id="incidencias">
                        @include('manage.bookings.incidencias.list', ['booking_id'=> $ficha->id])
                    </div>
                @endif

                @if(!$ficha->pagado && $ficha->monedas->count()>0)
                    <div role="tabpanel" class="tab-pane fade in" id="monedas">
                        @include('manage.bookings.monedas.list', ['booking_id'=> $ficha->id])
                    </div>
                @endif

                @if(!$ficha->es_cancelado && !$ficha->es_refund)
                    <div role="tabpanel" class="tab-pane fade in" id="descuentos">
                        @include('manage.bookings.descuentos.list', ['booking_id'=> $ficha->id, 'add'=> true])
                    </div>
                @endif

                @if($ficha->es_cancelado || $ficha->es_prebooking || $ficha->es_refund || $ficha->es_vuelta || $ficha->es_fuera)
                    <div role="tabpanel" class="tab-pane fade in" id="devoluciones">
                        @include('manage.bookings.devoluciones.list', ['booking_id'=> $ficha->id, 'add'=> true])
                    </div>
                @endif

                <div role="tabpanel" class="tab-pane fade in" id="historial">
                    @include('manage.bookings.logs.list', ['booking_id'=> $ficha->id])
                </div>

                <div role="tabpanel" class="tab-pane fade in" id="pdf">
                    <div class="col-md-12">
                        <br>
                        <div class="row">
                        
                            <table class="table table-bordered">
                                <caption>Bookings para el cliente</caption>
                                <thead>
                                    <tr>
                                        <th class="col-md-8">PDF</th>
                                        <th></th>
                                        <th>Firmas</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $pubpath = "/files/bookings/" . $ficha->viajero_id . "/";
                                    $path = storage_path($pubpath);


                                    if(is_dir($path))
                                    {
                                        $results = scandir($path);
                                        //$files = File::allFiles($path);
                                        foreach( $results as $result )
                                        {
                                            if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0]==='.') continue;

                                            $file = $path . '/' . $result;

                                            $fbooking = "ooking_".$ficha->id."-";
                                            $href = $pubpath.$result;

                                            if( is_file($file) )
                                            {
                                                if(strpos($result,$fbooking))
                                                {

                                                    $pdf = $ficha->getArchivoFirma($result);

                                                    $firmado = $pdf ? $pdf->es_firmado : false;

                                                    ?>
                                                    
                                                    <tr>
                                                        <td>
                                                            <a target="_blank" href="{{$href}}">{{$result}}</a>
                                                        </td>

                                                        <td>
                                                            {!! ConfigHelper::iframe($result, $path, $pubpath) !!}
                                                        </td>

                                                        <td>
                                                            
                                                            @if($firmado)
                                                                @if($pdf->firmas && count($pdf->firmas))
                                                                    @foreach($pdf->firmas as $firma)
                                                                        {{$firma['fecha']}} por {{$firma['firmante']}} [ip::{{$firma['ip']}}]
                                                                        <br>
                                                                        <img src="{{$firma['firma']}}" alt="{{$firma['firmante']}}" class="booking-firma">
                                                                        <hr>
                                                                    @endforeach
                                                                @else
                                                                    -FIRMADO-
                                                                @endif
                                                            @else
                                                                @if($pdf && $pdf->es_firma_solicitada)
                                                                    <i>(Solicitada el {{$pdf->updated_at->format('d/m/Y H:i')}})</i>
                                                                @endif
                                                                <a href="{{ route('manage.bookings.firma.solicitar', [$ficha->id,$result])}}">Solicitar firma</a>
                                                            @endif
                                                            
                                                        </td>
                                                    </tr>

                                                    <?php
                                                }
                                            }

                                        }
                                    }
                                    ?>

                                </tbody>
                            </table>

                        </div>

                        <hr>

                        <br>
                        <div class="row">
                        
                            <table class="table table-bordered">
                            <caption>Bookings para el proveedor</caption>
                            <thead>
                                <tr>
                                    <th>PDF</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $pubpath = "/files/bookings/" . $ficha->viajero_id . "/";
                                $path = storage_path($pubpath);


                                if(is_dir($path))
                                {
                                    $results = scandir($path);
                                    foreach ($results as $result) {
                                        if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0]==='.') continue;

                                        $file = $path . '/' . $result;

                                        $fbooking = "ooking_";
                                        $ffactura = "actura_";
                                        $fotro = "_$ficha->id.pdf";

                                        if( is_file($file) )
                                        {
                                            if(!strpos($result,$fbooking) && !strpos($result,$ffactura) && strpos($result,$fotro))
                                            {
                                                ?>
                                                <tr>
                                                    <td>
                                                        <a target="_blank" href="{{$pubpath}}{{$result}}">{{$result}}</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>

                            </tbody>
                        </table>

                        </div>

                    </div>
                </div>

                @if( ConfigHelper::config('facturas') && ConfigHelper::canEdit('facturas')  )  {{-- && $ficha->pagado --}}
                <div role="tabpanel" class="tab-pane fade in" id="facturas">
                    <div class="col-md-12">
                        @include('manage.bookings.facturas', ['booking_id'=> $ficha->id])
                    </div>
                </div>
                @endif

                @if(!$ficha->es_cancelado && !$ficha->es_refund)
                @if($ficha->vuelo)
                <div role="tabpanel" class="tab-pane fade in" id="vuelo-info">

                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <caption></caption>
                        <thead>
                        <tr>
                            <th class="col-md-6">IDA</th>
                            <th>VUELTA</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    @foreach($ficha->vuelo->etapas->where('tipo',0) as $v)
                                        {{Carbon::parse($v->salida_fecha)->format('d/m/Y')}} {{$v->company}} {{$v->vuelo}}
                                        <br>DEPARTURE <br>{{$v->salida_aeropuerto}} {{substr($v->salida_hora,0,5)}}H
                                        <br>ARRIVAL <br>{{$v->llegada_aeropuerto}} {{substr($v->llegada_hora,0,5)}}H
                                        <br>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($ficha->vuelo->etapas->where('tipo',1) as $v)
                                        {{Carbon::parse($v->salida_fecha)->format('d/m/Y')}} {{$v->company}} {{$v->vuelo}}
                                        <br>DEPARTURE <br>{{$v->salida_aeropuerto}} {{substr($v->salida_hora,0,5)}}H
                                        <br>ARRIVAL <br>{{$v->llegada_aeropuerto}} {{Carbon::parse($v->llegada_fecha)->format('d/m/Y')}} {{substr($v->llegada_hora,0,5)}}H
                                        <br>
                                    @endforeach
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                @endif
                @endif

                @if($ficha->alojamiento)
                    @if($ficha->alojamiento->tipo->es_familia)
                        <div role="tabpanel" class="tab-pane fade" id="familia">
                            @include('manage.bookings.ficha-familia')
                        </div>
                    @endif
                @endif

                <div role="tabpanel" class="tab-pane fade" id="school">
                    @include('manage.bookings.ficha-school')
                </div>

                @if($ficha->es_campamento_cic)
                    <div role="tabpanel" class="tab-pane fade" id="niveles">

                        <div class="col-md-12">

                        {!! Form::model($ficha, array('route' => array('manage.bookings.ficha', $ficha->id))) !!}

                            <div class="form-group row">
                                <div class="col-md-4">
                                    @include('includes.form_checkbox', [ 'campo'=> 'cic', 'texto'=> 'Estudias en un centro de idiomas del CIC', 'ficha'=> $ficha->datos ])
                                </div>
                                <div id="cic_nivel_div" class="col-md-8" style="display:none;">
                                    @include('includes.form_select', [ 'campo'=> 'cic_nivel', 'texto'=> 'Nivel', 'select'=> ConfigHelper::getCICNivel(), 'ficha'=>$ficha->datos ])
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-2">
                                    @include('includes.form_checkbox', [ 'campo'=> 'trinity_exam', 'texto'=> 'Has hecho el Trinity Exam', 'ficha'=> $ficha->datos ])
                                </div>
                                <div id="trinity_exam_div" style="display:none;">
                                    <div class="col-md-2">
                                        @include('includes.form_input_text', [ 'campo'=> 'trinity_any', 'texto'=> 'Año', 'ficha'=> $ficha->datos ])
                                    </div>
                                    <div class="col-md-6">
                                        @include('includes.form_select', [ 'campo'=> 'trinity_nivel', 'texto'=> 'Nivel', 'select'=> ConfigHelper::getTrinityNivel(), 'ficha'=> $ficha->datos ])
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    @include('includes.form_select', [ 'campo'=> 'nivel_cic', 'texto'=> 'Nivel CIC Camp', 'select'=> ConfigHelper::getCICNivel() ])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_select', [ 'campo'=> 'nivel_trinity', 'texto'=> 'Nivel Trinity Camp', 'select'=> ConfigHelper::getTrinityNivel() ])
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-2 pull-right">
                                    {!! Form::submit('Actualizar', array('name'=> 'submit_niveles','class' => 'btn btn-success')) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                        </div>

                        <script type="text/javascript">
                            $(document).ready( function() {

                                @if($ficha->datos->trinity_exam)
                                    $('#trinity_exam_div').show();
                                @endif

                                $("#trinity_exam").click(function(){
                                    if( $(this).is(':checked') )
                                    {
                                        $('#trinity_exam_div').show();
                                    }
                                    else
                                    {
                                        $('#trinity_exam_div').hide();
                                    }
                                });

                            });
                        </script>


                    </div>
                @endif

                @if( $ficha->cuestionarios->count() )
                    <div role="tabpanel" class="tab-pane fade" id="cuestionarios">
                        @include('manage.system.cuestionarios.booking', ['booking'=> $ficha])
                    </div>
                @endif

                <div role="tabpanel" class="tab-pane fade" id="archivos">

                    <div class="row">
                    <div class="col-md-12">

                    <?php
                    $path = storage_path("files/viajeros/" . $ficha->viajero->id) ."/";
                    $folder = "/files/viajeros/" . $ficha->viajero->id ."/";
                    ?>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4>Archivos <i>(Visibles en el área de cliente)</i></h4>
                        </div>
                        <div class="panel-body">

                            <table class="table table-bordered table-hover">
                            <?php
                            foreach($ficha->archivos as $archivo)
                            {
                                if($archivo->visible==0)
                                {
                                    continue;
                                }

                                $name = $archivo->doc;
                                $file = $path . '/' . $name;
                                $filew = $folder .'/'. $name;
                                if($name && $name[0] == "/")
                                {
                                    $file = $path . $name;
                                    $filew = $folder . $name;
                                }

                                $filew = preg_replace('/(\/+)/','/', $filew);

                                $btn_class = "btn-xs btn-danger";
                                if($archivo->visible==1)
                                {
                                    $btn_class = "btn-xs btn-success";
                                }

                                ?>

                                @if(is_file($file))
                                <tr>
                                <td class="col-sm-9">
                                    
                                    {{$name}}

                                    @if($archivo->doc_especifico_id)
                                        [Doc. específico]
                                    @endif

                                    @if(starts_with($archivo->doc, 'Nota_Pago_Final_'))
                                        <br>({{$archivo->notapago_enviado?"Enviado [". $archivo->created_at->format('d/m/Y H:i') ."]" :"No Enviado"}})
                                    @endif

                                </td>

                                <td>{!! ConfigHelper::iframe($name, $path, $folder); !!}</td>

                                <td class="col-sm-3">
                                    <a href="{{ route('manage.viajeros.archivo.delete', [$archivo->id]) }}" class="btn btn-danger btn-xs" role="button">Borrar</a>
                                    <a href="{{ route('manage.viajeros.archivo.visible', [$archivo->id,1]) }}" class="btn {{$btn_class}} pull-right" role="button">{{($archivo->visible==1?"Visible":"No visible")}}</a>
                                    <hr>
                                    <a class="btn btn-success btn-block" href="{{$filew}}" target="_blank" download>
                                        <i class="fa fa-download"></i> Descargar
                                    </a>
                                </td>
                                </tr>
                                @endif

                                <?php
                            }
                            ?>
                            </table>

                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                    <div class="form-group">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4>Añadir archivos visibles (img/pdf)</h4>
                                            </div>

                                            <div class="dropzone" id="myDropzone1"></div>

                                        </div>
                                    </div>
                                    @include('includes.script_dropzone', ['name'=> 'myDropzone1', 'url'=> route('manage.viajeros.upload', [$ficha->viajero->id,1,$ficha->id])])
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                    <div class="form-group">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4>Facturas</h4>
                                            </div>

                                            <div class="panel-body">

                                                <table class="table table-bordered">  
                                                <thead>
                                                    <tr>
                                                        <th>Factura</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach($ficha->facturas->where('manual',0) as $factura)
                                                        <tr>

                                                        <?php
                                                            $dir = "";
                                                            $name = "";
                                                            if($factura->grup)
                                                            {
                                                                $dir = "/files/facturas/";
                                                                $name = "factura_". $factura->grup->numero .".pdf";
                                                            }
                                                            else
                                                            {
                                                                $dir = "/files/bookings/". $factura->booking->viajero->id . "/";
                                                                $name = "factura_". $factura->numero .".pdf";
                                                            }
                                                            $file = $dir . $name;
                                                        ?>
                                                        <td>
                                                            <a href='{{$file}}' target='_blank'><i class='fa fa-file-pdf-o'></i> {{$name}}</a>
                                                        </td>

                                                        <td>{!! ConfigHelper::iframePdf($file, $name) !!}</td>

                                                        </tr>

                                                    @endforeach

                                                </tbody>
                                                </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h4>Archivos <i>(NO Visibles en el área de cliente)</i></h4>
                        </div>
                        <div class="panel-body">

                            <table class="table table-bordered table-hover">
                            <?php
                            foreach($ficha->archivos as $archivo)
                            {
                                if($archivo->visible==1)
                                {
                                    continue;
                                }


                                $name = $archivo->doc;
                                $file = $path . '/' . $name;
                                $filew = $folder .'/'. $name;
                                if($name && $name[0] == "/")
                                {
                                    $file = $path . $name;
                                    $filew = $folder . $name;
                                }

                                $btn_class = "btn-xs btn-danger";
                                if($archivo->visible==1)
                                {
                                    $btn_class = "btn-xs btn-success";
                                }

                                ?>

                                @if(is_file($file))
                                <tr>
                                <td class="col-sm-9">
                                    
                                    {{$name}}

                                    @if(starts_with($archivo->doc, 'Nota_Pago_Final_'))
                                        <br>({{$archivo->notapago_enviado?"Enviado [". $archivo->created_at->format('d/m/Y H:i') ."]" :"No Enviado"}})
                                    @endif

                                </td>
                                <td>{!! ConfigHelper::iframe($name, $path, $folder); !!}</td>
                                <td class="col-sm-3">
                                    <a href="{{ route('manage.viajeros.archivo.delete', [$archivo->id]) }}" class="btn btn-danger btn-xs" role="button">Borrar</a>
                                    <a href="{{ route('manage.viajeros.archivo.visible', [$archivo->id,1]) }}" class="btn {{$btn_class}} pull-right" role="button">{{($archivo->visible==1?"Visible":"No visible")}}</a>
                                    <hr>
                                    <a class="btn btn-success btn-block" href="{{$filew}}" target="_blank" download>
                                        <i class="fa fa-download"></i> Descargar
                                    </a>
                                </td>
                                </tr>
                                @endif

                                <?php
                            }
                            ?>
                            </table>

                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                    <div class="form-group">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4>Añadir archivos NO visibles (img/pdf)</h4>
                                            </div>

                                            <div class="dropzone" id="myDropzone0"></div>

                                        </div>
                                    </div>
                                    @include('includes.script_dropzone', ['name'=> 'myDropzone0', 'url'=> route('manage.viajeros.upload', [$ficha->viajero->id,0,$ficha->id])])
                                </div>
                            </div>

                        </div>
                    </div>


                    </div>
                    </div>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="documentos_e">

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4>Documentos específicos</i></h4>
                        </div>
                        <div class="panel-body">

                            <table class="table table-bordered table-hover">

                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Notas</th>
                                        <th>Descargable</th>
                                        <th>Adjunto</th>
                                        <th></th>
                                    </tr>
                                </thead>

                                <?php
                                    $dir = "/assets/uploads/doc_especificos/";
                                ?>

                                @foreach($booking->documentos_especificos as $doc)

                                    <?php
                                        $dir = $dir . $doc->modelo ."/". $doc->modelo_id . "/". $doc->idioma ."/";

                                        $path = storage_path("files/viajeros/". $ficha->viajero_id ."/");
                                        $folder = "/files/viajeros/". $ficha->viajero_id ."/";
                                    ?>

                                    <tr>
                                    <td class="col-sm-3">
                                        {{$doc->name}}
                                    </td>
                                    <td class="col-sm-6">
                                        {{$doc->notas}}
                                    </td>
                                    <td>
                                        @if($doc->doc)    
                                            {!! ConfigHelper::iframe($doc->doc, public_path($dir), $dir); !!}
                                        @else
                                        @endif 
                                    </td>
                                    
                                    
                                    <?php
                                        $doc_id = $doc->idioma ? $doc->doc_id : $doc->id;
                                        $adjunto = $booking->archivos->where('doc_especifico_id', $doc_id)->first();
                                    ?>

                                    <td>
                                        @if($adjunto)
                                            {!! ConfigHelper::iframe($adjunto->doc, $path, $folder, true); !!}
                                        @endif

                                    </td>

                                    <td class="col-sm-3">
                                        @if($adjunto)
                                            <a href="{{ route('manage.viajeros.archivo.delete', [$adjunto->id]) }}" class="btn btn-danger btn-xs" role="button">Borrar</a>

                                            <hr>
                                            @if($adjunto->doc_especifico_status)
                                                Aceptado
                                                <br>
                                                <a href="{{ route('manage.viajeros.archivo.status', [$adjunto->id, 0]) }}" class="btn btn-warning btn-xs" role="button">Denegar</a>
                                            @else
                                                Pend. validar
                                                <br>
                                                <a href="{{ route('manage.viajeros.archivo.status', [$adjunto->id, 0]) }}" class="btn btn-warning btn-xs" role="button">Denegar</a>
                                                <a href="{{ route('manage.viajeros.archivo.status', [$adjunto->id, 1]) }}" class="btn btn-success btn-xs" role="button">Validar</a>
                                            @endif
                                        @else
                                            Pendiente
                                        @endif
                                    </td>

                                    </tr>
                                @endforeach
                            </table>

                        </div>
                    </div>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="documentos">
                    <ul>
                        <li> <a target="_blank" href="{{ route('manage.generar.notapago', $ficha->id) }}">Nota de pago (ES)</a> </li>
                        <li> <a target="_blank" href="{{ route('manage.generar.notapago', [$ficha->id, 'ca']) }}">Nota de pago (CA)</a> </li>
                        <li> <a target="_blank" href="{{ route('manage.generar.booking-proveedor', [$ficha->id]) }}">Booking form proveedor</a> </li>
                        <li> <a target="_blank" href="{{ route('manage.generar.recibo', [$ficha->id]) }}">Recibo (ES)</a> </li>
                        <li> <a target="_blank" href="{{ route('manage.generar.recibo', [$ficha->id, 'ca']) }}">Recibo (CA)</a> </li>
                    </ul>
                </div>

            </div>

        </div>
    </div>
</div>

@include('includes.script_booking', ['ficha'=> $ficha])

@include('manage.bookings.incidencias.new')

<script type="text/javascript">
$(document).ready(function() {

    @if($ficha->viajero->es_cic)
        @if($ficha->viajero->datos->cic)
            $('#cic_nivel_div').show();
        @endif

        $("#cic").click(function(){
            if( $(this).is(':checked') )
            {
                $('#cic_nivel_div').show();
            }
            else
            {
                $('#cic_nivel_div').hide();
            }
        });
    @endif

    $('#btn-vuelo').click( function(e) {
        e.preventDefault();

        bootbox.confirm("¿Estás seguro que quieres cambiar los datos de vuelo?", function(result) {
            if(result)
            {
                $('#frm-vuelo').submit();
            }
        });
    });

});
</script>

@php
    $asignados = \VCN\Models\User::asignados()->get()->pluck('full_name', 'id');
@endphp
@include('manage.viajeros.logs.new', ['ficha' => $ficha->viajero, 'asignados' => $asignados, 'bookingId' => $ficha->id])

@stop