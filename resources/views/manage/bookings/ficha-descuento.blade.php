<div class="col-md-12">
    <strong>
    @include('includes.form_checkbox', [ 'campo'=> 'descuento_chk', 'texto'=> 'Crear DESCUENTO' ])
    </strong>

    <div id="booking-descuento-add" class="row" style='display:none;'>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-gift fa-fw"></i> DESCUENTO
        </div>
        <div class="panel-body">
            @include('manage.descuentos.tipos.ficha-add', ['booking_id'=> $ficha->id])
        </div>
    </div>
    </div>

</div>

{{--
<div class="col-md-12">
    <strong>
    @include('includes.form_checkbox', [ 'campo'=> 'extra', 'texto'=> 'Crear EXTRA' ])
    </strong>

    <div id="booking-extra-add" class="row" style='display:none;'>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-gift fa-fw"></i> Añadir Extra
        </div>
        <div class="panel-body">
            {!! Form::open(array('route' => array('manage.bookings.extra.add', $ficha->id))) !!}

                <div class="form-group row">
                    <div class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'extra_importe', 'texto'=> 'Importe'])
                    </div>

                    <div class="col-md-6">
                        @include('includes.form_input_text', [ 'campo'=> 'extra_notas', 'texto'=> 'Notas'])
                    </div>

                    <div class="col-md-3">
                        {!! Form::label('Añadir') !!}<br>
                        {!! Form::submit('Crear', array('class' => 'btn btn-success')) !!}
                    </div>

                </div>

            {!! Form::close() !!}
        </div>
    </div>
    </div>
</div>
--}}


<script type="text/javascript">
$(document).ready(function(){
    $('#descuento_chk').change( function(e) {
        $('#booking-descuento-add').toggle();
    });

    $('#extra').change( function(e) {
        $('#booking-extra-add').toggle();
    });
});
</script>