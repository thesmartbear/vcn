{{-- TOTAL --}}
<?php
$precio_total = $ficha->precio_total;
$descuento_name = isset($descuento)?"[".$descuento->name."]":"";
$descuento = isset($descuento)?$descuento:$precio_total['descuento_especial'];
?>
<table class="table total">
    <thead>
        <tr class="thead">
            <td colspan="2" align="center"><span>TOTAL</span></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="subtotal">Subtotal por divisas</td>
            <td class="subtotal">
                <span id="booking-subtotal">
                    @foreach($precio_total['total_subtotal'] as $subtotal)
                        {{$subtotal}}<br>
                    @endforeach
                </span>
            </td>
        </tr>

        @if($ficha->divisa_variacion && !$ficha->promo_cambio_fijo)
        <tr>
            <td>Variación Divisa</td>
            <td>{{ConfigHelper::parseMoneda($ficha->divisa_variacion)}}</td>
        </tr>
        @endif

        {{-- DESCUENTOS --}}
        @if($descuento)
        <tr>
            <td>Descuento especial {{$descuento_name}}</td><td><span id="booking-descuento_especial">{{$precio_total['descuento_especial_txt']}}</span></td>
        </tr>
        @endif

        @if($ficha->descuentos_noearly->count()>0 || ($ficha->fase==1 && $descuento) )
        <tr>
            <td>Descuentos</td>
            <td><span id="booking-descuento">{{$precio_total['descuento_txt']}}</span></td>
        </tr>
        @endif

        @if($ficha->descuento_early)
        <tr>
            <td>Descuento Early Bird</td>
            <td>{{$ficha->descuento_early_importe}} {{$ficha->descuento_early->moneda->name}}</td>
        </tr>
        @endif

        <tr>
            <td>TOTAL EN {{Session::get('vcn.moneda')}}</td>
            <td><span class="booking-total" id="booking-total">{{$precio_total['total_txt']}}</span></td>
        </tr>

        <tr>
            <td colspan='2' class="cambioaplicado">
                <em><small>Cambio aplicado: {!! implode(" - ", $ficha->monedas_usadas_txt) !!}</small></em>
            </td>
        </tr>

    </tbody>
</table>