@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.bookings.nuevo') !!}
@stop


@section('content')

<div class="modal fade" id="modalBooking-loading" style="top: 20% !important;">
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            Booking: Recalculando...
        </div>
        <div class="modal-body">

            <i class="fa fa-spinner fa-spin fa-2x"></i>

        </div>
        <div class="modal-footer">

        </div>
    </div>
</div>
</div>

<?php

$plazas = $booking->estado_plazas;
$hayPlazas = (bool) $plazas['hayPlazas'];
$plazas_alojamiento = $plazas['alojamiento'];
$plazas_vuelo = $plazas['vuelo'];

?>


<div class="modal fade" id="modalBooking-borrador" style="top: 20% !important;">
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Enviar borrador a cliente :: Escoger destinatarios
        </div>

        {!! Form::open(array('id'=> 'frm-booking-online-borrador', 'method' => 'POST', 'url' => route('manage.bookings.online', $ficha->id) , 'role' => 'form', 'class' => 'frm-1click')) !!}
            <div class="modal-body">

                <div class="form-group">
                    @include('includes.form_select_multi', ['campo'=> 'destinatarios', 'texto'=> "Seleccionar", 'select'=> $ficha->online_destinatarios])
                </div>

                <div class="form-group">
                    <i>* Ten en cuenta que se eliminará cualquier booking en borrador (no acabado) que pueda tener el tutor/viajero.</i>
                </div>
                {{-- pa{{$plazas_alojamiento}}\pv{{$plazas_vuelo}}\hp{{$hayPlazas}} --}}
                @if(!$hayPlazas)

                    {{ Form::hidden('es_overbooking', 1) }}
                    
                    <div class="note note-danger">
                        <h4 class="block">Alerta</h4>
                        <p>
                            <strong>Ovbkg: No hay plazas disponibles.</strong>
                            @if($plazas_alojamiento < 1)
                                [Alojamiento]
                            @endif
        
                            @if($plazas_vuelo < 1)
                                [Vuelo]
                            @endif
                        </p>

                        <hr>
                        Autorizado por:
                        @include('includes.form_asignados', ['campo'=> 'autorizado'])

                    </div>
                    
                @endif

            </div>
            <div class="modal-footer">
                {!! Form::submit('Enviar', array('class' => 'btn btn-success btn-block col-md-12')) !!}
            </div>

        {!! Form::close() !!}

    </div>
</div>
</div>


<div class="booking">
<div class="portlet box grey" id="myBooking">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user fa-fw"></i> Inscrito ::
            <a href="{{route('manage.viajeros.ficha',$ficha->viajero->id)}}" target="_blank" data-label="Ver Ficha">
                {{$ficha->viajero->full_name}}
            </a>
            :: {{$ficha->fecha}}
        </div>

        @if($ficha->fase<5 && ConfigHelper::canEdit('booking-borrar'))
        <div class="caption pull-right">
            <a href='#destroy' data-label='Cancelar' data-model='Inscripción' data-action='{{route( 'manage.bookings.delete', [$ficha->id])}}' data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>
        </div>
        @endif

        @if(!$ficha->es_presupuesto)
            @if($ficha->es_online)
                <div class="caption pull-right">[ONLINE :: {!! $ficha->online_creador !!}]&nbsp;</div>
            @else
                <div class="caption pull-right">
                    <a href="#" data-toggle="modal" data-target="#modalBooking-borrador" class="btn btn-info btn-xs">Enviar borrador a cliente</a> &nbsp;
                </div>
            @endif
        @endif
    </div>

    <style type="text/css" media="screen">
        .disabled{
            pointer-events:none;
        }
    </style>

    <div class="portlet-body">

        <div class="wizard">
            <div class="wizard-inner">

                @if(!$ficha->es_presupuesto)
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <?php $paso = 1; ?>
                    <li role="presentation" class="{{$booking->fase==$paso?'active':''}}">
                        <div class="label">Datos Curso</div>
                        <a class="withlabel" href="#step{{$paso}}" data-toggle="tab" aria-controls="step{{$paso}}" role="tab" title="Paso {{$paso}}">
                            <span class="round-tab"><i class="fa fa-graduation-cap"></i></span>
                        </a>
                    </li>

                    <?php $paso = 2; ?>
                    <li role="presentation" class="{{$booking->fase==$paso?'active':''}} {{$booking->fase>=$paso?'':'disabled'}}">
                        <div class="label">Datos Viajero</div>
                        <a href="#step{{$paso}}" data-toggle="tab" aria-controls="step{{$paso}}" role="tab" title="Paso {{$paso}}">
                            <span class="round-tab"><i class="fa fa-user"></i></span>
                        </a>
                    </li>

                    <?php $paso = 3; ?>
                    <li role="presentation" class="{{$booking->fase==$paso?'active':''}} {{$booking->fase>=$paso?'':'disabled'}}">
                        <div class="label">Datos Tutores</div>
                        <a href="#step{{$paso}}" data-toggle="tab" aria-controls="step{{$paso}}" role="tab" title="Paso {{$paso}}">
                            <span class="round-tab"><i class="fa fa-users"></i></span>
                        </a>
                    </li>

                    <?php $paso = 4; ?>
                    <li role="presentation" class="{{$booking->fase==$paso?'active':''}} {{$booking->fase>=$paso?'':'disabled'}}">
                        <div class="label">Confirmación y Pago</div>
                        <a href="#step{{$paso}}" data-toggle="tab" aria-controls="step{{$paso}}" role="tab" title="Paso {{$paso}}">
                            <span class="round-tab"><i class="fa fa-credit-card"></i></span>
                        </a>
                    </li>

                </ul>
                @else
                    <h1>Presupuesto</h1>
                    <hr>
                @endif

            </div>

            <div class="tab-content col-md-12">

                <?php $paso = 1; ?>
                <div class="tab-pane {{$booking->fase==$paso?'active':''}}" role="tabpanel" id="step{{$paso}}">

                    @include('manage.bookings.booking.header')
                    <br>

                    {!! Form::open(array('id'=> 'frm-booking-curso', 'method' => 'POST', 'url' => route('manage.bookings.crear.post', $ficha->id) , 'role' => 'form', 'class' => '')) !!}

                        {!! Form::hidden('fase',$paso) !!}

                        {!! Form::hidden('booking-id',$booking->id) !!}
                        {!! Form::hidden('booking-ccerrada_id',$booking->convocatory_close_id) !!}
                        {!! Form::hidden('booking-cabierta_id',$booking->convocatory_open_id) !!}
                        {!! Form::hidden('booking-cmulti_id',$booking->convocatory_multi_id) !!}
                        {!! Form::hidden('booking-alojamiento_id',$booking->accommodation_id) !!}
                        {!! Form::hidden('booking-vuelo_id',$booking->vuelo_id) !!}


                        @if($booking->descuento)
                            <div class="descuento col-md-12">Descuento especial de {{ $booking->descuento->name }}</div>
                            <hr>
                        @endif

                        @include("manage.bookings.booking.paso$paso", ['ficha'=> $booking])
                        <hr>

                        @include('manage.bookings.booking.total', ['ficha'=> $booking])

                        <hr>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                    Prescriptor:
                                </div>
                                <div class="col-md-3">
                                    @include('includes.form_select2', ['campo'=> 'prescriptor_id', 'texto'=> null, 'select'=> $prescriptores, 'valor'=> ($ficha->prescriptor_id ?: $ficha->viajero->prescriptor_id)])
                                </div>
                            </div>
                        </div>

                        @if($ficha->descuentos->count()>0)

                            {!! Datatable::table()
                                ->addColumn([
                                  'fecha'        => 'Fecha',
                                  'usuario'        => 'Creado',
                                  'importe'        => 'Importe',
                                  'moneda'        => 'Moneda',
                                  'notas'       => 'Notas',
                                  'options'     => ''
                                ])
                                ->setUrl( route('manage.bookings.descuentos.index', $ficha->id) )
                                ->setOptions(
                                  "aoColumnDefs", array(
                                    [ "bSortable" => false, "aTargets" => [5] ]
                                  )
                                )
                                ->render() !!}

                        @endif

                        <div class="row margintop60">
                            <div class="col-md-2">
                                @if($ficha->fase<5 && ConfigHelper::canEdit('booking-borrar'))
                                    <a href="{{route( 'manage.bookings.delete', [$ficha->id])}}" class="btn btn-danger">Cancelar</a>
                                @endif
                            </div>
                            <div class="col-md-10 pull-right">
                                @if($ficha->es_presupuesto)
                                    {!! Form::submit('Crear presupuesto', array('id'=> 'btn-next-curso', 'class' => 'btn btn-warning btn-block col-md-12')) !!}
                                @else
                                    {!! Form::submit('Siguiente', array('id'=> 'btn-next-curso', 'class' => 'btn btn-success btn-block col-md-12')) !!}
                                @endif
                            </div>
                        </div>

                    {!! Form::close() !!}

                    <hr>
                    <div class="row">
                        @include('manage.bookings.booking.descuento')
                    </div>
                    <hr>

                </div>


                <?php $paso = 2; ?>
                @if($booking->fase>=$paso)
                <div class="tab-pane {{$booking->fase==$paso?'active':''}}" role="tabpanel" id="step{{$paso}}">
                    <h3 class="booking-title notable">Datos del Viajero</h3>

                    {!! Form::open(array('id'=> 'frm-booking-curso', 'method' => 'POST', 'url' => route('manage.bookings.crear.post', $ficha->id) , 'role' => 'form', 'class' => '')) !!}
                        {!! Form::hidden('fase',$paso) !!}

                        {!! Form::hidden('booking-id',$booking->id) !!}
                        {!! Form::hidden('booking-ccerrada_id',$booking->convocatory_close_id) !!}
                        {!! Form::hidden('booking-cabierta_id',$booking->convocatory_open_id) !!}
                        {!! Form::hidden('booking-cmulti_id',$booking->convocatory_multi_id) !!}
                        {!! Form::hidden('booking-alojamiento_id',$booking->accommodation_id) !!}

                        @include("manage.bookings.booking.paso$paso", ['ficha'=> $booking])

                        <div class="row margintop60 marginbottom60">
                            <div class="col-md-2">
                                <a href="{{route( 'manage.bookings.delete', [$ficha->id])}}" class="btn btn-danger">Cancelar</a>
                            </div>
                            <div class="col-md-10 pull-right">
                                {!! Form::submit('Siguiente', array('id'=> 'btn-next-curso', 'class' => 'btn btn-success btn-block col-md-12')) !!}
                            </div>
                        </div>

                    {!! Form::close() !!}

                </div>
                @endif

                <?php $paso = 3; ?>
                @if($booking->fase>=$paso)
                <div class="tab-pane {{$booking->fase==$paso?'active':''}}" role="tabpanel" id="step{{$paso}}">
                    <h3 class="booking-title notable">Datos Tutores</h3>

                    {!! Form::open(array('id'=> 'frm-booking-curso', 'method' => 'POST', 'url' => route('manage.bookings.crear.post', $ficha->id) , 'role' => 'form', 'class' => 'frmValidar3')) !!}
                        {!! Form::hidden('fase',$paso) !!}

                        {!! Form::hidden('booking-id',$booking->id) !!}
                        {!! Form::hidden('booking-ccerrada_id',$booking->convocatory_close_id) !!}
                        {!! Form::hidden('booking-cabierta_id',$booking->convocatory_open_id) !!}
                        {!! Form::hidden('booking-cmulti_id',$booking->convocatory_multi_id) !!}
                        {!! Form::hidden('booking-alojamiento_id',$booking->accommodation_id) !!}

                        @include("manage.bookings.booking.paso$paso", ['ficha'=> $booking])

                        <div class="row margintop60 marginbottom60">
                            <div class="col-md-2">
                                <a href="{{route( 'manage.bookings.delete', [$ficha->id])}}" class="btn btn-danger">Cancelar</a>
                            </div>
                            <div class="col-md-10 pull-right">
                                {!! Form::submit('Siguiente', array('id'=> 'btn-next-curso', 'class' => 'btn btn-success btn-block col-md-12')) !!}
                                
                                @if( Session::get('vcn.booking-nif') )
                                    {!! trans('area.nota_nif_no') !!}
                                    <script>
                                    $(document).ready(function(){

                                        bootbox.confirm({
                                            title: "NOTA",
                                            message: "{!! trans('area.nota_nif') !!}",
                                            buttons: {
                                                confirm: {
                                                label: "{{trans('area.nota_nif_seguir')}}",
                                                className: 'btn-warning'
                                                },
                                                cancel: {
                                                label: "{{trans('area.nota_nif_volver')}}",
                                                className: 'btn-success'
                                                }
                                            },
                                            callback: function (result) {
                                                if(result)
                                                {
                                                    $('#booking-nif').prop('checked', true);
                                                    $('#frmValidar3').submit();
                                                }
                                            }
                                        });
                                    });
                                    </script>
                                @endif

                            </div>
                        </div>


                    {!! Form::close() !!}

                </div>
                @endif

                <?php $paso = 4; ?>
                @if($booking->fase>=$paso)
                <div class="tab-pane {{$booking->fase==$paso?'active':''}}" role="tabpanel" id="step{{$paso}}">
                    <h3 class="booking-title notable">Resumen</h3>

                    @include("manage.bookings.booking.paso$paso", ['ficha'=> $booking])

                    {{-- {!! Form::open(array('id'=> 'frm-booking-curso', 'method' => 'POST', 'url' => route('manage.bookings.crear.post', $ficha->id) , 'role' => 'form', 'class' => 'frmValidar3')) !!}
                        {!! Form::hidden('fase',$paso) !!}

                        {!! Form::hidden('booking-id',$booking->id) !!}
                        {!! Form::hidden('booking-ccerrada_id',$booking->convocatory_close_id) !!}
                        {!! Form::hidden('booking-cabierta_id',$booking->convocatory_open_id) !!}
                        {!! Form::hidden('booking-cmulti_id',$booking->convocatory_multi_id) !!}
                        {!! Form::hidden('booking-alojamiento_id',$booking->accommodation_id) !!}

                        @include("manage.bookings.booking.paso$paso", ['ficha'=> $booking])

                        <div class="row margintop60 marginbottom60">
                            <div class="col-md-2">
                                <a href="{{route( 'manage.bookings.delete', [$ficha->id])}}" class="btn btn-danger">Cancelar</a>
                            </div>
                            <div class="col-md-10 pull-right">
                                {!! Form::submit('Siguiente', array('id'=> 'btn-next-curso', 'class' => 'btn btn-success btn-block col-md-12')) !!}
                            </div>
                        </div>

                    {!! Form::close() !!} --}}

                </div>
                @endif

            </div>

        </div>

    </div>

</div>
</div>

@include('includes.script_booking', ['ficha'=> $booking])

<script type="text/javascript">

function pagoValidar()
{
    var ret = false;

    var $importe = $('#importe').val();
    var $tipo = $('#tipo').val();

    if($importe>0 && $tipo>0)
    {
        ret = true;
    }
    else
    {
        if($tipo<=0)
        {
            $("button[data-id='tipo']").css('border-color','red');
        }

        if($importe<=0)
        {
            $('#importe').css('border-color','red');
        }
    }

    return ret;
}

$(document).ready(function() {

    var $form = $('#fBooking');

    $('#siguiente2').click(function(e){
        e.preventDefault();

        var $pv = $('#plazas_vuelo').val();
        var $pa = $('#plazas_alojamiento').val();

        // if($pv>0 && $pa>0)
        {
            $('#booking-pago').show();
        }

        $('#modalBooking').modal('show');

    });

    $('#siguiente3').click(function(e){
        e.preventDefault();

        $('#modalBooking').modal('show');
    });

    $('#btn-pago').click(function(e){
        e.preventDefault();

        $fecha = $('#pfecha').val();
        if(!validaFechaPago($fecha))
        {
            var pregunta = "¿Es correcta la fecha "+ $fecha +" ?";
            bootbox.confirm(pregunta, function(result) {
                if(result)
                {
                    form_submit();
                }
            });

        }
        else
        {
            form_submit();
        }
    });

    function form_submit()
    {
        $('#reserva-tipo').val(1);

        $('#pago-importe').val($('#importe').val());
        $('#pago-moneda').val($('#moneda').val());
        $('#pago-rate').val($('#rate').val());
        $('#pago-tipo').val($('#tipo').val());
        $('#pago-fecha').val($('#pfecha').val());

        $('#mail_confirmacion').val($('#mail-confirmacion').is(':checked')?1:0);
        $('#area').val($('#booking-area').is(':checked')?1:0);
        $('#area_pagos').val($('#booking-area-pagos').is(':checked')?1:0);
        $('#area_reunion').val($('#booking-area-reunion').is(':checked')?1:0);

        if(pagoValidar())
        {
            $('#btn-pago').hide();
            $form.submit();
        }
    }

    $('#btn-cancelar').click(function(e){
        e.preventDefault();

        $('#reserva-tipo').val(2);

        $('#pago-importe').val(0);
        $('#pago-tipo').val(0);

        $form.submit();
    });

    $('#btn-pago-overbooking').click(function(e){
        e.preventDefault();

        $('#reserva-tipo').val(3);

        $('#pago-importe').val($('#importe').val());
        $('#pago-moneda').val($('#moneda').val());
        $('#pago-rate').val($('#rate').val());
        $('#pago-tipo').val($('#tipo').val());
        $('#pago-ovbkg').val($('#ovbkg_id').val());

        $('#mail_confirmacion').val($('#mail-confirmacion').is(':checked')?1:0);
        $('#area').val($('#booking-area').is(':checked')?1:0);
        $('#area_pagos').val($('#booking-area-pagos').is(':checked')?1:0);
        $('#area_reunion').val($('#booking-area-reunion').is(':checked')?1:0);

        if(pagoValidar())
        {
            $form.submit();
        }
    });

    $('.dt-buttons').hide();

    $('.from-actions').css({'margin-top':'60px'});

    @if( $ficha->curso->es_convocatoria_multi )

        $('#btn-next-curso').click( function(e) {
            e.preventDefault();

            var $iEsp = 0;
            $('.booking-multi-especialidad option:selected').each(function() {
                if($(this).val() == 0)
                {
                    $iEsp++;
                }
            });

            if( $iEsp > 0 )
            {
                var iEsp = $('.booking-multi-especialidad option[value="0"]').length;
                bootbox.alert('Revise las Especialidades. Hay '+iEsp+' sin seleccionar.');
            }
            else
            {
                $('#frm-booking-curso').unbind("submit").submit();
            }

        });

    @else

        $('#btn-next-curso').on('click', function(){
            $('#frm-booking-curso').submit();
        });

    @endif
});
</script>

<script type="text/javascript">
$(document).ready( function() {

    var $cambios = <?php echo json_encode($monedas_cambio); ?>

    $('#moneda').change(function(e) {

        var $id = $(this).val();

        $('#rate').val($cambios[$id]);

    });

});
</script>

@stop