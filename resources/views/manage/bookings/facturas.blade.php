{{-- <div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-database fa-fw"></i> Datos Facturación (Viajero) <i>Sólo a modo informativo, PARA LA FACTURA HAY QUE RELLENAR EL BLOQUE SIGUIENTE</i>
    </div>
    <div class="panel-body">

        {!! Form::model($ficha->viajero->datos, array('id'=>'formulario', 'route' => array('manage.viajeros.ficha.datos', $ficha->viajero->id))) !!}

            {!! Form::hidden('facturacion',true) !!}
            <div class="form-group row">
                <div class="col-md-3">
                    @include('includes.form_input_text', [ 'campo'=> 'fact_nif', 'texto'=> 'NIF'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'fact_razonsocial', 'texto'=> 'Razón Social'])
                </div>
                <div class="col-md-5">
                    @include('includes.form_input_text', [ 'campo'=> 'fact_domicilio', 'texto'=> 'Domicilio'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'fact_cp', 'texto'=> 'CP'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'fact_poblacion', 'texto'=> 'Población'])
                </div>
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'fact_concepto', 'texto'=> 'Concepto'])
                </div>
            </div>

            @include('includes.form_submit', [ 'permiso'=> 'viajeros', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div> --}}

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-database fa-fw"></i> Datos Facturación (Booking) <i>A rellenar con la información que tiene que aparecer en la factura</i>
    </div>
    <div class="panel-body">

        {!! Form::model($ficha->datos, array('id'=>'formulario', 'route' => array('manage.bookings.facturas.datos', $ficha->id))) !!}

            <div class="form-group row">
                <div class="col-md-3">
                    @include('includes.form_input_text', [ 'campo'=> 'fact_nif', 'texto'=> 'NIF'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'fact_razonsocial', 'texto'=> 'Razón Social'])
                </div>
                <div class="col-md-5">
                    @include('includes.form_input_text', [ 'campo'=> 'fact_domicilio', 'texto'=> 'Domicilio'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'fact_cp', 'texto'=> 'CP'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'fact_poblacion', 'texto'=> 'Población'])
                </div>
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'fact_concepto', 'texto'=> 'Concepto'])
                </div>
            </div>

            @include('includes.form_submit', [ 'permiso'=> 'bookings', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

@if( ConfigHelper::config('facturas') && ConfigHelper::canEdit('facturas')  )  {{-- && $ficha->pagado --}}
<div class="row facturas">
    <div class="col-md-12">
        <div class="mt-element-list margin-top-20">

            @if($ficha->factura)
                @if($ficha->factura->grup )
                    <div class="note note-warning">
                        <h4 class="block">Booking facturado</h4>
                        <p>Booking ya facturado como parte de una factura agrupando varios bookings.</p>
                    </div>
                @endif
            @endif


            @if(!$ficha->factura || $ficha->es_facturable)
                <div class="mt-list-head list-simple bg-grey">
                    <div class="list-head-title-container">
                        <h5 class="list-title sbold"><i class="fa fa-book"></i> Facturar
                            @if($ficha->no_facturar==0)
                                (Facturación sistema <i>Emitir factura desde la convocatoria de grupo o desde el propio booking para individuales.</i>)
                            @elseif($ficha->no_facturar==1)
                                (No emitir factura por el sistema)
                                @if($ficha->prescriptor_no_facturar)
                                    [Prescriptor]
                                @endif
                            @elseif($ficha->no_facturar)
                                (Facturación parcial)
                            @endif
                        </h5>
                    </div>
                </div>

                <div class="mt-list-container list-simple ">
                    <div class="mt-list-title uppercase" style="padding: 10px 0;">Seleccione una opción:
                    </div>
                    <ul>
                        <li class="mt-list-item">
                            <div class="list-icon-container {{$ficha->no_facturar==0?'done':''}}">
                                <a href="{{ route('manage.bookings.facturar.opcion', [$ficha->id, 0])}}">
                                    <i class="{{$ficha->no_facturar==0?'icon-check':'icon-close'}}"></i>
                                </a>
                            </div>
                            <div class="list-datetime"></div>
                            <div class="list-item-content">
                                <h3>
                                    <a href="{{ route('manage.bookings.facturar.opcion', [$ficha->id, 0])}}">Facturación sistema <i>(Emitir factura desde la convocatoria de grupo o desde el propio booking para individuales.)</i></a>
                                </h3>
                            </div>
                        </li>
                        <li class="mt-list-item">
                            <div class="list-icon-container {{$ficha->no_facturar==1?'done':''}}">
                                <a href="{{ route('manage.bookings.facturar.opcion', [$ficha->id, 1])}}">
                                    <i class="{{$ficha->no_facturar==1?'icon-check':'icon-close'}}"></i>
                                </a>
                            </div>
                            <div class="list-datetime"></div>
                            <div class="list-item-content">
                                <h3>
                                    <a href="{{ route('manage.bookings.facturar.opcion', [$ficha->id, 1])}}">No emitir factura por el sistema.</a>
                                </h3>
                            </div>
                        </li>
                        <li class="mt-list-item">
                            <div class="list-icon-container {{$ficha->no_facturar==2?'done':''}}">
                                <a href="{{ route('manage.bookings.facturar.opcion', [$ficha->id, 2])}}">
                                    <i class="{{$ficha->no_facturar==2?'icon-check':'icon-close'}}"></i>
                                </a>
                            </div>
                            <div class="list-datetime"></div>
                            <div class="list-item-content">
                                <h3>
                                    <a href="{{ route('manage.bookings.facturar.opcion', [$ficha->id, 2])}}">Facturar sólo una parte por el sistema y asignar nº de factura(s) manual(es) para el resto.</a>
                                </h3>
                            </div>
                        </li>
                    </ul>
                </div>
            @endif


        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">

        <?php
            $facturable = $ficha->es_facturable == 0;
            $factura = $ficha->factura_last;

            $bFacturado = false;
            if(!$facturable && $factura && $factura->enviada)
            {
                if(!$ficha->es_facturable)
                {
                    $bFacturado = true;
                }
            }
        ?>

        @if($bFacturado)
            -- Booking Facturado --

        @else

            @if($ficha->no_facturar==0) {{-- facturación sistema --}}

                <div class="col-md-2">
                    @if( $ficha->es_facturable )
                        <a data-label='Emitir factura' href='{{ route('manage.bookings.facturar',['Booking',$ficha->id]) }}' class='btn btn-success btn-xs'><i class='fa fa-ticket'></i> Emitir Factura</a>
                    {{-- @else
                        @if(!$ficha->factura->grup )
                            <a id="btn-factura" data-label='Emitir Abono y Factura nueva' href='#' data-ruta='{{ route('manage.bookings.facturar',['Booking',$ficha->id,1]) }}' class='btn btn-danger btn-xs'><i class='fa fa-ticket'></i> Emitir Abono y Factura nueva</a>
                        @endif --}}
                    @else
                        :: Booking Facturado ::
                    @endif
                </div>

            @elseif($ficha->no_facturar==1) {{-- no emitir factura por el sistema --}}

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-ticket"></i> Factura manual (No emitir factura por el sistema)
                    </div>
                    <div class="panel-body">

                        {!! Form::open( array('method'=> "POST", 'route' => array('manage.bookings.facturas',$ficha->id) )) !!}
                            {{-- <a id="facturas_add" href="#facturas_add" class='btn btn-xs btn-warning' data-label="Añadir"><i class="fa fa-plus-square"></i></a> --}}

                            <div id="facturas_div" class="form-group">
                                <div class="facturas_form row">
                                    <div class="col-md-1">
                                        @include('includes.form_input_text', [ 'campo'=> "facturas[numero]", 'texto'=> 'Número'])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_input_datetime', [ 'campo'=> "facturas[fecha]", 'texto'=> 'Fecha' ])
                                    </div>
                                    <div class="col-md-1">
                                        @include('includes.form_input_text', [ 'campo'=> "facturas[cp]", 'texto'=> 'CP' ])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_input_text', [ 'campo'=> "facturas[importe]", 'texto'=> 'Importe', 'help'=> "En blanco: Total"])
                                    </div>
                                    <div class="col-md-4">
                                        @include('includes.form_input_text', [ 'campo'=> "facturas[razonsocial]", 'texto'=> 'Razón Social', 'help'=> "En blanco: del Booking"])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_input_text', [ 'campo'=> "facturas[cif]", 'texto'=> 'CIF', 'help'=> "En blanco: del Booking"])
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::submit('Guardar', array('class'=> 'btn btn-success')) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}

                    </div>
                </div>

            @elseif($ficha->no_facturar==2) {{-- facturar parte --}}

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-ticket"></i> Factura parcial :: [Total: {{ConfigHelper::parseMoneda($ficha->total)}}, quedan {{ConfigHelper::parseMoneda($ficha->facturable)}} pendientes de facturar]
                    </div>
                    <div class="panel-body">

                        @if($ficha->es_facturable)
                            {!! Form::open( array('method'=> "POST", 'route' => array('manage.bookings.facturar.parcial',$ficha->id) )) !!}

                            <div class="row">
                                <div class="col-md-4">
                                    @include('includes.form_input_text', [ 'campo'=> 'valor', 'texto'=> 'Valor', 'help'=> "En blanco: importe pendiente de facturar (".ConfigHelper::parseMoneda($ficha->facturable) .")"])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'select'=> [0=>'Importe',1=>'Porcentaje'], 'valor'=> 0 ])
                                </div>
                                <div class="col-md-4">
                                    <label>&nbsp;</label><br>
                                    {!! Form::submit('Generar', array('class'=> 'btn btn-success')) !!}
                                </div>
                            </div>

                            {!! Form::close() !!}
                        @else
                            <strong>No queda importe pendiente de facturar.</strong>
                        @endif

                    </div>
                </div>

                <hr>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-ticket"></i> Factura manual (Facturar sólo una parte por el sistema y asignar nº de factura(s) manual(es) para el resto):
                    </div>
                    <div class="panel-body">
                        {!! Form::open( array('method'=> "POST", 'route' => array('manage.bookings.facturas',$ficha->id) )) !!}

                            <div id="facturas_div" class="form-group">
                                <div class="facturas_form row">
                                    <div class="col-md-1">
                                        @include('includes.form_input_text', [ 'campo'=> "facturas[numero]", 'texto'=> 'Número'])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_input_datetime', [ 'campo'=> "facturas[fecha]", 'texto'=> 'Fecha' ])
                                    </div>
                                    <div class="col-md-1">
                                        @include('includes.form_input_text', [ 'campo'=> "facturas[cp]", 'texto'=> 'CP'])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_input_text', [ 'campo'=> "facturas[importe]", 'texto'=> 'Importe', 'help'=> "En blanco: Total"])
                                    </div>
                                    <div class="col-md-4">
                                        @include('includes.form_input_text', [ 'campo'=> "facturas[razonsocial]", 'texto'=> 'Razón Social', 'help'=> "En blanco: del Booking"])
                                    </div>
                                    <div class="col-md-2">
                                        @include('includes.form_input_text', [ 'campo'=> "facturas[cif]", 'texto'=> 'CIF', 'help'=> "En blanco: del Booking"])
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    {!! Form::submit('Guardar', array('class'=> 'btn btn-success')) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>

            @endif

        {{-- @endif --}}
        @endif

    </div>
</div>

<hr>



{{-- @include('includes.script_form_add', ['campo'=> 'facturas', 'nomulti'=>1 ]) --}}

@endif

<div class="panel panel-default margin-top-30">
    <div class="panel-heading">
        <i class="fa fa-ticket"></i> Facturas
    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
              'fecha'   => 'Fecha',
              'numero'  => 'Numero',
              'booking_id'  => 'Booking',
              'viajero' => 'Viajero',
              'total'   => 'Total',
              'grup'    => 'Agrupada',
              'manual'  => 'Manual',
              'pdf'     => 'Ver',
              'options' => ''

            ])
            ->setUrl( route('manage.bookings.facturas.index', [$booking_id]) )
            ->setOptions('iDisplayLength', 100)
            ->setOptions(
              "columnDefs", array(
                [ "sortable" => false, "targets" => [7,8] ],
                [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
              )
            )
            ->render() !!}

            <script type="text/javascript">
            $(document).ready(function() {

                $('table.dataTable').on('click', '.btn-abonar-grup',function(e)
                {
                    e.preventDefault();

                    var ruta = $(this).data('ruta');
                    bootbox.confirm({

                        message: "Es una factura agrupada. ¿Está seguro de abonar la totalidad de la factura agrupada?",
                        buttons: {
                            confirm: {
                                label: 'Abonar',
                                className: 'btn-danger'
                            },
                        },
                        callback: function(result) {
                            if(result)
                            {
                                window.location = ruta;
                            }
                        }
                    });

                });

            });
            </script>

    </div>

</div>