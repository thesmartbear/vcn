<div class="modal fade" id="modalTutores">
<div class="modal-dialog modal-md">
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modalDestroy-Label">Tutores</h4>
    </div>
    <div class="modal-body">

        <div class="panel panel-default">
            <div class="panel-heading">Nuevo Tutor</div>

            <div class="panel-body">

            {!! Form::open(array('id'=> 'formulario-tutor','method' => 'POST', 'url' => route('manage.tutores.ficha',0), 'role' => 'form', 'class' => '')) !!}

                {!! Form::hidden('viajero_id',$viajero_id) !!}
                {!! Form::hidden('booking_id',$booking_id) !!}

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'relacion', 'texto'=> 'Relación', 'valor'=> 0,
                        'select'=> ConfigHelper::getTutorRelacion()])
                </div>

                <div class="form-group">
                    <div class="col-md-6">
                        @include('includes.form_input_text', [ 'campo'=> 'tutor_name', 'valor'=>'', 'texto'=> 'Nombre'])
                    </div>
                    <div class="col-md-6">
                        @include('includes.form_input_text', [ 'campo'=> 'tutor_lastname', 'valor'=>'', 'texto'=> 'Apellidos'])
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-5">
                        @include('includes.form_input_text', [ 'campo'=> 'tutor_email', 'valor'=>'', 'texto'=> 'E-mail'])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_select', [ 'campo'=> 'tutor_tipodoc', 'texto'=> "Tipo", 'select'=> ConfigHelper::getTipoDoc() ])
                    </div>
                    <div class="col-md-5">
                        @include('includes.form_input_text', [ 'campo'=> 'tutor_nif', 'valor'=>'', 'texto'=> 'DNI'])
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6">
                        @include('includes.form_input_text', [ 'campo'=> 'tutor_phone', 'valor'=>'', 'texto'=> 'Teléfono'])
                    </div>
                    <div class="col-md-6">
                        @include('includes.form_input_text', [ 'campo'=> 'tutor_movil', 'valor'=>'', 'texto'=> 'Movil'])
                    </div>
                </div>

                <div class="form-group pull-right">
                    {!! Form::submit('Añadir', array('name'=> 'booking', 'class' => 'btn btn-success')) !!}
                </div>

            {!! Form::close() !!}
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Importar Tutor</div>

            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.tutores.ficha.asignar',$viajero_id), 'role' => 'form', 'id' => 'frmValidar')) !!}

                    {!! Form::hidden('booking_id',$booking_id) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'tutor1', 'texto'=> 'Tutor','valor'=>''])
                        {!! Form::hidden('tutor_id',null, array('id'=>'tutor1_id')) !!}
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'relacion', 'texto'=> 'Relación', 'valor'=> 1, 'select'=> ConfigHelper::getTutorRelacion()])
                    </div>

                    <div class="form-group pull-right">
                        {!! Form::submit('Importar', array('name'=> 'booking', 'class' => 'btn btn-success')) !!}
                    </div>

                {!! Form::close() !!}

            </div>
        </div>


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
    </div>
</div>
</div>
</div>
