<!DOCTYPE html>
<html lang="es">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Factura {{$factura->numero}}</title>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>

        <?php
            $p = $ficha->plataforma ?: 1;
        ?>

        {!! Html::style('//'.ConfigHelper::config('web',$p).'/assets/css/pdf.css') !!}
        {!! Html::style('//'.ConfigHelper::config('web',$p).'/assets/css/bootstrap.css') !!}
        {!! Html::style('//'.ConfigHelper::config('web',$p).'/assets/css/recibo.css') !!}


        <style>
            caption{
                @if(ConfigHelper::config('sufijo',$p) == 'bs')
                    color: #f1c40f;
                @elseif(ConfigHelper::config('sufijo',$p) == 'cic')
                    color: #3B6990;
                @elseif(ConfigHelper::config('sufijo',$p) == 'sf')
                    color: #24aab6;
                @else
                    color: #000000;
                @endif
            }
            h1{
                @if(ConfigHelper::config('sufijo',$p) == 'bs')
                    border-bottom: 1px solid #f1c40f;
                @elseif(ConfigHelper::config('sufijo',$p) == 'cic')
                    border-bottom: 1px solid #3B6990;
                @elseif(ConfigHelper::config('sufijo',$p) == 'sf')
                    border-bottom: 1px solid #24aab6;
                @else
                   border-bottom: 1px solid #CCCCCC;
                @endif
            }


        </style>

    </head>
    <body>
    <div class="page">
        <div class="row">
            <div class="col-xs-12"><img style="width: 4.5cm; height: auto; margin-top: 4px;" class="pull-right" src="https://{{ConfigHelper::config('web',$p)}}/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb',$p)}}" /></div>
        </div>


        <div class="direccion">
            @section('direccion')
                @if($ficha->oficina)
                    <strong>{{$ficha->oficina->name}}</strong>
                    <br>
                    {{$ficha->oficina->direccion}}
                    <br>
                    {{$ficha->oficina->cp}} {{$ficha->oficina->poblacion}}
                    @if($ficha->oficina->provincia)
                        ({{$ficha->oficina->provincia->name}})
                    @endif
                    <br>
                    {{$ficha->oficina->telefono}}
                    <br>
                    {{$ficha->oficina->email}}
                @endif
            @show
        </div>

        <div class="row" style="margin-top: 3cm;">
            <div class="col-xs-12"><h1>FACTURA</h1></div>
        </div>

        <div class="row">
            <div class="col-xs-8"><strong>Nº Factura: {{$factura->numero}}</strong></div>
            <div class="col-xs-4"><strong>Fecha: {{$factura->fecha->format('d/m/Y')}}</strong></div>
        </div>

        <div class="row">
            <div class="col-xs-4">
                @if($ficha->contable_code)
                    Código contable: {{$ficha->contable_code}}
                @endif
            </div>
        </div>


        <div class="row" style="margin-top: 1cm;">
            <div class="col-xs-12">

                <table class="table">
                    <thead>
                        <tr class="thead">
                            <td class="col-md-3">Razón Social</td>
                            <td class="col-md-6">Dirección</td>
                            <td>CIF/NIF</td>
                        </tr>
                    </thead>
                    <tbody>

                        @if(!$factura->datos)
                            <tr>
                                <td>{{$ficha->datos->fact_razonsocial?$ficha->datos->fact_razonsocial:$ficha->datos->full_name}}</td>
                                <td>
                                    {{$ficha->datos->fact_domicilio?$ficha->datos->fact_domicilio:$ficha->datos->direccion}},
                                    {{$ficha->datos->fact_cp?$ficha->datos->fact_cp:$ficha->datos->cp}} {{$ficha->datos->fact_ciudad?$ficha->datos->fact_ciudad:$ficha->datos->ciudad}}
                                </td>
                                <td>{{$ficha->datos->fact_nif?$ficha->datos->fact_nif:$ficha->datos->documento}}</td>
                            </tr>
                        @else
                            <tr>
                                <td>{{$factura->datos['razonsocial']}}</td>
                                <td>{{$factura->datos['direccion']}}</td>
                                <td>{{$factura->datos['cif']}}</td>
                            </tr>
                        @endif

                    </tbody>
                </table>

                <table class="table">
                    <caption>Concepto</caption>
                    <thead>
                        <tr class="thead">
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!$factura->datos)
                            <tr>
                                <td>{{$ficha->datos->fact_concepto?$ficha->datos->fact_concepto:$ficha->curso->name}}</td>
                                <td align='right'>{{$ficha->datos->fact_concepto?"":ConfigHelper::parseMoneda($ficha->course_total_amount, $ficha->curso_moneda)}}</td>
                            </tr>
                        @else
                            <tr>
                                <td>{{$factura->datos['concepto1']}}</td>
                                <td align='right'>{{$factura->datos['concepto2']}}</td>
                            </tr>
                        @endif

                        @if($factura->parcial)
                        <tr>
                            <td colspan="2">Cantidad facturada:
                                @if($factura->parcial_porcentaje)
                                    {{$factura->parcial_porcentaje}}%
                                @else
                                    {{ConfigHelper::parseMoneda($factura->parcial_importe)}}
                                @endif
                            </td>
                        </tr>
                        @endif

                    </tbody>
                </table>

                {{-- TOTAL --}}
                <table class="table total">
                    <tr class="thead">
                        <td colspan="2" align="center"><strong>TOTAL</strong></td>
                    </tr>

                    <tr>
                        <td>SubTotal</td>
                        <td>
                            <span id="booking-subtotal-resumen">

                                @if(!$factura->precio_total)

                                    @foreach($ficha->precio_total['subtotal_txt'] as $subtotal)
                                        {{$subtotal}}<br>
                                    @endforeach

                                @else

                                    @foreach($factura->precio_total['subtotal_txt'] as $subtotal)
                                        {{$subtotal}}<br>
                                    @endforeach

                                @endif

                            </span>
                        </td>
                    </tr>

                    @if($factura->divisa_variacion && !$factura->booking->promo_cambio_fijo)
                    <tr>
                        <td>Variación Divisa</td>
                        <td>{{ConfigHelper::parseMoneda($factura->divisa_variacion)}}</td>
                    </tr>
                    @endif

                    {{-- DESCUENTOS --}}
                    @if($factura->precio_total['descuento_especial'] > 0)
                        <tr>
                            <td>Descuento especial</td>
                            <td>{{$factura->precio_total['descuento_especial_txt']}}</td>
                        </tr>
                    @endif

                    @if($ficha->descuentos && $ficha->descuentos->count()>0)
                        <tr>
                            <td>Descuentos</td>
                            <td>{{$factura->precio_total['descuento_txt']}}</td>
                        </tr>
                    @endif

                    <tr>
                        <td>TOTAL EN {{Session::get('vcn.moneda')}}</td>
                        @if($factura->parcial)
                            <td><span class="booking-total" id="booking-total-resumen">{{$factura->parcial_importe}}</span></td>
                        @else
                            <td><span class="booking-total" id="booking-total-resumen">{{$factura->total_divisa_txt}}</span></td>
                        @endif
                    </tr>

                    <tr>
                        <td colspan="2">{{$info->factura_iva_pie}}</td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            Cambio aplicado:<br>
                            @if(!$factura->monedas)
                                @foreach($ficha->monedas_usadas_txt as $mu)
                                    <small>{{$mu}}</small><br>
                                @endforeach
                            @else
                                {!! $factura->monedas !!}
                            @endif
                        </td>
                    </tr>

                </table>

            </div>
        </div>

    </div>
    </body>
</html>