<div class="row">
    <div class="col-md-12">


        @if($ficha->familias)
            <table class="table table-bordered table-hover">
                <caption>Familias</caption>
                <thead>
                    <tr>
                        <th>Familia</th>
                        <th class='col-md-2'>Desde</th>
                        <th class='col-md-2'>Hasta</th>
                        <th class='col-md-1'>Área Cliente</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ficha->familias as $fam)

                        {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.familias', [$ficha->id,$fam->id]) , 'role' => 'form', 'class' => '')) !!}

                        {!! Form::hidden('familia_id',$fam->id) !!}
                        <tr>
                            <td>
                                <a href="{{route('manage.centros.familias.ficha',$fam->familia->id)}}">{{$fam->familia->name}}</a>
                            </td>
                            <td>
                                @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=> old('desde',$fam->desde->format('d/m/Y')) ])
                            </td>
                            <td>
                                @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=> old('desde',$fam->hasta->format('d/m/Y')) ])
                            </td>
                            <td>
                                @if($fam->area)
                                    <a data-label='Desactivar área' href='{{ route('manage.bookings.familias.activo',[$fam->id]) }}' class='label label-success'>
                                        <i class='fa fa-check-circle fa-1x'></i>
                                    </a>
                                @else
                                    <a data-label='Activar área' href='{{ route('manage.bookings.familias.activo',[$fam->id]) }}' class='label label-danger'>
                                        <i class='fa fa-times-circle fa-1x'></i>
                                    </a>
                                @endif
                            </td>
                            <td>
                                <a href='#destroy' data-label='Borrar' data-model='Asignación de Familia' data-action=" {{route( 'manage.bookings.familias.delete', $fam->id)}}" data-toggle='modal' data-target='#modalDestroy'><i class='fa fa-times-circle'></i></a>

                                @include('includes.form_submit', [ 'permiso'=> 'bookings', 'texto'=> 'Actualizar'])

                                @if($fam->cambio_avisado)
                                    <a data-label='Enviar' href='{{ route('manage.bookings.familias.mail', [$fam->id]) }}' class='btn btn-xs btn-success'><i class='fa fa-envelope'></i></a>
                                @else
                                    <a data-label='Enviar (pendiente avisar cambios a cliente)' href='{{ route('manage.bookings.familias.mail', [$fam->id]) }}' class='btn btn-xs btn-warning'><i class='fa fa-envelope'></i></a>
                                @endif
                            </td>
                        </tr>

                        {!! Form::close() !!}
                    @endforeach
                </tbody>
            </table>

            <hr>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-users fa-fw"></i> Añadir Familia
            </div>
            <div class="panel-body">

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.familias', $ficha->id) , 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_select2', [ 'campo'=> 'familia_id', 'texto'=> 'Familia', 'select'=> $familias, 'valor'=> old('familia_id') ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> 'Desde', 'valor'=> old('desde',$ficha->alojamiento_start_date) ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> 'Hasta', 'valor'=> old('hasta',$ficha->alojamiento_end_date) ])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'area', 'texto'=> 'Mostrar en el Área de Cliente', 'valor'=> old('area',true) ])
                        </div>
                        <div class="col-md-1">
                            <br>
                            @include('includes.form_submit', [ 'id'=>'btn-add', 'permiso'=> 'bookings', 'texto'=> 'Añadir'])
                        </div>
                    </div>

                {!! Form::close() !!}

            </div>
        </div>

    </div>
</div>