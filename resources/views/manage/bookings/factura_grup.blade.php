<!DOCTYPE html>
<html lang="es">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Factura {{$factura->numero}}</title>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>

        <?php
            $p = $ficha->plataforma ?: 1;
        ?>

        {!! Html::style('https://'.ConfigHelper::config('web',$p).'/assets/css/pdf.css') !!}
        {!! Html::style('https://'.ConfigHelper::config('web',$p).'/assets/css/bootstrap.css') !!}
        {!! Html::style('https://'.ConfigHelper::config('web',$p).'/assets/css/recibo.css') !!}


        <style>
            caption{
                @if(ConfigHelper::config('sufijo',$p) == 'bs')
                    color: #f1c40f;
                @elseif(ConfigHelper::config('sufijo',$p) == 'cic')
                    color: #3B6990;
                @elseif(ConfigHelper::config('sufijo',$p) == 'sf')
                    color: #24aab6;
                @else
                    color: #000000;
                @endif
            }
            h1{
                @if(ConfigHelper::config('sufijo',$p) == 'bs')
                    border-bottom: 1px solid #f1c40f;
                @elseif(ConfigHelper::config('sufijo',$p) == 'cic')
                    border-bottom: 1px solid #3B6990;
                @elseif(ConfigHelper::config('sufijo',$p) == 'sf')
                    border-bottom: 1px solid #24aab6;
                @else
                   border-bottom: 1px solid #CCCCCC;
                @endif
            }
        </style>

    </head>
    <body>
    <div class="page">
        <div class="row">
            <div class="col-xs-12"><img style="width: 4.5cm; height: auto; margin-top: 4px;" class="pull-right" src="https://{{ConfigHelper::config('web',$p)}}/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb',$p)}}" /></div>
        </div>


        {{-- <div class="direccion">
            @section('direccion')
                @if($ficha->oficina)
                    <strong>{{$ficha->oficina->name}}</strong>
                    <br>
                    {{$ficha->oficina->direccion}}
                    <br>
                    {{$ficha->oficina->cp}} {{$ficha->oficina->poblacion}}
                    @if($ficha->oficina->provincia)
                        ({{$ficha->oficina->provincia->name}})
                    @endif
                    <br>
                    {{$ficha->oficina->telefono}}
                    <br>
                    {{$ficha->oficina->email}}
                @endif
            @show
        </div> --}}

        <div class="row" style="margin-top: 3cm;">
            <div class="col-xs-12"><h1>FACTURA</h1></div>
        </div>

        <div class="row">
            <div class="col-xs-8"><strong>Nº Factura: {{$factura->numero}}</strong></div>
            <div class="col-xs-4"><strong>Fecha: {{$factura->fecha->format('d/m/Y')}}</strong></div>
        </div>

        <div class="row">
            <div class="col-xs-4">
                @if($convocatoria->contable)
                    Código contable: {{$convocatoria->contable}}
                @endif
            </div>
        </div>


        <div class="row" style="margin-top: 1cm;">
            <div class="col-xs-12">

                <table class="table">
                    <thead>
                        <tr class="thead">
                            <td class="col-md-3">Razón Social</td>
                            <td class="col-md-6">Dirección</td>
                            <td>CIF/NIF</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$factura->razonsocial}}</td>
                            <td>{{$factura->direccion}}</td>
                            <td>{{$factura->nif}}</td>
                        </tr>
                    </tbody>
                </table>

                <table class="table">
                    <caption>Concepto</caption>
                    <thead>
                        <tr class="thead">
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                {{$convocatoria->name}}
                                <hr>
                                <ul>
                                @foreach($factura->facturas as $v)
                                    <li>
                                        @if($v->booking->datos_congelados)
                                            {{$v->booking->datos->full_name}}
                                        @else
                                            {{$v->booking->viajero->full_name}}
                                        @endif
                                    </li>
                                @endforeach
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>




                {{-- TOTAL --}}
                <table class="table total">
                    <tr class="thead">
                        <td colspan="2" align="center"><strong>TOTAL</strong></td>
                    </tr>

                    <tr>
                        <td>TOTAL EN {{Session::get('vcn.moneda')}}</td>
                        <td><span class="booking-total" id="booking-total-resumen">{{ConfigHelper::parseMoneda($total)}}</span></td>
                    </tr>

                    <tr>
                        <td colspan="2">{{$info->factura_iva_pie}}</td>
                    </tr>

                </table>


            </div>
        </div>

        <div class="instrucciones">
            <h3>Condiciones de pago:</h3>
            <strong>
                Fecha límite de pago: {{Carbon::parse($ficha->course_start_date)->subDays(30)->format('d/m/Y')}}
                <br>
                Cuenta bancaria: {{$ficha->oficina?$ficha->oficina->banco:"-"}} - {{$ficha->oficina?$ficha->oficina->txtIban($ficha):"-"}}
                <br>
                Nota: Rogamos se envíe una copia de la transferencia a {{$ficha->oficina?strtolower($ficha->oficina->email):"-"}} incluyendo el nombre del participante al
                programa.
            </strong>
            <br>
            <small>{{ConfigHelper::config('factura_iva_pie')}}</small>
        </div>

    </div>
    </body>
</html>