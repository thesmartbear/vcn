<!DOCTYPE html>
<html lang="es">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Factura {{$factura->numero}}</title>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>

        <?php
            $p = $ficha->plataforma ?: 1;
        ?>

        {!! Html::style('https://'.ConfigHelper::config('web',$p).'/assets/css/pdf.css') !!}
        {!! Html::style('https://'.ConfigHelper::config('web',$p).'/assets/css/bootstrap.css') !!}
        {!! Html::style('https://'.ConfigHelper::config('web',$p).'/assets/css/recibo.css') !!}


        <style>
            caption{
                @if(ConfigHelper::config('sufijo', $p) == 'bs')
                    color: #f1c40f;
                @elseif(ConfigHelper::config('sufijo', $p) == 'cic')
                    color: #3B6990;
                @elseif(ConfigHelper::config('sufijo', $p) == 'sf')
                    color: #24aab6;
                @else
                    color: #000000;
                @endif
            }
            h1{
                @if(ConfigHelper::config('sufijo', $p) == 'bs')
                    border-bottom: 1px solid #f1c40f;
                @elseif(ConfigHelper::config('sufijo', $p) == 'cic')
                    border-bottom: 1px solid #3B6990;
                @elseif(ConfigHelper::config('sufijo', $p) == 'sf')
                    border-bottom: 1px solid #24aab6;
                @else
                   border-bottom: 1px solid #CCCCCC;
                @endif
            }


        </style>

    </head>
    <body>
    <div class="page">
        <div class="row">
            <div class="col-xs-12"><img style="width: 4.5cm; height: auto; margin-top: 4px;" class="pull-right" src="https://{{ConfigHelper::config('web', $p)}}/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb', $p)}}" /></div>
        </div>


        <div class="direccion">
            @section('direccion')
                @if($ficha->oficina)
                    <strong>{{$ficha->oficina->name}}</strong>
                    <br>
                    {{$ficha->oficina->direccion}}
                    <br>
                    {{$ficha->oficina->cp}} {{$ficha->oficina->poblacion}}
                    @if($ficha->oficina->provincia)
                        ({{$ficha->oficina->provincia->name}})
                    @endif
                    <br>
                    {{$ficha->oficina->telefono}}
                    <br>
                    {{$ficha->oficina->email}}
                @endif
            @show
        </div>

        <div class="row" style="margin-top: 3cm;">
            <div class="col-xs-12"><h1>FACTURA</h1></div>
        </div>

        <div class="row">
            <div class="col-xs-8"><strong>Nº Factura: {{$factura->numero}}</strong></div>
            <div class="col-xs-4"><strong>Fecha: {{$factura->fecha->format('d/m/Y')}}</strong></div>
        </div>

        <div class="row">
            <div class="col-xs-4">
                @if($ficha->contable_code)
                    Código contable: {{$ficha->contable_code}}
                @endif
            </div>
        </div>


        <div class="row" style="margin-top: 1cm;">
            <div class="col-xs-12">

                <table class="table">
                    <thead>
                        <tr class="thead">
                            <td class="col-md-3">Razón Social</td>
                            <td class="col-md-6">Dirección</td>
                            <td>CIF/NIF</td>
                        </tr>
                    </thead>
                    <tbody>

                        @if(!$factura->datos)
                            <tr>
                                <td>{{$ficha->datos->fact_razonsocial?$ficha->datos->fact_razonsocial:$ficha->datos->full_name}}</td>
                                <td>
                                    {{$ficha->datos->fact_domicilio?$ficha->datos->fact_domicilio:$ficha->datos->direccion}},
                                    {{$ficha->datos->fact_cp?$ficha->datos->fact_cp:$ficha->datos->cp}} {{$ficha->datos->fact_poblacion?$ficha->datos->fact_poblacion:$ficha->datos->ciudad}}
                                </td>
                                <td>{{$ficha->datos->fact_nif?$ficha->datos->fact_nif:$ficha->datos->documento}}</td>
                            </tr>
                        @else
                            <tr>
                                <td>{{$factura->datos['razonsocial']}}</td>
                                <td>{{$factura->datos['direccion']}}</td>
                                <td>{{$factura->datos['cif']}}</td>
                            </tr>
                        @endif

                    </tbody>
                </table>

                <table class="table">
                    <caption>Concepto</caption>
                    <thead>
                        <tr class="thead">
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!$factura->datos)
                            <tr>
                                <td>{{$ficha->datos->fact_concepto?$ficha->datos->fact_concepto:$ficha->curso->name}}</td>
                                <td align='right'>{{$ficha->datos->fact_concepto?"":ConfigHelper::parseMoneda($ficha->course_total_amount, $ficha->curso_moneda)}}</td>
                            </tr>
                        @else
                            <tr>
                                <td>{{$factura->datos['concepto1']}}</td>
                                <td align='right'>{{$factura->datos['concepto2']}}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>

                @if(!$ficha->datos->fact_concepto)

                    @if($ficha->curso->es_convocatoria_multi)
                    {{-- Especialidades --}}
                    <table class='table'>
                        <tbody id='booking-multi-especialidades-table'>
                            @foreach($ficha->multis as $esp)
                            <tr>
                                <td class='col-md-1'>Semana {{$esp->n}} [{{$esp->semana->semana}}]</td>
                                <td>
                                    Especialidad: {{$esp->especialidad?$esp->especialidad->name:'-'}}
                                </td>
                                <td align='right' id="booking-multi-especialidad-precio-{{$esp->id}}">{{ ConfigHelper::parseMoneda($esp->precio, $ficha->convocatoria->moneda_name) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif

                    @if($ficha->alojamiento)
                    <?php $u = $ficha->accommodation_weeks; ?>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>
                                Alojamiento: {{$ficha->alojamiento?$ficha->alojamiento->name:"-"}} ::
                                Del {{Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y')}}
                                al {{Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y')}}
                            </td>
                            <td>
                                {{$ficha->accommodation_weeks}} {{$ficha->alojamiento?$ficha->alojamiento->getDuracionName($u):$ficha->convocatoria->getDuracionName($u)}}
                            </td>
                            <td align='right'>
                                {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    @endif


                    {{-- EXTRAS --}}
                    @if(count($ficha->extras))
                    <table class="table">
                        <tbody>

                            @foreach($ficha->extras_curso as $extra)
                            <tr>
                                <td>Extra: {{$extra->name}}</td>
                                <td>
                                    {{$extra->tipo_unidad_name}}

                                    @if($extra->tipo_unidad)
                                        &nbsp;({{$extra->unidad_name}})
                                    @endif
                                    <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda->name) }}</i>
                                    x{{$extra->unidades}}
                                </td>
                                <td align="right">
                                    {{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda->name) }}
                                </td>
                            </tr>
                            @endforeach

                            @foreach($ficha->extras_centro as $extra)
                                @include('includes.bookings_tr_extra', ['extra'=> $extra])
                            @endforeach

                            @foreach($ficha->extras_alojamiento as $extra)
                                @include('includes.bookings_tr_extra', ['extra'=> $extra])
                            @endforeach

                            @foreach($ficha->extras_generico as $extra)
                                @include('includes.bookings_tr_extra', ['extra'=> $extra])
                            @endforeach

                            @foreach($ficha->extras_cancelacion as $extra)
                                @include('includes.bookings_tr_extra', ['extra'=> $extra])
                            @endforeach

                            @foreach($ficha->extrasOtros as $extra)
                            <tr>
                                <td>{{$extra->notas}}</td>
                                <td>
                                    <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
                                    x{{$extra->unidades}}
                                </td>
                                <td align="right">{{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda_name) }}</td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    @endif

                @endif


                {{-- TOTAL --}}
                <table class="table total">
                    <tr class="thead">
                        <td colspan="2" align="center"><strong>TOTAL</strong></td>
                    </tr>

                    <tr>
                        <td>SubTotal</td>
                        <td>
                            <span id="booking-subtotal-resumen">

                                @if(!$factura->precio_total)

                                    @foreach($factura->booking->precio_total['subtotal_txt'] as $subtotal)
                                        {{$subtotal}}<br>
                                    @endforeach

                                @else

                                    @foreach($factura->precio_total['subtotal_txt'] as $subtotal)
                                        {{$subtotal}}<br>
                                    @endforeach

                                @endif

                            </span>
                        </td>
                    </tr>

                    @if($factura->divisa_variacion && !$factura->booking->promo_cambio_fijo)
                    <tr>
                        <td>Variación Divisa</td>
                        <td>{{ConfigHelper::parseMoneda($factura->divisa_variacion)}}</td>
                    </tr>
                    @endif

                    {{-- DESCUENTOS --}}
                    @if($factura->precio_total['descuento_especial'] > 0)
                        <tr>
                            <td>Descuento especial</td>
                            <td>{{$factura->precio_total['descuento_especial_txt']}}</td>
                        </tr>
                    @endif

                    @if($ficha->descuentos->count()>0)
                        <tr>
                            <td>Descuentos</td>
                            <td>{{$factura->precio_total['descuento_txt']}}</td>
                        </tr>
                    @endif

                    <tr>
                        <td>TOTAL EN {{Session::get('vcn.moneda')}}</td>
                        <td><span class="booking-total" id="booking-total-resumen">{{$factura->total_divisa_txt}}</span></td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            Cambio aplicado:<br>
                            @if(!$factura->monedas)
                                @foreach($ficha->monedas_usadas_txt as $mu)
                                    <small>{{$mu}}</small><br>
                                @endforeach
                            @else
                                {!! $factura->monedas !!}
                            @endif
                        </td>
                    </tr>

                </table>

            </div>
        </div>


        <div style="border: '2px solid black'">

        <table width='100%'>
        <tbody>

            <tr class="acuenta">
                <td><strong>Entregas a cuenta</strong></td>
                <td colspan='2' align='right'>{{ConfigHelper::parseMoneda($ficha->saldo)}}</td>
            </tr>

            <tr class="pendientepago">
                <td><strong>Pendiente Pago</strong></td>
                <td colspan='2' align='right'><strong>{{ConfigHelper::parseMoneda($ficha->saldo_pendiente)}}</strong></td>
            </tr>

            </tbody>
        </table>

        <div class="instrucciones">
            <h3>Condiciones de pago:</h3>
            <strong>
            Fecha límite de pago: {{Carbon::parse($ficha->course_start_date)->subDays(30)->format('d/m/Y')}}
            <br>
            Cuenta bancaria: {{$ficha->oficina?$ficha->oficina->banco:"-"}} - {{$ficha->oficina?$ficha->oficina->txtIban($ficha):"-"}}
            <br>
            Nota: Rogamos se envíe una copia de la transferencia a {{$ficha->oficina?strtolower($ficha->oficina->email):"-"}} incluyendo el nombre del participante al
            programa.
            </strong>
        </div>

        <hr>
        <div class="instrucciones">
            @if($ficha->idioma_contacto == "es")

                PROTECCIÓN DE DATOS PERSONALES. Informamos que los datos se tratarán con la finalidad de prestar y facturar nuestros servicios, en base a la ejecución de un contrato y al cumplimiento de obligaciones legales. Los datos se conservarán durante el plazo establecido en las normativas tributaria y mercantil, como mínimo. Pueden ejercer los derechos de acceso, rectificación, supresión, portabilidad de datos, limitación, oposición y revocación del consentimiento, ante el responsable del tratamiento BRITISH SUMMER EXPERIENCIES, S.L., en su dirección que figura en la factura. Tienen derecho a realizar una reclamación ante las autoridades de protección de datos. Más información en https://www.britishsummer.com/

            @elseif($ficha->idioma_contacto == "ca")

                PROTECCIÓ DE DADES PERSONALS. Informem que les dades es tractaran amb la finalitat de prestar i facturar els nostres serveis, en base a l'execució d'un contracte i al compliment d'obligacions legals. Les dades es conservaran durant el termini establert en les normatives tributària i mercantil, com a mínim. Poden exercir els drets d'accés, rectificació, supressió, portabilitat de dades, limitació, oposició i revocació del consentiment, davant el responsable del tractament BRITISH SUMMER EXPERIENCIES, S.L., a la seva adreça que figura a la factura. Tenen dret a fer una reclamació davant les autoritats de protecció de dades. Més informació a https://www.britishsummer.com/

            @endif

            <br>
            <small>{{ConfigHelper::config('factura_iva_pie')}}</small>
        </div>

        </div>

    </div>
    </body>
</html>