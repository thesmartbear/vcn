<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Booking {{$ficha->id}}</title>

        <?php
            $p = $ficha->plataforma ?: 1;
        ?>

        {!! Html::style('assets/css/pdf.css') !!}
        {!! Html::style('assets/css/bootstrap.css') !!}

        <style>
            caption{
                @if(ConfigHelper::config('sufijo',$p) == 'bs')
                    color: #f1c40f;
                @elseif(ConfigHelper::config('sufijo',$p) == 'cic')
                    color: #3B6990;
                @elseif(ConfigHelper::config('sufijo',$p) == 'sf')
                    color: #24aab6;
                @else
                    color: #000000;
                @endif
            }
            h1{
                @if(ConfigHelper::config('sufijo',$p) == 'bs')
                    border-bottom: 1px solid #f1c40f;
                @elseif(ConfigHelper::config('sufijo',$p) == 'cic')
                    border-bottom: 1px solid #3B6990;
                @elseif(ConfigHelper::config('sufijo',$p) == 'sf')
                    border-bottom: 1px solid #24aab6;
                @else
                   border-bottom: 1px solid #CCCCCC;
                @endif
            }
        </style>

    </head>
    <body>
        <div class="row">
            <div class="col-xs-12"><img style="width: 4.5cm; height: auto; margin-top: 4px;" class="pull-right" src="https://{{ConfigHelper::config('web',$p)}}/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb',$p)}}" /></div>
        </div>
        <div class="row">
            <div class="col-xs-12"><h1>Booking {{$ficha->id}}</h1></div>
            <div class="col-xs-12">
                <table class="table datosbasicos">
                    <caption>Student</caption>
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td>{{$ficha->datos->name}}</td>
                        </tr>
                        <tr>
                            <td>Family Name</td>
                            <td>{{$ficha->datos->lastname}} {{$ficha->datos->lastname2}}</td>
                        </tr>
                        <tr>
                            <td>DOB</td>
                            <td>{{Carbon::parse($ficha->datos->fechanac)->format('d/m/Y')}}</td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>{{$ficha->datos->sexo==1?'Male':'Female'}}</td>
                        </tr>
                        <tr>
                            <td>Nationality</td>
                            <td>{{$ficha->datos->nacionalidad}}</td>
                        </tr>
                        <tr>
                            <td>Language Level</td>
                            <td>{{$ficha->datos->idioma}} ({{$ficha->viajero->datos->idioma_nivel ?? "-"}})</td>
                        </tr>
                        <tr>
                            <td>Contact phone number</td>
                            <td>{{$ficha->datos->movil?$ficha->datos->movil:$ficha->datos->phone}}</td>
                        </tr>

                        @if(!$ficha->curso->es_menor)
                        <tr>
                            <td>Smoker</td>
                            <td>{{$ficha->datos->fumador?'Yes':'No'}}</td>
                        </tr>
                        @endif

                        <tr>
                            <td>Allergies</td>
                            <td>{{$ficha->datos->alergias2 ? "Allergies:": ""}} {{$ficha->datos->alergias2}}</td>
                        </tr>
                        <tr>
                            <td>Illness</td>
                            <td>{{$ficha->datos->enfermedad2 ? "Illness:": ""}} {{$ficha->datos->enfermedad2}}</td>
                        </tr>
                        <tr>
                            <td>Medication</td>
                            <td>{{$ficha->datos->medicacion2 ? "Medication:": ""}} {{$ficha->datos->medicacion2}}</td>
                        </tr>
                        <tr>
                            <td>Treatments</td>
                            <td>{{$ficha->datos->tratamiento2 ? "Treatment:": ""}} {{$ficha->datos->tratamiento2}}</td>
                        </tr>
                        <tr>
                            <td>Special diet</td>
                            <td>{{$ficha->datos->dieta2 ? "Diet:": ""}} {{$ficha->datos->dieta2}}</td>
                        </tr>
                        <tr>
                            <td>Interests / Hobbies</td>
                            <td>{{$ficha->datos->hobby}}</td>
                        </tr>
                        <tr>
                            <td>REMARKS / SPECIAL REQUESTS</td>
                            <td>{{$ficha->notas2}}</td>
                        </tr>
                    
                        <tr class="emergency">
                            <td>Emergency contacts</td>
                            <td>
                                <ul>
                                @foreach($ficha->datos_congelados ? $ficha->datos->tutores : $ficha->viajero->tutores as $tutor)
                                    <li>{{$ficha->datos_congelados ? ($tutor['name']." ".$tutor['lastname']) : $tutor->full_name}} ({{ConfigHelper::getTutorRelacion($ficha->datos_congelados ? $tutor['pivot']['relacion'] : ($tutor->pivot ? $tutor->pivot->relacion : ""))}}): {{$tutor->movil}} / {{$tutor->phone}} / {{$tutor->empresa_phone}}</li>
                                @endforeach

                                @if($ficha->curso->es_menor)
                                    <li>{{$ficha->datos->emergencia_contacto}}: {{$ficha->datos->emergencia_telefono}}</li>
                                @endif
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table class="table datosviajero">
                    <caption>Course</caption>
                    <tbody>
                        <tr>
                            <td>Course</td>
                            <td>{{$ficha->curso->name}}</td>
                        </tr>
                        <tr>
                            <td>Start date</td>
                            <td>{{$ficha->curso_start_date}}</td>
                        </tr>
                        <tr>
                            <td>End date</td>
                            <td>{{$ficha->curso_end_date}}</td>
                        </tr>
                        <tr>
                            <td>Duration</td>
                            <td>{{$ficha->weeks}} {{$ficha->curso->duracion_name_eng}}</td>
                        </tr>
                        <tr>
                            <td>Language sessions / week</td>
                            <td>{{$ficha->curso->course_language_sessions}}</td>
                        </tr>
                    </tbody>
                </table>

                <table class="table datosviajero">
                    <caption>Accommodation</caption>
                    <tbody>
                    <tr>
                        <td>Accommodation</td>
                        <td>{{$ficha->alojamiento?$ficha->alojamiento->name:'-'}}</td>
                    </tr>
                    <tr>
                        <td>Start date</td>
                        <td>{{$ficha->alojamiento_start_date}}</td>
                    </tr>
                    <tr>
                        <td>End date</td>
                        <td>{{$ficha->alojamiento_end_date}}</td>
                    </tr>
                    <tr>
                        <td>Duration</td>
                        <td>{{$ficha->accommodation_weeks}} {{$ficha->curso->duracion_name_eng}}</td>
                    </tr>
                    @if($ficha->curso->subcategory_id==10)
                        <tr>
                            <td>Transfer</td>
                            <td>{{$ficha->accommodation_weeks}} {{$ficha->curso->duracion_name_eng}}</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">

                {{-- EXTRAS --}}
                @if(count($ficha->extras))
                <table class="table">
                    <caption>Extras</caption>
                    <tbody>

                        @foreach($ficha->extras_curso as $extra)
                        <tr>
                            <td>{{$extra->name}}</td>
                            <td>
                                {{$extra->tipo_unidad_name}}

                                @if($extra->tipo_unidad)
                                    &nbsp;({{$extra->unidad_name}})
                                @endif
                                <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda->name) }}</i>
                                x{{$extra->unidades}}
                            </td>
                            <td align="right">
                                {{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda->name) }}
                            </td>
                        </tr>
                        @endforeach

                        @foreach($ficha->extras_centro as $extra)
                            @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        @endforeach

                        @foreach($ficha->extras_alojamiento as $extra)
                            @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        @endforeach

                        {{--@foreach($ficha->extras_generico as $extra)--}}
                            {{--@include('includes.bookings_tr_extra', ['extra'=> $extra])--}}
                        {{--@endforeach--}}

                        {{--@foreach($ficha->extras_cancelacion as $extra)
                            @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        @endforeach--}}

                        @foreach($ficha->extrasOtros as $extra)
                        <tr>
                            <td>{{$extra->notas}}</td>
                            <td>
                                <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
                                x{{$extra->unidades}}
                            </td>
                            <td align="right">{{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda_name) }}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                @endif
                    
            </div>
        </div>
        
    </body>
</html>