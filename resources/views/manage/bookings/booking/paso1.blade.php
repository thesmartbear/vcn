<?php
$style_alojamiento = "display:none;";
$style_transporte = "display:none;";
$style_extras = "display:none;";
$class_extras = "";


if($ficha->convocatoria)
{
    $style_alojamiento = "";

    if($ficha->convocatory_close_id)
    {
        $style_transporte = "";
    }
}

if($ficha->convocatoria && $ficha->alojamiento)
{
    $style_transporte = "";

    if($ficha->convocatory_open_id && !$ficha->vuelos )
    {
        $style_extras = "";
    }
}

//Abierta + Sin Alojamiento
if($ficha->convocatory_open_id && !$ficha->alojamiento )
{
    $style_transporte = "";

    if($ficha->convocatory_open_id && !$ficha->vuelos )
    {
        $style_extras = "";
    }
}

if($ficha->vuelo || $ficha->transporte_no)
{
    $style_extras = "";
}

if(!$ficha->vuelos)
{
    $style_extras = "";
    $class_extras = "no-fade";
}

?>

{{-- CONVOCATORIA --}}
<h2 class="booking-title">Elige tus fechas</h2>
@if($booking->curso->es_convocatoria_cerrada)

    {{-- CONVOCATORIA CERRADA --}}
    @if( $booking->curso->convocatoriasCerradasSolo->count() )
    <table class="table">
        <tbody>

            @foreach($booking->curso->convocatoriasCerradasSolo->where('convocatory_close_status',1) as $cc)

                <?php
                    $plazas_alojamiento = 1;
                    if($cc->alojamiento)
                    {
                        $pa = $cc->getPlazas($cc->alojamiento->id);
                        if($pa)
                        {
                           $plazas_alojamiento = $pa->plazas_disponibles;
                        }
                    }

                    if(!$plazas_alojamiento)
                    {
                        $plazas_alojamiento = $ficha->getEsPlazaOnlineAlojamiento($cc->id, $cc->alojamiento_id);
                    }
                ?>

                <tr>
                    <td>
                    {{-- @if($plazas_alojamiento<1)
                        <i class="booking-disabled"></i>
                        <span class="booking-disabled">
                            {{$cc->duracion}} {{$cc->getDuracionName($cc->duracion)}} {{$cc->alojamiento?" en ".$cc->alojamiento->tipo->name:"SIN ALOJAMIENTO"}} del {{$cc->start_date}} al {{$cc->end_date}}
                        </span>
                        <i> No quedan plazas</i>
                    @else --}}
                        <i class="{{$ficha->convocatoria_class($cc->id)}} booking-ccerrada" id="ccerrada-{{$cc->id}}"
                            data-id='{{$cc->id}}' data-semiopen="{{$cc->convocatory_semiopen}}"
                            data-alojamiento="{{$cc->alojamiento_id?$cc->alojamiento_id:0}}"
                            data-start="{{$cc->convocatory_close_start_date}}"
                            data-end="{{$cc->convocatory_close_end_date}}"
                            data-day="{{$cc->convocatory_semiopen_start_day}}"></i>

                        {{$cc->name}}: {{$cc->duracion}} {{$cc->getDuracionName($cc->duracion)}} {{$cc->alojamiento?" en ".$cc->alojamiento->tipo->name:"SIN ALOJAMIENTO"}} del {{$cc->start_date}} al {{$cc->end_date}}

                        @if($plazas_alojamiento<1)
                            <i> No quedan plazas [OBVKG]</i>
                        @else
                            <i>[Plazas: {{$plazas_alojamiento}}]</i>
                        @endif
                    {{-- @endif --}}

                    </td>

                    <td align='right'>
                        <span class="booking-ccerrada-precio" id="ccerrada-precio-{{$cc->id}}" data-moneda="{{$cc->convocatory_close_currency_id}}">

                            {{ ConfigHelper::parseMoneda($cc->precio, $cc->moneda_name) }}

                        </span>
                    </td>
                </tr>

            @endforeach

        </tbody>
    </table>
    @endif

    @if($booking->curso->es_convocatoria_semicerrada)

        @if(!$booking->curso->es_convocatoria_semicerrada_solo)
            <h3 class="booking-title">Plan B</h3>
            <h4>
                ¿Quieres viajar en otras fechas? Escoge la fecha de inicio y la duración.<br />
                <small><i>*Para nuestros programas de jóvenes, esta opción no incluye billetes de avión ni asistencia del monitor.</i></small>
            </h4>

        @endif

        {{-- CONVOCATORIA SEMICERRADA --}}
        @foreach($booking->curso->convocatoriasSemiCerradas->where('convocatory_close_status',1) as $cc)

            <?php
                $plazas_alojamiento = 1;
                if($cc->alojamiento)
                {
                    $pa = $cc->getPlazas($cc->alojamiento->id);
                    if($pa)
                    {
                       $plazas_alojamiento = $pa->plazas_disponibles;
                    }
                }

                if(!$plazas_alojamiento)
                {
                    $plazas_alojamiento = $ficha->getEsPlazaOnlineAlojamiento($cc->id, $cc->alojamiento_id);
                }
            ?>

            <table class="table csemi">
            <tbody>
                <tr>
                    <td class="csname">
                    {{-- @if($plazas_alojamiento<1)
                        <i class="booking-disabled"></i>
                        <span class="booking-disabled">{{$cc->duracion}} {{$cc->getDuracionName($cc->duracion)}} en {{$cc->alojamiento?$cc->alojamiento->tipo->name:'?'}} a escoger entre {{$cc->start_date}} y {{$cc->end_date}}</span>
                        <i>No quedan plazas</i>
                    @else --}}
                        <i class="{{$booking->convocatoria_class($cc->id)}} booking-ccerrada" id="ccerrada-{{$cc->id}}"
                            data-id='{{$cc->id}}' data-semiopen="{{$cc->convocatory_semiopen}}"
                            data-alojamiento="{{$cc->alojamiento_id?$cc->alojamiento_id:0}}"
                            data-start="{{$cc->convocatory_close_start_date}}"
                            data-end="{{$cc->convocatory_close_end_date}}"
                            data-day="{{$cc->convocatory_semiopen_start_day}}"></i>

                        <span>{{$cc->duracion}} {{$cc->getDuracionName($cc->duracion)}} en {{$cc->alojamiento?$cc->alojamiento->tipo->name:'-SELECCIONAR-'}} a escoger entre {{$cc->start_date}} y {{$cc->end_date}}</span>

                        @if($plazas_alojamiento<1)
                            <i> No quedan plazas [OBVKG]</i>
                        @else
                            <i>&nbsp;[Plazas: {{$plazas_alojamiento}}]</i>
                        @endif
                    {{-- @endif --}}
                    </td>

                    <td align='right'>
                        <span class="booking-ccerrada-precio" id="ccerrada-precio-{{$cc->id}}" data-moneda="{{$cc->convocatory_close_currency_id}}">

                            {{ ConfigHelper::parseMoneda($cc->precio, $cc->moneda_name) }}

                        </span>
                    </td>
                </tr>
            </tbody>
            </table>
        @endforeach

        <hr>
        <div id="booking-semiopen" class="form-group row">
            <div class="col-md-3">
                {!! Form::label('course_start_date', 'Escoge tu Fecha de Inicio') !!}
            </div>
            <div class="col-md-3">
                {!! Form::text('course_start_date', Carbon::parse($booking->course_start_date)->format('d/m/Y'), array( 'id'=>'course_start_date', 'placeholder' => 'Fecha Inicio', 'class' => 'form-control', 'required'=>true)) !!}
                <span class="help-block">{{ $errors->first('course_start_date') }}</span>
            </div>
            <div class="col-md-1">
                <a id="booking-ccerrada-semi" href="#" class="btn btn-warning">Calcular</a>
            </div>
            <div class="col-md-5">
                {!! Form::label('course_end_date', 'Fecha Fin') !!}:
                <span id='course_end_date'>{{Carbon::parse($booking->course_end_date)->format('d/m/Y')}}</span>
            </div>
        </div>

    @endif

    {{-- ALOJAMIENTOS --}}
    @if($booking->curso->alojamientos && $booking->convocatoria)

    <div id="paso1-alojamiento" style='{{$style_alojamiento}}'>
    @if($booking->convocatoria->alojamiento_id!=0 || $ficha->curso->alojamientos->count())
        <h2 class="booking-title">Elige tu alojamiento</h2>
    @endif

    <table class="table">
        <tbody>

        @if($ficha->convocatoria)
        @if($ficha->convocatoria->alojamiento_id==0)
                <tr>
                <td>
                    <i class="{{$booking->alojamiento_class(0, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}} booking-ccerrada-alojamiento"
                        id="ccerrada-alojamiento-0"
                        data-id="0"
                        data-precio="0"
                        data-moneda="0"
                        >
                    </i>

                    Sin Alojamiento

                </td>

                <td align="right">
                    <span id="ccerrada-alojamiento-precio-0">
                        {{ConfigHelper::parseMoneda(0)}}
                    </span>
                </td>

                </tr>

            @foreach($ficha->curso->alojamientos as $alojamiento)

                <?php
                    $alojamiento_precio = $alojamiento->calcularPrecio(
                        Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y'),
                        Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y'), $ficha->duracion);

                        $pa = $ficha->convocatoria ? $ficha->convocatoria->getPlazas($alojamiento->id) : null;

                        $plazas_alojamiento = $pa ? 1 : 0;

                        if($pa)
                        {
                            $plazas_alojamiento = $pa->plazas_disponibles;
                        }

                        if(!$plazas_alojamiento)
                        {
                            $plazas_alojamiento = $ficha->convocactoria ? $ficha->getEsPlazaOnlineAlojamiento($ficha->convocactoria->id, $alojamiento->id) : 0;
                        }
                ?>

                <tr>

                <td>
                    {{-- @if($plazas_alojamiento<1)
                        <i class="booking-disabled {{$ficha->alojamiento_class($alojamiento->id, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}}"></i>
                        <span class="booking-disabled">
                            {{$alojamiento->name}}<i>( {{$alojamiento->tipo_name}} )</i>
                        </span>
                        <i>No quedan plazas</i>
                    @else --}}
                        <i class="{{$ficha->alojamiento_class($alojamiento->id, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}} booking-ccerrada-alojamiento"
                            id="ccerrada-alojamiento-{{$alojamiento->id}}"
                            data-id="{{$alojamiento->id}}"
                            data-precio="{{$alojamiento_precio['importe']}}"
                            data-moneda="{{$alojamiento_precio['moneda']}}"
                            >
                        </i>
                        {{$alojamiento->name}} <i>( {{$alojamiento->tipo_name}} )</i>

                        @if($plazas_alojamiento<1)
                            <i> No quedan plazas [OBVKG]</i>
                        @else
                            <i>[Plazas: {{$plazas_alojamiento}}]</i>
                        @endif
                    {{-- @endif --}}

                </td>

                <td align="right">
                    <span id="ccerrada-alojamiento-precio-{{$alojamiento->id}}">
                        {{ConfigHelper::parseMoneda($alojamiento_precio['importe'],$alojamiento_precio['moneda'])}}
                    </span>
                </td>

                </tr>

            @endforeach

        @else

            <?php
                //alojamiento asignado:
                $alojamiento = $ficha->convocatoria->alojamiento;

                $alojamiento_precio = $alojamiento->calcularPrecio(
                    Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y'),
                    Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y'), $ficha->duracion, $ficha->duracion_fijo);

                $plazas_alojamiento = 1;
                $pa = $ficha->convocatoria?$ficha->convocatoria->getPlazas($alojamiento->id):null;
                if($pa)
                {
                   $plazas_alojamiento = $pa->plazas_disponibles;
                }

                if(!$plazas_alojamiento)
                {
                    $plazas_alojamiento = $ficha->getEsPlazaOnlineAlojamiento($ficha->convocatory_close_id, $alojamiento->id);
                }
            ?>

            @if($alojamiento_precio)
            <tr>

            <td>
                {{-- @if($plazas_alojamiento<1)
                    <i class="booking-disabled {{$ficha->alojamiento_class($alojamiento->id, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}}"></i>
                    <span class="booking-disabled">
                        {{$alojamiento->name}}<i>( {{$alojamiento->tipo_name}} )</i>
                    </span>
                    <i>No quedan plazas</i>
                @else --}}

                    <i class="{{$ficha->alojamiento_class($alojamiento->id, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}} booking-ccerrada-alojamiento"
                        id="ccerrada-alojamiento-{{$alojamiento->id}}"
                        data-id="{{$alojamiento->id}}"
                        data-precio="{{$alojamiento_precio['importe']}}"
                        data-moneda="{{$alojamiento_precio['moneda']}}"
                        >
                    </i>
                    {{$alojamiento->name}} <i>( {{$alojamiento->tipo_name}} )</i>

                    @if($plazas_alojamiento<1)
                        <i> No quedan plazas [OBVKG]</i>
                    @else
                        <i>[Plazas: {{$plazas_alojamiento}}]</i>
                    @endif

                {{-- @endif --}}

            </td>

            <td align="right">
                <span id="ccerrada-alojamiento-precio-{{$alojamiento->id}}">
                    {{ConfigHelper::parseMoneda($alojamiento_precio['importe'],$alojamiento_precio['moneda'])}}
                </span>
            </td>

            </tr>
            @endif

        @endif

        @else
            <div class="note note-warning">
                <h4 class="block">Atención</h4>
                <p>Debe seleccionar la Convocatoria</p>
            </div>
        @endif

        </tbody>

    </table>
    </div>
    @endif

@elseif($booking->curso->es_convocatoria_abierta)

    {{-- OCULTO PA ES ALGO INTERNO --}}
    <div class="hidden">
    <table class="table">
        <thead>
        <tr class="thead">
            <td>Fechas</td>
        </tr>
        </thead>
        <tbody>

        @foreach($ficha->curso->convocatoriasAbiertas as $co)
            <tr>
                <td>
                    <i class="{{$ficha->convocatoria_class($co->id)}} booking-cabierta" id="cabierta-{{$co->id}}"
                        data-id='{{$co->id}}'
                        data-start="{{$co->convocatory_open_valid_start_date}}"
                        data-end="{{$co->convocatory_open_valid_end_date}}"
                        data-day="{{$co->convocatory_open_start_day}}"></i>

                    Del {{$co->start_date}} al {{$co->end_date}}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    </div>
    {{-- OCULTO PA ES ALGO INTERNO --}}

    {{-- CONVOCATORIA ABIERTA --}}
    <table class="table">
        <thead>
        <tr class="thead">
            <td>Fecha Inicio</td>
            <td>Fecha Fin</td>
            <td>Duración</td>
            <td align="right">Precio</td>
        </tr>
        </thead>
        <tbody>
        <tr>

            <td>
                {!! Form::text('booking-cabierta-fecha_ini', $ficha->course_start_date?Carbon::parse($ficha->course_start_date)->format('d/m/Y'):"", array( 'id'=>'booking-cabierta-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('booking-cabierta-fecha_ini') }}</span>
                <span id="booking-cabierta-alerta" class='booking-alerta'>{{isset($ficha->alertas['cabierta'])?$ficha->alertas['cabierta']:""}}</span>
                <span id="booking-cabierta-precio-detalle" class='booking-precio'>
                    @if($ficha->curso_precio_extra>0)
                        {{"Precio base: ". ConfigHelper::parseMoneda( ($ficha->course_price), $ficha->curso_moneda)}}
                        {{" + Extra temporada: ". ConfigHelper::parseMoneda( ($ficha->curso_precio_extra), $ficha->curso_moneda)}}
                    @endif
                </span>
            </td>
            <td>
                {!! Form::text('booking-cabierta-fecha_fin', $ficha->course_end_date?Carbon::parse($ficha->course_end_date)->format('d/m/Y'):"", array( 'id'=>'booking-cabierta-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
            </td>
            <td>
                {!! Form::select('booking-cabierta-semanas', $semanas, $ficha->duracion, array( 'id'=> 'booking-cabierta-semanas', 'class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('booking-cabierta-semanas') }}</span>
            </td>
            <td align='right'>
                <span class="booking-cabierta-precio" id="booking-cabierta-precio">
                    @if($ficha->convocatoria)
                        {{ ConfigHelper::parseMoneda( ($ficha->course_total_amount), $ficha->curso_moneda) }}
                    @endif
                </span>
            </td>
        </tr>

        {{-- DESCUENTOS --}}
        @if($descuento)
        <tr>
            <td colspan="3">Descuento aplicado [{{$descuento->name}}]:</td>
            <td align='right'>
                <span id="booking-cabierta-precio-dto">
                    {{-- ConfigHelper::parseMoneda( (($ficha->course_price * $ficha->duracion) * ($descuentos[0]/100)), $ficha->curso_moneda) --}}
                    {{ ConfigHelper::parseMoneda( $ficha->center_discount_amount, $ficha->curso_moneda) }}
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">Precio a pagar:</td>
            <td align='right'>
                <span id="booking-cabierta-precio-dtopagar">
                    {{ ConfigHelper::parseMoneda( ($ficha->course_total_amount-$ficha->center_discount_amount), $ficha->curso_moneda) }}
                </span>
            </td>
        </tr>
        @endif

        </tbody>
    </table>

    {{-- ALOJAMIENTOS --}}
    <div id="paso1-alojamiento" style='{{$style_alojamiento}}'>

        <h2 class="booking-title">Elige tu alojamiento</h2>

        <table class="table">
            <thead>
            <tr class="thead">
                <td>Alojamiento</td>
                <td>Fecha Inicio</td>
                <td>Fecha Fin</td>
                <td>Duración</td>
                <td class="col-md-2" align="right">Precio</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    {!! Form::select('booking-cabierta-alojamiento', [""=>"Sin Alojamiento"] + $ficha->curso->alojamientos->pluck('name','id')->toArray(), $ficha->accommodation_id,
                            array(
                                'id'=> 'booking-cabierta-alojamiento',
                                'class' => 'form-control',
                                'data-day' => ($ficha->alojamiento?$ficha->alojamiento->start_day:null),
                            )
                        )
                    !!}
                    <span class="help-block">{{ $errors->first('booking-alojamiento') }}</span>
                    <span id="booking-alojamiento-alerta" class='booking-alerta'>{{isset($ficha->alertas['alojamiento'])?$ficha->alertas['alojamiento']:""}}</span>
                    <span id="booking-alojamiento-precio-detalle" class='booking-precio'>
                        @if($ficha->alojamiento_precio_extra>0)
                            {{"Precio base: ". ConfigHelper::parseMoneda( ($ficha->accommodation_price), $ficha->curso_moneda)}}
                            {{" + Extra temporada: ". ConfigHelper::parseMoneda( ($ficha->alojamiento_precio_extra), $ficha->curso_moneda)}}
                        @endif
                    </span>
                </td>
                <td>
                    {!! Form::text('booking-alojamiento-fecha_ini', $ficha->accommodation_start_date?Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y'):"", array( 'id'=>'booking-alojamiento-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('booking-alojamiento-fecha_ini') }}</span>
                </td>
                <td>
                    {!! Form::text('booking-alojamiento-fecha_fin', $ficha->accommodation_end_date?Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y'):"", array( 'id'=>'booking-alojamiento-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
                </td>
                <td>
                    {!! Form::select('booking-alojamiento-semanas', $semanas, $ficha->accommodation_weeks, array( 'id'=> 'booking-alojamiento-semanas', 'class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('booking-alojamiento-semanas') }}</span>
                </td>
                <td align='right'>
                    <span class="booking-alojamiento-precio" id="booking-alojamiento-precio">
                        {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
                    </span>
                </td>
            </tr>
            </tbody>
        </table>

    </div>


@elseif($booking->curso->es_convocatoria_multi)

    <?php
        $plazas_multi = $ficha->convocatoria?$ficha->convocatoria->hayPlazasDisponibles($ficha->id):false;
    ?>

    @if(!$plazas_multi)
        <div class="note note-danger">
            <h4 class="block">Atención</h4>
            <p>No quedan plazas disponibles</p>
        </div>
    @endif

    {{-- CONVOCATORIA MULTI --}}
    <table class='table'>
        <thead>
            <tr>
                <th>Fecha Inicio</th>
                <th>Semanas</th>
                <th>Fecha Fin</th>
                <td align='right'>Precio</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class="col-md-4">
                        @include('includes.form_select', ['campo'=> 'booking-multi-fecha_ini', 'valor'=> $multi['mfdesde_id'], 'texto'=> null, 'select'=> $multi['mfdesde']])
                    </div>
                </td>
                <td>
                    @include('includes.form_select', ['campo'=> 'booking-multi-semanas', 'valor'=> $ficha->weeks, 'texto'=> null, 'select'=> $multi['msemanas']])
                </td>
                <td><span id="booking-multi-fecha_fin">{{$ficha->course_end_date?Carbon::parse($ficha->course_end_date)->format('d/m/Y'):"-"}}</span></td>
                <td align='right'><span id="booking-multi-precio">{{ ConfigHelper::parseMoneda($ficha->course_price, $ficha->convocatoria?$ficha->convocatoria->moneda_name:null) }}</span></td>
            </tr>
        </tbody>
    </table>

    {{-- Especialidades --}}
    <h2 class="booking-title">Elige la especialidad</h2>
    <table class='table'>
        <thead>
            <tr>
                <th>Semana</th>
                <th>Especialidad</th>
                <td>Plazas</td>
                <td align='right'>Precio</td>
            </tr>
        </thead>
        <tbody id='booking-multi-especialidades-table'>
            @foreach($ficha->multis as $esp)

            <?php
                $plazas = $ficha->convocatoria->getPlazasSemana($esp->semana->semana);
            ?>

            <tr>
                <td class='col-md-1'>
                    Semana {{$esp->n}} [{{$esp->semana->semana}}]
                </td>
                <td>
                    {!! Form::select('booking-multi-especialidad-'.$esp->id, $ficha->convocatoria->especialidadesBySemana($esp->semana->semana), $esp->especialidad_id, array( 'id'=> 'booking-multi-especialidad-'.$esp->id, 'class' => 'booking-multi-especialidad form-control', 'data-id'=> $esp->id, 'data-semana'=> $esp->n )) !!}
                </td>
                <td>{{$plazas}}</td>
                <td align='right' id="booking-multi-especialidad-precio-{{$esp->id}}">{{ ConfigHelper::parseMoneda($esp->precio, $ficha->convocatoria->moneda_name) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endif

<hr>
{{-- @include('comprar.booking_codigo') --}}

{{-- BOOKING DIRECTO --}}
@if(!$booking->es_presupuesto && $booking->curso->activo_directo)
@if(ConfigHelper::canEdit('bookings-directos'))
    {!! Form::submit('Booking Directo', array('id'=> 'btn-next-directo', 'name'=> 'booking-directo', 'class' => 'btn btn-info')) !!}
    {!! Form::hidden('booking_directo', 0, array('id'=> 'booking_directo')) !!}
    <script>
        $(document).ready(function() {

            $('#btn-next-directo').click( function(e) {

                e.preventDefault();

                bootbox.confirm("¿Está seguro de realizar un Booking directo?", function(result) {
                    if(result)
                    {
                        $('#booking_directo').val(1);
                        $('#frm-booking-curso').submit();
                    }
                });
            });

        });
    </script>
@endif
@endif

{{-- TRANSPORTE --}}
@if( $booking->curso->es_convocatoria_cerrada || $booking->curso->es_convocatoria_abierta)
<div id="paso1-transporte" style='{{$style_transporte}}'>

    <hr>
    <h2 class="booking-title">Elige tu transporte</h2>
    
    @if(!$ficha->vuelos)
    <div class="alert alert-danger" role="alert" style="text-align:justify;">
        {{trans('area.booking.sin_vuelos')}}
    </div>

    <div class="transporte_no" id="transporte_no_div"
        @if($ficha->transporte_otro)
            style="display:none;"
        @endif
    >
    @include('includes.form_checkbox', [ 'campo'=> 'transporte_no', 'texto'=> 'NO necesito transporte' ])
    </div>
    @endif

    <div class="booking-vuelo-table">
    <div id="booking-vuelo-table"
        @if($ficha->transporte_no)
            style="display:none;"
        @endif
        >

        <?php
            $bSinPlazas = true;
            $transporte_vuelos = false;
            if($ficha->vuelos)
            {
                $v = $ficha->vuelos->first()->vuelo->transporte;
                if($v=="Vuelo" || $v==0 || is_null($v))
                {
                    $transporte_vuelos = true;
                }
            }
        ?>

        @if($ficha->vuelos)

            @if( ConfigHelper::config('tpv_aeropuertos') && $transporte_vuelos )

                {{-- BCN/MAD/OTRO ??? --}}
                {!! Form::hidden('aporte', null) !!}

                <?php
                    $bcn = null; //Barcelona (BCN), Barcelona
                    $mad = null; //Barajas (MAD), Madrid
                    $otro = null;

                    $bSinPlazas_bcn = true;
                    $bSinPlazas_mad = true;
                    $bSinPlazas_otro = true;

                    foreach( $ficha->vuelos as $vuelo )
                    {
                        if(!$vuelo->vuelo)
                        {
                            continue;
                        }

                        $plazasv = $vuelo->vuelo->plazas_disponibles;
                        if(!$plazasv)
                        {
                            if($booking->getEsPlazaOnlineVuelo($vuelo->vuelo_id))
                            {
                                $plazasv = true;
                            }
                        }

                        if($vuelo->vuelo->es_bcn && !$bcn)
                        {
                            $bcn = $vuelo;
                            $bSinPlazas_bcn = $plazasv>0?false:true;
                        }

                        if($vuelo->vuelo->es_madrid && !$mad)
                        {
                            $mad = $vuelo;
                            $bSinPlazas_mad = $plazasv>0?false:true;
                        }

                        if($vuelo->vuelo->es_otro && !$otro)
                        {
                            $otro = $vuelo;
                            $bSinPlazas_otro = $plazasv>0?false:true;
                        }
                    }

                    //Si no hay bcn => mad
                    $bcn = $bcn?:$mad;
                    //Si no hay mad => bcn
                    $mad = $mad?:$bcn;
                    $otro = $otro?:$bcn;

                    $bSinPlazas = $bSinPlazas_bcn && $bSinPlazas_mad && $bSinPlazas_otro;

                    $bSinPlazas = false;
                ?>

                {{-- SOLO SI HAY PLAZAS --}}
                @if(!$bSinPlazas)
                    <h4 class="booking-title">Selecciona el Aeropuerto de Salida</h4>
                    <table class="table vuelos">

                        <tbody>

                            {{-- BCN --}}
                            <?php
                                $vuelo = $bcn;
                                $vuelo_name = "Barcelona (BCN), Barcelona";

                                $aporte = "";
                                if($vuelo && $vuelo->vuelo && !$vuelo->vuelo->es_bcn)
                                {
                                    $aporte = "BCN";
                                }

                                $classv = "fa fa-circle-thin";
                                if($vuelo && $vuelo->vuelo_id==$ficha->vuelo_id )
                                {
                                    if( $bcn==$mad )
                                    {
                                        if($ficha->transporte_recogida==$aporte )
                                        {
                                            $classv = "fa fa-check-circle-o";
                                        }
                                    }
                                    elseif($ficha->transporte_recogida=="BCN" || $ficha->transporte_recogida=="" )
                                    {
                                        $classv = "fa fa-check-circle-o";   
                                    }
                                }
                                elseif($ficha->transporte_recogida=="BCN" )
                                {
                                    $classv = "fa fa-check-circle-o";
                                }
                            ?>
                            <tr>
                                <td>
                                    @if($vuelo && $vuelo->vuelo)
                                        {{-- @if(!$vuelo->vuelo->plazas_disponibles)
                                            <i class="fa fa-circle-thin obligatorio"></i>
                                            <span class="booking-disabled">
                                                {{$vuelo_name}}
                                            </span>
                                            <i>No quedan plazas</i>
                                        @else --}}

                                            <i class="{{$classv}} booking-vuelo-aeropuerto" data-aeropuerto='bcn' data-aporte="{{$aporte}}" id="vuelo-bcn-{{$vuelo->vuelo->id}}" data-id='{{$vuelo->vuelo->id}}' data-tipo='vuelo'></i>
                                            {{-- {{$vuelo->vuelo->id}}:{{$vuelo->vuelo->plazas_disponibles}} --}}
                                            {{$vuelo_name}}

                                            <?php
                                                $plazasv = $vuelo->vuelo->plazas_disponibles;
                                                if(!$plazasv)
                                                {
                                                    if($ficha->getEsPlazaOnlineVuelo($vuelo->vuelo_id))
                                                    {
                                                        $plazasv = true;
                                                    }
                                                }
                                            ?>

                                            @if(!$plazasv)
                                                <i>No quedan plazas [OVBKG]</i>
                                            @endif

                                        {{-- @endif --}}
                                    @endif
                                </td>
                            </tr>

                            {{-- MADRID --}}
                            <?php
                                $vuelo = $mad;
                                $vuelo_name = "Barajas (MAD), Madrid";

                                $aporte = "";
                                if($vuelo && $vuelo->vuelo && !$vuelo->vuelo->es_madrid)
                                {
                                    $aporte = "MADRID";
                                }

                                $classv = "fa fa-circle-thin";
                                if($vuelo && $vuelo->vuelo_id==$ficha->vuelo_id )
                                {
                                    if( $bcn==$mad )
                                    {
                                        if($ficha->transporte_recogida==$aporte )
                                        {
                                            $classv = "fa fa-check-circle-o";
                                        }
                                    }
                                    elseif($ficha->transporte_recogida=="MADRID" || $ficha->transporte_recogida=="" )
                                    {
                                        $classv = "fa fa-check-circle-o";   
                                    }
                                }
                                elseif($ficha->transporte_recogida=="MADRID" )
                                {
                                    $classv = "fa fa-check-circle-o";
                                }
                            ?>
                            <tr>
                                <td>
                                    @if($vuelo && $vuelo->vuelo)
                                        {{-- @if(!$vuelo->vuelo->plazas_disponibles)
                                            <i class="fa fa-circle-thin obligatorio"></i>
                                            <span class="booking-disabled">
                                                {{$vuelo_name}}
                                            </span>
                                            <i>No quedan plazas</i>
                                        @else --}}

                                            <i class="{{$classv}} booking-vuelo-aeropuerto" data-aeropuerto='mad' data-aporte="{{$aporte}}" id="vuelo-mad-{{$vuelo->vuelo->id}}" data-id='{{$vuelo->vuelo->id}}' data-tipo='vuelo'></i>
                                            {{-- {{$vuelo->vuelo->id}}:{{$vuelo->vuelo->plazas_disponibles}} --}}
                                            {{$vuelo_name}}

                                            <?php
                                                $plazasv = $vuelo->vuelo->plazas_disponibles;
                                                if(!$plazasv)
                                                {
                                                    if($ficha->getEsPlazaOnlineVuelo($vuelo->vuelo_id))
                                                    {
                                                        $plazasv = true;
                                                    }
                                                }
                                            ?>

                                            @if(!$plazasv)
                                                <i>No quedan plazas [OVBKG]</i>
                                            @endif

                                        {{-- @endif --}}
                                    @endif
                                </td>
                            </tr>

                            {{-- OTRO --}}
                            <?php
                                $vuelo = $otro;
                                if( ($otro==$bcn || $otro==$mad) && !$otro->vuelo->es_otro)
                                {
                                    $vuelo_name = "OTRO";
                                    $aporte = "OTRO";

                                    $classv = "fa fa-circle-thin";
                                    if($vuelo && $vuelo->vuelo_id==$ficha->vuelo_id && $ficha->transporte_recogida!='' && $ficha->transporte_recogida!='BCN' && $ficha->transporte_recogida!='MADRID'  )
                                    {
                                        $classv = "fa fa-check-circle-o";
                                    }

                                    $vuelo_otro = "";
                                    if($ficha->transporte_recogida && $ficha->transporte_recogida != "OTRO")
                                    {
                                        $vuelo_otro = " (". $ficha->transporte_recogida .")";
                                    }
                                }
                                else
                                {
                                    $vuelo_name = $vuelo->vuelo->aeropuerto;
                                    $aporte = "";
                                    $vuelo_otro = "";

                                    $classv = $ficha->vuelo_class($vuelo->vuelo->id);
                                }
                            ?>
                            <tr>
                                <td>
                                    @if($vuelo && $vuelo->vuelo)
                                        {{-- @if(!$vuelo->vuelo->plazas_disponibles)
                                            <i class="fa fa-circle-thin obligatorio"></i>
                                            <span class="booking-disabled">
                                                {{$vuelo_name}}
                                            </span>
                                            <i>No quedan plazas</i>
                                        @else --}}

                                            <i class="{{$classv}} booking-vuelo-aeropuerto" data-aeropuerto='otro' data-aporte="{{$aporte}}" id="vuelo-otro-{{$vuelo->vuelo->id}}" data-id='{{$vuelo->vuelo->id}}' data-tipo='vuelo'></i>
                                            {{-- {{$vuelo->vuelo->id}}:{{$vuelo->vuelo->plazas_disponibles}} --}}
                                            {{$vuelo_name}} <span id="vuelo_otro">{{$vuelo_otro}}</span>

                                            <?php
                                                $plazasv = $vuelo->vuelo->plazas_disponibles;
                                                if(!$plazasv)
                                                {
                                                    if($ficha->getEsPlazaOnlineVuelo($vuelo->vuelo_id))
                                                    {
                                                        $plazasv = true;
                                                    }
                                                }
                                            ?>

                                            @if(!$plazasv)
                                                <i>No quedan plazas [OVBKG]</i>
                                            @endif

                                        {{-- @endif --}}
                                    @endif
                                </td>
                            </tr>

                        </tbody>

                    </table>
                @endif

            @else

                <table class="table vuelos">
                    <thead>
                    <tr class="thead">
                        <th>
                            @if($transporte_vuelos)
                                Seleccionar Aeropuerto de Salida
                            @else
                                Seleccionar Ciudad de Salida
                            @endif

                            {{-- @if(!$ficha->vuelos)
                                [No hay vuelos para la Convocatoria o no hay Convocatoria seleccionada.]
                            @endif --}}
                        </th>
                        {{-- <td>Precio</td> --}}
                    </tr>
                    </thead>

                    <tbody>
                        @if($ficha->vuelos)

                            @foreach( $ficha->vuelos as $vuelo )
                                <?php
                                    $plazasv = $vuelo->vuelo->plazas_disponibles;
                                ?>
                                <tr>
                                    <td>
                                        {{-- @if(!$plazasv)
                                            <i class="fa fa-check-circle-o obligatorio"></i>
                                            <span class="booking-disabled">
                                                {{$vuelo->vuelo->aeropuerto}} [{{$vuelo->vuelo->name}}]
                                            </span>
                                            <i>No quedan plazas</i>
                                        @else --}}
                                            <?php
                                                $bSinPlazas = false;
                                            ?>
                                            <i class="{{$ficha->vuelo_class($vuelo->vuelo->id)}} booking-vuelo" id="vuelo-{{$vuelo->vuelo->id}}" data-id='{{$vuelo->vuelo->id}}' data-tipo='vuelo'></i>
                                            {{$vuelo->vuelo->aeropuerto}} [{{$vuelo->vuelo->name}}]

                                            @if(!$plazasv)
                                                <i> No quedan plazas [OBVKG]</i>
                                            @else
                                                <i>[Plazas: {{$plazasv}}]</i>
                                            @endif
                                        {{-- @endif --}}
                                    </td>

                                    {{-- <td></td> --}}

                                </tr>
                            @endforeach
                        @endif
                    </tbody>

                </table>
            @endif

            {{-- @if($ficha->vuelos && $bSinPlazas)
                <div class="alert alert-danger" role="alert" style="text-align:justify;">
                    No hay vuelos disponibles para estas fechas. Por favor, cambiar de fechas, de curso o contactar con nuestras oficinas.
                </div>
            @endif --}}

            @if($transporte_vuelos)
                <div id="div_transporte_requisitos">
                    @include('includes.form_input_text', [ 'campo'=> 'transporte_requisitos', 'texto'=> 'Requisitos especiales para este vuelo (dieta, cambio de vuelo,...)'])
                </div>

                @if( !ConfigHelper::config('tpv_aeropuertos') )
                <br />
                @include('includes.form_checkbox', [ 'campo'=> 'transporte_recogida_bool',
                    'texto'=> '¿Quieres que tramitemos un vuelo desde el aeropuerto más cercano a tu ciudad hasta el aeropuerto de salida de nuestro grupo?', 'valor'=>$ficha->transporte_recogida ])
                <div id="div_transporte_recogida_bool"
                    @if(!$ficha->transporte_recogida)
                        style="display:none;"
                    @endif
                >

                    @include('includes.form_input_text', [ 'campo'=> 'transporte_recogida',
                        'texto'=> 'Indica el aeropuerto de salida y regreso más cercano a tu ciudad'])
                </div>
                @endif
            @endif

        @endif

        @if(!$ficha->vuelos || $booking->curso->es_convocatoria_abierta)
            <div id="transporte_otro_div"
                @if($ficha->transporte_no)
                    style="display:none;"
                @endif
            >
            <hr>
            @include('includes.form_checkbox', [ 'campo'=> 'transporte_otro', 'texto'=> '¿Necesitas que te ayudemos a buscar tu vuelo?' ])
            <div id="div_transporte_otro"
                @if(!$ficha->transporte_otro)
                    style="display:none;"
                @endif
            >
                @include('includes.form_input_text', [ 'campo'=> 'transporte_detalles', 'texto'=> 'Indica tu aeropuerto de salida'])
            </div>
            </div>
        @endif

    </div>
    </div>

</div>
@endif

<div id="paso1-extras" style='{{$style_extras}}' class="{{$class_extras}}">
    @include('manage.bookings.booking.curso-extras')
</div>
