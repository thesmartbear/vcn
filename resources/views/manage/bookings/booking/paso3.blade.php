<?php
$emailRequired = $ficha->viajero->tutores->filter(function ($item) {
    return $item->email!="";
})->count()==0;

$totTutores = $ficha->viajero->tutores->count();

if( !$ficha->curso->es_menor )
{
    $emailRequired = false;
}
?>

@if($ficha->curso->es_menor)
    @if($emailRequired)
        <div class="alert alert-danger" role="alert" style="text-align:justify;">
            NOTA: Debe tener al menos un tutor con un email válido.
        </div>
    @endif
@endif

@if(!$ficha->factura_cif && ConfigHelper::config('nif'))
    <div class="alert alert-danger" role="alert" style="text-align:justify;">
        @lang('area.nota_nif')
    </div>
@endif

<?php $iTutor = 0; ?>
@foreach($ficha->viajero->tutores as $tutor)
<?php
$iTutor++
?>
<div class="panel panel-default">
    <div class="panel-heading">Tutor {{$iTutor}}</div>

    <div class="panel-body">

        <div class="form-group">
            @include('includes.form_select2', [ 'campo'=> 'relacion_'.$iTutor, 'texto'=> 'Relación', 'valor'=> $tutor->pivot->relacion,
                'select'=> [""=>""] + ConfigHelper::getTutorRelacion(), 'required'=> true])
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                @include('includes.form_input_text', [ 'campo'=> 'name_'.$iTutor, 'texto'=> 'Nombre', 'valor'=> $tutor->name, 'required'=> true])
            </div>
            <div class="col-md-6">
                @include('includes.form_input_text', [ 'campo'=> 'lastname_'.$iTutor, 'texto'=> 'Apellidos', 'valor'=> $tutor->lastname, 'required'=> true])
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-5">
                <?php
                    $reqemail = $emailRequired;
                    if(!$emailRequired && $tutor->email)
                    {
                        $reqemail = true;
                    }
                ?>

                @include('includes.form_input_text', [ 'campo'=> 'email_'.$iTutor, 'texto'=> 'E-mail', 'valor'=> $tutor->email, 'required'=> $reqemail])
            </div>
            <div class="col-md-2">
                @include('includes.form_select', [ 'campo'=> 'tipodoc_'.$iTutor, 'texto'=> "Tipo", 'select'=> ConfigHelper::getTipoDoc(), 'valor'=> $tutor->tipodoc])
            </div>
            <div class="col-md-5">
                @include('includes.form_input_text', [ 'campo'=> 'nif_'.$iTutor, 'texto'=> 'DNI', 'valor'=> $tutor->nif])
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                @include('includes.form_input_text', [ 'campo'=> 'phone_'.$iTutor, 'texto'=> 'Teléfono', 'valor'=> $tutor->phone])
            </div>
            <div class="col-md-6">
                @include('includes.form_input_text', [ 'campo'=> 'movil_'.$iTutor, 'texto'=> 'Movil', 'valor'=> $tutor->movil])
            </div>
        </div>

    </div>

</div>
@endforeach

{{-- NUEVO TUTOR --}}
@if($totTutores<2)

    <?php
    $iTutor = 10;
    ?>
    @for ($i = $totTutores; $i < 2; $i++)
    <?php
    $iTutor++;
    ?>
    <div class="panel panel-success">
        <div class="panel-heading">Añadir Tutor</div>

        <div class="panel-body">

            <div class="form-group">
                @include('includes.form_select2', [ 'campo'=> 'relacion_'.$iTutor, 'texto'=> 'Relación', 'valor'=> "",
                    'select'=> [""=>""] + ConfigHelper::getTutorRelacion()])
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'name_'.$iTutor, 'valor'=>'', 'texto'=> 'Nombre'])
                </div>
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'lastname_'.$iTutor, 'valor'=>'', 'texto'=> 'Apellidos'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-5">
                    @include('includes.form_input_text', [ 'campo'=> 'email_'.$iTutor, 'valor'=>'', 'texto'=> 'E-mail'])
                </div>
                <div class="col-md-2">
                    @include('includes.form_select', [ 'campo'=> 'tipodoc_'.$iTutor, 'texto'=> "Tipo", 'select'=> ConfigHelper::getTipoDoc()])
                </div>
                <div class="col-md-5">
                    @include('includes.form_input_text', [ 'campo'=> 'nif_'.$iTutor, 'valor'=>'', 'texto'=> 'DNI'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'phone_'.$iTutor, 'valor'=>'', 'texto'=> 'Teléfono'])
                </div>
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'movil_'.$iTutor, 'valor'=>'', 'texto'=> 'Movil'])
                </div>
            </div>

        </div>

    </div>
    @endfor

@endif

<script type="text/javascript">
$(document).ready(function() {

    //rules nif/nie
    function tipodoc_nif(n)
    {
        return $('#tipodoc_'+n).val()==0;
    }

    function tipodoc_nie(n)
    {
        return $('#tipodoc_'+n).val()==1;
    }

    function tiporelacion(n)
    {
        return ($('#name_'+n).val()!=="");
    }


    $("#tipodoc_1").change( function(){

        $("#nif_1").rules( "remove" );
        $("#nif_1").rules("add",{
            nifES: tipodoc_nif(1),
            nieES: tipodoc_nie(1),
        });

    });

    $("#tipodoc_2").change( function(){

        $("#nif_2").rules( "remove" );
        $("#nif_2").rules("add",{
            nifES: tipodoc_nif(2),
            nieES: tipodoc_nie(2),
        });

    });

    $("#name_1").keyup( function(){

        $("#relacion_1").rules( "remove" );
        $("#relacion_1").rules("add",{
            required: tiporelacion(1)
        });

    });

    $("#name_2").keyup( function(){

        $("#relacion_2").rules( "remove" );
        $("#relacion_2").rules("add",{
            required: tiporelacion(2)
        });

    });

    $(".frmValidar3").validate({
        rules:
        {
            nif_1: {
                nifES: tipodoc_nif(1),
                nieES: tipodoc_nie(1),
            },
            nif_2: {
                nifES: tipodoc_nif(2),
                nieES: tipodoc_nie(2),
            },
            relacion_1: {
                required: tiporelacion(1)
            },
            relacion_2: {
                required: tiporelacion(2)
            },
        }
    });

});
</script>