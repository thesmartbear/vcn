<div class="row" style="margin-top:40px">
    <div class="col-sm-6">
        <h3 class="text-success">{{trans('area.curso')}}: {{ $booking->curso->es_convocatoria_multi?$booking->convocatoria_name:$booking->curso->name}}</h3>
        <h4>{{trans('area.pais')}}: {{$booking->centro->pais_name}}</h4>
    </div>

    <div class="col-sm-6">
        @if($booking->fase==1)
            <a href="{{route('manage.bookings.cambiar', $ficha->id)}}" class="btn btn-danger pull-right">Cambiar curso</a>
        @endif
    </div>
</div>
<hr>