@if(!$ficha->factura_cif)
    <div class="alert alert-danger" role="alert" style="text-align:justify;">
        NOTA: Debe tener al menos un tutor con un NIF válido.
    </div>
@endif

<div id="booking-paso4">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-success"><i class="fa fa-book fa-fw"></i> Booking: {{$ficha->id}}</h3>
            <h4><i class="fa fa-user fa-fw"></i> Viajero: {{$ficha->viajero->full_name}}</h4>
        </div>
    </div>

    <div class="row margintop60">
        <div class="col-md-12">

            {{-- <h4>Viajero: {{$ficha->viajero->full_name}}</h4>
            <hr> --}}

            @include('manage.bookings.booking.total_desglose')

            {{-- <hr>
            @include('comprar.booking_total', ['ficha'=> $booking]) --}}

            <hr>
            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'booking-area', 'texto'=> 'Área Cliente', 'valor'=> $ficha->area_default])
                </div>
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'booking-area-pagos', 'texto'=> 'Área Cliente (Pagos)', 'valor'=> $ficha->area_pagos_default])
                </div>
                <div class="col-md-3">
                    @include('includes.form_checkbox', [ 'campo'=> 'booking-area-reunion', 'texto'=> 'Área Cliente (Reunión)', 'valor'=> $ficha->area_reunion_default])
                </div>
            </div>

            <hr>
            <div class="note note-success">

                <p>
                Para confirmar la inscripción, hay que realizar un pago de: <h4>{{ConfigHelper::parseMoneda($booking->online_reserva)}}</h4>
                </p>

            </div>

            {{-- FINALIZAR --}}

            @if( $ficha->curso->es_convocatoria_cerrada )

                <?php $plazas_vuelo = $ficha->vuelo?$ficha->vuelo->plazas_disponibles:1; ?>
                <?php $plazas_umbral = $ficha->vuelo?$ficha->vuelo->plazas_umbral:0; ?>

                <?php
                    if($plazas_vuelo < 1)
                    {
                        if($ficha->getEsPlazaOnlineVuelo())
                        {
                            $plazas_vuelo = true;
                        }
                    }
                ?>

                <?php
                    if( !$ficha->alojamiento )
                    {
                        $plazas_a = null;
                        $plazas_alojamiento = 1;
                    }
                    else
                    {
                        $plazas_a = $ficha->convocatoria->getPlazas($ficha->alojamiento->id);
                        $plazas_alojamiento = $plazas_a?$plazas_a->plazas_disponibles:0;
                    }

                    if(!$plazas_alojamiento)
                    {
                        $plazas_alojamiento = $ficha->getEsPlazaOnlineAlojamiento()?1:0;
                    }
                ?>

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.crear.post', $ficha->id) , 'role' => 'form', 'class' => '', 'id'=> 'fBooking')) !!}

                {!! Form::hidden('fase',$paso) !!}

                {{-- @include('manage.bookings.ficha-resumen') --}}
                {{-- @include('includes.form_checkbox', [ 'campo'=> 'condiciones', 'texto'=> 'El/Los tutores Han leído y aceptado las condiciones.']) --}}

                <hr>
                <div class="form-actions">
                    <div class="row">
                        <div class="form-group col-md-5">
                            @if($plazas_alojamiento && ($plazas_vuelo > $plazas_umbral) )
                                {!! Form::submit('Realizar Inscripción (Pre-Reserva)', array( 'name'=> 'pre-reserva', 'id'=> 'siguiente1', 'class' => 'btn btn-danger col-md-12 siguiente')) !!}
                            @else
                                <strong>Pre-Reservas NO aceptadas</strong>
                            @endif
                        </div>
                        <div class="form-group col-md-5 col-md-offset-2">
                            {!! Form::submit('Realizar Inscripción (Reserva)', array( 'name'=> 'reserva', 'id'=> 'siguiente2', 'class' => 'btn btn-success col-md-12 siguiente')) !!}

                            {!! Form::hidden('plazas_vuelo',$plazas_vuelo, array('id'=>'plazas_vuelo')) !!}
                            {!! Form::hidden('plazas_alojamiento',$plazas_alojamiento, array('id'=>'plazas_alojamiento')) !!}

                            {!! Form::hidden('pago-importe',0, array('id'=>'pago-importe')) !!}
                            {!! Form::hidden('pago-moneda',0, array('id'=>'pago-moneda')) !!}
                            {!! Form::hidden('pago-rate',0, array('id'=>'pago-rate')) !!}
                            {!! Form::hidden('pago-tipo',0, array('id'=>'pago-tipo')) !!}
                            {!! Form::hidden('pago-ovbkg',0, array('id'=>'pago-ovbkg')) !!}
                            {!! Form::hidden('pago-fecha',Carbon::now()->format('d/m/Y'), array('id'=>'pago-fecha')) !!}
                            {!! Form::hidden('mail_confirmacion',1, array('id'=>'mail_confirmacion')) !!}
                            {!! Form::hidden('area',$ficha->area_default, array('id'=>'area')) !!}
                            {!! Form::hidden('area_pagos',$ficha->area_pagos_default, array('id'=>'area_pagos')) !!}
                            {!! Form::hidden('area_reunion',$ficha->area_reunion_default, array('id'=>'area_reunion')) !!}

                            {!! Form::hidden('reserva-tipo',2, array('id'=>'reserva-tipo')) !!}
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}

            @elseif( $ficha->curso->es_convocatoria_abierta )

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.crear.post', $ficha->id) , 'role' => 'form', 'class' => '', 'id'=> 'fBooking')) !!}

                {!! Form::hidden('fase',$paso) !!}

                {!! Form::hidden('pago-importe',0, array('id'=>'pago-importe')) !!}
                {!! Form::hidden('pago-moneda',0, array('id'=>'pago-moneda')) !!}
                {!! Form::hidden('pago-rate',0, array('id'=>'pago-rate')) !!}
                {!! Form::hidden('pago-tipo',0, array('id'=>'pago-tipo')) !!}
                {!! Form::hidden('pago-fecha',Carbon::now()->format('d/m/Y'), array('id'=>'pago-fecha')) !!}
                {!! Form::hidden('mail_confirmacion',1, array('id'=>'mail_confirmacion')) !!}
                {!! Form::hidden('area',$ficha->area_default, array('id'=>'area')) !!}
                {!! Form::hidden('area_pagos',$ficha->area_pagos_default, array('id'=>'area_pagos')) !!}
                {!! Form::hidden('area_reunion',$ficha->area_reunion_default, array('id'=>'area_reunion')) !!}

                {{-- @include('manage.bookings.ficha-resumen') --}}
                {{-- @include('includes.form_checkbox', [ 'campo'=> 'condiciones', 'texto'=> 'El/Los tutores Han leído y aceptado las condiciones.']) --}}

                <div class="form-actions">
                        {!! Form::submit('Realizar Inscripción (Pre-Booking)', array( 'name'=> 'pre-booking', 'class' => 'btn btn-success col-md-12 siguiente', 'id'=> 'siguiente2')) !!}
                </div>
                {!! Form::close() !!}

            @elseif( $ficha->curso->es_convocatoria_multi )

                <?php $plazas = $ficha->convocatoria->hayPlazasDisponibles($ficha->id); ?>

                {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.crear.post', $ficha->id) , 'role' => 'form', 'class' => '', 'id'=> 'fBooking')) !!}

                {!! Form::hidden('fase',$paso) !!}

                {{-- @include('manage.bookings.ficha-resumen') --}}
                {{-- @include('includes.form_checkbox', [ 'campo'=> 'condiciones', 'texto'=> 'El/Los tutores Han leído y aceptado las condiciones.']) --}}

                <hr>
                <div class="form-actions">
                    <div class="row">
                    <div class="form-group col-md-5">
                        @if($plazas)
                            {!! Form::submit('Realizar Inscripción (Pre-Reserva)', array( 'name'=> 'pre-reserva', 'id'=> 'siguiente1', 'class' => 'btn btn-danger col-md-12 siguiente')) !!}
                        @else
                            <strong>Pre-Reservas NO aceptadas</strong>
                        @endif
                    </div>
                    <div class="form-group col-md-5 col-md-offset-2">
                        {!! Form::submit('Realizar Inscripción (Reserva)', array( 'name'=> 'reserva', 'id'=> 'siguiente2', 'class' => 'btn btn-success col-md-12 siguiente')) !!}

                        {!! Form::hidden('pago-importe',0, array('id'=>'pago-importe')) !!}
                        {!! Form::hidden('pago-moneda',0, array('id'=>'pago-moneda')) !!}
                        {!! Form::hidden('pago-rate',0, array('id'=>'pago-rate')) !!}
                        {!! Form::hidden('pago-tipo',0, array('id'=>'pago-tipo')) !!}
                        {!! Form::hidden('pago-ovbkg',0, array('id'=>'pago-ovbkg')) !!}
                        {!! Form::hidden('pago-fecha',Carbon::now()->format('d/m/Y'), array('id'=>'pago-fecha')) !!}
                        {!! Form::hidden('mail_confirmacion',1, array('id'=>'mail_confirmacion')) !!}
                        {!! Form::hidden('area',$ficha->area_default, array('id'=>'area')) !!}
                        {!! Form::hidden('area_pagos',$ficha->area_pagos_default, array('id'=>'area_pagos')) !!}
                        {!! Form::hidden('area_reunion',$ficha->area_reunion_default, array('id'=>'area_reunion')) !!}

                        {!! Form::hidden('reserva-tipo',2, array('id'=>'reserva-tipo')) !!}
                    </div>
                        </div>
                </div>
                {!! Form::close() !!}

            @endif

        </div>
    </div>
</div>

{{-- modals --}}
@if($ficha->fase==4)
    @if( $ficha->curso->es_convocatoria_cerrada )
    <div class="modal fade" id="modalBooking">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Reserva Booking</h4>
            </div>
            <div class="modal-body">

                @if($plazas_vuelo < 1 || $plazas_alojamiento < 1 )
                    <strong>No hay plazas disponibles</strong>
                    <br>
                    <i>
                        Plazas disponibles Vuelo: {{$plazas_vuelo}}, Plazas disponibles Alojamientos: {{$plazas_alojamiento}}
                    </i>
                    <hr>

                    <button id='btn-cancelar' type="button" class="btn btn-warning pull-right">Cancelar Reserva</button>
                    <div class="clearfix"></div>
                    <hr>
                @endif

                <div id="booking-pago" style="display:none;">

                    <h4>Pago:</h4>

                    <div class="form-group row">
                        <div class="col-md-6">
                            @include('includes.form_input_text', [ 'campo'=> 'importe', 'texto'=> 'Importe'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'moneda', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ConfigHelper::default_moneda_id()])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'rate', 'texto'=> 'Cambio ['. ConfigHelper::default_moneda()->name .']', 'valor'=> 1 ])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_datetime', [ 'campo'=> 'pfecha', 'texto'=> 'Fecha', 'valor'=> Carbon::now()->format('d/m/Y')])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'select'=> ConfigHelper::getTipoPago()])
                    </div>

                    @if($plazas_vuelo <= 0 || $plazas_alojamiento <= 0 )
                        @include('includes.form_select', [ 'campo'=> 'ovbkg_id', 'texto'=> 'Autorizado por:', 'select'=> VCN\Models\User::transporte()->pluck('full_name','id')])
                        <button id='btn-pago-overbooking' type="button" class="btn btn-danger pull-right">Overbooking</button>
                    @else

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'mail-confirmacion', 'texto'=> 'Enviar email de confirmación a viajero/tutores', 'valor'=> 1])
                        </div>

                        <button id='btn-pago' type="button" class="btn btn-success pull-right">Aceptar</button>
                    @endif

                    <div class="clearfix"></div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Volver</button>
            </div>
        </div>
    </div>
    </div>

    @elseif( $ficha->curso->es_convocatoria_abierta )

    <div class="modal fade" id="modalBooking">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Reserva Booking</h4>
            </div>
            <div class="modal-body">

                <h4>Pago:</h4>
                <div class="form-group row">
                    <div class="col-md-6">
                        @include('includes.form_input_text', [ 'campo'=> 'importe', 'texto'=> 'Importe'])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_select', [ 'campo'=> 'moneda', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ConfigHelper::default_moneda_id()])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'rate', 'texto'=> 'Cambio ['. ConfigHelper::default_moneda()->name .']', 'valor'=> 1 ])
                    </div>
                </div>

                <div class="form-group">
                    @include('includes.form_input_datetime', [ 'campo'=> 'pfecha', 'texto'=> 'Fecha', 'valor'=> Carbon::now()->format('d/m/Y')])
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'select'=> ConfigHelper::getTipoPago()])
                </div>

                <div class="form-group">
                    @include('includes.form_checkbox', [ 'campo'=> 'mail-confirmacion', 'texto'=> 'Enviar email de confirmación a viajero/tutores', 'valor'=> 1])
                </div>

                <button id='btn-pago' type="button" class="btn btn-success pull-right">Aceptar</button>

                <div class="clearfix"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Volver</button>
            </div>
        </div>
    </div>
    </div>

    @elseif( $ficha->curso->es_convocatoria_multi )

    <div class="modal fade" id="modalBooking">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Reserva Booking</h4>
            </div>
            <div class="modal-body">

                @if(!$plazas)
                    <strong>No hay plazas disponibles</strong>
                    <br>
                    <i>
                        Plazas Disponibles:<br>
                        @foreach($ficha->convocatoria->getPlazas($ficha->id) as $kp=>$kv )
                            Semana {{$kp}}: {{$kv}}<br>
                        @endforeach
                    </i>
                    <hr>

                    <button id='btn-cancelar' type="button" class="btn btn-warning pull-right">Cancelar Reserva</button>
                    <div class="clearfix"></div>
                    <hr>
                @endif

                <div id="booking-pago" style="display:none;">

                    <h4>Pago:</h4>
                    <div class="form-group row">
                        <div class="col-md-6">
                            @include('includes.form_input_text', [ 'campo'=> 'importe', 'texto'=> 'Importe'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'moneda', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ConfigHelper::default_moneda_id()])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'rate', 'texto'=> 'Cambio ['. ConfigHelper::default_moneda()->name .']', 'valor'=> 1 ])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_datetime', [ 'campo'=> 'pfecha', 'texto'=> 'Fecha', 'valor'=> Carbon::now()->format('d/m/Y')])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'select'=> ConfigHelper::getTipoPago()])
                    </div>

                    @if(!$plazas)
                        @include('includes.form_select', [ 'campo'=> 'ovbkg_id', 'texto'=> 'Autorizado por:', 'select'=> VCN\Models\User::transporte()->pluck('full_name','id')])
                        <button id='btn-pago-overbooking' type="button" class="btn btn-danger pull-right">Overbooking</button>
                    @else
                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'mail-confirmacion', 'texto'=> 'Enviar email de confirmación a viajero/tutores', 'valor'=> 1])
                        </div>

                        <button id='btn-pago' type="button" class="btn btn-success pull-right">Aceptar</button>
                    @endif

                    <div class="clearfix"></div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Volver</button>
            </div>
        </div>
    </div>
    </div>

    @endif

@endif