<div class="portlet light bordered">
    <div class="portlet-title tabbable-line">
        <div class="caption font-blue-sharp">
            @lang('area.datoscampamento')
            {{-- <br><span class="badge badge-help">{{trans('area.datos_campamento', ['plataforma'=> $ficha->plataforma_name])}}</span> --}}
        </div>

        <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
    </div>
    <div class="portlet-body flip-scroll">

        <div class="alert alert-warning" role="alert">
          {{trans('area.datos_campamento', ['plataforma'=> $ficha->plataforma_name])}}
        </div>

        <hr>

        <?php
            $required = isset($required) ? $required : false;
        ?>

        @include('includes.form_div_bool', ['campo'=> 'campamento_enuresis', 'nombre'=> trans('area.campamento.enuresis'), 'required'=> $required ])
        @include('includes.form_div_bool', ['campo'=> "campamento_comer", 'nombre'=> trans('area.campamento.comer'), 'required'=> $required ])
        @include('includes.form_div_bool', ['campo'=> "campamento_bici", 'nombre'=> trans('area.campamento.bici'), 'required'=> $required ])
        @include('includes.form_div_bool', ['campo'=> 'campamento_nadar', 'nombre'=> trans('area.campamento.nadar'), 'required'=> $required ])
        @include('includes.form_div_bool', ['campo'=> "campamento_mareos", 'nombre'=> trans('area.campamento.mareos'), 'required'=> $required ])
        @include('includes.form_div_bool', ['campo'=> "campamento_insomnio", 'nombre'=> trans('area.campamento.insomnio'), 'required'=> $required ])
        @include('includes.form_div_bool', ['campo'=> "campamento_cansado", 'nombre'=> trans('area.campamento.cansado'), 'required'=> $required ])
        {{-- @include('includes.form_div_bool', ['campo'=> "campamento_enfermedad", 'nombre'=> trans('area.campamento.enfermedad'), 'required'=> $required ]) --}}
        {{-- @include('includes.form_div_bool', ['campo'=> "campamento_alergia", 'nombre'=> trans('area.campamento.alergia'), 'label'=> trans('area.campamento.alergia_label'), 'required'=> $required ]) --}}
        {{-- @include('includes.form_div_bool', ['campo'=> "campamento_medicacion", 'nombre'=> trans('area.campamento.medicacion'), 'label'=> trans('area.campamento.medicacion_label'), 'required'=> $required ]) --}}
        @include('includes.form_div_bool', ['campo'=> "campamento_enfermizo", 'nombre'=> trans('area.campamento.enfermizo'), 'label'=> trans('area.campamento.enfermizo_label'), 'required'=> $required ])
        @include('includes.form_div_bool', ['campo'=> "campamento_operaciones", 'nombre'=> trans('area.campamento.operaciones'), 'required'=> $required ])
        @include('includes.form_div_bool', ['campo'=> "campamento_discapacidad", 'nombre'=> trans('area.campamento.discapacidad'), 'label'=> trans('area.campamento.discapacidad_label'), 'required'=> $required ])
        @include('includes.form_div_bool', ['campo'=> "campamento_aprendizaje", 'nombre'=> trans('area.campamento.aprendizaje'), 'label'=> trans('area.campamento.aprendizaje_label'), 'required'=> $required ])
        @include('includes.form_div_bool', ['campo'=> "campamento_familiar", 'nombre'=> trans('area.campamento.familiar'), 'required'=> $required ])
        @include('includes.form_div_bool', ['campo'=> "campamento_miedo", 'nombre'=> trans('area.campamento.miedo'), 'required'=> $required ])

        {{-- <br>
        @include('includes.form_textarea', [ 'campo'=> 'campamento_notas', 'texto'=> trans('area.campamento.notas') ]) --}}

    </div>
</div>

{{-- @include('includes.script_boolean', ['campo'=> "campamento_enfermedad", 'required'=>$required ])
@include('includes.script_boolean', ['campo'=> "campamento_alergia", 'required'=>$required ])
@include('includes.script_boolean', ['campo'=> "campamento_alergia_1", 'required'=>0 ])
@include('includes.script_boolean', ['campo'=> "campamento_alergia_2", 'required'=>0 ])
@include('includes.script_boolean', ['campo'=> "campamento_alergia_3", 'required'=>0 ])
@include('includes.script_boolean', ['campo'=> "campamento_medicacion", 'required'=>$required ]) --}}
@include('includes.script_boolean', ['campo'=> "campamento_enfermizo", 'required'=>$required ])
@include('includes.script_boolean', ['campo'=> "campamento_operaciones", 'required'=>$required ])
@include('includes.script_boolean', ['campo'=> "campamento_discapacidad", 'required'=>$required ])
@include('includes.script_boolean', ['campo'=> "campamento_aprendizaje", 'required'=>$required ])
@include('includes.script_boolean', ['campo'=> "campamento_familiar", 'required'=>$required ])
@include('includes.script_boolean', ['campo'=> "campamento_miedo", 'required'=>$required ])