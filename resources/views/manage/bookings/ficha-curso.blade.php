
{{-- DESCUENTOS --}}

@if($descuento)
     <div class="descuento col-md-12">Descuento especial de {{ $descuento->name }}</div>
@endif

@if($ficha->descuento_early)
    <div class="descuento col-md-12">
        Descuento Early Bird {{$ficha->descuento_early->importe}} {{$ficha->descuento_early->moneda->name}}
        @if($ficha->curso->es_convocatoria_multi)
            por Semana
        @endif
    </div>
@endif

<table class="table">
    <tr>
        <td><strong>Nombre del Curso</strong></td>
        <td colspan="3">
            @if(!$ficha->convocatoria)
                {{$ficha->curso->name}} [SIN CONVOCATORIA]
            @else
                {{$ficha->curso->es_convocatoria_multi?$ficha->convocatoria->name:$ficha->curso->name}}
            @endif
            <a href="{{route('manage.bookings.cambiar', $ficha->id)}}" data-label="Cambiar curso"><i class="fa fa-retweet"></i></a>
            <br>
            <span id='booking-convocatoria'>
                {!!$ficha->convocatoria?"[Convocatoria: ".$ficha->convocatoria->name."]":''!!}
            </span>
        </td>
    </tr>

    <tr>
        <td><strong>Centro</strong></td>
        <td colspan="3">{{$ficha->centro->name}}</td>
    </tr>
    <tr>
        <td><strong>Pais</strong></td>
        <td>{{$ficha->centro->pais->name}}</td>
        <td><strong>Ciudad</strong></td>
        <td>{{$ficha->centro->ciudad->city_name}}</td>
    </tr>
    <tr>
        <td><strong>Categorías</strong></td>
        <td>{{$ficha->curso->categoria_name}} - {{$ficha->curso->subcategoria_name}} - {{$ficha->curso->subcategoria_detalle_name}}</td>
        <td><strong>Tipo Convocatoria</strong></td>
        <td>{{$ficha->curso->convocatoria_tipo}}</td>
    </tr>
    <tr>
        {{-- <td><strong>Unidad duración</strong></td>
        <td>{{$ficha->curso->duracion_name}}</td> --}}
        <td><strong>Moneda</strong></td>
        <td colspan="3">{{Session::get('vcn.moneda')}}</td>
    </tr>
</table>

{{-- CONVOCATORIA CERRADA --}}
@if( $ficha->curso->es_convocatoria_cerrada )

    <?php
        $bSemiopen = false;
    ?>

    <table class="table">
        <thead>
        <tr class="thead">
            <td>Fechas</td>
            <td>Duración</td>
            <td>Precio</td>
        </tr>
        </thead>
        <tbody>

        <tr><th colspan="3">Cerradas:</th></tr>
        @foreach($ficha->curso->convocatoriasCerradas->where('convocatory_semiopen',0) as $cc)

            <?php
                $plazas_alojamiento = "Disp.";
                if($cc->alojamiento)
                {
                    $pa = $cc->getPlazas($cc->alojamiento->id);
                    if($pa)
                    {
                       $plazas_alojamiento = $pa->plazas_disponibles;
                    }
                }
            ?>

            <tr>
                <td>
                    <i class="{{$ficha->convocatoria_class($cc->id)}} booking-ccerrada" id="ccerrada-{{$cc->id}}"
                        data-id='{{$cc->id}}' data-semiopen="{{$cc->convocatory_semiopen}}"
                        data-alojamiento="{{$cc->alojamiento_id?$cc->alojamiento_id:0}}"
                        data-start="{{$cc->convocatory_close_start_date}}"
                        data-end="{{$cc->convocatory_close_end_date}}"
                        data-day="{{$cc->convocatory_semiopen_start_day}}"></i>

                    {{$cc->name}}: Del {{$cc->start_date}} al {{$cc->end_date}}
                    <i>[Plazas: {{$plazas_alojamiento}}]</i>
                </td>
                <td>{{$cc->duracion}} {{$cc->getDuracionName($cc->duracion)}}</td>
                <td align='right'>
                    <span class="booking-ccerrada-precio" id="ccerrada-precio-{{$cc->id}}" data-moneda="{{$cc->convocatory_close_currency_id}}">

                        {{ ConfigHelper::parseMoneda($cc->precio, $cc->moneda_name) }}

                    </span>
                </td>
            </tr>

        @endforeach

        <tr><th colspan="3">SemiCerradas:</th></tr>

        @foreach($ficha->curso->convocatoriasCerradas->where('convocatory_semiopen',1) as $cc)

            <?php
                $bSemiopen = true;

                $plazas_alojamiento = "Disp.";
                if($cc->alojamiento)
                {
                    $pa = $cc->getPlazas($cc->alojamiento->id);
                    if($pa)
                    {
                       $plazas_alojamiento = $pa->plazas_disponibles;
                    }
                }
            ?>

            <tr>
                <td>
                    <i class="{{$ficha->convocatoria_class($cc->id)}} booking-ccerrada" id="ccerrada-{{$cc->id}}"
                        data-id='{{$cc->id}}' data-semiopen="{{$cc->convocatory_semiopen}}"
                        data-alojamiento="{{$cc->alojamiento_id?$cc->alojamiento_id:0}}"
                        data-start="{{$cc->convocatory_close_start_date}}"
                        data-end="{{$cc->convocatory_close_end_date}}"
                        data-day="{{$cc->convocatory_semiopen_start_day}}"></i>

                    {{$cc->name}}: Del {{$cc->start_date}} al {{$cc->end_date}}
                </td>
                <td>{{$cc->duracion}} {{$cc->getDuracionName($cc->duracion)}}</td>
                <td align='right'>
                    <span class="booking-ccerrada-precio" id="ccerrada-precio-{{$cc->id}}" data-moneda="{{$cc->convocatory_close_currency_id}}">

                        {{ ConfigHelper::parseMoneda($cc->precio, $cc->moneda_name) }}
                        <i>[Plazas: {{$plazas_alojamiento}}]</i>

                    </span>
                </td>
            </tr>

        @endforeach

        {{-- DESCUENTOS --}}
        @if($descuento)
        <tr>
            <td colspan="2">Descuento aplicado [{{$descuento->name}}]:</td>
            <td align='right'>
                <span>
                    {{ ConfigHelper::parseMoneda( $ficha->center_discount_amount, $ficha->curso_moneda) }}
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="2">Precio a pagar:</td>
            <td align='right'>
                <span>
                    {{ ConfigHelper::parseMoneda( ($ficha->course_total_amount-$ficha->center_discount_amount), $ficha->curso_moneda) }}
                </span>
            </td>
        </tr>
        @endif

        </tbody>
    </table>

        {{-- CONVOCATORIA SEMI CERRADA --}}
        @if($bSemiopen)

            <div id="booking-semiopen" class="form-group row">
                <div class="col-md-2">
                    {!! Form::label('course_start_date', 'Fecha Inicio (SemiCerrada)') !!}
                </div>
                <div class="col-md-3">
                    {!! Form::text('course_start_date', Carbon::parse($ficha->course_start_date)->format('d/m/Y'), array( 'id'=>'course_start_date', 'placeholder' => 'Fecha Inicio', 'class' => 'form-control', 'required'=>true)) !!}
                    <span class="help-block">{{ $errors->first('course_start_date') }}</span>
                </div>
                <div class="col-md-1">
                    <a id="booking-ccerrada-semi" href="#" class="btn btn-warning">Calcular</a>
                </div>
                <div class="col-md-5">
                    {!! Form::label('course_end_date', 'Fecha Fin (SemiCerrada)') !!}:
                    <span id='course_end_date'>{{Carbon::parse($ficha->course_end_date)->format('d/m/Y')}}</span>
                </div>
            </div>

        @endif


    {{-- ALOJAMIENTOS --}}
    <table class="table">
        <thead>
        <tr class="thead">
            <td>Alojamiento</td>
            <td align="right">Precio</td>
        </tr>
        </thead>
        <tbody>

        @if($ficha->convocatoria)
        @if($ficha->convocatoria->alojamiento_id==0)
                <tr>
                <td>
                    <i class="{{$ficha->alojamiento_class(0, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}} booking-ccerrada-alojamiento"
                        id="ccerrada-alojamiento-0"
                        data-id="0"
                        data-precio="0"
                        data-moneda="0"
                        >
                    </i>

                    Sin Alojamiento

                </td>

                <td align="right">
                    <span id="ccerrada-alojamiento-precio-0">
                        {{ConfigHelper::parseMoneda(0)}}
                    </span>
                </td>

                </tr>

            @foreach($ficha->curso->alojamientos as $alojamiento)

                <?php
                    $alojamiento_precio = $alojamiento->calcularPrecio(
                        Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y'),
                        Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y'), $ficha->duracion);

                    $plazas_alojamiento = "Disp.";
                    $pa = $ficha->convocatoria?$ficha->convocatoria->getPlazas($alojamiento->id):null;
                    if($pa)
                    {
                       $plazas_alojamiento = $pa->plazas_disponibles;
                    }
                ?>
                <tr>
                <td>
                    @if($alojamiento_precio)
                    <i class="{{$ficha->alojamiento_class($alojamiento->id, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}} booking-ccerrada-alojamiento"
                        id="ccerrada-alojamiento-{{$alojamiento->id}}"
                        data-id="{{$alojamiento->id}}"
                        data-precio="{{$alojamiento_precio['importe']}}"
                        data-moneda="{{$alojamiento_precio['moneda']}}"
                        >
                    </i>
                    @endif

                    {{-- {{$alojamiento->id}}: --}}

                    {{$alojamiento->name}} <i>( {{$alojamiento->tipo_name}} )</i>
                    <i>[Plazas: {{$plazas_alojamiento}}]</i>

                </td>

                <td align="right">
                    <span id="ccerrada-alojamiento-precio-{{$alojamiento->id}}">
                        @if($alojamiento_precio)
                            {{ConfigHelper::parseMoneda($alojamiento_precio['importe'],$alojamiento_precio['moneda'])}}
                        @else
                            - N/D-
                        @endif
                    </span>
                </td>

                </tr>

            @endforeach

        @else

            <?php
                //alojamiento asignado:
                $alojamiento = $ficha->convocatoria->alojamiento;

                $alojamiento_precio = $alojamiento->calcularPrecio(
                    Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y'),
                    Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y'), $ficha->duracion);

                $plazas_alojamiento = "Disp.";
                $pa = $ficha->convocatoria?$ficha->convocatoria->getPlazas($alojamiento->id):null;
                if($pa)
                {
                   $plazas_alojamiento = $pa->plazas_disponibles;
                }
            ?>
            <tr>
            <td>
                <i class="{{$ficha->alojamiento_class($alojamiento->id, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}} booking-ccerrada-alojamiento"
                    id="ccerrada-alojamiento-{{$alojamiento->id}}"
                    data-id="{{$alojamiento->id}}"
                    data-precio="{{$alojamiento_precio['importe']}}"
                    data-moneda="{{$alojamiento_precio['moneda']}}"
                    >
                </i>

                {{$alojamiento->name}} <i>( {{$alojamiento->tipo_name}} )</i>
                <i>[Plazas: {{$plazas_alojamiento}}]</i>

            </td>

            <td align="right">
                <span id="ccerrada-alojamiento-precio-{{$alojamiento->id}}">
                    {{ConfigHelper::parseMoneda($alojamiento_precio['importe'],$alojamiento_precio['moneda'])}}
                </span>
            </td>

            </tr>

        @endif

        @else
            <div class="note note-warning">
                <h4 class="block">Atención</h4>
                <p>Booking sin Convocatoria</p>
            </div>
        @endif

        </tbody>

    </table>

@endif

{{-- CONVOCATORIA ABIERTA --}}
@if( $ficha->curso->es_convocatoria_abierta )

    {{-- Se elije según fechas?? --}}

    <div class="hidden">{{-- OCULTO PA ES ALGO INTERNO --}}
    <table class="table">
        <thead>
        <tr class="thead">
            <td>Fechas</td>
        </tr>
        </thead>
        <tbody>

        @foreach($ficha->curso->convocatoriasAbiertas as $co)
            <tr>
                <td>
                    <i class="{{$ficha->convocatoria_class($co->id)}} booking-cabierta" id="cabierta-{{$co->id}}"
                        data-id='{{$co->id}}'
                        data-start="{{$co->convocatory_open_valid_start_date}}"
                        data-end="{{$co->convocatory_open_valid_end_date}}"
                        data-day="{{$co->convocatory_open_start_day}}"></i>

                    Del {{$co->start_date}} al {{$co->end_date}}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th colspan="4">Curso</th>
        </tr>
        <tr class="thead">
            <td>Fecha Inicio</td>
            <td>Fecha Fin</td>
            <td>Duración</td>
            <td align="right">Precio</td>
        </tr>
        </thead>
        <tbody>
        <tr>

            <td>
                {!! Form::text('booking-cabierta-fecha_ini', $ficha->course_start_date?Carbon::parse($ficha->course_start_date)->format('d/m/Y'):"", array( 'id'=>'booking-cabierta-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('booking-cabierta-fecha_ini') }}</span>
                <span id="booking-cabierta-alerta" class='booking-alerta'>{{isset($ficha->alertas['cabierta'])?$ficha->alertas['cabierta']:""}}</span>
                <span id="booking-cabierta-precio-detalle" class='booking-precio'>
                    @if($ficha->curso_precio_extra>0)
                        {{"Precio base: ". ConfigHelper::parseMoneda( ($ficha->course_price), $ficha->curso_moneda)}}
                        {{" + Extra temporada: ". ConfigHelper::parseMoneda( ($ficha->curso_precio_extra), $ficha->curso_moneda)}}
                    @endif
                </span>
            </td>
            <td>
                {!! Form::text('booking-cabierta-fecha_fin', $ficha->course_end_date?Carbon::parse($ficha->course_end_date)->format('d/m/Y'):"", array( 'id'=>'booking-cabierta-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
            </td>
            <td>
                {!! Form::select('booking-cabierta-semanas', $semanas, $ficha->duracion, array( 'id'=> 'booking-cabierta-semanas', 'class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('booking-cabierta-semanas') }}</span>
            </td>
            <td align='right'>
                <span class="booking-cabierta-precio" id="booking-cabierta-precio">
                    @if($ficha->convocatoria)
                        {{ ConfigHelper::parseMoneda( ($ficha->course_total_amount), $ficha->curso_moneda) }}
                    @endif
                </span>
            </td>
        </tr>

        {{-- DESCUENTOS --}}
        @if($descuento)
        <tr>
            <td colspan="3">Descuento aplicado [{{$descuento->name}}]:</td>
            <td align='right'>
                <span id="booking-cabierta-precio-dto">
                    {{-- ConfigHelper::parseMoneda( (($ficha->course_price * $ficha->duracion) * ($descuentos[0]/100)), $ficha->curso_moneda) --}}
                    {{ ConfigHelper::parseMoneda( $ficha->center_discount_amount, $ficha->curso_moneda) }}
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">Precio a pagar:</td>
            <td align='right'>
                <span id="booking-cabierta-precio-dtopagar">
                    {{ ConfigHelper::parseMoneda( ($ficha->course_total_amount-$ficha->center_discount_amount), $ficha->curso_moneda) }}
                </span>
            </td>
        </tr>
        @endif

        </tbody>
    </table>


    {{-- ALOJAMIENTOS --}}
    <table class="table">
        <thead>
        <tr>
            <th colspan="5">Alojamiento</th>
        </tr>
        <tr class="thead">
            <td>Alojamiento</td>
            <td>Fecha Inicio</td>
            <td>Fecha Fin</td>
            <td>Duración</td>
            <td class="col-md-2" align="right">Precio</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                {!! Form::select('booking-cabierta-alojamiento', [""=>"Sin Alojamiento"] + $ficha->curso->alojamientos->pluck('name','id')->toArray(), $ficha->accommodation_id,
                        array(
                            'id'=> 'booking-cabierta-alojamiento',
                            'class' => 'form-control',
                            'data-day' => ($ficha->alojamiento?$ficha->alojamiento->start_day:null),
                        )
                    )
                !!}
                <span class="help-block">{{ $errors->first('booking-alojamiento') }}</span>
                <span id="booking-alojamiento-alerta" class='booking-alerta'>{{isset($ficha->alertas['alojamiento'])?$ficha->alertas['alojamiento']:""}}</span>
                <span id="booking-alojamiento-precio-detalle" class='booking-precio'>
                    @if($ficha->alojamiento_precio_extra>0)
                        {{"Precio base: ". ConfigHelper::parseMoneda( ($ficha->accommodation_price), $ficha->curso_moneda)}}
                        {{" + Extra temporada: ". ConfigHelper::parseMoneda( ($ficha->alojamiento_precio_extra), $ficha->curso_moneda)}}
                    @endif
                </span>
            </td>
            <td>
                {!! Form::text('booking-alojamiento-fecha_ini', $ficha->accommodation_start_date?Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y'):"", array( 'id'=>'booking-alojamiento-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('booking-alojamiento-fecha_ini') }}</span>
            </td>
            <td>
                {!! Form::text('booking-alojamiento-fecha_fin', $ficha->accommodation_end_date?Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y'):"", array( 'id'=>'booking-alojamiento-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
            </td>
            <td>
                {!! Form::select('booking-alojamiento-semanas', $semanas, $ficha->accommodation_weeks, array( 'id'=> 'booking-alojamiento-semanas', 'class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('booking-alojamiento-semanas') }}</span>
            </td>
            <td align='right'>
                <span class="booking-alojamiento-precio" id="booking-alojamiento-precio">
                    {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
                </span>
            </td>
        </tr>
        </tbody>
    </table>

@endif


{{-- CONVOCATORIA MULTI --}}
@if( $ficha->curso->es_convocatoria_multi )

    <?php
        $plazas_multi = $ficha->convocatoria?$ficha->convocatoria->hayPlazasDisponibles($ficha->id):0;
    ?>

    @if(!$plazas_multi)
        <div class="note note-danger">
            <h4 class="block">Atención</h4>
            <p>No hay plazas disponibles</p>
        </div>
    @endif

    {{-- Fechas --}}
    <table class='table'>
        <caption>Fechas</caption>
        <thead>
            <tr>
                <th>Fecha Inicio</th>
                <th>Semanas</th>
                <th>Fecha Fin</th>
                <td align='right'>Precio</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class="col-md-4">
                        @include('includes.form_select', ['campo'=> 'booking-multi-fecha_ini', 'valor'=> $multi['mfdesde_id'], 'texto'=> null, 'select'=> $multi['mfdesde']])
                    </div>
                </td>
                <td>
                    @include('includes.form_select', ['campo'=> 'booking-multi-semanas', 'valor'=> $ficha->weeks, 'texto'=> null, 'select'=> $multi['msemanas']])
                </td>
                <td><span id="booking-multi-fecha_fin">{{$ficha->course_end_date?Carbon::parse($ficha->course_end_date)->format('d/m/Y'):"-"}}</span></td>
                <td align='right'><span id="booking-multi-precio">{{ ConfigHelper::parseMoneda($ficha->course_price, $ficha->convocatoria?$ficha->convocatoria->moneda_name:null) }}</span></td>
            </tr>
        </tbody>
    </table>

    {{-- Especialidades --}}
    <table class='table'>
        <caption>Especialidades</caption>
        <thead>
            <tr>
                <th>Semana</th>
                <th>Especialidad</th>
                <td align='right'>Precio</td>
            </tr>
        </thead>
        <tbody id='booking-multi-especialidades-table'>
            @foreach($ficha->multis as $esp)
            <tr>
                <td class='col-md-1'>
                    Semana {{$esp->n}} [{{$esp->semana->semana}}] <i>[Plazas: {{$ficha->convocatoria->getPlazasSemana($esp->semana->semana)}}]</i>
                </td>
                <td>
                    {!! Form::select('booking-multi-especialidad-'.$esp->id, $ficha->convocatoria->especialidadesBySemana($esp->semana->semana), $esp->especialidad_id, array( 'id'=> 'booking-multi-especialidad-'.$esp->id, 'class' => 'booking-multi-especialidad form-control', 'data-id'=> $esp->id, 'data-semana'=> $esp->n )) !!}
                </td>
                <td align='right' id="booking-multi-especialidad-precio-{{$esp->id}}">{{ ConfigHelper::parseMoneda($esp->precio, $ficha->convocatoria->moneda_name) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endif

@if( $ficha->curso->es_convocatoria_nula )
    <strong>Convocatoria/s SIN ESPECIFICAR</strong>
@endif

<hr>

@include('manage.bookings.ficha-curso-extras')

<hr>

{{-- TRANSPORTE --}}
@if( $ficha->curso->es_convocatoria_cerrada || $ficha->curso->es_convocatoria_abierta)

    <?php
        $transporte_vuelos = false;
        if($ficha->vuelos_web)
        {
            $v = $ficha->vuelos_web->first()->vuelo->transporte;
            if($v=="Vuelo" || $v==0 || is_null($v))
            {
                $transporte_vuelos = true;
            }
        }
    ?>

@include('includes.form_checkbox', [ 'campo'=> 'transporte_no', 'texto'=> 'NO necesito transporte' ])

<div class="booking-vuelo-table">
    <div id="booking-vuelo-table"
    @if($ficha->transporte_no)
        style="display:none;"
    @endif
>

    @if($ficha->vuelos)

        <table class="table vuelos">
            <thead>
            <tr class="thead">
                <th>
                    @if($transporte_vuelos)
                        Seleccionar Aeropuerto de Salida
                    @else
                        Seleccionar Ciudad de Salida
                    @endif

                    {{-- @if(!$ficha->vuelos)
                        [No hay vuelos para la Convocatoria o no hay Convocatoria seleccionada.]
                    @endif --}}
                </th>
                {{-- <td>Precio</td> --}}
            </tr>
            </thead>

            <tbody>
                @foreach( $ficha->vuelos as $vuelo )
                    <?php
                        $plazasv = $vuelo->vuelo->plazas_disponibles;
                    ?>
                    <tr>
                        <td>
                            <i class="{{$ficha->vuelo_class($vuelo->vuelo->id)}} booking-vuelo" id="vuelo-{{$vuelo->vuelo->id}}" data-id='{{$vuelo->vuelo->id}}' data-tipo='vuelo'></i>
                            {{$vuelo->vuelo->aeropuerto}} [{{$vuelo->vuelo->name}}] <i>[Plazas: {{$plazasv}}]</i>
                        </td>

                        {{-- <td></td> --}}

                    </tr>
                @endforeach
            </tbody>

        </table>

        @if($transporte_vuelos)
        <div id="div_transporte_requisitos">
            @include('includes.form_input_text', [ 'campo'=> 'transporte_requisitos', 'texto'=> 'Requisitos especiales para este vuelo (dieta, cambio de vuelo,...)'])
        </div>

        <br />
        @include('includes.form_checkbox', [ 'campo'=> 'transporte_recogida_bool',
            'texto'=> '¿Quieres que tramitemos un vuelo desde el aeropuerto más cercano a tu ciudad hasta el aeropuerto de salida de nuestro grupo?', 'valor'=>$ficha->transporte_recogida ])
        <div id="div_transporte_recogida_bool"
            @if(!$ficha->transporte_recogida)
                style="display:none;"
            @endif
        >

            @include('includes.form_input_text', [ 'campo'=> 'transporte_recogida',
                'texto'=> 'Indica el aeropuerto de salida y regreso más cercano a tu ciudad'])
        </div>
        @endif

    @endif

    @if(!$ficha->vuelos || $booking->curso->es_convocatoria_abierta)
        <hr>
        @include('includes.form_checkbox', [ 'campo'=> 'transporte_otro', 'texto'=> '¿Necesitas que te ayudemos a buscar tu vuelo?' ])
        <div id="div_transporte_otro"
            @if(!$ficha->transporte_otro)
                style="display:none;"
            @endif
        >
            @include('includes.form_input_text', [ 'campo'=> 'transporte_detalles', 'texto'=> 'Indica tu aeropuerto de salida'])
        </div>
    @endif

    </div>
</div>
@endif

<br><br>
@include('manage.bookings.ficha-total')

