<table class="table">
    <tr>
        <td>Nombre del Curso</td>
        <td>
            {{ $ficha->curso->es_convocatoria_multi?$ficha->convocatoria->name:$ficha->curso->name}}
            {!!$ficha->convocatoria?"<br>[Convocatoria: ".$ficha->convocatoria->name."]":''!!}
        </td>
    </tr>
    <tr>
        <td>Categorías</td>
        <td>{{$ficha->curso->categoria_name}} - {{$ficha->curso->subcategoria_name}} - {{$ficha->curso->subcategoria_detalle_name}}</td>
    </tr>
    <tr>
        <td>Centro</td>
        <td>{{$ficha->centro->name}}</td>
    </tr>
    <tr>
        <td>Pais</td>
        <td>{{$ficha->centro->pais->name}}</td>
    </tr>
    <tr>
        <td>Ciudad</td>
        <td>{{$ficha->centro->ciudad->city_name}}</td>
    </tr>
    <tr>
        <td>Tipo Convocatoria</td>
        <td>{{$ficha->curso->convocatoria_tipo}}</td>
    </tr>
    <tr>
        <td>Moneda</td>
        <td>{{Session::get('vcn.moneda')}}</td>
    </tr>
</table>

<table class="table">
    <caption>Convocatoria</caption>
    <thead>
        <tr class="thead">
            <td class="col-md-5">Fecha</td>
            <td>Duración</td>
            <td align="right">Precio</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                Del {{$ficha->curso_start_date}} al {{$ficha->curso_end_date}}
                <span id="booking-cabierta-alerta" class='booking-alerta'>{{isset($ficha->alertas['cabierta'])?$ficha->alertas['cabierta']:""}}</span>
                <span id="booking-cabierta-precio-detalle" class='booking-precio'>
                    @if($ficha->curso_precio_extra>0)
                        {{"Precio base: ". ConfigHelper::parseMoneda( ($ficha->course_price), $ficha->curso_moneda)}}
                        {{" + Extra temporada: ". ConfigHelper::parseMoneda( ($ficha->curso_precio_extra), $ficha->curso_moneda)}}
                    @endif
                </span>
            </td>
            <td>{{$ficha->weeks}} {{$ficha->convocatoria->getDuracionName($ficha->weeks)}}</td>
            <td align="right">
                {{ConfigHelper::parseMoneda($ficha->course_total_amount, $ficha->curso_moneda)}}
            </td>
        </tr>
    </tbody>
</table>

@if($ficha->curso->es_convocatoria_multi)
{{-- Especialidades --}}
<table class='table'>
    <caption>Especialidades</caption>
    <thead>
        <tr>
            <th>Semana</th>
            <th>Especialidad</th>
            <td align='right'>Precio</td>
        </tr>
    </thead>
    <tbody id='booking-multi-especialidades-table'>
        @foreach($ficha->multis as $esp)
        <tr>
            <td class='col-md-1'>Semana {{$esp->n}} [{{$esp->semana->semana}}]</td>
            <td>
                {{$esp->especialidad?$esp->especialidad->name:'-'}}
            </td>
            <td align='right' id="booking-multi-especialidad-precio-{{$esp->id}}">{{ ConfigHelper::parseMoneda($esp->precio, $ficha->convocatoria->moneda_name) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@if($ficha->alojamiento)
<table class="table">
    <caption>Alojamiento</caption>
    <thead>
        <tr class="thead">
            <td>Fechas</td>
            <td>Duración</td>
            <td align="right">Precio</td>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            {{$ficha->alojamiento?$ficha->alojamiento->name:"-"}} ::
            Del {{Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y')}}
            al {{Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y')}}
            <span id="booking-alojamiento-alerta" class='booking-alerta'>{{isset($ficha->alertas['alojamiento'])?$ficha->alertas['alojamiento']:""}}</span>
            <span id="booking-alojamiento-precio-detalle" class='booking-precio'>
                @if($ficha->alojamiento_precio_extra>0)
                    {{"Precio base: ". ConfigHelper::parseMoneda( ($ficha->accommodation_price), $ficha->curso_moneda)}}
                    {{" + Extra temporada: ". ConfigHelper::parseMoneda( ($ficha->alojamiento_precio_extra), $ficha->curso_moneda)}}
                @endif
            </span>
        </td>
        <td>
            <?php $u = $ficha->accommodation_weeks; ?>
            @if($ficha->curso->es_convocatoria_cerrada)
                {{$ficha->accommodation_weeks}} {{$ficha->convocatoria->getDuracionName($u)}}
            @else
                {{$ficha->accommodation_weeks}} {{$ficha->alojamiento->duracion_name?:$ficha->convocatoria->getDuracionName($u)}}
            @endif
        </td>
        <td align='right'>
            {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
        </td>
    </tr>
    </tbody>
</table>
@endif

<table class="table">
    <caption>Extras</caption>
    <thead>
        <tr class="thead">
            <td>Extra</td>
            <td>Unidad</td>
            <td>Precio</td>
        </tr>
    </thead>
    <tbody>

        @foreach($ficha->extrasCurso as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasCentro as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasAlojamiento as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasGenerico as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasCancelacion as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasOtros as $extra)
        <tr>
            <td>{{$extra->notas}}</td>
            <td>
                <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
                x{{$extra->unidades}}
            </td>
            <td align="right">{{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda_name) }}</td>
        </tr>
        @endforeach

    </tbody>
</table>

@if($ficha->curso->es_convocatoria_cerrada)
<table class="table">
    <caption>Vuelo</caption>
    <thead>
        <tr class="thead">
            <td>Vuelo</td>
            {{-- <td align="right">Precio</td> --}}
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            @if($ficha->vuelo)
                {{$ficha->vuelo->aeropuerto}} [{{$ficha->vuelo->name}}]
                <br><br>
                Requisitos especiales: {{$ficha->transporte_requisitos}}
                <br>
                Aporte desde/a: {{$ficha->transporte_recogida}}
            @else
                @if($ficha->transporte_no)
                    NO necesito transporte
                @elseif($ficha->transporte_otro)
                    Otro transporte: {{$ficha->transporte_detalles}}
                @else
                    -
                @endif
            @endif
        </td>
        {{-- <td align='right'></td> --}}
    </tr>
    </tbody>
</table>
@endif

@if( $ficha->descuentos->count() )
    <table class="table">
    <caption>Descuentos</caption>
    <tbody>
        @foreach($ficha->descuentos as $dto)
        <tr>
            <td>{{$dto->notas}}</td>
            <td>{{ConfigHelper::parseMoneda($dto->importe, $dto->moneda->name)}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

<table class="table">
    <caption>Viajero</caption>
    <thead>
        <tr>
            <td>Campo</td>
            <td align='left'>Valor</td>
        </tr>
    </thead>
    <tbody>
        <tr>
           <td>Idioma/s:</td>
           <td>{{$ficha->viajero->datos->idioma}} ({{$ficha->viajero->datos->idioma_nivel}})</td>
        </tr>

        <tr>
            <td>Viajero</td>
            <td>{{$ficha->viajero->full_name}}</td>
        </tr>

        <tr>
            <td>F.Nacimiento</td>
            <td>{{Carbon::parse($ficha->viajero->fechanac)->format('d/m/Y')}}</td>
        </tr>

        <tr>
            <td>Sexo</td>
            <td><i class="fa {{$ficha->viajero->sexo==1?'fa-male':'fa-female'}}"></i></td>
        </tr>

        <tr>
            <td>Nacionalidad</td>
            <td>{{$ficha->viajero->datos->nacionalidad}}</td>
        </tr>

        <tr>
            <td>Teléfono</td>
            <td>{{$ficha->viajero->phone}}</td>
        </tr>

        <tr>
            <td>Móvil</td>
            <td>{{$ficha->viajero->movil}}</td>
        </tr>

        <tr>
            <td>E-mail</td>
            <td>{{$ficha->viajero->email}}</td>
        </tr>

        @if(!$ficha->curso->es_menor)

            <tr><td colspan="2"><hr></td></tr>

            <tr>
                <td>Fumador</td>
                <td>{{$ficha->viajero->datos->fumador?'SI':'NO'}}</td>
            </tr>

            <tr>
                <td>Profesión</td>
                <td>{{$ficha->viajero->datos->profesion}}</td>
            </tr>

            <tr>
                <td>Empresa</td>
                <td>{{$ficha->viajero->datos->empresa}}</td>
            </tr>

        @endif

        <tr><td colspan="2"><hr></td></tr>

        <tr>
            <td>Alergias</td>
            <td>{{$ficha->viajero->datos->alergias}}</td>
        </tr>

        <tr>
            <td>Enfermedad</td>
            <td>{{$ficha->viajero->datos->enfermedad}}</td>
        </tr>

        <tr>
            <td>Medicación</td>
            <td>{{$ficha->viajero->datos->medicacion}}</td>
        </tr>

        <tr>
            <td>Tratamiento</td>
            <td>{{$ficha->viajero->datos->tratamiento}}</td>
        </tr>

        <tr>
            <td>Dieta Especial</td>
            <td>{{$ficha->viajero->datos->dieta}}</td>
        </tr>

        <hr>

        <tr>
            <td>Pasaporte</td>
            <td>
                {{$ficha->viajero->datos->pasaporte}} ({{$ficha->viajero->datos->pasaporte_pais}})
                Del {{$ficha->viajero->datos->pasaporte_emision!='0000-00-00'?Carbon::parse($ficha->viajero->datos->pasaporte_emision)->format('d/m/Y'):""}} al {{$ficha->viajero->datos->pasaporte_caduca!='0000-00-00'?Carbon::parse($ficha->viajero->datos->pasaporte_caduca)->format('d/m/Y'):""}}
            </td>
        </tr>

        <tr>
            <td>Dirección</td>
            <td>{{$ficha->viajero->datos->tipovia_name}} {{$ficha->viajero->datos->direccion}}, {{$ficha->viajero->datos->ciudad}} {{$ficha->viajero->datos->cp}} ({{$ficha->viajero->datos->provincia}}) [{{$ficha->viajero->datos->pais}}]</td>
        </tr>

        <hr>

        <tr>
            <td>¿Has realizado algún curso de idiomas en el extranjero con anterioridad?</td>
            <td>{{$ficha->viajero->datos->curso_anterior==""?"NO":$ficha->viajero->datos->curso_anterior}}</td>
        </tr>

        <tr>
            <td>¿Tienes algún título oficial?</td>
            <td>{{$ficha->viajero->datos->titulacion==""?"NO":$ficha->viajero->datos->titulacion}}</td>
        </tr>

        <tr>
            <td>Aficiones especiales, hobbies y deportes</td>
            <td>{{$ficha->viajero->datos->hobby}}</td>
        </tr>

        <tr>
            <td>Notas internas</td>
            <td>{{$ficha->viajero->datos->notas}}</td>
        </tr>

        <tr>
            <td>Observaciones (viajero)</td>
            <td>{{$ficha->notas}}</td>
        </tr>

        <tr>
            <td>Notas Booking</td>
            <td>{{$ficha->notas2}}</td>
        </tr>


    </tbody>
</table>

<table class="table table-responsive">
    <caption>TUTOR/ES</caption>
    <thead>
        <tr>
            <th>Relación</th>
            <th>Nombre</th>
            <th>E-mail</th>
            <th>Teléfono</th>
        </tr>
    </thead>
    <tbody>
        @foreach($ficha->viajero->tutores as $tutor)
        <tr>
            <td>{{ConfigHelper::getTutorRelacion($tutor->pivot->relacion)}}</td>
            <td>{{$tutor->full_name}}</td>
            <td>{{$tutor->email}}</td>
            <td>{{$tutor->phone}} {{$tutor->movil?" / ".$tutor->movil:''}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

@include('manage.bookings.ficha-total')

<hr>
<div class="form-group row">
    <div class="col-md-2">
        @include('includes.form_checkbox', [ 'campo'=> 'booking-area', 'texto'=> 'Área Cliente', 'valor'=> $ficha->area_default])
    </div>
    <div class="col-md-2">
        @include('includes.form_checkbox', [ 'campo'=> 'booking-area-pagos', 'texto'=> 'Área Cliente (Pagos)', 'valor'=> $ficha->area_pagos_default])
    </div>
    <div class="col-md-3">
        @include('includes.form_checkbox', [ 'campo'=> 'booking-area-reunion', 'texto'=> 'Área Cliente (Reunión)', 'valor'=> $ficha->area_reunion_default])
    </div>
</div>