<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-exclamation-triangle"></i> Incidencias

        {{-- <span class="pull-right"><a href="{{ route('manage.viajeros.tareas.nuevo', $viajero_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Tarea</a></span> --}}

    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
              'fecha'    => 'Fecha',
              'tipo'     => 'Tipo',
              'notas'    => 'Notas',
              'usuario'  => 'Creado',
              'asignado' => 'Asignado',
              'estado'  => 'Estado',
              'options' => ''

            ])
            ->setUrl( route('manage.bookings.incidencias.index', $booking_id) )
            ->setOptions('iDisplayLength', 100)
            ->setOptions(
              "aoColumnDefs", array(
                [ "bSortable" => false, "aTargets" => [6] ]
              )
            )
            ->render() !!}

    </div>
</div>

@include('manage.bookings.incidencias.edit')