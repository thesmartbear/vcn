<div class="portlet box {{$color_estado}}">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-tasks"></i> {{$filtro_name}}
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
            <a href="" class="reload reload-tasks"></a>
            <a href="" class="fullscreen"></a>
        </div>

    </div>
    <div class="portlet-body">

        {!! Datatable::table()
            ->setId('DttIncidencias')
            ->addColumn([
              'fecha'    => 'Fecha - Hora',
              'viajero'  => 'Viajero',
              'tipo'    => 'Tipo',
              'notas'    => 'Notas',
              'usuario'  => 'Creado',
              'asignado' => 'Asignado',
              'estado'  => 'Estado',
              'rating'  => 'Rating',
              'options' => ''

            ])
            ->setUrl( route('manage.bookings.incidencias.index.resumen', [0,$oficina_id]) )
            ->setOptions(
              "aoColumnDefs", array(
                [ "bSortable" => false, "aTargets" => [6] ]
                //[ "targets" => [2,3], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
              )
            )
            ->render() !!}

    </div>
</div>