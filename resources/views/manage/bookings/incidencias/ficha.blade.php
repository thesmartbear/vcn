@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.bookings.incidencias.ficha', $ficha) !!}
@stop

@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-exclamation-triangle fa-fw"></i> Incidencia :: <a href="{{route('manage.bookings.ficha',$booking_id)}}#incidencias">Booking {{$booking_id}}</a>
        &nbsp;&nbsp;<i class='fa {{$ficha->estado?'fa-check':'fa-clock-o'}}'></i> {{$ficha->estado?" [Resuelta]":" [Sin resolver]"}}

        <div class="pull-right">
            &nbsp;<a href="#" data-toggle='modal' data-target='#modalBookingIncidenciaLog' class="btn btn-warning btn-xs">
                <i class="fa fa-plus-circle"></i> Seguimiento
            </a>
            &nbsp;<a href="#" data-toggle='modal' data-target='#modalBookingIncidenciaTarea' class="btn btn-danger btn-xs">
                <i class="fa fa-plus-circle"></i> Nueva Tarea
            </a>

            @if(!$ficha->estado)
                &nbsp;<span class='pull-right'>
                    <a data-label='Resolver' href='{{ route('manage.bookings.incidencias.resolver',$ficha->id) }}' class='btn btn-success btn-xs'><i class='fa fa-check'></i></a>
                </span>
            @else
                &nbsp;<span class='pull-right'>
                    <a data-label='Marcar como no resuelta' href='{{ route('manage.bookings.incidencias.resolver',[$ficha->id,0]) }}' class='btn btn-danger btn-xs'><i class='fa fa-clock-o'></i></a>
                </span>
            @endif

        </div>

        {{-- <hr>
        {{Carbon::parse($ficha->fecha)->format('d/m/Y H:i')}} --}}

    </div>
    <div class="panel-body">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Incidencia</a></li>
            <li role="presentation"><a href="#tareas" aria-controls="tareas" role="tab" data-toggle="tab">Tareas</a></li>
            <li role="presentation"><a href="#seguimiento" aria-controls="seguimiento" role="tab" data-toggle="tab">Seguimiento</a></li>
            <li role="presentation"><a data-label="Historial" href="#historial" aria-controls="historial" role="tab" data-toggle="tab"><i class="fa fa-history"></i></a></li>
        </ul>


        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                {!! Form::model($ficha, array('route' => array('manage.bookings.incidencias.ficha',$ficha->id))) !!}

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_input_datetime', [ 'campo'=> 'incidencia_fecha', 'texto'=> 'Fecha', 'valor'=>Carbon::parse($ficha->fecha)->format('d/m/Y') ])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_datetime_hora', [ 'campo'=> 'incidencia_hora', 'texto'=> 'Hora', 'valor'=>Carbon::parse($ficha->fecha)->format('H:i') ])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'valor'=> $ficha->tipo, 'select'=> $tipos])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'asign_to', 'texto'=> 'Asignada a', 'valor'=> $ficha->asign_to, 'select'=> $asignados])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Resumen'])
                    </div>

                    <div class="form-group pull-right">
                        {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                        <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                    </div>

                {!! Form::close() !!}

            </div>


            <div role="tabpanel" class="tab-pane fade" id="tareas">
                @include('manage.bookings.incidencias.tareas.list', ['incidencia_id'=> $ficha->id])
            </div>

            <div role="tabpanel" class="tab-pane fade" id="seguimiento">
                @include('manage.bookings.incidencias.logs.list', ['incidencia_id'=> $ficha->id, 'todos'=> 0])
            </div>

            <div role="tabpanel" class="tab-pane fade" id="historial">
                @include('manage.bookings.incidencias.logs.list', ['incidencia_id'=> $ficha->id, 'todos'=> 1])
            </div>

        </div>

    </div>
</div>

@include('manage.bookings.incidencias.logs.new')
@include('manage.bookings.incidencias.tareas.new')


@stop