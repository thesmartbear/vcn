<div class="modal fade" id="modalBookingIncidencia">
<div class="modal-dialog modal-md">
    <div class="modal-content">

        {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.incidencias.ficha',0), 'role' => 'form', 'class' => '')) !!}

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class="fa fa-exclamation-triangle fa-fw"></i> Nueva Incidencia :: Booking {{$ficha->id}}</h4>
        </div>
        <div class="modal-body">

                {!! Form::hidden('booking_id', $ficha->id) !!}

                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_input_datetime', [ 'campo'=> 'incidencia_fecha', 'texto'=> 'Fecha', 'valor'=> Carbon::now()->format('d/m/Y')])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_datetime_hora', [ 'campo'=> 'incidencia_hora', 'texto'=> 'Hora', 'valor'=> Carbon::now()->format('H:i')])
                    </div>
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'valor'=> 0, 'select'=> ConfigHelper::getTipoIncidencia() ])
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'asign_to', 'texto'=> 'Asignada a', 'valor'=> $ficha->user_id, 'select'=> \VCN\Models\User::asignados()->get()->pluck('full_name', 'id')])
                </div>

                <div class="form-group">
                    @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Resumen', 'valor'=> ''])
                </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
        </div>
        {!! Form::close() !!}

    </div>
</div>
</div>