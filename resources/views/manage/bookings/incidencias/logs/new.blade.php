<div class="modal fade" id="modalBookingIncidenciaLog">
<div class="modal-dialog modal-md">
    <div class="modal-content">

        {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.incidencias.logs.ficha',0), 'role' => 'form', 'class' => '')) !!}

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class="fa fa-history fa-fw"></i> Nuevo Seguimiento :: Booking {{$ficha->booking->id}}</h4>
        </div>
        <div class="modal-body">

                {!! Form::hidden('incidencia_id', $ficha->id) !!}

                <div class="form-group">
                    {{-- @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'valor'=> 0, 'select'=> ConfigHelper::getTipoTarea() ]) --}}
                    <div class="col-md-6">
                        <label for="tipo">Enviado:</label><br>
                        @foreach(ConfigHelper::getTipoSeguimientoEnviado() as $tipo)
                            {!! Form::radio('tipo', $tipo) !!} {{$tipo}}<br>
                        @endforeach
                    </div>

                    <div class="col-md-6">
                        <label for="tipo">Recibido</label><br>
                        @foreach(ConfigHelper::getTipoSeguimientoRecibido() as $tipo)
                            {!! Form::radio('tipo', $tipo) !!} {{$tipo}}<br>
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'asign_to', 'texto'=> 'Asignado a', 'valor'=> auth()->user()->id, 'select'=> $asignados])
                </div>

                <div class="form-group">
                    @include('includes.form_textarea', [ 'campo'=> 'tarea_notas', 'texto'=> 'Resumen'])
                </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
        </div>
        {!! Form::close() !!}

    </div>
</div>
</div>