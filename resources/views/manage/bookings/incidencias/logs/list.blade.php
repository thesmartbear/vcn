    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-history"></i>
              @if($todos)
                Historial
              @else
                Seguimiento
              @endif

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'fecha'    => 'Fecha',
                  'tipo'     => 'Tipo',
                  'usuario'  => 'Actualizado',
                  'asignado' => 'Asignado',
                  'notas'    => 'Notas',
                  'options' => ''

                ])
                ->setUrl( route('manage.bookings.incidencias.logs.index', [$incidencia_id, $todos]) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [5] ]
                  )
                )
                ->render() !!}

        </div>
    </div>