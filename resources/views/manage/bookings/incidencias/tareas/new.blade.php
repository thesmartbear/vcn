<div class="modal fade" id="modalBookingIncidenciaTarea">
<div class="modal-dialog modal-md">
    <div class="modal-content">

        {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.incidencias.tareas.ficha',0), 'role' => 'form', 'class' => '')) !!}

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class="fa fa-history fa-fw"></i> Nueva Tarea :: Viajero {{$ficha->full_name}}</h4>
        </div>
        <div class="modal-body">

                {!! Form::hidden('incidencia_id', $ficha->id) !!}

                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_input_datetime', [ 'campo'=> 'tarea_fecha', 'texto'=> 'Fecha'])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_datetime_hora', [ 'campo'=> 'tarea_hora', 'texto'=> 'Hora'])
                    </div>
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'valor'=> 0, 'select'=> ConfigHelper::getTipoTarea() ])
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'asign_to', 'texto'=> 'Asignada a', 'valor'=> $ficha->asign_to, 'select'=> $asignados])
                </div>

                <div class="form-group">
                    @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Resumen', 'valor'=> ''])
                </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
        </div>
        {!! Form::close() !!}

    </div>
</div>
</div>