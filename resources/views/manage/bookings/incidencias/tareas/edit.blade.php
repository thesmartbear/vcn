<div class="modal fade" id="modalBookingTarea-edit">
<div class="modal-dialog modal-md">
    <div class="modal-content">

        {!! Form::open(array('method' => 'POST', 'url' => route('manage.viajeros.tareas.ficha',0), 'role' => 'form', 'class' => '')) !!}

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class="fa fa-history fa-fw"></i> Tarea :: Incidencia Booking <span id="titulo"></span> </h4>
        </div>
        <div class="modal-body">

                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_input_datetime', [ 'campo'=> 'tarea_fecha', 'texto'=> 'Fecha' ])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_datetime_hora', [ 'campo'=> 'tarea_hora', 'texto'=> 'Hora' ])
                    </div>
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'valor'=> 0, 'select'=> ConfigHelper::getTipoTarea() ])
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'asign_to', 'texto'=> 'Asignada a', 'select'=> $asignados])
                </div>

                <div class="form-group">
                    @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Resumen'])
                </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
        </div>
        {!! Form::close() !!}

    </div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function() {

    var $token = "{{ Session::token() }}";

    $('table.dataTable').on('click', 'a[href="#tarea-edit"]',function(e) {

        e.preventDefault();

        var $action = $(this).data('action');
        var $home = $(this).data('home');

        var $url = $(this).data('get');
        var $data = { '_token': $token };

          $.ajax({
            url: $url,
            type: 'GET',
            dataType : 'json',
            data: $data,
            success: function(data) { console.log(data);

                $div = "#modalBookingTarea-edit";

                $($div +" form").attr("action", $action);

                $($div +" input[name='tarea_fecha']").val(data.datos.fecha);
                $($div +" input[name='tarea_hora']").val(data.datos.hora);

                $($div +" #tipo").val(data.datos.tipo);
                $($div +" #notas").text(data.datos.notas);
                $($div +" #asign_to").val(data.datos.asign_to);

                $($div +" #titulo").html(data.incidencia.booking_id);

                $('.selectpicker').selectpicker('refresh');

                $($div).modal('show');

            },
            error: function(xhr, desc, err) {
              console.log(xhr.responseText);
              console.log("Details: " + desc + "\nError:" + err);

              if(xhr.status == 401) { location.reload(); }
            }
          }); // end ajax call
    });
});

</script>