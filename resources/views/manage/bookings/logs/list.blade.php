<div class="col-md-12"><br>

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-history"></i> Historial {{isset($filtro)?"($filtro)":""}}
        </div>
        <div class="panel-body">

            {{-- {{route('manage.bookings.logs.index', [$booking_id, isset($filtro)?$filtro:0 ])}} --}}

            {!! Datatable::table()
                ->setId("Dtt-logs-$booking_id". (isset($filtro)?"-".snake_case($filtro):"") )
                ->addColumn([
                  'fecha'    => 'Fecha',
                  'tipo'     => 'Tipo',
                  'usuario'  => 'Creado',
                  'notas'    => 'Notas',
                  'options' => ''

                ])
                ->setUrl( route('manage.bookings.logs.index', [$booking_id, isset($filtro)?$filtro:0 ]) )
                //->setOptions("order", [ [0, 'desc'] ])
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [3,4] ],
                    //[ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>