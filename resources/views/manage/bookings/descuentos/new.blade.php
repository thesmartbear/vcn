@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-credit-card fa-fw"></i>
                    <a href="{{route('manage.bookings.ficha',$booking->id)}}#descuentos">
                    Booking {{$booking->fecha}}
                    </a>
                    :: Descuento Nuevo
            </div>
            <div class="panel-body">

                {!! Form::open(array('route' => array('manage.bookings.descuentos.ficha', 0))) !!}

                    {!! Form::hidden('booking_id', $booking->id) !!}

                    <div class="form-group">
                        @include('includes.form_input_datetime', [ 'campo'=> 'fecha', 'texto'=> 'Fecha'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'importe', 'texto'=> 'Importe'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'currency_id', 'texto'=> 'Moneda', 'valor'=> 0, 'select'=> $monedas])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas'])
                    </div>

                    <div class="form-group pull-right">
                        {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                        <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                    </div>

                    <div class="clearfix"></div>

                {!! Form::close() !!}


            </div>
        </div>

@stop