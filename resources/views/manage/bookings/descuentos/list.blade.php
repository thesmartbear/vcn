<div class="col-md-12"><br>

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-book"></i> Descuentos Booking

            <span class="pull-right"><a href="{{ route('manage.bookings.descuentos.nuevo', $booking_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Descuento</a></span>

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'fecha'        => 'Fecha',
                  'usuario'        => 'Creado',
                  'importe'        => 'Importe',
                  'moneda'        => 'Moneda',
                  'notas'       => 'Notas',
                  'options'     => ''
                ])
                ->setUrl( route('manage.bookings.descuentos.index', $booking_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [5] ]
                  )
                )
                ->render() !!}

            @if(isset($add))
                <hr>

                @include('manage.descuentos.tipos.ficha-add', ['booking_id'=> $booking_id])
            @endif

        </div>
    </div>

</div>

