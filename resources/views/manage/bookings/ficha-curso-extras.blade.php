<?php
$max_duracion = $ficha->convocatoria?$ficha->convocatoria->duracion:0;
?>
{{-- EXTRAS CURSO --}}
<table class="table">
    <thead>
    <tr class="thead">
        <td>Extras Curso</td>
        <td>Unidad</td>
        <td>Precio</td>
    </tr>
    </thead>

    <tbody>
        {{-- EXTRAS CURSO OBLIGATORIOS --}}
        @foreach( $ficha->curso->extras->where('course_extras_required',1) as $extra )
            <tr>
                <td>
                    <i class="{{$ficha->extra_class_curso($extra->id)}} booking-extra obligatorio" id="extra-curso-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='curso'></i>
                    {{$extra->name}} <i>(obligatorio)</i>
                </td>

                @include('includes.bookings_td_unidades', ['prefix'=> 'extra-curso', 'tipo'=>0])
                @include('includes.bookings_td_precio', ['prefix'=> 'extra-curso-precio', 'tipo'=>0])

            </tr>
        @endforeach

        {{-- EXTRAS CURSO NO OBLIGATORIOS --}}
        @foreach( $ficha->curso->extras->where('course_extras_required',0) as $extra )
            <tr>
                <td>
                    <i class="{{$ficha->extra_class_curso($extra->id)}} booking-extra" id="extra-curso-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='curso'></i>
                    {{$extra->name}}
                </td>

                @include('includes.bookings_td_unidades', ['prefix'=> 'extra-curso', 'tipo'=>0])
                @include('includes.bookings_td_precio', ['prefix'=> 'extra-curso-precio', 'tipo'=>0])

            </tr>
        @endforeach
    </tbody>

</table>


{{-- EXTRAS CENTRO --}}
<table class="table">
    <thead>
    <tr class="thead">
        <td>Extras Centro</td>
        <td>Unidad</td>
        <td>Precio</td>
    </tr>
    </thead>

    <tbody>
        {{-- EXTRAS CENTRO OBLIGATORIOS --}}
        @foreach( $ficha->centro->extras->where('center_extras_required',1) as $extra )
            <tr>
                <td>
                    <i class="{{$ficha->extra_class_centro($extra->id)}} booking-extra obligatorio" id="extra-centro-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='centro'></i>
                    {{$extra->name}} <i>(obligatorio)</i>
                </td>

                @include('includes.bookings_td_unidades', ['prefix'=> 'extra-centro', 'tipo'=>1])
                @include('includes.bookings_td_precio', ['prefix'=> 'extra-centro-precio', 'tipo'=>1])

            </tr>
        @endforeach

        {{-- EXTRAS CENTRO NO OBLIGATORIOS --}}
        @foreach( $ficha->centro->extras->where('center_extras_required',0) as $extra )
            <tr>
                <td>
                    <i class="{{$ficha->extra_class_centro($extra->id)}} booking-extra" id="extra-centro-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='centro'></i>
                    {{$extra->name}}
                </td>

                @include('includes.bookings_td_unidades', ['prefix'=> 'extra-centro', 'tipo'=>1])
                @include('includes.bookings_td_precio', ['prefix'=> 'extra-centro-precio', 'tipo'=>1])

            </tr>
        @endforeach
    </tbody>

</table>

{{-- EXTRAS ALOJAMIENTO --}}
<table class="table">
    <thead>
    <tr class="thead">
        <td>Extras Alojamiento</td>
        <td>Unidad</td>
        <td>Precio</td>
    </tr>
    </thead>

    <tbody>
        {{-- EXTRAS ALOJAMIENTO OBLIGATORIOS --}}
        @if($ficha->alojamiento)
        @foreach( $ficha->alojamiento->cuotas->where('required',1) as $extra )
            <tr>
                <td>
                    <i class="{{$ficha->extra_class_alojamiento($extra->id)}} booking-extra obligatorio" id="extra-alojamiento-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='alojamiento'></i>
                    {{$extra->name}} <i>(obligatorio)</i>
                </td>

                @include('includes.bookings_td_unidades', ['prefix'=> 'extra-alojamiento', 'tipo'=>3])
                @include('includes.bookings_td_precio', ['prefix'=> 'extra-alojamiento-precio', 'tipo'=>3])

            </tr>
        @endforeach

        {{-- EXTRAS ALOJAMIENTO NO OBLIGATORIOS --}}
        @foreach( $ficha->alojamiento->cuotas->where('required',0) as $extra )
            <tr>
                <td>
                    <i class="{{$ficha->extra_class_alojamiento($extra->id)}} booking-extra" id="extra-alojamiento-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='alojamiento'></i>
                    {{$extra->name}}
                </td>

                @include('includes.bookings_td_unidades', ['prefix'=> 'extra-alojamiento', 'tipo'=>3])
                @include('includes.bookings_td_precio', ['prefix'=> 'extra-alojamiento-precio', 'tipo'=>3])

            </tr>
        @endforeach
        @endif
    </tbody>

</table>

{{-- EXTRAS GENERICOS --}}
<table class="table">
    <thead>
        <tr class="thead">
            <td>Extras Genéricos</td>
            <td>Unidad</td>
            <td>Precio</td>
        </tr>
    </thead>

    <tbody>
        {{-- EXTRAS GENERICOS OBLIGATORIOS --}}
        @foreach( $ficha->curso->extrasGenericos as $extra )

            @if($extra->generico->generic_required)

            <tr>
                <td>
                    <i class="{{$ficha->extra_class_generico($extra->id)}} booking-extra obligatorio" id="extra-generico-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='generico'></i>
                    {{$extra->name}} <i>(obligatorio)</i>
                </td>

                @include('includes.bookings_td_unidades', ['prefix'=> 'extra-generico', 'tipo'=>2])
                @include('includes.bookings_td_precio', ['prefix'=> 'extra-generico-precio', 'tipo'=>2])

            </tr>

            @endif

        @endforeach

        {{-- EXTRAS GENERICOS NO OBLIGATORIOS --}}
        @foreach( $ficha->curso->extrasGenericos as $extra )

            @if(!$extra->generico->generic_required)

                <tr>
                    <td>
                        <i class="{{$ficha->extra_class_generico($extra->id)}} booking-extra" id="extra-generico-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='generico'></i>
                        {{$extra->name}}
                    </td>

                    @include('includes.bookings_td_unidades', ['prefix'=> 'extra-generico', 'tipo'=>2])
                    @include('includes.bookings_td_precio', ['prefix'=> 'extra-generico-precio', 'tipo'=>2])

                </tr>

            @endif

        @endforeach

    </tbody>

</table>

{{-- SEGURO CANCELACIÓN AUTO --}}
@if(ConfigHelper::config('seguro'))

    <?php
    $seguro = $ficha->cancelacion_seguro;
    $texto = trans('area.booking.seguro_cancelacion');
    $seguro_name = "";
    if($seguro)
    {
        $seguro_name = $seguro->full_name;
        $pdf = $ficha->cancelacion_pdf;
        $seguro_name = $pdf ? "<a href='/$pdf' target='_blank'>$seguro_name</a>" : $seguro_name;
    }
    ?>

@if($ficha->es_terminado)
{!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.ficha.seguro', $ficha->id) , 'role' => 'form', 'class' => '')) !!}
<hr>
@include('includes.form_checkbox', [ 'campo'=> 'booking-seguro', 'valor'=> $ficha->cancelacion, 'texto'=> 'Seguro de cancelación' ])

    @if($ficha->cancelacion)
        @if( $ficha->cancelacion_seguro )

            <span id="booking-seguro-name">[{!! $seguro_name !!}]</span>

        @endif
    @endif

    <br><br>
    <div class="form-group">
        {!! Form::submit('Actualizar', array('class' => 'btn btn-success')) !!}
    </div>

    <hr>
    {!! Form::close() !!}

@else

    <hr>
    @include('includes.form_checkbox', [ 'campo'=> 'booking-seguro-ajax', 'valor'=> $ficha->cancelacion, 'texto'=> 'Seguro de cancelación' ])

    <span id="booking-seguro-name">
        @if($seguro)
            <span id="booking-seguro-name">[{!! $seguro_name !!}]</span>
        @endif
    </span>

@endif
@endif

{{-- EXTRAS OTROS --}}
@if($ficha->extras_otros->count()>0)
<table class="table">
    <thead>
        <tr class="thead">
            <td>Extras Otros</td>
            <td>Unidad</td>
            <td>Precio</td>
        </tr>
    </thead>

    <tbody>
        @foreach( $ficha->extrasOtros as $extra )

            <tr>
                <td>
                    <i class="booking-extra-otro {{$ficha->extra_class_otro($extra->id)}} obligatorio" id="extra-otro-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='otro'></i>
                    {{$extra->name}} [{{$extra->notas}}]
                </td>

                <td>1</td>
                <td align='right'>{{$extra->precio}} {{$extra->moneda_name}}</td>

            </tr>

        @endforeach

    </tbody>

</table>
@endif