<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Booking {{$ficha->id}}</title>

        {!! Html::style('assets/css/pdf.css') !!}
        {!! Html::style('assets/css/bootstrap.css') !!}
    
        <?php
            $p = $ficha->plataforma ?: 1;
            $sufijo = ConfigHelper::config('sufijo', $p);
            $web = ConfigHelper::config('web',$p);
        ?>

        <style>
            caption{
                @if($sufijo == 'bs')
                    color: #f1c40f;
                @elseif($sufijo == 'cic')
                    color: #3B6990;
                @elseif($sufijo == 'sf')
                    color: #24aab6;
                @else
                    color: #000000;
                @endif
            }
            h1{
                @if($sufijo == 'bs')
                    border-bottom: 1px solid #f1c40f;
                @elseif($sufijo == 'cic')
                    border-bottom: 1px solid #3B6990;
                @elseif($sufijo == 'sf')
                    border-bottom: 1px solid #24aab6;
                @else
                   border-bottom: 1px solid #CCCCCC;
                @endif
            }
            table.total thead td {
                @if($sufijo == 'bs')
                    color: #f1c40f;
                @elseif($sufijo == 'cic')
                    color: #3B6990;
                @elseif($sufijo == 'sf')
                    color: #24aab6;
                @else
                    color: #000000;
                @endif
            }
        </style>

    </head>
    <body>
        <div class="row">
            <div class="col-xs-12"><img style="width: 4.5cm; height: auto; margin-top: 4px;" class="pull-right" src="https://{{$web}}/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb', $p)}}" /></div>
        </div>

        <div class="row" style="margin-top: 1cm;">
            <div class="col-xs-12"><h1>Booking {{$ficha->id}}</h1></div>
        </div>

        <div class="row">
            <div class="col-xs-12">

                <table class="table nombrecurso">
                    <tr>
                        <td>@lang('area.curso')</td>
                        <td>{{ $ficha->curso->es_convocatoria_multi?$ficha->convocatoria->name:$ficha->curso->name}}</td>
                    </tr>
                    <tr>
                        <td>@lang('area.centro')</td>
                        <td>{{$ficha->centro->name}} ({{$ficha->centro->ciudad->city_name}}. {{$ficha->centro->pais->name}})</td>
                    </tr>
                </table>

                <table class="table table-condensed">
                    <caption>@lang('area.convocatoria')</caption>
                    <tbody>
                        <tr>
                            <td width="60%">
                                Del {{$ficha->curso_start_date}} al {{$ficha->curso_end_date}}
                                <span id="booking-cabierta-alerta" class='booking-alerta'>{{isset($ficha->alertas['cabierta'])?$ficha->alertas['cabierta']:""}}</span>
                                <span id="booking-cabierta-precio-detalle" class='booking-precio'>
                                @if($ficha->curso_precio_extra>0)
                                    <br />
                                        {{"Precio base: ". ConfigHelper::parseMoneda( ($ficha->course_price), $ficha->curso_moneda)}}
                                        {{" + Extra temporada: ". ConfigHelper::parseMoneda( ($ficha->curso_precio_extra), $ficha->curso_moneda)}}
                                    @endif
                                </span>
                            </td>
                            <td width="25%" align='center'>{{$ficha->weeks}} {{$ficha->convocatoria->getDuracionName($ficha->weeks)}}</td>
                            <td width="15%" align='right'>{{ConfigHelper::parseMoneda($ficha->course_total_amount, $ficha->curso_moneda)}}</td>
                        </tr>

                        {{-- DESCUENTOS --}}
                        <?php
                        $descuentos = $ficha->getDescuentos();
                        $descuento = null;
                        if($descuentos->count()>0)
                        {
                            $descuento = $descuentos->first();
                        }
                        ?>

                        @if($descuento)
                        <tr>
                            <td colspan="2">@lang('area.descuento') [{{$descuento->name}}]:</td>
                            <td align='right'>
                                <span>
                                    {{ ConfigHelper::parseMoneda( $ficha->center_discount_amount, $ficha->curso_moneda) }}
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">@lang('area.precio'):</td>
                            <td align='right'>
                                <span>
                                    {{ ConfigHelper::parseMoneda( ($ficha->course_total_amount-$ficha->center_discount_amount), $ficha->curso_moneda) }}
                                </span>
                            </td>
                        </tr>
                        @endif

                        @if($ficha->curso->es_convocatoria_cerrada)
                            <tr>
                                <td colspan="3">
                                    @if($ficha->vuelo)
                                        {{$ficha->vuelo->name}}
                                    @else
                                        @if($ficha->transporte_no)
                                            @lang('area.transporte_no')
                                        @elseif($ficha->transporte_otro)
                                            @lang('area.transporte_otro'): {{$ficha->transporte_detalles}}
                                        @else
                                            -
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endif

                    </tbody>
                </table>

                @if($ficha->curso->es_convocatoria_multi)
                {{-- Especialidades --}}
                <table class='table'>
                    <caption>@lang('area.especialidades')</caption>
                    <tbody id='booking-multi-especialidades-table'>
                        @foreach($ficha->multis as $esp)
                        <tr>
                            <td class='col-md-1'>@lang('area.semana') {{$esp->n}} [{{$esp->semana->semana}}]</td>
                            <td>
                                {{$esp->especialidad?$esp->especialidad->name:'-'}}
                            </td>
                            <td align='right' id="booking-multi-especialidad-precio-{{$esp->id}}">{{ ConfigHelper::parseMoneda($esp->precio, $ficha->convocatoria->moneda_name) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif

                @if($ficha->alojamiento)
                <table class="table">
                    <caption>@lang('area.alojamiento')</caption>
                    <tbody>
                    <tr>
                        <td width="60%"><b>{{$ficha->alojamiento?$ficha->alojamiento->name:"-"}}</b> ::
                            Del {{Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y')}}
                            al {{Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y')}}
                            <span id="booking-alojamiento-alerta" class='booking-alerta'>{{isset($ficha->alertas['alojamiento'])?$ficha->alertas['alojamiento']:""}}</span>
                            <span id="booking-alojamiento-precio-detalle" class='booking-precio'>
                                @if($ficha->alojamiento_precio_extra>0)
                                    <br />
                                    @lang('area.precio_base'): {{ConfigHelper::parseMoneda( ($ficha->accommodation_price), $ficha->curso_moneda)}}
                                    + @lang('area.precio_extra'): {{ConfigHelper::parseMoneda( ($ficha->alojamiento_precio_extra), $ficha->curso_moneda)}}
                                @endif
                            </span>
                        </td>
                        <td width="25%" align='center'>
                            <?php $u = $ficha->accommodation_weeks; ?>
                            {{$ficha->accommodation_weeks}} {{$ficha->alojamiento?$ficha->alojamiento->getDuracionName($u):$ficha->convocatoria->getDuracionName($u)}}
                        </td>
                        <td width="15%" align='right'>
                            {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
                        </td>
                    </tr>
                    </tbody>
                </table>
                @endif


                {{-- EXTRAS --}}
                @if(count($ficha->extras))
                    <table class="table">
                    <caption>@lang('area.extras')</caption>
                    <tbody>

                        @foreach($ficha->extras_curso as $extra)
                        <tr>
                            <td>{{$extra->name}}</td>
                            <td>
                                {{$extra->tipo_unidad_name}}

                                @if($extra->tipo_unidad)
                                    &nbsp;({{$extra->unidad_name}})
                                @endif
                                <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda->name) }}</i>
                                x{{$extra->unidades}}
                            </td>
                            <td align="right">
                                {{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda->name) }}
                            </td>
                        </tr>
                        @endforeach

                        @foreach($ficha->extras_centro as $extra)
                            @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        @endforeach

                        @foreach($ficha->extras_alojamiento as $extra)
                            @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        @endforeach

                        @foreach($ficha->extras_generico as $extra)
                            @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        @endforeach

                        @foreach($ficha->extras_cancelacion as $extra)
                            @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        @endforeach

                        @foreach($ficha->extrasOtros as $extra)
                        <tr>
                            <td>{{$extra->notas}}</td>
                            <td>
                                <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
                                x{{$extra->unidades}}
                            </td>
                            <td align="right">{{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda_name) }}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                @endif

                @if( $ficha->descuentos->count() )
                    <table class="table">
                    <caption>@lang('area.descuentos')</caption>
                    {{-- <thead>
                        <tr class="thead">
                            <th>Descuento</th>
                            <th>Precio</th>
                        </tr>
                    </thead> --}}
                    <tbody>
                        @foreach($ficha->descuentos as $dto)
                        <tr>
                            <td width="85%">{{$dto->notas}}</td>
                            <td width="15%" align="right">{{ConfigHelper::parseMoneda($dto->importe, $dto->moneda->name)}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif

                <div class="totales">
                    @include('manage.bookings.ficha-total')
                </div>


                <table class="table datosviajero">
                    <caption>@lang('area.viajero')</caption>
                    <tbody>
                        <tr>
                           <td>@lang('area.idiomas')</td>
                           <td>{{$ficha->datos->idioma}} ({{$ficha->viajero->datos->idioma_nivel}})</td>
                        </tr>

                        <tr>
                            <td>@lang('area.viajero')</td>
                            <td>{{$ficha->datos->full_name}}</td>
                        </tr>

                        <tr>
                            <td>@lang('area.nacimiento')</td>
                            <td>{{Carbon::parse($ficha->datos->fechanac)->format('d/m/Y')}}</td>
                        </tr>

                        <tr>
                            <td>@lang('area.sexo')</td>
                            <td>{{$ficha->datos->sexo==1?'Hombre':'Mujer'}}</td>
                        </tr>

                        <tr>
                            <td>@lang('area.nacionalidad')</td>
                            <td>{{$ficha->datos->nacionalidad}}</td>
                        </tr>

                        <tr>
                            <td>@lang('area.telefono')</td>
                            <td>{{$ficha->datos->phone}}</td>
                        </tr>

                        <tr>
                            <td>@lang('area.movil')</td>
                            <td>{{$ficha->datos->movil}}</td>
                        </tr>

                        <tr>
                            <td>@lang('area.email')</td>
                            <td>{{$ficha->datos->email}}</td>
                        </tr>

                        @if(!$ficha->curso->es_menor)

                            <tr class="separador">
                                <td>@lang('area.fumador')</td>
                                <td>{{$ficha->datos->fumador?'SI':'NO'}}</td>
                            </tr>

                            <tr>
                                <td>@lang('area.profesion')</td>
                                <td>{{$ficha->datos->profesion}}</td>
                            </tr>

                            <tr>
                                <td>@lang('area.empresa')</td>
                                <td>{{$ficha->datos->empresa}}</td>
                            </tr>

                        @endif

                        <tr class="separador">
                            <td>@lang('area.pasaporte')</td>
                            <td>
                                {{$ficha->datos->pasaporte}} ({{$ficha->datos->pasaporte_pais}})
                                Del {{$ficha->datos->pasaporte_emision!='0000-00-00'?Carbon::parse($ficha->datos->pasaporte_emision)->format('d/m/Y'):""}} al {{$ficha->datos->pasaporte_caduca!='0000-00-00'?Carbon::parse($ficha->datos->pasaporte_caduca)->format('d/m/Y'):""}}
                            </td>
                        </tr>

                        <tr>
                            <td>@lang('area.direccion')</td>
                            <td>{{$ficha->datos->tipovia_name}} {{$ficha->datos->direccion}}, {{$ficha->datos->ciudad}} {{$ficha->datos->cp}} ({{$ficha->datos->provincia}}) [{{$ficha->datos->pais}}]</td>
                        </tr>

                        <tr class="separador">
                            <td>@lang('area.inglesacademia')</td>
                            <td>{{($ficha->datos->ingles_academia)?$ficha->datos->ingles_academia:"NO"}}</td>
                        </tr>

                        <tr>
                            <td>@lang('area.inglesextranjero')</td>
                            <td>{{($ficha->datos->curso_anterior)?$ficha->datos->curso_anterior:"NO"}}</td>
                        </tr>

                        <tr>
                            <td>@lang('area.titulacion')</td>
                            <td>{{($ficha->datos->titulacion=="")?"NO":$ficha->datos->titulacion}}</td>
                        </tr>

                        <tr>
                            <td>@lang('area.hobby')</td>
                            <td>{{$ficha->datos->hobby}}</td>
                        </tr>

                        <tr>
                            <td>@lang('area.observaciones')</td>
                            <td>{!! $ficha->datos->notas !!}</td>
                        </tr>

                    </tbody>
                </table>

                <table class="table table-responsive tutores">
                    <caption>@lang('area.tutores')</caption>
                    <thead>
                        <tr>
                            <th>@lang('area.relacion')</th>
                            <th>@lang('area.nombre')</th>
                            <th>@lang('area.email')</th>
                            <th>@lang('area.telefono')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ficha->datos_congelados?$ficha->datos->tutores:$ficha->viajero->tutores as $tutor)
                        <tr>
                            <td>{{ConfigHelper::getTutorRelacion($ficha->datos_congelados?$tutor['pivot']['relacion']:$tutor->pivot->relacion)}}</td>
                            <td>{{$ficha->datos_congelados?($tutor['name']." ".$tutor['lastname']):$tutor->full_name}}</td>
                            <td>{{$ficha->datos_congelados?$tutor['email']:$tutor->email}}</td>
                            <td>
                            {{$ficha->datos_congelados?$tutor['phone']:$tutor->phone}}&nbsp;
                            {{ $ficha->datos_congelados ? $tutor['movil'] : ($tutor->movil ? (" / ".$ficha->datos_congelados ? $tutor['movil'] : $tutor->movil) : '') }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <?php
                    $bscd32 = false;
                    if( $ficha->curso->subcategoria_detalle )
                    {
                        if($ficha->curso->subcategoria_detalle->id == 32)
                        {
                            $bscd32 = true;
                        }
                    }
                ?>

                {{-- DATOS MEDICOS --}}
                <table class="table datosmedicos">
                    <caption>@lang('area.datosmedicos')</caption>
                    <tbody>
                        
                        <tr>
                            <td>@lang('area.alergia')</td>
                            <td>{{($ficha->datos->alergias=="")?"NO":$ficha->datos->alergias}}</td>
                        </tr>
                        <tr>
                            <td>@lang('area.enfermedad')</td>
                            <td>{{($ficha->datos->enfermedad=="")?"NO":$ficha->datos->enfermedad}}</td>
                        </tr>
                        <tr>
                            <td>@lang('area.medicacion')</td>
                            <td>{{($ficha->datos->medicacion=="")?"NO":$ficha->datos->medicacion}}</td>
                        </tr>
                        <tr>
                            <td>@lang('area.tratamiento')</td>
                            <td>{{($ficha->datos->tratamiento=="")?"NO":$ficha->datos->tratamiento}}</td>
                        </tr>
                        <tr>
                            <td>@lang('area.dieta')</td>
                            <td>{{($ficha->datos->dieta=="")?"NO":$ficha->datos->dieta}}</td>
                        </tr>
                        <tr>
                            <td>@lang('area.animales')</td>
                            <td>{{($ficha->datos->animales=="")?"NO":$ficha->datos->animales}}</td>
                        </tr>

                    </tbody>
                </table>

                {{-- DATOS CAMPAMENTO --}}
                <?php
                    $datos_campamento = ['campamento_nadar','campamento_enuresis','campamento_comer','campamento_bici',
                        'campamento_mareos','campamento_insomnio','campamento_cansado',
                        //'campamento_enfermedad','campamento_alergia','campamento_alergia_1','campamento_alergia_2','campamento_alergia_3','campamento_medicacion',
                        'campamento_enfermizo','campamento_operaciones','campamento_discapacidad',
                        'campamento_aprendizaje','campamento_familiar','campamento_miedo'
                    ];
                ?>
                @if($ficha->curso->categoria && $ficha->curso->categoria->es_info_campamento)
                    <table class="table datoscampamento">
                        <caption>@lang('area.datoscampamento')</caption>
                        <tbody>
                            @foreach($datos_campamento as $campo)
                                <?php
                                    $nom = substr($campo, 11);
                                    $nom = trans("area.campamento.".$nom);
                                ?>

                                <tr>
                                    <td>{{$nom}}</td>
                                    <td>
                                        
                                        <?php
                                            $valor = "NO";
                                            if(isset($ficha->datos_campamento[$campo]))
                                            {
                                                $valor = $ficha->datos_campamento[$campo];
                                                if( $valor === 1)
                                                {
                                                    $valor = "SI";
                                                }
                                                elseif( $valor === 0)
                                                {
                                                    $valor = "NO";
                                                }
                                                elseif( $valor === "")
                                                {
                                                    $valor = "NO";
                                                }
                                            }
                                        ?>

                                        {{$valor}}

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif

            </div>
        </div>
    </body>
</html>