@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-credit-card fa-fw"></i>
                    <a href="{{route('manage.bookings.ficha',$ficha->booking_id)}}#devoluciones">
                    Booking: ({{$ficha->booking->fecha}})
                    </a>
                    Devolución :: {{$ficha->fecha_dmy}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Status</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panels -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::model($ficha, array('route' => array('manage.bookings.devoluciones.ficha', $ficha->id))) !!}

                        {!! Form::hidden('booking_id', $ficha->booking_id) !!}

                        <div class="form-group">
                            @include('includes.form_input_datetime', [ 'campo'=> 'fecha', 'valor'=>$ficha->fecha_dmy, 'texto'=> 'Fecha'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'importe', 'texto'=> 'Importe'])
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'currency_id', 'texto'=> 'Moneda', 'select'=> $monedas])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas'])
                        </div>

                        <div class="form-group pull-right">
                            {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                            <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                        </div>

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'BookingDescuentosController',
                                'campos_text'=> [],
                                'campos_textarea'=> [],
                                'campos_textarea_basic'=> [ ['notas'=> 'Notas'], ]
                            ])

                    </div>

                </div>

            </div>
        </div>

@stop