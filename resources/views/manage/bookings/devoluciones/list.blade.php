<div class="col-md-12"><br>

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-recycle"></i> Devoluciones Booking

            <span class="pull-right"><a href="{{ route('manage.bookings.devoluciones.nuevo', $booking_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Devolucion</a></span>

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'fecha'        => 'Fecha',
                  'usuario'        => 'Creado',
                  'importe'        => 'Importe',
                  'moneda'        => 'Moneda',
                  'notas'       => 'Notas',
                  'options'     => ''
                ])
                ->setUrl( route('manage.bookings.devoluciones.index', $booking_id) )
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [5] ]
                  )
                )
                ->render() !!}

            @if(isset($add))
                <hr>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-plus-circle fa-fw"></i> Añadir Nuevo
                    </div>
                    <div class="panel-body">

                        {!! Form::open(array('route' => array('manage.bookings.devoluciones.ficha', 0))) !!}

                        {!! Form::hidden('booking_id', $booking_id) !!}

                        <div class="form-group row">
                            <div class="col-md-2">
                                @include('includes.form_input_datetime', [ 'campo'=> 'fecha', 'texto'=> 'Fecha', 'valor'=> Carbon::now()->format('d/m/Y')])
                            </div>
                            <div class="col-md-2">
                                @include('includes.form_input_text', [ 'campo'=> 'importe', 'texto'=> 'Importe'])
                            </div>

                            <div class="col-md-2">
                                @include('includes.form_select', [ 'campo'=> 'currency_id', 'texto'=> 'Moneda', 'select'=> $monedas,
                                'valor'=> ConfigHelper::default_moneda_id()])
                            </div>

                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'notas', 'texto'=> 'Notas'])
                            </div>

                            <div class="col-md-2">
                                {!! Form::label('Añadir') !!}<br>
                                {!! Form::submit('Crear', array('class' => 'btn btn-success')) !!}
                            </div>

                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            @endif

        </div>
    </div>

</div>

