@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tags fa-fw"></i> Nuevo CheckList Status Booking
            </div>
            <div class="panel-body">


                {!! Form::open(array('method' => 'POST', 'url' => route('manage.bookings.checklist.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'status_id', 'texto'=> 'Status', 'valor'=> 0, 'select'=> $statuses])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_number', [ 'campo'=> 'orden', 'texto'=> 'Orden', 'required'=>true])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'category_id', 'texto'=> 'Categoría', 'select'=> $categorias])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'subcategory_id', 'texto'=> 'Subcategoría', 'select'=> null])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'subcategory_det_id', 'texto'=> 'Detalle Subcategoría', 'select'=> null])
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            @include('includes.form_select2_multi', [ 'campo'=> 'curso_id[]', 'texto'=> 'Curso', 'select'=> $cursos])
                        </div>
                        <div class="col-md-6">
                            @include('includes.form_select2_multi', [ 'campo'=> 'centro_id[]', 'texto'=> 'Centro', 'select'=> $centros])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'CheckList'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select_multi', [ 'campo'=> 'seguimiento', 'texto'=> 'Seguimiento Tipo', 'select'=> ConfigHelper::getTipoSeguimiento() ])
                    </div>

                    <div class="form-group pull-right">
                        @include('includes.form_submit', [ 'id'=>'btn-add', 'permiso'=> 'checklist-bookings', 'texto'=> 'Añadir'])
                    </div>

                {!! Form::close() !!}

            </div>
        </div>

@include('includes.script_categoria')

@stop