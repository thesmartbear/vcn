@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tags fa-fw"></i> CheckLists Status Booking
                <span class="pull-right"><a href="{{ route('manage.bookings.checklist.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo CheckList</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'    => 'CheckList',
                      'orden'   => 'Orden',
                      'status'  => 'Status',
                      'categoria' => 'Categoria',
                      'subcategoria' => 'SubCategoria',
                      'detalle' => 'Detalle SubCat.',
                      'curso' => 'Cursos',
                      'centro' => 'Centros',
                      'seguimiento'=> 'Seguimiento',
                      'options' => ''
                    ])
                    ->setUrl(route('manage.bookings.checklist.index'))
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [9] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop