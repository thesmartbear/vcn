@extends('layouts.manage')

@section('content')
<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
          @if(isset($presupuesto))
            <i class="fa fa-book fa-fw"></i> Nuevo Presupuesto ({{$viajero_full_name}}) :: Seleccionar Curso
          @else
            <i class="fa fa-book fa-fw"></i> Nuevo Booking ({{$viajero_full_name}}) :: Seleccionar Curso
          @endif
        </div>
        <div class="panel-body">

            <?php
                $categorias = [0=>'Todas'] + \VCN\Models\Categoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
                $subcategorias = [0=>'Todas'] + \VCN\Models\Subcategoria::plataforma()->sortBy('name')->pluck('name','id')->toArray();
                // $subcategoriasdet = [0=>'Todas'] + SubcategoriaDetalle::plataforma()->sortBy('name')->pluck('name','id')->toArray();
                // $cursos = [0=>'Todos'] + Curso::plataforma()->where('activo_web',1)->sortBy('name')->pluck('name','id')->toArray();
            ?>

            <div class="panel panel-default">
                <div class="panel-heading">Filtro</div>
                <div class="panel-body">

                    <div class="form-group row">
                        <div class="col-md-4">
                        {!! Form::label('categorias', 'Categoría') !!}
                        @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                        <br>
                        {!! Form::select('categorias', $categorias, 0, array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                        {{-- @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos']) --}}
                        @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                        {{-- @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategoriasdet']) --}}
                        </div>

                        <div class="col-md-4">
                        {!! Form::label('subcategorias', 'SubCategoría') !!}
                        @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                        <br>
                        {!! Form::select('subcategorias', $subcategorias, 0, array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
                        {{-- @include('includes.script_filtros', ['filtro'=> 'subcategorias', 'destino'=> 'cursos']) --}}
                        {{-- @include('includes.script_filtros', ['filtro'=> 'subcategorias', 'destino'=> 'subcategoriasdet']) --}}
                        </div>
                    </div>

                    {{-- <div class="form-group">
                        <a id="filtrar" href="#filtrar" class="btn btn-info">Filtrar</a>
                    </div> --}}

                </div>
            </div>

            <hr>

            {!! Datatable::table()
                ->setId('DttCursos')
                ->addColumn([
                  'name'            => 'Curso',
                  'categoria'       => 'Categorías',
                  'proveedor'       => 'Proveedor',
                  'centro'          => 'Centro',
                  'convocatoria'    => 'Convocatoria',
                  'options'         => ''
                ])
                ->setUrl(route('manage.bookings.nuevo.cursos', [$viajero_id, $booking_id, isset($presupuesto)]))
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [4] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

</div>

<script type="text/javascript">
$(document).ready(function() {

    var $url = "{{ route('manage.bookings.nuevo.cursos', [$viajero_id, $booking_id, isset($presupuesto)]) }}";

    function filtrarDtt()
    {
        var $cat = $('#filtro-categorias').val();
        var $scat = $('#filtro-subcategorias').val();

        // $('#DttCursos').html("cargando...");

        var newUrl = $url + "?cat="+ $cat +"&scat="+ $scat;
        $('#DttCursos').DataTable().ajax.url( newUrl ).load();
    }


    $('#filtro-categorias').change(function(e){

        filtrarDtt()

    });

    $('#filtro-subcategorias').change(function(e){

        filtrarDtt()

    });

});
</script>
@stop