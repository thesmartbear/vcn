@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-credit-card fa-fw"></i>
                    <a href="{{route('manage.bookings.ficha',$ficha->booking_id)}}#pagos">
                    Booking: ({{$ficha->booking->fecha}})
                    </a>
                    Pago :: {{$ficha->fecha_dmy}}
            </div>
            <div class="panel-body">

                @if(!$ficha->enviado && !$ficha->es_reserva_inicial)

                {!! Form::model($ficha, array('route' => array('manage.bookings.pagos.ficha', $ficha->id))) !!}

                {!! Form::hidden('booking_id', $ficha->booking_id) !!}

                <div class="form-group">
                    @include('includes.form_input_datetime', [ 'campo'=> 'fecha', 'valor'=>$ficha->fecha_dmy, 'texto'=> 'Fecha'])
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        @include('includes.form_input_text', [ 'campo'=> 'importe_pago', 'texto'=> 'Importe'])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_select', [ 'campo'=> 'moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ConfigHelper::default_moneda_id()])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'rate', 'texto'=> 'Cambio ['. ConfigHelper::default_moneda()->name .']', 'valor'=> 1 ])
                    </div>
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'required'=> 'required', 'select'=> ConfigHelper::getTipoPago()])
                </div>

                <div class="form-group">
                    @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas'])
                </div>
                
                <div class="form-group">
                    @include('includes.form_checkbox', [ 'campo'=> 'avisar', 'texto'=> 'Enviar aviso a cliente', 'checked'=> true ])
                </div>

                <div class="form-group pull-right">
                    {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                    <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                </div>

                {!! Form::close() !!}

                @elseif($ficha->enviado)

                    <div class="content">
                        <div class="alert alert-info" role="alert">
                            No puede editar un pago enviado / Reserva inicial.
                        </div>
                    </div>
                
                @endif

            </div>
        </div>

<script type="text/javascript">
$(document).ready( function() {

    var $cambios = <?php echo json_encode($monedas_cambio); ?>

    $('#moneda').change(function(e) {

        var $id = $(this).val();

        $('#rate').val($cambios[$id]);

    });

});
</script>

@stop