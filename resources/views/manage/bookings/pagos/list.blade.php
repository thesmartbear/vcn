<div class="col-md-12"><br>
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-book"></i> Pagos Booking

        @if($ficha->es_refund)
            <a class="btn btn-warning" href="{{ route('manage.bookings.devolucion',$ficha->id)}}">Devolucion completa</a>
        @endif

        <div class="pull-right">
            @if(!$ficha->pagado)
                <a class="btn btn-warning btn-xs" href="{{ route('manage.bookings.monedas',$ficha->id)}}"><i class="fa fa-money"></i> Modificar Tipo de cambio</a>
            @endif

            @if(ConfigHelper::canEdit('notapago'))
                @if( $ficha->status_id == ConfigHelper::config('booking_status_prebooking') || $ficha->status_id == ConfigHelper::config('booking_status_presalida') || $ficha->status_id == ConfigHelper::config('booking_status_fuera') )
                    @if($ficha->area_pagos && $ficha->saldo_pendiente)
                        @if(!$ficha->es_notapago35_generada)
                            <a data-label='Nota Pago' href='{{ route('manage.bookings.notapago35', [$ficha->id]) }}' class='btn btn-warning btn-xs'><i class='fa fa-money'></i></a>
                        @else
                            <a data-label='Generar nueva Nota Pago' href='{{ route('manage.bookings.notapago35', [$ficha->id]) }}' class='btn btn-info btn-xs'><i class='fa fa-money'></i></a>
                            <a data-label='Enviar Última Nota Pago' href='{{ route('manage.bookings.notapago35.mail', [$ficha->id]) }}' class='btn {{$ficha->es_notapago35_enviada?'btn-success':'btn-warning'}} btn-xs'><i class='fa fa-money'></i> <i class='fa fa-envelope'></i></a>
                        @endif
                    @else

                    @endif
                @else

                @endif
            @else

            @endif

        </div>

    </div>
    <div class="panel-body">

        {!! Datatable::table()
                ->addColumn([
                    'fecha'     => 'Fecha',
                    'tipo'      => 'Tipo',
                    'importe'       => 'Importe',
                    'importe_pago'  => 'Importe pago',
                    'notas'         => 'Notas',
                    'avisado'       => 'Avisado',
                    'options'       => ''
                    ])
                ->setUrl( route('manage.bookings.pagos.index', $booking_id) )
                ->setOptions(
                    "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [5] ]
                        )
                    )
                ->render() !!}

        <hr>

        @if(!$ficha->pagado)
        {!! Form::open( array('id'=>'frm-pago-pagado', 'method'=> "GET", 'route' => array('manage.bookings.pagado',$ficha->id) )) !!}
            {!! Form::submit('Pago Completado', array('class'=> 'btn btn-success')) !!}
        {!! Form::close() !!}
        <hr>
        @endif


        @if($ficha->pagado)
            <a href="{{ route('manage.bookings.pagado', [$ficha->id, 0]) }}" class="btn btn-warning">Reabrir pagos</a>
            <hr>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-credit-card"></i> Nuevo Pago
            </div>
            <div class="panel-body">

                <div class="col-md-7">

                    {!! Form::open(array('id'=>'frm-pago', 'route' => array('manage.bookings.pagos.ficha', 0))) !!}

                        {!! Form::hidden('booking_id', $ficha->id) !!}

                        <div class="form-group">
                            @include('includes.form_input_datetime', [ 'campo'=> 'fecha', 'texto'=> 'Fecha Pago', 'valor'=> ''])
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                @include('includes.form_input_text', [ 'campo'=> 'importe_pago', 'texto'=> 'Importe *'])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_select', [ 'campo'=> 'moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ConfigHelper::default_moneda_id()])
                            </div>
                            <div class="col-md-3">
                                @include('includes.form_input_text', [ 'campo'=> 'rate', 'texto'=> 'Cambio ['. ConfigHelper::default_moneda()->name .']', 'valor'=> 1 ])
                            </div>
                        </div>

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo *', 'select'=> ConfigHelper::getTipoPago()])
                        </div>

                        <div class="form-group">
                            @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas', 'valor'=>''])
                        </div>

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'avisar', 'texto'=> 'Enviar aviso a cliente', 'checked'=> true ])
                        </div>

                        <div class="form-group pull-right">
                            {!! Form::submit('Añadir', array('class' => 'btn btn-success', 'id'=>'btn-pago-add')) !!}
                            <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                        </div>

                        <div class="clearfix"></div>

                    {!! Form::close() !!}

                </div>

                <div class="col-md-5">
                    <table class="table table-bordered table-striped table-condensed">
                        <caption>DESGLOSE</caption>
                        <thead>
                            <tr>
                                <th width='60%'>Concepto</th>
                                <th colspan='2'>Importe</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>Curso</td>
                                <td colspan='2' align="right">
                                    {{ConfigHelper::parseMoneda($ficha->course_total_amount, $ficha->curso_moneda)}}
                                </td>
                            </tr>

                            <tr>
                                <td>Alojamiento</td>
                                <td colspan='2' align='right'>
                                    {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
                                </td>
                            </tr>

                            <tr><th colspan="3">Extras</th></tr>

                            @foreach($ficha->extrasCurso as $extra)
                                @include('includes.bookings_tr_extra', ['extra'=> $extra])
                            @endforeach

                            @foreach($ficha->extrasCentro as $extra)
                                @include('includes.bookings_tr_extra', ['extra'=> $extra])
                            @endforeach

                            @foreach($ficha->extras_alojamiento as $extra)
                                @include('includes.bookings_tr_extra', ['extra'=> $extra])
                            @endforeach

                            @foreach($ficha->extrasGenerico as $extra)
                                @include('includes.bookings_tr_extra', ['extra'=> $extra])
                            @endforeach

                            @foreach($ficha->extrasCancelacion as $extra)
                                @include('includes.bookings_tr_extra', ['extra'=> $extra])
                            @endforeach

                            @foreach($ficha->extrasOtros as $extra)
                            <tr>
                                <td>{{$extra->name}} [{{$extra->notas}}]</td>
                                <td>
                                    <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
                                    x{{$extra->unidades}}
                                </td>
                                <td align="right">{{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda_name) }}</td>
                            </tr>
                            @endforeach

                            <?php 
                                $precioTotal = $ficha->precio_total;
                                // dd($precioTotal);
                            ?>

                            <tr>
                                <td>SubTotal</td>
                                <td colspan='2' align="right">
                                    @foreach($precioTotal['subtotal_txt'] as $subtotal)
                                        {{$subtotal}}<br>
                                    @endforeach
                                </td>
                            </tr>

                            @if($ficha->divisa_variacion && !$ficha->promo_cambio_fijo)
                            <tr>
                                <td>Variación Divisa</td>
                                <td colspan='2' align="right">{{ConfigHelper::parseMoneda($ficha->divisa_variacion)}}</td>
                            </tr>
                            @endif

                            @if($precioTotal['descuento_especial'] > 0)
                            <tr>
                                <td>Descuento especial:</td>
                                <td colspan='2' align="right">{{$precioTotal['descuento_especial_txt']}}</td>
                            </tr>
                            @endif

                            @if($ficha->descuentos->count()>0)
                            <tr>
                                <th colspan="3">Descuentos</th></tr>
                                @foreach($ficha->descuentos as $descuento)
                                <tr>
                                    <td>{{$descuento->notas}}</td>
                                    <td colspan='2' align="right">{{ ConfigHelper::parseMoneda($descuento->importe, $descuento->moneda->name) }}</td>
                                </tr>
                                @endforeach
                            @endif

                            @if($ficha->devoluciones->count()>0)
                            <tr>
                                <th colspan="3">Devoluciones</th></tr>
                                @foreach($ficha->devoluciones as $devolucion)
                                <tr>
                                    <td>{{$devolucion->notas}}</td>
                                    <td colspan='2' align="right">{{ ConfigHelper::parseMoneda($devolucion->importe, $devolucion->moneda->name) }}</td>
                                </tr>
                                @endforeach
                            @endif

                            <tr>
                                <th>
                                    TOTAL
                                    @if($ficha->es_cancelado || $ficha->es_refund || $ficha->pagado)
                                        <i class='fa fa-lock'></i>
                                    @endif
                                </th>
                                <td colspan='2' align="right"><strong>{{$precioTotal['total_txt']}}</strong></td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>PENDIENTE</strong>
                                    <i>({{$precioTotal['total']}} - pagos:{{$ficha->pagos->sum('importe')}}) = </i></td>
                                <td colspan='2' align="right">
                                    <h4><strong>
                                        {{ ConfigHelper::parseMoneda(($precioTotal['total']-$ficha->pagos->sum('importe')), Session::get('vcn.moneda')) }}
                                    </strong></h4>
                                </td>
                            </tr>

                            @if(!$ficha->es_cancelado && !$ficha->es_refund)
                            <tr>
                                <td colspan="3">
                                    Cambio aplicado:<br>
                                    @foreach($ficha->monedas_usadas_txt as $mu)
                                        <small>{{$mu}}</small><br>
                                    @endforeach
                                </td>
                            </tr>
                            @endif



                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>
</div>
</div>

@if($ficha->saldo_pendiente>0)
<script type="text/javascript">
$(document).ready(function() {

    $('#frm-pago-pagado').on('submit', function(e, options) {
        options = options || {};

        if ( !options.lots_of_stuff_done ) {
            e.preventDefault();

            var saldo = "{{$ficha->saldo_pendiente}}" + " {{ConfigHelper::moneda()}}";
            var $pregunta = "El saldo restante por pagar es de "+ saldo +". <br>¿Seguro que quieres marcar esta inscripción como pagada?";

            bootbox.confirm($pregunta, function(result) {
                if(result)
                {
                    $(e.currentTarget).trigger('submit', { 'lots_of_stuff_done': true });
                }
            });
        }
    });
});
</script>
@endif

{{-- @if($ficha->es_cancelado || $ficha->es_refund)
<script type="text/javascript">
$(document).ready(function() {

    $('#btn-pago-add').click(function(e) {
        e.preventDefault();

        var $pregunta = "Inscripción cancelada. ¿Estás seguro que quieres entrar un pago?";

        bootbox.confirm($pregunta, function(result) {
            if(result)
            {
                $('#frm-pago').submit();
            }
        });
    });
});
</script>
@endif --}}

<script type="text/javascript">
$(document).ready(function() {
    // $('#btn-factura').click(function(e){

    //     e.preventDefault();

    //     bootbox.confirm("Este booking ya ha sido facturado. ¿Seguro que quiere emitir un Abono y una nueva factura?", function(result) {
    //         if(result)
    //         {
    //             var ruta = $('#btn-factura').data('ruta');
    //             window.location = ruta;
    //         }
    //     });

    // });

    $('#btn-pago-add').click(function(e) {
        e.preventDefault();

        var continuar = true;
        @if($ficha->es_cancelado || $ficha->es_refund)
        {
            var $pregunta = "Inscripción cancelada. ¿Estás seguro que quieres entrar un pago?";

            bootbox.confirm($pregunta, function(result) {
                if(!result)
                {
                    continuar = false;
                }
            });
        }
        @endif

        if(!continuar)
        {
            return;
        }

        $fecha = $('#fecha').val();
        if(!validaFechaPago($fecha))
        {
            var pregunta = "¿Es correcta la fecha "+ $fecha +" ?"
            bootbox.confirm(pregunta, function(result) {
                if(result)
                {
                    $('#frm-pago').submit();
                }
            });
        }
        else
        {
            $('#frm-pago').submit();
        }
    });

    var $cambios = <?php echo json_encode($monedas_cambio); ?>

    $('#frm-pago #moneda_id').change(function(e) {

        var $id = $(this).val();

        $('#rate').val($cambios[$id]);

    });

});
</script>