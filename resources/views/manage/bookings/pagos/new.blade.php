@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-credit-card fa-fw"></i>
                    <a href="{{route('manage.bookings.ficha',$booking->id)}}#pagos">
                    Booking {{$booking->fecha}}
                    </a>
                    :: Pago Nuevo
            </div>
            <div class="panel-body">

                {!! Form::open(array('route' => array('manage.bookings.pagos.ficha', 0))) !!}

                    {!! Form::hidden('booking_id', $booking->id) !!}

                    <div class="form-group">
                        @include('includes.form_input_datetime', [ 'campo'=> 'fecha', 'texto'=> 'Fecha Pago'])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            @include('includes.form_input_text', [ 'campo'=> 'importe_pago', 'texto'=> 'Importe'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'moneda_id', 'texto'=> 'Moneda', 'select'=> $monedas, 'valor'=> ConfigHelper::default_moneda_id()])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'rate', 'texto'=> 'Cambio ['. ConfigHelper::default_moneda()->name .']', 'valor'=> 1 ])
                        </div>
                    </div>


                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'required'=> 'required', 'select'=> ConfigHelper::getTipoPago()])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas','valor'=>''])
                    </div>
                    
                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'avisar', 'texto'=> 'Enviar aviso a cliente', 'checked'=> true ])
                    </div>

                    <div class="form-group pull-right">
                        {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                        <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                    </div>

                    <div class="clearfix"></div>

                {!! Form::close() !!}


            </div>
        </div>

@stop