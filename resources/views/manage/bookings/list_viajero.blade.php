
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-pencil-square"></i> Inscripciones
            <span class="pull-right"><a href="{{ route('manage.bookings.nuevo', $viajero_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Inscripción</a></span>
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->setId('DttBookings')
                ->addColumn([
                  'fecha_reserva'   => 'Fecha Booking',
                  'fecha'           => 'Fecha Inicio',
                  'curso'           => 'Convocatoria/Curso',
                  'duracion'        => 'Ud. duración',
                  'status'          => 'Status',
                  'options' => ''

                ])
                ->setUrl( route('manage.bookings.index.viajero', $viajero_id) )
                ->setOptions(
                  "columnDefs", array(
                    [ "sortable" => false, "targets" => [5] ],
                    [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
                ->setOptions(
                    "rowCallback", "function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                        if(aData['status'].indexOf('badge-incidencias')>0) { $('td', nRow).addClass('td-st-incidencias'); }
                        if(aData['status'].indexOf('fa-exclamation-triangle')>0) { $('td', nRow).addClass('td-st-incidencia'); }
                     }"
                )
                ->render() !!}

        </div>
    </div>

@include('includes.script_checklist_status', ['booking'=> true])