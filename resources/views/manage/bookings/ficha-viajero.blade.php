<div class="row">
    <div class="col-sm-6 col-xs-6">
        <div class="input-group">
            <span class="input-group-addon">Idioma</span>
            @include('includes.form_select', [ 'campo'=> 'idioma', 'select'=> ConfigHelper::getIdioma(), 'ficha'=>$ficha->datos])
        </div>
    </div>
    <div class="col-sm-6 col-xs-6">
        <div class="input-group">
            <span class="input-group-addon">Nivel de Idioma</span>
            @include('includes.form_select', [ 'campo'=> 'idioma_nivel', 'select'=> ConfigHelper::getIdiomaNivel(), 'ficha'=>$ficha->datos])
        </div>
    </div>
</div>
<br />
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">Nombre</span>
            @include('includes.form_input_text', [ 'campo'=> 'name', 'ficha'=>$ficha->datos ])
        </div>
    </div>
</div>
<br />
<div class="row">
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
        <span class="input-group-addon">Apellido 1</span>
        @include('includes.form_input_text', [ 'campo'=> 'lastname', 'ficha'=>$ficha->datos ])
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
        <span class="input-group-addon">Apellido 2</span>
        @include('includes.form_input_text', [ 'campo'=> 'lastname2', 'ficha'=>$ficha->datos])
        </div>
    </div>
</div>
<br />

<div class="row">
    <div class="col-md-4">
        <div class="input-group">
            <span class="input-group-addon">Fecha Nacimiento</span>
            @include('includes.form_input_datetime', [ 'campo'=> 'fechanac', 'valor'=> $ficha->datos->fechanac?Carbon::parse($ficha->datos->fechanac)->format('d/m/Y'):'', 'ficha'=>$ficha->datos ])
        </div>
    </div>

    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon">Sexo:</span>
            <div class="pull-left form-radio">
                <label class="radio-inline">
                    {!! Form::radio('sexo', '1', ($ficha->datos->sexo==1)) !!} <i class="fa fa-male"></i>
                </label>
                <label class="radio-inline">
                    {!! Form::radio('sexo', '2', ($ficha->datos->sexo==2)) !!} <i class="fa fa-female"></i>
                </label>
            </div>
            <span class="help-block">{{ $errors->first('sexo') }}</span>
        </div>
    </div>

    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">Nacionalidad:</span>
            @include('includes.form_select', [ 'campo'=> 'nacionalidad', 'ficha'=> $ficha->datos, 'select'=> [""=>""] + \VCN\Models\Nacionalidad::pluck('name','name')->toArray()])
        </div>
    </div>
</div>

<br>
<div class="row">
    <div class="col-md-3 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">Teléfono:</span>
            @include('includes.form_input_text', [ 'campo'=> 'phone', 'ficha'=>$ficha->datos ])
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">Movil:</span>
            @include('includes.form_input_text', [ 'campo'=> 'movil', 'ficha'=>$ficha->datos ])
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">E-mail:</span>
            @include('includes.form_input_text', [ 'campo'=> 'email', 'ficha'=>$ficha->datos, 'esbooking'=>1 ])
        </div>
    </div>
</div>

<br>
<div class="row">
    <div class="col-md-3 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">DNI:</span>
            @include('includes.form_input_text', [ 'campo'=> 'documento', 'ficha'=>$ficha->datos ])
        </div>
    </div>
</div>

<hr>
<div class="form-group row">
    <div class="col-md-3">
        @include('includes.form_input_text', [ 'campo'=> 'rs_instagram', 'texto'=> 'Instagram', 'ficha'=> $ficha->datos])
    </div>
</div>



<hr>

@if(!$ficha->curso->es_menor)

    <br>
    <div class="row">

        <div class="col-md-3">
            <div class="input-group">
                <span class="input-group-addon">Fumador:</span>
                <div class="pull-left form-radio">
                    <label class="radio-inline">
                        {!! Form::radio('fumador', '1', ($ficha->datos->fumador==1),array('required' => 'required')) !!} SI
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('fumador', '0', ($ficha->datos->fumador==0),array('required' => 'required')) !!} NO
                    </label>
                </div>
                <span class="help-block">{{ $errors->first('fumador') }}</span>
            </div>
        </div>

        <div class="col-md-3">
            <div class="input-group">
                <span class="input-group-addon">Profesión:</span>
                @include('includes.form_input_text', [ 'campo'=> 'profesion', 'ficha'=>$ficha->datos ])
            </div>
        </div>

        <div class="col-md-3">
            <div class="input-group">
                <span class="input-group-addon">Empresa:</span>
                @include('includes.form_input_text', [ 'campo'=> 'empresa', 'ficha'=>$ficha->datos ])
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group">
                <span class="input-group-addon">Tel. Empresa:</span>
                @include('includes.form_input_text', [ 'campo'=> 'empresa_phone', 'ficha'=>$ficha->datos ])
            </div>
        </div>
    </div>

@else
    @if($ficha->viajero->tutores->count()<1)
        <div class="alert alert-danger" role="alert" style="text-align:justify;">
            NOTA: Debe tener al menos un tutor con un email válido.
        </div>
    @endif
@endif

<hr>

<table class="table table-responsive">
    <caption>TUTOR/ES
        @if(!$ficha->datos_congelados)
        <span class="pull-right"><a href="#" data-toggle='modal' data-target='#modalTutores' class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Tutor</a></span>
        @include('includes.script_tutores', ['viajero_id' => $ficha->viajero_id])
        @endif
    </caption>
    <thead>
        <tr>
            <th>Relación</th>
            <th>Nombre</th>
            <th>E-mail</th>
            <th>Teléfono</th>
            <th>DNI</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $tutores = $ficha->datos_congelados ? $ficha->datos->tutores : $ficha->viajero->tutores;
            $tutores = $tutores ?: $ficha->viajero->tutores;
        ?>
        @foreach($tutores as $tutor)
        <tr>
            <td>{{ConfigHelper::getTutorRelacion($ficha->datos_congelados?$tutor['pivot']['relacion']:$tutor->pivot->relacion)}}</td>
            <td>
            <a href="{{route('manage.tutores.ficha', ($ficha->datos_congelados?$tutor['id']:$tutor->id))}}" target="_blank">
                {{$ficha->datos_congelados?($tutor['name']." ".$tutor['lastname']):$tutor->full_name}}
            </a>
            </td>
            <td>{{$ficha->datos_congelados?$tutor['email']:$tutor->email}}</td>
            <td>{{$ficha->datos_congelados ? $tutor['phone'] : $tutor->phone}} / {{$ficha->datos_congelados ? $tutor['movil'] : $tutor->movil}}</td>
            <td>{{$ficha->datos_congelados?$tutor['nif']:$tutor->nif}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

<hr>
<div class="alert alert-danger" role="alert" style="text-align:justify;">
    NOTA: Cualquier tipo de trastorno o incidencia física o psíquica, así como su historial y tratamiento, debe ser notificado por escrito a {{ConfigHelper::config('propietario')==2?'viatges@iccic.edu':'British Summer'}}. Del mismo modo, debe ser notificada por escrito cualquier otra circunstancia del participante que pueda afectar al desarrollo del programa.
</div>

<br>
<div class="input-group">
    <span class="input-group-addon">Alergia:</span>
    <div class="pull-left form-radio">
        <label class="radio-inline">
            {!! Form::radio('alergias_bool', '1', ($ficha->datos->alergias)) !!} SI
        </label>
        <label class="radio-inline">
            {!! Form::radio('alergias_bool', '0', (!$ficha->datos->alergias)) !!} NO
        </label>
    </div>

    <div id="alergias_div" style="display:none;">
        <br>
        @include('includes.form_textarea', [ 'campo'=> 'alergias', 'ficha'=>$ficha->datos ])
        @include('includes.form_textarea', [ 'campo'=> 'alergias2', 'ficha'=>$ficha->datos, 'novisible'=> true, 'help'=> 'Información en inglés que aparecerá tal cual en las notas booking para el proveedor.' ])
        <div class="form-group">
            {!! Form::submit('Actualizar', array('class' => 'btn btn-danger', 'name'=> 'submit_notas')) !!}
        </div>
    </div>
    <span class="help-block">{{ $errors->first('alergias') }}</span>
</div>

<br>
<div class="input-group">
    <span class="input-group-addon">Enfermedad:</span>
    <div class="pull-left form-radio">
        <label class="radio-inline">
            {!! Form::radio('enfermedad_bool', '1', ($ficha->datos->enfermedad)) !!} SI
        </label>
        <label class="radio-inline">
            {!! Form::radio('enfermedad_bool', '0', (!$ficha->datos->enfermedad)) !!} NO
        </label>
    </div>

    <div id="enfermedad_div" style="display:none;">
        <br>
        @include('includes.form_textarea', [ 'campo'=> 'enfermedad', 'ficha'=>$ficha->datos ])
        @include('includes.form_textarea', [ 'campo'=> 'enfermedad2', 'ficha'=>$ficha->datos, 'novisible'=> true, 'help'=> 'Información en inglés que aparecerá tal cual en las notas booking para el proveedor.' ])
        <div class="form-group">
            {!! Form::submit('Actualizar', array('class' => 'btn btn-danger', 'name'=> 'submit_notas')) !!}
        </div>
    </div>
    <span class="help-block">{{ $errors->first('enfermedad') }}</span>
</div>

<br>
<div class="input-group">
    <span class="input-group-addon">Medicación:</span>
    <div class="pull-left form-radio">
        <label class="radio-inline">
            {!! Form::radio('medicacion_bool', '1', ($ficha->datos->medicacion)) !!} SI
        </label>
        <label class="radio-inline">
            {!! Form::radio('medicacion_bool', '0', (!$ficha->datos->medicacion)) !!} NO
        </label>
    </div>

    <div id="medicacion_div" style="display:none;">
        <br>
        @include('includes.form_textarea', [ 'campo'=> 'medicacion', 'ficha'=>$ficha->datos ])
        @include('includes.form_textarea', [ 'campo'=> 'medicacion2', 'ficha'=>$ficha->datos, 'novisible'=> true, 'help'=> 'Información en inglés que aparecerá tal cual en las notas booking para el proveedor.' ])
        <div class="form-group">
            {!! Form::submit('Actualizar', array('class' => 'btn btn-danger', 'name'=> 'submit_notas')) !!}
        </div>
    </div>
    <span class="help-block">{{ $errors->first('medicacion') }}</span>
</div>

<br>
<div class="input-group">
    <span class="input-group-addon">Tratamiento:</span>
    <div class="pull-left form-radio">
        <label class="radio-inline">
            {!! Form::radio('tratamiento_bool', '1', ($ficha->datos->tratamiento)) !!} SI
        </label>
        <label class="radio-inline">
            {!! Form::radio('tratamiento_bool', '0', (!$ficha->datos->tratamiento)) !!} NO
        </label>
    </div>

    <div id="tratamiento_div" style="display:none;">
        <br>
        @include('includes.form_textarea', [ 'campo'=> 'tratamiento', 'ficha'=>$ficha->datos ])
        @include('includes.form_textarea', [ 'campo'=> 'tratamiento2', 'ficha'=>$ficha->datos, 'novisible'=> true, 'help'=> 'Información en inglés que aparecerá tal cual en las notas booking para el proveedor.' ])
        <div class="form-group">
            {!! Form::submit('Actualizar', array('class' => 'btn btn-danger', 'name'=> 'submit_notas')) !!}
        </div>
    </div>
    <span class="help-block">{{ $errors->first('tratamiento') }}</span>
</div>

<br>
<div class="input-group">
    <span class="input-group-addon">Dieta especial:</span>
    <div class="pull-left form-radio">
        <label class="radio-inline">
            {!! Form::radio('dieta_bool', '1', ($ficha->datos->dieta)) !!} SI
        </label>
        <label class="radio-inline">
            {!! Form::radio('dieta_bool', '0', (!$ficha->datos->dieta)) !!} NO
        </label>
    </div>

    <div id="dieta_div" style="display:none;">
        <br>
        @include('includes.form_textarea', [ 'campo'=> 'dieta', 'ficha'=>$ficha->datos ])
        @include('includes.form_textarea', [ 'campo'=> 'dieta2', 'ficha'=>$ficha->datos, 'novisible'=> true, 'help'=> 'Información en inglés que aparecerá tal cual en las notas booking para el proveedor.' ])
        <div class="form-group">
            {!! Form::submit('Actualizar', array('class' => 'btn btn-danger', 'name'=> 'submit_notas')) !!}
        </div>
    </div>
    <span class="help-block">{{ $errors->first('dieta') }}</span>
</div>

<br>
<div class="input-group">
    <span class="input-group-addon">¿Te molestan los animales?</span>
    <div class="pull-left form-radio">
        <label class="radio-inline">
            {!! Form::radio('animales_bool', '1', ($ficha->datos->animales)) !!} SI
        </label>
        <label class="radio-inline">
            {!! Form::radio('animales_bool', '0', (!$ficha->datos->animales)) !!} NO
        </label>
    </div>

    <div id="animales_div" style="display:none;">
        <br>
        @include('includes.form_textarea', [ 'campo'=> 'animales', 'ficha'=>$ficha->datos ])
    </div>
    <span class="help-block">{{ $errors->first('animales') }}</span>
</div>

<hr>

<div class="row">

    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon">Pasaporte:</span>
            @include('includes.form_input_text', [ 'campo'=> 'pasaporte', 'ficha'=>$ficha->datos ])
        </div>
    </div>

    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon">País expedición:</span>
            @include('includes.form_select_pais', [ 'campo'=> 'pasaporte_pais', 'ficha'=>$ficha->datos ])
        </div>
    </div>

    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon">F.Emisión:</span>
            @include('includes.form_input_datetime', [ 'campo'=> 'pasaporte_emision', 'ficha'=>$ficha->datos])
        </div>
    </div>

    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon">F.Caducidad:</span>
            @include('includes.form_input_datetime', [ 'campo'=> 'pasaporte_caduca', 'ficha'=>$ficha->datos])
        </div>
    </div>
</div>

<hr>

<div class="input-group row">
    <div class="col-md-3">
        <span class="input-group-addon">Tipo:</span>
        @include('includes.form_select', [ 'campo'=> 'tipovia', 'select'=> ConfigHelper::getTipoVia(), 'ficha'=>$ficha->tipovia ])
    </div>
    <div class="col-md-9">
        <span class="input-group-addon">Dirección:</span>
        @include('includes.form_textarea', [ 'campo'=> 'direccion', 'placeholder'=> 'Dirección', 'ficha'=>$ficha->datos ])
        <span class="help-block">{{ $errors->first('direccion') }}</span>
    </div>
</div>

<br>

<div class="row">

    <div class="col-md-4">
        <div class="input-group">
            <span class="input-group-addon">Ciudad:</span>
            @include('includes.form_input_text', [ 'campo'=> 'ciudad', 'ficha'=>$ficha->datos ])
        </div>
    </div>

    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon">Provincia:</span>
            @include('includes.form_select', [ 'campo'=> 'provincia', 'select'=>[""=>""]+ \VCN\Models\Provincia::pluck('name','name')->toArray(), 'ficha'=>$ficha->datos])
        </div>
    </div>

    <div class="col-md-2">
        <div class="input-group">
            <span class="input-group-addon">CP:</span>
            @include('includes.form_input_text', [ 'campo'=> 'cp', 'ficha'=>$ficha->datos ])
        </div>
    </div>

    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon">País:</span>
            @include('includes.form_select_pais', [ 'campo'=> 'pais', 'texto'=> null, 'ficha'=> $ficha->datos])
        </div>
    </div>

</div>

<hr>

<br>
@include('includes.form_div_bool', [ 'campo'=> 'ingles_academia', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.inglesacademia') ])

<div class="input-group">
    <span class="input-group-addon">¿Has realizado algún curso de idiomas en el extranjero con anterioridad?:</span>
    <div class="pull-left form-radio">
        <label class="radio-inline">
            {!! Form::radio('curso_anterior_bool', '1', ($ficha->datos->curso_anterior)) !!} SI
        </label>
        <label class="radio-inline">
            {!! Form::radio('curso_anterior_bool', '0', (!$ficha->datos->curso_anterior)) !!} NO
        </label>
    </div>

    <div id="curso_anterior_div" style="display:none;">
        <br>
        @include('includes.form_input_text', [ 'campo'=> 'curso_anterior', 'ficha'=>$ficha->datos ])
    </div>
    <span class="help-block">{{ $errors->first('curso_anterior') }}</span>
</div>
<br>
<div class="input-group">
    <span class="input-group-addon">¿Tienes algún título oficial?:</span>
    <div class="pull-left form-radio">
        <label class="radio-inline">
            {!! Form::radio('titulacion_bool', '1', ($ficha->datos->titulacion)) !!} SI
        </label>
        <label class="radio-inline">
            {!! Form::radio('titulacion_bool', '0', (!$ficha->datos->titulacion)) !!} NO
        </label>
    </div>

    <div id="titulacion_div" style="display:none;">
        <br>
        @include('includes.form_input_text', [ 'campo'=> 'titulacion', 'ficha'=>$ficha->datos ])
    </div>
    <span class="help-block">{{ $errors->first('titulacion') }}</span>
</div>

{{-- <br>
<div class="row">
    <div class="col-md-4">
        <div class="input-group">
            <span class="input-group-addon">¿Cómo nos ha conocido?:</span>
            @include('includes.form_select', [ 'campo'=> 'origen_id', 'select'=> $conocidos])
        </div>
    </div>
    <div class="col-md-4">
        <div class="input-group">
            <span class="input-group-addon">Sub-Origen:</span>
            @include('includes.form_select', [ 'campo'=> 'suborigen_id', 'select'=> $subconocidos])
        </div>
    </div>
    <div class="col-md-4">
        <div class="input-group">
            <span class="input-group-addon">Sub-Origen Detalle:</span>
            @include('includes.form_select', [ 'campo'=> 'suborigendet_id', 'select'=> $subconocidosdet])
        </div>
    </div>
</div>
 --}}
<br>
<div class="input-group">
    <span class="input-group-addon">Aficiones, hobbies y deportes:</span>
    @include('includes.form_textarea', [ 'campo'=> 'hobby', 'ficha'=>$ficha->datos ])
</div>
<br>

@if($ficha->viajero->es_cic)
    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_select', [ 'campo'=> 'cic_thau', 'texto'=> '¿Vas a la escuela Thau Barcelona o Sant Cugat?', 'select'=> ConfigHelper::getCicThau(),'ficha'=>$ficha->datos ])
        </div>
    </div>
@endif

@if($ficha->curso->categoria && $ficha->curso->categoria->es_info_campamento)
    <hr>
    @include('manage.bookings.booking.paso2_campamento', ['ficha'=> $ficha, 'required'=> false])
@endif

<br>
<div class="input-group">
    <span class="input-group-addon">Observaciones (viajero):</span>
    @include('includes.form_textarea', [ 'campo'=> 'booking_notas', 'valor'=>$ficha->notas ])
</div>
<br>

<hr>
<div class="row">
    <div class="col-md-12">
        <h4 class="text-danger">Notas internas no visibles por el cliente</h4>
    </div>
</div>
    <div class="input-group">
        <span class="input-group-addon">Notas internas:</span>
        @include('includes.form_textarea', [ 'campo'=> 'notas', 'ficha'=>$ficha->viajero, 'novisible'=> true ])
    </div>
<br>
<div class="input-group">
    <span class="input-group-addon">Notas Booking (en Inglés):</span>
    @include('includes.form_textarea', [ 'campo'=> 'booking_notas2', 'valor'=>$ficha->notas2, 'novisible'=> true ])
</div>

<div class="form-group">
    {!! Form::submit('Actualizar', array('class' => 'btn btn-danger', 'name'=> 'submit_notas')) !!}
</div>


@include('includes.script_boolean', ['campo'=> 'alergias', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'medicacion', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'tratamiento', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'enfermedad', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'dieta', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'animales', 'required'=>0 ])
@include('includes.script_boolean', ['campo'=> 'curso_anterior' ])
@include('includes.script_boolean', ['campo'=> 'titulacion' ])
@include('includes.script_boolean', ['campo'=> 'ingles_academia', 'required'=>0 ])
@include('includes.script_conocido')

@if($ficha->datos_congelados)
<script type="text/javascript">
    $(document).ready(function() {
        $("#datos input").prop('disabled', true);
        $("#datos :radio").prop('disabled', true);
        $("#datos select").prop('disabled', true);
        $('#datos .selectpicker').prop('disabled', true);
        $('#datos .selectpicker').selectpicker('refresh');
    });
</script>
@endif

<script type="text/javascript">
    $(document).ready(function() {

        var bool = "#cic";
        var div = "#cic_nivel_div";

        var v = $(bool).is(":checked");
        if(v==0) { $(div).hide(); } else { $(div).show(); }

        $(bool).change(function() {

            var v = $(this).is(":checked");
            if(v==0) { $(div).hide(); } else { $(div).show(); }

        });


        // $("input[name='alergias_bool']").change(function() {

        //     var v = $(this).val();
        //     if(v==1)
        //     {
        //         var t = $('#booking_notas2').val();
        //         var t2 = "\nDISEASE  ?? ALLERGY:";
        //         $('#booking_notas2').val(t + t2);
        //     }

        // });

        // $("input[name='medicacion_bool']").change(function() {

        //     var v = $(this).val();
        //     if(v==1)
        //     {
        //         var t = $('#booking_notas2').val();
        //         var t2 = "\nMEDICATION, DIET  ?? TREATMENT:";
        //         $('#booking_notas2').val(t + t2);
        //     }

        // });

    });
</script>