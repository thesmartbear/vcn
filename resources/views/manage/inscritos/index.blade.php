@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.inscritos.index') !!}
@stop

@section('titulo')
    <i class="fa fa-user"></i> Inscritos
@stop

@section('container')

    {{-- <div class="row">
        Por Status Inscrito:
            <a class="btn btn-default {{!$status_id?'btn-success':''}}" href="{{ route('manage.inscritos.index') }}">Todos</a>
        @foreach($statuses as $status)
            <a class="btn btn-default {{$status_id==$status->id?'btn-success':''}}" href="{{ route('manage.inscritos.index', $status->id) }}">{{$status->name}}</a>
        @endforeach
    </div> --}}

    <div class="row">

        {{-- <div class="col-md-1">
            {!! Form::select('any', $anys, $any, array('class'=>'select2', 'data-style'=>'blue', 'id'=>'select-any-filtro'))  !!}
        </div> --}}
        
        <div class="col-md-2">
            @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=> $fecha1])
        </div>
        <div class="col-md-2">
            @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=> $fecha2])
        </div>
        <div class="col-md-1">
            <button id="select-any-button" class = 'btn btn-info'>Filtrar</button>
        </div>
        @include('includes.script_filtro_any')

        <?php $user_id = $asign_to; ?>
        @include('includes.select_asignados', ['route'=> 'manage.inscritos.index'])

    </div>

    <div class="row">
        <div class="col-md-2"><i>(=fecha inicio booking)</i></div>
    </div>

    <hr>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active all"><a href="#todos" aria-controls="todos" role="tab" data-toggle="tab">Todos [{{$statuses_total[0]}}]</a></li>

        @foreach($statuses as $status)
        <li role="presentation">
            <a href="#status-{{$status->id}}" aria-controls="status-{{$status->id}}" role="tab" data-toggle="tab"><span>{{$status->name}} [{{$statuses_total[$status->id]}}]</span></a>
        </li>
        @endforeach
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <div role="tabpanel" class="tab-pane fade in active" id="todos">
            @include('manage.inscritos.list', ['status_id'=> $status_id, 'status_name' => 'Todos'])
        </div>

        @foreach($statuses as $status)
        <div role="tabpanel" class="tab-pane fade in" id="status-{{$status->id}}">
            @include('manage.inscritos.list', ['status_id'=> $status->id, 'status_name' => $status->name])
        </div>
        @endforeach

    </div>

@stop