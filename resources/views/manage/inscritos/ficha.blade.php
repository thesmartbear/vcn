@extends('layouts.manage')


@section('container')

{!! Form::model($ficha, array('route' => array('manage.viajeros.ficha', $ficha->id))) !!}

    <div class="col-md-9">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-user fa-fw"></i> Inscrito :: {{$ficha->full_name}}

                <a href="{{route('manage.viajeros.ficha', $ficha->id)}}">(Ver Lead)</a>

                <div class="pull-right">
                    <a href="{{route('manage.bookings.nuevo', $ficha->id)}}" class='btn btn-info btn-xs'>
                        <i class='fa fa-plus-circle'></i> Inscripción
                    </a>
                </div>

            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Viajero</a></li>
                    <li role="presentation"><a href="#tutores" aria-controls="tutores" role="tab" data-toggle="tab">Tutores</a></li>
                    <li role="presentation"><a href="#facturas" aria-controls="facturas" role="tab" data-toggle="tab">Datos Fact.</a></li>
                    <li role="presentation"><a href="#archivos" aria-controls="archivos" role="tab" data-toggle="tab">Archivos</a></li>
                    <li role="presentation"><a href="#tareas" aria-controls="tareas" role="tab" data-toggle="tab">Tareas</a>
                    <li role="presentation"><a href="#historial" aria-controls="historial" role="tab" data-toggle="tab">Historial</a></li>

                    <li role="presentation"><a href="#bookings" aria-controls="bookings" role="tab" data-toggle="tab">Inscripciones</a></li>

                    <li><a href="{{route('manage.viajeros.ficha.datos',$ficha->id)}}" aria-controls="datos">Otros Datos</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        <br>

                        <div class="form-group row">
                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'lastname', 'texto'=> 'Apellido'])
                            </div>
                            <div class="col-md-4">
                                @include('includes.form_input_text', [ 'campo'=> 'lastname2', 'texto'=> 'Apellido2'])
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'Email'])
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('sexo', 'Sexo') !!}:
                                <label class="radio-inline">
                                {!! Form::radio('sexo', '1', ($ficha->sexo==1)) !!} <i class="fa fa-male"></i>
                                </label>
                                <label class="radio-inline">
                                {!! Form::radio('sexo', '2', ($ficha->sexo==2)) !!} <i class="fa fa-female"></i>
                                </label>
                                <span class="help-block">{{ $errors->first('sexo') }}</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                @include('includes.form_input_text', [ 'campo'=> 'phone', 'texto'=> 'Teléfono'])
                            </div>
                            <div class="col-md-6">
                                @include('includes.form_input_text', [ 'campo'=> 'movil', 'texto'=> 'Móvil'])
                            </div>
                        </div>

                        <div class="form-group pull-right">
                            {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                            <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tutores">
                        @include('manage.viajeros.ficha_tutores', ['ficha'=> $ficha])
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="facturas">
                        @include('manage.viajeros.facturas.list', ['viajero_id'=> $ficha->id])
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="archivos">
                    .a.
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="tareas">
                        @include('manage.viajeros.tareas.list', ['viajero_id'=> $ficha->id])
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="historial">
                        @include('manage.viajeros.logs.list', ['viajero_id'=> $ficha->id, 'todos'=> 0])
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="bookings">
                        @include('manage.bookings.list_viajero', ['viajero_id'=> $ficha->id])
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="col-md-3">

        <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bookmark fa-fw"></i> Asignado a
            </div>
            <div class="panel-body">
                @include('includes.form_select', ['campo'=> 'asign_to', 'texto'=> '', 'select'=> $asignados])
            </div>
        </div>
        </div>

        <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tag fa-fw"></i> Status Inscrito
            </div>
            <div class="panel-body">
                @include('includes.form_select', ['campo'=> 'inscrito_status_id', 'texto'=> '', 'select'=> $statuses])
                + CHECKLIST
            </div>
        </div>
        </div>

        <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tasks fa-fw"></i> Tareas
            </div>
            <div class="panel-body">
            </div>
        </div>
        </div>

        <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-pencil fa-fw"></i> Notas
            </div>
            <div class="panel-body">
                @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> ''])
            </div>
        </div>
        </div>

        <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-history fa-fw"></i> Historial
            </div>
            <div class="panel-body">
            </div>
        </div>
        </div>

    </div>

{!! Form::close() !!}

@include('includes.script_categoria')
@include('includes.script_conocido')

@stop