
<div class="portlet light tabs">

    <div class="portlet-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'            => 'Nombre',
                  'reserva'         => 'Fecha Booking',
                  'curso'           => 'Convocatoria/Curso',
                  'fecha_curso'     => 'Fecha Inicio',
                  'duracion'        => 'Ud. duración',
                  'categoria'       => 'Categoria',
                  'status'          => 'Status',
                  'origen'            => 'Origen',
                  'asignado'          => 'Asignado',
                  'email'             => 'E-mail',
                  'idioma'            => 'Idioma',
                  'tutor1'            => 'Tutor1',
                  'movil_tutor1'      => 'Tutor1 Móvil',
                  'email_tutor1'      => 'Tutor1 Email',
                  'tutor2'            => 'Tutor2',
                  'movil_tutor2'      => 'Tutor2 Móvil',
                  'email_tutor2'      => 'Tutor2 Email',
                  'prescriptor'   => 'Prescriptor',
                  'oficina'       => 'Oficina',
                  'ig'       => 'IG',
                  'edad'    => 'Edad',
                  'options'       => ''
                ])
                ->setUrl( route('manage.inscritos.index', [$status_id, $asign_to, $oficina_id, 'any'=> $any]) )
                ->setOptions('iDisplayLength',100)
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "sortable" => false, "targets" => [0,4,7,17,18] ],
                    [ "targets" => [1,3], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
                ->render() !!}

    </div>

</div>