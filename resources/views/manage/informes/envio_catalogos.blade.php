@extends('layouts.manage')

@section('breadcrumb')
  {!! Breadcrumbs::render('manage.informes.ruta', 'Listado Envío catálogos', 'manage.informes.envio-catalogos') !!}
@stop

@section('titulo')
    <i class="fa fa-envelope-o fa-fw"></i> Informe de envío de catálogos
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-list fa-fw"></i>Listado
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'viajero' => 'Viajero',
                  'tipovia' => 'Tipo vía',
                  'direccion'=> 'Dirección',
                  'cp'  => 'CP',
                  'ciudad'=> 'Ciudad',
                  'idioma' => 'Idioma',
                  'log'   => 'Registro',
                  'boton'   => '',
                ])
                ->setUrl(route('manage.informes.envio-catalogos'))
                ->setOptions('iDisplayLength', 0)
                ->setOptions(
                  "columnDefs", array(
                    // [ "sortable" => false, "targets" => [0] ],
                    // [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
                ->render() !!}

        </div>
    </div>


@stop