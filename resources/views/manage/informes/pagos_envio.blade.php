@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.pagos') !!}
@stop

@section('titulo')
    <i class="fa fa-money fa-fw"></i> Informe de pagos envío
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-money fa-fw"></i> Pagos Envío
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'importe'     => 'Importe',
                  'fecha'       => 'Fecha',
                  'booking_id'  => 'Booking',
                  'viajero_id'  => 'Cliente',
                  'viajero'     => 'Razón Social',
                  'nif'         => 'NIF',
                  'tipopago'    => 'Tipo Pago',
                  'tipoprograma'    => 'Tipo Programa',
                  'contable'    => 'Código',
                  'ccc'         => 'Cuenta Banc.',
                  'oficina'     => 'Oficina',
                ])
                ->setUrl(route('manage.informes.pagos.envio'))
                ->setOptions('iDisplayLength', 0)
                ->setOptions(
                  "columnDefs", array(
                    //[ "sortable" => false, "targets" => [0] ],
                    [ "targets" => [1], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
                ->render() !!}

        </div>
    </div>


@stop