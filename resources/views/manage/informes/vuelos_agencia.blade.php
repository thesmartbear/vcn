@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Infovuelo Agencia', 'manage.informes.vuelos-agencia') !!}
@stop

@section('titulo')
    <i class="fa fa-plane fa-fw"></i> Listado Infovuelo Agencia
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.vuelos-agencia'), 'method'=> 'GET', 'class' => 'form']) !!}

                {!! Form::hidden('tipoc',$valores['tipoc'], ['id'=> 'filtro-tipoc']) !!}

                <div class="form-group row">

                    <div class="col-md-4">
                    {!! Form::label('proveedores', 'Proveedor') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'proveedores-cargando'])

                    {!! Form::select('proveedores', $proveedores, $valores['proveedores'], array('class'=>'select2', 'data-style'=>'purple', 'id'=>'filtro-proveedores'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'cursos'])
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'convocatorias'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-6">
                    {!! Form::label('centros', 'Centro') !!}
                    @include('includes.form_input_cargando',['id'=> 'centros-cargando'])
                    <br>
                    {!! Form::select('centros', $centros, $valores['centros'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-centros'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'cursos'])
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'convocatorias'])
                    </div>

                    <div class="col-md-5">
                    {!! Form::label('cursos', 'Curso') !!}
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])

                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'cursos', 'destino'=> 'convocatorias'])
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-6">
                    {!! Form::label('convocatorias', 'Convocatoria') !!}
                    @include('includes.form_input_cargando',['id'=> 'convocatorias-cargando'])
                    <br>
                    {!! Form::select('convocatorias', $convocatorias, $valores['convocatorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-convocatorias'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}

        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plane"></i>
                <span class="caption-subject bold">Vuelos</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <ul class="nav nav-tabs" role="tablist">
                    @foreach($tabs as $tab)
                        <li role="presentation" class="{{($tab === reset($tabs))?'active':''}}">
                            <a href="#tab-{{$tab}}" aria-controls="tab-{{$tab}}" role="tab" data-toggle="tab">
                            <?php
                                $t = \VCN\Models\Cursos\Curso::find($tab);
                            ?>
                            {{$t?$t->name:"- Curso NO EXISTE -"}}
                            </a>
                        </li>
                    @endforeach
                    </ul>

                    <div class="tab-content">
                    @foreach($tabs as $tab)
                        <div role="tabpanel" class="tab-pane fade in {{($tab === reset($tabs))?'active':''}}" id="tab-{{$tab}}">


                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-globe fa-fw"></i> Listado
                                </div>
                                <div class="panel-body">

                                    <?php
                                        $valores['cursos'] = $tab;
                                    ?>

                                    {!! Datatable::table()
                                        ->addColumn([
                                            'convocatoria'  => 'Convocatoria',
                                            'vuelo'         => 'Vuelo',
                                            'localizador'   => 'Localizador',
                                            'encuentro_fecha'   => 'Día encuentro',
                                            'encuentro_hora'    => 'Hora encuentro',
                                            'encuentro_punto'   => 'Punto encuentro',
                                            'infovuelo_ida'     => 'Infovuelo IDA',
                                            'infovuelo_vuelta'  => 'Infovuelo VUELTA',
                                        ])
                                        ->setUrl( route('manage.informes.vuelos-agencia', $valores) )
                                        ->setOptions('iDisplayLength', 100)
                                        ->setOptions(
                                            "columnDefs", array(
                                                [ "targets" => [3], "render"=> "function(data, type, full) {return moment(data).isValid()?moment(data).format('DD/MM/YYYY'):'-';}" ],
                                            )
                                        )
                                        ->render() !!}

                                </div>
                            </div>

                        </div>
                    @endforeach
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop