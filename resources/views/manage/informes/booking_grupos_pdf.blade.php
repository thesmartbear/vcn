<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>

        {!! Html::style('assets/css/pdf.css') !!}
        {!! Html::style('assets/css/bootstrap.css') !!}

    </head>
    <body>

        <h1>Group Bookings</h1>

        @foreach($tabs as $ktab=>$vtab)

            Empresa: {{$valores_txt['plataformas']}}, Oficina: {{$valores_txt['oficinas']}}, Centro: {{$valores_txt['centros']}}
            <br>School: {{\VCN\Models\Proveedores\Proveedor::find($ktab)->name}}

            @foreach($tabs[$ktab] as $c)
                <?php
                    $valores['proveedores'] = $ktab;
                    $valores['cursos'] = $c;
                    $bookings = \VCN\Models\Bookings\Booking::bookingGrupos($valores);
                ?>

                @foreach($bookings->orderBy('fecha_reserva')->get()->groupBy('convocatory_id')->sortBy('duracion_dias') as $convo)

                    <div class="page-off">
                    <table class="table table-bordered no-footer">
                        <caption>
                            <h3>School: {{\VCN\Models\Proveedores\Proveedor::find($ktab)->name}}</h3>
                            <h4>Program: {{\VCN\Models\Cursos\Curso::find($c)->name}} :: {{$convo->first()->convocatoria?$convo->first()->convocatoria->name:"-"}}</h4>
                        </caption>
                        <thead>
                            <tr>
                                <th>Firstname</th>
                                <th>Surname</th>
                                <th>Gender</th>
                                <th>DOB</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Booking</th>
                                <th>Status</th>
                                <th>Accomodation</th>
                                <th>Comments</th>
                                <th>Extras</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($bookings->orderBy('fecha_reserva')->get()->where('convocatory_id',$convo->first()->convocatory_id) as $booking)
                            <tr>
                                <td>{{$booking->viajero->name}}</td>
                                <td>{{$booking->viajero->lastname}} {{$booking->viajero->lastname2}}</td>
                                <td>{{$booking->viajero->sexo_name_en}}</td>
                                <td>{{$booking->viajero->fechanac_dmy}}</td>
                                <td>{{Carbon::parse($booking->course_start_date)->format('d/m/Y')}}</td>
                                <td>{{Carbon::parse($booking->course_end_date)->format('d/m/Y')}}</td>
                                <td>{{$booking->fecha_reserva?$booking->fecha_reserva->format('d/m/Y'):"-"}}</td>
                                <td>{{$booking->status_name}}</td>
                                <td>{{$booking->alojamiento?$booking->alojamiento->name:"-"}}</td>
                                <td>
                                    <?php
                                        $ret = $booking->notas2?$booking->notas2:"";
                                        if($booking->datos)
                                        {
                                            $ret .= $booking->datos->alergias2?"Allergies: ".$booking->datos->alergias2.". ":"";
                                            $ret .= $booking->datos->enfermedad2?"Illnes: ".$booking->datos->enfermedad2.". ":"";
                                            $ret .= $booking->datos->medicacion2?"Medication: ".$booking->datos->medicacion2.". ":"";
                                            $ret .= $booking->datos->tratamiento2?"Treatment: ".$booking->datos->tratamiento2.". ":"";
                                            $ret .= $booking->datos->dieta2?"Diet: ".$booking->datos->dieta2.". ":"";
                                        }
                                    ?>

                                    {{$ret}}
                                </td>
                                <td>{{$booking->extras_name_pdf}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    </div>

                @endforeach

            @endforeach

        @endforeach

    </body>
</html>