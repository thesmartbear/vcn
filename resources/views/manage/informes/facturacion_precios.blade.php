@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.facturacion-precios') !!}
@stop

@section('titulo')
    <i class="fa fa-money fa-fw"></i> Control Precios
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.facturacion-precios'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categorias'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'proveedores'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'cursos'])
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('tipoc', 'Tipo Convocatoria') !!}
                    @include('includes.form_input_cargando',['id'=> 'tipoc-cargando'])
                    <br>
                    {!! Form::select('tipoc', ConfigHelper::getConvocatoriaTipo(), $valores['tipoc'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-tipoc'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-3">
                    {!! Form::label('oficinas', 'Oficina') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-oficinas'))  !!}
                    </div>
                    <div class="col-md-3">
                    {!! Form::label('prescriptores', 'Prescriptor') !!}
                    @include('includes.form_input_cargando',['id'=> 'prescriptores-cargando'])
                    <br>
                    {!! Form::select('prescriptores', $prescriptores, $valores['prescriptores'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-prescriptores'))  !!}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-5">
                    {!! Form::label('centros', 'Centro') !!}
                    @include('includes.form_input_cargando',['id'=> 'centros-cargando'])
                    <br>
                    {!! Form::select('centros', $centros, $valores['centros'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-centros'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-7">
                    {!! Form::label('cursos', 'Curso') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])

                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-3">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=> $valores['desde']])
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=> $valores['hasta']])
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        {!! Form::label('(Fechas: primer pago)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}
        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-globe fa-fw"></i> Listado ({{$results}})
                        </div>
                        <div class="panel-body">

                            <?php
                            $cols = [
                                'total'           => "Importe Total (". ConfigHelper::moneda() .")",
                                'pagado'          => "Pagado (". ConfigHelper::moneda() .")",
                                'pendiente'       => "Pendiente (". ConfigHelper::moneda() .")",
                                'curso'           => 'Curso',
                                'alojamiento'     => 'Alojamiento',
                                'early'           => 'EarlyBird',
                                'descuentos'      => 'Descuentos',
                                'seguro'          => 'Seguro Canc.',
                                'extras_centro'   => 'Extras Centro',
                                'extras_curso'    => 'Extras Curso',
                                'extras_alojamiento'    => 'Extras Alojamiento',
                                'extras_generico' => 'Extras Generico',
                                'extras_otros'    => 'Extras Booking'
                            ];
                            ?>

                            <table class="table table-bordered">
                                <caption>Totales</caption>
                                <thead>
                                    <tr>
                                        @foreach($cols as $ci=>$ctxt)
                                            <th>{{$ctxt}}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @foreach($cols as $ci=>$ctxt)
                                            <td>{{ConfigHelper::parseMoneda($totales[$ci])}}</td>
                                        @endforeach
                                    </tr>
                                </tbody>
                            </table>

                            <hr>

                            {!! Datatable::table()
                                ->addColumn([
                                    'fecha_reserva'   => 'Fecha Booking',
                                    'viajero'         => 'Viajero',
                                    'convocatoria'    => 'Convocatoria',
                                    'curso_fecha'     => 'Fecha Inicio',
                                    'oficina'         => 'Oficina',
                                    'total'           => "Importe Total (". ConfigHelper::moneda() .")",
                                    'pagado'          => "Pagado (". ConfigHelper::moneda() .")",
                                    'pendiente'       => "Pendiente (". ConfigHelper::moneda() .")",
                                    'curso'           => 'Curso',
                                    'curso_moneda'    => 'Moneda',
                                    'alojamiento'     => 'Alojamiento',
                                    'alojamiento_moneda'    => 'Moneda',
                                    'early'           => 'EarlyBird',
                                    'early_moneda'    => 'Moneda',
                                    'descuentos'      => 'Descuentos',
                                    'seguro'          => 'Seguro Canc.',
                                    'extras_centro'   => 'Extras Centro',
                                    'extras_curso'    => 'Extras Curso',
                                    'extras_alojamiento'    => 'Extras Alojamiento',
                                    'extras_generico' => 'Extras Generico',
                                    'extras_otros'    => 'Extras Booking',
                                    'origen_txt' => 'Orígen',
                                    'divisa'        => 'Divisa',
                                    'vdivisa'=> 'Variación divisa',
                                    'options'         => ''

                                ])
                                ->setUrl( route('manage.informes.facturacion-precios', $valores) )
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                  "columnDefs", array(
                                    //[ "sortable" => false, "targets" => [18] ],
                                    [ "targets" => [0,3], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                  )
                                )
                                ->render() !!}

                        </div>
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop