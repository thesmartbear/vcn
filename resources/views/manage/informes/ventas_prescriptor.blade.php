@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Ventas por Prescriptor', 'manage.informes.ventas-prescriptor') !!}
@stop

@section('titulo')
    <i class="fa fa-money fa-fw"></i> Ventas por Prescriptor
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.ventas-prescriptor'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categoriasp'])
                    </div>

                    <div class="col-md-3">
                    {!! Form::label('oficinas', 'Oficina') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-oficinas'))  !!}
                    </div>

                    <div class="col-md-3">
                    {!! Form::label('categoriasp', 'Categoría Prescriptor') !!}
                    @include('includes.form_input_cargando',['id'=> 'categoriasp-cargando'])
                    <br>
                    {!! Form::select('categoriasp', $categoriasp, $valores['categoriasp'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categoriasp'))  !!}
                    </div>

                    <div class="col-md-3"></div>
                </div>

                <div class="form-group row">

                    <div class="col-md-2">
                        {!! Form::label('categorias', 'Categoría') !!}
                        @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                        <br>
                        {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                        {{-- @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos']) --}}
                        @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                        {{-- @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategoriasdet']) --}}
                    </div>

                    <div class="col-md-4">
                        {!! Form::label('subcategorias', 'SubCategoría') !!}
                        @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                        <br>
                        {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
                        {{-- @include('includes.script_filtros', ['filtro'=> 'subcategorias', 'destino'=> 'cursos']) --}}
                        {{-- @include('includes.script_filtros', ['filtro'=> 'subcategorias', 'destino'=> 'subcategoriasdet']) --}}
                    </div>
                </div>

                <hr>

                <div class="form-group row">

                    {{--
                    <div class="col-md-1">
                        Año:
                    </div>
                    --}}

                    <div class="col-md-3">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=> $valores['desde']])
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=> $valores['hasta']])
                    </div>

                    <div class="col-md-2 col-md-offset-1">
                        {!! Form::label('(Fechas: 1r pago)') !!}
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}
        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-globe fa-fw"></i> Listado
                        </div>
                        <div class="panel-body">

                            {!! Datatable::table()
                                ->addColumn([
                                    'viajero' => 'Viajero',
                                    'curso' => 'Curso',
                                    'convocatoria' => 'Convocatoria',
                                    'duracion' => 'Duración',
                                    'duracion_name' => 'Ud.Duración',
                                    'fecha' => 'Fecha',
                                    'fecha_salida' => 'F.Salida',
                                    'categoria' => 'Categoría',
                                    'prescriptor' => 'Prescriptor',
                                    'categoriap' => 'Cat.Prescriptor',
                                    'oficina' => 'Oficina',
                                    'fecha_p1' => 'Fecha 1r pago',
                                ])
                                ->setUrl(route('manage.informes.ventas-prescriptor',$valores))
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                  "columnDefs", array(
                                    // [ "sortable" => false, "targets" => [0,1,2,3,7,8,9,10,11] ],
                                    [ "targets" => [5,6,11], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                  )
                                )
                                ->render()
                            !!}

                        </div>
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop