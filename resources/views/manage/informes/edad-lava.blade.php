@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Edad', 'manage.informes.edad') !!}
@stop

@section('titulo')
    <i class="fa fa-birthday-cake fa-fw"></i> Edad
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-money fa-fw"></i> Listado
        </div>
        <div class="panel-body">

            {!! Form::open(array('method' => 'GET', 'url' => route('manage.informes.edad'), 'role' => 'form', 'class' => '')) !!}

            <div class="form-group row">
                <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    {{-- @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categorias'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'subcategorias']) --}}
                </div>
            </div>

            <div class="form-group row">

                <div class="col-md-2">
                {!! Form::label('oficinas', 'Oficina') !!}
                @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])
                <br>
                {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-oficinas'))  !!}
                </div>

                <div class="col-md-2">
                {!! Form::label('categorias', 'Categoría') !!}
                @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                <br>
                {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                </div>

                <div class="col-md-2">
                {!! Form::label('subcategorias', 'Subcategoría') !!}
                @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                <br>
                {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
                </div>

            </div>

            <div class="form-group row">
                <div class="col-md-2">
                {!! Form::label('any', 'Año Booking') !!}
                <br>
                {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any'))  !!}
                </div>

                <div class="col-md-4">
                    {!! Form::label('filtro1','(Fechas: Inicio Booking)') !!}<br>
                    {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                </div>
            </div>


            {!! Form::close() !!}

            <hr>

            @if($listado)

                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#tab-edades" aria-controls="tab-edades" role="tab" data-toggle="tab">% Edades</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab-graficos" aria-controls="tab-graficos" role="tab" data-toggle="tab">Gráficos</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab-listado" aria-controls="tab-listado" role="tab" data-toggle="tab">Listado [{{$total}} Bookings]</a>
                    </li>
                </ul>

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in" id="tab-graficos">

                        <div id="dashboard1_div">
                            <table class="table table-bordered columns">
                                <caption><h3>Edades [{{$total}} Bookings]</h3></caption>
                                <tr>
                                    <td><div id="slider1_div"></div></td>
                                    <td><div id="select1_div"></div></td>
                                </tr>
                                <tr>
                                    <td colspan='2'><div id="chart1_div"></div></td>
                                </tr>
                            </table>
                        </div>

                        @dashboard('Dashboard1', 'dashboard1_div')

                        <div id="dashboard2_div">
                            <table class="table table-bordered columns">
                                <caption><h3>Edades x País</h3></caption>
                                <tr>
                                    <td><div id="slider2_div"></div></td>
                                    <td><div id="select2_div"></div></td>
                                </tr>
                                <tr>
                                    <td colspan='2'><div id="chart2_div"></div></td>
                                </tr>
                            </table>
                        </div>

                        @dashboard('Dashboard2', 'dashboard2_div')

                    </div>

                    <div role="tabpanel" class="tab-pane fade in active" id="tab-edades">

                        <div class="informe-tabla"><div>

                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <caption><h3>% Edades [{{$total}} Bookings]</h3></caption>
                                <thead class="flip-content">
                                    <tr>
                                        <th class='td-fijo-1'>Edad</th>
                                        @foreach($edades as $kedad=>$edad)
                                            <th>{{$kedad}}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th class='td-fijo-1'>%</th>
                                        @foreach($edades as $edad)
                                            <td>{{round($edad/$total * 100,2)}}</td>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        <th class='td-fijo-1'>Total</th>
                                        @foreach($edades as $edad)
                                            <td>{{$edad}}</td>
                                        @endforeach
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <caption><h3>% Edades x País</h3></caption>
                                <thead class="flip-content">
                                    <tr>
                                        <th class='td-fijo-1'>Edad</th>
                                        @foreach($edades as $kedad=>$edad)
                                            <th>{{$kedad}}</th>
                                        @endforeach
                                        <th>Total</th>
                                    </tr>

                                </thead>
                                <tbody>

                                    @foreach($paises as $kpais=>$tpais)
                                    <tr>
                                        <th class='td-fijo-1'>{{$kpais}} [{{$tpais}}]</th>
                                        @foreach($edades as $kedad=>$edad)

                                            <?php
                                                $v = 0;
                                                $t = $total;
                                                if(isset($edades_pais[$kedad][$kpais]))
                                                {
                                                    $v = $edades_pais[$kedad][$kpais];
                                                    $t = $edades[$kedad];

                                                    $v = round($v/$t * 100,2);
                                                }
                                            ?>

                                            <td>{{$v}}</td>

                                        @endforeach

                                        <th>{{round($tpais/$total * 100,2)}}</th>

                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>

                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <caption><h3>% Edades x Tipo Alojamiento</h3></caption>
                                <thead class="flip-content">
                                    <tr>
                                        <th class='td-fijo-1'>Edad</th>
                                        @foreach($edades as $kedad=>$edad)
                                            <th>{{$kedad}}</th>
                                        @endforeach
                                        <th>Total</th>
                                    </tr>

                                </thead>
                                <tbody>

                                    @foreach($alojamientos as $kaloj=>$taloj)
                                    <tr>
                                        <th class='td-fijo-1'>{{$kaloj}} [{{$taloj}}]</th>
                                        @foreach($edades as $kedad=>$edad)

                                            <?php
                                                $v = 0;
                                                $t = $total;
                                                if(isset($edades_alojamiento[$kedad][$kaloj]))
                                                {
                                                    $v = $edades_alojamiento[$kedad][$kaloj];
                                                    $t = $edades[$kedad];

                                                    $v = round($v/$t * 100,2);
                                                }
                                            ?>

                                            <td>{{$v}}</td>

                                        @endforeach

                                        <th>{{round($taloj/$total * 100,2)}}</th>

                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>

                        </div></div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="tab-listado">

                        {!! Datatable::table()
                            ->addColumn([
                              'booking' => 'Booking',
                              'edad'    => 'Edad',
                              'pais'    => 'Pais',
                              'alojamiento' => 'Alojamiento',
                              'tipo'    => 'Tipo'
                            ])
                            ->setUrl(route('manage.informes.edad',$valores))
                            ->setOptions('iDisplayLength', 100)
                            ->render()
                        !!}

                    </div>
                </div>

            @else
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @endif

        </div>

    </div>

@stop