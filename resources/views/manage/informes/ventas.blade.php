@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ventas') !!}
@stop

@section('titulo')
    <i class="fa fa-money fa-fw"></i> Ventas
@stop


@section('container')

    @if(sizeof($anys)>1)
        <?php $any1 = $anys[sizeof($anys)-1]->any; ?>
        <?php $any2 = $anys[sizeof($anys)-2]->any; ?>

        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money"></i>
                    <span class="caption-subject bold">Ventas {{$any1}} vs {{$any2}} acumulado hasta semana {{$semanaActual}} </span>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat blue">
                            <div class="visual">
                                <i class="fa fa-bar-chart"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span>{{$jovenes->where('any',$any1)->sum('semanas')}}</span>
                                    vs
                                    <span>{{$jovenes->where('any',$any2)->sum('semanas')}}</span>
                                </div>
                                <div class="desc">
                                    Bookings:
                                    <span>{{$jovenes->where('any',$any1)->sum('inscripciones')}}</span>
                                    vs
                                    <span>{{$jovenes->where('any',$any2)->sum('inscripciones')}}</span>
                                </div>
                            </div>
                            <a class="more" href="javascript:;"> JÓVENES
                                <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat red">
                            <div class="visual">
                                <i class="fa fa-bar-chart"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span>{{$adultos->where('any',$any1)->sum('semanas')}}</span>
                                    vs
                                    <span>{{$adultos->where('any',$any2)->sum('semanas')}}</span>
                                </div>
                                <div class="desc">
                                    Bookings:
                                    <span>{{$adultos->where('any',$any1)->sum('inscripciones')}}</span>
                                    vs
                                    <span>{{$adultos->where('any',$any2)->sum('inscripciones')}}</span>
                                </div>
                            </div>
                            <a class="more" href="javascript:;"> ADULTOS
                                <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat green">
                            <div class="visual">
                                <i class="fa fa-bar-chart"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span>{{$aescolar->where('any',$any1)->sum('semanas')}}</span>
                                    vs
                                    <span>{{$aescolar->where('any',$any2)->sum('semanas')}}</span>
                                </div>
                                <div class="desc">
                                    Bookings:
                                    <span>{{$aescolar->where('any',$any1)->sum('inscripciones')}}</span>
                                    vs
                                    <span>{{$aescolar->where('any',$any2)->sum('inscripciones')}}</span>
                                </div>
                            </div>
                            <a class="more" href="javascript:;"> AÑO ESCOLAR
                                <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat purple">
                            <div class="visual">
                                <i class="fa fa-bar-chart"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span>{{$camps->where('any',$any1)->sum('semanas')}}</span>
                                    vs
                                    <span>{{$camps->where('any',$any2)->sum('semanas')}}</span>
                                </div>
                                <div class="desc">
                                    Bookings:
                                    <span>{{$camps->where('any',$any1)->sum('inscripciones')}}</span>
                                    vs
                                    <span>{{$camps->where('any',$any2)->sum('inscripciones')}}</span>
                                </div>
                            </div>
                            <a class="more" href="javascript:;"> {{$camps_txt}}
                                <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    @else
        <div class="content">
            <div class="alert alert-warning" role="alert"> <i class="fa fa-bell"></i> No se puede mostrar comparativa. Solamente hay 1 año de ventas.</div>
        </div>
    @endif


        <ul class="nav nav-tabs" role="tablist">
        @foreach($anys as $any)
            <li role="presentation" class="{{$any->any==Carbon::now()->format('Y')?'active':''}}"><a href="#ficha-{{$any->any}}" aria-controls="ficha-{{$any->any}}" role="tab" data-toggle="tab">{{$any->any}} [{{$ventas[$any->any]->sum('semanas')}}s en {{$ventas[$any->any]->sum('inscripciones')}}b]</a></li>
        @endforeach
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            @foreach($anys as $any)
                <div role="tabpanel" class="tab-pane fade in {{$any->any==Carbon::now()->format('Y')?'active':''}}" id="ficha-{{$any->any}}">

                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#ficha-cat-{{$any->any}}" aria-controls="ficha-cat-{{$any->any}}" role="tab" data-toggle="tab">Por CATEGORÍA</a></li>
                        <li role="presentation"><a href="#ficha-orig-{{$any->any}}" aria-controls="ficha-orig-{{$any->any}}" role="tab" data-toggle="tab">Por ORÍGEN</a></li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="ficha-cat-{{$any->any}}">
                        @foreach($oficinas[$any->any] as $ofi)
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-money"></i>
                                    <span class="caption-subject bold">Ventas Oficina {{$ofi->oficina->name}} [{{$ventas[$any->any]->where('oficina_id',$ofi->oficina_id)->sum('semanas')}}s en {{$ventas[$any->any]->where('oficina_id',$ofi->oficina_id)->sum('inscripciones')}}b]</span>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <div class="informe-tabla">
                                    <div>
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                    <caption>Ventas</caption>
                                    <thead class="flip-content">
                                        <tr>
                                            <th class='td-fijo'>CATEGORÍA</th>
                                            @foreach($ventas[$any->any]->where('oficina_id',$ofi->oficina_id)->sortBy('semana')->groupBy('semana') as $vsemana )
                                                <?php $iSemana = $vsemana->first()->semana; ?>
                                                <th colspan='2'>{{$iSemana}}</th>
                                            @endforeach
                                            <th colspan="2">TOTAL</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ventas[$any->any]->where('oficina_id',$ofi->oficina_id)->sortBy('category_id')->groupBy('category_id') as $categorias )

                                            @foreach($categorias->groupBy('subcategory_id') as $subcategorias)
                                                @foreach($subcategorias->groupBy('subcategory_det_id') as $categoria)

                                                    <?php $cat = $categoria->first(); ?>
                                                    <?php $iCat = $cat->category_id; ?>
                                                    <?php $iSubcat = $cat->subcategory_id; ?>
                                                    <?php $iSubcat_det = $cat->subcategory_det_id; ?>
                                                    <?php $vcat = $ventas[$any->any]->where('oficina_id',$ofi->oficina_id)->where('category_id',$iCat)->where('subcategory_id',$iSubcat)->where('subcategory_det_id',$iSubcat_det); ?>
                                                    <tr>
                                                    <td class='td-fijo'>{{$cat->full_name_categoria}}</td>

                                                    <?php $vSemanas = $ventas[$any->any]->where('oficina_id',$ofi->oficina_id); ?>
                                                    @foreach($vSemanas->sortBy('semana')->groupBy('semana') as $vsemana )

                                                        <?php $iSemana = $vsemana->first()->semana; ?>
                                                        <?php $v = $vcat->where('semana',$iSemana); ?>
                                                        <td>{{$v->sum('inscripciones')}}</td>
                                                        <td>{{$v->sum('semanas')}}</td>

                                                    @endforeach

                                                    <td>{{$vcat->sum('inscripciones')}}</td>
                                                    <td>{{$vcat->sum('semanas')}}</td>

                                                    </tr>

                                                @endforeach
                                            @endforeach

                                        @endforeach
                                    </tbody>
                                    </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endforeach
                        </div>

                        <div role="tabpanel" class="tab-pane fade in" id="ficha-orig-{{$any->any}}">
                        @foreach($oficinas[$any->any] as $ofi)
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-money"></i>
                                    <span class="caption-subject bold">Ventas Oficina {{$ofi->oficina->name}} [{{$ventas[$any->any]->where('oficina_id',$ofi->oficina_id)->sum('semanas')}}s en {{$ventas[$any->any]->where('oficina_id',$ofi->oficina_id)->sum('inscripciones')}}b]</span>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <div class="informe-tabla">
                                    <div>
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                    <caption>Ventas</caption>
                                    <thead class="flip-content">
                                        <tr>
                                            <th class='td-fijo'>ORÍGEN</th>
                                            @foreach($ventas[$any->any]->where('oficina_id',$ofi->oficina_id)->sortBy('semana')->groupBy('semana') as $vsemana )
                                                <?php $iSemana = $vsemana->first()->semana; ?>
                                                <th colspan='2'>{{$iSemana}}</th>
                                            @endforeach
                                            <th colspan="2">TOTAL</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ventas[$any->any]->where('oficina_id',$ofi->oficina_id)->sortBy('origen_id')->groupBy('origen_id') as $categorias )

                                            @foreach($categorias->groupBy('suborigen_id') as $subcategorias)
                                                @foreach($subcategorias->groupBy('suborigendet_id') as $categoria)

                                                    <?php $cat = $categoria->first(); ?>
                                                    <?php $iCat = $cat->origen_id; ?>
                                                    <?php $iSubcat = $cat->suborigen_id; ?>
                                                    <?php $iSubcat_det = $cat->suborigendet_id; ?>
                                                    <?php $vcat = $ventas[$any->any]->where('oficina_id',$ofi->oficina_id)->where('origen_id',$iCat)->where('suborigen_id',$iSubcat)->where('suborigendet_id',$iSubcat_det); ?>
                                                    <tr>
                                                    <td class='td-fijo'>{{$cat->full_name_origen}} {{--({{$iCat}}.{{$iSubcat}}.{{$iSubcat_det}})--}}</td>

                                                    <?php $vSemanas = $ventas[$any->any]->where('oficina_id',$ofi->oficina_id); ?>
                                                    @foreach($vSemanas->sortBy('semana')->groupBy('semana') as $vsemana )

                                                        <?php $iSemana = $vsemana->first()->semana; ?>
                                                        <?php $v = $vcat->where('semana',$iSemana); ?>
                                                        <td>{{$v->sum('inscripciones')}}</td>
                                                        <td>{{$v->sum('semanas')}}</td>

                                                    @endforeach

                                                    <td>{{$vcat->sum('inscripciones')}}</td>
                                                    <td>{{$vcat->sum('semanas')}}</td>

                                                    </tr>

                                                @endforeach
                                            @endforeach

                                        @endforeach
                                    </tbody>
                                    </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endforeach
                        </div>
                    </div>

                </div>
            @endforeach

        </div>


@stop