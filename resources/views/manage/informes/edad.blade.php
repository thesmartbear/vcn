@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Edad', 'manage.informes.edad') !!}
@stop

@section('extra_head')

@if($listado)
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    google.charts.load('current', {'packages':['corechart', 'controls']});
    google.charts.setOnLoadCallback(drawDashboard);

    function drawDashboard() {

        // Países
        var data2 = google.visualization.arrayToDataTable([
            ['Pais', 'Edad', 'Cantidad'],
            @foreach($paises as $kpais=>$tpais)
                @foreach($edades as $kedad => $tedad)
                    ['{{$kpais}}','{{$kedad}}',{{isset($edades_pais[$kedad][$kpais])?$edades_pais[$kedad][$kpais]:0}}],
                @endforeach
            @endforeach
            ]);

        var dashboard2 = new google.visualization.Dashboard(
            document.getElementById('dashboard2_div'));

        var select2a = new google.visualization.ControlWrapper({
            'controlType': 'CategoryFilter',
            'containerId': 'select2a_div',
            'options': {
                'filterColumnLabel': 'Pais',
            }
        });

        var select2b = new google.visualization.ControlWrapper({
            'controlType': 'CategoryFilter',
            'containerId': 'select2b_div',
            'options': {
                'filterColumnLabel': 'Edad',
            }
        });

        var pieChart2 = new google.visualization.ChartWrapper({
            'chartType': 'PieChart',
            'containerId': 'chart2_div',
            'options': {
                'width': 600,
                'height': 400,
                'pieSliceText': 'value',
                'legend': 'right'
            },
            'view': {'columns': [1, 2]}
        });

        dashboard2.bind([select2a, select2b], pieChart2);
        dashboard2.draw(data2);
    }

    // google.charts.load('current', {'packages':['bar']});
    // google.charts.setOnLoadCallback(drawChart);

    // function drawChart() {

    //     var data = google.visualization.arrayToDataTable([
    //       ['Genre', 'Fantasy & Sci Fi', 'Romance', 'Mystery/Crime', 'General',
    //        'Western', 'Literature', { role: 'annotation' } ],
    //       ['2010', 10, 24, 20, 32, 18, 5, ''],
    //       ['2020', 16, 22, 23, 30, 16, 9, ''],
    //       ['2030', 28, 19, 29, 30, 12, 13, '']
    //     ]);

    //     var options_fullStacked = {
    //       isStacked: 'percent',
    //       width: 900,
    //       height: 600,
    //       legend: {position: 'top', maxLines: 3},
    //       hAxis: {
    //         minValue: 0,
    //         ticks: [0, .3, .6, .9, 1]
    //       }
    //     };

    //     var chart_fullStacked = new google.visualization.BarChart(
    //         document.getElementById('full_stacked_div'));
    //     chart_fullStacked.draw(data, options_fullStacked);
    // }

</script>
@endif

@stop

@section('titulo')
    <i class="fa fa-birthday-cake fa-fw"></i> Edad
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-money fa-fw"></i> Listado
        </div>
        <div class="panel-body">

            {!! Form::open(array('method' => 'GET', 'url' => route('manage.informes.edad'), 'role' => 'form', 'class' => '')) !!}

            <div class="form-group row">
                <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    {{-- @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categorias'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'subcategorias']) --}}
                </div>
            </div>

            <div class="form-group row">

                <div class="col-md-2">
                {!! Form::label('oficinas', 'Oficina') !!}
                @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])
                <br>
                {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-oficinas'))  !!}
                </div>

                <div class="col-md-2">
                {!! Form::label('categorias', 'Categoría') !!}
                @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                <br>
                {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                </div>

                <div class="col-md-2">
                {!! Form::label('subcategorias', 'Subcategoría') !!}
                @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                <br>
                {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
                </div>

            </div>

            <div class="form-group row">
                <div class="col-md-2">
                {!! Form::label('any', 'Año Booking') !!}
                <br>
                {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any'))  !!}
                </div>

                <div class="col-md-4">
                    {!! Form::label('filtro1','(Fechas: Inicio Booking)') !!}<br>
                    {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                </div>
            </div>


            {!! Form::close() !!}

            <hr>

            @if($listado)

                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#tab-edades" aria-controls="tab-edades" role="tab" data-toggle="tab">% Edades</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab-graficos" aria-controls="tab-graficos" role="tab" data-toggle="tab">Gráficos</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab-listado" aria-controls="tab-listado" role="tab" data-toggle="tab">Listado [{{$total}} Bookings]</a>
                    </li>
                </ul>

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in" id="tab-graficos">

                        <div class="row">
                            <div class="col-md-12">

                                <div id="dashboard2_div">
                                    <table class="table table-bordered columns">
                                        <caption><h3>Edades x País</h3></caption>
                                        <tr>
                                            <td><div id="select2a_div"></div></td>
                                            <td><div id="select2b_div"></div></td>
                                        </tr>
                                        <tr>
                                            <td colspan='2'><div id="chart2_div"></div></td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-md-12">

                                <div id="chart_edad_pais">
                                    <table class="table table-bordered columns">
                                        <caption><h3>Edades x País</h3></caption>
                                        <tr>
                                            <td><div id="control"></div></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan='2'><div id="chart"></div></td>
                                        </tr>
                                    </table>
                                </div>

                                @dashboard('EdadesPais', 'chart_edad_pais')
                            </div>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade in active" id="tab-edades">

                        <div class="informe-tabla"><div>

                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <caption><h3>% Edades [{{$total}} Bookings]</h3></caption>
                                <thead class="flip-content">
                                    <tr>
                                        <th class='td-fijo-1'>Edad</th>
                                        @foreach($edades as $kedad=>$edad)
                                            <th>{{$kedad}}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th class='td-fijo-1'>%</th>
                                        @foreach($edades as $edad)
                                            <td>{{round($edad/$total * 100,2)}}</td>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        <th class='td-fijo-1'>Total</th>
                                        @foreach($edades as $edad)
                                            <td>{{$edad}}</td>
                                        @endforeach
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <caption><h3>% Edades x País</h3></caption>
                                <thead class="flip-content">
                                    <tr>
                                        <th class='td-fijo-1'>Edad</th>
                                        @foreach($edades as $kedad=>$edad)
                                            <th>{{$kedad}}</th>
                                        @endforeach
                                        <th>Total</th>
                                    </tr>

                                </thead>
                                <tbody>

                                    @foreach($paises as $kpais=>$tpais)
                                    <tr>
                                        <th class='td-fijo-1'>{{$kpais}} [{{$tpais}}]</th>
                                        @foreach($edades as $kedad=>$edad)

                                            <?php
                                                $v = 0;
                                                $vv = 0;
                                                $t = $total;
                                                if(isset($edades_pais[$kedad][$kpais]))
                                                {
                                                    $v = $edades_pais[$kedad][$kpais];
                                                    $vv = $v;
                                                    $t = $edades[$kedad];

                                                    $v = round($v/$t * 100,2);
                                                }
                                            ?>

                                            {{-- <td>{{$vv}}:{{$v}}</td> --}}
                                            <td>{{$v}}</td>

                                        @endforeach

                                        <th>{{round($tpais/$total * 100,2)}}</th>

                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>

                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <caption><h3>% Edades x Tipo Alojamiento</h3></caption>
                                <thead class="flip-content">
                                    <tr>
                                        <th class='td-fijo-1'>Edad</th>
                                        @foreach($edades as $kedad=>$edad)
                                            <th>{{$kedad}}</th>
                                        @endforeach
                                        <th>Total</th>
                                    </tr>

                                </thead>
                                <tbody>

                                    @foreach($alojamientos as $kaloj=>$taloj)
                                    <tr>
                                        <th class='td-fijo-1'>{{$kaloj}} [{{$taloj}}]</th>
                                        @foreach($edades as $kedad=>$edad)

                                            <?php
                                                $v = 0;
                                                $t = $total;
                                                if(isset($edades_alojamiento[$kedad][$kaloj]))
                                                {
                                                    $v = $edades_alojamiento[$kedad][$kaloj];
                                                    $t = $edades[$kedad];

                                                    $v = round($v/$t * 100,2);
                                                }
                                            ?>

                                            <td>{{$v}}</td>

                                        @endforeach

                                        <th>{{round($taloj/$total * 100,2)}}</th>

                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>

                        </div></div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="tab-listado">

                        {!! Datatable::table()
                            ->addColumn([
                              'booking' => 'Booking',
                              'edad'    => 'Edad',
                              'pais'    => 'Pais',
                              'alojamiento' => 'Alojamiento',
                              'tipo'    => 'Tipo'
                            ])
                            ->setUrl(route('manage.informes.edad',$valores))
                            ->setOptions('iDisplayLength', 100)
                            ->render()
                        !!}

                    </div>
                </div>

            @else
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @endif

        </div>

    </div>

@stop