@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.aportes') !!}
@stop

@section('titulo')
    <i class="fa fa-plane fa-fw"></i> Aportes
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.aportes'), 'method'=> 'GET', 'class' => 'form']) !!}

            <div class="form-group row">

                <div class="col-md-1">
                    {!! Form::label('any','Año') !!}
                    {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                </div>
                <div class="col-md-1">-o-</div>

                <div class="col-md-2">
                    {!! Form::label('desde','Desde') !!}
                    @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                </div>
                <div class="col-md-2">
                    {!! Form::label('hasta','Hasta') !!}
                    @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                </div>

                <div class="col-md-2 col-md-offset-4">
                    {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                    {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                </div>

            </div>

            {!! Form::close() !!}

        </div>
    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Inscripciones</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong><i class="fa fa-list fa-fw"></i> Listado {{$valores['desdes']}} - {{$valores['hastas']}}</strong>
                    </div>
                    <div class="panel-body">

                        {!! Datatable::table()
                            ->addColumn([
                              'viajero' => 'Viajero',
                              'fechanac' => 'Fecha Nac.',
                              'localizador' => 'Localizador',
                              'agencia' => 'Agencia',
                              'origen' => 'Aeropuerto origen',
                              'comentarios' => 'Comentarios',
                              'vuelo' => 'Vuelo',
                              'convocatoria' => 'Convocatoria',
                              'plataforma' => 'Plataforma',
                            ])
                            ->setUrl(route('manage.informes.aportes', $valores))
                            ->setOptions('iDisplayLength', 100)
                            ->setOptions(
                              "columnDefs", array(
                                // [ "sortable" => false, "targets" => [10] ],
                                [ "targets" => [1], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                              )
                            )
                            ->render()
                        !!}

                    </div>
                </div>

            @endif

        </div>
    </div>

@stop