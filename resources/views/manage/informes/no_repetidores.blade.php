@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'No repetidores', 'manage.informes.no-repetidores') !!}
@stop

@section('titulo')
    <i class="fa fa-user fa-fw"></i> No repetidores
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-money fa-fw"></i> Listado
        </div>
        <div class="panel-body">

            {!! Form::open(array('method' => 'GET', 'url' => route('manage.informes.no-repetidores'), 'role' => 'form', 'class' => '')) !!}

            <div class="form-group row">
                <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    {{-- @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categorias'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'subcategorias']) --}}
                </div>
            </div>

            <div class="form-group row">

                <div class="col-md-2">
                {!! Form::label('oficinas', 'Oficina') !!}
                @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])
                <br>
                {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-oficinas'))  !!}
                </div>

                <div class="col-md-2">
                {!! Form::label('categorias', 'Categoría') !!}
                @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                <br>
                {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                </div>

                <div class="col-md-2">
                {!! Form::label('subcategorias', 'Subcategoría') !!}
                @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                <br>
                {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
                </div>

            </div>

            <div class="form-group row">
                <div class="col-md-2">
                {!! Form::label('any1', 'Año Booking') !!}
                <br>
                {!! Form::select('any1', $anys, $valores['any1'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any1'))  !!}
                </div>

                <div class="col-md-2">
                {!! Form::label('any', 'Año sin Booking') !!}
                <br>
                {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any'))  !!}
                </div>

                <div class="col-md-4">
                    {!! Form::label('filtro1','(=Fecha 1er pago)') !!}<br>
                    {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                </div>
            </div>


            {!! Form::close() !!}

            <hr>

            @if($listado)

                {!! Datatable::table()
                    ->addColumn([
                      'viajero'      => 'Viajero',
                      'convocatoria' => 'Convocatoria',
                      'fecha'           => 'Fecha',
                      'edad'        => 'Edad',
                      'tutor1_nombre' => 'Tutor1',
                      'tutor1_movil' => 'Tutor1 movil',
                      'tutor2_nombre' => 'Tutor2',
                      'tutor2_movil' => 'Tutor2 movil',
                      'prescriptor' => 'Prescriptor'
                    ])
                    ->setUrl(route('manage.informes.no-repetidores',$valores))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "columnDefs", array(
                        //[ "sortable" => false, "targets" => [0] ],
                        [ "targets" => [2], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                      )
                    )
                    ->render() !!}

            @else
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @endif

        </div>

    </div>

@stop