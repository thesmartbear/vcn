@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Datos InfoPersonal Camps', 'manage.informes.cic-camps') !!}
@stop

@section('titulo')
    <i class="fa fa-list fa-fw"></i> Datos InfoPersonal Camps
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.cic-camps'), 'method'=> 'GET', 'class' => 'form']) !!}


                <div class="form-group row">

                    <div class="col-md-5">
                    {!! Form::label('cursos', 'Curso') !!}
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])

                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'cursos', 'destino'=> 'convocatorias'])
                    </div>

                    <div class="col-md-6">
                    {!! Form::label('convocatorias', 'Convocatoria') !!}
                    @include('includes.form_input_cargando',['id'=> 'convocatorias-cargando'])
                    <br>
                    {!! Form::select('convocatorias', $convocatorias, $valores['convocatorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-convocatorias'))  !!}
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>
                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-2">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}

        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-globe fa-fw"></i> Listado
                        </div>
                        <div class="panel-body">

                            <?php

                                $cols = [
                                    'apellidos'     => 'Cognoms',
                                    'nombre'        => 'Nom',
                                    'convocatoria'  => 'Convocatoria',
                                    'fechanac'      => 'DOB',
                                    'sexo'          => 'Sex',
                                ];

                                //area.campamento.$campo
                                if($datos_campamento)
                                {
                                    foreach($datos_campamento as $campo)
                                    {
                                        $nom = substr($campo, 11);
                                        $cols[$campo] = trans("area.campamento.".$nom);
                                    }
                                }

                                $cols += ['log'=> 'Log'];
                            ?>

                            {!! Datatable::table()
                                ->addColumn($cols)
                                ->setUrl( route('manage.informes.cic-camps', $valores) )
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                    "columnDefs", array(
                                        [ "targets" => [3], "render"=> "function(data, type, full) {return moment(data).isValid()?moment(data).format('DD/MM/YYYY'):'-';}" ],
                                    )
                                )
                                ->render() !!}

                        </div>
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop