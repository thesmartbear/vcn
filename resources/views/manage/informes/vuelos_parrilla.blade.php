@extends('layouts.manage')

@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('manage.informes.vuelos-parrilla') !!} --}}
@stop

@section('titulo')
    <i class="fa fa-plane fa-fw"></i> Parrilla vuelos
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.vuelos-parrilla'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">

                    <div class="col-md-2">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                    </div>

                    <div class="col-md-2">
                    {!! Form::label('subcategorias', 'Subcategoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                    <br>
                    {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('aeropuerto', 'Aeropuerto') !!}
                    @include('includes.form_input_cargando',['id'=> 'aeropuertos-cargando'])
                    <br>
                    {!! Form::select('aeropuertos', $aeropuertos, $valores['aeropuertos'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-aeropuertos'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        {!! Form::label('Consultar:') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}

        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Vuelos</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><i class="fa fa-list fa-fw"></i> Listado </strong>
                        </div>
                        <div class="panel-body">

                            {!! Datatable::table()
                                ->addColumn([
                                    'convocatoria'  => 'Convocatoria',
                                    'vuelo'         => 'Vuelo',
                                    'company'       => 'Aerolínea',
                                    'nvuelo'        => 'N.Vuelo',
                                    'localizador'   => 'Localizador',
                                    'terminal'      => 'Terminal',
                                    'fecha'         => 'Fecha',
                                    'encuentro'     => 'H. Encuentro',
                                    'salida'        => 'H. Salida',
                                    'monitor'       => 'P.Monitor',
                                    'plazas'        => 'PAX'

                                ])
                                ->setUrl( route('manage.informes.vuelos-parrilla', $valores) )
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                  "columnDefs", array(
                                    [ "targets" => [6], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                  )
                                )
                                ->render() !!}


                        </div>
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop