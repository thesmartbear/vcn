<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>

        {!! Html::style('assets/css/pdf.css') !!}
        {!! Html::style('assets/css/bootstrap.css') !!}

    </head>
    <body>
    @foreach(ConfigHelper::plataformas(false) as $p=>$plataforma)
    <div class="page"></div>
        <?php
            $valores['plataformas'] = $p;
        ?>

        <h1>Listado Monitores [{{$plataforma}}]</h1>
        <i>{{Carbon::now()->format("d/m/Y H:i")}}</i>

        <hr>
        <h4>Filtros:</h4>
        Categoria: {{$valores_txt['categorias']}}
        <br>
        Centro: {{$valores_txt['centros']}}, Curso: {{$valores_txt['cursos']}}

        <hr>

        @foreach($tabs as $ktab=>$vtab)

            <?php
                $valores['cursos'] = $vtab;
                $curso = \VCN\Models\Cursos\Curso::find($vtab);
                $bookings = \VCN\Models\Bookings\Booking::listadoFiltros($valores, true);

                $f = $curso->convocatoria_tipo_num;
                $ftipoc = ConfigHelper::getConvocatoriaIndex($f);

                $convos = clone $bookings;
            ?>

            <h3>Curso: {{ $curso?$curso->name:"-" }}</h3>

            @foreach($convos->groupBy($ftipoc)->groupBy('course_start_date')->groupBy('course_end_date')->orderBy('fecha_reserva')->get() as $convo)

                <?php
                    $convocatoria = $convo->convocatoria;
                ?>

                <div class="page">
                    <table class="table table-bordered no-footer">
                    <caption>
                        <h4>Convocatoria: {{$convocatoria->name}} ({{$convo->curso_start_date}} - {{$convo->curso_end_date}})</h4>
                    </caption>
                    <thead>
                        <tr>
                            {{-- <th>Fechas</th> --}}
                            <th>Apellido1</th>
                            <th>Apellido2</th>
                            <th>Nombre</th>
                            <th>Convocatoria</th>
                            <th>Fecha Nac.</th>
                            <th>Teléfono</th>
                            <th>Móvil</th>
                            <th>Tutor1 Móvil</th>
                            <th>Tutor2 Móvil</th>
                            <th>Población</th>
                            <th>Notas</th>
                            <th>Empresa</th>
                            <th>Alojamiento</th>
                            <th>Extras</th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php
                        $bookingsfe = clone $bookings;
                    ?>

                    @foreach($bookingsfe->where($ftipoc,$convo->convocatory_id)->where('course_start_date',$convo->course_start_date)->where('course_end_date',$convo->course_end_date)->orderBy('fecha_reserva')->get()->sortBy('viajero.lastname') as $model)
                        <tr>
                            {{-- <td>{{$model->id}}:({{$model->convocatory_id}}) {{$model->curso_start_date}} - {{$model->curso_end_date}}</td> --}}
                            <td>{{$model->viajero->lastname}}</td>
                            <td>{{$model->viajero->lastname2}}</td>
                            <td>{{$model->viajero->name}}</td>
                            <td>{{$model->convocatoria->name}}</td>
                            <td>{{Carbon::parse($model->viajero->fechanac)->format('d/m/Y')}}</td>
                            <td>{{$model->viajero->telefono}}</td>
                            <td>{{$model->viajero->movil}}</td>
                            <td>{{$model->viajero->tutor1?$model->viajero->tutor1->movil:"-"}}</td>
                            <td>{{$model->viajero->tutor2?$model->viajero->tutor2->movil:"-"}}</td>
                            <td>{{$model->viajero->datos->ciudad}}</td>
                            <td>
                                <?php
                                    $ret = $model->notas2?$model->notas2:"";
                                    if($model->datos)
                                    {
                                        $ret .= $model->datos->alergias2?"Allergies: ".$model->datos->alergias2.". ":"";
                                        $ret .= $model->datos->enfermedad2?"Illnes: ".$model->datos->enfermedad2.". ":"";
                                        $ret .= $model->datos->medicacion2?"Medication: ".$model->datos->medicacion2.". ":"";
                                        $ret .= $model->datos->tratamiento2?"Treatment: ".$model->datos->tratamiento2.". ":"";
                                        $ret .= $model->datos->dieta2?"Diet: ".$model->datos->dieta2.". ":"";
                                    }
                                ?>

                                {{$ret}}
                            </td>
                            <td>{{ConfigHelper::plataforma($model->viajero->plataforma)}}</td>
                            <td>{{$model->alojamiento?($model->alojamiento->tipo?$model->alojamiento->tipo->name:"-"):"-"}}</td>
                            <td>{{$model->extras_name}}</td>
                        </tr>

                        @if($model->familias->count()>0)

                            <tr>
                            <td colspan="14">

                                <table class="table table-bordered no-footer">
                                    <thead>
                                        <th>Familia</th>
                                        <th>Dirección</th>
                                        <th>Población</th>
                                        <th>Tlf1</th>
                                        <th>Tlf2</th>
                                        <th>Tlf3</th>
                                    </thead>
                                    <tbody>
                                        @foreach($model->familias as $fam)
                                            <?php
                                                $familia = $fam->familia;
                                            ?>
                                            <tr>
                                                <td>
                                                    {{$familia->name}}
                                                </td>
                                                <td>
                                                    {{$familia->direccion}}
                                                </td>
                                                <td>
                                                    {{$familia->poblacion}}
                                                </td>
                                                <td>
                                                    {{$familia->telefono}}
                                                </td>
                                                <td>
                                                    <?php
                                                    $tlf = "";
                                                    if($familia->adultos)
                                                    {
                                                        if(isset($familia->adultos['movil']))
                                                        {
                                                            if(isset($familia->adultos['movil'][0]))
                                                            {
                                                                $tlf = $familia->adultos['movil'][0]?$familia->adultos['movil'][0]:"";
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    {{$tlf}}
                                                </td>
                                                <td>
                                                    <?php
                                                    $tlf = "";
                                                    if($familia->adultos)
                                                    {
                                                        if(isset($familia->adultos['movil']))
                                                        {
                                                            if(isset($familia->adultos['movil'][1]))
                                                            {
                                                                $tlf = $familia->adultos['movil'][1]?$familia->adultos['movil'][1]:"";
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    {{$tlf}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </td></tr>

                        @endif

                    @endforeach

                    </tbody>
                </table>
                </div>

            @endforeach

            <hr>

        @endforeach

    @endforeach
    </body>
</html>