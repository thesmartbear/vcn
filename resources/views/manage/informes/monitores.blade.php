@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Listado Monitores', 'manage.informes.monitores') !!}
@stop

@section('titulo')
    <i class="fa fa-users fa-fw"></i> Listado Monitores
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.monitores'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    {{-- <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'cursos'])
                    </div> --}}

                    <div class="col-md-3">
                    {!! Form::label('tipoc', 'Tipo Convocatoria') !!}
                    @include('includes.form_input_cargando',['id'=> 'tipoc-cargando'])
                    <br>
                    {!! Form::select('tipoc', ConfigHelper::getConvocatoriaTipo(), $valores['tipoc'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-tipoc'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'cursos'])
                    @include('includes.script_filtros_multi', ['filtro'=> 'tipoc', 'destino'=> 'convocatorias'])
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-4">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-6">
                    {!! Form::label('centros', 'Centro') !!}
                    @include('includes.form_input_cargando',['id'=> 'centros-cargando'])
                    <br>
                    {!! Form::select('centros', $centros, $valores['centros'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-centros'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'cursos'])
                    @include('includes.script_filtros_multi', ['filtro'=> 'centros', 'destino'=> 'convocatorias'])
                    </div>

                    <div class="col-md-6">
                    {!! Form::label('cursos', 'Curso') !!}
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])
                    <br>
                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-cursos'))  !!}
                    @include('includes.script_filtros_multi', ['filtro'=> 'cursos', 'destino'=> 'convocatorias'])
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-4">
                    {!! Form::label('convocatorias', 'Convocatoria') !!}
                    @include('includes.form_input_cargando',['id'=> 'convocatorias-cargando'])
                    <br>
                    {{-- {!! Form::select('convocatorias', $convocatorias, $valores['convocatorias'], array('class'=>'select2 col-md-12', 'data-style'=>'red', 'id'=>'filtro-convocatorias'))  !!} --}}
                    {!! Form::select('convocatorias', $convocatorias, ($valores['convocatorias'] ?: []), array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-convocatorias', 'name'=> 'convocatorias[]'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}
        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
                    @if($results)
                        &nbsp;<a target='_blank' href="{{route('manage.informes.monitores.pdf',$valores)}}" class='btn btn-danger'><i class='fa fa-file-pdf-o'></i> PDF</a>
                    @endif
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                    <ul class="nav nav-tabs" role="tablist">
                    @foreach(ConfigHelper::plataformas(false) as $p=>$plataforma)
                        <li role="presentation" class="">
                            <a href="#tab-plat-{{$p}}" aria-controls="tab-plat-{{$p}}" role="tab" data-toggle="tab">
                            {{$plataforma}}
                            </a>
                        </li>
                    @endforeach
                    </ul>

                    <div class="tab-content">
                    @foreach(ConfigHelper::plataformas(false) as $p=>$plataforma)
                        <div role="tabpanel" class="tab-pane fade in" id="tab-plat-{{$p}}">

                            <?php
                                $valores['plataformas'] = $p;
                            ?>
                            

                            <ul class="nav nav-tabs" role="tablist">
                            @foreach($tabs[$p] as $ktab=>$vtab)
                                <?php
                                $curso = \VCN\Models\Cursos\Curso::find($vtab);
                                $curso_name = $curso->name;
                                ?>
                                <li role="presentation" class="{{($vtab === reset($tabs[$p]))?'active':''}}">
                                    <a href="#tab-{{$ktab}}" aria-controls="tab-{{$ktab}}" role="tab" data-toggle="tab">
                                    {{$curso_name}}
                                    </a>
                                </li>
                            @endforeach
                            </ul>

                            <div class="tab-content">
                            @foreach($tabs[$p] as $ktab=>$vtab)
                                <?php
                                $curso_name = \VCN\Models\Cursos\Curso::find($vtab)->name;
                                ?>
                                <div role="tabpanel" class="tab-pane fade in {{($vtab === reset($tabs[$p]))?'active':''}}" id="tab-{{$ktab}}">

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="fa fa-list fa-fw"></i> Listado {{$curso_name}} : {{$valores['desdes']}} - {{$valores['hastas']}}
                                        </div>
                                        <div class="panel-body">

                                            <?php
                                                $valores['cursos'] = $vtab;
                                            ?>

                                            {!! Datatable::table()
                                                ->addColumn([
                                                    'apellido1'     => 'Apellido1',
                                                    'apellido2'     => 'Apellido2',
                                                    'nombre'        => 'Nombre',
                                                    'convocatoria'  => 'Convocatoria',
                                                    'fechanac'      => 'Fecha Nac.',
                                                    'telefono'      => 'Teléfono',
                                                    'movil'         => 'Móvil',
                                                    'tutor1_movil'  => 'Tutor1 móvil',
                                                    'tutor2_movil'  => 'Tutor2 móvil',
                                                    'poblacion'     => 'Población',
                                                    'notas'         => 'Notas Booking',
                                                    'plataforma'    => 'Empresa',
                                                    'alojamiento'   => 'Alojamiento',
                                                    'familia'       => 'Familia',
                                                    'extras'        => 'Extras'
                                                ])
                                                ->setUrl(route('manage.informes.monitores',$valores))
                                                ->setOptions('iDisplayLength', 100)
                                                ->setOptions(
                                                  "columnDefs", array(
                                                    // [ "sortable" => false, "targets" => [0,1,2,3,7,8,9,10,11,12] ],
                                                    [ "targets" => [4], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                                  )
                                                )
                                                ->render()
                                            !!}

                                        </div>
                                    </div>

                                </div>

                            @endforeach
                            </div>
                        
                        </div>
                    @endforeach
                    </div>

           @endif


        </div>

    </div>

@stop