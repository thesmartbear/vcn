@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Actividad Usuarios', 'manage.informes.actividad') !!}
@stop

@section('titulo')
    <i class="fa fa-briefcase fa-fw"></i> Actividad Usuarios
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.actividad'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    {{-- @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas']) --}}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'users'])
                    </div>

                    {{-- <div class="col-md-3">
                    {!! Form::label('oficinas', 'Oficina') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'multiple'=>'multiple', 'id'=>'filtro-oficinas', 'name'=> 'oficinas[]'))  !!}
                    </div> --}}

                    <div class="col-md-3">
                        {!! Form::label('users', 'Usuarios') !!}
                        @include('includes.form_input_cargando',['id'=> 'users-cargando'])
                        <br>
                        {!! Form::select('users', $users, $valores['users'], array('class'=> 'select2 form-control', 'multiple'=>'multiple', 'id'=>'filtro-users', 'name'=> 'users[]'))  !!}
                    </div>
                </div>

                <hr>
                <div class="form-group row">
                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>$valores['desdec']])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>$valores['hastac']])
                    </div>

                    <div class="col-md-1 col-md-offset-3">
                        {!! Form::label('Días') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro_fechas', 'class' => 'btn btn-info')) !!}
                    </div>

                    <div class="col-md-1">
                        {!! Form::label('Semanas') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro_semanas', 'class' => 'btn btn-info')) !!}
                    </div>

                    <div class="col-md-1">
                        {!! Form::label('Meses') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro_meses', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}
        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Resultados {{$listado?" por $modo":""}}</span>
            </div>
        </div>

        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                <ul class="nav nav-tabs" role="tablist">
                @foreach($usuarios as $user_id=>$user)
                    <li role="presentation" class="{{($user === reset($usuarios))?'active':''}}">
                        <a href="#tab-{{$user_id}}" aria-controls="tab-{{$user_id}}" role="tab" data-toggle="tab">
                        {{$user}}
                        </a>
                    </li>
                @endforeach
                </ul>

                <div class="tab-content">

                    @foreach($usuarios as $user_id=>$user)
                        <div role="tabpanel" class="tab-pane fade in {{($user === reset($usuarios))?'active':''}}" id="tab-{{$user_id}}">

                        <?php

                            $user = \VCN\Models\User::find($user_id);

                            $solicitudes = clone $resultados['solicitudes'];
                            $solicitudes = $solicitudes->where('user_id',$user_id);

                            $bookings = clone $resultados['bookings'];
                            $bookings = $bookings->where('user_id',$user_id);

                            $presupuestos = clone $resultados['presupuestos'];
                            $presupuestos = $presupuestos->where('user_id',$user_id);

                            $seguimientos1 = clone $seguimientos[$user_id];
                            $seguimientos2 = clone $seguimientos[$user_id];
                        ?>

                        <div class="informe-tabla"><div>

                        {{-- SEGUIMIENTOS ENVIADOS --}}
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <caption><h3>Seguimientos Enviados</h3></caption>
                            <thead class="flip-content">

                                <tr>
                                    <th class='td-fijo'>Tipo</th>
                                    @foreach($fechas_txt as $fdm)
                                        <th>{{$fdm}}</th>
                                    @endforeach
                                </tr>

                            </thead>
                            <tbody>

                                @foreach(ConfigHelper::getTipoSeguimientoEnviado() as $icat => $cat)
                                <tr>
                                    <td class='td-fijo'>{{$cat}}</td>
                                    @foreach($fechas_txt as $idia => $fdm)
                                        <?php
                                            $item = $seguimientos1->where('fecha_grup',$fechas[$idia])->where('tipo',"$icat")->first();
                                        ?>
                                        <td>{{$item?$item->cuenta:0}}</td>
                                    @endforeach
                                </tr>
                                @endforeach

                            </tbody>
                        </table>

                        {{-- SEGUIMIENTOS RECIBIDOS --}}
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <caption><h3>Seguimientos Recibidos</h3></caption>
                            <thead class="flip-content">

                                <tr>
                                    <th class='td-fijo'>Tipo</th>
                                    @foreach($fechas_txt as $fdm)
                                        <th>{{$fdm}}</th>
                                    @endforeach
                                </tr>

                            </thead>
                            <tbody>

                                @foreach(ConfigHelper::getTipoSeguimientoRecibido() as $icat => $cat)
                                <tr>
                                    <td class='td-fijo'>{{$cat}}</td>
                                    @foreach($fechas_txt as $idia => $fdm)
                                        <?php
                                            $item = $seguimientos2->where('fecha_grup',$fechas[$idia])->where('tipo',$icat)->first();
                                        ?>
                                        <td>{{$item?$item->cuenta:0}}</td>
                                    @endforeach
                                </tr>
                                @endforeach

                            </tbody>
                        </table>

                        {{-- SOLICITUDES --}}
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <caption><h3>Solicitudes</h3></caption>
                            <thead class="flip-content">

                                <tr>
                                    <th class='td-fijo'>Categoría</th>
                                    @foreach($fechas_txt as $fdm)
                                        <th>{{$fdm}}</th>
                                    @endforeach
                                </tr>

                            </thead>
                            <tbody>

                                @foreach($categorias_solicitud as $icat => $cat)
                                <tr>
                                    <td class='td-fijo'>{{$cat}}</td>
                                    @foreach($fechas_txt as $idia => $fdm)
                                        <?php
                                            $item = $solicitudes->where('fecha_grup',$fechas[$idia])->where('category_id',$icat)->first();
                                        ?>
                                        <td>{{$item?$item->cuenta:0}}</td>
                                    @endforeach
                                </tr>
                                @endforeach

                            </tbody>
                        </table>

                        {{-- BOOKINGS --}}
                        <table class="table table-bordered">
                            <caption><h3>Bookings</h3></caption>
                            <thead class="flip-content">

                                <tr>
                                    <th class='td-fijo'>Categoría</th>
                                    @foreach($fechas_txt as $fdm)
                                        <th>{{$fdm}}</th>
                                    @endforeach
                                </tr>

                            </thead>
                            <tbody>

                                @foreach($categorias_booking as $icat => $cat)
                                <tr>
                                    <td class='td-fijo'>{{$cat}}</td>
                                    @foreach($fechas_txt as $idia => $fdm)
                                        <?php
                                            $item = $bookings->where('fecha_grup',$fechas[$idia])->where('category_id',$icat)->first();
                                        ?>
                                        <td>{{$item?$item->cuenta:0}}</td>
                                    @endforeach
                                </tr>
                                @endforeach

                            </tbody>
                        </table>

                        {{-- PRESUPUESTOS --}}
                        <table class="table table-bordered">
                            <caption><h3>Presupuestos</h3></caption>
                            <thead class="flip-content">

                                <tr>
                                    <th class='td-fijo'>Categoría</th>
                                    @foreach($fechas_txt as $fdm)
                                        <th>{{$fdm}}</th>
                                    @endforeach
                                </tr>

                            </thead>
                            <tbody>

                                @foreach($categorias_presupuesto as $icat => $cat)
                                <tr>
                                    <td class='td-fijo'>{{$cat}}</td>
                                    @foreach($fechas_txt as $idia => $fdm)
                                        <?php
                                            $item = $presupuestos->where('fecha_grup',$fechas[$idia])->where('category_id',$icat)->first();
                                        ?>
                                        <td>{{$item?$item->cuenta:0}}</td>
                                    @endforeach
                                </tr>
                                @endforeach

                            </tbody>
                        </table>

                        </div></div>


                        </div>
                    @endforeach

                </div>

            @endif
        </div>

    </div>

@stop