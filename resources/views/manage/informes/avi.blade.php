@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Listado AVI', 'manage.informes.avi') !!}
@stop

@section('titulo')

    <i class="fa fa-ticket fa-fw"></i> Listado AVI

    <span class="badge badge-danger" style='letter-spacing: 0.1em'>(Para ver sólo los que tienen seguro médico seleccionado, seleccione el check 'AVI adultos')</span>

@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.avi'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categorias'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'proveedores'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-3">
                    {!! Form::label('oficinas', 'Oficina') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-oficinas'))  !!}
                    </div>

                    <div class="col-md-3">
                    {!! Form::label('tipoc', 'Tipo Convocatoria') !!}
                    @include('includes.form_input_cargando',['id'=> 'tipoc-cargando'])
                    <br>
                    {!! Form::select('tipoc', ConfigHelper::getConvocatoriaTipo(), $valores['tipoc'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-tipoc'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'cursos'])
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-4">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('subcategorias', 'SubCategoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                    <br>
                    {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'id'=>'filtro-subcategorias'))  !!}
                    </div>

                    <div class="col-md-3"></div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                    {!! Form::label('proveedores', 'Proveedor') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'proveedores-cargando'])

                    {!! Form::select('proveedores', $proveedores, $valores['proveedores'], array('class'=>'select2', 'data-style'=>'purple', 'id'=>'filtro-proveedores'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('centros', 'Centro') !!}
                    @include('includes.form_input_cargando',['id'=> 'centros-cargando'])
                    <br>
                    {!! Form::select('centros', $centros, $valores['centros'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-centros'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'cursos'])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-5">
                    {!! Form::label('cursos', 'Curso') !!}
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])

                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    {{--
                    <div class="col-md-1">
                        Año:
                    </div>
                    --}}

                    <div class="col-md-3">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=> $valores['desde']])
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=> $valores['hasta']])
                    </div>

                    <div class="col-md-1">
                        @include('includes.form_checkbox', [ 'campo'=> 'adultos', 'texto'=> 'AVI Adultos', 'valor'=> $valores['adultos'] ])
                    </div>

                    <div class="col-md-2 col-md-offset-2">
                        {!! Form::label('(Fechas: 1r Pago)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>
                    <div class="col-md-1">
                        {!! Form::label('&nbsp;') !!}
                        {!! Form::submit('Exportar', array( 'name'=> 'xls', 'class' => 'btn btn-success')) !!}
                    </div>

                </div>

            {!! Form::close() !!}
        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings {{$valores['adultos']?" Adultos":""}}</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <ul class="nav nav-tabs" role="tablist">
                    @foreach($tabs as $k=>$tab)

                        @foreach($tabs[$k] as $kc=>$tabconvo)
                        <li role="presentation">
                            <a href="#tab-{{$k}}-{{$tabconvo}}" aria-controls="tab-{{$k}}-{{$tabconvo}}" role="tab" data-toggle="tab">
                            <?php
                                $convo = null;
                                switch($k)
                                {
                                    case 1:
                                    case 2:
                                    case 5:
                                    {
                                        $convo = \VCN\Models\Convocatorias\Cerrada::find($tabconvo);
                                    }
                                    break;

                                    case 3:
                                    {
                                        $convo = \VCN\Models\Convocatorias\Abierta::find($tabconvo);
                                    }
                                    break;

                                    case 4:
                                    {
                                        $convo = \VCN\Models\Convocatorias\ConvocatoriaMulti::find($tabconvo);
                                    }
                                    break;
                                }
                            ?>
                            {{$convo?$convo->name:"- Convocatoria NO EXISTE -"}}
                            </a>
                        </li>
                        @endforeach
                    @endforeach
                    </ul>

                    <div class="tab-content">

                        @foreach($tabs as $k=>$tab)
                            @foreach($tabs[$k] as $kc=>$tabconvo)

                                <div role="tabpanel" class="tab-pane fade in" id="tab-{{$k}}-{{$tabconvo}}">

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="fa fa-globe fa-fw"></i> Listado
                                        </div>
                                        <div class="panel-body">

                                            <?php
                                                $valores['tipoc'] = $k;
                                                $valores['convocatorias'] = $tabconvo;
                                            ?>

                                            {!! Datatable::table()
                                                ->addColumn([
                                                    'gender'        => 'Gender',
                                                    'apellido1'     => 'LastName1',
                                                    'apellido2'     => 'LastName2',
                                                    'name'          => 'FirstName',
                                                    'fechanac'      => 'Date of Birth',
                                                    'pais_home'     => 'Home Country',
                                                    'pais_dest'     => 'Dest. Country',
                                                    'program'       => 'Program',
                                                    'fecha_ini'     => 'Dept. Date',
                                                    'fecha_fin'     => 'Return Date',
                                                    'contable'      => 'Cod. Contable'
                                                ])
                                                ->setUrl( route('manage.informes.avi', $valores) )
                                                ->setOptions('iDisplayLength', 100)
                                                ->setOptions(
                                                  "columnDefs", array(
                                                    [ "targets" => [4,8,9], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                                  )
                                                )
                                                ->render() !!}


                                        </div>
                                    </div>

                                </div>

                            @endforeach
                        @endforeach

                    </div>

                @endif



           @endif


        </div>

    </div>

@stop