@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Edad', 'manage.informes.edad') !!}
@stop

@section('titulo')
    <i class="fa fa-globe fa-fw"></i> Comportamiento Viajeros
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-money fa-fw"></i> Listado
        </div>
        <div class="panel-body">

            {!! Form::open(array('method' => 'GET', 'url' => route('manage.informes.trasvase'), 'role' => 'form', 'class' => '')) !!}

            <div class="form-group row">
                <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                </div>

                <div class="col-md-2">
                {!! Form::label('oficinas', 'Oficina') !!}
                @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])
                <br>
                {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-oficinas'))  !!}
                </div>

            </div>

            <div class="form-group row">
                <div class="col-md-2">
                {!! Form::label('any0', 'Año Inicial') !!}
                <br>
                {!! Form::select('any0', $anys, $valores['any0'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any0'))  !!}
                </div>

                <div class="col-md-2">
                {!! Form::label('any', 'Año Destino') !!}
                <br>
                {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any'))  !!}
                </div>

                <div class="col-md-4">
                    {!! Form::label('filtro1','(Fechas: Inicio Booking)') !!}<br>
                    {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                </div>
            </div>


            {!! Form::close() !!}

            <hr>

            @if($listado)

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-globe fa-fw"></i> Comportamiento viajeros {{$any0}} => {{$any}}
                    </div>
                    <div class="panel-body">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#tablas" aria-controls="tablas" role="tab" data-toggle="tab">Datos</a></li>
                            <li role="presentation"><a href="#chart" aria-controls="chart" role="tab" data-toggle="tab">Gráfico</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="tablas">

                                @foreach($trasvase as $k=>$destinos)

                                    <?php
                                        $cat0 = \VCN\Models\Categoria::find($k);
                                    ?>

                                    <table class="table table-bordered table-striped table-condensed">
                                        <caption><h4>Categoría: {{$cat0?$cat0->name:"Sin Booking"}} {{$any0}} [{{$trasvase_cantidad[$k]}}]</h4></caption>
                                        <thead>
                                            <tr>
                                                <th>Categoría {{$any}}</th>
                                                <th>Bookings</th>
                                                <th>% vs Categoría en {{$any0}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($destinos as $kd => $cantidad)

                                                <?php
                                                    $cat = \VCN\Models\Categoria::find($kd);
                                                ?>

                                                <tr>
                                                    <td>{{$cat?$cat->name:"Sin Booking"}}</td>
                                                    <td>{{$cantidad}}</td>
                                                    <td>
                                                        {{round(($cantidad/$trasvase_cantidad[$k]*100), 2)}} %
                                                    </td>
                                                </tr>

                                            @endforeach


                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <th>{{$trasvase_cantidad[$k]}}</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>

                                @endforeach
                            </div>

                            <div role="tabpanel" class="tab-pane fade in" id="chart">

                                <div class="col-md-12">
                                    <div id="chart"></div>
                                    @sankeychart('Trasvase', 'chart')
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            @else
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @endif

        </div>

    </div>

@stop