@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Control Inscripciones', 'manage.informes.checklist') !!}
@stop

@section('titulo')
    <i class="fa fa-pencil-square fa-fw"></i> Control Inscripciones
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.checklist'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categorias'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('oficinas', 'Oficina') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-oficinas', 'name'=> 'oficinas[]'))  !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('paises', 'País') !!}
                        <br>
                        {!! Form::select('paises', $paises, $valores['paises'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-paises'))  !!}
                    </div>

                    <div class="col-md-3">
                        {!! Form::label('asignados', 'Asignado') !!}
                        <br>
                        {!! Form::select('asignados', $users, $valores['asignados'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-asignados'))  !!}
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-2">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('subcategorias', 'SubCategoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                    <br>
                    {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'id'=>'filtro-subcategorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'subcategorias', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-2">
                    {!! Form::label('cursos', 'Curso') !!}
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])
                    <br>
                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
                    </div>

                </div>

                <div class="form-group row">

                    {{-- <div class="col-md-2">
                    {!! Form::label('status', 'Status Booking') !!}
                    @include('includes.form_input_cargando',['id'=> 'status-cargando'])
                    <br>
                    {!! Form::select('status', $status, $valores['status'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-status'))  !!}
                    </div> --}}

                    <div class="col-md-3">
                        {!! Form::label('status', 'Status Booking') !!}
                        @include('includes.form_input_cargando',['id'=> 'status-cargando'])
                        <br>
                        {!! Form::select('status', $status, $valores['status'], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-status', 'name'=> 'status[]'))  !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('desde_pago','Desde (1r Pago)') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde_pago', 'texto'=> null, 'valor'=> $valores['desde_pago']])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta_pago','Hasta (1r Pago)') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta_pago', 'texto'=> null, 'valor'=> $valores['hasta_pago']])
                    </div>

                    <div class="col-md-3"></div>

                    <div class="col-md-1">
                        {!! Form::label('&nbsp;') !!}
                        {!! Form::submit('Consultar', array( 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=> $valores['desdes']])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=> $valores['hastas']])
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}

        </div>
    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Inscripciones</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-globe fa-fw"></i> Listado
                    </div>
                    <div class="panel-body">

                        <?php

                            $columns = [
                                'fecha'   => 'Fecha Booking',
                                'booking' => 'Booking',
                                'viajero' => 'Viajero',
                                'convocatoria'    => 'Convocatoria',
                                'pais'    => 'País',
                                'proveedor'   => 'Proveedor',
                                'duracion_name'   => 'Ud. Duración',
                                'oficina' => 'Oficina',
                                'usuario' => 'Usuario',
                            ];

                            foreach($checklist as $check)
                            {
                                $columns['chk_'.$check->id] = $check->status->name .": ". $check->name;
                            }
                        ?>

                        {!! Datatable::table()
                            ->addColumn($columns)
                            ->setUrl(route('manage.informes.checklist', $valores))
                            ->setOptions('iDisplayLength', 100)
                            ->setOptions('responsive', false)
                            ->setOptions('scrollY', '800px')
                            ->setOptions('scrollX', true)
                            ->setOptions('scrollCollapse', true)
                            ->setOptions('fixedColumns', ['leftColumns' => 3 ] )
                            ->setOptions(
                              "columnDefs", array(
                                [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                              )
                            )
                            ->render()
                        !!}

                    </div>
                </div>

           @endif


        </div>

    </div>

<script type="text/javascript">

function postChecklist( id, checkid, fecha )
{
    var $token = "{{ Session::token() }}";

    var $data = { '_token': $token, 'id': id, 'check_id': checkid, 'check_fecha': fecha };
    var $url = "/manage/bookings/ajax/" + id + "/checklist";

    console.log($url);
    console.log($data);
    // return;

    $.ajax({
        url: $url,
        type: 'POST',
        dataType : 'json',
        data: $data,
        success: function(data) { console.log(data);

            var $check = $("#check-"+data.id+"-"+data.check_id);

            if(data.result)
            {
                $check.removeClass().addClass('fa fa-check-circle-o fa-fw');
                h = $check.parent().html();
                $check.parent().html(h + "["+ data.fecha +"] ("+ data.user +")");
            }
            else
            {
                $check.removeClass().addClass('fa fa-circle-thin fa-fw');
            }

            if(data.status>0)
            {
                $('#status_id').val(data.status);
                // location.reload();
            }

        },
        error: function(xhr, desc, err) {
            console.log(xhr.responseText);
            console.log("Details: " + desc + "\nError:" + err);
        }

    }); // end ajax call

}

$(document).ready(function() {

    $('table.dataTable').on('click', '.status-check',function(e) {
    
        var $id = $(this).data('parent');
        var $checkid = $(this).data('id');

        $("#check-"+$checkid).removeClass().addClass('fa fa-spinner fa-spin fa-fw');

        var $input = "<div id='Dtt-logs-datetime' class='input-group date datetime'><input id='checklist-fecha' name='checklist-fecha' class='form-control' value='{{Carbon::now()->format('d/m/Y')}}'></input><span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span></div>";

        bootbox.dialog({
           message: $input,
           title: "Fecha Checklist",
           buttons: {
                main: {
                    label: "Aceptar",
                    className: "btn-success",
                    callback: function() {
                        postChecklist($id,$checkid,$('#checklist-fecha').val());
                    }
                }
           }
        })

        $("#Dtt-logs-datetime").datetimepicker({
            locale: 'es',
            format: 'L',
        });

    });

});
</script>

@stop