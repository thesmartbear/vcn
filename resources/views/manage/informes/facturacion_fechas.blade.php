@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Informe Facturación', 'manage.informes.facturacion-fechas') !!}
@stop

@section('titulo')
    <i class="fa fa-money fa-fw"></i> Informe Facturación
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-ticket fa-fw"></i> Facturas
                @if($desde)
                   :: Entre {{$desde}} y {{$hasta}}
                @endif
        </div>
        <div class="panel-body">

            {!! Form::open(array('method' => 'GET', 'url' => route('manage.informes.facturacion-fechas'), 'role' => 'form', 'class' => '')) !!}
            <div class="row">
                <div class="col-md-2">Filtro entre fechas:</div>
                <div class="col-md-3">
                    @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                </div>
                <div class="col-md-3">
                    @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                </div>
                <div class="col-md-3">
                    <button type='submit' class="btn btn-info">Filtrar</button>
                </div>
            </div>
            {!! Form::close() !!}

            <hr>

            @if($desde && $hasta)

                {!! Datatable::table()
                    ->addColumn([
                      'importe'     => 'Importe1',
                      'factura_id'  => 'ID_Factura',
                      'factura_num' => 'FACT_NUM',
                      'booking_id'  => 'Booking',
                      'factura_fecha' => 'Fecha_Fact',
                      'cliente'     => 'aux1_codigo',
                      'razonsocial' => 'aux1_nombre',
                      'nif'         => 'aux1_NIF',
                      'contable'    => 'Codigo_Contable',
                      'curso'       => 'Curso',
                      'pais'        => 'PAIS',
                      'oficina'     => 'Oficina',
                      'oficina_id'     => 'CODI_OFICINA',
                      'concepto'     => 'CONCEPTO',
                      'columna'     => 'Columna',
                      'quien'       => 'Destino'
                    ])
                    ->setUrl(route('manage.informes.facturacion-fechas',['desde'=>$desde, 'hasta'=> $hasta]))
                    ->setOptions('iDisplayLength', 0)
                    ->setOptions('order', array([1,'asc']))
                    ->setOptions(
                      "columnDefs", array(
                        //[ "sortable" => false, "targets" => [0] ],
                        [ "targets" => [4], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                      )
                    )
                    ->render() !!}

            @else
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @endif

        </div>
    </div>


@stop