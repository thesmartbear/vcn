@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.pagos') !!}
@stop

@section('titulo')
    <i class="fa fa-money fa-fw"></i> Informe de pagos
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-money fa-fw"></i> Pagos
                @if($desde)
                   :: Entre {{$desde}} y {{$hasta}}
                @endif
        </div>
        <div class="panel-body">

            {!! Form::open(array('method' => 'GET', 'url' => route('manage.informes.pagos'), 'role' => 'form', 'class' => '')) !!}
            <div class="row">
                <div class="col-md-2">Filtro entre fechas:</div>
                <div class="col-md-3">
                    @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=> $desde ?: ""])
                </div>
                <div class="col-md-3">
                    @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=> $hasta ?: ""])
                </div>
                <div class="col-md-3">
                    <button type='submit' class="btn btn-info">Filtrar</button>
                </div>
            </div>
            {!! Form::close() !!}

            <hr>

            @if($desde && $hasta)

                {!! Datatable::table()
                    ->addColumn([
                      'concepto'    => 'Concepto',
                      'pais'        => 'País',
                      'fecha'       => 'Fecha',
                      'importe'     => 'Importe1',
                      'booking_id'  => 'Booking',
                      'fecha_ini'   => 'Fecha Ini curso',
                      'viajero_id'  => 'aux1_codigo',
                      'viajero'     => 'aux1_nombre',
                      'nif'         => 'aux1_nif',
                      'cp'          => 'aux1_codpos',
                      'contable'    => 'Código',
                      'ccc'         => 'aux2_codigo',
                      'oficina'     => 'Oficina',
                      'oficina_id'  => 'CODI_OFICINA',
                      'idt'         => 'ID transacción',
                      'columna'     => 'Columna',
                      'fact_num'    => 'FACT_NUM'
                    ])
                    ->setUrl(route('manage.informes.pagos',['desde'=>$desde, 'hasta'=> $hasta]))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "columnDefs", array(
                        //[ "sortable" => false, "targets" => [0] ],
                        [ "targets" => [2,5], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                      )
                    )
                    ->render() !!}

            @else
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @endif

        </div>
    </div>


@stop