@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes') !!}
@stop

@section('titulo')
    <i class="fa fa-ticket fa-fw"></i> Convocatoria Multi
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Convocatoria Multi {{isset($convocatoria)?(" :: ". $convocatoria->name):'' }} </span>
            </div>
        </div>
        <div class="portlet-body">
            {!! Form::open(['method'=> 'GET','route' => array('manage.informes.convocatorias-multi'), 'class' => 'form']) !!}

            <div class="form-group row">
                <div class="col-md-6">
                {!! Form::label('convocatorias', 'Convocatoria') !!}<br>
                {!! Form::select('convocatorias', $convocatorias, $valores['convocatorias'], array('class'=>'select2', 'data-style'=>'purple', 'id'=>'filtro-convocatorias'))  !!}
                </div>
                <div class="col-md-4">
                {!! Form::label('prescriptores', 'Prescriptor') !!}
                @include('includes.form_input_cargando',['id'=> 'prescriptores-cargando'])
                <br>
                {!! Form::select('prescriptores', $prescriptores, $valores['prescriptores'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-prescriptores'))  !!}
                </div>

                {{-- <div class="col-md-1">
                    {!! Form::label('&nbsp;') !!}
                    {!! Form::submit('Consultar', array( 'class' => 'btn btn-info')) !!}
                </div> --}}
            </div>

            <hr>

            <div class="form-group row">

                <div class="col-md-1">
                    {!! Form::label('any','Año') !!}
                    {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                </div>
                <div class="col-md-1">-o-</div>

                <div class="col-md-2">
                    {!! Form::label('desde','Desde') !!}
                    @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                </div>
                <div class="col-md-2">
                    {!! Form::label('hasta','Hasta') !!}
                    @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                </div>

                <div class="col-md-2 col-md-offset-4">
                    {!! Form::label('(Fechas: 1r Pago)') !!}<br>
                    {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                </div>

            </div>

            {!! Form::close() !!}
        </div>
    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Informe</span>
            </div>
        </div>
        <div class="portlet-body">

            @if(!$convo_id)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione una Convocatoria
                    </div>
                </div>
            @else

                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#semanas" aria-controls="semanas" role="tab" data-toggle="tab">Por Semanas</a></li>
                    <li role="presentation"><a href="#edades" aria-controls="edades" role="tab" data-toggle="tab">Por Edades y Semana</a></li>
                    <li role="presentation"><a href="#bloques" aria-controls="bloques" role="tab" data-toggle="tab">Por Bloques de semanas</a></li>
                    <li role="presentation"><a href="#especialidades" aria-controls="especialidades" role="tab" data-toggle="tab">Especialidades</a></li>
                    <li role="presentation"><a href="#datos" aria-controls="datos" role="tab" data-toggle="tab">Datos Contacto</a></li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="semanas">

                        <?php $datos = $convocatoria->getBookingsSemanas($valores['desdes'],$valores['hastas'], $valores['prescriptores']); ?>

                        <table class="table table-bordered no-footer">
                            <caption>Distribución por Semanas</caption>
                            <thead>
                                <tr>
                                    <th>Especialidad/Semana</th>
                                    @foreach($convocatoria->semanas_list['fsemanas'] as $semana)
                                        <th colspan='2'>{{$semana}}</th>
                                    @endforeach
                                    <th colspan='2'>TODAS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>TOTAL (Total | Niños/Niñas)</th>
                                    @foreach($convocatoria->semanas_list['nsemanas'] as $semana)
                                        <td>
                                            {{isset($datos['totales'][$semana])?$datos['totales'][$semana]:0}}
                                        </td>
                                        <td>
                                            {{isset($datos['sexo'][11][$semana])?$datos['sexo'][11][$semana]:0}}
                                            / {{isset($datos['sexo'][12][$semana])?$datos['sexo'][12][$semana]:0}}
                                        </td>
                                    @endforeach
                                    <th>{{$datos['cuenta']}}</th>
                                    <th>
                                        {{isset($datos['sexo'][31])?$datos['sexo'][31]:0}}
                                        / {{isset($datos['sexo'][32])?$datos['sexo'][32]:0}}
                                    </th>
                                </tr>
                                @foreach($convocatoria->especialidades->sortBy('name') as $esp)
                                <tr>
                                    <td>{{$esp->especialidad_name}}</td>
                                    @foreach($convocatoria->semanas_list['nsemanas'] as $semana)
                                        <td>
                                            {{isset($datos['total'][$semana][$esp->id])?$datos['total'][$semana][$esp->id]:0}}
                                        </td>
                                        <td>
                                            {{isset($datos['sexo'][1][$semana][$esp->id])?$datos['sexo'][1][$semana][$esp->id]:0}}
                                            / {{isset($datos['sexo'][2][$semana][$esp->id])?$datos['sexo'][2][$semana][$esp->id]:0}}
                                        </td>
                                    @endforeach
                                    <td>
                                        {{isset($datos['total'][0][$esp->id])?$datos['total'][0][$esp->id]:0}}
                                    </td>
                                    <td>
                                        {{isset($datos['sexo'][21][$esp->id])?$datos['sexo'][21][$esp->id]:0}}
                                        / {{isset($datos['sexo'][22][$esp->id])?$datos['sexo'][22][$esp->id]:0}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="edades">

                        <?php $datos = $convocatoria->getBookingsEdades($valores['desdes'],$valores['hastas'], $valores['prescriptores']); ?>

                        @foreach($convocatoria->semanas_list['fsemanas'] as $semana)

                            <table class="table table-bordered no-footer">
                                <caption>Semana {{$semana}}</caption>
                                <thead>
                                    <tr>
                                        <th>Especialidad/Edad</th>
                                        @foreach($convocatoria->edades as $edad)
                                            <th colspan='2'>{{$edad}}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>TOTAL (Total | Niños/Niñas)</th>
                                        @foreach($convocatoria->edades as $edad)
                                            <td>
                                                {{isset($datos['totales'][$semana][$edad])?$datos['totales'][$semana][$edad]:0}}
                                            </td>
                                            <td>
                                                {{isset($datos['sexo'][11][$semana][$edad])?$datos['sexo'][11][$semana][$edad]:0}}
                                                / {{isset($datos['sexo'][12][$semana][$edad])?$datos['sexo'][12][$semana][$edad]:0}}
                                            </td>
                                        @endforeach
                                    </tr>
                                    @foreach($convocatoria->especialidades->sortBy('name') as $esp)
                                    <tr>
                                        <td>{{$esp->especialidad_name}}</td>
                                        @foreach($convocatoria->edades as $edad)
                                            <td>
                                                {{isset($datos['total'][$semana][$esp->id][$edad])?$datos['total'][$semana][$esp->id][$edad]:0}}
                                            </td>
                                            <td>
                                                {{isset($datos['sexo'][1][$semana][$esp->id][$edad])?$datos['sexo'][1][$semana][$esp->id][$edad]:0}}
                                                / {{isset($datos['sexo'][2][$semana][$esp->id][$edad])?$datos['sexo'][2][$semana][$esp->id][$edad]:0}}
                                            </td>
                                        @endforeach
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        @endforeach

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="bloques">

                        <?php $datos = $convocatoria->getBookingsBloques($valores['desdes'],$valores['hastas'], $valores['prescriptores']); ?>

                        @foreach($convocatoria->semanas_bloques as $semana)

                            <table class="table table-bordered no-footer">
                                <caption>Participantes de {{$semana}} Semana/s </caption>
                                <thead>
                                    <tr>
                                        <th>SEMANA</th>
                                        <th colspan='2'>TOTAL (Total | Niños/Niñas)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($convocatoria->getSemanasBloquesDetalle($semana) as $bloque)
                                        <tr>
                                            <td>{{$bloque}}</td>
                                            <td>
                                                {{isset($datos['total'][$semana][$bloque])?$datos['total'][$semana][$bloque]:0}}
                                            </td>
                                            <td>
                                                {{isset($datos['sexo'][1][$semana][$bloque])?$datos['sexo'][1][$semana][$bloque]:0}}
                                                / {{isset($datos['sexo'][2][$semana][$bloque])?$datos['sexo'][2][$semana][$bloque]:0}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <hr>

                        @endforeach

                    </div>


                    <div role="tabpanel" class="tab-pane fade in" id="especialidades">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-star fa-fw"></i> Especialidades
                            </div>
                            <div class="panel-body">

                                <?php

                                    $columns = [
                                        'nombre'        => 'Nombre',
                                        'apellidos'     => 'Apellidos',
                                        'fechanac'      => 'F.Nac.',
                                        'fecha_reserva' => 'F.Booking',
                                        'fechaini'      => 'F.Inicio',
                                        'semanas'       => 'NºSem.'
                                    ];

                                    for ($i=1; $i <= $semanas ; $i++)
                                    {
                                        $columns['semana'.$i] = "S.$i";
                                    }

                                    $columns += [
                                        'extras'        => 'Extras',
                                        'notas'         => 'Notas',
                                        'cuestionario_resultado'    => 'Test resultado',
                                        'cuestionario_nivel'        => 'Test nivel',
                                        'origen_txt' => 'Orígen',
                                    ];

                                    $valores['datos'] = false;
                                ?>

                                {!! Datatable::table()
                                    ->addColumn($columns)
                                    ->setUrl(route('manage.informes.convocatorias-multi',$valores))
                                    ->setOptions('iDisplayLength', 100)
                                    ->setOptions(
                                            "columnDefs", array(
                                                //[ "bSortable" => false, "aTargets" => [3] ],
                                                [ "targets" => [2,3,4], "render"=> "function(data, type, full) {return moment(data).isValid()?moment(data).format('DD/MM/YYYY'):'-';}" ],
                                            )
                                        )
                                    ->render()
                                !!}

                            </div>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="datos">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-star fa-fw"></i> Datos Contacto
                            </div>
                            <div class="panel-body">

                                <?php

                                    $columns = [
                                        'nombre'        => 'Nombre',
                                        'apellidos'     => 'Apellidos',
                                        'origen_txt' => 'Orígen',
                                        'semanas'      => 'Semanas',
                                        'tutor1' => 'Nombre Tutor1',
                                        'tutor1_movil' => 'Móvil Tutor1',
                                        'tutor2' => 'Nombre Tutor2',
                                        'tutor2_movil' => 'Móvil Tutor2',
                                    ];

                                    $valores['datos'] = true;
                                ?>

                                {!! Datatable::table()
                                    ->addColumn($columns)
                                    ->setUrl(route('manage.informes.convocatorias-multi',$valores))
                                    ->setOptions('iDisplayLength', 100)
                                    ->render()
                                !!}

                            </div>
                        </div>

                    </div>

                </div>

            @endif

        </div>
    </div>


@stop