@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Datos contacto inscritos', 'manage.informes.datos.inscritos') !!}
@stop

@section('titulo')
    <i class="fa fa-users fa-fw"></i> Datos contacto inscritos
@stop

@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

                {!! Form::open(['route' => array('manage.informes.datos.inscritos'), 'method'=> 'GET', 'class' => 'form']) !!}

                {!! Form::hidden('listado', true) !!}

                <div id="filtros_div">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-filter fa-fw"></i> Filtros
                        </div>
                        <div class="panel-body">
                            @include('includes.filtros_multi', ['filtro_fechas'=>false])
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-filter fa-fw"></i> Filtros excepto
                        </div>
                        <div class="panel-body">
                            @include('includes.filtros_not_multi')
                        </div>
                    </div>

                </div>


                <div class="form-group pull-right">
                    {!! Form::submit('Generar', array( 'class'=> 'btn btn-info' ) ) !!}
                </div>

                {!! Form::close() !!}

                <div class="clearfix"></div>

            </div>
        </div>


        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>
                    <span class="caption-subject bold">Listado</span>

                </div>
            </div>
            <div class="portlet-body">
                @if(!$listado)
                    <div class="content">
                        <div class="alert alert-info" role="alert">
                            Seleccione los filtros correspondientes
                        </div>
                    </div>
                @else

                    <div class="content">

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#viajeros" aria-controls="viajeros" role="tab" data-toggle="tab">Viajeros</a></li>
                            <li role="presentation"><a href="#tutores" aria-controls="tutores" role="tab" data-toggle="tab">Tutores</a></li>
                        </ul>

                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane fade in active" id="viajeros">

                                <?php
                                    $filtros['list'] = "viajeros";
                                ?>

                                {!! Datatable::table()
                                    ->addColumn([
                                    'email'   => 'Email',
                                    'name'  => 'Nombre',
                                    'lastname'   => 'Apellido1',
                                    'lastname2'   => 'Apellido2',
                                    'edad'    => 'Edad',
                                    'idioma'  => 'Idioma',
                                    'movil'   => 'Móvil',
                                    'optout'  => 'Optout',
                                    'convocatoria'  => 'Convocatoria',
                                    'asignado'  => 'Asignado',
                                    'oficina'   => 'Oficina',
                                    ])
                                    ->setUrl(route('manage.informes.datos.inscritos', $filtros))
                                    ->setOptions('iDisplayLength', 100)
                                    ->setOptions(
                                    "aoColumnDefs", array(
                                            //[ "bSortable" => false, "aTargets" => [0] ],
                                        )
                                    )
                                    ->render() !!}

                            </div>

                            <div role="tabpanel" class="tab-pane fade in" id="tutores">
                                
                                <?php
                                    $filtros['list'] = "tutores";
                                ?>

                                {!! Datatable::table()
                                    ->addColumn([
                                    'email'   => 'Email',
                                    'name'  => 'Nombre',
                                    'lastname'   => 'Apellidos',
                                    'edad'    => 'Edad',
                                    'idioma'  => 'Idioma',
                                    'movil'   => 'Móvil',
                                    'viajero_nom' => 'Viajero Nombre',
                                    'viajero_ape' => 'Viajero Apellidos',
                                    'optout'  => 'Optout',
                                    'convocatoria'  => 'Convocatoria',
                                    'asignado'  => 'Asignado',
                                    'oficina'   => 'Oficina',
                                    ])
                                    ->setUrl(route('manage.informes.datos.inscritos', $filtros))
                                    ->setOptions('iDisplayLength', 100)
                                    ->setOptions(
                                    "aoColumnDefs", array(
                                            //[ "bSortable" => false, "aTargets" => [0] ],
                                        )
                                    )
                                    ->render() !!}

                            </div>

                    </div>

                @endif

            </div>
        </div>

@stop