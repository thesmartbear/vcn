@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.booking-grupos') !!}
@stop

@section('titulo')
    <i class="fa fa-ticket fa-fw"></i> Booking Grupos
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.booking-grupos'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'proveedores'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-2">
                    {!! Form::label('oficinas', 'Oficina') !!}
                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-oficinas'))  !!}
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-3">
                    {!! Form::label('tipoc', 'Tipo Convocatoria') !!}
                    @include('includes.form_input_cargando',['id'=> 'tipoc-cargando'])
                    <br>
                    {!! Form::select('tipoc', ConfigHelper::getConvocatoriaTipo(), $valores['tipoc'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-tipoc'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-3"></div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                    {!! Form::label('proveedores', 'Proveedor') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'proveedores-cargando'])

                    {!! Form::select('proveedores', $proveedores, $valores['proveedores'], array('class'=>'select2', 'data-style'=>'purple', 'id'=>'filtro-proveedores'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('centros', 'Centro') !!}
                    @include('includes.form_input_cargando',['id'=> 'centros-cargando'])
                    <br>
                    {!! Form::select('centros', $centros, $valores['centros'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-centros'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'cursos'])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-5">
                    {!! Form::label('cursos', 'Curso') !!}
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])

                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>


            {!! Form::close() !!}
        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
                    @if(count($tabs)>0)
                        &nbsp;<a target='_blank' href="{{route('manage.informes.booking-grupos.pdf',$valores)}}" class='btn btn-danger'><i class='fa fa-file-pdf-o'></i> PDF</a>
                    @endif
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(count($tabs)<=0)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                <ul class="nav nav-tabs" role="tablist">
                @foreach($tabs as $ktab=>$vtab)
                    <li role="presentation" class="{{($vtab === reset($tabs))?'active':''}}">
                        <a href="#tab-{{$ktab}}" aria-controls="tab-{{$ktab}}" role="tab" data-toggle="tab">
                        {{\VCN\Models\Proveedores\Proveedor::find($ktab)->name}}
                        </a>
                    </li>
                @endforeach
                </ul>

                <div class="tab-content">
                    @foreach($tabs as $ktab=>$vtab)
                    <div role="tabpanel" class="tab-pane fade in {{($vtab === reset($tabs))?'active':''}}" id="tab-{{$ktab}}">

                        <ul class="nav nav-tabs" role="tablist">
                            @foreach($tabs[$ktab] as $c)
                                <li role="presentation" class="{{($c === reset($tabs[$ktab]))?'active':''}}">
                                    <a href="#tab-c{{$c}}" aria-controls="tab-c{{$c}}" role="tab" data-toggle="tab">
                                    {{\VCN\Models\Cursos\Curso::find($c)->name}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>

                        <div class="tab-content">
                        @foreach($tabs[$ktab] as $c)
                            <div role="tabpanel" class="tab-pane fade in {{($c === reset($tabs[$ktab]))?'active':''}}" id="tab-c{{$c}}">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <i class="fa fa-globe fa-fw"></i> Listado
                                    </div>
                                    <div class="panel-body">

                                        <?php
                                            $valores['proveedores'] = $ktab;
                                            $valores['cursos'] = $c;
                                        ?>

                                        {!! Datatable::table()
                                            ->addColumn([
                                                'apellidos' => 'Apellidos',
                                                'nombre' => 'Nombre',
                                              'sexo'    => 'Sexo',
                                              'fechanac'=> 'Fecha Nac.',
                                              'convocatoria'=> 'Convocatoria',
                                              'convo_ini'   => 'F.Ini',
                                              'convo_fin'   => 'F.Fin',
                                              'fecha'       => 'F.Booking',
                                              'alojamiento'=> 'Alojamiento',
                                              'status'      => 'Status',
                                              'asignado'      => 'Asignado',
                                              'comentarios'=> 'Comentarios',
                                              'extras'  => 'Extras'
                                            ])
                                            ->setUrl(route('manage.informes.booking-grupos',$valores))
                                            ->setOptions('iDisplayLength', 100)
                                            ->setOptions(
                                              "columnDefs", array(
                                                // [ "sortable" => false, "targets" => [0,1,2,3,7,8,9,10,11,12] ],
                                                [ "targets" => [3,5,6,7], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                              )
                                            )
                                            ->render()
                                        !!}
                                    </div>
                                </div>

                            </div>
                        @endforeach
                        </div>

                    </div>
                    @endforeach
                </div>

                @endif

           @endif


        </div>

    </div>

@stop