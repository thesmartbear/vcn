@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.escuelas') !!}
@stop

@section('titulo')
    <i class="fa fa-graduation-cap fa-fw"></i> Escuela y Academia
@stop


@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-list fa-fw"></i> Listado
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'escuela' => 'Escuela',
                  'academia' => 'Academia',
                  'ciudad' => 'Ciudad',
                  'oficina' => 'Oficina',
                  'categoria' => 'Categoría curso',
                  'prescriptor' => 'Prescriptor',
                ])
                ->setUrl(route('manage.informes.escuelas'))
                ->setOptions('iDisplayLength', 100)
                ->render()
            !!}

        </div>
    </div>

@stop