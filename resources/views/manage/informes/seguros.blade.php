@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.seguros') !!}
@stop

@section('titulo')
    <i class="fa fa-ban fa-fw"></i> Informe Seguros de Cancelación
@stop

@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(array('method' => 'GET', 'url' => route('manage.informes.seguros'), 'role' => 'form', 'class' => '')) !!}
            
                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    </div>

                    <div class="col-md-2">
                    {!! Form::label('oficinas', 'Oficina') !!}
                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-oficinas'))  !!}
                    </div>

                </div>
                
                <div class="form-group row">
                    <div class="col-md-3">
                        {!! Form::label('desde', 'Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>$desde])
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('hasta', 'Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>$hasta])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('any','Año inicio Booking') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        <button type='submit' class="btn btn-info">Filtrar</button>
                    </div>
                </div>
            
            {!! Form::close() !!}

        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-ban fa-fw"></i> Informe
                @if($results)
                    @if($desde && $hasta)
                    :: Oficina {{$oficina_name}} entre {{$desde}} y {{$hasta}} [Bookings: {{$valores['any']}}]
                    @else
                        :: Oficina {{$oficina_name}} [Bookings: {{$valores['any']}}]
                    @endif
                @endif
        </div>
        <div class="panel-body">

            @if($results)

                {!! Datatable::table()
                    ->addColumn([
                      'viajero'     => 'Viajero',
                      'booking'     => 'Booking',
                      'fecha_ini'   => 'Fecha Ini',
                      'fecha_fin'   => 'Fecha Fin',
                      'reserva'     => 'Reserva',
                      'tramo'       => 'Tramo',
                      'contable'    => 'Cod.Contable',
                      //'opciones'    => '',
                    ])
                    ->setUrl(route('manage.informes.seguros', $valores))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "columnDefs", array(
                        //[ "sortable" => false, "targets" => [7] ],
                        [ "targets" => [2,3,4], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                      )
                    )
                    ->render() !!}

            @else
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @endif

        </div>
    </div>


@stop