<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>

    {!! Html::style('assets/css/pdf.css') !!}
    {!! Html::style('assets/css/bootstrap.css') !!}

</head>
<body>

    <h1><i class="fa fa-group fa-fw"></i> Plazas x Semanas</h1>
    <i>{{Carbon::now()->format("d/m/Y H:i")}}</i>

    <hr>
    <h4>Filtros:</h4>
    Proveedor: {{$valores_txt['proveedores']}}, Categoria: {{$valores_txt['categorias']}}
    <br>
    Centro: {{$valores_txt['centros']}}, Curso: {{$valores_txt['cursos']}}

    <hr>

    <table class="table table-bordered table-striped table-condensed flip-content tablesorter">
        <caption>
            <strong><i class="fa fa-list fa-fw"></i> Listado {{$valores['desdes']}} - {{$valores['hastas']}}</strong>
        </caption>
        {{-- <thead class="flip-content">
            <tr>
                <th>Curso</th>
                <th colspan='{{count($semanas)}}'>Semanas</th>
            </tr>
        </thead> --}}
        <tbody>

            @foreach($plazas as $curso_id => $plazas_semanas)
                <tr>
                    <th class='td-fijo'>Curso: {{\VCN\Models\Cursos\Curso::find($curso_id)->name}}</th>
                    @foreach($semanas as $semana=>$total_s)
                        <th colspan='2'>Semana {{$semana}}</th>
                    @endforeach
                </tr>
                <tr>
                    <td class='td-fijo'>Edad</td>
                    @foreach($semanas as $semana=>$total_s)
                        <td>Chicas</td>
                        <td>Chicos</td>
                    @endforeach
                </tr>

                @foreach($edades as $edad=>$total_e)
                    <tr>
                        <td class='td-fijo'>{{$edad}}</td>
                        @foreach($semanas as $semana=>$total_s)
                        <td>{{isset($plazas_semanas[$semana][$edad][2])?$plazas_semanas[$semana][$edad][2]:0}}</td>
                        <td>{{isset($plazas_semanas[$semana][$edad][1])?$plazas_semanas[$semana][$edad][1]:0}}</td>
                        @endforeach
                    </tr>
                @endforeach

                <tr>
                    <td class='td-fijo'>Total</td>
                    @foreach($semanas as $semana=>$total_s)
                        <td colspan='2'>{{isset($subtotales[$curso_id][$semana])?$subtotales[$curso_id][$semana]:0}}</td>
                    @endforeach
                </tr>

            @endforeach

            <tr>
                <th class='td-fijo'>TOTAL</th>
                @foreach($semanas as $semana=>$total_s)
                    <th colspan='2'>{{$total_s}}</th>
                @endforeach
            </tr>
        </tbody>

    </table>

</body>
</html>