@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Datos de Contacto de Viajeros', 'manage.informes.viajeros.datos') !!}
@stop

@section('titulo')
    <i class="fa fa-ticket fa-fw"></i> Datos de Contacto de Viajeros
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.viajeros.datos'), 'method'=> 'GET', 'class' => 'form']) !!}

                {!! Form::hidden('tipoc',$valores['tipoc'], ['id'=> 'filtro-tipoc']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-6">
                    {!! Form::label('centros', 'Centro') !!}
                    @include('includes.form_input_cargando',['id'=> 'centros-cargando'])
                    <br>
                    {!! Form::select('centros', $centros, $valores['centros'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-centros'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'cursos'])
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'convocatorias'])
                    </div>

                    <div class="col-md-6">
                    {!! Form::label('cursos', 'Curso') !!}
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])
                    <br>
                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-cursos'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'cursos', 'destino'=> 'convocatorias'])
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-6">
                    {!! Form::label('convocatorias', 'Convocatoria') !!}
                    @include('includes.form_input_cargando',['id'=> 'convocatorias-cargando'])
                    <br>
                    {!! Form::select('convocatorias', $convocatorias, $valores['convocatorias'], array('class'=>'select2 col-md-6', 'data-style'=>'red', 'id'=>'filtro-convocatorias'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        {!! Form::label('filtro1',"(=Fecha Inicio Booking)") !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}
        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-globe fa-fw"></i> Listado
                        </div>
                        <div class="panel-body">

                            {!! Datatable::table()
                                ->addColumn([
                                    'apellido1'     => 'Apellido1',
                                    'apellido2'     => 'Apellido2',
                                    'nombre'        => 'Nombre',
                                    'fechanac'      => 'Fecha Nac.',
                                    'centro'        => 'Centro',
                                    'curso'         => 'Curso',
                                    'convocatoria'  => 'Convocatoria',
                                    'oficina'       => 'Oficina',
                                    'prescriptor'   => 'Prescriptor',
                                    'email'         => 'Email',
                                    'optin_v'         => 'Optin',
                                    'telefono'      => 'Teléfono',
                                    'movil'         => 'Móvil',
                                    'instagram'     => 'IG',
                                    'tutor1_movil'  => 'Tutor1 móvil',
                                    'tutor2_movil'  => 'Tutor2 móvil',
                                    'tutor1_nombre' => 'Tutor1 nombre',
                                    'tutor1_email'  => 'Tutor1 email',
                                    'optin_t1'         => 'Optin',
                                    'tutor2_nombre' => 'Tutor2 nombre',
                                    'tutor2_email'  => 'Tutor2 email',
                                    'optin_t2'         => 'Optin',
                                    'direccion'     => 'Dirección',
                                    'poblacion'     => 'Población',
                                    'cp'            => 'CP',
                                    'provincia'     => 'Provincia',
                                    'dni'           => 'DNI',
                                    'pasaporte'     => 'Pasaporte',
                                    'escuela'       => 'Escuela',
                                    'academia'      => 'Academia',
                                    'fecha_ini'     => 'Desde',
                                    'fecha_fin'     => 'Hasta',
                                ])
                                ->setUrl(route('manage.informes.viajeros.datos',$valores))
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                  "columnDefs", array(
                                    // [ "sortable" => false, "targets" => [0,1,2,3,7,8,9,10,11,12] ],
                                    [ "targets" => [3,29,30], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                  )
                                )
                                ->render()
                            !!}
                        </div>
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop