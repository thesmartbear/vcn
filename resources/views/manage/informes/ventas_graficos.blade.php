@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Gráficos Ventas', 'manage.informes.ventas-graficos') !!}
@stop

@section('titulo')
    <i class="fa fa-bar-chart fa-fw"></i> Gráficos de Ventas
@stop


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">Filtros <strong class="pull-right">(Estamos en la semana {{Carbon::now()->format('W')}})</strong> </div>
    <div class="panel-body">

        {!! Form::open(['route' => array('manage.informes.ventas-graficos'), 'method'=> 'GET', 'class' => 'form']) !!}

        <div class="form-group row">
            <div class="col-md-1">
            {!! Form::label('any', 'Año') !!}
            <br>
            {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any'))  !!}
            </div>

            {{-- <div class="col-md-1">
                {!! Form::label('any','Año') !!}
                {!! Form::select('any', ConfigHelper::getAnys(), $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-any'))  !!}
            </div> --}}

            <div class="col-md-1">
            {!! Form::label('semana', 'Semana') !!}
            <br>
            {!! Form::select('semana', $semanas, ($valores['semana']?:Carbon::now()->format('W')), array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-semana'))  !!}
            </div>

            <div class="col-md-2">
            {!! Form::label('plataformas', 'Plataforma') !!}
            <br>
            {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
            @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
            @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categoriasp'])
            </div>

            @if( auth()->user()->filtro_oficinas )
            <div class="col-md-3">
            {!! Form::label('oficinas', 'Oficina') !!}
            <br>
            @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

            {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'multiple'=>'multiple', 'id'=>'filtro-oficinas', 'name'=> 'oficinas[]'))  !!}

            {{-- {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-oficinas'))  !!} --}}
            </div>
            @endif

        </div>

        <div class="form-group row">
            <div class="col-md-4">
            {!! Form::label('categoriasp', 'Categoría Prescriptor') !!}
            @include('includes.form_input_cargando',['id'=> 'categoriasp-cargando'])
            <br>
            {!! Form::select('categoriasp', $categoriasp, $valores['categoriasp'], array('class'=> 'select2', 'data-style'=>'blue', 'id'=>'filtro-categoriasp'))  !!}
            </div>
        </div>

        <div class="form-group row">

            <div class="col-md-2">
            {!! Form::label('categorias', 'Categoría') !!}
            @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
            <br>
            {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
            {{-- @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos']) --}}
            @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
            {{-- @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategoriasdet']) --}}
            </div>

            <div class="col-md-4">
            {!! Form::label('subcategorias', 'SubCategoría') !!}
            @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
            <br>
            {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
            {{-- @include('includes.script_filtros', ['filtro'=> 'subcategorias', 'destino'=> 'cursos']) --}}
            {{-- @include('includes.script_filtros', ['filtro'=> 'subcategorias', 'destino'=> 'subcategoriasdet']) --}}
            </div>

            <div class="col-md-1 col-md-offset-3">
                {!! Form::label('&nbsp;') !!}
                {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
            </div>

        </div>

        {!! Form::close() !!}

    </div>
</div>

<hr>

@if(!$resultados)
    <div class="content">
        <div class="alert alert-info" role="alert">
            No hay datos que mostrar.
        </div>
    </div>
@else

<div class="panel panel-default">
    <div class="panel-heading">Bookings x Categoría (acumulado hasta semana seleccionada) -por fecha de inicio booking</div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-12">
                <div id="chart_6"></div>
                @columnchart('Chart-Bookings-Categoria', 'chart_6')
                {{-- {!! Lava::render('ColumnChart', 'Chart-Bookings-Categoria', 'chart_6') !!} --}}
            </div>
        </div>

    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Bookings x Mes</div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-12">
                <div id="chart_9"></div>
                @columnchart('Chart-Bookings-Mes', 'chart_9')
            </div>
        </div>

    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Solicitudes x Categoría (acumulado hasta semana seleccionada)</div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-12">
                <div id="chart_8"></div>
                @columnchart('Chart-Solicitudes-Categoria', 'chart_8')
            </div>
        </div>

    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Semanas x Categoría (acumulado hasta semana seleccionada) -por fecha de inicio booking</div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-12">
                <div id="chart_7"></div>
                @columnchart('Chart-Semanas-Categoria', 'chart_7')
            </div>
        </div>

    </div>
</div>

<hr>

<div class="row">
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">Evolución semanal (Bookings)</div>
        <div class="panel-body">
            <div id="chart_4"></div>
            @linechart('Chart-Bookings-Evolucion', 'chart_4')
        </div>
    </div>
</div>
</div>

<div class="row">
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">Evolución semanal (Solicitudes)</div>
        <div class="panel-body">
            <div id="chart_5"></div>
            @linechart('Chart-Solicitudes-Evolucion', 'chart_5')
        </div>
    </div>
</div>
</div>

<hr>

<div class="panel panel-default">
    <div class="panel-heading">Bookings y Semanas (para semanas seleccionadas)</div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-6">
                <div id="chart_1"></div>
                {!! Lava::render('ColumnChart', 'Chart-Bookings-1', 'chart_1') !!}
                {{-- @columnchart('Chart-Bookings-1', 'chart_1') --}}
            </div>
            <div class="col-md-6">
                <div id="chart_2"></div>
                @columnchart('Chart-Bookings-2', 'chart_2')
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Bookings x Oficina (acumulado hasta semana seleccionada) - por fecha de inicio booking</div>
    <div class="panel-body">
        <div id="chart_3"></div>
        @barchart('Chart-Bookings-Oficinas', 'chart_3')
    </div>
</div>

@if(auth()->user()->isFullAdmin())
<hr>
<div class="panel panel-default">
    <div class="panel-heading">Bookings y Semanas x Oficinas (acumulado hasta semana seleccionada) -por fecha de inicio booking</div>
    <div class="panel-body">

        <div class="row">
        @foreach($ofis as $o=>$ofi_id)
            <div class="col-md-6">
                <div id="chart_admin1_{{$ofi_id}}"></div>
                @columnchart("Chart-Bookings-Categoria-Oficina-$ofi_id", "chart_admin1_$ofi_id")
            </div>
            <div class="col-md-6">
                <div id="chart_admin2_{{$ofi_id}}"></div>
                @columnchart("Chart-Semanas-Categoria-Oficina-$ofi_id", "chart_admin2_$ofi_id")
            </div>
        @endforeach
        </div>

    </div>
</div>

@endif


@endif


@stop