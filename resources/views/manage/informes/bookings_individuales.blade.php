@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Bookings Individuales', 'manage.informes.bookings-individuales') !!}
@stop

@section('titulo')
    <i class="fa fa-ticket fa-fw"></i> Bookings Individuales
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.bookings-individuales'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'proveedores'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-2">
                    {!! Form::label('oficinas', 'Oficina') !!}
                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-oficinas'))  !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('tipoc', 'Tipo Convocatoria') !!}
                        @include('includes.form_input_cargando',['id'=> 'tipoc-cargando'])
                        <br>
                        {!! Form::select('tipoc', ConfigHelper::getConvocatoriaTipo(), $valores['tipoc'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-tipoc'))  !!}
                        @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'centros'])
                        @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-3">
                        <?php
                            $select = [0 => 'Todos'] + \VCN\Models\System\Contacto::activos()->where('es_proveedor',0)->get()->pluck('name','id')->toArray();
                        ?>
                        {!! Form::label('sos', 'SOS Plataforma') !!}
                        {!! Form::select('sos', $select, $valores['sos'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-sos'))  !!}
                    </div>

                    <div class="col-md-2">
                        @include('includes.form_select', ['campo'=> 'directos', 'texto'=> 'Booking directos', 'valor'=> $valores['directos'], 'select'=> [1=> 'Incluir', 0=> 'Excluir', 2=> 'Sólo directos'] ])
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategoriasdet'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('subcategorias', 'SubCategoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                    <br>
                    {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'subcategorias', 'destino'=> 'cursos'])
                    @include('includes.script_filtros', ['filtro'=> 'subcategorias', 'destino'=> 'subcategoriasdet'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('subcategoriasdet', 'SubCategoría Detalle') !!}
                    @include('includes.form_input_cargando',['id'=> 'subcategoriasdet-cargando'])
                    <br>
                    {!! Form::select('subcategoriasdet', $subcategoriasdet, $valores['subcategoriasdet'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategoriasdet'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'subcategoriasdet', 'destino'=> 'cursos'])
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                    {!! Form::label('proveedores', 'Proveedor') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'proveedores-cargando'])

                    {!! Form::select('proveedores', $proveedores, $valores['proveedores'], array('class'=>'select2', 'data-style'=>'purple', 'id'=>'filtro-proveedores'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('centros', 'Centro') !!}
                    @include('includes.form_input_cargando',['id'=> 'centros-cargando'])
                    <br>
                    {!! Form::select('centros', $centros, $valores['centros'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-centros'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'cursos'])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                    {!! Form::label('cursos', 'Curso') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])

                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
                    </div>

                    <div class="col-md-6">
                    {!! Form::label('asignados', 'Usuario') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'asignados-cargando'])

                    {!! Form::select('asignados', $asignados, $valores['asignados'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-asignados'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-2">
                        {!! Form::label('(Fechas: Primer pago)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro2', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}
        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><i class="fa fa-list fa-fw"></i> Listado [{{$tipof}}]</strong>
                        </div>
                        <div class="panel-body">
                            
                            {!! Datatable::table()
                                ->addColumn([
                                    'fecha_creacion'   => 'Fecha Booking',
                                    'viajero'   => 'Viajero',
                                    'proveedor' => 'Proveedor',
                                    'sosp'       => 'SOS Proveedor',
                                    'sosp_notas' => 'Notas SOS',
                                    'centro'    => 'Centro',
                                    'curso'     => 'Curso',
                                    'fecha_ini'   => 'Inicio',
                                    'fecha_fin'   => 'Fin',
                                    'pais'      => 'País',
                                    'sos_plataforma'    => 'SOS Plat',
                                    'movil'       => 'Móvil',
                                    'tutor1_movil'=> 'Tutor1 móvil',
                                    'tutor2_movil'=> 'Tutor2 móvil',
                                    'prescriptor' => 'Prescriptor',
                                    'usuario'     => 'Usuario reserva',
                                    'oficina'     => 'Oficina'                                  
                                ])
                                ->setUrl(route('manage.informes.bookings-individuales',$valores))
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                  "columnDefs", array(
                                    // [ "sortable" => false, "targets" => [0,1,2,3,7,8,9,10,11,12] ],
                                    [ "targets" => [0,7,8], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                  )
                                )
                                ->render()
                            !!}
                        </div>
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop