@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Origen Ventas', 'manage.informes.ventas-origen') !!}
@stop

@section('titulo')
    <i class="fa fa-tag fa-fw"></i> Origen Ventas
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.ventas-origen'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    </div>

                    <div class="col-md-5">
                    {!! Form::label('oficinas', 'Oficina') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                    {{-- {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-oficinas'))  !!} --}}
                    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-oficinas', 'name'=> 'oficinas[]'))  !!}
                    </div>

                    <div class="col-md-5">
                    {!! Form::label('origenes', 'Orígen') !!}
                    @include('includes.form_input_cargando',['id'=> 'origenes-cargando'])
                    <br>
                    {{-- {!! Form::select('origenes', $origenes, $valores['origenes'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-origenes'))  !!} --}}
                    {!! Form::select('origenes', $origenes, $valores['origenes'], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-origenes', 'name'=> 'origenes[]'))  !!}
                    </div>

                    <div class="col-md-3"></div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        {!! Form::label('categorias', 'Categoría') !!}
                        @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                        <br>
                        {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-categorias', 'name'=> 'categorias[]'))  !!}
                        @include('includes.script_filtros_multi', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('subcategorias', 'SubCategoría') !!}
                        @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                        <br>
                        {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-subcategorias', 'name'=> 'subcategorias[]'))  !!}
                        @include('includes.script_filtros_multi', ['filtro'=> 'subcategorias', 'destino'=> 'cursos'])
                        @include('includes.script_filtros_multi', ['filtro'=> 'subcategorias', 'destino'=> 'subcategoriasdet'])
                    </div>
                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-1">
                        {!! Form::label('(Fechas: 1r Pago)') !!}
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro2', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}
        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings [{{$tipof}}]</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-globe fa-fw"></i> Listado
                        </div>
                        <div class="panel-body">

                            {!! Datatable::table()
                                ->addColumn([
                                    'origen'        => 'Origen',
                                    'num'           => 'Nº Inscrip.',
                                    'total_sem'     => 'Semanas',
                                    'total_curso'   => "Imp.Curso <i class='fa fa-info-circle' data-label='Importe del curso, sin extras ni descuentos, ni alojamiento'></i>",
                                    'total'         => "Imp.Total <i class='fa fa-info-circle' data-label='Importe total a pagar por el cliente'></i>",
                                    'detalles'      => 'Detalles',
                                ])
                                ->setUrl(route('manage.informes.ventas-origen',$valores))
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                  "columnDefs", array(
                                    [ "sortable" => false, "targets" => [5] ],
                                    //[ "targets" => [5,6], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                  )
                                )
                                ->render()
                            !!}

                            <hr>

                             <table class="table table-bordered table-hover">
                                <caption>Totales</caption>
                                <thead>
                                    <tr>
                                        <th>Total Inscrip.</th>
                                        <th>Total Semanas</th>
                                        <th>Total Curso</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{$totales['total_num']}}</td>
                                        <td>{{$totales['total_sem']}}</td>
                                        <td>{{ConfigHelper::parseMoneda($totales['total_curso'])}}</td>
                                        <td>{{ConfigHelper::parseMoneda($totales['total'])}}</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop