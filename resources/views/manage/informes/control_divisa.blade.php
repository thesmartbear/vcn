@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Control Divisa', 'manage.informes.control-divisa') !!}
@stop

@section('titulo')
    <i class="fa fa-money fa-fw"></i> Control Divisa
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-money fa-fw"></i> Listado
        </div>
        <div class="panel-body">

            {!! Form::open(array('method' => 'GET', 'url' => route('manage.informes.control-divisa'), 'role' => 'form', 'class' => '')) !!}

            <div class="form-group row">
                <div class="col-md-3">
                {!! Form::label('any', 'Año (=Fecha incio Booking)') !!}
                <br>
                {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any'))  !!}
                </div>
                <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'proveedores'])
                </div>
                <div class="col-md-2">
                    {!! Form::label('monedas', 'Monedas') !!}
                    <br>
                    {!! Form::select('monedas', $monedas, $valores['monedas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-monedas'))  !!}
                </div>

                <div class="col-md-1">
                    @include('includes.form_checkbox', [ 'campo'=> 'cancelados', 'texto'=> 'Incluir cancelados', 'valor'=> $valores['cancelados'] ])
                </div>
            </div>

            <div class="form-group row">

                <div class="col-md-2">
                {!! Form::label('categorias', 'Categoría') !!}
                @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                <br>
                {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                </div>

                <div class="col-md-4">
                {!! Form::label('proveedores', 'Proveedor') !!}
                @include('includes.form_input_cargando',['id'=> 'proveedores-cargando'])
                <br>
                {!! Form::select('proveedores', $proveedores, $valores['proveedores'], array('class'=>'select2', 'data-style'=>'purple', 'id'=>'filtro-proveedores'))  !!}
                </div>

                <div class="col-md-1">
                    {!! Form::label('&nbsp;') !!}
                    {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                </div>

            </div>


            {!! Form::close() !!}

            <hr>

            @if($listado)

                {!! Datatable::table()
                    ->setId("Dtt-divisa")
                    ->addColumn([
                      'booking'     => 'ID Booking',
                      'contable'    => 'Contable',
                      'fecha'       => 'F.Booking (=Pago1)',
                      'viajero'     => 'Viajero',
                      'oficina'     => 'Oficina',
                      'convocatoria'=> 'Convocatoria',
                      'duracion'    => 'Duración',
                      'pais'        => 'País (Centro)',
                      'proveedor'   => 'Proveedor',
                      'coste'       => 'Coste',
                      'divisa'      => 'Divisa',
                      'total'       => 'Importe Total',
                      'pagado'      => 'Pagado',
                      'pendiente'   => 'Pendiente',
                      'status'      => 'Status',
                    ])
                    ->setUrl(route('manage.informes.control-divisa',$valores))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "columnDefs", array(
                        //[ "sortable" => false, "targets" => [0] ],
                        [ "targets" => [2], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                      )
                    )
                    ->setCallbacks('footerCallback', "function ( tfoot, data, start, end, display ) {

                      var api = this.api(), data;

                      var intVal = function ( i ) {
                          return typeof i === 'string' ?
                              i.replace(/[\$,]/g, '')*1 :
                              typeof i === 'number' ?
                                  i : 0;
                      };

                      var Columns = [9,11,12,13];

                      Columns.forEach( function(val, index, array)
                      {
                        // Total over all pages
                        total = api
                            .column( val )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        // Total over this page
                        pageTotal = api
                            .column( val, { page: 'current'} )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        // Update footer
                        console.log( $( api.column(val).footer() ) );

                        $( api.column( val ).footer() ).html(
                            pageTotal.toFixed(2) +' ('+ total.toFixed(2) +')'
                        );
                      });

                    }")
                    ->render('datatables.footer') !!}

            @else
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @endif

        </div>

    </div>

@stop