@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Informe Incidencias', 'manage.informes.bookings-incidencias') !!}
@stop

@section('titulo')
    <i class="fa fa-exclamation-triangle fa-fw"></i> Listado Incidencias
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros</span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.bookings-incidencias'), 'method'=> 'GET', 'class' => 'form']) !!}

            <div class="row">
                <div class="col-md-1">
                    {!! Form::label('any', 'Año') !!}
                    {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'select-any-filtro'))  !!}
                </div>

                <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'categorias'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'users'])
                </div>
                <div class="col-md-4">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    {{-- @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos']) --}}
                </div>

                <div class="col-md-3">
                    {!! Form::label('users', 'Usuarios') !!}
                    @include('includes.form_input_cargando',['id'=> 'users-cargando'])
                    <br>
                    {!! Form::select('users', $users, $valores['users'], array('class'=> 'select2 form-control', 'id'=>'filtro-users', 'name'=> 'users'))  !!}
                </div>
            </div>

            <hr>
            <div class="form-group row">
                <div class="col-md-1">
                    {!! Form::submit('Consultar', array( 'class' => 'btn btn-info')) !!}
                </div>
            </div>

            {{-- @include('includes.script_filtro_any') --}}

            {!! Form::close() !!}

        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-exclamation-triangle"></i>
                <span class="caption-subject bold">Incidencias</span>
            </div>
        </div>
        <div class="portlet-body">

            @if(!$results)
                <br>
                <div class="content">
                    <div class="alert alert-warning" role="alert">
                        Sin Resultados
                    </div>
                </div>
            @else

                <ul class="nav nav-tabs" role="tablist">
                @foreach($tabs as $ktab=>$tab)
                    <li role="presentation" class="{{($tab === reset($tabs))?'active':''}}">
                        <a href="#tab-{{$ktab}}" aria-controls="tab-{{$ktab}}" role="tab" data-toggle="tab">
                        {{$tab}}
                        </a>
                    </li>
                @endforeach
                </ul>

                <div class="tab-content">
                @foreach($tabs as $ktab=>$tab)
                    <div role="tabpanel" class="tab-pane fade in {{($tab === reset($tabs))?'active':''}}" id="tab-{{$ktab}}">

                        <?php
                            $valores['filtro'] = $ktab;
                        ?>

                        {!! Datatable::table()
                            ->addColumn([
                                'fecha'     => 'Fecha',
                                'hora'      => 'Hora',
                                'viajero'   => 'Viajero',
                                'booking'   => 'Booking',
                                'convocatoria'  => 'Convocatoria',
                                'notas'     => 'Descripción',
                                'asignado'  => 'Asignado',
                                'tipo'      => 'Tipo',
                                'estado_fecha'  => 'Resuelta fecha',
                                'estado_hora'   => 'Resuelta hora',
                                'status'    => 'Estado',
                                'options'   => '',
                            ])
                            ->setUrl( route('manage.informes.bookings-incidencias', $valores) )
                            ->setOptions('iDisplayLength', 100)
                            ->setOptions(
                                "columnDefs", array(
                                    [ "sortable" => false, "targets" => [1,3,4,5,9,11] ],
                                    [ "targets" => [0,8], "render"=> "function(data, type, full) {return moment(data).isValid()?moment(data).format('DD/MM/YYYY'):'-';}" ],
                                )
                            )
                            ->render() !!}

                    </div>
                @endforeach
                </div>

            @endif

        </div>

    </div>

@stop