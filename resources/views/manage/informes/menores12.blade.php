@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.menores12') !!}
@stop

@section('titulo')
    <i class="fa fa-users fa-fw"></i> Menores de 12 años en grupo
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-users fa-fw"></i> Informe
                {{--
                @if($desde)
                   :: Entre {{$desde}} y {{$hasta}}
                @endif
                --}}
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                    'convocatoria'=> 'Convocatoria',
                    'vuelo'=> 'Vuelo',
                    'agencia'=> 'Agencia',
                    'viajero'=> 'Viajero',
                    'pasaporte'=> 'Pasaporte',
                    'edad'=> 'Edad',
                    'opciones' => '',
                ])
                ->setUrl(route('manage.informes.menores12'))//,['desde'=>$desde, 'hasta'=> $hasta]))
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "columnDefs", array(
                    //[ "sortable" => false, "targets" => [0] ],
                    //[ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
                ->render() !!}

        </div>
    </div>


@stop