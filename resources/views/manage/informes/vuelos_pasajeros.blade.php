@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Listado Pasajeros', 'manage.informes.vuelos-pasajeros') !!}
@stop

@section('titulo')
    <i class="fa fa-plane fa-fw"></i> Listado Pasajeros
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.vuelos-pasajeros'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">

                    <div class="col-md-2">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-6">
                    {!! Form::label('vuelos', 'Vuelo') !!}
                    @include('includes.form_input_cargando',['id'=> 'vuelos-cargando'])
                    <br>
                    {!! Form::select('vuelos', $vuelos, $valores['vuelos'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-vuelos'))  !!}
                    </div>

                    <div class="col-md-2">
                    {!! Form::label('agencias', 'Agencia') !!}
                    @include('includes.form_input_cargando',['id'=> 'agencias-cargando'])
                    <br>
                    {!! Form::select('agencias', $agencias, $valores['agencias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-agencias'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}

        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <ul class="nav nav-tabs" role="tablist">
                    @foreach($tabs as $tab)
                        <li role="presentation" class="{{($tab === reset($tabs))?'active':''}}">
                            <a href="#tab-{{$tab}}" aria-controls="tab-{{$tab}}" role="tab" data-toggle="tab">
                            <?php
                                $vuelo = \VCN\Models\Convocatorias\Vuelo::find($tab);
                            ?>
                            {{$vuelo?$vuelo->name:"- VUELO NO EXISTE -"}}
                            </a>
                        </li>
                    @endforeach
                    </ul>

                    <div class="tab-content">
                    @foreach($tabs as $tab)
                        <div role="tabpanel" class="tab-pane fade in {{($tab === reset($tabs))?'active':''}}" id="tab-{{$tab}}">


                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <strong><i class="fa fa-list fa-fw"></i> Listado {{$valores['desdes']}} - {{$valores['hastas']}}</strong>
                                </div>
                                <div class="panel-body">

                                    <?php
                                        $valores['vuelos'] = $tab;
                                    ?>

                                    {!! Datatable::table()
                                        ->addColumn([
                                            'num' => 'Nº',
                                            'viajero'       => 'Apellidos, Nombre',
                                            'fechanac'      => 'Fecha Nac.',
                                            'estado'        => 'Estado',
                                            'plataforma'    => 'Empresa',
                                            'requisitos'    => 'Dietas, Alergias',
                                            'convocatoria'  => 'Convocatoria',
                                            'convo_ini'     => 'Fecha Inicio',
                                            'convo_fin'     => 'Fecha Fin'


                                        ])
                                        ->setUrl( route('manage.informes.vuelos-pasajeros', $valores) )
                                        ->setOptions('iDisplayLength', 100)
                                        ->setOptions(
                                            "columnDefs", array(
                                                [ "targets" => [2,7,8], "render"=> "function(data, type, full) {return moment(data).isValid()?moment(data).format('DD/MM/YYYY'):'-';}" ],
                                                [ "targets" => [0], "render"=> "function(data,type,row,meta) { return meta.row+1; }" ],
                                            )
                                        )
                                        ->render() !!}

                                </div>
                            </div>

                        </div>
                    @endforeach
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop