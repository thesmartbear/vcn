@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.facturacion') !!}
@stop

@section('titulo')
    <i class="fa fa-money"></i> Listado Facturación
@stop


@section('container')
    <div class="row">
        <div class="col-md-1">
            {!! Form::select('any', $anys, $any, array('class'=>'select2', 'data-style'=>'blue', 'id'=>'select-any-filtro'))  !!}
            @include('includes.script_filtro_any')
        </div>

        <div class="col-md-5"><i>(=año de la fecha inicio booking)</i></div>

        <?php
            $status_id = $saldo;
        ?>
        @include('includes.select_asignados', ['route'=> 'manage.informes.facturacion'])
        
    </div>

    <hr>

     <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active all"><a href="#todos" aria-controls="todos" role="tab" data-toggle="tab">Todos</a></li>

        <li role="presentation">
            <a href="#saldo-0" aria-controls="saldo-0" role="tab" data-toggle="tab">Saldo 0</a>
        </li>

        <li role="presentation">
            <a href="#saldo-1" aria-controls="saldo-1" role="tab" data-toggle="tab">Saldo &gt;0</a>
        </li>

        <li role="presentation">
            <a href="#saldo-2" aria-controls="saldo-2" role="tab" data-toggle="tab">Saldo &lt;0</a>
        </li>


    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <div role="tabpanel" class="tab-pane fade in active" id="todos">
            @include('manage.informes.facturacion_list', ['saldo'=> 9, 'user_id'=> $user_id, 'oficina_id'=> $oficina_id, 'tab' => 'Todos'])
        </div>

        <div role="tabpanel" class="tab-pane fade in" id="saldo-0">
            @include('manage.informes.facturacion_list', ['saldo'=> 0, 'user_id'=> $user_id, 'oficina_id'=> $oficina_id, 'tab' => 'Saldo 0'])
        </div>

        <div role="tabpanel" class="tab-pane fade in" id="saldo-1">
            @include('manage.informes.facturacion_list', ['saldo'=> 1, 'user_id'=> $user_id, 'oficina_id'=> $oficina_id, 'tab' => 'Saldo >0'])
        </div>

        <div role="tabpanel" class="tab-pane fade in" id="saldo-2">
            @include('manage.informes.facturacion_list', ['saldo'=> 2, 'user_id'=> $user_id, 'oficina_id'=> $oficina_id, 'tab' => 'Saldo <0'])
        </div>

    </div>


@include('includes.script_checklist_status', ['booking'=> true])

@stop