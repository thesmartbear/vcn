@if(!count($anys))
    <div class="alert alert-warning" role="alert">
        Sin datos
    </div>
@endif


<ul class="nav nav-tabs" role="tablist">
    @foreach($anys as $any)
        @if($iany == $any)
            <li role="presentation" class="active"><a href="#ficha-{{$kplat}}-{{$any}}" aria-controls="ficha-{{$any}}" role="tab" data-toggle="tab">{{$any}}</a></li>
        @else
            <?php $link = $href . "&any=$any";?>
            <li><a href="{{$link}}">{{$any}}</a></li>
        @endif
    @endforeach
</ul>
    
<!-- Tab panes -->
<div class="tab-content">

@foreach($anys as $any)
    
    @if($iany != $any)
        @continue
    @endif

    <div role="tabpanel" class="tab-pane fade in active" id="ficha-{{$kplat}}-{{$any}}">

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#ficha-{{$kplat}}-cat-{{$any}}" aria-controls="ficha-cat-{{$any}}" role="tab" data-toggle="tab">Por Categoría y País</a></li>
            <li role="presentation"><a href="#ficha-{{$kplat}}-prov-{{$any}}" aria-controls="ficha-prov-{{$any}}" role="tab" data-toggle="tab">Por Proveedor</a></li>
            <li role="presentation"><a href="#ficha-{{$kplat}}-orig-{{$any}}" aria-controls="ficha-orig-{{$any}}" role="tab" data-toggle="tab">Por Orígen</a></li>
            <li role="presentation"><a href="#ficha-{{$kplat}}-cat-{{$any}}-det" aria-controls="ficha-cat-{{$any}}-det" role="tab" data-toggle="tab">Por Categoría y País (Detalle)</a></li>
            <li role="presentation"><a href="#ficha-{{$kplat}}-orig-{{$any}}-det" aria-controls="ficha-orig-{{$any}}-det" role="tab" data-toggle="tab">Por Origen (Detalle)</a></li>
        </ul>

        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="ficha-{{$kplat}}-cat-{{$any}}">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pie-chart"></i>
                            <span class="caption-subject bold">Distribución por Categoría y País</span>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <i class="fa fa-question-circle"></i> Facturación = Importe total

                        <div class="informe-tabla">
                            <div>
                            <table class="table table-bordered table-striped table-condensed flip-content tablesorter">
                            <caption>
                                <strong>[Inscripciones | % vs Total | Semanas | % vs Total | Facturación | % vs Total]</strong>
                            </caption>
                            <thead class="flip-content">

                                <?php
                                    $vTot = $ventas;
                                    $iTotI = $vTot->sum('inscripciones');
                                    $iTotS = $vTot->sum('semanas');
                                    $iTotF = $vTot->sum('total');
                                ?>

                                <tr>
                                    <th colspan="7">TOTAL</th>
                                    @foreach($ventas->sortBy('pais_id')->groupBy('pais_id') as $vpais )
                                        <?php $iPais = $vpais->first()->pais; ?>
                                        <th colspan='6'>{{$iPais?$iPais->name:"-"}}</th>
                                    @endforeach
                                </tr>

                                <tr>
                                    <th class='td-fijo'>CATEGORÍA</th>
                                    <th>Inscripciones</th>
                                    <th>% vs Total</th>
                                    <th>Semanas</th>
                                    <th>% vs Total</th>
                                    <th>Facturación</th>
                                    <th>% vs Total</th>

                                    @foreach($ventas->sortBy('pais_id')->groupBy('pais_id') as $vpais )
                                        <th>Inscripciones</th>
                                        <th>% vs Total</th>
                                        <th>Semanas</th>
                                        <th>% vs Total</th>
                                        <th>Facturación</th>
                                        <th>% vs Total</th>
                                    @endforeach

                                </tr>

                            </thead>
                            <tbody>
                                @foreach($ventas->sortBy('category_id')->groupBy('category_id') as $categorias )

                                    <?php $cat = $categorias->first(); ?>
                                    <?php $iCat = $cat->category_id; ?>
                                    <?php $vcat = $ventas->where('category_id',$iCat); ?>
                                    <tr>
                                    <td class='td-fijo'>{{$cat->categoria?$cat->categoria->name:"-"}}</td>

                                    <?php $tvi = $vcat->sum('inscripciones'); ?>
                                    <?php $tvs = $vcat->sum('semanas'); ?>
                                    <?php $tvf = $vcat->sum('total'); ?>

                                    <th>{{$tvi}}</th>
                                    <th>
                                        {{$iTotI>0?number_format(($tvi/$iTotI)*100,0):"-"}}%
                                    </th>
                                    <th>{{$tvs}}</th>
                                    <th>
                                        {{$iTotS>0?number_format(($tvs/$iTotS)*100,0):"-"}}%
                                    </th>
                                    <th>{{ConfigHelper::parseMoneda($tvf)}}</th>
                                    <th>
                                        {{$iTotF>0?number_format(($tvf/$iTotF)*100):"-"}}%
                                    </th>

                                    @foreach($ventas->sortBy('pais_id')->groupBy('pais_id') as $vpais )

                                        <?php
                                            $iPais = $vpais->first()->pais_id;
                                            $v = $vcat->where('pais_id',$iPais);

                                            $ivi = $v->sum('inscripciones');
                                            $ivs = $v->sum('semanas');
                                            $ivf = $v->sum('total');
                                        ?>

                                        <td>{{$ivi}}</td>
                                        <td>{{$iTotI>0?number_format(($ivi/$iTotI)*100,0):"-"}}%</td>
                                        <td>{{$ivs}}</td>
                                        <td>{{$iTotS?number_format(($ivs/$iTotS)*100,0):"-"}}%</td>
                                        <td>{{ConfigHelper::parseMoneda($ivf)}}</td>
                                        <td>{{$iTotF>0?number_format(($ivf/$iTotF)*100):"-"}}%</td>

                                    @endforeach

                                    </tr>

                                @endforeach

                                <tr class="tr-total">
                                    <th class='td-fijo tr-total'>[TOTAL]</th>

                                    <th>{{$iTotI}}</th>
                                    <td>{{$iTotI>0?number_format(($iTotI/$iTotI)*100):"-"}}%</td>
                                    <th>{{$iTotS}}</th>
                                    <td>{{$iTotS>0?number_format(($iTotS/$iTotS)*100,0):"-"}}%</td>
                                    <th>{{ConfigHelper::parseMoneda($iTotF)}}</td>
                                    <th>{{$iTotF>0?number_format(($iTotF/$iTotF)*100,0):"-"}}%</th>

                                    @foreach($ventas->sortBy('pais_id')->groupBy('pais_id') as $vpais )

                                        <?php
                                            $iPais = $vpais->first()->pais_id;
                                            $vTot_p = $ventas->where('pais_id',$iPais);
                                            $iTotI_p = $vTot_p->sum('inscripciones');
                                            $iTotS_p = $vTot_p->sum('semanas');
                                            $iTotF_p = $vTot_p->sum('total');
                                        ?>

                                        <th>{{$iTotI_p}}</th>
                                        <th>{{$iTotI>0?number_format(($iTotI_p/$iTotI)*100,0):"-"}}%</th>
                                        <th>{{$iTotS_p}}</th>
                                        <th>{{$iTotS>0?number_format(($iTotS_p/$iTotS)*100,0):"-"}}%</th>
                                        <th>{{ConfigHelper::parseMoneda($iTotF_p)}}</th>
                                        <th>{{$iTotF>0?number_format(($iTotS_p/$iTotF)*100,0):"-"}}%</th>

                                    @endforeach

                                </tr>

                            </tbody>
                            </table>
                            </div>
                        </div>

                    </div>
                </div>

                <hr>

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pie-chart"></i>
                            <span class="caption-subject bold">Distribución por Categoría y País x Oficina</span>
                        </div>
                    </div>
                    <div class="portlet-body">

                    @foreach($oficinas as $ofi)

                        <?php
                            if($esCurso)
                            {
                                // $ofi = $ofi->first();
                            }
                        ?>

                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-money"></i>
                                    <span class="caption-subject bold">Ventas Oficina {{($ofi->oficina?$ofi->oficina->name:"Todas")}} [{{$ventas->where('oficina_id',$ofi->oficina_id)->sum('semanas')}}s en {{$ventas->where('oficina_id',$ofi->oficina_id)->sum('inscripciones')}}b]</span>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <div class="informe-tabla">
                                    <div>
                                    <table class="table table-bordered table-striped table-condensed flip-content tablesorter">
                                    <caption>
                                        <strong>[Inscripciones | % vs Total | Semanas | % vs Total | Facturación | % vs Total]</strong>
                                    </caption>
                                    <thead class="flip-content">

                                        <?php
                                            $vTot = $ventas->where('oficina_id',$ofi->oficina_id);
                                            $iTotI = $vTot->sum('inscripciones');
                                            $iTotS = $vTot->sum('semanas');
                                            $iTotF = $vTot->sum('total');
                                        ?>

                                        <tr>
                                            <th colspan="7">TOTAL</th>
                                            @foreach($ventas->where('oficina_id',$ofi->oficina_id)->sortBy('pais_id')->groupBy('pais_id') as $vpais )
                                                <?php $iPais = $vpais->first()->pais; ?>
                                                <th colspan='6'>{{$iPais?$iPais->name:"-"}}</th>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <th class='td-fijo'>CATEGORÍA</th>
                                            <th>Inscripciones</th>
                                            <th>% vs Total</th>
                                            <th>Semanas</th>
                                            <th>% vs Total</th>
                                            <th>Facturación</th>
                                            <th>% vs Total</th>

                                            @foreach($ventas->where('oficina_id',$ofi->oficina_id)->sortBy('pais_id')->groupBy('pais_id') as $vpais )
                                                <th>Inscripciones</th>
                                                <th>% vs Total</th>
                                                <th>Semanas</th>
                                                <th>% vs Total</th>
                                                <th>Facturación</th>
                                                <th>% vs Total</th>
                                            @endforeach

                                        </tr>

                                    </thead>
                                    <tbody>
                                        @foreach($ventas->where('oficina_id',$ofi->oficina_id)->sortBy('category_id')->groupBy('category_id') as $categorias )

                                            <?php $cat = $categorias->first(); ?>
                                            <?php $iCat = $cat->category_id; ?>
                                            <?php $vcat = $ventas->where('oficina_id',$ofi->oficina_id)->where('category_id',$iCat); ?>
                                            <tr>
                                            <td class='td-fijo'>{{$cat->categoria?$cat->categoria->name:"-"}}</td>

                                            <?php $tvi = $vcat->sum('inscripciones'); ?>
                                            <?php $tvs = $vcat->sum('semanas'); ?>
                                            <?php $tvf = $vcat->sum('total'); ?>

                                            <th>{{$tvi}}</th>
                                            <th>
                                                {{$iTotI>0?number_format(($tvi/$iTotI)*100,0):"-"}}%
                                            </th>
                                            <th>{{$tvs}}</th>
                                            <th>
                                                {{$iTotS>0?number_format(($tvs/$iTotS)*100,0):"-"}}%
                                            </th>
                                            <th>{{ConfigHelper::parseMoneda($tvf)}}</th>
                                            <th>
                                                {{$iTotF>0?number_format(($tvf/$iTotF)*100):"-"}}%
                                            </th>

                                            @foreach($ventas->where('oficina_id',$ofi->oficina_id)->sortBy('pais_id')->groupBy('pais_id') as $vpais )

                                                <?php $iPais = $vpais->first()->pais_id; ?>
                                                <?php $v = $vcat->where('pais_id',$iPais); ?>

                                                <?php
                                                    $ivi = $v->sum('inscripciones');
                                                    $ivs = $v->sum('semanas');
                                                    $ivf = $v->sum('total');
                                                ?>

                                                <td>{{$ivi}}</td>
                                                <td>{{$iTotI>0?number_format(($ivi/$iTotI)*100,0):"-"}}%</td>
                                                <td>{{$ivs}}</td>
                                                <td>{{$iTotS?number_format(($ivs/$iTotS)*100,0):"-"}}%</td>
                                                <td>{{ConfigHelper::parseMoneda($ivf)}}</td>
                                                <td>{{$iTotF>0?number_format(($ivf/$iTotF)*100):"-"}}%</td>

                                            @endforeach

                                            </tr>

                                        @endforeach

                                        <tr class="tr-total">
                                            <th class='td-fijo tr-total'>[TOTAL]</th>

                                            <th>{{$iTotI}}</th>
                                            <td>{{$iTotI>0?number_format(($iTotI/$iTotI)*100):"-"}}%</td>
                                            <th>{{$iTotS}}</th>
                                            <td>{{$iTotS>0?number_format(($iTotS/$iTotS)*100,0):"-"}}%</td>
                                            <th>{{ConfigHelper::parseMoneda($iTotF)}}</td>
                                            <th>{{$iTotF>0?number_format(($iTotF/$iTotF)*100,0):"-"}}%</th>

                                            @foreach($ventas->where('oficina_id',$ofi->oficina_id)->sortBy('pais_id')->groupBy('pais_id') as $vpais )

                                                <?php
                                                    $iPais = $vpais->first()->pais_id;
                                                    $vTot_p = $ventas->where('oficina_id',$ofi->oficina_id)->where('pais_id',$iPais);
                                                    $iTotI_p = $vTot_p->sum('inscripciones');
                                                    $iTotS_p = $vTot_p->sum('semanas');
                                                    $iTotF_p = $vTot_p->sum('total');
                                                ?>

                                                <th>{{$iTotI_p}}</th>
                                                <th>{{$iTotI>0?number_format(($iTotI_p/$iTotI)*100,0):"-"}}%</th>
                                                <th>{{$iTotS_p}}</th>
                                                <th>{{$iTotS>0?number_format(($iTotS_p/$iTotS)*100,0):"-"}}%</th>
                                                <th>{{ConfigHelper::parseMoneda($iTotF_p)}}</th>
                                                <th>{{$iTotF>0?number_format(($iTotS_p/$iTotF)*100,0):"-"}}%</th>

                                            @endforeach

                                        </tr>

                                    </tbody>
                                    </table>
                                    </div>

                                </div>
                            </div>
                        </div>

                    @endforeach

                    </div>

                </div>

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="ficha-{{$kplat}}-cat-{{$any}}-det">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pie-chart"></i>
                            <span class="caption-subject bold">Distribución por Categoría y País (Detalle)</span>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <i class="fa fa-question-circle"></i> Facturación = Importe total

                        <div class="informe-tabla">
                            <div>
                            <table class="table table-bordered table-striped table-condensed flip-content tablesorter">
                            <caption><strong>[Inscripciones | % vs Total | Semanas | % vs Total | Facturación | % vs Total]</strong></caption>
                            <thead class="flip-content">

                                <?php
                                    $vTot = $ventas;
                                    $iTotI = $vTot->sum('inscripciones');
                                    $iTotS = $vTot->sum('semanas');
                                    $iTotF = $vTot->sum('total');
                                ?>

                                <tr>
                                    <th colspan="6">TOTAL</th>
                                    @foreach($ventas->sortBy('pais_id')->groupBy('pais_id') as $vpais )
                                        <?php $iPais = $vpais->first()->pais; ?>
                                        <th colspan='6'>{{$iPais?$iPais->name:"-"}}</th>
                                    @endforeach
                                </tr>

                                <tr>
                                    <th class='td-fijo'>CATEGORÍA</th>
                                    <th>Inscripciones</th>
                                    <th>% vs Total</th>
                                    <th>Semanas</th>
                                    <th>% vs Total</th>
                                    <th>Facturación</th>
                                    <th>% vs Total</th>

                                    @foreach($ventas->sortBy('pais_id')->groupBy('pais_id') as $vpais )
                                        <th>Inscripciones</th>
                                        <th>% vs Total</th>
                                        <th>Semanas</th>
                                        <th>% vs Total</th>
                                        <th>Facturación</th>
                                        <th>% vs Total</th>
                                    @endforeach

                                </tr>

                            </thead>
                            <tbody>
                                @foreach($ventas->sortBy('category_id')->groupBy('category_id') as $categorias )

                                    @foreach($categorias->groupBy('subcategory_id') as $subcategorias)
                                        @foreach($subcategorias->groupBy('subcategory_det_id') as $categoria)

                                            <?php $cat = $categoria->first(); ?>
                                            <?php $iCat = $cat->category_id; ?>
                                            <?php $iSubcat = $cat->subcategory_id; ?>
                                            <?php $iSubcat_det = $cat->subcategory_det_id; ?>
                                            <?php $vcat = $ventas->where('category_id',$iCat)->where('subcategory_id',$iSubcat)->where('subcategory_det_id',$iSubcat_det); ?>
                                            <tr>
                                            <td class='td-fijo'>{{$cat->full_name_categoria}}</td>

                                            <?php $tvi = $vcat->sum('inscripciones'); ?>
                                            <?php $tvs = $vcat->sum('semanas'); ?>
                                            <?php $tvf = $vcat->sum('total'); ?>

                                            <th>{{$tvi}}</th>
                                            <th>{{$iTotI>0?number_format(($tvi/$iTotI)*100,0):"-"}}%</th>
                                            <th>{{$tvs}}</th>
                                            <th>{{$iTotS>0?number_format(($tvs/$iTotS)*100,0):"-"}}%</th>
                                            <th>{{ConfigHelper::parseMoneda($tvf)}}</th>
                                            <th>{{$iTotF>0?number_format(($tvf/$iTotF)*100):"-"}}%</th>

                                            @foreach($ventas->sortBy('pais_id')->groupBy('pais_id') as $vpais )

                                                <?php $iPais = $vpais->first()->pais_id; ?>
                                                <?php $v = $vcat->where('pais_id',$iPais); ?>

                                                <?php
                                                    $ivi = $v->sum('inscripciones');
                                                    $ivs = $v->sum('semanas');
                                                    $ivf = $v->sum('total');
                                                ?>

                                                <td>{{$ivi}}</td>
                                                <td>{{$iTotI>0?number_format(($ivi/$iTotI)*100,0):"-"}}%</td>
                                                <td>{{$ivs}}</td>
                                                <td>{{$iTotS>0?number_format(($ivs/$iTotS)*100,0):0}}%</td>
                                                <td>{{ConfigHelper::parseMoneda($ivf)}}</td>
                                                <td>{{$iTotF>0?number_format(($ivf/$iTotF)*100):"-"}}%</td>

                                            @endforeach

                                            </tr>

                                        @endforeach
                                    @endforeach

                                @endforeach

                                <tr class="tr-total">
                                    <th class='td-fijo tr-total'>[TOTAL]</th>

                                    <th>{{$iTotI}}</th>
                                    <th>{{$iTotI>0?number_format(($iTotI/$iTotI)*100,0):"-"}}%</th>
                                    <th>{{$iTotS}}</th>
                                    <th>{{$iTotS>0?number_format(($iTotS/$iTotS)*100,0):"-"}}%</th>
                                    <th>{{ConfigHelper::parseMoneda($iTotF)}}</td>
                                    <th>{{$iTotF>0?number_format(($iTotF/$iTotF)*100):"-"}}%</th>

                                    @foreach($ventas->sortBy('pais_id')->groupBy('pais_id') as $vpais )

                                        <?php
                                            $iPais = $vpais->first()->pais_id;
                                            $vTot_p = $ventas->where('pais_id',$iPais);
                                            $iTotI_p = $vTot_p->sum('inscripciones');
                                            $iTotS_p = $vTot_p->sum('semanas');
                                            $iTotF_p = $vTot_p->sum('total');
                                        ?>

                                        <th>{{$iTotI_p}}</th>
                                        <th>{{$iTotI>0?number_format(($iTotI_p/$iTotI)*100,0):"-"}}%</th>
                                        <th>{{$iTotS_p}}</th>
                                        <th>{{$iTotS>0?number_format(($iTotS_p/$iTotS)*100,0):"-"}}%</th>
                                        <th>{{ConfigHelper::parseMoneda($iTotF_p)}}</th>
                                        <th>{{$iTotF>0?number_format(($iTotF_p/$iTotF)*100):"-"}}%</th>

                                    @endforeach

                                </tr>

                            </tbody>
                            </table>
                            </div>
                        </div>

                        {{-- Desglose Categoria --}}
                        @foreach($ventas->sortBy('category_id')->groupBy('category_id') as $categorias )

                            <?php $cat = $categorias->first(); ?>
                            <?php $iCat = $cat->category_id; ?>

                            <?php
                                $vTot = $ventas->where('category_id',$iCat);
                                $iTotI = $vTot->sum('inscripciones');
                                $iTotS = $vTot->sum('semanas');
                                $iTotF = $vTot->sum('total');
                            ?>

                            <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-pie-chart"></i>
                                    <span class="caption-subject bold">Categoría {{$cat->categoria?$cat->categoria->name:"-"}} [{{$iTotI}} | {{$iTotS}} | {{ConfigHelper::parseMoneda($iTotF)}}]</span>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <i class="fa fa-question-circle"></i> Facturación = Importe total

                                <div class="informe-tabla">
                                    <div>
                                    <table class="table table-bordered table-striped table-condensed flip-content tablesorter">
                                    <caption><strong>[Inscripciones | % vs Total | Semanas | % vs Total | Facturación | % vs Total]</strong></caption>
                                    <thead class="flip-content">
                                        <tr>
                                            <th class='td-fijo'>CATEGORÍA</th>
                                            <th colspan="6">TOTAL</th>
                                            @foreach($ventas->where('category_id',$iCat)->sortBy('pais_id')->groupBy('pais_id') as $vpais )
                                                <?php $iPais = $vpais->first()->pais; ?>
                                                <th colspan='6'>{{$iPais?$iPais->name:"-"}}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($categorias->groupBy('subcategory_id') as $subcategorias)
                                            @foreach($subcategorias->groupBy('subcategory_det_id') as $categoria)

                                                <?php $cat = $categoria->first(); ?>

                                                <?php $iSubcat = $cat->subcategory_id; ?>
                                                <?php $iSubcat_det = $cat->subcategory_det_id; ?>
                                                <?php $vcat = $ventas->where('category_id',$iCat)->where('subcategory_id',$iSubcat)->where('subcategory_det_id',$iSubcat_det); ?>
                                                <tr>
                                                <td class='td-fijo'>{{$cat->full_name_categoria}}</td>

                                                <?php $tvi = $vcat->sum('inscripciones'); ?>
                                                <?php $tvs = $vcat->sum('semanas'); ?>
                                                <?php $tvf = $vcat->sum('total'); ?>

                                                <th>{{$tvi}}</th>
                                                <th>{{$iTotI>0?number_format(($tvi/$iTotI)*100,0):"-"}}%</th>
                                                <th>{{$tvs}}</th>
                                                <th>{{$iTotS>0?number_format(($tvs/$iTotS)*100,0):"-"}}%</th>
                                                <th>{{ConfigHelper::parseMoneda($tvf)}}</th>
                                                <th>{{$iTotF>0?number_format(($tvf/$iTotF)*100):"-"}}%</th>

                                                @foreach($ventas->where('category_id',$iCat)->sortBy('pais_id')->groupBy('pais_id') as $vpais )

                                                    <?php $iPais = $vpais->first()->pais_id; ?>
                                                    <?php $v = $vcat->where('pais_id',$iPais); ?>

                                                    <?php
                                                        $ivi = $v->sum('inscripciones');
                                                        $ivs = $v->sum('semanas');
                                                        $ivf = $v->sum('total');
                                                    ?>

                                                    <td>{{$ivi}}</td>
                                                    <td>{{$iTotI>0?number_format(($ivi/$iTotI)*100,0):"-"}}%</td>
                                                    <td>{{$ivs}}</td>
                                                    <td>{{$iTotS>0?number_format(($ivs/$iTotS)*100,0):"-"}}%</td>
                                                    <td>{{ConfigHelper::parseMoneda($ivf)}}</td>
                                                    <td>{{$iTotF>0?number_format(($ivf/$iTotF)*100):"-"}}%</td>

                                                @endforeach

                                                </tr>

                                            @endforeach
                                        @endforeach

                                        <tr class="tr-total">
                                            <th class='td-fijo tr-total'>[TOTAL]</th>

                                            <th>{{$iTotI}}</th>
                                            <th>{{$iTotI>0?number_format(($iTotI/$iTotI)*100,0):"-"}}%</th>
                                            <th>{{$iTotS}}</th>
                                            <th>{{$iTotS>0?number_format(($iTotS/$iTotS)*100,0):"-"}}%</th>
                                            <th>{{ConfigHelper::parseMoneda($iTotF)}}</td>
                                            <th>{{$iTotF>0?number_format(($iTotF/$iTotF)*100):"-"}}%</th>

                                            @foreach($ventas->where('category_id',$iCat)->sortBy('pais_id')->groupBy('pais_id') as $vpais )

                                                <?php
                                                    $iPais = $vpais->first()->pais_id;
                                                    $vTot_p = $ventas->where('category_id',$iCat)->where('pais_id',$iPais);
                                                    $iTotI_p = $vTot_p->sum('inscripciones');
                                                    $iTotS_p = $vTot_p->sum('semanas');
                                                    $iTotF_p = $vTot_p->sum('total');
                                                ?>

                                                <th>{{$iTotI_p}}</th>
                                                <th>{{$iTotI>0?number_format(($iTotI_p/$iTotI)*100,0):"-"}}%</th>
                                                <th>{{$iTotS_p}}</th>
                                                <th>{{$iTotS>0?number_format(($iTotS_p/$iTotS)*100,0):"-"}}%</th>
                                                <th>{{ConfigHelper::parseMoneda($iTotF_p)}}</th>
                                                <th>{{$iTotF>0?number_format(($iTotF_p/$iTotF)*100):"-"}}%</th>

                                            @endforeach

                                        </tr>

                                    </tbody>
                                    </table>
                                    </div>
                                </div>

                            </div>
                            </div>
                        @endforeach

                    </div>
                </div>

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="ficha-{{$kplat}}-prov-{{$any}}">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pie-chart"></i>
                            <span class="caption-subject bold">Distribución por Proveedor</span>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <i class="fa fa-question-circle"></i> Facturación = Importe total

                        <div class="informe-tabla">
                            <div>
                            <table class="table table-bordered table-striped table-condensed flip-content tablesorter">
                            <caption></caption>
                            <thead class="flip-content">

                                <?php
                                    $vTot = $ventas;
                                    $iTotI = $vTot->sum('inscripciones');
                                    $iTotS = $vTot->sum('semanas');
                                    $iTotF = $vTot->sum('total');
                                ?>

                                <tr>
                                    <th class='td-fijo'>PROVEEDOR</th>
                                    <th>Inscripciones</th>
                                    <th>% vs Total</th>
                                    <th>Semanas</th>
                                    <th>% vs Total</th>
                                    <th>Facturación</th>
                                    <th>% vs Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($ventas->sortBy('proveedor_id')->groupBy('proveedor_id') as $proveedor )

                                    <?php $prov = $proveedor->first(); ?>
                                    <?php $iProv = $prov->proveedor_id; ?>
                                    <?php $vprov = $ventas->where('proveedor_id',$iProv); ?>

                                    <?php
                                        $ivi = $vprov->sum('inscripciones');
                                        $ivs = $vprov->sum('semanas');
                                        $ivf = $vprov->sum('total');
                                    ?>

                                    <tr>
                                    <td class='td-fijo'>{{$prov->proveedor?$prov->proveedor->name:"-"}}</td>
                                    <td>{{$ivi}}</td>
                                    <td>{{$iTotI>0?number_format(($ivi/$iTotI)*100):"-"}}%</td>
                                    <td>{{$ivs}}</td>
                                    <td>{{$iTotS>0?number_format(($ivs/$iTotS)*100):"-"}}%</td>
                                    <td>{{ConfigHelper::parseMoneda($ivf)}}</td>
                                    <td>{{$iTotF>0?number_format(($ivf/$iTotF)*100):"-"}}%</td>
                                    </tr>

                                @endforeach

                                <tr class="tr-total">
                                    <th class='td-fijo tr-total'>[TOTAL]</th>

                                    <th>{{$iTotI}}</th>
                                    <th>{{$iTotI>0?number_format(($iTotI/$iTotI)*100):"-"}}%</th>
                                    <th>{{$iTotS}}</th>
                                    <th>{{$iTotS>0?number_format(($iTotS/$iTotS)*100):"-"}}%</th>
                                    <th>{{ConfigHelper::parseMoneda($iTotF)}}</th>
                                    <th>{{$iTotF>0?number_format(($iTotF/$iTotF)*100):"-"}}%</th>
                                </tr>

                            </tbody>
                            </table>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="ficha-{{$kplat}}-orig-{{$any}}">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pie-chart"></i>
                            <span class="caption-subject bold">Distribución por Orígen</span>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <i class="fa fa-question-circle"></i> Facturación = Importe total

                        <div class="informe-tabla">
                            <div>
                            <table class="table table-bordered table-striped table-condensed flip-content tablesorter">
                            <caption></caption>
                            <thead class="flip-content">

                                <?php
                                    $vTot = $ventas;
                                    $iTotI = $vTot->sum('inscripciones');
                                    $iTotS = $vTot->sum('semanas');
                                    $iTotF = $vTot->sum('total');
                                ?>

                                <tr>
                                    <th class='td-fijo'>ORÍGEN</th>
                                    <th>Inscripciones</th>
                                    <th>% vs Total</th>
                                    <th>Semanas</th>
                                    <th>% vs Total</th>
                                    <th>Facturación</th>
                                    <th>% vs Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($ventas->sortBy('origen_id')->groupBy('origen_id') as $origen )

                                    <?php $orig = $origen->first(); ?>
                                    <?php $iOrig = $orig->origen_id; ?>
                                    <?php $vorig = $ventas->where('origen_id',$iOrig); ?>

                                    <?php
                                        $ivi = $vorig->sum('inscripciones');
                                        $ivs = $vorig->sum('semanas');
                                        $ivf = $vorig->sum('total');
                                    ?>

                                    <tr>
                                    <td class='td-fijo'>{{$orig->origen?$orig->origen->name:"-"}}</td>
                                    <td>{{$ivi}}</td>
                                    <td>{{$iTotI>0?number_format(($ivi/$iTotI)*100):"-"}}%</td>
                                    <td>{{$ivs}}</td>
                                    <td>{{$iTotS>0?number_format(($ivs/$iTotS)*100):"-"}}%</td>
                                    <td>{{ConfigHelper::parseMoneda($ivf)}}</td>
                                    <td>{{$iTotF>0?number_format(($ivf/$iTotF)*100):"_"}}%</td>
                                    </tr>

                                @endforeach

                                <tr class="tr-total">
                                    <th class='td-fijo tr-total'>[TOTAL]</th>

                                    <th>{{$iTotI}}</th>
                                    <th>{{$iTotI>0?number_format(($iTotI/$iTotI)*100):"-"}}%</th>
                                    <th>{{$iTotS}}</th>
                                    <th>{{$iTotS>0?number_format(($iTotS/$iTotS)*100):"-"}}%</th>
                                    <th>{{ConfigHelper::parseMoneda($iTotF)}}</th>
                                    <th>{{$iTotF>0?number_format(($iTotF/$iTotF)*100):"-"}}%</th>
                                </tr>

                            </tbody>
                            </table>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div role="tabpanel" class="tab-pane fade in" id="ficha-{{$kplat}}-orig-{{$any}}-det">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pie-chart"></i>
                            <span class="caption-subject bold">Distribución por Orígen (Detalle)</span>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <i class="fa fa-question-circle"></i> Facturación = Importe total

                        <div class="informe-tabla">
                            <div>
                            <table class="table table-bordered table-striped table-condensed flip-content tablesorter">
                            <caption><strong>[Inscripciones | % vs Total | Semanas | % vs Total | Facturación | % vs Total]</strong></caption>
                            <thead class="flip-content">

                                <?php
                                    $vTot = $ventas;
                                    $iTotI = $vTot->sum('inscripciones');
                                    $iTotS = $vTot->sum('semanas');
                                    $iTotF = $vTot->sum('total');
                                ?>

                                <tr>
                                    <th class='td-fijo'>ORÍGEN</th>
                                    <th>Inscripciones</th>
                                    <th>% vs Total</th>
                                    <th>Semanas</th>
                                    <th>% vs Total</th>
                                    <th>Facturación</th>
                                    <th>% vs Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($ventas->sortBy('origen_id')->groupBy('origen_id') as $origenes )

                                    @foreach($origenes->groupBy('suborigen_id') as $suborigenes)
                                        @foreach($suborigenes->groupBy('suborigendet_id') as $origen)

                                            <?php $orig = $origen->first(); ?>
                                            <?php $iOrig = $orig->origen_id; ?>
                                            <?php $iSuborig = $orig->suborigen_id; ?>
                                            <?php $iSuborig_det = $orig->suborigendet_id; ?>
                                            <?php $vorig = $ventas->where('origen_id',$iOrig)->where('suborigen_id',$iSuborig)->where('suborigendet_id',$iSuborig_det); ?>
                                            <tr>
                                            <td class='td-fijo'>{{$orig->full_name_origen}}</td>

                                            <?php $tvi = $vorig->sum('inscripciones'); ?>
                                            <?php $tvs = $vorig->sum('semanas'); ?>
                                            <?php $tvf = $vorig->sum('total'); ?>

                                            <td>{{$tvi}}</td>
                                            <td>{{$iTotI>0?number_format(($tvi/$iTotI)*100,0):"-"}}%</td>
                                            <td>{{$tvs}}</td>
                                            <td>{{$iTotS>0?number_format(($tvs/$iTotS)*100,0):"-"}}%</td>
                                            <td>{{ConfigHelper::parseMoneda($tvf)}}</td>
                                            <td>{{$iTotF>0?number_format(($tvf/$iTotF)*100):"-"}}%</td>

                                            </tr>

                                        @endforeach
                                    @endforeach

                                @endforeach

                                <tr class="tr-total">
                                    <th class='td-fijo tr-total'>[TOTAL]</th>

                                    <th>{{$iTotI}}</th>
                                    <th>{{$iTotI>0?number_format(($iTotI/$iTotI)*100,0):"-"}}%</th>
                                    <th>{{$iTotS}}</th>
                                    <th>{{$iTotS>0?number_format(($iTotS/$iTotS)*100,0):"-"}}%</th>
                                    <th>{{ConfigHelper::parseMoneda($iTotF)}}</th>
                                    <th>{{$iTotF>0?number_format(($iTotF/$iTotF)*100):"-"}}%</th>

                                </tr>

                            </tbody>
                            </table>
                            </div>
                        </div>

                        {{-- Desglose Orígen --}}
                        @foreach($ventas->sortBy('origen_id')->groupBy('origen_id') as $origenes )

                            <?php $orig = $origenes->first(); ?>
                            <?php $iOrig = $orig->origen_id; ?>

                            <?php
                                $vTot = $ventas->where('origen_id',$iOrig);
                                $iTotI = $vTot->sum('inscripciones');
                                $iTotS = $vTot->sum('semanas');
                                $iTotF = $vTot->sum('total');
                            ?>

                            <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-pie-chart"></i>
                                    <span class="caption-subject bold">Orígen {{$orig->origen?$orig->origen->name:"-"}} [{{$iTotI}} | {{$iTotS}} | {{ConfigHelper::parseMoneda($iTotF)}}]</span>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <i class="fa fa-question-circle"></i> Facturación = Importe total

                                <div class="informe-tabla">
                                    <div>
                                    <table class="table table-bordered table-striped table-condensed flip-content tablesorter">
                                    <caption><strong>[Inscripciones | % vs Total | Semanas | % vs Total | Facturación | % vs Total]</strong></caption>
                                    <thead class="flip-content">
                                        <tr>
                                            <th class='td-fijo'>ORÍGEN</th>
                                            <th>Inscripciones</th>
                                            <th>% vs Total</th>
                                            <th>Semanas</th>
                                            <th>% vs Total</th>
                                            <th>Facturación</th>
                                            <th>% vs Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($origenes->groupBy('suborigen_id') as $suborigenes)
                                            @foreach($suborigenes->groupBy('suborigendet_id') as $origen)

                                                <?php $orig = $origen->first(); ?>

                                                <?php $iSuborig = $orig->suborigen_id; ?>
                                                <?php $iSuborig_det = $orig->suborigendet_id; ?>
                                                <?php $vorig = $ventas->where('origen_id',$iOrig)->where('suborigen_id',$iSuborig)->where('suborigendet_id',$iSuborig_det); ?>
                                                <tr>
                                                <td class='td-fijo'>{{$orig->full_name_origen}}</td>

                                                <?php $tvi = $vorig->sum('inscripciones'); ?>
                                                <?php $tvs = $vorig->sum('semanas'); ?>
                                                <?php $tvf = $vorig->sum('total'); ?>

                                                <td>{{$tvi}}</td>
                                                <td>{{$iTotI>0?number_format(($tvi/$iTotI)*100,0):"-"}}%</td>
                                                <td>{{$tvs}}</td>
                                                <td>{{$iTotS>0?number_format(($tvs/$iTotS)*100,0):"-"}}%</td>
                                                <td>{{ConfigHelper::parseMoneda($tvf)}}</td>
                                                <td>{{$iTotF>0?number_format(($tvf/$iTotF)*100):"-"}}%</td>

                                                </tr>

                                            @endforeach
                                        @endforeach

                                        <tr class="tr-total">
                                            <th class='td-fijo tr-total'>[TOTAL]</th>

                                            <th>{{$iTotI}}</th>
                                            <th>{{$iTotI>0?number_format(($iTotI/$iTotI)*100,0):"-"}}%</th>
                                            <th>{{$iTotS}}</th>
                                            <th>{{$iTotS>0?number_format(($iTotS/$iTotS)*100,0):"-"}}%</th>
                                            <th>{{ConfigHelper::parseMoneda($iTotF)}}</td>
                                            <th>{{$iTotF>0?number_format(($iTotF/$iTotF)*100):"-"}}%</th>
                                        </tr>

                                    </tbody>
                                    </table>
                                    </div>
                                </div>

                            </div>
                            </div>
                        @endforeach


                    </div>
                </div>

            </div>

        </div>

    </div>

@endforeach

</div>