@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Análisis Ventas', 'manage.informes.ventas-analisis') !!}
@stop

@section('titulo')
    <i class="fa fa-pie-chart fa-fw"></i> Análisis Ventas
    @if($semanaActual)
        <i>(Datos acumulados hasta semana {{$semanaActual}})</i>
    @else
        <i>(Datos acumulados total año)</i>
    @endif
@stop


@section('container')

<div class="form-group row">
    <div class="col-md-3">
        @if($esCurso)
            <a class="btn btn-info" href="{{route('manage.informes.ventas-analisis',[($semanaActual?0:1),0])}}">Agrupar por fecha booking</a>
        @else
            <a class="btn btn-info" href="{{route('manage.informes.ventas-analisis',[($semanaActual?0:1),1])}}">Agrupar por fecha inicio programa</a>
        @endif
    </div>
    <div class="col-md-3">
        @if($semanaActual)
            <a class="btn btn-success" href="{{route('manage.informes.ventas-analisis',[1,$esCurso])}}">Ver Datos acumulados total año</a>
            <hr>
        @else
            <a class="btn btn-success" href="{{route('manage.informes.ventas-analisis',[0,$esCurso])}}">Ver Datos acumulados hasta semana actual</a>
            <hr>
        @endif
    </div>

</div>

<div class="form-group row">

    @if(auth()->user()->informe_oficinas)
    {!! Form::open(['url' => route('manage.informes.ventas-analisis',[($semanaActual?0:1),$esCurso]), 'method'=> 'GET', 'class' => 'form']) !!}
    <div class="col-md-3">
        {!! Form::label('fil_oficinas', 'Oficinas') !!}
        {!! Form::select('fil_oficinas', $fil_oficinas, $valores['fil_oficinas'], array('class'=>'select2', 'multiple'=>'multiple', 'id'=>'filtro-oficinas', 'name'=> 'fil_oficinas[]'))  !!}
    </div>

    <div class="col-md-2">
        {!! Form::label('categorias', 'Categoría') !!}
        @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
        <br>
        {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
        @include('includes.script_filtros_multi', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
        @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
    </div>
    
    <div class="col-md-2">
        {!! Form::label('subcategorias', 'Subcategoría') !!}
        @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
        <br>
        {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
        @include('includes.script_filtros_multi', ['filtro'=> 'subcategorias', 'destino'=> 'cursos'])
    </div>

    <div class="col-md-3">
        {!! Form::submit('Filtrar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
    </div>
    {!! Form::close() !!}
    @endif

</div>


@if($esCurso)
    <i><strong>Bookings agrupados por año de inicio programa (no por fecha booking)</strong></i>
@else
    <i><strong>Bookings agrupados por año de fecha booking</strong></i>
@endif

<hr>

    @include('manage.informes.ventas_analisis_tablas', ['kplat'=> 0])


<script type="text/javascript">
    $(document).ready(function() {

        $(".tablesorter").tablesorter();

    });

</script>

@stop