@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Descuentos', 'manage.informes.descuentos') !!}
@stop

@section('titulo')
    <i class="fa fa-gift fa-fw"></i> Informe Descuentos
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.descuentos'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>


        </div>
    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Inscripciones</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong><i class="fa fa-list fa-fw"></i> Listado {{$valores['desdes']}} - {{$valores['hastas']}}</strong>
                    </div>
                    <div class="panel-body">

                        <?php
                            $dto_total = 0;
                            $cuenta_total = 0;
                        ?>

                        <table id="table-dtos" class="table table-bordered table-hover">
                            <caption>Totales</caption>
                            <thead>
                                <tr>
                                    <th>Descuento</th>
                                    <th>Cantidad</th>
                                    @foreach($categorias as $cati=>$catn)
                                        <th>{{$catn}}</th>
                                    @endforeach
                                    <th>TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($listado as $dto_nom=>$dto_importe)
                                <tr>
                                    <td>{{$dto_nom}}</td>
                                    <td>{{$cuenta[$dto_nom]}}</td>
                                    @foreach($categorias as $cati=>$catn)
                                        <td>{{isset($totales[$dto_nom][$cati])?$totales[$dto_nom][$cati]:'-'}}</td>
                                    @endforeach
                                    <th>{{$dto_importe}}</th>
                                </tr>
                                <?php
                                $dto_total += $dto_importe;
                                $cuenta_total += $cuenta[$dto_nom];
                                ?>
                                @endforeach

                                <tr>
                                    <th>TOTAL</th>
                                    <th>{{$cuenta_total}}</th>
                                    @foreach($categorias as $cati=>$catn)
                                        <th>{{$total[$cati]}}</th>
                                    @endforeach
                                    <th>{{$dto_total}}</th>
                                </tr>

                            </tbody>
                        </table>

                    </div>
                </div>

           @endif


        </div>

    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#table-dtos').dataTable(
                {
                    "sPaginationType":"full_numbers",
                    "bProcessing":true,
                    "responsive":true,
                    "bAutoWidth":false,
                    "oLanguage": {
                        "oPaginate":{ "sFirst":"Primero",
                        "sLast":"Ultimo",
                        "sPrevious":"Anterior",
                        "sNext":"Siguiente" },
                        "sSearch":"Buscar: ",
                        "sProcessing":"&nbsp;&nbsp;&nbsp;<i class='fa fa-refresh fa-spin'><\/i>",
                        "sLengthMenu":"Mostrar _MENU_ registros por p\u00e1gina",
                        "sZeroRecords":"No se encontraron resultados",
                        "sInfo":"Listando de _START_ a _END_ de _TOTAL_ registros",
                        "sInfoEmpty":"Listando de 0 a 0 de 0 registros",
                        "sInfoFiltered":"(filtrando desde _MAX_ total de registros)"
                    },
                    "lengthMenu":[ [ 10,25,50,100,999999 ], [ 10,25,50,100,"Todos" ] ],
                    "dom": '<"top"i>Bflrtip',
                    "buttons":[ "copy", "csv", "excel", "print" ],
                });
        });
    </script>

    {{-- @include('datatables.backup.javascript', array('id' => 'table-dtos', 'options' => config('chumper.datatable.table')['options'])) --}}

@stop