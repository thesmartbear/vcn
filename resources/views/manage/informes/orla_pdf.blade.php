<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>

        {!! Html::style('assets/css/pdf.css') !!}
        {!! Html::style('assets/css/bootstrap.css') !!}

        <style>

            .informe-orla ul {
                list-style-type: none;
                margin: 0;
                padding: 0;
                display: block;
            }
            .informe-orla li {
                float: left;
                width: 25%;
            }
            .informe-orla ul li .foto {
                height: 85px;
                width: auto;
                padding: 1px;
                margin-bottom: 8px;
            }

        </style>

    </head>
    <body>
    
        <div class="page">

        @foreach($tabs as $ktab=>$vtab)

            <h2>{{$vtab}}</h2>
            <br><br>

            <?php
                $convo_t = $valores['tipoc'];
                $convo_f = $valores['filtroc'];
                $convo_id = $ktab;

                $list = clone $bookings;
                $list = $list->where($convo_f, $convo_id);

                $viajeros = [];
                foreach($list->get() as $booking)
                {
                    $viajero = $booking->viajero;

                    $v['nom'] = $viajero->full_name;
                    $v['apellidos'] = "$viajero->lastname $viajero->lastname2";
                    $v['foto'] = $viajero->avatar ?: $viajero->foto;
                    
                    $viajeros[] = $v;
                }

                asort($viajeros);
            ?>
                
                <div class="page">
                <div id="orla_{{$convo_id}}" class="informe-orla">
                    <ul>
                        @foreach($viajeros as $viajero)
                        <li class='col-md-3'>
                            <div class="orla-box">
                                <div class="orla-foto">
                                    <img src="{{$viajero['foto']}}" class="foto">
                                    {{$viajero['nom']}}
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    <div class="clearfix"></div>
                </div>
                </div>

            
        @endforeach
        </div>
    
    </body>
</html>