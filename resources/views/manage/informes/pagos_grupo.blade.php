@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.pagos') !!}
@stop

@section('titulo')
    <i class="fa fa-money fa-fw"></i> Informe de pagos agrupados
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-money fa-fw"></i> Pagos
                @if($desde)
                   :: Entre {{$desde}} y {{$hasta}}
                @endif
        </div>
        <div class="panel-body">

            {!! Form::open(array('method' => 'GET', 'url' => route('manage.informes.pagos.grupo'), 'role' => 'form', 'class' => '')) !!}
            <div class="row">
                <div class="col-md-1">Filtro entre fechas:</div>
                <div class="col-md-3">
                    @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                </div>
                <div class="col-md-3">
                    @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                </div>
                <div class="col-md-3">
                    <button type='submit' class="btn btn-info">Filtrar</button>
                </div>
            </div>
            {!! Form::close() !!}

            <hr>

            @if($desde && $hasta)

                {!! Datatable::table()
                    ->addColumn([
                      'fecha' => 'Fecha',
                      'viajero' => 'Viajero',
                      'importe' => 'Importe Total',
                      'contable' => 'Código',
                    ])
                    ->setUrl(route('manage.informes.pagos',['desde'=>$desde, 'hasta'=> $hasta]))
                    ->setOptions('iDisplayLength', 0)
                    ->setOptions(
                      "columnDefs", array(
                        //[ "sortable" => false, "targets" => [0] ],
                        [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                      )
                    )
                    ->render() !!}

            @else
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @endif

        </div>
    </div>


@stop