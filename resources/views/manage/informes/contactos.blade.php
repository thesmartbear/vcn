@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Contactos diarios', 'manage.informes.contactos') !!}
@stop

@section('titulo')
    <i class="fa fa-list fa-fw"></i> Contactos diarios
@stop

@section('container')

<?php
$user = auth()->user();
?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-filter"></i>
            <span class="caption-subject bold">Filtros </span>
        </div>
    </div>
    <div class="portlet-body">

        {!! Form::open(['route' => array('manage.informes.contactos'), 'method'=> 'GET', 'class' => 'form']) !!}

            <div class="form-group row">
                @if(auth()->user()->isFullAdmin())
                <div class="col-md-2">
                {!! Form::label('plataformas', 'Plataforma') !!}
                <br>
                {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'users'])
                </div>
                @endif

                @if( auth()->user()->filtro_oficinas )
                <div class="col-md-3">
                {!! Form::label('oficinas', 'Oficina') !!}
                <br>
                @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'multiple'=>'multiple', 'id'=>'filtro-oficinas', 'name'=> 'oficinas[]'))  !!}

                </div>
                @endif

                <div class="col-md-3">
                {!! Form::label('usuarios', 'Usuarios') !!}
                <br>
                @include('includes.form_input_cargando',['id'=> 'users-cargando'])

                {!! Form::select('users', $users, $valores['users'], array('class'=>'select2', 'multiple'=>'multiple', 'id'=>'filtro-users', 'name'=> 'users[]'))  !!}
                </div>

            </div>

            <div class="form-group row">

                <div class="col-md-2">
                    {!! Form::label('desde','Desde') !!}
                    @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=> $valores['desde']])
                </div>
                <div class="col-md-2">
                    {!! Form::label('hasta','Hasta') !!}
                    @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=> $valores['hasta']])
                </div>

                <div class="col-md-2">
                    {!! Form::label('&nbsp;') !!}<br>
                    {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                </div>

                <div class="col-md-1">
                    {!! Form::label('&nbsp;') !!}
                    {!! Form::submit('30 días', array( 'name'=> 'filtro_auto_1', 'class' => 'btn btn-info')) !!}
                </div>

                <div class="col-md-1">
                    {!! Form::label('&nbsp;') !!}
                    {!! Form::submit('90 días', array( 'name'=> 'filtro_auto_2', 'class' => 'btn btn-info')) !!}
                </div>

            </div>

        {!! Form::close() !!}

    </div>

</div>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-ticket"></i>
            <span class="caption-subject bold">CONTACTOS [{{$valores['desde']}} - {{$valores['hasta']}}]</span>
        </div>
    </div>

    <div class="portlet-body">

    @if(!$resultados)
        <div class="content">
            <div class="alert alert-info" role="alert">
                Seleccione los filtros correspondientes
            </div>
        </div>
    @else

        <div id="chart_c1"></div>
        @columnchart('Chart-Contactos', 'chart_c1')

        <hr>
    
        <div class="table-responsive">
        <table class="table table-bordered table-striped table-condensed">
            <thead>
                <tr>
                    <th></th>
                    @foreach($usuarios as $u)
                        <th>{{$u->full_name}}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($fechas as $f)
                <tr>
                    <td>{{$f}}</td>
                    @foreach($usuarios as $u)
                        <td>{{$contactos[$f][$u->id]}}</td>
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        
        </table>
        </div>

    @endif

    </div>
</div>

@stop