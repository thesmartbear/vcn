@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Tráfico', 'manage.informes.trafico') !!}
@stop

@section('titulo')
    <i class="fa fa-globe fa-fw"></i> Informe Tráfico
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.trafico'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-4">
                        {!! Form::label('proveedores', 'Proveedor') !!}
                        <br>
                        {!! Form::select('proveedores', $proveedores, $valores['proveedores'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-proveedores'))  !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('paises', 'País') !!}
                        <br>
                        {!! Form::select('paises', $paises, $valores['paises'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-paises'))  !!}
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        {!! Form::label('&nbsp;') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}

        </div>
    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Resultados</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$registros)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#registros" aria-controls="registros" role="tab" data-toggle="tab">Registros</a></li>
                    <li role="presentation"><a href="#visitas" aria-controls="visitas" role="tab" data-toggle="tab">Visitas</a></li>
                    <li role="presentation"><a href="#visitas-user" aria-controls="visitas-user" role="tab" data-toggle="tab">Visitas x Usuario</a></li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="registros">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong><i class="fa fa-list fa-fw"></i> Registros {{$valores['desde']}} - {{$valores['hasta']}}</strong>
                            </div>
                            <div class="panel-body">

                                <?php
                                    $cursos = clone $registros;
                                    $totales = clone $registros;

                                    $cursos = $cursos->groupBy('curso_id')->selectRaw('*, sum(registros_home) as tot_registros_home, sum(registros_info) as tot_registros_info')->get();
                                    $totales = $totales->selectRaw('sum(registros_home) as tot_registros_home, sum(registros_info) as tot_registros_info')->first();
                                ?>

                                <table class="table table-bordered no-footer">
                                    <caption>Registros Web Totales</caption>
                                    <thead>
                                        <tr>
                                            <th>Origen</th>
                                            <th>Registros</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Home</td>
                                            <td>{{$totales->tot_registros_home}}</td>
                                        </tr>
                                        <tr>
                                            <td>Info Curso</td>
                                            <td>{{$totales->tot_registros_info}}</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered no-footer">
                                    <caption>Registros Web x Curso</caption>
                                    <thead>
                                        <tr>
                                            <th>Proveedor</th>
                                            <th>Curso</th>
                                            <th>Registros</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($cursos as $curso)
                                        <tr>
                                            <td>{{$curso->curso?$curso->curso->proveedor->name:"-"}}</td>
                                            <td>{{$curso->curso?$curso->curso->name:"Web Home"}}</td>
                                            <td>{{$curso->curso?$curso->tot_registros_info:$curso->tot_registros_home}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <hr>

                                <div class="portlet light bordered">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption">Detalle</div>
                                    </div>
                                    <div class="portlet-body flip-scroll">

                                        <?php
                                            $valores['listado'] = "registros";
                                        ?>

                                        {!! Datatable::table()
                                            ->addColumn([
                                              'fecha'   => 'Fecha',
                                              'curso'   => 'Curso',
                                              'total'   => 'Registros',
                                              'usuarios'    => 'Usuarios'
                                            ])
                                            ->setUrl(route('manage.informes.trafico', $valores))
                                            ->setOptions('iDisplayLength', 100)
                                            ->setOptions(
                                              "columnDefs", array(
                                                // [ "sortable" => false, "targets" => [10] ],
                                                [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                              )
                                            )
                                            ->render()
                                        !!}
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="visitas">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong><i class="fa fa-list fa-fw"></i> Visitas {{$valores['desde']}} - {{$valores['hasta']}}</strong>
                            </div>
                            <div class="panel-body">

                                <?php
                                    $cursos = clone $visitas;
                                    $cursos = $cursos->groupBy('curso_id')->selectRaw('*, sum(visitas) as total')->get();

                                    $cu0 = clone $visitas;
                                    $cu1 = clone $visitas;

                                    $cursos_0 = $cu0->where('user_id',0)->groupBy('curso_id')->selectRaw('*, sum(visitas) as total, sum(visitas_proveedor) as total_proveedor')->get();
                                    $cursos_1 = $cu1->where('user_id','>',0)->groupBy('curso_id')->selectRaw('*, sum(visitas) as total, sum(visitas_proveedor) as total_proveedor')->get();

                                    $tu0 = clone $visitas;
                                    $tu1 = clone $visitas;

                                    $totu0 = $tu0->where('user_id',0);//->selectRaw('sum(visitas) as total')->first();
                                    $totu1 = $tu1->where('user_id','>',0);//->selectRaw('sum(visitas) as total')->first();

                                    $totales_home_0 = clone $totu0;
                                    $totales_curso_0 = clone $totu0;

                                    $totales_home_0 = $totales_home_0->where('curso_id',0)->selectRaw('sum(visitas) as total')->first();
                                    $totales_curso_0 = $totales_curso_0->where('curso_id','>',0)->selectRaw('sum(visitas) as total, sum(visitas_proveedor) as total_proveedor')->first();

                                    $totales_home_1 = clone $totu1;
                                    $totales_curso_1 = clone $totu1;

                                    $totales_home_1 = $totales_home_1->where('curso_id',0)->selectRaw('sum(visitas) as total')->first();
                                    $totales_curso_1 = $totales_curso_1->where('curso_id','>',0)->selectRaw('sum(visitas) as total, sum(visitas_proveedor) as total_proveedor')->first();
                                ?>

                                <table class="table table-bordered no-footer">
                                    <caption>Visitas Totales</caption>
                                    <thead>
                                        <tr>
                                            <th>Origen</th>
                                            <th>Visitantes</th>
                                            <th>Registrados</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Home</td>
                                            <td>{{$totales_home_0->total}}</td>
                                            <td>{{$totales_home_1->total}}</td>
                                        </tr>
                                        <tr>
                                            <td>Curso</td>
                                            <td>{{$totales_curso_0->total}}</td>
                                            <td>{{$totales_curso_1->total}}</td>
                                        </tr>
                                        <tr>
                                            <td>Proveedor</td>
                                            <td>{{$totales_curso_0->total_proveedor}}</td>
                                            <td>{{$totales_curso_1->total_proveedor}}</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table table-bordered no-footer">
                                    <caption>Visitas Web x Curso</caption>
                                    <thead>
                                        <tr>
                                            <th>Proveedor</th>
                                            <th>Curso</th>
                                            <th>Visitantes</th>
                                            <th>Registrados</th>
                                            <th>Proveedor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($cursos as $curso)
                                        <?php
                                            $t0 = null;
                                            $t1 = null;
                                            if($curso->curso->id)
                                            {
                                                $t0 = $cursos_0->where('curso_id',$curso->curso->id)->first();
                                                $t1 = $cursos_1->where('curso_id',$curso->curso->id)->first();
                                            }
                                        ?>
                                        <tr>
                                            <td>{{$curso->curso ? $curso->curso->proveedor->name : "-"}}</td>
                                            <td>{{$curso->curso ? $curso->curso->name : "Web Home"}}</td>
                                            <td>
                                                {{$t0?$t0->total:0}}
                                            </td>
                                            <td>
                                                {{$t1?$t1->total:0}}
                                            </td>
                                            <td>
                                                {{$t1?$t1->total_proveedor:0}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <hr>

                                <div class="portlet light bordered">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption">Detalle</div>
                                    </div>
                                    <div class="portlet-body flip-scroll">

                                        <?php
                                            $valores['listado'] = "visitas";
                                        ?>

                                        {!! Datatable::table()
                                            ->addColumn([
                                              'fecha'   => 'Fecha',
                                              'curso'   => 'Curso',
                                              'tipo'    => 'Tipo',
                                              'visitas' => 'Visitas',
                                              'proveedor' => 'Proveedor',
                                            ])
                                            ->setUrl(route('manage.informes.trafico', $valores))
                                            ->setOptions('iDisplayLength', 100)
                                            ->setOptions(
                                              "columnDefs", array(
                                                // [ "sortable" => false, "targets" => [10] ],
                                                [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                              )
                                            )
                                            ->render()
                                        !!}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="visitas-user">

                        <div class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption">Detalle</div>
                            </div>
                            <div class="portlet-body flip-scroll">

                                <?php
                                    $valores['listado'] = "visitas-user";
                                ?>

                                {!! Datatable::table()
                                    ->addColumn([
                                      'fecha'   => 'Fecha',
                                      'curso'   => 'Curso',
                                      'user'    => 'Visitante',
                                      'visitas' => 'Visitas',
                                      'proveedor' => 'Proveedor',
                                    ])
                                    ->setUrl(route('manage.informes.trafico', $valores))
                                    ->setOptions('iDisplayLength', 100)
                                    ->setOptions(
                                      "columnDefs", array(
                                        // [ "sortable" => false, "targets" => [10] ],
                                        [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                      )
                                    )
                                    ->render()
                                !!}
                            </div>
                        </div>

                    </div>

                </div>

            @endif
        </div>
    </div>

@stop