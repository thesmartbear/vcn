@extends('layouts.manage')

@section('breadcrumb')

@stop

@section('titulo')
    <i class="fa fa-plane fa-fw"></i> Listado API2
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.vuelos-api2'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">

                    <div class="col-md-2">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    </div>

                    <div class="col-md-2">
                    {!! Form::label('anyv', 'Año Vuelo') !!}
                    @include('includes.form_input_cargando',['id'=> 'anyv-cargando'])
                    <br>
                    {!! Form::select('anyv', $anys, $valores['anyv'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-anyv'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'anyv', 'destino'=> 'vuelos'])
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-6">
                    {!! Form::label('vuelos', 'Vuelo') !!}
                    @include('includes.form_input_cargando',['id'=> 'vuelos-cargando'])
                    <br>
                    {!! Form::select('vuelos', $vuelos, $valores['vuelos'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-vuelos'))  !!}
                    </div>

                    <div class="col-md-2">
                    {!! Form::label('agencias', 'Agencia') !!}
                    @include('includes.form_input_cargando',['id'=> 'agencias-cargando'])
                    <br>
                    {!! Form::select('agencias', $agencias, $valores['agencias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-agencias'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}

        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <ul class="nav nav-tabs" role="tablist">
                    @foreach($tabs as $tab)
                        <li role="presentation" class="{{($tab === reset($tabs))?'active':''}}">
                            <a href="#tab-{{$tab}}" aria-controls="tab-{{$tab}}" role="tab" data-toggle="tab">
                            <?php
                                $vuelo = \VCN\Models\Convocatorias\Vuelo::find($tab);
                            ?>
                            {{$vuelo?$vuelo->name:"- VUELO NO EXISTE -"}}
                            </a>
                        </li>
                    @endforeach
                    </ul>

                    <div class="tab-content">
                    @foreach($tabs as $tab)
                        <div role="tabpanel" class="tab-pane fade in {{($tab === reset($tabs))?'active':''}}" id="tab-{{$tab}}">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <strong><i class="fa fa-list fa-fw"></i> Listado {{$valores['desdes']}} - {{$valores['hastas']}}</strong>
                                </div>
                                <div class="panel-body">

                                    <?php
                                        $valores['vuelos'] = $tab;
                                    ?>

                                    {!! Datatable::table()
                                        ->addColumn([
                                            'last_name'      => 'last_name',
                                            'first_name'      => 'first_name',
                                            'title'       => 'title',
                                            'ptc'  => 'ptc',
                                            'gender'      => 'gender',
                                            'date_of_birth'        => 'date_of_birth',
                                            'passport_last_name'   => 'passport_last_name',
                                            'passport_first_name'    => 'passport_first_name',
                                            'passport_number'     => 'passport_number',
                                            'passport_nationality'        => 'passport_nationality',
                                            'passport_issue_country'    => 'passport_issue_country',
                                            'passport_expiry_date'    => 'passport_expiry_date',
                                            'visa_number'    => 'visa_number',
                                            'visa_type'    => 'visa_type',
                                            'visa_issue_date'    => 'visa_issue_date',
                                            'place_of_birth'    => 'place_of_birth',
                                            'visa_place_of_issue'    => 'visa_place_of_issue',
                                            'visa_country_of_application'    => 'visa_country_of_application',
                                            'address_type'    => 'address_type',
                                            'address_country'    => 'address_country',
                                            'address_details'    => 'address_details',
                                            'address_city'    => 'address_city',
                                            'address_state'    => 'address_state',
                                            'address_zip_code'    => 'address_zip_code',
                                            'fqtv_carrier_1'    => 'fqtv_carrier_1',
                                            'fqtv_number_1'    => 'fqtv_number_1',
                                            'fqtv_carrier_2'    => 'fqtv_carrier_2',
                                            'fqtv_number_2'    => 'fqtv_number_2',
                                            'fqtv_carrier_3'    => 'fqtv_carrier_3',
                                            'fqtv_number_3'    => 'fqtv_number_3',
                                            


                                        ])
                                        ->setUrl( route('manage.informes.vuelos-api2', $valores) )
                                        ->setOptions('iDisplayLength', 100)
                                        ->render() !!}


                                </div>
                            </div>

                        </div>
                    @endforeach
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop