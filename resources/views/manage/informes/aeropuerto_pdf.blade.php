<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>

        {!! Html::style('assets/css/pdf.css') !!}
        {!! Html::style('assets/css/bootstrap.css') !!}

    </head>
    <body>
    @foreach(ConfigHelper::plataformas(false) as $p=>$plataforma)
    <div class="page"></div>
        <?php
            $valores['plataformas'] = $p;
        ?>

        <h1>Listado Aeropuerto [{{$plataforma}}]</h1>
        <i>{{Carbon::now()->format("d/m/Y H:i")}}</i>

        <hr>
        <h4>Filtros:</h4>
        Categoria: {{$valores_txt['categorias']}}
        <br>
        Centro: {{$valores_txt['centros']}}, Curso: {{$valores_txt['cursos']}}

        <hr>


        @foreach($tabs as $ktab=>$vtab)

            <?php
                $convo = \VCN\Models\Convocatorias\Cerrada::find($ktab);
                $valores['convocatorias'] = $ktab;
            ?>

            <h3>Convocatoria: {{ $convo?$convo->name:"-" }}</h3>

            @foreach($tabs[$ktab] as $c)
                <?php
                    $vuelo = \VCN\Models\Convocatorias\Vuelo::find($c);

                    $valores['vuelos'] = $c;
                    $bookings = \VCN\Models\Bookings\Booking::listadoFiltros($valores, true);
                ?>

                <h4>Vuelo: {{$vuelo?$vuelo->name:"-"}} :: <i>Localizador: {{$vuelo?$vuelo->localizador:"-"}}</i></h4>

                <div class="page">
                <table class="table table-bordered no-footer">
                    <thead>
                        <tr>
                            <th>Apellido1</th>
                            <th>Apellido2</th>
                            <th>Nombre</th>
                            <th>Poblacion</th>
                            <th>Convocatoria</th>
                            <th>Vuelo</th>
                            <th>Localizador</th>
                            <th>Teléfono</th>
                            <th>Móvil</th>
                            <th>Tutor1 Móvil</th>
                            <th>Tutor2 Móvil</th>
                            <th>Empresa</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($bookings->orderBy('course_start_date')->get()->sortBy('viajero.lastname') as $model)
                        <tr>
                            <td>{{$model->viajero->lastname}}</td>
                            <td>{{$model->viajero->lastname2}}</td>
                            <td>{{$model->viajero->name}}</td>
                            <td>{{$model->viajero->datos->ciudad}}</td>
                            <td>{{$model->convocatoria->name}}</td>
                            <td>{{$model->vuelo?$model->vuelo->name:"-"}}</td>
                            <td>{{$model->vuelo?$model->vuelo->localizador:"-"}}</td>
                            <td>&nbsp;{{$model->viajero->telefono}}&nbsp;</td>
                            <td>&nbsp;{{$model->viajero->movil}}&nbsp;</td>
                            <td>&nbsp;{{$model->viajero->tutor1?$model->viajero->tutor1->movil:"-"}}&nbsp;</td>
                            <td>&nbsp;{{$model->viajero->tutor2?$model->viajero->tutor2->movil:"-"}}&nbsp;</td>
                            <td>&nbsp;{{ConfigHelper::plataforma($model->viajero->plataforma)}}&nbsp;</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                </div>

            @endforeach

            <hr>

        @endforeach

    
    @endforeach
    </body>
</html>