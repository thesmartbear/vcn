@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Informe Facturación 347', 'manage.informes.facturacion-347') !!}
@stop

@section('titulo')
    <i class="fa fa-file-text fa-fw"></i> Informe Facturación 347
@stop

@section('container')

  @if(!ConfigHelper::config('facturas'))
    <div class="content">
        <div class="alert alert-info" role="alert">
            Informe no activo
        </div>
    </div>
  @else

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-file-text-o fa-fw"></i> Facturas
                @if($desde)
                   :: Entre {{$desde}} y {{$hasta}}
                @endif
        </div>
        <div class="panel-body">

            {!! Form::open(array('method' => 'GET', 'url' => route('manage.informes.facturacion-347'), 'role' => 'form', 'class' => '')) !!}
            <div class="row">
                <div class="col-md-2">Filtro entre fechas (=fecha factura):</div>
                <div class="col-md-3">
                    @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=> $desde])
                </div>
                <div class="col-md-3">
                    @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=> $hasta])
                </div>
                <div class="col-md-3">
                    <button type='submit' class="btn btn-info">Filtrar</button>
                </div>
            </div>
            {!! Form::close() !!}

            <hr>

            @if($desde && $hasta)

                {!! Datatable::table()
                    ->addColumn([
                      'nif'     => 'DNI/NIF',
                      'razonsocial' => 'Razón Social',
                      'cp'      => 'CP',
                      'provincia' => 'Provincia',
                      'pais'    => 'Pais',
                      'importe1t' => 'Importe 1T',
                      'importe2t' => 'Importe 2T',
                      'importe3t' => 'Importe 3T',
                      'importe4t' => 'Importe 4T',
                      'importet'  => 'Importe facturado Total (=total de todas las facturas con este DNI)',
                      'cuenta'    => 'Facturas',
                    ])
                    ->setUrl(route('manage.informes.facturacion-347',['desde'=>$desde, 'hasta'=> $hasta]))
                    ->setOptions('iDisplayLength', 100)
                    //->setOptions('order', array([1,'asc']))
                    ->setOptions(
                      "columnDefs", array(
                        //[ "sortable" => false, "targets" => [0] ],
                        //[ "targets" => [4], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                      )
                    )
                    ->render() !!}

            @else
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @endif

        </div>
    </div>

  @endif

@stop