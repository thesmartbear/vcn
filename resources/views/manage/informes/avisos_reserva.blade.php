@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.avisos') !!}
@stop

@section('titulo')
    <i class="fa fa-users fa-fw"></i> Avisos Pre-Reserva
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-users fa-fw"></i> Informe

        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                    'viajero'   => 'Viajero',
                    'plataforma'   => 'Plataforma',
                    'booking'   => 'Booking',
                    'convocatoria'   => 'Convocatoria',
                    'curso_ini' => 'Curso Fecha',
                    'reserva'   => 'F.Reserva',
                    'aviso1'    => 'Aviso Viajero y Tutores',
                    'aviso2'    => 'Aviso Usuario',
                    'caduca'    => 'Caduca',
                    'asignado'    => 'Asignado a',
                    'opciones'  => '',
                ])
                ->setUrl(route('manage.informes.avisos.reserva'))
                // ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "columnDefs", array(
                    [ "sortable" => false, "targets" => [10] ],
                    [ "targets" => [4], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
                ->render() !!}

        </div>
    </div>


@stop