@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Orla', 'manage.informes.orla') !!}
@stop

@section('titulo')
    <i class="fa fa-users fa-fw"></i> Orla
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.orla'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">

                    <div class="col-md-4">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-6">
                    {!! Form::label('centros', 'Centro') !!}
                    @include('includes.form_input_cargando',['id'=> 'centros-cargando'])
                    <br>
                    {!! Form::select('centros', $centros, $valores['centros'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-centros'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'cursos'])
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'convocatorias'])
                    </div>

                    <div class="col-md-6">
                    {!! Form::label('cursos', 'Curso') !!}
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])
                    <br>
                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-cursos'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'cursos', 'destino'=> 'convocatorias'])
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-4">
                    {!! Form::label('convocatorias', 'Convocatoria') !!}
                    @include('includes.form_input_cargando',['id'=> 'convocatorias-cargando'])
                    <br>
                    {!! Form::select('convocatorias', $convocatorias, $valores['convocatorias'], array('class'=>'select2 col-md-12', 'data-style'=>'red', 'id'=>'filtro-convocatorias'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}
        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
                    @if($results)
                        &nbsp;<a target='_blank' href="{{route('manage.informes.orla.pdf',$valores)}}" class='btn btn-danger'><i class='fa fa-file-pdf-o'></i> PDF</a>
                    @endif
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else


                    <ul class="nav nav-tabs" role="tablist">
                        @foreach($tabs as $ktab=>$vtab)
                            <li role="presentation" class="{{($vtab === reset($tabs))?'active':''}}">
                                <a href="#tab-{{$ktab}}" aria-controls="tab-{{$ktab}}" role="tab" data-toggle="tab">
                                {{$vtab}}
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <div class="tab-content">
                        @foreach($tabs as $ktab=>$vtab)
                            <div role="tabpanel" class="tab-pane fade in {{($vtab === reset($tabs))?'active':''}}" id="tab-{{$ktab}}">
                                
                                <?php
                                    $convo_t = $valores['tipoc'];
                                    $convo_f = $valores['filtroc'];
                                    $convo_id = $ktab;

                                    $list = clone $bookings;
                                    $list = $list->where($convo_f, $convo_id);

                                    $viajeros = [];
                                    foreach($list->get() as $booking)
                                    {
                                        $viajero = $booking->viajero;

                                        $v['nom'] = $viajero->full_name;
                                        $v['apellidos'] = "$viajero->lastname $viajero->lastname2";
                                        $v['foto'] = $viajero->avatar ?: $viajero->foto;
                                        
                                        $viajeros[] = $v;
                                    }

                                    asort($viajeros);
                                ?>

                                <div id="orla_{{$convo_id}}" class="informe-orla">
                                    <ul>
                                        @foreach($viajeros as $viajero)
                                        <li class='col-md-3'>
                                            <div class="orla-box">
                                                <div class="orla-foto">
                                                    <img src="{{$viajero['foto']}}" class="foto">
                                                    {{$viajero['nom']}}
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                        @endforeach
                    </div>


                @endif

            @endif

        </div>

    </div>

@stop