@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.reunion') !!}
@stop

@section('titulo')
    <i class="fa fa-group fa-fw"></i> Info Reunión
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.reunion'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'proveedores'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-2">
                    {!! Form::label('oficinas', 'Oficina') !!}
                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-oficinas'))  !!}
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-3">
                    {!! Form::label('tipoc', 'Tipo Convocatoria') !!}
                    @include('includes.form_input_cargando',['id'=> 'tipoc-cargando'])
                    <br>
                    {!! Form::select('tipoc', ConfigHelper::getConvocatoriaTipo(), $valores['tipoc'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-tipoc'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-3"></div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                    {!! Form::label('proveedores', 'Proveedor') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'proveedores-cargando'])

                    {!! Form::select('proveedores', $proveedores, $valores['proveedores'], array('class'=>'select2', 'data-style'=>'purple', 'id'=>'filtro-proveedores'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('centros', 'Centro') !!}
                    @include('includes.form_input_cargando',['id'=> 'centros-cargando'])
                    <br>
                    {!! Form::select('centros', $centros, $valores['centros'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-centros'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'cursos'])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-5">
                    {!! Form::label('cursos', 'Curso') !!}
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])

                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                    {!! Form::label('any1', 'Año Booking') !!}
                    {!! Form::select('any1', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any1'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=> $valores['desdes']])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=> $valores['hastas']])
                    </div>

                    <div class="col-md-2 col-md-offset-3">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}
        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">

            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <ul class="nav nav-tabs" role="tablist">
                    @foreach($tabs as $tab)
                        <li role="presentation" class="{{($tab === reset($tabs))?'active':''}}">
                            <a href="#tab-{{$tab}}" aria-controls="tab-{{$tab}}" role="tab" data-toggle="tab">
                            {{\VCN\Models\Cursos\Curso::find($tab)->name}}
                            </a>
                        </li>
                    @endforeach
                    </ul>

                    <div class="tab-content">
                    @foreach($tabs as $tab)
                        <div role="tabpanel" class="tab-pane fade in {{($tab === reset($tabs))?'active':''}}" id="tab-{{$tab}}">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-globe fa-fw"></i> Listado
                                </div>
                                <div class="panel-body">

                                    <?php
                                        $valores['cursos'] = $tab;
                                    ?>

                                    {!! Datatable::table()
                                        ->addColumn([
                                          'apellido1' => 'Apellido1',
                                          'apellido2' => 'Apellido2',
                                          'nombre' => 'Nombre Viajero',
                                          'ciudad' => 'Ciudad',
                                          'provincia' => 'Provincia',
                                          'convocatoria' => 'Convocatoria',
                                          'plataforma' => 'Plataforma',
                                          'oficina' => 'Oficina',

                                        ])
                                        ->setUrl(route('manage.informes.reunion',$valores))
                                        ->setOptions('iDisplayLength', 100)
                                        ->setOptions(
                                          "columnDefs", array(
                                            // [ "sortable" => false, "targets" => [0,1,2,3,7,8,9,10,11] ],
                                            // [ "targets" => [2,4,5,6], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                          )
                                        )
                                        ->render()
                                    !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>

                @endif

           @endif

        </div>

    </div>

@stop