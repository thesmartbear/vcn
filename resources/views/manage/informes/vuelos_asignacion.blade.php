@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.vuelos-asignacion') !!}
@stop

@section('titulo')
    <i class="fa fa-plane fa-fw"></i> Asignación de Vuelos
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.vuelos-asignacion'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'proveedores'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-2">
                    {!! Form::label('oficinas', 'Oficina') !!}
                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-oficinas'))  !!}
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-2">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('proveedores', 'Proveedor') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'proveedores-cargando'])

                    {!! Form::select('proveedores', $proveedores, $valores['proveedores'], array('class'=>'select2', 'data-style'=>'purple', 'id'=>'filtro-proveedores'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'cursos'])
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-4">
                    {!! Form::label('centros', 'Centro') !!}
                    @include('includes.form_input_cargando',['id'=> 'centros-cargando'])
                    <br>
                    {!! Form::select('centros', $centros, $valores['centros'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-centros'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'cursos'])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-5">
                    {!! Form::label('cursos', 'Curso') !!}
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])

                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-4">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}

        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><i class="fa fa-list fa-fw"></i> Listado {{$valores['desdes']}} - {{$valores['hastas']}}</strong>
                        </div>
                        <div class="panel-body">

                            {!! Datatable::table()
                                ->addColumn([
                                    'viajero'       => 'Viajero',
                                    'curso'         => 'Curso',
                                    'convocatoria'  => 'Convocatoria',
                                    'vuelo'         => 'Vuelo',
                                    'aporte'        => 'Aporte',
                                    'vuelo_salida'  => 'Salida',
                                    'vuelo_llegada' => 'Llegada',
                                    'aporte_extra'  => 'Extra',

                                ])
                                ->setUrl( route('manage.informes.vuelos-asignacion', $valores) )
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                  "columnDefs", array(
                                    [ "targets" => [5,6], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                  )
                                )
                                ->render() !!}


                        </div>
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop