@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Niveles CIC', 'manage.informes.cic-niveles') !!}
@stop

@section('titulo')
    <i class="fa fa-list fa-fw"></i> Niveles CIC
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.cic-niveles'), 'method'=> 'GET', 'class' => 'form']) !!}


                <div class="form-group row">

                    <div class="col-md-3">
                    {!! Form::label('tipoc', 'Tipo Convocatoria') !!}
                    @include('includes.form_input_cargando',['id'=> 'tipoc-cargando'])
                    <br>
                    {!! Form::select('tipoc', ConfigHelper::getConvocatoriaTipo(), $valores['tipoc'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-tipoc'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'centros'])
                    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'cursos'])
                    @include('includes.script_filtros_multi', ['filtro'=> 'tipoc', 'destino'=> 'convocatorias'])
                    </div>

                    <div class="col-md-4">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-5">
                    {!! Form::label('cursos', 'Curso') !!}
                    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])

                    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'cursos', 'destino'=> 'convocatorias'])
                    </div>

                    <div class="col-md-6">
                    {!! Form::label('convocatorias', 'Convocatoria') !!}
                    @include('includes.form_input_cargando',['id'=> 'convocatorias-cargando'])
                    <br>
                    {!! Form::select('convocatorias', $convocatorias, $valores['convocatorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-convocatorias'))  !!}
                    </div>

                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-2">
                        {!! Form::label('&nbsp;') !!}
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}

        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-globe fa-fw"></i> Listado
                        </div>
                        <div class="panel-body">

                            {!! Datatable::table()
                                ->addColumn([
                                    'convocatoria'  => 'Convocatoria',
                                    'apellidos'     => 'Cognoms',
                                    'nombre'        => 'Nom',
                                    'sexo'          => 'Sexe',
                                    'fechanac'      => 'DOB',
                                    'trinity_exam' => 'Trinity',
                                    'nivel_trinity' => 'Trinity Assolit',
                                    'trinity_any'   => 'Any Trinity',
                                    'nivel_trinity_camp' => 'Trinity Assignat',
                                    'nivel_cic' => 'Nivell CIC actual',
                                    'nivel_cic_camp' => 'Nivell CIC Camp',
                                    'thau'          => 'Escola',
                                    'escuela_curso' => 'Curs Acadèmic'

                                ])
                                ->setUrl( route('manage.informes.cic-niveles', $valores) )
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                    "columnDefs", array(
                                        [ "targets" => [4], "render"=> "function(data, type, full) {return moment(data).isValid()?moment(data).format('DD/MM/YYYY'):'-';}" ],
                                    )
                                )
                                ->render() !!}

                        </div>
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop