@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Bookings Origen', 'manage.informes.bookings-origen') !!}
@stop

@section('titulo')
    <i class="fa fa-ticket fa-fw"></i> Bookings Origen
@stop


@section('container')

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-filter"></i>
                <span class="caption-subject bold">Filtros </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::open(['route' => array('manage.informes.bookings-origen'), 'method'=> 'GET', 'class' => 'form']) !!}

                <div class="form-group row">
                    <div class="col-md-2">
                    {!! Form::label('plataformas', 'Plataforma') !!}
                    <br>
                    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                    @include('includes.script_filtros_multi', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                    </div>

                    <div class="col-md-3">
                    {!! Form::label('oficinas', 'Oficina') !!}<br>
                    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-oficinas', 'name'=> 'oficinas[]'))  !!}
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        {!! Form::label('categorias', 'Categoría') !!}
                        @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                        <br>
                        {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-categorias', 'name'=> 'categorias[]'))  !!}
                        @include('includes.script_filtros_multi', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('subcategorias', 'SubCategoría') !!}
                        @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                        <br>
                        {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-subcategorias', 'name'=> 'subcategorias[]'))  !!}
                    </div>
                </div>

                <hr>

                <div class="form-group row">

                    <div class="col-md-1">
                        {!! Form::label('any','Año') !!}
                        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2 col-md-12', 'data-style'=>'green', 'id'=>'filtro-anys'))  !!}
                    </div>
                    <div class="col-md-1">-o-</div>

                    <div class="col-md-2">
                        {!! Form::label('desde','Desde') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('hasta','Hasta') !!}
                        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
                    </div>

                    <div class="col-md-2 col-md-offset-2">
                        {!! Form::label('(Fechas: Primer pago)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::label('(Fechas: Inicio Booking)') !!}<br>
                        {!! Form::submit('Consultar', array( 'name'=> 'filtro2', 'class' => 'btn btn-info')) !!}
                    </div>

                </div>

            {!! Form::close() !!}
        </div>

    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-ticket"></i>
                <span class="caption-subject bold">Bookings</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(!$listado)
                <div class="content">
                    <div class="alert alert-info" role="alert">
                        Seleccione los filtros correspondientes
                    </div>
                </div>
            @else

                @if(!$results)
                    <br>
                    <div class="content">
                        <div class="alert alert-warning" role="alert">
                            Sin Resultados
                        </div>
                    </div>
                @else

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><i class="fa fa-list fa-fw"></i> Listado [{{$tipof}}: {{$fdesde}} - {{$fhasta}}]</strong>
                        </div>
                        <div class="panel-body">

                            {!! Datatable::table()
                                ->addColumn([
                                  'viajero'=> 'Viajero',
                                  'curso'=> 'Curso',
                                  'semanas'=> 'Semanas',
                                  'total_curso'=> 'Importe curso',
                                  'total'=> 'Importe total',
                                  'origen'=> 'Origen',
                                  'suborigen'=> 'Suborigen',
                                  'suborigen_det'=> 'Detalle Suborigen',
                                  'fecha_p1'=> 'Fecha (=primer pago)',
                                ])
                                ->setUrl(route('manage.informes.bookings-origen',$valores))
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                  "columnDefs", array(
                                    //[ "sortable" => false, "targets" => [5] ],
                                    [ "targets" => [8], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                  )
                                )
                                ->render()
                            !!}
                        </div>
                    </div>

                @endif

           @endif


        </div>

    </div>

@stop