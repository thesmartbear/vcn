@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.informes.ruta', 'Análisis solicitudes', 'manage.informes.anal-solicitudes') !!}
@stop

@section('titulo')
    <i class="fa fa-pie-chart fa-fw"></i> Análisis solicitudes
@stop

@section('container')

<?php
$user = auth()->user();
?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-filter"></i>
            <span class="caption-subject bold">Filtros </span>
        </div>
    </div>
    <div class="portlet-body">

        {!! Form::open(['route' => array('manage.informes.anal-solicitudes'), 'method'=> 'GET', 'class' => 'form']) !!}

            <div class="form-group row">
                @if(auth()->user()->isFullAdmin())
                <div class="col-md-2">
                {!! Form::label('plataformas', 'Plataforma') !!}
                <br>
                {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
                @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
                @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'users'])
                </div>
                @endif

                @if( auth()->user()->filtro_oficinas )
                <div class="col-md-3">
                {!! Form::label('oficinas', 'Oficina') !!}
                <br>
                @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])

                {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'multiple'=>'multiple', 'id'=>'filtro-oficinas', 'name'=> 'oficinas[]'))  !!}

                </div>
                @endif

                <div class="col-md-3">
                {!! Form::label('usuarios', 'Usuarios') !!}
                <br>
                @include('includes.form_input_cargando',['id'=> 'users-cargando'])

                {!! Form::select('users', $users, $valores['users'], array('class'=>'select2', 'multiple'=>'multiple', 'id'=>'filtro-users', 'name'=> 'users[]'))  !!}
                </div>

                <div class="col-md-">
                    @include('includes.form_checkbox', [ 'campo'=> 'agrupados', 'texto'=> 'Usuarios agrupados', 'valor'=> $valores['agrupados'] ])
                </div>

            </div>

            <div class="form-group row">

                <div class="col-md-2">
                    {!! Form::label('categorias', 'Categoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
                    <br>
                    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
                    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
                    </div>
    
                    <div class="col-md-2">
                    {!! Form::label('subcategorias', 'Subcategoría') !!}
                    @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
                    <br>
                    {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
                </div>

            </div>

            <div class="form-group row">

                <div class="col-md-2">
                    {!! Form::label('desde','Desde') !!}
                    @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=> $valores['desde']])
                </div>
                <div class="col-md-2">
                    {!! Form::label('hasta','Hasta') !!}
                    @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=> $valores['hasta']])
                </div>

                <div class="col-md-1 col-md-offset-3">
                    {!! Form::label('Días') !!}<br>
                    {!! Form::submit('Consultar', array( 'name'=> 'filtro_fechas', 'class' => 'btn btn-info')) !!}
                </div>

                <div class="col-md-1">
                    {!! Form::label('Semanas') !!}<br>
                    {!! Form::submit('Consultar', array( 'name'=> 'filtro_semanas', 'class' => 'btn btn-info')) !!}
                </div>

                <div class="col-md-1">
                    {!! Form::label('Meses') !!}<br>
                    {!! Form::submit('Consultar', array( 'name'=> 'filtro_meses', 'class' => 'btn btn-info')) !!}
                </div>

                <div class="col-md-1">
                    {!! Form::label('Años') !!}<br>
                    {!! Form::submit('Consultar', array( 'name'=> 'filtro_anys', 'class' => 'btn btn-info')) !!}
                </div>

            </div>

        {!! Form::close() !!}

    </div>

</div>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-ticket"></i>
            <span class="caption-subject bold">ANÁLISIS [{{$valores['desde']}} - {{$valores['hasta']}}]</span>
        </div>
    </div>

    <div class="portlet-body">

    @if(!$resultados)
        <div class="content">
            <div class="alert alert-info" role="alert">
                Seleccione los filtros correspondientes
            </div>
        </div>
    @else

        <ul class="nav nav-tabs" role="tablist">
            @foreach($usuarios as $user_id=>$user)
                <li role="presentation" class="{{($user === reset($usuarios))?'active':''}}">
                    <a href="#tab-{{$user_id}}" aria-controls="tab-{{$user_id}}" role="tab" data-toggle="tab">
                    {{$user}}
                    </a>
                </li>
            @endforeach
        </ul>

        <div class="tab-content">
            @php
                $totalS0 = 0;
                $totalS1 = 0;
                $totalS2 = 0;
                $totalB = 0;
            @endphp
        @foreach($usuarios as $user_id=>$user)

            @php
                $solicitudesUser = clone $solicitudes;
                $solicitudesPrevUser = clone $solicitudesPrev;
                $solicitudesDescUser = clone $solicitudesDesc;
                if($user_id)
                {
                    $solicitudesUser = $solicitudesUser->where('user_id',$user_id);
                    $solicitudesPrevUser = $solicitudesPrevUser->where('user_id',$user_id);
                    $solicitudesDescUser = $solicitudesDescUser->where('user_id',$user_id);
                }
            @endphp

            <div role="tabpanel" class="tab-pane fade in {{($user === reset($usuarios)) ? 'active' : ''}}" id="tab-{{$user_id}}">

                <div class="informe-tabla"><div>

                <table class="table table-bordered table-striped table-condensed flip-content">
                    <caption><h3>Solicitudes</h3></caption>
                    <thead class="flip-content">
                        <tr>
                            <th class='td-fijo'>Fecha</th>
                            @foreach($fechas_txt as $fdm)
                                <th colspan="2">{{$fdm}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>

                        @php
                            $itemPre = clone $solicitudesPrevUser;
                            $cuentaPre = $itemPre->count() ? $itemPre->sum('cuenta') : 0;
                            $totalS0 += $cuentaPre;
                        @endphp
                        
                        <tr>
                            <td class='td-fijo'>Solicitudes (Totales/Descartadas) (PREV.:{{$cuentaPre}})</td>
                            @foreach($fechas_txt as $idia => $fdm)
                                
                                <?php
                                    $item = clone $solicitudesUser;
                                    $item = $item->where('fecha_grup',$fechas[$idia]);
                                    $cuenta = $item->count() ? $item->sum('cuenta') : 0;
                                    
                                    $totalS1 += $cuenta;
                                    $cuenta += $cuentaPre;

                                    $itemD = $solicitudesDescUser->where('fecha_grup',$fechas[$idia]);
                                    $cuentaDesc = $itemD->count() ? $itemD->sum('cuenta') : 0;

                                    $totalS2 += $cuentaDesc;
                                ?>
                                <td>{{$cuenta}}</td>
                                <td>{{$cuentaDesc}}</td>

                            @endforeach
                        </tr>
                    
                    </tbody>
                </table>
                
                <hr>

                <table class="table table-bordered table-striped table-condensed flip-content">
                    <caption><h3>Bookings</h3></caption>
                    <thead class="flip-content">
                        <tr>
                            <th class='td-fijo'>Fecha</th>
                            @foreach($fechas_txt as $fdm)
                                <th>{{$fdm}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                        <td class='td-fijo'>Bookings</td>
                        @foreach($fechas_txt as $idia => $fdm)
                            <?php
                                $item = clone $bookings;
                                $item = $item->where('fecha_grup',$fechas[$idia]);
                                $cuentaB = $item->count() ? $item->sum('cuenta') : 0;

                                $totalB += $cuentaB;
                            ?>
                            <td>{{$cuentaB}}</td>
                        @endforeach
                        </tr>
                    
                    </tbody>
                </table>

                </div></div>

                Total Solicitudes: {{$totalS0}}[Previas] + {{$totalS1}} | {{$totalS2}}[Descartadas]
                <br>
                Total Bookings: {{$totalB}}
                
            </div>
        @endforeach
        </div>
    
    @endif

    </div>
</div>

@stop