
<div class="portlet light tabs">

    <div class="portlet-body">

            {!! Datatable::table()
                ->addColumn([
                  'fecha_reserva'   => 'Fecha Booking',
                  'viajero'         => 'Viajero',
                  'convocatoria'    => 'Convocatoria',
                  'curso_fecha'     => 'Fecha Inicio',
                  'total'           => "Importe Total (". ConfigHelper::moneda() .")",
                  'pagado'          => "Pagado (". ConfigHelper::moneda() .")",
                  'pendiente'       => "Pendiente (". ConfigHelper::moneda() .")",
                  'oficina'         => 'Oficina',
                  'options' => ''

                ])
                ->setUrl( route('manage.informes.facturacion', [$saldo,$user_id,$oficina_id]) )
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "columnDefs", array(
                    [ "sortable" => false, "targets" => [8] ],
                    [ "targets" => [0,3], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                  )
                )
                ->render() !!}

    </div>

</div>