@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-globe fa-fw"></i> Nacionalidades
                <span class="pull-right"><a href="{{ route('manage.nacionalidades.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Nacionalidad</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Nombre',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.nacionalidades.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [1] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop