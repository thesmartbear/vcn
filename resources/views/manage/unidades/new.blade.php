@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Nueva Unidad Extra
            </div>
            <div class="panel-body">


                {!! Form::open(array('method' => 'POST', 'url' => route('manage.unidades.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Unidad'])
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'configuracion', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop