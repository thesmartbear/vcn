@extends('layouts.manage')


@section('container')

<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Agencia</a></li>

    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
</ul>

<div class="tab-content">

    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Unidad Extra :: {{$ficha->name}}
            </div>
            <div class="panel-body">


                {!! Form::model($ficha, array('route' => array('manage.unidades.ficha', $ficha->id))) !!}

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Unidad Extra'])
                </div>

                @include('includes.form_submit', [ 'permiso'=> 'configuracion', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

    </div>

    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

        @include('includes.traduccion-tab',
                ['modelo'=> 'ExtraUnidad',
                'campos_text'=> [ ['name'=> 'Nombre'], ],
                'campos_textarea'=> []
            ])

    </div>

</div>

@stop