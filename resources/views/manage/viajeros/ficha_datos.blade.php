@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.viajeros.ficha.datos', $viajero) !!}
@stop


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-database fa-fw"></i> Otros Datos ::
        Viajero :: <a href="{{route('manage.viajeros.ficha', $viajero->id)}}">{{$viajero->full_name}}</a>
    </div>
    <div class="panel-body">

        {!! Form::model($ficha, array('id'=>'frmValidar', 'route' => array('manage.viajeros.ficha.datos', $viajero->id))) !!}

            {!! Form::hidden('viajero_id',$viajero->id) !!}

            <div class="form-group row">
                <div class="col-md-5">
                    @include('includes.form_select', [ 'campo'=> 'nacionalidad', 'texto'=> 'Nacionalidad', 'select'=> [""=>""] + \VCN\Models\Nacionalidad::pluck('name','name')->toArray()])
                </div>
                <div class="col-md-2">
                    @include('includes.form_select', [ 'campo'=> 'tipodoc', 'texto'=> "Tipo", 'select'=> ConfigHelper::getTipoDoc() ])
                </div>
                <div class="col-md-5">
                    @include('includes.form_input_text', [ 'campo'=> 'documento', 'texto'=> 'DNI'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    @include('includes.form_input_text', [ 'campo'=> 'pasaporte', 'texto'=> 'Pasaporte'])
                </div>
                <div class="col-md-3">
                    @include('includes.form_select_pais', [ 'campo'=> 'pasaporte_pais', 'texto'=> 'País pasaporte'])
                </div>
                <div class="col-md-3">
                    @include('includes.form_input_datetime', [ 'campo'=> 'pasaporte_emision', 'texto'=> 'F.Emisión pasaporte'])
                </div>
                <div class="col-md-3">
                    @include('includes.form_input_datetime', [ 'campo'=> 'pasaporte_caduca', 'texto'=> 'F.Caducidad pasaporte'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_select', [ 'campo'=> 'tipovia', 'texto'=> 'Tipo', 'select'=> ConfigHelper::getTipoVia()])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'direccion', 'texto'=> 'Dirección'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'ciudad', 'texto'=> 'Ciudad'])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'cp', 'texto'=> 'CP'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    @include('includes.form_select', [ 'campo'=> 'provincia_id', 'texto'=> 'Provincia', 'select'=>[""=>""]+ \VCN\Models\Provincia::pluck('name','id')->toArray()])
                </div>
                <div class="col-md-6">
                    @include('includes.form_select', [ 'campo'=> 'pais_id', 'texto'=> 'País', 'select'=>[""=>""]+ \VCN\Models\Pais::orderBy('name')->pluck('name','id')->toArray()])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-8">
                    @include('includes.form_input_text', [ 'campo'=> 'telefonos', 'texto'=> 'Otros teléfonos'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_select', [ 'campo'=> 'idioma', 'texto'=> 'Idioma', 'select'=> ConfigHelper::getIdioma()])
                </div>
                <div class="col-md-4">
                    @include('includes.form_select', [ 'campo'=> 'idioma_nivel', 'texto'=> 'Nivel de idioma', 'select'=> ConfigHelper::getIdiomaNivel()])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'titulacion', 'texto'=> 'Titulación/Estudios'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'escuela', 'texto'=> 'Escuela'])
                </div>
                <div class="col-md-2">
                    @include('includes.form_select', [ 'campo'=> 'escuela_curso', 'texto'=> 'Curso académico actual', 'select'=> ConfigHelper::getEscuelaCurso()])
                </div>
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'ingles', 'texto'=> 'Inglés Academia'])
                </div>
                <div id="ingles_academia_div" class="col-md-4" style="display:none;">
                    @include('includes.form_input_text', [ 'campo'=> 'ingles_academia', 'texto'=> 'Academia'])
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    <h4 class="form-section">Hermanos:</h4>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'h1_nom', 'texto'=> 'Hermano 1'])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'h1_fnac', 'texto'=> 'Hermano 1 Fecha Nac.'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'h2_nom', 'texto'=> 'Hermano 2'])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'h2_fnac', 'texto'=> 'Hermano 2 Fecha Nac.'])
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'h3_nom', 'texto'=> 'Hermano 3'])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'h3_fnac', 'texto'=> 'Hermano 3 Fecha Nac.'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'h4_nom', 'texto'=> 'Hermano 4'])
                </div>
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'h4_fnac', 'texto'=> 'Hermano 4 Fecha Nac.',])
                </div>
            </div>

            <hr>

            @if($viajero->es_menor)
                {{-- -== es menor ==- --}}
            @endif

            <br>
            <div class="input-group">
                <span class="input-group-addon">Alergia o Enfermedad:</span>
                <div class="pull-left form-radio">
                    <label class="radio-inline">
                        {!! Form::radio('alergias_bool', '1', ($ficha->alergias!="")) !!} SI
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('alergias_bool', '0', ($ficha->alergias=="")) !!} NO
                    </label>
                </div>

                <div id="alergias_div" style="display:none;">
                    <br>
                    @include('includes.form_textarea', [ 'campo'=> 'alergias' ])
                </div>
                <span class="help-block">{{ $errors->first('alergias') }}</span>
            </div>
            <br>
            <div class="input-group">
                <span class="input-group-addon">Medicación, tratamiento o dieta:</span>
                <div class="pull-left form-radio">
                    <label class="radio-inline">
                        {!! Form::radio('medicacion_bool', '1', ($ficha->medicacion!="")) !!} SI
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('medicacion_bool', '0', ($ficha->medicacion=="")) !!} NO
                    </label>
                </div>

                <div id="medicacion_div" style="display:none;">
                    <br>
                    @include('includes.form_textarea', [ 'campo'=> 'medicacion' ])
                </div>
                <span class="help-block">{{ $errors->first('medicacion') }}</span>
            </div>
            <br>

            <div class="form-group">
                @include('includes.form_textarea', [ 'campo'=> 'hobby', 'texto'=> 'Hobby'])
            </div>

            <br>
            <div class="input-group">
                <span class="input-group-addon">¿Te molestan los animales?</span>
                <div class="pull-left form-radio">
                    <label class="radio-inline">
                        {!! Form::radio('animales_bool', '1', ($ficha->animales!="")) !!} SI
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('animales_bool', '0', ($ficha->animales=="")) !!} NO
                    </label>
                </div>

                <div id="animales_div" style="display:none;">
                    <br>
                    @include('includes.form_textarea', [ 'campo'=> 'animales' ])
                </div>
                <span class="help-block">{{ $errors->first('animales') }}</span>
            </div>

            <br>

            <div class="input-group">
                <span class="input-group-addon">¿Ha realizado algún curso de idiomas en el extranjero con anterioridad?</span>
                <div class="pull-left form-radio">
                    <label class="radio-inline">
                        {!! Form::radio('curso_anterior_bool', '1', ($ficha->curso_anterior!="")) !!} SI
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('curso_anterior_bool', '0', ($ficha->curso_anterior=="")) !!} NO
                    </label>
                </div>

                <div id="curso_anterior_div" style="display:none;">
                    <br>
                    @include('includes.form_input_text', [ 'campo'=> 'curso_anterior' ])
                </div>
                <span class="help-block">{{ $errors->first('curso_anterior') }}</span>
            </div>

            <br>

            @if($viajero->es_adulto)
            <div class="form-group">
                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'fumador', 'texto'=> 'Fumador'])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'profesion', 'texto'=> 'Profesión'])
                </div>
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'empresa', 'texto'=> 'Empresa'])
                </div>
            </div>
            @endif

            @if($viajero->es_cic)
            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_checkbox', [ 'campo'=> 'cic', 'texto'=> 'Estudias en un centro de idiomas del CIC'])
                </div>
                <div id="cic_nivel_div" class="col-md-8" style="display:none;">
                    @include('includes.form_select', [ 'campo'=> 'cic_nivel', 'texto'=> 'Nivel', 'select'=> ConfigHelper::getCICNivel()])
                </div>
            </div>
            @endif

            <div class="form-group">
                @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas internas', 'novisible'=>true])
            </div>

            @include('includes.form_submit', [ 'permiso'=> 'viajeros', 'texto'=> 'Guardar'])

        {!! Form::close() !!}

    </div>
</div>

@include('includes.script_boolean', ['campo'=> 'alergias', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'medicacion', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'animales', 'required'=>0 ])
@include('includes.script_boolean', ['campo'=> 'curso_anterior', 'required'=>0 ])

<script type="text/javascript">
$(document).ready(function() {

    @if($viajero->es_cic)
        @if($ficha->cic)
            $('#cic_nivel_div').show();
        @endif

        $("#cic").click(function(){
            if( $(this).is(':checked') )
            {
                $('#cic_nivel_div').show();
            }
            else
            {
                $('#cic_nivel_div').hide();
            }
        });
    @endif

});
</script>

@stop