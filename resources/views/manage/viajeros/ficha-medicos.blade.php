<div class="portlet light bordered">

    <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Alergia:</span>
        <div class="pull-left form-radio">
            <label class="radio-inline">
                {!! Form::radio('alergias_bool', '1', ($ficha->alergias)) !!} SI
            </label>
            <label class="radio-inline">
                {!! Form::radio('alergias_bool', '0', (!$ficha->alergias)) !!} NO
            </label>
        </div>

        <div id="alergias_div" style="display:none;">
            <br>
            @include('includes.form_textarea', [ 'campo'=> 'alergias' ])
            @include('includes.form_textarea', [ 'campo'=> 'alergias2', 'novisible'=> true, 'help'=> 'Información en inglés que aparecerá tal cual en las notas booking para el proveedor.' ])
        </div>
        <span class="help-block">{{ $errors->first('alergias') }}</span>
    </div>
    </div>

    <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Enfermedad:</span>
        <div class="pull-left form-radio">
            <label class="radio-inline">
                {!! Form::radio('enfermedad_bool', '1', ($ficha->enfermedad)) !!} SI
            </label>
            <label class="radio-inline">
                {!! Form::radio('enfermedad_bool', '0', (!$ficha->enfermedad)) !!} NO
            </label>
        </div>

        <div id="enfermedad_div" style="display:none;">
            <br>
            @include('includes.form_textarea', [ 'campo'=> 'enfermedad' ])
            @include('includes.form_textarea', [ 'campo'=> 'enfermedad2', 'novisible'=> true, 'help'=> 'Información en inglés que aparecerá tal cual en las notas booking para el proveedor.' ])
        </div>
        <span class="help-block">{{ $errors->first('enfermedad') }}</span>
    </div>
    </div>

    <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Medicación:</span>
        <div class="pull-left form-radio">
            <label class="radio-inline">
                {!! Form::radio('medicacion_bool', '1', ($ficha->medicacion)) !!} SI
            </label>
            <label class="radio-inline">
                {!! Form::radio('medicacion_bool', '0', (!$ficha->medicacion)) !!} NO
            </label>
        </div>

        <div id="medicacion_div" style="display:none;">
            <br>
            @include('includes.form_textarea', [ 'campo'=> 'medicacion' ])
            @include('includes.form_textarea', [ 'campo'=> 'medicacion2', 'novisible'=> true, 'help'=> 'Información en inglés que aparecerá tal cual en las notas booking para el proveedor.' ])
        </div>
        <span class="help-block">{{ $errors->first('medicacion') }}</span>
    </div>
    </div>

    <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Tratamiento:</span>
        <div class="pull-left form-radio">
            <label class="radio-inline">
                {!! Form::radio('tratamiento_bool', '1', ($ficha->tratamiento)) !!} SI
            </label>
            <label class="radio-inline">
                {!! Form::radio('tratamiento_bool', '0', (!$ficha->tratamiento)) !!} NO
            </label>
        </div>

        <div id="tratamiento_div" style="display:none;">
            <br>
            @include('includes.form_textarea', [ 'campo'=> 'tratamiento' ])
            @include('includes.form_textarea', [ 'campo'=> 'tratamiento2', 'novisible'=> true, 'help'=> 'Información en inglés que aparecerá tal cual en las notas booking para el proveedor.' ])
        </div>
        <span class="help-block">{{ $errors->first('tratamiento') }}</span>
    </div>
    </div>

    <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Dieta especial:</span>
        <div class="pull-left form-radio">
            <label class="radio-inline">
                {!! Form::radio('dieta_bool', '1', ($ficha->dieta)) !!} SI
            </label>
            <label class="radio-inline">
                {!! Form::radio('dieta_bool', '0', (!$ficha->dieta)) !!} NO
            </label>
        </div>

        <div id="dieta_div" style="display:none;">
            <br>
            @include('includes.form_textarea', [ 'campo'=> 'dieta' ])
            @include('includes.form_textarea', [ 'campo'=> 'dieta2', 'novisible'=> true, 'help'=> 'Información en inglés que aparecerá tal cual en las notas booking para el proveedor.' ])
        </div>
        <span class="help-block">{{ $errors->first('dieta') }}</span>
    </div>
    </div>

    <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">¿Te molestan los animales?</span>
        <div class="pull-left form-radio">
            <label class="radio-inline">
                {!! Form::radio('animales_bool', '1', ($ficha->animales)) !!} SI
            </label>
            <label class="radio-inline">
                {!! Form::radio('animales_bool', '0', (!$ficha->animales)) !!} NO
            </label>
        </div>

        <div id="animales_div" style="display:none;">
            <br>
            @include('includes.form_textarea', [ 'campo'=> 'animales' ])
        </div>
        <span class="help-block">{{ $errors->first('animales') }}</span>
    </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-12">
        {!! Form::submit('Guardar', array( 'name'=>'submit_medicos', 'class' => 'btn btn-success pull-right')) !!}
    </div>
</div>

@include('includes.script_boolean', ['campo'=> 'alergias', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'enfermedad', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'medicacion', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'tratamiento', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'dieta', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'animales', 'required'=>0 ])
