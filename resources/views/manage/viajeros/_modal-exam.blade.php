<div class="modal fade" id="modalBooking-exam" style="top: 20% !important;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Enviar Test a cliente :: Escoger destinatarios
            </div>
    
            {!! Form::open(array('id'=> 'frm-viajero-test', 'method' => 'POST', 'url' => route('manage.viajeros.exam', $ficha->id) , 'role' => 'form', 'class' => 'frm-1click')) !!}
                <div class="modal-body">
    
                    <div class="form-group">
                        @include('includes.form_select_multi', ['campo'=> 'destinatarios', 'texto'=> "Seleccionar", 'select'=> $ficha->destinatarios])
                    </div>

                    @php
                        $examenes = \VCN\Models\Exams\Examen::activo()->pluck('name','id')->toArray();
                    @endphp

                    <div class="note note-info">
                        @include('includes.form_select', ['campo'=> 'examen_id', 'texto'=> 'Test', 'select'=> $examenes])
                    </div>
    
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Enviar', array('class' => 'btn btn-success btn-block col-md-12')) !!}
                </div>
    
            {!! Form::close() !!}
    
        </div>
    </div>
</div>