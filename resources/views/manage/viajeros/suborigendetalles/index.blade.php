@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-group fa-fw"></i> Sub-Origen Detalle Lead
                <span class="pull-right"><a href="{{ route('manage.viajeros.suborigendetalles.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Sub-Origen Detalle Lead</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Sub-Origen Detalle',
                      'suborigen'       => 'Sub-Origen',
                      'origen'          => 'Origen',
                      'plataforma'      => 'Plataforma',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.viajeros.suborigendetalles.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [4] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop