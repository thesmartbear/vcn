
<div class="portlet light tabs">
    <div class="portlet-title">
        <div class="actions">
            <span class="pull-right"><a href="{{ route('manage.viajeros.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Lead</a></span>
        </div>
    </div>

    <div class="portlet-body">

        {!! Datatable::table()
            ->addColumn([
              'name'        => 'Nombre',
              'lastname'        => 'Apellido 1',
              'lastname2'        => 'Apellido 2',
              'asignado'    => 'Asignado',
              'email'       => 'Email',
              'phone'       => 'Tlf',
              'movil'       => 'Móvil',
              'categoria'      => 'Categoria',
              'status'      => 'Status',
              'rating'      => 'Rating',
              'idioma'      => 'Idioma',
              'tutor1'      => 'Tutor1',
              'movil_tutor1'      => 'Tutor1 Móvil',
              'email_tutor1'      => 'Tutor1 Email',
              'tutor2'      => 'Tutor2',
              'movil_tutor2'      => 'Tutor2 Móvil',
              'email_tutor2'      => 'Tutor2 Email',
              'options' => ''
            ])
            ->setUrl( route('manage.viajeros.index', [$status_id, $asign_to, $oficina_id]) )
            ->setOptions('iDisplayLength',100)
            ->setOptions(
              "aoColumnDefs", array(
                [ "bSortable" => false, "aTargets" => [17] ]
              )
            )
            ->render() !!}

    </div>
</div>