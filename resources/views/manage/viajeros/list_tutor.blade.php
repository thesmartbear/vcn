<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-suitcase"></i> Leads del Tutor
    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
              'name'        => 'Nombre',
              'relacion'    => 'Relación',
              'asignado'    => 'Asignado',
              'email'       => 'Email',
              'status'      => 'Status',
              'change'      => 'Cambiar Relación',
              'options' => ''

            ])
            ->setUrl( route('manage.viajeros.index.tutor', $tutor_id) )
            ->setOptions(
              "aoColumnDefs", array(
                [ "bSortable" => false, "aTargets" => [5] ]
              )
            )
            ->render() !!}

    </div>
</div>