@extends('layouts.manage')


@section('breadcrumb')
{{--    {!! Breadcrumbs::render('manage.viajeros.buscar') !!}--}}
@stop


@section('titulo')
    <i class="fa fa-suitcase"></i> Resultados de búsqueda: {{$query}}
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-suitcase"></i> Resultados búsqueda por <strong>{{$campo}}</strong>
        </div>
        <div class="panel-body">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#viajeros" aria-controls="viajeros" role="tab" data-toggle="tab">Viajeros [{{$totV}}]</a></li>
                <li role="presentation"><a href="#tutores" aria-controls="tutores" role="tab" data-toggle="tab">Tutores [{{$totT}}]</a></li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="viajeros">

                    {!! Datatable::table()
                        ->setId('DttBuscarViajeros')
                        ->addColumn([
                          'name'        => 'Nombre',
                          'tutores'     => 'Tutor/es',
                          'asignado'    => 'Asignado',
                          'email'       => 'Email',
                          'status'      => 'Status',
                          'rating'      => 'Rating',
                          'creado'      => 'F.Creación',
                          'options'     => ''

                        ])
                        ->setUrl( route('manage.viajeros.buscar', [$campo,$query]) )
                        ->setOptions('iDisplayLength',100)
                        ->setOptions(
                          "columnDefs", array(
                            [ "sortable" => false, "aTargets" => [7] ],
                          )
                        )
                        ->setOptions(
                            "rowCallback", "function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                                if(aData['status'].indexOf('badge-incidencias')>0) { $('td', nRow).addClass('td-st-incidencias'); }
                                if(aData['status'].indexOf('fa-exclamation-triangle')>0) { $('td', nRow).addClass('td-st-incidencia'); }
                             }"
                        )
                        ->render() !!}

                </div>

                <div role="tabpanel" class="tab-pane fade in" id="tutores">

                    {!! Datatable::table()
                        ->setId('DttBuscarTutores')
                        ->addColumn([
                            'nombre'      => 'Tutor',
                            'email'       => 'Email',
                            'viajeros'    => 'Leads/Relación',
                            'phone'       => 'Teléfono',
                            'movil'       => 'Movil',
                            'empresa'     => 'Empresa',
                            'options'     => ''

                            ])
                        ->setUrl( route('manage.tutores.buscar', [$campo,$query]) )
                        ->setOptions('iDisplayLength',100)
                        ->setOptions(
                            "columnDefs", array(
                                [ "sortable" => false, "aTargets" => [6] ]
                            )
                        )
                        ->render() !!}


                </div>

            </div>

        </div>
    </div>

@stop