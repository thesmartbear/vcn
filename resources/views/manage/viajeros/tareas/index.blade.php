@extends('layouts.manage')


@section('container')

    @include('manage.viajeros.tareas.list', ['viajero_id'=> $viajero_id])

@stop