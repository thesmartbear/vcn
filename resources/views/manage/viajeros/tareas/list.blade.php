<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-tasks"></i> Tareas

        {{-- <span class="pull-right"><a href="{{ route('manage.viajeros.tareas.nuevo', $viajero_id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Tarea</a></span> --}}

        <span class="pull-right">
          <a href="#" data-toggle='modal' data-target='#modalViajeroTarea' class="btn btn-danger btn-xs">
            <i class="fa fa-plus-circle"></i> Nueva Tarea
          </a>
        </span>

    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
              'fecha'    => 'Fecha',
              'tipo'     => 'Tipo',
              'notas'    => 'Notas',
              'usuario'  => 'Creado',
              'asignado' => 'Asignado',
              'estado'  => 'Estado',
              'options' => ''

            ])
            ->setUrl( route('manage.viajeros.tareas.index', $viajero_id) )
            ->setOptions('iDisplayLength', 100)
            ->setOptions('order', [[ 0, 'desc' ]])
            ->setOptions(
              "aoColumnDefs", array(
                [ "bSortable" => false, "aTargets" => [6] ]
              )
            )
            ->render() !!}

    </div>
</div>

@include('manage.viajeros.tareas.edit')