@extends('layouts.manage')


@section('container')

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-flag fa-fw"></i> Tarea editar :: Viajero {{$viajero}}
    </div>
    <div class="panel-body">


        {!! Form::model($ficha, array('route' => array('manage.viajeros.tareas.ficha',$ficha->id))) !!}

            {!! Form::hidden('viajero_id', $ficha->viajero_id) !!}

            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_input_datetime', [ 'campo'=> 'tarea_fecha', 'texto'=> 'Fecha', 'valor'=>Carbon::parse($ficha->fecha)->format('d/m/Y') ])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_datetime_hora', [ 'campo'=> 'tarea_hora', 'texto'=> 'Hora', 'valor'=>Carbon::parse($ficha->fecha)->format('H:i') ])
                </div>
            </div>

            <div class="form-group">
                @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'valor'=> $ficha->tipo, 'select'=> $tipos])
            </div>

            <div class="form-group">
                @include('includes.form_select', [ 'campo'=> 'asign_to', 'texto'=> 'Asignada a', 'valor'=> $ficha->asign_to, 'select'=> $asignados])
            </div>

            <div class="form-group">
                @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Resumen'])
            </div>

            <div class="form-group pull-right">
                {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
            </div>

        {!! Form::close() !!}

    </div>
</div>
@stop