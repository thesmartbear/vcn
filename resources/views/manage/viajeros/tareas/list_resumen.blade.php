<div class="portlet box {{$color_estado}}">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-tasks"></i> Tareas :: {{$filtro_name}}
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
            <a href="" class="reload reload-tasks"></a>
            <a href="" class="fullscreen"></a>
        </div>

    </div>
    <div class="portlet-body">

      <?php
        $r = route('manage.viajeros.tareas.index.resumen', [$filtro,0,$oficina_id]);
      ?>

            {!! Datatable::table()
                ->setId('DttTareas-'.$filtro)
                ->addColumn([
                  'fecha'    => 'Fecha',
                  'viajero'  => 'Viajero',
                  'tipo'     => 'Tipo',
                  'notas'    => 'Notas',
                  'usuario'  => 'Creado',
                  'asignado' => 'Asignado',
                  'estado'  => 'Estado',
                  'rating'  => 'Rating',
                  'options' => ''

                ])
                ->setUrl( $r )
                ->setOptions('iDisplayLength', 100)
                ->setOptions('order', [ 0, 'desc' ])
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [7] ],
                    [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY HH:mm'):'-';}" ],
                  )
                )
                ->render() !!}

        </div>
</div>