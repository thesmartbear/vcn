@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Seguimiento editar :: Viajero {{$viajero}}
            </div>
            <div class="panel-body">


                {!! Form::model($ficha, array('route' => array('manage.viajeros.logs.ficha',$ficha->id))) !!}

                    {!! Form::hidden('viajero_id', $ficha->viajero_id) !!}

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'tipo', 'texto'=> 'Tipo', 'valor'=> $ficha->tipo, 'select'=> $tipos])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'asign_to', 'texto'=> 'Asignada a', 'valor'=> $ficha->asign_to, 'select'=> $asignados])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Resumen'])
                    </div>

                    <div class="form-group pull-right">
                        {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                        <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                    </div>

                {!! Form::close() !!}

            </div>
        </div>

@stop