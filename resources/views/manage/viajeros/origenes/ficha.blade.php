@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-group fa-fw"></i> Origen :: {{$ficha->name}}
            </div>
            <div class="panel-body">


                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Origen</a></li>

                    @if(ConfigHelper::canEdit('informes'))
                    <li role="presentation"><a href="#ventas" aria-controls="ventas" role="tab" data-toggle="tab">Ventas</a></li>
                    <li role="presentation"><a href="#ventasdet" aria-controls="ventasdet" role="tab" data-toggle="tab">Ventas detalle</a></li>
                    <li role="presentation"><a href="#solicitudes" aria-controls="solicitudes" role="tab" data-toggle="tab">Solicitudes</a></li>
                    @endif
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">
                        {!! Form::model($ficha, array('route' => array('manage.viajeros.origenes.ficha', $ficha->id))) !!}

                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Origen'])
                        </div>

                        @if(count(config('vcn.plataformas')))
                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'propietario', 'texto'=> 'Privado'])
                        </div>
                        @endif

                        <div class="form-group pull-right">
                            {!! Form::submit('Guardar', array('class' => 'btn btn-success')) !!}
                            <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
                        </div>

                        {!! Form::close() !!}
                    </div>

                    @if(ConfigHelper::canEdit('informes'))
                    <div role="tabpanel" class="tab-pane fade in" id="ventas">
                        <div class="row">
                            <div class="col-md-12">

                                <?php
                                    $anyd = 2015;
                                    $anyh = intval(Carbon::now()->format('Y'));
                                ?>

                                <ul class="nav nav-tabs" role="tablist">
                                @for ($i = $anyd; $i <= $anyh; $i++)
                                    @if($totales[$i]['total_num']>0)
                                        <li role="presentation" class="{{($i === $anyh)?'active':''}}"><a href="#any-{{$i}}" aria-controls="any-{{$i}}" role="tab" data-toggle="tab">{{$i}}</a></li>
                                    @endif
                                @endfor
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    @for ($i = $anyd; $i <= $anyh; $i++)
                                        @if($totales[$i]['total_num']>0)
                                        <div role="tabpanel" id="any-{{$i}}" class="tab-pane fade in {{($i === $anyh)?'active':''}}">

                                            {!! Datatable::table()
                                                ->addColumn([
                                                    'categoria'     => 'Categoria',
                                                    'num'           => 'Nº Inscrip.',
                                                    'total_sem'     => 'Semanas',
                                                    'total_curso'   => "Imp.Curso <i class='fa fa-info-circle' data-label='Importe del curso, sin extras ni descuentos, ni alojamiento'></i>",
                                                    'total'         => "Imp.Total <i class='fa fa-info-circle' data-label='Importe total a pagar por el cliente'></i>",
                                                ])
                                                ->setUrl( route('manage.viajeros.origenes.ventas',[$ficha->id,$i]) )
                                                ->setOptions('iDisplayLength', 100)
                                                ->render()
                                            !!}

                                            <hr>

                                            <table class="table table-bordered table-hover">
                                                <caption>Totales</caption>
                                                <thead>
                                                    <tr>
                                                        <th>Total Inscrip.</th>
                                                        <th>Total Semanas</th>
                                                        <th>Total Curso</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>{{$totales[$i]['total_num']}}</td>
                                                        <td>{{$totales[$i]['total_sem']}}</td>
                                                        <td>{{ConfigHelper::parseMoneda($totales[$i]['total_curso'])}}</td>
                                                        <td>{{ConfigHelper::parseMoneda($totales[$i]['total'])}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                        @endif
                                    @endfor

                                </div>

                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="ventasdet">

                        <div class="row">
                            <div class="col-md-12">

                                <?php
                                    $anyd = 2015;
                                    $anyh = intval(Carbon::now()->format('Y'));
                                ?>

                                <ul class="nav nav-tabs" role="tablist">
                                @for ($i = $anyd; $i <= $anyh; $i++)
                                    @if($totales[$i]['total_num']>0)
                                        <li role="presentation" class="{{($i === $anyh)?'active':''}}"><a href="#anyd-{{$i}}" aria-controls="anyd-{{$i}}" role="tab" data-toggle="tab">{{$i}}</a></li>
                                    @endif
                                @endfor
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    @for ($i = $anyd; $i <= $anyh; $i++)
                                        @if($totales[$i]['total_num']>0)
                                        <div role="tabpanel" id="anyd-{{$i}}" class="tab-pane fade in {{($i === $anyh)?'active':''}}">

                                            {!! Datatable::table()
                                                ->addColumn([
                                                    'viajero'   => 'Viajero',
                                                    'curso'     => 'Curso',
                                                    'convocatoria'  => 'Convocatoria',
                                                    'semanas'   => 'NºSemanas',
                                                    'curso_total'   => 'Imp. Curso',
                                                    'fecha'     => 'Fecha Booking',
                                                    'origen'  => 'Origen',
                                                    'total'     => 'Importe total',
                                                    'seguro'    => 'Seguro cancelación',
                                                    'descuentos'=> 'Descuentos',
                                                    'options'   => ''

                                                ])
                                                ->setUrl( route('manage.viajeros.origenes.ventas-detalle',[$ficha->id,$i]) )
                                                ->setOptions('iDisplayLength', 100)
                                                ->setOptions(
                                                  "columnDefs", array(
                                                    [ "sortable" => false, "targets" => [10] ],
                                                    [ "targets" => [5], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                                  )
                                                )
                                                ->render()
                                            !!}

                                        </div>
                                        @endif
                                    @endfor

                                </div>

                            </div>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="solicitudes">

                        <div class="row">
                            <div class="col-md-12">

                                <?php
                                    $anyd = 2015;
                                    $anyh = intval(Carbon::now()->format('Y'));

                                    $valores['origen_id'] = $ficha->id;
                                    $valores['suborigen_id'] = 0;
                                    $valores['suborigendet_id'] = 0;
                                ?>

                                <ul class="nav nav-tabs" role="tablist">
                                @for ($i = $anyd; $i <= $anyh; $i++)
                                    <li role="presentation" class="{{($i === $anyh)?'active':''}}"><a href="#anys-{{$i}}" aria-controls="anys-{{$i}}" role="tab" data-toggle="tab">{{$i}}</a></li>
                                @endfor
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    @for ($i = $anyd; $i <= $anyh; $i++)
                                        <div role="tabpanel" id="anys-{{$i}}" class="tab-pane fade in {{($i === $anyh)?'active':''}}">

                                            @include('manage.solicitudes.list', ['status_id'=> 0, 'user_id'=> 'all', 'oficina_id'=> 'all', 'status_name' => 0, 'any'=> $i, 'valores'=> $valores])

                                        </div>
                                    @endfor

                                </div>

                            </div>
                        </div>

                    </div>
                    @endif

                </div>

            </div>
        </div>

@stop