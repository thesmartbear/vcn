@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-group fa-fw"></i> Leads Origen
                <span class="pull-right"><a href="{{ route('manage.viajeros.origenes.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Origen Lead</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Origen',
                      'plataforma'      => 'Plataforma',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.viajeros.origenes.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [2] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop