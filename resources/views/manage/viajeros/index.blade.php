@extends('layouts.manage')


@section('breadcrumb')
    {!! Breadcrumbs::render('manage.viajeros.index') !!}
@stop


@section('titulo')
    <i class="fa fa-suitcase"></i> Viajeros
@stop

@section('container')
    {{--
    <div class="row">
        Por Status:
            <a class="btn btn-default {{!$status_id?'btn-success':''}}" href="{{ route('manage.viajeros.index') }}">Todos</a>
        @foreach($statuses as $status)
            <a class="btn btn-default {{$status_id==$status->id?'btn-success':''}}" href="{{ route('manage.viajeros.index', $status->id) }}">{{$status->name}}</a>
        @endforeach
    </div>
    --}}

    @if( !auth()->user()->es_aislado )
    <div class="row">
        <div class="col-md-6"></div>
        {{-- <div class="col-md-1">
            {!! Form::select('any', $anys, $any, array('class'=>'select2', 'data-style'=>'blue', 'id'=>'select-any-filtro'))  !!}
            @include('includes.script_filtro_any')
        </div>
        <div class="col-md-5"><i>(=año de la fecha inicio booking)</i></div> --}}

        <?php $user_id = $asign_to; ?>
        @include('includes.select_asignados', ['route'=> 'manage.viajeros.index'])

    </div>

    <hr>

    <div class="row">
        <div class="col-md-2">
            <a class="btn btn-success" href="{{ route('manage.viajeros.index.excel') }}"><i class="fa fa-file-excel-o"></i> Listado para Excel</a>
        </div>
        <div class="col-md-2">
            <a href='#confirma' class="btn btn-success" data-label='Exportar' data-pregunta='Este proceso puede tardar bastante, sea paciente.' data-action=" {{ route('manage.viajeros.index.excel.download',1) }}"><i class="fa fa-download"></i> Listado completo</a>
        </div>
        <div class="col-md-2">
            <a href='#confirma' class="btn btn-success" data-label='Exportar' data-pregunta='Este proceso puede tardar bastante, sea paciente.' data-action=" {{ route('manage.viajeros.index.excel.download') }}"><i class="fa fa-download"></i> Listado completo SIN optouts</a>
        </div>
    </div>
    <hr>
    @endif

    <!-- Nav tabs -->
    <ul class="nav nav-tabs arrows" role="tablist">
        <li role="presentation" class="active all"><a href="#todos" aria-controls="todos" role="tab" data-toggle="tab">Todos [{{$statuses_total[0]}}]</a></li>

        @foreach($statuses as $status)
            <li role="presentation" class="status-{{$status->orden}}">
                <a href="#status-{{$status->id}}" aria-controls="status-{{$status->id}}" role="tab" data-toggle="tab"><span>{{$status->name}} [{{$statuses_total[$status->id]}}]</span></a>
            </li>
        @endforeach

    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <div role="tabpanel" class="tab-pane fade in active" id="todos">
            @include('manage.viajeros.list', ['status_id'=> 0, 'asign_to'=>$asign_to, 'oficina_id'=> $oficina_id, 'status_name' => 'Todos'])
        </div>

        @foreach($statuses as $status)
        <div role="tabpanel" class="tab-pane fade in" id="status-{{$status->id}}">
            @include('manage.viajeros.list', ['status_id'=> $status->id, 'asign_to'=>$asign_to, 'oficina_id'=> $oficina_id, 'status_name' => $status->name])
        </div>
        @endforeach

    </div>

@stop