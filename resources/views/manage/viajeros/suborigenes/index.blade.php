@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-group fa-fw"></i> Sub-Origen Lead
                <span class="pull-right"><a href="{{ route('manage.viajeros.suborigenes.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Sub-Origen Lead</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Sub-Origen',
                      'origen'          => 'Origen',
                      'plataforma'      => 'Plataforma',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.viajeros.suborigenes.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [3] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop