{{-- <br>
<div class="row">
    <div class="col-md-2 col-md-offset-10">
    <span class="pull-right"><a href="{{ route('manage.tutores.nuevo', $ficha->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Tutor</a></span>
    </div>
</div> --}}

<br>

@foreach($ficha->tutores as $tutor)
    <div class="col-md-6">
        @include('manage.tutores.ficha_include', ['ficha'=> $tutor, 'viajero_id'=> $ficha->id])
    </div>
@endforeach

<div id="tutor-new-2" class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            Añadir nuevo Tutor:


            <span class="pull-right">
                <button data-label="Añadir tutor" class="btn btn-xs btn-success btn-tutor-new"><i class="fa fa-plus-circle"></i> Añadir</button>
            </span>

        </div>
        <div class="panel-body">

            <form action="" id="formulario">

            <div class="form-group">
                @include('includes.form_select', [ 'campo'=> 'tutor_relacion', 'texto'=> 'Relación', 'valor'=> 1, 'select'=> ConfigHelper::getTutorRelacion()])
            </div>

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'tutor_name', 'valor'=>'', 'texto'=> 'Nombre'])
            </div>

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'tutor_lastname', 'valor'=>'', 'texto'=> 'Apellidos'])
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_select', [ 'campo'=> 'tutor_tipodoc', 'texto'=> "Tipo", 'select'=> ConfigHelper::getTipoDoc() ])
                </div>
                <div class="col-md-8">
                    @include('includes.form_input_text', [ 'campo'=> 'tutor_nif', 'valor'=> $ficha->nif, 'texto'=> 'DNI'])
                </div>
            </div>

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'tutor_email', 'valor'=>'', 'texto'=> 'E-mail'])
            </div>

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'tutor_phone', 'valor'=>'', 'texto'=> 'Teléfono'])
            </div>

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'tutor_movil', 'valor'=>'', 'texto'=> 'Movil'])
            </div>

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'tutor_profesion', 'valor'=>'', 'texto'=> 'Profesión'])
            </div>

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'tutor_empresa', 'valor'=>'', 'texto'=> 'Empresa'])
            </div>

            <div class="form-group">
                @include('includes.form_input_text', [ 'campo'=> 'tutor_empresa_phone', 'texto'=> 'Tlf. Empresa'])
            </div>

            <span class="form-group pull-right">
                <button data-label="Añadir tutor" class="btn btn-xs btn-success btn-tutor-new"><i class="fa fa-plus-circle"></i> Añadir</button>
            </span>

            </form>

        </div>
    </div>
</div>

<script type="text/javascript">
$(function() {

    //rules nif/nie
    function tipodoc_nif()
    {
        return $('#tutor_tipodoc').val()==0;
    }

    function tipodoc_nie()
    {
        return $('#tutor_tipodoc').val()==1;
    }

    $("#tutor_tipodoc").change( function(){

        $("#tutor_nif").rules( "remove" );
        $("#tutor_nif").rules("add",{
            nifES: tipodoc_nif(),
            nieES: tipodoc_nie(),
        });

    });

    $("#frmValidar").validate({
        rules:
        {
            tutor_nif: {
                nifES: tipodoc_nif(),
                nieES: tipodoc_nie(),
            },
        }
    });
});
</script>