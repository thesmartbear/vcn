<div class="panel panel-default margin-top-30">
    <div class="panel-heading">
        <i class="fa fa-ticket"></i> Facturas
    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
              'fecha'   => 'Fecha',
              'numero'  => 'Numero',
              'booking_id'  => 'Booking',
              'viajero' => 'Viajero',
              'total'   => 'Total',
              'grup'    => 'Agrupada',
              'manual'  => 'Manual',
              'pdf'     => 'Ver',
              'options' => ''

            ])
            ->setUrl( route('manage.viajeros.facturas', [$viajero_id]) )
            ->setOptions('iDisplayLength', 100)
            ->setOptions(
              "columnDefs", array(
                [ "sortable" => false, "targets" => [7,8] ],
                [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
              )
            )
            ->render() !!}

    </div>

</div>

<hr>
<div class="row">
<?php
$pubpath = "/files/bookings/" . $viajero_id . "/";
$path = storage_path($pubpath);


if(is_dir($path))
{
    $results = scandir($path);
    foreach ($results as $result) {
      if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0]==='.') continue;

      $file = $path . '/' . $result;

      echo "<ul>";

      if( is_file($file) )
      {
        ?>
        <li><a target="_blank" href="{{$pubpath}}{{$result}}">{{$result}}</a></li>
        <?php
      }

      echo "</ul>";

    }
}
?>
</div>
