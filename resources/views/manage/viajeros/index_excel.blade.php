@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.viajeros.index') !!}
@stop

@section('container')

<div class="row">
    <div class="pull-right">
        <div class="col-md-3">

            @foreach($statuses as $a)
                <?php $o[$a->id] = $a->name; ?>
            @endforeach
                <?php $o['all'] = 'TODOS'; ?>
                <?php $o1 = $status_id?$status_id:'all'; ?>

            {!! Form::select('status', $o, $o1, array('class'=>'selectpicker show-tick pull-right', 'data-style'=>'purple', 'id'=>'select-filtro-status'))  !!}

        </div>
    </div>

    @include('includes.script_filtro_status')
</div>

<br>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-suitcase fa-fw"></i> Viajeros (Todos)
    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
              'name'        => 'Nombre',
              'lastname'        => 'Apellido 1',
              'lastname2'        => 'Apellido 2',
              'asignado'    => 'Asignado',
              'email'       => 'Email',
              'phone'       => 'Tlf',
              'movil'       => 'Móvil',
              'categoria'      => 'Categoria',
              'status'      => 'Status',
              'rating'      => 'Rating',
              'idioma'      => 'Idioma',
              'tutor1'      => 'Tutor1',
              'movil_tutor1'      => 'Tutor1 Móvil',
              'email_tutor1'      => 'Tutor1 Email',
              'tutor2'      => 'Tutor2',
              'movil_tutor2'      => 'Tutor2 Móvil',
              'email_tutor2'      => 'Tutor2 Email',
            ])
            ->setUrl( route('manage.viajeros.index.excel', [$status_id]) )
            ->setOptions('iDisplayLength',100)
            ->render() !!}

    </div>
</div>
@stop