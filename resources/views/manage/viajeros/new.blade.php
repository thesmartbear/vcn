@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.viajeros.nuevo') !!}
@stop

@section('container')


{!! Form::open(array('method' => 'POST', 'url' => route('manage.viajeros.ficha',0), 'role' => 'form', 'class' => '')) !!}

{!! Form::hidden('rating', 0, array('class' => 'rating')) !!}

<div class="row">

    <div class="col-md-9">

        <div class="portlet box blue-ebonyclay">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-suitcase fa-fw"></i> Viajero Nuevo
                </div>
                <div class="tools">
                    <div id="stars" class="pull-right"></div>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-body row">

                    <div class="form-group">
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'lastname', 'texto'=> 'Apellido'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'lastname2', 'texto'=> 'Apellido2'])
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-3">
                            {!! Form::label('sexo', 'Sexo') !!}:<br>
                            <label class="radio-inline">
                                {!! Form::radio('sexo', '1', false) !!} <i class="fa fa-male"></i>
                            </label>
                            <label class="radio-inline">
                                {!! Form::radio('sexo', '2', false) !!} <i class="fa fa-female"></i>
                            </label>
                            <span class="help-block">{{ $errors->first('sexo') }}</span>
                        </div>
                        <div class="col-md-1">
                            @include('includes.form_checkbox', [ 'campo'=> 'es_adulto', 'texto'=> 'Adulto'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_datetime', [ 'campo'=> 'fechanac', 'texto'=> 'Fecha Nac.', 'required'=> true])
                        </div>
                        <div class="col-md-4">
                            {{-- @include('includes.form_select2', [ 'campo'=> 'paisnac', 'texto'=> 'País Nac.', 'select'=> ConfigHelper::getPaises()]) --}}
                            @include('includes.form_select_pais', [ 'campo'=> 'paisnac', 'texto'=> 'País Nac.'])
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6">
                            @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'Email'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'phone', 'texto'=> 'Teléfono'])
                        </div>
                        <div class="col-md-3">
                            @include('includes.form_input_text', [ 'campo'=> 'movil', 'texto'=> 'Móvil'])
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-5">
                            @include('includes.form_input_text', [ 'campo'=> 'datos_ciudad', 'texto'=> 'Ciudad'])
                        </div>

                        @if( count(ConfigHelper::idiomas())>1 )
                        <div class="col-md-3">
                            @include('includes.form_select', [ 'campo'=> 'idioma_contacto', 'texto'=> 'Idioma contacto', 'select'=> ConfigHelper::getIdiomaContacto()])
                        </div>
                        @endif

                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <h4 class="form-section">¿Cómo nos has conocido?</h4>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4">
                            @include('includes.form_select2', [ 'campo'=> 'origen_id', 'texto'=> 'Origen', 'select'=> $conocidos])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select2', [ 'campo'=> 'suborigen_id', 'texto'=> 'Sub-Origen', 'select'=> null])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select2', [ 'campo'=> 'suborigendet_id', 'texto'=> 'Sub-Origen Detalle', 'select'=> null])
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <h4 class="form-section">Solicitud</h4>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'category_id', 'texto'=> 'Categoría', 'select'=> $categorias])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'subcategory_id', 'texto'=> 'Sub-Categoría','select'=> null])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'subcategory_det_id', 'texto'=> 'Detalle','select'=> null])
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4">
                            @include('includes.form_select', [ 'campo'=> 'destinos', 'texto'=> "País", 'select'=> [""=>""] + \VCN\Models\Pais::orderBy('name')->pluck('name','name')->toArray()])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'ciudad', 'texto'=> 'Ciudad'])
                        </div>
                        <div class="col-md-4">
                            @include('includes.form_input_text', [ 'campo'=> 'cursos', 'texto'=> 'Cursos'])
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas'])
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <br>

                            @include('includes.form_submit', [ 'permiso'=> 'viajeros', 'texto'=> 'Guardar'])
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>

    <div class="col-md-3 sortable column sidebar-panels">

        <div class="portlet portlet-sortable box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bookmark fa-fw"></i> Asignado a
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                </div>

            </div>
            <div class="portlet-body">
                @include('includes.form_select', ['campo'=> 'asign_to', 'texto'=> null, 'select'=> $asignados, 'valor'=> auth()->user()->id])
            </div>
        </div>

        <div class="portlet portlet-sortable box blue-steel">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bookmark fa-fw"></i> Prescriptor Booking
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                @include('includes.form_select', ['campo'=> 'prescriptor_id', 'texto'=> null, 'select'=> $prescriptores])
            </div>
        </div>


        <div class="portlet portlet-sortable box purple-plum">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-tag fa-fw"></i> Estado
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                </div>

            </div>
            <div class="portlet-body">
                @include('includes.form_select', ['campo'=> 'status_id', 'texto'=> null, 'select'=> $statuses, 'valor'=> 1])
            </div>
        </div>

    </div>

</div>
{!! Form::close() !!}

@include('includes.script_categoria')
@include('includes.script_conocido')
@include('includes.script_stars', ['rating'=> 0])
@include('includes.script_sortable_portlets')
@stop