<div class="portlet light bordered">
    <div class="form-group">
        @include('includes.form_checkbox', [ 'campo'=> 'fumador', 'texto'=> 'Fumador'])
    </div>
    <div class="form-group">
        @include('includes.form_input_text', [ 'campo'=> 'profesion', 'texto'=> 'Profesión'])
    </div>
    <div class="form-group">
        @include('includes.form_input_text', [ 'campo'=> 'empresa', 'texto'=> 'Empresa'])
    </div>

</div>

<div class="row">
    <div class="col-sm-12">
        {!! Form::submit('Guardar', array( 'name'=>'submit_adultos', 'class' => 'btn btn-success pull-right')) !!}
    </div>
</div>