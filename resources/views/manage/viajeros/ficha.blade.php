@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.viajeros.ficha', $ficha) !!}
@stop


@section('titulo')
    <div class="row">
    <div class="col-md-9">
        <img class="img-circle img-avatar" width="60px" src="{{ $ficha->avatar }}"> Lead: <b>{{$ficha->full_name}}</b> ({{$ficha->status_tipo}}) :: {{$ficha->status_name}}
        {!! $ficha->status_icono !!}
    </div>

    <div class="col-md-3">
    @if($ficha->es_booking)
        @if($ficha->booking->tiene_incidencias_pendientes)
            <a href='{{  route('manage.bookings.ficha', $ficha->booking_id) }}#incidencias'><i class="fa fa-exclamation-triangle icon-st-incidencia"></i></a>
        @endif
    @endif

    {{$ficha->fechanac ? Carbon::parse($ficha->fechanac)->age : "-"}} años

        @if($ficha->user && ConfigHelper::canEdit('superlogin'))
            <a class='pull-right' href='{{ route('manage.system.admins.login', [$ficha->user_id, 'url'=> Request::url()]) }}'><i class='fa fa-sign-in'></i></a>
        @endif

    @if($ficha->es_cumple)
        <i data-label="Es su cumpleaños" class="fa fa-birthday-cake"></i>
    @endif
    </div>

    </div>
@stop

@section('container')

@if(!$ficha->es_booking)
    @include('includes.script_archivar', ['booking'=> 0])
@endif

<div class="row">
    <div class="col-md-9">

        @if(!$ficha->asign_to)
            <div class="content">
                <div class="alert alert-danger" role="alert"> <i class="fa fa-warning"></i> Este Viajero está SIN ASIGNAR</div>
            </div>
        @endif

        @if(!$ficha->solicitud && !$ficha->booking)
            <div class="content">
                <div class="alert alert-warning" role="alert"> <i class="fa fa-bell"></i> Cuidado, este viajero no tiene ninguna solicitud abierta. Abre una con el botón: <a href="{{ route('manage.solicitudes.nuevo', $ficha->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Solicitud</a></div>
            </div>
        @endif

        @if($ficha->es_email_tutor)
            <div class="content">
                <div class="alert alert-warning" role="alert"> <i class="fa fa-bell"></i>
                    Cuidado, este viajero tiene un e-mail que corresponde a un Tutor (<a target="_blank" href="{{route('manage.tutores.ficha',$ficha->es_email_tutor->id)}}">{{$ficha->es_email_tutor->full_name}}</a>). Revisarlo.
                    <br>
                    <strong>En caso de realizar una Inscripción se anulará el e-mail del Viajero y sólo se creará el Área de Cliente para el Tutor.</strong>
                </div>
            </div>
        @endif

        <div class="portlet light">
            <div class="portlet-title">

                <div id="stars" class="pull-right"></div>

                <?php /*
                @if($ficha->es_cliente)
                    <a href="{{route('manage.inscritos.ficha', $ficha->id)}}">(Ver Inscrito)</a>

                    <div class="pull-right">
                        &nbsp;<a href="{{route('manage.inscritos.lead',[$ficha->id])}}" class='btn btn-danger btn-xs'><i class='fa fa-arrow-circle-left'></i> Volver a Lead</a>
                    </div>
                @else
                    <div class="pull-right">
                        {{-- <a href="{{route('manage.viajeros.inscribir',[$ficha->id])}}" class='btn btn-warning btn-xs'><i class='fa fa-arrow-circle-right'></i> Convertir a Inscrito</a> --}}
                    </div>
                @endif
                */ ?>

                <div class="pull-left">

                    @if(!$ficha->solicitud)
                        {{-- <a id="booking-no" href="#booking-no" class='btn btn-info btn-xs'><i class='fa fa-plus-circle'></i> Inscripción</a> --}}
                        <a id="booking-sin-solicitud" href="#booking-sin-solicitud" class='btn btn-info btn-xs'><i class='fa fa-plus-circle'></i> Inscripción</a>
                    @else
                        <a id="booking-new" href="#booking-new" class='btn btn-info btn-xs'><i class='fa fa-plus-circle'></i> Inscripción</a>
                    @endif

                    <a id="booking-presupuesto" href="#booking-presupuesto" class='btn btn-warning btn-xs'><i class='fa fa-plus-circle'></i> Presupuesto</a>

                    @if($ficha->solicitud_activa)
                        &nbsp;<a href="#confirma" data-pregunta="Ya hay una solicitud abierta para este viajero. ¿Seguro que la quieres archivar y abrir una nueva?" data-action="{{ route('manage.solicitudes.nuevo', $ficha->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Solicitud</a>
                    @elseif($ficha->es_booking)
                        &nbsp;<a href="#confirma" data-pregunta="Hay un Booking para este viajero. ¿Seguro que la quieres crear?" data-action="{{ route('manage.solicitudes.nuevo', $ficha->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Solicitud</a>
                    @else
                        &nbsp;<a href="{{ route('manage.solicitudes.nuevo', $ficha->id) }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Solicitud</a>
                    @endif

                    @if($ficha->usuario)
                        <a target='_blank' href='{{ route('manage.viajeros.area', $ficha->id) }}' class='btn btn-success btn-xs'><i class='fa fa-sign-in'></i> Area</a>
                    @endif

                    <a href="#" data-toggle="modal" data-target="#modalBooking-exam" class="btn btn-info btn-xs">Enviar Test</a> &nbsp;
                    @include('manage.viajeros._modal-exam', ['ficha'=> $ficha])

                </div>

                <div class="pull-right">

                    &nbsp;<a href="#" data-toggle='modal' data-target='#modalViajeroLog' class="btn btn-warning btn-xs">
                        <i class="fa fa-plus-circle"></i> Seguimiento
                    </a>
                    &nbsp;<a href="#" data-toggle='modal' data-target='#modalViajeroTarea' class="btn btn-danger btn-xs">
                        <i class="fa fa-plus-circle"></i> Nueva Tarea
                    </a>

                </div>

                @if( $errors->first('tarea_fecha') || $errors->first('tarea_hora') )
                    <script type="text/javascript">
                        $(document).ready( function() {
                            $("#modalViajeroTarea").modal('show');
                        });
                    </script>
                @endif

                <script type="text/javascript">
                    var $alertas = {{$ficha->alerta_booking_total?:0}};
                    @if($ficha->alerta_booking_total>1)
                        var $alertas_txt = "Ya existe/n {{$ficha->alerta_booking_total}} booking, el último con status '{{$ficha->alerta_booking->status_name}}' para este viajero. ¿Seguro que quieres crear uno nuevo o mejor primero miras que no lo estás duplicando?";
                    @elseif($ficha->alerta_booking_total==1)
                        var $alertas_txt = "Ya existe un booking con status '{{$ficha->alerta_booking->status_name}}' para este viajero. ¿Seguro que quieres crear uno nuevo o mejor primero miras que no lo estás duplicando?";
                    @endif
                </script>

            </div>
            <div class="portlet-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a data-label="Ficha" href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab"><i class="fa fa-pencil"></i></a></li>
                    <li role="presentation"><a data-label="Datos Médicos" href="#medicos" aria-controls="medicos" role="tab" data-toggle="tab"><i class="fa fa-user-md"></i></a></li>
                    @if($ficha->es_adulto)
                        <li role="presentation"><a data-label="Info Adultos" href="#info-adulto" aria-controls="info-adulto" role="tab" data-toggle="tab"><i class="fa fa-info-circle"></i></a></li>
                    @endif
                    <li role="presentation"><a href="#solicitud" aria-controls="solicitud" role="tab" data-toggle="tab">Solicitud</a></li>
                    <li role="presentation"><a href="#tutores" aria-controls="tutores" role="tab" data-toggle="tab">Tutores</a></li>
                    {{-- <li role="presentation"><a href="#facturas" aria-controls="facturas" role="tab" data-toggle="tab">D.Fact.</a></li> --}}
                    <li role="presentation"><a href="#tareas" aria-controls="tareas" role="tab" data-toggle="tab">Tareas</a></li>
                    <li role="presentation"><a href="#seguimiento" aria-controls="seguimiento" role="tab" data-toggle="tab">Seguimiento</a></li>
                    <li role="presentation"><a data-label="Historial" href="#historial" aria-controls="historial" role="tab" data-toggle="tab"><i class="fa fa-history"></i></a></li>

                    @if($ficha->bookings->count()>0)
                        <li role="presentation"><a data-label="Inscripciones" href="#bookings" aria-controls="bookings" role="tab" data-toggle="tab"><i class="fa fa-edit"></i></a></li>
                    @endif

                    <li role="presentation"><a data-label="Archivos" href="#archivos" aria-controls="archivos" role="tab" data-toggle="tab"><i class="fa fa-paperclip"></i></a></li>

                    @if($ficha->usuario)
                        <li role="presentation"><a data-label="Visitas" href="#visitas" aria-controls="visitas" role="tab" data-toggle="tab"><i class="fa fa-globe"></i></a></li>
                    @endif

                    @if( $ficha->exam_respuestas->count())
                        <li role="presentation"><a data-label="Tests" href="#tests" aria-controls="tests" role="tab" data-toggle="tab"><i class="fa fa-list-alt"></i></a></li>
                    @endif

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">
                        <div class="row">
                        <div class="col-md-12">
                            @include('manage.viajeros.ficha-ficha', ['ficha'=> $ficha])
                        </div>
                        </div>
                    </div>

                    @if( $ficha->exam_respuestas->count())
                        <div role="tabpanel" class="tab-pane fade" id="tests">
                            @include('manage.exams._respuestas', ['route'=> route('manage.exams.asks', [0, 'viajero' => $ficha->id]), ])
                        </div>
                    @endif

                    <div role="tabpanel" class="tab-pane fade in" id="medicos">
                        <div class="row">
                            <div class="col-md-12">
                            {!! Form::model($ficha, array('route' => array('manage.viajeros.ficha', $ficha->id))) !!}
                            @include('manage.viajeros.ficha-medicos', ['ficha'=> $ficha->datos])
                            {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                    @if($ficha->es_adulto)
                    <div role="tabpanel" class="tab-pane fade in" id="info-adulto">
                        <div class="row">
                            <div class="col-md-12">
                            {!! Form::model($ficha, array('route' => array('manage.viajeros.ficha', $ficha->id))) !!}
                            @include('manage.viajeros.ficha-adultos', ['ficha'=> $ficha->datos])
                            {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    @endif


                    <div role="tabpanel" class="tab-pane fade in" id="solicitud">

                        @if( $ficha->solicitud && !$ficha->solicitud->es_inscrito)
                            @include('manage.solicitudes.ficha_viajero', ['ficha'=> $ficha->solicitud])
                            <br><br>
                        @else
                            <br><br>
                            <strong>No hay Solicitudes o todas están archivadas/inscritas</strong>
                        @endif

                        <hr>
                        @include('manage.solicitudes.list_viajero', ['viajero_id'=> $ficha->id])


                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="tutores">

                        @include('manage.viajeros.ficha_tutores', ['ficha'=> $ficha])

                        <div class="row">
                        <div id="tutor-new-1" class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Importar Tutor existente
                                </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <div class="col-md-2">
                                            @include('includes.form_input_text', [ 'campo'=> 'tutor1_id', 'texto'=> 'ID','valor'=>''])
                                        </div>
                                        <div class="col-md-4">
                                            @include('includes.form_input_text', [ 'campo'=> 'tutor1', 'texto'=> 'Tutor','valor'=>''])
                                        </div>

                                        <div class="col-md-3">
                                            @include('includes.form_select', [ 'campo'=> 'tutor1_relacion', 'texto'=> 'Relación', 'valor'=> 1, 'select'=> ConfigHelper::getTutorRelacion()])
                                        </div>

                                        <div class="col-md-2">
                                            &nbsp;<br>
                                            <button id="btn-tutor-importar" data-label="Importar tutor" class="btn btn-success">Importar</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        </div>

                    </div>

                    {{-- <div role="tabpanel" class="tab-pane fade" id="facturas">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-database fa-fw"></i> Datos Facturación
                            </div>
                            <div class="panel-body">

                                {!! Form::model($ficha->datos, array('id'=>'formulario', 'route' => array('manage.viajeros.ficha.datos', $ficha->id))) !!}

                                    {!! Form::hidden('facturacion',true) !!}
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            @include('includes.form_input_text', [ 'campo'=> 'fact_nif', 'texto'=> 'NIF'])
                                        </div>
                                        <div class="col-md-4">
                                            @include('includes.form_input_text', [ 'campo'=> 'fact_razonsocial', 'texto'=> 'Razón Social'])
                                        </div>
                                        <div class="col-md-5">
                                            @include('includes.form_input_text', [ 'campo'=> 'fact_domicilio', 'texto'=> 'Domicilio'])
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            @include('includes.form_input_text', [ 'campo'=> 'fact_cp', 'texto'=> 'CP'])
                                        </div>
                                        <div class="col-md-4">
                                            @include('includes.form_input_text', [ 'campo'=> 'fact_poblacion', 'texto'=> 'Población'])
                                        </div>
                                        <div class="col-md-6">
                                            @include('includes.form_input_text', [ 'campo'=> 'fact_concepto', 'texto'=> 'Concepto'])
                                        </div>
                                    </div>

                                    @include('includes.form_submit', [ 'permiso'=> 'viajeros', 'texto'=> 'Guardar'])

                                {!! Form::close() !!}

                            </div>
                        </div>

                        @include('manage.viajeros.facturas.list', ['viajero_id'=> $ficha->id])
                    </div> --}}

                    <div role="tabpanel" class="tab-pane fade" id="tareas">
                        @include('manage.viajeros.tareas.list', ['viajero_id'=> $ficha->id])
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="seguimiento">
                        @include('manage.viajeros.logs.list', ['viajero_id'=> $ficha->id, 'todos'=> 0])
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="historial">
                        @include('manage.viajeros.logs.list', ['viajero_id'=> $ficha->id, 'todos'=> true])
                    </div>

                    @if($ficha->bookings->count()>0)
                    <div role="tabpanel" class="tab-pane fade" id="bookings">
                        @include('manage.bookings.list_viajero', ['viajero_id'=> $ficha->id])
                    </div>
                    @endif

                    <div role="tabpanel" class="tab-pane fade" id="archivos">
                        <div class="row">
                        <div class="col-md-12">

                        <?php
                        $path = storage_path("files/viajeros/" . $ficha->id);
                        $folder = "/files/viajeros/" . $ficha->id;
                        ?>

                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h4>Archivos <i>(Visibles en el área de cliente)</i></h4>
                                </div>
                                <div class="panel-body">

                                    <div class="row">
                                    <div class="col-md-12">
                                    <?php
                                    foreach($ficha->archivos->where('booking_id',0) as $archivo)
                                    {
                                        if($archivo->visible==0)
                                        {
                                            continue;
                                        }

                                        $result = $archivo->doc;
                                        $file = $path . '/' . $result;

                                        $btn_class = "btn-xs btn-danger";
                                        if($archivo->visible==1)
                                        {
                                            $btn_class = "btn-success";
                                        }

                                        // getimagesize

                                        if( is_file($file) )
                                        {
                                            // if(getimagesize($file))
                                            // {
                                            //     echo '
                                            //     <div class="col-md-4">
                                            //         <div class="thumbnail">
                                            //             <img src="'.$folder . '/' . $result.'" alt="">
                                            //             <div class="caption">
                                            //                 <p>
                                            //                     <a href="'. route('manage.viajeros.archivo.delete', [$archivo->id]).'" class="btn btn-danger btn-xs" role="button">Borrar</a>
                                            //                     <a href="'. route('manage.viajeros.archivo.visible', [$archivo->id]).'" class="btn '. $btn_class .' pull-right" role="button">Visible</a>
                                            //                 </p>
                                            //             </div>
                                            //         </div>
                                            //     </div>';
                                            // }
                                            // else
                                            {
                                                echo '
                                                  <div class="col-md-4">
                                                  <div class="thumbnail">
                                                  <small>'.$result.'</small>
                                                  <hr>
                                                  <a class="btn btn-success" href="'.$folder . '/' . $result.'" target="_blank">
                                                  <i class="fa fa-download"></i> Download</a>
                                                  <p>
                                                    <a href="'. route('manage.viajeros.archivo.delete', [$archivo->id]).'" class="btn btn-danger btn-xs" role="button">Borrar</a>
                                                    <a href="'. route('manage.viajeros.archivo.visible', [$archivo->id]).'" class="btn '. $btn_class .' pull-right" role="button">Visible</a>
                                                  </p>
                                                  </div>
                                                  </div>';
                                            }
                                        }
                                    }
                                    ?>
                                    </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                            <div class="form-group">
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4>Añadir archivos visibles (img/pdf)</h4>
                                                    </div>

                                                    <div class="dropzone" id="myDropzone1"></div>

                                                </div>
                                            </div>
                                            @include('includes.script_dropzone', ['name'=> 'myDropzone1', 'url'=> route('manage.viajeros.upload', [$ficha->id,1])])
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h4>Archivos <i>(Visibles en el área de cliente)</i></h4>
                                </div>
                                <div class="panel-body">

                                    <div class="row">
                                    <div class="col-md-12">
                                    <?php
                                    foreach($ficha->archivos->where('booking_id',0) as $archivo)
                                    {
                                        if($archivo->visible==1)
                                        {
                                            continue;
                                        }

                                        $result = $archivo->doc;
                                        $file = $path . '/' . $result;

                                        $btn_class = "btn-xs btn-danger";
                                        if($archivo->visible==1)
                                        {
                                            $btn_class = "btn-success";
                                        }

                                        // getimagesize

                                        if( is_file($file) )
                                        {
                                            // if(getimagesize($file))
                                            // {
                                            //     echo '
                                            //     <div class="col-md-4">
                                            //         <div class="thumbnail">
                                            //             <img src="'.$folder . '/' . $result.'" alt="">
                                            //             <div class="caption">
                                            //                 <p>
                                            //                     <a href="'. route('manage.viajeros.archivo.delete', [$archivo->id]).'" class="btn btn-danger btn-xs" role="button">Borrar</a>
                                            //                     <a href="'. route('manage.viajeros.archivo.visible', [$archivo->id]).'" class="btn '. $btn_class .' pull-right" role="button">Visible</a>
                                            //                 </p>
                                            //             </div>
                                            //         </div>
                                            //     </div>';
                                            // }
                                            // else
                                            {
                                                echo '
                                                  <div class="col-md-4">
                                                  <div class="thumbnail">
                                                  <small>'.$result.'</small>
                                                  <hr>
                                                  <a class="btn btn-success" href="'.$folder . '/' . $result.'" target="_blank">
                                                  <i class="fa fa-download"></i> Download</a>
                                                  <p>
                                                    <a href="'. route('manage.viajeros.archivo.delete', [$archivo->id]).'" class="btn btn-danger btn-xs" role="button">Borrar</a>
                                                    <a href="'. route('manage.viajeros.archivo.visible', [$archivo->id]).'" class="btn '. $btn_class .' pull-right" role="button">Visible</a>
                                                  </p>
                                                  </div>
                                                  </div>';
                                            }
                                        }
                                    }
                                    ?>
                                    </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                            <div class="form-group">
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4>Añadir archivos NO visibles (img/pdf)</h4>
                                                    </div>

                                                    <div class="dropzone" id="myDropzone0"></div>

                                                </div>
                                            </div>
                                            @include('includes.script_dropzone', ['name'=> 'myDropzone0', 'url'=> route('manage.viajeros.upload', [$ficha->id,0])])
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        </div>

                    </div>

                    @if($ficha->usuario)
                    <div role="tabpanel" class="tab-pane fade" id="visitas">

                        @if($ficha->usuario->visitas->count())

                            {!! Datatable::table()
                                ->addColumn([
                                  'fecha'   => 'Fecha',
                                  'curso'   => 'Curso',
                                  'visitas' => 'Visitas',
                                ])
                                ->setUrl(route('manage.viajeros.trafico', $ficha->id))
                                ->setOptions('iDisplayLength', 100)
                                ->setOptions(
                                  "columnDefs", array(
                                    // [ "sortable" => false, "targets" => [10] ],
                                    [ "targets" => [0], "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                                  )
                                )
                                ->render()
                            !!}

                        @else
                            - No hay visitas contabilizadas -
                        @endif

                    </div>
                    @endif

                </div>

            </div>
        </div>

    </div>

    <div class="col-md-3 sortable column sidebar-panels">

        <div class="portlet portlet-sortable box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info fa-fw"></i>
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                Contactos: {{$ficha->seguimientos_enviados->count()}} / Respuestas: {{$ficha->seguimientos_recibidos->count()}}
                
                @if($ficha->solicitud)
                    <hr>
                    Solicitud: {{$ficha->solicitud->categoria_name}} :: {{$ficha->solicitud->notas}}
                @endif

                @if($ficha->tareas_last)
                    <?php
                        $tarea = $ficha->tareas_last;
                        $fecha = Carbon::parse($tarea->fecha);
                    ?>
                    <hr>
                    <a data-label='Completar' href="{{ route('manage.viajeros.tareas.completar',[$tarea->id,1])}}" class='btn btn-success btn-xs'><i class='fa fa-check'></i></a>
                    Tarea: {{$fecha->format('d/m/Y H:i')}} :: {{$tarea->tipo}} : {{$tarea->notas}}
                @endif

            </div>
        </div>

        <div class="portlet portlet-sortable box purple-plum">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-tag fa-fw"></i> Status ( {{$ficha->status_tipo}} ) :: {{$ficha->status_name}}
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">

                @if( $ficha->es_booking && $ficha->booking && ($ficha->booking->es_cancelado || $ficha->booking->es_refund) )
                    - NO SE PUEDE CAMBIAR MANUALMENTE -
                @else

                    {!! Form::model($ficha, array('route' => array('manage.viajeros.ficha', $ficha->id))) !!}

                        {!! Form::hidden('archivar_motivo',null) !!}
                        {!! Form::hidden('archivar_motivo_nota',null) !!}

                        @include('includes.form_select', ['campo'=> 'status_id',
                            'valor'=> ($ficha->es_booking?$ficha->booking_status_id:$ficha->status_id), 'texto'=> 'Cambiar Status', 'select'=> $statuses])

                        <div class="row">
                            <div class="col-sm-12">
                            {!! Form::submit('Cambiar', array( 'name'=>'submit_status', 'class' => 'btn btn-xs btn-success pull-right', 'id'=> 'submit_status')) !!}

                            </div>
                        </div>
                    {!! Form::close() !!}

                @endif

                <br>
                <hr>

                @include('includes.checklist', ['ficha'=> $ficha, 'booking_id'=> $ficha->booking_id])

            </div>
        </div>

        <div class="portlet portlet-sortable box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bookmark fa-fw"></i> Asignado a
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                {!! Form::model($ficha, array('route' => array('manage.viajeros.ficha', $ficha->id))) !!}
                    @include('includes.form_select', ['campo'=> 'asign_to', 'texto'=> null, 'select'=> $asignados])
                <div class="row">
                    <div class="col-sm-12">
                        {!! Form::submit('Cambiar', array( 'name'=>'submit_asignar', 'class' => 'btn btn-xs btn-success pull-right')) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

        <div class="portlet portlet-sortable box blue-steel">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bookmark fa-fw"></i> Prescriptor Booking
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                {!! Form::model($ficha, array('route' => array('manage.viajeros.ficha', $ficha->id))) !!}
                    @include('includes.form_select_chosen', ['campo'=> 'prescriptor_id', 'texto'=> null, 'select'=> $prescriptores])
                <div class="row">
                    <div class="col-sm-12">
                        {!! Form::submit('Cambiar', array( 'name'=>'submit_prescriptor', 'class' => 'btn btn-xs btn-success pull-right')) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

        <div class="portlet portlet-sortable box yellow-gold">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-tasks fa-fw"></i> Tareas
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
            </div>
        </div>

        <div class="portlet portlet-sortable box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bookmark fa-fw"></i> Notas Internas
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                {!! Form::model($ficha, array('route' => array('manage.viajeros.ficha', $ficha->id))) !!}
                    @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> 'Notas internas', 'novisible'=>true, 'valor'=> $ficha->notas])
                <div class="row">
                    <div class="col-sm-12">
                        {!! Form::submit('Guardar', array( 'name'=>'submit_notas', 'class' => 'btn btn-xs btn-danger pull-right')) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>


        <div class="portlet portlet-sortable box grey-silver">
            <div class="portlet-title">
                <div class="caption">
                <i class="fa fa-history fa-fw"></i> Historial
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="modalBookingUser">
<div class="modal-dialog modal-md">
    <div class="modal-content">

        {!! Form::open(array('method' => 'GET', 'url' => route('manage.bookings.nuevo', $ficha->id), 'role' => 'form', 'class' => '')) !!}

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class="fa fa-history fa-fw"></i> Viajero asignado a otro Usuario:</h4>
        </div>
        <div class="modal-body">

                <div class="form-group">
                    El booking quedará asignado a:
                </div>
                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'booking_user', 'texto'=> 'Usuario', 'valor'=> $ficha->asign_to, 'select'=> $asignados ])
                </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            {!! Form::submit('Aceptar', array('class' => 'btn btn-success')) !!}
        </div>
        {!! Form::close() !!}

    </div>
</div>
</div>

@include('manage.viajeros.logs.new')
@include('manage.viajeros.tareas.new')

@include('includes.script_categoria')
@include('includes.script_conocido')
@include('includes.script_tutores', ['viajero_id' => $ficha->id])
@include('includes.script_stars', ['rating'=> $ficha->rating])
@include('includes.script_sortable_portlets')

<script type="text/javascript">
var $error_MailTutor = {{$error_MailTutor?1:0}};
var $url_presupuesto = "{{route('manage.bookings.presupuesto', $ficha->id)}}";

function booking_new()
{
    if($error_MailTutor)
    {
        bootbox.confirm("El/Los Tutor/es no tienen un email válido.", function(result) {
            if(result)
            {
                $('#modalBookingUser').modal('show');
            }
        });
    }
    else
    {
        if($alertas)
        {
            bootbox.confirm({
                message: $alertas_txt,
                buttons: {
                    confirm: {
                        label: 'Continuar',
                        className: 'btn-success'
                    },
                },
                callback: function(result) {
                    if(result)
                    {
                        $('#modalBookingUser').modal('show');
                    }
                }
            });
        }
        else
        {
            $('#modalBookingUser').modal('show');
        }
    }
}

$(document).ready( function() {
    $('#booking-new').click(function(e) {
        e.preventDefault();

        booking_new();

    });

    $('#booking-no').click(function(e) {
        e.preventDefault();

        bootbox.alert("No se puede crear una Inscripción sin una Solicitud.");
    });

    $('#booking-sin-solicitud').click(function(e) {
        e.preventDefault();

        bootbox.confirm("No hay solicitud abierta. ¿Estás seguro que quieres crear un booking sin solicitud?.", function(result) {
            if(result)
            {
                booking_new();
            }
        });

    });

    $('#booking-presupuesto').click(function(e) {
        e.preventDefault();

        window.location = $url_presupuesto;

        // if($alertas)
        // {
        //     bootbox.confirm({
        //         message: $alertas_txt,
        //         buttons: {
        //             confirm: {
        //                 label: 'Continuar',
        //                 className: 'btn-success'
        //             },
        //         },
        //         callback: function(result) {
        //             if(result)
        //             {
        //                 $('#modalBookingUser').modal('show');
        //             }
        //         }
        //     });
        // }
        // else
        // {
        //     window.location = $url_presupuesto;
        // }
    });
});
</script>

@stop