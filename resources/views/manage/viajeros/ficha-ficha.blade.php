{!! Form::model($ficha, array('id'=> 'frmValidar' ,'route' => array('manage.viajeros.ficha', $ficha->id))) !!}

    {!! Form::hidden('rating',$ficha->rating, array('class' => 'rating')) !!}

@if(ConfigHelper::canEdit('viajeros'))
<div class="form-group row">
    <div class="col-sm-12">
        @include('includes.form_submit', [ 'permiso'=> 'viajeros', 'texto'=> 'Guardar'])
    </div>
</div>
@endif


<div class="form-group">
    @if(ConfigHelper::canEdit('oficinas'))
        @include('includes.form_select2', [ 'campo'=> 'oficina_id', 'texto'=> 'Oficina', 'select'=> $oficinas, 'help'=> 'Recuerda que este cambio de oficina NO afecta a Bookings ya hechos de este Viajero'])
    @else
        Oficina: {{$ficha->oficina?$ficha->oficina->name:"-"}}
    @endif
</div>

<div class="portlet light bordered">
    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre'])
        </div>
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'lastname', 'texto'=> 'Apellido'])
        </div>
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'lastname2', 'texto'=> 'Apellido2'])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-3">
            {!! Form::label('sexo', 'Sexo') !!}:<br>
            <label class="radio-inline">
            {!! Form::radio('sexo', '1', ($ficha->sexo==1)) !!} <i class="fa fa-male"></i>
            </label>
            <label class="radio-inline">
            {!! Form::radio('sexo', '2', ($ficha->sexo==2)) !!} <i class="fa fa-female"></i>
            </label>
            <span class="help-block">{{ $errors->first('sexo') }}</span>
        </div>
        <div class="col-md-1">
            @include('includes.form_checkbox', [ 'campo'=> 'es_adulto', 'texto'=> 'Adulto'])
        </div>
        <div class="col-md-4">
            @include('includes.form_input_datetime', [ 'campo'=> 'fechanac', 'texto'=> 'Fecha Nac.'])
        </div>
        <div class="col-md-4">
            {{-- @include('includes.form_select2', [ 'campo'=> 'paisnac', 'texto'=> "País Nac.", 'select'=> ConfigHelper::getPaises()]) --}}
            @include('includes.form_select_pais', [ 'campo'=> 'paisnac', 'texto'=> 'País Nac.'])
        </div>
    </div>
</div>

@if($ficha->usuario)
<div class="portlet light bordered">
    [Username: {{$ficha->usuario->username}}]
    (Último login: {{$ficha->usuario->login_attempts>0?Carbon::parse($ficha->usuario->login_last)->format('d/m/Y H:i:s'):""}} de {{$ficha->usuario->login_attempts}})
</div>
@endif

<div class="portlet light bordered">
<div class="portlet-title tabbable-line">
    <div class="caption font-blue-sharp">Datos muy relevantes</div>
    <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
</div>
<div class="portlet-body flip-scroll">
    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_select', [ 'campo'=> 'nacionalidad', 'texto'=> 'Nacionalidad', 'select'=> [""=>""] + \VCN\Models\Nacionalidad::pluck('name','name')->toArray(), 'ficha'=> $ficha->datos])
        </div>
        @if(ConfigHelper::config('nif'))
        <div class="col-md-2">
            @include('includes.form_select', [ 'campo'=> 'tipodoc', 'texto'=> "Tipo", 'select'=> ConfigHelper::getTipoDoc(), 'ficha'=> $ficha->datos ])
        </div>
        <div class="col-md-6">
            @include('includes.form_input_text', [ 'campo'=> 'documento', 'texto'=> 'DNI', 'ficha'=> $ficha->datos])
        </div>
        @endif
    </div>

    <div class="form-group row">
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'pasaporte', 'texto'=> 'Pasaporte', 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-3">
            @include('includes.form_select_pais', [ 'campo'=> 'pasaporte_pais', 'texto'=> 'País pasaporte', 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-3">
            @include('includes.form_input_datetime', [ 'campo'=> 'pasaporte_emision', 'texto'=> 'F.Emisión pasaporte', 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-3">
            @include('includes.form_input_datetime', [ 'campo'=> 'pasaporte_caduca', 'texto'=> 'F.Caducidad pasaporte', 'ficha'=> $ficha->datos])
        </div>
    </div>
</div>
</div>

<div class="portlet light bordered">
<div class="portlet-title tabbable-line">
    <div class="caption font-blue-sharp">¿Cómo nos has conocido?</div>
    <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
</div>
<div class="portlet-body flip-scroll">
    @if($ficha->bookings->count()<=0 || ConfigHelper::canEdit('booking-cambio-origen'))
        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_select2', [ 'campo'=> 'origen_id', 'texto'=> 'Origen', 'select'=> $conocidos])
            </div>
            <div class="col-md-4">
                @include('includes.form_select2', [ 'campo'=> 'suborigen_id', 'texto'=> 'Sub-Origen', 'select'=> $subconocidos])
            </div>
            <div class="col-md-4">
                @include('includes.form_select2', [ 'campo'=> 'suborigendet_id', 'texto'=> 'Sub-Origen Detalle', 'select'=> $subconocidosdet])
            </div>
        </div>
    @else
        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_select', [ 'campo'=> 'origen_id', 'disabled'=> true, 'texto'=> 'Origen', 'select'=> $conocidos])
            </div>
            <div class="col-md-4">
                @include('includes.form_select', [ 'campo'=> 'suborigen_id', 'disabled'=> true, 'texto'=> 'Sub-Origen', 'select'=> $subconocidos])
            </div>
            <div class="col-md-4">
                @include('includes.form_select', [ 'campo'=> 'suborigendet_id', 'disabled'=> true, 'texto'=> 'Sub-Origen Detalle', 'select'=> $subconocidosdet])
            </div>
        </div>
    @endif
</div>
</div>


<div class="portlet light bordered">
<div class="portlet-title tabbable-line">
    <div class="caption font-blue-sharp">Datos de contacto</div>
    <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
</div>
<div class="portlet-body flip-scroll">

    <div class="form-group row">
        @if( count(ConfigHelper::idiomas())>1 )
        <div class="col-md-4">
            @include('includes.form_select', [ 'campo'=> 'idioma_contacto', 'texto'=> 'Idioma contacto', 'select'=> ConfigHelper::getIdiomaContacto(), 'valor'=> $ficha->datos?$ficha->datos->idioma_contacto:""])
        </div>
        @endif
        <div class="form-group">
            <div class="col-md-7">
                @if(ConfigHelper::config('es_rgpd'))
                    @include('includes.form_checkbox', [ 'campo'=> 'lopd_check2', 'texto'=> trans('area.booking.lopd_check2', ['plataforma'=> ConfigHelper::plataformaApp()]), 'html'=> true ])
                @else
                    @include('includes.form_checkbox', [ 'campo'=> 'optout', 'texto'=> trans('area.optout'), 'html'=> true ])
                @endif

                @if($ficha->optout && $ficha->optout_fecha)
                    (Desde el {{$ficha->optout_fecha->format('d/m/Y')}})
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'E-mail'])
        </div>
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'phone', 'texto'=> 'Teléfono'])
        </div>
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'movil', 'texto'=> 'Móvil'])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-2">
            @include('includes.form_select', [ 'campo'=> 'tipovia', 'texto'=> 'Tipo', 'select'=> ConfigHelper::getTipoVia(), 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'direccion', 'texto'=> 'Dirección', 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'ciudad', 'texto'=> 'Ciudad', 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-2">
            @include('includes.form_input_text', [ 'campo'=> 'cp', 'texto'=> 'CP', 'ficha'=> $ficha->datos])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-6">
            @include('includes.form_select', [ 'campo'=> 'provincia', 'texto'=> 'Provincia', 'select'=>[""=>""]+ \VCN\Models\Provincia::pluck('name','name')->toArray(), 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-6">
            {{-- @include('includes.form_select', [ 'campo'=> 'pais_id', 'texto'=> 'País', 'select'=> ConfigHelper::getPaises(), 'ficha'=> $ficha->datos]) --}}
            @include('includes.form_select_pais', [ 'campo'=> 'pais', 'texto'=> 'País', 'ficha'=> $ficha->datos])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-8">
            @include('includes.form_input_text', [ 'campo'=> 'telefonos', 'texto'=> 'Otros teléfonos', 'ficha'=> $ficha->datos])
        </div>
    </div>

    @if($ficha->es_adulto)
        <div class="form-group row">
            <div class="col-md-6">
                @include('includes.form_input_text', [ 'campo'=> 'emergencia_contacto', 'texto'=> 'Contacto emergencia'])
            </div>
            <div class="col-md-6">
                @include('includes.form_input_text', [ 'campo'=> 'emergencia_telefono', 'texto'=> 'Teléfono emergencia'])
            </div>
        </div>
    @endif

    <hr>
    <div class="form-group row">
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'rs_instagram', 'texto'=> 'Instagram', 'ficha'=> $ficha->datos])
        </div>
    </div>

</div>
</div>

<div class="portlet light bordered">
<div class="portlet-title tabbable-line">
    <div class="caption font-blue-sharp">Datos académicos</div>
    <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
</div>
<div class="portlet-body flip-scroll">
    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_select', [ 'campo'=> 'idioma', 'texto'=> 'Idioma', 'select'=> ConfigHelper::getIdioma(), 'ficha'=> $ficha->datos ])
        </div>
        <div class="col-md-4">
            @include('includes.form_select', [ 'campo'=> 'idioma_nivel', 'texto'=> 'Nivel de idioma', 'select'=> ConfigHelper::getIdiomaNivel(), 'ficha'=> $ficha->datos ])
        </div>
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'titulacion', 'texto'=> 'Titulación/Estudios', 'ficha'=> $ficha->datos ])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'escuela', 'texto'=> 'Escuela', 'ficha'=> $ficha->datos ])
        </div>
        <div class="col-md-2">
            @include('includes.form_select', [ 'campo'=> 'escuela_curso', 'texto'=> 'Curso académico actual', 'select'=> ConfigHelper::getEscuelaCurso(), 'ficha'=> $ficha->datos ])
        </div>
    </div>

    @include('includes.form_div_bool', [ 'campo'=> 'ingles_academia', 'ficha'=> $ficha->datos, 'nombre'=> '¿Estudias inglés en alguna academia?' ])
    @include('includes.script_boolean', ['campo'=> 'ingles_academia', 'required'=>0 ])

    @if($ficha->es_cic)
    <div class="form-group row">
        <div class="col-md-5">
            @include('includes.form_checkbox', [ 'campo'=> 'cic', 'texto'=> 'Estudias en un centro de idiomas del CIC', 'ficha'=> $ficha->datos ])
        </div>
        <div id="cic_nivel_div" class="col-md-5" style="display:none;">
            @include('includes.form_select', [ 'campo'=> 'cic_nivel', 'texto'=> 'Nivel', 'select'=> ConfigHelper::getCICNivel(), 'ficha'=> $ficha->datos ])
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12">
            @include('includes.form_select', [ 'campo'=> 'cic_thau', 'texto'=> '¿Vas a la escuela Thau Barcelona o Sant Cugat?', 'select'=> ConfigHelper::getCicThau(), 'ficha'=> $ficha->datos ])
        </div>
    </div>
    @endif

    <div class="input-group">
        <span class="input-group-addon">¿Ha realizado algún curso de idiomas en el extranjero con anterioridad?</span>
        <div class="pull-left form-radio">
            <label class="radio-inline">
                {!! Form::radio('curso_anterior_bool', '1', ($ficha->datos->curso_anterior!="")) !!} SI
            </label>
            <label class="radio-inline">
                {!! Form::radio('curso_anterior_bool', '0', ($ficha->datos->curso_anterior=="")) !!} NO
            </label>
        </div>

        <div id="curso_anterior_div" style="display:none;">
            <br>
            @include('includes.form_input_text', [ 'campo'=> 'curso_anterior', 'ficha'=> $ficha->datos ])
        </div>
        <span class="help-block">{{ $errors->first('curso_anterior') }}</span>
    </div>

    <br>
    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_checkbox', [ 'campo'=> 'trinity_exam', 'texto'=> 'Has hecho el Trinity Exam', 'ficha'=> $ficha->datos ])
        </div>
        <div id="trinity_exam_div" style="display:none;">
            <div class="col-md-2">
                @include('includes.form_input_text', [ 'campo'=> 'trinity_any', 'texto'=> 'Año', 'ficha'=> $ficha->datos ])
            </div>
            <div class="col-md-6">
                @include('includes.form_select', [ 'campo'=> 'trinity_nivel', 'texto'=> 'Nivel', 'select'=> ConfigHelper::getTrinityNivel(), 'ficha'=> $ficha->datos ])
            </div>
        </div>
    </div>

    <br>

    <div class="form-group">
        @include('includes.form_textarea', [ 'campo'=> 'hobby', 'texto'=> 'Hobby', 'ficha'=> $ficha->datos])
    </div>
</div>
</div>

<div class="portlet light bordered">
<div class="portlet-title tabbable-line">
    <div class="caption font-blue-sharp">Hermanos</div>
    <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
</div>
<div class="portlet-body flip-scroll">

    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'h1_nom', 'texto'=> 'Hermano 1', 'ficha'=> $ficha->datos ])
        </div>
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'h1_fnac', 'texto'=> 'Hermano 1 Fecha Nac.', 'ficha'=> $ficha->datos ])
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'h2_nom', 'texto'=> 'Hermano 2', 'ficha'=> $ficha->datos ])
        </div>
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'h2_fnac', 'texto'=> 'Hermano 2 Fecha Nac.', 'ficha'=> $ficha->datos ])
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'h3_nom', 'texto'=> 'Hermano 3', 'ficha'=> $ficha->datos ])
        </div>
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'h3_fnac', 'texto'=> 'Hermano 3 Fecha Nac.', 'ficha'=> $ficha->datos ])
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'h4_nom', 'texto'=> 'Hermano 4', 'ficha'=> $ficha->datos ])
        </div>
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'h4_fnac', 'texto'=> 'Hermano 4 Fecha Nac.', 'ficha'=> $ficha->datos ])
        </div>
    </div>

</div>
</div>


@if(ConfigHelper::canEdit('viajeros'))
<hr>
<div class="form-group row">
    <div class="col-md-2">
        <a href="{{route('manage.viajeros.archivar',[$ficha->id, $ficha->archivado])}}" class="btn btn-warning">
            {{$ficha->archivado ? "Recuperar" : "Archivar"}}
        </a>
    </div>
    <div class="col-md-2">
        @if(ConfigHelper::canEdit('viajero-eliminar'))
            <a href='#destroy' data-label='Borrar' data-model='Viajero' data-action="{{route( 'manage.viajeros.delete', $ficha->id)}}" data-toggle='modal' data-target='#modalDestroy' class="btn btn-danger"><i class='fa fa-times-circle'></i> Eliminar</a>
        @endif
    </div>
    <div class="col-sm-8">
        @include('includes.form_submit', [ 'permiso'=> 'viajeros', 'texto'=> 'Guardar'])
    </div>
</div>
@endif


{!! Form::close() !!}

@include('includes.script_boolean', ['campo'=> 'curso_anterior', 'required'=>0 ])

<script type="text/javascript">
$(document).ready(function() {

    @if($ficha->es_cic)
        @if($ficha->datos->cic)
            $('#cic_nivel_div').show();
        @endif

        $("#cic").click(function(){
            if( $(this).is(':checked') )
            {
                $('#cic_nivel_div').show();
            }
            else
            {
                $('#cic_nivel_div').hide();
            }
        });
    @endif

    @if($ficha->datos->trinity_exam)
        $('#trinity_exam_div').show();
    @endif

    $("#trinity_exam").click(function(){
        if( $(this).is(':checked') )
        {
            $('#trinity_exam_div').show();
        }
        else
        {
            $('#trinity_exam_div').hide();
        }
    });

});
</script>