@extends('layouts.manage')


@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-flag fa-fw"></i> Nueva Ciudad
        </div>
        <div class="panel-body">


            {!! Form::open(array('method' => 'POST', 'url' => route('manage.ciudades.ficha',0), 'role' => 'form', 'class' => '')) !!}

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'country_id', 'texto'=> 'País', 'valor'=> $pais_id, 'select'=> $paises])
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'provincia_id', 'texto'=> 'Provincia', 'valor'=> $provincia_id, 'select'=> $provincias])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'city_name', 'texto'=> 'Ciudad'])
                </div>

                @include('includes.form_submit', [ 'permiso'=> 'tablas', 'texto'=> 'Guardar'])

            {!! Form::close() !!}

        </div>
    </div>

@stop