@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Ciudad :: {{$ficha->city_name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">País</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">
                        {!! Form::model($ficha, array('route' => array('manage.ciudades.ficha', $ficha->id))) !!}

                        <div class="form-group">
                            @include('includes.form_select', [ 'campo'=> 'country_id', 'texto'=> 'País', 'valor'=> $ficha->country_id, 'select'=> $paises])
                        </div>



                        <div class="form-group">
                            @include('includes.form_input_text', [ 'campo'=> 'city_name', 'texto'=> 'Ciudad'])
                        </div>

                        @include('includes.form_submit', [ 'permiso'=> 'tablas', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'Ciudad',
                                'campos_text'=> [ ['city_name'=> 'Nombre'], ],
                                'campos_textarea'=> []
                            ])

                    </div>

                </div>

            </div>
        </div>

@stop