@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Ciudades
                <span class="pull-right"><a href="{{ route('manage.ciudades.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Ciudad</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Ciudad',
                      'provincia'            => 'Provincia',
                      //'pais'            => 'Pais',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.ciudades.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [2] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop