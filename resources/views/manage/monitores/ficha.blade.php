@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-users fa-fw"></i> Monitor :: {{$ficha->name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Ficha</a></li>
                    <li role="presentation"><a href="#relaciones" aria-controls="relaciones" role="tab" data-toggle="tab"><i class="fa fa-list fa-fw"></i>Relaciones</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::open(array('method' => 'POST', 'files'=>true, 'url' => route('manage.monitores.ficha',$ficha->id), 'role' => 'form', 'class' => '')) !!}

                            <div class="form-group row">
                                <div class="col-md-8">
                                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Monitor'])
                                </div>
                                <div class="col-md-4">
                                @include('includes.form_input_file', [ 'campo'=> 'foto', 'texto'=> 'Foto'])
                                </div>
                            </div>

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'E-mail'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_textarea_tinymce', [ 'campo'=> 'notas', 'texto'=> 'Descripción' ])
                            </div>

                            <div class="form-group row">
                                <div class="col-md-2">
                                    @include('includes.form_checkbox', [ 'campo'=> 'es_director', 'texto'=> 'Director'])
                                </div>
                            </div>

                            @include('includes.form_plataforma', ['campo'=> 'plataforma', 'todas'=> true])

                            <div class="form-group row">
                                <div class="col-md-8">
                                    @if(!$ficha->usuario)
                                        <a class="btn btn-info" href="{{ route('manage.monitores.user', $ficha->id) }}">Generar usuario</a>
                                    @else
                                    [Username: {{$ficha->usuario->username}}]
                                    {{-- <a target='_blank' href='{{ route('manage.monitores.area', $ficha->id)}}' class='btn btn-success btn-xs'><i class='fa fa-sign-in'></i> Area</a> --}}
                                    <br>
                                    (Último login: {{$ficha->usuario->login_attempts>0?Carbon::parse($ficha->usuario->login_last)->format('d/m/Y H:i:s'):""}} de {{$ficha->usuario->login_attempts}})
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_submit', [ 'permiso'=> 'monitores', 'texto'=> 'Guardar'])
                                </div>
                            </div>

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="relaciones">
                        {!! Datatable::table()
                            ->addColumn([
                              'tipo'   => 'Tipo',
                              'relacion'   => 'Relación',
                              'options'  => ''
                            ])
                            ->setUrl(route('manage.monitores.relaciones', $ficha->id))
                            ->setOptions('iDisplayLength', 100)
                            ->setOptions(
                              "aoColumnDefs", array(
                                [ "bSortable" => false, "aTargets" => [2] ]
                              )
                            )
                            ->render() !!}
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'Monitor',
                                'campos_text'=> [
                                    //['name'=> 'Nombre'],
                                ],
                                'campos_textarea'=> [
                                    ['notas'=> 'Descripción'],
                                ]
                            ])

                    </div>

                </div>

            </div>
        </div>

@stop