@extends('layouts.manage')


@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-users fa-fw"></i> Monitores
            <span class="pull-right"><a href="{{ route('manage.monitores.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Monitor</a></span>
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                  'name'   => 'Monitor',
                  'relaciones' => 'Relaciones',
                  'user'      => 'Usuario',
                  'options'   => '',
                ])
                ->setUrl(route('manage.monitores.index'))
                ->setOptions('iDisplayLength', 100)
                ->setOptions(
                  "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [3] ]
                  )
                )
                ->render() !!}

        </div>
    </div>

@stop