@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-users fa-fw"></i> Nuevo Monitor
            </div>
            <div class="panel-body">


                {!! Form::open(array('method' => 'POST', 'files'=>true, 'url' => route('manage.monitores.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group row">
                        <div class="col-md-8">
                            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Monitor'])
                        </div>
                        <div class="col-md-4">
                        @include('includes.form_input_file', [ 'campo'=> 'foto', 'texto'=> 'Foto'])
                        </div>
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> 'E-mail'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_textarea_tinymce', [ 'campo'=> 'notas', 'texto'=> 'Descripción' ])
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2">
                            @include('includes.form_checkbox', [ 'campo'=> 'es_director', 'texto'=> 'Director'])
                        </div>
                    </div>

                    @include('includes.form_plataforma', ['campo'=> 'plataforma', 'todas'=> true])

                    @include('includes.form_submit', [ 'permiso'=> 'monitores', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@stop