@extends('layouts.manage')


@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-flag fa-fw"></i> Nueva Provincia
        </div>
        <div class="panel-body">


            {!! Form::open(array('method' => 'POST', 'url' => route('manage.provincias.ficha',0), 'role' => 'form', 'class' => '')) !!}

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'pais_id', 'texto'=> 'País', 'valor'=> $id, 'select'=> $paises])
                </div>

                <div class="form-group">
                    @include('includes.form_select', [ 'campo'=> 'idioma_contacto', 'texto'=> 'Idioma contacto', 'select'=> ConfigHelper::getIdiomaContacto()])
                </div>

                <div class="form-group">
                    @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Provincia'])
                </div>

                @include('includes.form_submit', [ 'permiso'=> 'tablas', 'texto'=> 'Guardar'])

            {!! Form::close() !!}

        </div>
    </div>

@stop