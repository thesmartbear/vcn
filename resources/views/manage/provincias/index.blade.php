@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-flag fa-fw"></i> Provincias
                <span class="pull-right"><a href="{{ route('manage.provincias.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Provincia</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Provincia',
                      'pais'            => 'Pais',
                      'idioma_contacto' => 'Idioma contacto',
                      'oficina'         => 'Oficina',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.provincias.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [4] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop