
@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-money fa-fw"></i> Extras Genéricos
                <span class="pull-right"><a href="{{ route('manage.extras.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nuevo Extra Genérico</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name' => 'Nombre',
                      'unidad' => 'Unidad',
                      'generic_price' => 'Importe',
                      'categorias' => 'Categorías',
                      'requerido' => 'Obligatorio',
                      'es_cancelacion' => 'Cancelación',
                      'options' => ''
                    ])
                    ->setUrl(route('manage.extras.index'))
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [5] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop