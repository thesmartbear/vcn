@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-money fa-fw"></i> Nuevo Extra Genérico
            </div>
            <div class="panel-body">


                {!! Form::open(array('method' => 'POST', 'url' => route('manage.extras.ficha',0), 'role' => 'form', 'class' => '')) !!}

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'generic_name', 'texto'=> 'Nombre'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'generic_unit', 'texto'=> 'Unidad', 'valor'=> 0, 'select'=> $unidades_tipo])
                    </div>

                    <div id="generic_unit_div" class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'generic_unit_id', 'texto'=> 'Tipo Unidad', 'valor'=> 0, 'select'=> $unidades])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select', [ 'campo'=> 'generic_currency_id', 'texto'=> 'Moneda', 'valor'=> 0, 'select'=> $monedas])
                    </div>

                    <div class="form-group">
                        @include('includes.form_input_text', [ 'campo'=> 'generic_price', 'texto'=> 'Importe'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_select2_multi', [ 'campo'=> 'categorias_id[]', 'texto'=> 'Categorías', 'select'=> $categorias, 'help'=> 'Se asignará a todos los cursos de estas categorias'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'generic_required', 'texto'=> 'Obligatorio'])
                    </div>

                    <div class="form-group">
                        @include('includes.form_checkbox', [ 'campo'=> 'es_cancelacion', 'texto'=> 'Seguro Cancelación'])
                    </div>

                    <div id="es_cancelacion_div" class="form-group row" style='display:none;'>
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'cancelacion_rango1', 'texto'=> 'Desde'])
                        </div>
                        <div class="col-md-2">
                            @include('includes.form_input_text', [ 'campo'=> 'cancelacion_rango2', 'texto'=> 'Hasta'])
                        </div>
                    </div>

                    @include('includes.form_submit', [ 'permiso'=> 'extras', 'texto'=> 'Guardar'])

                {!! Form::close() !!}

            </div>
        </div>

@include('includes.script_unidades', ['div'=>'#generic_unit' ])

<script type="text/javascript">
$(document).ready(function() {

    $("#es_cancelacion").click(function(){
        if( $(this).is(':checked') )
        {
            $('#es_cancelacion_div').show();
        }
        else
        {
            $('#es_cancelacion_div').hide();
        }
    });

});
</script>

@stop