@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-money fa-fw"></i> Extra Genérico :: {{$ficha->generic_name}}
            </div>
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Extra</a></li>
                    <li role="presentation"><a href="#traduccion" aria-controls="traduccion" role="tab" data-toggle="tab"><i class="fa fa-globe fa-fw"></i> Traducciones</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="ficha">

                        {!! Form::open(array('method' => 'POST', 'files'=> true, 'url' => route('manage.extras.ficha',$ficha->id), 'role' => 'form', 'class' => '')) !!}

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'generic_name', 'texto'=> 'Nombre'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_select', [ 'campo'=> 'generic_unit', 'texto'=> 'Unidad', 'valor'=> $ficha->generic_unit, 'select'=> $unidades_tipo])
                            </div>

                            <div id="generic_unit_div" class="form-group">
                                @include('includes.form_select', [ 'campo'=> 'generic_unit_id', 'texto'=> 'Tipo Unidad', 'valor'=> $ficha->generic_unit_id, 'select'=> $unidades])
                            </div>

                            <div class="form-group">
                                @include('includes.form_select', [ 'campo'=> 'generic_currency_id', 'texto'=> 'Moneda', 'valor'=> $ficha->generic_currency_id, 'select'=> $monedas])
                            </div>

                            <div class="form-group">
                                @include('includes.form_input_text', [ 'campo'=> 'generic_price', 'texto'=> 'Valor'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_select2_multi', [ 'campo'=> 'categorias_id[]',
                                    'valor'=> ($ficha->categorias_id?explode(',',$ficha->categorias_id):[]),
                                    'texto'=> 'Categorías', 'select'=> $categorias, 'help'=> 'Se asignará a todos los cursos de estas categorias'])
                            </div>

                            <div class="form-group">
                                @include('includes.form_checkbox', [ 'campo'=> 'generic_required', 'texto'=> 'Obligatorio'])
                            </div>

                            <div class="form-group row">
                                <div class="col-md-2">
                                    @include('includes.form_checkbox', [ 'campo'=> 'es_cancelacion', 'texto'=> 'Seguro Cancelación'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_checkbox', [ 'campo'=> 'es_medico', 'texto'=> 'Seguro Médico'])
                                </div>
                            </div>

                            <div id="es_cancelacion_div" class="form-group row" style='display:none;'>
                                <div class="col-md-2">
                                    @include('includes.form_input_text', [ 'campo'=> 'cancelacion_rango1', 'texto'=> 'Desde'])
                                </div>
                                <div class="col-md-2">
                                    @include('includes.form_input_text', [ 'campo'=> 'cancelacion_rango2', 'texto'=> 'Hasta'])
                                </div>
                                <div class="col-md-4">
                                    @include('includes.form_input_file', [ 'campo'=> "pdf_condiciones", 'texto'=> 'PDF condiciones'])
                                </div>
                            </div>

                            @include('includes.form_submit', [ 'permiso'=> 'extras', 'texto'=> 'Guardar'])

                        {!! Form::close() !!}

                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="traduccion">

                        @include('includes.traduccion-tab',
                                ['modelo'=> 'Extra',
                                'campos_text'=> [ ['generic_name'=> 'Nombre'], ],
                                'campos_file'=> [ ['pdf_condiciones'=> 'PDF condiciones'] ]
                            ])

                    </div>

                </div>

            </div>
        </div>

@include('includes.script_unidades', ['div'=>'#generic_unit' ])

<script type="text/javascript">
$(document).ready(function() {

    @if($ficha->es_cancelacion)
        $('#es_cancelacion_div').show();
    @endif

    $("#es_cancelacion").click(function(){
        if( $(this).is(':checked') )
        {
            $('#es_cancelacion_div').show();
        }
        else
        {
            $('#es_cancelacion_div').hide();
        }
    });

});
</script>

@stop