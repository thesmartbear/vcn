@extends('layouts.manage')


@section('container')

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tag fa-fw"></i> Categorias
                <span class="pull-right"><a href="{{ route('manage.categorias.nuevo') }}" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Nueva Categoría</a></span>
            </div>
            <div class="panel-body">

                {!! Datatable::table()
                    ->addColumn([
                      'name'            => 'Categoría',
                      'menor'            => 'Menor',
                      'avisos'  => 'Aviso Bookings',
                      'avisos_online'  => 'Aviso Bookings Online',
                      'options'         => ''
                    ])
                    ->setUrl(route('manage.categorias.index'))
                    ->setOptions('iDisplayLength', 100)
                    ->setOptions(
                      "aoColumnDefs", array(
                        [ "bSortable" => false, "aTargets" => [4] ]
                      )
                    )
                    ->render() !!}

            </div>
        </div>

@stop