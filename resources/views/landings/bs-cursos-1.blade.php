@extends('layouts.landing-bs-1')

@section('bloque-formulario-form-right')
    <div class="col-12 col-md-6 padding">
        <div class="form-group" v-if="es_viajero!=0">
            <label for="Nombre">@lang("landings.Padres/Tutor")</label>
            <input type="text" class="form-control" name="tutor_nombre" aria-describedby="emailHelp" placeholder="Nombre">
        </div>
        <div class="form-group" v-if="es_viajero!=0">
            <label for="telefono">@lang("landings.Teléfono")</label>
            <input type="text" class="form-control" name="tutor_telefono" placeholder="Teléfono">
        </div>
        <div class="form-group">
            <label for="fechanac">@lang("landings.fechanac")</label>
            <input type="date" class="form-control" name="fechanac" placeholder="dd/mm/aaaa" required>
        </div>
        <div class="form-group">
            @php
                $options1 = Traductor::trans('Landing', 'form_options1', $landing);
                $options1 = explode(",", $options1);
            @endphp
            <label for="destino">@lang("landings.destino_tipo")</label>
            <select class="form-control form-control-lg" name="destino" required>
                <option value="">-</option>
                @foreach($options1 as $o1)
                    <option value="{{$o1}}">{{$o1}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="tiempo">@lang("landings.destino_tiempo")</label>
            @php
                $options2 = Traductor::trans('Landing', 'form_options2', $landing);
                $options2 = explode(",", $options2);
            @endphp
            <select class="form-control form-control-lg" name="duracion" required>
                <option value="">-</option>
                @foreach($options2 as $o2)
                    <option value="{{$o2}}">{{$o2}}</option>
                @endforeach
            </select>
        </div>
        <div class="row d-flex justify-content-center">
            <button type="submit" class="btn btn-primary shadow-button">@lang("landings.Solicitar")</button>
        </div>
    </div>
@endsection
        

@section('bloque-destino-lista')
    
    @foreach($landing->seccion_list as $item)
    <div class="row shadow block">
        <div class="col-12 col-md-2 bg-img" style="background-image: url({{$item->foto}});"></div>
        <div class="col-12 col-md-2 padding-1 d-flex align-items-center">
            <h2>
                {!! Traductor::trans($landing->seccion_model, 'title', $item) !!}
                <span class="subv">{!! Traductor::trans($landing->seccion_model, 'subtitle', $item) !!}</span>
            </h2>
            {{-- <div class="subvencionada">
                <h2>Escuela Pública<br><span class="subv">(Subvencionada)</span></h2>
            </div>
            <div class="subvencionada-break">
                <h2>Escuela Pública<br><span class="subv">(Subvencio-<br>nada)</span></h2>
            </div> --}}
        </div>
        <div class="col-6 col-sm-3 col-md-2 d-flex align-items-center justify-content-center padding-3 padding-left-movil">
            <div class="wrapper">
                {{-- <p class="text-gray">@lang("landings.Edad")</p> --}}
                <div class="borde-right">
                    <p class="text-middle">{!! Traductor::trans($landing->seccion_model, 'edad', $item) !!}</p>
                    <p class="text-gray text-right">@lang("años")</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-md-2 d-flex align-items-center justify-content-center padding-3 padding-right-movil">
            <div class="wrapper">
                {{-- <p class="text-gray">@lang("landings.Precio")</p> --}}
                <div class="borde-right">
                    <p class="text-middle">{!! Traductor::trans($landing->seccion_model, 'precio', $item) !!}</p>
                    <p class="text-gray text-right">{!! Traductor::trans($landing->seccion_model, 'precio_duracion', $item) !!}</p>
                </div>
                
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4 d-flex padding-2 align-items-center justify-content-center">
            <div class="wrapper">
                {{-- <p class="text-gray">@lang("landings.Descripción")</p> --}}
                <p class="text-description">{!! Traductor::trans($landing->seccion_model, 'texto', $item) !!}</p>
            </div>
        </div>
    </div>
    @endforeach

@endsection