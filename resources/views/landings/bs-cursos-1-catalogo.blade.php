@extends('layouts.landing-bs-1-catalogo')

@section('bloque-formulario-form-right')
    {!! Form::hidden('es_catalogo', 1) !!}
    <div class="col-12 col-md-6 padding">
        <div class="form-group" v-if="es_viajero!=0">
            <label for="Nombre">@lang("landings.Padres/Tutor")</label>
            <input type="text" class="form-control" name="tutor_nombre" aria-describedby="emailHelp" placeholder="Nombre">
        </div>
        <div class="form-group" v-if="es_viajero!=0">
            <label for="telefono">@lang("landings.Teléfono")</label>
            <input type="text" class="form-control" name="tutor_telefono" placeholder="Teléfono">
        </div>
        <div class="form-group">
            <label for="fechanac">@lang("landings.fechanac")</label>
            <input type="date" class="form-control" name="fechanac" placeholder="dd/mm/aaaa" required>
        </div>
        <div class="form-group">
            @php
                $options1 = Traductor::trans('Landing', 'form_options1', $landing);
                $options1 = explode(",", $options1);
            @endphp
            <label for="destino">@lang("landings.destino_tipo")</label>
            <select class="form-control form-control-lg" name="destino" required>
                <option value="">-</option>
                @foreach($options1 as $o1)
                    <option value="{{$o1}}">{{$o1}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="tiempo">@lang("landings.destino_tiempo")</label>
            @php
                $options2 = Traductor::trans('Landing', 'form_options2', $landing);
                $options2 = explode(",", $options2);
            @endphp
            <select class="form-control form-control-lg" name="duracion" required>
                <option value="">-</option>
                @foreach($options2 as $o2)
                    <option value="{{$o2}}">{{$o2}}</option>
                @endforeach
            </select>
        </div>
        <div class="row d-flex justify-content-center">
            <button type="submit" class="btn btn-primary shadow-button">@lang("landings.Solicitar")</button>
        </div>
    </div>
@endsection