<div class="row">
    <div class="col-md-12">

    <h4 class="text-success">{!! trans('area.datoseconomicos') !!}</h4>

    <p>
        @if($booking->oficina && $booking->es_corporativo_pago)
            {!! trans('area.numcuenta',['cuenta' => $booking->oficina->txtIban($booking), 'banco' => $booking->oficina->banco]) !!}
        @endif
    </p>

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr class="thead">
                        <td>{!! trans('area.fecha') !!}</td>
                        <td>{!! trans('area.tipo') !!}</td>
                        <td>{!! trans('area.importe') !!}</td>
                        <td align="right"></td>
                    </tr>
                </thead>
                <tbody>
                @foreach($ficha->pagos as $pago)
                <tr>
                    <td>{{$pago->fecha_dmy}}</td>
                    <td>{!! trans('area.'.$pago->tipo_pago) !!}</td>
                    <td>
                        {{$pago->importe_pago_moneda}}
                    </td>
                    <td align='right'>
                        <a href='{{route('area.pagos.pdf', [$pago->id])}}' class='btn btn-danger btn-xs'><i class='fa fa-file-pdf-o'></i> {!! trans('area.recibo') !!}</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

    <hr>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped table-condensed">
                <?php 
                    $monedasUsadas = [];
                    $total = $ficha->precio_total;
                ?>
                <caption class="text-uppercase">{!! trans('area.desglose') !!}</caption>
                <thead>
                    <tr>
                        <th width='60%'>{!! trans('area.concepto') !!}</th>
                        <th colspan='2'>{!! trans('area.importe') !!}</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td>{!! trans('area.curso') !!}</td>
                        <td colspan='2' align="right">
                            {{ConfigHelper::parseMoneda($ficha->course_total_amount, $ficha->curso_moneda)}}
                        </td>
                    </tr>
                    
                    <?php array_push($monedasUsadas,
                        $ficha->curso_moneda . " : " .ConfigHelper::monedaCambio($ficha->course_currency_id,$ficha->id) ) ;?>

                    @if($ficha->accommodation_total_amount)
                    <tr>
                        <td>{!! trans('area.alojamiento') !!}</td>
                        <td colspan='2' align='right'>
                            {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
                        </td>
                    </tr>
                    <?php array_push($monedasUsadas,
                            $ficha->alojamiento_moneda . " : " .ConfigHelper::monedaCambio($ficha->accommodation_currency_id,$ficha->id) ); ?>
                    @endif

                    <tr><th colspan="3">{!! trans('area.extras') !!}</th></tr>

                    @foreach($ficha->extrasCurso as $extra)
                        @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        <?php array_push($monedasUsadas,
                            $extra->moneda->name . " : " .ConfigHelper::monedaCambio($extra->moneda_id,$ficha->id) ); ?>
                    @endforeach

                    @foreach($ficha->extrasCentro as $extra)
                        @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        <?php array_push($monedasUsadas,
                            $extra->moneda->name . " : " .ConfigHelper::monedaCambio($extra->moneda_id,$ficha->id) ); ?>
                    @endforeach

                    @foreach($ficha->extras_alojamiento as $extra)
                        @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        <?php array_push($monedasUsadas,
                            $extra->moneda->name . " : " .ConfigHelper::monedaCambio($extra->moneda_id,$ficha->id) ); ?>
                    @endforeach

                    @foreach($ficha->extrasGenerico as $extra)
                        @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        <?php array_push($monedasUsadas,
                            $extra->moneda->name . " : " .ConfigHelper::monedaCambio($extra->moneda_id,$ficha->id) ); ?>
                    @endforeach

                    @foreach($ficha->extras_cancelacion as $extra)
                        @include('includes.bookings_tr_extra', ['extra'=> $extra])
                    @endforeach

                    @foreach($ficha->extrasOtros as $extra)
                    <tr>
                        <td>{{$extra->name}} [{{$extra->notas}}]</td>
                        <td>
                            <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
                            x{{$extra->unidades}}
                        </td>
                        <td align="right">{{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda_name) }}</td>
                    </tr>
                    @endforeach

                    <tr>
                        <td>{!! trans('area.subtotal') !!}</td>
                        <td colspan='2' align="right">
                            @foreach($ficha->precio_total['subtotal_txt'] as $subtotal)
                                {{$subtotal}}<br>
                            @endforeach
                        </td>
                    </tr>

                    @if($ficha->divisa_variacion && !$ficha->promo_cambio_fijo)
                    <tr>
                        <td>Variación Divisa</td>
                        <td>{{ConfigHelper::parseMoneda($ficha->divisa_variacion)}}</td>
                    </tr>
                    @endif

                    @if($ficha->precio_total['descuento_especial'] > 0)
                    <tr>
                        <td>{!! trans('area.descuentoespecial') !!}:</td>
                        <td colspan='2' align="right">{{$ficha->precio_total['descuento_especial_txt']}}</td>
                    </tr>
                    @endif

                    @if($ficha->descuentos->count()>0)
                    <tr>
                        <th colspan="3">{!! trans('area.descuentos') !!}</th></tr>
                        @foreach($ficha->descuentos as $descuento)
                        <tr>
                            <td>{{$descuento->notas}}</td>
                            <td colspan='2' align="right">-{{ ConfigHelper::parseMoneda($descuento->importe, $descuento->moneda->name) }}</td>
                        </tr>
                        @endforeach
                    @endif

                    <tr>
                        <th>
                            <span class="text-uppercase">{!! trans('area.total') !!}</span>
                            @if($ficha->es_cancelado || $ficha->es_refund || $ficha->pagado)
                                <i class='fa fa-lock'></i>
                            @endif
                        </th>
                        <td colspan='2' align="right"><strong>{{$ficha->total_divisa_txt}}</strong></td>
                    </tr>

                    <tr>
                        <td>
                            <span class="text-uppercase bold">{!! trans('area.pendiente') !!}</span>
                            <i>({{$ficha->precio_total['total']}} - {!! trans('area.pagos') !!}: {{$ficha->pagos->sum('importe')}}) = </i></td>
                        <td colspan='2' align="right">
                            <h4><strong>
                                {{ ConfigHelper::parseMoneda(($ficha->precio_total['total']-$ficha->pagos->sum('importe')), Session::get('vcn.moneda')) }}
                            </strong></h4>
                        </td>
                    </tr>

                    <?php $monedasUsadas = array_unique($monedasUsadas); ?>

                    @if(!$ficha->es_cancelado && !$ficha->es_refund)
                    <tr>
                        <td colspan="3">
                            {!! trans('area.cambio') !!}:<br />
                            @foreach($ficha->monedas_usadas_txt as $mu)
                                <small>{{$mu}}</small><br />
                            @endforeach
                        </td>
                    </tr>
                    @endif

                </tbody>
            </table>
        </div>
    </div>


    </div>
</div>