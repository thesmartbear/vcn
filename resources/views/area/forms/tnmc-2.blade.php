<div id="test02">
    {!! Form::open(array('url' => route('area.forms.post',[$form->id, $booking->id]), 'id'=>'segundo')) !!}

    <div class="form-group row">
            <div class="col-md-10">

                <fieldset>
                    <legend>Read the text. Choose the correct words</legend>
                    <h3>Hospitals</h3>
                    <p>
                        When we think of <strong><input type="radio" name="respuesta[sA01]" id="respuesta[sA01]" value="a" /> a / <input type="radio" name="respuesta[sA01]" id="respuesta[sA01]" value="an" /> an /  <input type="radio" name="respuesta[sA01]" id="respuesta[sA01]" value="the" /> the</strong> &nbsp;hospital, perhaps we only  think of doctors and nurses there, but other people &nbsp;&nbsp;<strong><input type="radio" name="respuesta[sA02]" id="respuesta[sA02]" value="work" /> work / <input type="radio" name="respuesta[sA02]" id="respuesta[sA02]" value="works" /> works / <input type="radio" name="respuesta[sA02]" id="respuesta[sA02]" value="working" /> working</strong> &nbsp;there too. They all do important jobs. &nbsp;<strong><input type="radio" name="respuesta[sA03]" id="respuesta[sA03]" value="That" /> That / <input type="radio" name="respuesta[sA03]" id="respuesta[sA03]" value="There" /> There / <input type="radio" name="respuesta[sA03]" id="respuesta[sA03]" value="Someone" /> Someone</strong> are secretaries, cooks and engineers. &nbsp;In hospitals with a lot  of children, they have teachers who give <strong><input type="radio" name="respuesta[sA04]" id="respuesta[sA04]" value="this" /> this / <input type="radio" name="respuesta[sA04]" id="respuesta[sA04]" value="these" /> these / <input type="radio" name="respuesta[sA04]" id="respuesta[sA04]" value="them" /> them</strong> &nbsp;lessons when they can&rsquo;t go to  school.&nbsp;&nbsp; Some people go to hospital&nbsp;&nbsp; <strong><input type="radio" name="respuesta[sA05]" id="respuesta[sA05]" value="just" /> just / <input type="radio" name="respuesta[sA05]" id="respuesta[sA05]" value="still" /> still / <input type="radio" name="respuesta[sA05]" id="respuesta[sA05]" value="already" /> already</strong>&nbsp; for one day, but other people need to&nbsp; <strong><input type="radio" name="respuesta[sA06]" id="respuesta[sA06]" value="stay" /> stay / <input type="radio" name="respuesta[sA06]" id="respuesta[sA06]" value="stays" /> stays / <input type="radio" name="respuesta[sA06]" id="respuesta[sA06]" value="staying" /> staying</strong>&nbsp; there for a longer time.&nbsp; If you go to hospital, sometimes you&nbsp; <strong><input type="radio" name="respuesta[sA07]" id="respuesta[sA07]" value="have" /> have / <input type="radio" name="respuesta[sA07]" id="respuesta[sA07]" value="must" /> must / <input type="radio" name="respuesta[sA07]" id="respuesta[sA07]" value="should" /> should</strong>&nbsp;&nbsp; to wait a long time before you see the doctor&nbsp;&nbsp; <strong><input type="radio" name="respuesta[sA08]" id="respuesta[sA08]" value="but" /> but / <input type="radio" name="respuesta[sA08]" id="respuesta[sA08]" value="because" /> because / <input type="radio" name="respuesta[sA08]" id="respuesta[sA08]" value="if" /> if</strong>&nbsp; doctors have a lot of work to do.&nbsp; If you are in hospital for a long time, you  need to take clothes and&nbsp;&nbsp; <strong><input type="radio" name="respuesta[sA09]" id="respuesta[sA09]" value="much" /> much / <input type="radio" name="respuesta[sA09]" id="respuesta[sA09]" value="some" /> some /  <input type="radio" name="respuesta[sA09]" id="respuesta[sA09]" value="every" /> every</strong>&nbsp;  books  ?? comics to read.&nbsp; Often  your friends and family send you cards and flowers&nbsp;&nbsp; <strong><input type="radio" name="respuesta[sA10]" id="respuesta[sA10]" value="than" /> than / <input type="radio" name="respuesta[sA10]" id="respuesta[sA10]" value="when" /> when / <input type="radio" name="respuesta[sA10]" id="respuesta[sA10]" value="or" /> or</strong>&nbsp; they visit you. Hospitals are full of people <strong><input type="radio" name="respuesta[sA11]" id="respuesta[sA11]" value="who" /> who / <input type="radio" name="respuesta[sA11]" id="respuesta[sA11]" value="which" /> which / <input type="radio" name="respuesta[sA11]" id="respuesta[sA11]" value="what" /> what</strong> want to help you, but most of us still want to go home quickly.
                    </p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </fieldset>
                <fieldset>
                    <legend>Read the text and choose the best answer for each gap</legend>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="">
                                <p><strong>Do you like cooking?</strong><br />
                                    <input type="radio" name="respuesta[sB01]" id="respuesta[sB01]" value="Yes, I have" /> Yes, I have<br />
                                    <input type="radio" name="respuesta[sB01]" id="respuesta[sB01]" value="It's very well" /> It&rsquo;s very well<br />
                                    <input type="radio" name="respuesta[sB01]" id="respuesta[sB01]" value="Sometimes" /> Sometimes
                                </p>
                                <p>
                                    <strong>How often do you go to the cinema?</strong><br />
                                    <input type="radio" name="respuesta[sB02]" id="respuesta[sB02]" value="Twice a month." /> Twice  a month.<br />
                                    <input type="radio" name="respuesta[sB02]" id="respuesta[sB02]" value="There are three cinemas in my town." /> There are three cinemas in my town.<br />
                                    <input type="radio" name="respuesta[sB02]" id="respuesta[sB02]" value="Yes, I do" /> Yes, I do
                                </p>
                                <p>
                                    <strong>What are you wearing?</strong><br />
                                    <input type="radio" name="respuesta[sB03]" id="respuesta[sB03]" value="I have a blue school bag" /> I have a blue school bag<br />
                                    <input type="radio" name="respuesta[sB03]" id="respuesta[sB03]" value="I like shopping" /> I like shopping<br />
                                    <input type="radio" name="respuesta[sB03]" id="respuesta[sB03]" value="Black trousers and a jumper" /> Black trousers and a jumper
                                </p>
                                <p>
                                    <strong>What are you holiday plans?</strong><br />
                                    <input type="radio" name="respuesta[sB04]" id="respuesta[sB04]" value="I'm going to Tenerife with my family." /> I'm going to Tenerife with my family.<br />
                                    <input type="radio" name="respuesta[sB04]" id="respuesta[sB04]" value="I will stay at home." /> I will stay at home.<br />
                                    <input type="radio" name="respuesta[sB04]" id="respuesta[sB04]" value="I go to visit my grandparents" /> I go to visit my grandparents
                                </p>
                            </td>
                            <td>
                                <p>
                                    <strong>What were your hobbies when you were younger?</strong><br />
                                    <input type="radio" name="respuesta[sB05]" id="respuesta[sB05]" value="I collect stickers" /> I collect stickers<br />
                                    <input type="radio" name="respuesta[sB05]" id="respuesta[sB05]" value="I used to play with dolls" /> I used to play with dolls<br />
                                    <input type="radio" name="respuesta[sB05]" id="respuesta[sB05]" value="I was playing with my friends." /> I was playing with my friends.
                                </p>
                                <p>
                                    <strong>What would you do if you met a famous person?</strong><br />
                                    <input type="radio" name="respuesta[sB06]" id="respuesta[sB06]" value="I say  hello." /> I say  hello.<br />
                                    <input type="radio" name="respuesta[sB06]" id="respuesta[sB06]" value="I don't speak." /> I don't speak.<br />
                                    <input type="radio" name="respuesta[sB06]" id="respuesta[sB06]" value="I'd ask for their autograph." /> I'd ask for their autograph.
                                </p>
                                <p>
                                    <strong>What did you do last weekend?</strong><br />
                                    <input type="radio" name="respuesta[sB07]" id="respuesta[sB07]" value="I'm going to the cinema" /> I'm going to the cinema<br />
                                    <input type="radio" name="respuesta[sB07]" id="respuesta[sB07]" value="I go to the cinema" /> I go to the cinema<br />
                                    <input type="radio" name="respuesta[sB07]" id="respuesta[sB07]" value="I went to the cinema" /> I went to the cinema
                                </p>
                                <p>
                                    <strong>What will you do when you leave school?</strong><br />
                                    <input type="radio" name="respuesta[sB08]" id="respuesta[sB08]" value="I'm going to the park this afternoon" /> I'm going to the park this afternoon<br />
                                    <input type="radio" name="respuesta[sB08]" id="respuesta[sB08]" value="I want to go to university" /> I want to go to university<br />
                                    <input type="radio" name="respuesta[sB08]" id="respuesta[sB08]" value="I went to the dentist&rsquo;s yesterday" /> I went to the dentist&rsquo;s yesterday
                                </p>
                            </td>
                        </tr>
                    </table>

                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </fieldset>
                <fieldset>
                    <legend>Read the text. Choose the correct words</legend>
                    <h3>Deep sleep</h3>
                    <p>
                        Deep sleep is important for everyone. The actual <strong><input type="radio" name="respuesta[sC01]" id="respuesta[sC01]" value="sum" /> sum / <input type="radio" name="respuesta[sC01]" id="respuesta[sC01]" value="size" /> size / <input type="radio" name="respuesta[sC01]" id="respuesta[sC01]" value="amount" /> amount</strong> of sleep you need depends <strong><input type="radio" name="respuesta[sC02]" id="respuesta[sC02]" value="on" /> on / <input type="radio" name="respuesta[sC02]" id="respuesta[sC02]" value="to" />to / <input type="radio" name="respuesta[sC02]" id="respuesta[sC02]" value="of" /> of</strong> your age. A young child <strong><input type="radio" name="respuesta[sC03]" id="respuesta[sC03]" value="should" /> should / <input type="radio" name="respuesta[sC03]" id="respuesta[sC03]" value="ought" /> ought / <input type="radio" name="respuesta[sC03]" id="respuesta[sC03]" value="must" /> must</strong> to sleep ten to twelve hours, and a teenager about nine hours. Adults differ a lot in their sleeping <strong><input type="radio" name="respuesta[sC04]" id="respuesta[sC04]" value="habits" /> habits / <input type="radio" name="respuesta[sC04]" id="respuesta[sC04]" value="ways" /> ways / <input type="radio" name="respuesta[sC04]" id="respuesta[sC04]" value="manners" /> manners.</strong>&nbsp; For most of them, seven to eight hours a  night is <strong><input type="radio" name="respuesta[sC05]" id="respuesta[sC05]" value="few" /> few / <input type="radio" name="respuesta[sC05]" id="respuesta[sC05]" value="well" /> well / <input type="radio" name="respuesta[sC05]" id="respuesta[sC05]" value="enough" /> enough</strong>, but some sleep longer, while others manage with only four hours.
                    </p>
                    <p>
                        For a good  night, having a comfortable <strong><input type="radio" name="respuesta[sC06]" id="respuesta[sC06]" value="point" /> point / <input type="radio" name="respuesta[sC06]" id="respuesta[sC06]" value="position" /> position / <input type="radio" name="respuesta[sC06]" id="respuesta[sC06]" value="part" /> part</strong> to sleep is very important. Also, there  should be <strong><input type="radio" name="respuesta[sC07]" id="respuesta[sC07]" value="plenty" /> plenty / <input type="radio" name="respuesta[sC07]" id="respuesta[sC07]" value="much" /> much / <input type="radio" name="respuesta[sC07]" id="respuesta[sC07]" value="many" /> many</strong> of fresh air in the room. A warm drink sometimes helps people to sleep, <strong><input type="radio" name="respuesta[sC08]" id="respuesta[sC08]" value="as" /> as / <input type="radio" name="respuesta[sC08]" id="respuesta[sC08]" value="although" /> although / <input type="radio" name="respuesta[sC08]" id="respuesta[sC08]" value="even" /> even</strong> it is not a good idea to  drink coffee before going to bed.
                    </p>
                    <p>
                        <strong><input type="radio" name="respuesta[sC09]" id="respuesta[sC09]" value="Since" /> Since / <input type="radio" name="respuesta[sC09]" id="respuesta[sC09]" value="Until" /> Until / <input type="radio" name="respuesta[sC09]" id="respuesta[sC09]" value="If" /> If</strong> you have to travel a long distance, try to go to bed earlier than usual the day before the <strong><input type="radio" name="respuesta[sC10]" id="respuesta[sC10]" value="journey" /> journey / <input type="radio" name="respuesta[sC10]" id="respuesta[sC10]" value="voyage" /> voyage / <input type="radio" name="respuesta[sC10]" id="respuesta[sC10]" value="call" /> call</strong>.&nbsp; This will help you feel more rested when you  arrive.
                    </p>
                </fieldset>

                <hr>

            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-10">
                <button id="enviarform02" class="btn btn-block btn-success">Enviar</button>
                {!! Form::hidden('datos', serialize($_POST['datos']), array('id' => 'datos')) !!}
            </div>
        </div>

    {!! Form::close() !!}

</div>