<div class="row">
    <div class="col-md-10">
        <legend>Read the text. Choose the correct words</legend>
        <h3>Hospitals</h3>
        <p>When we think  of <strong>{{$respuesta->sA01}}</strong> hospital, perhaps we only think of doctors and nurses there, but other people &nbsp;<strong>{{$respuesta->sA02}}</strong> there too. They all do important jobs.  <strong>{{$respuesta->sA03}}</strong> are secretaries, cooks and engineers. &nbsp;In hospitals with a lot  of children, they have teachers who give<strong>{{$respuesta->sA04}}</strong>&nbsp;lessons when they can&rsquo;t go to  school. Some people go to hospital <strong>{{$respuesta->sA05}}</strong> for one day, but other people need to <strong>{{$respuesta->sA06}}</strong> there for a longer time.&nbsp; If you go to hospital, sometimes you&nbsp;<strong>{{$respuesta->sA07}}</strong>&nbsp; to wait a long time before you see the doctor&nbsp;&nbsp;<strong>{{$respuesta->sA08}}</strong>&nbsp; doctors have a lot of work to do.&nbsp; If you are in hospital for a long time, you  need to take clothes and&nbsp;&nbsp;<strong>{{$respuesta->sA09}}</strong>  books  ?? comics to read.&nbsp; Often  your friends and family send you cards and flowers&nbsp;<strong>{{$respuesta->sA10}}</strong> they visit you. Hospitals are full of people <strong>{{$respuesta->sA11}}</strong> want to help you, but most of us still want to go home quickly.</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <legend>Read  the text and choose the best answer for each gap</legend>
        <p>&nbsp;</p>
        <p><strong>Do you like cooking?</strong> {{$respuesta->sB01}}</p>
        <p><br />
            <strong>How often do you go to the cinema? </strong>{{$respuesta->sB02}}</p>
        <p><br />
            <strong>What are you wearing?</strong><strong> </strong>{{$respuesta->sB03}}</p>
        <p><br />
            <strong>What are you holiday plans?</strong><strong> </strong>{{$respuesta->sB04}}</p>
        <p><br />
            <strong>What were your hobbies when you were younger?</strong><strong> </strong>{{$respuesta->sB05}}</p>
        <p><br />
            <strong>What would you do if you met a famous person?</strong><strong> </strong>{{$respuesta->sB06}}</p>
        <p><br />
            <strong>What did you do last weekend?</strong><strong> </strong>{{$respuesta->sB07}}</p>
        <p><br />
            <strong>What will you do when you leave school?</strong><strong> </strong>{{$respuesta->sB08}}</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <legend>Read the text. Choose the correct words</legend>
        <h3>Deep sleep</h3>
        <p>Deep sleep is important for everyone. The actual <strong>{{$respuesta->sC01}}</strong> of sleep you need depends <strong> {{$respuesta->sC02}}</strong> your age. A young child <strong>{{$respuesta->sC03}}</strong> to sleep ten to twelve hours, and a teenager about nine hours. Adults differ a lot in their sleeping <strong>{{$respuesta->sC04}}.</strong>&nbsp; For most of them, seven to eight hours a  night is <strong>{{$respuesta->sC05}}</strong>, but some sleep longer, while others manage with only four hours.</p>
        <p>For a good  night, having a comfortable <strong>{{$respuesta->sC06}}</strong> to sleep is very important. Also, there  should be <strong>{{$respuesta->sC07}}</strong> of fresh air in the room. A warm drink sometimes helps people to sleep,<strong> {{$respuesta->sC08}}</strong> it is not a good idea to drink coffee before going to bed.</p>
        <p><strong>{{$respuesta->sC09}}</strong> you have to travel a long distance, try to go to bed earlier than usual the day before the <strong>{{$respuesta->sC10}}</strong>.&nbsp; This will help you feel more rested when you  arrive.</p>
    </div>
</div>