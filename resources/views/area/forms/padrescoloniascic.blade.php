@extends('layouts.area')

@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop

@section('container')
    <h2 class="text-capitalize text-success">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</h2>
    <hr>

    {!! Form::open(array('url' => route('area.forms.post',[$form->id, $booking->id]), 'id'=>'padrescoloniascic')) !!}

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r1">{{trans('forms.padrescoloniascic.p01')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r1-0">
                        <input type="radio" name="respuesta[r01]" id="r1-0" value="muydeacuerdo">
                        {{trans('forms.muydeacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r1-1">
                        <input type="radio" name="respuesta[r01]" id="r1-1" value="deacuerdo">
                        {{trans('forms.deacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r1-2">
                        <input type="radio" name="respuesta[r01]" id="r1-2" value="endesacuerdo">
                        {{trans('forms.endesacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r1-3">
                        <input type="radio" name="respuesta[r01]" id="r1-3" value="muyendesacuerdo">
                        {{trans('forms.muyendesacuerdo')}}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-10 margintop20 control-label" for="r1t">{{trans('forms.comentarios')}}</label>
                <div class="col-md-8">
                    <textarea class="form-control" id="r1t" name="respuesta[r01t]" rows="6"></textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r2">{{trans('forms.padrescoloniascic.p02')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r2-0">
                        <input type="radio" name="respuesta[r02]" id="r2-0" value="muydeacuerdo">
                        {{trans('forms.muydeacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r2-1">
                        <input type="radio" name="respuesta[r02]" id="r2-1" value="deacuerdo">
                        {{trans('forms.deacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r2-2">
                        <input type="radio" name="respuesta[r02]" id="r2-2" value="endesacuerdo">
                        {{trans('forms.endesacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r2-3">
                        <input type="radio" name="respuesta[r02]" id="r2-3" value="muyendesacuerdo">
                        {{trans('forms.muyendesacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r2-4">
                        <input type="radio" name="respuesta[r02]" id="r2-4" value="noaplica">
                        {{trans('forms.muyendesacuerdo')}}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-10 margintop20 control-label" for="r2t">{{trans('forms.comentarios')}}</label>
                <div class="col-md-8">
                    <textarea class="form-control" id="r2t" name="respuesta[r02t]" rows="6"></textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r3">{{trans('forms.padrescoloniascic.p03')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r3-0">
                        <input type="radio" name="respuesta[r03]" id="r3-0" value="muydeacuerdo">
                        {{trans('forms.muydeacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r3-1">
                        <input type="radio" name="respuesta[r03]" id="r3-1" value="deacuerdo">
                        {{trans('forms.deacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r3-2">
                        <input type="radio" name="respuesta[r03]" id="r3-2" value="endesacuerdo">
                        {{trans('forms.endesacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r3-3">
                        <input type="radio" name="respuesta[r03]" id="r3-3" value="muyendesacuerdo">
                        {{trans('forms.muyendesacuerdo')}}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-10 margintop20 control-label" for="r3t">{{trans('forms.comentarios')}}</label>
                <div class="col-md-8">
                    <textarea class="form-control" id="r3t" name="respuesta[r03t]" rows="6"></textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r4">{{trans('forms.padrescoloniascic.p04')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r4-0">
                        <input type="radio" name="respuesta[r04]" id="r4-0" value="muydeacuerdo">
                        {{trans('forms.muydeacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r4-1">
                        <input type="radio" name="respuesta[r04]" id="r4-1" value="deacuerdo">
                        {{trans('forms.deacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r4-2">
                        <input type="radio" name="respuesta[r04]" id="r4-2" value="endesacuerdo">
                        {{trans('forms.endesacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r4-3">
                        <input type="radio" name="respuesta[r04]" id="r4-3" value="muyendesacuerdo">
                        {{trans('forms.muyendesacuerdo')}}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-10 margintop20 control-label" for="r4t">{{trans('forms.comentarios')}}</label>
                <div class="col-md-8">
                    <textarea class="form-control" id="r4t" name="respuesta[r04t]" rows="6"></textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r5">{{trans('forms.padrescoloniascic.p05')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r5-0">
                        <input type="radio" name="respuesta[r05]" id="r5-0" value="muydeacuerdo">
                        {{trans('forms.muydeacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r5-1">
                        <input type="radio" name="respuesta[r05]" id="r5-1" value="deacuerdo">
                        {{trans('forms.deacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r5-2">
                        <input type="radio" name="respuesta[r05]" id="r5-2" value="endesacuerdo">
                        {{trans('forms.endesacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r5-3">
                        <input type="radio" name="respuesta[r05]" id="r5-3" value="muyendesacuerdo">
                        {{trans('forms.muyendesacuerdo')}}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-10 margintop20 control-label" for="r5t">{{trans('forms.comentarios')}}</label>
                <div class="col-md-8">
                    <textarea class="form-control" id="r5t" name="respuesta[r05t]" rows="6"></textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r6">{{trans('forms.padrescoloniascic.p06')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r6-0">
                        <input type="radio" name="respuesta[r06]" id="r6-0" value="muydeacuerdo">
                        {{trans('forms.muydeacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r6-1">
                        <input type="radio" name="respuesta[r06]" id="r6-1" value="deacuerdo">
                        {{trans('forms.deacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r6-2">
                        <input type="radio" name="respuesta[r06]" id="r6-2" value="endesacuerdo">
                        {{trans('forms.endesacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r6-3">
                        <input type="radio" name="respuesta[r06]" id="r6-3" value="muyendesacuerdo">
                        {{trans('forms.muyendesacuerdo')}}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-10 margintop20 control-label" for="r6t">{{trans('forms.comentarios')}}</label>
                <div class="col-md-8">
                    <textarea class="form-control" id="r6t" name="respuesta[r06t]" rows="6"></textarea>
                </div>
            </div>

            {{-- <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r8">{{trans('forms.padrescoloniascic.p08')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r8-0">
                        <input type="radio" name="respuesta[r08]" id="r8-0" value="indispensable">
                        {{trans('forms.indispensable')}}
                    </label>
                    <label class="radio-inline" for="r8-1">
                        <input type="radio" name="respuesta[r08]" id="r8-1" value="muyutil">
                        {{trans('forms.muyutil')}}
                    </label>
                    <label class="radio-inline" for="r8-2">
                        <input type="radio" name="respuesta[r08]" id="r8-2" value="indiferente">
                        {{trans('forms.indiferente')}}
                    </label>
                    <label class="radio-inline" for="r8-3">
                        <input type="radio" name="respuesta[r08]" id="r8-3" value="innecesario">
                        {{trans('forms.innecesario')}}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-10 margintop20 control-label" for="r8t">{{trans('forms.comentarios')}}</label>
                <div class="col-md-8">
                    <textarea class="form-control" id="r8t" name="respuesta[r08t]" rows="6"></textarea>
                </div>
            </div> --}}

            {{-- <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r9">{{trans('forms.padrescoloniascic.p09')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r9-0">
                        <input type="radio" name="respuesta[r09]" id="r9-0" value="sabado">
                        {{trans('forms.sabado')}}
                    </label>
                    <label class="radio-inline" for="r9-1">
                        <input type="radio" name="respuesta[r09]" id="r9-1" value="domingo">
                        {{trans('forms.domingo')}}
                    </label>
                    <label class="radio-inline" for="r9-2">
                        <input type="radio" name="respuesta[r09]" id="r9-2" value="lunes">
                        {{trans('forms.lunes')}}
                    </label>
                    <label class="radio-inline" for="r9-3">
                        <input type="radio" name="respuesta[r09]" id="r9-3" value="indiferente">
                        {{trans('forms.indiferente')}}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-10 margintop20 control-label" for="r9t">{{trans('forms.comentarios')}}</label>
                <div class="col-md-8">
                    <textarea class="form-control" id="r9t" name="respuesta[r09t]" rows="6"></textarea>
                </div>
            </div> --}}

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r7">{{trans('forms.padrescoloniascic.p07')}}</label>
                <div class="col-md-8">
                    <textarea class="form-control" id="r7" name="respuesta[r07]" rows="6"></textarea>
                </div>
            </div>

    <div class="form-group row">
        <div class="col-md-10">
            <h3 class="margintop20">{{trans('forms.valora_1-10')}}</h3>
        </div>
        <div class="col-md-10">
            <label class="margintop20 control-label" for="blog">{{trans('forms.padrescoloniascic.web')}}</label>
            <select id="blog" name="respuesta[web]" class="input-small validar">
                <option> </option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select>
        </div>
        <div class="col-md-10">
            <label class="margintop20 control-label" for="blog">{{trans('forms.padrescoloniascic.catalogo')}}</label>
            <select id="blog" name="respuesta[catalogo]" class="input-small validar">
                <option> </option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select>
        </div>
        <div class="col-md-10">
            <label class="margintop20 control-label" for="blog">{{trans('forms.padrescoloniascic.blog')}}</label>
            <select id="blog" name="respuesta[blog]" class="input-small validar">
                <option> </option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select>
        </div>
        {{-- <div class="col-md-10">
            <label class="margintop20 control-label" for="autocares">{{trans('forms.padrescoloniascic.autocares')}}</label>
            <select id="autocares" name="respuesta[autocares]" class="input-small validar">
                <option> </option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select>
        </div> --}}
        <div class="col-md-10">
            <label class="margintop20 control-label" for="lavado">{{trans('forms.padrescoloniascic.lavado')}}</label>
            <select id="lavado" name="respuesta[lavado]" class="input-small validar">
                <option> </option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select>
        </div>
        {{-- <div class="col-md-10">
            <label class="margintop20 control-label" for="trinity">{{trans('forms.padrescoloniascic.trinity')}}</label>
            <select id="trinity" name="respuesta[trinity]" class="input-small validar">
                <option> </option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select>
        </div> --}}
        <div class="col-md-10">
            <label class="margintop20 control-label" for="trato">{{trans('forms.padrescoloniascic.trato')}}</label>
            <select id="trato" name="respuesta[trato]" class="input-small validar">
                <option> </option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select>
        </div>
        <div class="col-md-10">
            <label class="margintop20 control-label" for="trato">{{trans('forms.padrescoloniascic.informe')}}</label>
            <select id="informe" name="respuesta[informe]" class="input-small validar">
                <option> </option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select>
        </div>
    </div>


    <hr>
    <div class="form-group row">
        <div class="col-md-10">
            <button id="enviardatos" class="btn btn-block btn-success">Enviar</button>
            {!! Form::hidden('datos[booking]', $booking->id, array('id' => 'datos[booking]', 'class' => 'booking')) !!}
        </div>
    </div>

    {!! Form::close() !!}


    <div class="modal fade" tabindex="-1" role="dialog" id="error">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger">Error</h4>
                </div>
                <div class="modal-body">
                    <p class="errortext"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('extra_footer')
    <script type="text/javascript">

    $( "#enviardatos" ).click(function() {
        cuantos = 0;
        $('input[type="radio"]').each(function( index ) {
            if ($(this).prop("checked") != true){
                cuantos++;
            }
        });

        validar = 0;
        $('.validar').each(function( index ) {
            
            if( $(this).val() > 0 )
            {
                validar++;
            }

        });

        if (cuantos > 25 || validar < 5){
            $('.errortext').html('{{trans('forms.responder')}}');
            $('#error').modal();
            return false;
        }else{
            $('#padrescoloniascic').submit();
        }
    });

    </script>
@stop