<div id="test01">
    {!! Form::open(array('url' => route('area.forms.post',[$form->id, $booking->id]), 'id'=>'cictest')) !!}

    <div class="form-group row">
        <div class="col-md-10">
            <fieldset class="cictest">
                <legend>Choose the correct answer. Only one answer is correct.</legend>
                <p><strong>1. TV programmes are ______ computer games with today's teenagers. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="a" />
                        less popular that
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="b" />
                        not as popular as
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="c" />
                        the same popular
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="d" />
                        as popular so
                    </div>
                </div>

                <br />

                <p><strong>2. "Whose flowers are they?" &quot;They're _______.&quot; </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="a" />
                        to Mary
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="b" />
                        of Mary
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="c" />
                        Maries
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="d" />
                        Mary's
                    </div>
                </div>

                <br />

                <p><strong>3.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="a" />
                        Give his the jacket.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="b" />
                        Give him the jacket.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="c" />
                        Give we the jacket.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="d" />
                        Give their the jacket.
                    </div>
                </div>

                <br />

                <p><strong>4. _________ to the party last Saturday? </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="a" />
                        Do you went...?
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="b" />
                        Did you went...?
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="c" />
                        Did you go...?
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="d" />
                        Do you go...?
                    </div>
                </div>

                <br />

                <p><strong>5. Tony is looking at _________.  </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="a" />
                        she
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="b" />
                        here
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="c" />
                        he
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="d" />
                        her
                    </div>
                </div>

                <br />

                <p><strong>6. When _________, give her this book. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="a" />
                        Alison will arrive
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="b" />
                        Alison arrives
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="c" />
                        Alison arrive
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="d" />
                        is Alison arriving
                    </div>
                </div>

                <br />

                <p><strong>7. This mobile phone is  ________ that one. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="a" />
                        cheaper than
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="b" />
                        the cheapest
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="c" />
                        more cheap
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="d" />
                        very cheaper
                    </div>
                </div>

                <br />

                <p><strong>8. ____________ lovely food! </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="a" />
                        What
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="b" />
                        Which a
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="c" />
                        Which
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="d" />
                        What a
                    </div>
                </div>

                <br />

                <p><strong>9. I've had a bad cold _______. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="a" />
                        since five days
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="b" />
                        since last Friday
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="c" />
                        five days ago
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="d" />
                        last Friday
                    </div>
                </div>

                <br />

                <p><strong>10. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="a" />
                        That girl is some of my friends.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="b" />
                        That girl is me friend.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="c" />
                        This girl is one of my friends.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="d" />
                        These girl's are friends.
                    </div>
                </div>

                <br />

                <p><strong>11. When we got to school, we _______ the bell.  </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="a" />
                        were listening
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="b" />
                        were hearing
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="c" />
                        listened
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="d" />
                        heard
                    </div>
                </div>

                <br />

                <p><strong>12. I feel very well because I went to bed very early _____. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="a" />
                        last night
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="b" />
                        tonight
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="c" />
                        this night
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="d" />
                        in the night

                    </div>
                </div>

                <br />

                <p><strong>13. Have you heard their latest song?<br />
                        Yes, __________it on the radio last week. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="a" />
                        I was hearing
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="b" />
                        I hear
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="c" />
                        I've heard
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="d" />
                        I heard
                    </div>
                </div>

                <br />

                <p><strong>14. My brother was __________ all week. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="a" />
                        at the home
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="b" />
                        in the home
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="c" />
                        in home
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="d" />
                        at home
                    </div>
                </div>

                <br />

                <p><strong>15.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="a" />
                        He listens at the radio every day.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="b" />
                        He hears to the radio every day.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="c" />
                        He listens to the radio every day.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="d" />
                        He listens the radio every day.
                    </div>
                </div>

                <br />

                <p><strong>16. What's ______ ocean in the world? </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="a" />
                        the bigest
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="b" />
                        the most big
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="c" />
                        the bigger
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="d" />
                        the biggest
                    </div>
                </div>

                <br />

                <p><strong>17. "Do you like that shop?" "Yes, I ______ every week."  </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="a" />
                        go there
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="b" />
                        come here
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="c" />
                        come there
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="d" />
                        go here
                    </div>
                </div>

                <br />

                <p><strong>18. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="a" />
                        There's not lot snow.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="b" />
                        There is not many snow.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="c" />
                        There aren't much snow.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="d" />
                        There isn't much snow.
                    </div>
                </div>

                <br />

                <p><strong>19. How's the baby? </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="a" />
                        She's very well.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="b" />
                        He's Alison's.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="c" />
                        That's the baby.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="d" />
                        She's a girl.
                    </div>
                </div>

                <br />

                <p><strong>20. That horror film was boring. It  _____________. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="a" />
                        was as scary
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="b" />
                        was more scary
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="c" />
                        was enough scary
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="d" />
                        wasn't scary enough
                    </div>
                </div>

                <br />

                <p><strong>21. I ______ my bike at least five times.  </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="a" />
                        have crash
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="b" />
                        've crashed
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="c" />
                        crashed
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="d" />
                        crash
                    </div>
                </div>

                <br />

                <p><strong>22. James ________ to play football tomorrow.  </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="a" />
                        is going
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="b" />
                        can
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="c" />
                        shall
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="d" />
                        will
                    </div>
                </div>

                <br />

                <p><strong>23. Where _______ on Saturdays? </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="a" />
                        John does go
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="b" />
                        does John go
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="c" />
                        John goes
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="d" />
                        do go John
                    </div>
                </div>

                <br />

                <p><strong>24. What does that girl do? </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="a" />
                        She's a student.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="b" />
                        She's student.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="c" />
                        It's a student.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="d" />
                        She's a student girl.
                    </div>
                </div>

                <br />

                <p><strong>25. I'm going to give __________. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="a" />
                        to him a DVD
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="b" />
                        a DVD him
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="c" />
                        him a DVD
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="d" />
                        some DVD to him
                    </div>
                </div>
            </fieldset>


            <fieldset class="addmargintop60">
                <legend>Write a paragraph / some sentences about yourself.</legend>
                {!! Form::textarea('respuesta[ptxt]', null, ['id' => 'respuesta[ptxt]', 'class' => 'form-control ptxt']) !!}
            </fieldset>

            <hr>

        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-10">
            <button id="enviarformCIC" class="btn btn-block btn-success">Enviar</button>
            {!! Form::hidden('datos', serialize($_POST['datos']), array('id' => 'datos')) !!}
        </div>
    </div>

    {!! Form::close() !!}

</div>