<div id="test01">
    {!! Form::open(array('url' => route('area.forms.post',[$form->id, $booking->id]), 'id'=>'tercero')) !!}

    <div class="form-group row">
            <div class="col-md-10">
                <fieldset>
                    <legend>Text A</legend>
                    <h2>Read the tests below and choose an appropriate word in each space.</h2>
                    <div>
                        <blockquote>
                            <p>
                                Dear Jose,</p>
                            <p align="justify">Here is a photograph of me.<strong> ___(1)___</strong>'m afraid it isn't very good! I'
                                <strong>___(2)___</strong> tall and very thin, and I've got
                                <strong>___(3)___</strong> brown hair and grey eyes. I look like
                                <strong>___(4)___</strong> mother - she's a social worker.  My father
                                <strong>___(5)___</strong> for a chemical company.  I've got a
                                <strong>___(6)___</strong> called Jack, and two sisters, Emma and Rose.
                                <strong>___(7)___</strong> 're all younger than me.<br />
                                <br />
                                I'm 18,
                                <strong>___(8)___</strong> I was born in Northwich, a town in
                                <strong>___(9)___</strong> north-west of England, near Manchester. I went
                                <strong>___(10)___</strong> school there for seven years. Then my parents
                                <strong>___(11)___</strong> to London, so I went to secondary school
                                <strong>___(12)___</strong> London. I left school last year. Now I'
                                <strong>___(13)___</strong> a student and I'm going to be
                                <strong>___(14)___</strong> doctor. Its very hard work, but I
                                <strong>___(15)___</strong> it!<br />
                                <br />
                                I don't have much free time
                                <strong>___(16)___</strong> the moment, but I like going to the
                                <strong>___(17)___</strong> and going to concerts.  I also like football
                                <strong>___(18)___</strong> sailing.

                                Please write and tell me all about
                                <strong>___(19)___</strong>.</p>
                            <p>Pat </p>
                        </blockquote>

                        <p>&nbsp;</p>
                        <div>
                            <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="30" height="25" align="left" valign="bottom"><strong> &nbsp; 1.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA01]" id="respuesta[tA01]" value="You" />
                                        You</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA01]" id="respuesta[tA01]" value="He" />
                                        He</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA01]" id="respuesta[tA01]" value="I" />
                                        I</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA01]" id="respuesta[tA01]" value="It" />
                                        It</td>
                                    <td align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>11.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA11]" id="respuesta[tA11]" value="went" />
                                        went</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA11]" id="respuesta[tA11]" value="moved" />
                                        moved</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA11]" id="respuesta[tA11]" value="lived" />
                                        lived</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA11]" id="respuesta[tA11]" value="translated" />
                                        translated</td>
                                </tr>
                                <tr>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>&nbsp; 2.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA02]" id="respuesta[tA02]" value="m" />
                                        m</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA02]" id="respuesta[tA02]" value="re" />
                                        re</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA02]" id="respuesta[tA02]" value="ve" />
                                        ve</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA02]" id="respuesta[tA02]" value="s" />
                                        s</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>12.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA12]" id="respuesta[tA12]" value="at" />
                                        at</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA12]" id="respuesta[tA12]" value="in" />
                                        in</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA12]" id="respuesta[tA12]" value="on" />
                                        on</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA12]" id="respuesta[tA12]" value="the" />
                                        the</td>
                                </tr>
                                <tr>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>&nbsp; 3.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA03]" id="respuesta[tA03]" value="a" />
                                        a</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA03]" id="respuesta[tA03]" value="the" />
                                        the</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA03]" id="respuesta[tA03]" value="any" />
                                        any</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA03]" id="respuesta[tA03]" value="light " />
                                        light</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>13.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA13]" id="respuesta[tA13]" value="m" />
                                        m</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA13]" id="respuesta[tA13]" value="do" />
                                        do</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA13]" id="respuesta[tA13]" value="have" />
                                        have</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA13]" id="respuesta[tA13]" value="study" />
                                        study</td>
                                </tr>
                                <tr>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>&nbsp; 4.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA04]" id="respuesta[tA04]" value="me" />
                                        me</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA04]" id="respuesta[tA04]" value="my" />
                                        my</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA04]" id="respuesta[tA04]" value="her" />
                                        her</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA04]" id="respuesta[tA04]" value="the" />
                                        the</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>14.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA14]" id="respuesta[tA14]" value="the" />
                                        the</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA14]" id="respuesta[tA14]" value="as" />
                                        as</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA14]" id="respuesta[tA14]" value="a" />
                                        a</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA14]" id="respuesta[tA14]" value="work" />
                                        work</td>
                                </tr>
                                <tr>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>&nbsp; 5.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA05]" id="respuesta[tA05]" value="work" />
                                        work</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA05]" id="respuesta[tA05]" value="works" />
                                        works</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA05]" id="respuesta[tA05]" value="scientist" />
                                        scientist</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA05]" id="respuesta[tA05]" value="travails" />
                                        travails</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>15.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA15]" id="respuesta[tA15]" value="like" />
                                        like</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA15]" id="respuesta[tA15]" value="loving" />
                                        loving</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA15]" id="respuesta[tA15]" value="enjoyed" />
                                        enjoyed</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA15]" id="respuesta[tA15]" value="have" />
                                        have</td>
                                </tr>
                                <tr>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>&nbsp; 6.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA06]" id="respuesta[tA06]" value="brother" />
                                        brother</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA06]" id="respuesta[tA06]" value="sister" />
                                        sister</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA06]" id="respuesta[tA06]" value="brothers" />
                                        brothers</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA06]" id="respuesta[tA06]" value="friend" />
                                        friend</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>16.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA16]" id="respuesta[tA16]" value="in" />
                                        in</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA16]" id="respuesta[tA16]" value="on" />
                                        on</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA16]" id="respuesta[tA16]" value="by" />
                                        by</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA16]" id="respuesta[tA16]" value="at" />
                                        at</td>
                                </tr>
                                <tr>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>&nbsp; 7.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA07]" id="respuesta[tA07]" value="They" />
                                        They</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA07]" id="respuesta[tA07]" value="He" />
                                        He</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA07]" id="respuesta[tA07]" value="She" />
                                        She</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA07]" id="respuesta[tA07]" value="We" />
                                        We</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>17.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA17]" id="respuesta[tA17]" value="TV" />
                                        TV</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA17]" id="respuesta[tA17]" value="theatre" />
                                        theatre</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA17]" id="respuesta[tA17]" value="movie" />
                                        movie</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA17]" id="respuesta[tA17]" value="swimming" />
                                        swimming</td>
                                </tr>
                                <tr>
                                    <td width="30" height="25" align="left" valign="bottom"><strong> &nbsp; 8.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA08]" id="respuesta[tA08]" value="but" />
                                        but</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA08]" id="respuesta[tA08]" value="or" />
                                        or</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA08]" id="respuesta[tA08]" value="and" />
                                        and</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA08]" id="respuesta[tA08]" value="too" />
                                        too</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>18.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA18]" id="respuesta[tA18]" value="but" />
                                        but</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA18]" id="respuesta[tA18]" value="also" />
                                        also</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA18]" id="respuesta[tA18]" value="and" />
                                        and</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA18]" id="respuesta[tA18]" value="very" />
                                        very</td>
                                </tr>
                                <tr>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>&nbsp; 9.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA09]" id="respuesta[tA09]" value="a" />
                                        a</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA09]" id="respuesta[tA09]" value="more" />
                                        more</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA09]" id="respuesta[tA09]" value="at" />
                                        at</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA09]" id="respuesta[tA09]" value="the" />
                                        the</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>19.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA19]" id="respuesta[tA19]" value="yourself" />
                                        yourself</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA19]" id="respuesta[tA19]" value="me" />
                                        me</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA19]" id="respuesta[tA19]" value="your" />
                                        your</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA19]" id="respuesta[tA19]" value="it" />
                                        it</td>
                                </tr>
                                <tr>
                                    <td width="30" height="25" align="left" valign="bottom"><strong>10.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA10]" id="respuesta[tA10]" value="to" />
                                        to</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA10]" id="respuesta[tA10]" value="the" />
                                        the</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA10]" id="respuesta[tA10]" value="a" />
                                        a</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA10]" id="respuesta[tA10]" value="at" />
                                        at</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="85" height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="85" height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="85" height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="85" height="25" align="left" valign="bottom">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                        <br />
                    </div>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </fieldset>
                <fieldset>
                    <legend>Text B</legend>
                    <h2>Read the tests below and choose an appropriate word in each space.</h2>
                    <div>
                        <blockquote>
                            <p>
                                Dear Mary,
                            </p>
                            <p align="justify">I worked for a small
                                <strong>___(20)___</strong> for ten years before I took on
                                <strong>___(21)___</strong> new job with a much larger firm.
                                <strong>___(22)___</strong> I didn't earn as much in
                                <strong>___(23)___</strong> old job, I got on much better
                                <strong>___(24)___</strong> my boss and the other employees  than
                                <strong>___(25)___</strong> do now.
                            </p>
                            <p align="justify">I've had the new
                                <strong>___(26)___</strong> for more than a year and have
                                <strong>___(27)___</strong> that my personal life has suffered.&nbsp; I
                                <strong>___(28)___</strong> expected to travel a lot and never
                                <strong>___(29)___</strong> any time for my family.&nbsp; I find
                                <strong>___(30)___</strong> job less interesting  than the old one
                                <strong>___(31)___</strong> .&nbsp; There isn't the same contact with
                                <strong>___(32)___</strong> I used to have.
                            </p>
                            <p align="justify">A few days
                                <strong>___(33)___</strong> I happened to see my old boss
                                <strong>___(34)___</strong> .&nbsp; When I told him how I felt,
                                <strong>___(35)___</strong> offered me my old job back.&nbsp; I
                                <strong>___(36)___</strong> him I would think about it.&nbsp; If
                                <strong>___(37)___</strong> take the offer, I will be happier
                                <strong>___(38)___</strong> my salary won't be as good.</p>
                            <p>
                                <strong>___(39)___</strong> would you advise me to do?</p>
                            <p>
                                <br />
                                M.L. Hamilton
                            </p>
                            </blockquote>

                            <p>&nbsp;</p>
                            <div>
                                <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="30" align="left" valign="bottom"><strong> 20.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA20]" id="respuesta[tA20]" value="enterprise" />
                                            enterprise</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA20]" id="respuesta[tA20]" value="company" />
                                            company</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA20]" id="respuesta[tA20]" value="empire" />
                                            empire</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA20]" id="respuesta[tA20]" value="shopping" />
                                            shopping</td>
                                        <td align="left" valign="bottom">&nbsp;</td>
                                        <td width="30" align="left" valign="bottom"><strong>30.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA30]" id="respuesta[tA30]" value="that" />
                                            that</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA30]" id="respuesta[tA30]" value="these" />
                                            these</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA30]" id="respuesta[tA30]" value="this" />
                                            this</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA30]" id="respuesta[tA30]" value="actual" />
                                            actual</td>
                                    </tr>
                                    <tr>
                                        <td width="30" align="left" valign="bottom"><strong>21.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA21]" id="respuesta[tA21]" value="a" />
                                            a</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA21]" id="respuesta[tA21]" value="the" />
                                            the</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA21]" id="respuesta[tA21]" value="his" />
                                            his</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA21]" id="respuesta[tA21]" value="any" />
                                            any</td>
                                        <td height="25" align="left" valign="bottom">&nbsp;</td>
                                        <td width="30" align="left" valign="bottom"><strong>31.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA31]" id="respuesta[tA31]" value="does" />
                                            does</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA31]" id="respuesta[tA31]" value="had" />
                                            had</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA31]" id="respuesta[tA31]" value="job" />
                                            job</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA31]" id="respuesta[tA31]" value="was" />
                                            was</td>
                                    </tr>
                                    <tr>
                                        <td width="30" align="left" valign="bottom"><strong>22.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA22]" id="respuesta[tA22]" value="However" />
                                            However</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA22]" id="respuesta[tA22]" value="Despite" />
                                            Despite</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA22]" id="respuesta[tA22]" value="But" />
                                            But</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA22]" id="respuesta[tA22]" value="Although" />
                                            Although</td>
                                        <td height="25" align="left" valign="bottom">&nbsp;</td>
                                        <td width="30" align="left" valign="bottom"><strong>32.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA32]" id="respuesta[tA32]" value="people" />
                                            people</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA32]" id="respuesta[tA32]" value="atmosphere" />
                                            atmosphere</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA32]" id="respuesta[tA32]" value="friendly" />
                                            friendly</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA32]" id="respuesta[tA32]" value="persons" />
                                            persons</td>
                                    </tr>
                                    <tr>
                                        <td width="30" align="left" valign="bottom"><strong>23.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA23]" id="respuesta[tA23]" value="the" />
                                            the</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA23]" id="respuesta[tA23]" value="her" />
                                            her</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA23]" id="respuesta[tA23]" value="other" />
                                            other</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA23]" id="respuesta[tA23]" value="every" />
                                            every</td>
                                        <td height="25" align="left" valign="bottom">&nbsp;</td>
                                        <td width="30" align="left" valign="bottom"><strong>33.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA33]" id="respuesta[tA33]" value="before" />
                                            before</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA33]" id="respuesta[tA33]" value="after" />
                                            after</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA33]" id="respuesta[tA33]" value="ago" />
                                            ago</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA33]" id="respuesta[tA33]" value="early" />
                                            early</td>
                                    </tr>
                                    <tr>
                                        <td width="30" align="left" valign="bottom"><strong>24.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA24]" id="respuesta[tA24]" value="over" />
                                            over</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA24]" id="respuesta[tA24]" value="with" />
                                            with</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA24]" id="respuesta[tA24]" value="together" />
                                            together</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA24]" id="respuesta[tA24]" value="at" />
                                            at</td>
                                        <td height="25" align="left" valign="bottom">&nbsp;</td>
                                        <td width="30" align="left" valign="bottom"><strong>34.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA34]" id="respuesta[tA34]" value="another" />
                                            another</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA34]" id="respuesta[tA34]" value="other" />
                                            other</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA34]" id="respuesta[tA34]" value="after" />
                                            after</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA34]" id="respuesta[tA34]" value="again" />
                                            again</td>
                                    </tr>
                                    <tr>
                                        <td width="30" align="left" valign="bottom"><strong>25.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA25]" id="respuesta[tA25]" value="I" />
                                            I</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA25]" id="respuesta[tA25]" value="he" />
                                            he</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA25]" id="respuesta[tA25]" value="they" />
                                            they</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA25]" id="respuesta[tA25]" value="me" />
                                            me</td>
                                        <td height="25" align="left" valign="bottom">&nbsp;</td>
                                        <td width="30" align="left" valign="bottom"><strong>35.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA35]" id="respuesta[tA35]" value="he" />
                                            he</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA35]" id="respuesta[tA35]" value="didn&rsquo;t" />
                                            didn&rsquo;t</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA35]" id="respuesta[tA35]" value="was" />
                                            was</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA35]" id="respuesta[tA35]" value="I" />
                                            I</td>
                                    </tr>
                                    <tr>
                                        <td width="30" align="left" valign="bottom"><strong>26.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA26]" id="respuesta[tA26]" value="work" />
                                            work</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA26]" id="respuesta[tA26]" value="office" />
                                            office</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA26]" id="respuesta[tA26]" value="working" />
                                            working</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA26]" id="respuesta[tA26]" value="job" />
                                            job</td>
                                        <td height="25" align="left" valign="bottom">&nbsp;</td>
                                        <td width="30" align="left" valign="bottom"><strong>36.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA36]" id="respuesta[tA36]" value="said" />
                                            said</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA36]" id="respuesta[tA36]" value="told" />
                                            told</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA36]" id="respuesta[tA36]" value="say" />
                                            say</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA36]" id="respuesta[tA36]" value="explained" />
                                            explained</td>
                                    </tr>
                                    <tr>
                                        <td width="30" align="left" valign="bottom"><strong> 27.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA27]" id="respuesta[tA27]" value="had" />
                                            had</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA27]" id="respuesta[tA27]" value="done" />
                                            done</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA27]" id="respuesta[tA27]" value="found" />
                                            found</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA27]" id="respuesta[tA27]" value="explained" />
                                            explained</td>
                                        <td height="25" align="left" valign="bottom">&nbsp;</td>
                                        <td width="30" align="left" valign="bottom"><strong>37.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA37]" id="respuesta[tA37]" value="I" />
                                            I</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA37]" id="respuesta[tA37]" value="he" />
                                            he</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA37]" id="respuesta[tA37]" value="don't" />
                                            don't</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA37]" id="respuesta[tA37]" value="will" />
                                            will</td>
                                    </tr>
                                    <tr>
                                        <td width="30" align="left" valign="bottom"><strong>28.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA28]" id="respuesta[tA28]" value="m" />
                                            m</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA28]" id="respuesta[tA28]" value="have" />
                                            have</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA28]" id="respuesta[tA28]" value="go" />
                                            go</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA28]" id="respuesta[tA28]" value="didn't" />
                                            didn't</td>
                                        <td height="25" align="left" valign="bottom">&nbsp;</td>
                                        <td width="30" align="left" valign="bottom"><strong>38.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA38]" id="respuesta[tA38]" value="despite" />
                                            despite</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA38]" id="respuesta[tA38]" value="and" />
                                            and</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA38]" id="respuesta[tA38]" value="though" />
                                            though</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA38]" id="respuesta[tA38]" value="also" />
                                            also</td>
                                    </tr>
                                    <tr>
                                        <td width="30" align="left" valign="bottom"><strong>29.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA29]" id="respuesta[tA29]" value="having" />
                                            having</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA29]" id="respuesta[tA29]" value="have" />
                                            have</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA29]" id="respuesta[tA29]" value="got" />
                                            got</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA29]" id="respuesta[tA29]" value="stay" />
                                            stay</td>
                                        <td height="25" align="left" valign="bottom">&nbsp;</td>
                                        <td width="30" align="left" valign="bottom"><strong>39.</strong></td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA39]" id="respuesta[tA39]" value="What" />
                                            What</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA39]" id="respuesta[tA39]" value="How" />
                                            How</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA39]" id="respuesta[tA39]" value="Why" />
                                            Why</td>
                                        <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA39]" id="respuesta[tA39]" value="it" />
                                            Tell</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </fieldset>
                <fieldset>
                    <legend>Text C</legend>
                    <h2>Read the tests below and choose an appropriate word in each space.</h2>
                    <div>
                            <blockquote>
                                <p align="justify">Hello and welcome to the programme
                                    <strong>___(40)___</strong> today features the latest devices  designed
                                    <strong>___(41)___</strong> protect you and your property from
                                    <strong>___(42)___</strong>.</p>
                                <p align="justify">Statistics  show that a burglary takes
                                    <strong>___(43)___</strong> every 90 seconds night and day:
                                    <strong>___(44)___</strong> out of ten break-ins are spontaneous
                                    <strong>___(45)___</strong> take less than ten minutes:  and
                                    <strong>___(46)___</strong> out of ten are through insecure
                                    <strong>___(47)___</strong> and windows.</p>
                                <p align="justify">Tomorrow  the first exhibition
                                    <strong>___(48)___</strong> it's kind ever to be seen
                                    <strong>___(49)___</strong> the  public, the National Crime Prevention
                                    <strong>___(50)___</strong> will open at London's Barbican Centre
                                    <strong>___(51)___</strong> more than sixty companies will be
                                    <strong>___(52)___</strong> their household security  systems.</p>
                                <p align="justify">The  depressing
                                    <strong>___(53)___</strong> is that household and personal security
                                    <strong>___(54)___</strong> now a boom business.&nbsp; The kind
                                    <strong>___(55)___</strong> devices which you can fit and
                                    <strong>___(56)___</strong> yourself and which emit an  ear-shattering
                                    <strong>___(57)___</strong> are becoming increasingly popular and these
                                    <strong>___(58)___</strong> be represented at the exhibition along
                                    <strong>___(59)___</strong> the more conventional  alarms and locks.</p>
                                <p align="left">
                                    <strong>___(60)___</strong> reporter, Nick Bell went along to a preview of the exhibition and here's his  report...
                                </p>
                            </blockquote>
                            <p>&nbsp;</p>
                        </div>
                    <div>
                            <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="30" align="left" valign="bottom"><strong> 40.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA40]" id="respuesta[tA40]" value="and" />
                                        and</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA40]" id="respuesta[tA40]" value="which" />
                                        which</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA40]" id="respuesta[tA40]" value="what" />
                                        what</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA40]" id="respuesta[tA40]" value="where" />
                                        where</td>
                                    <td align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" align="left" valign="bottom"><strong>51.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA51]" id="respuesta[tA51]" value="which" />
                                        which</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA51]" id="respuesta[tA51]" value="that" />
                                        that</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA51]" id="respuesta[tA51]" value="with" />
                                        with</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA51]" id="respuesta[tA51]" value="where" />
                                        where</td>
                                </tr>
                                <tr>
                                    <td width="30" align="left" valign="bottom"><strong>41.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA41]" id="respuesta[tA41]" value="to" />
                                        to</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA41]" id="respuesta[tA41]" value="for" />
                                        for</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA41]" id="respuesta[tA41]" value="by" />
                                        by</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA41]" id="respuesta[tA41]" value="in" />
                                        in</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" align="left" valign="bottom"><strong>52.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA52]" id="respuesta[tA52]" value="show" />
                                        show</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA52]" id="respuesta[tA52]" value="displaying" />
                                        displaying</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA52]" id="respuesta[tA52]" value="doing" />
                                        doing</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA52]" id="respuesta[tA52]" value="preventing" />
                                        preventing</td>
                                </tr>
                                <tr>
                                    <td width="30" align="left" valign="bottom"><strong>42.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA42]" id="respuesta[tA42]" value="stolen" />
                                        stolen</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA42]" id="respuesta[tA42]" value="robber" />
                                        robber</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA42]" id="respuesta[tA42]" value="losing" />
                                        losing</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA42]" id="respuesta[tA42]" value="theft" />
                                        theft</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" align="left" valign="bottom"><strong>53.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA53]" id="respuesta[tA53]" value="fact" />
                                        fact</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA53]" id="respuesta[tA53]" value="think" />
                                        think</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA53]" id="respuesta[tA53]" value="however" />
                                        however</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA53]" id="respuesta[tA53]" value="people" />
                                        people</td>
                                </tr>
                                <tr>
                                    <td width="30" align="left" valign="bottom"><strong>43.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA43]" id="respuesta[tA43]" value="place" />
                                        place</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA43]" id="respuesta[tA43]" value="happens" />
                                        happens</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA43]" id="respuesta[tA43]" value="part" />
                                        part</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA43]" id="respuesta[tA43]" value="on" />
                                        on</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" align="left" valign="bottom"><strong>54.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA54]" id="respuesta[tA54]" value="becoming" />
                                        becoming</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA54]" id="respuesta[tA54]" value="does" />
                                        does</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA54]" id="respuesta[tA54]" value="is" />
                                        is</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA54]" id="respuesta[tA54]" value="can" />
                                        can</td>
                                </tr>
                                <tr>
                                    <td width="30" align="left" valign="bottom"><strong>44.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA44]" id="respuesta[tA44]" value="any" />
                                        any</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA44]" id="respuesta[tA44]" value="five" />
                                        five</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA44]" id="respuesta[tA44]" value="most" />
                                        most</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA44]" id="respuesta[tA44]" value="almost" />
                                        almost</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" align="left" valign="bottom"><strong>55.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA55]" id="respuesta[tA55]" value="type" />
                                        type</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA55]" id="respuesta[tA55]" value="of" />
                                        of</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA55]" id="respuesta[tA55]" value="some" />
                                        some</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA55]" id="respuesta[tA55]" value="that" />
                                        that</td>
                                </tr>
                                <tr>
                                    <td width="30" align="left" valign="bottom"><strong>45.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA45]" id="respuesta[tA45]" value="and" />
                                        and</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA45]" id="respuesta[tA45]" value="they" />
                                        they</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA45]" id="respuesta[tA45]" value="which" />
                                        which</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA45]" id="respuesta[tA45]" value="don't" />
                                        don't</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" align="left" valign="bottom"><strong>56.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA56]" id="respuesta[tA56]" value="using" />
                                        using</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA56]" id="respuesta[tA56]" value="for" />
                                        for</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA56]" id="respuesta[tA56]" value="safety" />
                                        safety</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA56]" id="respuesta[tA56]" value="remove" />
                                        remove</td>
                                </tr>
                                <tr>
                                    <td width="30" align="left" valign="bottom"><strong>46.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA46]" id="respuesta[tA46]" value="every" />
                                        every</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA46]" id="respuesta[tA46]" value="three" />
                                        three</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA46]" id="respuesta[tA46]" value="some" />
                                        some</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA46]" id="respuesta[tA46]" value="many" />
                                        many</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" align="left" valign="bottom"><strong>57.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA57]" id="respuesta[tA57]" value="sound" />
                                        sound</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA57]" id="respuesta[tA57]" value="hear" />
                                        hear</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA57]" id="respuesta[tA57]" value="loud" />
                                        loud</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA57]" id="respuesta[tA57]" value="effect" />
                                        effect</td>
                                </tr>
                                <tr>
                                    <td width="30" align="left" valign="bottom"><strong> 47.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA47]" id="respuesta[tA47]" value="open" />
                                        open</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA47]" id="respuesta[tA47]" value="closed" />
                                        closed</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA47]" id="respuesta[tA47]" value="locks" />
                                        locks</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA47]" id="respuesta[tA47]" value="keys" />
                                        keys</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" align="left" valign="bottom"><strong>58.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA58]" id="respuesta[tA58]" value="have" />
                                        have</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA58]" id="respuesta[tA58]" value="will" />
                                        will</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA58]" id="respuesta[tA58]" value="to" />
                                        to</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA58]" id="respuesta[tA58]" value="would" />
                                        would</td>
                                </tr>
                                <tr>
                                    <td width="30" align="left" valign="bottom"><strong>48.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA48]" id="respuesta[tA48]" value="of" />
                                        of</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA48]" id="respuesta[tA48]" value="ever" />
                                        ever</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA48]" id="respuesta[tA48]" value="be" />
                                        be</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA48]" id="respuesta[tA48]" value="in" />
                                        in</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" align="left" valign="bottom"><strong>59.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA59]" id="respuesta[tA59]" value="with" />
                                        with</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA59]" id="respuesta[tA59]" value="by" />
                                        by</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA59]" id="respuesta[tA59]" value="together" />
                                        together</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA59]" id="respuesta[tA59]" value="all" />
                                        all</td>
                                </tr>
                                <tr>
                                    <td width="30" align="left" valign="bottom"><strong>49.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA49]" id="respuesta[tA49]" value="for" />
                                        for</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA49]" id="respuesta[tA49]" value="by" />
                                        by</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA49]" id="respuesta[tA49]" value="in" />
                                        in</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA49]" id="respuesta[tA49]" value="with" />
                                        with</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" align="left" valign="bottom"><strong>60.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA60]" id="respuesta[tA60]" value="His" />
                                        His</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA60]" id="respuesta[tA60]" value="When" />
                                        When</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA60]" id="respuesta[tA60]" value="Our" />
                                        Our</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA60]" id="respuesta[tA60]" value="However" />
                                        However</td>
                                </tr>
                                <tr>
                                    <td width="30" align="left" valign="bottom"><strong>50.</strong></td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA50]" id="respuesta[tA50]" value="exhibition" />
                                        exhibition</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA50]" id="respuesta[tA50]" value="theft" />
                                        theft</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA50]" id="respuesta[tA50]" value="which" />
                                        which</td>
                                    <td width="85" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[tA50]" id="respuesta[tA50]" value="exposition" />
                                        exposition</td>
                                    <td height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="30" align="left" valign="bottom">&nbsp;</td>
                                    <td width="85" height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="85" height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="85" height="25" align="left" valign="bottom">&nbsp;</td>
                                    <td width="85" height="25" align="left" valign="bottom">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                </fieldset>

                <hr>

            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-10">
                <button id="enviarform03" class="btn btn-block btn-success">Enviar</button>
                {!! Form::hidden('datos', serialize($_POST['datos']), array('id' => 'datos')) !!}
            </div>
        </div>

    {!! Form::close() !!}

</div>