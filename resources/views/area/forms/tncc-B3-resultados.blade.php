<div class="form-group row">
    <div class="col-md-10">
        <fieldset class="cictest">
            <legend>Choose the correct answer. Only one answer is correct.</legend>

            <p><strong>1. TV programmes are ______ computer games with today's teenagers. </strong></p>
			@if($respuesta->p01  == 'a') 
                less popular that
			@endif
			@if($respuesta->p01  == 'b') 
                not as popular as
			@endif
			@if($respuesta->p01  == 'c') 
                the same popular
			@endif
			@if($respuesta->p01  == 'd') 
                as popular so
			@endif
            
            <br />

			<p><strong>1.	100 competitors had ______ the race.</strong></p>
				@if($respuesta->p01  == 'a') 
					put their names for
				@endif
				@if($respuesta->p01  == 'b') 
					entered for
				@endif
				@if($respuesta->p01  == 'c') 
					put themselves for
				@endif
				@if($respuesta->p01  == 'd') 
					taken part
				@endif

			<br />

			<p><strong>2. I _________ since breakfast and I'm very tired</strong></p>
				@if($respuesta->p02  == 'a') 
					travel
				@endif
				@if($respuesta->p02  == 'b') 
					am travelling<br>
				@endif
				@if($respuesta->p02  == 'c') 
					was travelling
				@endif
				@if($respuesta->p02  == 'd') 
					have been travelling
				@endif

			<br />

			<p><strong>3.	He __________ in his homework. </strong></p>
				@if($respuesta->p03  == 'a') 
					did a lot of faults
				@endif
				@if($respuesta->p03  == 'b') 
					made a lot of mistakes<br>
				@endif
				@if($respuesta->p03  == 'c') 
					did a lot of mistakes
				@endif
				@if($respuesta->p03  == 'd') 
					made a lot of faults
				@endif

			<br />

			<p><strong>4.	I would like ___________ it again.</strong></p>
				@if($respuesta->p04  == 'a') 
					that you read
				@endif
				@if($respuesta->p04  == 'b') 
					you reading
				@endif
				@if($respuesta->p04  == 'c') 
					you to read
				@endif
				@if($respuesta->p04  == 'd') 
					you read
				@endif

			<br />

			<p><strong>5.	I wish I ________ suggest something more suitable, but this is all we have. </strong></p>
				@if($respuesta->p05  == 'a') 
					should
				@endif
				@if($respuesta->p05  == 'b') 
					can
				@endif
				@if($respuesta->p05  == 'c') 
					would
				@endif
				@if($respuesta->p05  == 'd') 
					could
				@endif

			<br />

			<p><strong>6.	ÒWhere are you from?Ó They asked the boy _______________</strong></p>
				@if($respuesta->p06  == 'a') 
					he was from where
				@endif
				@if($respuesta->p06  == 'b') 
					where he was from
				@endif
				@if($respuesta->p06  == 'c') 
					where was he from
				@endif
				@if($respuesta->p06  == 'd') 
					where are you from
				@endif

			<br />

			<p><strong>7. My teachers always give me too much homework. But you ________ the same problems with yours, too.</strong></p>
				@if($respuesta->p07  == 'a') 
					must have
				@endif
				@if($respuesta->p07  == 'b') 
					ought to have<br>
				@endif
				@if($respuesta->p07  == 'c') 
					have to have
				@endif
				@if($respuesta->p07  == 'd') 
					can have
				@endif

			<br />

			<p><strong>8. If I ____________about it earlier, I would have told you.</strong></p>
				@if($respuesta->p08  == 'a') 
					had known
				@endif
				@if($respuesta->p08  == 'b') 
					would have known
				@endif
				@if($respuesta->p08  == 'c') 
					would know
				@endif
				@if($respuesta->p08  == 'd') 
					knew
				@endif

			<br />

			<p><strong>9. Don't worry. If the exam ______ tomorrow, I _________ you my notes.</strong></p>
				@if($respuesta->p09  == 'a') 
					will be / Ôll lend
				@endif
				@if($respuesta->p09  == 'b') 
					is / will lend
				@endif
				@if($respuesta->p09  == 'c') 
					be / will lend
				@endif
				@if($respuesta->p09  == 'd') 
					is / Ôd lend
				@endif

			<br />

			<p><strong>10. I don't think we've met before. You're confusing me with _______.</strong></p>
				@if($respuesta->p10  == 'a') 
					one other
				@endif
				@if($respuesta->p10  == 'b') 
					other person<br>
				@endif
				@if($respuesta->p10  == 'c') 
					someone else
				@endif
				@if($respuesta->p10  == 'd') 
					some other
				@endif

			<br />

			<p><strong>11.	She went to the dentist yesterday and  ________________.</strong></p>
				@if($respuesta->p11  == 'a') 
					has a tooth pulled out
				@endif
				@if($respuesta->p11  == 'b') 
					had pulled out a tooth
				@endif
				@if($respuesta->p11  == 'c') 
					was pulled out by the dentist
				@endif
				@if($respuesta->p11  == 'd') 
					had a tooth pulled out
				@endif

			<br />

			<p><strong>12.	He was a good runner so he _________ escape from the police.</strong></p>
				@if($respuesta->p12  == 'a') 
					was able to
				@endif
				@if($respuesta->p12  == 'b') 
					succeeded to
				@endif
				@if($respuesta->p12  == 'c') 
					could
				@endif
				@if($respuesta->p12  == 'd') 
					might
				@endif

			<br />

			<p><strong>13.	Most of the planet ______________ into desert by the year 2050.</strong></p>
				@if($respuesta->p13  == 'a') 
					is going to turn
				@endif
				@if($respuesta->p13  == 'b') 
					will turn
				@endif
				@if($respuesta->p13  == 'c') 
					is turning
				@endif
				@if($respuesta->p13  == 'd') 
					will have turned
				@endif

			<br />

			<p><strong>14.	Scientists __________ that the Earth is getting darker because of pollution in the atmosphere.</strong></p>
				@if($respuesta->p14  == 'a') 
					tell
				@endif
				@if($respuesta->p14  == 'b') 
					instruct
				@endif
				@if($respuesta->p14  == 'c') 
					inform
				@endif
				@if($respuesta->p14  == 'd') 
					claim
				@endif

			<br />

			<p><strong>15.	How long does the train take to ______ to London?</strong></p>
				@if($respuesta->p15  == 'a') 
					make
				@endif
				@if($respuesta->p15  == 'b') 
					reach
				@endif
				@if($respuesta->p15  == 'c') 
					get
				@endif
				@if($respuesta->p15  == 'd') 
					arrive
				@endif

			<br />

			<p><strong>16.	He came to the party, _____ he hadn't been invited.</strong></p>
				@if($respuesta->p16  == 'a') 
					in case
				@endif
				@if($respuesta->p16  == 'b') 
					even
				@endif
				@if($respuesta->p16  == 'c') 
					in spite of
				@endif
				@if($respuesta->p16  == 'd') 
					although
				@endif

			<br />

			<p><strong>17.	If I _____________the president I __________ the environment.Ó</strong></p>
				@if($respuesta->p17  == 'a') 
					were / would protect
				@endif
				@if($respuesta->p17  == 'b') 
					am / will to protect<br>
				@endif
				@if($respuesta->p17  == 'c') 
					was / will protect
				@endif
				@if($respuesta->p17  == 'd') 
					am / protect
				@endif

			<br />

			<p><strong>18.	His text said, ÒI ________ on the 7th.Ó</strong></p>
				@if($respuesta->p18  == 'a') 
					will be arrive
				@endif
				@if($respuesta->p18  == 'b') 
					will be arrived<br>
				@endif
				@if($respuesta->p18  == 'c') 
					would arrive
				@endif
				@if($respuesta->p18  == 'd') 
					am arriving
				@endif

			<br />

			<p><strong>19.	She  _____________________________ fantastic.</strong></p>
				@if($respuesta->p19  == 'a') 
					said the concert had been
				@endif
				@if($respuesta->p19  == 'b') 
					said the concert has been<br>
				@endif
				@if($respuesta->p19  == 'c') 
					told me the concert has been
				@endif
				@if($respuesta->p19  == 'd') 
					told the concert had been
				@endif

			<br />

			<p><strong>20.	That was a long journey. We _______ at 7 a.m. and only arrived at 10 p.m. </strong></p>
				@if($respuesta->p20  == 'a') 
					set away
				@endif
				@if($respuesta->p20  == 'b') 
					get away
				@endif
				@if($respuesta->p20  == 'c') 
					take off
				@endif
				@if($respuesta->p20  == 'd') 
					set off
				@endif

			<br />

			<p><strong>21.	She was sitting _________ on the park bench. </strong></p>
				@if($respuesta->p21  == 'a') 
					for herself
				@endif
				@if($respuesta->p21  == 'b') 
					by herself
				@endif
				@if($respuesta->p21  == 'c') 
					only herself
				@endif
				@if($respuesta->p21  == 'd') 
					in her own
				@endif

			<br />

			<p><strong>22.	That's the university course ____________.</strong></p>
				@if($respuesta->p22  == 'a') 
					I'm interested in
				@endif
				@if($respuesta->p22  == 'b') 
					what I'm interested on<br>
				@endif
				@if($respuesta->p22  == 'c') 
					I'm interested on
				@endif
				@if($respuesta->p22  == 'd') 
					what I'm interested in
				@endif

			<br />

			<p><strong>23.	 You _________ be 18 to drive a car in Europe.</strong></p>
				@if($respuesta->p23  == 'a') 
					should
				@endif
				@if($respuesta->p23  == 'b') 
					have to
				@endif
				@if($respuesta->p23  == 'c') 
					can't
				@endif
				@if($respuesta->p23  == 'd') 
					ought to
			
				@endif

			<br />

			<p><strong>24.	__________ for her birthday.</strong></p>
				@if($respuesta->p24  == 'a') 
					She was given $50
				@endif
				@if($respuesta->p24  == 'b') 
					She was been given $50<br>
				@endif
				@if($respuesta->p24  == 'c') 
					$50 they were given to her
				@endif
				@if($respuesta->p24  == 'd') 
					They were given to her $50
			
				@endif

			<br />

			<p><strong>25.	We talked about a lot of things ________ the way to the cinema.</strong></p>
				@if($respuesta->p25  == 'a') 
					through
				@endif
				@if($respuesta->p25  == 'b') 
					by
				@endif
				@if($respuesta->p25  == 'c') 
					on
				@endif
				@if($respuesta->p25  == 'd') 
					in
				@endif


        </fieldset>


        <fieldset class="addmargintop60">
			<legend>Choose ONE of the questions below and write your answer on the area provided. You should write about 150 words. Put the question number at the top of the text.</legend>
			<p><strong>(1) Narrative: </strong>write an email to a friend about a summer camp that went wrong. Mention what the problem was,  the consequences and what happened in the end. 150 words.</p>
			<p><strong>OR</strong></p>
			<p><strong>(2) Description: </strong>describe a person who was important to you when you were a child and say why you remember him  ?? her so well.  150 words.</p>
			<div class="addmargintop60"></div>
            {!! nl2br($respuesta->ptxt) !!}
        </fieldset>
        <hr>

    </div>
</div>