<div id="test01">
    {!! Form::open(array('url' => route('area.forms.post',[$form->id, $booking->id]), 'id'=>'primero')) !!}

    <div class="form-group row">
            <div class="col-md-10">
                <fieldset>
                    <legend>Choose the right word</legend>
                    <p><strong>1. You use these to cut paper. </strong>  <br />
                        <input type="radio" name="respuesta[pA01]" value="glasses" />
                        glasses&nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA01]" value="scissors" />
                        scissors&nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA01]" value="sharpeners" />
                        sharpeners<br />
                        <br />
                        <strong>2. When you study this subject you learn about islands and jungles.</strong><br />
                        <input type="radio" name="respuesta[pA02]" value="physics" />
                        physics&nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA02]" value="school" />
                        school&nbsp;&nbsp;&nbsp;/
                        <input type="radio" name="respuesta[pA02]" value="geography" />
                        geography<br />
                        <br />
                        <strong>3. If you can&rsquo;t spell a word you can use this to help you.</strong><br />
                        <input type="radio" name="respuesta[pA03]" value="dictionary" />
                        dictionary
                        &nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA03]" value="diary" />
                        diary
                        &nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA03]" value="calender" />
                        calender<br />
                        <br />
                        <strong>4. If you like drawing and painting you should study this subject.</strong><br />
                        <input type="radio" name="respuesta[pA04]" value="art" />
                        art
                        &nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA04]" value="books" />
                        books&nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA04]" value="technology" />
                        technology<br />
                        <br />
                        <strong>5. We need one of these to eat soup  ?? fruit salad.</strong><br />
                        <input type="radio" name="respuesta[pA05]" value="cup" />
                        cup
                        &nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA05]" value="fork" />
                        fork
                        &nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA05]" value="spoon" />
                        spoon<br />
                        <br />
                        <strong>6. When you are on holiday you send this to your  friends.</strong><br />
                        <input type="radio" name="respuesta[pA06]" value="beach" />
                        beach
                        &nbsp;/&nbsp;&nbsp;
                        <input type="radio" name="respuesta[pA06]" value="postcard" />
                        postcard&nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA06]" value="stamp" />
                        stamp<br />
                        <br />
                        <strong>7. You use this to cut bread, cheese and meat.</strong><br />
                        <input type="radio" name="respuesta[pA07]" value="knife" />
                        knife
                        &nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA07]" value="kitchen" />
                        kitchen
                        &nbsp;/&nbsp;&nbsp;
                        <input type="radio" name="respuesta[pA07]" value="fridge" />
                        fridge<br />
                        <br />
                        <strong>8. You can study here after you leave school.</strong><br />
                        <input type="radio" name="respuesta[pA08]" value="science" />
                        science&nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA08]" value="folder" />
                        folder&nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA08]" value="university" />
                        university<br />
                        <br />
                        <strong>9. We put a letter  ?? a card in this before we post  it.</strong><br />
                        <input type="radio" name="respuesta[pA09]" value="address" />
                        address&nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA09]" value="envelope" />
                        envelope&nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA09]" value="stamp" />
                        stamp<br />
                        <br />
                        <strong>10. You can buy this every day and read about things  which have happened.</strong><br />
                        <input type="radio" name="respuesta[pA10]" value="Internet" />
                        Internet
                        &nbsp;&nbsp;/&nbsp;
                        <input type="radio" name="respuesta[pA10]" value="e-mail" />
                        e-mail
                        &nbsp;/&nbsp;&nbsp;
                        <input type="radio" name="respuesta[pA10]" value="newspaper" />
                        newspaper<br />
                    </p>
                </fieldset>
                <fieldset>
                    <legend>Read the text and choose the best answer for each gap</legend>
                    <p><strong>Example</strong>&nbsp;&nbsp;&nbsp;&nbsp;</p>
                    <table width="650" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="60" height="25" align="left" valign="bottom"><strong>Nick:</strong></td>
                            <td height="25" colspan="3" align="left" valign="bottom">Hi,  Tom! Where are you going?</td>
                        </tr>
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="60" height="25" align="left" valign="bottom"><strong>Tom:</strong></td>
                            <td width="185" height="25" align="left" valign="bottom"><img src="/assets/forms/testnivelmaxcamps/checked.gif" alt="ok" width="18" height="18" />I&rsquo;m going to the park.</td>
                            <td width="185" height="25" align="left" valign="bottom"><img src="/assets/forms/testnivelmaxcamps/unchecked.gif" alt="ok" width="18" height="18" />I go to the park.</td>
                            <td width="185" height="25" align="left" valign="bottom"><img src="/assets/forms/testnivelmaxcamps/unchecked.gif" alt="ok" width="18" height="18" />I went to&nbsp; the park. </td>
                        </tr>
                    </table>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p><strong>Questions</strong></p>
                    <table width="650" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom"><strong>1.</strong></td>
                            <td width="60" height="25" align="left" valign="bottom"><strong>Nick:</strong></td>
                            <td width="185" height="25" align="left" valign="bottom">Can I come with you?</td>
                            <td width="185" height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="185" height="25" align="left" valign="bottom">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="60" height="25" align="left" valign="bottom"><strong>Tom:</strong></td>
                            <td width="185" height="25" align="left" valign="bottom"><input name="respuesta[pB01]" type="radio" value="Yes, you do." />
                                Yes, you do.</td>
                            <td width="185" height="25" align="left" valign="bottom"><input name="respuesta[pB01]" type="radio" value="Yes, you can." />
                                Yes, you can.</td>
                            <td width="185" height="25" align="left" valign="bottom"><input  name="respuesta[pB01]" type="radio"value="Yes, you are." />
                                Yes, you are.</td>
                        </tr>
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom"><strong>2.</strong></td>
                            <td width="60" height="25" align="left" valign="bottom"><strong>Nick:</strong></td>
                            <td width="185" height="60" align="left" valign="bottom">Do you like skating?</td>
                            <td width="185" height="60" align="left" valign="bottom">&nbsp;</td>
                            <td width="185" height="60" align="left" valign="bottom">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="60" height="25" align="left" valign="bottom"><strong>Tom:</strong></td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB02]" id="respuesta[pB02]" value="Yes, I liked it." />
                                Yes, I liked it.</td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB02]" id="respuesta[pB02]" value="Yes, I'm OK." />
                                Yes, I&rsquo;m OK.</td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB02]" id="respuesta[pB02]" value="Yes, I love it." />
                                Yes, I love it.</td>
                        </tr>
                        <tr>
                            <td width="35" height="60" align="left" valign="bottom"><strong>3.</strong></td>
                            <td width="60" height="60" align="left" valign="bottom"><strong>Nick:</strong></td>
                            <td height="60" colspan="3" align="left" valign="bottom">We can skate in the  park. Shall we go there now?</td>
                        </tr>
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="60" height="25" align="left" valign="bottom"><strong>Tom:</strong></td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB03]" id="respuesta[pB03]" value="All right." />
                                All  right.</td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB03]" id="respuesta[pB03]" value="It's fine." />
                                It&rsquo;s fine.</td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB03]" id="respuesta[pB03]" value="We are." />We are.</td>
                        </tr>
                        <tr>
                            <td width="35" height="60" align="left" valign="bottom"><strong>4.</strong></td>
                            <td width="60" height="60" align="left" valign="bottom"><strong>Nick:</strong></td>
                            <td width="185" height="60" align="left" valign="bottom">Can you skate well?</td>
                            <td width="185" height="60" align="left" valign="bottom">&nbsp;</td>
                            <td width="185" height="60" align="left" valign="bottom">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="60" height="25" align="left" valign="bottom"><strong>Tom:</strong></td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB04]" id="respuesta[pB04]" value="Yes, I'm well." />
                                Yes, I&rsquo;m well.</td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB04]" id="respuesta[pB04]" value="Yes, please." />Yes, please.</td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB04]" id="respuesta[pB04]" value="Yes, it's easy." />
                                Yes, it&rsquo;s easy.</td>
                        </tr>
                        <tr>
                            <td width="35" height="60" align="left" valign="bottom"><strong>5.</strong></td>
                            <td width="60" height="60" align="left" valign="bottom"><strong>Nick:</strong></td>
                            <td width="185" height="60" align="left" valign="bottom">What sports do you like?</td>
                            <td width="185" height="60" align="left" valign="bottom">&nbsp;</td>
                            <td width="185" height="60" align="left" valign="bottom">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="60" height="25" align="left" valign="bottom"><strong>Tom:</strong></td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB05]" id="respuesta[pB05]" value="I like playing football." />
                                I like playing football.</td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB05]" id="respuesta[pB05]" value="I am playing football." />I am playing football.</td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB05]" id="respuesta[pB05]" value="I can play football." />I can play football</td>
                        </tr>
                        <tr>
                            <td width="35" height="60" align="left" valign="bottom"><strong>6.</strong></td>
                            <td width="60" height="60" align="left" valign="bottom"><strong>Nick:</strong></td>
                            <td height="60" colspan="3" align="left" valign="bottom">I often play  football at the beach. Would you like  to come with me on Saturday?</td>
                        </tr>
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="60" height="25" align="left" valign="bottom"><strong>Tom:</strong></td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB06]" id="respuesta[pB06]" value="Sorry, I can't." />
                                Sorry, I can&rsquo;t.</td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB06]" id="respuesta[pB06]" value="Sorry, I didn't." />
                                Sorry, I didn&rsquo;t.</td>
                            <td width="185" align="left" valign="bottom"><input type="radio" name="respuesta[pB06]" id="respuesta[pB06]" value="Sorry, I'm not." />
                                Sorry, I&rsquo;m not.</td>
                        </tr>
                        <tr>
                            <td width="35" height="60" align="left" valign="bottom">&nbsp;</td>
                            <td width="60" align="left" valign="bottom"><strong>Nick:</strong></td>
                            <td width="185" height="60" align="left" valign="bottom">How about Sunday?</td>
                            <td width="185" height="60" align="left" valign="bottom">&nbsp;</td>
                            <td width="185" height="60" align="left" valign="bottom">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="35" height="30" align="left" valign="bottom">&nbsp;</td>
                            <td width="60" height="30" align="left" valign="bottom"><strong>Tom:</strong></td>
                            <td width="185" height="30" align="left" valign="bottom">Great! </td>
                            <td width="185" height="30" align="left" valign="bottom">&nbsp;</td>
                            <td width="185" height="30" align="left" valign="bottom">&nbsp;</td>
                        </tr>
                    </table>
                    <p>&nbsp;</p>
                </fieldset>
                <fieldset>
                    <legend>Read the text and choose the best answer for each gap</legend>
                    <h3><strong><br />
                            Dolphins</strong>  </h3>
                    <p>People love dolphins because <strong>__(0)__</strong>.  are beautiful to watch and friendly. Dolphins are also __<strong>(1)</strong>__ of the  cleverest animals and are just as clever as dogs.&nbsp; __<strong>(2)</strong>__ is possible to teach them in the  same way we teach monkeys and dogs.&nbsp; Some  people __<strong>(3)</strong>__ believe that dolphins have a special way of __<strong>(4)</strong>__ to each  other.<br />
                        __<strong>(5)</strong>__ many other sea animals  and fish, dolphins are in danger. Many dolphins are caught __<strong>(6)</strong>__ mistake in fishing  nets, but a __<strong>(7)__</strong> greater problem is that thousands of dolphins __<strong>(8)</strong>__ dying because the sea is no longer clean enough. </p>
                    <p>&nbsp;</p>
                    <p><strong>Example</strong></p>
                    <table width="317" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom"><strong>0.</strong></td>
                            <td width="94" height="25" align="left" valign="bottom"><img src="/assets/forms/testnivelmaxcamps/checked.gif" alt="ok" width="18" height="18" /> they</td>
                            <td width="94" height="25" align="left" valign="bottom"><img src="/assets/forms/testnivelmaxcamps/unchecked.gif" alt="ok" width="18" height="18" /> we</td>
                            <td width="94" height="25" align="left" valign="bottom"><img src="/assets/forms/testnivelmaxcamps/unchecked.gif" alt="ok" width="18" height="18" /> you</td>
                        </tr>
                    </table>
                    <p>&nbsp;</p>
                    <table width="80%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom"><strong>1.</strong></td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC01]" value="another" /> another</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC01]" value="one" /> one</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC01]" value="another" />all</td>
                            <td height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="35" height="25" align="left" valign="bottom"><strong>5.</strong></td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC05]" value="As" /> As</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC05]" value="For" /> For</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC05]" value="Like" /> Like</td>
                        </tr>
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom"><strong>2.</strong></td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC02]" value="There" /> There</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC02]" value="It" /> It</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC02]" value="This" /> This</td>
                            <td height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="35" height="25" align="left" valign="bottom"><strong>6.</strong></td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC06]" value="with" /> with</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC06]" value="by" /> by</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC06]" value="from" /> from</td>
                        </tr>
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom"><strong>3.</strong></td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC03]" value="quite" /> quite</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC03]" value="yet" /> yet</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC03]" value="even" /> even</td>
                            <td height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="35" height="25" align="left" valign="bottom"><strong>7.</strong></td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC07]" value="more" /> more</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC07]" value="much" /> much</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC07]" value="most" /> most</td>
                        </tr>
                        <tr>
                            <td width="35" height="25" align="left" valign="bottom"><strong>4.</strong></td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC04]" value="talking" /> talking</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC04]" value="talk" /> talk</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC04]" value="talked" /> talked</td>
                            <td height="25" align="left" valign="bottom">&nbsp;</td>
                            <td width="35" height="25" align="left" valign="bottom"><strong>8.</strong></td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC08]" value="were" /> were</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC08]" value="is" /> is</td>
                            <td width="94" height="25" align="left" valign="bottom"><input type="radio" name="respuesta[pC08]" value="are" /> are</td>
                        </tr>
                    </table>
                </fieldset>

                <hr>

            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-10">
                <button id="enviarform01" class="btn btn-block btn-success">Enviar</button>
                {!! Form::hidden('datos', serialize($_POST['datos']), array('id' => 'datos')) !!}
            </div>
        </div>

    {!! Form::close() !!}

</div>