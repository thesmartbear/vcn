<div class="row">
    <div class="col-md-10">
        <legend>Choose the right word.</legend>
        <p><strong>1. You use these to cut paper.</strong><br />
            {{$respuesta->pA01}}
            <br />
            <br />
            <strong>2. When you study this subject you learn about islands and jungles.</strong><br />
            {{$respuesta->pA02}}<br />
            <br />
            <strong>3. If you can&rsquo;t spell a word you can use this to  help you.</strong><br />
            {{$respuesta->pA03}}<br />
            <br />
            <strong>4. If you like drawing and painting you should  study this subject.</strong><br />
            {{$respuesta->pA04}}<br />
            <br />
            <strong>5. We need one of these to eat soup  ?? fruit salad.</strong><br />
            {{$respuesta->pA05}}<br />
            <br />
            <strong>6. When you are on holiday you send this to your  friends.</strong><br />
            {{$respuesta->pA06}}<br />
            <br />
            <strong>7. You use this to cut bread, cheese and meat.</strong><br />
            {{$respuesta->pA07}}<br />
            <br />
            <strong>8. You can study here after you leave school.</strong><br />
            {{$respuesta->pA08}}<br />
            <br />
            <strong>9. We put a letter  ?? a card in this before we post it.</strong><br />
            {{$respuesta->pA09}}<br />
            <br />
            <strong>10. You can buy this every day and read about things  which have happened.</strong><br />
            {{$respuesta->pA10}}<br />
        </p>
        <p>&nbsp;</p>

        <legend>Read  the text and choose the best answer for each gap </legend>
        <table width='650' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'><strong>1.</strong></td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td width='185' height='25' align='left' valign='bottom'>Can I come with you?</td>
                <td width='185' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='185' height='25' align='left' valign='bottom'>&nbsp;</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td height='25' colspan='3' align='left' valign='bottom'>
                    {{$respuesta->pB01}}</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'><strong>2.</strong></td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td width='185' height='60' align='left' valign='bottom'>Do you like skating?</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td colspan='3' align='left' valign='bottom'><label>{{$respuesta->pB02}}</label></td>
            </tr>
            <tr>
                <td width='35' height='60' align='left' valign='bottom'><strong>3.</strong></td>
                <td width='60' height='60' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td height='60' colspan='3' align='left' valign='bottom'>We can skate in the  park. Shall we go there now?</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td colspan='3' align='left' valign='bottom'><label>{{$respuesta->pB03}}</label></td>
            </tr>
            <tr>
                <td width='35' height='60' align='left' valign='bottom'><strong>4.</strong></td>
                <td width='60' height='60' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td width='185' height='60' align='left' valign='bottom'>Can you skate well?</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td colspan='3' align='left' valign='bottom'><label>{{$respuesta->pB04}}</label></td>
            </tr>
            <tr>
                <td width='35' height='60' align='left' valign='bottom'><strong>5.</strong></td>
                <td width='60' height='60' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td width='185' height='60' align='left' valign='bottom'>What sports do you like?</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td colspan='3' align='left' valign='bottom'>{{$respuesta->pB05}}</td>
            </tr>
            <tr>
                <td width='35' height='60' align='left' valign='bottom'><strong>6.</strong></td>
                <td width='60' height='60' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td height='60' colspan='3' align='left' valign='bottom'>I often play  football at the beach. Would you like  to come with me on Saturday?</td>
            </tr>
            <tr>
                <td width='35' height='25' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='25' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td colspan='3' align='left' valign='bottom'>{{$respuesta->pB06}}</td>
            </tr>
            <tr>
                <td width='35' height='60' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' align='left' valign='bottom'><strong>Nick:</strong></td>
                <td width='185' height='60' align='left' valign='bottom'>How about Sunday?</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
                <td width='185' height='60' align='left' valign='bottom'>&nbsp;</td>
            </tr>
            <tr>
                <td width='35' height='30' align='left' valign='bottom'>&nbsp;</td>
                <td width='60' height='30' align='left' valign='bottom'><strong>Tom:</strong></td>
                <td width='185' height='30' align='left' valign='bottom'>Great! </td>
                <td width='185' height='30' align='left' valign='bottom'>&nbsp;</td>
                <td width='185' height='30' align='left' valign='bottom'>&nbsp;</td>
            </tr>
        </table>
        <p>&nbsp;</p>
        <p>&nbsp;</p>

        <legend>Read  the text and choose the best answer for each gap </legend>
        <h3>Dolphins</h3>
        <p>People love dolphins because they are beautiful to watch and friendly. Dolphins are also <strong>{{$respuesta->pC01}}</strong> of the  cleverest animals and are just as clever as dogs.&nbsp;<strong>{{$respuesta->pC02}}</strong> is possible to teach them in the  same way we teach monkeys and dogs.&nbsp; Some  people <strong>{{$respuesta->pC03}}</strong> believe that dolphins have a special way of <strong>{{$respuesta->pC04}}</strong> to each  other.<br />
            <strong>{{$respuesta->pC05}}</strong> many other sea animals  and fish, dolphins are in danger. Many dolphins are caught <strong>{{$respuesta->pC06}}</strong> mistake in fishing  nets, but a <strong>{{$respuesta->pC07}}</strong> greater problem is that thousands of dolphins <strong>{{$respuesta->pC08}}</strong> dying because the sea is no longer clean enough. </p>
    </div>
</div>