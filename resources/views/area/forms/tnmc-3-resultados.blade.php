<div class="row">
    <div class="col-md-10">
        <legend>Read the tests below and choose an appropriate word in each space.</legend>
        <h2>Text A</h2>
        <blockquote>
            <p>
                Dear Jose,
            </p>
            <p align='justify'>Here is a photograph of me. <b>{{$respuesta->tA01}}</b>'m afraid it isn't very good! I'<b>{{$respuesta->tA02}}</b> tall and very thin, and I've got <b>{{$respuesta->tA03}}</b> brown hair and grey eyes.  I look like <b>{{$respuesta->tA04}}</b> mother - she's a social worker.  My father <b>{{$respuesta->tA05}}</b> for a chemical company.  I've got a <b>{{$respuesta->tA06}}</b> called Jack, and two sisters, Emma and Rose. <b>{{$respuesta->tA07}}</b>'re all younger than me.<br />
                <br />
                I'm 18, <b>{{$respuesta->tA08}}</b> I was born in Northwich, a town in <b>{{$respuesta->tA09}}</b> north-west of England, near Manchester.  I went <b>{{$respuesta->tA10}}</b> school there for seven years.  Then my parents <b>{{$respuesta->tA11}}</b> to London, so I went to secondary school <b>{{$respuesta->tA12}}</b> London.  I left school last year.  Now I'<b>{{$respuesta->tA13}}</b> a student and I'm going to be <b>{{$respuesta->tA14}}</b> doctor.  Its very hard work, but I <b>{{$respuesta->tA15}}</b> it!<br />
                <br />
                I don't have much free time <b>{{$respuesta->tA16}}</b> the moment, but I like going to the <b>{{$respuesta->tA17}}</b> and going to concerts.  I also like football <b>{{$respuesta->tA18}}</b> sailing.<br />
                <br />
                Please write and tell me all about <b>{{$respuesta->tA19}}</b>.</p>
            <p>Pat </p>
        </blockquote>
        <p>&nbsp;</p>
        <h2>Text B</h2>
        <blockquote>
            <p>
                Dear Mary,
            </p>
            <p align='justify'>I  worked for a small <b>{{$respuesta->tA20}}</b> for ten years before I took on <b>{{$respuesta->tA21}}</b> new job with a much larger firm. <b>{{$respuesta->tA22}}</b> I didn't earn as much in <b>{{$respuesta->tA23}}</b> old job, I got on much better <b>{{$respuesta->tA24}}</b> my boss and the other employees  than <b>{{$respuesta->tA25}}</b> do now.</p>
            <p align='justify'>I've  had the new <b>{{$respuesta->tA26}}</b> for more than a year and have <b>{{$respuesta->tA27}}</b> that my  personal life has suffered.&nbsp; I <b>{{$respuesta->tA28}}</b> expected to travel a lot and never <b>{{$respuesta->tA29}}</b> any time for my family.&nbsp; I find <b>{{$respuesta->tA30}}</b> job less interesting  than the old one <b>{{$respuesta->tA31}}</b>. There isn't the same contact with  <b>{{$respuesta->tA32}}</b> I used to have.</p>
            <p align='justify'>A  few days  <b>{{$respuesta->tA33}}</b> I happened to see my old boss <b>{{$respuesta->tA34}}</b>. When I told him how I felt, <b>{{$respuesta->tA35}}</b> offered me my old job back. I <b>{{$respuesta->tA36}}</b> him I would think about it. If <b>{{$respuesta->tA37}}</b> take the offer, I will be happier <b>{{$respuesta->tA38}}</b> my salary won't be as good.</p>
            <p><b>{{$respuesta->tA39}}</b> would you advise me to do?</p>
            <p><br />
                M.L. Hamilton </p>
        </blockquote>
        <p>&nbsp;</p>
        <h2>Text C</h2>
        <blockquote>
            <p align='justify'>Hello and welcome to the programme <b>{{$respuesta->tA40}}</b> today features the latest devices designed <b>{{$respuesta->tA41}}</b> protect you and your property from <b>{{$respuesta->tA42}}</b>.</p>
            <p align='justify'>Statistics  show that a burglary takes <b>{{$respuesta->tA43}}</b> every 90 seconds night and day: <b>{{$respuesta->tA44}}</b> out of ten break-ins are spontaneous <b>{{$respuesta->tA45}}</b> take less than ten minutes: and <b>{{$respuesta->tA46}}</b> out of ten are through insecure <b>{{$respuesta->tA47}}</b> and windows.</p>
            <p align='justify'>Tomorrow  the first exhibition <b>{{$respuesta->tA48}}</b> it's kind ever to be seen <b>{{$respuesta->tA49}}</b> the public, the National Crime Prevention <b>{{$respuesta->tA50}}</b> will open at London's Barbican Centre <b>{{$respuesta->tA51}}</b>more than sixty companies will be <b>{{$respuesta->tA52}}</b> their household security  systems.</p>
            <p align='justify'>The depressing <b>{{$respuesta->tA53}}</b> is that household and personal security <b>{{$respuesta->tA54}}</b> now a boom business. The kind  <b>{{$respuesta->tA55}}</b> devices which you can fit and <b>{{$respuesta->tA56}}</b> yourself and which emit an ear-shattering <b>{{$respuesta->tA57}}</b> are becoming increasingly popular and these <b>{{$respuesta->tA58}}</b> be represented at the exhibition along <b>{{$respuesta->tA59}}</b> the more conventional  alarms and locks.</p>
            <p align='left'><b>{{$respuesta->tA60}}</b> reporter, Nick Bell went along to a preview of the exhibition and here's his  report...</p>
        </blockquote>
    </div>
</div>