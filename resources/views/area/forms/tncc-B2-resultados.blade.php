<div class="form-group row">
    <div class="col-md-10">
        <fieldset class="cictest">
            <legend>Choose the correct answer. Only one answer is correct.</legend>

			<p><strong>1. I didn't hear what he was_________. </strong></p>
				@if($respuesta->p01  == 'a')              
					speaking
				@endif
				@if($respuesta->p01  == 'b')
						saying
					@endif
				@if($respuesta->p01  == 'c')
						talking
					@endif
				@if($respuesta->p01  == 'd')
					telling
				@endif

			<br />

			<p><strong>2. Jake couldn't remember where he ________ his car. </strong></p>
				@if($respuesta->p02  == 'a')              
					was parking
				@endif
				@if($respuesta->p02  == 'b')
						has parked
					@endif
				@if($respuesta->p02  == 'c')
						was parked
					@endif
				@if($respuesta->p02  == 'd')
					had parked
				@endif

			<br />

			<p><strong>3. The girl ________mother was ill was crying. </strong></p>
				@if($respuesta->p03  == 'a')              
					which
				@endif
				@if($respuesta->p03  == 'b')
						whose
					@endif
				@if($respuesta->p03  == 'c')
						of which
					@endif
				@if($respuesta->p03  == 'd')
					of whom
				@endif

			<br />

			<p><strong>4. Your letter _____________. </strong></p>
				@if($respuesta->p04  == 'a')              
					has arrived two days ago
				@endif
				@if($respuesta->p04  == 'b')
						arrived since two days
					@endif
				@if($respuesta->p04  == 'c')
						arrived two days ago
					@endif
				@if($respuesta->p04  == 'd')
					has arrived since two days.
				@endif

			<br />

			<p><strong>5. There are twelve of us, so______ get into the car at the same time. </strong></p>
				@if($respuesta->p05  == 'a')              
					we may not all
				@endif
				@if($respuesta->p05  == 'b')
						all we may not
					@endif
				@if($respuesta->p05  == 'c')
						all we can't
					@endif
				@if($respuesta->p05  == 'd')
					we can't all
				@endif

			<br />

			<p><strong>6. I'll have to buy __________ trousers. </strong></p>
				@if($respuesta->p06  == 'a')              
					two
				@endif
				@if($respuesta->p06  == 'b')
						a pair of
					@endif
				@if($respuesta->p06  == 'c')
						a couple of
					@endif
				@if($respuesta->p06  == 'd')
					a
				@endif

			<br />

			<p><strong>7. If you ________ help you, you only have to ask me. </strong></p>
				@if($respuesta->p07  == 'a')              
					want me to
				@endif
				@if($respuesta->p07  == 'b')
						want that I
					@endif
				@if($respuesta->p07  == 'c')
						want I should
					@endif
				@if($respuesta->p07  == 'd')
						are wanting me to
				@endif

			<br />

			<p><strong>8. I've been looking for you _________. </strong></p>
				@if($respuesta->p08  == 'a')              
					everywhere
				@endif
				@if($respuesta->p08  == 'b')
						for all places
					@endif
				@if($respuesta->p08  == 'c')
						anywhere
					@endif
				@if($respuesta->p08  == 'd')
						in all places
				@endif

			<br />

			<p><strong>9. Did you _______ sightseeing in Prague? </strong></p>
				@if($respuesta->p09  == 'a')              
					do
				@endif
				@if($respuesta->p09  == 'b')
						go
					@endif
				@if($respuesta->p09  == 'c')
						make
					@endif
				@if($respuesta->p09  == 'd')
					practise
				@endif

			<br />

			<p><strong>10. He ________ in his homework. </strong></p>
				@if($respuesta->p10  == 'a')              
					did a lot of faults
				@endif
				@if($respuesta->p10  == 'b')
						did a lot of mistakes
					@endif
				@if($respuesta->p10  == 'c')
						made a lot of mistakes
					@endif
				@if($respuesta->p10  == 'd')
					made a lot of faults
				@endif

			<br />

			<p><strong>11. I'll ring you as soon as I _______ there. </strong></p>
				@if($respuesta->p11  == 'a')              
					will get
				@endif
				@if($respuesta->p11  == 'b')
						will have got
					@endif
				@if($respuesta->p11  == 'c')
						shall get
					@endif
				@if($respuesta->p11  == 'd')
					get
				@endif

			<br />

			<p><strong>12. &quot;I went to the cinema last night&quot;.<br />
					He told ___________________ to the cinema the previous night. </strong></p>
				@if($respuesta->p12  == 'a')              
					me that he had been
				@endif
				@if($respuesta->p12  == 'b')
						to me that he went
					@endif
				@if($respuesta->p12  == 'c')
						me that he had gone
					@endif
				@if($respuesta->p12  == 'd')
						to me that he had been
				@endif

			<br />

			<p><strong>13. How _________? </strong></p>
				@if($respuesta->p13  == 'a')              
					was the thief getting in
				@endif
				@if($respuesta->p13  == 'b')
						the thief got in
					@endif
				@if($respuesta->p13  == 'c')
						has the thief got in
					@endif
				@if($respuesta->p13  == 'd')
						did the thief get in
				@endif

			<br />

			<p><strong>14. This question is ________difficult for me. </strong></p>
				@if($respuesta->p14  == 'a')              
					so much
				@endif
				@if($respuesta->p14  == 'b')
						enough
					@endif
				@if($respuesta->p14  == 'c')
						too much
					@endif
				@if($respuesta->p14  == 'd')
						too
				@endif

			<br />

			<p><strong>15. We ______ go to school tomorrow. It's a holiday. </strong></p>
				@if($respuesta->p15  == 'a')              
					mustn't
				@endif
				@if($respuesta->p15  == 'b')
						have to
					@endif
				@if($respuesta->p15  == 'c')
						don't have to
					@endif
				@if($respuesta->p15  == 'd')
						should
				@endif

			<br />

			<p><strong>16. If I _____________ seventeen, I could drive a car. </strong></p>
				@if($respuesta->p16  == 'a')              
					would have
				@endif
				@if($respuesta->p16  == 'b')
						would be
					@endif
				@if($respuesta->p16  == 'c')
						had
					@endif
				@if($respuesta->p16  == 'd')
					were
				@endif

			<br />

			<p><strong>17. Ask him to go to the library ________ some books and DVDs. </strong></p>
				@if($respuesta->p17  == 'a')              
					to get
				@endif
			@if($respuesta->p17  == 'b')              
					for getting
				@endif
			@if($respuesta->p17  == 'c')              
					that he gets
				@endif
			@if($respuesta->p17  == 'd')              
					in order he gets
				@endif

			<br />

			<p><strong>18. _________ at the moment, I'll go into the shops.</strong></p>
				@if($respuesta->p18  == 'a')              
					As it doesn't rain
				@endif
				@if($respuesta->p18  == 'b')              
						For it doesn't rain
					@endif
				@if($respuesta->p18  == 'c')              
						For it isn't raining
					@endif
				@if($respuesta->p18  == 'd')              
						As it isn't raining
				@endif

			<br />

			<p><strong>19. Have they started cooking ________? </strong></p>
				@if($respuesta->p19  == 'a')              
					yet
				@endif
				@if($respuesta->p19  == 'b')              
						ever
				@endif
				@if($respuesta->p19  == 'c')              
						still
				@endif
				@if($respuesta->p19  == 'd')              
					already
				@endif

			<br />

			<p><strong>20.	That was a long journey. We ___________ at 7 o'clock this morning and only arrived at 10 p.m. </strong></p>
				@if($respuesta->p20  == 'a')              
					set on
				@endif
				@if($respuesta->p20  == 'b')              
					took on
				@endif
				@if($respuesta->p20  == 'c')              
					take off
				@endif
				@if($respuesta->p20  == 'd')              
					set off
				@endif

			<br />

			<p><strong>21.	&quot;They will go out tomorrow night.&quot;<br />
					They said that they _________. </strong></p>
				@if($respuesta->p21  == 'a')              
					will go out the following night
				@endif
				@if($respuesta->p21  == 'b')
					would go out the following night
				@endif
				@if($respuesta->p21  == 'c')
					would go out tomorrow night
				@endif
				@if($respuesta->p21  == 'd')
					would went out the following night
				@endif

			<br />

			<p><strong>22. I _________ to your email from last week. </strong></p>
				@if($respuesta->p22  == 'a')              
					would like to reply
				@endif
				@if($respuesta->p22  == 'b')
					like to reply
				@endif
				@if($respuesta->p22  == 'c')
					am wanting to reply
				@endif
				@if($respuesta->p22  == 'd')
					would like replying
				@endif

			<br />

			<p><strong>23.	Your bicycle shouldn't be in the house! ______________. </strong></p>
				@if($respuesta->p23  == 'a')              
					Get out it!
				@endif
				@if($respuesta->p23  == 'b')
					Take it out!
				@endif
				@if($respuesta->p23  == 'c')
					Put it off!
				@endif
				@if($respuesta->p23  == 'd')
					Take it away!
				@endif

			<br />

			<p><strong>24. I had to go up the stairs because the lift _______. </strong></p>
				@if($respuesta->p24  == 'a')              
					was being repaired
				@endif
				@if($respuesta->p24  == 'b')
					was been repairing
				@endif
				@if($respuesta->p24  == 'c')
					was been repaired
				@endif
				@if($respuesta->p24  == 'd')
					was repairing
				@endif

			<br />

			<p><strong>25. She's been very kind, ________? </strong></p>
				@if($respuesta->p25  == 'a')              
					doesn't she
				@endif
				@if($respuesta->p25  == 'b')
					isn't she
				@endif
				@if($respuesta->p25  == 'c')
					hasn't she
				@endif
				@if($respuesta->p25  == 'd')
					wasn't she
				@endif

        </fieldset>


        <fieldset class="addmargintop60">
			<legend>Choose one of the following:</legend>
			<p><strong>(1) Describe your last birthday. Tell us:</strong></p>
			<ul>
				<li>when  it was</li>
				<li>what you did</li>
				<li>what presents you got</li>
				<li>how you felt at the end of the day</li>
			</ul>
			<p><strong>OR</strong></p>
			<p><strong>(2) Tell us about your first school. Say:</strong></p>
			<ul>
				<li>if you liked the building and why</li>
				<li>how you felt there</li>
				<li>what friends  you made</li>
				<li>who your favourite teacher  was and why</li>
				<li>what your favourite activity was and why </li>
			</ul>
			<div class="addmargintop60"></div>
            {!! nl2br($respuesta->ptxt) !!}
        </fieldset>
        <hr>

    </div>
</div>