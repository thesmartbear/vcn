<div class="form-group row">
    <div class="col-md-10">
        <fieldset class="cictest">
            <legend>Choose the correct answer. Only one answer is correct.</legend>

            <p><strong>1. TV programmes are ______ computer games with today's teenagers. </strong></p>
			@if($respuesta->p01  == 'a') 
                less popular that
			@endif
			@if($respuesta->p01  == 'b') 
                not as popular as
			@endif
			@if($respuesta->p01  == 'c') 
                the same popular
			@endif
			@if($respuesta->p01  == 'd') 
                as popular so
			@endif
            
            <br />

            <p><strong>2. "Whose flowers are they?" &quot;They're _______.&quot; </strong></p>
			@if($respuesta->p02  == 'a')
                to Mary
			@endif
			@if($respuesta->p02  == 'b')
                of Mary
			@endif
			@if($respuesta->p02  == 'c')
                Maries
			@endif
			@if($respuesta->p02  == 'd')
                Mary's
			@endif
            
            <br />

            <p><strong>3.</strong></p>
			@if($respuesta->p03  == 'a')
                Give his the jacket.
			@endif
			@if($respuesta->p03  == 'b')
                Give him the jacket.<br>
			@endif
			@if($respuesta->p03  == 'c')
                Give we the jacket.
			@endif
			@if($respuesta->p03  == 'd')
                Give their the jacket.
			@endif
            
            <br />

            <p><strong>4. _________ to the party last Saturday? </strong></p>
			@if($respuesta->p04  == 'a')
                Do you went...?
			@endif
			@if($respuesta->p04  == 'b')
                Did you went...?<br>
			@endif
			@if($respuesta->p04  == 'c')
                Did you go...?
			@endif
			@if($respuesta->p04  == 'd')
                Do you go...?
			@endif
            
            <br />

            <p><strong>5. Tony is looking at _________.  </strong></p>
			@if($respuesta->p05  == 'a')
                she
			@endif
			@if($respuesta->p05  == 'b')
                here
			@endif
			@if($respuesta->p05  == 'c')
                he
			@endif
			@if($respuesta->p05  == 'd')
                her
			@endif
            
            <br />

            <p><strong>6. When _________, give her this book. </strong></p>
			@if($respuesta->p06  == 'a')
                Alison will arrive
			@endif
			@if($respuesta->p06  == 'b')
                Alison arrives<br>
			@endif
			@if($respuesta->p06  == 'c')
                Alison arrive
			@endif
			@if($respuesta->p06  == 'd')
                is Alison arriving
			@endif
            
            <br />

            <p><strong>7. This mobile phone is  ________ that one. </strong></p>
			@if($respuesta->p07  == 'a')
                cheaper than
			@endif
			@if($respuesta->p07  == 'b')
                the cheapest<br>
			@endif
			@if($respuesta->p07  == 'c')
                more cheap
			@endif
			@if($respuesta->p07  == 'd')
                very cheaper
			@endif
            
            <br />

            <p><strong>8. ____________ lovely food! </strong></p>
			@if($respuesta->p08  == 'a')
                What
			@endif
			@if($respuesta->p08  == 'b')
                Which a
			@endif
			@if($respuesta->p08  == 'c')
                Which
			@endif
			@if($respuesta->p08  == 'd')
                What a
			@endif
            
            <br />

            <p><strong>9. I've had a bad cold _______. </strong></p>
			@if($respuesta->p09  == 'a')
                since five days
			@endif
			@if($respuesta->p09  == 'b')
                since last Friday<br>
			@endif
			@if($respuesta->p09  == 'c')
                five days ago
			@endif
			@if($respuesta->p09  == 'd')
                last Friday
			@endif
            
            <br />

            <p><strong>10. </strong></p>
			@if($respuesta->p10  == 'a')
                That girl is some of my friends.
			@endif
			@if($respuesta->p10  == 'b')
                That girl is me friend. <br>
			@endif
			@if($respuesta->p10  == 'c')
                This girl is one of my friends.
			@endif
			@if($respuesta->p10  == 'd')
                These girl's are friends.
			@endif
            
            <br />

            <p><strong>11. When we got to school, we _______ the bell.  </strong></p>
			@if($respuesta->p11  == 'a')
                were listening
			@endif
			@if($respuesta->p11  == 'b')
                were hearing
			@endif
			@if($respuesta->p11  == 'c')
                listened
			@endif
			@if($respuesta->p11  == 'd')
                heard
			@endif
            
            <br />

            <p><strong>12. I feel very well because I went to bed very early _____. </strong></p>
			@if($respuesta->p12  == 'a')
                last night
			@endif
			@if($respuesta->p12  == 'b')
                tonight
			@endif
			@if($respuesta->p12  == 'c')
                this night
			@endif
			@if($respuesta->p12  == 'd')
                in the night<br>

			@endif
            
            <br />

            <p><strong>13. Have you heard their latest song?<br />
                    Yes, __________it on the radio last week. </strong></p>
			@if($respuesta->p13  == 'a')
                I was hearing
			@endif
			@if($respuesta->p13  == 'b')
                I hear<br>
			@endif
			@if($respuesta->p13  == 'c')
                I've heard
			@endif
			@if($respuesta->p13  == 'd')
                I heard
			@endif
            
            <br />

            <p><strong>14. My brother was __________ all week. </strong></p>
			@if($respuesta->p14  == 'a')
                at the home
			@endif
			@if($respuesta->p14  == 'b')
                in the home<br>
			@endif
			@if($respuesta->p14  == 'c')
                in home
			@endif
			@if($respuesta->p14  == 'd')
                at home
			@endif
            
            <br />

            <p><strong>15.</strong></p>
			@if($respuesta->p15  == 'a')
                He listens at the radio every day.
			@endif
			@if($respuesta->p15  == 'b')
                He hears to the radio every day.<br>
			@endif
			@if($respuesta->p15  == 'c')
                He listens to the radio every day.
			@endif
			@if($respuesta->p15  == 'd')
                He listens the radio every day.
			@endif
            
            <br />

            <p><strong>16. What's ______ ocean in the world? </strong></p>
			@if($respuesta->p16  == 'a')
                the bigest
			@endif
			@if($respuesta->p16  == 'b')
                the most big<br>
			@endif
			@if($respuesta->p16  == 'c')
                the bigger
			@endif
			@if($respuesta->p16  == 'd')
                the biggest
			@endif
            
            <br />

            <p><strong>17. "Do you like that shop?" "Yes, I ______ every week."  </strong></p>
			@if($respuesta->p17  == 'a')
                go there
			@endif
			@if($respuesta->p17  == 'b')
                come here
			@endif
			@if($respuesta->p17  == 'c')
                come there
			@endif
			@if($respuesta->p17  == 'd')
                go here
			@endif
            
            <br />

            <p><strong>18. </strong></p>
			@if($respuesta->p18  == 'a')
                There's not lot snow.
			@endif
			@if($respuesta->p18  == 'b')
                There is not many snow.<br>
			@endif
			@if($respuesta->p18  == 'c')
                There aren't much snow.
			@endif
			@if($respuesta->p18  == 'd')
                There isn't much snow.
			@endif
            
            <br />

            <p><strong>19. How's the baby? </strong></p>
			@if($respuesta->p19  == 'a')
                She's very well.
			@endif
			@if($respuesta->p19  == 'b')
                He's Alison's.<br>
			@endif
			@if($respuesta->p19  == 'c')
                That's the baby.
			@endif
			@if($respuesta->p19  == 'd')
                She's a girl.
			@endif
            
            <br />

            <p><strong>20. That horror film was boring. It  _____________. </strong></p>
			@if($respuesta->p01  == 'a') 
                was as scary
			@endif
			@if($respuesta->p20  == 'b')
                was more scary<br>
			@endif
			@if($respuesta->p20  == 'c')
                was enough scary
			@endif
			@if($respuesta->p20  == 'd')
                wasn't scary enough
			@endif
            
            <br />

            <p><strong>21. I ______ my bike at least five times.  </strong></p>
			@if($respuesta->p21  == 'a')
                have crash
			@endif
			@if($respuesta->p21  == 'b')
                've crashed<br>
			@endif
			@if($respuesta->p21  == 'c')
                crashed
			@endif
			@if($respuesta->p21  == 'd')
                crash
			@endif
            
            <br />

            <p><strong>22. James ________ to play football tomorrow.  </strong></p>
			@if($respuesta->p22  == 'a')
                is going
			@endif
			@if($respuesta->p22  == 'b')
                can <br>
			@endif
			@if($respuesta->p22  == 'c')
                shall
			@endif
			@if($respuesta->p22  == 'd')
                will
			@endif
            
            <br />

            <p><strong>23. Where _______ on Saturdays? </strong></p>
			@if($respuesta->p23  == 'a')
                John does go
			@endif
			@if($respuesta->p23  == 'b')
                does John go
			@endif
			@if($respuesta->p23  == 'c')
                John goes
			@endif
			@if($respuesta->p23  == 'd')
                do go John
			@endif
            
            <br />

            <p><strong>24. What does that girl do? </strong></p>
			@if($respuesta->p24  == 'a')
                She's a student.
			@endif
			@if($respuesta->p24  == 'b')
                She's student.<br>
			@endif
			@if($respuesta->p24  == 'c')
                It's a student.
			@endif
			@if($respuesta->p24  == 'd')
                She's a student girl.
			@endif
            
            <br />

            <p><strong>25. I'm going to give __________. </strong></p>
			@if($respuesta->p25  == 'a')
                to him a DVD
			@endif
			@if($respuesta->p25  == 'b')
                a DVD him
			@endif
			@if($respuesta->p25  == 'c')
                him a DVD
			@endif
			@if($respuesta->p25  == 'd')
                some DVD to him
			@endif

        </fieldset>


        <fieldset class="addmargintop60">
            <legend>Write a paragraph / some sentences about yourself.</legend>
            {!! nl2br($respuesta->ptxt) !!}
        </fieldset>
        <hr>

    </div>
</div>