@extends('layouts.area')

@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop

@section('container')
    <h2 class="text-capitalize text-success">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</h2>
    <hr>

    {!! Form::open(array('url' => route('area.forms.post',[$form->id, $booking->id]), 'id'=>'opinionjovenes')) !!}

    <h2>Valoración</h2>
    <p>&nbsp;</p>

    @if(!$booking->convocatoria->alojamiento->tipo->es_familia)
        <h4>Alojamiento</h4>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-striped">
            <tbody>
            <tr>
                <th width="30%">&nbsp;</th>
                <th width="14%" class="text-center">Mal</th>
                <th width="14%" class="text-center">Regular</th>
                <th width="14%" class="text-center">Bien</th>
                <th width="14%" class="text-center">Muy Bien</th>
                <th width="14%" class="text-center">Excelente</th>
            </tr>
            <tr>
                <td width="30%">Habitación</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r01]" id="r1-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r01]" id="r1-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r01]" id="r1-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r01]" id="r1-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r01]" id="r1-4" value="5"></td>
            </tr>
            <tr>
                <td width="30%">Aseos/Duchas</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r02]" id="r2-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r02]" id="r2-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r02]" id="r2-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r02]" id="r2-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r02]" id="r2-4" value="5"></td>
            </tr>
            <tr>
                <td>Comedor</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r03]" id="r3-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r03]" id="r3-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r03]" id="r3-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r03]" id="r3-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r03]" id="r3-4" value="5"></td>
            </tr>
            <tr>
                <td width="30%">Zonas comunes</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r04]" id="r4-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r04]" id="r4-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r04]" id="r4-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r04]" id="r4-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r04]" id="r4-4" value="5"></td>
            </tr>
            <tr>
                <td width="30%">Lavandería</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r05]" id="r5-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r05]" id="r5-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r05]" id="r5-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r05]" id="r5-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r05]" id="r5-4" value="5"></td>
            </tr>
            <tr>
                <td width="30%">Instalaciones deportivas</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r06]" id="r6-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r06]" id="r6-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r06]" id="r6-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r06]" id="r6-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r06]" id="r6-4" value="5"></td>
            </tr>
            <tr>
                <td width="30%">Cantidad comida </td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r07]" id="r7-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r07]" id="r7-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r07]" id="r7-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r07]" id="r7-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r07]" id="r7-4" value="5"></td>
            </tr>
            <tr>
                <td width="30%">Calidad comida</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r08]" id="r8-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r08]" id="r8-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r08]" id="r8-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r08]" id="r8-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[r08]" id="r8-4" value="5"></td>
            </tr>
            </tbody>
        </table>
        <p>&nbsp;</p>
    @else
        <h4>Alojamiento</h4>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-striped">
            <tbody>
            <tr>
                <th width="30%">&nbsp;</th>
                <th width="14%" class="text-center">Mal</th>
                <th width="14%" class="text-center">Regular</th>
                <th width="14%" class="text-center">Bien</th>
                <th width="14%" class="text-center">Muy Bien</th>
                <th width="14%" class="text-center">Excelente</th>
            </tr>
            <tr>
                <td width="30%">Habitación</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f01]" id="f1-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f01]" id="f1-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f01]" id="f1-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f01]" id="f1-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f01]" id="f1-4" value="5"></td>
            </tr>
            <tr>
                <td width="30%">Aseos/Duchas</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f02]" id="f2-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f02]" id="f2-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f02]" id="f2-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f02]" id="f2-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f02]" id="f2-4" value="5"></td>
            </tr>
            <tr>
                <td>Comedor</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f03]" id="f3-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f03]" id="f3-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f03]" id="f3-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f03]" id="f3-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f03]" id="f3-4" value="5"></td>
            </tr>
            <tr>
                <td width="30%">Cocina</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f04]" id="f4-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f04]" id="f4-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f04]" id="f4-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f04]" id="f4-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f04]" id="f4-4" value="5"></td>
            </tr>
            <tr>
                <td width="30%">Cantidad comida </td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f05]" id="f5-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f05]" id="f5-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f05]" id="f5-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f05]" id="f5-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f05]" id="f5-4" value="5"></td>
            </tr>
            <tr>
                <td width="30%">Calidad comida</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f06]" id="f6-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f06]" id="f6-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f06]" id="f6-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f06]" id="f6-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f06]" id="f6-4" value="5"></td>
            </tr>
            <tr>
                <td width="30%">¿Te han lavado la ropa?</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f07]" id="f7-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f07]" id="f7-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f07]" id="f7-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f07]" id="f7-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f07]" id="f7-4" value="5"></td>
            </tr>
            <tr>
                <td>Comunicación</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f08]" id="f8-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f08]" id="f8-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f08]" id="f8-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f08]" id="f8-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f08]" id="f8-4" value="5"></td>
            </tr>
            <tr>
                <td width="30%">Hospitalidad</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f09]" id="f9-0" value="1"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f09]" id="f9-1" value="2"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f09]" id="f9-2" value="3"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f09]" id="f9-3" value="4"></td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f09]" id="f9-4" value="5"></td>
            </tr>
            </tbody>
        </table>
        <p>&nbsp;</p>

    @endif


    <h4>Escuela</h4>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th width="30%">&nbsp;</th>
            <th width="14%" class="text-center">Mal</th>
            <th width="14%" class="text-center">Regular</th>
            <th width="14%" class="text-center">Bien</th>
            <th width="14%" class="text-center">Muy Bien</th>
            <th width="14%" class="text-center">Excelente</th>
        </tr>
        <tr>
            <td width="30%">Aulas</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p01]" id="p1-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p01]" id="p1-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p01]" id="p1-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p01]" id="p1-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p01]" id="p1-4" value="5"></td>
        </tr>
        <tr>
            <td width="30%">Profesor/a</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p02]" id="p2-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p02]" id="p2-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p02]" id="p2-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p02]" id="p2-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p02]" id="p2-4" value="5"></td>
        </tr>
        <tr>
            <td>Material didáctico</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p03]" id="p3-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p03]" id="p3-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p03]" id="p3-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p03]" id="p3-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p03]" id="p3-4" value="5"></td>
        </tr>
        <tr>
            <td width="30%">Nivel asignado</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p04]" id="p4-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p04]" id="p4-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p04]" id="p4-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p04]" id="p4-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p04]" id="p4-4" value="5"></td>
        </tr>
        <tr>
            <td width="30%">¿Has mejorado tu nivel?</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p05]" id="p5-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p05]" id="p5-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p05]" id="p5-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p05]" id="p5-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p05]" id="p5-4" value="5"></td>
        </tr>
        </tbody>
    </table>
    <p>&nbsp;</p>

    <h4>Actividades</h4>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th width="30%">&nbsp;</th>
            <th width="14%" class="text-center">Mal</th>
            <th width="14%" class="text-center">Regular</th>
            <th width="14%" class="text-center">Bien</th>
            <th width="14%" class="text-center">Muy Bien</th>
            <th width="14%" class="text-center">Excelente</th>
        </tr>
        <tr>
            <td width="30%">Deportivas</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p06]" id="p6-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p06]" id="p6-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p06]" id="p6-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p06]" id="p6-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p06]" id="p6-4" value="5"></td>
        </tr>
        <tr>
            <td width="30%">Culturales</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p07]" id="p7-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p07]" id="p7-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p07]" id="p7-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p07]" id="p7-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p07]" id="p7-4" value="5"></td>
        </tr>
        <tr>
            <td>Ocio</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p08]" id="p8-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p08]" id="p8-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p08]" id="p8-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p08]" id="p8-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p08]" id="p8-4" value="5"></td>
        </tr>
        <tr>
            <td width="30%">Talleres</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p09]" id="p9-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p09]" id="p9-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p09]" id="p9-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p09]" id="p9-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p09]" id="p9-4" value="5"></td>
        </tr>
        <tr>
            <td width="30%">Excursiones</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p10]" id="p10-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p10]" id="p10-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p10]" id="p10-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p10]" id="p10-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p10]" id="p10-4" value="5"></td>
        </tr>
        </tbody>
    </table>
    <p>&nbsp;</p>

    <h4>Personal</h4>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th width="30%">&nbsp;</th>
            <th width="14%" class="text-center">Mal</th>
            <th width="14%" class="text-center">Regular</th>
            <th width="14%" class="text-center">Bien</th>
            <th width="14%" class="text-center">Muy Bien</th>
            <th width="14%" class="text-center">Excelente</th>
        </tr>
        <tr>
            <td width="30%">Monitor español</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p11]" id="p11-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p11]" id="p11-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p11]" id="p11-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p11]" id="p11-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p11]" id="p11-4" value="5"></td>
        </tr>
        <tr>
            <td width="30%">Personal del colegio</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p12]" id="p12-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p12]" id="p12-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p12]" id="p12-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p12]" id="p12-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p12]" id="p12-4" value="5"></td>
        </tr>
        <tr>
            <td>Personal del lugar de inscripción en España</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p13]" id="p13-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p13]" id="p13-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p13]" id="p13-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p13]" id="p13-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p13]" id="p13-4" value="5"></td>
        </tr>
        </tbody>
    </table>
    <p>&nbsp;</p>

    <h4>Aspectos Generales</h4>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th width="30%">&nbsp;</th>
            <th width="14%" class="text-center">Mal</th>
            <th width="14%" class="text-center">Regular</th>
            <th width="14%" class="text-center">Bien</th>
            <th width="14%" class="text-center">Muy Bien</th>
            <th width="14%" class="text-center">Excelente</th>
        </tr>
        <tr>
            <td width="30%">Valoración general del curso</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p14]" id="p14-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p14]" id="p14-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p14]" id="p14-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p14]" id="p14-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p14]" id="p14-4" value="5"></td>
        </tr>
        <tr>
            <td>Viaje al país de destino</td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p15]" id="p15-0" value="1"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p15]" id="p15-1" value="2"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p15]" id="p15-2" value="3"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p15]" id="p15-3" value="4"></td>
            <td width="14%" class="text-center"><input type="radio" name="respuesta[p15]" id="p15-4" value="5"></td>
        </tr>
        </tbody>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th width="30%">&nbsp;</th>
            <th width="11.66%" class="text-center">No aplicable</th>
            <th width="11.66%" class="text-center">Mal</th>
            <th width="11.66%" class="text-center">Regular</th>
            <th width="11.66%" class="text-center">Bien</th>
            <th width="11.66%" class="text-center">Muy Bien</th>
            <th width="11.66%" class="text-center">Excelente</th>
        </tr>
        <tr>
            <td width="30%">Reunión informativa previa</td>
            <td width="11.66%" class="text-center"><input type="radio" name="respuesta[p16]" id="p16-5" value="0"></td>
            <td width="11.66%" class="text-center"><input type="radio" name="respuesta[p16]" id="p16-0" value="1"></td>
            <td width="11.66%" class="text-center"><input type="radio" name="respuesta[p16]" id="p16-1" value="2"></td>
            <td width="11.66%" class="text-center"><input type="radio" name="respuesta[p16]" id="p16-2" value="3"></td>
            <td width="11.66%" class="text-center"><input type="radio" name="respuesta[p16]" id="p16-3" value="4"></td>
            <td width="11.66%" class="text-center"><input type="radio" name="respuesta[p16]" id="p16-4" value="5"></td>
        </tr>
        </tbody>
    </table>
    <p>&nbsp;</p>


    @if($booking->convocatoria->alojamiento->tipo->es_familia)
        <h4>Familia</h4>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-striped">
            <tbody>
            <tr>
                <th width="30%">&nbsp;</th>
                <th width="14%" class="text-center">Sí</th>
                <th width="56%" align="left" style="padding-left: 75px;">No</th>
            </tr>
            <tr>
                <td width="30%">¿Recomendarías a la familia?</td>
                <td width="14%" class="text-center"><input type="radio" name="respuesta[f10]" id="f10-0" value="1"></td>
                <td width="56%" align="left" style="padding-left: 75px;"><input type="radio" name="respuesta[f10]" id="f10-0" value="0"></td>
            </tr>
            </tbody>
        </table>

        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <h2>Transporte</h2>
        <p>&nbsp;</p>
        <h4>Transporte utilizado casa-escuela</h4>

        <p>
            <label class="radio-inline" for="f11-0">
                <input type="radio" name="respuesta[f11]" id="f11-0" value="caminando" class="f11">
                Caminando
            </label>
        </p>
        <p>
            <label class="radio-inline" for="f11-1">
                <input type="radio" name="respuesta[f11]" id="f11-1" value="bus" class="f11">
                Bus
            </label>
        </p>
        <p>
            <label class="radio-inline" for="f11-2">
                <input type="radio" name="respuesta[f11]" id="f11-2" value="metro" class="f11">
                Metro
            </label>
        </p>
        <p>
            <label class="radio-inline" for="f11-3">
                <input type="radio" name="respuesta[f11]" id="f11-3" value="tren" class="f11">
                Tren
            </label>
        </p>
        <p>
            <label class="radio-inline" for="f11-4">
                <input type="radio" name="respuesta[f11]" id="f11-4" value="coche" class="f11">
                Coche
            </label>
        </p>
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-1">
                    <label class="radio-inline" for="f11-5">
                        <input type="radio" name="respuesta[f11]" id="f11-5" value="otro" class="f11">
                        Otro:
                    </label>
                </div>
                <div class="col-sm-6">
                    <input type="text" name="respuesta[f11o]" id="f11o" value="" class="form-control">
                </div>
            </div>
        </div>
        <p>&nbsp;</p>


        <h4>Tiempo en trasladarte casa-escuela en minutos</h4>

        <p>
            <label class="radio-inline" for="f12-0">
                <input type="radio" name="respuesta[f12]" id="f12-0" value="menos10min">
                Menos de 10 minutos
            </label>
        </p>
        <p>
            <label class="radio-inline" for="f12-1">
                <input type="radio" name="respuesta[f12]" id="f12-1" value="entre10y20">
                Entre 10 y 20 minutos
            </label>
        </p>
        <p>
            <label class="radio-inline" for="f12-2">
                <input type="radio" name="respuesta[f12]" id="f12-2" value="entre20y40">
                Entre 20 y 40 minutos
            </label>
        </p>
        <p>
            <label class="radio-inline" for="f12-3">
                <input type="radio" name="respuesta[f12]" id="f12-3" value="entre40y60">
                Entre 40 y 60 minutos
            </label>
        </p>
        <p>
            <label class="radio-inline" for="f12-4">
                <input type="radio" name="respuesta[f12]" id="f12-4" value="mas1h">
                Más de 1 hora
            </label>
        </p>
        <p>&nbsp;</p>


        <h4>¿Cuántos transportes utilizabas casa-escuela?</h4>
        <div class="row">
            <div class="col-sm-2">
                <select name="respuesta[f13]" id="f13" class="form-control">
                    <option value=""></option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
        </div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    @endif


    <h2>Otros</h2>
    <p>&nbsp;</p>
    <h4>Resume en unas cuantas palabras como ha sido tu experiencia</h4>
    <div class="row">
        <div class="col-sm-6">
            <textarea rows="8" name="respuesta[p17]" id="p17" class="form-control"></textarea>
        </div>
    </div>
    <p>&nbsp;</p>

    <h4>¿Qué crees que tenemos que mejorar en nuestro programa?</h4>
    <div class="row">
        <div class="col-sm-6">
            <textarea rows="8" name="respuesta[p18]" id="p18" class="form-control"></textarea>
        </div>
    </div>
    <p>&nbsp;</p>

    <h4>Actividades y excursiones que añadirías al programa</h4>
    <div class="row">
        <div class="col-sm-6">
            <textarea rows="8" name="respuesta[p19]" id="p19" class="form-control"></textarea>
        </div>
    </div>
    <p>&nbsp;</p>

    <h4>¿Qué nuevo programa te gustaría ver en nuestro catálogo?</h4>
    <div class="row">
        <div class="col-sm-6">
            <textarea rows="8" name="respuesta[p20]" id="p20" class="form-control"></textarea>
        </div>
    </div>
    <p>&nbsp;</p>

    <h4>¿Por qué te has apuntado a éste programa?</h4>
    <p>
        <label class="radio-inline" for="p21-0">
            <input type="radio" name="respuesta[p21]" id="p21-0" value="decididospadres">
            Lo han decidido mis padres
        </label>
    </p>
    <p>
        <label class="radio-inline" for="p21-1">
            <input type="radio" name="respuesta[p21]" id="p21-1" value="decididoyo">
            Lo he dicidido yo
        </label>
    </p>
    <p>&nbsp;</p>


    <h4>Comentarios</h4>
    <div class="row">
        <div class="col-sm-6">
            <textarea rows="8" name="respuesta[p22]" id="p22-0" class="form-control"></textarea>
        </div>
    </div>
    <p>&nbsp;</p>



    <hr>
    <div class="form-group row">
        <div class="col-md-10">
            <button id="enviardatos" class="btn btn-block btn-success">Enviar</button>
            {!! Form::hidden('datos[booking]', $booking->id, array('id' => 'datos[booking]', 'class' => 'booking')) !!}
        </div>
    </div>

    {!! Form::close() !!}


    <div class="modal fade" tabindex="-1" role="dialog" id="error">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger">Error</h4>
                </div>
                <div class="modal-body">
                    <p class="errortext"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('extra_footer')
    <script type="text/javascript">
    $('#f11o').prop('disabled',true);

    $('#f11-5').on('change',function(){
        if($('#f11-5').is(':checked')){
            $('#f11o').prop('disabled',false);
        }else{
            $('#f11o').prop('disabled',true);
        }
    });




    $( "#enviardatos" ).click(function() {
        cuantos = 0;
        $('input[type="radio"]').each(function( index ) {
            if ($(this).prop("checked") != true){
                cuantos++;
            }
        });

        @if(!$booking->convocatoria->alojamiento->tipo->es_familia)
            var seleccionados = 98;
        @else
            var seleccionados = 112;
        @endif



        if(($('#f11-5').is(':checked') && $('#f11o').val() != '') || !$('#f11-5').is(':checked')){
            var enviar = 1;
        }else{
            var enviar = 0;
        }

        if (cuantos > seleccionados || $('#f13').val() == '' || $('#p17').val() == '' || $('#p18').val() == '' || $('#19').val() == '' || $('#p20').val() == '' || enviar == 0){
            $('.errortext').html('Tienes que responder todas las preguntas.');
            $('#error').modal();
            return false;
        }else{
            $('#opinionjovenes').submit();
        }

    });

    </script>
@stop