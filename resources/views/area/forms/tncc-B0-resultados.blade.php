<div class="form-group row">
    <div class="col-md-10">
        <fieldset class="cictest">
            <legend>Choose the correct answer. Only one answer is correct.</legend>
            <p><strong>1. ____________ are you from? </strong></p>
            <p>
                    @if($respuesta->p01 == 'a') What @endif
                
                    @if($respuesta->p01 == 'b') Where @endif
                
                    @if($respuesta->p01 == 'c') How old @endif
                
                    @if($respuesta->p01 == 'd')
                    Were @endif
            </p>

            <br />

            <p><strong>2. Hello, Peter, how are you?</strong></p>
            <p>
                    @if($respuesta->p02 == 'a')
                    I'm 11 years old. @endif
                
                    @if($respuesta->p02 == 'b')
                    I'm John's cousin. @endif
                
                    @if($respuesta->p02 == 'c')
                    I'm big. @endif
                
                    @if($respuesta->p02 == 'd')
                    I'm not very well.  @endif
                </p>

            <br />

            <p><strong>3. Is your dad at home?</strong></p>
            <p>
                    @if($respuesta->p03 == 'a')
                    It's his new home. @endif
                
                    @if($respuesta->p03 == 'b')
                    Only my mum. @endif
                
                    @if($respuesta->p03 == 'c')
                    Yes, she is. @endif
                
                    @if($respuesta->p03 == 'd')
                    At the bus station. @endif
                </p>

            <br />

            <p><strong>4. My neighbour's name is Jane. I see __________  every morning.</strong></p>
            <p>
                    @if($respuesta->p04 == 'a')
                    him @endif
                
                    @if($respuesta->p04 == 'b')
                    she @endif
                
                    @if($respuesta->p04 == 'c')
                    her @endif
                
                    @if($respuesta->p04 == 'd')
                    he @endif
                </p>

            <br />

            <p><strong>5. What's the matter? Have you got a stomach ache?</strong></p>
            <p>
                    @if($respuesta->p05 == 'a')
                    No, thank you. @endif
                
                    @if($respuesta->p05 == 'b')
                    No, I have. @endif
                
                    @if($respuesta->p05 == 'c')
                    No, I haven't got it. @endif
                
                    @if($respuesta->p05 == 'd')
                    No, I've got toothache. @endif
                </p>

            <br />

            <p><strong>6. Yesterday, Mark  _______________ a pencil case for school.</strong></p>
            <p>
                    @if($respuesta->p06 == 'a')
                    buy @endif
                
                    @if($respuesta->p06 == 'b')
                    bought @endif
                
                    @if($respuesta->p06 == 'c')
                    buys @endif
                
                    @if($respuesta->p06 == 'd')
                    buyed @endif
                </p>

            <br />

            <p><strong>7. My brother ______________ football at the moment?</strong></p>
            <p>
                    @if($respuesta->p07 == 'a')
                    is playing @endif
                
                    @if($respuesta->p07 == 'b')
                    plays @endif
                
                    @if($respuesta->p07 == 'c')
                    playing @endif
                
                    @if($respuesta->p07 == 'd')
                    are playing @endif
                </p>

            <br />

            <p><strong>8. Is there any pasta?</strong></p>
            <p>
                    @if($respuesta->p08 == 'a')
                    Yes, there's some here. @endif
                
                    @if($respuesta->p08 == 'b')
                    Not many. @endif
                
                    @if($respuesta->p08 == 'c')
                    No, there aren't any. @endif
                
                    @if($respuesta->p08 == 'd')
                    Yes, there is any @endif
                </p>

            <br />

            <p><strong>9. He _______________ two brothers and one sister.</strong></p>
            <p>
                    @if($respuesta->p09 == 'a')
                    haves got @endif
                
                    @if($respuesta->p09 == 'b')
                    has got @endif
                
                    @if($respuesta->p09 == 'c')
                    have got @endif
                
                    @if($respuesta->p09 == 'd')
                    have @endif
                </p>

            <br />

            <p><strong>10. ____________ you open the window, please?</strong></p>
            <p>
                    @if($respuesta->p10 == 'a')
                    Are @endif
                
                    @if($respuesta->p10 == 'b')
                    Do @endif
                
                    @if($respuesta->p10 == 'c')
                    Could @endif
                
                    @if($respuesta->p10 == 'd')
                    Have @endif
                </p>

            <br />

            <p><strong>11. Do you want a drink of water?</strong></p>
            <p>
                    @if($respuesta->p11 == 'a')
                    Yes, do @endif
                
                    @if($respuesta->p11 == 'b')
                    Yes, it is. @endif
                
                    @if($respuesta->p11 == 'c')
                    Yes, I had @endif
                
                    @if($respuesta->p11 == 'd')
                    Yes, please. @endif
                </p>

            <br />

            <p><strong>12. ___________ are five children in the playground.</strong></p>
            <p>
                    @if($respuesta->p12 == 'a')
                    There @endif
                
                    @if($respuesta->p12 == 'b')
                    These @endif
                
                    @if($respuesta->p12 == 'c')
                    Their @endif
                
                    @if($respuesta->p12 == 'd')
                    They @endif
                </p>

            <br />

            <p><strong>13. ____________ have dogs in their homes.</strong></p>
            <p>
                    @if($respuesta->p13 == 'a')
                    Any @endif
                
                    @if($respuesta->p13 == 'b')
                    Every @endif
                
                    @if($respuesta->p13 == 'c')
                    A @endif
                
                    @if($respuesta->p13 == 'd')
                    Some @endif
                </p>

            <br />

            <p><strong>14. My sister is older ____________ you.</strong></p>
            <p>
                    @if($respuesta->p14 == 'a')
                    is @endif
                
                    @if($respuesta->p14 == 'b')
                    that @endif
                
                    @if($respuesta->p14 == 'c')
                    to @endif
                
                    @if($respuesta->p14 == 'd')
                    than @endif
                </p>

            <br />

            <p><strong>15. Would you like to come to my house?</strong></p>
            <p>
                    @if($respuesta->p15 == 'a')
                    Yes, I do @endif
                
                    @if($respuesta->p15 == 'b')
                    I like my house. @endif
                
                    @if($respuesta->p15 == 'c')
                    No, I want to go home. @endif
                
                    @if($respuesta->p15 == 'd')
                    Yes, good bye. @endif
                </p>

            <br />

            <p><strong>16. How often do you go horse riding?</strong></p>
            <p>
                    @if($respuesta->p16 == 'a')
                    Yes, I do. @endif
                
                    @if($respuesta->p16 == 'b')
                    I went on Saturday. @endif
                
                    @if($respuesta->p16 == 'c')
                    I'm riding a horse. @endif
                
                    @if($respuesta->p16 == 'd')
                    Every weekend. @endif
                </p>

            <br />

            <p><strong>17. He ____________ have a car.</strong></p>
            <p>
                    @if($respuesta->p17 == 'a')
                    doesn't @endif
                
                    @if($respuesta->p17 == 'b')
                    don't @endif
                
                    @if($respuesta->p17 == 'c')
                    hasn't @endif
                
                    @if($respuesta->p17 == 'd')
                    not @endif
                </p>

            <br />

            <p><strong>18. Last Monday, Mum's taxi ____________________ at six o'clock.</strong></p>
            <p>
                    @if($respuesta->p18 == 'a')
                    don't arrived @endif
                
                    @if($respuesta->p18 == 'b')
                    arrives @endif
                
                    @if($respuesta->p18 == 'c')
                    didn't arrived @endif
                
                    @if($respuesta->p18 == 'd')
                    didn't arrive @endif
                </p>

            <br />

            <p><strong>19. They usually go to work ___________ train.</strong></p>
            <p>
                    @if($respuesta->p19 == 'a')
                    by @endif
                
                    @if($respuesta->p19 == 'b')
                    in @endif
                
                    @if($respuesta->p19 == 'c')
                    on @endif
                
                    @if($respuesta->p19 == 'd')
                    with @endif
                </p>

            <br />

            <p><strong>20. They ___________ eat chocolate at bed time.</strong></p>
            <p>
                    @if($respuesta->p20 == 'a')
                    Tuesday @endif
                
                    @if($respuesta->p20 == 'b')
                    any day @endif
                
                    @if($respuesta->p20 == 'c')
                    some day @endif
                
                    @if($respuesta->p20 == 'd')
                    never @endif
                </p>

            <br />

            <p><strong>21. That is _____________ restaurant in town.</strong></p>
            <p>
                    @if($respuesta->p21 == 'a')
                    better @endif
                
                    @if($respuesta->p21 == 'b')
                    the best @endif
                
                    @if($respuesta->p21 == 'c')
                    the most good @endif
                
                    @if($respuesta->p21 == 'd')
                    goodest @endif
                </p>

            <br />

            <p><strong>22. I like ________________ in the park.</strong></p>
            <p>
                    @if($respuesta->p22 == 'a')
                    walking @endif
                
                    @if($respuesta->p22 == 'b')
                    walk @endif
                
                    @if($respuesta->p22 == 'c')
                    to walking @endif
                
                    @if($respuesta->p22 == 'd')
                    not walk @endif
                </p>

            <br />

            <p><strong>23. What did you do last weekend?</strong></p>
            <p>
                    @if($respuesta->p23 == 'a')
                    Yes, I did. @endif
                
                    @if($respuesta->p23 == 'b')
                    I had a party. @endif
                
                    @if($respuesta->p23 == 'c')
                    I went to a friend. @endif
                
                    @if($respuesta->p23 == 'd')
                    I go to the cinema. @endif
                </p>

            <br />

            <p><strong>24. Bananas grow _________ hot countries.</strong></p>
            <p>
                    @if($respuesta->p24 == 'a')
                    in @endif
                
                    @if($respuesta->p24 == 'b')
                    under @endif
                
                    @if($respuesta->p24 == 'c')
                    at @endif
                
                    @if($respuesta->p24 == 'd')
                    between @endif
                </p>

            <br />

            <p><strong>25. Are you ______________ to come to my house this weekend?</strong></p>
            <p>
                    @if($respuesta->p25 == 'a')
                    go @endif
                
                    @if($respuesta->p25 == 'b')
                    went @endif
                
                    @if($respuesta->p25 == 'c')
                    going @endif
                
                    @if($respuesta->p25 == 'd')
                    to go @endif
                </p>
        </fieldset>


        <fieldset class="addmargintop60">
            <legend>Write a paragraph / some sentences about yourself.</legend>
            {!! nl2br($respuesta->ptxt) !!}
        </fieldset>
        <hr>

    </div>
</div>