@extends('layouts.area')

@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop

@section('container')
    <h2 class="text-capitalize text-success">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</h2>
    <hr>

    @if((!isset($_POST['datos']['testnumber'])))
        {{isset($_POST['datos']['testnumber'])}}

    {!! Form::open(array('url' => route('area.forms.post',[$form->id, $booking->id]), 'id'=>'testnivel')) !!}

    <div class="form-group row">
        <div class="col-md-12">
            <h4 class="form-section">{{trans('area.datosacademicos')}}</h4>
        </div>
    </div>
    <div class="form-group row">

        <div class="col-md-5">
            {!! Form::label('datos[escuela_curso]', trans('area.cursoactual')) !!}
            {!! Form::select('datos[escuela_curso]',ConfigHelper::getCICAcademicYearTestColonias(),
            '',
            array(
                    'id'=> 'escuela_curso',
                    'name'=> 'datos[escuela_curso]',
                    'class' => "selectpicker form-control escuela_curso",
                    'title' => "...",
                    'required'
                )
            )
            !!}
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-5">
            <div id="englishlevel-div">
                {!! Form::label('datos[englishlevel]', 'English Level') !!}
                {!! Form::select('datos[englishlevel]',
                array(
                    0 => '',
                    1 => 'Low',
                    2 => 'High'
                ),
                '',
                array(
                        'id'=> 'englishlevel',
                        'name'=> 'datos[englishlevel]',
                        'class' => "selectpicker form-control englishlevel",
                        'title' => "...",
                        'required'
                    )
                )
                !!}
            </div>
        </div>

    </div>

    <hr>
    <div class="form-group row">
        <div class="col-md-10">
            <button id="enviardatos" class="btn btn-block btn-success">Siguiente</button>
            {!! Form::hidden('datos[testnumber]', '0', array('id' => 'datos[testnumber]', 'class' => 'testnumber')) !!}
            {!! Form::hidden('datos[booking]', $booking->id, array('id' => 'datos[booking]', 'class' => 'booking')) !!}
        </div>
    </div>

    {!! Form::close() !!}

    @elseif( isset($_POST['datos']) && $_POST['datos']['testnumber'] == 'B0' )

        @include('area.forms.tncc-B0')

    @elseif( isset($_POST['datos']) && $_POST['datos']['testnumber'] == 'B1')

        @include('area.forms.tncc-B1')

    @elseif( isset($_POST['datos']) && $_POST['datos']['testnumber'] == 'B2')

        @include('area.forms.tncc-B2')

    @elseif( isset($_POST['datos']) && $_POST['datos']['testnumber'] == 'B3')

        @include('area.forms.tncc-B3')

    @endif


    <div class="modal fade" tabindex="-1" role="dialog" id="error">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger">Error</h4>
                </div>
                <div class="modal-body">
                    <p class="errortext"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('extra_footer')
    <script type="text/javascript">
    /*
        $('.form-control').prop( "disabled", true );
        $('.form-radio input').prop( "disabled", true );
        $('input[type="checkbox"]').prop( "disabled", true );
    */
    $("#englishlevel-div").hide();


    $(".escuela_curso").change(function() {
        curso = $(".escuela_curso").find('option:selected').val();


        $('#englishlevel').selectpicker('val','0');

        if(curso != 4 && curso != 6 && curso != 8 && curso != 10){
            $("#englishlevel-div").fadeIn();
        }else{
            $("#englishlevel-div").fadeOut()
        }

        quetest();
    });

    $("#englishlevel").change(function() {
        quetest();
    });

    function quetest(){
        $('.testnumber').val('0');
        curso = $(".escuela_curso").find('option:selected').val();
        englishlevel = $(".englishlevel").find('option:selected').val();

        if (curso == 4 || (curso == 5 && englishlevel == 1)) {
            $('.testnumber').val('B0');
        }
        else if ((curso == 5 && englishlevel == 2) || curso == 6 || (curso == 7 && englishlevel == 1)) {
            $('.testnumber').val('B1');
        }
        else if ((curso == 7 && englishlevel == 2) || curso == 8 || (curso == 9 && englishlevel == 1)) {
            $('input.testnumber').val('B2');
        }
        else if ((curso == 9 && englishlevel == 2) || curso == 10 ) {
            $('.testnumber').val('B3');
        }


        console.log('testnumber: ' + $('input.testnumber').val());
    }

    $( "#enviardatos" ).click(function() {
        var error = false;

        if ($(".testnumber").val() != 0){
            $('#testnivel').submit();
        }else{
            $('.errortext').html('{{trans('forms.responder')}}');
            $('#error').modal();
            return false;
        }
    });

    $( "#enviarformCIC" ).click(function() {
        cuantos = 0;
        $('input[type="radio"]').each(function( index ) {
            if ($(this).prop("checked") != true){
                cuantos++;
            }
        });

        if (cuantos > 75 || $('.ptxt').val() == ''){
            $('.errortext').html('{{trans('forms.responder')}}');
            $('#error').modal();
            return false;
        }else{
            $('#cictest').submit();
        }
    });




    </script>
@stop