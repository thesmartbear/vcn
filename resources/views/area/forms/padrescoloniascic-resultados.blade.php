@extends('layouts.area')

@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop

@section('container')
    <div class="row">
        <div class="col-md-8">
            <h2 class="text-capitalize text-success">
                {!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}
            </h2>
        </div>
        <div class="col-md-4">
            <a href="/area" class="btn btn-sm btn-danger text-uppercase pull-right back"><i class="fa fa-chevron-left"></i> volver</a>

            @if($estado == 1)
                <form>
                <a href="{{route('area.forms',[$form->id,$booking_id])}}" class="btn btn-sm btn-primary text-uppercase pull-right ">reenvíar</a>
                </form>
            @endif
        </div>

    </div>
    <hr>

    <div class="row">
        <div class="col-md-12 respuestas">
            @if( isset($respuesta))
                <h4>{{trans('forms.padrescoloniascic.p01')}}</h4><p>{{trans('forms.'.$respuesta->r01)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r01t) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.p02')}}</h4><p>{{trans('forms.'.$respuesta->r02)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r02t) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.p03')}}</h4><p>{{trans('forms.'.$respuesta->r03)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r03t) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.p04')}}</h4><p>{{trans('forms.'.$respuesta->r04)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r04t) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.p05')}}</h4><p>{{trans('forms.'.$respuesta->r05)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r05t) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.p06')}}</h4><p>{{trans('forms.'.$respuesta->r06)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r06t) !!}</p>

                {{-- <h4>{{trans('forms.padrescoloniascic.p08')}}</h4><p>{{trans('forms.'.$respuesta->r08)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r08t) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.p09')}}</h4><p>{{trans('forms.'.$respuesta->r09)}}</p>
                <h4>{{trans('forms.comentarios')}}</h4><p>{!! nl2br($respuesta->r09t) !!}</p> --}}

                <h4>{{trans('forms.padrescoloniascic.p07')}}</h4><p>{!! nl2br($respuesta->r07) !!}</p>
                <h4>{{trans('forms.padrescoloniascic.web')}}</h4><p>{{$respuesta->web}}</p>
                <h4>{{trans('forms.padrescoloniascic.catalogo')}}</h4><p>{{$respuesta->catalogo}}</p>
                <h4>{{trans('forms.padrescoloniascic.blog')}}</h4><p>{{$respuesta->blog}}</p>
                {{-- <h4>{{trans('forms.padrescoloniascic.autocares')}}</h4><p>{{$respuesta->autocares}}</p> --}}
                <h4>{{trans('forms.padrescoloniascic.lavado')}}</h4><p>{{$respuesta->lavado}}</p>
                {{-- @if($respuesta->trinity != '')
                    <h4>{{trans('forms.padrescoloniascic.trinity')}}</h4><p>{{$respuesta->trinity}}</p>
                @endif --}}
                @if($respuesta->trato != '')
                    <h4>{{trans('forms.padrescoloniascic.trato')}}</h4><p>{{$respuesta->trato}}</p>
                @endif
                <h4>{{trans('forms.padrescoloniascic.informe')}}</h4><p>{{$respuesta->informe}}</p>
            @endif
        </div>
    </div>

@stop