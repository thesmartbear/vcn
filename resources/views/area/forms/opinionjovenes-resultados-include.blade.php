

    <div class="row">
        <div class="col-md-12 respuestas opinionjovenes">
            <h2>Valoración</h2>
            <p>&nbsp;</p>
            @if( isset($respuesta) && isset($datos))
                @if(isset($respuesta->r01))
                    <h4 class="text-primary">Alojamiento en residencia</h4>
                    <p><b>Habitación:</b> {{trans('forms.opinionjovenes.'.$respuesta->r01)}}</p>
                    <p><b>Aseos/Duchas: </b> {{trans('forms.opinionjovenes.'.$respuesta->r02)}}</p>
                    <p><b>Comedor: </b></b> {{trans('forms.opinionjovenes.'.$respuesta->r03)}}</p>
                    <p><b>Zonas comunes: </b> {{trans('forms.opinionjovenes.'.$respuesta->r04)}}</p>
                    <p><b>Lavandería: </b> {{trans('forms.opinionjovenes.'.$respuesta->r05)}}</p>
                    <p><b>Instalaciones deportivas: </b> {{trans('forms.opinionjovenes.'.$respuesta->r06)}}</p>
                    <p><b>Cantidad comida: </b> {{trans('forms.opinionjovenes.'.$respuesta->r07)}}</p>
                    <p><b>Calidad comida: </b> {{trans('forms.opinionjovenes.'.$respuesta->r08)}}</p>
                @else
                    <h4 class="text-primary">Alojamiento en familia</h4>
                    <p><b>Habitación:</b> {{trans('forms.opinionjovenes.'.$respuesta->f01)}}</p>
                    <p><b>Aseos/Duchas:</b> {{trans('forms.opinionjovenes.'.$respuesta->f02)}}</p>
                    <p><b>Comedor:</b> {{trans('forms.opinionjovenes.'.$respuesta->f03)}}</p>
                    <p><b>Cocina:</b> {{trans('forms.opinionjovenes.'.$respuesta->f04)}}</p>
                    <p><b>Cantidad comida:</b> {{trans('forms.opinionjovenes.'.$respuesta->f05)}}</p>
                    <p><b>Calidad comida:</b> {{trans('forms.opinionjovenes.'.$respuesta->f06)}}</p>
                    <p><b>¿Te han lavado la ropa?:</b> {{trans('forms.opinionjovenes.'.$respuesta->f07)}}</p>
                    <p><b>Comunicación:</b> {{trans('forms.opinionjovenes.'.$respuesta->f08)}}</p>
                    <p><b>Hospitalidad:</b> {{trans('forms.opinionjovenes.'.$respuesta->f09)}}</p>


                @endif
                <p>&nbsp;</p>


                    <h4 class="text-primary">Escuela</h4>
                    <p><b>Aulas:</b> {{trans('forms.opinionjovenes.'.$respuesta->p01)}}</p>
                    <p><b>Profesor/a:</b> {{trans('forms.opinionjovenes.'.$respuesta->p02)}}</p>
                    <p><b>Material didáctico:</b> {{trans('forms.opinionjovenes.'.$respuesta->p03)}}</p>
                    <p><b>Nivel asignado:</b> {{trans('forms.opinionjovenes.'.$respuesta->p04)}}</p>
                    <p><b>¿Has mejorado tu nivel?:</b> {{trans('forms.opinionjovenes.'.$respuesta->p05)}}</p>
                    <p>&nbsp;</p>

                    <h4 class="text-primary">Actividades</h4>
                    <p><b>Deportivas:</b> {{trans('forms.opinionjovenes.'.$respuesta->p06)}}</p>
                    <p><b>Culturales:</b> {{trans('forms.opinionjovenes.'.$respuesta->p07)}}</p>
                    <p><b>Ocio:</b> {{trans('forms.opinionjovenes.'.$respuesta->p08)}}</p>
                    <p><b>Talleres:</b> {{trans('forms.opinionjovenes.'.$respuesta->p09)}}</p>
                    <p><b>Excursiones:</b> {{trans('forms.opinionjovenes.'.$respuesta->p10)}}</p>
                    <p>&nbsp;</p>

                    <h4 class="text-primary">Personal</h4>
                    <p><b>Monitor español:</b> {{trans('forms.opinionjovenes.'.$respuesta->p11)}}</p>
                    <p><b>Personal del colegio:</b> {{trans('forms.opinionjovenes.'.$respuesta->p12)}}</p>
                    <p><b>Personal del lugar de inscripción en España:</b> {{trans('forms.opinionjovenes.'.$respuesta->p13)}}</p>
                    <p>&nbsp;</p>

                    <h4 class="text-primary">Aspectos Generales</h4>
                    <p><b>Valoración general del curso:</b> {{trans('forms.opinionjovenes.'.$respuesta->p14)}}</p>
                    <p><b>Viaje al país de destino:</b> {{trans('forms.opinionjovenes.'.$respuesta->p15)}}</p>
                    <p><b>Reunión informativa previa:</b> {{trans('forms.opinionjovenes.'.$respuesta->p16)}}</p>
                    <p>&nbsp;</p>


                @if(isset($respuesta->f10))
                        <h4 class="text-primary">Familia</h4>
                        <p><b>¿Recomendarías a la familia?</b> @if($respuesta->f10 == 1) Sí @else No @endif</p>

                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>

                        <h2>Transporte</h2>
                        <p>&nbsp;</p>
                        <h4>Transporte utilizado casa-escuela</h4>
                        <p>{{trans('forms.opinionjovenes.'.$respuesta->f11)}}@if(isset($respuesta->f11o)): {{$respuesta->f11o}}@endif</p>

                        <h4>Tiempo en trasladarte casa-escuela en minutos</h4>
                        <p>{{trans('forms.opinionjovenes.'.$respuesta->f12)}}</p>

                        <h4>¿Cuántos transportes utilizabas casa-escuela?</h4>
                        <p>{{$respuesta->f13}}</p>
                @endif


                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>


                    <h2>Otros</h2>
                    <p>&nbsp;</p>
                    <h4>Resume en unas cuantas palabras como ha sido tu experiencia</h4>
                    <p>{!! nl2br($respuesta->p17) !!}</p>

                    <h4>¿Qué crees que tenemos que mejorar en nuestro programa?</h4>
                    <p>{!! nl2br($respuesta->p18) !!}</p>

                    <h4>Actividades y excursiones que añadirías al programa</h4>
                    <p>{!! nl2br($respuesta->p19) !!}</p>

                    <h4>¿Qué nuevo programa te gustaría ver en nuestro catálogo?</h4>
                    <p>{!! nl2br($respuesta->p20) !!}</p>

                    <h4>¿Por qué te has apuntado a éste programa?</h4>
                    <p>{!! nl2br($respuesta->p21) !!}</p>
                    <p>&nbsp;</p>


                @if(isset($respuesta->p22) && $respuesta->p22 != '')
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>

                    <h3 class="text-primary">Comentarios</h3>
                    <p>{{nl2br($respuesta->p22)}}</p>
                    <p>&nbsp;</p>
                @endif





            @endif
        </div>
    </div>

