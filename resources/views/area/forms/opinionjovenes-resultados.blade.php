@extends('layouts.area')

@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop

@section('container')
    <div class="row">
        <div class="col-md-8">
            <h2 class="text-capitalize text-success">
                {!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}
            </h2>
        </div>
        <div class="col-md-4">
            <a href="/area" class="btn btn-sm btn-danger text-uppercase pull-right back"><i class="fa fa-chevron-left"></i> volver</a>
        </div>

    </div>
    <hr>

    @include('area.forms.opinionjovenes-resultados-include', ['respuesta'=> $respuesta, 'datos' => $datos])

@stop