<div id="test01">
    {!! Form::open(array('url' => route('area.forms.post',[$form->id, $booking->id]), 'id'=>'cictest')) !!}

    <div class="form-group row">
        <div class="col-md-10">
            <fieldset class="cictest">
                <legend>Choose the correct answer. Only one answer is correct.</legend>
                <p><strong>1.	100 competitors had ______ the race.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="a" />
                        put their names for
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="b" />
                        entered for
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="c" />
                        put themselves for
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="d" />
                        taken part
                    </div>
                </div>

                <br />

                <p><strong>2. I _________ since breakfast and I'm very tired</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="a" />
                        travel
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="b" />
                        am travelling<br>
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="c" />
                        was travelling
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="d" />
                        have been travelling
                    </div>
                </div>

                <br />

                <p><strong>3.	He __________ in his homework. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="a" />
                        did a lot of faults
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="b" />
                        made a lot of mistakes<br>
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="c" />
                        did a lot of mistakes
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="d" />
                        made a lot of faults
                    </div>
                </div>

                <br />

                <p><strong>4.	I would like ___________ it again.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="a" />
                        that you read
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="b" />
                        you reading
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="c" />
                        you to read
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="d" />
                        you read
                    </div>
                </div>

                <br />

                <p><strong>5.	I wish I ________ suggest something more suitable, but this is all we have. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="a" />
                        should
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="b" />
                        can
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="c" />
                        would
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="d" />
                        could
                    </div>
                </div>

                <br />

                <p><strong>6.	ÒWhere are you from?Ó They asked the boy _______________</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="a" />
                        he was from where
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="b" />
                        where he was from
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="c" />
                        where was he from
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="d" />
                        where are you from
                    </div>
                </div>

                <br />

                <p><strong>7. My teachers always give me too much homework. But you ________ the same problems with yours, too.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="a" />
                        must have
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="b" />
                        ought to have<br>
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="c" />
                        have to have
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="d" />
                        can have
                    </div>
                </div>

                <br />

                <p><strong>8. If I ____________about it earlier, I would have told you.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="a" />
                        had known
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="b" />
                        would have known
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="c" />
                        would know
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="d" />
                        knew
                    </div>
                </div>

                <br />

                <p><strong>9. Don't worry. If the exam ______ tomorrow, I _________ you my notes.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="a" />
                        will be / Ôll lend
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="b" />
                        is / will lend
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="c" />
                        be / will lend
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="d" />
                        is / Ôd lend
                    </div>
                </div>

                <br />

                <p><strong>10. I don't think we've met before. You're confusing me with _______.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="a" />
                        one other
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="b" />
                        other person<br>
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="c" />
                        someone else
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="d" />
                        some other
                    </div>
                </div>

                <br />

                <p><strong>11.	She went to the dentist yesterday and  ________________.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="a" />
                        has a tooth pulled out
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="b" />
                        had pulled out a tooth
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="c" />
                        was pulled out by the dentist
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="d" />
                        had a tooth pulled out
                    </div>
                </div>

                <br />

                <p><strong>12.	He was a good runner so he _________ escape from the police.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="a" />
                        was able to
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="b" />
                        succeeded to
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="c" />
                        could
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="d" />
                        might
                    </div>
                </div>

                <br />

                <p><strong>13.	Most of the planet ______________ into desert by the year 2050.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="a" />
                        is going to turn
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="b" />
                        will turn
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="c" />
                        is turning
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="d" />
                        will have turned
                    </div>
                </div>

                <br />

                <p><strong>14.	Scientists __________ that the Earth is getting darker because of pollution in the atmosphere.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="a" />
                        tell
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="b" />
                        instruct
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="c" />
                        inform
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="d" />
                        claim
                    </div>
                </div>

                <br />

                <p><strong>15.	How long does the train take to ______ to London?</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="a" />
                        make
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="b" />
                        reach
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="c" />
                        get
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="d" />
                        arrive
                    </div>
                </div>

                <br />

                <p><strong>16.	He came to the party, _____ he hadn't been invited.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="a" />
                        in case
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="b" />
                        even
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="c" />
                        in spite of
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="d" />
                        although
                    </div>
                </div>

                <br />

                <p><strong>17.	If I _____________the president I __________ the environment.Ó</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="a" />
                        were / would protect
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="b" />
                        am / will to protect<br>
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="c" />
                        was / will protect
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="d" />
                        am / protect
                    </div>
                </div>

                <br />

                <p><strong>18.	His text said, ÒI ________ on the 7th.Ó</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="a" />
                        will be arrive
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="b" />
                        will be arrived<br>
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="c" />
                        would arrive
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="d" />
                        am arriving
                    </div>
                </div>

                <br />

                <p><strong>19.	She  _____________________________ fantastic.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="a" />
                        said the concert had been
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="b" />
                        said the concert has been<br>
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="c" />
                        told me the concert has been
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="d" />
                        told the concert had been
                    </div>
                </div>

                <br />

                <p><strong>20.	That was a long journey. We _______ at 7 a.m. and only arrived at 10 p.m. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="a" />
                        set away
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="b" />
                        get away
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="c" />
                        take off
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="d" />
                        set off
                    </div>
                </div>

                <br />

                <p><strong>21.	She was sitting _________ on the park bench. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="a" />
                        for herself
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="b" />
                        by herself
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="c" />
                        only herself
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="d" />
                        in her own
                    </div>
                </div>

                <br />

                <p><strong>22.	That's the university course ____________.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="a" />
                        I'm interested in
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="b" />
                        what I'm interested on<br>
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="c" />
                        I'm interested on
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="d" />
                        what I'm interested in
                    </div>
                </div>

                <br />

                <p><strong>23.	 You _________ be 18 to drive a car in Europe.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="a" />
                        should
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="b" />
                        have to
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="c" />
                        can't
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="d" />
                        ought to
                    </div>
                </div>

                <br />

                <p><strong>24.	__________ for her birthday.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="a" />
                        She was given $50
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="b" />
                        She was been given $50<br>
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="c" />
                        $50 they were given to her
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="d" />
                        They were given to her $50
                    </div>
                </div>

                <br />

                <p><strong>25.	We talked about a lot of things ________ the way to the cinema.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="a" />
                        through
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="b" />
                        by
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="c" />
                        on
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="d" />
                        in
                    </div>
                </div>
            </fieldset>


            <fieldset class="addmargintop60">
                <legend>Choose ONE of the questions below and write your answer on the area provided. You should write about 150 words. Put the question number at the top of the text.</legend>
                <p><strong>(1) Narrative: </strong>write an email to a friend about a summer camp that went wrong. Mention what the problem was,  the consequences and what happened in the end. 150 words.</p>
                <p><strong>OR</strong></p>
                <p><strong>(2) Description: </strong>describe a person who was important to you when you were a child and say why you remember him  ?? her so well.  150 words.</p>
                <div class="addmargintop60"></div>
                {!! Form::textarea('respuesta[ptxt]', null, ['id' => 'respuesta[ptxt]', 'class' => 'form-control ptxt']) !!}
            </fieldset>

            <hr>

        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-10">
            <button id="enviarformCIC" class="btn btn-block btn-success">Enviar</button>
            {!! Form::hidden('datos', serialize($_POST['datos']), array('id' => 'datos')) !!}
        </div>
    </div>

    {!! Form::close() !!}

</div>