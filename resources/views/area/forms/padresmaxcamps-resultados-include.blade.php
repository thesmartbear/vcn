<div class="row">
    <div class="col-md-12 respuestas">
        @if( isset($respuesta))
            @if(isset($respuesta->r01))
                <h4>{{trans('forms.padresmaxcamps.p01')}}</h4><p>{{trans('forms.'.$respuesta->r01)}}</p>
            @endif
            @if(isset($respuesta->r02))
                <h4>{{trans('forms.padresmaxcamps.p02')}}</h4><p>{{trans('forms.'.$respuesta->r02)}}</p>
            @endif
            @if(isset($respuesta->r03))
                <h4>{{trans('forms.padresmaxcamps.p03')}}</h4><p>{{trans('forms.'.$respuesta->r03)}}</p>
            @endif
            @if(isset($respuesta->r04))
                <h4>{{trans('forms.padresmaxcamps.p04')}}</h4><p>{{trans('forms.'.$respuesta->r04)}}</p>
            @endif
            @if(isset($respuesta->r05))
                <h4>{{trans('forms.padresmaxcamps.p05')}}</h4><p>{{trans('forms.'.$respuesta->r05)}}</p>
            @endif
            @if(isset($respuesta->r06))
                <h4>{{trans('forms.padresmaxcamps.p06')}}</h4><p>{{trans('forms.'.$respuesta->r06)}}</p>
            @endif
            @if(isset($respuesta->r07))
                <h4>{{trans('forms.padresmaxcamps.p07')}}</h4><p>{!! nl2br($respuesta->r07) !!}</p>
            @endif
            @if(isset($respuesta->r08))
                <h4>{{trans('forms.padresmaxcamps.p08')}}</h4><p>{{trans('forms.'.$respuesta->r08)}}</p>
            @endif
            @if(isset($respuesta->r09))
                <h4>{{trans('forms.padresmaxcamps.p09')}}</h4><p>{{trans('forms.'.$respuesta->r09)}}</p>
            @endif
            <h4>{{trans('forms.padresmaxcamps.web')}}</h4><p>{{$respuesta->web}}</p>
            <h4>{{trans('forms.padresmaxcamps.catalogo')}}</h4><p>{{$respuesta->catalogo}}</p>
            <h4>{{trans('forms.padresmaxcamps.blog')}}</h4><p>{{$respuesta->blog}}</p>
            <h4>{{trans('forms.padresmaxcamps.autocares')}}</h4><p>{{$respuesta->autocares}}</p>
            <h4>{{trans('forms.padresmaxcamps.lavado')}}</h4><p>{{$respuesta->lavado}}</p>
            @if($respuesta->trinity != '')
                <h4>{{trans('forms.padresmaxcamps.trinity')}}</h4><p>{{$respuesta->trinity}}</p>
            @endif
            @if($respuesta->trato != '')
                <h4>{{trans('forms.padresmaxcamps.trato')}}</h4><p>{{$respuesta->trato}}</p>
            @endif
            <h4>{{trans('forms.padresmaxcamps.informe')}}</h4><p>{{$respuesta->informe}}</p>
        @endif
    </div>
</div>