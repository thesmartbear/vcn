@extends('layouts.area')

@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop

@section('container')
    <h2 class="text-capitalize text-success">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</h2>
    <hr>

    @if( isset($respuesta) && isset($datos))

        <h3 class="text-primary">{{trans('area.Mis Datos')}}</h3>
        <p>
            <b>{{trans('area.cursoactual')}}:</b> {{ConfigHelper::getEscuelaCurso($datos->escuela_curso)}}<br />
            @if($datos->englishlevel != 0)
                <b>English Level:</b> {{ConfigHelper::getCICenglishlevelTestColonias($datos->englishlevel)}}
            @endif
        </p>

        <h3 class="text-primary margintop60">{{trans('forms.testnivelcoloniascic.mitestdenivel')}}</h3>
        @include('area.forms.tncc-'.$datos->testnumber.'-resultados')

    @endif

@stop