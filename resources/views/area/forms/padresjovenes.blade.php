@extends('layouts.area')

@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop

@section('container')
    <h2 class="text-capitalize text-success">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</h2>
    <hr>

    {!! Form::open(array('url' => route('area.forms.post',[$form->id, $booking->id]), 'id'=>'padresjovenes')) !!}

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r1">{{trans('forms.padresjovenes.p01')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r1-0">
                        <input type="radio" name="respuesta[r01]" id="r1-0" value="muydeacuerdo">
                        {{trans('forms.muydeacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r1-1">
                        <input type="radio" name="respuesta[r01]" id="r1-1" value="deacuerdo">
                        {{trans('forms.deacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r1-2">
                        <input type="radio" name="respuesta[r01]" id="r1-2" value="endesacuerdo">
                        {{trans('forms.endesacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r1-3">
                        <input type="radio" name="respuesta[r01]" id="r1-3" value="muyendesacuerdo">
                        {{trans('forms.muyendesacuerdo')}}
                    </label>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r2">{{trans('forms.padresjovenes.p02')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r2-0">
                        <input type="radio" name="respuesta[r02]" id="r2-0" value="muydeacuerdo">
                        {{trans('forms.muydeacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r2-1">
                        <input type="radio" name="respuesta[r02]" id="r2-1" value="deacuerdo">
                        {{trans('forms.deacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r2-2">
                        <input type="radio" name="respuesta[r02]" id="r2-2" value="endesacuerdo">
                        En desacuerdo
                    </label>
                    <label class="radio-inline" for="r2-3">
                        <input type="radio" name="respuesta[r02]" id="r2-3" value="muyendesacuerdo">
                        {{trans('forms.muyendesacuerdo')}}
                    </label>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r3">{{trans('forms.padresjovenes.p03')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r3-0">
                        <input type="radio" name="respuesta[r03]" id="r3-0" value="muydeacuerdo">
                        {{trans('forms.muydeacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r3-1">
                        <input type="radio" name="respuesta[r03]" id="r3-1" value="deacuerdo">
                        De acuerdo
                    </label>
                    <label class="radio-inline" for="r3-2">
                        <input type="radio" name="respuesta[r03]" id="r3-2" value="endesacuerdo">
                        En desacuerdo
                    </label>
                    <label class="radio-inline" for="r3-3">
                        <input type="radio" name="respuesta[r03]" id="r3-3" value="muyendesacuerdo">
                        {{trans('forms.muyendesacuerdo')}}
                    </label>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r4">{{trans('forms.padresjovenes.p04')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r4-0">
                        <input type="radio" name="respuesta[r04]" id="r4-0" value="muydeacuerdo">
                        {{trans('forms.muydeacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r4-1">
                        <input type="radio" name="respuesta[r04]" id="r4-1" value="deacuerdo">
                        De acuerdo
                    </label>
                    <label class="radio-inline" for="r4-2">
                        <input type="radio" name="respuesta[r04]" id="r4-2" value="endesacuerdo">
                        En desacuerdo
                    </label>
                    <label class="radio-inline" for="r4-3">
                        <input type="radio" name="respuesta[r04]" id="r4-3" value="muyendesacuerdo">
                        {{trans('forms.muyendesacuerdo')}}
                    </label>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r5">{{trans('forms.padresjovenes.p05')}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r5-0">
                        <input type="radio" name="respuesta[r05]" id="r5-0" value="muydeacuerdo">
                        {{trans('forms.muydeacuerdo')}}
                    </label>
                    <label class="radio-inline" for="r5-1">
                        <input type="radio" name="respuesta[r05]" id="r5-1" value="deacuerdo">
                        De acuerdo
                    </label>
                    <label class="radio-inline" for="r5-2">
                        <input type="radio" name="respuesta[r05]" id="r5-2" value="endesacuerdo">
                        En desacuerdo
                    </label>
                    <label class="radio-inline" for="r5-3">
                        <input type="radio" name="respuesta[r05]]" id="r5-3" value="muyendesacuerdo">
                        {{trans('forms.muyendesacuerdo')}}
                    </label>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r6">{{trans('forms.padresjovenes.p06')}}</label>
                <div class="col-md-8">
                    <textarea class="form-control" id="r6" name="respuesta[r06]" rows="6"></textarea>
                </div>
            </div>

            <?php
            $a = "residencia";
            if($booking->convocatoria->alojamiento->tipo->es_familia)
            {
                $a = "familia";
            }
            ?>

            <?php $rid = 7; ?>
            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r{{$rid}}">{{trans("forms.padresjovenes.p0$rid-$a")}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r{{$rid}}-0">
                        <input type="radio" name="respuesta[r0{{$rid}}]" id="r{{$rid}}-0" value="mal">
                        {{trans('forms.mal')}}
                    </label>
                    <label class="radio-inline" for="r{{$rid}}-1">
                        <input type="radio" name="respuesta[r0{{$rid}}]" id="r{{$rid}}-1" value="regular">
                        {{trans('forms.regular')}}
                    </label>
                    <label class="radio-inline" for="r{{$rid}}-2">
                        <input type="radio" name="respuesta[r0{{$rid}}]" id="r{{$rid}}-2" value="bien">
                        {{trans('forms.bien')}}
                    </label>
                    <label class="radio-inline" for="r{{$rid}}-3">
                        <input type="radio" name="respuesta[r0{{$rid}}]" id="r{{$rid}}-3" value="muybien">
                        {{trans('forms.muybien')}}
                    </label>
                    <label class="radio-inline" for="r{{$rid}}-4">
                        <input type="radio" name="respuesta[r0{{$rid}}]" id="r{{$rid}}-4" value="excelente">
                        {{trans('forms.excelente')}}
                    </label>
                </div>
            </div>

            <?php $rid = 8; ?>
            <div class="form-group row">
                <label class="col-md-10 margintop20 control-label" for="r{{$rid}}">{{trans("forms.padresjovenes.p0$rid")}}</label>
                <div class="col-md-10">
                    <label class="radio-inline" for="r{{$rid}}-0">
                        <input type="radio" name="respuesta[r0{{$rid}}]" id="r{{$rid}}-0" value="mal">
                        {{trans('forms.mal')}}
                    </label>
                    <label class="radio-inline" for="r{{$rid}}-1">
                        <input type="radio" name="respuesta[r0{{$rid}}]" id="r{{$rid}}-1" value="regular">
                        {{trans('forms.regular')}}
                    </label>
                    <label class="radio-inline" for="r{{$rid}}-2">
                        <input type="radio" name="respuesta[r0{{$rid}}]" id="r{{$rid}}-2" value="bien">
                        {{trans('forms.bien')}}
                    </label>
                    <label class="radio-inline" for="r{{$rid}}-3">
                        <input type="radio" name="respuesta[r0{{$rid}}]" id="r{{$rid}}-3" value="muybien">
                        {{trans('forms.muybien')}}
                    </label>
                    <label class="radio-inline" for="r{{$rid}}-4">
                        <input type="radio" name="respuesta[r0{{$rid}}]" id="r{{$rid}}-4" value="excelente">
                        {{trans('forms.excelente')}}
                    </label>
                </div>
            </div>


    <hr>
    <div class="form-group row">
        <div class="col-md-10">
            <button id="enviardatos" class="btn btn-block btn-success">Enviar</button>
            {!! Form::hidden('datos[booking]', $booking->id, array('id' => 'datos[booking]', 'class' => 'booking')) !!}
        </div>
    </div>

    {!! Form::close() !!}


    <div class="modal fade" tabindex="-1" role="dialog" id="error">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger">Error</h4>
                </div>
                <div class="modal-body">
                    <p class="errortext"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('extra_footer')
    <script type="text/javascript">

    $( "#enviardatos" ).click(function() {
        cuantos = 0;
        $('input[type="radio"]').each(function( index ) {
            if ($(this).prop("checked") != true){
                cuantos++;
            }
        });
        if (cuantos > 25 || $('#r6').val() == ''){
            $('.errortext').html('Tienes que responder todas las preguntas.');
            $('#error').modal();
            return false;
        }else{
            $('#padresjovenes').submit();
        }
    });

    </script>
@stop