@extends('layouts.area')

@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop

@section('container')
    <h2 class="text-capitalize text-success">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</h2>
    <hr>

    @if((!isset($_POST['datos']['testnumber']) || ($_POST['datos']['testnumber'] == 0)) && !isset($resultados))

    {!! Form::open(array('url' => route('area.forms.post',[$form->id, $booking->id]), 'id'=>'testnivel')) !!}

    <div class="form-group row">
        <div class="col-md-12">
            <h4 class="form-section">{{trans('area.datosacademicos')}}</h4>
        </div>
    </div>
    <div class="form-group row">

        <div class="col-md-5">
            {!! Form::label('datos[escuela_curso]', trans('area.cursoactual')) !!}
            {!! Form::select('datos[escuela_curso]', ConfigHelper::getEscuelaCurso(), '',
                array(
                    'id'=> 'datos[escuela_curso]',
                    'class' => "selectpicker form-control escuela_curso",
                    'title' => "...",
                    'required'
                )
            )
            !!}
        </div>

        <div class="col-md-5">
            {!! Form::label('datos[escuela]', trans('area.escuela')) !!}
            {!! Form::text('datos[escuela]', $ficha->escuela, array('id'=>'datos[escuela]', 'class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-5">
            {!! Form::label('datos[idioma]', trans('area.nivelingles')) !!}
            {!! Form::select('datos[idioma]', ConfigHelper::getIdiomaNivel(), $ficha->idioma,
                array(
                    'id'=> 'datos[idioma]',
                    'class' => "selectpicker form-control idioma",
                    'title' => "...",
                    'required'
                )
            )
            !!}
        </div>

    </div>

    <div class="form-group row">
        <div class="col-md-5">
            {!! Form::label('datos[tienestitulo]', trans('forms.testnivelmaxcamps.tienestitulo')) !!}
            <br />
            <label class="radio-inline">
                {!! Form::radio('datos[tienestitulo]', '1', false, array('id' => 'datos[tienestitulo]', 'class' => 'tienestitulo')) !!} Si
            </label>
            <label class="radio-inline">
                {!! Form::radio('datos[tienestitulo]', '0', false, array('id' => 'datos[tienestitulo]', 'class' => 'tienestitulo')) !!} No
            </label>
        </div>

        <div id="ingles_academia_div" class="col-md-5">
            {!! Form::label('datos[titulooficial]', trans('forms.testnivelmaxcamps.titulooficial')) !!}
            {!! Form::text('datos[titulooficial]', '', array('id'=>'datos[titulooficial]', 'class'=>'form-control titulooficial','required', 'disabled')) !!}
        </div>
    </div>


    <hr>
    <div class="form-group row">
        <div class="col-md-10">
            <button id="enviardatos" class="btn btn-block btn-success">Siguiente</button>
            {!! Form::hidden('datos[testnumber]', '0', array('id' => 'datos[testnumber]', 'class' => 'testnumber')) !!}
            {!! Form::hidden('datos[booking]', $booking->id, array('id' => 'datos[booking]', 'class' => 'booking')) !!}
        </div>
    </div>

    {!! Form::close() !!}

    @elseif( isset($_POST['datos']) && $_POST['datos']['testnumber'] == 1 )

        @include('area.forms.tnmc-1')

    @elseif( isset($_POST['datos']) && $_POST['datos']['testnumber'] == 2)

        @include('area.forms.tnmc-2')

    @elseif( isset($_POST['datos']) && $_POST['datos']['testnumber'] == 3)

        @include('area.forms.tnmc-3')

    @endif


    <div class="modal fade" tabindex="-1" role="dialog" id="error">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger">Error</h4>
                </div>
                <div class="modal-body">
                    <p class="errortext"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('extra_footer')
    <script type="text/javascript">
    /*
        $('.form-control').prop( "disabled", true );
        $('.form-radio input').prop( "disabled", true );
        $('input[type="checkbox"]').prop( "disabled", true );
    */

    $(".escuela_curso").change(function() {
        curso = $(".escuela_curso").find('option:selected').val();
        if (curso != 0 && curso <= 5) {
            $('.testnumber').val('1');
        }else if (curso >= 6 && curso <= 8){
            $('.testnumber').val('2');
        }else if (curso >= 9){
            $('.testnumber').val('3');
        }else{
            $('.testnumber').val('0');
        }
    });

    $( "#enviardatos" ).click(function() {
        var error = false;
        if( ($('.tienestitulo:checked').val() == 1 && $('.titulooficial').val() == '') || $('.tienestitulo:checked').val() == undefined){
            var error = true;
        }else{
            error = false;
        }
        if ($(".testnumber").val() > 0 && $('.escuela').val() != '' && $('.idioma').find('option:selected').val() != '' && error == false){
            $('#testnivel').submit();
        }else{
            $('.errortext').html('Tienes que rellenar todos los datos antes de empezar el test de nivel.');
            $('#error').modal();
            return false;
        }
    });

    $( "#enviarform01" ).click(function() {
        cuantos = 0;
        $('input[type="radio"]').each(function( index ) {
            if ($(this).prop("checked") != true){
                cuantos++;
            }
        });
        if (cuantos > 48){
            $('.errortext').html('{{trans('forms.responder')}}');
            $('#error').modal();
            return false;
        }else{
            $('#primero').submit();
        }
    });

    $( "#enviarform02" ).click(function() {
        cuantos = 0;
        $('input[type="radio"]').each(function( index ) {
            if ($(this).prop("checked") != true){
                cuantos++;
            }
        });
        if (cuantos > 58){
            $('.errortext').html('{{trans('forms.responder')}}');
            $('#error').modal();
            return false;
        }else{
            $('#segundo').submit();
        }
    });

    $( "#enviarform03" ).click(function() {
        cuantos = 0;
        $('input[type="radio"]').each(function( index ) {
            if ($(this).prop("checked") != true){
                cuantos++;
            }
        });
        if (cuantos > 180){
            $('.errortext').html('{{trans('forms.responder')}}');
            $('#error').modal();
            return false;
        }else{
            $('#tercero').submit();
        }
    });

    $(".tienestitulo").change(function() {
        console.log($(this).val());
        if($(this).val() != 1 && $(".titulooficial").val() == ''){
            $(".titulooficial").attr('disabled','disabled');
        }else{
            $(".titulooficial").removeAttr('disabled');
        }
    });



    </script>
@stop