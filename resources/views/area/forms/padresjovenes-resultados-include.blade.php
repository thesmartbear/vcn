<div class="row">
    <div class="col-md-12 respuestas">
        <?php
            print_r($respuesta);
        ?>
        @if( isset($respuesta) )
            <h4>{{trans('forms.padresjovenes.p01')}}</h4><p>{{trans('forms.'.$respuesta->r01)}}</p>
            <h4>{{trans('forms.padresjovenes.p02')}}</h4><p>{{ isset($respuesta->r02) ? trans('forms.'.$respuesta->r02) : "-"}}</p>
            <h4>{{trans('forms.padresjovenes.p03')}}</h4><p>{{trans('forms.'.$respuesta->r03)}}</p>
            <h4>{{trans('forms.padresjovenes.p04')}}</h4><p>{{trans('forms.'.$respuesta->r04)}}</p>
            
            <h4>{{trans('forms.padresjovenes.p05')}}</h4>
            @if(isset($respuesta->r05))
            <p>{{trans('forms.'.$respuesta->r05)}}</p>
            @endif
            
            <h4>{{trans('forms.padresjovenes.p06')}}</h4>
            @if(isset($respuesta->r06))
            <p>{{$respuesta->r06}}</p>
            @endif
            @if(isset($respuesta->r07))
                <h4>{{trans('forms.padresjovenes.p07')}}</h4><p>{{trans('forms.'.$respuesta->r07)}}</p>
                <h4>{{trans('forms.padresjovenes.p08')}}</h4><p>{{trans('forms.'.$respuesta->r08)}}</p>
            @endif
        @endif
    </div>
</div>
