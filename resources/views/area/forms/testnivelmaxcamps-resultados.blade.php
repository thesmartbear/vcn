@extends('layouts.area')

@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop

@section('container')
    <h2 class="text-capitalize text-success">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}: {{trans('forms.testnivelmaxcamps.mitest')}}</h2>
    <hr>

    @if( isset($respuesta) && isset($datos))

        <h3 class="text-primary">{{trans('area.Mis Datos')}}</h3>
        <p>
            <b>{{trans('area.cursoactual')}}:</b> {{ConfigHelper::getEscuelaCurso($datos->escuela_curso)}}<br />
            <b>{{trans('area.escuela')}}:</b> {{$datos->escuela}}<br />
            <b>{{trans('forms.testnivelmaxcamps.tienestitulo')}}:</b> {{$datos->tienestitulo?'Si':'No'}}<br />
            @if($datos->tienestitulo == 1)
                <b>{{trans('forms.testnivelmaxcamps.titulooficial')}}:</b> {{$datos->titulooficial}}
            @endif
        </p>

        <h3 class="text-primary margintop60">{{trans('forms.testnivelmaxcamps.mitestdenivel')}}</h3>
        @include('area.forms.tnmc-'.$datos->testnumber.'-resultados')

    @endif

@stop