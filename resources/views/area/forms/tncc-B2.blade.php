<div id="test01">
    {!! Form::open(array('url' => route('area.forms.post',[$form->id, $booking->id]), 'id'=>'cictest')) !!}

    <div class="form-group row">
        <div class="col-md-10">
            <fieldset class="cictest">
                <legend>Choose the correct answer. Only one answer is correct.</legend>
                <p><strong>1. I didn't hear what he was_________. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="a" />
                        speaking
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="b" />
                        saying
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="c" />
                        talking
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="d" />
                        telling
                    </div>
                </div>

                <br />

                <p><strong>2. Jake couldn't remember where he ________ his car. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="a" />
                        was parking
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="b" />
                        has parked
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="c" />
                        was parked
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="d" />
                        had parked
                    </div>
                </div>

                <br />

                <p><strong>3. The girl ________mother was ill was crying. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="a" />
                        which
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="b" />
                        whose
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="c" />
                        of which
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="d" />
                        of whom
                    </div>
                </div>

                <br />

                <p><strong>4. Your letter _____________. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="a" />
                        has arrived two days ago
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="b" />
                        arrived since two days
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="c" />
                        arrived two days ago
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="d" />
                        has arrived since two days.
                    </div>
                </div>

                <br />

                <p><strong>5. There are twelve of us, so______ get into the car at the same time. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="a" />
                        we may not all
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="b" />
                        all we may not
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="c" />
                        all we can't
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="d" />
                        we can't all
                    </div>
                </div>

                <br />

                <p><strong>6. I'll have to buy __________ trousers. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="a" />
                        two
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="b" />
                        a pair of
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="c" />
                        a couple of
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="d" />
                        a
                    </div>
                </div>

                <br />

                <p><strong>7. If you ________ help you, you only have to ask me. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="a" />
                        want me to
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="b" />
                        want that I
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="c" />
                        want I should
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="d" />
                        are wanting me to
                    </div>
                </div>

                <br />

                <p><strong>8. I've been looking for you _________. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="a" />
                        everywhere
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="b" />
                        for all places
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="c" />
                        anywhere
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="d" />
                        in all places
                    </div>
                </div>

                <br />

                <p><strong>9. Did you _______ sightseeing in Prague? </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="a" />
                        do
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="b" />
                        go
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="c" />
                        make
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="d" />
                        practise
                    </div>
                </div>

                <br />

                <p><strong>10. He ________ in his homework. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="a" />
                        did a lot of faults
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="b" />
                        did a lot of mistakes
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="c" />
                        made a lot of mistakes
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="d" />
                        made a lot of faults
                    </div>
                </div>

                <br />

                <p><strong>11. I'll ring you as soon as I _______ there. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="a" />
                        will get
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="b" />
                        will have got
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="c" />
                        shall get
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="d" />
                        get
                    </div>
                </div>

                <br />

                <p><strong>12. &quot;I went to the cinema last night&quot;.<br />
                        He told ___________________ to the cinema the previous night. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="a" />
                        me that he had been
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="b" />
                        to me that he went
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="c" />
                        me that he had gone
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="d" />
                        to me that he had been
                    </div>
                </div>

                <br />

                <p><strong>13. How _________? </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="a" />
                        was the thief getting in
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="b" />
                        the thief got in
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="c" />
                        has the thief got in
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="d" />
                        did the thief get in
                    </div>
                </div>

                <br />

                <p><strong>14. This question is ________difficult for me. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="a" />
                        so much
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="b" />
                        enough
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="c" />
                        too much
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="d" />
                        too
                    </div>
                </div>

                <br />

                <p><strong>15. We ______ go to school tomorrow. It's a holiday. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="a" />
                        mustn't
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="b" />
                        have to
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="c" />
                        don't have to
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="d" />
                        should
                    </div>
                </div>

                <br />

                <p><strong>16. If I _____________ seventeen, I could drive a car. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="a" />
                        would have
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="b" />
                        would be
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="c" />
                        had
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="d" />
                        were
                    </div>
                </div>

                <br />

                <p><strong>17. Ask him to go to the library ________ some books and DVDs. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="a" />
                        to get
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="b" />
                        for getting
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="c" />
                        that he gets
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="d" />
                        in order he gets
                    </div>
                </div>

                <br />

                <p><strong>18. _________ at the moment, I'll go into the shops.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="a" />
                        As it doesn't rain
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="b" />
                        For it doesn't rain
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="c" />
                        For it isn't raining
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="d" />
                        As it isn't raining
                    </div>
                </div>

                <br />

                <p><strong>19. Have they started cooking ________? </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="a" />
                        yet
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="b" />
                        ever
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="c" />
                        still
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="d" />
                        already
                    </div>
                </div>

                <br />

                <p><strong>20.	That was a long journey. We ___________ at 7 o'clock this morning and only arrived at 10 p.m. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="a" />
                        set on
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="b" />
                        took on
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="c" />
                        take off
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="d" />
                        set off
                    </div>
                </div>

                <br />

                <p><strong>21.	&quot;They will go out tomorrow night.&quot;<br />
                        They said that they _________. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="a" />
                        will go out the following night
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="b" />
                        would go out the following night
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="c" />
                        would go out tomorrow night
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="d" />
                        would went out the following night
                    </div>
                </div>

                <br />

                <p><strong>22. I _________ to your email from last week. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="a" />
                        would like to reply
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="b" />
                        like to reply
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="c" />
                        am wanting to reply
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="d" />
                        would like replying
                    </div>
                </div>

                <br />

                <p><strong>23.	Your bicycle shouldn't be in the house! ______________. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="a" />
                        Get out it!
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="b" />
                        Take it out!
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="c" />
                        Put it off!
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="d" />
                        Take it away!
                    </div>
                </div>

                <br />

                <p><strong>24. I had to go up the stairs because the lift _______. </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="a" />
                        was being repaired
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="b" />
                        was been repairing
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="c" />
                        was been repaired
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="d" />
                        was repairing
                    </div>
                </div>

                <br />

                <p><strong>25. She's been very kind, ________? </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="a" />
                        doesn't she
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="b" />
                        isn't she
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="c" />
                        hasn't she
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="d" />
                        wasn't she
                    </div>
                </div>
            </fieldset>


            <fieldset class="addmargintop60">
                <legend>Choose one of the following:</legend>
                <p><strong>(1) Describe your last birthday. Tell us:</strong></p>
                <ul>
                    <li>when  it was</li>
                    <li>what you did</li>
                    <li>what presents you got</li>
                    <li>how you felt at the end of the day</li>
                </ul>
                <p><strong>OR</strong></p>
                <p><strong>(2) Tell us about your first school. Say:</strong></p>
                <ul>
                    <li>if you liked the building and why</li>
                    <li>how you felt there</li>
                    <li>what friends  you made</li>
                    <li>who your favourite teacher  was and why</li>
                    <li>what your favourite activity was and why </li>
                </ul>
                <div class="addmargintop60"></div>
                {!! Form::textarea('respuesta[ptxt]', null, ['id' => 'respuesta[ptxt]', 'class' => 'form-control ptxt']) !!}
            </fieldset>

            <hr>

        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-10">
            <button id="enviarformCIC" class="btn btn-block btn-success">Enviar</button>
            {!! Form::hidden('datos', serialize($_POST['datos']), array('id' => 'datos')) !!}
        </div>
    </div>

    {!! Form::close() !!}

</div>