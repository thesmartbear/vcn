<div id="test01">
    {!! Form::open(array('url' => route('area.forms.post',[$form->id, $booking->id]), 'id'=>'cictest')) !!}

    <div class="form-group row">
        <div class="col-md-10">
            <fieldset class="cictest">
                <legend>Choose the correct answer. Only one answer is correct.</legend>
                <p><strong>1. ____________ are you from? </strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="a" />
                        What
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="b" />
                        Where
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="c" />
                        How old
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p01]" value="d" />
                        Were
                    </div>
                </div>

                <br />

                <p><strong>2. Hello, Peter, how are you?</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="a" />
                        I'm 11 years old.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="b" />
                        I'm John's cousin.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="c" />
                        I'm big.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p02]" value="d" />
                        I'm not very well.
                    </div>
                </div>

                <br />

                <p><strong>3. Is your dad at home?</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="a" />
                        It's his new home.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="b" />
                        Only my mum.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="c" />
                        Yes, she is.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p03]" value="d" />
                        At the bus station.
                    </div>
                </div>

                <br />

                <p><strong>4. My neighbour's name is Jane. I see __________  every morning.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="a" />
                        him
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="b" />
                        she
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="c" />
                        her
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p04]" value="d" />
                        he
                    </div>
                </div>

                <br />

                <p><strong>5. What's the matter? Have you got a stomach ache?</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="a" />
                        No, thank you.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="b" />
                        No, I have.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="c" />
                        No, I haven't got it.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p05]" value="d" />
                        No, I've got toothache.
                    </div>
                </div>

                <br />

                <p><strong>6. Yesterday, Mark  _______________ a pencil case for school.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="a" />
                        buy
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="b" />
                        bought
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="c" />
                        buys
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p06]" value="d" />
                        buyed
                    </div>
                </div>

                <br />

                <p><strong>7. My brother ______________ football at the moment?</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="a" />
                        is playing
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="b" />
                        plays
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="c" />
                        playing
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p07]" value="d" />
                        are playing
                    </div>
                </div>

                <br />

                <p><strong>8. Is there any pasta?</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="a" />
                        Yes, there's some here.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="b" />
                        Not many.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="c" />
                        No, there aren't any.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p08]" value="d" />
                        Yes, there is any
                    </div>
                </div>

                <br />

                <p><strong>9. He _______________ two brothers and one sister.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="a" />
                        haves got
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="b" />
                        has got
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="c" />
                        have got
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p09]" value="d" />
                        have
                    </div>
                </div>

                <br />

                <p><strong>10. ____________ you open the window, please?</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="a" />
                        Are
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="b" />
                        Do
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="c" />
                        Could
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p10]" value="d" />
                        Have
                    </div>
                </div>

                <br />

                <p><strong>11. Do you want a drink of water?</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="a" />
                        Yes, do
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="b" />
                        Yes, it is.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="c" />
                        Yes, I had
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p11]" value="d" />
                        Yes, please.
                    </div>
                </div>

                <br />

                <p><strong>12. ___________ are five children in the playground.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="a" />
                        There
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="b" />
                        These
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="c" />
                        Their
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p12]" value="d" />
                        They
                    </div>
                </div>

                <br />

                <p><strong>13. ____________ have dogs in their homes.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="a" />
                        Any
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="b" />
                        Every
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="c" />
                        A
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p13]" value="d" />
                        Some
                    </div>
                </div>

                <br />

                <p><strong>14. My sister is older ____________ you.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="a" />
                        is
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="b" />
                        that
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="c" />
                        to
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p14]" value="d" />
                        than
                    </div>
                </div>

                <br />

                <p><strong>15. Would you like to come to my house?</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="a" />
                        Yes, I do
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="b" />
                        I like my house.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="c" />
                        No, I want to go home.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p15]" value="d" />
                        Yes, good bye.
                    </div>
                </div>

                <br />

                <p><strong>16. How often do you go horse riding?</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="a" />
                        Yes, I do.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="b" />
                        I went on Saturday.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="c" />
                        I'm riding a horse.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p16]" value="d" />
                        Every weekend.
                    </div>
                </div>

                <br />

                <p><strong>17. He ____________ have a car.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="a" />
                        doesn't
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="b" />
                        don't
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="c" />
                        hasn't
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p17]" value="d" />
                        not
                    </div>
                </div>

                <br />

                <p><strong>18. Last Monday, Mum's taxi ____________________ at six o'clock.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="a" />
                        don't arrived
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="b" />
                        arrives
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="c" />
                        didn't arrived
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p18]" value="d" />
                        didn't arrive
                    </div>
                </div>

                <br />

                <p><strong>19. They usually go to work ___________ train.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="a" />
                        by
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="b" />
                        in
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="c" />
                        on
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p19]" value="d" />
                        with
                    </div>
                </div>

                <br />

                <p><strong>20. They ___________ eat chocolate at bed time.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="a" />
                        Tuesday
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="b" />
                        any day
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="c" />
                        some day
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p20]" value="d" />
                        never
                    </div>
                </div>

                <br />

                <p><strong>21. That is _____________ restaurant in town.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="a" />
                        better
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="b" />
                        the best
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="c" />
                        the most good
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p21]" value="d" />
                        goodest
                    </div>
                </div>

                <br />

                <p><strong>22. I like ________________ in the park.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="a" />
                        walking
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="b" />
                        walk
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="c" />
                        to walking
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p22]" value="d" />
                        not walk
                    </div>
                </div>

                <br />

                <p><strong>23. What did you do last weekend?</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="a" />
                        Yes, I did.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="b" />
                        I had a party.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="c" />
                        I went to a friend.
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p23]" value="d" />
                        I go to the cinema.
                    </div>
                </div>

                <br />

                <p><strong>24. Bananas grow _________ hot countries.</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="a" />
                        in
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="b" />
                        under
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="c" />
                        at
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p24]" value="d" />
                        between
                    </div>
                </div>

                <br />

                <p><strong>25. Are you ______________ to come to my house this weekend?</strong></p>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="a" />
                        go
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="b" />
                        went
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="c" />
                        going
                    </div>
                    <div class="col-sm-3">
                        <input type="radio" name="respuesta[p25]" value="d" />
                        to go
                    </div>
                </div>
            </fieldset>


            <fieldset class="addmargintop60">
                <legend>Write a paragraph / some sentences about yourself.</legend>
                {!! Form::textarea('respuesta[ptxt]', null, ['id' => 'respuesta[ptxt]', 'class' => 'form-control ptxt']) !!}
            </fieldset>

            <hr>

        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-10">
            <button id="enviarformCIC" class="btn btn-block btn-success">Enviar</button>
            {!! Form::hidden('datos', serialize($_POST['datos']), array('id' => 'datos')) !!}
        </div>
    </div>

    {!! Form::close() !!}

</div>