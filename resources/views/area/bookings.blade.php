<ul class='area-bookings'>
    @foreach($ficha->bookings_area as $booking)
        <?php
        $fotoscentro = '';
        $fotoscentroname = array();
        $path = public_path() ."/assets/uploads/center/" . $booking->curso->centro->center_images;
        $folder = "/assets/uploads/center/" . $booking->curso->centro->center_images;

        if (is_dir($path)) {
            $results = scandir($path);
            foreach ($results as $result) {
                if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                $file = $path . '/' . $result;

                if (is_file($file)) {
                    $fotoscentroname[] = $result;

                }
            }
        }
        ?>
        <?php
        $fotoscurso = '';
        $fotoscursoname = array();
        $path = public_path() ."/assets/uploads/course/" . $booking->curso->course_images;
        $folder = "/assets/uploads/course/" . $booking->curso->course_images;

        if (is_dir($path)) {
            $results = scandir($path);
            foreach ($results as $result) {
                if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                $file = $path . '/' . $result;

                if (is_file($file)) {
                    $fotoscursoname[] = $result;
                }
            }
        }
        
        $firmar = null;
        if($booking->firmante)
        {
            $firmar = $booking->getArchivoFirmaPendiente();
        }

        $csosp = $booking->contacto_sos_proveedor;
        $csos = $booking->contacto_sos;

        $curso = $booking->curso;
        
        ?>
        
        <li>
            <div class="portlet light bordered info">

                <div class="portlet-title tabbable-line">
                    <div class="portlet-photo">
                        @if(is_file(public_path() ."/assets/uploads/course/" . $booking->curso->course_images . "/" .$booking->curso->image_portada))
                            <div class="booking-foto" style="background-image: url('/assets/uploads/course/{{$booking->curso->course_images}}/{{$booking->curso->image_portada}}');"></div>
                        @elseif(is_file(public_path() ."/assets/uploads/center/" . $booking->curso->centro->center_images . "/" .$booking->curso->centro->center_image_portada))
                            <div class="booking-foto" style="background-image: url('/assets/uploads/center/{{$booking->curso->centro->center_images}}/{{$booking->curso->centro->center_image_portada}}');"></div>
                        @elseif(!is_file(public_path() ."/assets/uploads/course/" . $booking->curso->course_images . "/" .$booking->curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $booking->curso->centro->center_images . "/" .$booking->curso->centro->center_image_portada))
                            @if(count($fotoscursoname))
                                <div class="booking-foto" style="background-image: url('/assets/uploads/course/{{$booking->curso->course_images}}/{{$fotoscursoname[rand(0,count($fotoscursoname)-1)]}}');"></div>
                            @elseif(!count($fotoscursoname) && count($fotoscentroname))
                                <div class="booking-foto" style="background-image: url('/assets/uploads/center/{{$booking->curso->centro->center_images}}/{{$fotoscentroname[rand(0,count($fotoscentroname)-1)]}}');"></div>
                            @else
                                <div class="booking-foto" style="background-color: #e5e5e5;"></div>
                            @endif
                        @endif
                    </div>

                    <div class="caption font-green-sharp">
                        {{-- BID:{{$booking->id}} --}}
                        {!! ConfigHelper::getTipoTransporteIcono($booking->vuelo ? $booking->vuelo->transporte : 0) !!}
                        <span class="caption-subject bold uppercase">
                            {{ $booking->programa}}
                        </span>
                        <br />
                        <span class="caption-helper separator"> | </span>
                        <span class="caption-helper out"><i class="fa fa-arrow-circle-right fa-lg text-success"></i> {!! trans('area.finicio') !!}: {{$booking->curso_start_date}}</span>
                        <span class="caption-helper in"><i class="fa fa-arrow-circle-left fa-lg text-danger"></i> {!! trans('area.ffin') !!}: {{$booking->curso_end_date}}</span>
                        <h4 class="text-muted"><i class="fa fa-male fa-fw text-muted"></i> {{$booking->viajero->full_name}}</h4>
                    </div>

                    <div class="tools">
                        <a href="" class="collapse"></a>
                        <a href="" class="fullscreen"></a>
                        <br />
                        <span class="caption-helper label label-success label-lg text-right">{!! trans('area.estado') !!}: {{$booking->status_area}}</span>
                    </div>

                    @if($curso->instagram_user)
                    <h4 class="caption">
                        <a class="text-muted" href="https://instagram.com/{{$curso->instagram_user}}" target="_blank">{!! trans('area.instagram') !!}</a>
                    </h4>
                    @endif

                </div>

                <div class="portlet-body">
                    
                    @if($firmar)
                    <div class="row">
                        <div class="col-md-10">
                            <div class="note note-warning">
                                <h4 class="block">@lang('area.booking.atencion')</h4>
                                <p>
                                    @if($booking->archivos_firma_count>1)
                                        <strong>@lang('area.booking.pago_firma2')</strong>
                                    @else
                                        <strong>@lang('area.booking.pago_firma')</strong>
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="col-md-2">

                            <a href="{{ route('area.booking.firmar', $booking->id) }}" class="btn btn-success">Firmar</a>
                        </div>
                    </div>
                    <hr>
                    @endif

                    @if($booking->es_overbooking)
                    <div class="row">
                        <div class="note note-warning">
                            <strong>@lang('area.booking.overbooking-st')</strong>
                        </div>
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-lg-9 col-md-12">
                            <div class="row">
                                <div class="col-md-4 col-xs-6">
                                    <div class="callout">
                                        <h4 class="text-success">{!! trans('area.datosbasicosviajero') !!}:</h4>
                                        <p>
                                            <strong>{!! trans('area.fnacimiento') !!}:</strong> {{Carbon::parse($booking->viajero->fechanac)->format('d/m/Y')}}
                                            <br /><strong>{!! trans('area.dni') !!}:</strong> {{$booking->viajero->datos->documento}}
                                        </p>
                                        <p>
                                            <strong>{!! trans('area.pasaporte') !!}:</strong> {{$booking->viajero->datos->pasaporte}}
                                            <br /><strong>{!! trans('area.femision') !!}:</strong> {{$booking->viajero->datos->pasaporte_emision!='0000-00-00'?Carbon::parse($booking->viajero->datos->pasaporte_emision)->format('d/m/Y'):""}}
                                            <br /><strong>{!! trans('area.fcaducidad') !!}:</strong> {{$booking->viajero->datos->pasaporte_caduca!='0000-00-00'?Carbon::parse($booking->viajero->datos->pasaporte_caduca)->format('d/m/Y'):""}}
                                        </p>
                                    </div>
                                </div>

                                @if($booking->curso->es_convocatoria_multi)
                                    <div class="col-md-7 col-md-offset-1 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-8 col-xs-12">
                                            <div class="callout">
                                                <h4 class="text-success">{!! trans('area.especialidades') !!}:</h4>
                                                @foreach($booking->multis->sortBy('n') as $multi)
                                                <ul>
                                                    <li>{!! trans('area.semana') !!} {{$multi->n}}: {{$multi->especialidad_name}}</li>
                                                </ul>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                @endif

                                @if($booking->vuelo_area)
                                <div class="col-md-7 col-md-offset-1 col-xs-12 vuelos">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            {{-- Info de vuelo según el día --}}
                                            @if($booking->vuelo_area)
                                                <h4 class="text-success">{!! trans($booking->vuelo_titulo_area) !!}:</h4>
                                                @foreach($booking->vuelo_area as $v)
                                                <p>
                                                    <strong>{{Carbon::parse($v->salida_fecha)->format('d/m/Y')}} {{$v->company}} {{$v->vuelo}}</strong>
                                                    <br /><strong>DEPARTURE</strong> {{$v->salida_aeropuerto}} {{Carbon::parse($v->salida_hora)->format('H:i')}}h
                                                    <br /><strong>ARRIVAL</strong> {{$v->llegada_aeropuerto}} {{Carbon::parse($v->llegada_fecha)->format('d/m/Y')}} {{Carbon::parse($v->llegada_hora)->format('H:i')}}h
                                                </p>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                        {{-- Info de punto encuentro según el día --}}
                                            @if($booking->vuelo_info_area)
                                                <h4 class="text-success">{!! trans('area.pencuentro') !!}:</h4>

                                                @if($booking->vuelo->encuentro_lugar)
                                                    <p>
                                                        <strong>{!! trans('area.dia') !!}: </strong>{{Carbon::parse($booking->vuelo->encuentro_fecha)->format('d/m/Y')}}
                                                        <br /><strong>{!! trans('area.hora') !!}:</strong> {{Carbon::parse($booking->vuelo->encuentro_hora)->format('H:i')}}h
                                                        <br />
                                                        <strong>{!! $booking->vuelo->transporte?trans('area.punto'):trans('area.aeropuerto') !!}: </strong>{{$booking->vuelo->encuentro_lugar}}

                                                            @if(!$booking->vuelo->transporte)
                                                            - Terminal: {{$booking->vuelo->encuentro_terminal}} - {!! trans('area.mostradores') !!} {{$booking->vuelo->encuentro_punto}}
                                                            @endif
                                                    </p>
                                                @else
                                                    @if($booking->vuelo_salida_area)
                                                        <p>
                                                            <strong>{!! trans('area.dia') !!}:</strong> {{Carbon::parse($booking->vuelo_salida_area->salida_fecha)->format('d/m/Y')}}
                                                            <br /><strong>{!! trans('area.hora') !!}:</strong> {{Carbon::parse($booking->vuelo_salida_area->salida_hora)->subHours(2)->format('H:i')}}h
                                                            <br /><strong>{!! $booking->vuelo->transporte?trans('area.punto'):trans('area.aeropuerto') !!}:</strong> {!! trans('area.mostradores') !!} {{$booking->vuelo_salida_area->company}}
                                                        </p>
                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endif

                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-lg-12">

                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">

                                            @if($booking->vuelo)
                                                <li role="presentation" class="active">
                                                    <a href="#b{{$booking->id}}-vuelo" aria-controls="b{{$booking->id}}-vuelo" role="tab" data-toggle="tab">
                                                        @if($booking->vuelo->transporte == 0 || $booking->vuelo->transporte == null)
                                                            {!! trans('area.vuelo') !!}
                                                        @else
                                                            {!! trans('area.transporte') !!}
                                                        @endif
                                                    </a>
                                                </li>
                                            @endif

                                            <li role="presentation" @if($booking->vuelo == null) class="active" @endif><a href="#b{{$booking->id}}-alojamiento" aria-controls="b{{$booking->id}}-alojamiento" role="tab" data-toggle="tab">{!! trans('area.alojamiento') !!}</a></li>

                                            @if($booking->schools_area->count())
                                                <li role="presentation"><a href="#b{{$booking->id}}-schools" aria-controls="b{{$booking->id}}-schools" role="tab" data-toggle="tab">{!! trans('area.escuela') !!}</a></li>
                                            @endif

                                            @if($booking->area_pagos)
                                                <li role="presentation"><a href="#b{{$booking->id}}-pagos" aria-controls="b{{$booking->id}}-pagos" role="tab" data-toggle="tab">{!! trans('area.pagos') !!}</a></li>
                                            @endif

                                            <li role="presentation"><a href="#b{{$booking->id}}-docs" aria-controls="b{{$booking->id}}-docs" role="tab" data-toggle="tab">{!! trans('area.documentos') !!}</a></li>

                                            @if($booking->documentos_especificos->count())
                                            <li role="presentation"><a href="#b{{$booking->id}}-docs_e" aria-controls="b{{$booking->id}}-docs_e" role="tab" data-toggle="tab">{!! trans('area.documentos_especificos') !!}</a></li>
                                            @endif

                                            @if($csosp || $csos)
                                            <li role="presentation"><a href="#b{{$booking->id}}-sos" aria-controls="b{{$booking->id}}-sos" role="tab" data-toggle="tab">{!! trans('area.sos_tab') !!}</a></li>
                                            @endif
                                            
                                        </ul>

                                        <div class="booking-botones">
                                            <!-- Tab panes -->
                                            <div class="tab-content">

                                                @if($booking->vuelo)
                                                    <div role="tabpanel" class="tab-pane fade in active" id="b{{$booking->id}}-vuelo">
                                                        @include('area.booking-vuelo',['ficha'=> $booking])
                                                    </div>
                                                @endif

                                                @if($csosp || $csos)
                                                <div role="tabpanel" class="tab-pane fade in" id="b{{$booking->id}}-sos">
                                                    {!! trans('area.sos_texto', ['phone'=> ($booking->oficina ? $booking->oficina->telefono : "-")]) !!}
                                                    <br><br>

                                                    @if($csos)
                                                        {!! trans('area.sos_plataforma', ['plataforma'=> $booking->plataforma_name, 'phone'=> $csos->phone]) !!}
                                                        <br>
                                                        {!! $booking->sos_plataforma_notas !!}
                                                    @endif
                                                    @if($csosp && $csosp->count())
                                                        <hr>
                                                        {!! trans('area.sos_proveedor') !!}
                                                        <br>
                                                        @foreach($csosp as $c)
                                                            @if($c)
                                                                {{$c->phone}} ({{$c->name}})
                                                                <br>
                                                            @endif
                                                        @endforeach
                                                        {!! $booking->sos_proveedor_notas !!}
                                                    @endif

                                                </div>
                                                @endif

                                                @if($booking->alojamiento)
                                                    <div role="tabpanel" class="tab-pane fade in @if($booking->vuelo == null) active @endif" id="b{{$booking->id}}-alojamiento">
                                                        @include('area.booking-alojamiento',['ficha'=> $booking])
                                                    </div>
                                                @endif

                                                @if($booking->schools_area->count())
                                                    <div role="tabpanel" class="tab-pane fade in" id="b{{$booking->id}}-schools">
                                                        @include('area.booking-schools',['ficha'=> $booking])
                                                    </div>
                                                @endif

                                                @if($booking->area_pagos)
                                                    <div role="tabpanel" class="tab-pane fade in" id="b{{$booking->id}}-pagos">
                                                        @include('area.booking-pagos',['ficha'=> $booking])
                                                    </div>
                                                @endif

                                                <div role="tabpanel" class="tab-pane fade in" id="b{{$booking->id}}-docs">
                                                    @include('area.booking-docs',['ficha'=> $booking])
                                                </div>

                                                @if($booking->documentos_especificos->count()) 
                                                <div role="tabpanel" class="tab-pane fade in" id="b{{$booking->id}}-docs_e">
                                                    @include('area.booking-doc_especificos',['ficha'=> $booking])
                                                </div>
                                                @endif

                                            </div>
                                        </div>
                                    </div>

                            </div>
                        </div>

                        <div class="col-lg-3 col-md-12">

                            @if($booking->area_reunion && count($booking->reuniones) > 0)
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-info-circle fa-2x"></i>{!! trans('area.reuniones') !!}
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    @foreach($booking->reuniones as $reunion)
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>
                                                    @if($reunion->notas && $reunion->notas != "")
                                                        {!! $reunion->notas !!}<br>
                                                    @endif
                                                
                                                    <strong><i class="fa fa-map-marker"></i> {!! trans('area.lugar') !!}:</strong> {{$reunion->lugar}}<br />
                                                    <strong><i class="fa fa-calendar"></i> {!! trans('area.dia') !!}:</strong> {{$reunion->fecha->format('d/m/Y')}}<br />
                                                    <strong><i class="fa fa-clock-o"></i> {!! trans('area.hora') !!}:</strong> {{Carbon::parse($reunion->hora)->format('H:i')}}h
                                                </p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif

                            @if($booking->area_examenes->count())
                            <div class="portlet box blue" id="area-box">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-list-alt fa-2x"></i>{!! trans('area.Examenes') !!}
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    @foreach($booking->area_examenes as $test)
                                        @php
                                            $rt = $test->getRespuestaBooking($booking->id);
                                            $et = $rt ? $rt->status : 0;    
                                        @endphp

                                        <div class="row">
                                            <div class="col-xs-8">
                                                <a href="{{ route('area.exams.ask', [$test->id,$booking->id]) }}">{{ $test->title }}</a>
                                            </div>
                                            <div class="col-xs-4 text-right">
                                                @if($et == 0)
                                                    <span class="text-danger">{!! trans("area.pendiente") !!}</span>
                                                @elseif($et == 1)
                                                    <span class="text-warning">{!! trans("area.completo") !!}</span>
                                                @endif
                                            </div>
                                        </div>

                                    @endforeach

                                </div>
                            </div>
                            @endif

                            @if($booking->area_cuestionarios->count())
                            <div class="portlet box blue" id="area-box">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-list-alt fa-2x"></i>{!! trans('area.Cuestionarios') !!}
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    {{-- @if(!Session::get('vcn.manage.area.home')) --}}
                                        @foreach($booking->area_cuestionarios as $form)
                                            @if($form->area)

                                                <?php
                                                $ac_estado = $booking->getAreaCuestionarioEstado($form->id);
                                                $actxt = ConfigHelper::areaFormEstado($ac_estado);

                                                if($form->destino == 'tutores'){
                                                    $tuser = "tutor_id";
                                                    $tuser_id = $usuario->ficha->id;
                                                }else{
                                                    $tuser = "viajero_id";
                                                    $tuser_id = $booking->viajero->id;
                                                }

                                                $r = VCN\Models\System\CuestionarioRespuesta::where('booking_id',$booking->id)->where($tuser, $tuser_id)->where('cuestionario_id',$form->id)->orderBy('updated_at', 'desc')->first();
                                                ?>
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        @if($ac_estado == 1 && $form->test == 0 && $r)
                                                            <a href="{{ route('area.forms.resultado',[$form->id,$booking->id,$r->id]) }}">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</a>
                                                        @else
                                                            <a href="{{ route('area.forms', [$form->id,$booking->id]) }}">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</a>
                                                        @endif
                                                    </div>
                                                    <div class="col-xs-4 text-right">
                                                        @if($ac_estado == 0)
                                                            <span class="text-danger">{!! trans("area.".$actxt) !!}</span>
                                                        @elseif($ac_estado == 1)
                                                            <span class="text-warning">{!! trans("area.".$actxt) !!}</span>
                                                        @elseif($ac_estado == 2)
                                                            <span class="text-success">{!! trans("area.".$actxt) !!}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="cuestionario"></div>
                                            @endif
                                        @endforeach
                                    {{-- @else
                                        <i>Sólo para usuarios</i>
                                    @endif --}}
                                </div>
                            </div>
                            @endif

                        </div>


                    </div>

                </div>
            </div>
        </li>
    @endforeach
</ul>