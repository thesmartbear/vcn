<div class="row">
    <div class="col-md-12">

        @if($ficha->vuelo && $ficha->transporte_recogida) 
            {{trans('area.transporte_otro_info', ['aeropuerto'=> $ficha->vuelo->aeropuerto, 'aporte'=> $ficha->transporte_recogida])}}
        <hr>
        @endif

        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead>
            <tr>
                <th class="col-md-6">{!! trans('area.ida') !!}</th>
                <th>{!! trans('area.vuelta') !!}</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        @foreach($ficha->vuelo->etapas->where('tipo',0) as $v)
                            <p>
                                <strong>{{Carbon::parse($v->salida_fecha)->format('d/m/Y')}} {{$v->company}} {{$v->vuelo}}</strong>
                                <br /><strong>DEPARTURE</strong> {{$v->salida_aeropuerto}} {{Carbon::parse($v->salida_hora)->format('H:i')}}h
                                <br /><strong>ARRIVAL</strong> {{$v->llegada_aeropuerto}} {{Carbon::parse($v->llegada_hora)->format('H:i')}}h
                            </p>
                        @endforeach
                    </td>
                    <td>
                        @foreach($ficha->vuelo->etapas->where('tipo',1) as $v)
                            <p>
                                <strong>{{Carbon::parse($v->salida_fecha)->format('d/m/Y')}} {{$v->company}} {{$v->vuelo}}</strong>
                                <br /><strong>DEPARTURE</strong> {{$v->salida_aeropuerto}} {{Carbon::parse($v->salida_hora)->format('H:i')}}h
                                <br /><strong>ARRIVAL</strong> {{$v->llegada_aeropuerto}} {{Carbon::parse($v->llegada_fecha)->format('d/m/Y')}} {{Carbon::parse($v->llegada_hora)->format('H:i')}}h
                            </p>
                        @endforeach
                    </td>
                </tr>
            </tbody>
        </table>

    </div>
</div>