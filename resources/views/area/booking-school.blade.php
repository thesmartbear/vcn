@extends('layouts.area')


@section('breadcrumb')
    {!! Breadcrumbs::render('area.booking.school',$booking) !!}
@stop


@section('content')
    <div class="row">
        <div class="col-md-10">
            <div class="caption font-green-sharp">
                {!! ConfigHelper::getTipoTransporteIcono($booking->vuelo ? $booking->vuelo->transporte : 0) !!}
                <span class="caption-subject bold uppercase">
                    {{ $booking->programa}}
                </span>
                <span class="caption-helper separator"> | </span>
                <span class="caption-helper out"><i class="fa fa-arrow-circle-right fa-lg text-success"></i> {!! trans('area.finicio') !!}: {{$booking->curso_start_date}}</span>
                <span class="caption-helper in"><i class="fa fa-arrow-circle-left fa-lg text-danger"></i> {!! trans('area.ffin') !!}: {{$booking->curso_end_date}}</span>
                <h4 class="text-muted"><i class="fa fa-male fa-fw text-muted"></i> {{$booking->viajero->full_name}}</h4>
            </div>
        </div>
        <div class="col-md-2">
            <?php $fam = $booking->schools_area->where('booking_id',$booking->id)->first(); ?>
            <a class="btn btn-block btn-success" href="{{route('area.booking.school-pdf', $fam->id)}}"><i class="fa fa-download"></i> {!! trans('area.descargar') !!}</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <hr>
            <h3 class="page-title font-green-sharp">{{$ficha->name}}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4  col-xs-12">
            @if($ficha->foto)
                <img class="img-responsive img-thumbnail" src="{{$ficha->foto}}" />
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.datoscontacto')}}</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="well">
                @if($ficha->direccion)
                    <strong>{{trans('area.direccion')}}:</strong> {{$ficha->direccion}}<br />
                @endif

                @if($ficha->cp)
                    <strong>{{trans('area.cp')}}:</strong> {{$ficha->cp}}<br />
                @endif

                @if($ficha->poblacion)
                    <strong>{{trans('area.poblacion')}}:</strong> {{$ficha->poblacion}}<br />
                @endif

                @if($ficha->pais)
                    <strong>{{trans('area.pais')}}:</strong> {{$ficha->pais->name}}
                @endif

                <hr>

                @if($ficha->telefono)
                    <strong>{{trans('area.telefono')}}:</strong> {{$ficha->telefono}}<br />
                @endif

                @if($ficha->email)
                    <strong>{{trans('area.email')}}:</strong> {{$ficha->email}}<br />
                @endif

                @if($ficha->notas)
                    <strong>{{trans('area.comentarios')}}:</strong> {{$ficha->notas}}
                @endif

            </div>
        </div>

        @if(strip_tags($ficha->direccion) != '')
            <div class="col-md-6">
                <img src="https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=625x250&center={{$ficha->direccion_completa}}&markers=clor:red|{{$ficha->direccion_completa}}&language={{App::getLocale()}}&zoom=13&key=AIzaSyCNPWo91Kd3D656Z5R2U6K1vFlx65KxpE8" />
            </div>
        @endif
    </div>

@stop
