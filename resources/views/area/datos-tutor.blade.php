<div class="portlet light bordered info">
    <div class="portlet-title">
        <div class="caption font-green-sharp">
            <i class="fa fa-shield fa-fw"></i>
            <span class="caption-subject bold uppercase">{{$ficha->full_name}} <small>(tutor)</small></span>
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
            <a href="" class="fullscreen"></a>
        </div>

    </div>

    <div class="alert alert-info" role="alert" style="text-align:justify;">
        {{trans('area.datos_tutor')}}
    </div>

    <div class="portlet-body flip-scroll">

        <h4>{!! trans('area.datostutor') !!}</h4>
        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> trans('area.nombre')])
            </div>
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'lastname', 'texto'=> trans('area.apellidos')])
            </div>
            <div class="col-md-4">
                <img src="{{$ficha->avatar ?: $ficha->foto}}" class="form-foto">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'phone', 'texto'=> trans('area.telefono')])
            </div>
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'movil', 'texto'=> trans('area.movil')])
            </div>
            <div class="col-md-4">
                <a href="#" data-toggle="tooltip" title="{{trans('area.datos_email')}}"><i class="fa fa-ban"></i></a>
                @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> trans('area.email'), 'disabled'=>true])
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'profesion', 'texto'=> trans('area.profesion')])
            </div>
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'empresa', 'texto'=> trans('area.empresa')])
            </div>
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'empresa_phone', 'texto'=> trans('area.telfempresa')])
            </div>
        </div>

        <div class="form-group">
            @if(ConfigHelper::config('es_rgpd'))
                @include('includes.form_checkbox', [ 'campo'=> 'lopd_check2', 'texto'=> trans('area.booking.lopd_check2', ['plataforma'=> ConfigHelper::plataformaApp()]), 'html'=> true ])
            @else
                @include('includes.form_checkbox', [ 'campo'=> 'optout', 'texto'=> trans('area.optout'), 'html'=> true ])
            @endif
        </div>
        
    </div>

</div>

<script type="text/javascript">

    $('.form-control').prop( "disabled", true );
    $('.form-radio input').prop( "disabled", true );
    $('input[type="checkbox"]').prop( "disabled", true );

</script>