@extends('layouts.area')


@section('breadcrumb')
    {{-- {!! Breadcrumbs::render('area.index') !!} --}}
@stop


@section('content')

<div class="row">
    <div class="col-md-12">
        <h4 class="text-success">{!! trans('area.Bookings pendientes') !!}</h4>
    </div>
</div>

<hr>

    @if($usuario->es_viajero)

        @if($usuario->ficha->bookings_area_pendientes->count()>0)
            <ul class='area-bookings'>
            @foreach($usuario->ficha->bookings_area_pendientes as $booking )
                <li>
                    <div class="portlet light bordered info">

                        <div class="portlet-title tabbable-line">
                            <div class="portlet-photo">
                                @if(is_file(public_path() ."/assets/uploads/course/" . $booking->curso->course_images . "/" .$booking->curso->image_portada))
                                    <div class="booking-foto" style="background-image: url('/assets/uploads/course/{{$booking->curso->course_images}}/{{$booking->curso->image_portada}}');"></div>
                                @elseif(is_file(public_path() ."/assets/uploads/center/" . $booking->curso->centro->center_images . "/" .$booking->curso->centro->center_image_portada))
                                    <div class="booking-foto" style="background-image: url('/assets/uploads/center/{{$booking->curso->centro->center_images}}/{{$booking->curso->centro->center_image_portada}}');"></div>
                                @elseif(!is_file(public_path() ."/assets/uploads/course/" . $booking->curso->course_images . "/" .$booking->curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $booking->curso->centro->center_images . "/" .$booking->curso->centro->center_image_portada))
                                @endif
                            </div>

                            <div class="caption font-green-sharp">
                                {{-- BID:{{$booking->id}} --}}
                                {!! ConfigHelper::getTipoTransporteIcono($booking->vuelo ? $booking->vuelo->transporte : 0) !!}
                                <span class="caption-subject bold uppercase">
                                    {{ $booking->programa}}
                                </span>
                                <br />
                                <span class="caption-helper separator"> | </span>
                                <span class="caption-helper out"><i class="fa fa-arrow-circle-right fa-lg text-success"></i> {!! trans('area.finicio') !!}: {{$booking->curso_start_date}}</span>
                                <span class="caption-helper in"><i class="fa fa-arrow-circle-left fa-lg text-danger"></i> {!! trans('area.ffin') !!}: {{$booking->curso_end_date}}</span>
                                <h4 class="text-muted"><i class="fa fa-male fa-fw text-muted"></i> {{$booking->viajero->full_name}}</h4>
                            </div>

                            <a target='_blank' href="{{ route('area.comprar.booking', $booking->id) }}" class="btn btn-success">CONTINUAR</a>

                        </div>

                    </div>

                </li>
            @endforeach
            </ul>
        @else
            <p>{!! trans('area.nobookings') !!}</p>
        @endif
        
    @elseif($usuario->es_tutor)

        <?php $y=1; ?>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            @foreach($usuario->tutor->viajeros as $viajero)
                <li role="presentation" @if($y == 1) class="active" @endif ><a href="#{{$y}}" aria-controls="{{$y}}" role="tab" data-toggle="tab"><h4 class="text-capitalize">{{mb_strtolower($viajero->full_name)}} <small>({!! trans('area.viajero') !!})</small></h4></a></li>
                <?php $y++; ?>
            @endforeach
            <li role="presentation" @if($y == 1) class="active" @endif><a href="#{{$y}}" aria-controls="{{$y}}" role="tab" data-toggle="tab"><h4>@lang("area.Sin Viajero")</h4></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <?php $y=1; ?>
            @foreach($usuario->tutor->viajeros as $viajero)
                <div role="tabpanel" class="tab-pane fade in @if($y == 1) active @endif " id="{{$y}}">
                    @if($viajero->bookings_area_pendientes->count()>0)
                        {{-- @include('area.bookings', ['ficha'=> $viajero] ) --}}
                        <ul class='area-bookings'>
                        @foreach($viajero->bookings_area_pendientes as $booking )
                            <li>
                                <div class="portlet light bordered info">

                                    <div class="portlet-title tabbable-line">
                                        <div class="portlet-photo">
                                            @if(is_file(public_path() ."/assets/uploads/course/" . $booking->curso->course_images . "/" .$booking->curso->image_portada))
                                                <div class="booking-foto" style="background-image: url('/assets/uploads/course/{{$booking->curso->course_images}}/{{$booking->curso->image_portada}}');"></div>
                                            @elseif(is_file(public_path() ."/assets/uploads/center/" . $booking->curso->centro->center_images . "/" .$booking->curso->centro->center_image_portada))
                                                <div class="booking-foto" style="background-image: url('/assets/uploads/center/{{$booking->curso->centro->center_images}}/{{$booking->curso->centro->center_image_portada}}');"></div>
                                            @elseif(!is_file(public_path() ."/assets/uploads/course/" . $booking->curso->course_images . "/" .$booking->curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $booking->curso->centro->center_images . "/" .$booking->curso->centro->center_image_portada))
                                            @endif
                                        </div>

                                        <div class="caption font-green-sharp">
                                            {{-- BID:{{$booking->id}} --}}
                                            {!! ConfigHelper::getTipoTransporteIcono($booking->vuelo ? $booking->vuelo->transporte : 0) !!}
                                            <span class="caption-subject bold uppercase">
                                                {{ $booking->programa}}
                                            </span>
                                            <br />
                                            <span class="caption-helper separator"> | </span>
                                            <span class="caption-helper out"><i class="fa fa-arrow-circle-right fa-lg text-success"></i> {!! trans('area.finicio') !!}: {{$booking->curso_start_date}}</span>
                                            <span class="caption-helper in"><i class="fa fa-arrow-circle-left fa-lg text-danger"></i> {!! trans('area.ffin') !!}: {{$booking->curso_end_date}}</span>
                                            <h4 class="text-muted"><i class="fa fa-male fa-fw text-muted"></i> {{$booking->viajero->full_name}}</h4>
                                        </div>

                                        <a target='_blank' href="{{ route('area.comprar.booking', $booking->id) }}" class="btn btn-success">CONTINUAR</a>

                                    </div>

                                </div>

                            </li>

                        @endforeach
                        </ul>
                    @else
                        <p>{!! trans('area.nobookings') !!}</p>
                    @endif
                </div>
                <?php $y++; ?>
            @endforeach


            <div role="tabpanel" class="tab-pane fade in @if($y == 1) active @endif " id="{{$y}}">
                @if($usuario->tutor->bookings_area_pendientes->count()>0)
                    <ul class='area-bookings'>
                    @foreach($usuario->tutor->bookings_area_pendientes as $booking )
                        <li>
                            <div class="portlet light bordered info">

                                <div class="portlet-title tabbable-line">
                                    <div class="portlet-photo">
                                        @if(is_file(public_path() ."/assets/uploads/course/" . $booking->curso->course_images . "/" .$booking->curso->image_portada))
                                            <div class="booking-foto" style="background-image: url('/assets/uploads/course/{{$booking->curso->course_images}}/{{$booking->curso->image_portada}}');"></div>
                                        @elseif(is_file(public_path() ."/assets/uploads/center/" . $booking->curso->centro->center_images . "/" .$booking->curso->centro->center_image_portada))
                                            <div class="booking-foto" style="background-image: url('/assets/uploads/center/{{$booking->curso->centro->center_images}}/{{$booking->curso->centro->center_image_portada}}');"></div>
                                        @elseif(!is_file(public_path() ."/assets/uploads/course/" . $booking->curso->course_images . "/" .$booking->curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $booking->curso->centro->center_images . "/" .$booking->curso->centro->center_image_portada))
                                        @endif
                                    </div>

                                    <div class="caption font-green-sharp">
                                        {{-- BID:{{$booking->id}} --}}
                                        {!! ConfigHelper::getTipoTransporteIcono($booking->vuelo ? $booking->vuelo->transporte : 0) !!}
                                        <span class="caption-subject bold uppercase">
                                            {{ $booking->programa}}
                                        </span>
                                        <br />
                                        <span class="caption-helper separator"> | </span>
                                        <span class="caption-helper out"><i class="fa fa-arrow-circle-right fa-lg text-success"></i> {!! trans('area.finicio') !!}: {{$booking->curso_start_date}}</span>
                                        <span class="caption-helper in"><i class="fa fa-arrow-circle-left fa-lg text-danger"></i> {!! trans('area.ffin') !!}: {{$booking->curso_end_date}}</span>
                                        {{-- <h4 class="text-muted"><i class="fa fa-male fa-fw text-muted"></i> {{$booking->viajero->full_name}}</h4> --}}
                                    </div>

                                    <a target='_blank' href="{{ route('area.comprar.booking', $booking->id) }}" class="btn btn-success">CONTINUAR</a>

                                </div>

                            </div>

                        </li>

                    @endforeach
                    </ul>
                @else
                    <p>{!! trans('area.nobookings') !!}</p>
                @endif
            </div>


        </div>
        
    @endif

@stop