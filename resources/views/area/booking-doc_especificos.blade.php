<div class="row">
    <div class="col-md-12">
        <h4 class="text-success">{!! trans('area.documentos_especificos') !!}</h4>

        <table class="table table-bordered table-hover">
            <?php
                $dirb = "/assets/uploads/doc_especificos/";
            ?>

            <thead>
                <th>{{ trans('area.doc_especifico.nombre') }}</th>
                <th>{{ trans('area.doc_especifico.notas') }}</th>
                <th>{{ trans('area.doc_especifico.doc') }}</th>
                <th>{{ trans('area.doc_especifico.doc2') }}</th>
                <th>{{ trans('area.doc_especifico.status') }}</th>
            </thead>

            @foreach($booking->documentos_especificos as $doc)

                <?php
                    $dir = $dirb . $doc->modelo ."/". $doc->modelo_id . "/". $doc->idioma . "/";
                ?>

                <tr>
                <td class="col-sm-3">
                    {{$doc->name}}
                </td>
                <td class="col-sm-6">
                    {!! $doc->notas !!}
                </td>
                <td>
                    @if($doc->doc)
                        {!! ConfigHelper::iframe($doc->doc, public_path($dir), $dir, true); !!}
                    @endif 
                </td>
                <td>

                    <?php
                        $doc_id = $doc->idioma ? $doc->doc_id : $doc->id;
                        $adjunto = $booking->archivos->where('doc_especifico_id', $doc_id)->first();
                    ?>

                    @if(!$adjunto)

                        {!! Form::open(array('method' => 'POST', 'files'=> true, 'url' => route('area.booking.adjuntar.post', $booking->id), 'role' => 'form', 'class' => '')) !!}

                            {!! Form::hidden('doc_id', $doc_id ) !!}
                            {!! Form::hidden('doc_idioma', $doc->idioma) !!}

                            @include('includes.form_input_file', [ 'campo'=> 'doc_adjunto', 'texto'=> 'Adjuntar', 'ficha'=> null, 'noborrar'=> true])

                            {!! Form::submit(trans('area.enviar'), array('name'=> 'submit_adjunto', 'class' => 'btn btn-success')) !!}

                        {!! Form::close() !!}

                    @else
                        <?php 
                            $path = storage_path("files/viajeros/". $ficha->viajero_id ."/");
                            $folder = "/files/viajeros/". $ficha->viajero_id ."/";
                        ?>
                        {!! ConfigHelper::iframe($adjunto->doc, $path, $folder, true); !!}
                    @endif

                </td>
                <td>
                    @if($adjunto)
                        {{ trans("area.doc_especifico.status_". $adjunto->doc_especifico_status ) }}
                    @endif
                </td>
                </tr>
            @endforeach
        </table>

    </div>

</div>