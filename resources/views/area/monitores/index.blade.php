@foreach($monitor->relaciones as $relacion)
    <h4>{{$relacion->modelo_name}}</h4>
    <?php $b = $monitor->getBookings(); ?>
    <table class="table table-bordered table-striped table-hover">
        @if($relacion->modelo == 'Centro')
            <?php $barray = Array(); ?>
            @foreach($monitor->getBookingsByCentro($relacion->modelo_id) as $b)
                <?php
                    $barray[ConfigHelper::limpiarAcentos($b->viajero->full_name)] = $b;
                ?>
            @endforeach
            <?php ksort($barray); ?>
                @foreach($barray as $booking)
                <tr>
                    <td class="col-sm-6">
                        <h5>{{$booking->viajero->full_name}}</h5>
                    </td>
                    <td class="col-sm-6">
                        @if($booking->area_cuestionarios->count())
                            @if(!Session::get('vcn.manage.area.home'))
                                @foreach($booking->area_cuestionarios as $form)
                                    @if($form->area)

                                        <?php
                                        $ac_estado = $booking->getAreaCuestionarioEstado($form->id);
                                        $actxt = ConfigHelper::areaFormEstado($ac_estado);

                                        if($form->destino == 'viajero'){

                                        $tuser = "viajero_id";
                                        $tuser_id = $booking->viajero->id;

                                        $r = VCN\Models\System\CuestionarioRespuesta::where('booking_id',$booking->id)->where($tuser, $tuser_id)->where('cuestionario_id',$form->id)->orderBy('updated_at', 'desc')->first();

                                        ?>
                                        <div class="row">
                                                <div class="col-xs-8">
                                                    @if($ac_estado == 1 && $form->test == 0 && $r)
                                                        <a href="{{ route('area.forms.resultado',[$form->id,$booking->id,$r->id]) }}">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</a>
                                                    @else
                                                        <a href="{{ route('area.forms', [$form->id,$booking->id]) }}">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</a>
                                                    @endif
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    @if($ac_estado == 0)
                                                        <span class="text-danger">{!! trans("area.".$actxt) !!}</span>
                                                    @elseif($ac_estado == 1)
                                                        <span class="text-warning">{!! trans("area.".$actxt) !!}</span>
                                                    @elseif($ac_estado == 2)
                                                        <span class="text-success">{!! trans("area.".$actxt) !!}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="cuestionario"></div>
                                        <?php } ?>
                                        @endif
                                    @endforeach
                                @else
                                    <i>Sólo para usuarios</i>
                                @endif

                        @else
                            - SIN Cuestionario/s -
                        @endif
                    </td>
                </tr>
            @endforeach

        @endif
        @if($relacion->modelo == 'Curso')
            <?php $barray = Array(); ?>
                @foreach($monitor->getBookingsByCurso($relacion->modelo_id)  as $b)
                    <?php
                    $barray[ConfigHelper::limpiarAcentos($b->viajero->full_name)] = $b;
                    ?>
                @endforeach
                <?php ksort($barray); ?>
                @foreach($barray as $booking)
                    <tr>
                        <td class="col-sm-6">
                            <h4>{{$booking->viajero->full_name}}</h4>
                        </td>
                        <td class="col-sm-6">
                            @if($booking->area_cuestionarios->count())
                                @if(!Session::get('vcn.manage.area.home'))
                                    @foreach($booking->area_cuestionarios as $form)
                                    
                                        @if($form->area)

                                            <?php
                                            $ac_estado = $booking->getAreaCuestionarioEstado($form->id);
                                            $actxt = ConfigHelper::areaFormEstado($ac_estado);

                                            if($form->destino == 'viajero'){

                                            $tuser = "viajero_id";
                                            $tuser_id = $booking->viajero->id;

                                            $r = VCN\Models\System\CuestionarioRespuesta::where('booking_id',$booking->id)->where($tuser, $tuser_id)->where('cuestionario_id',$form->id)->orderBy('updated_at', 'desc')->first();

                                            ?>
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    @if($ac_estado == 1 && $form->test == 0 && $r)
                                                        <a href="{{ route('area.forms.resultado',[$form->id,$booking->id,$r->id]) }}">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</a>
                                                    @else
                                                        <a href="{{ route('area.forms', [$form->id,$booking->id]) }}">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</a>
                                                    @endif
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    @if($ac_estado == 0)
                                                        <span class="text-danger">{!! trans("area.".$actxt) !!}</span>
                                                    @elseif($ac_estado == 1)
                                                        <span class="text-warning">{!! trans("area.".$actxt) !!}</span>
                                                    @elseif($ac_estado == 2)
                                                        <span class="text-success">{!! trans("area.".$actxt) !!}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="cuestionario"></div>
                                            <?php } ?>
                                        @endif
                                    @endforeach
                                @else
                                    <i>Sólo para usuarios</i>
                                @endif

                            @else
                                - SIN Cuestionario/s -
                            @endif
                        </td>
                    </tr>
            @endforeach
        @endif

        @if($relacion->modelo == 'Cerrada')
                <?php $barray = Array(); ?>
                @foreach($monitor->getBookingsByCerrada($relacion->modelo_id) as $b)
                    <?php
                    $barray[ConfigHelper::limpiarAcentos($b->viajero->full_name)] = $b;
                    ?>
                @endforeach
                <?php ksort($barray); ?>
                @foreach($barray as $booking)
                    <tr>
                        <td class="col-sm-6">
                            <h4>{{$booking->viajero->full_name}}</h4>
                        </td>
                        <td class="col-sm-6">
                            @if($booking->area_cuestionarios->count())
                                @if(!Session::get('vcn.manage.area.home'))
                                    @foreach($booking->area_cuestionarios as $form)
                                        @if($form->area)

                                            <?php
                                            $ac_estado = $booking->getAreaCuestionarioEstado($form->id);
                                            $actxt = ConfigHelper::areaFormEstado($ac_estado);

                                            if($form->destino == 'viajero'){

                                            $tuser = "viajero_id";
                                            $tuser_id = $booking->viajero->id;

                                            $r = VCN\Models\System\CuestionarioRespuesta::where('booking_id',$booking->id)->where($tuser, $tuser_id)->where('cuestionario_id',$form->id)->orderBy('updated_at', 'desc')->first();

                                            ?>
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    @if($ac_estado == 1 && $form->test == 0 && $r)
                                                        <a href="{{ route('area.forms.resultado',[$form->id,$booking->id,$r->id]) }}">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</a>
                                                    @else
                                                        <a href="{{ route('area.forms', [$form->id,$booking->id]) }}">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</a>
                                                    @endif
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    @if($ac_estado == 0)
                                                        <span class="text-danger">{!! trans("area.".$actxt) !!}</span>
                                                    @elseif($ac_estado == 1)
                                                        <span class="text-warning">{!! trans("area.".$actxt) !!}</span>
                                                    @elseif($ac_estado == 2)
                                                        <span class="text-success">{!! trans("area.".$actxt) !!}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="cuestionario"></div>
                                            <?php } ?>
                                        @endif
                                    @endforeach
                                @else
                                    <i>Sólo para usuarios</i>
                                @endif

                            @else
                                - SIN Cuestionario/s -
                            @endif
                        </td>
                    </tr>
            @endforeach
        @endif

        @if($relacion->modelo == 'Abierta')
                <?php $barray = Array(); ?>
                @foreach($monitor->getBookingsByAbierta($relacion->modelo_id) as $b)
                    <?php
                    $barray[ConfigHelper::limpiarAcentos($b->viajero->full_name)] = $b;
                    ?>
                @endforeach
                <?php ksort($barray); ?>
                @foreach($barray as $booking)
                    <tr>
                        <td class="col-sm-6">
                            <h4>{{$booking->viajero->full_name}}</h4>
                        </td>
                        <td class="col-sm-6">
                            @if($booking->area_cuestionarios->count())
                                @if(!Session::get('vcn.manage.area.home'))
                                    @foreach($booking->area_cuestionarios as $form)
                                        @if($form->area)

                                            <?php
                                            $ac_estado = $booking->getAreaCuestionarioEstado($form->id);
                                            $actxt = ConfigHelper::areaFormEstado($ac_estado);

                                            if($form->destino == 'viajero'){

                                            $tuser = "viajero_id";
                                            $tuser_id = $booking->viajero->id;

                                            $r = VCN\Models\System\CuestionarioRespuesta::where('booking_id',$booking->id)->where($tuser, $tuser_id)->where('cuestionario_id',$form->id)->orderBy('updated_at', 'desc')->first();

                                            ?>
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    @if($ac_estado == 1 && $form->test == 0)
                                                        <a href="{{ route('area.forms.resultado',[$form->id,$booking->id,$r->id]) }}">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</a>
                                                    @else
                                                        <a href="{{ route('area.forms', [$form->id,$booking->id]) }}">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</a>
                                                    @endif
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    @if($ac_estado == 0)
                                                        <span class="text-danger">{!! trans("area.".$actxt) !!}</span>
                                                    @elseif($ac_estado == 1)
                                                        <span class="text-warning">{!! trans("area.".$actxt) !!}</span>
                                                    @elseif($ac_estado == 2)
                                                        <span class="text-success">{!! trans("area.".$actxt) !!}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="cuestionario"></div>
                                            <?php } ?>
                                        @endif
                                    @endforeach
                                @else
                                    <i>Sólo para usuarios</i>
                                @endif

                            @else
                                - SIN Cuestionario/s -
                            @endif
                        </td>
                    </tr>
            @endforeach
        @endif

        @if($relacion->modelo == 'ConvocatoriaMulti')
                <?php $barray = Array(); ?>
                @foreach($monitor->getBookingsByMulti($relacion->modelo_id) as $b)
                    <?php
                    $barray[ConfigHelper::limpiarAcentos($b->viajero->full_name)] = $b;
                    ?>
                @endforeach
                <?php ksort($barray); ?>
                @foreach($barray as $booking)
                    <tr>
                        <td class="col-sm-6">
                            <h4>{{$booking->viajero->full_name}}</h4>
                        </td>
                        <td class="col-sm-6">
                            @if($booking->area_cuestionarios->count())
                                @if(!Session::get('vcn.manage.area.home'))
                                    @foreach($booking->area_cuestionarios as $form)
                                        @if($form->area)

                                            <?php
                                            $ac_estado = $booking->getAreaCuestionarioEstado($form->id);
                                            $actxt = ConfigHelper::areaFormEstado($ac_estado);

                                            if($form->destino == 'viajero'){

                                            $tuser = "viajero_id";
                                            $tuser_id = $booking->viajero->id;

                                            $r = VCN\Models\System\CuestionarioRespuesta::where('booking_id',$booking->id)->where($tuser, $tuser_id)->where('cuestionario_id',$form->id)->orderBy('updated_at', 'desc')->first();

                                            ?>
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    @if($ac_estado == 1 && $form->test == 0 && $r)
                                                        <a href="{{ route('area.forms.resultado',[$form->id,$booking->id,$r->id]) }}">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</a>
                                                    @else
                                                        <a href="{{ route('area.forms', [$form->id,$booking->id]) }}">{!!Traductor::getWeb(App::getLocale(), 'Cuestionario', 'name', $form->id, $form->name)!!}</a>
                                                    @endif
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    @if($ac_estado == 0)
                                                        <span class="text-danger">{!! trans("area.".$actxt) !!}</span>
                                                    @elseif($ac_estado == 1)
                                                        <span class="text-warning">{!! trans("area.".$actxt) !!}</span>
                                                    @elseif($ac_estado == 2)
                                                        <span class="text-success">{!! trans("area.".$actxt) !!}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="cuestionario"></div>
                                            <?php } ?>
                                        @endif
                                    @endforeach
                                @else
                                    <i>Sólo para usuarios</i>
                                @endif

                            @else
                                - SIN Cuestionario/s -
                            @endif
                        </td>
                    </tr>
            @endforeach
        @endif

    </table>
    <hr>
@endforeach

