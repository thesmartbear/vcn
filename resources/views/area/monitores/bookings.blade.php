@extends('layouts.area')


@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop


@section('content')

@foreach($monitor->getCursos() as $curso)

    <h3>{{$curso->name}}</h3>

    <?php
        $tipo = "Cerrada";
        if($curso->es_convocatoria_abierta)
        {
            $tipo = "Abierta";
        }
        if($curso->es_convocatoria_abierta)
        {
            $tipo = "ConvocatoriaMulti";
        } 
    ?>

    @foreach($curso->convocatorias as $convo)

        <?php
            $bookings = collect();

            if($tipo == "Cerrada")
            {
                $bookings = $monitor->getBookingsByCerrada($convo->id);
            }
            elseif($tipo == "Abierta")
            {
                $bookings = $monitor->getBookingsByAbierta($convo->id);
            }
            elseif($tipo == "ConvocatoriaMulti")
            {
                $bookings = $monitor->getBookingsByMulti($convo->id);
            }
        ?>

        @if($bookings->count())

            <table class="table table-bordered table-striped table-hover">
                <caption>{{$convo->name}}</caption>

                <thead>
                    <tr>
                        <td width='10%'>Booking</td>
                        <td width='70%'>Viajero</td>
                        <td>PDF Booking</td>
                        <td>PDF Info</td>
                    </tr>
                </thead>

                <tbody>
                    @foreach($bookings as $booking)
                        <tr>
                            <td>{{$booking->id}}</td>
                            <td>{{$booking->viajero->full_name}}</td>
                            <td>
                                {!! $booking->pdf_enlace !!}
                            </td>
                            <td>
                                {!! $booking->pdf_monitor_enlace !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                
            </table>

            <hr>

        @endif

    @endforeach

@endforeach

@stop