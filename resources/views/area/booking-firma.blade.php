@extends('layouts.area')

@section('breadcrumb')
{!! Breadcrumbs::render('area.index') !!}
@stop


@section('extra_head')
<style type="text/css" media="screen">
    #signature_container {
        color:darkblue;
        /*background-color:lightgrey;*/
        padding: 8px;
        /*position: absolute;
        top: 100px;
        right: 200px;
        z-index: 9999;*/
    }

    .signature_box {
        padding: 0 0 0 0;
        margin: .2em 0 0 .3em;
        border: 2px dotted #000;
        width: 340px;
        height: 240px;
    }
    .signature_data {
        display: none;
        /*height: 120px;
        width: 340px;
        border: 1px solid #ccc;*/
        /*padding: 20px;*/
    }

    .signature_iframe {
        /*width: 800px;*/
        min-height: 600px;
        margin-bottom: 20px;
    }

    @media (max-width: 640px) {
        .signature_box {
            width: 97%;
        }
    }
</style>
@stop


@section('content')

<?php
$firmar = null;
$fichero = null;
if($pdf)
{
    $dir = "/files/firmar/" . $booking->viajero_id . "/";

    //$fichero = url("/") . $dir . $pdf->doc;
    //$fichero = "https://docs.google.com/gview?url=$fichero"."&embedded=true";

    //if( App::environment('local') )
    {
        $fichero = $dir . $pdf->doc;
    }
    //echo $fichero;

    if($booking->firmante)
    {
        $firmar = $booking->getArchivoFirmaPendiente();
    }
}
?>

@if($firmar)
<div class="row">
    <div class="col-md-12">
        <div class="note note-warning">
            <h4 class="block">@lang('area.booking.atencion')</h4>
            <p>
                <strong>@lang('area.booking.pago_firma')</strong>
            </p>
        </div>
    </div>
</div>
<hr>
@endif

{!! Form::open( array('id'=> 'frmFirmar', 'method'=> 'post', 'url' => route('area.booking.firmar.post', $booking->id)) ) !!}

    <div class="form-group row">

        <div class="col-md-5">
          <div id='signature_container'>
              <div id="signature" class="signature_box"></div>
          </div>
        </div>

        <div class="col-md-3 col-xs-8">
            <button id="sign_reset" class="btn btn-warning" disabled>@lang('area.reset')</button>
        </div>

        <div class="col-md-2 col-xs-4">
            <button id="sign_ok" class="btn btn-success pull-right" disabled>@lang('area.booking.btn-firmar')</button>

            {{-- {!! Form::submit(trans('area.booking.btn-firmar'), array('id'=> 'btn-firmar','class' => 'btn btn-success pull-right', 'disabled'=> true)) !!} --}}
        </div>

    </div>

    <div class="form-group row">
        <iframe class="signature_iframe col-md-10" src="{{$fichero}}" frameborder="0"> </iframe>
    </div>

    <div id="signature_img" class='signature_data'></div>

    {!! Form::hidden('doc_id', ($pdf?$pdf->id:0)) !!}
    {!! Form::hidden('signature_b64', null, array('id'=> 'signature_b64')) !!}

{!! Form::close() !!}

<!--[if lt IE 9]>
<script type="text/javascript" src="libs/flashcanvas.js"></script>
<![endif]-->
{!! Html::script('assets/plugins/jSignature/jSignature.min.js') !!}
<div id="signature"></div>
<script>
    $(document).ready(function() {

        {{--
        @if( isset($booking) && $booking->firmas && isset($booking->firmas['viajero']) )

          var i = new Image()
          i.src = "{{$booking->firmas['viajero']}}"
          $(i).appendTo($("#signature_img"))

        @endif
        --}}

        var options = {
            // defaultAction: 'drawIt',
            // penColour: '#2c3e50',
            // lineWidth: 0,
            'decor-color': 'transparent',
            'height': '240px',
            'width': '340px'
        }

        $sigdiv = $("#signature");
        $sigdiv.jSignature(options);
        $sigdiv.jSignature("reset");

        $sigdiv.bind('change', function(e){
            $('#sign_reset').prop('disabled', false);
            $('#sign_ok').prop('disabled', false);
        });

        $('#sign_reset').click( function(e) {
            e.preventDefault();
            $sigdiv.jSignature("reset");

            $(this).prop('disabled', true);
            $("#signature_img").html("");
            $('#sign_ok').prop('disabled', true);
        });

        $('#sign_ok').click( function(e) {
            e.preventDefault();
            
            var datapair = $sigdiv.jSignature("getData", 'image')
            var i = new Image()
            var b64 = "data:" + datapair[0] + "," + datapair[1];
            // console.log(b64)

            i.src = b64
            $(i).appendTo($("#signature_img"))
            $('#signature_b64').val(b64);

            $('#frmFirmar').submit(); 
        });
    })
</script>

@stop