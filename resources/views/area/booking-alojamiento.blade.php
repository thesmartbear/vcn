<div class="row">
    <div class="col-md-12">
        <h4 class="text-success">{!! trans('area.alojamiento') !!}: {{$ficha->alojamiento?$ficha->alojamiento->name:"-"}}</h4>

        <table class="table">
            <thead>
                <tr class="thead">
                    <td>{!! trans('area.fechas') !!}</td>

                    @if($ficha->alojamiento && $ficha->alojamiento->duracion_name)
                    <td>{!! trans('area.duracion') !!}</td>
                    @endif

                    @if($ficha->accommodation_total_amount>0)
                        <td align="right">{!! trans('area.precio') !!}</td>
                    @endif
                </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    {{$ficha->alojamiento?$ficha->alojamiento->name:"-"}} ::
                    {!! trans('area.del') !!} {{Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y')}}
                    {!! trans('area.al') !!} {{Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y')}}
                    <span id="booking-alojamiento-alerta" class='booking-alerta'>{{isset($ficha->alertas['alojamiento'])?$ficha->alertas['alojamiento']:""}}</span>
                    <span id="booking-alojamiento-precio-detalle" class='booking-precio'>
                        @if($ficha->alojamiento_precio_extra>0)
                            {{ trans('area.precio')." base: ". ConfigHelper::parseMoneda( ($ficha->accommodation_total_amount), $ficha->curso_moneda)}}
                            {{" + Extra temporada: ". ConfigHelper::parseMoneda( ($ficha->alojamiento_precio_extra), $ficha->curso_moneda)}}
                        @endif
                    </span>
                </td>

                @if($ficha->alojamiento)
                <?php $u = $ficha->accommodation_weeks; ?>
                <td>
                    @if($ficha->curso->es_convocatoria_cerrada)
                        {{$ficha->accommodation_weeks}} {{$ficha->convocatoria ? $ficha->convocatoria->getDuracionName($u) : ""}}
                    @else
                        {{$ficha->accommodation_weeks}} {{$ficha->alojamiento?($ficha->alojamiento->duracion_name?:$ficha->convocatoria->getDuracionName($u)):$ficha->convocatoria->getDuracionName($u)}}
                    @endif
                </td>
                @endif

                @if($ficha->accommodation_total_amount>0)
                <td align='right'>
                    {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
                </td>
                @endif
            </tr>
            </tbody>
        </table>

        <hr>

        @if( $ficha->familias_area->count()>0 )
        <h4 class="text-warning">{!! trans('area.Familias') !!}:</h4>

        <table class="table">
            <thead>
                <tr class="thead">
                    <td>Familia</td>
                    <td>{!! trans('area.fechas') !!}</td>
                    <td width="20%"></td>
                    <td width="20%"></td>
                </tr>
            </thead>
            <tbody>
            @foreach($ficha->familias_area as $bfam)
                <tr>
                    <td><a data-label="{!! trans('area.verdetalles') !!}" href="{{route('area.booking.familia',$bfam->id)}}">{{$bfam->familia->name}}</a></td>
                    <td>
                        {!! trans('area.del') !!} {{$bfam->desde?$bfam->desde->format('d/m/Y'):"-"}}
                        {!! trans('area.al') !!} {{$bfam->hasta?$bfam->hasta->format('d/m/Y'):"-"}}
                    </td>
                    <td><a class="btn btn-block btn-primary" href="{{route('area.booking.familia',$bfam->id)}}"><i class="fa fa-eye"></i> {!! trans('area.verdetalles') !!}</a></td>
                    <td><a class="btn btn-block btn-success" href="{{route('area.booking.familia-pdf',$bfam->id)}}"><i class="fa fa-download"></i> {!! trans('area.descargar') !!}</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @endif

    </div>
</div>