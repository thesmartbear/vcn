@extends('layouts.area')


@section('breadcrumb')
    {!! Breadcrumbs::render('area.booking.pocketguide',$booking) !!}
@stop


@section('content')

    @include('manage.cursos.pocketguide', ['curso'=> $ficha, 'lang'=> $idioma, 'pdf' => false])

@stop