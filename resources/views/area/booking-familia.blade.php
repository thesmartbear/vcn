@extends('layouts.area')


@section('breadcrumb')
    {!! Breadcrumbs::render('area.booking.familia',$booking) !!}
@stop


@section('content')
    <div class="row">
        <div class="col-md-10">
            <div class="caption font-green-sharp">
                {!! ConfigHelper::getTipoTransporteIcono($booking->vuelo ? $booking->vuelo->transporte : 0) !!}
                <span class="caption-subject bold uppercase">
                    {{ $booking->programa}}
                </span>
                <span class="caption-helper separator"> | </span>
                <span class="caption-helper out"><i class="fa fa-arrow-circle-right fa-lg text-success"></i> {!! trans('area.finicio') !!}: {{$booking->curso_start_date}}</span>
                <span class="caption-helper in"><i class="fa fa-arrow-circle-left fa-lg text-danger"></i> {!! trans('area.ffin') !!}: {{$booking->curso_end_date}}</span>
                <h4 class="text-muted"><i class="fa fa-male fa-fw text-muted"></i> {{$booking->viajero->full_name}}</h4>
            </div>
        </div>
        <div class="col-md-2">
            <?php $fam = $booking->familias_area->where('booking_id',$booking->id)->first(); ?>
            <a class="btn btn-block btn-success" href="{{route('area.booking.familia-pdf', $fam->id)}}"><i class="fa fa-download"></i> {!! trans('area.descargar') !!}</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <hr>
            <h3 class="page-title font-green-sharp">Familia {{$ficha->name}} <small>[{!! trans('area.alojamiento') !!}: {{$booking->alojamiento?$booking->alojamiento->name:"-"}}]</small></h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.datoscontacto')}}</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="well">
                @if($ficha->direccion)
                    <strong>{{trans('area.direccion')}}:</strong> {{$ficha->direccion}}<br />
                @endif

                @if($ficha->cp)
                    <strong>{{trans('area.cp')}}:</strong> {{$ficha->cp}}<br />
                @endif

                @if($ficha->poblacion)
                    <strong>{{trans('area.poblacion')}}:</strong> {{$ficha->poblacion}}<br />
                @endif

                @if($ficha->pais)
                    <strong>{{trans('area.pais')}}:</strong> {{$ficha->pais->name}}
                @endif

                <hr>

                @if($ficha->telefono)
                    <strong>{{trans('area.telefono')}}:</strong> {{$ficha->telefono}}<br />
                @endif

                @if($ficha->email)
                    <strong>{{trans('area.email')}}:</strong> {{$ficha->email}}<br />
                @endif

                @if($ficha->skype)
                    <strong><i class='fa fa-skype'></i>:</strong> {{$ficha->skype}}<br />
                @endif

                @if($ficha->rrss)
                    <strong>{{trans('area.redessociales')}}:</strong> {{$ficha->rrss}}<br />
                @endif


                @if($ficha->distancia)
                    <strong>{{trans('area.distanciacentro')}}:</strong> {{$ficha->distancia}}<br />
                @endif

                @if($ficha->transporte)
                    <strong>{{trans('area.transportepublico')}}:</strong> {{$ficha->transporte}}<br />
                @endif

                @if($ficha->animales)
                    <strong>{{trans('area.animalesdomesticos')}}:</strong> {{$ficha->animales}}<br />
                @endif

                @if($ficha->aficiones)
                    <strong>{{trans('area.aficionesfamiliares')}}:</strong> {{$ficha->aficiones}}<br />
                @endif

                @if($ficha->notas)
                    <strong>{{trans('area.comentarios')}}:</strong> {{$ficha->notas}}
                @endif

            </div>
        </div>

        @if(strip_tags($ficha->direccion) != '')
            <div class="col-md-6">
                <iframe
                        width="100%"
                        height="300"
                        frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCNPWo91Kd3D656Z5R2U6K1vFlx65KxpE8&q={{$ficha->direccion_completa}}&language={{App::getLocale()}}&zoom=13" allowfullscreen>
                </iframe>
            </div>
        @endif
    </div>


    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.adultos')}}</h4>
        </div>

        @if(is_array($ficha->adultos))
            @foreach($ficha->adultos['nombre'] as $kform=>$vform)
                <div class="row">
                    <div class="col-sm-6">
                        @if(isset($ficha->adultos['foto']))
                            @if(isset($ficha->adultos['foto'][$kform]))
                                <div class="col-sm-4 col-xs-12">
                                    <img class="img-responsive img-thumbnail" src="{{$ficha->adultos['foto'][$kform]}}" />
                                </div>
                            @endif
                        @endif

                        <div class="col-sm-8">
                            <strong>{{trans('area.nombre')}}:</strong> {{$ficha->adultos['nombre'][$kform]}}<br />

                            @if($ficha->adultos['edad'][$kform])
                                <strong>{{trans('area.edad')}}:</strong> {{$ficha->adultos['edad'][$kform]}}<br />
                            @endif

                            @if(!$ficha->adultos['edades'][$kform] && $ficha->adultos['edades'][$kform])
                                <strong>{{trans('area.edad')}}:</strong> {{$ficha->adultos['edades'][$kform]}}<br />
                            @endif

                            @if(isset($ficha->adultos['sexo']) && $ficha->adultos['sexo'][$kform])
                                <strong>{{trans('area.sexo')}}:</strong> {{$ficha->adultos['sexo'][$kform]==1?trans('area.hombre'):trans('area.mujer')}}<br />
                            @endif

                            @if($ficha->adultos['ocupacion'][$kform])
                                <strong>{{trans('area.ocupacion')}}:</strong> {{$ficha->adultos['ocupacion'][$kform]}}<br />
                            @endif

                            @if($ficha->adultos['aficiones'][$kform])
                                <strong>{{trans('area.aficiones')}}:</strong> {{$ficha->adultos['aficiones'][$kform]}}<br />
                            @endif

                            @if($ficha->adultos['movil'][$kform])
                                <strong>{{trans('area.movil')}}:</strong> {{$ficha->adultos['movil'][$kform]}}<br />
                            @endif

                            @if($ficha->adultos['email'][$kform])
                                <strong>{{trans('area.email')}}:</strong> {{$ficha->adultos['email'][$kform]}}<br />
                            @endif
                        </div>

                    </div>
                </div>
            @endforeach
        @endif
    </div>

    @if(is_array($ficha->hijos))
    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.hijosencasa')}}</h4>
        </div>
            @foreach($ficha->hijos['nombre'] as $kform=>$vform)

                <div class="col-md-3">
                    <strong>{{trans('area.nombre')}}:</strong> {{$ficha->hijos['nombre'][$kform]}}<br />

                    @if($ficha->hijos['fechanac'][$kform])
                        <strong>{{trans('area.fnacimiento')}}:</strong> {{$ficha->hijos['fechanac'][$kform]}}<br />
                    @endif

                    @if(isset($ficha->hijos['edad']))
                    <strong>{{trans('area.edad')}}:</strong>{{$ficha->hijos['edad'][$kform]?$ficha->hijos['edad'][$kform]:"-"}}<br />
                    @endif

                    @if(isset($ficha->hijos['sexo']))
                    <strong>{{trans('area.sexo')}}:</strong> {{$ficha->hijos['sexo'][$kform]?trans('area.sexo_'.$ficha->hijos['sexo'][$kform]):""}}
                    <br />
                    @endif

                    @if($ficha->hijos['aficiones'][$kform])
                        <strong>{{trans('area.aficiones')}}:</strong> {{$ficha->hijos['aficiones'][$kform]}}
                    @endif

                </div>
            @endforeach
    </div>
    @endif

    @if(is_array($ficha->hijos_fuera))
    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.hijosfuera')}}</h4>
        </div>
            @foreach($ficha->hijos_fuera['nombre'] as $kform=>$vform)
                <div class="col-md-3">
                    <strong>{{trans('area.nombre')}}:</strong> {{$ficha->hijos_fuera['nombre'][$kform]}}<br />
                    <strong>{{trans('area.edad')}}:</strong> {{$ficha->hijos_fuera['edad'][$kform]?$ficha->hijos_fuera['edad'][$kform]:"-"}}<br />
                    <strong>{{trans('area.sexo')}}:</strong> {{$ficha->hijos_fuera['sexo'][$kform]?trans('area.sexo_'.$ficha->hijos_fuera['sexo'][$kform])
                        :""}}
                </div>
            @endforeach
    </div>
    @endif

    @if(is_array($ficha->familia))
    <div class="row">
        <div class="col-md-12">
            <h4 class="form-section text-primary">{{trans('area.otrosmiembros')}}</h4>
        </div>


            @foreach($ficha->familia['nombre'] as $kform=>$vform)
                <div class="col-md-3">
                    <strong>{{trans('area.nombre')}}:</strong> {{$ficha->familia['nombre'][$kform]}}<br />

                    @if($ficha->familia['parentesco'][$kform])
                        <strong>{{trans('area.parentesco')}}:</strong> {{$ficha->familia['parentesco'][$kform]}}<br />
                    @endif

                    @if($ficha->familia['edad'][$kform])
                        <strong>{{trans('area.edad')}}:</strong> {{$ficha->familia['edad'][$kform]}}<br />
                    @endif
                </div>
            @endforeach

    </div>
    @endif


    @if($ficha->habitacion_compartida == 1)
        <div class="row">
            <div class="col-md-12">
                <h4 class="form-section text-primary">{{trans('area.habitacion')}}</h4>
            </div>
            <div class="col-md-12">
                <p><strong>{{trans('area.tipohabitacion')}}:</strong> {{$ficha->habitacion_compartida?trans('area.compartida'):trans('area.individual')}}</p>
                @if($ficha->habitacion_compartida_notas)
                    <p><strong>{{trans('area.notas')}}:</strong> {{$ficha->habitacion_compartida_notas}}</p>
                @endif
            </div>
        </div>
    @endif


    <?php
    $folder = "/assets/uploads/familia/" . $ficha->id;
    $path = public_path() . $folder;
    $fotos = '';

    if (is_dir($path)) {
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

            $file = $path . '/' . $result;

            if (is_file($file)) {
                $fotos .= '
                            <div class="col-md-3">
                                <a rel="group" href="' . $folder . '/' . $result . '"><div style="position: relative; overflow: hidden; pading-bottom: 100%;"><img style="margin-bottom: 10px;" class="img-responsive full-width img-thumbnail" src="' . $folder . '/thumb/' . $result . '" alt=""></div></a>
                            </div>';

            }
        }
    }
    ?>
    @if ($fotos != '')
        <div class="row">
            <div class="col-md-12">
                <h4 class="form-section text-primary">{{trans('area.fotos')}}</h4>
                <div id="fotos">
                    {!! $fotos !!}
                </div>
            </div>
        </div>
    @endif

    <div class="clear"></div>

    <?php
        $origen = strip_tags($ficha->direccion_completa);
        $destino = strip_tags($ficha->centro->address);
        if($booking->schools->first())
        {
            $destino = strip_tags($booking->schools_area->first()->school->direccion_completa);
        }
    ?>
    @if(strip_tags($ficha->direccion) != '' && strip_tags($destino) != '')
        <div class="row">
            <div class="col-md-12">
                <h4 class="form-section text-primary">{{trans('area.trayectocentro')}}</h4>
                <iframe
                        width="100%"
                        height="350"
                        frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyCNPWo91Kd3D656Z5R2U6K1vFlx65KxpE8&origin={{$origen}}&destination={{$destino}}&mode=transit" allowfullscreen>
                </iframe>
            </div>
        </div>
    @endif

@stop

@section('extra_footer')
    <script>
        $('#fotos').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image',
            gallery: {
                enabled: true
            },
            titleSrc: 'title'
        });
    </script>
@stop