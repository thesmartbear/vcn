@extends('layouts.area')


@section('breadcrumb')
    {!! Breadcrumbs::render('area.exams') !!}
@stop


@section('content')

<div class="row">
    <div class="col-md-12">
        <h4 class="text-success">{!! trans('area.Examenes') !!}</h4>
    </div>
</div>

<hr>

<div class="portlet box blue" id="area-box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-list-alt fa-2x"></i>{!! trans('area.Examenes') !!}
        </div>
    </div>
    <div class="portlet-body">

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    @if($usuario->es_tutor)
                    <th>@lang('area.Viajero')</th>
                    @endif 
                    <th>@lang('area.Examen')</th>
                    <th>@lang('area.estado')</th>
                    <th>@lang('area.Resultado')</th>
                </tr>
            </thead>
            <tbody>

                @foreach($exams as $resp)
                    
                    @php
                        $test = $resp->examen;
                        $et = $resp->status;

                        if($resp->booking_id)
                        {
                            $route = route('area.exams.ask', [$test->id, $resp->booking_id]);
                        }
                        else
                        {
                            $route = route('area.exams.ask', [$test->id, $resp->viajero_id, 'viajero']);
                        }
                    @endphp

                    <tr>
                        @if($usuario->es_tutor)
                        <td>{{$resp->viajero->full_name}}</td>
                        @endif
                        <td>
                            <a href="{{ $route }}">{{ $test->title }}</a>
                        </td>
                        <td>
                            @if($et == 1)
                                <span class="text-warning">{!! trans("area.completo") !!}</span>
                            @else
                                <span class="text-danger">{!! trans("area.pendiente") !!}</span>
                            @endif
                        </td>
                        <td>
                            {!! $resp->notas !!}
                        </td>
                    </tr>

                @endforeach

            </tbody>
        </table>

    </div>
</div>

@stop