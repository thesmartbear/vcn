@extends('layouts.area')


@section('breadcrumb')
    {!! Breadcrumbs::render('area.deseos') !!}
@stop


@section('content')

<div class="row">
    <div class="col-md-12">
        <h4 class="text-success">{!! trans('area.Mis Favoritos') !!}</h4>
    </div>
</div>

<hr>

<ul class='area-bookings'>
@foreach($user->deseos as $deseo)

    {{-- Foto portada - curso + botones: reservar, eliminar --}}

    <?php
        $fotoscentro = '';
        $fotoscentroname = array();
        $path = public_path() ."/assets/uploads/center/" . $deseo->curso->centro->center_images;
        $folder = "/assets/uploads/center/" . $deseo->curso->centro->center_images;

        if (is_dir($path)) {
            $results = scandir($path);
            foreach ($results as $result) {
                if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                $file = $path . '/' . $result;

                if (is_file($file)) {
                    $fotoscentroname[] = $result;

                }
            }
        }

        $fotoscurso = '';
        $fotoscursoname = array();
        $path = public_path() ."/assets/uploads/course/" . $deseo->curso->course_images;
        $folder = "/assets/uploads/course/" . $deseo->curso->course_images;

        if (is_dir($path)) {
            $results = scandir($path);
            foreach ($results as $result) {
                if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                $file = $path . '/' . $result;

                if (is_file($file)) {
                    $fotoscursoname[] = $result;
                }
            }
        }
    ?>

    <li>
        <div class="portlet light bordered info">

            <div class="portlet-title tabbable-line">
                <div class="portlet-photo">
                    @if(is_file(public_path() ."/assets/uploads/course/" . $deseo->curso->course_images . "/" .$deseo->curso->image_portada))
                        <div class="booking-foto" style="background-image: url('/assets/uploads/course/{{$deseo->curso->course_images}}/{{$deseo->curso->image_portada}}');"></div>
                    @elseif(is_file(public_path() ."/assets/uploads/center/" . $deseo->curso->centro->center_images . "/" .$deseo->curso->centro->center_image_portada))
                        <div class="booking-foto" style="background-image: url('/assets/uploads/center/{{$deseo->curso->centro->center_images}}/{{$deseo->curso->centro->center_image_portada}}');"></div>
                    @elseif(!is_file(public_path() ."/assets/uploads/course/" . $deseo->curso->course_images . "/" .$deseo->curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $deseo->curso->centro->center_images . "/" .$deseo->curso->centro->center_image_portada))
                        @if(count($fotoscursoname))
                            <div class="booking-foto" style="background-image: url('/assets/uploads/course/{{$deseo->curso->course_images}}/{{$fotoscursoname[rand(0,count($fotoscursoname)-1)]}}');"></div>
                        @elseif(!count($fotoscursoname) && count($fotoscentroname))
                            <div class="booking-foto" style="background-image: url('/assets/uploads/center/{{$deseo->curso->centro->center_images}}/{{$fotoscentroname[rand(0,count($fotoscentroname)-1)]}}');"></div>
                        @else
                            <div class="booking-foto" style="background-color: #e5e5e5;"></div>
                        @endif
                    @endif
                </div>

                <div class="caption font-green-sharp">
                    {{ConfigHelper::getTipoTransporteIcono($deseo->vuelo ? $deseo->vuelo->transporte : 0)}}
                    <span class="caption-subject bold uppercase">
                        {{ $deseo->curso->es_convocatoria_multi?$deseo->convocatoria->name:$deseo->curso->name}}
                    </span>
                </div>

                <div class="pull-right">
                    {{-- <a href="" class="collapse"></a> --}}
                    {{-- <a href="" class="fullscreen"></a>
                    <br /> --}}

                    <a href="{{route('area.deseos.borrar', $deseo->id)}}" class="btn btn-danger btn-xs hidden-print"><i class="fa fa-times"></i></a>
                    <br><br>
                    {{-- @if(auth()->user()->compra_en_curso)
                        <i class="fa fa-info-circle" data-label="Ya tiene un booking online iniciado. Tiene que completar o cancelar el que tiene en curso."></i>
                    @else --}}
                        <a href="{{route('area.comprar.compra', $deseo->curso->id)}}" class="btn btn-success plusinfolink pibtn hidden-print">{!! trans('web.curso.comprar') !!}</a>
                    {{-- @endif --}}
                </div>


            </div>
        </div>
    </li>

@endforeach
</ul>

@stop