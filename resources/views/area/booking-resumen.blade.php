<div class="row">
    <div class="col-md-12">
        <h4 class="text-success">Resumen</h4>
    </div>
</div>

        <div class="row">
            <div class="col-md-3">
                Programa: {{ $ficha->curso->es_convocatoria_multi?$ficha->convocatoria->name:$ficha->curso->name}}
                <span id="booking-cabierta-alerta" class='booking-alerta'>{{isset($ficha->alertas['cabierta'])?$ficha->alertas['cabierta']:""}}</span>
                <span id="booking-cabierta-precio-detalle" class='booking-precio'>
                    @if($ficha->curso_precio_extra>0)
                        {{"Precio base: ". ConfigHelper::parseMoneda( ($ficha->course_price), $ficha->curso_moneda)}}
                        {{" + Extra temporada: ". ConfigHelper::parseMoneda( ($ficha->curso_precio_extra), $ficha->curso_moneda)}}
                    @endif
                </span>
            </div>
            <div class="col-md-4">
                Fecha inicio: {{$ficha->curso_start_date}}
            </div>
            <div class="col-md-4">
                Fecha final: {{$ficha->curso_end_date}}
            </div>
        </div>


        <hr>
        Extras:<br>
        <ul class="booking-extras">
            @foreach($ficha->extras->sortBy('tipo') as $extra)
            <li>

                {{$extra->full_name}}

            </li>
            @endforeach
        </ul>

        <hr>

        <table class="table total">
            <thead>
                <tr class="thead">
                    <td colspan="2" align="center">TOTAL</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>SubTotal</td>
                    <td><span id="booking-subtotal-resumen">
                        @foreach($ficha->precio_total['subtotal_txt'] as $subtotal)
                            {{$subtotal}}<br>
                        @endforeach
                    </span></td>
                </tr>

                {{-- DESCUENTOS --}}
                @if($ficha->precio_total['descuento_especial'] > 0)
                <tr>
                    <td>Descuento especial</td>
                    <td>{{$ficha->precio_total['descuento_especial_txt']}}</td>
                </tr>
                @endif

                @if($ficha->descuentos->count()>0)
                <tr>
                    <td>Descuentos</td>
                    <td>{{$ficha->precio_total['descuento_txt']}}</td>
                </tr>
                @endif

                <tr>
                    <td>TOTAL EN {{Session::get('vcn.moneda')}}</td>
                    <td><span class="booking-total" id="booking-total-resumen">{{$ficha->precio_total['total_txt']}}</span></td>
                </tr>

            </tbody>
        </table>
