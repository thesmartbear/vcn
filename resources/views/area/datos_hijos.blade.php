@extends('layouts.area')


@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop


@section('content')

    <h2 class="text-success">{!! trans('area.Mis Hijos') !!} {!! $usuario->datos_campamento_pendiente !!}</h2>
    <hr>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">

        <?php $y = 1; ?>
        @foreach($usuario->tutor->viajeros as $hijo)
            <li role="presentation" @if($y == 1) class="active" @endif>
                <a href="#hijo-{{$hijo->id}}" aria-controls="hijo-{{$hijo->id}}" role="tab" data-toggle="tab">
                    <i class="fa fa-child"></i> {{$hijo->full_name}}
                </a>
            </li>
            <?php $y++; ?>
        @endforeach

    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <?php $y = 1; ?>
        @foreach($usuario->tutor->viajeros as $hijo)
            <div role="tabpanel" class="tab-pane fade in @if($y == 1) active @endif " id="hijo-{{$hijo->id}}">

                @include('area.datos-viajero', ['ficha'=> $hijo, 'route'=> route('area.datos.hijos.post', $hijo->id)] )

            </div>
            <?php $y++; ?>
        @endforeach

    </div>

    <div class="clearfix"></div>


{{-- <script type="text/javascript">

    $('.form-control').prop( "disabled", true );
    $('.form-radio input').prop( "disabled", true );
    $('input[type="checkbox"]').prop( "disabled", true );

</script> --}}

@stop