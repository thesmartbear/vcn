@extends('layouts.area')


@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop


@section('content')

<h2 class="text-success">{!! trans('area.Tutores') !!}</h2>
<hr>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">

        <?php $y = 1; ?>
        @foreach($ficha->tutores as $tutor)
            <li role="presentation" @if($y==1) class="active" @endif>
                <a href="#tutor-{{$tutor->id}}" aria-controls="tutor-{{$tutor->id}}" role="tab" data-toggle="tab">
                    <i class="fa fa-shield"></i> {{$tutor->full_name}}
                </a>
            </li>
            <?php $y++; ?>
        @endforeach

    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <?php $y = 1; ?>
        @foreach($ficha->tutores as $tutor)
            <div role="tabpanel" class="tab-pane fade in @if($y==1) active @endif" id="tutor-{{$tutor->id}}">
                @include('area.datos-tutor',['ficha'=> $tutor])
            </div>
            <?php $y++; ?>
        @endforeach

    </div>

<hr>

@stop