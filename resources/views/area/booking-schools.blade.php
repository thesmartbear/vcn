<div class="row">
    <div class="col-md-12">
        
        @if( $booking->schools_area->count()>0 )
        <h4 class="text-warning">{!! trans('area.escuelas') !!}:</h4>

        <table class="table">
            <thead>
                <tr class="thead">
                    <td>{!! trans('area.escuela') !!}</td>
                    <td>{!! trans('area.fechas') !!}</td>
                    <td width="20%"></td>
                    <td width="20%"></td>
                </tr>
            </thead>
            <tbody>
            @foreach($booking->schools_area as $bfam)
                <tr>
                    <td><a data-label="{!! trans('area.verdetalles') !!}" href="{{route('area.booking.school',$bfam->id)}}">{{$bfam->school->name}}</a></td>
                    <td>
                        {!! trans('area.del') !!} {{$bfam->desde?$bfam->desde->format('d/m/Y'):"-"}}
                        {!! trans('area.al') !!} {{$bfam->hasta?$bfam->hasta->format('d/m/Y'):"-"}}
                    </td>
                    <td><a class="btn btn-block btn-primary" href="{{route('area.booking.school',$bfam->id)}}"><i class="fa fa-eye"></i> {!! trans('area.verdetalles') !!}</a></td>
                    <td><a class="btn btn-block btn-success" href="{{route('area.booking.school-pdf',$bfam->id)}}"><i class="fa fa-download"></i> {!! trans('area.descargar') !!}</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @endif

    </div>
</div>