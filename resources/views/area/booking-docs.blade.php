<div class="row">
    <div class="col-md-12">
        <h4 class="text-success">{!! trans('area.documentosimportantes') !!}</h4>

        @if($booking->curso->es_pocket)
            <table class="table table-bordered table-hover">
                <tr>
                    <td class="col-sm-8">
                        Pocket Guide
                    </td>
                    <td class="col-sm-2">
                        <a class="btn btn-primary btn-block" href="{{route('area.booking.pocketguide',$booking->id)}}">
                            <i class="fa fa-eye"></i> {!! trans('area.verdetalles') !!}
                        </a>
                    </td>
                    {{-- <td>
                        iframe
                    </td> --}}
                    <td class="col-sm-2">
                        <a class="btn btn-success btn-block" href="{{route('manage.cursos.pocket-guide', [$booking->curso->id, $booking->viajero->idioma_contacto])}}" target="_blank" download>
                            <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                        </a>
                    </td>
                </tr>
            </table>
            <hr>
        @endif


        <table class="table table-bordered table-hover">
                <?php
                    $path = storage_path("files/viajeros/" . $ficha->viajero_id);
                    $folder = "/files/viajeros/" . $ficha->viajero_id;
                ?>

                @foreach($booking->viajero->archivos_importantes as $archivo)

                    <?php
                        $result = $archivo->doc;
                        $file = $path . '/' . $result;
                        $filew = $folder .'/'. $result;
                        if($result && $result[0] == "/")
                        {
                            $file = $path . $result;
                            $filew = $folder . $result;
                        }

                        if(!file_exists($file))
                        {
                            continue;
                        }
                    ?>

                    <tr>
                    <td class="col-sm-10">
                        {{$archivo->doc}}
                    </td>
                    <td>
                        {!! ConfigHelper::iframe($result, $path, $folder) !!}
                    </td>
                    <td class="col-sm-2">
                        <a class="btn btn-success btn-block" href="{{$filew}}" target="_blank" download>
                            <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                        </a>
                    </td>
                    </tr>
                @endforeach

                {{-- PDF Cancelación --}}
                @if($booking->cancelacion_seguro && $booking->cancelacion_pdf)
                    <?php
                        $pathc = "";

                        $seguro = $booking->cancelacion_seguro;
                        $seguro_name = $seguro->name;
                        $pdf = $booking->cancelacion_pdf;
                        //$seguro_name = $pdf ? "<a href='/$pdf' target='_blank'>$seguro_name</a>" : $seguro_name;
                    ?>
                    <tr>
                        <td class="col-sm-10">
                            {{$seguro->name}}
                        </td>
                        <td>
                            {!! ConfigHelper::iframe($pdf, public_path($pathc), $pathc) !!}
                        </td>
                        <td class="col-sm-2">
                            <a class="btn btn-success btn-block" href="/{{$pdf}}" target="_blank" download>
                                <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                            </a>
                        </td>
                    </tr>

                @endif

                {{-- Archivos visibles (viajero) --}}
                @foreach($booking->archivos_importantes as $archivo)

                    <?php
                        $result = $archivo->doc;
                        $file = $path . '/' . $result;
                        $filew = $folder .'/'. $result;
                        if($result && $result[0] == "/")
                        {
                            $file = $path . $result;
                            $filew = $folder . $result;
                        }

                        if(!file_exists($file))
                        {
                            continue;
                        }
                    ?>

                    <tr>
                    <td class="col-sm-10">
                        {{$archivo->doc}}
                    </td>
                    <td>
                        {!! ConfigHelper::iframe($result, $path, $folder) !!}
                    </td>
                    <td class="col-sm-2">
                        <a class="btn btn-success btn-block" href="{{$filew}}" target="_blank" download>
                            <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                        </a>
                    </td>
                    </tr>
                @endforeach

                {{-- Archivos visibles (bookings) --}}
                @foreach($booking->archivos_importantes as $archivo)

                    <?php
                        $path = storage_path("files/bookings/" . $ficha->viajero_id);
                        $folder = "/files/bookings/" . $ficha->viajero_id;
                    ?>

                    <?php
                        $result = $archivo->doc;
                        $file = $path . '/' . $result;
                        $filew = $folder .'/'. $result;
                        if($result && $result[0] == "/")
                        {
                            $file = $path . $result;
                            $filew = $folder . $result;
                        }

                        if(!file_exists($file))
                        {
                            continue;
                        }
                    ?>

                    <tr>
                    <td class="col-sm-10">
                        {{$archivo->doc}}
                    </td>
                    <td>
                        {!! ConfigHelper::iframe($result, $path, $folder) !!}
                    </td>
                    <td class="col-sm-2">
                        <a class="btn btn-success btn-block" href="{{$filew}}" target="_blank" download>
                            <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                        </a>
                    </td>
                    </tr>
                @endforeach

                {{-- Firmas --}}
                @foreach($booking->archivos_firmas as $archivo)

                    <?php
                        $path = storage_path("files/bookings/" . $ficha->viajero_id);
                        $folder = "/files/bookings/" . $ficha->viajero_id;
                    ?>

                    <?php
                        $result = $archivo->doc;
                        $file = $path . '/' . $result;
                        $filew = $folder .'/'. $result;
                        if($result && $result[0] == "/")
                        {
                            $file = $path . $result;
                            $filew = $folder . $result;
                        }

                        if(!file_exists($file))
                        {
                            continue;
                        }
                    ?>

                    <tr>
                    <td class="col-sm-10">
                        {{$archivo->doc}}
                    </td>
                    <td>
                        {!! ConfigHelper::iframe($result, $path, $folder) !!}
                    </td>
                    <td class="col-sm-2">
                        <a class="btn btn-success btn-block" href="{{$filew}}" target="_blank" download>
                            <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                        </a>
                    </td>
                    </tr>
                @endforeach

                {{-- Facturas --}}
                @if($booking->area_pagos)
                @foreach($booking->facturas->where('manual',0) as $factura)

                    <?php
                        $dir = "";
                        $name = "";
                        if($factura->grup)
                        {
                            $dir = "/files/facturas/";
                            $name = "factura_". $factura->grup->numero .".pdf";
                        }
                        else
                        {
                            $dir = "/files/bookings/". $factura->booking->viajero->id . "/";
                            $name = "factura_". $factura->numero .".pdf";
                        }
                        $file = $dir . $name;
                    ?>

                    <tr>
                    <td class="col-sm-10">
                        {{$name}}
                    </td>
                    <td>
                        {!! ConfigHelper::iframe($name, storage_path($dir), $dir) !!}
                    </td>
                    <td class="col-sm-2">
                        <a class="btn btn-success btn-block" href="{{$file}}" target="_blank" download>
                            <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                        </a>
                    </td>
                    </tr>

                @endforeach
                @endif

                {{-- Centro --}}
                @foreach($booking->curso->centro->getDocumentosArea("-", $booking->plataforma) as $archivo)

                    <?php
                        $name = $archivo->doc;
                        $dir = "/assets/uploads/documentos/". $archivo->idioma ."/". $archivo->modelo ."/". $archivo->modelo_id;
                        $href = $dir ."/". $archivo->doc;
                    ?>

                    <tr>
                    <td class="col-sm-10">
                        {{$name}}
                    </td>
                    <td>
                        {!! ConfigHelper::iframe($archivo->doc, public_path($dir), $dir) !!}
                    </td>
                    <td class="col-sm-2">
                        <a class="btn btn-success btn-block" href="{{$href}}" target="_blank" download>
                            <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                        </a>
                    </td>
                    </tr>
                @endforeach
                
                @foreach($booking->curso->centro->getDocumentosArea($booking->viajero->idioma_contacto, $booking->plataforma) as $archivo)

                    <?php
                        $name = $archivo->doc;
                        $dir = "/assets/uploads/documentos/". $archivo->idioma ."/". $archivo->modelo ."/". $archivo->modelo_id;
                        $href = $dir ."/". $archivo->doc;
                    ?>

                    <tr>
                    <td class="col-sm-10">
                        {{$name}}
                    </td>
                    <td>
                        {!! ConfigHelper::iframe($archivo->doc, public_path($dir), $dir) !!}
                    </td>
                    <td class="col-sm-2">
                        <a class="btn btn-success btn-block" href="{{$href}}" target="_blank" download>
                            <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                        </a>
                    </td>
                    </tr>
                @endforeach

                {{-- Curso --}}
                @foreach($booking->curso->getDocumentosArea("-", $booking->plataforma) as $archivo)

                    <?php
                        $name = $archivo->doc;
                        $dir = "/assets/uploads/documentos/". $archivo->idioma ."/". $archivo->modelo ."/". $archivo->modelo_id;
                        $href = $dir ."/". $archivo->doc;
                    ?>

                    <tr>
                    <td class="col-sm-10">
                        {{$name}}
                    </td>
                    <td>
                        {!! ConfigHelper::iframe($archivo->doc, public_path($dir), $dir) !!}
                    </td>
                    <td class="col-sm-2">
                        <a class="btn btn-success btn-block" href="{{$href}}" target="_blank" download>
                            <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                        </a>
                    </td>
                    </tr>
                @endforeach
                @foreach($booking->curso->getDocumentosArea($booking->viajero->idioma_contacto, $booking->plataforma) as $archivo)

                    <?php
                        $name = $archivo->doc;
                        $dir = "/assets/uploads/documentos/". $archivo->idioma ."/". $archivo->modelo ."/". $archivo->modelo_id;
                        $href = $dir ."/". $archivo->doc;
                    ?>

                    <tr>
                    <td class="col-sm-10">
                        {{$name}}
                    </td>
                    <td>
                        {!! ConfigHelper::iframe($archivo->doc, public_path($dir), $dir) !!}
                    </td>
                    <td class="col-sm-2">
                        <a class="btn btn-success btn-block" href="{{$href}}" target="_blank" download>
                            <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                        </a>
                    </td>
                    </tr>
                @endforeach

                {{-- Categoria --}}
                @if( $booking->curso->categoria )
                    @foreach($booking->curso->categoria->getDocumentosArea("-", $booking->plataforma) as $archivo)

                        <?php
                            $name = $archivo->doc;
                            $dir = "/assets/uploads/documentos/". $archivo->idioma ."/". $archivo->modelo ."/". $archivo->modelo_id;
                            $href = $dir ."/". $archivo->doc;
                        ?>

                        <tr>
                        <td class="col-sm-10">
                            {{$name}}
                        </td>
                        <td>
                            {!! ConfigHelper::iframe($archivo->doc, public_path($dir), $dir) !!}
                        </td>
                        <td class="col-sm-2">
                            <a class="btn btn-success btn-block" href="{{$href}}" target="_blank" download>
                                <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                            </a>
                        </td>
                        </tr>
                    @endforeach
                    @foreach($booking->curso->categoria->getDocumentosArea($booking->viajero->idioma_contacto, $booking->plataforma) as $archivo)
                        <?php
                            $name = $archivo->doc;
                            $dir = "/assets/uploads/documentos/". $archivo->idioma ."/". $archivo->modelo ."/". $archivo->modelo_id;
                            $href = $dir ."/". $archivo->doc;
                        ?>

                        <tr>
                        <td class="col-sm-10">
                            {{$name}}
                        </td>
                        <td>
                            {!! ConfigHelper::iframe($archivo->doc, public_path($dir), $dir) !!}
                        </td>
                        <td class="col-sm-2">
                            <a class="btn btn-success btn-block" href="{{$href}}" target="_blank" download>
                                <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                            </a>
                        </td>
                        </tr>
                    @endforeach
                @endif

                {{-- Subcategoria --}}
                @if( $booking->curso->subcategoria )
                    @foreach($booking->curso->subcategoria->getDocumentosArea("-", $booking->plataforma) as $archivo)
                        <?php
                            $name = $archivo->doc;
                            $dir = "/assets/uploads/documentos/". $archivo->idioma ."/". $archivo->modelo ."/". $archivo->modelo_id;
                            $href = $dir ."/". $archivo->doc;
                        ?>

                        <tr>
                        <td class="col-sm-10">
                            {{$name}}
                        </td>
                        <td>
                            {!! ConfigHelper::iframe($archivo->doc, public_path($dir), $dir) !!}
                        </td>
                        <td class="col-sm-2">
                            <a class="btn btn-success btn-block" href="{{$href}}" target="_blank" download>
                                <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                            </a>
                        </td>
                        </tr>
                    @endforeach
                    @foreach($booking->curso->subcategoria->getDocumentosArea($booking->viajero->idioma_contacto, $booking->plataforma) as $archivo)
                        <?php
                            $name = $archivo->doc;
                            $dir = "/assets/uploads/documentos/". $archivo->idioma ."/". $archivo->modelo ."/". $archivo->modelo_id;
                            $href = $dir ."/". $archivo->doc;
                        ?>

                        <tr>
                        <td class="col-sm-10">
                            {{$name}}
                        </td>
                        <td>
                            {!! ConfigHelper::iframe($archivo->doc, public_path($dir), $dir) !!}
                        </td>
                        <td class="col-sm-2">
                            <a class="btn btn-success btn-block" href="{{$href}}" target="_blank" download>
                                <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                            </a>
                        </td>
                        </tr>
                    @endforeach
                @endif

                {{-- Convocatoria --}}
                @if($booking->convocatoria)
                @foreach($booking->convocatoria->getDocumentosArea("-", $booking->plataforma) as $archivo)
                    <?php
                        $name = $archivo->doc;
                        $dir = "/assets/uploads/documentos/". $archivo->idioma ."/". $archivo->modelo ."/". $archivo->modelo_id;
                        $href = $dir ."/". $archivo->doc;
                    ?>

                    <tr>
                    <td class="col-sm-10">
                        {{$name}}
                    </td>
                    <td>
                        {!! ConfigHelper::iframe($archivo->doc, public_path($dir), $dir) !!}
                    </td>
                    <td class="col-sm-2">
                        <a class="btn btn-success btn-block" href="{{$href}}" target="_blank" download>
                            <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                        </a>
                    </td>
                    </tr>
                @endforeach
                
                @foreach($booking->convocatoria->getDocumentosArea($booking->viajero->idioma_contacto, $booking->plataforma) as $archivo)
                    <?php
                        $name = $archivo->doc;
                        $dir = "/assets/uploads/documentos/". $archivo->idioma ."/". $archivo->modelo ."/". $archivo->modelo_id;
                        $href = $dir ."/". $archivo->doc;
                    ?>

                    <tr>
                    <td class="col-sm-10">
                        {{$name}}
                    </td>
                    <td>
                        {!! ConfigHelper::iframe($archivo->doc, public_path($dir), $dir) !!}
                    </td>
                    <td class="col-sm-2">
                        <a class="btn btn-success btn-block" href="{{$href}}" target="_blank" download>
                            <i class="fa fa-download"></i> {!! trans('area.descargar') !!}
                        </a>
                    </td>
                    </tr>
                @endforeach
                @endif

        </table>
    </div>
</div>