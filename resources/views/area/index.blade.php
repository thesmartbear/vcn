@extends('layouts.area')


@section('content')
  
    <h2 class="text-capitalize text-success">Hola {{mb_strtolower($usuario->ficha->name)}}</h2>
    
    @if($usuario->user_tutor_viajero)
      @if($usuario->tutor)
        [@lang('area.tutor')] <a href="{{route('area.login.swap')}}">@lang('area.login_viajero')</a>
      @elseif($usuario->viajero)
        [@lang('area.viajero')] <a href="{{route('area.login.swap')}}">@lang('area.login_tutor')</a>
      @endif
    @endif



    @if( $usuario->es_tutor && !is_null($usuario->datos_campamento_pendiente) )

      <div class="alert alert-warning" role="alert">
        <a href="{{ route('area.datos.hijos') }}">
          <strong><span class='badge badge-warning'><i class='fa fa-exclamation-triangle'></i></span> @lang('area.datos_campamento_pendiente')</strong>  
        </a>
      </div>
      
    @endif
    
    <hr>

    @if($usuario->es_aviso_foto)
        <div class="row">
            <div class="col-md-10">
                <a href="{{ $usuario->es_viajero ? route('area.datos.index') : route('area.datos.hijos') }}">
                <div class="note note-warning">
                    <h4 class="block">@lang('area.booking.atencion')</h4>
                    <p>{!! trans('area.datosfoto_aviso') !!}</p>
                </div>
                </a>
            </div>
            <div class="col-md-2">

            </div>
        </div>
        <hr>
    @endif

    @if($usuario->es_monitor)
        @include('area.monitores.index', ['monitor'=> $usuario->ficha])
    @endif

    @if($usuario->es_viajero)
        @if($usuario->viajero->oficina)
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption font-green-sharp">
                    <i class="fa fa-building fa-2x"></i>
                    <span class="caption-subject bold uppercase">{!! trans('area.mioficina') !!}: {{$usuario->viajero->oficina->name}}</span>
                </div>
                <div class="tools">
                    <a href="" class="expand btn btn-circle btn-default btn-text" data-original-title="" title=""> {!! trans('area.verdatos') !!}</a>
                </div>
            </div>
            <div class="portlet-body flip-scroll" style="display: none;">
                <div class="row">
                    <div class="col-sm-9">
                        <p>
                            <strong>{!! trans('area.tlf') !!}:</strong> {{$usuario->viajero->oficina->telefono}}
                            <br /><strong>{!! trans('area.email') !!}:</strong> <span class="text-lowercase">{{$usuario->viajero->oficina->email}}</span>
                        </p>
                        <p>
                            <strong>{!! trans('area.direccion') !!}:</strong> {{$usuario->viajero->oficina->direccion}}. {{$usuario->viajero->oficina->poblacion}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        @endif
    @endif

    {{-- LOPD_CHECKS 2018 --}}
    <?php
      $bArea = false;
      $u = auth()->user();
      if($u->es_viajero || $u->es_tutor || $u->es_monitor)
      {
        $bArea = true;
      }

      if(ConfigHelper::config('es_rgpd'))
      {
        $bArea = false; 
      }
    
    ?>
    @if(is_null($usuario->ficha->lopd_check1) && $bArea)

        <?php
            $bModal = false;
            if($usuario->ficha->lopd_booking_any == 2018)
            {
                $bModal = true;
            }
        ?>

        @if($bModal)
        <div class="modal fade" tabindex="-1" role="dialog" id="lopd">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">LOPD</h4>
                    </div>

                    {!! Form::open(array('id'=> 'frmLOPD', 'method'=> 'post', 'url' => route('area.datos.post'))) !!}
                    <div class="modal-body">

                        {!! Form::hidden('submit_lopd', 1) !!}

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'lopd_check1', 'texto'=> trans('area.booking.lopd_check1', ['responsable'=> ConfigHelper::config('name')]), 'required'=> true, 'html'=> true])
                        </div>

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'lopd_check2', 'texto'=> trans('area.booking.lopd_check2', ['plataforma'=> ConfigHelper::plataformaApp()]), 'html'=> true ])
                        </div>

                        <div class="form-group">
                            @include('includes.form_checkbox', [ 'campo'=> 'lopd_check3', 'texto'=> trans('area.booking.lopd_check3'), 'html'=> true ])
                        </div>
                        
                    </div>
                    <div class="modal-footer">

                        <div class="form-group pull-right">
                            {!! Form::submit(trans('area.actualizar'), array('id'=> 'btnLOPD', 'class' => 'btn btn-success')) !!}
                        </div>
                        
                    </div>
                    {!! Form::close() !!}

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script type="text/javascript">
            $(document).ready(function() {
                $('#lopd').modal({backdrop: 'static', keyboard: false}); 
            });
        </script>
        @endif
        
    @endif


    @if($usuario->es_viajero)
        @include('area.bookings', ['ficha'=> $usuario->viajero] )
    @elseif($usuario->es_tutor)
        <?php $y=1; ?>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
        @foreach($usuario->tutor->viajeros as $viajero)
            <li role="presentation" @if($y == 1) class="active" @endif ><a href="#{{$y}}" aria-controls="{{$y}}" role="tab" data-toggle="tab"><h4 class="text-capitalize">{{mb_strtolower($viajero->full_name)}} <small>({!! trans('area.viajero') !!})</small></h4></a></li>
            <?php $y++; ?>
        @endforeach
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <?php $y=1; ?>
            @foreach($usuario->tutor->viajeros as $viajero)
                <div role="tabpanel" class="tab-pane fade in @if($y == 1) active @endif " id="{{$y}}">
                    @if($viajero->bookings_area->count()>0)
                        @include('area.bookings', ['ficha'=> $viajero] )
                    @else
                        <p>{!! trans('area.nobookings') !!}</p>
                    @endif
                </div>
                <?php $y++; ?>
            @endforeach
        </div>
    @endif

<script type="text/javascript">

function checkLOPD(_callback)
{
  @if(!ConfigHelper::config('es_rgpd'))
    _callback();
    return;
  @endif

  chk1 = $('#lopd_check1').is(':checked');
  chk2 = $('#lopd_check2').is(':checked');
  chk3 = $('#lopd_check3').is(':checked');

  if(!chk1)
  {
    $mensaje = "{{trans('area.booking.lopd_check1_no')}}";

    bootbox.alert($mensaje);
    return;
  }

  $b2ret = true;

  if(!chk2 || !chk3)
  {
    $b2ret = false;
  }

  if(!chk2 && !chk3)
  {
    $mensaje = "{{trans('area.booking.lopd_check2y3_no')}}";
  }
  else if(!chk2)
  {
    $mensaje = "{{trans('area.booking.lopd_check2_no')}}";
  }
  else if(!chk3)
  {
    $mensaje = "{{trans('area.booking.lopd_check3_no')}}";
  }

  if(!$b2ret)
  {
    bootbox.confirm({
      title: "LOPD",
      message: $mensaje,
      buttons: {
        confirm: {
          label: "{{trans('area.booking.lopd_check_seguir')}}",
          className: 'btn-warning'
        },
        cancel: {
          label: "{{trans('area.booking.lopd_check_volver')}}",
          className: 'btn-success'
        }
      },
      callback: function (result) {
        if(result)
        {
          $b2ret = true;
          _callback();
        }
      }
    });
  }
  else
  {
    _callback();
  }

  return $b2ret;
}

$('#btnLOPD').click( function(e) {
    e.preventDefault();

    checkLOPD( function(e) {
        $('#frmLOPD').submit();
    });
});

</script>

@stop