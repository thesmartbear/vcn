<div class="portlet light bordered info">
    <div class="portlet-title">
        <div class="caption font-green-sharp">
            <i class="fa fa-child fa-fw"></i>
            <span class="caption-subject bold uppercase">{{$ficha->full_name}} <small>({!! trans('area.viajero') !!})</small></span>
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
            <a href="" class="fullscreen"></a>
        </div>

    </div>

    <?php
        $booking = $ficha->booking;
        if($booking)
        {
            if($booking->curso->categoria && $booking->curso->categoria->es_info_campamento)
            {

            }
            else
            {
                $booking = null;
            }
        }
    ?>

    <div class="portlet-body flip-scroll">
        <ul class="nav nav-tabs" role="tablist">

            <li role="presentation" class="active"><a href="#{{$ficha->id}}-datos" aria-controls="{{$ficha->id}}-datos" role="tab" data-toggle="tab">{!! trans('area.datos') !!}</a></li>

            <li role="presentation"><a href="#{{$ficha->id}}-datos2" aria-controls="{{$ficha->id}}-datos2" role="tab" data-toggle="tab">{!! trans('area.datosmedicos') !!}</a></li>
    
            @if($booking)
                <li role="presentation"><a href="#{{$ficha->id}}-datos3" aria-controls="{{$ficha->id}}-datos3" role="tab" data-toggle="tab">{!! trans('area.datosinteres') !!} {!! $usuario->datos_campamento_pendiente !!}</a></li>
            @endif

            @if($usuario->es_tutor)
                <li role="presentation"><a href="#{{$ficha->id}}-tutores" aria-controls="{{$ficha->id}}-tutores" role="tab" data-toggle="tab">{!! trans('area.tutores') !!}</a></li>
            @endif

            <li role="presentation"><a href="#{{$ficha->id}}-avatar" aria-controls="{{$ficha->id}}-avatar" role="tab" data-toggle="tab">{!! trans('area.datosfoto') !!}</a></li>
        </ul>

        <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active ficha" id="{{$ficha->id}}-datos">

                {!! Form::open( array('id'=> 'frmActualizar', 'method'=> 'post', 'url' => $route) ) !!}
                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> trans('area.nombre')])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'lastname', 'texto'=> trans('area.apellido1')])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'lastname2', 'texto'=> trans('area.apellido2')])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        <span for='sexo'>{!! trans('area.sexo') !!}:</span>
                        <div class="form-radio">
                            <label class="radio-inline">
                                {!! Form::radio('sexo', '1', ($ficha->sexo==1)) !!} <i class="fa fa-male"></i>
                            </label>
                            <label class="radio-inline">
                                {!! Form::radio('sexo', '2', ($ficha->sexo==2)) !!} <i class="fa fa-female"></i>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_datetime', [ 'campo'=> 'fechanac', 'texto'=> trans('area.fnacimiento')])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_select_pais', [ 'campo'=> 'paisnac', 'texto'=> trans('area.pnacimiento')])
                    </div>
                </div>

                @if($usuario->es_viajero)
                <div class="form-group">
                    @include('includes.form_checkbox', [ 'campo'=> 'lopd_check2', 'texto'=> trans('area.booking.lopd_check2', ['plataforma'=> ConfigHelper::plataformaApp()]), 'ficha'=> $usuario->viajero, 'html'=> true ])
                </div>
                @endif

                <div class="form-group row">
                    <div class="col-md-12">
                        <h4 class="form-section">{!! trans('area.datosrelevantes') !!}:</h4>
                    </div>
                </div>
                
                <div class="form-group row">
                    @if(ConfigHelper::config('nif'))
                    <div class="col-md-2">
                        @include('includes.form_select', [ 'campo'=> 'tipodoc', 'texto'=> "Tipo", 'select'=> ConfigHelper::getTipoDoc(), 'ficha'=> $ficha->datos])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'documento', 'texto'=> trans('area.nif'), 'ficha'=> $ficha->datos])
                    </div>
                    @endif
                    <div class="col-md-3">
                        @include('includes.form_select', [ 'campo'=> 'nacionalidad', 'texto'=> trans('area.nacionalidad'), 'ficha'=> $ficha->datos, 'select'=> [""=>""] + \VCN\Models\Nacionalidad::pluck('name','name')->toArray()])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        @include('includes.form_input_text', [ 'campo'=> 'pasaporte', 'texto'=> trans('area.pasaporte'), 'ficha'=> $ficha->datos])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_select_pais', [ 'campo'=> 'pasaporte_pais', 'texto'=> trans('area.paispasaporte'), 'ficha'=>$ficha->datos ])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_datetime', [ 'campo'=> 'pasaporte_emision', 'texto'=> trans('area.femision').' '.trans('area.pasaporte'),'ficha'=>$ficha->datos ])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_datetime', [ 'campo'=> 'pasaporte_caduca', 'texto'=> trans('area.fcaducidad').' '.trans('area.pasaporte'),'ficha'=>$ficha->datos ])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <h4 class="form-section">{!! trans('area.datoscontacto') !!}</h4>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_select', [ 'campo'=> 'idioma_contacto', 'texto'=> trans('area.idiomacontacto'), 'select'=> ConfigHelper::getIdiomaContacto(), 'valor'=> $ficha->datos?$ficha->datos->idioma_contacto:""])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        <a href="#" data-toggle="tooltip" title="{{trans('area.datos_email')}}"><i class="fa fa-ban"></i></a>
                        @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> trans('area.email'), 'disabled'=>true])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'phone', 'texto'=> trans('area.telefono')])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'movil', 'texto'=> trans('area.movil')])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        @include('includes.form_select', [ 'campo'=> 'tipovia', 'texto'=> trans('area.tipo'), 'select'=> ConfigHelper::getTipoVia(), 'ficha'=> $ficha->datos])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'direccion', 'texto'=> trans('area.direccion'), 'ficha'=> $ficha->datos])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'ciudad', 'texto'=> trans('area.ciudad'), 'ficha'=> $ficha->datos])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_input_text', [ 'campo'=> 'cp', 'texto'=> trans('area.cp'), 'ficha'=> $ficha->datos])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        @include('includes.form_select2', [ 'campo'=> 'provincia', 'texto'=> trans('area.provincia'), 'select'=>[""=>""]+ \VCN\Models\Provincia::pluck('name','name')->toArray(), 'ficha'=> $ficha->datos])
                    </div>
                    <div class="col-md-6">
                        @include('includes.form_select_pais', [ 'campo'=> 'pais', 'texto'=> trans('area.pais'), 'ficha'=> $ficha->datos])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-8">
                        @include('includes.form_input_text', [ 'campo'=> 'telefonos', 'texto'=> trans('area.otrostelfs'), 'ficha'=> $ficha->datos])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'rs_instagram', 'texto'=> 'Instagram', 'ficha'=> $ficha->datos])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <h4 class="form-section">Datos académicos:</h4>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_select', [ 'campo'=> 'idioma', 'texto'=> trans('area.nivelingles'), 'select'=> ConfigHelper::getIdioma(), 'ficha'=> $ficha->datos ])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_select', [ 'campo'=> 'idioma_nivel', 'texto'=> trans('area.nivelingles'), 'select'=> ConfigHelper::getIdiomaNivel(), 'ficha'=> $ficha->datos ])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'titulacion', 'texto'=> trans('area.titulacion'), 'ficha'=> $ficha->datos ])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'escuela', 'texto'=> trans('area.escuela'), 'ficha'=> $ficha->datos ])
                    </div>
                    <div class="col-md-2">
                        @include('includes.form_select', [ 'campo'=> 'escuela_curso', 'texto'=> trans('area.cursoactual'), 'select'=> ConfigHelper::getEscuelaCurso(), 'ficha'=> $ficha->datos ])
                    </div>
                </div>

                @include('includes.form_div_bool', [ 'campo'=> 'ingles_academia', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.inglesacademia') ])

                <div class="form-group row">
                    <div class="col-md-6">
                        <span class="input-group-addon">{!! trans('area.hobby') !!}:</span>
                        @include('includes.form_textarea', [ 'campo'=> 'hobby', 'ficha'=> $ficha->datos ])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <h4 class="form-section">{!! trans('area.hermanos') !!}:</h4>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'h1_nom', 'texto'=> trans('area.hermano',['num' => 1]), 'ficha'=> $ficha->datos ])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'h1_fnac', 'texto'=> trans('area.hermanofnacimiento',['num' => 1]), 'ficha'=> $ficha->datos ])
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'h2_nom', 'texto'=> trans('area.hermano',['num' => 2]), 'ficha'=> $ficha->datos ])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'h2_fnac', 'texto'=> trans('area.hermanofnacimiento',['num' => 2]), 'ficha'=> $ficha->datos ])
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'h3_nom', 'texto'=> trans('area.hermano',['num' => 3]), 'ficha'=> $ficha->datos ])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'h3_fnac', 'texto'=> trans('area.hermanofnacimiento',['num' => 3]), 'ficha'=> $ficha->datos ])
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'h4_nom', 'texto'=> trans('area.hermano',['num' => 4]), 'ficha'=> $ficha->datos ])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_text', [ 'campo'=> 'h4_fnac', 'texto'=> trans('area.hermanofnacimiento',['num' => 4]), 'ficha'=> $ficha->datos ])
                    </div>
                </div>

                <div class="form-group pull-right">
                    {!! Form::submit(trans('area.actualizar'), array('id'=> 'btnActualizar', 'class' => 'btn btn-success')) !!}
                </div>
                <div class="clearfix"></div>
                {!! Form::close() !!}


            </div>


            <div role="tabpanel" class="tab-pane fade in ficha" id="{{$ficha->id}}-datos2">

                {!! Form::open( array('method'=> 'post', 'url' => $route)) !!}
                @include('includes.form_div_bool', [ 'campo'=> 'alergias', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.alergia'), 'required'=> true ])

                @include('includes.form_div_bool', [ 'campo'=> 'enfermedad', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.enfermedad'), 'required'=> true ])

                @include('includes.form_div_bool', [ 'campo'=> 'medicacion', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.medicacion'), 'required'=> true ])

                @include('includes.form_div_bool', [ 'campo'=> 'tratamiento', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.tratamiento'), 'required'=> true ])

                @include('includes.form_div_bool', [ 'campo'=> 'dieta', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.dieta'), 'required'=> true ])

                @include('includes.form_div_bool', [ 'campo'=> 'animales', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.animales') ])

                <div class="form-group pull-right">
                    {!! Form::submit(trans('area.actualizar'), array('name'=> 'submit_datos_medicos', 'class' => 'btn btn-success')) !!}
                </div>
                <div class="clearfix"></div>
                {!! Form::close() !!}

            </div>

            
            @if($booking)
                <div role="tabpanel" class="tab-pane fade in ficha" id="{{$ficha->id}}-datos3">
                    {!! Form::open( array('method'=> 'post', 'url' => $route)) !!}
                        
                        @include('manage.bookings.booking.paso2_campamento', ['ficha'=> $booking])

                        <div class="form-group pull-right">
                            {!! Form::submit(trans('area.actualizar'), array('name'=> 'submit_datos_campamento', 'class' => 'btn btn-success')) !!}
                        </div>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            @endif
            

            @if($usuario->es_tutor)
            <div role="tabpanel" class="tab-pane fade in ficha" id="{{$ficha->id}}-tutores">
                <ul class="list-simple">
                    @foreach($ficha->tutores as $t)
                        <li>{{$t->full_name}}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div role="tabpanel" class="tab-pane fade in avatar" id="{{$ficha->id}}-avatar">
                    
                {!! Form::open( array('id'=> 'frmAvatar', 'method'=> 'post', 'files'=> true, 'url' => $route) ) !!}
                  
                    <div class="form-group row">  
                        <div class="col-md-5">
                            @include('includes.form_input_file', [ 'campo'=> 'foto', 'texto'=> 'Foto', 'noborrar'=> true])

                            <br>
                            <div class="note note-info">
                            {!! trans('area.datosfoto_info') !!}
                            </div>

                        </div>
                        <div class="col-md-7">
                            @include('includes.webcam')
                        </div>
                    </div>

                    <div class="form-group pull-right">
                        {!! Form::submit(trans('area.reset'), array('name'=> 'submit_avatar_reset', 'class' => 'btn btn-warning')) !!}
                        {!! Form::submit(trans('area.actualizar'), array('name'=> 'submit_avatar', 'class' => 'btn btn-success')) !!}
                    </div>
                    <div class="clearfix"></div>

                {!! Form::close() !!}

                
            </div>

        </div>


    </div>

</div>

<script type="text/javascript">
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
</script>

@push('scripts')
@include('includes.script_boolean', ['campo'=> 'alergias', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'enfermedad', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'medicacion', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'tratamiento', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'dieta', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'animales', 'required'=>0 ])
@endpush