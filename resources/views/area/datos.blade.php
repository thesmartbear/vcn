@extends('layouts.area')


@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop


@section('content')

    <h2 class="text-success">{!! trans('area.Mis Datos') !!} <small>[{{$usuario->username}}]</small></h2>
    <hr>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">{!! trans('area.Mis Datos') !!}</a></li>
        
        <li role="presentation"><a href="#password" aria-controls="password" role="tab" data-toggle="tab"><i class="fa fa-unlock-alt fa-fw"></i> {!! trans('area.cambiarpass') !!}</a></li>

        <li role="presentation"><a href="#avatar" aria-controls="avatar" role="tab" data-toggle="tab">{!! trans('area.datosfoto') !!}</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <div role="tabpanel" class="tab-pane fade in active" id="ficha">

            @if($usuario->es_viajero)
                @if($usuario->viajero->datos)
                {{-- {!! Form::open(array('id'=> 'frmValidar', 'method'=> 'post', 'url' => route('area.datos.post'))) !!} --}}
                    @include('area.datos-viajero',['ficha'=> $usuario->viajero, 'route'=> route('area.datos.post')])
                    {{-- <div class="form-group pull-right">
                        {!! Form::submit(trans('area.actualizar'), array('class' => 'btn btn-success')) !!}
                    </div>
                {!! Form::close() !!}
                <div class="clearfix"></div> --}}
                @endif
            @endif

            @if($usuario->es_tutor)
                <h4>{!! trans('area.datostutor') !!}</h4>

                {!! Form::open(array('id'=> 'frmActualizar', 'method'=> 'post', 'url' => route('area.datos.post'))) !!}
                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> trans('area.nombre')])
                    </div>
                    <div class="col-md-8">
                        @include('includes.form_input_text', [ 'campo'=> 'lastname', 'texto'=> trans('area.apellidos')])
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'phone', 'texto'=> trans('area.telefono')])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'movil', 'texto'=> trans('area.movil')])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> trans('area.email'), 'disabled'=>true])
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'profesion', 'texto'=> trans('area.profesion')])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'empresa', 'texto'=> trans('area.empresa')])
                    </div>
                    <div class="col-md-4">
                        @include('includes.form_input_text', [ 'campo'=> 'empresa_phone', 'texto'=> trans('area.telfempresa')])
                    </div>
                </div>

                <div class="form-group">
                    {{-- @include('includes.form_checkbox', [ 'campo'=> 'optout', 'texto'=> trans('area.optout', ['plataforma'=> $ficha->plataforma_name]) ]) --}}
                    @include('includes.form_checkbox', [ 'campo'=> 'lopd_check2', 'texto'=> trans('area.booking.lopd_check2', ['plataforma'=> $ficha->plataforma_name]), 'ficha'=> $usuario->tutor ])
                </div>

                <div class="form-group pull-right">
                    {!! Form::submit(trans('area.actualizar'), array('id'=> 'btnActualizar', 'class' => 'btn btn-success')) !!}
                </div>

                {!! Form::close() !!}
            @endif

        </div>


        <div role="tabpanel" class="tab-pane fade in" id="password">
            {!! Form::open(array('method'=> 'post', 'url' => route('area.datos.password'))) !!}

            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_input_password', [ 'campo'=> 'password', 'texto'=> trans('area.pass')])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_password', [ 'campo'=> 'newpass', 'texto'=> trans('area.nuevapass')])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_password', [ 'campo'=> 'newpass_confirmation', 'texto'=> trans('area.nuevapass').' ('.trans('area.repetir').')'])
                </div>
            </div>

            <div class="form-group pull-right">
                {!! Form::submit(trans('area.actualizar'), array('class' => 'btn btn-success')) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div role="tabpanel" class="tab-pane fade in avatar" id="avatar">
                    
            {!! Form::open( array('id'=> 'frmAvatar', 'method'=> 'post', 'files'=> true, 'url' => route('area.datos.post')) ) !!}
              
                <div class="form-group row">  
                    <div class="col-md-5">
                        @include('includes.form_input_file', [ 'campo'=> 'foto', 'texto'=> 'Foto', 'noborrar'=> true])
                        <br>
                        <div class="note note-info">
                        {!! trans('area.datosfoto_info') !!}
                        </div>
                    </div>
                    <div class="col-md-7">
                        @include('includes.webcam')
                    </div>
                </div>

                <div class="form-group pull-right">
                    {!! Form::submit(trans('area.reset'), array('name'=> 'submit_avatar_reset', 'class' => 'btn btn-warning')) !!}
                    {!! Form::submit(trans('area.actualizar'), array('name'=> 'submit_avatar', 'class' => 'btn btn-success')) !!}
                </div>
                <div class="clearfix"></div>

            {!! Form::close() !!}

            
        </div>

    </div>


<script type="text/javascript">

function checkLOPD(_callback)
{
  @if(!ConfigHelper::config('es_rgpd'))
    _callback();
    return;
  @endif

  chk2 = $('#lopd_check2').is(':checked');
  
  $b2ret = true;

  if(!chk2)
  {
    $b2ret = false;
  }

  if(!chk2)
  {
    $mensaje = "{{trans('area.booking.lopd_check2_no')}}";
  }
  
  if(!$b2ret)
  {
    bootbox.confirm({
      title: "LOPD",
      message: $mensaje,
      buttons: {
        confirm: {
          label: "{{trans('area.booking.lopd_check_seguir')}}",
          className: 'btn-warning'
        },
        cancel: {
          label: "{{trans('area.booking.lopd_check_volver')}}",
          className: 'btn-success'
        }
      },
      callback: function (result) {
        if(result)
        {
          $b2ret = true;
          _callback();
        }
      }
    });
  }
  else
  {
    _callback();
  }

  return $b2ret;
}

    // $('#ficha .form-control').prop( "disabled", true );
    // $('#ficha .form-radio input').prop( "disabled", true );
    // $('#ficha input[type="checkbox"]').prop( "disabled", true );

    $('#btnActualizar').click( function(e) {
        e.preventDefault();

        checkLOPD( function(e) {
            $('#frmActualizar').submit();
        });
    });

</script>

@stop