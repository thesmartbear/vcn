@if(isset($texto))
    {!! Form::label($campo, $texto) !!}
@endif
<br>

<label class="radio-inline">
    {!! Form::radio($campo, '1', false) !!} 1
</label>
<label class="radio-inline">
    {!! Form::radio($campo, '2', false) !!} 2
</label>

<span class="help-block">{{ $errors->first($campo) }}</span>