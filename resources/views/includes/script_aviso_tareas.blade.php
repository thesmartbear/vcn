<div id="app_aviso_tareas">

<div class="modal fade" tabindex="-1" role="dialog" id="Aviso-Tareas">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-warning">Aviso :: Tarea vencida</h4>
            </div>
            <div class="modal-body" id="avisos-info">

                <ul>
                    <li v-for="tarea in tareas_viajero">

                        [@{{tarea.fecha_dmy}}] @{{tarea.notas}} ::
                        <a target='_blank' :href="tarea.info_link">@{{tarea.info_txt}}</a>
                        <br>

                        <button type='button' v-on:click='clickPosponer(1, $event)' class='btn btn-xs btn-info' :data-id=tarea.id data-model='ViajeroTarea'>Posponer 5min.</button>
                        <button type='button' v-on:click='clickPosponer(2, $event)' class='btn btn-xs btn-info' :data-id=tarea.id data-model='ViajeroTarea'>Posponer 15min.</button>
                        <button type='button' v-on:click='clickPosponer(3, $event)' class='btn btn-xs btn-info' :data-id=tarea.id data-model='ViajeroTarea'>Posponer 30min.</button>
                        <button type='button' v-on:click='clickPosponer(4, $event)' class='btn btn-xs btn-info' :data-id=tarea.id data-model='ViajeroTarea'>Posponer 1h.</button>
                        <button type='button' v-on:click='clickPosponer(5, $event)' class='btn btn-xs btn-info' :data-id=tarea.id data-model='ViajeroTarea'>Posponer 1d.</button>

                        {{-- <button type='button' class='btn btn-xs btn-danger' data-dismiss='modal'>Ignorar</button> --}}
                    </li>

                    <li v-for="tarea in tareas_booking">

                        [@{{tarea.fecha_dmy}}] @{{tarea.notas}} ::
                        <a target='_blank' :href="tarea.info_link">@{{tarea.info_txt}}</a>
                        <br>

                        <button type='button' v-on:click='clickPosponer(1, $event)' class='btn btn-xs btn-info' :data-id=tarea.id data-model='BookingTarea'>Posponer 5min.</button>
                        <button type='button' v-on:click='clickPosponer(2, $event)' class='btn btn-xs btn-info' :data-id=tarea.id data-model='BookingTarea'>Posponer 15min.</button>
                        <button type='button' v-on:click='clickPosponer(3, $event)' class='btn btn-xs btn-info' :data-id=tarea.id data-model='BookingTarea'>Posponer 30min.</button>
                        <button type='button' v-on:click='clickPosponer(4, $event)' class='btn btn-xs btn-info' :data-id=tarea.id data-model='BookingTarea'>Posponer 1h.</button>
                        <button type='button' v-on:click='clickPosponer(5, $event)' class='btn btn-xs btn-info' :data-id=tarea.id data-model='BookingTarea'>Posponer 1d.</button>

                        {{-- <button type='button' class='btn btn-xs btn-danger' data-dismiss='modal'>Ignorar</button> --}}
                    </li>
                </ul>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Ignorar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</div>

@push('scripts')
<script type="text/javascript">

function notifOn() {

    if(!Notification )
    {
        return false;
    }

    if( Notification )
    {
        if (Notification.permission !== "granted")
        {
            Notification.requestPermission()
        }
    }
}

function notifTarea() {

    notifOn();

    var title = "VCN";
    var extra = {
        icon: "/assets/logos/{{ConfigHelper::config('logo')}}",
        body: "Tarea vencida"
    }
    var noti = new Notification( title, extra);

    noti.onclick = function(x) { window.focus(); }

    noti.onclose = {
        // Al cerrar
    }

    // setTimeout( function() { noti.close() }, 15000)
}

var vmAvisos = new Vue({
    el: '#app_aviso_tareas',
    data: function() {
        return {
            total: 0,
            data: null,
            tareas_viajero: [],
            tareas_booking: [],
        };
    },
    mounted: function() {

        notifOn();

        this.fetchTareas();
        setInterval(function () {
          this.fetchTareas();
        }.bind(this), 60000);

    },
    destroyed () {

    },
    methods: {
        fetchTareas: function() {

            this.$http.get('{{route('manage.ajax.avisos.tareas')}}').then(function (response) {

                console.log(response.data);

                this.data = response.data;

                if( this.data.total )
                {
                    this.tareas_viajero = this.data.tareas_viajero;
                    this.tareas_booking = this.data.tareas_booking;

                    console.log(this.data.tareas_booking);

                    $("#Aviso-Tareas").modal("show");
                    // this.list.push.apply(this.list, response.data);
                    // alert("Tarea vencida");

                    notifTarea();

                }

            });
        },
        clickPosponer: function(boton, event) {

            data = {
                _token: '{{Session::token()}}',
                modelo: $(event.target).data('model'),
                id: $(event.target).data('id'),
                boton: boton
            };

            this.$http.post('{{route('manage.ajax.avisos.tareas')}}', data).then(function (response) {
                console.log(response.data);
                $("#Aviso-Tareas").modal("hide");
            });
        }
    }
});

</script>
@endpush

