<hr>
<div class="form-group row">
    <div class="col-md-2">
        @include('includes.form_select', [ 'campo'=> 'reserva_tipo', 'texto'=> 'Monto Reserva', 'select'=> ConfigHelper::getTipoOnlineReserva()])
    </div>
    <div class="col-md-3">
        @include('includes.form_input_text', [ 'campo'=> 'reserva_valor', 'texto'=> 'Valor'])
    </div>
    <div class="col-md-7">
        @if(isset($ficha))
            {{$ficha->curso?"Curso: ".$ficha->curso->monto_reserva_txt:""}}
            <br>
            {{$ficha->subcategoria?"SubCategoría: ".$ficha->subcategoria->monto_reserva_txt:""}}
            <br>
            {{$ficha->categoria?"Categoría: ".$ficha->categoria->monto_reserva_txt:""}}
        @endif
    </div>
</div>
<hr>