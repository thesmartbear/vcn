@if(isset($texto))
    {!! Form::label($campo, $texto) !!}
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

{!! Form::select($campo, isset($select)?$select:[], isset($valor)?$valor:(isset($ficha)?$ficha->$campo:0),
        array(
            'id'=> $campo,
            'class' => "select2 form-control ". (isset($sortable)?'select2_sortable':'') ." ". (isset($class)?$class:''),
            'placeholder' => "...",
            'multiple',
            (isset($required)?'required':null),
            (isset($disabled)?'disabled':null)
        )
    ) !!}
<span class="help-block">{{ $errors->first($campo) }}</span>

@if(isset($sortable))

<input type="hidden" name="{{$sortable}}_value" id="{{$sortable}}_value" value='{{$sortable_ids}}'>
* Ordenable

<style>
    #{{$sortable}} .select2-selection__rendered li {
        width: 100%;
    }
</style>

@endif