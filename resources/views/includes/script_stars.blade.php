<script type="text/javascript">
  $(document).ready(function() {

    var rating = {{$rating}};

    $('#stars').raty({
      cancel   : true,
      half     : false,
      starType : 'i',
      number: 5,
      numberMax: 5,
      score: rating,
      click: function(score, evt) {
        $('.rating').val(score);
      }
    });

  });
</script>