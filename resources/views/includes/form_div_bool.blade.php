<div class="form-group">
{{-- <div class="input-group"> --}}
    <span class="input-group-addon" style='border-right: 1px solid #ccc; text-align: left;'>{{$nombre}}:</span>

    <?php
        $valor = isset($valor) ? $valor : $ficha->$campo;

        if( strpos($campo, "campamento_") !== false )
        {
            $valor = ( isset($ficha->datos_campamento[$campo])?$ficha->datos_campamento[$campo]:null );
        }

        $valor = $valor ?: 0;
    ?>

    <div class="form-radio">
        <label class="radio-inline">
            @if( isset($required) && $required )
                {!! Form::radio($campo.'_bool', '1', ($valor), array( 'autocomplete'=>'off', 'required'=> true ) ) !!} SI
            @else
                {!! Form::radio($campo.'_bool', '1', ($valor), array( 'autocomplete'=>'off' ) ) !!} SI
            @endif
        </label>
        <label class="radio-inline">
            {!! Form::radio($campo.'_bool', '0', ($valor==="" || $valor===0 )) !!} NO
        </label>
    </div>

    <div id="{{$campo}}_div" style="display:none;">
    @if($campo == "campamento_alergia")
        
        <br>
        @include('includes.form_div_bool', ['campo'=> "campamento_alergia_1", 'nombre'=> trans('area.campamento.alergia_1'), 'required'=>0 ])
        <br>
        @include('includes.form_div_bool', ['campo'=> "campamento_alergia_2", 'nombre'=> trans('area.campamento.alergia_2'), 'required'=>0 ])
        <br>
        @include('includes.form_div_bool', ['campo'=> "campamento_alergia_3", 'nombre'=> trans('area.campamento.alergia_3'), 'required'=>0 ])

    @elseif($campo == "campo_xxxx")
        
        <br>
        {{-- @include('includes.form_div_bool', ['campo'=> "campo_alergia_1", 'nombre'=> trans('area.campamento.alergia_1'), 'required'=> true ]) --}}
        
    @else
        @if(isset($label))
            {!! Form::label($campo, $label) !!}:
        @else
            {!! Form::label($campo, "Especificar") !!}:
        @endif
        @include('includes.form_textarea', [ 'campo'=> $campo, 'ficha'=> $ficha, 'valor'=> $valor ])
    @endif
    </div>
    <span class="help-block">{{ $errors->first($campo) }}</span>
{{-- </div> --}}
</div>