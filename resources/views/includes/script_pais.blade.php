<script type="text/javascript">
$(document).ready(function() {

    $("#country_id").change(function() {

        $("#city_id > option").each(function(){
            $(this).remove();
        });

        $("#city_id").prop('disabled', true);

        var country_id = $("#country_id").val();

        $.ajax({
          url: "{{route('manage.ciudades.index')}}",
          type: 'GET',
          dataType : 'json',
          data: {'country_id': country_id},
          success: function(data) {

            $.each(data, function(i, item) {

                $("#city_id").append($('<option>', {
                  value: i,
                  text: item
                }));

            });

            $("#city_id").prop('disabled', false);
            $('#city_id').selectpicker('refresh');
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        }); // end ajax call
    });

});
</script>