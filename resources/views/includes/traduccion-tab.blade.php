<ul class="nav nav-tabs" role="tablist">
@foreach(ConfigHelper::idiomas() as $key=>$idioma)

    @if($idioma!=ConfigHelper::idiomaCRM())
    <li role="presentation" class="{{$key==1?'active':''}}"><a href="#trans-{{$idioma}}" aria-controls="ficha" role="trans-{{$idioma}}" data-toggle="tab">{{$idioma}}</a></li>
    @endif

@endforeach
</ul>

<div class="tab-content">
@foreach(ConfigHelper::idiomas() as $key=>$idioma)

    @if($idioma!=ConfigHelper::idiomaCRM())
    <div role="tabpanel" class="tab-pane fade in {{$key==1?'active':''}}" id="trans-{{$idioma}}">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-globe fa-fw"></i> Traduccion :: {{$idioma}}
            </div>
            <div class="panel-body">


                {!! Form::open(array('method'=> 'post', 'files'=> true, 'url' => route('manage.traducciones.post', [$idioma,$modelo,$ficha->id]))) !!}

                    @foreach($campos_text as $campo)
                        @foreach($campo as $ic=>$tc)
                            <div class="form-group">
                            @include('includes.form_input_text',
                                ['valor'=> Traductor::get($idioma, $modelo, $ic, $ficha->id), 'campo' => $idioma."_".$ic, 'texto'=> $tc])
                            </div>
                        @endforeach
                    @endforeach

                    @if(isset($campos_textarea))
                    <hr>
                    @foreach($campos_textarea as $campo)
                        @foreach($campo as $ic=>$tc)
                            <div class="form-group">
                            @include('includes.form_textarea_tinymce',
                                ['valor'=> Traductor::get($idioma, $modelo, $ic, $ficha->id), 'campo' => $idioma."_".$ic, 'texto'=> $tc])
                            </div>
                        @endforeach
                    @endforeach
                    @endif

                    @if(isset($campos_textarea_basic))
                    <hr>
                    @foreach($campos_textarea_basic as $campo)
                        @foreach($campo as $ic=>$tc)
                            <div class="form-group">
                            @include('includes.form_textarea',
                                ['valor'=> Traductor::get($idioma, $modelo, $ic, $ficha->id), 'campo' => $idioma."_".$ic, 'texto'=> $tc])
                            </div>
                        @endforeach
                    @endforeach
                    @endif

                    @if(isset($campos_select))
                    <hr>
                    @foreach($campos_select as $campo)
                        @foreach($campo as $ic=>$tca)
                            @php
                                $tc = $tca['title'];
                                $select = $tca['select'];
                            @endphp
                            <div class="form-group">
                            @include('includes.form_select',
                                ['valor'=> Traductor::get($idioma, $modelo, $ic, $ficha->id), 'campo' => $idioma."_".$ic, 'texto'=> $tc, 'select'=> $select])
                            </div>
                        @endforeach
                    @endforeach
                    @endif

                    @if(isset($campos_file))
                        <hr>
                        @foreach($campos_file as $campo)
                            @foreach($campo as $ic=>$tc)
                                <div class="form-group">
                                    <?php
                                        $valor = Traductor::get($idioma, $modelo, $ic, $ficha->id);
                                    ?>
                                    @include('includes.form_input_file',
                                        ['valor'=> $valor, 'campo' => $idioma."_".$ic, 'texto'=> $tc])

                                    @if($valor)
                                        <br>
                                        <a href="{{$valor }}" target="_blank">{{$valor }}</a>
                                    @endif
                                </div>
                            @endforeach
                        @endforeach
                    @endif

                    <br>
                    @include('includes.form_submit', ['permiso'=> 'traducciones', 'texto'=> 'Guardar'])

                {!! Form::close() !!}



            </div>

        </div>

    </div>
    @endif

@endforeach
</div>