<script type="text/javascript">
function tipo(t)
{
    if(t>0)
    {
        $('#div_dto_tipo_pc').hide();
        $('#div_dto_moneda').show();
    }
    else
    {
        $('#div_dto_tipo_pc').show();
        $('#div_dto_moneda').hide();
    }
}

$(document).ready(function(){

    tipo($('#dto_tipo').val());

    $('#dto_tipo').change( function(e) {

        tipo($(this).val());

    });
});
</script>