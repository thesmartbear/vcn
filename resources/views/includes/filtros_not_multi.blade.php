<div class="row">
    <div class="col-md-2">
        {!! Form::label('not-prescriptores', 'Prescriptor') !!}
        @include('includes.form_input_cargando',['id'=> 'not-prescriptores-cargando'])
        <br>
        {!! Form::select('not-prescriptores', $prescriptores,
            ($valoresNot['prescriptores'] ? explode(',',$valoresNot['prescriptores']) : []),
            array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'data-style'=>'red', 'id'=>'filtro-not-prescriptores', 'name'=> 'not-prescriptores[]'))  !!}

        @include('includes.script_filtros_multi', ['filtro'=> 'plataformas', 'destino'=> 'not-prescriptores'])
        @include('includes.script_filtros_multi', ['filtro'=> 'oficinas', 'destino'=> 'not-prescriptores'])
    </div>
    {{--
    <div class="col-md-3">
        {!! Form::label('not-tipoc', 'Tipo Convocatoria') !!}
        @include('includes.form_input_cargando',['id'=> 'not-tipoc-cargando'])
        <br>
        {!! Form::select('not-cursos', ConfigHelper::getConvocatoriaTipo(), [],
            array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'data-style'=>'red', 'id'=>'filtro-not-tipoc', 'name'=> 'not-tipoc[]'))  !!}
    </div>
    --}}

    <div class="col-md-4">
        {!! Form::label('not-cursos', 'Cursos') !!}
        @include('includes.form_input_cargando',['id'=> 'not-cursos-cargando'])
        <br>
        {!! Form::select('not-cursos', $cursos, 
            ($valoresNot['cursos'] ? explode(',',$valoresNot['cursos']) : []),
            array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'data-style'=>'red', 'id'=>'filtro-not-cursos', 'name'=> 'not-cursos[]')) !!}
    </div>

    @if(isset($paisesm))
    <div class="col-md-4">
        {!! Form::label('not-paises', 'Paises') !!}
        <br>
        {!! Form::select('not-paises', $paisesm,
            ($valoresNot['paises'] ? explode(',',$valoresNot['paises']) : []),
            array('class'=>'multiselect form-control', 'multiple'=>'multiple', 'data-style'=>'green', 'id'=>'filtro-not-paises', 'name'=> 'not-paises[]')) !!}
    </div>
    @endif
    
</div>