<div class="form-group row">
    <div class="col-md-2">
    {!! Form::label('plataformas', 'Plataforma') !!}
    <br>
    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
    @include('includes.script_filtros_multi', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'proveedores'])
    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'centros'])
    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'cursos'])
    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'prescriptores'])
    </div>

    <div class="col-md-8">
    {!! Form::label('oficinas', 'Oficinas') !!}
    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])
    <br>
    {{-- {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'id'=>'filtro-oficinas'))  !!} --}}
    {!! Form::select('oficinas', $oficinas, ($valores['oficinas']?explode(',',$valores['oficinas']):[]), array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-oficinas', 'name'=> 'oficinas[]'))  !!}

    @include('includes.script_filtros', ['filtro'=> 'oficinas', 'destino'=> 'prescriptores'])
    </div>

    <div class="col-md-2">
        @include('includes.form_select', ['campo'=> 'directos', 'texto'=> 'Booking directos', 'valor'=> $valores['directos'], 'select'=> [1=> 'Incluir', 0=> 'Excluir', 2=> 'Sólo directos'] ])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-4">
    {!! Form::label('prescriptores', 'Prescriptor') !!}
    @include('includes.form_input_cargando',['id'=> 'prescriptores-cargando'])
    <br>
    {!! Form::select('prescriptores', $prescriptores, $valores['prescriptores'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-prescriptores'))  !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-md-2">
    {!! Form::label('tipoc', 'Tipo Convocatoria') !!}
    @include('includes.form_input_cargando',['id'=> 'tipoc-cargando'])
    <br>
    {!! Form::select('tipoc', ConfigHelper::getConvocatoriaTipo(), $valores['tipoc'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-tipoc'))  !!}
    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'centros'])
    @include('includes.script_filtros_multi', ['filtro'=> 'tipoc', 'destino'=> 'cursos'])
    @include('includes.script_filtros_multi', ['filtro'=> 'tipoc', 'destino'=> 'convocatorias'])
    </div>

    <div class="col-md-2">
    {!! Form::label('categorias', 'Categoría') !!}
    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
    <br>
    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
    @include('includes.script_filtros_multi', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias'])
    </div>

    <div class="col-md-2">
    {!! Form::label('subcategorias', 'Subcategoría') !!}
    @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
    <br>
    {!! Form::select('subcategorias', $subcategorias, $valores['subcategorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
    @include('includes.script_filtros_multi', ['filtro'=> 'subcategorias', 'destino'=> 'cursos'])
    </div>

    <div class="col-md-4">
    {!! Form::label('proveedores', 'Proveedor') !!}
    @include('includes.form_input_cargando',['id'=> 'proveedores-cargando'])
    <br>
    {!! Form::select('proveedores', $proveedores, $valores['proveedores'], array('class'=>'select2', 'data-style'=>'purple', 'id'=>'filtro-proveedores'))  !!}
    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'centros'])
    @include('includes.script_filtros_multi', ['filtro'=> 'proveedores', 'destino'=> 'cursos'])
    </div>

</div>

<div class="form-group row">
    <div class="col-md-5">
    {!! Form::label('centros', 'Centro') !!}
    @include('includes.form_input_cargando',['id'=> 'centros-cargando'])
    <br>
    {!! Form::select('centros', $centros, $valores['centros'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-centros'))  !!}
    @include('includes.script_filtros_multi', ['filtro'=> 'centros', 'destino'=> 'cursos'])
    @include('includes.script_filtros_multi', ['filtro'=> 'centros', 'destino'=> 'convocatorias'])
    </div>

    <div class="col-md-2">
    {!! Form::label('cursos', 'Curso') !!}
    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])
    <br>
    {{-- {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!} --}}
    {!! Form::select('cursos', $cursos, ($valores['cursos']?explode(',',$valores['cursos']):[]), array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-cursos', 'name'=> 'cursos[]'))  !!}
    @include('includes.script_filtros_multi', ['filtro'=> 'cursos', 'destino'=> 'convocatorias'])
    </div>
</div>

<div class="form-group row">
    <div class="col-md-12">
    {!! Form::label('convocatorias', 'Convocatoria') !!}
    @include('includes.form_input_cargando',['id'=> 'convocatorias-cargando'])
    <br>
    {{-- {!! Form::select('convocatorias', $convocatorias, $valores['convocatorias'], array('class'=>'select2 col-md-12', 'data-style'=>'red', 'id'=>'filtro-convocatorias'))  !!} --}}
    {!! Form::select('convocatorias', $convocatorias, ($valores['convocatorias']?explode(',',$valores['convocatorias']):[]), array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-convocatorias', 'name'=> 'convocatorias[]'))  !!}
    </div>
</div>

<div class="form-group row">
    @if(isset($paises))
    <div class="col-md-4">
        {!! Form::label('paises', 'Paises') !!}
        <br>
        {!! Form::select('paises', $paises, $valores['paises'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-paises', 'name'=> 'paises'))  !!}
    </div>
    @endif

    @if(isset($anys))
    <div class="col-md-1">
        {!! Form::label('any', 'Año') !!}
        <br>
        {!! Form::select('any', $anys, $valores['any'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-any'))  !!}
    </div>
    @endif

    @if(isset($paisesm))
    <div class="col-md-4">
        {!! Form::label('paises', 'País') !!}
        <br>
        {!! Form::select('paises', $paisesm, $valores['paises'], 
            array('class'=>'multiselect form-control', 'multiple'=>'multiple', 'data-style'=>'green', 'id'=>'filtro-paises', 'name'=> 'paises[]'))  !!}
    </div>
    @endif
</div>

<hr>
@if($filtro_fechas)
<div class="form-group row">

    <div class="col-md-3">
        {!! Form::label('desde','Desde') !!}
        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
    </div>
    <div class="col-md-3">
        {!! Form::label('hasta','Hasta') !!}
        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
    </div>

    <div class="col-md-1 col-md-offset-1">
        {!! Form::label('&nbsp;') !!}
        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
    </div>

</div>
@endif