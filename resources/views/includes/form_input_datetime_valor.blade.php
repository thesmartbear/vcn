@if(isset($texto))
{!! Form::label($campo, $texto) !!}
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

<?php $valor = isset($valor)?$valor:(isset($ficha)?$ficha->$campo:old($campo)); ?>

<div class='input-group date datetime'>
    {!! Form::text($campo, $valor, array( 'id'=>$campo,
    'placeholder' => isset($placeholder)?$placeholder:(isset($texto)?$texto:""),
    'class' => 'form-control', (isset($required)?'required':''), (isset($disabled)?'disabled':''))) !!}
    <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
    </span>
</div>

<span class="help-block">{{ $errors->first($campo) }}</span>