<script type="text/javascript">
$(document).ready(function() {

  $('#select-oficinas-filtro option:contains(TODAS), #select-oficinas-filtro option:contains(SIN ASIGNAR)').css('font-weight', 'bold');

  $('#select-oficinas-filtro').change(function(e) {
    e.preventDefault();

    var $any = "";
    var $a = $('#select-any-filtro').val();
    if($a != null)
    {
      $any = "?any=" + $a;
    }

    var $d = "";
    var $d = $('#desde').val();
    if($d != null)
    {
      var $h = $('#hasta').val();
      $any = "?desde=" + $d + "&hasta=" + $h;
    }

    var val = $(this).val();
    if(val == 0)
    {
      return;      
    }

    location.href = val + $any;

  });

});
</script>