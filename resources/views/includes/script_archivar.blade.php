<div class="modal fade" id="modalArchivar">
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            {{-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> --}}
            <h4 class="modal-title">Motivo para archivar</h4>
        </div>
        <div class="modal-body">

          @include('includes.form_select', ['campo'=> 'motivo', 'texto'=> '', 'select'=> ConfigHelper::getArchivarMotivo()])

          <div id="motivo_nota_div" class="collapse">
            @include('includes.form_input_text', [ 'campo'=> 'motivo_nota', 'texto'=> 'Motivo otro'])
          </div>

        </div>
        <div class="modal-footer">
            {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button> --}}
            <button id="archivar-ok" type="button" class="btn btn-default" data-dismiss="modal" disabled>Aceptar</button>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function() {

  @if($booking)
    var $archivado = "{{ConfigHelper::config('booking_status_archivado')}}";
  @else
    var $archivado = "{{ConfigHelper::config('solicitud_status_archivado')}}";
  @endif

  $("#status_id").change(function() {

    if( $(this).val() == $archivado )
    {
      $('#modalArchivar').modal('show');
    }

  });

  $("#motivo").change(function() {
    if($(this).val() == 'otro')
    {
      $("#motivo_nota_div").show();
    }
    else
    {
      $("#motivo_nota_div").hide();
    }
    $('#archivar-ok').removeAttr('disabled');
  });

  $('#archivar-ok').click( function(e) {
    e.preventDefault();

    $("input[name='archivar_motivo']").val( $('#modalArchivar #motivo').val() );
    $("input[name='archivar_motivo_nota']").val( $('#modalArchivar #motivo_nota').val() );

    $('#submit_status').click();
  });

});
</script>