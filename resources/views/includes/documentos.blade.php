@foreach(ConfigHelper::getIdiomaContactoArea() as $kidioma => $vidioma)

<?php $idioma = strtolower($kidioma); ?>

<div class="panel panel-default {{strtolower($idioma)}}color">
    <div class="panel-heading">
        <h4>Documentos [{{$vidioma}}]: {{$ficha->getDocumentos($idioma)->count()}}</h4>
        <i>{{$kidioma=="-"?'Se mostrará para todos los bookings.':('Se mostrará para los bookings con idioma contacto '.$vidioma)}}</i>
    </div>
    <div class="panel-body">

        <div class="row">
        <div class="col-md-12">
            <?php

            $folder = "/assets/uploads/documentos/". strtolower($idioma) ."/". $modelo ."/". $modelo_id;
            $path = public_path() . $folder;

            ?>

            <table class="table table-bordered table-hover">
            <?php
            foreach($ficha->getDocumentos($idioma) as $archivo)
            {
                $result = $archivo->doc;
                $file = $path . '/' . $result;

                $btn_class = "btn-xs btn-danger";
                if($archivo->visible == 1)
                {
                    $btn_class = "btn-xs btn-success";
                }

                ?>

                @if(is_file($file))
                <tr>
                <td class="col-sm-7">
                    @if(getimagesize($file))
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="{{$folder}}/{{$result}}">
                            </a>
                        </div>
                    @endif

                    {{$archivo->doc}}
                    <hr>
                    <i>Subido por: {{$archivo->user->full_name ?? "-"}} <br> {{$archivo->created_at->format("d/m/Y H:i:s")}}</i>
                    <i> - Plataforma: {{$archivo->plataforma_name}}</i>
                </td>
                <td class="col-md-2">
                    {!! Form::model($archivo, array('route' => array('manage.system.docs.plataforma', $archivo->id))) !!}
                        @include('includes.form_select', ['campo'=> 'plataforma', 'texto'=> null, 'select'=> ConfigHelper::plataformas(), 'valor'=> $archivo->plataforma])
                        {!! Form::submit('Cambiar', array( 'name'=>'submit_plataforma', 'class' => 'btn btn-xs btn-success pull-right')) !!}
                    {!! Form::close() !!}
                </td>
                <td class="col-sm-3">
                    <a href="{{ route('manage.system.docs.delete', [$archivo->id]) }}" class="btn btn-danger btn-xs" role="button">Borrar</a>
                    <a href="{{ route('manage.system.docs.visible', [$archivo->id]) }}" class="btn {{$btn_class}} pull-right" role="button">{{($archivo->visible==1?"Visible":"No visible")}}</a>
                    <hr>
                    <a class="btn btn-success btn-block" href="/assets/uploads/documentos/{{$archivo->idioma}}/{{$archivo->modelo}}/{{$archivo->modelo_id}}/{{$archivo->doc}}" target="_blank" download>
                        <i class="fa fa-download"></i> Descargar
                    </a>
                </td>
                </tr>
                @endif

                <?php
            }
            ?>
            </table>

        </div>
        </div>

        <div class="row">
        <div class="col-md-12">
            <h4>Añadir documentos [{{$idioma}}]</h4>

            <div class="dropzone" id="myDropzone-{{$idioma}}-{{$modelo}}-{{$modelo_id}}"></div>

            @include('includes.script_dropzone_docs', ['name'=> 'myDropzone-'.$idioma."-".$modelo."-".$modelo_id, 'url'=> route('manage.system.docs.upload', [$modelo,$modelo_id]), 'idioma'=> $idioma])
        </div>
        </div>

    </div>
</div>

@endforeach