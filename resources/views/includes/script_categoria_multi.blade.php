<script type="text/javascript">
$(document).ready(function() {

    var url = "{{$url}}";

    $("#course_category").change(function() {

        $("#course_subcategory").empty();

        $("#course_subcategory").multiselect('destroy');//.prop('disabled', true);

        var category_id = $("#course_category").val();

        if(!category_id)
        {
            $("#course_subcategory").multiselect({
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true
            });
            return;
        }

        category_id = category_id.join(",");

        $.ajax({
          url: url,
          type: 'GET',
          dataType : 'json',
          data: {'category_id': category_id},
          success: function(data) {

            $.each(data, function(i, item) {

                $("#course_subcategory").append($('<optgroup>', {
                  label: i
                }));

                $.each(item, function(i, item) {

                    $("#course_subcategory").append($('<option>', {
                      value: i,
                      text: item
                    }));

                });


            });

            $("#course_subcategory").multiselect({
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true
            });
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        }); // end ajax call
    });

});
</script>