<div id="checklist">
@if($ficha->status)

    @if($ficha->es_booking)

        <ul class="checklist">
        @php $stId = 0; @endphp
        @foreach($ficha->status->getChecklistBooking($booking_id) as $check)
            
            @if($stId != $check->status_id)
                <li><br>:: [{{$check->status->name}}] ::</li>
            @endif
                
            <li>
                <i class="{{$ficha->getStatusChecklistClass($check->id)}} status-check" id="check-{{$check->id}}"
                    data-parent="{{ $ficha->es_booking ? $booking_id : $ficha->solicitud_id }}"
                    data-id="{{$check->id}}"></i>
                {{$check->name}}

                {{$ficha->getStatusChecklistFecha($check->id)}}
            </li>

            @php $stId = $check->status_id; @endphp

        @endforeach
        </ul>

    @else

        <ul class="checklist">
        @php $stId = 0; @endphp
        @if($ficha->status)
        @foreach($ficha->status->getChecklistSolicitud($ficha->solicitud_id) as $check)

            @if($stId != $check->status_id)
                <li>.:: [{{$check->status->name}}] ::.</li>
            @endif

            <li>
                <i class="{{$ficha->getStatusChecklistClass($check->id)}} status-check" id="check-{{$check->id}}"
                    data-parent="{{ $ficha->es_booking ? $booking_id : $ficha->solicitud_id }}"
                    data-id="{{$check->id}}"></i>
                {{$check->name}}

                {{$ficha->getStatusChecklistFecha($check->id)}}
            </li>

            @php $stId = $check->status_id; @endphp

        @endforeach
        @endif
        </ul>

    @endif

@endif
</div>

<script type="text/javascript">
function postChecklist( id, checkid, fecha )
{
    var $token = "{{ Session::token() }}";

    var $data = { '_token': $token, 'id': id, 'check_id': checkid, 'check_fecha': fecha };

    @if($ficha->es_booking)
        var $url = "{{route('manage.bookings.ajax.checklist', $ficha->booking_id)}}";
    @else
        var $url = "{{route('manage.solicitudes.ajax.checklist', $ficha->solicitud_id)}}";
    @endif

    // console.log($data);

    $.ajax({
        url: $url,
        type: 'POST',
        dataType : 'json',
        data: $data,
        success: function(data) { console.log(data);

            if(data.result)
            {
                $("#check-"+data.check_id).removeClass().addClass('fa fa-check-circle-o fa-fw');
                h = $("#check-"+data.check_id).parent().html();
                $("#check-"+data.check_id).parent().html(h + "["+ data.fecha +"] ("+ data.user +")");
            }
            else
            {
                $("#check-"+data.check_id).removeClass().addClass('fa fa-circle-thin fa-fw');
            }

            if(data.status>0)
            {
                $('#status_id').val(data.status);
                location.reload();
            }

            @if(isset($booking_id))
                var newUrl = "{{route('manage.bookings.logs.index', [$booking_id, 'checklist' ])}}";
                $('#Dtt-logs-{{$booking_id}}-checklist').DataTable().ajax.url( newUrl ).load();
            @endif

        },
        error: function(xhr, desc, err) {
            console.log(xhr.responseText);
            console.log("Details: " + desc + "\nError:" + err);
        }

    }); // end ajax call
}

$(document).ready(function() {

    $(".status-check").click(function(e) {

        var $id = $(this).data('parent');
        var $checkid = $(this).data('id');

        $("#check-"+$checkid).removeClass().addClass('fa fa-spinner fa-spin fa-fw');

        var $input = "<div id='Dtt-logs-datetime' class='input-group date datetime'><input id='checklist-fecha' name='checklist-fecha' class='form-control' value='{{Carbon::now()->format('d/m/Y')}}'></input><span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span></div>";

        bootbox.dialog({
           message: $input,
           title: "Fecha Checklist",
           buttons: {
                main: {
                    label: "Aceptar",
                    className: "btn-success",
                    callback: function() {
                        postChecklist($id,$checkid,$('#checklist-fecha').val());
                    }
                }
           }
        })

        $("#Dtt-logs-datetime").datetimepicker({
            locale: 'es',
            format: 'L',
        });

    });

});
</script>