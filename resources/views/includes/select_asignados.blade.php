@if( !auth()->user()->es_aislado )
<div class="col-md-3">
    
    <?php
        foreach($oficinas as $a)
        {
            $o[route($route, [$status_id,$user_id,$a->id])] = $a->name;
        }
        
        $o[route($route, [$status_id,$user_id,'all'])] = 'TODAS';
        $o1 =  route($route, [$status_id,$user_id,$oficina_id?$oficina_id:'all']);
    ?>

    @if( auth()->user()->filtro_oficinas )
        {!! Form::select('oficinas', $o, $o1, array('class'=>'select2 show-tick pull-right', 'data-style'=>'blue', 'id'=>'select-oficinas-filtro'))  !!}
    @endif
</div>

<div class="col-md-3">

    <?php

        if($oficina_id>0)
        {
            $asignados = \VCN\Models\User::asignados()->where('oficina_id',$oficina_id)->orderBy('fname')->get();
        }
        else
        {
            $asignados = \VCN\Models\User::asignados()->get();
        }


        foreach($asignados as $a)
        {
            $u[route($route, [$status_id,$a->id,$oficina_id])] = $a->full_name;
        }

        /*
        $asignadosOff = \VCN\Models\User::asignadosOff()->get();
        if((int)$oficina_id>0)
        {
            $asignadosOff = $asignadosOff->where('oficina_id', (int)$oficina_id);
        }
        */

        $u[route($route, [$status_id,'all',$oficina_id])] = 'TODOS';
        $u[route($route, [$status_id,'all-off',$oficina_id])] = 'NO ACTIVOS';
        $u1 = route($route, [$status_id,$user_id,$oficina_id]);
    ?>

    {!! Form::select('asignados', $u, $u1, array('class'=>'select2 show-tick pull-right', 'data-style'=>'green', 'id'=>'select-asignados-filtro'))  !!}

    
    <?php /*
    <script type="text/javascript">

    $(document).ready( function(){

        <?php
            if($asignadosOff->count())
            {
                ?>
                $('#select-asignados-filtro').append('<optgroup label="NO ACTIVOS:"></optgroup>');
                <?php
            }

            foreach($asignadosOff as $a)
            {
                $r = route($route, [$status_id,$a->id,$oficina_id]);
                $u = $a->full_name;

                $opt = "<option value='". $r ."'>". $u ."</option>";
                ?>
                $('#select-asignados-filtro').append("{!! $opt !!}");
                <?php
            }
        ?>
    
        $('#select-asignados-filtro').val("{!! $u1 !!}").trigger('change');

    });
    </script>
    */ ?>

</div>
@endif

@include('includes.script_filtro_asignados')
@include('includes.script_filtro_oficinas')