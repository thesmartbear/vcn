<td align='left'>
    {{$extra->tipo_unidad_name}}
    @if($extra->tipo_unidad==1)
        ({{$extra->unidad_name}})
        <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda) }}</i>

        <input type="number" min="0" max="{{$max_duracion}}" class="booking-extra-unidades {{ $ficha->extra_activo($extra->id, $tipo)?'visible':'' }}"
            id="{{$prefix}}-unidades-{{$extra->id}}" name="{{$prefix}}-unidades-{{$extra->id}}"
            value="{{ $ficha->extra_unidades($extra->id,$tipo) }}"
            data-id="{{$extra->id}}" data-tipo={{$tipo}} data-unidad="{{$extra->tipo_unidad}}" data-precio="{{$extra->precio}}" data-moneda="{{$extra->moneda}}" size="3">

    @elseif($extra->tipo_unidad==2)
        <i>{{$extra->precio}} %</i>
    @else
        <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda) }}</i>
    @endif
</td>