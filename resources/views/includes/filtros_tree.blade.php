<?php
$oficinas = \VCN\Models\System\Oficina::plataforma();
//$users = \VCN\Models\User::asignados()->get()->pluck('full_name','id')->toArray();

$origenes = \VCN\Models\Leads\Origen::plataforma();
$categorias = \VCN\Models\Categoria::plataforma();
$paises = \VCN\Models\Pais::get()->pluck('name','id')->toArray();

$categorias = ConfigHelper::arrayToJson($categorias, "categorias");
$origenes = ConfigHelper::arrayToJson($origenes, "origenes");
$oficinas = ConfigHelper::arrayToJson($oficinas, "oficinas");
$paises = ConfigHelper::arrayToJson($paises);

//$any = intval($request->input('any',Carbon::now()->format('Y')));
$anys = \VCN\Models\Informes\Venta::groupBy('any')->get()->pluck('any','any')->toArray();
$anys[end($anys)+1] = end($anys)+1;
asort($anys);
$anys = ConfigHelper::arrayToJson($anys);

?>
<div id="test">

    <div class="row">
        <div class="col-md-3">
            <treeselect v-model="val_oficinas" :multiple="true" :clearable="true" :options="opt_oficinas" :flat="true" placeholder="Oficinas" />
        </div>
        <div class="col-md-3">
            <treeselect v-model="val_origenes" :multiple="true" :clearable="true" :options="opt_origenes" :flat="true" placeholder="Orígenes" />
        </div>
        <div class="col-md-3">
            <treeselect v-model="val_categorias" :multiple="true":clearable="true" :options="opt_categorias" :flat="true" placeholder="Categorías" />
        </div>
        <div class="col-md-3">
            <treeselect v-model="val_paises" :multiple="true" :clearable="true" :options="opt_paises" :flat="true" placeholder="Paises" />
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-2">
            <treeselect v-model="val_anys" :multiple="false" :clearable="true" :options="opt_anys" placeholder="Año" />
        </div>
        <div class="col-md-1 pull-right">
            <div class="btn btn-success" v-on:click='filtrar'>Filtrar</div>
        </div>
    </div>

    <hr>

</div>

@push('scripts')
    <script type="text/javascript">

        {{--console.log(@json($valores['categorias']));--}}

        var route = '{{ $route }}';

        Vue.component('treeselect', VueTreeselect.Treeselect)

        new Vue({
            el: '#test',
            data: {
                val_oficinas: @json($valores['oficinas']),
                val_origenes: @json($valores['origenes']),
                val_categorias: @json($valores['categorias']),
                val_paises:@json($valores['paises']),
                val_anys: '{{ $valores['anys'] }}',
                opt_oficinas: {!! $oficinas !!},
                opt_origenes: {!! $origenes !!},
                opt_categorias: {!! $categorias !!},
                opt_paises: {!! $paises !!},
                opt_anys: {!! $anys !!},
            },
            mounted: function() {

            },
            methods: {
                filtrar() {

                    data = {
                        //_token: '{{ csrf_token() }}',
                        'oficinas': this.val_oficinas || '',
                        'origenes': this.val_origenes || '',
                        'categorias': this.val_categorias || '',
                        'paises': this.val_paises || '',
                        'anys': this.val_anys || '',
                    }

                    // this.$http.post(route, data).then(function (response) {
                    //     console.log(response.data);
                    // });

                    url = Object.keys(data).map(function(k) {
                        return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
                    }).join('&')

                    // console.log(route +"?"+ url)
                    location.href = route +"?"+ url

                }
            }
        })

    </script>
@endpush