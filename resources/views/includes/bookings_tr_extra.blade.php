<tr>
    <td>{{$extra->name}}</td>
    <td>
        {{$extra->tipo_unidad_name}}

        @if($extra->tipo_unidad)
            &nbsp;({{$extra->unidad_name}})
        @endif

        <br>
        @if($extra->tipo_unidad==1)
            <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
            x{{$extra->unidades}}
        @elseif($extra->tipo_unidad==2)
            <i>{{$extra->porcentaje}} %</i>
        @else
            <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
        @endif

    </td>
    <td align="right">
        @if($extra->tipo_unidad==2)
            @if(isset($ficha->total['total_descontar']))
                {{ ConfigHelper::parseMoneda((($extra->precio * $ficha->total['total_descontar'])/100), $extra->moneda_name) }}
            @else
                {{ ConfigHelper::parseMoneda(($extra->precio), $extra->moneda_name) }}
            @endif
        @else
            {{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda_name) }}
        @endif
    </td>
</tr>