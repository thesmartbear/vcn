<script type="text/javascript">

var $u = 'all';
var $o = 'all';

$(document).ready(function() {

  // $('#select-asignados-filtro option:contains(TODOS), #select-asignados-filtro option:contains(SIN ASIGNAR)').css('font-weight', 'bold');
  // $('#select-asignados-filtro option:contains(NO ACTIVOS)').prop('disabled', 'disabled');

  $('#select-asignados-filtro').change(function(e) {
    e.preventDefault();

    var $any = "";
    var $a = $('#select-any-filtro').val();
    if($a != null)
    {
      $any = "?any=" + $a;
    }

    var $d = "";
    var $d = $('#desde').val();
    if($d != null)
    {
      var $h = $('#hasta').val();
      $any = "?desde=" + $d + "&hasta=" + $h;
    }

    var val = $(this).val();
    if(val == 0)
    {
      return;      
    }

    location.href = val + $any;

  });

  // $('#select-resumen-filtro option:contains(TODOS)').css('font-weight', 'bold');

  $('#select-resumen-filtro').change(function(e) {
    e.preventDefault();

    $u = $(this).val();
    if($u == 0)
    {
      return;      
    }
    $o = $('#select-resumen-oficina').val();

    window.location.href = "/manage/resumen?u=" + $u + "&o=" + $o;

  });


  // OFICINA

  // $('#select-resumen-oficina option:contains(TODAS)').css('font-weight', 'bold');

  $('#select-resumen-oficina').change(function(e) {
    e.preventDefault();

    $u = $('#select-resumen-filtro').val();
    $o = $(this).val();

    window.location.href = "/manage/resumen?u=" + $u + "&o=" + $o;

  });

});
</script>