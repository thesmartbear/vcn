@if( isset($texto) && isset($required) )
    {!! Form::label($campo, $texto." *") !!}
@elseif( isset($texto) )
    {!! Form::label($campo, $texto) !!}
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

@if(!isset($valor))
    @if(isset($ficha))
        @if($ficha->$campo)
            <?php
                $valor = ($ficha->$campo!='0000-00-00')?Carbon::parse($ficha->$campo)->format('d/m/Y'):old($campo);
            ?>
        @else
            <?php
                $valor = old($campo);
            ?>
        @endif
    @else
        <?php
            $valor = old($campo);
        ?>
    @endif
@endif


<div class='input-group date datetime'>

    {!! Form::text($campo, $valor, array(
        'id'=> isset($id)?$id:$campo,
        'placeholder' => isset($placeholder)?$placeholder:(isset($texto)?$texto:""),
        'class' => 'form-control', (isset($required)?'required':null), (isset($disabled)?'disabled':null))) !!}

    <span class="input-group-addon">
        <i class="fas fa-calendar-alt"></i>
    </span>
</div>

<span class="help-block">{{ $errors->first($campo) }}</span>