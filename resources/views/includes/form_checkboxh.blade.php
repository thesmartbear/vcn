@if( isset($position) && $position == 'right' )

    @if( isset($texto) && isset($required) )
        {!! Form::label($campo, $texto." *") !!}
    @elseif( isset($texto) )
        {!! Form::label($campo, $texto) !!}
    @endif

@endif

<input name="{{$campo}}" type="hidden" value="0">
{!! Form::checkbox($campo, true, (isset($valor)?$valor:(isset($ficha)?$ficha->$campo:old($campo))),
    array(
        'id'=>$campo,
        'class' => isset($novisible)?'novisible':'',
            ((isset($required) && $required)?'required':''), (isset($disabled)?'disabled':''),
            (isset($checked)?($checked?'checked':''):'')
    )
) !!}

@if( !isset($position) || (isset($position) && $position == 'left') )

    @if( isset($html) && $html )
        {!! $texto !!}
    @elseif( isset($texto) && isset($required) )
        {!! Form::label($campo, $texto." *") !!}
    @elseif( isset($texto) )
        {!! Form::label($campo, $texto) !!}
    @endif

@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

<span class="help-block">{{ $errors->first($campo) }}</span>