<script type="text/javascript">
$(document).ready(function() {

  $('#select-filtro-status option:contains(TODOS)').css('font-weight', 'bold');

  $('#select-filtro-status').change(function(e) {
    e.preventDefault();

    window.location.href = '/manage/viajeros/excel/' + $(this).val();

  });

});
</script>