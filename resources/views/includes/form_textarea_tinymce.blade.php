{!! Form::label($campo, $texto) !!}:

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

{!! Form::textarea($campo, (isset($valor)?$valor:(isset($ficha)?$ficha->$campo:old($campo))), ['id'=> $campo, 'placeholder'=> $texto, 'class' => 'form-control myTextEditor', 'rows'=> 4, 'cols'=> 50]) !!}

@push('scripts')
<script type="text/javascript">

    var myTags = JSON.parse('{!! ConfigHelper::tipoMailTags() !!}');

    var myPlugins = ["code","table", "link", "lists", "image", "wordcount", "media"];
    var myToolbar = "code | bold italic | styleselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | link | image media";

    @if(isset($tags) && $tags)
        myPlugins.push( "vcntags" );
        myToolbar += " | vcntags";
    @endif

    var editor_config = {
        forced_root_block : false,
        path_absolute : "/manage/",
        // selector: "textarea.my-editor",
        mode : "specific_textareas",
        editor_selector : 'myTextEditor',
        language: 'es',
        menubar : false,
        height: "420",

        plugins: myPlugins,
        toolbar: myToolbar,
        // toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",

        relative_urls: false,
        convert_urls: false,

        file_picker_types: 'file image', //media
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            if(type=='media') return;

            var cmsURL = editor_config.path_absolute + 'filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };

    tinymce.init(editor_config);

    @if(isset($tags) && $tags)
    tinymce.PluginManager.add('vcntags', function(editor, url) {
        // Add a button that opens a window
        editor.addButton('vcntags', {
            type: 'listbox',
            text: 'vcnTags',
            icon: false,
            onselect: function (e) {console.log(e);
                var tag = "[tag]"+ this.value() +"[/tag]";
                // var tag = this.value();
                editor.insertContent(tag);
            },
            values: myTags,
            onPostRender: function () {
                // Select the second item by default
                // this.value('&nbsp;<em>Some italic text!</em>');
            }
        });
    });
    @endif

</script>
@endpush
