<div class="form-group row">
<div class="col-md-12">

{!! Form::open(array('method' => 'POST', 'url' => route('manage.system.contactos.asignar'), 'role' => 'form', 'class' => '')) !!}            

{!! Form::hidden('modelo', $modelo) !!}
{!! Form::hidden('modelo_id', $modelo_id) !!}

@if(!isset($noProveedor) || (isset($noProveedor) && !$noProveedor)) 
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-book"></i> Contacto SOS Proveedor

    </div>
    <div class="panel-body">

            <?php
                $c = \VCN\Models\System\ContactoModel::where('es_proveedor', 1)->where('modelo', $modelo)->where('modelo_id', $modelo_id)->pluck('contacto_id')->toArray();
                $cn = \VCN\Models\System\ContactoModel::where('es_proveedor', 1)->where('modelo', $modelo)->where('modelo_id', $modelo_id)->first();
                $select = \VCN\Models\System\Contacto::activos()->where('es_proveedor',1)->get()->pluck('full_name','id')->toArray();
                $cid = $c ?: [];
                $cn = $cn ? $cn->notas : "";
            ?>

            @include('includes.form_select2_multi', ['campo'=> 'contacto[p][]', 'texto'=> 'Proveedor', 'valor'=> $cid, 'select'=> [0=> 'Ninguno'] + $select ])

            @include('includes.form_textarea', ['campo'=> "notas[p]", 'texto'=> 'Notas', 'valor'=> $cn ])

    </div>
</div>
@endif
   
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-book"></i> Contacto SOS Plataforma

    </div>
    <div class="panel-body">

            <?php $cn = ""; ?>

            @foreach(ConfigHelper::plataformas() as $keyp=>$plataforma)

                <?php
                    $c = \VCN\Models\System\ContactoModel::where('es_proveedor', 0)->where('plataforma', $keyp)->where('modelo', $modelo)->where('modelo_id', $modelo_id)->first();
                    $cid = $c ? $c->contacto_id : 0;
                    $cn = $c ? $c->notas : $cn;
                ?>
                @include('includes.form_select2', ['campo'=> "contacto[$keyp]", 'texto'=> $plataforma, 'valor'=> $cid, 'select'=> [0=> 'Ninguno'] + \VCN\Models\System\Contacto::activos()->where('es_proveedor',0)->get()->pluck('full_name','id')->toArray() ])

            @endforeach

            @include('includes.form_textarea', ['campo'=> "notas[plat]", 'texto'=> 'Notas', 'valor'=> $cn ])

    </div>
</div>

<div class="form-group">
@include('includes.form_submit', [ 'permiso'=> 'sos', 'texto'=> 'Guardar contactos'])
</div>

</div>
</div>

{!! Form::close() !!}

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-book"></i> Nuevo Contacto SOS

    </div>
    <div class="panel-body">

        {!! Form::open(array('method' => 'POST', 'url' => route('manage.system.contactos.ficha',0), 'role' => 'form', 'class' => '')) !!}
        
            {!! Form::hidden('es_tab', 1) !!}

            <div class="form-group row">
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'contacto_name', 'texto'=> 'Nombre', 'valor'=> ''])
                </div>
                <div class="col-md-4">
                    @include('includes.form_input_text', [ 'campo'=> 'contacto_phone', 'texto'=> 'Teléfono', 'valor'=> ''])
                </div>

                <div class="col-md-2">
                    @include('includes.form_checkbox', [ 'campo'=> 'es_proveedor', 'texto'=> 'Proveedor'])
                </div>
                <div class="col-md-1">
                    @include('includes.form_checkbox', [ 'campo'=> 'activo', 'texto'=> 'Activo'])
                </div>

                <div class="col-md-1">
                    @include('includes.form_submit', [ 'permiso'=> 'sos', 'texto'=> 'Crear'])
                </div>
            </div>

        {!! Form::close() !!}

    </div>
</div>