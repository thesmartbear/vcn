<script type="text/javascript">
$(document).ready(function() {

  var $token = "{{ Session::token() }}";
  var viajero_id = {{$viajero_id}};

  $( "#tutor1" ).autocomplete({
      source: "{{ route('manage.tutores.autocomplete') }}",
      minLength: 2,
      // response: function(event, ui) {
      //   console.log(ui);
      // },
      create: function() {
          $(this).data('ui-autocomplete')._renderItem = function( ul, item ) {
              return $( "<li>" )
                  .append( "<a>" + item.name +' '+ item.lastname + ' (' + item.email +' / '+ item.phone +')' + "</a>" )
                  .appendTo( ul );
          };
      },
      select: function( event, ui ) {
          $(this).val( ui.item.name + " " + ui.item.lastname );
          $('#tutor1_id').val(ui.item.id);
          return false;
      },
  });

  $("#btn-tutor-importar").click( function(e){

    e.preventDefault();

    var tutor_id = $('#tutor-new-1 #tutor1_id').val();
    var relacion = $('#tutor-new-1 #tutor1_relacion').val();

    var $data = { '_token': $token, 'viajero_id': viajero_id, 'tutor_id': tutor_id, 'relacion': relacion };

    $.ajax({
      url: "{{route('manage.tutores.ficha.asignar', $viajero_id)}}",
      type: 'POST',
      dataType : 'json',
      data: $data,
      success: function(data) {

        if(data.result)
        {
          // $('#DttTutores').DataTable().ajax.reload();
          http_reload('#tutores');

        }

      },
      error: function(xhr, desc, err) {
        console.log(xhr.responseText);
        console.log("Details: " + desc + "\nError:" + err);
      }
    }); // end ajax call

  });

  $(".btn-tutor-new").click( function(e) {

    e.preventDefault();

    var relacion = $('#tutor-new-2 #tutor_relacion').val();

    var $tutor_id = $(this).data('id');

    var $data = { '_token': $token, 'tutor_id': $tutor_id, 'viajero_id': viajero_id, 'relacion': relacion };

    var bRequired = false;
    $("#tutor-new-2 input").each( function() {

      var key = $(this).attr('name');
      var valor = $(this).val();
      $data[key.substr(6)] = valor;

      if(key == 'tutor_name' && valor=="")
      {
        $(this).parent().parent().find('#tutor_name').css('border-color','red');
        $(this).parent().parent().find('#tutor_name').attr('placeholder','Nombre requerido');
        bRequired = true;
      }

      // if(key == 'tutor_email' && valor=="")
      // {
      //   $('#tutor_email').css('border-color','red');
      //   $('#tutor_email').attr('placeholder','Email requerido');
      //   bRequired = true;
      // }

    });

    // if($('#tutor-new-2 #tutor_tipodoc').val()==0)
    // {
    //   if( !validaNif( $('#tutor-new-2 #tutor_nif').val() ) )
    //   {
    //     $("#tutor-new-2 input").parent().parent().find('#tutor_nif').css('border-color','red');
    //     $("#tutor-new-2 input").parent().parent().find('#tutor_nif').attr('placeholder','NIF incorrecto');
    //     return;
    //   }
    // }
    // elseif($('#tutor-new-2 #tutor_tipodoc').val()==1)
    // {
    //   if( !validaNie( $('#tutor-new-2 #tutor_nif').val() ) )
    //   {
    //     $("#tutor-new-2 input").parent().parent().find('#tutor_nif').css('border-color','red');
    //     $("#tutor-new-2 input").parent().parent().find('#tutor_nif').attr('placeholder','NIE incorrecto');
    //     return;
    //   }
    // }

    if(bRequired) { return; };

    $.ajax({
      url: "{{route('manage.tutores.ficha', 0)}}",
      type: 'POST',
      dataType : 'json',
      data: $data,
      success: function(data) {

        if(data.result)
        {
          // $('#DttTutores').DataTable().ajax.reload();
          http_reload('#tutores');
        }
        else
        {
          $(this).parent().parent().find('#tutor_email').css('border-color','red');
          bootbox.alert(data.mensaje);
        }

      },
      error: function(xhr, desc, err) {

        console.log(xhr.responseText);
        console.log("Details: " + desc + "\nError:" + err);
      }
    }); // end ajax call

  });

});
</script>
