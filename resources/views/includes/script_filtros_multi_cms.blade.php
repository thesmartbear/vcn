<script type="text/javascript">
$(document).ready(function() {



    $("#filtro-{{$filtro}}").on( 'change', function() {


        console.log($("#filtro-{{$destino}}").val());
        var valordestino = 'none';
        if ($("#filtro-{{$destino}}").val() == null || $("#filtro-{{$destino}}").val() == undefined){
            var valordestino = 'none';
        }else {
            var valordestino = $("#filtro-{{$destino}}").val();

        }

        $("#filtro-{{$destino}} > option").each(function(){
            $(this).remove();
        });


        $("#filtro-{{$destino}} > optgroup").each(function () {
            $(this).remove();
        });

        $("#filtro-{{$destino}}").val(null);

        $("#filtro-{{$destino}}").prop('disabled', true);

        $("#{{$destino}}-cargando").show();


        if ($("#filtro-{{$filtro}}").val() == null || $("#filtro-{{$filtro}}").val() == undefined){
            var $valor = 'none';
        }else {
            var $valor = $("#filtro-{{$filtro}}").val();
        }




        var $tipoc = $("#filtro-tipoc").val();


        var $data = {'filtro': '{{$filtro}}', 'valor': $valor, 'destino': '{{$destino}}', 'tipoc': $tipoc };


        var total = true;
        if($('#filtro-{{$filtro}}').val() != 0){
            var total = false;
        }


        if($valor != 'none') {
            console.log('ajax call->  filtro: {{$filtro}} - destino: {{$destino}} - valor: ' + $valor);
            $.ajax({
                url: "{{route('manage.informes.filtros.ajax')}}",
                type: 'GET',
                dataType: 'json',
                data: {'data': $data},
                success: function (data) {
                    console.log(data);
                    if (!data.result) {
                        alert("Error");
                        return;
                    }

                    //console.log(data);
                    if (data.group == 'group') {
                        if(data.datos != null) {
                            $.each(data.datos, function (i, item) {
                                $("#filtro-{{$destino}}").append($('<optgroup>', {
                                    label: $("#filtro-{{$filtro}} option[value=\"" + i + "\"]").text()
                                }));
                                $.each(item, function (x, element) {
                                    $("#filtro-{{$destino}}").append($('<option>', {
                                        value: element.index,
                                        text: element.value
                                    }));
                                });
                            });
                        }else{
                            console.log('deselectAll');
                            $("#filtro-{{$destino}}").multiselect('deselectAll');
                            $("#filtro-{{$destino}}").multiselect('rebuild');
                            $("#{{$destino}}-cargando").fadeOut();
                            return;
                        }
                    } else {
                        console.log('no group');
                        $.each(data.datos, function (i, item) {
                            $("#filtro-{{$destino}}").append($('<option>', {
                                value: item.index,
                                text: item.value
                            }));
                        });
                    }

                    $("#filtro-{{$destino}}").prop('disabled', false);

                    //Pendiente hacerlo a todos
                    // $("#filtro-prescriptores").multiselect('rebuild');
                    // $("#filtro-not-prescriptores").multiselect('rebuild');



                    console.log('valordestino ({{$destino}}): '+valordestino);
                    if(valordestino != 'none'){
                        /*
                        for (i = 0; i <= valordestino.length; i++){
                            $('option[value="'+valordestino[i]+'"]', $('#filtro-subcategoriasg')).prop('selected', true);
                        }
                        */
                        $("#filtro-{{$destino}}").multiselect('select', valordestino);

                    }else{
                        console.log('deselectAll');
                        $("#filtro-{{$destino}}").multiselect('deselectAll');
                    }
                    if(total == false) {
                        $("#filtro-{{$destino}}").multiselect('setOptions', {includeSelectAllOption: false, selectAllText: false});
                        $("#filtro-{{$destino}}").multiselect('updateButtonText', true);
                    }


                    $("#filtro-{{$destino}}").multiselect('rebuild');

                    $("#{{$destino}}-cargando").fadeOut();
                },
                error: function (xhr, desc, err) {
                    console.log(xhr.responseText);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            }); // end ajax call

        }else{
            console.log('valor: ' + $valor + ' - Destino: {{$destino}}');
            $("#filtro-{{$destino}}").empty();
            $("#filtro-{{$destino}}").val('none');
            $("#filtro-{{$destino}}").prop('disabled', true);
            $("#filtro-{{$destino}}").multiselect('rebuild');
            $("#filtro-{{$destino}}").multiselect('disable');
            $("#{{$destino}}-cargando").fadeOut();
        }

    });




});
</script>