<script type="text/javascript">
$(document).ready(function() {

    $("#especialidad_id").change(function() {

        $("#subespecialidad_id > option").each(function(){
            $(this).remove();
        });

        $("#subespecialidad_id").empty();
        $('#subespecialidad_id').multiselect('destroy');
        $("#subespecialidad_id").prop('disabled', true);

        var category_id = $("#especialidad_id").val();

        $.ajax({
          url: "{{route('manage.subespecialidades.index')}}",
          type: 'GET',
          dataType : 'json',
          data: {'especialidad_id': category_id},
          success: function(data) {

            $("#subespecialidad_id").append($('<option>', {
                  value: 0,
                  text: ''
            }));

            $.each(data, function(i, item) {

                $("#subespecialidad_id").append($('<option>', {
                  value: i,
                  text: item
                }));

            });

            $("#subespecialidad_id").prop('disabled', false);
            // $("#subespecialidad_id").multiselect({
            //     enableFiltering: true,
            //     enableCaseInsensitiveFiltering: true
            // });
            $("#subespecialidad_id").chosen();

          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        }); // end ajax call
    });

});
</script>