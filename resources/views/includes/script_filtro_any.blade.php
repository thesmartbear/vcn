<script type="text/javascript">
$(document).ready(function() {

  // $('#select-anys-filtro option:contains(TODOS), #select-oficinas-filtro option:contains(SIN ASIGNAR)').css('font-weight', 'bold');

  $('#select-any-filtro').change(function(e) {
    e.preventDefault();

    var loc = location.href.replace(/&?\?any=([^&]$|[^&]*)/i, "");
    location.href = loc + "?any=" + $(this).val();

  });

  $('#select-any-button').click(function(e) {
    e.preventDefault();
    console.log("select-any-button")
    var d = $("#desde").val();
    var h = $("#hasta").val();

    var loc = location.href.replace(/&?\?desde=([^&]$|[^&]*)/i, "");
    loc = loc.replace(/&?hasta=([^&]$|[^&]*)/i, "");
    location.href = loc + "?desde=" + d + "&hasta=" + h;

  });

});
</script>