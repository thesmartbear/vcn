@if(isset($texto))
    {!! Form::label($campo, $texto) !!}
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

{!! Form::select($campo, isset($select)?$select:[], isset($valor)?$valor:(isset($ficha)?$ficha->$campo:old($campo)),
        array(
            'id'=> $campo,
            'class' => "select2 form-control ". (isset($class)?$class:''),
            'placeholder' => "...",
            (isset($required)?'required':null),
            (isset($disabled)?'disabled':null)
        )
    ) !!}
<span class="help-block">{{ $errors->first($campo) }}</span>

<script>
    $("#{{$campo}}").val( "{{ isset($valor)?$valor:(isset($ficha)?$ficha->$campo:old($campo)) }}" );
</script>