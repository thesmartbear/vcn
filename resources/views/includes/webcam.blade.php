@section('extra_footer')
{!! Html::script('assets/plugins/webcamjs/webcam.min.js') !!}
@stop

<script language="JavaScript">
$(document).ready(function() {

    Webcam.set({
        width: 320,
        height: 240,
        // device capture size
        dest_width: 320,
        dest_height: 240,
        // final cropped size
        crop_width: 240,
        crop_height: 240,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    // Webcam.attach( '#my_camera' );
    
});

function setup() {
    Webcam.reset();
    Webcam.attach( '#my_camera' );
    $("#btn-snapshot").show();
}

function take_snapshot() {
    // take snapshot and get image data
    Webcam.snap( function(data_uri) {
        // display results in page
        document.getElementById('results').innerHTML = 
            // '<h2>Here is your image:</h2>' + 
            '<img src="'+data_uri+'"/>';

        $('#webcam64').val(data_uri);
    } );
}


</script>

<div class="row">
    <div class="col-md-6">
        <div id="my_camera"></div>
    </div>

    <div class="col-md-6">
        <div id="results"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <input type="button" value="Webcam" onClick="setup(); $(this).hide().next().show();" class="btn btn-warning">
    </div>
    <div class="col-md-2">
        <input type=button value="Foto" id="btn-snapshot" onClick="take_snapshot()" class="btn btn-success" style='display:none;'>
    </div>
    <div class="col-md-2">
        {!! Form::hidden('webcam64', null, array('id'=> 'webcam64')) !!}
    </div>
</div>