<script type="text/javascript">
$(document).ready(function() {

    $("#filtro-{{$filtro}}").on( 'change', function() {

        $("#filtro-{{$destino}} > option").each(function(){
            $(this).remove();
        });

        $("#filtro-{{$destino}}").val(null);

        $("#filtro-{{$destino}}").prop('disabled', true);

        $("#{{$destino}}-cargando").show();

        var $valor = $("#filtro-{{$filtro}}").val();
        var $tipoc = $("#filtro-tipoc").val();

        var $data = {'filtro': '{{$filtro}}', 'valor': $valor, 'destino': '{{$destino}}', 'tipoc': $tipoc, 'online': '{{isset($online)?$online:0}}' };

        console.log($data);

        $.ajax({
          url: "{{route('manage.informes.filtros.ajax')}}",
          type: 'GET',
          dataType : 'json',
          data: {'data': $data},
          success: function(data) {

            if(!data.result)
            {
                alert("Error"); return;
            }

            $.each(data.datos, function(i, item) {

                $("#filtro-{{$destino}}").append($('<option>', {
                  value: item.index,
                  text: item.value
                }));

            });

            $("#filtro-{{$destino}}").prop('disabled', false);

            $("#{{$destino}}-cargando").fadeOut();
          },
          error: function(xhr, desc, err) {
            console.log(xhr.responseText);
            console.log("Details: " + desc + "\nError:" + err);
          }
        }); // end ajax call

    });

});
</script>