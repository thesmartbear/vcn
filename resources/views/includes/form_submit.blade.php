@if(ConfigHelper::canEdit($permiso))

    <div class="form-group pull-right">
        {!! Form::submit(isset($texto)?$texto:"Guardar", array('class' => 'btn btn-success',
            'id'=> (isset($id)?$id:("btn-".$texto))) ) !!}

        @if($texto=="Guardar")
            <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
        @endif
    </div>

@else

@endif