@if( isset($texto) && isset($required) && $required )
    {!! Form::label($campo, $texto." *") !!}
@elseif( isset($texto) )
    {!! Form::label($campo, $texto) !!}
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

@if( $campo=="email" && isset($ficha) && !isset($disabled) && $ficha->email )
    @if(isset($esbooking))
        <span class="input-group-addon"><a href="mailto:{{$ficha->email}}"><i class="fa fa-envelope"></i></a></span>
    @else
        <a href="mailto:{{$ficha->email}}"><i class="fa fa-envelope"></i></a>
    @endif
@endif

@php
    $value = isset($valor) ? $valor : (isset($ficha) ? $ficha->$campo : old($campo));
    $phones = ['phone', 'movil', 'tutor_phone', 'tutor_movil'];    
    $esPhone = in_array($campo, $phones);
    
    $wpre = $esPhone ? (ConfigHelper::config('whatsapp_pre') ?: '+34') : '';
@endphp

@if( $esPhone && !isset($disabled) )
    
    @if( $value )
        &nbsp;&nbsp;<a href="https://api.​whatsapp​.com/send?phone={{ $wpre }}{{ $value }}" target="_blank" class="href"><i class="fa fa-whatsapp"></i></a>
    @endif

@endif

{!! Form::text($campo, $value,
        array(
            'id'=> isset($id)?$id:$campo,
            'placeholder' => (isset($texto)?$texto:(isset($placeholder)?$placeholder:"")),
            'class' => isset($novisible)?'form-control novisible':'form-control',
            ((isset($required) && $required) ? 'required' : ''), (isset($disabled)?'disabled':'')) ) !!}

<span class="help-block">{{ $errors->first($campo) }}</span>