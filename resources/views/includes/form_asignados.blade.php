<?php

    if($ficha->oficina_id>0)
    {
        $asignados = \VCN\Models\User::asignados()->where('oficina_id',$ficha->oficina_id)->orderBy('fname')->get()->pluck('full_name','id');
    }
    else
    {
        $asignados = \VCN\Models\User::asignados()->get()->pluck('full_name','id');
    }

    $asignados = [0=> ""] + $asignados->toArray();
?>

{!! Form::select($campo, $asignados)  !!}