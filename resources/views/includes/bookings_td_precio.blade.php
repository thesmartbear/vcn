<td align='right'>
    <span class='booking-extra-precio' id="{{$prefix}}-{{$extra->id}}" data-precio="{{$extra->precio}}" data-moneda="{{$extra->moneda_id}}">
    @if($extra->tipo_unidad==2)
        {{$ficha->extra_unidades($extra->id,$tipo) * (($extra->precio * $total['total_descontar'])/100)}} {{$extra->moneda}}
    @else
        {{$ficha->extra_unidades($extra->id,$tipo) * $extra->precio}} {{$extra->moneda}}
    @endif
    </span>
</td>