@if(isset($texto))
{!! Form::label($campo, $texto) !!}
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

@if(isset($ficha))
    @if($ficha->monitor_foto)
        <br><img src="{{$ficha->monitor_foto}}" class="form-foto">
    @endif

    @if($ficha->foto)
        <br><img src="{{$ficha->foto}}" class="form-foto">
    @elseif($ficha->avatar)
        <br><img src="{{$ficha->avatar ?: $ficha->foto}}" class="form-foto">
    @endif

    @if($ficha->icono)
        <br><img src="{{$ficha->icono}}" class="form-foto">
    @endif

@endif

{!! Form::file($campo) !!}
@if(!isset($noborrar))
@include('includes.form_checkbox', [ 'campo'=> $campo."_delete", 'texto'=> 'Eliminar', 'valor'=> 0])
@endif

@if( isset($ficha) && $ficha->$campo && !starts_with($ficha->$campo, 'data:') )
    @if($campo == 'imagen' && $ficha->$campo)
        <br><img src="{{$ficha->$campo}}" class="form-foto">
    @elseif($campo == 'home_imagen' && $ficha->$campo)
        <br><img src="{{$ficha->$campo}}" class="form-foto">
    @else
        <br><a href="{{$ficha->$campo}}" target="_blank">{{$ficha->$campo}}</a>
    @endif
@endif

<span class="help-block">{{ $errors->first($campo) }}</span>