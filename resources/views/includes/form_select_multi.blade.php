@if(isset($texto))
{!! Form::label($campo, $texto) !!}
<br>
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

{!! Form::select($campo, $select?$select:[], isset($valor)?$valor:(isset($ficha)?$ficha->$campo:''),
    array(
        'id'=> $campo,
        'class'=> 'multiselect form-control '. (isset($class)?$class:''),
        'multiple',
        'name'=>$campo.'[]'
    )
)!!}
<span class="help-block">{{ $errors->first($campo) }}</span>