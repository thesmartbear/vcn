<div class="portlet light bordered">
    <div class="portlet-title tabbable-line">
        <div class="caption font-blue-sharp">Promoción Tipo de Cambio Fijo
            <a id="promo_cambios_add" href="#hijos_add" class='btn btn-xs btn-warning' data-label="Añadir"><i class="fa fa-plus-square"></i></a>
        </div>
    </div>
    <div class="portlet-body flip-scroll">

        <div id="promo_cambios_div" class="row">

            @if(is_array($ficha->promo_cambio_fijo_fechas))

                @foreach($ficha->promo_cambio_fijo_fechas['desde'] as $kform=>$vform)

                    <div class="promo_cambios_form row">
                        <div class="col-md-3">
                            @include('includes.form_input_datetime', [ 'campo'=> "promo_cambio_fijo_fechas[desde][$kform]", 'texto'=> 'Desde', 'valor'=> $ficha->promo_cambio_fijo_fechas['desde'][$kform] ])
                        </div>

                        <div class="col-md-3">
                            @include('includes.form_input_datetime', [ 'campo'=> "promo_cambio_fijo_fechas[hasta][$kform]", 'texto'=> 'Hasta', 'valor'=> $ficha->promo_cambio_fijo_fechas['hasta'][$kform] ])
                        </div>
                    </div>

                @endforeach

            @else

                <div class="promo_cambios_form row">
                    <div class="col-md-3">
                        @include('includes.form_input_datetime', [ 'campo'=> 'promo_cambio_fijo_fechas[desde][]', 'texto'=> 'Desde' ])
                    </div>
                    <div class="col-md-3">
                        @include('includes.form_input_datetime', [ 'campo'=> 'promo_cambio_fijo_fechas[hasta][]', 'texto'=> 'Hasta' ])
                    </div>
                </div>

            @endif

            @include('includes.script_form_add', ['campo'=> 'promo_cambios' ])

        </div>

    </div>
</div>