@if( isset($texto) && isset($required) )
    {!! Form::label($campo, $texto." *") !!}
@elseif( isset($texto) )
    {!! Form::label($campo, $texto) !!}
@endif
<br>
@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

@php
    $showError = isset($showError) ? $showError : true;
@endphp

{!! Form::select($campo, isset($select)?$select:[], isset($valor)?$valor:(isset($ficha)?$ficha->$campo:old($campo)),
        array(
            'id'=> isset($id)?$id:$campo,
            'class' => "selectpicker form-control ". (isset($class)?$class:''),
            'title' => "...",
            (isset($required)?'required':null),
            (isset($disabled)?'disabled':null),
            'data-width' => $width ?? "auto"
        )
    ) !!}

@if($showError)
<span class="help-block">{{ $errors->first($campo) }}</span>
@endif
