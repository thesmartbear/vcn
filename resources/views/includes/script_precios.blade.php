<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-plus-circle fa-fw"></i> Añadir Precio
    </div>
    <div class="panel-body">

        {!! Form::open( array('id'=>"frm-precio-add-$extras", 'url' => $route, 'class'=> 'form-inline')) !!}

        {!! Form::hidden($campo, $campo_id) !!}

        <div class="form-group">
            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> 'Nombre', 'valor'=>'', 'required'=> 'required'])
        </div>

        @if($fechas)
            <div class="form-group">
                Validez:
                @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> 'Desde', 'required'=> 'required'])
            </div>
            <div class="form-group">
                @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> 'Hasta', 'required'=> 'required'])
            </div>
            {{-- <span class="badge badge-help">IMPORTANTE: Fechas dentro del mismo año</span> --}}
            <br><br>
        @endif

        <div class="form-group">
            Precio:
            @include('includes.form_select', [ 'campo'=> 'moneda_id', 'texto'=> 'Moneda', 'valor'=> $moneda_id, 'select'=> $monedas, 'required'=> 'required'])
        </div>

        <div class="form-group">
            @include('includes.form_input_text', [ 'campo'=> 'importe', 'texto'=> 'Precio', 'required'=> 'required'])
        </div>

        <div class="form-group">
            @include('includes.form_select', [ 'campo'=> 'duracion', 'id'=> 'duracion-'.$extras, 'texto'=> 'Duración', 'valor'=> $regla?$regla->duracion:0, 'select'=> ConfigHelper::getPrecioDuracion()])
        </div>

        <div id="duracion_tipo_div-{{$extras}}" class="form-group collapse">
            <span id="duracion_tipo_titulo-{{$extras}}">para:</span>
            @include('includes.form_select', [ 'campo'=> 'duracion_tipo', 'id'=> 'duracion_tipo-'.$extras, 'texto'=> '', 'valor'=> $regla?$regla->duracion_tipo:0, 'select'=> ConfigHelper::getPrecioDuracionTipo()])
        </div>

        <div id="rango1_div-{{$extras}}" class="form-group collapse">
            <span id="rango1_titulo"></span>
            @include('includes.form_select', [ 'campo'=> 'rango1', 'id'=> 'rango1-'.$extras, 'texto'=> 'R1', 'valor'=> 0, 'select'=> ConfigHelper::getPrecioRangos()])
        </div>

        <div id="duracion_fijo_div-{{$extras}}" class="form-group collapse">
            @include('includes.form_select', [ 'campo'=> 'duracion_fijo', 'id'=> 'duracion_fijo-'.$extras, 'texto'=> '', 'valor'=> $regla?$regla->duracion_fijo:0, 'select'=> ConfigHelper::getPrecioDuracionUnit()])
        </div>

        <div id="rango2_div-{{$extras}}" class="form-group collapse">
            <span id="rango2_titulo"></span>
            @include('includes.form_select', [ 'campo'=> 'rango2', 'id'=> 'rango2-'.$extras, 'texto'=> 'R2', 'valor'=> 0, 'select'=> ConfigHelper::getPrecioRangos()])
        </div>

        <div id="rango_div-{{$extras}}" class="form-group collapse">
            <span id="rango_titulo-{{$extras}}"></span>
        </div>

        @include('includes.form_submit', [ 'id'=>'btn-add', 'permiso'=> 'proveedores', 'texto'=> 'Añadir'])

        {!! Form::close() !!}


    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {

    var $duracion_default = 0;
    var $duracion_fijo_default = 0;
    var $duracion_tipo_default = -1;

    @if($regla)
        $duracion_default = {{$regla->duracion}};
        $duracion_fijo_default = {{$regla->duracion_fijo}};
        $duracion_tipo_default = {{$regla->duracion_tipo}};
    @endif

    if($duracion_tipo_default==0)
    {
        // $("#btn-add").prop("disabled","disabled");
    }

    if($duracion_tipo_default>0)
    {
        // $('#duracion_tipo').children('option[value="0"]').remove();
    }

    $(".form-inline label").addClass('sr-only');

    $('#duracion_fijo_div-{{$extras}}').hide();
    $('#duracion_tipo_div-{{$extras}}').hide();
    $('#rango1_div-{{$extras}}').hide();
    $('#rango2_div-{{$extras}}').hide();

    var $duracion = $duracion_default;

    $('#duracion-{{$extras}}').val(0);
    $('#duracion_fijo-{{$extras}}').val(0);
    $('#duracion_tipo-{{$extras}}').val(0);

    if($duracion>0)
    {
        $('#duracion-{{$extras}}').val($duracion);
        $('#duracion-{{$extras}}').prop("disabled", "disabled");

        if($duracion==1)
        {
            $('#duracion_fijo-{{$extras}}').val($duracion_fijo_default);
            $('#duracion_fijo-{{$extras}}').prop("disabled", "disabled");

            $('#duracion_fijo_div-{{$extras}}').show();
            $('#rango1_titulo-{{$extras}}').text('para:');
            $('#rango1_div-{{$extras}}').show();
        }
        else
        {
            $('#duracion_tipo_div-{{$extras}}').show();
        }

    }

    $("#duracion-{{$extras}}").change( function(e) {

        //1:monto fijo, 2:semana, 3:mes, 4:semestre, 5:año

        $('#duracion_fijo_div-{{$extras}}').hide();
        $('#duracion_tipo_div-{{$extras}}').hide();
        $('#rango1_div-{{$extras}}').hide();
        $('#rango2_div-{{$extras}}').hide();
        $('#rango_div-{{$extras}}').hide();

        $("#duracion_tipo-{{$extras}}").val(0);

        $duracion = $(this).val();

        switch($duracion)
        {
            case 1:
            case '1': //monto fijo
            {
                $('#duracion_fijo_div-{{$extras}}').fadeIn();
                $('#rango1_titulo-{{$extras}}').text('para:');
                $('#rango1_div-{{$extras}}').fadeIn();

            }
            break;

            default:
            {
                $('#duracion_tipo_div-{{$extras}}').fadeIn();

            }
            break;
        }

    });

    $("#duracion_fijo-{{$extras}}").change( function(e) {
        $txt = $("#duracion_fijo-{{$extras}} option:selected").text();

        $('#rango_titulo-{{$extras}}').text($txt);
    });

    $("#duracion_tipo-{{$extras}}").change( function(e) {

        $('#rango1_div-{{$extras}}').hide();
        $('#rango2_div-{{$extras}}').hide();
        $('#rango_div-{{$extras}}').hide();

        $('#rango1-{{$extras}}').val(0);
        $('#rango2-{{$extras}}').val(0);
        $('#rango1_titulo-{{$extras}}').text('');
        $('#rango2_titulo-{{$extras}}').text('');

        var $tipo = $(this).val();

        if($duracion>1)
        {
            var $titulo = "Semanas";
            switch($duracion)
            {
                case '3':
                {
                    $titulo = "Meses";
                }
                break;

                case '4':
                {
                    $titulo = "Trimestres";
                }
                break;

                case '5':
                {
                    $titulo = "Semestres";
                }
                break;

                case '6':
                {
                    $titulo = "Años";
                }
                break;
            }
            $('#rango_titulo').text($titulo);
        }

        switch($tipo)
        {
            case 1:
            case '1':
            {
                $('#rango1_div-{{$extras}}').fadeIn();
                $('#rango_div-{{$extras}}').fadeIn();
            }
            break;

            case '2':
            {
                $('#rango1_titulo-{{$extras}}').text('Entre: ');
                $('#rango2_titulo-{{$extras}}').text('y: ');

                $('#rango1_div-{{$extras}}').fadeIn();
                $('#rango2_div-{{$extras}}').fadeIn();

                $('#rango_div-{{$extras}}').fadeIn();
            }
            break;
        }

    });

    $("#frm-precio-add-{{$extras}}").on("submit", function(e){
        e.preventDefault();

        $("#duracion-{{$extras}}").css('border-color','#e5e5e5');
        $("#duracion_fijo-{{$extras}}").css('border-color','#e5e5e5');
        $("#rango1-{{$extras}}").css('border-color','#e5e5e5');
        $("#rango2-{{$extras}}").css('border-color','#e5e5e5');

        if($duracion<1)
        {
            $("#duracion-{{$extras}}").css('border-color','red');
            bootbox.alert("Selecciona la Duración");
            return;
        }

        switch($duracion)
        {
            case 1:
            case '1': //monto fijo
            {
                $v = $('#duracion_fijo-{{$extras}}').val();
                if($v<1)
                {
                    $("#duracion_fijo-{{$extras}}").css('border-color','red');
                    bootbox.alert("Selecciona Duración del Monto fijo.");
                    return;
                }

                $v = $('#rango1-{{$extras}}').val();
                if($v<1)
                {
                    $("#rango1-{{$extras}}").css('border-color','red');
                    bootbox.alert("Selecciona un valor válido.");
                    return;
                }
            }
            break;

            default:
            {
                $tipo = $("#duracion_tipo-{{$extras}}").val();
                switch($tipo)
                {
                    case '1':
                    {
                        $v = $('#rango1-{{$extras}}').val();
                        if($v<1)
                        {
                            $("#rango1-{{$extras}}").css('border-color','red');
                            bootbox.alert("Selecciona un valor válido.");
                            return;
                        }
                    }
                    break;

                    case '2':
                    {
                        $v = $('#rango1-{{$extras}}').val();
                        if($v<1)
                        {
                            $("#rango1-{{$extras}}").css('border-color','red');
                            bootbox.alert("Selecciona un valor válido.");
                            return;
                        }

                        $v = $('#rango2-{{$extras}}').val();
                        if($v<1)
                        {
                            $("#rango2-{{$extras}}").css('border-color','red');
                            bootbox.alert("Selecciona un valor válido.");
                            return;
                        }
                    }
                    break;
                }
            }
            break;
        }

        $('#duracion-{{$extras}}').removeAttr('disabled');
        $('#duracion_fijo-{{$extras}}').removeAttr('disabled');

        $(this).unbind("submit").submit();

    });

});
</script>

{{-- <div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-search fa-fw"></i> Test Precio
    </div>
    <div class="panel-body">

        {!! Form::open( array('id'=>'frm-precio-test', 'url' => $route, 'class'=> 'form-inline')) !!}

        {!! Form::hidden($campo, $campo_id) !!}

        @if($fechas)
            <div class="form-group">
                Validez:
                @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> 'Desde', 'required'=> 'required'])
            </div>
            <div class="form-group">
                @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> 'Hasta', 'required'=> 'required'])
            </div>
        @endif

        <div class="form-group">
            @include('includes.form_input_text', [ 'campo'=> 'test', 'texto'=> $ficha->duracion_name, 'valor'=> old('test'), 'required'=> 'required'])
        </div>

        <br>
        Resultado = {{print_r(Session::get('test'),true)}}


        @include('includes.form_submit', [ 'id'=>'btn-test', 'permiso'=> 'proveedores', 'texto'=> 'Test'])

        {!! Form::close() !!}

    </div>

</div> --}}