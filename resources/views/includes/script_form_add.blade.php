<script type="text/javascript">
$(document).ready(function() {

  var form = ".{{$campo}}_form";
  var div = "#{{$campo}}_div";

  var bMulti = {{isset($nomulti)?0:1}};

  $(".{{$campo}}_del").click(function(e) {
    e.preventDefault();

    var $id = $(this).data('id');

    var $borrar = $( div +" div"+ form +":eq("+$id+")");
    $borrar.remove();
  });

  $("#{{$campo}}_add").click(function(e) {

    e.preventDefault();

    var $clone = $( div +" div"+ form +":eq(0)").clone();
    $(div).append($clone);
    $(div +" div"+ form).eq(-1).find("input").val('');
    $(div +" div"+ form).eq(-1).find(".frm-img").remove();

    if($clone.find('.selectpicker').length)
    {
        // $clone.find('.bootstrap-select').remove();
        $clone.find('.bootstrap-select').replaceWith(function() { return $('select', this); });
        $clone.find('.selectpicker').selectpicker();
    }

    // $('.datetime').data("DateTimePicker").destroy();

    $clone.find('.datetime').datetimepicker({
        locale: 'es',
        format: 'L',
    });

    //clone de input file le deja el index
    var iFile = $(div +" div"+ form).find("input[type='file']").length;
    if(iFile>1)
    {
        var $tFile = $(div +" div"+ form).eq(iFile-1).find("input[type='file']");

        var nFile = $tFile.attr('name');
        nFile = nFile.slice(0,-3);
        $tFile.attr('name', nFile+"[]");
        $tFile.attr('id', nFile+"[]");
    }

    if(!bMulti)
    {
        return;
    }

    $(div +" div"+ form).find("input").each( function(){

        var n = $(this).attr('name');

        //hacerlo solo si es [X]
        var x = n.slice(n.length-3);
        if(x!="][]")
        {
            n = n.slice(0,-3);
            $(this).attr('name', n+"[]");
            $(this).attr('id', n+"[]");
        }
    });

  });


    $(".{{$campo}}_del").click(function(e) {

        e.preventDefault();

        var b = $(this).data('id');
        var val = $("#facturas_del").val();
        $("#facturas_del").val( val +","+ b );

        $('#li-'+b).hide();

    });

});
</script>