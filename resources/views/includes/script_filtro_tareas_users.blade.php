<script type="text/javascript">

var $u = 'all';
var $o = 'all';

function reloadDtt()
{
  newUrl = "manage/viajeros/tareas/resumen/1/"+$u+"/"+$o;
  $('#DttTareas-1').DataTable().ajax.url( newUrl ).load();
  newUrl = "manage/viajeros/tareas/resumen/2/"+$u+"/"+$o;
  $('#DttTareas-2').DataTable().ajax.url( newUrl ).load();
  newUrl = "manage/viajeros/tareas/resumen/3/"+$u+"/"+$o;
  $('#DttTareas-3').DataTable().ajax.url( newUrl ).load();
  newUrl = "manage/viajeros/tareas/resumen/4/"+$u+"/"+$o;
  $('#DttTareas-4').DataTable().ajax.url( newUrl ).load();

  newUrl = "manage/bookings/incidencias/resumen/"+$u+"/"+$o;
  $('#DttIncidencias').DataTable().ajax.url( newUrl ).load();
}

$('#select-tareas-filtro').val("{{$user->id}}");

//data-ofi show/hide

$(document).ready(function() {

  // $('#select-tareas-filtro option[value="all"], #select-tareas-filtro option[value="null"]').css('font-weight', 'bold');
  // $('#select-tareas-oficina option[value="all"], #select-tareas-oficina option[value="null"]').css('font-weight', 'bold');

  $('#select-tareas-filtro').change(function(e) {
    e.preventDefault();

    $u = $(this).val();
    $o = $('#select-tareas-oficina').val();
    reloadDtt();

  });

  $('#select-tareas-oficina').change(function(e) {
    e.preventDefault();

    $u = $('#select-tareas-filtro').val();
    $o = $(this).val();

    $("#select-tareas-filtro option").show();
    $("#select-tareas-filtro option[data-ofi!='" + $o + "']").hide();
    $("#select-tareas-filtro option[data-ofi='0']").show();

    var $p = $("#select-tareas-filtro option[data-ofi='" + $o + "']").val();
    if(typeof $p === 'undefined')
    {
      $p = 'all';
    }

    $("#select-tareas-filtro").val($p);
    $u = $p;

    reloadDtt();

  });

  $('.reload-tasks').click(function(){
    $('#select-tareas-filtro').trigger('change');
  });

});
</script>