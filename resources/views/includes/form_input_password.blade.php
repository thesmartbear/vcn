@if(isset($texto))
{!! Form::label($campo, $texto) !!}:
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

{!! Form::password($campo, ['class' => 'form-control', (isset($required)?'required':'')] ) !!}

<span class="help-block">{{ $errors->first($campo) }}</span>