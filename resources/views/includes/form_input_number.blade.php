@if(isset($texto))
{!! Form::label($campo, $texto) !!}
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

<input type="number" class="form-control" id="{{$campo}}"  name="{{$campo}}" placeholder="{{$texto}}"
    value="{{(isset($valor)?$valor:(isset($ficha)?$ficha->$campo:old($campo)))}}" step="{{isset($step)?$step:0}}">
<span class="help-block">{{ $errors->first($campo) }}</span>