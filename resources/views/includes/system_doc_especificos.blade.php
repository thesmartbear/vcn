<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-book"></i> Documentos específicos

    </div>
    <div class="panel-body">

        {!! Datatable::table()
            ->addColumn([
                'name'      => 'Nombre',
                //'idioma'  => 'Idioma',
                'notas'     => 'Notas',
                'documento' => 'Documento',
                'langs'     => 'Idiomas',
                'options'   => ''
            ])
            ->setUrl(route('manage.system.docs.index', [$modelo, $modelo_id, 1]))
            ->setOptions(
                "aoColumnDefs", array(
                    [ "bSortable" => false, "aTargets" => [3] ]
                )
            )
            ->render() !!}

    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-book"></i> Nuevo Documentos específico

    </div>
    <div class="panel-body">

        {!! Form::open(array('method' => 'POST', 'files'=> true, 'url' => route('manage.system.docs.ficha',0), 'role' => 'form', 'class' => '')) !!}

            {!! Form::hidden('modelo', $modelo) !!}
            {!! Form::hidden('modelo_id', $modelo_id) !!}
            {!! Form::hidden('tipo', 1) !!}

            <div class="form-group row">
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'doc_name', 'texto'=> 'Nombre', 'valor'=> ""])
                </div>
                {{-- <div class="col-md-1">
                    @include('includes.form_select', [ 'campo'=> 'doc_idioma', 'texto'=> "Idioma", 'select'=> ConfigHelper::getIdiomaContacto()])
                </div> --}}
                <div class="col-md-5">
                    @include('includes.form_input_text', [ 'campo'=> 'doc_notas', 'texto'=> 'Notas', 'valor'=> ""])
                </div>
                <div class="col-md-3">
                    @include('includes.form_input_file', [ 'campo'=> 'doc_adjunto', 'texto'=> 'Documento (descargable)', 'ficha'=> null])
                </div>
            </div>

            @include('includes.form_submit', [ 'permiso'=> 'proveedores', 'texto'=> 'Añadir'])
        
        {!! Form::close() !!}


    </div>

</div>