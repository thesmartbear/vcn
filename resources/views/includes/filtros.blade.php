<div class="form-group row">
    <div class="col-md-2">
    {!! Form::label('plataformas', 'Plataforma') !!}
    <br>
    {!! Form::select('plataformas', $plataformas, $valores['plataformas'], array('class'=>'select2', 'data-style'=>'green', 'id'=>'filtro-plataformas'))  !!}
    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'oficinas'])
    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'proveedores'])
    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'centros'])
    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'cursos'])
    @include('includes.script_filtros', ['filtro'=> 'plataformas', 'destino'=> 'prescriptores'])
    </div>

    <div class="col-md-3">
    {!! Form::label('oficinas', 'Oficina') !!}
    @include('includes.form_input_cargando',['id'=> 'oficinas-cargando'])
    <br>
    {!! Form::select('oficinas', $oficinas, $valores['oficinas'], array('class'=>'select2', 'data-style'=>'blue', 'id'=>'filtro-oficinas'))  !!}
    @include('includes.script_filtros', ['filtro'=> 'oficinas', 'destino'=> 'prescriptores'])
    </div>

    <div class="col-md-4">
    {!! Form::label('prescriptores', 'Prescriptor') !!}
    @include('includes.form_input_cargando',['id'=> 'prescriptores-cargando'])
    <br>
    {!! Form::select('prescriptores', $prescriptores, $valores['prescriptores'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-prescriptores'))  !!}
    </div>

</div>

<div class="form-group row">
    <div class="col-md-2">
    {!! Form::label('tipoc', 'Tipo Convocatoria') !!}
    @include('includes.form_input_cargando',['id'=> 'tipoc-cargando'])
    <br>
    {!! Form::select('tipoc', ConfigHelper::getConvocatoriaTipo(), $valores['tipoc'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-tipoc'))  !!}
    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'centros'])
    @include('includes.script_filtros', ['filtro'=> 'tipoc', 'destino'=> 'cursos'])
    </div>

    <div class="col-md-2">
    {!! Form::label('categorias', 'Categoría') !!}
    @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
    <br>
    {!! Form::select('categorias', $categorias, $valores['categorias'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
    @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos'])
    </div>

    <div class="col-md-4">
    {!! Form::label('proveedores', 'Proveedor') !!}
    @include('includes.form_input_cargando',['id'=> 'proveedores-cargando'])
    <br>
    {!! Form::select('proveedores', $proveedores, $valores['proveedores'], array('class'=>'select2', 'data-style'=>'purple', 'id'=>'filtro-proveedores'))  !!}
    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'centros'])
    @include('includes.script_filtros', ['filtro'=> 'proveedores', 'destino'=> 'cursos'])
    </div>

</div>

<div class="form-group row">
    <div class="col-md-5">
    {!! Form::label('centros', 'Centro') !!}
    @include('includes.form_input_cargando',['id'=> 'centros-cargando'])
    <br>
    {!! Form::select('centros', $centros, $valores['centros'], array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-centros'))  !!}
    @include('includes.script_filtros', ['filtro'=> 'centros', 'destino'=> 'cursos'])
    </div>

    <div class="col-md-2">
    {!! Form::label('cursos', 'Curso') !!}
    @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])
    <br>
    {!! Form::select('cursos', $cursos, $valores['cursos'], array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
    </div>

</div>

<hr>
@if($filtro_fechas)
<div class="form-group row">

    <div class="col-md-3">
        {!! Form::label('desde','Desde') !!}
        @include('includes.form_input_datetime', [ 'campo'=> 'desde', 'texto'=> null, 'valor'=>''])
    </div>
    <div class="col-md-3">
        {!! Form::label('hasta','Hasta') !!}
        @include('includes.form_input_datetime', [ 'campo'=> 'hasta', 'texto'=> null, 'valor'=>''])
    </div>

    <div class="col-md-1 col-md-offset-1">
        {!! Form::label('&nbsp;') !!}
        {!! Form::submit('Consultar', array( 'name'=> 'filtro1', 'class' => 'btn btn-info')) !!}
    </div>

</div>
@endif