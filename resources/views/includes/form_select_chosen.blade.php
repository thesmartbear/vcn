@if(isset($texto))
    {!! Form::label($campo, $texto) !!}
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

{!! Form::select($campo, isset($select)?$select:[], isset($valor)?$valor:(isset($ficha)?$ficha->$campo:0), array( 'id'=> $campo, 'class' => "chosen-select form-control ". (isset($class)?$class:''), isset($required)?'required':'' )) !!}
<span class="help-block">{{ $errors->first($campo) }}</span>
