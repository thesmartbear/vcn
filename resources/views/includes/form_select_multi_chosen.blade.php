@if($texto)
{!! Form::label($campo, $texto) !!}:
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif
<br>
{!! Form::select($campo, $select?$select:[], $valor, array('id'=> $campo, 'class'=> 'chosen-select-multi form-control', 'multiple'=>'multiple', 'name'=>$campo.'[]'))!!}
<span class="help-block">{{ $errors->first($campo) }}</span>