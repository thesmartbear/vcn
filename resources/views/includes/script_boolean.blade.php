<script type="text/javascript">
$(document).ready(function() {

  var bool = "input[name='{{$campo}}_bool']";
  var div = "#{{$campo}}_div";
  var btn = "#{{$campo}}_add";

  var required = {{isset($required)?1:0}};
  var campo = "#{{$campo}}";

  var $checked = {{isset($checked)?$checked:1}};

  if( $checkbox )
  {
    var v = $(bool).is(':checked')?1:0;
  }
  else
  {
    var v = $(bool+":checked").val();
  }

  if(typeof v === "undefined")
  {
    v = false;
  }

  if(required)
  {
    if(v==$checked) { $(div).show(); $(campo).prop('required',true); } else { $(div).hide(); $(campo).removeAttr('required'); }
  }
  else
  {
    if(v==$checked) { $(div).show(); } else { $(div).hide(); }
  }

  if(v==!$checked) { $(btn).hide(); } else { $(btn).show(); }


  var $checkbox = {{isset($checkbox)?1:0}};

  $(bool).change(function() {

      if( $checkbox )
      {
        var v = $(this).is(':checked');
      }
      else
      {
        var v = $(this).val();
      }

      if(required)
      {
        if(v==$checked) { $(div).show(); $(campo).prop('required',true); } else { $(div).hide(); $(campo).removeAttr('required'); }
      }
      else
      {
        if(v==$checked) { $(div).show(); } else { $(div).hide(); }
      }

      if( $(btn).length > 0 )
      {
        if(v==$checked) { $(btn).show(); } else { $(btn).hide(); }
      }

  });

});
</script>