@if( isset($texto) && isset($required) )
    {!! Form::label($campo, $texto." *") !!}
@elseif( isset($texto) )
    {!! Form::label($campo, $texto) !!}
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

{{-- {!! Form::text($campo, isset($valor)?$valor:(isset($ficha)?$ficha->$campo:old($campo)), array( 'id'=>$campo,
    'placeholder' => isset($placeholder)?$placeholder:(isset($texto)?$texto:""),
    'class' => 'form-control datetime-time')) !!}--}}

<div class='input-group date datetime-time'>
    {!! Form::text($campo, isset($valor)?$valor:(isset($ficha)?$ficha->$campo:old($campo)), array( 'id'=>$campo,
    'placeholder' => isset($placeholder)?$placeholder:(isset($texto)?$texto:""),
    'class' => 'form-control', (isset($required)?'required':null), (isset($disabled)?'disabled':null))) !!}
    <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
    </span>
</div>

<span class="help-block">{{ $errors->first($campo) }}</span>