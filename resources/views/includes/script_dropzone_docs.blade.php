<script type="text/javascript">
    var url = "{{ $url }}";
    var token = "{{ Session::token() }}";
    var idioma = "{{$idioma}}";

    var div = "div#" + "{{$name}}";

    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone( div, {
        url: url,
        params: {
            _token: token,
            idioma: idioma,
        },
        paramName: "file",
        maxFilesize: 30, // MB
        maxFiles: 25,
        // accept: function(file, done) {

        // },
        // success: function(file, response){
        // },
        sending: function(file, xhr, data) {
            // data.append("name", $('#doc_name').val());
        },
        error: function(file, response) {
        },
        // addRemoveLinks: true,
        removedfile: function(file) {
            var name = file.name;
            $.ajax({
                type: "POST",
                url: url,
                data: "f="+name+"&_token="+token,
                dataType: 'html'
            });
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
        dictDefaultMessage: "Arrastra aquí el fichero o haz click para seleccionarlo (tamaño máx. 30MB y máximo 25 a la vez)",
        dictFallbackMessage: "Tu navegador no soporta drag'n'drop.",
        dictInvalidFileType: "Tipo de archivo no permitido.",
        dictCancelUpload: "Cancelar",
        dictCancelUploadConfirmation: "¿Está seguro de cancelar?",
        dictRemoveFile: "Borrar archivo",
        dictRemoveFileConfirmation: null,
        dictMaxFilesExceeded: "No puede adjunar mas archivos.",
    });



 </script>