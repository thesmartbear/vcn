@if( isset($texto) && isset($required) )
    {!! Form::label($campo, $texto." *") !!}
@elseif( isset($texto) )
    {!! Form::label($campo, $texto) !!}
@endif

@if(isset($help))
<span class="badge badge-help">{{$help}}</span>
@endif

{!! Form::textarea($campo, (isset($valor)?$valor:(isset($ficha)?$ficha->$campo:old($campo))), ['id'=> $campo,
    'placeholder' => isset($texto)?$texto:(isset($placeholder)?$placeholder:""),
    'class' => isset($novisible)?'form-control novisible':'form-control', 'rows'=> $rows ?? 4, 'cols'=> $cols ?? 50]) !!}