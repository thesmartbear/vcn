<script type="text/javascript">
$(document).ready(function() {

    $("#origen_id").change(function() {

        $("#suborigen_id > option").each(function(){
            $(this).remove();
        });

        $("#suborigen_id").empty();
        $("#suborigen_id").prop('disabled', true);
        // $('.disable-example').selectpicker('refresh');

        var origen_id = $("#origen_id").val();

        $.ajax({
          url: "{{route('manage.viajeros.suborigenes.index')}}",
          type: 'GET',
          dataType : 'json',
          data: {'origen_id': origen_id},
          success: function(data) {

            $("#suborigen_id").append($('<option>', {
                  value: 0,
                  text: ''
            }));

            $.each(data, function(i, item) {

                $("#suborigen_id").append($('<option>', {
                  value: i,
                  text: item
                }));

            });

            $("#suborigen_id").prop('disabled', false);
            // $('#suborigen_id').selectpicker('refresh');
            $("#suborigen_id").val('').trigger("chosen:updated");

          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        }); // end ajax call
    });

    $("#suborigen_id").change(function() {

        $("#suborigendet_id > option").each(function(){
            $(this).remove();
        });

        $("#suborigendet_id").empty();
        $("#suborigendet_id").prop('disabled', true);

        var origen_id = $("#suborigen_id").val();

        $.ajax({
          url: "{{route('manage.viajeros.suborigendetalles.index')}}",
          type: 'GET',
          dataType : 'json',
          data: {'origen_id': origen_id},
          success: function(data) {

            $("#suborigendet_id").append($('<option>', {
                  value: 0,
                  text: ''
            }));

            $.each(data, function(i, item) {

                $("#suborigendet_id").append($('<option>', {
                  value: i,
                  text: item
                }));

            });

            $("#suborigendet_id").prop('disabled', false);
            // $('#suborigendet_id').selectpicker('refresh');
            $("#suborigendet_id").val('').trigger("chosen:updated");
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        }); // end ajax call
    });

});
</script>