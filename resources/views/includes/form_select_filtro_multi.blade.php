{!! Form::label($campo, $texto) !!}
    @include('includes.form_input_cargando',['id'=> $campo.'-cargando'])
    <br>

{!! Form::select($campo, $select,
    ($valor[$campo]?explode(',',$valor[$campo]):[]),
    array('class'=> 'multiselect form-control', 'multiple'=>'multiple', 'id'=>'filtro-'.$campo, 'name'=> $campo.'[]'))  !!}