<?php
    $c = \VCN\Models\System\Campo::where('name',$campo)->first();

    if(!$c)
    {
        echo "Error";
        exit;
    }

    $v = $ficha->$campo?:$c->default;
?>

@if($c)

    @if($c->tipo==0 || $c->tipo==2)
        @include('includes.form_checkbox', [ 'campo'=> $campo."_check", 'texto'=> $c->nombre ." (Visible)"])
    @endif

    @if($c->tipo)

        @if($c->textarea)
            @include('includes.form_textarea_tinymce', [ 'campo'=> $campo, 'texto'=> $c->nombre, 'help'=> (isset($helpc)?$helpc:null)])
        @else
            @include('includes.form_input_text', [ 'campo'=> $campo, 'texto'=> $c->nombre, 'valor'=> $v, 'help'=> (isset($helpc)?$helpc:null)])
        @endif

    @endif

@endif