<script type="text/javascript">

@if($booking)
    var $archivado = "{{ConfigHelper::config('booking_status_archivado')}}";
@else
    var $archivado = "{{ConfigHelper::config('solicitud_status_archivado')}}";
@endif

var $token = "{{ Session::token() }}";
var $booking = "{{$booking}}";

var $id = 0;
var $status = 0;

$(document).ready(function() {

    $('table.dataTable').on('change', '.status-item', function(e) {
        e.preventDefault();

        $id = $(this).data('id');
        $status = $(this).val();

        if( $status == $archivado )
        {
            $('#modalArchivar').modal('show');
        }
        else
        {
            status_ajax($id,$status);
        }

    });

    $("#motivo").change(function() {

        if($(this).val() == 'otro')
        {
            $("#motivo_nota_div").show();
        }
        else
        {
            $("#motivo_nota_div").hide();
        }
        $('#archivar-ok').removeAttr('disabled');
    });

    $('#archivar-ok').click( function(e) {
        e.preventDefault();

        status_ajax($id,$status);
    });


    $('table.dataTable').on('click', '.checklist-item', function(e) {
        e.preventDefault();

        var $id = $(this).data('id');

        var $url = "/manage/solicitudes/ajax/"+$id+"/checklist";
        if($booking)
        {
            var $url = "/manage/bookings/ajax/"+$id+"/checklist";
        }

        var $data = {'_token': $token, 'id': $id };

        $.ajax({
            url: $url,
            type: 'GET',
            dataType : 'json',
            data: $data,
            success: function(data) {

                var $checklist;

                $checklist = "<ul class='checklist'>";
                $.each(data.checklist, function(i, item) {

                    $checklist += "<li><i class='"+ item.class  +" fa-fw' id='check-"+ item.id +"' ";
                    $checklist += "data-parent='"+ $id +"' data-id='"+ item.id +"'";
                    $checklist += "></i>"+ item.name +"</li>";

                });
                $checklist += "</ul>";


                $('#modalBookingChecklist #checklist-title').text(data.titulo);
                $('#modalBookingChecklist .modal-body #checklist').html($checklist);
                $('#modalBookingChecklist').modal('show');

                checklist_ajax( data.booking, $id );

            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        }); // end ajax call

    });

});

function status_ajax(id, status)
{
    var $url = "/manage/solicitudes/ajax/"+id+"/status";
    if($booking)
    {
        var $url = "/manage/bookings/ajax/"+id+"/status";
    }

    var $motivo = $('#modalArchivar #motivo').val();
    var $motivo_nota = $('#modalArchivar #motivo_nota').val();

    var $data = {'_token': $token, 'id': id, 'status': status, 'motivo': $motivo, 'motivo_nota': $motivo_nota };

    $.ajax({
      url: $url,
      type: 'POST',
      dataType : 'json',
      data: $data,
      success: function(data) {

        $('.dataTable').DataTable().ajax.reload();

      },
      error: function(xhr, desc, err) {
        console.log(xhr);
        console.log("Details: " + desc + "\nError:" + err);
      }
    }); // end ajax call
}


function checklist_ajax( booking, id )
{
    var $url = "/manage/solicitudes/ajax/"+id+"/checklist";
    if(booking)
    {
        $url = "/manage/bookings/ajax/"+id+"/checklist";
    }

    var $token = "{{ Session::token() }}";

    $("#checklist").on('click', 'i', function(e) {

        var $id = $(this).data('parent');
        var $checkid = $(this).data('id');

        var $data = { '_token': $token, 'id': $id, 'check_id': $checkid };

        $("#check-"+$checkid).removeClass().addClass('fa fa-spinner fa-spin fa-fw');

        $.ajax({
          url: $url,
          type: 'POST',
          dataType : 'json',
          data: $data,
          success: function(data) {

            // $("#check-"+data.check_id).removeClass();

            if(data.result)
            {
                $("#check-"+data.check_id).removeClass().addClass('fa fa-check-circle-o fa-fw');
            }
            else
            {
                $("#check-"+data.check_id).removeClass().addClass('fa fa-circle-thin fa-fw');
            }
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        }); // end ajax call
    });
}

</script>

<div class="modal fade" id="modalBookingChecklist">
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Booking Checklist :: <span id="checklist-title"></span></h4>
        </div>
        <div class="modal-body">

            <div id="checklist"></div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="modalArchivar">
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            {{-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> --}}
            <h4 class="modal-title">Motivo para archivar</h4>
        </div>
        <div class="modal-body">

          @include('includes.form_select', ['campo'=> 'motivo', 'texto'=> '', 'select'=> ConfigHelper::getArchivarMotivo()])

          <div id="motivo_nota_div" class="collapse">
            @include('includes.form_input_text', [ 'campo'=> 'motivo_nota', 'texto'=> 'Motivo otro'])
          </div>

        </div>
        <div class="modal-footer">
            {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button> --}}
            <button id="archivar-ok" type="button" class="btn btn-default" data-dismiss="modal" disabled>Aceptar</button>
        </div>
    </div>
</div>
</div>