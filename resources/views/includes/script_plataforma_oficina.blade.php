<script type="text/javascript">
$(document).ready(function() {

    $("#plataforma").change(function() {

        var plataforma = $("#plataforma").val();

        $("#oficina_id > option").each(function(){
            $(this).remove();
        });

        $("#oficina_id").val(null);
        $('#oficina_id').selectpicker('refresh');

        $.ajax({
          url: "{{route('manage.system.oficinas.json')}}",
          type: 'GET',
          dataType : 'json',
          data: {'plataforma': plataforma},
          success: function(data) { console.log(data);

            $("#oficina_id").append($('<option>', {
                  value: 0,
                  text: 'Todas'
            }));

            $.each(data, function(i, item) {

                $("#oficina_id").append($('<option>', {
                  value: i,
                  text: item
                }));

            });

            $("#oficina_id").prop('disabled', false);
            $('#oficina_id').selectpicker('refresh');
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        }); // end ajax call
    });
});
</script>