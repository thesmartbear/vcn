@if(count(config('vcn.plataformas')))
<div class="form-group">
    @if( auth()->user()->isFullAdmin() )
        @include('includes.form_select', ['campo'=> $campo, 'texto'=> 'Plataforma', 'select'=> ConfigHelper::plataformas(isset($todas) ? $todas : false)])
    @else
        @include('includes.form_checkbox', [ 'campo'=> $campo.'_check', 'valor'=> isset($valor) ? $valor : (isset($ficha) ? $ficha->$campo : null), 'texto'=> 'Privado'])
    @endif
</div>
@endif