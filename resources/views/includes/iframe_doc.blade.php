<canvas class="form-iframe" id="iframe-{{$doc->id}}">{{$doc->id}}</canvas>
<script>

    pdfjsLib.getDocument("{{ConfigHelper::iframe($archivo->doc, public_path($dir), $dir)}}").promise.then( function(pdf) {
        pdf.getPage(1).then(function(page) {

            var scale = 1.5;
            var viewport = page.getViewport({scale:scale});

            var canvas = document.getElementById('iframe-{{$archivo->id}}');
            var context = canvas.getContext('2d');

            canvas.height = viewport.height;
            canvas.width = viewport.width;

            var renderContext = {
                canvasContext: context,
                viewport: viewport
            };
            page.render(renderContext);

        });
    });

</script>