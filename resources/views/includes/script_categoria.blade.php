<script type="text/javascript">
$(document).ready(function() {

    $("#category_id").change(function() {

        $("#subcategory_id > option").each(function(){
            $(this).remove();
        });

        $("#subcategory_det_id > option").each(function(){
            $(this).remove();
        });

        $("#subcategory_id").val(null);
        $('#subcategory_id').selectpicker('refresh');

        $("#subcategory_det_id").val(null);
        $('#subcategory_det_id').selectpicker('refresh');

        $("#subcategory_id").prop('disabled', true);
        $("#subcategory_det_id").prop('disabled', true);

        var category_id = $("#category_id").val();

        $.ajax({
          url: "{{route('manage.subcategorias.index')}}",
          type: 'GET',
          dataType : 'json',
          data: {'category_id': category_id},
          success: function(data) {

            $("#subcategory_id").append($('<option>', {
                  value: 0,
                  text: ''
            }));

            $.each(data, function(i, item) {

                $("#subcategory_id").append($('<option>', {
                  value: i,
                  text: item
                }));

            });

            $("#subcategory_id").prop('disabled', false);
            $('#subcategory_id').selectpicker('refresh');
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        }); // end ajax call
    });

    $("#subcategory_id").change(function() {

        $("#subcategory_det_id > option").each(function(){
            $(this).remove();
        });

        $("#subcategory_det_id").val(null);
        $('#subcategory_det_id').selectpicker('refresh');

        $("#subcategory_det_id").prop('disabled', true);

        var category_id = $("#subcategory_id").val();

        $.ajax({
          url: "{{route('manage.subcategoria-detalles.index')}}",
          type: 'GET',
          dataType : 'json',
          data: {'subcategory_id': category_id},
          success: function(data) {

            $("#subcategory_det_id").append($('<option>', {
                  value: 0,
                  text: ''
            }));

            $.each(data, function(i, item) {

                $("#subcategory_det_id").append($('<option>', {
                  value: i,
                  text: item
                }));

            });

            $("#subcategory_det_id").prop('disabled', false);
            $('#subcategory_det_id').selectpicker('refresh');
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        }); // end ajax call
    });

});
</script>