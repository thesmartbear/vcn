<script type="text/javascript">
$(document).ready(function() {

    var url = "{{$url}}";

    $("#center_id").change(function() {

        $("#course_accommodation").empty();

        $("#course_accommodation").multiselect('destroy');//.prop('disabled', true);

        var center_id = $("#center_id").val();

        $.ajax({
          url: url,
          type: 'GET',
          dataType : 'json',
          data: {'centro_id': center_id},
          success: function(data) {

            $.each(data, function(i, item) {

                $("#course_accommodation").append($('<option>', {
                  value: i,
                  text: item
                }));

            });

            $("#course_accommodation").multiselect({
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true
            });
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
          }
        }); // end ajax call
    });

});
</script>