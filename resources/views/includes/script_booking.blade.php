<script type="text/javascript">

var $url = "{{route('manage.bookings.ajax', $ficha->id)}}";
var $token = "{{ Session::token() }}";

function bookingMultiSemanas()
{
    var $id = $("input[name='booking-cmulti_id']").val();
    var $semanas = $('#booking-multi-semanas').val(); //$(this).val();
    var $fecha = $('#booking-multi-fecha_ini').val();

    var $data = { '_token': $token, 'campo': 'cmulti', 'id': $id, 'fecha': $fecha , 'valor': $semanas};

    console.log($data);

    $("#booking-multi-fecha_fin").html('<i class="fa fa-spinner fa-spin"></i>');
    $("#booking-multi-precio").html('<i class="fa fa-spinner fa-spin"></i>');

    /*
    $('#booking-multi-especialidades-table').html('<i class="fa fa-spinner fa-spin fa-2x"></i>');
    $('#modalBooking-loading').modal();
    */

    $.ajax({
        url: $url,
        type: 'POST',
        dataType : 'json',
        data: $data,
        success: function(data) { console.log(data);

          if(data.result)
          {
            $('#booking-multi-especialidades-table').html('<i class="fa fa-spinner fa-spin fa-2x"></i>');
            $('#modalBooking-loading').modal();

            $('#booking-multi-fecha_fin').html(data.fecha);
            $('#booking-multi-precio').html(data.total_txt);
            window.location.reload();
          }
          else
          {
            if(data.bloques)
            {
              $("#booking-multi-fecha_fin").html('-');
              $("#booking-multi-precio").html('-');
              bootbox.alert(data.error);
            }
          }

          precioTotal();

        },
        error: function(xhr, desc, err) {
          console.log(xhr.responseText);
          console.log("Details: " + desc + "\nError:" + err);

          if(xhr.status == 401) { location.reload(); }

        }
      }); // end ajax call
}

function bookingCerradaSemi($reset)
{
    var $id = $("input[name='booking-ccerrada_id']").val();
    var $fecha = $("#course_start_date").val();
    var $data = { '_token': $token, 'campo': 'ccerrada-semi', 'id': $id, 'fecha': $fecha, 'reset': $reset };

    $('#course_end_date').html('<i class="fa fa-spinner fa-spin"></i>');

    // console.log($data);

    $.ajax({
      url: $url,
      type: 'POST',
      dataType : 'json',
      data: $data,
      success: function(data) { console.log(data);
        if(data.result)
        {
          $('#course_end_date').html(data.fecha);
        }
        else
        {
          $('#course_end_date').html(data.error);
        }
      },
      error: function(xhr, desc, err) {
        // console.log(xhr.responseText);
        console.log("Details: " + desc + "\nError:" + err);

        if(xhr.status == 401) { location.reload(); }
      }
    }); // end ajax call
}

function precioTotal()
{
  $("#booking-subtotal").html('<i class="fa fa-spinner fa-spin"></i>');
  $("#booking-subtotal-total").html('<i class="fa fa-spinner fa-spin"></i>');
  $("#booking-total").html('<i class="fa fa-spinner fa-spin"></i>');

  var $id = $("input[name='booking-id']").val();

  var $data = { '_token': $token, 'campo': 'booking-total', 'id':$id };
  console.log($data);

  $.ajax({
    url: $url,
    type: 'POST',
    dataType : 'json',
    data: $data,
    success: function(data) {
      // console.log(data);

      // var total = data.moneda + " " + data.result.total;
      var total = data.result.total_txt;
      $("#booking-total").html(total);
      // $("#booking-total-resumen").html(total);

      var subtotal = "";
      $.each(data.result.subtotal_txt, function( key, valor ) {
        subtotal += valor+"<br>";
      });

      $("#booking-subtotal").html(subtotal);
      $("#booking-subtotal-total").html(data.result.total_subtotal_txt);

      $("#booking-descuento_especial").html(data.result.descuento_especial_txt);
      $("#booking-descuento").html(data.result.descuento_txt);
      $("#booking-descuento-tr").hide();
      if(data.result.descuento)
      {
        $("#booking-descuento-tr").show();
      }

    },
    error: function(xhr, desc, err) {
      console.log(xhr.responseText);
      console.log("Details: " + desc + "\nError:" + err);

      if(xhr.status == 401) { location.reload(); }
    }
  }); // end ajax call

}

/*function precioObligatorios()
{
  $("input[data-obligatorio='1']").each( function() {
    var moneda = $(this).data('moneda');
    var precio = $(this).data('precio');
    $(this).parent().next("td").html(moneda+" "+precio); //.find('.booking-extra-precio')
  });
}*/

function precioLinea(id, campo, valor)
{
  $("#"+campo+"-precio-"+id).html(valor);
}


function bookingUpdate(field, valor)
{
  var $data = { '_token': $token, 'campo': 'booking-update', 'field': field, 'valor': valor};
  console.log($data);
  // $('#'+campo).html("<i class='fa fa-spin fa-spinner'></i>");

  $.ajax({
    url: $url,
    type: 'POST',
    dataType : 'json',
    data: $data,
    success: function(data) { console.log(data);
      // $('#'+campo)
    }
  }); // end ajax call
}

$('#transporte_no').click( function(e) {
  // e.preventDefault();

  var $v = $(this).is(':checked');
  if($v)
  {
    $('#transporte_otro_div').hide();
    $('#booking-vuelo-table').hide();
    $v = 1;
  }
  else
  {
    $('#transporte_otro_div').show();
    $('#booking-vuelo-table').show();
    $v = 0;
  }

  bookingUpdate('transporte_no',$v);

  if(!$('#paso1-extras').hasClass('no-fade'))
  {
    if($v)
    {
      $('#paso1-extras').fadeIn();
    }
    else
    {
      $('#paso1-extras').hide();
    }
  }

});

$('#transporte_otro').click( function(e) {
  // e.preventDefault();

  var $v = $(this).is(':checked');

  if($v)
  {
    $('#transporte_no_div').hide();
    $('#div_transporte_otro').show();
    $v = 1;
  }
  else
  {
    $('#transporte_no_div').show();
    $('#div_transporte_otro').hide();
    $v = 0;
  }

  bookingUpdate('transporte_otro',$v);

});

function ccerrada_semiopen_fecha(id)
{
  if(id<=0)
  {
    $('#course_start_date').attr("placeholder", "Seleccione primero una convocatoria");
    $('#course_start_date').val("Seleccione primero una convocatoria");
    $('#course_start_date').attr("readonly", true);
    return;
  }

  var d1 = $('#course_start_date').val();

  $('#course_start_date').removeAttr("readonly");
  $('#course_start_date').attr("placeholder", "Fecha Inicio");
  if( $('#course_start_date').val()=="Seleccione primero una convocatoria" )
  {
    $('#course_start_date').val("");
  }

  var $ccsemi = $("#ccerrada-"+id).data('semiopen');
  var $startDate = $("#ccerrada-"+id).data('start');
  var $endDate = $("#ccerrada-"+id).data('end');
  var $dayDate = $("#ccerrada-"+id).data('day');

  if( $ccsemi )
  {
    $('#booking-semiopen').show();

    if(typeof $('#course_start_date').data("DateTimePicker") !== 'undefined' )
    {
      $('#course_start_date').data("DateTimePicker").destroy();
      $('#course_start_date').val('');
    }

    var $disabled = [0,1,2,3,4,5,6]; //0:domingo, 1:lunes, martes, miercoles, jueves, viernes, sabado
    var $unset = $dayDate;

    if($unset==7) { $unset = 0; }

    if($unset==='')
    {
      $disabled = [];
    }
    else
    {
      // delete($disabled[$unset]);
      $disabled.splice($unset, 1);
    }

    try{
      $('#course_start_date').datetimepicker({
        locale: 'es',
        format: 'L',
        // useCurrent: true,
        daysOfWeekDisabled: $disabled,
        minDate: $startDate,
        maxDate: $endDate,
      }).val(d1);
    }
    catch(err)
    {
      $('#course_start_date').datetimepicker({
        locale: 'es',
        format: 'L',
        // useCurrent: true,
        daysOfWeekDisabled: $disabled,
        minDate: $startDate,
      }).val(d1);
      // bootbox.alert("Curso fuera de fechas");
    }

  }
  else
  {
    $('#booking-semiopen').hide();
  }

}

function ccerrada_alojamiento_precio(precio, moneda)
{
  var id = $("input[name='booking-alojamiento_id']").val();
  $("#ccerrada-alojamiento-precio-"+id).html(moneda +" "+precio);
}

function ccerrada_alojamiento_precio_init()
{
  var id = $("input[name='booking-alojamiento_id']").val();
  var moneda = $("#ccerrada-alojamiento-precio-"+id).data('moneda');
  var precio = $("#ccerrada-alojamiento-precio-"+id).data('precio');

  if( typeof precio !== 'undefined')
  {
    $("#ccerrada-alojamiento-precio-"+id).html(moneda +" "+precio);
  }
}

function cabierta_fecha_ini(id)
{
  if(id<=0)
  {
    // $('#booking-cabierta-fecha_ini').attr("placeholder", "Seleccione primero una convocatoria");
    // $('#booking-cabierta-fecha_ini').val("Seleccione primero una convocatoria");
    // $('#booking-cabierta-fecha_ini').attr("readonly", true);
    // return;

    $disabled = [];

    $('#booking-cabierta-fecha_ini').datetimepicker({
      locale: 'es',
      format: 'L',
      // useCurrent: true,
      // daysOfWeekDisabled: $disabled
    });

    $('#booking-alojamiento-fecha_ini').attr("placeholder", "Seleccione primero fecha del curso.");

    return;
  }

  //bug
  var d1 = $('#booking-cabierta-fecha_ini').val();
  var d2 = $('#booking-alojamiento-fecha_ini').val();

  $('#booking-cabierta-fecha_ini').removeAttr("readonly");
  $('#booking-cabierta-fecha_ini').attr("placeholder", "Fecha Inicio");
  if( $('#booking-cabierta-fecha_ini').val()=="Seleccione primero una convocatoria" )
  {
    $('#booking-cabierta-fecha_ini').val("");
  }

  var $startDate = $("#cabierta-"+id).data('start');
  var $endDate = $("#cabierta-"+id).data('end');
  var $dayDate = $("#cabierta-"+id).data('day');

  if(typeof $('#booking-cabierta-fecha_ini').data("DateTimePicker") !== 'undefined' )
  {
    $('#booking-cabierta-fecha_ini').data("DateTimePicker").destroy();
    $('#booking-cabierta-fecha_ini').val('');

    // $('#booking-alojamiento-fecha_ini').data("DateTimePicker").destroy();
    $('#booking-alojamiento-fecha_ini').val('');
  }

  var $disabled = [0,1,2,3,4,5,6]; //0:domingo, 1:lunes, martes, miercoles, jueves, viernes, sabado

  var $unset = $dayDate;
  if($unset==7) { $unset = 0; }

  if($unset==='')
  {
    $disabled = [];
  }
  else
  {
    // delete($disabled[$unset]);
    $disabled.splice($unset, 1);
  }


  $('#booking-cabierta-fecha_ini').datetimepicker({
    locale: 'es',
    format: 'L',
    // useCurrent: true,
    daysOfWeekDisabled: $disabled,
    // minDate: $startDate,
    // maxDate: $endDate,
  }).val(d1);

  console.log($disabled);

  //alojamiento
  $dayDate = $("#booking-cabierta-alojamiento").data('day') || false;
  $disabled = [0,1,2,3,4,5,6];

  $unset = $dayDate;

  if(!$unset)
  {
    $disabled = [1,2,3,4,5]; //por defecto
  }
  else
  {
    if($unset==7) { $unset = 0; }

    // delete($disabled[$unset]);
    $disabled.splice($unset, 1);
  }

  if(typeof $('#booking-alojamiento-fecha_ini').data("DateTimePicker") !== 'undefined' )
  {
    $('#booking-alojamiento-fecha_ini').data("DateTimePicker").destroy();
    $('#booking-alojamiento-fecha_ini').val('');
  }

  $('#booking-alojamiento-fecha_ini').datetimepicker({
    locale: 'es',
    format: 'L',
    daysOfWeekDisabled: $disabled,
  }).val(d2);

}

function cabierta_fechas()
{
  var $id = $("input[name='booking-cabierta_id']").val();
  var $semanas = $("#booking-cabierta-semanas").val();
  var $fecha = $("#booking-cabierta-fecha_ini").val();

  if($fecha=="") { return; }

  var $data = { '_token': $token, 'campo': 'cabierta-semanas', 'id': $id, 'fecha': $fecha, 'valor': $semanas};

  $('#booking-cabierta-precio').html("<i class='fa fa-spin fa-spinner'></i>");
  $("#booking-cabierta-precio-detalle").html("");

  $.ajax({
    url: $url,
    type: 'POST',
    dataType : 'json',
    data: $data,
    success: function(data) { console.log(data);

      if(data.result>0)
      {
        $("input[name='booking-cabierta_id']").val(data.id);

        $("#booking-cabierta-precio").html(data.total_txt);
        $("#booking-cabierta-fecha_fin").val(data.fechafin);

        if(data.descuento)
        {
          $("#booking-cabierta-precio-dto").html(data.descuento_txt);
          $("#booking-cabierta-precio-dtopagar").html(data.descuento_pagar_txt);
        }

        if(data.precio_extra)
        {
          var $detalle = "Precio base: " + data.precio_txt + " + Extra temporada: " + data.precio_extra_txt;
          $("#booking-cabierta-precio-detalle").html($detalle);
        }

        precioTotal();

        if(data.alerta)
        {
          $('#booking-cabierta-alerta').html(data.alerta);
          bootbox.alert(data.alerta);
        }

        cabierta_fecha_ini(data.id);
        $("#booking-convocatoria").html(data.convocatoria);

      }
      else
      {
        $('#booking-cabierta-precio').html("NO");
        // if(data.minimo)
        // {
        //   bootbox.alert("El curso tiene que ser mínimo de "+data.minimo+" semanas");
        // }
        // else
        {
          bootbox.alert(data.error);
        }
      }

      if(data.reload)
      {
        $('#modalBooking-loading').modal('show');
        window.location.reload();
      }

      if(data.caducado)
      {
        bootbox.alert("<strong>Atención.</strong> La fecha de inicio es anterior a la fecha actual.");
      }

    },
    error: function(xhr, desc, err) {
      console.log(xhr.responseText);
      console.log("Details: " + desc + "\nError:" + err);

      if(xhr.status == 401) { location.reload(); }

    }
  }); // end ajax call
}

function cabierta_alojamiento()
{
  // var $id = $("input[name='booking-alojamiento_id']").val();
  var $id = $('#booking-cabierta-alojamiento').val();
  var $semanas = $("#booking-alojamiento-semanas").val();
  var $fecha = $("#booking-alojamiento-fecha_ini").val();

  // if($fecha=="") { return; }

  var $data = { '_token': $token, 'campo': 'cabierta-alojamiento', 'id': $id, 'fecha': $fecha, 'valor': $semanas};

  console.log($data);

  $("#booking-alojamiento-precio").html("<i class='fa fa-spin fa-spinner'></i>");
  $("#booking-alojamiento-precio-detalle").html("");

  $.ajax({
    url: $url,
    type: 'POST',
    dataType : 'json',
    data: $data,
    success: function(data) { console.log(data);

      if(data.error)
      {
        $("#booking-alojamiento-precio").html("NO");
        $('#booking-alojamiento-semanas').val(0);

        bootbox.alert(data.error);
        return;
      }

      if(data.result==0 && $id>0)
      {
        $("#booking-alojamiento-precio").html("-");
      }
      else if(data.result==0 && data.id==0)
      {
        $("#booking-alojamiento-precio").html("-");
        $('#booking-alojamiento-semanas').val(0);
      }
      else
      {

        $("#booking-alojamiento-precio").html(data.total_txt);
        $("#booking-alojamiento-fecha_fin").val(data.fechafin);

        $(".booking-cabierta-alojamiento").removeClass('fa-check-circle-o').addClass('fa-circle-thin');
        $("#cabierta-alojamiento-"+data.id).removeClass('fa-circle-thin').addClass('fa-check-circle-o');

        $("input[name='booking-alojamiento_id']").val(data.id);
      }

      if(data.pagado)
      {
        bootbox.alert(data.error);
      }

      //Reload: hay varios alojamientos
      if($('#booking-cabierta-alojamiento option').size()>1)
      {
        if(data.reload)
        {
          $('#modalBooking-loading').modal('show');
          window.location.reload();
        }
      }

      if(data.precio_extra)
      {
        var $detalle = "Precio base: " + data.precio_txt + " + Extra temporada: " + data.precio_extra_txt;
        $("#booking-alojamiento-precio-detalle").html($detalle);
      }

      precioTotal();

      if(data.alerta)
      {
        $('#booking-alojamiento-alerta').html(data.alerta);
        bootbox.alert(data.alerta);
      }

    },
    error: function(xhr, desc, err) {
      console.log(xhr.responseText);
      console.log("Details: " + desc + "\nError:" + err);

      if(xhr.status == 401) { location.reload(); }

    }
  }); // end ajax call
}

function bookingExtra( $id, $tipo, $units )
{
  var $valor = $("#extra-"+$tipo+"-"+$id).hasClass('fa-check-circle-o');
  var $precio = $("#extra-"+$tipo+"-precio-"+$id).data('precio');
  var $unidades = $("#extra-"+$tipo+"-unidades-"+$id).val();

  var $data = { '_token': $token, 'campo': 'extra-'+$tipo, 'id': $id, 'valor': $valor, 'unidades': $units};

//   console.log($data);

  $("#extra-"+$tipo+"-"+$id).removeClass().addClass("fa fa-spin fa-spinner");

  $.ajax({
    url: $url,
    type: 'POST',
    dataType : 'json',
    data: $data,
    success: function(data) { console.log(data);

      $("#"+data.campo+"-"+data.id).removeClass("fa fa-spin fa-spinner");

      if(data.result)
      {
        $("#"+data.campo+"-"+data.id).removeClass('fa-circle-thin').addClass('fa fa-check-circle-o');
        $("#"+data.campo+"-unidades-"+data.id).show();

        if( typeof $unidades === 'undefined' )
        {
          precioLinea(data.id, data.campo, data.precio);
        }
        else
        {
          // precioLinea(data.id, data.campo, ($units * parseFloat(data.precio)) );
          precioLinea(data.id, data.campo, data.precio );

          if($units > data.unidades)
          {
            bootbox.alert(data.error);
          }

          $units = data.unidades;
          $("#"+data.campo+"-unidades-"+data.id).val($units);
        }

      }
      else
      {
        $("#"+data.campo+"-"+data.id).removeClass('fa-check-circle-o').addClass('fa fa-circle-thin');
        $("#"+data.campo+"-unidades-"+data.id).val(0);
        $("#"+data.campo+"-unidades-"+data.id).hide();
        precioLinea(data.id, data.campo, 0);
      }

      if(data.pagado)
      {
        bootbox.alert(data.error);
      }

      precioTotal();

    },
    error: function(xhr, desc, err) {
      console.log(xhr.responseText);
      console.log("Details: " + desc + "\nError:" + err);

      if(xhr.status == 401) { location.reload(); }

    }
  }); // end ajax call
}


function checkLOPD(_callback)
{ 
  @if(!ConfigHelper::config('es_rgpd'))
    _callback();
    return;
  @endif

  chk2 = $('#lopd_check2').is(':checked');
  chk3 = $('#lopd_check3').is(':checked');

  $b2ret = true;

  if(!chk2 || !chk3)
  {
    $b2ret = false;
  }

  if(!chk2 && !chk3)
  {
    $mensaje = "{{trans('area.booking.lopd_check2y3_no')}}";
  }
  else if(!chk2)
  {
    $mensaje = "{{trans('area.booking.lopd_check2_no')}}";
  }
  else if(!chk3)
  {
    $mensaje = "{{trans('area.booking.lopd_check3_no')}}";
  }

  if(!$b2ret)
  {
    bootbox.confirm({
      title: "LOPD",
      message: $mensaje,
      buttons: {
        confirm: {
          label: "{{trans('area.booking.lopd_check_seguir')}}",
          className: 'btn-warning'
        },
        cancel: {
          label: "{{trans('area.booking.lopd_check_volver')}}",
          className: 'btn-success'
        }
      },
      callback: function (result) {
        if(result)
        {
          $b2ret = true;
          _callback();
        }
      }
    });
  }
  else
  {
    _callback();
  }

  return $b2ret;
}

$(document).ready(function() {

  precioTotal();

  ccerrada_semiopen_fecha( $("input[name='booking-ccerrada_id']").val() );
  cabierta_fecha_ini( $("input[name='booking-cabierta_id']").val() );
  ccerrada_alojamiento_precio_init();

  $('.booking-ccerrada').click( function() {

    var $id = $(this).data('id');
    var $fecha = $('#course_start_date').val();
    var $data = { '_token': $token, 'campo': 'ccerrada', 'id': $id };

    var $alojamiento_id = $(this).data('alojamiento');

    if( $(this).hasClass('fa-check-circle-o') )
    {
      return;
    }

    var $cc = $("input[name='booking-ccerrada_id']").val();
    var $vuelos = $('#booking-vuelo-table tr').length;
    var $convos = $('.booking-ccerrada').size();

    $("#ccerrada-"+$id).removeClass().addClass("fa fa-spin fa-spinner");

    $.ajax({
      url: $url,
      type: 'POST',
      dataType : 'json',
      data: $data,
      success: function(data) { console.log(data);

        if(data.result)
        {
          $(".booking-ccerrada").removeClass().addClass('fa fa-circle-thin booking-ccerrada');
          $("#ccerrada-"+data.id).removeClass().addClass('fa fa-check-circle-o booking-ccerrada');

          $("input[name='booking-ccerrada_id']").val(data.id);

          if(data.online)
          {
            $('#paso1-alojamiento').fadeIn();
            $('#paso1-transporte').fadeIn();
          }

          if($vuelos>1 || $cc==0 || $convos>1)
          {
            $('#modalBooking-loading').modal('show');
            window.location.reload();
          }

          precioTotal();

          ccerrada_semiopen_fecha(data.id);

          if($alojamiento_id)
          {
            $(".booking-ccerrada-alojamiento").removeClass('obligatorio');
            $("#ccerrada-alojamiento-"+$alojamiento_id).click().addClass('obligatorio');
          }
          else
          {
            $(".booking-ccerrada-alojamiento").removeClass('obligatorio');
          }

        }

      },
      error: function(xhr, desc, err) {
        console.log(xhr);
        console.log("Details: " + desc + "\nError:" + err);

        if(xhr.status == 401) { location.reload(); }
      }
    }); // end ajax call

  });

  @if($booking->curso->es_convocatoria_semicerrada && !$booking->status_id)
    bookingCerradaSemi(0);
  @endif

  // $('#course_start_date').on('dp.change', function() {
  $('#booking-ccerrada-semi').click( function(e) {
    e.preventDefault();

    bookingCerradaSemi(1);

  });


  $('.booking-ccerrada-alojamiento').click( function() {

    if( $('.booking-ccerrada-alojamiento.obligatorio').length ) return;

    var $id = $(this).data('id');
    var $data = { '_token': $token, 'campo': 'ccerrada-alojamiento', 'id': $id};
    console.log($data);

    if( $(this).hasClass('fa-check-circle-o') )
    {
      return;
    }

    var $precio = $(this).data('precio');

    $("#ccerrada-alojamiento-"+$id).removeClass("fa-check-circle-o fa-circle-thin").addClass("fa-spin fa-spinner");

    $.ajax({
      url: $url,
      type: 'POST',
      dataType : 'json',
      data: $data,
      success: function(data) {
        console.log(data);

        if(data.result)
        {
          $(".booking-ccerrada-alojamiento").removeClass("fa-spin fa-spinner fa-check-circle-o").addClass('fa-circle-thin');
          $("#ccerrada-alojamiento-"+data.id).removeClass("fa-spin fa-spinner fa-circle-thin").addClass('fa-check-circle-o');

          $("input[name='booking-alojamiento_id']").val(data.id);

          if(data.precio)
          {
            if(data.precio.importe>0)
            {
              ccerrada_alojamiento_precio(data.precio.importe, data.precio.moneda);
            }
          }

          precioTotal();

        }
        else
        {
          $("#ccerrada-alojamiento-"+data.id).addClass('fa-circle-thin').removeClass("fa-spin fa-spinner");
        }

      },
      error: function(xhr, desc, err) {
        console.log(xhr.responseText);
        console.log("Details: " + desc + "\nError:" + err);

        if(xhr.status == 401) { location.reload(); }
      }
    }); // end ajax call

  });

  $('.booking-cabierta').click( function() {

    var $id = $(this).data('id');
    var $data = { '_token': $token, 'campo': 'cabierta', 'id': $id};

    if( $(this).hasClass('fa-check-circle-o') )
    {
      return;
    }

    $.ajax({
      url: $url,
      type: 'POST',
      dataType : 'json',
      data: $data,
      success: function(data) {

        if(data.result)
        {
          $(".booking-cabierta").removeClass('fa-check-circle-o').addClass('fa-circle-thin');
          $("#cabierta-"+data.id).removeClass('fa-circle-thin').addClass('fa-check-circle-o');

          $("input[name='booking-cabierta_id']").val(data.id);

          precioTotal();

          cabierta_fecha_ini(data.id);

          if(data.online)
          {
            $('#paso1-alojamiento').fadeIn();
          }
        }

      },
      error: function(xhr, desc, err) {
        console.log(xhr);
        console.log("Details: " + desc + "\nError:" + err);

        if(xhr.status == 401) { location.reload(); }
      }
    }); // end ajax call

  });

  $('#booking-cabierta-semanas').change( function() {

    cabierta_fechas();

  });

  // $('#booking-cabierta-fecha_ini').change( function() {
  $('#booking-cabierta-fecha_ini').on( 'dp.hide', function() {

    var $semanas = $("#booking-cabierta-semanas").val();
    if($semanas>0)
    {
      cabierta_fechas();
    }

  });

  // $('.booking-cabierta-alojamiento').click( function() {
  $('#booking-cabierta-alojamiento').change( function() {

    // var $id = $(this).data('id');
    var $id = $(this).val();
    $("input[name='booking-alojamiento_id']").val($id);

    // var $semanas = $("#booking-alojamiento-semanas").val();
    // var $fecha = $("#booking-alojamiento-fecha_ini").val();

    // if($fecha=="")
    // {
    //   $('#booking-alojamiento-fecha_ini').attr("placeholder", "Seleccione una fecha");
    //   bootbox.alert("Seleccione una fecha");
    //   return;
    // }

    // if($semanas==0)
    // {
    //   bootbox.alert("Seleccione algún valor para duración");
    //   return;
    // }

    cabierta_alojamiento();

  });

  // $('#booking-alojamiento-fecha_ini').on( 'dp.hide', function() {

  //   var $semanas = $("#booking-alojamiento-semanas").val();
  //   if($semanas>0)
  //   {
  //     cabierta_alojamiento();
  //   }

  // });

  $('#booking-alojamiento-semanas').change( function() {

    cabierta_alojamiento();

  });


  $('.booking-extra').click( function() {

    var $id = $(this).data('id');
    var $tipo = $(this).data('tipo');

    var $obligatorio = $("#extra-"+$tipo+"-"+$id).hasClass('obligatorio');
    var $valor = $("#extra-"+$tipo+"-"+$id).hasClass('fa-check-circle-o');

    if($obligatorio && $valor)
    {
      return;
    }

    var $unidades = $("#extra-"+$tipo+"-unidades-"+$id).val();

    var $units = 0;
    if( !$valor && !(typeof $unidades === 'undefined') )
    {
      bootbox.prompt("Introduce la cantidad", function(result) {
        if (result === null)
        {
          return;
        }
        else
        {
          $units = result;
          bookingExtra($id,$tipo,$units);
        }
      });
    }
    else
    {
      bookingExtra($id,$tipo,1);
    }

    if($obligatorio)
    {
      $("#extra-"+$tipo+"-"+$id).addClass('obligatorio');
    }

  });

  $('.booking-extra-unidades').change( function() {

    var $id = $(this).data('id');
    var $tipo = $(this).data('tipo');
    var $valor = $(this).val();
    var $precio = $(this).data('precio');

    var $data = { '_token': $token, 'campo': 'extra-unidades', 'id': $id, 'tipo': $tipo, 'valor': $valor, 'precio': $precio};

    $.ajax({
      url: $url,
      type: 'POST',
      dataType : 'json',
      data: $data,
      success: function(data) { console.log(data);

        precioLinea(data.id, data.tipo, data.precio);

        precioTotal();

      },
      error: function(xhr, desc, err) {
        console.log(xhr);
        console.log("Details: " + desc + "\nError:" + err);

        if(xhr.status == 401) { location.reload(); }

      }
    }); // end ajax call

  });

  $('.booking-vuelo').click( function() {

      var $id = $(this).data('id');
      var $valor = $("#vuelo-"+$id).hasClass('fa-check-circle-o');

      var $precio = 0; //?? pendiente?

      var $data = { '_token': $token, 'campo': 'booking-vuelo', 'id': $id, 'valor': $valor, 'precio': $precio};

      $("#vuelo-"+$id).removeClass().addClass("fa fa-spin fa-spinner");

      $.ajax({
        url: $url,
        type: 'POST',
        dataType : 'json',
        data: $data,
        success: function(data) { console.log(data);

          if(data.result)
          {
            $(".booking-vuelo").removeClass().addClass('fa fa-circle-thin booking-vuelo');
            $("#vuelo-"+data.id).removeClass().addClass('fa fa-check-circle-o booking-vuelo');
          }
          else
          {
            $("#vuelo-"+data.id).removeClass().addClass('fa fa-circle-thin booking-vuelo');
          }

          if(data.online);
          {
            $('#paso1-extras').fadeIn();
          }

          $("input[name='booking-vuelo_id']").val(data.id);
          // precioTotal();

        },
        error: function(xhr, desc, err) {
          console.log(xhr);
          console.log("Details: " + desc + "\nError:" + err);

          if(xhr.status == 401) { location.reload(); }

        }
      }); // end ajax call


  });

  function ajaxAeropuerto($id, $aeropuerto, $aporte)
  {
    var $valor = $("#vuelo-"+$aeropuerto+"-"+$id).hasClass('fa-check-circle-o');
    var $precio = 0; //?? pendiente?

    var $data = { '_token': $token, 'campo': 'booking-vuelo', 'id': $id, 'valor': $valor, 'aporte': $aporte, 'precio': $precio };

    $("#vuelo-"+$aeropuerto+"-"+$id).removeClass().addClass("fa fa-spin fa-spinner");
    $('#aporte').val($aporte);

    console.log($data);

    $.ajax({
      url: $url,
      type: 'POST',
      dataType : 'json',
      data: $data,
      success: function(data) { console.log(data);

        if(data.result)
        {
          $(".booking-vuelo-aeropuerto").removeClass().addClass('fa fa-circle-thin booking-vuelo-aeropuerto');
          $("#vuelo-"+$aeropuerto+"-"+$id).removeClass().addClass('fa fa-check-circle-o booking-vuelo-aeropuerto');
        }
        else
        {
          $("#vuelo-"+$aeropuerto+"-"+$id).removeClass().addClass('fa fa-circle-thin booking-vuelo-aeropuerto');
        }

        if(data.online);
        {
          $('#paso1-extras').fadeIn();
        }

        $("input[name='booking-vuelo_id']").val(data.id);
        // precioTotal();

      },
      error: function(xhr, desc, err) {
        console.log(xhr);
        console.log("Details: " + desc + "\nError:" + err);

        if(xhr.status == 401) { location.reload(); }

      }
    }); // end ajax call
  }

  $('.booking-vuelo-aeropuerto').click( function() {

      var $id = $(this).data('id');
      var $aeropuerto = $(this).data('aeropuerto');
      var $aporte = $(this).data('aporte');

      if($aporte=="OTRO")
      {
        bootbox.prompt("{{trans('area.booking.datos_aeropuerto')}}", function(result) {
          if (result === null)
          {
            return false;
          }
          else
          {
            $aporte = result;
            $('#vuelo_otro').html("("+ $aporte +")");
            ajaxAeropuerto($id, $aeropuerto, $aporte);
          }
        });
      }
      else
      {
        ajaxAeropuerto($id, $aeropuerto, $aporte);
      }

      // $('#transporte_recogida_bool').prop('checked',false);
      // $('#div_transporte_recogida_bool').hide();
  });

  $('#transporte_recogida_bool').click( function(e) {
    if($(this).is(':checked'))
    {
      $('#div_transporte_recogida_bool').show();
    }
    else
    {
      $('#div_transporte_recogida_bool').hide();
    }
  });

  $("#booking-seguro-ajax").change( function() {

    console.log("booking-seguro-ajax");

    var $id = $("input[name='booking-id']").val();
    var $valor = this.checked;

    var $data = { '_token': $token, 'campo': 'booking-seguro', 'id': $id, 'valor': $valor};
    // console.log($data);

    $('#booking-seguro-name').html('<i class="fa fa-spinner fa-spin"></i>');

    $.ajax({
        url: $url,
        type: 'POST',
        dataType : 'json',
        data: $data,
        success: function(data) {

          $html = "";
          if(data.result)
          {
            $html = data.seguro_txt;

          }

          $('#booking-seguro-name').html($html);

          precioTotal();

        },
        error: function(xhr, desc, err) {
          console.log(xhr.responseText);
          console.log("Details: " + desc + "\nError:" + err);

          if(xhr.status == 401) { location.reload(); }

        }
      }); // end ajax call

  });


  $('#booking-multi-fecha_ini').on( 'change', function() { console.log("booking-multi-fecha_ini");
    
    bookingMultiSemanas();
    return;
    
    var $tots = {{isset($multi['mtotsemanas'])?$multi['mtotsemanas']:0}};
    var $finiv = $(this).val();
    var $fini = $('#booking-multi-fecha_ini option:selected').text();

    $tots = $tots-$finiv + 1;

    $("#booking-multi-semanas > option").each(function() {
      $(this).remove();
    });
    $("#booking-multi-semanas").append($('<option>', {
      value: 0,
      text: 0
    }));
    for(var i = 1; i <= $tots; i++) {
      $("#booking-multi-semanas").append($('<option>', {
        value: i,
        text: i
      }));
    };
    $('#booking-multi-semanas').selectpicker('refresh');

    $('#booking-multi-fecha_fin').html('-');
    $('#booking-multi-precio').html('-');

  });

  $('#booking-multi-semanas').change( function() {

    bookingMultiSemanas();

  });

  $(".booking-multi-especialidad").change( function(e) {
    e.preventDefault();

    var $id = $(this).data('id');
    var $n = $(this).data('semana');
    var $valor = $(this).val();

    var $data = { '_token': $token, 'campo': 'cmulti-especialidad', 'id': $id, 'valor': $valor};
    console.log($data);

    $('#booking-multi-especialidad-precio-'+$n).html('<i class="fa fa-spinner fa-spin"></i>');

    $.ajax({
        url: $url,
        type: 'POST',
        dataType : 'json',
        data: $data,
        success: function(data) { console.log(data);

          if(data.result)
          {
            // $('#booking-multi-especialidad-precio-'+$n).html(data.precio_txt);
            $('#booking-multi-especialidad-precio-'+$id).html(data.precio_txt);
          }

          precioTotal();

        },
        error: function(xhr, desc, err) {
          console.log(xhr.responseText);
          console.log("Details: " + desc + "\nError:" + err);

          if(xhr.status == 401) { location.reload(); }

        }
      }); // end ajax call

  });

  $('.booking-extra-otro').click( function(e) {
    e.preventDefault();

    var $id = $(this).data('id');

    var $data = { '_token': $token, 'campo': 'booking-extra-otro', 'id': $id, 'valor': 1};

    $("#extra-otro-"+$id).removeClass().addClass("fa fa-spin fa-spinner");

    $.ajax({
        url: "{{route('manage.bookings.extra.delete')}}",
        type: 'POST',
        dataType : 'json',
        data: $data,
        success: function(data) { console.log(data);

          if(data.result)
          {
            $("#extra-otro-"+$id).parent().parent().remove();
          }

          precioTotal();

        },
        error: function(xhr, desc, err) {
          console.log(xhr.responseText);
          console.log("Details: " + desc + "\nError:" + err);

          if(xhr.status == 401) { location.reload(); }

        }
      }); // end ajax call
  });

});
</script>