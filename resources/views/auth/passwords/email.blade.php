@extends('layouts.login')

@section('formulario')

    @if (session('status'))
        <div>{{ session('status') }}</div>
        <hr>
        <a href="/auth/login">Volver</a>
    @else

        {!! Form::open(['route' => 'password.email', 'class' => 'form login-form']) !!}

            {!! csrf_field() !!}

            <div class="form-group">
                <label><i class="fa fa-envelope"></i> Introducir vuestro email para recuperar la contraseña</label>
                <div class="input-icon">
                    {!! Form::text('email', old('email'), ['class'=> 'form-control', 'autocomplete'=> 'off', 'placeholder'=> 'e-mail']) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::submit('Recuperar contraseña',['class' => 'btn btn-success']) !!}
                <a class="btn-default btn pull-right" href="{{ route('login') }}">Volver</a>
            </div>

        {!! Form::close() !!}



    @endif

@stop