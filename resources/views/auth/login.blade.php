@extends('layouts.login')


@section('formulario')

{!! Form::open(['url' => 'auth/login', 'class' => 'form login-form']) !!}

    {!! csrf_field() !!}

    <div class="row">
        <div class="col-xs-6">
            <label><i class="fa fa-user"></i> {!! trans('auth.user') !!}</label>
            {!! Form::text('username', old('username'), ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'autocomplete'=> 'off', 'placeholder'=> trans('auth.user')]) !!}
        </div>
        <div class="col-xs-6">
            <label><i class="fa fa-lock"></i> {!! trans('auth.password') !!}</label>
            {!! Form::password('password', ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'autocomplete'=> 'off', 'placeholder'=> trans('auth.password')]) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="forgot-password">
                <a href="{{ route('password.request') }}" id="forget-password" class="forget-password">{!! trans('auth.recordarpass') !!}</a>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="rem-password">
                <label class="rememberme mt-checkbox mt-checkbox-outline">
                    <input type="checkbox" name="remember" value="1"/> {!! trans('auth.recordar') !!}
                    <span></span>
                </label>
            </div>
        </div>
        <div class="col-sm-3">
            {!! Form::submit(trans('auth.entrar'),['class' => 'btn btn-success pull-right']) !!}
        </div>
    </div>

{!! Form::close() !!}

@stop