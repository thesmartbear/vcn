@extends('layouts.login')


@section('formulario')

<form method="POST" action="{{route('auth/password.reset.post')}}">
    {!! csrf_field() !!}
    <input type="hidden" name="token" value="{{ $token }}">

    <div class="form-group">
        <label>Tu Email</label>
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            {!! Form::text('email', old('email'), ['class'=> 'form-control', 'autocomplete'=> 'off', 'placeholder'=> 'e-mail']) !!}
        </div>
    </div>

    <div class="form-group">
        <label>Escoge tu nueva contraseña (con al menos 8 caracteres)</label>
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            {!! Form::password('password', ['class'=> 'form-control', 'autocomplete'=> 'off', 'placeholder'=> 'contraseña']) !!}
        </div>
    </div>

    <div class="form-group">
        <label>Vuelve a introducir tu nueva contraseña</label>
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            {!! Form::password('password_confirmation', ['class'=> 'form-control', 'autocomplete'=> 'off', 'placeholder'=> 'contraseña (confirmación)']) !!}
        </div>
    </div>

    <div class="form-actions">
        {!! Form::submit('Enviar',['class' => 'btn btn-primary pull-right']) !!}
    </div>

</form>

@stop