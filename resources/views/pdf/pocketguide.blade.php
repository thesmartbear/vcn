<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Pocket Guide</title>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>

        {!! Html::style('https://'.ConfigHelper::config('web').'/assets/css/pdf.css') !!}
        {!! Html::style('https://'.ConfigHelper::config('web').'/assets/css/bootstrap.css') !!}

        <style>
            caption{
                @if(ConfigHelper::config('sufijo') == 'bs')
                    color: #f1c40f;
                @elseif(ConfigHelper::config('sufijo') == 'cic')
                    color: #3B6990;
                @elseif(ConfigHelper::config('sufijo') == 'sf')
                    color: #24aab6;
                @else
                    color: #000000;
                @endif
            }
            h1{
                @if(ConfigHelper::config('sufijo') == 'bs')
                    border-bottom: 1px solid #f1c40f;
                @elseif(ConfigHelper::config('sufijo') == 'cic')
                    border-bottom: 1px solid #3B6990;
                @elseif(ConfigHelper::config('sufijo') == 'sf')
                    border-bottom: 1px solid #24aab6;
                @else
                   border-bottom: 1px solid #CCCCCC;
                @endif
            }
        </style>

    </head>
    <body id="pgpdf">
            @include('manage.cursos.pocketguide', ['ficha'=> $ficha])

    </body>

</html>