<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Presupuesto {{$ficha->id}}</title>

        <?php
            $p = $ficha->plataforma ?: 1;
            $sufijo = ConfigHelper::config('sufijo', $p);
            $web = ConfigHelper::config('web',$p);
        ?>

        {!! Html::style('assets/css/pdf.css') !!}
        {!! Html::style('assets/css/bootstrap.css') !!}


        <style>
            caption{
                @if($sufijo == 'bs')
                    color: #f1c40f;
                @elseif($sufijo == 'cic')
                    color: #3B6990;
                @elseif($sufijo == 'sf')
                    color: #24aab6;
                @else
                    color: #000000;
                @endif
            }
            h1{
                @if($sufijo == 'bs')
                    border-bottom: 1px solid #f1c40f;
                @elseif($sufijo == 'cic')
                    border-bottom: 1px solid #3B6990;
                @elseif($sufijo == 'sf')
                    border-bottom: 1px solid #24aab6;
                @else
                   border-bottom: 1px solid #CCCCCC;
                @endif
            }
            table.total thead td {
                @if($sufijo == 'bs')
                    color: #f1c40f;
                @elseif($sufijo == 'cic')
                    color: #3B6990;
                @elseif($sufijo == 'sf')
                    color: #24aab6;
                @else
                    color: #000000;
                @endif
            }
        </style>

    </head>
    <body>

        <div class="row">
            <div class="col-xs-8" style="margin-top: 15mm;">
                @if($ficha->oficina)
                    <p>Oficina: <strong>{{$ficha->oficina->name}}</strong>
                        <br />
                        {{$ficha->oficina->direccion}}<br />
                        {{$ficha->oficina->cp}} {{$ficha->oficina->poblacion}}
                        @if($ficha->oficina->provincia)
                            ({{$ficha->oficina->provincia->name}})
                        @endif
                        <br />
                        {{$ficha->oficina->telefono}} | <span class="text-lowercase">{{$ficha->oficina->email}}</span>
                    </p>
                @endif
            </div>
            <div class="col-xs-4">
                <img style="width: 4.5cm; height: auto; margin-top: 4px;" class="pull-right" src="https://{{$web}}/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb',$p)}}" />

                <span class="pull-right" style="margin-top: 5mm;">
                    Fecha emisión: {{Carbon::now()->format('d/m/Y')}}
                </span>
            </div>
        </div>

        <div class="row" style="margin-top: 1cm;">
            <div class="col-xs-12"><h1>Presupuesto {{$ficha->id}}</h1></div>
        </div>


        <div class="row">
            <div class="col-xs-12">
                <p class="estudiante"><b>Estudiante: {{$ficha->viajero->full_name}}</b></p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">

                <table class="table nombrecurso">
                    <tr>
                        <td>Nombre del Curso</td>
                        <td>{{ $ficha->curso->es_convocatoria_multi?$ficha->convocatoria->name:$ficha->curso->name}}</td>
                    </tr>
                    @if($ficha->curso->course_language_sessions)
                    <tr>
                        <td>Número de sesiones por semana</td>
                        <td>{{$ficha->curso->course_language_sessions}}</td>
                    </tr>
                    @endif
                    <tr>
                        <td>Centro</td>
                        <td>{{$ficha->centro->name}} ({{$ficha->centro->ciudad->city_name}}. {{$ficha->centro->pais->name}})</td>
                    </tr>
                </table>

                <table class="table">
                    <caption>Curso</caption>
                    {{-- <thead>
                        <tr class="thead">
                            <td class="col-md-5">Fecha</td>
                            <td>Duración</td>
                            <td align="right">Precio</td>
                        </tr>
                    </thead> --}}
                    <tbody>
                        <tr>
                            <td width="60%">Del {{$ficha->curso_start_date}} al {{$ficha->curso_end_date}}</td>
                            <td width="25%">{{$ficha->weeks}} {{$ficha->curso->getDuracionName($ficha->weeks)}}</td>
                            <td width="15%" align="right">
                                {{ConfigHelper::parseMoneda($ficha->course_total_amount, $ficha->curso_moneda)}}
                            </td>
                        </tr>

                        {{-- DESCUENTOS --}}
                        <?php
                        $descuentos = $ficha->getDescuentos();
                        $descuento = null;
                        if($descuentos->count()>0)
                        {
                            $descuento = $descuentos->first();
                        }
                        ?>

                        @if($descuento)
                        <tr>
                            <td colspan="2">Descuento aplicado [{{$descuento->name}}]:</td>
                            <td align='right'>
                                <span>
                                    {{ ConfigHelper::parseMoneda( $ficha->center_discount_amount, $ficha->curso_moneda) }}
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">Precio a pagar:</td>
                            <td align='right'>
                                <span>
                                    {{ ConfigHelper::parseMoneda( ($ficha->course_total_amount-$ficha->center_discount_amount), $ficha->curso_moneda) }}
                                </span>
                            </td>
                        </tr>
                        @endif

                        @if($ficha->curso->es_convocatoria_cerrada)
                            <tr>
                                <td colspan="3">
                                    @if($ficha->vuelo)
                                        <b>Vuelo:</b> {{$ficha->vuelo->aeropuerto}} [{{$ficha->vuelo->name}}]
                                        @if($ficha->transporte_requisitos)<br />Requisitos especiales: {{$ficha->transporte_requisitos}}@endif
                                        @if($ficha->transporte_recogida)<br />Aporte desde/a: {{$ficha->transporte_recogida}}@endif
                                    @else
                                        @if($ficha->transporte_no)
                                            NO necesito transporte
                                        @elseif($ficha->transporte_otro)
                                            Otro transporte: {{$ficha->transporte_detalles}}
                                        @else
                                            -
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endif

                    </tbody>
                </table>


                @if($ficha->curso->es_convocatoria_multi)
                {{-- Especialidades --}}
                <table class='table'>
                    <caption>Especialidades</caption>
                    <tbody id='booking-multi-especialidades-table'>
                        @foreach($ficha->multis as $esp)
                        <tr>
                            <td width="60%">Semana {{$esp->n}} [{{$esp->semana->semana}}]</td>
                            <td width="20%">
                                {{$esp->especialidad?$esp->especialidad->name:'-'}}
                            </td>
                            <td width="15%" align='right' id="booking-multi-especialidad-precio-{{$esp->id}}">{{ ConfigHelper::parseMoneda($esp->precio, $ficha->convocatoria->moneda_name) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif

                @if($ficha->alojamiento)
                <table class="table">
                    <caption>Alojamiento</caption>
                    {{-- <thead>
                        <tr class="thead">
                            <th>Fechas</th>
                            <th>Duración</th>
                            <th align="right">Precio</th>
                        </tr>
                    </thead> --}}
                    <tbody>
                    <tr>
                        <td width="60%">
                            <b>{{$ficha->alojamiento?$ficha->alojamiento->name:"-"}}</b> ::
                            Del {{Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y')}}
                            al {{Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y')}}
                        </td>
                        <td width="20%">
                            <?php $u = $ficha->accommodation_weeks; ?>
                            {{$ficha->accommodation_weeks}} {{$ficha->alojamiento?$ficha->alojamiento->getDuracionName($u):$ficha->curso->getDuracionName($u)}}
                        </td>
                        <td width="15%" align='right'>
                            {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
                        </td>
                    </tr>
                    </tbody>
                </table>
                @endif




                {{-- EXTRAS --}}
                @if(count($ficha->extras))
                    <table class="table">
                    <caption>Extras</caption>
                    {{-- <thead>
                        <tr class="thead">
                            <th>Extra</th>
                            <th>Unidad</th>
                            <th>Precio</th>
                        </tr>
                    </thead> --}}
                    <tbody>

                        @foreach($ficha->extras_curso as $extra)
                        <tr>
                            <td width="60%">{{$extra->name}}</td>
                            <td width="20%">
                                {{$extra->tipo_unidad_name}}

                                @if($extra->tipo_unidad)
                                    &nbsp;({{$extra->unidad_name}})
                                @endif
                                <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda->name) }}</i>
                                x{{$extra->unidades}}
                            </td>
                            <td width="15%" align="right">
                                {{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda->name) }}
                            </td>
                        </tr>
                        @endforeach

                        @foreach($ficha->extras_centro as $extra)
                            @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        @endforeach

                        @foreach($ficha->extras_alojamiento as $extra)
                            @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        @endforeach

                        @foreach($ficha->extras_generico as $extra)
                            @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        @endforeach

                        @foreach($ficha->extras_cancelacion as $extra)
                            @include('includes.bookings_tr_extra', ['extra'=> $extra])
                        @endforeach

                        @foreach($ficha->extrasOtros as $extra)
                        <tr>
                            <td>{{$extra->notas}}</td>
                            <td>
                                <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
                                x{{$extra->unidades}}
                            </td>
                            <td align="right">{{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda_name) }}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                @endif

                @if( $ficha->descuentos->count() )
                    <table class="table">
                    <caption>Descuentos</caption>
                    {{-- <thead>
                        <tr class="thead">
                            <th>Descuento</th>
                            <th>Precio</th>
                        </tr>
                    </thead> --}}
                    <tbody>
                        @foreach($ficha->descuentos as $dto)
                        <tr>
                            <td>{{$dto->notas}}</td>
                            <td>{{ConfigHelper::parseMoneda($dto->importe, $dto->moneda->name)}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif

                {{-- TOTAL --}}
                @include('manage.bookings.ficha-total')

                <div class="pie">
                    <p>Este presupuesto tiene una validez de 15 días y el precio en {{Session::get('vcn.moneda')}} queda sujeto al tipo de cambio vigente en el momento del pago final.</p>
                    <p>Para cualquier pregunta, contactar con {{$ficha->asignado->full_name}} por teléfono ({{$ficha->oficina?$ficha->oficina->telefono:"-"}}) o email ({{$ficha->asignado?$ficha->asignado->email:"-"}})</p>
                </div>
            </div>
        </div>
    </body>
</html>