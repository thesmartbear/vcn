@extends('pdf.recibo')

@section('contenido')

    <table width='100%'>
        <tbody>
        <tr>
            <td colspan="2">
                <strong>Centre:</strong> {{$booking->centro->name}}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>Curs:</strong> {{ $booking->programa}}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>Convocatòria:</strong> {{$booking->convocatoria->name}}
                @if($booking->curso->es_convocatoria_multi)
                    @foreach($booking->multis->sortBy('n') as $multi)
                        Setmana {{$multi->n}}: {{$multi->especialidad?$multi->especialidad->name:"-"}},
                    @endforeach
                @endif
            </td>
        </tr>
        <tr>
            <td colspan="2"><strong>Tipus Allotjament:</strong> {{$booking->alojamiento?$booking->alojamiento->name:"-"}}</td>
        </tr>
        <tr>
            <td width="50%"><strong>Data Inici:</strong> {{$booking->curso_start_date}}</td>
            <td><strong>Data Final:</strong> {{$booking->curso_end_date}}</td>
        </tr>
        </tbody>
    </table>

@stop

@section('datos')

    <?php
    $booking_precio = $booking->precio_total;
    $ficha = $booking;
    ?>

    <table width='100%'>
        <tbody>

        <tr>
            <td>Curs</td>
            <td colspan='2' align="right">
                {{ConfigHelper::parseMoneda($ficha->course_total_amount, $ficha->curso_moneda)}}
            </td>
        </tr>

        @if($ficha->accommodation_total_amount>0)
        <tr>
            <td>Allotjament</td>
            <td colspan='2' align='right'>
                {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
            </td>
        </tr>
        @endif

        <tr><th colspan="3">Extres</th></tr>

        @foreach($ficha->extrasCurso as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasCentro as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extras_alojamiento as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasGenerico as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasCancelacion as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasOtros as $extra)
            <tr>
                <td>{{$extra->name}} [{{$extra->notas}}]</td>
                <td>
                    <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
                    x {{$extra->unidades}}
                </td>
                <td align="right">{{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda_name) }}</td>
            </tr>
        @endforeach

        <tr>
            <td>SubTotal</td>
            <td colspan='2' align="right">
                @foreach($booking_precio['subtotal_txt'] as $subtotal)
                    {{$subtotal}}<br>
                @endforeach
            </td>
        </tr>

        @if($booking->divisa_variacion)
        <tr>
            <td>Variació Divisa</td>
            <td>{{ConfigHelper::parseMoneda($booking->divisa_variacion)}}</td>
        </tr>
        @endif

        @if($booking_precio['descuento_especial'] > 0)
            <tr>
                <td>Descompte Especial:</td>
                <td colspan='2' align="right">{{$booking_precio['descuento_especial_txt']}}</td>
            </tr>
        @endif

        @if($ficha->descuentos->count()>0)
            <tr>
                <th colspan="3">Descomptes</th></tr>
            @foreach($ficha->descuentos as $descuento)
                <tr>
                    <td>{{$descuento->notas}}</td>
                    <td colspan='2' align="right">{{ ConfigHelper::parseMoneda($descuento->importe, $descuento->moneda->name) }}</td>
                </tr>
            @endforeach
        @endif

        <tr>
            <th>
                <span class="text-uppercase">TOTAL</span>
                @if($ficha->es_cancelado || $ficha->es_refund || $ficha->pagado)
                    <i class='fa fa-lock'></i>
                @endif
            </th>
            <td colspan='2' align="right"><strong>{{$ficha->total_divisa_txt}}</strong></td>
        </tr>

        <tr>
            <td colspan="2">
                <i><small>Canvi aplicat: {{join(' | ', $booking->monedas_usadas_txt)}}</small></i>
            </td>
        </tr>

        <tr class="acuenta">
            <td>Entregues a compte</td>
            <td colspan='2' align='right'>{{ConfigHelper::parseMoneda($booking->saldo)}}</td>
        </tr>

        <tr class="pendiente">
            <td><strong>Pendent Pagament</strong></td>
            <td colspan='2' align='right'><strong>{{ConfigHelper::parseMoneda($booking->saldo_pendiente)}}</strong></td>
        </tr>

        </tbody>
    </table>


    <div class="instrucciones">
        <strong>
            Data límit de pagament: {{Carbon::parse($booking->course_start_date)->subDays(30)->format('d/m/Y')}}
            <br>
            Compte bancari: {{$booking->oficina?$booking->oficina->banco:"-"}} - {{$booking->oficina?$booking->oficina->txtIban($booking):"-"}}
            <br>
            Nota: preguem envieu una copia de la transferència a {{$booking->oficina?strtolower($booking->oficina->email):"-"}} incloent el nom del participant al programa.
        </strong>
        <br>
        <small>{{ConfigHelper::config('factura_iva_pie')}}</small>
    </div>
@stop