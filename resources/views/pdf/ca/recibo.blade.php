@extends('pdf.recibo')

@section('contenido')

    <p>
        <strong>Centre:</strong> {{$booking->centro->name}}
    </p>

    <p>
        <strong>Curs:</strong> {{ $booking->programa}}
        <br>
        <table width='100%'>
            <tbody>
                <tr>
                    <td width='50%'><strong>Convocatòria:</strong> {{$booking->convocatoria->name}}</td>
                    <td><strong>Tipus Allotjament:</strong> {{$booking->alojamiento?$booking->alojamiento->name:"-"}}</td>
                </tr>
                <tr>
                    <td><strong>Data Inici:</strong> {{$booking->curso_start_date}}</td>
                    <td><strong>Data Final:</strong> {{$booking->curso_end_date}}</td>
                </tr>
            </tbody>
        </table>
    </p>

@stop

@section('datos')
    <table width='100%'>
        <tbody>
            <tr>
                <td width='50%'>Número rebut: {{$ficha->id}}</td>
                <td>Data: {{$ficha->fecha_dmy}}</td>
            </tr>
            <tr>
                <td>Codi Contable: {{$booking->contable}}</td>
                <td>Usuari: {{$booking->asignado?$booking->asignado->full_name:'-'}}</td>
            </tr>
            <tr>
                <td>Import: {{$ficha->importe}} {{ConfigHelper::config('moneda')}}</td>
                <td></td>
            </tr>
            <tr>
                <td>Forma de pagament: {{$ficha->tipo_pago}}</td>
                <td></td>
            </tr>
            <tr>
                <td>Concepte: {{$booking->datos->fact_concepto?$booking->datos->fact_concepto:$ficha->notas}}</td>
                <td></td>
            </tr>
        </tbody>
    </table>
@stop