@extends('pdf.recibo')

@section('contenido')

    <table width='100%'>
        <tbody>
        <tr>
            <td colspan="2">
                <strong>Centro:</strong> {{$booking->centro->name}}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>Curso:</strong> {{ $booking->programa}}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>Convocatoria:</strong> {{$booking->convocatoria->name}}
                @if($booking->curso->es_convocatoria_multi)
                    @foreach($booking->multis->sortBy('n') as $multi)
                        Semana {{$multi->n}}: {{$multi->especialidad?$multi->especialidad->name:"-"}},
                    @endforeach
                @endif
            </td>
        </tr>
        <tr>
            <td colspan="2"><strong>Tipo Alojamiento:</strong> {{$booking->alojamiento?$booking->alojamiento->name:"-"}}</td>
        </tr>
        <tr>
            <td width="50%"><strong>Fecha Inicio:</strong> {{$booking->curso_start_date}}</td>
            <td><strong>Fecha Final:</strong> {{$booking->curso_end_date}}</td>
        </tr>
        </tbody>
    </table>


@stop

@section('datos')

    <?php
    $booking_precio = $booking->precio_total;
    $ficha = $booking;
    ?>

    <table width='100%'>
        <tbody>

        <tr>
            <td>Curso</td>
            <td colspan='2' align="right">
                {{ConfigHelper::parseMoneda($ficha->course_total_amount, $ficha->curso_moneda)}}
            </td>
        </tr>

        @if($ficha->accommodation_total_amount>0)
        <tr>
            <td>Alojamiento</td>
            <td colspan='2' align='right'>
                {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
            </td>
        </tr>
        @endif

        <tr><th colspan="3">Extras</th></tr>

        @foreach($ficha->extrasCurso as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasCentro as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extras_alojamiento as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasGenerico as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasCancelacion as $extra)
            @include('includes.bookings_tr_extra', ['extra'=> $extra])
        @endforeach

        @foreach($ficha->extrasOtros as $extra)
            <tr>
                <td>{{$extra->name}} [{{$extra->notas}}]</td>
                <td>
                    <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
                    x {{$extra->unidades}}
                </td>
                <td align="right">{{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda_name) }}</td>
            </tr>
        @endforeach

        <tr>
            <td>SubTotal</td>
            <td colspan='2' align="right">
                @foreach($booking_precio['subtotal_txt'] as $subtotal)
                    {{$subtotal}}<br>
                @endforeach
            </td>
        </tr>

        @if($booking->divisa_variacion && !$booking->promo_cambio_fijo)
        <tr>
            <td>Variación Divisa</td>
            <td>{{ConfigHelper::parseMoneda($booking->divisa_variacion)}}</td>
        </tr>
        @endif


        @if($booking_precio['descuento_especial'] > 0)
            <tr>
                <td>Descuento especial:</td>
                <td colspan='2' align="right">{{$booking_precio['descuento_especial_txt']}}</td>
            </tr>
        @endif

        @if($ficha->descuentos->count()>0)
            <tr>
                <th colspan="3">Descuentos</th></tr>
            @foreach($ficha->descuentos as $descuento)
                <tr>
                    <td>{{$descuento->notas}}</td>
                    <td colspan='2' align="right">{{ ConfigHelper::parseMoneda($descuento->importe, $descuento->moneda->name) }}</td>
                </tr>
            @endforeach
        @endif

        <tr>
            <th>
                <span class="text-uppercase">TOTAL</span>
                @if($ficha->es_cancelado || $ficha->es_refund || $ficha->pagado)
                    <i class='fa fa-lock'></i>
                @endif
            </th>
            <td colspan='2' align="right"><strong>{{$ficha->total_divisa_txt}}</strong></td>
        </tr>

        <tr>
            <td colspan="2">
                <i><small>Cambio aplicado: {{join(' | ', $booking->monedas_usadas_txt)}}</small></i>
            </td>
        </tr>

        <tr class="acuenta">
            <td><strong>Entregas a cuenta</strong></td>
            <td colspan='2' align='right'>{{ConfigHelper::parseMoneda($booking->saldo)}}</td>
        </tr>

        <tr class="pendientepago">
            <td><strong>Pendiente Pago</strong></td>
            <td colspan='2' align='right'><strong>{{ConfigHelper::parseMoneda($booking->saldo_pendiente)}}</strong></td>
        </tr>

        </tbody>
    </table>

    <div class="instrucciones">
        <strong>
        Fecha límite de pago: {{Carbon::parse($booking->course_start_date)->subDays(30)->format('d/m/Y')}}
        <br>
        Cuenta bancaria: {{$booking->oficina?$booking->oficina->banco:"-"}} - {{$booking->oficina?$booking->oficina->txtIban($booking):"-"}}
        <br>
        Nota: Rogamos se envíe una copia de la transferencia a {{$booking->oficina?strtolower($booking->oficina->email):"-"}} incluyendo el nombre del participante al
        programa.
        </strong>
        <br>
        <small>{{ConfigHelper::config('factura_iva_pie')}}</small>
    </div>

@stop