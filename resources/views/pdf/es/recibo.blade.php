@extends('pdf.recibo')

@section('contenido')

    <p>
        <strong>Centro:</strong> {{$booking->centro->name}}
    </p>

    <p>
        <strong>Curso:</strong> {{ $booking->programa}}
        <br>
        <table width='100%'>
            <tbody>
                <tr>
                    <td width='50%'><strong>Convocatoria:</strong> {{$booking->convocatoria->name}}</td>
                    <td><strong>Tipo Alojamiento:</strong> {{$booking->alojamiento?$booking->alojamiento->name:"-"}}</td>
                </tr>
                <tr>
                    <td><strong>Fecha Inicio:</strong> {{$booking->curso_start_date}}</td>
                    <td><strong>Fecha Final:</strong> {{$booking->curso_end_date}}</td>
                </tr>
            </tbody>
        </table>
    </p>

@stop

@section('datos')
    <table width='100%'>
        <tbody>
            <tr>
                <td width='50%'>Número recibo: {{$ficha->id}}</td>
                <td>Fecha: {{$ficha->fecha_dmy}}</td>
            </tr>
            <tr>
                <td>Código Contable: {{$booking->contable}}</td>
                <td>Usuario: {{$booking->asignado?$booking->asignado->full_name:'-'}}</td>
            </tr>
            <tr>
                <td>Importe: {{$ficha->importe}} {{ConfigHelper::config('moneda')}}</td>
                <td></td>
            </tr>
            <tr>
                <td>Forma de pago: {{$ficha->tipo_pago}}</td>
                <td></td>
            </tr>
            <tr>
                <td>Concepto: {{$booking->datos->fact_concepto?$booking->datos->fact_concepto:$ficha->notas}}</td>
                <td></td>
            </tr>
        </tbody>
    </table>
@stop