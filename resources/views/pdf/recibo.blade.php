<!DOCTYPE html>
<html lang="es">
    <head>

        <?php
            $p = $booking->plataforma ?: 1;
            $sufijo = ConfigHelper::config('sufijo', $p);
            $web = ConfigHelper::config('web',$p);
        ?>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{ConfigHelper::config('nombre',$p)}}</title>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>
        
        {!! Html::style('https://'.$web.'/assets/css/pdf.css') !!}
        {!! Html::style('https://'.$web.'/assets/css/bootstrap.css') !!}
        {!! Html::style('https://'.$web.'/assets/css/recibo.css') !!}

        <style>
            caption{
                @if($sufijo == 'bs')
                    color: #f1c40f;
                @elseif($sufijo == 'cic')
                    color: #3B6990;
                @elseif($sufijo == 'sf')
                    color: #24aab6;
                @else
                    color: #000000;
            @endif
        }
            h1{
                @if($sufijo == 'bs')
                    border-bottom: 1px solid #f1c40f;
                @elseif($sufijo == 'cic')
                    border-bottom: 1px solid #3B6990;
                @elseif($sufijo == 'sf')
                    border-bottom: 1px solid #24aab6;
                @else
                   border-bottom: 1px solid #CCCCCC;
            @endif
        }
        </style>

    </head>
    <body>

        <div class="page">
            <div class="row">
                <div class="col-xs-12"><img style="width: 4.5cm; height: auto; margin-top: 4px;" class="pull-right" src="https://{{$web}}/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb',$p)}}" /></div>
            </div>

            <div class="direccion">
                @section('direccion')
                    @if($booking->oficina)
                        <strong>{{$booking->oficina->name}}</strong>
                        <br>
                        {{$booking->oficina->direccion}}
                        <br>
                        {{$booking->oficina->cp}} {{$booking->oficina->poblacion}}
                        @if($booking->oficina->provincia)
                            ({{$booking->oficina->provincia->name}})
                        @endif
                        <br>
                        {{$booking->oficina->telefono}}
                        <br>
                        {{$booking->oficina->email}}
                    @endif
                @show
            </div>

            <div class="cliente">
                @section('cliente')
                    {{$booking->viajero->datos->fact_razonsocial?$booking->viajero->datos->fact_razonsocial:$booking->viajero->full_name}}
                    <br>
                    {{$booking->viajero->datos->fact_domicilio?$booking->viajero->datos->fact_domicilio:$booking->viajero->datos->direccion}}
                    <br>
                    {{$booking->viajero->datos->fact_poblacion?$booking->viajero->datos->fact_cp:$booking->viajero->datos->cp}} {{$booking->viajero->datos->fact_cp?$booking->viajero->datos->fact_poblacion:$booking->viajero->datos->ciudad}}
                @show
            </div>

            <div class="contenido">
                @yield('contenido')
            </div>

            <div class="datos">
                @yield('datos')
            </div>

        </div>

    </body>
</html>