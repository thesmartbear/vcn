<script type="text/javascript">
    jQuery(document).ready(function(){

        var col = 1;

        // dynamic table
        oTable = jQuery('#{!! $id !!}').DataTable({
            "sPaginationType":"full_numbers",
            "bProcessing":true,
            "responsive":true,
            "bAutoWidth":false,
            "sAjaxSource": "/manage/informes/booking-grupos?plataformas=0&oficinas=0&proveedores=0&centros=0&cursos=0&desde=&hasta=",
            "bServerSide":true,
            "iDisplayLength":100,
            "aoColumns":[
                { "mData":"viajero" },
                { "mData":"sexo" },
                { "mData":"fechanac" },
                { "mData":"curso" },
                { "mData":"convocatoria" },
                { "mData":"alojamiento" },
                { "mData":"comentarios" },
                { "mData":"extras" }
            ],
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;

                api.column(col, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                        );

                        last = group;
                    }
                } );
            }
        });

        // Order by the grouping
        $('#{!! $id !!} tbody').on( 'click', 'tr.group', function () {

            var currentOrder = oTable.order()[0];
            if ( currentOrder[0] === col && currentOrder[1] === 'asc' ) {
                oTable.order( [ col, 'desc' ] ).draw();
            }
            else {
                oTable.order( [ col, 'asc' ] ).draw();
            }
        });

    });
</script>
