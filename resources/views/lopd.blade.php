@extends('layouts.base')

@section('container')

    <style>
        .container {
            vertical-align: middle;
            font-family: 'Lato';
        }

        .content {
            display: inline-block;
        }

        .title {
            font-size: 2em;
            margin-bottom: 40px;
        }

        .quote {
            font-size: 1.5em;
        }
    </style>

    <div class="container">
        <div class="content">
            <div class="row logo">
                <div class="col-xs-12"><img style="width: 4.5cm; height: auto; margin-top: 30px;" src="https://{{$plataforma->web}}/assets/logos/{{App::getLocale()}}/{{$plataforma->logoweb}}" /></div>
            </div>

            <br><br>
            <p>
            @if($ficha)
                @if($ficha->idioma_contacto=="ca")
                    Canvis fets correctament. GRÀCIES.
                @else
                    Cambios realizados correctamente. GRACIAS.
                @endif
            @else
                Se ha producido un error. Contacte con su oficina.
            @endif
            </p>

        </div>
    </div>
@stop
