<!DOCTYPE html>
<html>
    <head>

        {!! $landing->gtm_head ?? "" !!}

        <title>{!! Traductor::trans('Landing', 'title', $landing) !!}</title>
        
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <link href="img/favicon.png" rel="shortcut icon"/>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{$urlAssets}}/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Custom CSS -->
        <link href="{{$urlAssets}}/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="{{$urlAssets}}/../css/style.css" rel="stylesheet" type="text/css"/>
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:700&display=swap" rel="stylesheet">

        <!-- Vue.js -->
        <script src="https://cdn.jsdelivr.net/vue/2.2.6/vue.min.js"></script>

        <style>
            .landing-fondo {
                height: 871px;
                padding-left: 9.5%;
            }
        </style>

    </head>
    <body>

        <!-- TITULO -->
        <!-- EDITABLE: Titulo y segundo subtitulo -->
        <section class="title">
            <div class="container-fluid padding-container landing-fondo">
                <div class="row d-flex title-box">
                    <div class="col-12">
                        <h2 class="text-black titulo-normal">{!! Traductor::trans('Landing', 'catalogo_form_title', $landing) !!}</h2>
                        <h2 class="text-black titulo-movil">{!! Traductor::trans('Landing', 'catalogo_form_title', $landing) !!}</h2>
                        <div class="ralla-negra"></div>
                        <h3 class="text-black">{!! Traductor::trans('Landing', 'catalogo_form_subtitle', $landing) !!}</h3>

                        @if($landing->catalogo_form_texto)
                            <div class="textos-pre-after">{!! Traductor::trans('Landing', 'catalogo_form_texto', $landing) !!}</div>
                        @endif

                    </div>
                </div>
            </div>
        </section>

        <!-- BOTON FIJO ARRIBA CUANDO SCROLL -->
        <div class="fixed-button">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-4 ml-5">
                    <img src="/assets/landings/logos/{{$sufijo}}logoweb_black.png" alt="">
                </div>
                <div class="col-md-3 ml-5">
                    <a href="#frmLanding">@lang("Solicitar")</a>
                </div>
            </div>
        </div>

        <!-- LOGO -->
        <img class="logo-british" src="/assets/landings/logos/{{$sufijo}}logoweb_white.png">
        
        <!-- FORMULARIO -->
        @section('bloque-formulario')
        <section class="form form-first">
            <div class="container-fluid padding-container">
                <div class="row d-flex justify-content-center">
                    <div class="col-11 col-lg-10 col-xl-7 block shadow">

                        @if($gracias)
    
                            <div class="row mt-5">
                                <div class="col-12 col-md-12 mt-5">
                                    <p>{!! Traductor::trans('Landing', 'catalogo_gracias', $landing) !!}</p>
                                </div>
                            </div>

                            <div class="row d-flex justify-content-center mas-info">
                                <div class="col-md-4">
                                    <a class="btn btn-primary mb-2" href="{{$landing->catalogo->pdf}}" download>{{trans('area.descargar')}}</a>
                                </div>
                            </div>
                        
                        @else
                            {{-- @yield('bloque-formulario-form') --}}

                            @section('bloque-formulario-form1')    
                            {!! Form::open(array('id'=> 'frmLanding', 'method'=> 'post', 'url' => route('web.landing.post', $landing->id))) !!}
                                <div class="row d-flex">

                                    {!! Form::hidden('utm_source', request()->input('utm_source', '')) !!}
                                    {!! Form::hidden('utm_medium', request()->input('utm_medium', '')) !!}
                                    {!! Form::hidden('utm_campaign', request()->input('utm_campaign', '')) !!}

                                    {{-- FORM IZQUIERDA (COMÚN) --}}
                                    <div class="col-12 col-md-6 padding">
                                            <div class="form-group">
                                                <label for="Nombre">@lang("landings.Nombre")</label>
                                                <input type="text" class="form-control" name="nombre" required aria-describedby="emailHelp" placeholder="Judit">
                                            </div>
                                            <div class="form-group">
                                                <label for="telefono">@lang("landings.Teléfono")</label>
                                                <input type="text" class="form-control" name="telefono" required placeholder="671 09 21 35">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">@lang("landings.E-mail")</label>
                                                <input type="email" class="form-control" name="email" required aria-describedby="emailHelp" placeholder="loremipsum@gmail.com">
                                            </div>
                                            <div class="form-group">
                                                <label for="select">@lang("landings.viajero")</label>
                                                <select class="form-control form-control-lg" name="es_viajero" required v-model="es_viajero">
                                                    <option value="">-</option>
                                                    <option value="1">@lang("landings.Sí")</option>
                                                    <option value="0">@lang("landings.No")</option>
                                                </select>
                                            </div>
                                    </div>

                                    {{-- FORM DERECHA (xTIPO) --}}
                                    @yield('bloque-formulario-form-right')

                                </div>
                            {!! Form::close() !!}
                            @show

                        @endif

                    </div>
                </div>
            </div>
        </section>
        @show

        <div class="row d-flex justify-content-center mas-info">
            <div class="col-md-3">
                <a class="btn btn-primary mb-2" href="{{route('web.landing', $landing->slug)}}">{{trans('web.volver')}}</a>
            </div>
        </div>

        <!-- OFICINAS -->
        @if($sufijo == "bs")
        <section class="oficinas">
            <div class="container-fluid padding-container">
                <div class="row d-flex justify-content-center m-0">
                    <div class="col-md-10 col-lg-9 col-xl-11">

                        <div class="row">
                            <div class="col-12">
                                <h2 class="text-center titulo-2">@lang("landings.oficinas")</h2>
                            </div>
                        </div>

                        <div class="row d-flex justify-content-center">
                            <div class="col-12 col-sm col-md-6 col-xl-3 padding margin-1">
                                <h5>BARCELONA</h5>
                                <p>Vía Augusta, 33,</p>
                                <p>08006, Barcelona</p>
                                <p>Tel. <span class="underline"><a href="tel:932008888">93 200 88 88</a></span></p>
                                <p><span class="underline"><a href="mailto:infobs@britishsummer.com">infobs@britishsummer.com</a></span></p>
                            </div>
                            <div class="col-12 col-md-6 col-xl-3 padding margin-1">
                                <h5>MADRID</h5>
                                <p>Paseo de la Castellana, 136, bajos,</p>
                                <p>28046, Madrid</p>
                                <p>Tel. <span class="underline"><a href="tel:913459565">91 345 95 65</a></span></p>
                                <p><span class="underline"><a href="mailto:madrid@britishsummer.com">madrid@britishsummer.com</a></span></p>
                            </div>
                            <div class="col-12 col-md-6 col-xl-3 padding margin-2">
                                <h5>GIRONA</h5>
                                <p>Carrer Migdia, 25,</p>
                                <p>17002, Girona</p>
                                <p>Tel. <span class="underline"><a href="tel:972414902">972 414 902</a></span></p>
                                <p><span class="underline"><a href="mailto:girona@britishsummer.com">girona@britishsummer.com</a></span></p>
                            </div>
                            <div class="col-12 col-md-6 col-xl-3 padding margin-2">
                                <h5>SEVILLA</h5>
                                <p>Pza. Cristo de Burgos, 21, Bajo A,</p>
                                <p>41003, Sevilla</p>
                                <p>Tel. <span class="underline"><a href="tel:954210785">95 421 07 85</a></span></p>
                                <p><span class="underline"><a href="mailto:sevilla@britishsummer.com">sevilla@britishsummer.com</a></span></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        @endif

        <!-- WHATSAPP -->
        <section class="whatsapp">
            <div class="bg-red d-flex align-items-center">
                <div class="container-fluid">
                    <div class="row d-flex justify-content-center align-items-center m-0">
                        <a href="https://api.whatsapp.com/send?phone={{$landing->phone}}">
                        <img class="mr-3" width="68" src="{{$urlAssets}}/img/v4---general-iconfinderphone8216688@2x.png" alt="">
                        <h4 class="underline ml-2 text-white m-0">{{$landing->phone}}</a></h4>
                        </a>
                    </div>
                </div>
            </div>
            
        </section>

        <!-- Scripts -->
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{$urlAssets}}/js/jquery-3.4.1.min.js"></script>
        <script src="{{$urlAssets}}/js/bootstrap.min.js "></script>
        
        <script>
            $(window).bind('scroll', function () {
                if ($(window).scrollTop() > 10) {
                    $('.fixed-button').css("display", "block");
                } 
                else {
                    $('.fixed-button').css("display", "none");
                }
            });

            new Vue({
                el: '#frmLanding',
                data: {
                    es_viajero: null,
                },
                mounted: function() {
                },
            });

            new Vue({
                el: '#frmLandingRepe',
                data: {
                    es_viajero: null,
                },
                mounted: function() {
                },
            });

        </script>

        <!-- End of Scripts -->

        {!! $landing->gtm_body ?? "" !!}

        {{-- AC SCRIPT --}}
        <script type="text/javascript">
            (function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
            vgo('setAccount', '225344043');
            vgo('setTrackByDefault', true);
        
            vgo('process');
        </script>
        
    </body>
</html>
