<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- BEGIN THEME STYLES -->
    {!! Html::style('assets/css/components.css') !!}
    {!! Html::style('assets/css/plugins.css') !!}
    {!! Html::style('assets/layout/css/layout.css') !!}
    {!! Html::style('assets/layout/css/themes/darkblue.css') !!}
    {!! Html::style('assets/layout/css/custom.css') !!}

    <title>
        @section('title')
            {{ConfigHelper::config('web')}}
        @show
    </title>

    <link href="{{ mix('/css/app.css')}}" rel="stylesheet" type="text/css"/>
    {!! Html::style('assets/css/bootstrap.css') !!}
    
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100,400,700,700italic,400italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('extra_head')

</head>
<body>

    @yield('container')

    <!-- Scripts -->
    {!! Html::script('assets/js/jquery-1.11.3.min.js') !!}
    {!! Html::script('assets/js/bootstrap.min.js') !!}

    <script src="{{mix('/js/app.js')}}"></script>

    @yield('extra_footer')

    @stack('scripts')

</body>
</html>