@extends('layouts.base')

@section('extra_head')
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    {!! Html::style('assets/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('assets/plugins/simple-line-icons/simple-line-icons.min.css') !!}
    {!! Html::style('assets/css/bootstrap.css') !!}
    {!! Html::style('assets/plugins/uniform/css/uniform.default.css') !!}
    {!! Html::style('assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css') !!}
            <!-- END GLOBAL MANDATORY STYLES -->

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>

    <!-- BEGIN THEME STYLES -->
    {!! Html::style('assets/css/components.css') !!}
    {!! Html::style('assets/css/components_old.css') !!}
    {!! Html::style('assets/css/plugins.css') !!}
    {!! Html::style('assets/layout/css/login.css') !!}
@stop

@section('container')
    <div class="login">
        <div class="row bs-reset">
            <div class="col-md-6 bs-reset">
                <div class="login-bg" style="background-image: url('/assets/login/login1.jpg');">
                    <img class="login-logo" src="/assets/logos/{{ConfigHelper::config('logo')}}">
                </div>
            </div>
            <div class="col-md-6 login-container bs-reset">
                <div class="login-content">
                    {{--<h1>{{ ConfigHelper::config('nombre') }} @yield('titulo','Administración')</h1>--}}
                    <p></p>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <button class="close" data-close="alert"></button>
                                <span>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </span>
                        </div>
                    @endif

                    @yield('formulario')

                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra_footer')
    {!! Html::script('assets/plugins/jquery.backstretch.min.js') !!}
    <script>
        $(".login-bg").backstretch(["/assets/login/login1.jpg","/assets/login/login2.jpg","/assets/login/login3.jpg"], {
            fade: 1500,
            duration: 8000
        })
    </script>
@endsection