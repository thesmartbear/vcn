<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <link rel="shortcut icon" href="/favicon.ico"/>

    <title>
        @section('title')
            {{ConfigHelper::config('web')}}

            <?php
                $version = "v.".config('vcn.version');
            ?>
        @show
    </title>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    {!! Html::style('assets/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('assets/plugins/simple-line-icons/simple-line-icons.min.css') !!}
    {!! Html::style('assets/css/bootstrap.css') !!}
    {!! Html::style('assets/plugins/uniform/css/uniform.default.css') !!}
    {!! Html::style('assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css') !!}
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>


    <!-- PLUGINS STYLES -->
    {!! Html::style('assets/plugins/dropzone/css/basic.css') !!}
    {{--Html::style('assets/plugins/dropzone/css/dropzone.css')--}}
    {!! Html::script('assets/plugins/dropzone/dropzone.min.js') !!}

    {!! Html::style('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') !!}

    {!! Html::style('assets/plugins/multiselect/css/bootstrap-multiselect.css') !!}

    {!! Html::style('assets/plugins/raty/jquery.raty.css') !!}
    {!! Html::style('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') !!}

    {!! Html::style('assets/plugins/select2/css/select2.min.css') !!}
    <!-- END PLUGINS STYLES -->



    <!-- BEGIN THEME STYLES -->
    {!! Html::style('assets/css/components.css') !!}
    {!! Html::style('assets/css/components_old.css') !!}
    {!! Html::style('assets/css/plugins.css') !!}
    {!! Html::style('assets/layout/css/layout.css') !!}
    {!! Html::style('assets/layout/css/themes/light.css') !!}
    {!! Html::style('assets/layout/css/profile.css') !!}
    {!! Html::style('assets/layout/css/custom.css') !!}

    {!! Html::style("assets/css/manage.css?$version") !!}
    {!! Html::style("assets/css/area.css?$version") !!}
    <!-- END THEME STYLES -->

    {!! Html::style('assets/plugins/chosen/chosen.min.css') !!}

    {!! Html::style('assets/css/magnific-popup.css') !!}

    {!! Html::script('assets/js/jquery-1.11.3.min.js') !!}

    <!-- DataTables -->
    {!! Html::style('assets/plugins/DataTables/datatables.min.css') !!}
    {!! Html::script('assets/plugins/DataTables/datatables.min.js') !!}
    {{-- {!! Html::style('assets/plugins/datatables/plugins/integration/bootstrap/3/dataTables.bootstrap.css') !!} --}}
    {{-- {!! Html::script('assets/plugins/datatables/js/jquery.dataTables.min.js') !!} --}}

    {{--
    {!! Html::script('assets/plugins/flot/jquery.flot.min.js') !!}
    {!! Html::script('assets/plugins/flot/jquery.flot.categories.min.js') !!}
    {!! Html::script('assets/plugins/flot/jquery.flot.stack.min.js') !!}
    --}}

    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') !!}
    {!! Html::style('assets/plugins/morris/morris.css') !!}
    {!! Html::script('assets/plugins/morris/morris.min.js') !!}

    {!! Html::script('//mozilla.github.io/pdf.js/build/pdf.js') !!}

    {!! Html::script('assets/plugins/jquery-validation/jquery.validate.min.js') !!}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    {!! Html::style('assets/plugins/jstree/themes/metronic/style.min.css') !!}

    @yield('extra_head')

</head>
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-footer-fixed page-container-bg-solid">

    @section('navbar')
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="https://{{ConfigHelper::config('web')}}">
                        @if (ConfigHelper::config('logo'))
                            {!! Html::image('/assets/logos/'.ConfigHelper::config('logo'), ConfigHelper::config('nombre'), array('class' => 'logo-default')) !!}
                        @else
                            {{ ConfigHelper::config('nombre') }}
                        @endif
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->

                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                {{-- <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">

                        <li class="user">
                            [{{auth()->user()->role_name}}]: [{{ConfigHelper::username()}}]
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                <i class="ifa fa fa-sign-out fa-fw"></i> {{ __('Salir') }}
                            </a>
                        </li>
                    </ul>
                </div> --}}
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">

                        <li class="dropdown dropdown-user pull-right">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle img-avatar" src="{{ ConfigHelper::avatar() }}">
                                <span class="username username-hide-on-mobile"> [{{ConfigHelper::username()}}] </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <img class="foto-avatar" src="{{ ConfigHelper::avatar() }}">
                                </li>

                                <li>
                                    <a href="{{ route('area.datos.index') }}" class=""> 
                                        <i class="ifa fa fa-user fa-fw"></i> @lang('area.Mis Datos')
                                    </a>
                                </li>

                                @if(Session::get('vcn.admin'))
                                
                                    @php
                                    $sign = "<i class='fa fa-sign-out'></i> Admin";
                                    $logout = "<a href='". route('manage.system.admins.logout')  ."'> $sign </a>";
                                    @endphp

                                    <li>{!! $logout !!}</li>

                                @endif
                                
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="ifa fa fa-sign-out fa-fw"></i> {{ __('Salir') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                        
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
    @show

    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="margin-top:25px;">
                    <li class="start ">
                        <?php
                            $home = Session::get('vcn.manage.area.home', '/area');
                            if(!$home)
                            {
                                $home = "/area";
                            }
                        ?>

                        <a href="{{ $home }}">
                            <i class="icon-home"></i>
                            <span class="title">{!! trans('area.resumen') !!}</span>
                        </a>
                    </li>

                    @if(auth()->user()->compra)
                        <li class="">
                            <a href="{{ route('area.comprar.booking') }}">
                                <i class="fa fa-ticket"></i>
                                <span class="title">{!! trans('area.compra') !!}</span>
                            </a>
                        </li>
                    @endif

                    <li>
                    @section('navbar-manager')
                        @foreach($menuArea as $m1)
                            <li class="dropdown">
                                @if ( count($m1['submenu'])>0)
                                    <a href="{{$m1['route']}}">
                                        <i class="fa {{$m1['icono']}}" ></i>
                                        <span class="title">{!! trans('area.'.$m1['nom']) !!}</span>
                                        <span class="arrow "></span>
                                    </a>
                                    <ul class="sub-menu">
                                        @foreach ( $m1['submenu'] as $m2 )
                                            @if( $m2['route']=='!' )
                                                <li class="dropdown-header"> {!! trans('area.'.$m2['nom']) !!} :</li>
                                                <li class="divider"></li>
                                            @elseif( $m2['route']=="-" )
                                                <li class="divider"><hr></li>
                                            @else
                                                <li>
                                                    <a href="{{$m2['route']}}">
                                                        <i class="fa {{$m2['icono']}}"></i>
                                                        <span class="title">{!! trans('area.'.$m2['nom']) !!}</span>
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                @else
                                    <a href="{{$m1['route']}}">
                                        <i class="fa {{$m1['icono']}}"></i>
                                        <span class="title">{!! trans('area.'.$m1['nom']) !!}</span>
                                    </a>
                                @endif
                            </li>
                        @endforeach
                    @show
                    </li>

                    @if(ConfigHelper::es_booking_online() && $usuario && ($usuario->es_tutor || $usuario->es_viajero) )
                        <li class="">
                            <a href="{{ route('area.deseos') }}">
                                <i class="fa fa-star"></i>
                                <span class="title">{!! trans('area.Mis Favoritos') !!}</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="{{ route('area.pendientes', $usuario->id) }}">
                                <i class="fa fa-edit"></i>
                                <span class="title">{!! trans('area.Bookings pendientes') !!}</span>
                            </a>
                        </li>
                    @endif


                    @if($usuario && $usuario->es_monitor && $usuario->monitor->es_director)

                        <li class="">
                            <a href="{{ route('area.monitor.bookings') }}">
                                <i class="fa fa-book"></i>
                                <span class="title">Bookings</span>
                            </a>
                        </li>

                    @endif

                    {{-- Exams --}}
                    @if($usuario && ($usuario->es_viajero || $usuario->es_tutor))
                        <li class="">
                            <a href="{{ route('area.exams') }}">
                                <i class="fa fa-list"></i>
                                <span class="title">{!! trans('area.Examenes') !!}</span>
                            </a>
                        </li>
                    @endif

                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Modal title</h4>
                            </div>
                            <div class="modal-body">
                                Widget settings form goes here
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn blue">Save changes</button>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- BEGIN PAGE HEADER-->
                <div class="page-bar">
                    @yield('breadcrumb')
                </div>
               @section('titulo-seccion')
                    <h3 class="page-title">
                        @yield('titulo')
                    </h3>
               @show
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->

                @if( Session::get('mensaje') )

                    <div class="note note-warning">
                        <h4 class="block">@lang('area.booking.atencion')</h4>
                        <p>
                            {!! Session::get('mensaje') !!}
                        </p>
                    </div>

                @endif

                @if( Session::get('mensaje-alert') )
                    <div class="note note-danger">
                        <h4 class="block">Alerta</h4>
                        <p>
                            {!! Session::get('mensaje-alert') !!}
                        </p>
                    </div>
                @endif

                @if( Session::get('mensaje-ok') )

                    <div class="note note-success">
                        <h4 class="block">OK</h4>
                        <p>
                            {!! Session::get('mensaje-ok') !!}
                        </p>
                    </div>

                @endif

                @section('container')
                     @yield('content')
                @show
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->


    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner">
            {{ date('Y') }} &copy; {{ConfigHelper::config('nombre')}}
            - {{ConfigHelper::versionLite()}} - {{ConfigHelper::infoLogin()}}
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- END FOOTER -->


    <!-- Scripts -->
    {{-- {!! Html::script('assets/js/jquery-1.11.3.min.js') !!} --}}

    {!! Html::script('assets/plugins/jquery-migrate.min.js') !!}
    {!! Html::script('assets/plugins/jquery-ui/jquery-ui.min.js') !!}
    {!! Html::script('assets/js/bootstrap.min.js') !!}
    {!! Html::script('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') !!}
    {!! Html::script('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') !!}
    {!! Html::script('assets/plugins/jquery.blockui.min.js') !!}
    {!! Html::script('assets/plugins/jquery.cokie.min.js') !!}
    {{-- Html::script('assets/plugins/uniform/jquery.uniform.min.js') --}}
    {!! Html::script('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') !!}
    <!-- END CORE PLUGINS -->
    {!! Html::script('assets/layout/scripts/metronic.js') !!}
    {!! Html::script('assets/layout/scripts/layout.js') !!}
    {!! Html::script('assets/layout/scripts/quick-sidebar.js') !!}
    {!! Html::script('assets/layout/scripts/demo.js') !!}

    {!! Html::script('assets/plugins/moment-with-locales.min.js') !!}
    {!! Html::script('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('assets/plugins/multiselect/js/bootstrap-multiselect.js') !!}
    {!! Html::script('assets/plugins/chosen/chosen.jquery.min.js') !!}
    {!! Html::script('assets/plugins/bootbox.min.js') !!}
    {!! Html::script('assets/plugins/select2/js/select2.min.js') !!}

    <!-- DataTables -->
    {{-- {!! Html::script('assets/plugins/datatables/js/jquery.dataTables.min.js') !!} --}}
    {{-- {!! Html::script('assets/plugins/datatables/plugins/integration/bootstrap/3/dataTables.bootstrap.js') !!} --}}
    {{-- {!! Html::script('//cdn.datatables.net/plug-ins/1.10.10/sorting/datetime-moment.js') !!} --}}

    {!! Html::script('assets/plugins/tinymce/tinymce.min.js') !!}

    {!! Html::script('assets/plugins/raty/jquery.raty.js') !!}
    {!! Html::script('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') !!}
    {!! Html::script('assets/plugins/jstree/jstree.min.js') !!}

    {!! Html::script("assets/js/manage.js?$version") !!}

    {!! Html::script('assets/js/jquery.magnific-popup.min.js') !!}

    @yield('extra_footer')

    @stack('scripts')


    @if( Session::get('tab') )
        <script type="text/javascript">
            $('a[href="{{Session::get('tab')}}"]').tab('show');
        </script>
    @endif

    @if( Session::get('tab2') )
        <script type="text/javascript">
            $('a[href="{{Session::get('tab2')}}"]').tab('show');
        </script>
    @endif

    <script>
        jQuery(document).ready(function() {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            QuickSidebar.init(); // init quick sidebar
            Demo.init(); // init demo features

            //Hide the overview when click
            $('#someid').on('click', function () {
                $('#OverviewcollapseButton').removeClass("collapse").addClass("expand");
                $('#PaymentOverview').hide();
            });

            $('.nav-item.active').closest('.sub-menu').css({'display': 'block'});
            $('.nav-item.active').find('.arrow').addClass('open');
            $('.nav-item.active').closest('.sub-menu').closest('.nav-item').addClass('open active');

            if($('.page-sidebar-menu').children().hasClass('active') == false) {
                var url = window.location.href.split('/ficha');
                urlmenu = url[0];
                $('a[href=\"'+urlmenu+'\"]').parents('.nav-item').addClass('active');
                $('a[href=\"'+urlmenu+'\"]').parents('.sub-menu').css({'display': 'block'});
            }
        });
    </script>


</body>
</html>