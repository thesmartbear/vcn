<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <link rel="shortcut icon" href="/favicon.ico"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @section('title')
            {{ConfigHelper::config('web')}}
        @show
    </title>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    {!! Html::style('assets/plugins/simple-line-icons/simple-line-icons.min.css') !!}
    {!! Html::style('assets/css/bootstrap.css') !!}
    {!! Html::style('assets/plugins/uniform/css/uniform.default.css') !!}
    {!! Html::style('assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css') !!}
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>


    <!-- PLUGINS STYLES -->
    {!! Html::style('assets/plugins/dropzone/css/basic.css') !!}
    {{--Html::style('assets/plugins/dropzone/css/dropzone.css')--}}
    {!! Html::script('assets/plugins/dropzone/dropzone.min.js') !!}

    {!! Html::style('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') !!}

    {!! Html::style('assets/plugins/multiselect/css/bootstrap-multiselect.css') !!}

    {!! Html::style('assets/plugins/raty/jquery.raty.css') !!}
    {!! Html::style('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') !!}

    {!! Html::style('assets/plugins/select2/css/select2.min.css') !!}

    {!! Html::style('assets/plugins/tablesorter/themes/vcn/style.css') !!}
    <!-- END PLUGINS STYLES -->



    <!-- BEGIN THEME STYLES -->
    {!! Html::style('assets/css/components.css') !!}
    {!! Html::style('assets/css/components_old.css') !!}
    {!! Html::style('assets/css/plugins.css') !!}
    {!! Html::style('assets/layout/css/layout.css') !!}
    {!! Html::style('assets/layout/css/themes/darkblue.css') !!}
    {!! Html::style('assets/layout/css/custom.css') !!}

    {!! Html::style('assets/css/manage.css') !!}
    <!-- END THEME STYLES -->

    {!! Html::style('assets/plugins/chosen/chosen.min.css') !!}

    {!! Html::script('assets/js/jquery-1.11.3.min.js') !!}

    <!-- DataTables -->
    {!! Html::style('assets/plugins/DataTables/datatables.min.css') !!}
    {!! Html::script('assets/plugins/DataTables/datatables.min.js') !!}
    {{-- {!! Html::style('assets/plugins/datatables/plugins/integration/bootstrap/3/dataTables.bootstrap.css') !!} --}}
    {{-- {!! Html::script('assets/plugins/datatables/js/jquery.dataTables.min.js') !!} --}}

    {{--
    {!! Html::script('assets/plugins/flot/jquery.flot.min.js') !!}
    {!! Html::script('assets/plugins/flot/jquery.flot.categories.min.js') !!}
    {!! Html::script('assets/plugins/flot/jquery.flot.stack.min.js') !!}
    --}}

    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') !!}
    {!! Html::style('assets/plugins/morris/morris.css') !!}
    {!! Html::script('assets/plugins/morris/morris.min.js') !!}

    {!! Html::script('//mozilla.github.io/pdf.js/build/pdf.js') !!}

    {!! Html::script('assets/plugins/jquery-validation/jquery.validate.min.js') !!}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    {!! Html::style('assets/plugins/jstree/themes/metronic/style.min.css') !!}

    @yield('extra_head')

</head>
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-footer-fixed page-container-bg-solid page-sidebar-fixed">

    @section('navbar')
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="/manage">
                        @if (ConfigHelper::config('logo'))
                            {!! Html::image('/assets/logos/'.ConfigHelper::config('logo'), ConfigHelper::config('nombre'), array('class' => 'logo-default')) !!}
                        @else
                            {{ ConfigHelper::config('nombre') }}
                        @endif
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN HEADER SEARCH BOX -->
                <form class="search-form search-form-expanded" action="{{route('manage.viajeros.buscar',['nombre'])}}" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Nombre,Apellido1,Apellido2..." name="query" autocomplete="off">
                        <span class="input-group-btn">
                        <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                        </span>
                    </div>
                </form>

                <form class="search-form search-form-expanded" action="{{route('manage.viajeros.buscar',['email'])}}" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="E-mail / Teléfono" name="query" autocomplete="off">
                        <span class="input-group-btn">
                        <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                        </span>
                    </div>
                </form>
                <!-- END HEADER SEARCH BOX -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        
                        <!-- Notifications Dropdown -->
                        <li id="appPusher" class="dropdown dropdown-extended dropdown-notification">
                            <notifications-dropdown></notifications-dropdown>
                        </li>
                        <!-- END Notifications Dropdown -->
                        
                        @if(ConfigHelper::canEdit('viajeros-list'))
                        <li>
                            <a href="{{route('manage.viajeros.nuevo')}}" class="">
                                <i class="ifa fa fa-plus-circle fa-fw"></i> Nuevo Lead
                            </a>
                        </li>
                        @endif

                        @if(ConfigHelper::canEdit('solicitudes-list'))
                        <li>
                            <a href="{{route('manage.solicitudes.index')}}" class="">
                                <i class="ifa fa fa-pencil-square fa-fw"></i> Solicitudes
                            </a>
                        </li>
                        @endif

                        @if(ConfigHelper::canEdit('bookings-list'))
                        <li>
                            <a href="{{route('manage.bookings.index')}}" class="">
                                <i class="ifa fa fa-pencil-square fa-fw"></i> Inscripciones
                            </a>
                        </li>
                        @endif

                        <li class="dropdown dropdown-user pull-right">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle img-avatar" src="{{ ConfigHelper::avatar() }}">
                                <span class="username username-hide-on-mobile"> [{{ConfigHelper::username()}}] </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <img class="foto-avatar" src="{{ ConfigHelper::avatar() }}">
                                </li>
                                <li>
                                    <a href="{{ route('manage.index.misdatos') }}" class="">
                                        <i class="ifa fa fa-user fa-fw"></i> Mis Datos
                                    </a>
                                </li>
                                
                                @if(Session::get('vcn.admin'))
                                
                                    @php
                                    $sign = "<i class='fa fa-sign-out'></i> Admin";
                                    $logout = "<a href='". route('manage.system.admins.logout')  ."'> $sign </a>";
                                    @endphp

                                    <li>{!! $logout !!}</li>

                                @endif

                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="ifa fa fa-sign-out fa-fw"></i> {{ __('Salir') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
    @show

    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="margin-top:25px;">
                    <li class="start ">
                        <a href="/manage">
                            <i class="fa fa-dashboard"></i>
                            <span class="title">Dashboard</span>
                        </a>
                    </li>
                    <li>
                    @section('navbar-manager')
                    @foreach($menuManager as $m1)
                        @if( $m1['route']=="-" )
                                <li class="divider"></li>
                        @else
                            <li class="nav-item">
                                @if ( count($m1['submenu'])>0)
                                    <a href="{{$m1['route']}}" class="nav-link nav-toggle">
                                        <i class="fa {{$m1['icono']}}" ></i>
                                        <span class="title">{{ $m1['nom'] }}</span>
                                        <span class="arrow "></span>
                                    </a>
                                    <ul class="sub-menu">
                                        @foreach ( $m1['submenu'] as $m2 )
                                            @if( isset($m2['submenu']) )

                                                <li class="nav-item">
                                                    <a href="{{$m2['route']}}" class="nav-link nav-toggle">
                                                        <i class="fa {{$m2['icono']}}" ></i>
                                                        <span class="title">{{ $m2['nom'] }}</span>
                                                        <span class="arrow "></span>
                                                    </a>
                                                    <ul class="sub-menu">
                                                        @foreach ( $m2['submenu'] as $m3 )
                                                            @if( $m3['route']=="-" )
                                                                <li class="divider"></li>
                                                            @else
                                                                <li class="nav-item">
                                                                    <a href="{{$m3['route']}}" class="nav-link">
                                                                        <i class="fa {{$m3['icono']}}"></i>
                                                                        <span class="title">{{$m3['nom']}}</span>
                                                                    </a>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                </li>

                                            @elseif( $m2['route']=="-" )
                                                <li class="divider"></li>
                                            @else
                                                <li class="nav-item">
                                                    <a href="{{$m2['route']}}" class="nav-link">
                                                        <i class="fa {{$m2['icono']}}"></i>
                                                        <span class="title">{{$m2['nom']}}</span>
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                @else
                                    <a href="{{$m1['route']}}" class="nav-link">
                                        <i class="fa {{$m1['icono']}}"></i>
                                        <span class="title">{{$m1['nom']}}</span>
                                    </a>
                                @endif
                            </li>
                        @endif
                    @endforeach
                    @show
                    </li>
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Modal title</h4>
                            </div>
                            <div class="modal-body">
                                Widget settings form goes here
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn blue">Save changes</button>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                <!-- BEGIN PAGE HEADER-->
                <div class="page-bar">
                    @yield('breadcrumb')
                </div>
               @section('titulo-seccion')
                    <h3 class="page-title">
                        @yield('titulo')
                    </h3>
               @show
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->

                @if( isset($errors) && count($errors)>0 )
                    <div class="note note-danger">
                        <h4 class="block">Error al guardar</h4>
                        <p>
                            Revise los campos obligatorios.
                        </p>
                    </div>
                @endif

                @if( Session::get('mensaje') )

                    <div class="note note-warning">
                        <h4 class="block">Atención</h4>
                        <p>
                            {!! Session::get('mensaje') !!}
                        </p>
                    </div>

                @endif

                @if( Session::get('mensaje-alert') )
                    <div class="note note-danger">
                        <h4 class="block">Alerta</h4>
                        <p>
                            {!! Session::get('mensaje-alert') !!}
                        </p>
                    </div>
                @endif

                @if( Session::get('mensaje-ok') )

                    <div class="note note-success">
                        <h4 class="block">OK</h4>
                        <p>
                            {!! Session::get('mensaje-ok') !!}
                        </p>
                    </div>

                @endif

                @section('container')
                     @yield('content')
                @show

                @if(isset($ficha))
                    <i>{{class_basename(get_class($ficha))}} :
                    creado: {{ $ficha->created_at ? $ficha->created_at->format('d/m/Y H:i') : "-" }} ({{ConfigHelper::getCreatedBy($ficha)}}) y actualizado: {{ $ficha->updated_at ? $ficha->updated_at->format('d/m/Y H:i') : ""}} ({{ConfigHelper::getUpdatedBy($ficha)}}) </i>
                @endif
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->

        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->


    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner" style="width: 100%;">
            {{ date('Y') }} &copy; {{ConfigHelper::config('nombre')}}
            - {{ConfigHelper::version()}} - {{ConfigHelper::infoLogin()}}]

            @if(auth()->user()->es_full_admin)
            <span class="pull-right">
                {{-- {{ConfigHelper::infoActivity()}} --}}
            </span>
            @endif

        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- END FOOTER -->


    <div class="modal fade" id="modalDestroy">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modalDestroy-Label"></h4>
            </div>
            <div class="modal-body" id="modalDestroy-Body">
                Esta seguro que desea eliminar el objeto: <strong><span id='modalDestroy-Model'></span></strong>
            </div>
            <div class="modal-footer" id="modalDestroy-Footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a href='#' id='modalDestroy-Action' type="button" class="btn btn-primary">Borrar</a>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="modalInfo">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modalInfo-Label">Info</h4>
            </div>
            <div class="modal-body" id="modalInfo-Body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="modalPrompt">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                {!! Form::open( array('id'=> 'modalPrompt-Form', 'method'=> 'get', 'url' => 'x')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modalPrompt-Label"></h4>
                </div>
                <div class="modal-body" id="modalPrompt-Body">
                    <span id='modalPrompt-Model'></span>: <input type='text' id="modalPrompt-Input" name='prompt'>
                </div>
                <div class="modal-footer" id="modalPrompt-Footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Aceptar</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        </div>


    <!-- Scripts -->
    {{-- {!! Html::script('assets/js/jquery-1.11.3.min.js') !!} --}}

    {!! Html::script('assets/plugins/jquery-migrate.min.js') !!}
    {!! Html::script('assets/plugins/jquery-ui/jquery-ui.min.js') !!}
    {!! Html::script('assets/js/bootstrap.min.js') !!}
    {!! Html::script('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') !!}
    {!! Html::script('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') !!}
    {!! Html::script('assets/plugins/jquery.blockui.min.js') !!}
    {!! Html::script('assets/plugins/jquery.cokie.min.js') !!}
    {{-- Html::script('assets/plugins/uniform/jquery.uniform.min.js') --}}
    {!! Html::script('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') !!}
    <!-- END CORE PLUGINS -->
    {!! Html::script('assets/layout/scripts/metronic.js') !!}
    {!! Html::script('assets/layout/scripts/layout.js') !!}
    {!! Html::script('assets/layout/scripts/quick-sidebar.js') !!}
    {!! Html::script('assets/layout/scripts/demo.js') !!}

    {!! Html::script('assets/plugins/moment-with-locales.min.js') !!}
    {!! Html::script('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('assets/plugins/multiselect/js/bootstrap-multiselect.js') !!}
    {!! Html::script('assets/plugins/chosen/chosen.jquery.min.js') !!}
    {!! Html::script('assets/plugins/bootbox.min.js') !!}
    {!! Html::script('assets/plugins/select2/js/select2.full.min.js') !!}

    {!! Html::script('assets/plugins/tablesorter/jquery.tablesorter.min.js') !!}

    <!-- DataTables -->
    {{-- {!! Html::script('assets/plugins/datatables/js/jquery.dataTables.min.js') !!} --}}
    {{-- {!! Html::script('assets/plugins/datatables/plugins/integration/bootstrap/3/dataTables.bootstrap.js') !!} --}}
    {{-- {!! Html::script('//cdn.datatables.net/plug-ins/1.10.10/sorting/datetime-moment.js') !!} --}}

    {!! Html::script('assets/plugins/tinymce/tinymce.min.js') !!}

    {!! Html::script('assets/plugins/raty/jquery.raty.js') !!}
    {!! Html::script('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') !!}
    {!! Html::script('assets/plugins/jstree/jstree.min.js') !!}
    {!! Html::script('assets/plugins/iris.min.js') !!}

    <script src="https://cdn.jsdelivr.net/npm/vue@^2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.0/vue-resource.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.0/axios.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/@riophae/vue-treeselect@0.0.37/dist/vue-treeselect.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@riophae/vue-treeselect@0.0.37/dist/vue-treeselect.min.css">

    <link rel="stylesheet" href="/assets/css/chat.css">

    <!-- Scripts -->
    <script>
        window.AppPusher = true
        window.Laravel = {!! json_encode([
            'user' => Auth::user(),
            'csrfToken' => csrf_token(),
            'vapidPublicKey' => config('webpush.vapid.public_key'),
            'pusher' => [
                'key' => config('broadcasting.connections.pusher.key'),
                'cluster' => config('broadcasting.connections.pusher.options.cluster'),
            ],
        ]) !!};
    </script>

    <!-- Mix Scripts -->
    <script src="{{mix('/js/app.js')}}"></script>
    
    {!! Html::script('assets/js/manage.js') !!}

    @yield('extra_footer')
    @stack('scripts')

    @if( Session::get('tab') )
        <script type="text/javascript">
            $('a[href="{{Session::get('tab')}}"]').tab('show');
        </script>
    @endif

    <script>
        jQuery(document).ready(function() {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            QuickSidebar.init(); // init quick sidebar
            Demo.init(); // init demo features

            //Hide the overview when click
            $('#someid').on('click', function () {
                $('#OverviewcollapseButton').removeClass("collapse").addClass("expand");
                $('#PaymentOverview').hide();
            });


            $('.nav-item.active').closest('.sub-menu').css({'display': 'block'});
            $('.nav-item.active').find('.arrow').addClass('open');
            $('.nav-item.active').closest('.sub-menu').closest('.nav-item').addClass('open active');

            if($('.page-sidebar-menu').children().hasClass('active') == false) {
                var url = null;
                if(window.location.href.includes('/ficha')) {
                    var url = window.location.href.split('/ficha');
                }else if(window.location.href.includes('?')){
                    var url = window.location.href.split('?');
                }

                if(url)
                {
                    urlmenu = url[0];
                    $('a[href=\"' + urlmenu + '\"]').parents('.nav-item').addClass('active');
                    $('a[href=\"' + urlmenu + '\"]').parents('.sub-menu').css({'display': 'block'});

                }
            }

        });
    </script>

    @if(ConfigHelper::config('es_notificaciones'))
        @include('includes.script_aviso_tareas')
    @endif

</body>
</html>