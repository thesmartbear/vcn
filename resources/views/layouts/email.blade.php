<!DOCTYPE html>
<html lang="es-ES">
    <head>
        <meta charset="utf-8">

        <!--[if mso]>
        <style type=”text/css”>
        .fallback-font {
        font-family: Arial, sans-serif;
        }
        </style>
        <![endif]-->

        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <style>
        @media screen { 
            
            @font-face {
              font-family: 'Open Sans';
              font-style: normal;
              font-weight: 400;
              src: local('Open Sans Regular'), local('OpenSans-Regular'), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFVZ0bf8pkAg.woff2) format('woff2');
              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }
        }
        </style>



    </head>
    <body>

        <div class="fallback-font" style="font-family: 'Open Sans', sans-serif;">
        @section('contenido')

        @show
        </div>

    </body>
</html>
