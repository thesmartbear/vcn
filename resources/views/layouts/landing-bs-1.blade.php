<!DOCTYPE html>
<html>
    <head>

        @php
            $title = Traductor::trans('Landing', 'title', $landing);
        @endphp

        {!! $landing->gtm_head ?? "" !!}

        <title>{!! $title !!}</title>
        
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <link href="img/favicon.png" rel="shortcut icon"/>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{$urlAssets}}/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Custom CSS -->
        <link href="{{$urlAssets}}/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="{{$urlAssets}}/../css/style.css" rel="stylesheet" type="text/css"/>
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:700&display=swap" rel="stylesheet">

        <!-- Vue.js -->
        <script src="https://cdn.jsdelivr.net/vue/2.2.6/vue.min.js"></script>

        @php

            $checks = Traductor::trans('Landing', 'subtitle2', $landing);
            $checks = explode("\r\n", $checks);
            $h = count($checks) * 20;
            
            $ht = strlen($title);
            $h += $ht>24 ? 70 : 0;

            if($landing->es_form && $landing->form_pre)
            {
                $h += strlen($landing->form_pre) / 2;
            }

            $mdonde = $landing->es_form ? 0 : -290;
            
        @endphp

        <style>
            .landing-fondo {
                @if($landing->imagen)
                    background-image: url("{{$landing->imagen}}");
                @else
                    background-image: url("/assets/landings/bs-general-1/img/v4---general-marvin-meyer-syto3xs06fu-unsplash(4).png");
                @endif
                background-position: center;
                background-repeat: no-repeat;
                height: 871px;
                background-size: auto 100%;
                padding-left: 9.5%;
            }

            .form-repe {
                margin-top: 450px;
            }

            .donde {
                margin-top: {{$mdonde}}px;
            }

            @media (min-width: 576px) {
                .form-first {
                    margin-top: {{$h}}px;
                }
            } 

            @media (max-width: 575.98px) {
                .form-first {
                    margin-top: {{$h+160}}px;
                }
            }
        </style>

    </head>
    <body>

        <!-- RAYOS -->
        <div>
            <img class="rallo-1" src="{{$urlAssets}}/img/v4---general-llampbuit-03-2.png" alt="">
        </div>

        <div>
            <img class="rallo-2" src="{{$urlAssets}}/img/v4---general-llampbuit-03-2.png" alt="">
        </div>

        <div>
            <img class="rallo-3" src="{{$urlAssets}}/img/v4---general-llampbuit-03-2.png" alt="">
        </div>

        <!-- TITULO -->
        <!-- EDITABLE: Titulo y segundo subtitulo -->
        <section class="title">
            <div class="container-fluid padding-container landing-fondo">
                <div class="row d-flex title-box">
                    <div class="col-12">
                        <h2 class="text-white titulo-normal">{!! $title !!}</h2>
                        <h2 class="text-white titulo-movil">{!! $title !!}</h2>
                        <div class="ralla-blanca"></div>
                        <h3 class="text-white">{!! Traductor::trans('Landing', 'subtitle', $landing) !!}</h3>

                        @foreach( $checks as $check)
                        <div class="row d-flex">
                            <div class="col-2 col-sm-1 d-flex align-items-center">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="col-10 col-sm-11">
                                <h4 class="text-white">{!! $check !!}</h4>
                            </div>
                        </div>
                        @endforeach

                        @if($landing->es_form && $landing->form_pre)
                            <div class="textos-form">{!! Traductor::trans('Landing', 'form_pre', $landing) !!}</div>
                        @endif
    
                    </div>
                </div>
            </div>
        </section>

        <!-- BOTON FIJO ARRIBA CUANDO SCROLL -->
        <div class="fixed-button">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-4 ml-5">
                    <img src="/assets/landings/logos/{{$sufijo}}logoweb_black.png" alt="">
                </div>
                <div class="col-md-3 ml-5">
                    <a href="#frmLanding">@lang("Solicitar")</a>
                </div>
            </div>
        </div>

        <!-- LOGO -->
        <img class="logo-british" src="/assets/landings/logos/{{$sufijo}}logoweb_white.png">
        
        <!-- FORMULARIO -->
        @section('bloque-formulario')

        @if( $landing->es_form )
        <section class="form form-first">
            <div class="container-fluid padding-container">
                <div class="row d-flex justify-content-center">
                    <div class="col-11 col-lg-10 col-xl-7 block shadow">

                        <div class="row form-title">
                            <div class="col-12 col-md-12">
                                <h2>{!! Traductor::trans('Landing', 'form_title', $landing) !!}</h2>
                                <div class="ralla-verde ralla-verde-1"></div>
                                <div class="ralla-verde ralla-verde-2"></div>
                            </div>
                            {{-- <div class="col-12 col-md-6">
                                @if($landing->form_subtitle)
                                    <p>{!! Traductor::trans('Landing', 'form_subtitle', $landing) !!}</p>
                                @endif
                            </div> --}}
                        </div>
    
                        @if($gracias)
    
                            <div class="row mt-5">
                                <div class="col-12 col-md-12 mt-5">
                                    <p>{!! Traductor::trans('Landing', 'gracias', $landing) !!}</p>
                                </div>
                            </div>
                        
                        @else
                            {{-- @yield('bloque-formulario-form') --}}

                            @section('bloque-formulario-form1')    
                            {!! Form::open(array('id'=> 'frmLanding', 'method'=> 'post', 'url' => route('web.landing.post', $landing->id))) !!}
                                <div class="row d-flex">

                                    {!! Form::hidden('utm_source', request()->input('utm_source', '')) !!}
                                    {!! Form::hidden('utm_medium', request()->input('utm_medium', '')) !!}
                                    {!! Form::hidden('utm_campaign', request()->input('utm_campaign', '')) !!}

                                    {{-- FORM IZQUIERDA (COMÚN) --}}
                                    <div class="col-12 col-md-6 padding">
                                            <div class="form-group">
                                                <label for="Nombre">@lang("landings.Nombre")</label>
                                                <input type="text" class="form-control" name="nombre" required aria-describedby="emailHelp" placeholder="Judit">
                                            </div>
                                            <div class="form-group">
                                                <label for="telefono">@lang("landings.Teléfono")</label>
                                                <input type="text" class="form-control" name="telefono" required placeholder="671 09 21 35">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">@lang("landings.E-mail")</label>
                                                <input type="email" class="form-control" name="email" required aria-describedby="emailHelp" placeholder="loremipsum@gmail.com">
                                            </div>
                                            <div class="form-group">
                                                <label for="select">@lang("landings.viajero")</label>
                                                <select class="form-control form-control-lg" name="es_viajero" required v-model="es_viajero">
                                                    <option value="">-</option>
                                                    <option value="1">@lang("landings.Sí")</option>
                                                    <option value="0">@lang("landings.No")</option>
                                                </select>
                                            </div>
                                    </div>

                                    {{-- FORM DERECHA (xTIPO) --}}
                                    @yield('bloque-formulario-form-right')

                                </div>
                            {!! Form::close() !!}
                            @show

                        @endif

                    </div>
                </div>
            </div>
        </section>
        @endif

        @show

        @if( $landing->es_form && $landing->form_after)
        <div class="container-fluid padding-container">
            <div class="row d-flex justify-content-center">
                <div class="col-11 col-sm-10 col-xl-7">
                    <div class="textos-pre-after">{!! Traductor::trans('Landing', 'form_after', $landing) !!}</div>
                </div>
            </div>
        </div>
        @endif
        
        <!-- DONDE QUIERES IR -->
        @section('bloque-destino')
        <section class="donde">
            <div class="container-fluid padding-container">
                <div class="row d-flex justify-content-center">
                    <div class="col-11 col-sm-10 col-xl-7 bloque">
    
                        <div class="row m-0">
                            <div class="col-12 p-0">
                                <h2 class="titulo">{!! Traductor::trans('Landing', 'seccion_title', $landing) !!}</h2>
                                <div class="ralla-rosa"></div>
                            </div>
                        </div>
    
                        <div class="row m-0">
                            <div class=" col-12 col-md-8 p-0">
                                <p class="subtitulo">{!! Traductor::trans('Landing', 'seccion_subtitle', $landing) !!}</p>
                            </div>
                        </div>
                        
                        @yield('bloque-destino-lista')

                        @if($landing->lista_after)
                            <span class="textos-pre-after">{!! Traductor::trans('Landing', 'lista_after', $landing) !!}</span>
                        @endif
                        
                    </div>
                </div>
            </div>
        </section>
        @show

        <!-- MAS INFORMACION -->
        @if($landing->bloque_catalogo)
        <section class="mas-info">
            <div class="container-fluid">
                <div class="row d-flex justify-content-center">
                    <div class="col-12 col-md-10 col-xl-6 block-1 shadow d-flex align-items-center justify-content-center margin-right">
                        <h3>
                            {!! Traductor::trans('Landing', 'catalogo_texto', $landing, true) !!}
                            <br><br>
                            <a class="btn btn-primary mb-2" href="{{route('web.landing.catalogo', $landing->slug)}}">{{trans('web.catalogo')}}</a>
                        </h3>
                    </div>

                    {{-- {{$landing->catalogo}} --}}

                    
                    {{-- <div class="col-10 col-md-5 col-xl-3 block-2 shadow margin-left">
                        <form>
                            <div class="form-group">
                                <label for="nombre-mas">Nombre</label>
                                <input type="text" class="form-control" id="nombre-mas" placeholder="Judit">
                            </div>
                            <div class="form-group">
                                <label for="email-mas">E-mail</label>
                                <input type="email" class="form-control" id="email-mas" placeholder="loremipsum@gmail.com">
                            </div>
                            <div class="row d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary mb-2">Descargar catálogo</button>
                            </div>
                        </form>
                    </div> --}}
                </div>
            </div>
        </section>
        @endif

        @if($landing->nosotros_pre)
        <div class="container-fluid padding-container">
            <div class="row d-flex justify-content-center">
                <div class="col-11 col-sm-10 col-xl-7">
                    <div class="textos-pre-after">{!! Traductor::trans('Landing', 'nosotros_pre', $landing) !!}</div>
                </div>
            </div>
        </div>
        @endif

        <!-- SOBRE NOSOTROS -->
        <section class="sobre">
            <div class="container-fluid padding-container">

                <div class="row d-flex justify-content-center">
                    <div class="col-11 col-sm-10 col-xl-7">
                        <h2 class="titulo">{!! Traductor::trans('Landing', 'nosotros_title', $landing) !!}</h2>
                        <div class="ralla-rosa"></div>
                    </div>
                </div>

                <div class="row d-flex justify-content-center">
                    <div class="col-10 col-lg-8 col-xl-6 shadow block text-center">
                        @php
                        $txtLines = Traductor::trans('Landing', 'nosotros_subtitle', $landing);
                        @endphp
                        @foreach(explode("\n", $txtLines) as $line)
                            <p>{{$line}}</p>
                        @endforeach
                    </div>
                
                </div>
            </div>
        </section>

        @if($landing->porque_pre)
        <div class="container-fluid padding-container">
            <div class="row d-flex justify-content-center">
                <div class="col-11 col-sm-10 col-xl-7">
                    <div class="textos-pre-after">{!! Traductor::trans('Landing', 'porque_pre', $landing) !!}</div>
                </div>
            </div>
        </div>
        @endif

        <!-- POR QUE -->
        @if($landing->bloque_porque)
        <section class="porque padding-container">
            <div class="container-fluid">
                <div class="row d-flex justify-content-center">
                    <div class="col-11 col-sm-10 col-xl-7">

                        <div class="row">
                            <div class="col-12">
                                <h2 class="titulo">@lang("landings.porque", ['plataforma'=> ConfigHelper::plataformaApp()])</h2>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-12 ralla">
                                <div class="ralla-rosa"></div>
                            </div>
                        </div>

                    </div>

                    <div class="col-11 col-sm-10 col-md-8 col-lg-6 col-xl-7 numeros">

                        <div class="row">
                                
                            @php
                                $porques = Traductor::trans('Landing', 'porque', $landing);
                                $porques = explode("\r\n", $porques);
                                $icol = round(count($porques)/2);
                            @endphp

                            @foreach( $porques as $porque)

                                @php
                                    $div = ($loop->iteration % $icol) == 1;
                                @endphp
                                @if($div)
                                <div class="col-xl-6">
                                @endif

                                <div class="row margin-bottom">
                                    <div class="col-4">
                                        <div class="row d-flex align-items-center">
                                            <div class="col-12 d-flex justify-content-center">
                                                <h2>{{sprintf("%02d", $loop->iteration)}}</h2> 
                                            </div>
                                            <div class="col-12 d-flex justify-content-center">
                                                <div class="ralla-negra"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-8 d-flex align-items-center">
                                        <h6>{{$porque}}</h6>
                                    </div>
                                </div>

                                @php
                                    $div = ($loop->iteration % $icol) == 0;
                                @endphp 
                                @if($div)
                                </div>
                                @endif

                            @endforeach

                        </div>
                    </div>

                </div>
            </div>
        </section>
        @endif

        @if($landing->testimonios_pre)
        <div class="container-fluid padding-container">
            <div class="row d-flex justify-content-center">
                <div class="col-11 col-sm-10 col-xl-7">
                    <div class="textos-pre-after">{!! Traductor::trans('Landing', 'testimonios_pre', $landing) !!}</div>
                </div>
            </div>
        </div>
        @endif

        <!-- TESTIMONIOS -->
        @if($landing->testimonios_list->count() && $landing->bloque_testimonios)
            @section('bloque-testimonios')
            <section class="testimonios">
                <div class="container-fluid padding-container">
                    <div class="row d-flex justify-content-center m-0">
                        <div class="col-12 col-sm-10 col-xl-7">

                            <div class="row">
                                <div class="col-12">
                                    <h2 class="titulo">{!! Traductor::trans('Landing', 'testimonios_title', $landing) !!}</h2>
                                    <div class="ralla-rosa"></div>
                                </div>
                                
                                <div class="col-xl-6">
                                    <h5 class="subtitulo">{!! Traductor::trans('Landing', 'testimonios_subtitle', $landing) !!}</h5> 
                                </div>
                            </div>

                            @foreach($landing->testimonios_list->where('es_principal',1) as $t)
                            <div class="row shadow principal m-0 margin-bottom">
                                <div class="col-md-4 p-0 d-flex">
                                    <img src="{{$t->foto}}" alt="">
                                </div>
                                <div class="col-md-4 padding-1 pb-0">
                                    <p>{!! Traductor::trans('LandingTestimonio', 'texto', $t) !!}</p>
                                    <p class="text-right firma">- {!! Traductor::trans('LandingTestimonio', 'firma', $t) !!}</p>
                                </div>
                                <div class="col-md-4 padding-1">
                                    <p>{!! Traductor::trans('LandingTestimonio', 'texto2', $t) !!}</p>
                                    <p class="text-right firma">- {!! Traductor::trans('LandingTestimonio', 'firma2', $t) !!}</p>
                                </div>
                            </div>
                            @endforeach
        
                            <div class="row secundarios margin-bottom">
                            @foreach( $landing->testimonios_list->where('es_principal',0) as $t )
                                @php
                                    $tpadding = ($loop->iteration % 2) ? "padding-right" : "padding-left";
                                @endphp
        
                                <div class="col-md-6 {{$tpadding}}">
                                    <div class="row shadow m-0 mt-3">
                                        <div class="col-4 d-flex align-items-center justify-content-center pr-0">
                                            <img src="{{$t->foto}}" alt="">
                                        </div>
                                        <div class="col-8 padding-2">
                                            <p class="text-right">{!! Traductor::trans('LandingTestimonio', 'texto', $t) !!}</p>
                                            <p class="text-right firma">- {!! Traductor::trans('LandingTestimonio', 'firma', $t) !!}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
        
                        </div>
                    </div>
                </div>
            </section>
            @show
        @endif

        {{-- VIDEO --}}
        @if($landing->video)
        {{-- https://www.youtube.com/embed/zpOULjyy-n8?rel=0 --}}
        <section class="video">
            <div class="container-fluid padding-container d-flex justify-content-center">
                <div class="embed-responsive embed-responsive-16by9 video-width">
                    <iframe class="embed-responsive-item" src="{!! Traductor::trans('Landing', 'video', $landing) !!}"></iframe>
                </div>
            </div>
        </section>
        @endif

        @if($landing->sellos_pre)
        <div class="container-fluid padding-container">
            <div class="row d-flex justify-content-center">
                <div class="col-11 col-sm-10 col-xl-7">
                    <div class="textos-pre-after">{!! Traductor::trans('Landing', 'sellos_pre', $landing) !!}</div>
                </div>
            </div>
        </div>
        @endif

        {{-- <!-- SELLOS DE CALIDAD --> --}}
        <section class="sellos">
            <div class="container-fluid padding-container">
                <div class="row d-flex justify-content-center m-0">
                    <div class="col-12 col-md-11 col-lg-10 col-xl-8">

                        <div class="row">
                            <div class="col-12">
                                <h2 class="text-center titulo-2">@lang("landings.sellos")</h2>
                            </div>
                        </div>

                        <div class="row d-flex justify-content-center">
                            <div class="col-12 col-md-3 d-flex align-items-center justify-content-center p-0">
                                <img class="img-1" src="{{$urlAssets}}/img/v4---general-captura-de-pantalla-2020-02-07-a-las-81447.png" alt="">
                            </div>
                            <div class="col-12 col-md-3 d-flex align-items-center justify-content-center p-0">
                                <img class="img-2" src="{{$urlAssets}}/img/v4---general-captura-de-pantalla-2020-02-07-a-las-81454@2x.png" alt="">
                            </div>
                            <div class="col-12 col-md-3 d-flex align-items-center justify-content-center p-0">
                                <img class="img-3" src="{{$urlAssets}}/img/v4---general-captura-de-pantalla-2020-02-07-a-las-81505.png" alt="">
                            </div>
                            <div class="col-12 col-md-3 d-flex align-items-center justify-content-center p-0">
                                <img class="img-4" src="{{$urlAssets}}/img/v4---general-captura-de-pantalla-2020-02-07-a-las-81512@2x.png" alt="">
                            </div>
                        </div>

                    </div>
                </div>
            
            </div>
        </section>

        <!-- OFICINAS -->
        @if($sufijo == "bs")
        <section class="oficinas">
            <div class="container-fluid padding-container">
                <div class="row d-flex justify-content-center m-0">
                    <div class="col-md-10 col-lg-9 col-xl-11">

                        <div class="row">
                            <div class="col-12">
                                <h2 class="text-center titulo-2">@lang("landings.oficinas")</h2>
                            </div>
                        </div>

                        <div class="row d-flex justify-content-center">
                            <div class="col-12 col-sm col-md-6 col-xl-3 padding margin-1">
                                <h5>BARCELONA</h5>
                                <p>Vía Augusta, 33,</p>
                                <p>08006, Barcelona</p>
                                <p>Tel. <span class="underline"><a href="tel:932008888">93 200 88 88</a></span></p>
                                <p><span class="underline"><a href="mailto:infobs@britishsummer.com">infobs@britishsummer.com</a></span></p>
                            </div>
                            <div class="col-12 col-md-6 col-xl-3 padding margin-1">
                                <h5>MADRID</h5>
                                <p>Paseo de la Castellana, 136, bajos,</p>
                                <p>28046, Madrid</p>
                                <p>Tel. <span class="underline"><a href="tel:913459565">91 345 95 65</a></span></p>
                                <p><span class="underline"><a href="mailto:madrid@britishsummer.com">madrid@britishsummer.com</a></span></p>
                            </div>
                            <div class="col-12 col-md-6 col-xl-3 padding margin-2">
                                <h5>GIRONA</h5>
                                <p>Carrer Migdia, 25,</p>
                                <p>17002, Girona</p>
                                <p>Tel. <span class="underline"><a href="tel:972414902">972 414 902</a></span></p>
                                <p><span class="underline"><a href="mailto:girona@britishsummer.com">girona@britishsummer.com</a></span></p>
                            </div>
                            <div class="col-12 col-md-6 col-xl-3 padding margin-2">
                                <h5>SEVILLA</h5>
                                <p>Pza. Cristo de Burgos, 21, Bajo A,</p>
                                <p>41003, Sevilla</p>
                                <p>Tel. <span class="underline"><a href="tel:954210785">95 421 07 85</a></span></p>
                                <p><span class="underline"><a href="mailto:sevilla@britishsummer.com">sevilla@britishsummer.com</a></span></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        @endif

        <!-- FORMULARIO REPE -->
        @section('bloque-formulario-repe')
        <section class="form form-repe">
            <div class="container-fluid padding-container">
                <div class="row d-flex justify-content-center">
                    <div class="col-11 col-lg-10 col-xl-7 block shadow">
                        
                        <div class="row form-title">
                            <div class="col-12 col-md-12">
                                <h2>{!! Traductor::trans('Landing', 'form_title', $landing) !!}</h2>
                                <div class="ralla-verde ralla-verde-1"></div>
                                <div class="ralla-verde ralla-verde-2"></div>
                            </div>
                            {{-- <div class="col-12 col-md-6">
                                @if( $landing->es_form  && $landing->form_subtitle)
                                    <p>{!! Traductor::trans('Landing', 'form_subtitle', $landing) !!}</p>
                                @endif
                            </div>     --}}
                        </div>

                        @if($gracias)
    
                            <div class="row mt-5">
                                <div class="col-12 col-md-12 mt-5">
                                    <p>{!! Traductor::trans('Landing', 'gracias', $landing) !!}</p>
                                </div>
                            </div>
                        
                        @else
                            {{-- @yield('bloque-formulario-form') --}}

                            @section('bloque-formulario-form1r')
                            {!! Form::open(array('id'=> 'frmLandingRepe', 'method'=> 'post', 'url' => route('web.landing.post', $landing->id))) !!}
                                <div class="row d-flex">
                                    {{-- FORM IZQUIERDA (COMÚN) --}}
                                    <div class="col-12 col-md-6 padding">
                                            <div class="form-group">
                                                <label for="Nombre">@lang("landings.Nombre")</label>
                                                <input type="text" class="form-control" name="nombre" required aria-describedby="emailHelp" placeholder="Judit">
                                            </div>
                                            <div class="form-group">
                                                <label for="telefono">@lang("landings.Teléfono")</label>
                                                <input type="text" class="form-control" name="telefono" required placeholder="671 09 21 35">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">@lang("landings.E-mail")</label>
                                                <input type="email" class="form-control" name="email" required aria-describedby="emailHelp" placeholder="loremipsum@gmail.com">
                                            </div>
                                            <div class="form-group">
                                                <label for="select">@lang("landings.viajero")</label>
                                                <select class="form-control form-control-lg" name="es_viajero" required v-model="es_viajero">
                                                    <option value="">-</option>
                                                    <option value="1">@lang("landings.Sí")</option>
                                                    <option value="0">@lang("landings.No")</option>
                                                </select>
                                            </div>
                                    </div>

                                    {{-- FORM DERECHA (xTIPO) --}}
                                    @yield('bloque-formulario-form-right')

                                </div>
                            {!! Form::close() !!}
                            @show

                        @endif

                    </div>
                </div>
            </div>
        </section>
        @show

        <!-- WHATSAPP -->
        <section class="whatsapp">
            <div class="bg-red d-flex align-items-center">
                <div class="container-fluid">
                    <div class="row d-flex justify-content-center align-items-center m-0">
                        <a href="https://api.whatsapp.com/send?phone={{$landing->phone}}">
                        <img class="mr-3" width="68" src="{{$urlAssets}}/img/v4---general-iconfinderphone8216688@2x.png" alt="">
                        <h4 class="underline ml-2 text-white m-0">{{$landing->phone}}</a></h4>
                        </a>
                    </div>
                </div>
            </div>
            
        </section>

        <!-- Scripts -->
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{$urlAssets}}/js/jquery-3.4.1.min.js"></script>
        <script src="{{$urlAssets}}/js/bootstrap.min.js "></script>
        
        <script>
            $(window).bind('scroll', function () {
                if ($(window).scrollTop() > 10) {
                    $('.fixed-button').css("display", "block");
                } 
                else {
                    $('.fixed-button').css("display", "none");
                }
            });

            // $(document).ready(function(){
            //     $('form input[type="submit"]').prop("disabled", true);
            //     $(".agree").click(function(){
            //         if($(this).prop("checked") == true){
            //             $('form input[type="submit"]').prop("disabled", false);
            //         }
            //         else if($(this).prop("checked") == false){
            //             $('form input[type="submit"]').prop("disabled", true);
            //         }
            //     });
            // });

            new Vue({
                el: '#frmLanding',
                data: {
                    es_viajero: null,
                },
                mounted: function() {
                },
            });

            new Vue({
                el: '#frmLandingRepe',
                data: {
                    es_viajero: null,
                },
                mounted: function() {
                },
            });

        </script>

        <!-- End of Scripts -->

        {!! $landing->gtm_body ?? "" !!}

        {{-- AC SCRIPT --}}
        <script type="text/javascript">
            (function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
            vgo('setAccount', '225344043');
            vgo('setTrackByDefault', true);
        
            vgo('process');
        </script>
        
    </body>
</html>
