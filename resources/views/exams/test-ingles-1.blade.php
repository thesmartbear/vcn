@extends('layouts.area')


@section('breadcrumb')
    {!! Breadcrumbs::render('area.exams') !!}
@stop

@section('content')

@php
$booking_id = $booking_id ?? null;
$viajero_id = $viajero_id ?? null;

$submit = $booking_id || $viajero_id;

$abcd = ['a','b','c','d'];
$opcionesP2 = ['make','catch','take','keep'];
$opcionesP3 = ['','until','spoke','been','behind','out','through','taking','panic','caught','talking','worried','banana','for','to','gone','on','at','without','taken','and'];
$opcionesP3select = [0=> 'Select one ...'] + $opcionesP3;

$respuestas = [];
$results = false;

$required = "required";

$r = $respuesta->respuestas ?? false;
if(!$submit)
{
    // $results = true;
}

if($r)
{
    $respuestas = $respuesta->respuestas;
    $results = true;
    $submit = false;
}

@endphp

@if($submit)
{!! Form::open(array('method' => 'POST', 'url' => route('area.exams.ask.post', $exam->id), 'role' => 'form', 'class' => '')) !!}
@endif

    {!! Form::hidden('tipo', $tipo ?? 'booking') !!}
    {!! Form::hidden('booking_id', $booking_id ?? 0) !!}
    {!! Form::hidden('viajero_id', $viajero_id ?? 0) !!}

    <h1>English Test for Academic Programmes</h1>
    <hr>
    
    @if(!$results)

        <h2>Test procedure</h2>
        <div class="note note-success">
            <strong>Instructions</strong>
            <br>
            {!! $exam->texto !!}
        </div>

    @else

        <div class="portlet box default">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    Result
                </div>
            </div>
            <div class="portlet-body">

                <div class="note note-success">

                    <strong>Well done, test completed. Here are your results.</strong>
                    <br>
                    Test score: {{$respuesta->aciertos}} / {{$exam->preguntas_total}}
                    <br>
                    IELTS equivalence: {{$respuesta->ielts}} - {{$respuesta->ielts_txt}}

                </div>
                {!! $exam->gracias !!}
            </div>
        </div>

    @endif

    <hr>
    <section class="exam bloque-1">

        <div class="portlet box default">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    Part 1: Grammar
                </div>
            </div>
            <div class="portlet-body">

                <div class="note note-info">
                    Questions 1 – 20: choose the correct answer
                </div>

                <div class="note note-warning">
                    <h4>EXAMPLE</h4>
                    <div class="example">
                        High Schools International work with schools **** Australia, England and the USA
                        <div class="row">
                            <div class="col-md-1">a) at</div>
                            <div class="col-md-1">b) from</div>
                            <div class="col-md-1">c) in <b>X</b></div>
                            <div class="col-md-1">d) by</div>
                        </div>
                    </div>
                </div>

                <ul class="exam">
                @foreach($exam->preguntas->where('bloque',1) as $p)
                    @if($p->numero == 0) @continue @endif
                    <li>
                        <span class="ask">
                            {{$p->numero}}. {{$p->pregunta}}
                        </span>
                        
                        <div class="row form-group">

                            @foreach($p->opciones as $iopt => $opt)

                                @php
                                    $res = isset($respuestas[$p->id]) ? (int)$respuestas[$p->id] : -1;
                                    $checked = ($res==$iopt) ? 'checked' : '';

                                    $disabled = $results ? 'disabled' : '';
                                    $classResult = '';
                                    if($results)
                                    {
                                        $classResult = ($iopt == $p->respuesta) ? 'radio-ok' : '';
                                        if($res == $iopt)
                                        {
                                            $classResult = ($res == $p->respuesta) ? 'radio-ok' : 'radio-ko';
                                        }
                                    }
                                    
                                @endphp

                                <div class="col-md-2">
                                    <label class="radio">
                                        <input {{$required}} class="radio-inline {{$classResult}}" type="radio" name="{{$p->id}}" value='{{$iopt}}' {{$checked}} {{$disabled}}> <span>{{$opt}}</span>
                                    </label>
                                </div>
                                
                            @endforeach                      
                            
                        </div>

                    </li>
                @endforeach
                </ul>

                

            </div>
        </div>
        
    </section>

    <section class="exam bloque-2">

        <div class="portlet box default">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    Part 2: Vocabulary
                </div>
            </div>
            <div class="portlet-body">

                <div class="note note-info">
                    Questions 21 – 35: match the words or phrases with the correct verb.
                    <br>
                    Choose A, B, C or D.
                    <br><br>
                    A = make<br>
                    B = catch<br>
                    C = take<br>
                    D = keep<br>
                </div>

                <div class="note note-warning">
                    <h4>EXAMPLE</h4>
                    <div class="example">
                        <table>
                            <tr>
                                <td width='120'>**** a cup of tea</td>
                                <td width="40">= <b>A</b></td>
                                <td>make a cup of tea</td>
                            </tr>
                            <tr>
                                <td>**** a bus</td>
                                <td>= <b>B</b></td>
                                <td>catch a bus</td>
                            </tr>
                                <td>**** a break</td>
                                <td>= <b>C</b></td>
                                <td>take a breath</td>
                            </tr>
                            <tr>
                                <td>**** a pet</td>
                                <td>= <b>D</b></td>
                                <td>keep a pet</td>
                            </tr>
                        </table>
                        
                    </div>
                </div>

                <ul class="exam">
                    @foreach($exam->preguntas->where('bloque',2) as $p)
                        @if($p->numero == 0) @continue @endif
                        <li>
                            <span class="ask">
                                {{$p->numero}}. {{$p->pregunta}}
                            </span>

                            <div class="row form-group">

                                @foreach($p->opciones as $iopt => $opt)
                                    
                                    @php
                                        $res = isset($respuestas[$p->id]) ? (int)$respuestas[$p->id] : -1;
                                        $checked = ($res==$iopt) ? 'checked' : '';

                                        $disabled = $results ? 'disabled' : '';
                                        $classResult = '';
                                        if($results)
                                        {
                                            $classResult = ($iopt == $p->respuesta) ? 'radio-ok' : '';
                                            if($res == $iopt)
                                            {
                                                $classResult = ($res == $p->respuesta) ? 'radio-ok' : 'radio-ko';
                                            }
                                        }
                                        
                                    @endphp

                                    <div class="col-md-2">
                                        <label class="radio">
                                            <input {{$required}} class="radio-inline {{$classResult}}" type="radio" name="{{$p->id}}" value='{{$iopt}}' {{$checked}} {{$disabled}}> <span>{{$opt}}</span>
                                        </label>
                                    </div>

                                @endforeach               
                                
                            </div>
                            
                        </li>
                    @endforeach
                </ul>

            </div>
        </div>

    </section>

    <section class="exam bloque-3" id="app_test1">

        <div class="portlet box default">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    Part 3: Reading
                </div>
            </div>
            <div class="portlet-body">

                <div class="note note-info">
                    <b>Text one.</b> Study this passage. There are 11 words missing. There are 20 words.
                    <br>
                    Read the passage and choose one word for each of the ten spaces. Select the number, in the box provided.
                    <br>
                    Number one has been done for you.
                </div>

                <div class="note note-warning">
                    <b>Words: </b>
                    @foreach($opcionesP3 as $op)
                        {{$op}}
                        @if(!$loop->last),@endif
                    @endforeach
                </div>

                @php
                    $op3 = [];
                    $ok3 = [];
                    foreach($exam->preguntas->where('bloque',3)->where('numero','<',12) as $p)
                    {
                        $op3['part3-'. $p->numero] = isset($respuestas[$p->id]) ? $respuestas[$p->id] : -1;
                        $ok3['part3-'. $p->numero] = $p->respuesta;
                    }
                    $select = "opciones3";
                @endphp

                
                <div class="box-text">
                Mr Smith and his wife had taken their 9-year-old grand daughter to Paris ...{{$opcionesP3[13]}}... a holiday.
                One morning, they set @include('exams._ingles1-select', ['name'=> 'part3-2', 'select'=> $select, 'index'=> 2 ]) for a ride on the metro. In the rush to get @include('exams._ingles1-select', ['name'=> 'part3-3', 'select'=> $select, 'index'=> 3 ]) the train,
                the child was left @include('exams._ingles1-select', ['name'=> 'part3-4', 'select'=> $select, 'index'=> 4 ]) on the platform.
                Mr and Mrs Smith had to wait @include('exams._ingles1-select', ['name'=> 'part3-5', 'select'=> $select, 'index'=> 5 ]) the train stopped at the next station before they could do anything.
                When the train stopped, they @include('exams._ingles1-select', ['name'=> 'part3-6', 'select'=> $select, 'index'=> 6 ]) the first train back to where their grand daughter had @include('exams._ingles1-select', ['name'=> 'part3-7', 'select'=> $select, 'index'=> 7 ]) left.
                When they got off the train again, they started to @include('exams._ingles1-select', ['name'=> 'part3-8', 'select'=> $select, 'index'=> 8 ]) The little girl was nowhere in sight.
                They ran around looking for her, but they needn't have @include('exams._ingles1-select', ['name'=> 'part3-9', 'select'=> $select, 'index'=> 9 ])
                The station manager had noticed what had happened and had @include('exams._ingles1-select', ['name'=> 'part3-10', 'select'=> $select, 'index'=> 10 ]) the little girl to his office,
                where she was happily drinking tea and @include('exams._ingles1-select', ['name'=> 'part3-11', 'select'=> $select, 'index'=> 11 ]) to the station workers.
                </div>

                <br><br>
                <div class="note note-info">
                    <b>Text two.</b> Read the text, choose the correct answer select the correct answer.
                </div>

                <table>
                    <tr>
                        <td class="box-td">
                            Zoos are very popular tourist attractions for people of all ages.
                            They are places where people can see mammals, birds, reptiles, amphibians and insects from all parts of the globe.
                            Many zoos have beautiful gardens and wide tree-lined paths that lead from one display to another.
                            <br>
                            Australia has four major zoos as well as a number of wild life parks and native animal sanctuaries.
                            One of the best known zoos in Australia is the Melbourne Zoo situated close to the metropolitan area in leafy Royal Park.
                            <br>
                            Established in 1857, on the banks of the Yarra River, Melbourne Zoo moved to its present site in 1862. It was the first zoo built in Australia and is one of the oldest zoos in the world.
                            <br>
                            The animal enclosures are set amidst beautifully landscaped gardens and some of the tamer animals, like ducks, geese, wallabies and emus are allowed to wander freely around the gardens.
                            <br>
                            There have been many changes to the zoo during the last hundred years: the old iron-bar
                        </td>
                        <td class="box-td">
                            cages have been replaced by habitat exhibits which display animals from all over the world in as natural an environment as possible.
                            <br>
                            Natural areas of tropical rain forest have been created to house the African gorillas and the endangered Sumatran tiger, while rocky desert areas have been developed for the ever alert meerkats.
                            <br>
                            However, modern zoos are not only about providing entertainment for tourists and visitors but also play a very important role in wildlife conservation and educating people about the need to protect endangered species and their environments. They do this through their carefully controlled breeding programmes.
                            <br>
                            Melbourne Zoo achieved worldwide fame in 1984 when it successfully bred baby gorilla Mzuri followed in 1991 by baby gorilla Buzandi.
                            <br>
                            There are over 350 kinds of animals in the zoo and because it’s open every day of the year, there’s always something fascinating to see and do at the Melbourne Zoo.
                        </td>
                    </tr>
                </table>

                <div class="note note-warning">
                    Choose the correct answer select the correct answer.
                </div>

                <ul class="exam">
                    @foreach($exam->preguntas->where('bloque',3)->where('numero','>',11) as $p)
                        @if($p->numero == 0) @continue @endif
                        <li>
                            <span class="ask">
                                {{$p->numero-10}}. {{$p->pregunta}}
                            </span>
                            
                            <div class="row form-group">

                                @foreach($p->opciones as $iopt => $opt)
                                    
                                    @php
                                        $res = isset($respuestas[$p->id]) ? (int)$respuestas[$p->id] : -1;
                                        $checked = ($res==$iopt) ? 'checked' : '';

                                        $disabled = $results ? 'disabled' : '';
                                        $classResult = '';
                                        if($results)
                                        {
                                            $classResult = ($iopt == $p->respuesta) ? 'radio-ok' : '';
                                            if($res == $iopt)
                                            {
                                                $classResult = ($res == $p->respuesta) ? 'radio-ok' : 'radio-ko';
                                            }
                                        }
                                        
                                    @endphp

                                    <div class="col-md-2">
                                        <label class="radio">
                                            <input {{$required}} class="radio-inline {{$classResult}}" type="radio" name="{{$p->id}}" value='{{$iopt}}' {{$checked}} {{$disabled}}> <span>{{$opt}}</span>
                                        </label>
                                    </div>

                                @endforeach               
                                
                            </div>

                        </li>
                    @endforeach
                </ul>

            </div>

        </div>

    </section>


    {{-- <section class="exam bloque-3">

        <div class="portlet box default">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    Part 4: Writing
                </div>
            </div>
            <div class="portlet-body">

                <div class="note note-info">
                    @foreach($exam->preguntas->where('bloque',4) as $p)
                        @if($p->numero == 0) @continue @endif
                        <span class="ask">
                            <strong class='font-18'>{{$p->pregunta}}</strong>
                            <hr>
                            {{$p->respuesta}}
                        </span>
                    @endforeach
                </div>
                
                @include('includes.form_textarea', [ 'campo'=> $p->id, 'valor'=> $respuestas[$p->id] ?? '', 'rows'=> 20 ])

            </div>
        </div>
        
    </section> --}}

    <hr>

@if($submit)
        {!! Form::submit(trans('area.enviar'), array('name'=> 'submit_exam', 'class' => 'btn btn-success')) !!}

{!! Form::close() !!}
@endif

@stop

@push('scripts')

<style>
    .hide {
        display: none;
    }
</style>

<script src="https://cdn.jsdelivr.net/vue/2.2.6/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.0/vue-resource.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.0/axios.min.js"></script>

<script type="text/javascript">

    var vmTest = new Vue({
            el: '#app_test1',
            data: function () {
                return {
                    opciones3: @json( $opcionesP3select ),
                    part3_selects: [],
                    part3_selecteds: [],
                };
            },
            mounted: function () {
                for (let index = 1; index < 12; index++) {
                    this.part3_selects[index] = "#part3-" + index
                    this.part3_selecteds[index] = 0
                }
            },
            destroyed() {

            },

            methods: {
                op3select(i, event) {
                    let val = event.target.value
                    let opt = "#part3-" + i
                    this.part3_selecteds[i] = val

                    $('select option.hide').removeClass('hide');
                    // var sel = $('option:selected', event.target).index();
                    
                    this.part3_selects.forEach((value) => {
                        if(value != opt)
                        {
                            this.part3_selecteds.forEach((ivalue) => {
                                $( value +' option:eq("' + ivalue + '")').addClass('hide');
                            })
                        }
                    })

                    $('.selectpicker').selectpicker('refresh');
                    
                },
            },
        });

</script>
@endpush