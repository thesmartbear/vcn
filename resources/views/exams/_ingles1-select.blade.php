@php
    $res = $op3[$name];
@endphp

@if(!$results)
<select class="selectpicker form-control" name="{{$name}}" id="{{$name}}" value="{{$res}}" v-on:change="op3select({{$index}},$event)">
    <option v-for="(op, iop) in {{$select}}" :value="iop"> @{{ op }} </option>
</select>

@else

    @php

        $rok = $ok3[$name];
        
        $acierto = ($res==$rok) ? true : false;
        $fallo = "";
        if(!$acierto)
        {
            if($res<0)
            {
                $fallo = "<span class='res-fallo'>xxxxx</span>&nbsp;";
            }
            else
            { 
                $fallo = "<span class='res-fallo'>".$opcionesP3[$res] ." </span>&nbsp;";
            }
        }
        
        $classResult = ($acierto) ? 'res-ok' : 'res-ko';

    @endphp

    <span class="res {{$classResult}}">{!! $fallo !!}{{$opcionesP3[$rok]}}</span>

@endif 

