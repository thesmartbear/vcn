@extends('web.britishsummer.baseweb')

@section('extra_head')
        <!-- Custom style -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/style.css" rel="stylesheet">
<link href="/assets/{{ConfigHelper::config('tema')}}/css/home.css" rel="stylesheet">

@stop

@section('container')
    <div class="blurried">
        <div class="linea"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="titular-seccion"></div>

                    <div class="intro-seccion">
                        <div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:3px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:8px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:13px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:18px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:23px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:29px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:34px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:39px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:44px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:49px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:55px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:60px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:65px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:70px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:75px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:81px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:86px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:91px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:96px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:101px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:107px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:112px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:117px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:122px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:127px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:133px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:138px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:143px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:148px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:153px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:159px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:164px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:169px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:174px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:179px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:185px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:190px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:195px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:200px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:205px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:0px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:0px"></div>
                        <p></p>
                    </div>

                    <div class='intro-video'>
                        <div class='col-md-10'>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5 col-sm-offset-1 col-lg-7 col-lg-offset-1">
                    <div class="promo-seccion"></div>
                    <div class="enlaces"></div>
                </div>
            </div>
        </div>
    </div>

    @include('web.britishsummer.includes.menu'.ConfigHelper::config('sufijo').'-'.App::getLocale())


    <div class="progress-indicator">
        <span><img src="/assets/{{ConfigHelper::config('tema')}}/img/load.gif" alt="" /></span>
    </div>
    <div class="close-touch">+</div>
    <div id="promocion" style="display: none; visibility: hidden;">
        <div class="icon"></div>
        <a href="curso-academico-en-el-extranjero/estados-unidos-colegio-publico-familia-voluntaria.html">
            <h4>Año Escolar en USA<br />¡por 10.380€!<br />
                <small>Año escolar en Estados Unidos (Colegio público y Familia voluntaria) 700€ de descuento si te apuntas antes del 31/12/2015</small>
            </h4>
        </a>
    </div>
    <div class="marco">
        <div id="logo">
            <a href="/"><img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" /></a>
        </div>
        <div class="contenido">

            <div class="all">

                <div class="one big box">
                    <div class="more" data-bg="#1abc9c" data-intro="Para que nos vamos a engañar, el inglés es el idioma que todo el mundo quiere aprender y por supuesto saber. Por eso en esta sección sólo hay cursos de inglés, para que nadie se pierda en la búsqueda y encuentre su curso idóneo.<br /><br />Inglés para niños, inglés para adolescentes, inglés para adultos, inglés para familias enteras, inglés para profesionales desesperados o no tanto. <em>“English for everybody and everywhere”</em>." data-video="">
                        <div class="inside">
                            <ul>
                                <li><a href="/aprender-ingles-en-el-extranjero/jovenes/grupos-con-monitor">PROGRAMAS DE VERANO CON MONITOR <small>de 8 a 18 años</small></a></li>
                                <li><a href="/aprender-ingles-en-el-extranjero/jovenes/cursos-especiales">CURSOS MUY SINGULARES/ESPECIALIZADOS <small>de 10 a 19 años</small></a></li>
                                <li><a href="/aprender-ingles-en-el-extranjero/adultos-profesionales">ADULTOS Y PROFESIONALES <small>de 18 a 99 años</small></a></li>
                                <li><a href="/aprender-ingles-en-el-extranjero/toda-la-familia">TODA LA FAMILIA</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="inside">
                        <div class="text">
                            <h2>APRENDE<br />
                                <strong>INGLÉS</strong><br />
                                EN EL EXTRANJERO</h2>
                            <h4>TODAS LAS EDADES</h4>
                        </div>
                    </div>
                </div>
                <div class="two big box">
                    <div class="inside">
                        <div class="text">
                            <i class="fa fa-navicon" id="main-menu"></i>
                        </div>
                    </div>
                </div>
                <div id="menu">

                </div>
                <div class="clearfix"></div>
                <div class="three big box">
                    <div class="more" data-bg="#888535" data-intro="No sólo de inglés vive el hombre. ¡Imagínate en un mercado de Shanghái hablado sobre la textura de los dim-sum con su cocinero! O negociando en francés para tu negocio de moda. O debatiendo en alemán sobre la nueva arquitectura de la ciudad de Berlín.<br /><br />Chino, japonés, francés, alemán, ruso, italiano, árabe, portugués... en la variedad está el buen gusto ¿No es así?" data-video="">
                        <div class="inside">
                            <ul>
                                <li><a href="/aprender-otros-idiomas-en-el-extranjero/jovenes">De 8 a 18 a&ntilde;os</a></li>
                                <li><a href="/aprender-otros-idiomas-en-el-extranjero/adultos">De 18 a 99 a&ntilde;os</a></li>
                                <li><a href="/aprender-otros-idiomas-en-el-extranjero/toda-la-familia/">TODA LA FAMILIA</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="inside">
                        <div class="text">
                            <h2>OTROS IDIOMAS<br />
                                EN EL EXTRANJERO<br />
                                <small>Alemán, Chino, Francés,<br />Italiano, Japonés, Ruso...</small></h2>
                            <h4>TODAS LAS EDADES</h4>
                        </div>
                    </div>
                </div>
                <div class="four small box">
                    <div class="inside">
                    </div>
                </div>

                @if(ConfigHelper::config('propietario') == 1 || ConfigHelper::config('propietario') == 0)
                <div class="five small box">
                    <div class="more" data-bg="#8e44ad" data-intro="Si creéis que aún es pronto para ir al extranjero o estáis convencidos que un buen campamento de idiomas en España puede ser incluso mejor que uno fuera... O si este verano disponéis de menos tiempo o de un presupuesto más reducido... Sea como sea, nuestros campamentos en inglés y con inglés son verdaderamente espectaculares. Elige fecha, especialidad y destino; cárgate las pilas y disponte a aprender sin límite." data-video="<div class='col-sm-12'><video controls preload='none' poster='/assets/{{ConfigHelper::config('tema')}}/home/maxcamps2015.jpg'>
   <source src='/assets/{{ConfigHelper::config('tema')}}/home/maxcamps2015.mp4' type='video/mp4'></video></div>">
                        <div class="inside">
                            <ul>
                                @foreach(\VCN\Models\Categoria::where('slug','campamentos-verano-ingles')->where(function ($query) {
                                    return $query
                                        ->where('propietario', 0)
                                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                                    })->get() as $categoria)
                                    @foreach($categoria->subcategorias as $sc)
                                        <li><a href="{{$categoria->slug}}/{{$sc->slug}}">{{$sc->name_web}}</a></li>
                                    @endforeach
                                @endforeach
                            </ul>
                            <p class="addmargintop20"><a href="/campamentos-verano-ingles/"><strong>NUESTRAS CLASES</strong></a></p>
                        </div>
                    </div>
                    <div class="inside">
                        <div class="text camps">
                            <h3>MAX CAMPS</h3>
                            <h2>CAMPAMENTOS<br>
                                DE VERANO <span>+inglés</span><br />
                                EN ESPAÑA</h2>
                            <h4><strong>DE 7 A 17 AÑOS</strong></h4>
                        </div>
                    </div>
                </div>
                <div class="six small box"></div>
                    <div class="seven small box bs">
                        <div class="more" data-bg="#2980b9" data-intro="Cada vez más las escuelas se plantean aprovechar una semana de su calendario escolar o algún paréntesis del mismo, para integrarse en la cultura de un país y practicar el idioma que llevan tiempo estudiando. En British Summer ofrecemos gran variedad de programas que permiten realizar unas clases del idioma elegido en distintos niveles y con participantes internacionales, alojarse en familia o apartamentos y disponer de tiempo libre para compartir en conjunto los increíbles atractivos de algunas de las ciudades europeas más cosmopolitas." data-video="">
                            <div class="inside">
                                <ul>
                                    <li><a href="/catalogos/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Short Stays</a></li>
                                    <li><a href="/catalogos/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Summer Courses</a></li>
                                    <li><a href="/catalogos/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Trimestre Escolar</a></li>
                                    <li><a href="/catalogos/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Campamentos de idiomas</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="inside">
                            <div class="text ">
                                <h2><span><strong>VIAJES EDUCATIVOS</strong></span><br />
                                    PARA COLECTIVOS<br />
                                    Y ESCUELAS</h2>
                            </div>
                        </div>
                    </div>
                @elseif(ConfigHelper::config('propietario') == 2)
                    <div class="five small box">
                        <div class="more" data-bg="#8e44ad" data-intro="Como cada curso, desde el equipo de la ICCIC, trabajamos con mucha ilusión y ganas en este proyecto, siempre manteniendo unas características y metodología que son fundamentales para nosotros. Las franjas de edad específicas de cada colonia y los grupos reducidos para facilitar la integración y el aprovechamiento de todas las clases y actividades, son un buen ejemplo.">
                            <div class="inside">
                                <ul>
                                    <li><a href="/summer-camps-ingles/l-armentera.html">L'ARMENTERA <small>Osona. De 6 a 10 años</small></a></li>
                                    <li><a href="/summer-camps-ingles/els-pins.html">ELS PINS <small>Maresme. De 10 a 15 años</small></a></li>
                                    <li><a href="/summer-camps-ingles/la-solana.html">LA SOLANA <small>La Cerdanya. De 10 a 15 años</small></a></li>
                                </ul>
                                <p class="addmargintop20"><a href="/summer-camps-ingles"><strong>¿QUÉ ES UNA COLONIA CIC EN CATALUÑA?</strong></a></p>
                            </div>
                        </div>
                        <div class="inside">
                            <div class="text camps">
                                <h3>SUMMER CAMPS</h3>
                                <h2>CAMPAMENTOS<br>
                                    DE VERANO <span> en inglés</span></h2>
                                <h4><strong>DE 6 A 15 AÑOS</strong></h4>
                            </div>
                        </div>
                    </div>
                    <div class="six small box"></div>
                    <div class="seven small box daycamp">
                        <div class="more" data-bg="#8e44ad" data-intro="La actividad perfecta para complementar un curso intensivo en la escuela.">
                            <div class="inside">
                                <ul>
                                    <li><a href="/summer-camps-ingles/active-english.html">ACTIVE ENGLISH <small>Talleres, actividades, juegos y excursiones en inglés</small></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="inside">
                            <div class="text ">
                                <h3>DAY CAMP</h3>
                                <h2>ACTIVE ENGLISH</h2>
                                <h4><strong>DE 10 A 13 AÑOS</strong></h4>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="eight medium box">
                    @if(ConfigHelper::config('propietario') == 1 || ConfigHelper::config('propietario') == 0)
                    <div class="more" data-bg="rgba(231, 76, 60,1)" data-intro="Las madres opinan<br /><span class='col-sm-12'><video controls preload='none' poster='/assets/{{ConfigHelper::config('tema')}}/home/madres.png'>
   <source src='/assets/{{ConfigHelper::config('tema')}}/home/madres.mp4' type='video/mp4'></video></span>Un vídeo que nos permite entender lo que supone para un estadounidense acoger a un estudiante de año académico de forma voluntaria." data-video="
                    	<iframe class='col-sm-10' height='180' frameborder='0' wmode='opaque' allowfullscreen='' src='https://www.youtube.com/embed/YRP3PbaE-cA' />" data-promo="<div class='col-sm-12'><h2>Abierta la inscripción para el curso 2016/17<br />
                        <small>Pídenos cita para realizar tu test de nivel e informarte en profundidad.</small></h2>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0' class='direcciones table table-hover'>
                          <tr class='plusinfoaa' data-oficina='madrid' data-toggle='modal' data-target='#plusinfomodal'>
                            <td><strong>MADRID</strong></td>
                            <td class='text-center'>madrid@britishsummer.com</td>
                            <td class='text-right'>T. 91.345.95.65</td>
                          </tr>
                          <tr class='plusinfoaa' data-oficina='infobs' data-toggle='modal' data-target='#plusinfomodal'>
                            <td><strong>BARCELONA</strong></td>
                            <td class='text-center'>infobs@britishsummer.com</td>
                            <td class='text-right'>T. 93.200.88.88</td>
                          </tr>
                          <tr class='plusinfoaa' data-oficina='girona' data-toggle='modal' data-target='#plusinfomodal'>
                            <td><strong>GIRONA</strong></td>
                            <td class='text-center'>girona@britishsummer.com</td>
                            <td class='text-right'>T. 972.414.902</td>
                          </tr>
                          <tr class='plusinfoaa' data-oficina='sevilla' data-toggle='modal' data-target='#plusinfomodal'>
                            <td><strong>SEVILLA</strong></td>
                            <td class='text-center'>sevilla@britishsummer.com</td>
                            <td class='text-right'>T. 95.421.07.85</td>
                          </tr>
                        </table>
                        </div>
                        ">
                        @elseif(ConfigHelper::config('propietario') == 2)
                            <div class="more" data-bg="rgba(231, 76, 60,1)" data-intro="Las madres opinan<br /><span class='col-sm-12'><video controls preload='none' poster='/assets/{{ConfigHelper::config('tema')}}/home/madres.png'>
   <source src='/assets/{{ConfigHelper::config('tema')}}/home/madres.mp4' type='video/mp4'></video></span>Un vídeo que nos permite entender lo que supone para un estadounidense acoger a un estudiante de año académico de forma voluntaria." data-video="
                    	<iframe class='col-sm-10' height='180' frameborder='0' wmode='opaque' allowfullscreen='' src='https://www.youtube.com/embed/YRP3PbaE-cA' />" data-promo="">
                        @endif

                        <div class="inside">
                            <ul>
                                <li><a href="/curso-escolar-en-el-extranjero/curso-escolar/">A&ntilde;o Escolar</a></li>
                                <li><a href="/curso-escolar-en-el-extranjero/semestre-escolar/">Semestre Escolar</a></li>
                                <li><a href="/curso-escolar-en-el-extranjero/trimestre-escolar/">Trimestre Escolar</a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="inside" style="text-shadow: 1px 1px 0px black;">
                        <div class="text">
                            <h2>AÑO ACADÉMICO<br />
                                <small>Semestre / Trimestre<br />en el extranjero</small></h2>
                            <h4>Primaria, Secundaria<br />y Bachillerato</h4>
                            <h3>Inscripción abierta curso 2016/17</h3>
                        </div>
                    </div>
                    <div class="header-unit">
                        <div id="video-container">
                            <video autoplay loop class="fillWidth">
                                <source src="/assets/{{ConfigHelper::config('tema')}}/home/academico2015.mp4" type="video/mp4" />
                                <source src="/assets/{{ConfigHelper::config('tema')}}/academico.ogv" type="video/ogg" />
                                <source src="/assets/{{ConfigHelper::config('tema')}}/academico.webm" type="video/webm" />
                            </video>
                        </div><!-- end video-container -->
                    </div><!-- end .header-unit -->
                </div>
            </div>
        </div>

                <!-- Modal -->
        <div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="respuesta">
                            <h2>Año Académico 2016/17</h2>
                            <form action="plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                                <div class="msg"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <div class="form-group">
                                            <label for="name">Nombre</label>
                                            <input type="text" class="form-control" id="name" placeholder="Nombre">
                                        </div>
                                        <div class="form-group">
                                            <label for="tel">Teléfono</label>
                                            <input type="text" class="form-control" id="tel" placeholder="teléfono de contacto">
                                        </div>
                                        <div class="form-group">
                                            <label for="tel">Email</label>
                                            <input type="text" class="form-control" id="email" placeholder="email de contacto">
                                            <input type="hidden" id="curso" value="Año Académico 2016/17">
                                            <input type="hidden" id="oficina" value="">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary enviar" id="plusinfoenviar" type="button">Enviar</button>

                        <p class="text-center"><br /><small><a href="proteccion-de-datos.html" target="_blank">Protección de Datos de Carácter Personal</a></small></p>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
@stop

@section('extra_footer')

        <script src="/assets/{{ConfigHelper::config('tema')}}/js/home.js"></script>

        <script type="text/javascript">
            $(window).load(function(){
                $('#promomodal').modal('show');
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.msg').hide();
                $('#plusinfomodal').on('shown.bs.modal', function (event) {
                    queoficina = $(event.relatedTarget)
                    oficina = queoficina.data('oficina');
                    $('#oficina').val(oficina);
                    //console.log('-->' + oficina);
                })
                $("#plusinfoenviar").click(function(e) {
                    if ($('#name').val() == ''){
                        $('.msg').html('Debes indicar un nombre de contacto');
                        $('.msg').show();
                        return false;
                    }
                    if ($('#email').val() == ''){
                        $('.msg').html('Debes indicar un teléfono o un email de contacto');
                        $('.msg').show();
                        return false;
                    }

                    if ($('#email').val() != ''){
                        var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/;
                        if (re.test($('#email').val())) {
                            $('.msg').html('El email no tiene un formato correcto');
                            $('.msg').show();
                            return false
                        }

                    }

                    if (!/^([0-9])*$/.test($('#tel').val())){
                        $('.msg').html('El campo teléfono tiene que ser numérico');
                        $('.msg').show();
                        return false
                    }

                    post_data = {'name':$('#name').val(), 'tel':$('#tel').val(), 'email':$('#email').val(), 'curso':$('#curso').val(), 'emailoficina':$('#oficina').val()};

                    $.ajax({
                        type: "POST",
                        url: "plusinfoaasend.php",
                        data: post_data,
                        beforeSend: function(){
                            console.log('name: ' + $('#name').val() + ' tel: ' + $('#tel').val() + ' email: ' + $('#email').val(), 'curso: ' + $('#curso').val() + ' emailoficina: ' + $('#oficina').val());
                        },
                        success: function(msg){
                            $("#respuesta").html(msg);
                            $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                        },
                        error: function(){
                            alert("error!!");
                        }
                    });

                });



            });

            $('form input').blur(function () {
                $('.msg').hide();
            });
        </script>
@stop
