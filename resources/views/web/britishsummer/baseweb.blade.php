<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @section('title')
            {{ConfigHelper::config('web')}}
        @show
    </title>

    {!! Html::style('assets/css/bootstrap.css') !!}

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100,400,700,700italic,400italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <!-- ==============================================
		Favicons
		=============================================== -->
    <link rel="shortcut icon" href="/assets/{{ConfigHelper::config('tema')}}/favicon/favicon{{ConfigHelper::config('sufijo')}}.ico">
    <link rel="apple-touch-icon" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}-114x114.png">


    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Overwrite bootstrap -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/overwrite.css" rel="stylesheet">

    <!-- Font -->
    <link href='//fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/bs2015.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" media="all" href="/assets/{{ConfigHelper::config('tema')}}/css/jquery-jvectormap.css"/>


    <!-- font awesome -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- range slider -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/includes/ion-range-slider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
    <link href="/assets/{{ConfigHelper::config('tema')}}/includes/ion-range-slider/css/ion.rangeSlider.skinFlat.grey.css" rel="stylesheet" type="text/css" />


    <link rel="stylesheet" type="text/css" href="/assets/{{ConfigHelper::config('tema')}}/css/magnific-popup.css">

    <link href="/assets/{{ConfigHelper::config('tema')}}/css/bootstrap-select.css" rel="stylesheet" type="text/css" />

    @yield('extra_head')

</head>
<body>

<div style="z-index: 400; background: #9138BC; padding: 10px; border-radius: 5px; position: fixed; right: -60px; padding-right: 60px; padding-bottom: 0px; width: 280px; top: 80px;">
    @if (Auth::guest())
        <p><a class="btn widget-btn-default" href="auth/login">Login</a></p>
    @else
        <p><a href="/manage">Hi, {{ Auth::user()->fname }}</a></p>
        <p><a class="btn btn-default" href="{{route('auth/logout')}}">Logout</a></p>
    @endif
</div>

    @yield('container')


    @include('web.britishsummer.includes.footer-'.ConfigHelper::config('sufijo'))


    <script src='https://code.jquery.com/jquery-2.1.1.min.js' type='text/javascript'></script>
    <script src='https://code.jquery.com/jquery-migrate-1.2.1.min.js' type='text/javascript'></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


    <script src="/assets/{{ConfigHelper::config('tema')}}/js/jquery.cookie.js"></script>
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/jquery.cookiecuttr.js"></script>


    <!-- JavaScript totop -->
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/totop/jquery.ui.totop.js"></script>
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/totop/setting.js"></script>

    <!-- Contact validation js -->
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/validation.js"></script>

    <!-- mapa -->
    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/maps/jquery-jvectormap-1.2.2.min.js"></script>
    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/maps/map_es.js"></script>
    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/contacto.js"></script>


    <!-- Custom js -->
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/custom.js"></script>


    <!-- range slider -->
    <script src="/assets/{{ConfigHelper::config('tema')}}/includes/ion-range-slider/js/ion-rangeSlider/ion.rangeSlider.min.js" type="text/javascript"></script>

    <!-- detect mobile -->
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/detectmobilebrowser.js" type="text/javascript"></script>

    <!-- isotope -->
    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/isotope.pkgd.min.js"></script>



    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/jquery.magnific-popup.min.js"></script>

    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/bootstrap-select.js"></script>



    @yield('extra_footer')

    <script>

        $(document).ready(function () {
            // activate cookie cutter
            if($('html').attr('lang') == 'es'){
                cookieText = "Utilizamos cookies propias y de terceros para mejorar nuestros servicios y realizar anal&iacute;ticas web. Si continua navegando, consideramos que acepta su uso.<br />Puede cambiar la configuraci&oacute;n u obtener m&aacute;s informaci&oacute;n en ";
                cookieTextLink = "Pol&iacute;tica de Cookies";
                cookieLink = './politica-de-cookies.html';
                buttonText = 'Acepto';
            }else if($('html').attr('lang') == 'ca'){
                cookieText = "Utilitzem cookies pr&ograve;pies i de tercers per millorar nostres serveis i realitzar anal&iacute;tiques web. Si continua navegant, considerem que accepta el seu &uacute;s.<br />Pot canviar la configuraci&oacute; o obtenir m&eacute;s informaci&oacute; a ";
                cookieTextLink = "<br />Pol&iacute;tica de Cookies";
                cookieLink = './ca/politica-de-cookies.html';
                buttonText = 'Acepto';
            }else if($('html').attr('lang') == 'en'){
                cookieText = "We use cookies on this website, you can ";
                cookieTextLink = "<br />read about them here";
                cookieLink = './politica-de-cookies.html';
                buttonText = 'Accept cookies';
            }

            $.cookieCuttr({
                cookieDeclineButton: false,
                cookieAnalyticsMessage: cookieText,
                cookieWhatAreLinkText: cookieTextLink,
                cookieWhatAreTheyLink: cookieLink,
                cookieExpires: 365,
                cookieAcceptButtonText: buttonText
            });
        });


/*

        if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept") {

            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-633952-18', 'britishsummer.com');
            ga('send', 'pageview');


            // Remarketing
            document.write('<div style="height:0;position:absolute;width: 0px !important;">');
*/
            /* <![CDATA[ */
/*
            var google_conversion_id = 991933354;
            var google_conversion_label = "xM5aCP7qxwQQquf-2AM";
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
            /* ]]> */
/*
            document.write('<sc'+'ript' + ' type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></sc' + 'ript>');
            document.write('<noscript>');
            document.write('<div style="display:inline; height:0;">');
            document.write('<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/991933354/?value=1.000000&amp;label=xM5aCP7qxwQQquf-2AM&amp;guid=ON&amp;script=0"/>');
            document.write('</div>');
            document.write('</noscript>');
            document.write('</div>');

            document.write('<!-- Google Tag Manager -->');
            document.write('<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T6PHKG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>');
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-T6PHKG');
            document.write('<!-- End Google Tag Manager -->');

        }
*/
    </script>

</body>
</html>