@extends('web.britishsummer.baseweb')

@section('extra_head')
        <!-- Custom style -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/style.css" rel="stylesheet">
<link href="/assets/{{ConfigHelper::config('tema')}}/css/contents.css" rel="stylesheet">

<!-- Color style -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/{{$clase}}.css" rel="stylesheet">
@stop

@include('web.britishsummer.includes.menu'.ConfigHelper::config('sufijo').'-'.App::getLocale())
<div id="menubg">
    <div class="inside">
        <div class="text">
            <i class="fa fa-navicon" id="main-menu"></i>
        </div>
    </div>
</div>
<div id="menu"></div>

@section('container')
        <!-- Start wrapper -->
<section id="wrapper">
    <div class="max-width" id="allcontent">
        <!-- Start row1 -->
        <div class="row">
            <!-- Start article -->
            <div class="col-xs-12 wrapper-bg" id="header">
                <article>

                    <div class="intro-img col-xs-4 col-sm-6 pull-right" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/{{rand(1, 3)}}.png') repeat;">
                    </div>

                    <div class="intro col-xs-8 col-sm-6 pull-right">
                        <div class="titulo">
                            <div id="logo"><a href="/"><img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" /></a></div>
                            <h1 class="slogan">
                                <span></span>
                                @if($categorianame != '')
                                    <span>
                                        <ul class="breadcrumb">
                                            <li class="active"><a href="../">{{$categorianame}}</a></li>
                                            <li><a href="../{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $subcategoria->id, $subcategoria->slug) !!}">{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}</a></li>
                                        </ul>
                                    </span>
                                    {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoriadet->id, $subcategoriadet->name_web) !!}
                                    <br />
                                    <span></span>
                                    <small>
                                        @if(Lang::has('web.categorias.'.$clase.'-desc')){!! trans('web.categorias.'.$clase.'-desc') !!}@endif
                                    </small>
                                @elseif($subcategoriadet == '')
                                    <span>
                                       <ul class="breadcrumb">
                                           <li class="active"><a href="/{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $categoria->id, $categoria->slug) !!}">{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!}</a></li>
                                       </ul>
                                    </span>
                                    {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}
                                    <br />
                                    <span></span>
                                    <small>
                                        @if(Lang::has('web.categorias.'.$subcategoria->slug.'-desc')){!! trans('web.categorias.'.$subcategoria->slug.'-desc') !!}@endif
                                    </small>
                                @else
                                    <span>
                                       <ul class="breadcrumb">
                                            <li class="active"><a href="/{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $categoria->id, $categoria->slug) !!}">{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!}</a></li>
                                            <li><a href="/{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $categoria->id, $categoria->slug) !!}/{!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $subcategoria->id, $subcategoria->slug) !!}">{!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}</a></li>
                                        </ul>
                                    </span>
                                    {!! Traductor::getWeb(App::getLocale(), 'SubcategoriaDetalle', 'name_web', $subcategoriadet->id, $subcategoriadet->name_web) !!}
                                    <br />
                                    <span></span>
                                    <small>
                                    @if(Lang::has('web.categorias.'.$subcategoriadet->slug.'-desc')){!! trans('web.categorias.'.$subcategoriadet->slug.'-desc') !!}@endif
                                    </small>
                                @endif
                            </h1>

                            <h4 class="head seccion">{!! trans('web.categorias.eligecurso') !!}<span></span></h4>
                        </div>
                    </div>

                </article>
            </div>
        </div>
        <!-- End article -->




        <div class="container" id="contenido">

            @include('web.britishsummer.includes.plusinfo')

            <div class="row">

                <div class="col-sm-8 col-xs-12 addmargintop60 referencia">
                    <div class="introseccion">
                        @if($subcategoriadet != '')
                            @if(Lang::has('web.categorias.'.$subcategoriadet->slug.'-intro')){!! trans('web.categorias.'.$subcategoriadet->slug.'-intro') !!}@endif
                        @else
                            @if(Lang::has('web.categorias.'.$subcategoria->slug.'-intro')){!! trans('web.categorias.'.$subcategoria->slug.'-intro') !!}@endif
                        @endif
                    </div>
                    <div class="row addmargintop60" id="programas">
                        @if($categorianame != '')
                            <ul>
                                @foreach($cursos as $curso)
                                    <li><a href="../{!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug) !!}.html"><b>{!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_name', $curso->id, $curso->course_name) !!}</b> [{{$curso->centro->name}}] ({{$curso->centro->pais->name}})</a></li>
                                @endforeach
                            </ul>
                        @else
                            <ul>
                                @foreach($cursos as $curso)
                                    <li><a href="/{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $categoria->id, $categoria->slug) !!}/{!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug) !!}.html"><b>{!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_name', $curso->id, $curso->course_name) !!}</b> [{{$curso->centro->name}}] ({{$curso->centro->pais->name}})</a></li>
                                @endforeach
                            </ul>
                        @endif


                        @include('web.britishsummer.includes.copyright')

                    </div>
                </div>
                <!-- Start right sidebar -->
                <div class="col-sm-3 col-sm-offset-1 wrapper-bg" id="sidebar">

                    <div class="box">
                        <div class="widget clearfix filtros">

                        </div>
                    </div>
                </div>
                <!-- End right sidebar -->
            </div>

        </div>

</section>
<!-- End wrapper -->

<!-- Modal -->
<div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
            </div>
            <div class="modal-body">
                <div id="respuesta">
                    <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                    <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                        <div class="msg"></div>
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="form-group">
                                    <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                    <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                    <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                    <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                    <input type="hidden" id="curso" value="@if($categorianame != ''){{$categorianame}} - {!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!} @if($subcategoriadet != '')- {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoriadet->id, $subcategoriadet->name_web) !!}@endif @else{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!} - {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}@if($subcategoriadet != '') - {!! Traductor::getWeb(App::getLocale(), 'SubcategoriaDetalle', 'name_web', $subcategoriadet->id, $subcategoriadet->name_web) !!}@endif @endif">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>

                <p class="text-center"><br /><small><a href="/proteccion-de-datos.html" target="_blank">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</a></small></p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop


@section('extra_footer')
<!-- contenidos -->
<script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/contenidos.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.msg').hide();
        $("#plusinfoenviar").click(function() {
            //console.log('validar');
            if ($('#name').val() == ''){
                $('.msg').html('Debes indicar un nombre de contacto');
                $('.msg').show();
                return false;
            }
            if ($('#email').val() == ''){
                $('.msg').html('Debes indicar un teléfono o un email de contacto');
                $('.msg').show();
                return false;
            }
            /*
             if ($('#email').val() != ''){
             var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
             if (re.test($('#name').val())) {
             $('.msg').html('El email no tiene un formato correcto');
             $('.msg').show();
             }
             return false
             }
             */
            if (!/^([0-9])*$/.test($('#tel').val())){
                $('.msg').html('El campo teléfono tiene que ser numérico');
                $('.msg').show();
                return false
            }


            post_data = {'name':$('#name').val(), 'tel':$('#tel').val(), 'curso':$('#curso').val()};
            $.ajax({
                type: "POST",
                url: "//www.britishsummer.com/plusinfosend.php",
                data: post_data,
                success: function(msg){
                    $("#respuesta").html(msg);
                    $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                    ga('send', 'Solicitud', 'button', 'click', 'Año Académico', 1);
                },
                error: function(){
                    alert("error!!");
                }
            });
        });

    });

    $('form input').blur(function () {
        $('.msg').hide();
    });

    $('#fotos-curso').magnificPopup({
        delegate: 'a', // child items selector, by clicking on it popup will open
        type: 'image',
        gallery: {
            enabled: true
        },
        titleSrc: 'title'
    });





    //////////////////////////////////////




    var unique = function(origArr) {
        var newArr = [],
                origLen = origArr.length,
                found,
                x, y;

        for ( x = 0; x < origLen; x++ ) {
            found = undefined;
            for ( y = 0; y < newArr.length; y++ ) {
                if ( origArr[x] === newArr[y] ) {
                    found = true;
                    break;
                }
            }
            if ( !found) newArr.push( origArr[x] );
        }
        return newArr;
    }

    $(function(){

        var inicio = 1;
        /*
         $("#edad").ionRangeSlider({
         min: 8,
         max: 18,
         from: 8,
         to: 18,
         type: "double",
         step: 1,
         postfix: " a&ntilde;os",
         grid: true,
         gridMargin: 15,

         onFinish: function (data) {
         //console.log(data);

         var list = [];

         if(data.min == data.from && data.max == data.to){
         $('#edad-all').prop('checked', true).triggerHandler('change');
         }else{

         for (var i = data.from; i <= data.to; i++) {
         list.push(i);
         if ($('#edad-'+i).prop('checked') == false){
         $('#edad-'+i).prop('checked', true).triggerHandler('change');
         }
         }

         //console.log(list);
         //actualizar($('#edad-'+i).trigger('click'));

         for (var i = data.min; i <= data.max; i++) {
         if($.inArray(i, list) == -1){
         if ($('#edad-'+i).prop('checked') == true){
         $('#edad-'+i).prop('checked', false).triggerHandler('change');
         //$('#edad-'+i).trigger('click');
         }
         }
         }
         }

         },

         onUpdate: function (data) {
         //console.log(data);
         }
         });

         var slider = $("#edad").data("ionRangeSlider");

         function updateRange(from,to) {
         slider.update({
         from: from,
         to: to
         });
         };

         */
        var $container = $('#programas');
        var sortValue = $('#extras').val(); //'original-order';

        if(window.location.hash.slice(1) != ''){
            //console.log(window.location.hash.slice(1));
            filtros = window.location.hash.slice(1);
            filtros = filtros.replace(/.\s/g,"");
            filtros = filtros.split('.');
            filtros = unique(filtros);

            filtros = filtros.filter(Boolean);
            //console.log('filtros: ' + filtros);
            $('.filtros input:checkbox .all').attr('checked', true);
            $('.filtros input:checkbox:not(".all")').removeAttr('checked');

            for (i=0;i<filtros.length;i++){
                $('#' + filtros[i]).attr('checked', true);
                grupo = $('#' + filtros[i]).closest('.option-set').attr('data-group');
                nombre = grupo + '-all';
                $('#'+nombre).attr('checked', false);
                //console.log(nombre);

            }
            var hashash = window.location.hash.slice(1).split(', ');


        }


        $container.isotope({
            filter: window.location.hash.slice(1),
            getSortData: {
                <?
                //echo $sortextras;
                ?>
              },
            sortBy: sortValue,
            itemSelector: '.programa',
            masonry: {
                columnWidth: '.programa'
            }
        });

        $container.isotope( 'on', 'layoutComplete',
                function( isoInstance, laidOutItems ) {
                    //console.log('1');
                    var results = laidOutItems.length;
                    $('.filter-results span').html(results+'');
                }

        );



        var $container = $('#programas').isotope({
            filter: window.location.hash.slice(1),
            getSortData: {
                <?
              //echo $sortextras;
              ?>
            },
            sortBy: sortValue,
            itemSelector: '.programa',
            masonry: {
                columnWidth: '.programa'
            }});


//console.log('sortValue: '+sortValue);




        var filters = {};

        function actualizar(jQEvent,sortValue) {

            if(hashash != ''){
                //console.log(hashash);
            }
            //console.log(filters[ group ]);
            jQEvent.preventDefault();
            //console.log(jQEvent);
            var sortValue = sortValue;

            var checkbox = jQEvent.target;
            var $checkbox = $( checkbox );
            var value = checkbox.value;
            //console.log(value);
            var isAll = $checkbox.hasClass('all');
            var group = $checkbox.closest('.option-set').attr('data-group');
            // create array for filter group, if not there yet

            if ( !filters[ group ] ) {
                filters[ group ] = [];
            }else if(!filters[ group ] && window.location.hash.slice(1) != ''){
                filters[ group ] = window.location.hash.slice(1);
            }



            var index = $.inArray( checkbox.value, filters[ group ] );

            if ( isAll ) {
                //console.log('isAll');
                delete filters[ group ];
                //console.log('isAll checked?: '+checkbox.checked);
                $('.option-set[data-group='+group+']').find('input').removeAttr('checked');
                if ( !checkbox.checked ) {
                    checkbox.checked = 'checked';
                }

            }


            if ( checkbox.checked ) {
                //console.log('checkbox.checked:' + checkbox.checked);
                //console.log('checked: ' + $('.option-set[data-group='+group+']').find('input[checked]').length);
                var selector = isAll ? 'input' : 'input.all';
                //$('.option-set[data-group='+group+']').find(selector+'not:'+checkbox).removeAttr('checked');

                if ( !isAll && index === -1 ) {
                    // add filter to group
                    filters[ group ].push( checkbox.value );
                }
            } else if ( !isAll ) {
                // remove filter from group
                filters[ group ].splice( index, 1 );
                // if unchecked the last box, check the all

                if ( !$('.option-set[data-group='+group+']').find('input[checked]').length ) {
                    $('.option-set[data-group='+group+']').find('input.all').attr('checked', 'checked');
                }
            }

            //console.log('group ->' + filters[ group ]);

            var i = 0;
            var comboFilters = [];
            var message = []


            for ( var prop in filters ) {
                message.push( filters[ prop ].join(' ') );
                var filterGroup = filters[ prop ];
                //console.log('filterGroup' + i + ' ->' + filterGroup);
                // skip to next filter group if it doesn't have any values
                if ( !filterGroup.length ) {
                    continue;
                }

                if ( i === 0 ) {
                    // copy to new array
                    comboFilters = filterGroup.slice(0);
                } else {
                    var filterSelectors = [];
                    // copy to fresh array
                    var groupCombo = comboFilters.slice(0); // [ A, B ]
                    // merge filter Groups
                    for (var k=0, len3 = filterGroup.length; k < len3; k++) {
                        for (var j=0, len2 = groupCombo.length; j < len2; j++) {
                            filterSelectors.push( groupCombo[j] + filterGroup[k] ); // [ 1, 2 ]
                        }
                    }

                    // apply filter selectors to combo filters for next group
                    comboFilters = filterSelectors
                }
                i++
            }

            // console.log(comboFilters);
            window.location.hash = comboFilters.join(', ');
            //console.log( message );
            //console.log( comboFilters.join(', ') );


            $container.isotope({
                filter: comboFilters.join(', '),
                getSortData: {
                    <?
                 //echo $sortextras;
                 ?>
               },
                sortBy: sortValue,
                itemSelector: '.programa',
                masonry: {
                    columnWidth: '.programa'
                }
            });

            $container.isotope( 'on', 'layoutComplete',
                    function( isoInstance, laidOutItems ) {
                        //console.log('2');
                        //console.log(sortValue);
                        var results = laidOutItems.length;
                        $('.filter-results span').html(results+'');
                    }

            );
        }




        window.onpopstate = function(event){
            //console.log("location: " + document.location + ", state: " + event.state);
            //console.log('onpopstate');

            if(window.location.hash.slice(1) != ''){
                //console.log('hola');
                filtros = window.location.hash.slice(1);
                filtros = filtros.replace(/.\s/g,"");
                filtros = filtros.split('.');
                filtros = unique(filtros);
                filtros = filtros.filter(Boolean);
                //console.log('filtros: ' + filtros);

                //$('.filtros input:checkbox(".all")').attr('checked', true);
                //$('.filtros input:checkbox:not(".all")').removeAttr('checked');
                for (i=0;i<filtros.length;i++){
                    $('#' + filtros[i]).attr('checked', true);
                    grupo = $('#' + filtros[i]).closest('.option-set').attr('data-group');
                    //console.log('grupo ---> ' + grupo);
                    nombre = grupo + '-all';
                    //console.log('nombre ---> ' + nombre);
                    $('#'+nombre).attr('checked', false);
                }
                //$('.filtros input:checkbox(".all")').attr('checked', true);
                //$('.filtros input:checkbox:not(".all")').removeAttr('checked');
            }

            $container.isotope({
                filter: window.location.hash.slice(1),
                getSortData: {
                    <?
                 //echo $sortextras;
                 ?>
               },
                sortBy: sortValue,
                itemSelector: '.programa',
                masonry: {
                    columnWidth: '.programa'
                }
            });
            $container.isotope( 'on', 'layoutComplete',
                    function( isoInstance, laidOutItems ) {
                        var results = laidOutItems.length;
                        $('.filter-results span').html(results+'');
                    }
            );
        };



        $('.option-set').on( 'change', function( jQEvent ) {
            //console.log('change');
            var value = $('#extras').val()
            var sortValue = [value,'original-order'];
            actualizar(jQEvent,sortValue);
        });
        /*
         $('#edades input').on( 'change', function( jQEvent ) {
         console.log('change');
         var value = $('#extras').val()
         var sortValue = [value,'original-order'];
         actualizar(jQEvent,sortValue);
         });
         */

        // bind sort combobox
        $('#extras').on( 'change', function(e) {
            e.preventDefault();

            // make an array of values
            var value = $('#extras').val()
            var sortValue = [value,'original-order'];
            $('#programas').isotope({
                filter: window.location.hash.slice(1),
                getSortData: {
                    <?
                 //echo $sortextras;
                 ?>
               },
                sortBy: sortValue,
                itemSelector: '.programa',
                masonry: {
                    columnWidth: '.programa'
                }
            });
            //console.log(value);
            $('.programa').removeClass('programaactivo');
            $('.programa[data-'+value+'=1]').addClass('programaactivo');
        });


        $('input[type=checkbox][checked]').trigger('change');


    });

</script>
@stop


