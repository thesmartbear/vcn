@extends('web.britishsummer.baseweb')

@section('container')
    <div class="row">
        <div class="col-sm-12">
            <h2>Subcategoria detalle: {{$subcategoriadet->name}}</h2>
        </div>
    </div>

    @include('web.britishsummer.includes.copyright')
@stop
