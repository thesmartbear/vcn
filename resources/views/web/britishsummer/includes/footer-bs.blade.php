<div id="main-footer">
    <div>
        <div class="row">

            <div class="social-div">
                <div id="search-box">
                    <form action="{{route('web.buscar')}}" method="post" enctype="multipart/form-data" class="searchbox" autocomplete="off" >
                        <input type="text" placeholder="Buscar......" name="search"  id="search" class="searchbox-input" required>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    </form>
                </div>
                <a class="btn-search"><i class="fa fa-search"></i></a>
                <ul class="social">
                    <li><a href="//www.facebook.com/britishsummerfan" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="//www.twitter.com/britishsm" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="//www.youtube.com/britishsummersm" target="_blank"><i class="fa fa-youtube-play"></i></li>
                    <li><a href="https://instagram.com/britishsummeres/" target="_blank"><i class="fa fa-instagram"></i></li>
                    <li><a href="https://plus.google.com/106203615715916398922/posts" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="https://www.pinterest.com/britishsummer/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                    <li><a href="//www.linkedin.com/company/british-summer" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
            <div class="btns-div">
                <a class="btn-contact" href="#"><span>CONTACTO</span></a>
                <a class="btn-clientes" href="https://www.britishsummer.com/clientes/"><span>&Aacute;REA DE CLIENTES</span></a>
                @if(App::getLocale() == 'es')
                    <a class="btn-idioma" href="/ca"><span>CAT</span></a>
                @elseif(App::getLocale() == 'ca')
                    <a class="btn-idioma" href="/es"><span>ES</span></a>
                @endif

            </div>

            <ul class="secondary">
                <li><a href="/catalogo.html" class="white-txt">CAT&Aacute;LOGO</a></li>
                <li><a href="/inscripcion.html">HOJAS INSCRIPCI&Oacute;N</a></li>
                <li class="separator"></li>
                <li><a href="/quienes-somos.html">QUI&Eacute;NES SOMOS</a></li>
                <li><a href="/trabaja-con-nosotros.html">TRABAJO</a></li>
                <li><a href="/colaboradores.html">COLABORADORES</a></li>
                <li class="separator"></li>
                <li><a href="/blog-aprender-ingles-extranjero" target="_blank">BLOG</a></li>
                <li><a href="http://www.landedblog.com" target="_blank" class="destacado"><strong>{!! trans('web.landedslogan') !!}</strong></a></li>
            </ul>
        </div>
    </div>

</div>
</div>

<div id="dropdown-contact">

    <div class="container-fluid">
        <a href="#" class="contact-close">+</a>
        <div class="row addmargintop30">

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="head oficinas">Oficinas
                            <span></span>
                        </h4>
                    </div>
                    <div class="col-sm-4" id="mapa_info">
                        <p class="barcelona"><strong>BARCELONA</strong><br>
                            V&iacute;a Augusta, 33<br>
                            08006 Barcelona<br>
                            Tel. 93 200 88 88<br>
                            Fax 93 202 23 71<br>
                            <a href="mailto:infobs@britishsummer.com">infobs@britishsummer.com</a></p>

                        <p class="girona"><strong>GIRONA</strong><br>
                            Carrer Migdia, 25
                            <br>
                            17002 Girona<br>
                            Tel. 972 414 902<br>
                            <a href="mailto:girona@britishsummer.com">girona@britishsummer.com</a></p>

                        <p class="madrid"><strong>MADRID</strong><br>
                            Paseo de la Castellana <br>
                            136 bajos<br>
                            28046 MADRID<br>
                            Tel.: 91 345 95 65<br>
                            <a href="mailto:madrid@britishsummer.com">madrid@britishsummer.com</a></p>

                        <p class="sevilla"><strong>SEVILLA</strong><br>
                            Pza. Cristo de Burgos <br>
                            21. Bajo A<br>
                            41003 Sevilla<br>
                            Tel.: 95 421 07 85<br>
                            <a href="mailto:sevilla@britishsummer.com">sevilla@britishsummer.com</a></p>
                    </div>
                    <div class="col-sm-8 pull-right" style="float: left;">
                        <div id="mapa" style="width: 100%; height: 450px;"></div>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">
                <h4 class="head">Contacto<span></span></h4>
                <form id="contactform" action="contact.php" method="post" class="validateform" name="leaveContact">
                    <div id="sendmessage">
                        <div class="alert alert-info marginbot35">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Tu mensaje ha sido enviado.</strong><br />
                            En breve nos pondremos en contacto contigo.
                            <br />Gracias por confiar en nosotros.
                        </div>
                    </div>
                    <ul class="listForm">
                        <li>
                            <label>Oficina <span>*</span></label>
                            <select class="form-control" name="oficina" data-rule="required" data-msg="Por favor, selecciona una oficina"  />
                            <option value=""></option>
                            <option value="infobs@britishsummer.com">Barcelona</option>
                            <option value="girona@britishsummer.com">Girona</option>
                            <option value="madrid@britishsummer.com">Madrid</option>
                            <option value="sevilla@britishsummer.com">Sevilla</option>
                            </select>
                            <div class="validation"></div>
                        </li>
                        <li>
                            <label>Nombre <span>*</span></label>
                            <input class="form-control" type="text" name="name" data-rule="maxlen:4" data-msg="Tienes que introducir al menos 4 caracteres" />
                            <div class="validation"></div>
                        </li>
                        <li>
                            <label>Email <span>*</span></label>
                            <input class="form-control" type="text" name="email" data-rule="email" data-msg="Por favor, introduce un email válido" />
                            <div class="validation"></div>
                        </li>
                        <li>
                            <label>Mensaje <span>*</span></label>
                            <textarea class="form-control" rows="8" name="message" data-rule="required" data-msg="Por favor, escribe un mensaje"></textarea>
                            <div class="validation"></div>
                        </li>
                        <li>
                            <input type="submit" value="enviar mensaje" class="btn btn-default btn-block" name="Submit" />
                        </li>
                    </ul>
                    <p><small>*campos obligatorios</small></p>
                </form>
            </div>
        </div>
    </div>
</div>