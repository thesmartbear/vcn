<div class="col-xs-12">
    <div class="copyright" id="copy">
        <p>&copy; 2015 BRITISH SUMMER EXPERIENCES, S.L.  GC 003625 </p>
        <ul class="legal">
            <li><a href="/aviso-legal.html">AVISO LEGAL</a></li>
            <li><a href="/politica-de-cookies.html">POL&Iacute;TICA DE COOKIES</a></li>
        </ul>
    </div>
</div>