<div id="main-footer">
    <div>
        <div class="row">

            <div class="social-div">
                <div id="search-box">
                    <form action="/buscar-programas" method="get" enctype="multipart/form-data" class="searchbox" autocomplete="off">
                        <input type="text" placeholder="Buscar......" name="search" id="search" class="searchbox-input" required="">
                    </form>
                </div>
                <a class="btn-search"><i class="fa fa-search"></i></a>
                <ul class="social">
                    <li><a href="//twitter.com/cicviatges" target="_blank"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="btns-div">
                <a class="btn-contact" href="#"><span>CONTACTO</span></a>
                <a class="btn-clientes" href="//viatges.iccic.edu/clientes/"><span>ÁREA DE CLIENTES</span></a>
                <a class="btn-idioma" href="../"><span>CAT</span></a>
            </div>

            <ul class="secondary">
                <li><a href="/catalogo.html" class="white-txt">CATÁLOGO</a></li>
                <li><a href="/inscripcion.html">HOJAS DE INSCRIPCIÓN</a></li>
                <li class="separator"></li>
                <li><a href="/quienes-somos.html">QUIÉNES SOMOS</a></li>
                <li><a href="/trabaja-con-nosotros.html">TRABAJA CON NOSOTROS</a></li>
                <li><a href="/colaboradores.html">COLABORADORES</a></li>
                <li class="separator"></li>
                <li><a href="http://www.landedblog.com/iccic/" target="_blank" class="destacado"><strong>{!! trans('web.landedslogan') !!}</strong></a></li>
            </ul>
        </div>
    </div>

</div>

<div id="dropdown-contact" style="display: none;">

    <div class="container-fluid">
        <a href="#" class="contact-close">+</a>
        <div class="row addmargintop30">

            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="head oficinas">Oficinas
                            <span></span>
                        </h4>
                    </div>
                    <div class="col-sm-12 contactbig">
                        <p><strong>CIC escola idiomes</strong><br>
                            Vía Augusta, 205<br>
                            08021 Barcelona<br>
                            Tel. 93 200 11 33<br>
                            Fax 93 209 05 75<br>
                            <a href="mailto:viatges@iccic.edu">viatges@iccic.edu</a></p>
                    </div>

                    <div class="col-sm-12">
                        <h4 class="head oficinas addmargintop30">Horario
                            <span></span>
                        </h4>
                    </div>
                    <div class="col-sm-12 contactbig">
                        <p><strong>Horario otoño/invierno:</strong></p><p>Lunes a jueves de 10h a 14h y de 15:00h a 18:00h.</p><p>Viernes<strong>&nbsp;</strong>de 10 a 14h</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <h4 class="head">Contacto<span></span></h4>
                <form id="contactform" action="/contact.php" method="post" class="validateform" name="leaveContact">
                    <div id="sendmessage">
                        <div class="alert alert-info marginbot35">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>Tu mensaje ha sido enviado.</strong><br>
                            En breve nos pondremos en contacto contigo.
                            <br>Gracias por confiar en nosotros.
                        </div>
                    </div>
                    <ul class="listForm">
                        <li>
                            <label>Nombre <span>*</span></label>
                            <input class="form-control" type="text" name="name" data-rule="maxlen:4" data-msg="Tienes que introducir al menos 4 caracteres">
                            <div class="validation"></div>
                        </li>
                        <li>
                            <label>Email <span>*</span></label>
                            <input class="form-control" type="text" name="email" data-rule="email" data-msg="Por favor, introduce un email vÃ¡lido">
                            <div class="validation"></div>
                        </li>
                        <li>
                            <label>Mensaje <span>*</span></label>
                            <textarea class="form-control" rows="8" name="message" data-rule="required" data-msg="Por favor, escribe un mensaje"></textarea>
                            <div class="validation"></div>
                        </li>
                        <li>
                            <input type="submit" value="enviar mensaje" class="btn btn-default btn-block" name="Submit">
                        </li>
                    </ul>
                    <p><small>*campos obligatorios</small></p>
                </form>
            </div>
        </div>
    </div>
</div>