<div id="inside-menu" style="display: none;">
    <div class="container-fluid">
        <div class="row addmargintop30">
            <div class="col-md-12 text-center">
                <i class="fa fa-times" id="menu-close"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <div class="col">
                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/summer-camps-ingles"><h5 style="color: #8e44ad;">CAMPAMENTOS DE VERANO <span>en ingl&eacute;s</span></h5></a>
                            <h6>Summer Camps. De 6 a 15 a&ntilde;os</h6>
                        </div>
                        <ul>
                            @foreach(\VCN\Models\Categoria::where('slug','summer-camps-ingles')->where(function ($query) {
                                    return $query
                                        ->where('propietario', 0)
                                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                                    })->get() as $categoria)
                                @foreach($categoria->subcategorias as $sc)
                                    <li><a href="{{$categoria->slug}}/{{$sc->slug}}">{{$sc->name_web}}</a></li>
                                @endforeach
                            @endforeach
                        </ul>
                        <p class="addmargintop20"><a href="/summer-camps-ingles"><strong>¿QUÉ ES UNA COLONIA CIC EN CATALUÑA?</strong></a></p>
                    </div>
                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/summer-camps-ingles"><h5 style="color: #8e44ad;">DAY CAMP <span>en ingl&eacute;s</span></h5></a>
                            <h6>De 10 a 13 a&ntilde;os</h6>
                        </div>
                        <ul>
                            <li><a href="/summer-camps-ingles/active-english.html">ACTIVE ENGLISH</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="col">
                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/aprender-ingles-en-el-extranjero"><h5 style="color: #1abc9c;">APRENDE INGL&Eacute;S EN EL EXTRANJERO</h5></a>
                            <h6>Todas las edades</h6>
                        </div>
                        <ul>
                            <li><a href="/aprender-ingles-en-el-extranjero/jovenes/grupos-con-monitor/">PROGRAMAS DE VERANO CON MONITOR</a></li>
                            <li><a href="/aprender-ingles-en-el-extranjero/jovenes/cursos-especiales/">CURSOS MUY SINGULARES/ESPECIALIZADOS</a></li>
                            <li><a href="/aprender-ingles-en-el-extranjero/adultos-profesionales/">ADULTOS Y PROFESIONALES</a></li>
                            <li><a href="/aprender-ingles-en-el-extranjero/toda-la-familia/">TODA LA FAMILIA</a></li>

                        </ul>
                    </div>


                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/aprender-otros-idiomas-en-el-extranjero"><h5 style="color: #888535;">OTROS IDIOMAS EN EL EXTRANJERO</h5></a>
                            <h6>Todas las edades</h6>
                        </div>
                        <ul>
                            <li><a href="/aprender-otros-idiomas-en-el-extranjero/jovenes/">De 8 a 18 a&ntilde;os</a></li>
                            <li><a href="/aprender-otros-idiomas-en-el-extranjero/adultos-profesionales/">De 18 a 99 a&ntilde;os</a></li>
                            <li><a href="/aprender-otros-idiomas-en-el-extranjero/toda-la-familia/">TODA LA FAMILIA</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-xs-4">
                <div class="col">
                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/curso-escolar-en-el-extranjero"><h5 style="color: #d35400;">A&Ntilde;O ESCOLAR</h5></a>
                            <h6>Primaria, Secundaria y Bachillerato</h6>
                        </div>

                        <ul>
                            <li><a href="/curso-escolar-en-el-extranjero/curso-escolar/">A&ntilde;o Escolar</a></li>
                            <li><a href="/curso-escolar-en-el-extranjero/semestre-escolar/">Semestre Escolar</a></li>
                            <li><a href="/curso-escolar-en-el-extranjero/trimestre-escolar/">Trimestre Escolar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>