@extends('web.britishsummer.baseweb')

@section('container')

    Listado categorias:

    <ul>
    @foreach(\VCN\Models\Categoria::all() as $categoria)
        <li><strong>{{$categoria->name}}</strong>
            <ul>
            @foreach($categoria->subcategorias as $sc)
                <li><strong>{{$sc->name}}</strong></li>
                    <ul>
                        @foreach($sc->subcategoriasdet as $scd)
                            <li><strong>{{$scd->name}}</strong></li>
                        @endforeach
                    </ul>
            @endforeach
            </ul>
        </li>
    @endforeach
    </ul>



    Filtrando Categorias Por propietario (0 y 1):
    <ul>
        @foreach(\VCN\Models\Categoria::where('propietario',0)->orWhere('propietario',ConfigHelper::config('propietario'))->get() as $categoria)
            <li><strong>{{$categoria->name}}</strong>
                <ul>
                @foreach($categoria->subcategorias as $sc)
                     <li><strong>{{$sc->name}}</strong></li>
                         <ul>
                             @foreach($sc->subcategoriasdet as $scd)
                                    <li><strong>{{$scd->name}}</strong></li>
                                @endforeach
                        </ul>
                    @endforeach
                </ul>
            </li>
        @endforeach
    </ul>



@stop
