@extends('web.britishsummer.baseweb')

@section('extra_head')
    <!-- Custom style -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/style.css" rel="stylesheet">
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/contents.css" rel="stylesheet">

    <!-- Color style -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/{{$clase}}.css" rel="stylesheet">

    <script type="text/javascript">
    var $url = "{{route('ajax.info', $curso->id)}}";
    </script>

    {!! Html::style('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') !!}
@stop

@include('web.britishsummer.includes.menu'.ConfigHelper::config('sufijo').'-'.App::getLocale())
<div id="menubg">
    <div class="inside">
        <div class="text">
            <i class="fa fa-navicon" id="main-menu"></i>
        </div>
    </div>
</div>
<div id="menu"></div>

@section('container')

    <?php
    $fotoscentro = '';
    $fotoscentroname = array();
    $path = public_path() ."/assets/uploads/center/" . $curso->centro->center_images;
    $folder = "/assets/uploads/center/" . $curso->centro->center_images;

    if (is_dir($path)) {
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

            $file = $path . '/' . $result;

            if (is_file($file)) {
                $fotoscentro .= '

                        <div class="col-md-4">
                            <a rel="group" href="' . $folder . '/' . $result . '"><div style="width: 100%; height: 100%; display: block; overflow: hidden;"><img class="img-responsive" src="' . $folder . '/thumb/' . $result . '" alt=""></div></a>
                        </div>';
                $fotoscentroname[] = $result;

            }
        }
    }
    ?>
    <?php
    $fotoscurso = '';
    $fotoscursoname = array();
    $path = public_path() ."/assets/uploads/course/" . $curso->course_images;
    $folder = "/assets/uploads/course/" . $curso->course_images;

    if (is_dir($path)) {
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

            $file = $path . '/' . $result;

            if (is_file($file)) {
                $fotoscurso .= '
                    <div class="col-md-4">
                        <a rel="group" href="' . $folder . '/' . $result . '"><div style="position:relative; overflow:hidden; padding-bottom:100%;"><img class="img-responsive full-width" style="position: absolute;" src="' . $folder . '/thumb/' . $result . '" alt=""></div></a>
                    </div>';
                $fotoscursoname[] = $result;
            }
        }
    }
    ?>

<div class="curso">
    <section id="wrapper">
        <div class="max-width" id="allcontent">
            @if ($curso->course_promo == 1)
            <div id="promocion">
                <div class="icon"></div>
                {!!$curso->promo_texto!!}
            </div>
            @endif


            <!-- Start info header -->
            <div class="row">
                <div class="col-xs-12 wrapper-bg" id="header">
                    <article>
                        @if(is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada))
                            <div class="intro-img col-xs-2 col-sm-2 col-lg-4 pull-right" style="background-image: url(/assets/uploads/course/{{$curso->course_images}}/{{$curso->image_portada}});" id="intro-img">
                        @elseif(is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                            <div class="intro-img col-xs-2 col-sm-2 col-lg-4 pull-right" style="background-image: url(/assets/uploads/center/{{$curso->centro->center_images}}/{{$curso->centro->center_image_portada}});" id="intro-img">
                        @elseif(!is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                            @if(count($fotoscursoname))
                                <div class="intro-img col-xs-2 col-sm-2 col-lg-4 pull-right" style="background-image: url(/assets/uploads/course/{{$curso->course_images}}/{{$fotoscursoname[rand(0,count($fotoscursoname)-1)]}});" id="intro-img">
                            @elseif(!count($fotoscursoname) && count($fotoscentroname))
                                <div class="intro-img col-xs-2 col-sm-2 col-lg-4 pull-right" style="background-image: url(/assets/uploads/center/{{$curso->centro->center_images}}/{{$fotoscentroname[rand(0,count($fotoscentroname)-1)]}});" id="intro-img">
                            @else
                                <div class="intro-img col-xs-2 col-sm-2 col-lg-4 pull-right" style="background: #E5E5E5 url(/assets/britishsummer/img/patterns/{{rand(1,3)}}.png) repeat;" id="intro-img">
                            @endif
                        @endif

                        </div>

                        <div class="intro col-xs-10 col-sm-10 col-lg-8 pull-right">
                            <div class="titulo">
                                <div id="logo"><a href="/"><img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" /></a></div>
                                <h1 class="slogan" id="slogan">
                                    @if($categoria === trans('web.categorias.ingles-slug'))
                                        <span>
                                            <ul class="breadcrumb">
                                                <li><a href="/{{trans('web.categorias.ingles-slug')}}">{{trans('web.categorias.ingles')}}</a></li>
                                                <li class="active"><a href="/{{trans('web.categorias.ingles-slug')}}/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $curso->categoria->id, $curso->categoria->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $curso->categoria->id, $curso->categoria->name_web)!!}</a></li>

                                            </ul>
                                        </span>
                                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'name', $curso->id, $curso->name)!!}<br />
                                        {!! $curso->subcategoria_detalle ? '<small><a href="/'.trans('web.categorias.ingles-slug').'/'.Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $curso->categoria->id, $curso->categoria->slug).'/'.Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $curso->subcategoria->id, $curso->subcategoria->slug).'">' . Traductor::getWeb(App::getLocale(), 'SubcategoriaDetalle', 'name_web', $curso->subcategoria_detalle->id, $curso->subcategoria_detalle->name_web) . '</a></small>' : '' !!}
                                    @elseif($categoria === trans('web.categorias.idiomas-slug'))
                                        <span>
                                            <ul class="breadcrumb">
                                                <li><a href="/{{trans('web.categorias.idiomas-slug')}}">{{trans('web.categorias.idiomas')}}</a></li>
                                                <li class="active"><a href="/{{trans('web.categorias.otrosidiomas-slug')}}/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $curso->categoria->id, $curso->categoria->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $curso->categoria->id, $curso->categoria->name_web)!!}</a></li>
                                            </ul>
                                        </span>
                                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'name', $curso->id, $curso->name)!!}<br />
                                        {!! $curso->subcategoria_detalle ? '<small><a href="/'.trans('web.categorias.idiomas-slug').'/'.Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $curso->categoria->id, $curso->categoria->slug).'/'.Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $curso->subcategoria->id, $curso->subcategoria->slug).'">' . Traductor::getWeb(App::getLocale(), 'SubcategoriaDetalle', 'name_web', $curso->subcategoria_detalle->id, $curso->subcategoria_detalle->name_web) . '</a></small>' : '' !!}
                                    @else
                                        <span>
                                            <ul class="breadcrumb">
                                                <li><a href="/{{$curso->categoria->slug}}">{{$curso->categoria->name_web}}</a></li>
                                                <li class="active"><a href="/{{$curso->categoria->slug}}/{{$curso->subcategoria->slug}}">{{$curso->subcategoria->name_web}}</a></li>
                                            </ul>
                                        </span>
                                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'name', $curso->id, $curso->name)!!}<br />
                                        {!! $curso->subcategoria_detalle ? '<small><a href="/'.Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $curso->categoria->id, $curso->categoria->slug).'/'.Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $curso->subcategoria->id,$curso->subcategoria->slug).'/'.Traductor::getWeb(App::getLocale(), 'SubcategoriaDetalle', 'slug', $curso->subcategoria_detalle->id, $curso->subcategoria_detalle->slug).'">'. Traductor::getWeb(App::getLocale(), 'SubcategoriaDetalle', 'name_web', $curso->subcategoria_detalle->id, $curso->subcategoria_detalle->name_web) . '</a></small>' : '' !!}
                                    @endif

                                </h1>

                                <!-- Nav tabs -->
                                <h4>
                                    <ul class="nav nav-tabs" role="tablist" id="cursotabs">
                                        @if($curso->course_summary != '' || $curso->course_summary != null || $curso->course_content != '' || $curso->course_content != null  || $curso->frase != '' || $curso->frase != null)
                                            <li class="active"><a href="#{!! trans('web.curso.programa') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.programa') !!}</a></li>
                                        @endif
                                        @if($curso->centro->name != '' || $curso->centro->name != null || $curso->centro->description != '' || $curso->centro->description != null || $curso->centro->comidas != '' || $curso->centro->comidas != null || $curso->centro->center_video != '' || $curso->centro->center_video != null)
                                            <li><a href="#{!! trans('web.curso.centro') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.centro') !!}</a></li>
                                        @endif
                                        @if(count($curso->alojamientos))
                                            <li><a href="#{!! trans('web.curso.alojamiento') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.alojamiento') !!}</a></li>
                                        @endif
                                        @if($curso->course_activities != null || $curso->course_activities != '' || $curso->course_excursions != '' || $curso->course_excursions != null || $curso->centro->center_excursions != '' || $curso->center_excursions != null || $curso->centro->center_activities != '' || $curso->centro->center_activities != null)
                                            <li><a href="#{!! trans('web.curso.actividades') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.actividades') !!}</a></li>
                                        @endif
                                            <li><a href="#{!! trans('web.curso.fechas-precios') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.fechasyprecios') !!}</a></li>
                                        @if ($fotoscentro != '' || $fotoscurso != '')
                                            <li><a href="#{!! trans('web.curso.fotos') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.fotos') !!}</a></li>
                                        @endif
                                    </ul>
                                </h4>



                            </div>
                        </div>

                    </article>
                </div>
            </div>
            <!-- End info header -->




            <div class="container" id="contenido">

                @include('web.britishsummer.includes.plusinfo')

                <div class="row">
                    <div class="col-sm-6 col-xs-12 addmargintop60 referencia">
                        <div id="infocurso">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                @if($curso->course_summary != '' || $curso->course_summary != null || $curso->course_content != '' || $curso->course_content != null  || $curso->frase != '' || $curso->frase != null)
                                    <div class="tab-pane active" id="{!! trans('web.curso.programa') !!}">

                                    @if ($curso->course_summary != '' || $curso->course_summary != null)
                                        <div class="introduccion">
                                            {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_summary', $curso->id, $curso->course_summary)!!}
                                        </div>
                                    @endif

                                    @if ($curso->frase != '' || $curso->frase != null)
                                        <div class="info-general">
                                            {!!Traductor::getWeb(App::getLocale(), 'Curso', 'frase', $curso->id, $curso->frase)!!}
                                        </div>
                                    @endif

                                    @if ($curso->course_content != '' || $curso->course_content != null)
                                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_content', $curso->id, $curso->course_content)!!}
                                    @endif

                                    @if ($curso->course_language != '' || $curso->course_language != null)
                                        <p><strong>{!!trans('web.curso.idiomarequerido')!!}:</strong> {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_language', $curso->id, $curso->course_language)!!}</p>
                                    @endif

                                    @if ($curso->course_minimun_language != '' || $curso->course_minimun_language != null)
                                        <p><strong>{!!trans('web.curso.nivel')!!}:</strong> {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_minimun_language', $curso->id, $curso->course_minimun_language)!!}</p>
                                    @endif

                                    @if ($curso->course_language_sessions != '' || $curso->course_language_sessions != null)
                                        <p><strong>{!!trans('web.curso.sesiones')!!}:</strong> {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_language_sessions', $curso->id, $curso->course_language_sessions)!!}</p>
                                    @endif



                                            <?php
                                            $path = public_path() ."/assets/uploads/pdf/" . $curso->course_images;
                                            $folder = "/assets/uploads/pdf/" . $curso->course_images;

                                            if(is_dir($path))
                                            {
                                                echo '<h6>'.trans('web.curso.timetable').'</h6>';
                                                $results = scandir($path);
                                                foreach ($results as $result) {
                                                    if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0]==='.') continue;

                                                    $file = $path . '/' . $result;

                                                    if( is_file($file) )
                                                    {
                                                        echo '<a href="'.$folder . '/' . $result.'" target="_blank"><i class="fa fa-download"></i> '.$result.'</a>';
                                                    }
                                                }
                                            }
                                            ?>

                                            @if($curso->course_video != '' || $curso->centro->course_video != null)
                                                <div class="videowrapper well">
                                                    <iframe height="300" width="500" src="//www.youtube.com/embed/{{$curso->course_video}}" frameborder="0" allowfullscreen=""></iframe>
                                                </div>
                                            @endif
                                        @if ($curso->course_provider_url != '' || $curso->course_provider_url != null)
                                            <hr>
                                            <p><i class="fa fa-info-circle"></i> {!! trans('web.curso.masinfoproveedor1') !!} <a href="{!!$curso->course_provider_url!!}" target="_blank">{!! trans('web.curso.masinfoproveedor2') !!}</a></p>
                                        @endif
                                    </div>
                                @endif

                                @if($curso->centro->name != '' || $curso->centro->name != null || $curso->centro->description != '' || $curso->centro->description != null || $curso->centro->comidas != '' || $curso->centro->comidas != null || $curso->centro->center_video != '' || $curso->centro->center_video != null)
                                    <div class="tab-pane" id="{!! trans('web.curso.centro') !!}">
                                        <h4>{!!Traductor::getWeb(App::getLocale(), 'Centro', 'name', $curso->centro->id, $curso->centro->name)!!}<br />
                                            <small><b>{!!Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name)!!}</b>. {!!Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name)!!}</small>
                                        </h4>
                                        @if($curso->centro->description != '' || $curso->centro->description != null)
                                            {!!Traductor::getWeb(App::getLocale(), 'Centro', 'description', $curso->centro->id, $curso->centro->description)!!}
                                        @endif
                                        @if($curso->centro->transport != '' || $curso->centro->transport != null)
                                            <h3>{!! trans('web.curso.transporte') !!}</h3>
                                            {!!Traductor::getWeb(App::getLocale(), 'Centro', 'transport', $curso->centro->id, $curso->centro->transport)!!}
                                        @endif
                                        @if($curso->centro->comidas != '' || $curso->centro->comidas != null)
                                            <h3>{!! trans('web.curso.comidas') !!}</h3>
                                            {!!Traductor::getWeb(App::getLocale(), 'Centro', 'comidas', $curso->centro->id, $curso->centro->comidas)!!}
                                        @endif
                                        @if($curso->centro->center_video != '' || $curso->centro->center_video != null)
                                            <div class="videowrapper well">
                                                <iframe height="300" width="500" src="//www.youtube.com/embed/{{$curso->centro->center_video}}" frameborder="0" allowfullscreen=""></iframe>
                                            </div>
                                        @endif
                                        @if($curso->centro->address != '' || $curso->centro->address != null)
                                            <h3>{!!Traductor::getWeb(App::getLocale(), 'Centro', 'address', $curso->centro->id, $curso->centro->address)!!}</h3>
                                            <iframe style="margin-top: 30px;"
                                                    width="100%"
                                                    height="450"
                                                    frameborder="0" style="border:0"
                                                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCNPWo91Kd3D656Z5R2U6K1vFlx65KxpE8
    &q={{strip_tags($curso->centro->address)}}&language={{App::getLocale()}}&zoom=13" allowfullscreen>
                                            </iframe>
                                 @endif
                                 </div>
                             @endif

                                    @if(count($curso->alojamientos))
                                    <div class="tab-pane" id="{!! trans('web.curso.alojamiento') !!}">
                                        @foreach($curso->alojamientos as $alojamiento)
                                            <h4>{!!Traductor::getWeb(App::getLocale(), 'Alojamiento', 'name', $alojamiento->id, $alojamiento->name)!!}</h4>
                                            <p>{!!Traductor::getWeb(App::getLocale(), 'Alojamiento', 'accommodation_description', $alojamiento->id, $alojamiento->accommodation_description)!!}</p>
                                        @endforeach
                                    </div>
                                    @endif

                                    @if($curso->course_activities != null || $curso->course_activities != '' || $curso->course_excursions != '' || $curso->course_excursions != null || $curso->centro->center_excursions != '' || $curso->center_excursions != null || $curso->centro->center_activities != '' || $curso->centro->center_activities != null)
                                    <div class="tab-pane" id="{!! trans('web.curso.actividades') !!}">
                                        @if($curso->course_activities != null || $curso->course_activities != '' || $curso->centro->center_activities != null || $curso->centro->center_activities != '')
                                            <h3>{!! trans('web.curso.actividades') !!}</h3>
                                            @if($curso->course_activities != null || $curso->course_activities != '')
                                                {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_activities', $curso->id, $curso->course_activities)!!}
                                            @elseif(($curso->course_activities == null || $curso->course_activities == '') && ($curso->centro->center_activities != null || $curso->centro->center_activities != ''))
                                                {!!Traductor::getWeb(App::getLocale(), 'Centro', 'center_activities', $curso->centro->id, $curso->centro->center_activities)!!}
                                            @endif
                                        @endif
                                        @if($curso->course_excursions != null || $curso->course_excursions != '' || $curso->centro->center_excursions != null || $curso->centro->center_excursions != '')
                                            <h3>{!! trans('web.curso.excursiones') !!}</h3>
                                            @if($curso->course_excursions != null || $curso->course_excursions != '')
                                                    {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_excursions', $curso->id, $curso->course_excursions)!!}
                                            @elseif(($curso->course_excursions == null || $curso->course_excursions == '') && ($curso->centro->center_excursions != null || $curso->centro->center_excursions != ''))
                                                    {!!Traductor::getWeb(App::getLocale(), 'Centro', 'center_excursions', $curso->centro->id, $curso->centro->center_excursions)!!}
                                            @endif
                                        @endif
                                    </div>
                                    @endif



                                    <div class="tab-pane" id="{!! trans('web.curso.fechas-precios') !!}">
                                        {{-- convocatorias abiertas --}}
                                        {{-- precio curso: fecha + duracion => fecha_fin y precio --}}

                                        {{-- OJO CON VARIAS CONVOCATORIAS ABIERTAS A LA VEZ (se supone que no debe ser) PERO HABRÍA QUE CAMBIAR EL SCRIPT --}}

                                        @if(count($curso->convocatoriasAbiertas))
                                            @foreach($curso->convocatoriasAbiertas as $ca)
                                                @if($ca->convocatory_open_status == 1)
                                                    <div class="col-sm-12"><h6 class="separator">{!! trans('web.curso.curso') !!}</h6></div>

                                                        <script type="text/javascript">
                                                            var $startDate = "{{$ca->convocatory_open_valid_start_date}}";
                                                            var $endDate = "{{$ca->convocatory_open_valid_end_date}}";
                                                            var $dayDate = "{{$ca->convocatory_open_start_day}}";
                                                        </script>

                                                        <table class="table tabla-precios">
                                                            <thead>
                                                            <tr class="thead">
                                                                <td>{!! trans('web.curso.finicio') !!}</td>
                                                                <td>{!! trans('web.curso.duracion') !!}</td>
                                                                <td>{!! trans('web.curso.ffin') !!}</td>
                                                                <td align="right">{!! trans('web.curso.precio') !!}</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>

                                                                <td width="45%">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                        {!! Form::text('booking-cabierta-fecha_ini', '', array( 'id'=>'booking-cabierta-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'datetime form-control')) !!}
                                                                    </div>
                                                                </td>
                                                                <td width="20%">
                                                                    {!! Form::select('booking-cabierta-semanas', ConfigHelper::getSemanas($curso->duracion_name), 0, array( 'id'=> 'booking-cabierta-semanas', 'class' => 'form-control')) !!}
                                                                </td>
                                                                <td width="15%">
                                                                    {!! Form::text('booking-cabierta-fecha_fin', '', array( 'id'=>'booking-cabierta-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
                                                                </td>
                                                                <td width="20%" align='right'>
                                                                    <span class="booking-cabierta-precio" id="booking-cabierta-precio"></span>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                        <div class="col-sm-12"><h6 class="separator">{!! trans('web.curso.alojamiento') !!}</h6></div>

                                                        {{-- precio alojamiento: fecha + duracion => fecha_fin y precio --}}
                                                        <table class="table tabla-precios">
                                                            <thead>
                                                            <tr class="thead">
                                                                <td>{!! trans('web.curso.alojamiento') !!}</td>
                                                                <td>{!! trans('web.curso.finicio') !!}</td>
                                                                <td>{!! trans('web.curso.duracion') !!}</td>
                                                                <td>{!! trans('web.curso.ffin') !!}</td>
                                                                <td align="right">{!! trans('web.curso.precio') !!}</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td width="20%">
                                                                    {!! Form::select('booking-alojamiento', $curso->alojamientos->pluck('name','id'), 0, array( 'id'=> 'booking-alojamiento', 'class' => 'form-control')) !!}
                                                                </td>
                                                                <td width="25%">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                        {!! Form::text('booking-alojamiento-fecha_ini', '', array( 'id'=>'booking-alojamiento-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'datetime form-control')) !!}
                                                                    </div>
                                                                </td>
                                                                <td width="20%">
                                                                    {!! Form::select('booking-alojamiento-semanas', ConfigHelper::getSemanas($curso->duracion_name), 0, array( 'id'=> 'booking-alojamiento-semanas', 'class' => 'form-control')) !!}
                                                                </td>
                                                                <td width="15%">
                                                                    {!! Form::text('booking-alojamiento-fecha_fin', '', array( 'id'=>'booking-alojamiento-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
                                                                </td>
                                                                <td width="20%" align='right'>
                                                                    <span id="booking-alojamiento-precio"></span>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                        <h4 class="separator">{!!trans('web.curso.precioincluye')!!}</h4>

                                                        <ul class="incluye">
                                                            @foreach($ca->incluyes as $cai)
                                                                @if($cai->incluye->tipo == 0)
                                                                    <li>{{$cai->incluye->name}}</li>
                                                                @elseif($cai->incluye->tipo == 1 && $cai->valor != 0)
                                                                    <li>{{$cai->valor}} {{$cai->incluye->name}}</li>
                                                                @endif
                                                            @endforeach
                                                            @if($ca->incluye_horario != '')<li>{!! trans('web.curso.incluyeclasesidioma') !!} {!!$ca->incluye_horario!!}</li>@endif
                                                            @if($ca->convocatory_open_price_include != ''){!! strip_tags($ca->convocatory_open_price_include,'<p><li><a><b><strong><em>') !!}@endif
                                                        </ul>
                                                @endif
                                            @endforeach
                                        @endif

                                        @if(count($curso->convocatoriasCerradas))
                                            <div class="row">
                                            @foreach($curso->alojamientos as $aloja)
                                                <div class="col-sm-12"><h6 class="separator">{{$aloja->name}}</h6></div>
                                                @foreach($curso->convocatoriasCerradas->sortBy('convocatory_close_start_date') as $cc)
                                                    @if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 0)
                                                        <div class="col-sm-4 precio">
                                                            <h5>{{date('d.m.Y', strtotime($cc->convocatory_close_start_date))}} <span>{!! trans('web.curso.al') !!}</span> {{date('d.m.Y', strtotime($cc->convocatory_close_end_date))}}</h5>
                                                            <p class="duracion">{{$cc->convocatory_close_duration_weeks}} {{ ConfigHelper::getPrecioDuracionUnit($cc->duracion_fijo) }}</p>
                                                            @if($cc->dto_early == null || $cc->dto_early == '')
                                                                <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda->name) }}</b></p>
                                                            @else
                                                                @if((strtotime(date('Y-m-d')) >= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->desde)) && (strtotime(date('Y-m-d')) <= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->hasta)))
                                                                    <p><s>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda->name) }}</s></p>
                                                                    <p><img class="earlybird" src="/assets/britishsummer/img/earlybird.png"> <b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price-(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->importe), \VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->moneda->name) }}</b></p>
                                                                @else
                                                                    <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda->name) }}</b></p>
                                                                @endif
                                                            @endif
                                                            <a class="verincluye collapsed" data-toggle="collapse" data-target="#incluye-{{$cc->id}}" aria-expanded="false" aria-controls="collapseExample">
                                                                {!!trans('web.curso.precioincluye')!!}
                                                            </a>
                                                            <div class="collapse" id="incluye-{{$cc->id}}">
                                                                <ul class="incluye">
                                                                    @foreach($cc->incluyes as $cci)
                                                                        @if($cci->incluye->tipo == 0)
                                                                            <li>{{$cci->incluye->name}}</li>
                                                                        @elseif($cci->incluye->tipo == 1 && $cci->valor != 0)
                                                                            <li>{{$cci->valor}} {{$cci->incluye->name}}</li>
                                                                        @endif
                                                                    @endforeach
                                                                    @if($cc->incluye_horario != '')<li>{!! trans('web.curso.incluyeclasesidioma') !!} {!!$cc->incluye_horario!!}</li>@endif
                                                                    @if($cc->convocatory_close_price_include != ''){!! strip_tags($cc->convocatory_close_price_include,'<p><li><a><b><strong><em>') !!}@endif
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            </div>
                                        @endif

                                        @if($curso->convocatoriasCerradas->contains('convocatory_semiopen', 1))
                                            <div class="row">
                                            <h4 class="separator">{!! trans('web.curso.planb') !!}</h4>
                                            @foreach($curso->convocatoriasCerradas as $cc)
                                                @if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 1)
                                                    <div class="col-sm-6">
                                                        @if($cc->alojamiento_id != '' || $cc->alojamiento_id != 0)<div class="col-sm-12"><h6>{{\VCN\Models\Alojamientos\Alojamiento::find($cc->alojamiento_id)->name}}</h6></div>@endif
                                                        <h5><span>{!! trans('web.curso.entre') !!}</span> {{date('d.m.Y', strtotime($cc->convocatory_close_start_date))}} <span>{!! trans('web.curso.yel') !!}</span> {{date('d.m.Y', strtotime($cc->convocatory_close_end_date))}}</h5>
                                                        <p class="duracion">{{$cc->convocatory_close_duration_weeks}} {{ ConfigHelper::getPrecioDuracionUnit($cc->duracion_fijo) }}</p>
                                                        <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda->name) }}</b></p>
                                                        <a class="verincluye collapsed" data-toggle="collapse" data-target="#incluye-{{$cc->id}}" aria-expanded="false" aria-controls="collapseExample">
                                                            {!!trans('web.curso.precioincluye')!!}
                                                        </a>
                                                        <div class="collapse" id="incluye-{{$cc->id}}">
                                                            <ul class="incluye">
                                                                @foreach($cc->incluyes as $cci)
                                                                    @if($cci->incluye->tipo == 0)
                                                                        <li>{{$cci->incluye->name}}</li>
                                                                    @elseif($cci->incluye->tipo == 1 && $cci->valor != 0)
                                                                        <li>{{$cci->valor}} {{$cci->incluye->name}}</li>
                                                                    @endif
                                                                @endforeach
                                                                @if($cc->incluye_horario != '')<li>{!! trans('web.curso.incluyeclasesidioma') !!} {!!$cc->incluye_horario!!}</li>@endif
                                                                @if($cc->convocatory_close_price_include != ''){!! strip_tags($cc->convocatory_close_price_include,'<p><li><a><b><strong><em>') !!}@endif
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endif

                                            @endforeach
                                            </div>
                                        @endif

                                        @if(count($curso->extras) || $curso->extrasGenericos)
                                            <div class="row">
                                                <h4 class="separator">{!! trans('web.curso.extras') !!}</h4>
                                                @foreach($curso->extras as $e)
                                                    <h6>{{$e->course_extras_name}}</h6>
                                                    <p>{{$e->course_extras_description}}</p>
                                                    @if($e->course_extras_price > 0)
                                                        <p><b>{{ ConfigHelper::parseMoneda($e->course_extras_price, $e->moneda) }}@if($e->course_extras_unit_id == 1) / {{$e->unidad}}@endif</b></p>
                                                    @else
                                                        <p><b>{!! trans('web.curso.incluido') !!}</b></p>
                                                    @endif
                                                @endforeach
                                                @foreach($curso->extrasGenericos as $eg)
                                                    <h6>{{$eg->name}}</h6>
                                                    @if($eg->precio > 0)
                                                        <p><b>{{ ConfigHelper::parseMoneda($eg->precio, $eg->moneda) }}@if($eg->generico->generic_unit_id == 1) / {{trans_choice('web.curso.'.$eg->generico->tipo->name, 1)}} @endif</b></p>
                                                    @else
                                                        <p><b>{!! trans('web.curso.incluido') !!}</b></p>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>


                                    @if($fotoscentro != '' || $fotoscurso != '')
                                        <div class="tab-pane" id="{!! trans('web.curso.fotos') !!}">
                                            <div id="fotos-curso" class="hidden-print">
                                                <div class="row">
                                                    {!! $fotoscentro !!}
                                                    {!! $fotoscurso !!}
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                        @if($curso->course_video != '' || $curso->centro->course_video != null)
                                            <div class="videowrapper well">
                                                <iframe height="300" width="500" src="//www.youtube.com/embed/{{$curso->course_video}}" frameborder="0" allowfullscreen=""></iframe>
                                            </div>
                                        @endif

                            </div>
                            <!-- end tabs -->
                        </div>
                    </div>
                    <!-- Start right sidebar -->
                    <div class="col-sm-2 col-lg-3 col-sm-offset-1 wrapper-bg addmargintop60" id="sidebar">
                        <div class="box">
                                <div class="widget clearfix info-basica">
                                    <h5 class="head">{!! trans('web.curso.info-basica') !!}<span></span></h5>

                                    <div id="intro-curso">

                                        <div id="details">
                                            <ul>
                                                <li>
                                                    <i class="fa fa-group"></i> {!! $curso->course_age_range !!}
                                                </li>
                                                <li><i class="fa fa-flag"></i> {!!$curso->course_language!!}</li>
                                                @foreach($curso->alojamientos as $alojamiento)
                                                    <? $alojas[] = $alojamiento->tipo->accommodation_type_name; ?>
                                                @endforeach
                                                @foreach(array_unique($alojas) as $alojatipo)
                                                    <li><i class="fa fa-home"></i> {!!$alojatipo!!}</li>
                                                @endforeach
                                            </ul>

                                            @if ($curso->centro->internet == 1)
                                                <div class="internet">
                                                    <p><i class="fa fa-laptop fa-2x"></i> {!! trans('web.curso.internet-disponible') !!}</p>
                                                </div>
                                            @endif

                                            @if ($curso->centro->comentarios != '' || $curso->centro->comentarios != null)
                                                <div class="internetopciones">
                                                    <p>{!!$curso->centro->comentarios!!}</p>
                                                </div>
                                            @endif
                                        </div>


                                        <div id="utils">
                                            <ul>
                                                <li><i class="fa fa-edit"></i> <a href="" target="_blank">{!! trans('web.curso.inscripcion') !!}</a>
                                                </li>
                                                <li><i class="fa fa-book"></i> <a href="" target="_blank">{!! trans('web.curso.catalogo') !!}</a>
                                                </li>
                                                <li><i class="fa fa-lightbulb-o"></i> <a href="">{!! trans('web.curso.faq') !!}</a></li>
                                                <li><i class="fa fa-file-pdf-o"></i> <a href="">{!! trans('web.curso.descargar') !!}</a></li>
                                            </ul>
                                        </div>


                                        <a href="#plusinfomodal" class="btn btn-primary plusinfolink pibtn hidden-print"
                                           data-toggle="modal" data-target="#plusinfomodal">{!! trans('web.curso.reservar') !!}</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- End right sidebar -->


                    @include('web.britishsummer.includes.copyright')

                </div>

            </div>

        </div>
    </section>
</div>

<!-- Modal -->
<div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
            </div>
            <div class="modal-body">
                <div id="respuesta">
                    <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                    <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                        <div class="msg"></div>
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="form-group">
                                    <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                    <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                    <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                    <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                    <input type="hidden" id="curso" value="{{$curso->name}}">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});">Enviar</button>

                <p class="text-center"><br /><small><a href="/proteccion-de-datos.html" target="_blank">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</a></small></p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('extra_footer')
<!-- contenidos -->
<script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/contenidos.js"></script>

{!! Html::script('assets/js/manage-web.js') !!}
{!! Html::script('assets/plugins/moment-with-locales.min.js') !!}
{!! Html::script('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}

<script type="text/javascript">
    $(document).ready(function () {
        $('.msg').hide();
        $("#plusinfoenviar").click(function () {
            //console.log('validar');
            if ($('#name').val() == '') {
                $('.msg').html('Debes indicar un nombre de contacto');
                $('.msg').show();
                return false;
            }
            if ($('#email').val() == '') {
                $('.msg').html('Debes indicar un teléfono o un email de contacto');
                $('.msg').show();
                return false;
            }

            if ($('#email').val() != '') {
                var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
                if (re.test($('#name').val())) {
                    $('.msg').html('El email no tiene un formato correcto');
                    $('.msg').show();
                    return false
                }

            }

            if (!/^([0-9])*$/.test($('#tel').val())) {
                $('.msg').html('El campo teléfono tiene que ser numérico');
                $('.msg').show();
                return false
            }


            post_data = {
                'name': $('#name').val(),
                'tel': $('#tel').val(),
                'email': $('#email').val(),
                'curso': $('#curso').val()
            };
            $.ajax({
                type: "POST",
                url: "/plusinfosend.php",
                data: post_data,
                success: function (msg) {
                    console.log(msg);
                    $("#respuesta").html(msg);
                    $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                    ga('send', 'Solicitud', 'button', 'click', '{!!$curso->name!!}', 1);
                },
                error: function () {
                    alert("error!!");
                }
            });
        });

    });

    $('form input').blur(function () {
        $('.msg').hide();
    });

    $('#fotos-curso').magnificPopup({
        delegate: 'a', // child items selector, by clicking on it popup will open
        type: 'image',
        gallery: {
            enabled: true
        },
        titleSrc: 'title'
    });


    $(function () {
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');

        $('.nav-tabs a').click(function (e) {
            $(this).tab('show');
            var scrollmem = $('body').scrollTop();
            window.location.hash = this.hash;
            $('html,body').scrollTop(0);
        });
    });


</script>
@stop
