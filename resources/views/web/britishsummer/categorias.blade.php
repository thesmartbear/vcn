@extends('web.britishsummer.baseweb')

@section('extra_head')
    <!-- Custom style -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/style.css" rel="stylesheet">
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/contents.css" rel="stylesheet">

    <!-- Color style -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/dippy.css" rel="stylesheet">
@stop

@include('web.britishsummer.includes.menu'.ConfigHelper::config('sufijo').'-'.App::getLocale())

<div id="menubg">
    <div class="inside">
        <div class="text">
            <i class="fa fa-navicon" id="main-menu"></i>
        </div>
    </div>
</div>
<div id="menu"></div>

@section('container')

    <!-- Start wrapper -->
    <section id="wrapper">
        <div class="max-width" id="allcontent">
            <!-- Start row1 -->
            <div class="row">
                <!-- Start article -->
                <div class="col-xs-12 wrapper-bg" id="header">
                    <article>

                        <div class="intro-img col-xs-4 col-sm-6 pull-right" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/{{rand(1, 3)}}.png') repeat;">
                        </div>

                        <div class="intro col-xs-8 col-sm-6 pull-right">
                            <div class="titulo">
                                <div id="logo"><a href="/"><img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" /></a></div>
                                <h1 class="slogan">
                                    <span></span>
                                    @if($categorianame != '')
                                        {{$categorianame}}
                                        <br />
                                        <span></span>
                                        <small>{!! trans('web.categorias.'.$clase.'-desc') !!}</small>
                                    @else
                                        {!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!}
                                        <br />
                                        <span></span>
                                        <small>{!! trans('web.categorias.'.$categoria->slug.'-desc') !!}</small>
                                    @endif
                                </h1>

                                <h4 class="head seccion">{!! trans('web.categorias.eligecurso') !!}<span></span></h4>
                            </div>
                        </div>

                    </article>
                </div>
            </div>
            <!-- End article -->




            <div class="container" id="contenido">

                @include('web.britishsummer.includes.plusinfo')

                <div class="row">

                    <div class="col-sm-8 col-xs-12 addmargintop60 referencia {{$clase}}">
                        <div class="introseccion">

                            @if($categorianame != '')
                                @if(Lang::has('web.categorias.'.$clase.'-intro')){!! trans('web.categorias.'.$clase.'-intro') !!}@endif
                            @else
                                @if(Lang::has('web.categorias.'.$categoria->slug.'-intro')){!! trans('web.categorias.'.$categoria->slug.'-intro') !!}@endif
                            @endif

                        </div>
                        <div class="row addmargintop60" id="programas">
                            @if($categorianame != '')
                                    @foreach($categoria as $cat)
                                        <div class="col-sm-12 col-xs-12 wrapper-bg programa categoria">
                                            <div class="overflow">
                                                <a href="{{Request::url()}}/{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug) !!}" class="plus"><span>+ info</span></a>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h5><a href="{{Request::url()}}/{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug) !!}" class="titular">{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web) !!}
                                                                <small>@if(Lang::has('web.categorias.'.$cat->slug.'-desc')){!! trans('web.categorias.'.$cat->slug.'-desc') !!}@endif</small>
                                                            </a>
                                                        </h5>
                                                        <p></p>
                                                        <p><small>{{$numcursos = \VCN\Models\Cursos\Curso::where('category_id',$cat->id)->where('course_language', $operador, $idioma)->where('course_active', 1)->count()}} cursos</small></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                            @else
                                        @foreach($categoria->subcategorias as $sc)
                                            <div class="col-sm-12 col-xs-12 wrapper-bg programa categoria">
                                                <div class="overflow">
                                                    <a href="{{Request::url()}}/{!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $sc->id, $sc->slug) !!}" class="plus"><span>+ info</span></a>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <h5><a href="{{Request::url()}}/{!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $sc->id, $sc->slug) !!}" class="titular">{!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $sc->id, $sc->name_web) !!}
                                                                    <small>@if(Lang::has('web.categorias.'.$sc->slug.'-desc')) {!! trans('web.categorias.'.$sc->slug.'-desc') !!} @endif</small>
                                                                </a>
                                                            </h5>
                                                            <p></p>
                                                            <p><small>{{$numcursos = \VCN\Models\Cursos\Curso::where('subcategory_id',$sc->id)->where('category_id',$categoria->id)->where('course_language', $operador, $idioma)->where('course_active', 1)->count()}} cursos</small></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                            @endif


                        </div>

                        @include('web.britishsummer.includes.copyright')

                    </div>
                    <!-- Start right sidebar -->
                    <div class="col-sm-3 col-sm-offset-1 wrapper-bg" id="sidebar">
                        @if($categorianame != '')
                            @if(Lang::has('web.categorias.'.$categorianameslug.'-sidebar'))
                                <div class="widget clearfix filtros">
                                    <div class="box">
                                        {!! trans('web.categorias.'.$categorianameslug.'-sidebar') !!}
                                    </div>
                                </div>
                            @endif
                        @else
                            @if(Lang::has('web.categorias.'.$categoria->slug.'-sidebar'))
                                <div class="widget clearfix filtros">
                                    <div class="box">
                                        {!! trans('web.categorias.'.$categoria->slug.'-sidebar') !!}
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                    <!-- End right sidebar -->
                </div>

            </div>

    </section>
    <!-- End wrapper -->

<!-- Modal -->
<div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
            </div>
            <div class="modal-body">
                <div id="respuesta">
                    <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                    <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                        <div class="msg"></div>
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="form-group">
                                    <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                    <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                    <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                    <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                    <input type="hidden" id="curso" value="@if($categorianame != ''){{$categorianame}}@else{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!}@endif">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>

                <p class="text-center"><br /><small><a href="/proteccion-de-datos.html" target="_blank">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</a></small></p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@stop

@section('extra_footer')
        <!-- contenidos -->
    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/contenidos.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.msg').hide();
            $("#plusinfoenviar").click(function() {
                console.log('validar');
                if ($('#name').val() == ''){
                    $('.msg').html('Debes indicar un nombre de contacto');
                    $('.msg').show();
                    return false;
                }
                if ($('#email').val() == ''){
                    $('.msg').html('Debes indicar un teléfono o un email de contacto');
                    $('.msg').show();
                    return false;
                }

                if ($('#email').val() != ''){
                    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
                    if (re.test($('#name').val())) {
                        $('.msg').html('El email no tiene un formato correcto');
                        $('.msg').show();
                        return false
                    }

                }

                if (!/^([0-9])*$/.test($('#tel').val())){
                    $('.msg').html('El campo teléfono tiene que ser numérico');
                    $('.msg').show();
                    return false
                }


                post_data = {'name':$('#name').val(), 'tel':$('#tel').val(),'email':$('#email').val(), 'curso':$('#curso').val()};
                $.ajax({
                    type: "POST",
                    url: "plusinfosend.php",
                    data: post_data,
                    success: function(msg){
                        $("#respuesta").html(msg);
                        $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                        ga('send', 'Solicitud', 'button', 'click', 'Aprender ingles en el extranjero', 1);
                    },
                    error: function(){
                        alert("error!!");
                    }
                });
            });


            $('#programas').isotope({
                itemSelector: '.programa',
                masonry: {
                    columnWidth: '.programa'
                }
            });

        });

        $('form input').blur(function () {
            $('.msg').hide();
        });

        $('#fotos-curso').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image',
            gallery: {
                enabled: true
            },
            titleSrc: 'title'
        });



    </script>
@stop
