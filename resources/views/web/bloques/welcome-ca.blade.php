@extends('web.bloques.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')
        <!-- Custom style -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/style.css" rel="stylesheet">
<link href="/assets/{{ConfigHelper::config('tema')}}/css/home.css" rel="stylesheet">

@stop

@section('container')
    <div class="blurried">
        <div class="linea"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="titular-seccion"></div>

                    <div class="intro-seccion">
                        <div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:3px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:8px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:13px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:18px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:23px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:29px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:34px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:39px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:44px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:49px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:55px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:60px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:65px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:70px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:75px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:81px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:86px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:91px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:96px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:101px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:107px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:112px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:117px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:122px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:127px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:133px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:138px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:143px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:148px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:153px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:159px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:164px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:169px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:174px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:179px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:185px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:190px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:195px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:200px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:205px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:0px"></div><div class="hidden-xs" style="float:left;clear:left;height:15px;width:0px"></div><div class="hidden-xs" style="float:right;clear:right;height:15px;width:0px"></div>
                        <p></p>
                    </div>

                    <div class='intro-video'>
                        <div class='col-md-10'>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5 col-sm-offset-1 col-lg-7 col-lg-offset-1">
                    <div class="promo-seccion"></div>
                    <div class="enlaces"></div>
                </div>
            </div>
        </div>
    </div>

    @include('web.bloques.menu'.ConfigHelper::config('sufijo').'-'.App::getLocale())


    <div class="progress-indicator">
        <span><img src="/assets/{{ConfigHelper::config('tema')}}/img/load.gif" alt="" /></span>
    </div>
    <div class="close-touch">+</div>
    <div id="promocion" style="display: none; visibility: hidden;">
        <div class="icon"></div>
        <a href="curso-academico-en-el-extranjero/estados-unidos-colegio-publico-familia-voluntaria.html">
            <h4>Año Escolar en USA<br />¡por 10.380€!<br />
                <small>Año escolar en Estados Unidos (Colegio público y Familia voluntaria) 700€ de descuento si te apuntas antes del 31/12/2015</small>
            </h4>
        </a>
    </div>
    <div class="marco">
        <div id="logo">
            <a href="/"><img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" /></a>
        </div>
        <div class="contenido">

            <div class="all">

                <div class="one big box">
                    <div class="more" data-bg="#1abc9c" data-intro="Perquè ens anem a enganyar, l'anglès és l'idioma que tothom vol aprendre i per suposat saber. Per això en aquesta secció només hi ha cursos d'anglès, perquè ningú es perdi en la recerca i trobi el seu curs idoni.<br /><br />Anglès per a nens, anglès per a adolescents, anglès per a adults, anglès per a famílies senceres, anglès per a professionals desesperats o no tant. <em>“English for everybody and everywhere”</em>." data-video="">
                        <div class="inside">
                            <ul>
                                <li><a href="aprendre-angles-a-l-estranger/joves/grups-amb-monitor">PROGRAMES D'ESTIU AMB MONITOR <small>de 8 a 18 anys</small></a></li>
                                <li><a href="aprendre-angles-a-l-estranger/joves/cursos-especiales">CURSOS MOLT SINGULARS/ESPECIALITZATS <small>de 10 a 19 anys</small></a></li>
                                <li><a href="aprendre-angles-a-l-estranger/adults-professionals">ADULTS I PROFESSIONALS <small>de 18 a 99 anys</small></a></li>
                                <li><a href="aprendre-angles-a-l-estranger/tota-la-familia">TOTA LA FAMÍLIA</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="inside">
                        <div class="text">
                            <h2>APRENDRE<br />
                                <strong>ANGLÈS</strong><br />
                                A L'ESTRANGER</h2>
                            <h4>TOTES LES EDATS</h4>
                        </div>
                    </div>
                </div>
                <div class="two big box">
                    <div class="inside">
                        <div class="text">
                            <i class="fa fa-navicon" id="main-menu"></i>
                        </div>
                    </div>
                </div>
                <div id="menu">

                </div>
                <div class="clearfix"></div>
                <div class="three big box">
                    <div class="more" data-bg="#888535" data-intro="No només d'anglès viu l'home. Imagina't en un mercat de Xangai parlat sobre la textura dels dim-sum amb el seu cuiner! O negociant en francès per al teu negoci de moda. O debatent en alemany sobre la nova arquitectura de la ciutat de Berlín.<br /><br />Xinès, japonès, francès, alemany, rus, italià, àrab, portuguès ... en la varietat està el bon gust No és així?" data-video="">
                        <div class="inside">
                            <ul>
                                <li><a href="aprender-altres-idiomes-a-l-estranger/joves">De 8 a 18 anys</a></li>
                                <li><a href="aprender-altres-idiomes-a-l-estranger/adults">De 18 a 99 anys</a></li>
                                <li><a href="aprender-altres-idiomes-a-l-estranger/tota-la-familia/">TOTA LA FAMÍLIA</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="inside">
                        <div class="text">
                            <h2>ALTRES IDIOMES<br />
                                A L'ESTRANGER<br />
                                <small>ALEMANY, XINÈS, FRANCÈS,<br />
                                    ITALIÀ, JAPONÈS, RUS....</small></h2>
                            <h4>TOTES LES EDATS</h4>
                        </div>
                    </div>
                </div>
                <div class="four small box">
                    <div class="inside">
                    </div>
                </div>

                @if(ConfigHelper::config('propietario') == 1)
                <div class="five small box">
                    <div class="more" data-bg="#8e44ad" data-intro="Si creieu que encara és aviat per anar a l'estranger o esteu convençuts que un bon campament d'idiomes a Espanya pot ser fins i tot millor que un fos ... O si aquest estiu disposeu de menys temps o d'un pressupost més reduït ... Sigui com sigui, els nostres campaments en anglès i amb anglès són veritablement espectaculars. Tria data, especialitat i destí; carrega't les piles i disposa't a aprendre sense límit." data-video="<div class='col-sm-12'><video controls preload='none' poster='/assets/{{ConfigHelper::config('tema')}}/home/maxcamps2015.jpg'>
   <source src='/assets/{{ConfigHelper::config('tema')}}/home/maxcamps2015.mp4' type='video/mp4'></video></div>">
                        <div class="inside">
                            <ul>
                                @foreach(\VCN\Models\Subcategoria::where('category_id', \VCN\Models\Traducciones\Traduccion::select('modelo_id')->where(['idioma'=> 'ca', 'campo'=> 'slug', 'traduccion'=> 'campaments-estiu-angles'])->first()->modelo_id)->get() as $subcategoria)
                                    <li><a href="campaments-estiu-angles/{!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $subcategoria->id, $subcategoria->slug) !!}">{!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}</a></li>
                                @endforeach
                            </ul>
                            <p class="addmargintop20"><a href="campaments-estiu-angles/"><strong>LES NOSTRES CLASSES</strong></a></p>
                        </div>
                    </div>
                    <div class="inside">
                        <div class="text camps">
                            <h3>MAX CAMPS</h3>
                            <h2>CAMPAMENTS<br>
                                D'ESTIU <span>+anglès</span><br />
                                A ESPANYA</h2>
                            <h4><strong>DE 7 A 17 ANYS</strong></h4>
                        </div>
                    </div>
                </div>
                <div class="six small box"></div>
                    <div class="seven small box bs">
                        <div class="more" data-bg="#2980b9" data-intro="Cada vegada més les escoles es plantegen aprofitar una setmana del seu calendari escolar o algun parèntesi del mateix, per integrar-se en la cultura d'un país i practicar l'idioma que porten temps estudiant. A British Summer oferim gran varietat de programes que permeten realitzar unes classes de l'idioma triat en diferents nivells i amb participants internacionals, allotjar-se en família o apartaments i disposar de temps lliure per compartir en conjunt els increïbles atractius d'algunes de les ciutats europees més cosmopolites." data-video="">
                            <div class="inside">
                                <ul>
                                    <li><a href="/catalogos/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Short Stays</a></li>
                                    <li><a href="/catalogos/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Summer Courses</a></li>
                                    <li><a href="/catalogos/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Trimestre Escolar</a></li>
                                    <li><a href="/catalogos/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Campaments d'idiomes</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="inside">
                            <div class="text ">
                                <h2><span><strong>VIATGES EDUCATIUS</strong></span><br />
                                    PER A COL·LECTIUS<br />
                                    I ESCOLES</h2>
                            </div>
                        </div>
                    </div>
                @elseif(ConfigHelper::config('propietario') == 2)
                    <div class="five small box">
                        <div class="more" data-bg="#8e44ad" data-intro="Com cada curs, des de l’equip de la ICCIC, treballem amb molta il.lusió i ganes en aquest projecte, sempre mantenint unes característiques i metodologia que són fonamentals per a nosaltres. Les franges d’edat específiques de cada colònia i els grups reduïts per tal de facilitar la integració i l’aprofitament de totes les classes i activitats, en són un bon exemple.">
                            <div class="inside">
                                <ul>
                                    <li><a href="summer-camps-angles/l-armentera.html">L'ARMENTERA <small>Osona. De 6 a 10 años</small></a></li>
                                    <li><a href="summer-camps-angles/els-pins.html">ELS PINS <small>Maresme. De 10 a 15 años</small></a></li>
                                    <li><a href="summer-camps-angles/la-solana.html">LA SOLANA <small>La Cerdanya. De 10 a 15 años</small></a></li>
                                </ul>
                                <p class="addmargintop20"><a href="summer-camps-angles/"><strong>QUÈ ÉS UNA COLÓNIA CIC A CATALUNYA?</strong></a></p>
                            </div>
                        </div>
                        <div class="inside">
                            <div class="text camps">
                                <h3>SUMMER CAMPS</h3>
                                <h2>CAMPAMENTS<br>
                                    D'ESTIU <span> en anglès</span></h2>
                                <h4><strong>DE 6 A 15 ANYS</strong></h4>
                            </div>
                        </div>
                    </div>
                    <div class="six small box"></div>
                    <div class="seven small box daycamp">
                        <div class="more" data-bg="#8e44ad" data-intro="La actividad perfecta para complementar un curso intensivo en la escuela.">
                            <div class="inside">
                                <ul>
                                    <li><a href="summer-camps-angles/active-english.html">ACTIVE ENGLISH <small>Tallers, activitats, jocs i excursions en anglès</small></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="inside">
                            <div class="text ">
                                <h3>DAY CAMP</h3>
                                <h2>ACTIVE ENGLISH</h2>
                                <h4><strong>DE 10 A 13 ANYS</strong></h4>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="eight medium box">
                    @if(ConfigHelper::config('propietario') == 1)
                    <div class="more" data-bg="rgba(231, 76, 60,1)" data-intro="Les mares opinen<br /><span class='col-sm-12'><video controls preload='none' poster='/assets/{{ConfigHelper::config('tema')}}/home/madres.png'>
   <source src='/assets/{{ConfigHelper::config('tema')}}/home/madres.mp4' type='video/mp4'></video></span>Un vídeo que ens permet entendre el que suposa per a un nord-americà acollir un estudiant d'any acadèmic de forma voluntària." data-video="
                    	<iframe class='col-sm-10' height='180' frameborder='0' wmode='opaque' allowfullscreen='' src='https://www.youtube.com/embed/YRP3PbaE-cA' />" data-promo="<div class='col-sm-12'><h2>Oberta la inscripció per al curs 2016/17<br />
                        <small>Demana cita per realitzar el teu test de nivell i informar-te en profunditat.</small></h2>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0' class='direcciones table table-hover'>
                          <tr class='plusinfoaa' data-oficina='madrid' data-toggle='modal' data-target='#plusinfomodal'>
                            <td><strong>MADRID</strong></td>
                            <td class='text-center'>madrid@britishsummer.com</td>
                            <td class='text-right'>T. 91.345.95.65</td>
                          </tr>
                          <tr class='plusinfoaa' data-oficina='infobs' data-toggle='modal' data-target='#plusinfomodal'>
                            <td><strong>BARCELONA</strong></td>
                            <td class='text-center'>infobs@britishsummer.com</td>
                            <td class='text-right'>T. 93.200.88.88</td>
                          </tr>
                          <tr class='plusinfoaa' data-oficina='girona' data-toggle='modal' data-target='#plusinfomodal'>
                            <td><strong>GIRONA</strong></td>
                            <td class='text-center'>girona@britishsummer.com</td>
                            <td class='text-right'>T. 972.414.902</td>
                          </tr>
                          <tr class='plusinfoaa' data-oficina='sevilla' data-toggle='modal' data-target='#plusinfomodal'>
                            <td><strong>SEVILLA</strong></td>
                            <td class='text-center'>sevilla@britishsummer.com</td>
                            <td class='text-right'>T. 95.421.07.85</td>
                          </tr>
                        </table>
                        </div>
                        ">
                        @elseif(ConfigHelper::config('propietario') == 2)
                            <div class="more" data-bg="rgba(231, 76, 60,1)" data-intro="Les mares opinen<br /><span class='col-sm-12'><video controls preload='none' poster='/assets/{{ConfigHelper::config('tema')}}/home/madres.png'>
   <source src='/assets/{{ConfigHelper::config('tema')}}/home/madres.mp4' type='video/mp4'></video></span>Un vídeo que ens permet entendre el que suposa per a un nord-americà acollir un estudiant d'any acadèmic de forma voluntària." data-video="
                    	<iframe class='col-sm-10' height='180' frameborder='0' wmode='opaque' allowfullscreen='' src='https://www.youtube.com/embed/YRP3PbaE-cA' />" data-promo="">
                        @endif

                        <div class="inside">
                            <ul>
                                <li><a href="curs-escolar-a-l-estranger/curs-escolar/">Any Escolar</a></li>
                                <li><a href="curs-escolar-a-l-estranger/semestre-escolar/">Semestre Escolar</a></li>
                                <li><a href="curs-escolar-a-l-estranger/trimestre-escolar/">Trimestre Escolar</a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="inside" style="text-shadow: 1px 1px 0px black;">
                        <div class="text">
                            <h2>ANY ESCOLAR<br />
                                <small>Semestre / Trimestre<br />a l'estranger</small></h2>
                            <h4>Primària, Secundària<br />i Batxillerat</h4>
                            <h3>Inscripció oberta curs 2016/17</h3>
                        </div>
                    </div>
                    <div class="header-unit">
                        <div id="video-container">
                            <video autoplay loop class="fillWidth">
                                <source src="/assets/{{ConfigHelper::config('tema')}}/home/academico2015.mp4" type="video/mp4" />
                                <source src="/assets/{{ConfigHelper::config('tema')}}/academico.ogv" type="video/ogg" />
                                <source src="/assets/{{ConfigHelper::config('tema')}}/academico.webm" type="video/webm" />
                            </video>
                        </div><!-- end video-container -->
                    </div><!-- end .header-unit -->
                </div>
            </div>
        </div>

        <!-- Modal -->
        @include('web._partials.plusinfomodal', ['hidden'=> "welcome-ca"])
@stop

@section('extra_footer')

        <script src="/assets/{{ConfigHelper::config('tema')}}/js/home.js"></script>

        <script type="text/javascript">
            $(window).load(function(){
                $('#promomodal').modal('show');
            });
        </script>

@stop
