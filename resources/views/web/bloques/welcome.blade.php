@extends('web.bloques.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{urldecode(trans('web.seo-'.ConfigHelper::config('sufijo').'.subject'))}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')

    <!-- Link Swiper's CSS -->
    {{--<link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/sf.css" rel="stylesheet">
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/swiper/swiper.min.css" rel="stylesheet">--}}

    <link href="/assets/{{ConfigHelper::config('tema')}}/css/home.css" rel="stylesheet">

@stop

@section('container')

    <div class="container-fluid">
        <div class="app">

            <div class="cursos-group">

                @foreach($webcats as $iwc => $wc)

                    <?php
                        $bg = "";
                        $bgColor = "style='background:none;'";

                        if($wc->imagen_home)
                        {
                            $bg = "style='background: url(" . $wc->imagen . ") no-repeat; background-position: center center; background-size: cover;'";
                        }
                        elseif($wc->color)
                        {
                            $bgColor = "style='background:none; background-color: $wc->color'";
                            $bg = $bgColor;
                        }

                        if ($wc->es_link == 1)
                        {
                            $url = Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'link', $wc->id, $wc->link);
                        }else{
                            $url = Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $wc->id, $wc->seo_url);
                        }

                        $url = "/". App::getLocale() ."/$url";

                        if ($wc->link_blank == 1){
                            $link = 'href='.$url.' target="_blank"';
                        }else{
                            $link = 'href='.$url;
                        }

                        $name = Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'name', $wc->id, $wc->name);
                        $boton = null;
                        if($wc->home_boton_activo)
                        {
                            $boton = $wc->home_boton ? Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'home_boton', $wc->id, $wc->home_boton) : __('web.masinfo');
                        }

                        $titulo = $wc->home_titulo ?: $url;
                        $titulol = $wc->home_titulo_link ?: $url;
                        
                        if($titulol)
                        {
                            $titulo = "<a href='$titulol'>$titulo</a>";
                        }
                    ?>

                    @switch($wc->home_imagen_pos)

                        @case(0) {{-- fondo --}}

                            <div class="row cursos-group-item home-seccion" {!! $bg !!}>
                                <div class="course-container" style="background: none;">

                                    @if($titulo)

                                        <div class="row">

                                        @switch($wc->home_titulo_pos)
                                        
                                            @case(0)
                                                <div class="col-sm-12 boxcurso">
                                                    <div class="titulo">
                                                        {!! $titulo !!}
                                                        <div class="boxcurso2">
                                                            {!! $wc->home_titulo2 !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            @break

                                            @case(1)
                                                <div class="col-sm-6 boxcurso">
                                                    <div class="titulo">
                                                        {!! $titulo !!}
                                                        <div class="boxcurso2">
                                                            {!! $wc->home_titulo2 !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6"></div>
                                            @break

                                            @case(2)
                                                <div class="col-sm-6"></div>
                                                <div class="col-sm-6 boxcurso">
                                                    <div class="titulo">
                                                        {!! $titulo !!}
                                                        <div class="boxcurso2">
                                                            {!! $wc->home_titulo2 !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            @break

                                        @endswitch

                                        </div>
                                        
                                    @endif
                                        
                                    @if($boton)
                                        <div class="linkcurso">
                                            <a class="btn btn-info" {{$link}}>{{$boton}}</a>
                                        </div>
                                    @endif
                                    
                                </div>
                            </div>

                        @break


                        @case(1) {{-- izquierda --}}

                            <div class="row cursos-group-item home-seccion">
                                <div class="course-container" style="background: none;">

                                    <div class="row">
                                        <div class="col-md-6 home-caja" {!! $bg !!}></div>
                                        <div class="col-md-6 home-caja home-texto">

                                            <div class="col-sm-12 boxcurso">
                                                <div class="titulo">
                                                    {!! $titulo !!}
                                                    <div class="boxcurso2">
                                                        {!! $wc->home_titulo2 !!}
                                                    </div>
                                                </div>
                                            </div>

                                            @if($boton)
                                                <div class="linkcurso">
                                                    <a class="btn btn-info" {{$link}}>{{$boton}}</a>
                                                </div>
                                            @endif

                                        </div>
                                    </div>

                                </div>
                            </div>

                        @break

                        @case(2) {{-- derecha --}}

                            <div class="row cursos-group-item home-seccion">
                                <div class="course-container" style="background: none;">

                                    <div class="row">
                                        <div class="col-md-6 home-caja home-texto">

                                            <div class="col-sm-12 boxcurso">
                                                <div class="titulo">
                                                    {!! $titulo !!}
                                                    <div class="boxcurso2">
                                                        {!! $wc->home_titulo2 !!}
                                                    </div>
                                                </div>
                                            </div>

                                            @if($boton)
                                                <div class="linkcurso">
                                                    <a class="btn btn-info" {{$link}}>{{$boton}}</a>
                                                </div>
                                            @endif

                                        </div>
                                        <div class="col-md-6 home-caja" {!! $bg !!}></div>
                                    </div>

                                </div>
                            </div>

                        @break


                    @endswitch

                @endforeach

            </div>

        </div>
    </div>
@stop