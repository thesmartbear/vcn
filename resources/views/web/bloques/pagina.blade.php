@extends('web.bloques.baseweb')
@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')
<!-- Color style -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/bs.css" rel="stylesheet">
@stop


@section('container')
    <div class="headerbg" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/{{rand(1, 3)}}.png') repeat;">
        <div class="container" id="header">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo">
                        <h1 class="slogan">
                            <span></span>
                            {!! Traductor::getWeb(App::getLocale(), 'Pagina', 'name', $pagina->id, $pagina->titulo) !!}
                            <br />
                            <span></span>
                            <small>@if(Lang::has('web.'.$pagina->url.'-elige')){!! trans('web.'.$pagina->url.'-desc') !!}@endif</small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="headerbgoverlay"></div>
    </div>

    <main class="cd-main-content">
        <div class="container" id="contenido">
            <div class="row">
                <div class="col-sm-8 col-xs-12 pagina">
                    {!! Traductor::getWeb(App::getLocale(), 'Pagina', 'contenido', $pagina->id, $pagina->contenido) !!}
                </div>

                @include('web.bloques.includes.copyright')

                    <!-- Start right sidebar -->
                    <div class="col-sm-3 col-sm-offset-1 wrapper-bg" id="sidebar">
                        @if(Lang::has('web.categorias.'.$pagina->url.'-sidebar'))
                            <div class="widget clearfix filtros">
                                <div class="box">
                                    {!! trans('web.categorias.'.$pagina->url.'-sidebar') !!}
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- End right sidebar -->
            </div>
        </div>
    </main>

    <!-- Modal -->
    @include('web._partials.plusinfomodal', ['hidden'=> Traductor::getWeb(App::getLocale(), 'Pagina', 'titulo', $pagina->id, $pagina->titulo) ])

@stop
