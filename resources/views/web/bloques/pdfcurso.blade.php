
<html>
<head>
    <title>{{$curso->name}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>



    <!-- Bootstrap -->
    {!! Html::style('assets/css/bootstrap.css') !!}
    <!-- font awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    {!! Html::style('assets/'.ConfigHelper::config('tema').'/css/webstyle.css') !!}
    <!-- Color style -->
    {!! Html::style('assets/'.ConfigHelper::config('tema').'/css/skin/'.$clase.'.css') !!}


    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            background: transparent;
            text-shadow: none;
        }
        body{
            font-size: 1.2em;
            padding: 2cm;
        }
        #header{
            padding-top: 1cm;
            padding-bottom: 0;
        }
        .headerimg{
            height: 300px;
        }
        .page {

        }
        .white #sidebar{
            background: transparent;
            min-height: 0;
            margin-top: 30px;
            position: relative;
        }
        #contenido{
            padding-top: 0;
            margin-top: 0;
        }

        .container{
            padding: 0 2cm;
        }
        .curso #contenido{
            margin-top: 0;
        }
        ul.breadcrumb{
            padding-left: 0;
        }
        ul.breadcrumb li{
            font-size: 0.3em !important;
        }
        .breadcrumb > li + li:before {
            color: #ccc !important;
        }
        .paistitulo{
            font-size: 0.8em !important;
        }
        .idioma p{
            margin-bottom: 0;
        }
        h2{
            margin-top: 2cm;
            font-size: 1.8em;
            font-weight: bold;
            @if(ConfigHelper::config('propietario') == 1)
                color: #f1c40f;
            @elseif(ConfigHelper::config('propietario') == 2)
                color: #3B6990;
            @endif
        }
        h2:first-child{
            margin-top: 1cm;
        }
        h4{
            margin-top: 30px;
            page-break-before: always;
            page-break-inside: avoid;
            text-transform: capitalize;
        }
        .page{
            page-break-after: always;
            page-break-inside: avoid;
        }
        ul.incluye{
            list-style: none;
            padding-left: 0;
            font-size: 0.8em;
            line-height: 1em;
            color: #6D6D6D;
        }
        ul.incluye li{
            color: #6D6D6D;
        }
        p.duracion{
            margin-bottom: 0;
        }

        h6.separator,
        h4.separator{
            font-weight: bold;
            border-bottom: 1px solid #CCC;
            font-size: 1.2em;
            margin-top: 20px;
        }
        .precio h5,
        .precio h6{
            font-weight: bold;
            margin-bottom: 6px;
        }

        .fotospdf{
            page-break-after: always;
            page-break-inside: avoid;
        }

        @media print{
            .page{
                page-break-after: always;
                page-break-inside: avoid;
            }
            .fotospdf{
                page-break-after: always;
                page-break-inside: avoid;
            }
        }


    </style>
</head>
<body>


    <?php
    $fotoscentro = '';
    $fotoscentroname = array();
    $path = public_path() ."/assets/uploads/center/" . $curso->centro->center_images;
    $folder = "assets/uploads/center/" . $curso->centro->center_images;

    if (is_dir($path)) {
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;
            if(strpos($result, ".webp"))
            {
                $result = substr($result, 0, -5);
            }

            $file = $path . '/' . $result;

            if (is_file($file)) {
                $fotoscentro .= '
                                <div class="col-xs-3">
                                    <div style="position:relative; overflow:hidden; padding-bottom:100%;"><img class="img-responsive full-width" style="position: absolute;" src="https://'.ConfigHelper::config('web').'/'.$folder.'/thumb/'.$result.'" alt=""></div>
                                </div>';
                $fotoscentroname[] = $result;

            }
        }
    }
    ?>
    <?php
    $fotoscurso = '';
    $fotoscursoname = array();
    $path = public_path() ."/assets/uploads/course/" . $curso->course_images;
    $folder = "assets/uploads/course/" . $curso->course_images;

    if (is_dir($path)) {
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;
            if(strpos($result, ".webp"))
            {
                $result = substr($result, 0, -5);
            }

            $file = $path . '/' . $result;

            if (is_file($file)) {
                $fotoscurso .= '
                            <div class="col-xs-3">
                                <div style="position:relative; overflow:hidden; padding-bottom:100%;"><img class="img-responsive full-width" style="position: absolute;" src="https://'.ConfigHelper::config('web').'/'.$folder.'/thumb/'.$result.'" alt=""></div>
                            </div>';
                $fotoscursoname[] = $result;
            }
        }
    }
    ?>



<div class="page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12"><img style="width: 4.5cm; height: auto; margin-top: 30px;" class="pull-right" src="https://{{ConfigHelper::config('web')}}/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" /></div>
        </div>
    </div>


    <div class="container curso" id="header" style="position: relative; margin-bottom: 0;">
        <div class="row">
            <div class="col-xs-12">
                <div class="titulo">
                    <h1 class="slogan" id="slogan">
                            <span>
                                <ul class="breadcrumb">
                                    <li><a href="/{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url)!!}">{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo)!!}</a></li>
                                    @if($subcategoria != '')
                                        <li @if($subcategoria_detalle == '')class="activo"@endif><a href="/{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url)!!}/{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $subcategoria->id, $subcategoria->seo_url)!!}">{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo)!!}</a></li>
                                        @if($subcategoria_detalle != '')
                                            <li class="active"><a href="/{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url)!!}/{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $subcategoria->id, $subcategoria->seo_url)!!}/{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $subcategoria_detalle->id, $subcategoria_detalle->seo_url)!!}">{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria_detalle->id, $subcategoria_detalle->titulo)!!}</a></li>
                                        @endif
                                    @endif
                                </ul>
                            </span>
                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'name', $curso->id, $curso->name)!!}<br />

                        <small>@if($curso->centro->pais->name != 'España') {{Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name)}} @else {{Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name)}} @endif</small>
                    </h1>
                </div>

            </div>

            <!-- Start right sidebar -->
            <div class="col-xs-12" id="sidebar">
                <div class="box">

                    <div class="widget clearfix info-basica">
                        <div id="intro-curso">
                            <div id="details">
                                <ul>
                                    <li>
                                        <i class="fa fa-user"></i> {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_age_range', $curso->id, $curso->course_age_range)!!}
                                    </li>
                                    <li><i class="fa fa-globe"></i> {{trans('web.'.$curso->course_language)}}</li>
                                    <?php $alojas = array(); ?>
                                    @if(count($curso->alojamientos))
                                        @foreach($curso->alojamientos as $alojamiento)
                                            <?php $alojas[] = Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $alojamiento->tipo->id, $alojamiento->tipo->accommodation_type_name); ?>
                                        @endforeach
                                        @foreach(array_unique($alojas) as $alojatipo)
                                            <li><i class="fa fa-bed"></i> {!!$alojatipo!!}</li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <div id="espes">
                                <ul>
                                    <?php $anteriorespe = ''; ?>
                                    <?php $i = 1; ?>
                                    @foreach($curso->especialidades->sortBy('name') as $e)

                                        @if($e->especialidad->id != $anteriorespe || $anteriorespe == null)
                                            <li><i class="fa fa-star"></i><strong>{!!Traductor::getWeb(App::getLocale(), 'Especialidad', 'name', $e->especialidad->id, $e->especialidad->name)!!}</strong><br />
                                                @endif
                                                @if(count($curso->especialidades) == $i)
                                                    {!!Traductor::getWeb(App::getLocale(), 'Subespecialidad', 'name', $e->subespecialidad_id, $e->SubespecialidadesName)!!}.</li>
                                        @endif
                                        @if(($e->especialidad->id == $anteriorespe || $i == 1) && count($curso->especialidades) != $i)
                                            {!!Traductor::getWeb(App::getLocale(), 'Subespecialidad', 'name', $e->subespecialidad_id, $e->SubespecialidadesName)!!},
                                        @endif


                                        <?php $anteriorespe = $e->especialidad->id; ?>
                                        <?php $i++; ?>

                                    @endforeach
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- End right sidebar -->


        <div class="col-xs-12">
                    @if(is_file(public_path()."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada))
                        <div class="headerimg" style="background-image: url('https://{{ConfigHelper::config('web')}}/assets/uploads/course/{{$curso->course_images}}/{{$curso->image_portada}}'); background-size: cover;"></div>
                    @elseif(is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                        <div class="headerimg" style="background-image: url('https://{{ConfigHelper::config('web')}}/assets/uploads/center/{{$curso->centro->center_images}}/{{$curso->centro->center_image_portada}}'); background-size: cover;"></div>
                    @elseif(!is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                        @if(count($fotoscursoname))
                            <div class="headerimg" style="background-image: url('https://{{ConfigHelper::config('web')}}/assets/uploads/course/{{$curso->course_images}}/{{$fotoscursoname[rand(0,count($fotoscursoname)-1)]}}'); background-size: cover; background-position: center center;"></div>
                        @elseif(!count($fotoscursoname) && count($fotoscentroname))
                            <div class="headerimg" style="background-image: url('https://{{ConfigHelper::config('web')}}/assets/uploads/center/{{$curso->centro->center_images}}/{{$fotoscentroname[rand(0,count($fotoscentroname)-1)]}}'); background-size: cover;"></div>
                        @else
                        @endif
                    @endif

        </div>
</div>


        </div>

    <div class="curso" style="margin-top: 0;">
        <div class="container" id="contenido">
            <div class="row">
                <div class="col-xs-12" id="infocurso">
                                    <div>
                                        @if($curso->course_summary != '' || $curso->course_summary != null || $curso->course_content != '' || $curso->course_content != null  || $curso->frase != '' || $curso->frase != null || $curso->requisitos != '' || $curso->requisitos != null)
                                            <div id="{!! trans('web.curso.programa') !!}">
                                                <h2>{!! trans('web.curso.programa') !!}</h2>

                                                @if ($curso->course_summary != '' || $curso->course_summary != null)
                                                    <div class="introduccion">
                                                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_summary', $curso->id, $curso->course_summary)!!}
                                                    </div>
                                                @endif

                                                @if ($curso->frase != '' || $curso->frase != null)
                                                    <div class="info-general">
                                                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'frase', $curso->id, $curso->frase)!!}
                                                    </div>
                                                @endif

                                                @if ($curso->course_content != '' || $curso->course_content != null)

                                                    <?php
                                                        $string = Traductor::getWeb(App::getLocale(), 'Curso', 'course_content', $curso->id, $curso->course_content);
                                                        $string = preg_replace('/<iframe.*?\/iframe>/i','', $string);
                                                    ?>

                                                    {!! $string !!}

                                                @endif

                                                <div class="idioma">
                                                    @if ($curso->course_language != '' || $curso->course_language != null)
                                                        <p><strong>{!!trans('web.curso.idiomarequerido')!!}:</strong> {{trans('web.'.$curso->course_language)}}</p>
                                                    @endif

                                                    @if ($curso->course_minimun_language != '' || $curso->course_minimun_language != null)
                                                        <p><strong>{!!trans('web.curso.nivel')!!}:</strong> {{trans('web.'.$curso->course_minimun_language)}}</p>
                                                    @endif

                                                    @if ($curso->course_language_sessions != '' || $curso->course_language_sessions != null)
                                                        <p><strong>{!!trans('web.curso.sesiones')!!}:</strong> {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_language_sessions', $curso->id, $curso->course_language_sessions)!!}</p>
                                                    @endif
                                                </div>

                                                @if ($curso->requisitos != '' || $curso->requisitos != null)
                                                    <h4>{!! trans('web.curso.requisitos') !!}</h4>
                                                    {!!Traductor::getWeb(App::getLocale(), 'Curso', 'requisitos', $curso->id, $curso->requisitos)!!}
                                                @endif




                                                <?php
                                                $path = public_path() ."/assets/uploads/pdf/" . $curso->course_images;
                                                $folder = "/assets/uploads/pdf/" . $curso->course_images;

                                                if(is_dir($path))
                                                {
                                                    echo '<h4>'.trans('web.curso.timetable').'</h4>';
                                                    $results = scandir($path);
                                                    foreach ($results as $result) {
                                                        if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0]==='.') continue;

                                                        $file = $path . '/' . $result;

                                                        if( is_file($file) )
                                                        {
                                                            echo '<a href="//'.ConfigHelper::config('web').'/'.$folder . '/' . $result.'" target="_blank"><i class="fa fa-download"></i> '.$result.'</a><br />';
                                                        }
                                                    }
                                                }
                                                ?>

                                                @if ($curso->centro->internet == 1)
                                                    <div class="internet">
                                                        <p><i class="fa fa-laptop"></i> {!! trans('web.curso.internet-disponible') !!}</p>
                                                    </div>
                                                @endif
                                                @if ($curso->centro->comentarios != '' || $curso->centro->comentarios != null)
                                                    <div class="internetopciones">
                                                        <p>{!!$curso->centro->comentarios!!}</p>
                                                    </div>
                                                @endif


                                                @if ($curso->course_provider_url != '' || $curso->course_provider_url != null)
                                                    <hr>
                                                    <p><i class="fa fa-info-circle"></i> {!! trans('web.curso.masinfoproveedor1') !!} <a href="{!!$curso->course_provider_url!!}" target="_blank">{!! trans('web.curso.masinfoproveedor2') !!}</a></p>
                                                @endif
                                            </div>
                                        @endif

                                        @if($curso->centro->name != '' || $curso->centro->name != null || $curso->centro->description != '' || $curso->centro->description != null || $curso->centro->foods != '' || $curso->centro->foods != null || $curso->centro->center_video != '' || $curso->centro->center_video != null || $curso->centro->settingup != '' || $curso->centro->settingup != null)

                                                    <h2>{!! trans('web.curso.centro') !!}</h2>
                                                    <h3>{!!Traductor::getWeb(App::getLocale(), 'Centro', 'name', $curso->centro->id, $curso->centro->name)!!}<br />
                                                        <small><b>{!!Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name)!!}</b>. {!!Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name)!!}</small>
                                                    </h3>

                                                    @if($curso->centro->description != '' || $curso->centro->description != null)

                                                        <?php
                                                        $string = Traductor::getWeb(App::getLocale(), 'Centro', 'description', $curso->centro->id, $curso->centro->description);
                                                        $string = preg_replace('/<iframe.*?\/iframe>/i','', $string);
                                                        ?>

                                                        {!! $string !!}

                                                    @endif

                                                    @if($curso->centro->settingup != '' || $curso->centro->settingup != null)
                                                        <h4>{!! trans('web.curso.instalaciones') !!}</h4>
                                                        {!!Traductor::getWeb(App::getLocale(), 'Centro', 'settingup', $curso->centro->id, $curso->centro->settingup)!!}
                                                    @endif
                                                    @if($curso->centro->foods != '' || $curso->centro->foods != null)
                                                        <h4>{!! trans('web.curso.comidas') !!}</h4>
                                                        {!!Traductor::getWeb(App::getLocale(), 'Centro', 'food', $curso->centro->id, $curso->centro->foods)!!}
                                                    @endif
                                                    @if($curso->centro->transport != '' || $curso->centro->transport != null)
                                                        <h4>{!! trans('web.curso.transporte') !!}</h4>
                                                        {!!Traductor::getWeb(App::getLocale(), 'Centro', 'transport', $curso->centro->id, $curso->centro->transport)!!}
                                                    @endif


                                                    @if($curso->centro->address != '' || $curso->centro->address != null)

                                                        <h4>{!!Traductor::getWeb(App::getLocale(), 'Centro', 'address', $curso->centro->id, $curso->centro->address)!!}</h4>

                                                        <img src="https://maps.googleapis.com/maps/api/staticmap?center={{strip_tags($curso->centro->address)}}&zoom=13&size=640x400&key=AIzaSyCNPWo91Kd3D656Z5R2U6K1vFlx65KxpE8" />

                                                    @endif

                                        @endif

                                        @if(count($curso->alojamientos))

                                                <h2>{!! trans('web.curso.alojamiento') !!}</h2>
                                                @foreach($curso->alojamientos as $alojamiento)

                                                    <h4>{!!Traductor::getWeb(App::getLocale(), 'Alojamiento', 'name', $alojamiento->id, $alojamiento->name)!!}</h4>
                                                    
                                                    <p>
                                                        <?php
                                                        $string = Traductor::getWeb(App::getLocale(), 'Alojamiento', 'accommodation_description', $alojamiento->id, $alojamiento->accommodation_description);
                                                        $string = preg_replace('/<iframe.*?\/iframe>/i','', $string);
                                                        ?>

                                                        {!! $string !!}
                                                    </p>

                                                    <?php
                                                    $fotosaloja = '';
                                                    $fotosalojaname = array();
                                                    $path = public_path() ."/assets/uploads/alojamiento/" . $alojamiento->image_dir;
                                                    $folder = "/assets/uploads/alojamiento/" . $alojamiento->image_dir;

                                                    if (is_dir($path)) {
                                                        $results = scandir($path);
                                                        foreach ($results as $result) {
                                                            if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;
                                                            if(strpos($result, ".webp"))
                                                            {
                                                                $result = substr($result, 0, -5);
                                                            }
                                                            
                                                            $file = $path . '/' . $result;

                                                            if (is_file($file)) {
                                                                $fotosaloja .= '
                                                                    <div class="col-xs-3">
                                                                        <div style="position:relative; overflow:hidden; padding-bottom:100%;"><img class="img-responsive full-width" style="position: absolute;" src="https://'.ConfigHelper::config('web').'/'.$folder.'/thumb/'.$result.'" alt=""></div>
                                                                    </div>';
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    @if($fotosaloja != '')
                                                        <div id="fotos-aloja">
                                                            <div class="row fotospdf">
                                                                {!!$fotosaloja!!}
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach

                                        @endif

                                        @if($curso->course_activities != null || $curso->course_activities != '' || $curso->course_excursions != '' || $curso->course_excursions != null || $curso->centro->center_excursions != '' || $curso->center_excursions != null || $curso->centro->center_activities != '' || $curso->centro->center_activities != null)
                                            <div class="actividadesyexcursiones">
                                                    <h2>{!! trans('web.curso.actividades') !!}</h2>
                                                    @if($curso->course_activities != null || $curso->course_activities != '' || $curso->centro->center_activities != null || $curso->centro->center_activities != '')
                                                        <h4>{!! trans('web.curso.actividades') !!}</h4>
                                                        @if($curso->course_activities != null || $curso->course_activities != '')
                                                            <?php $string = Traductor::getWeb(App::getLocale(), 'Curso', 'course_activities', $curso->id, $curso->course_activities); ?>    
                                                        @elseif(($curso->course_activities == null || $curso->course_activities == '') && ($curso->centro->center_activities != null || $curso->centro->center_activities != ''))
                                                            <?php $string = Traductor::getWeb(App::getLocale(), 'Centro', 'center_activities', $curso->centro->id, $curso->centro->center_activities); ?>
                                                        @endif

                                                        <?php
                                                        $string = preg_replace('/<iframe.*?\/iframe>/i','', $string);
                                                        ?>

                                                        {!! $string !!}
                                                        
                                                    @endif
                                                    @if($curso->course_excursions != null || $curso->course_excursions != '' || $curso->centro->center_excursions != null || $curso->centro->center_excursions != '')
                                                        <h4>{!! trans('web.curso.excursiones') !!}</h4>
                                                        @if($curso->course_excursions != null || $curso->course_excursions != '')
                                                            <?php $string = Traductor::getWeb(App::getLocale(), 'Curso', 'course_excursions', $curso->id, $curso->course_excursions); ?>
                                                        @elseif(($curso->course_excursions == null || $curso->course_excursions == '') && ($curso->centro->center_excursions != null || $curso->centro->center_excursions != ''))
                                                            <?php $string = Traductor::getWeb(App::getLocale(), 'Centro', 'center_excursions', $curso->centro->id, $curso->centro->center_excursions); ?>
                                                        @endif

                                                        <?php
                                                        $string = preg_replace('/<iframe.*?\/iframe>/i','', $string);
                                                        ?>

                                                        {!! $string !!}
                                                        
                                                    @endif
                                            </div>
                                        @endif


                                            @if($curso->monitor_name != null || $curso->monitor_name != '')
                                                @if($curso->category_id == 4 || $curso->category_id == 6)
                                                    <h2>{!! trans('web.curso.coordinador') !!}</h2>
                                                @else
                                                    <h2>{!! trans('web.curso.monitor') !!}</h2>
                                                @endif
                                                <h4>{!! $curso->monitor_name !!}</h4>
                                                <div class="row">
                                                    @if($curso->monitor_foto != null || $curso->monitor_foto != '')
                                                        <div class="col-xs-4">
                                                            <img class="img-responsive img-thumbnail" src="https://{{ConfigHelper::config('web')}}/{{$curso->monitor_foto}}" />
                                                        </div>
                                                    @endif
                                                    @if($curso->monitor_desc != null || $curso->monitor_desc != '')
                                                        <div class="col-xs-8">
                                                            {!!$curso->monitor_desc!!}
                                                        </div>
                                                    @endif
                                                </div>
                                            @endif
                                    </div>
                </div>
            </div>
        </div>
</div>
</div>
{{-- page --}}
<div class="page">
    <div class="curso" style="margin-top: 0;">
                        <div class="container" id="contenido">
                            <div class="row">
                                <div class="col-xs-12" id="infocurso">

                                        <div class="fechasyprecios">
                                                <h2>{!! trans('web.curso.fechasyprecios') !!}</h2>
                                                {{-- convocatorias abiertas --}}
                                                {{-- precio curso: fecha + duracion => fecha_fin y precio --}}

                                                {{-- OJO CON VARIAS CONVOCATORIAS ABIERTAS A LA VEZ (se supone que no debe ser) PERO HABRÍA QUE CAMBIAR EL SCRIPT --}}

                                                @if(count($curso->convocatoriasAbiertas))
                                                    <p>{{trans('web.curso.presupuesto')}}</p>

                                                @endif


                                                <?php $plazas_disponibles = 0; ?>
                                                @if(count($curso->convocatoriasCerradas) && $curso->convocatoriasCerradas->contains('convocatory_semiopen', 0))
                                                    <div class="row">
                                                        @foreach($curso->alojamientos as $aloja)
                                                            <div class="col-sm-12"><h6 class="separator">{!!Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $aloja->accommodation_type_id, $aloja->accommodation_name)!!}</h6></div>
                                                            <?php
                                                            $convos = $curso->convocatoriasCerradas;
                                                            $convosordenadas = $convos->sortBy(function($convos) {
                                                                return sprintf('%-12s%s', $convos->convocatory_close_start_date, $convos->convocatory_close_duration_weeks);
                                                            });
                                                            ?>

                                                            @foreach($convosordenadas as $cc)
                                                                @if($cc->activo_web == 1)

                                                                    @if($cc->alojamiento_id == $aloja->id)
                                                                        @if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 0)
                                                                            <div class="col-xs-4 precio">

                                                                                {{-- PLAZAS POR ALOJAMIENTO --}}



                                                                                <h5>{{date('d.m.Y', strtotime($cc->convocatory_close_start_date))}} <span>{!! trans('web.curso.al') !!}</span> {{date('d.m.Y', strtotime($cc->convocatory_close_end_date))}}</h5>
                                                                                <p class="duracion">{{$cc->convocatory_close_duration_weeks}} {{trans_choice('web.'.ConfigHelper::getPrecioDuracionUnit($cc->duracion_fijo),$cc->convocatory_close_duration_weeks)}}</p>
                                                                                @if($cc->dto_early == null || $cc->dto_early == '')
                                                                                    <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b></p>
                                                                                @else
                                                                                    @if((strtotime(date('Y-m-d')) >= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->desde)) && (strtotime(date('Y-m-d')) <= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->hasta)))
                                                                                        <p><s>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</s></p>
                                                                                        <p><img class="earlybird" src="https://{{ConfigHelper::config('web')}}/assets/britishsummer/img/earlybird.png"> <b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price-(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->importe), \VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->moneda_name) }}</b></p>
                                                                                    @else
                                                                                        <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b></p>
                                                                                    @endif
                                                                                @endif
                                                                                    {!!trans('web.curso.precioincluye')!!}

                                                                                    <ul class="incluye">
                                                                                        @foreach($cc->incluyes as $cci)
                                                                                            @if($cci->incluye->tipo == 0)
                                                                                                <li>{!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cci->incluye->id, $cci->incluye->name) !!}</li>
                                                                                            @elseif($cci->incluye->tipo == 1 && $cci->valor != 0)
                                                                                                <li>{{$cci->valor}} {!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cci->incluye->id, $cci->incluye->name) !!}</li>
                                                                                            @endif
                                                                                        @endforeach
                                                                                        @if($cc->incluye_horario != '')<li>{!! trans('web.curso.incluyeclasesidioma') !!} {!! Traductor::getWeb(App::getLocale(), 'Cerrada', 'incluye_horario', $cc->id, $cc->incluye_horario) !!}</li>@endif
                                                                                        @if($cc->convocatory_close_price_include != ''){!! strip_tags(Traductor::getWeb(App::getLocale(), 'Cerrada', 'convocatory_close_price_include', $cc->id, $cc->convocatory_close_price_include),'<p><li><a><b><strong><em>') !!}@endif
                                                                                    </ul>
                                                                            </div>
                                                                        @endif
                                                                    @endif
                                                                @endif
                                                            @endforeach

                                                        @endforeach
                                                    </div>
                                                @endif


                                                @if($curso->convocatoriasCerradas->contains('convocatory_semiopen', 1))
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            @if($curso->subcategory_id != '10')
                                                                <h4 class="separator">{!! trans('web.curso.planb') !!}</h4>
                                                                <p>{!! trans('web.curso.planbfrase') !!}</p>
                                                            @endif
                                                        </div>
                                                        @foreach($curso->convocatoriasCerradas as $cc)
                                                            @if($cc->activo_web == 1)
                                                                @if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 1)
                                                                    <div class="col-xs-6 precio">
                                                                        @if($cc->alojamiento_id != '' || $cc->alojamiento_id != 0)<h6>{!! Traductor::getWeb(App::getLocale(), 'Alojamiento', 'name', $cc->alojamiento_id, \VCN\Models\Alojamientos\Alojamiento::find($cc->alojamiento_id)->name) !!}</h6>@endif
                                                                        <h5><span>{!! trans('web.curso.entre') !!}</span> {{date('d.m.Y', strtotime($cc->convocatory_close_start_date))}} <span>{!! trans('web.curso.yel') !!}</span> {{date('d.m.Y', strtotime($cc->convocatory_close_end_date))}}</h5>
                                                                        <p class="duracion">{{$cc->convocatory_close_duration_weeks}} {{trans_choice('web.'.ConfigHelper::getPrecioDuracionUnit($cc->duracion_fijo),$cc->convocatory_close_duration_weeks)}}</p>

                                                                            @if($cc->dto_early == null || $cc->dto_early == '')
                                                                                <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b></p>
                                                                            @else
                                                                                @if((strtotime(date('Y-m-d')) >= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->desde)) && (strtotime(date('Y-m-d')) <= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->hasta)))
                                                                                    <p><s>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</s></p>
                                                                                    <p><img class="earlybird" src="https://{{ConfigHelper::config('web')}}/assets/britishsummer/img/earlybird.png"> <b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price-(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->importe), \VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->moneda_name) }}</b></p>
                                                                                @else
                                                                                    <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b></p>
                                                                                @endif
                                                                            @endif

                                                                            {!!trans('web.curso.precioincluye')!!}
                                                                            <ul class="incluye">
                                                                                @foreach($cc->incluyes as $cci)
                                                                                    @if($cci->incluye->tipo == 0)
                                                                                        <li>{!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cci->incluye->id, $cci->incluye->name) !!}</li>
                                                                                    @elseif($cci->incluye->tipo == 1 && $cci->valor != 0)
                                                                                        <li>{{$cci->valor}} {!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cci->incluye->id, $cci->incluye->name) !!}</li>
                                                                                    @endif
                                                                                @endforeach
                                                                                @if($cc->incluye_horario != '')<li>{!! trans('web.curso.incluyeclasesidioma') !!} {!!$cc->incluye_horario!!}</li>@endif
                                                                                @if($cc->convocatory_close_price_include != ''){!! strip_tags($cc->convocatory_close_price_include,'<p><li><a><b><strong><em>') !!}@endif
                                                                            </ul>
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @endif


                                                @if($curso->es_convocatoria_multi == 1)
                                                    {!!Traductor::getWeb(App::getLocale(), 'Curso', 'preciosyfechas', $curso->id, $curso->preciosyfechas)!!}
                                                @endif

                                                @if(count($curso->extras))
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <h4 class="separator">{!! trans('web.curso.extras') !!}</h4>
                                                            @foreach($curso->extras as $e)
                                                                <h6>{!!Traductor::getWeb(App::getLocale(), 'CursoExtra', 'course_extras_name', $e->id, $e->course_extras_name)!!}</h6>
                                                                <p>{!!Traductor::getWeb(App::getLocale(), 'CursoExtra', 'course_extras_description', $e->id, $e->course_extras_description)!!}</p>
                                                                @if($e->course_extras_price > 0)
                                                                    <p><b>{{ ConfigHelper::parseMoneda($e->course_extras_price, $e->moneda) }}@if($e->course_extras_unit) / {{trans_choice('web.curso.'.$e->tipo->name, 1)}} @endif</b></p>
                                                                @else
                                                                    <p><b>{!! trans('web.curso.incluido') !!}</b></p>
                                                                @endif
                                                            @endforeach
                                                            {{--
                                                            @foreach($curso->extrasGenericos as $eg)
                                                                <h6>{{$eg->name}}</h6>
                                                                @if($eg->precio > 0)
                                                                    <p><b>{{ ConfigHelper::parseMoneda($eg->precio, $eg->moneda) }}@if($eg->generico->generic_unit_id == 1) / {{trans_choice('web.curso.'.$eg->generico->tipo->name, 1)}} @endif</b></p>
                                                                @else
                                                                    <p><b>{!! trans('web.curso.incluido') !!}</b></p>
                                                                @endif
                                                            @endforeach
                                                            --}}
                                                        </div>
                                                    </div>
                                                @endif
                                        </div>


                                        @if($fotoscentro != '' || $fotoscurso != '')
                                            <div id="{!! trans('web.curso.fotos') !!}" class="fotospdf">
                                                    <h2>{!! trans('web.curso.fotos') !!}</h2>
                                                    <div id="fotos-curso">
                                                        <div class="row">
                                                            {!! $fotoscentro !!}
                                                            {!! $fotoscurso !!}
                                                        </div>
                                                    </div>
                                            </div>
                                        @endif



                                    </div>
                                    <!-- end tabs -->
                                </div>

            </div>
       </div>
</div>

</body>
</html>

