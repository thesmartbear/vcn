<?php
    $tema = ConfigHelper::config('tema');
    $clasef = isset($clase) ? public_path("/assets/$tema/css/skin/$clase.css") : null;
    if(!file_exists($clasef))
    {
        $clase = "ingles";
    }
?>

@push('scripts')
<script src="//cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js" type="text/javascript"></script>
<script src="/assets/{{$tema}}/js/dropdowns-enhancement.js"></script>
<script src="/assets/{{$tema}}/js/filters.js" type="text/javascript"></script>

<link href="/assets/{{$tema}}/css/skin/{{$clase}}.css" rel="stylesheet">
<link href="/assets/{{$tema}}/css/dropdowns-enhancement.css" rel="stylesheet">

<script>
    $('#viewlist').click( function(){
        if(!$('.mix').hasClass('one')) {
            $('.fichacurso').hide().delay(300).fadeIn(200);
        }
        $('.mix, .gap').addClass('one').removeClass('two');
        $('.layout').siblings().removeClass('active');
        $(this).addClass('active');
    });
    $('#viewcolsbig').click( function(){
        $('.mix, .gap').addClass('two').removeClass('one');
        $('.layout').removeClass('active');
        $(this).addClass('active');
    });
    $('#viewcolssmall').click( function(){
        $('.mix, .gap').removeClass('two').removeClass('one');
        $('.layout').removeClass('active');
        $(this).addClass('active');
    });
    $(document).ready(function () {
        $('.lista #contenido').css({'padding-top': $('#sidebar').height()+'px'});
    });


    $('#sortPromo').click(function(){
        $('#cursos-lista').mixItUp('sort', 'promo:desc, name:asc');
        $('.order').removeClass('active');
        $(this).addClass('active');
    });
    $('#sortName').click(function(){
        $('#cursos-lista').mixItUp('sort', 'name:asc');
        $('.order').removeClass('active');
        $(this).addClass('active');
    });
    $('#sortPais').click(function(){
        $('#cursos-lista').mixItUp('sort', 'pais:asc');
        $('.order').removeClass('active');
        $(this).addClass('active');
    });
    $('.foto').hover(
            function(){
                $(this).addClass('active');
            },
            function () {
                $(this).removeClass('active');
            }
    );

    $('.nombrecurso').hover(function(e) {
        $(this).parents('.mix').find('.fotocurso').find('a').trigger(e.type);
    });

    if($(window).width() <= '767'){
        $('#viewlist').trigger('click');
        $('.gap').css({'display': 'none', 'visbility': 'hidden'});
    }

</script>

@endpush

<div class="row">
<div class="col-sm-12 col-xs-12 {{$clase}} categorias">
<div id="programas">
    <div id="cursos-lista">
        <div class="fail-message"><span>{{trans('web.sinresultados')}}</span></div>
        <div class="slides">
            @foreach($cursos as $curso)
                <?php
                $fotoscentro = '';
                $fotoscentroname = array();
                $path = public_path() ."/assets/uploads/center/" . $curso->centro->center_images;
                $folder = "/assets/uploads/center/" . $curso->centro->center_images;

                if (is_dir($path)) {
                    $results = scandir($path);
                    foreach ($results as $result) {
                        if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                        $file = $path . '/' . $result;

                        if (is_file($file)) {
                            $fotoscentroname[] = $result;

                        }
                    }
                }
                ?>
                <?php
                $fotoscurso = '';
                $fotoscursoname = array();
                $path = public_path() ."/assets/uploads/course/" . $curso->course_images;
                $folder = "/assets/uploads/course/" . $curso->course_images;

                if (is_dir($path)) {
                    $results = scandir($path);
                    foreach ($results as $result) {
                        if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                        $file = $path . '/' . $result;

                        if (is_file($file)) {
                            $fotoscursoname[] = $result;
                        }
                    }
                }
                ?>

                <?php $espes = array(); ?>
                @foreach($curso->especialidades as $e)
                    <?php $espes[] = str_slug(Traductor::getWeb(App::getLocale(), 'Especialidad', 'name', $e->especialidad->id, $e->especialidad->name)); ?>
                @endforeach

                <?php $alojascurso = array(); ?>
                @foreach($curso->alojamientos as $alojamiento)
                    <?php $alojascurso[] = str_slug(Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $alojamiento->tipo->id, $alojamiento->tipo->accommodation_type_name)); ?>
                @endforeach


                <?php $edades = array(); ?>
                @if($menor == 1)
                    @foreach(explode(',',$curso->course_age) as $edad)
                        <?php $edades[] = trans('web.edad').'-'.trim($edad); ?>
                    @endforeach
                    <?php $edades = array_map('trim',$edades); ?>
                @endif

                <?php $cursolink = '/'.Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url).'/'.Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug).'.html'; ?>


                @if($idiomas && $idiomas->count() > 1)
                    @if(is_array($curso->course_language))
                        <?php $idiomascurso = implode(' ',$curso->course_language); ?>
                    @else
                    <?php $idiomascurso = $curso->course_language; ?>
                    @endif
                @else
                    <?php $idiomascurso = ''; ?>
                @endif

                @if(is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada))
                    <div class="mix @if($curso->centro->pais->name != 'España') {{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}} @else {{str_slug(Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name))}} @endif {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}} {{implode($edades,' ')}} {{str_slug($idiomascurso)}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}}">
                        @if($curso->course_promo == 1)
                            <div class="promo-lista">
                                <i class="fa fa-asterisk"></i>
                            </div>
                        @endif
                        <div class="fotocurso" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/thumb/{{$curso->image_portada}}');">
                            <a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif{{$cursolink}}" class="foto">
                @elseif(is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                    <div class="mix @if($curso->centro->pais->name != 'España') {{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}} @else {{str_slug(Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name))}} @endif  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}} {{implode($edades,' ')}} {{str_slug($idiomascurso)}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}}">
                        @if($curso->course_promo == 1)
                            <div class="promo-lista">
                                <i class="fa fa-asterisk"></i>
                            </div>
                        @endif
                        <div class="fotocurso" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/thumb/{{$curso->centro->center_image_portada}}');">
                            <a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif{{$cursolink}}" class="foto">
                @elseif(!is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                    @if(count($fotoscursoname))
                        <div class="mix @if($curso->centro->pais->name != 'España') {{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}} @else {{str_slug(Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name))}} @endif  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}} {{implode($edades,' ')}} {{str_slug($idiomascurso)}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}}">
                            @if($curso->course_promo == 1)
                                <div class="promo-lista">
                                    <i class="fa fa-asterisk"></i>
                                </div>
                            @endif
                            <div class="fotocurso" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/thumb/{{$fotoscursoname[rand(0,count($fotoscursoname)-1)]}}'); background-size: cover; background-position: center center;">
                                <a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif{{$cursolink}}" class="foto">
                    @elseif(!count($fotoscursoname) && count($fotoscentroname))
                        <div class="mix @if($curso->centro->pais->name != 'España') {{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}} @else {{str_slug(Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name))}} @endif  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}} {{implode($edades,' ')}} {{str_slug($idiomascurso)}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}}">
                            @if($curso->course_promo == 1)
                                <div class="promo-lista">
                                    <i class="fa fa-asterisk"></i>
                                </div>
                            @endif
                            <div class="fotocurso" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/thumb/{{$fotoscentroname[rand(0,count($fotoscentroname)-1)]}}'); background-size: cover;">
                                <a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif{{$cursolink}}" class="foto">
                    @else
                        <div class="mix @if($curso->centro->pais->name != 'España') {{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}} @else {{str_slug(Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name))}} @endif  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}} {{implode($edades,' ')}} {{str_slug($idiomascurso)}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}}">
                            @if($curso->course_promo == 1)
                                <div class="promo-lista">
                                    <i class="fa fa-asterisk"></i>
                                </div>
                            @endif
                            <div class="fotocurso" style="background: #E5E5E5;">
                                <a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif{{$cursolink}}" class="foto">

                    @endif
                @endif
                        </a>
                    </div>

                    <div class="fichacurso">
                        <h4 class="nombrecurso">
                            <div class="row">
                                <small class="col-sm-12 nombrepais">@if($curso->centro->pais->name != 'España') {{Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name)}} @else {{Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name)}} @endif</small>

                                @if(count($curso->convocatoriasCerradas) && $curso->convocatoriasCerradas->contains('convocatory_semiopen', 0) && $curso->category_id == 2)
                                    <?php $plazasrestantes = 0; ?>
                                    @foreach($curso->convocatoriasCerradas->sortBy('convocatory_close_start_date')->sortBy('convocatory_close_duration_weeks') as $cc)
                                        @if($cc->activo_web == 1)
                                            {{--@if($cc->alojamiento_id == $aloja->id)--}}
                                            @if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 0)
                                                <?php $plazasrestantes += $cc->plazas_disponibles; ?>
                                            @endif
                                        @endif
                                    @endforeach
                                        @if($plazasrestantes <= 0 && $cerrado != 1)
                                            <small class="col-sm-6 pull-right text-right grupocerrado plazas">{{trans('web.grupocerrado')}}</small>
                                        @elseif($plazasrestantes != 0 && $plazasrestantes < 6 && $cerrado != 1)
                                            <small class="col-sm-6 pull-right text-right ultimasplazas plazas">{{trans('web.ultimasplazas')}}</small>
                                        @endif
                                @endif
                            </div>

                            <a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif{{$cursolink}}">
                            {!! Traductor::getWeb(App::getLocale(), 'Curso', 'name', $curso->id, $curso->course_name) !!}
                            </a>
                        </h4>

                        <p class="edades">{!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_age_range', $curso->id, $curso->course_age_range) !!}</p>

                        <div class="separator-ficha"></div>

                        <?php $alojas = array(); ?>
                        @foreach($curso->alojamientos as $alojamiento)
                            <?php $alojas[] = Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $alojamiento->tipo->id, $alojamiento->tipo->accommodation_type_name); ?>
                        @endforeach
                        <p class="cursoalojas text-capitalize">{{implode(array_unique($alojas),', ')}}</p>

                            @if(count($curso->especialidades))
                                <div class="especialidades pull-left"><span>

                                        <?php $anteriorespe = ''; ?>
                                        <?php $i = 1; ?>

                                        @foreach($curso->especialidades->sortBy('name') as $e)

                                            @if($e->especialidad->id != $anteriorespe || $anteriorespe == null)
                                                {!!Traductor::getWeb(App::getLocale(), 'Especialidad', 'name', $e->especialidad->id, $e->especialidad->name)!!}:
                                            @endif

                                            @if(($e->especialidad->id == $anteriorespe || $i == 1) && count($curso->especialidades) != $i)
                                                {!!Traductor::getWeb(App::getLocale(), 'Subespecialidad', 'name', $e->subespecialidad_id, $e->SubespecialidadesName)!!},
                                            @endif

                                            @if(count($curso->especialidades) == $i)
                                                {!!Traductor::getWeb(App::getLocale(), 'Subespecialidad', 'name', $e->subespecialidad_id, $e->SubespecialidadesName)!!}.<br />
                                            @endif

                                            <?php $anteriorespe = $e->especialidad->id; ?>
                                            <?php $i++; ?>

                                        @endforeach
                                </span></div>

                            @endif
                    </div>
                </div>
            @endforeach
            <div class="gap"></div>
            <div class="gap"></div><div class="gap"></div>
        </div>
    </div>
</div>
</div>
</div>