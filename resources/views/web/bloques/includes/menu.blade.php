<nav id="cd-lateral-nav">
    <ul class="cd-navigation">
        @foreach(\VCN\Models\Categoria::where(function ($query) {
                 return $query
                 ->where('propietario', 0)
                 ->orWhere('propietario', ConfigHelper::config('propietario'));
                 })->orderBy('id','ASC')->get() as $cat)

            @if(\VCN\Models\Cursos\Curso::where('category_id', $cat->id)->where('activo_web', 1)
                ->where(function ($query) {
                    return $query
                    ->where('propietario', 0)
                    ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })->count())
                <li class="item-has-children">
                    <a href="#0">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a>
                    @if(count($cat->subcategorias))
                        <ul class="sub-menu">
                            @foreach($cat->subcategorias as $subcat)
                                @if(\VCN\Models\Cursos\Curso::where('subcategory_id', $subcat->id)->where('activo_web', 1)
                                ->where(function ($query) {
                                return $query
                                    ->where('propietario', 0)
                                    ->orWhere('propietario', ConfigHelper::config('propietario'));
                                    })->count())

                                    <li><a href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}/{!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $subcat->id, $subcat->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcat->id, $subcat->name_web)!!}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    @endif
                </li> <!-- item-has-children -->
            @endif
        @endforeach


        <ul class="cd-navigation cd-single-item-wrapper">
            @foreach(VCN\Models\CMS\Pagina::WhereIn('propietario', [0,ConfigHelper::config('propietario')])->where('menu',1)->get() as $pagina)
                <li><a href="/{!! Traductor::getWeb(App::getLocale(), 'Pagina', 'url', $pagina->id, $pagina->url) !!}.html">{!! Traductor::getWeb(App::getLocale(), 'Pagina', 'titulo', $pagina->id, $pagina->titulo) !!}</a></li>
            @endforeach
            <li><a href="/blog" target="_blank">Blog</a></li>
        </ul>
        <div class="cd-navigation socials">
            <a href="https://www.facebook.com/studyfuera" target="_blank"><i class="fa fa-facebook-square"></i> Facebook</a>
        </div>

    </ul> <!-- primary-nav -->
</nav> <!-- cd-nav -->

<div id="cd-search" class="cd-search">
    <form action="{{route('web.buscar')}}" method="post" enctype="multipart/form-data" class="searchbox" autocomplete="off" >
        <input type="text" placeholder="{{trans('web.buscar')}}" name="search"  id="search" class="searchbox-input" required>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    </form>
</div>