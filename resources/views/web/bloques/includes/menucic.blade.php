<nav id="cd-lateral-nav">
    <ul class="cd-navigation">

        @foreach(VCN\Models\CMS\CategoriaWeb::arbol() as $menucat)
            @if(isset($menucat['subcategorias']) && count($menucat['subcategorias']))
                <li class="item-has-children">
                    <a href="">{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $menucat['id'], $menucat['titulo'])!!}</a>
                    <ul class="sub-menu">
                        @foreach($menucat['subcategorias'] as $subcat)
                            <?php
                            if($subcat['es_link'] == 1){
                                $url = (App::getLocale() != ConfigHelper::config('idioma') ? '/'.App::getLocale().'/' : '/') .Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'link', $subcat['id'], $subcat['link']);
                            }else{
                                $url = (App::getLocale() != ConfigHelper::config('idioma') ? '/'.App::getLocale().'/' : '/') .Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $menucat['id'], $menucat['seo_url']).'/'.Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $subcat['id'], $subcat['seo_url']);
                            }

                            if ($subcat['link_blank'] == 1){
                                $link = 'href="'.$url.'" target="_blank"';
                            }else{
                                $link = 'href="'.$url.'"';
                            }
                            ?>
                            <li><a {!! $link !!}>@if($subcat['titulo'] != ''){!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcat['id'], $subcat['titulo'])!!} @else {!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'name', $subcat['id'], $subcat['name'])!!}@endif</a></li>
                        @endforeach
                    </ul>
                </li> <!-- item-has-children -->
            @else
                <li class="item">
                    <?php
                        if($menucat['es_link'] == 1){
                            $url = (App::getLocale() != ConfigHelper::config('idioma') ? '/'.App::getLocale().'/' : '/') .Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'link', $menucat['id'], $menucat['link']);
                        }else{
                            $url = (App::getLocale() != ConfigHelper::config('idioma') ? '/'.App::getLocale().'/' : '/') .Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $menucat['id'], $menucat['seo_url']);
                        }
                        if ($menucat['link_blank'] == 1){
                            $link = 'href="'.$url.'" target="_blank"';
                        }else{
                            $link = 'href="'.$url.'"';
                        }
                    ?>
                    <a {!! $link !!}>{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $menucat['id'], $menucat['titulo'])!!}</a>
                </li> <!-- item-has-children -->
            @endif
        @endforeach


        <ul class="cd-navigation cd-single-item-wrapper">
            @foreach(VCN\Models\CMS\Pagina::WhereIn('propietario', [0,ConfigHelper::config('propietario')])->where('menu',1)->get() as $pagina)
                <li><a href="/{!! Traductor::getWeb(App::getLocale(), 'Pagina', 'url', $pagina->id, $pagina->url) !!}.html">{!! Traductor::getWeb(App::getLocale(), 'Pagina', 'titulo', $pagina->id, $pagina->titulo) !!}</a></li>
            @endforeach
            {{--<li><a href="#" target="_blank"><strong>/{{trans('web.quienes-somos')}}</strong></a></li>--}}
            {{--<li><a href="#" target="_blank"><strong>/{{trans('web.trabajo')}}</strong></a></li>--}}
            <li><a href="//viatges.iccic.edu/be-cicviatges/" target="_blank">Be CIC Viatges. CONCURSOS</a></li>
            <li><a href="http://www.landedblog.com/iccic/" target="_blank">{!! trans('web.landedslogan') !!}</a></li>
        </ul>

        <ul class="cd-navigation cd-single-item-wrapper">
            {{--<li><a href="/{{trans('web.inscripcion-slug')}}">{{trans('web.inscripcion')}}</a></li>--}}
        </ul>

        <div class="cd-navigation socials">
            <a href="https://es-es.facebook.com/CIC-Escola-dIdiomes-1426848570864710/" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="//twitter.com/cicviatges" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="https://www.youtube.com/channel/UCZXmgNRCnJiBBy_eWZ5o0Mg" target="_blank"><i class="fa fa-youtube-play"></i></a>
            <a href="https://instagram.com/cicviatges" target="_blank"><i class="fa fa-instagram"></i></a>
        </div>
        <ul class="cd-navigation cd-single-item-wrapper">
            @if (Auth::guest())
                <li><a href="//{{ConfigHelper::config('sufijo')}}.estudiaryviajar.com/auth/login"><i class="fa fa-lock"></i> {{trans('web.aclientes')}}</a></li>
            @else
                <li><a href="//{{ConfigHelper::config('sufijo')}}.estudiaryviajar.com/manage"><i class="fa fa-lock"></i> {{trans('web.aclientes')}}</a></li>
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="ifa fa fa-sign-out fa-fw"></i> {{ trans('web.salir') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                </li>
            @endif


                @if(isset($weblangs))
                    @foreach ($weblangs as $weblang => $linklang)
                        @if($weblang != App::getLocale())
                            <li><a href="{{$linklang}}" class="language" data-idioma="{{$weblang}}"/><i class="fa fa-globe"></i> <span>{{ConfigHelper::getIdiomaWeb($weblang)}}</span></a></li>
                        @endif
                    @endforeach
                @else
                    @if(App::getLocale() == 'es')
                        <li><a href="/ca" class="language" data-idioma="ca"><i class="fa fa-globe"></i> <span>{{ConfigHelper::getIdiomaWeb('ca')}}</span></a></li>
                    @elseif(App::getLocale() == 'ca')
                        <li><a href="/es" class="language" data-idioma="es"><i class="fa fa-globe"></i> <span>{{ConfigHelper::getIdiomaWeb('es')}}</span></a></li>
                    @endif
                @endif
        </ul>


    </ul> <!-- primary-nav -->
</nav> <!-- cd-nav -->

<div id="cd-search" class="cd-search">
    <form action="{{route('web.buscar')}}" method="post" enctype="multipart/form-data" class="searchbox" autocomplete="off" >
        <input type="text" placeholder="{{trans('web.buscar')}}" name="search"  id="search" class="searchbox-input" required>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    </form>
</div>