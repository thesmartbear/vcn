<div id="sidebar" class="lista">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
            <div class="pull-left">
                <div class="widget clearfix filtros">
                    <form id="Filters">
                        
                        @if($menor == 1)
                            <fieldset class="filter-group checkboxes">
                                <div class="btn-group">
                                    <button class="btn btn-primary">{{trans('web.edad')}}</button>
                                    <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                    <ul class="dropdown-menu bullet pull-center">
                                        <li><input type="checkbox" id="{{trans('web.edad')}}-all" value=".{{trans('web.edad')}}-all" class="all" checked="checked"><label for="{{trans('web.edad')}}-all">{{trans('web.todas')}}</label></li>
                                        @for($edad=6; $edad <=19; $edad++)
                                            <li><input type="checkbox" id="{{trans('web.edad')}}-{{$edad}}" value=".{{trans('web.edad')}}-{{$edad}}"><label for="{{trans('web.edad')}}-{{$edad}}">{{$edad}}</label></li>
                                        @endfor
                                    </ul>
                                </div>
                            </fieldset>
                        @endif

                        <?php $paises = array(); ?>
                        <?php $ciudad = false; ?>
                        <fieldset class="filter-group checkboxes">
                            
                            @foreach($cursos as $c)
                                @foreach(\VCN\Models\Centros\Centro::distinct()->where('id', $c->centro->id)->get() as $p)
                                    @if($p->pais->name != 'España')
                                        <?php $paises[] = Traductor::getWeb(App::getLocale(), 'Pais', 'name', $p->pais->id, $p->pais->name); ?>
                                    @else
                                        <?php $ciudad = true; ?>
                                        <?php $paises[] = Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $p->ciudad->id, $p->ciudad->city_name); ?>
                                    @endif
                                @endforeach
                            @endforeach
                            <div class="btn-group">
                                <button class="btn btn-primary">@if($ciudad == false) {{trans('web.pais')}} @else {{trans('web.ciudad')}} @endif</button>
                                <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                <ul class="dropdown-menu bullet pull-center">
                                        <li><input type="checkbox" id="pais-all" value=".pais-all" class="all" checked="checked"><label for="pais-all">{{trans('web.todos')}}</label></li>
                                    @foreach(array_unique($paises) as $pais)
                                        <li><input type="checkbox" id="{{str_slug($pais)}}" value=".{{str_slug($pais)}}"><label for="{{str_slug($pais)}}">{{$pais}}</label></li>
                                    @endforeach
                                </ul>
                            </div>
                        </fieldset>

                        <?php $alojas = array(); ?>
                        <fieldset class="filter-group checkboxes">
                            @foreach($cursos as $c)
                                @foreach($c->alojamientos as $alojamiento)
                                    <?php $alojas[] = Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $alojamiento->tipo->id, $alojamiento->tipo->accommodation_type_name); ?>
                                @endforeach
                            @endforeach
                            <div class="btn-group">
                                <button class="btn btn-primary">{{trans('web.alojamiento')}}</button>
                                <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                <ul class="dropdown-menu bullet pull-center">
                                    <li><input type="checkbox" id="alojamiento-all" value=".alojamiento-all" class="all" checked="checked"><label for="alojamiento-all">{{trans('web.todos')}}</label></li>
                                    @foreach(array_unique($alojas) as $aloja)
                                        <li><input type="checkbox" id="{{str_slug($aloja)}}" value=".{{str_slug($aloja)}}"><label for="{{str_slug($aloja)}}">{{$aloja}}</label></li>
                                    @endforeach
                                </ul>
                            </div>
                        </fieldset>

                        <?php $especialidades = array(); ?>
                        @foreach($cursos as $c)
                            @foreach(\VCN\Models\Cursos\CursoEspecialidad::distinct()->where('curso_id',$c->id)->get() as $e)
                                <?php $especialidades[] = Traductor::getWeb(App::getLocale(), 'Especialidad', 'name', $e->especialidad->id, $e->especialidad->name); ?>
                            @endforeach
                        @endforeach
                        @if(count($especialidades))
                            <fieldset class="filter-group checkboxes">
                                <div class="btn-group">
                                    <button class="btn btn-primary">{{trans('web.especialidad')}}</button>
                                    <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                    <ul class="dropdown-menu bullet pull-center">
                                        <li><input type="checkbox" id="especialidades-all" value=".especialidades-all" class="all" checked="checked"><label for="especialidades-all">{{trans('web.todas')}}</label></li>
                                        @foreach(array_unique($especialidades) as $especialidad)
                                            <li><input type="checkbox" id="{{str_slug($especialidad)}}" value=".{{str_slug($especialidad)}}"><label for="{{str_slug($especialidad)}}">{{$especialidad}}</label></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </fieldset>
                        @endif

                        @if($idiomas && $idiomas->count() > 1)
                            <fieldset class="filter-group checkboxes">
                                <div class="btn-group">
                                    <button class="btn btn-primary">Idioma</button>
                                    <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                    <ul class="dropdown-menu bullet pull-center">
                                        <li><input type="checkbox" id="idioma-all" value=".idioma-all" class="all" checked="checked"><label for="idioma-all">{{trans('web.todos')}}</label></li>
                                        @foreach($cursos->pluck('course_language')->unique()->toArray() as $idioma)
                                            @if(in_array($idioma,$idiomas))
                                                <li><input type="checkbox" id="{{str_slug($idioma)}}" value=".{{str_slug($idioma)}}"><label for="{{str_slug($idioma)}}">{{trans('web.idiomas.'.$idioma)}}</label></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </fieldset>
                        @endif

                        {{-- edad --}}
                        

                        {{-- subcategorias --}}
                        

                    </form>
                </div>
            </div>
            <div class="controls pull-right text-right">
                    <div class="total-cursos"><i class="fa fa-eye"></i> <span></span></div>
                    <button id="viewcolssmall" class="layout active"><i class="fa fa-th"></i></button>
                    <button id="viewcolsbig" class="layout"><i class="fa fa-th-large"></i></button>
                    <button id="viewlist" class="layout"><i class="fa fa-list"></i></button>
                    <div class="separator"></div>
                    <button id="sortPromo" class="order active"><i class="fa fa-asterisk"></i></button>
                    <button id="sortName" class="order"><i class="fa fa-sort-alpha-asc"></i></button>
                    <button id="sortPais" class="order"><i class="fa fa-map-marker"></i></button>
                <!--
                <div class="col-sm-2 pull-right">
                    <button id="Reset" class="btn-block">ver todos</button>
                </div>
                -->
            </div>
            </div>

        </div>
    </div>
</div>