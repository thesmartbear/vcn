@extends('web.britishsummerv2.baseweb')

@section('extra_head')
<!-- Color style -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/{{$clase}}.css" rel="stylesheet">
<link href="/assets/{{ConfigHelper::config('tema')}}/js/bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet">
<style>
    button{
        display: inline-block;
        vertical-align: top;
        padding: .4em .8em;
        margin: 0;
        background: #68b8c4;
        border: 0;
        color: #333;
        font-size: 16px;
        font-weight: 700;
        border-radius: 4px;
        cursor: pointer;
    }

    button:focus{
        outline: 0 none;
    }

    .controls{

    }

    fieldset{
        display: inline-block;
        vertical-align: top;
    }

    .checkbox{
        display: block;
        position: relative;
        cursor: pointer;
        margin-bottom: 8px;
        margin-right: 15px;
    }

    .checkbox input[type="checkbox"]{
        position: absolute;
        display: inline-block;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        cursor: pointer;
        margin: 0;
        opacity: 0;
        z-index: 1;
    }

    .checkbox label{
        display: inline-block;
        vertical-align: top;
        text-align: left;
        padding-left: 1.5em;
    }

    .checkbox label:before,
    .checkbox label:after{
        content: '';
        display: block;
        position: absolute;
    }

    .checkbox label:before{
        left: 0;
        top: 0;
        width: 18px;
        height: 18px;
        margin-right: 10px;
        background: #ddd;
        border-radius: 3px;
    }

    .checkbox label:after{
        content: '';
        position: absolute;
        top: 4px;
        left: 4px;
        width: 10px;
        height: 10px;
        border-radius: 2px;
        background: #68b8c4;
        opacity: 0;
        pointer-events: none;
    }

    .checkbox input:checked ~ label:after{
        opacity: 1;
    }

    .checkbox input:focus ~ label:before{
        background: #eee;
    }

    /**
     * Container/Target Styles
     */

    .slides{
        width: 100%;
    }
    #cursos-lista{
        padding: 0;
        min-height: 400px;
        text-align: justify;
        position: relative;
        overflow: hidden;
    }

    #cursos-lista .mix,
    #cursos-lista .gap{
        width: 32%;
        height: 350px;
        display: inline-block;
        margin-bottom: 1%;
        -webkit-transition: all 500ms ease;
        -moz-transition: all 500ms ease;
        -ms-transition: all 500ms ease;
        -o-transition: all 500ms ease;
        transition: all 500ms ease;
        -webkit-transform-origin: left top;
        -moz-transform-origin: left top;
        -ms-transform-origin: left top;
        -o-transform-origin: left top;
        transform-origin: left top;
    }

    #cursos-lista .mix{
        width: 32%;
        margin-bottom: 1%;
        background: #FBFBFB;
        display: none;
        overflow: hidden;
    }



    #cursos-lista .mix.one,
    #cursos-lista .gap.one{
        width: 100%;
        height: 210px;
    }

    #cursos-lista .mix.two,
    #cursos-lista .gap.two{
        width: 49%;
        height: 500px;
    }

    #cursos-lista .fotocurso {
        width: 100%;
        position: relative;
        height: 60% !important;
        display: block !important;
        z-index: 1;
        margin-left: 0;
        background-position: center center;

    }

    #cursos-lista .one .fotocurso {
        position: absolute;
        left: 0;
        height: 210px !important;
        width: 32%;
        display: block;
        z-index: 1;
    }

    #cursos-lista .two .fotocurso {
        height: 60% !important;
    }

    #cursos-lista .fichacurso {
        display: block;
        padding: 10px 20px 10px 20px;
        height: 140px;
        position: relative;
        z-index: 4;
        text-align: left;

    }
    @media screen and (max-width: 1200px){
        #cursos-lista .mix,
        #cursos-lista .gap{
            width: 32%;
        }
    }
    #cursos-lista .one .fichacurso {
        margin-left: 32%;
        margin-top: 0;
    }

    #cursos-lista .two .fichacurso {
        /*margin-top: -480px;*/
    }

    #cursos-lista .fichacurso h4 {
        font-size: 24px;
    }
    #cursos-lista .fichacurso p {
        color: #222;
    }

    #cursos-lista .gap{
      display: inline-block;
    }
    #cursos-lista .gap:before{
      content: '';
    }

     /**
     * Fail message styles
     */

    #cursos-lista .fail-message{
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        text-align: center;
        opacity: 0;
        pointer-events: none;

        -webkit-transition: 150ms;
        -moz-transition: 150ms;
        transition: 150ms;
    }

    #cursos-lista .fail-message:before{
        content: '';
        display: inline-block;
        vertical-align: middle;
        height: 100%;
    }

    #cursos-lista .fail-message span{
        display: inline-block;
        vertical-align: middle;
        font-size: 20px;
        font-weight: 700;
    }

    #cursos-lista.fail .fail-message{
        opacity: 1;
        pointer-events: auto;
    }

</style>
@stop


@section('container')
    <div class="headerbg" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/{{rand(1, 3)}}.png') repeat;">
        <div class="container" id="header" style="position: relative;">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="titulo">
                        <h1 class="slogan">
                                <span></span>
                                @if($categorianame != '')
                                    <span>
                                        <ul class="breadcrumb">
                                            <li class="active"><a href="../">{{$categorianame}}</a></li>
                                            <li><a href="../{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $subcategoria->id, $subcategoria->slug) !!}">{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}</a></li>
                                        </ul>
                                    </span>
                                    {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoriadet->id, $subcategoriadet->name_web) !!}

                                    <small>
                                        @if(Lang::has('web.categorias.'.$subcategoria->slug.'-desc')){!! trans('web.categorias.'.$subcategoria->slug.'-desc') !!}@endif
                                    </small>
                                @elseif($subcategoriadet == '')
                                    <span>
                                       <ul class="breadcrumb">
                                           <li class="active"><a href="/{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $categoria->id, $categoria->slug) !!}">{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!}</a></li>
                                       </ul>
                                    </span>
                                    {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}

                                    <small>
                                        @if(Lang::has('web.categorias.'.$subcategoria->slug.'-desc')){!! trans('web.categorias.'.$subcategoria->slug.'-desc') !!}@endif
                                    </small>
                                @else
                                    <span>
                                       <ul class="breadcrumb">
                                            <li class="active"><a href="/{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $categoria->id, $categoria->slug) !!}">{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!}</a></li>
                                            <li><a href="/{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $categoria->id, $categoria->slug) !!}/{!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $subcategoria->id, $subcategoria->slug) !!}">{!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}</a></li>
                                        </ul>
                                    </span>
                                    {!! Traductor::getWeb(App::getLocale(), 'SubcategoriaDetalle', 'name_web', $subcategoriadet->id, $subcategoriadet->name_web) !!}

                                    <small>
                                    @if(Lang::has('web.categorias.'.$subcategoriadet->slug.'-desc')){!! trans('web.categorias.'.$subcategoriadet->slug.'-desc') !!}@endif
                                    </small>
                                @endif
                            </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="headerbgoverlay"></div>

    <div id="sidebar" class="lista">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="widget clearfix filtros">
                        <form class="controls" id="Filters">
                            <!-- We can add an unlimited number of "filter groups" using the following format: -->
                            <fieldset class="filter-group checkboxes">
                                <div class="checkbox">
                                    <input type="checkbox" value=".pais-all" class="all" checked="checked"/>
                                    <label>Todos los países</label>
                                </div>
                                @foreach($cursos as $c)
                                    @foreach(\VCN\Models\Centros\Centro::distinct()->where('id', $c->centro->id)->get() as $p)
                                        <?php $paises[] = \VCN\Models\Pais::distinct('name')->where('id', $p->pais->id)->first()->name; ?>
                                    @endforeach
                                @endforeach
                                @foreach(array_unique($paises) as $pais)
                                    <div class="checkbox">
                                        <input type="checkbox" value=".{{str_slug($pais)}}"/>
                                        <label>{{$pais}}</label>
                                    </div>
                                @endforeach
                                <select id="paises" multiple="multiple">
                                    @foreach(array_unique($paises) as $pais)
                                        <option value=".{{str_slug($pais)}}">{{$pais}}</option>
                                    @endforeach
                                </select>
                            </fieldset>

                            <fieldset class="filter-group checkboxes">
                                <div class="checkbox">
                                    <input type="checkbox" value=".especialidad-all" class="all" checked="checked"/>
                                    <label>Todas las especialidades</label>
                                </div>
                                @foreach($cursos as $c)
                                    @foreach(\VCN\Models\Cursos\CursoEspecialidad::distinct()->where('curso_id',$c->id)->get() as $e)
                                        <?php $especialidades[] = \VCN\Models\Especialidad::distinct('name')->where('id', $e->especialidad_id)->first()->name; ?>
                                    @endforeach
                                @endforeach
                                @foreach(array_unique($especialidades) as $especialidad)
                                    <div class="checkbox">
                                        <input type="checkbox" value=".{{str_slug($especialidad)}}"/>
                                        <label>{{$especialidad}}</label>
                                    </div>
                                @endforeach
                            </fieldset>

                            <fieldset class="filter-group checkboxes">
                                <div class="checkbox">
                                    <input type="checkbox" value=".alojamiento-all" class="all" checked="checked" />
                                    <label>Todos alojamientos</label>
                                </div>
                                @foreach($cursos as $c)
                                    @foreach($c->alojamientos as $alojamiento)
                                        <? $alojas[] = $alojamiento->tipo->accommodation_type_name; ?>
                                    @endforeach
                                @endforeach
                                @foreach(array_unique($alojas) as $aloja)
                                    <div class="checkbox">
                                        <input type="checkbox" value=".{{str_slug($aloja)}}"/>
                                        <label>{{$aloja}}</label>
                                    </div>
                                @endforeach
                                <select id="alojas" multiple="multiple">
                                    @foreach(array_unique($alojas) as $aloja)
                                        <option value=".{{str_slug($aloja)}}">{{$aloja}}</option>
                                    @endforeach
                                </select>
                            </fieldset>

                            <button id="Reset">Sin Filtros</button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-3 pull-right">
                    <div class="row">
                        <div class="col-sm-12">
                            <button id="viewcolssmall" class="layout active"><i class="fa fa-th"></i></button>
                            <button id="viewcolsbig" class="layout"><i class="fa fa-th-large"></i></button>
                            <button id="viewlist" class="layout"><i class="fa fa-list"></i></button>
                        </div>
                        <div class="col-sm-12">
                            <div class="total-cursos"><span></span> cursos</div>
                            <button id="sortName">Ordenar Nombre</button>
                            <button id="sortPromo">Ordenar Promoción</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
    <main class="cd-main-content lista">

        <div class="container" id="contenido">

            <div class="row">
                <div class="col-sm-12 col-xs-12 {{$clase}} categorias">
                    <div class="introseccion">
                        @if($subcategoriadet != '')
                            @if(Lang::has('web.categorias.'.$subcategoriadet->slug.'-intro')){!! trans('web.categorias.'.$subcategoriadet->slug.'-intro') !!}@endif
                        @else
                            @if(Lang::has('web.categorias.'.$subcategoria->slug.'-intro')){!! trans('web.categorias.'.$subcategoria->slug.'-intro') !!}@endif
                        @endif
                    </div>
                    <div id="programas">
                        @if($categorianame != '')
                            <div id="cursos-lista">
                                <div class="fail-message"><span>Ningún programa coicide con los filtros aplicados</span></div>
                                <div class="slides">
                                    @foreach($cursos as $curso)
                                        <?php
                                        $fotoscentro = '';
                                        $fotoscentroname = array();
                                        $path = public_path() ."/assets/uploads/center/" . $curso->centro->center_images;
                                        $folder = "/assets/uploads/center/" . $curso->centro->center_images;

                                        if (is_dir($path)) {
                                            $results = scandir($path);
                                            foreach ($results as $result) {
                                                if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                                                $file = $path . '/' . $result;

                                                if (is_file($file)) {
                                                    $fotoscentroname[] = $result;

                                                }
                                            }
                                        }
                                        ?>
                                        <?php
                                        $fotoscurso = '';
                                        $fotoscursoname = array();
                                        $path = public_path() ."/assets/uploads/course/" . $curso->course_images;
                                        $folder = "/assets/uploads/course/" . $curso->course_images;

                                        if (is_dir($path)) {
                                            $results = scandir($path);
                                            foreach ($results as $result) {
                                                if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                                                $file = $path . '/' . $result;

                                                if (is_file($file)) {
                                                    $fotoscursoname[] = $result;
                                                }
                                            }
                                        }
                                        ?>

                                        <? $espes = array(); ?>
                                        @foreach($curso->especialidades as $e)
                                            <? $espes[] = str_slug($e->especialidad->name); ?>
                                        @endforeach

                                        @foreach($curso->alojamientos as $alojamiento)
                                            <? $alojascurso[] = str_slug($alojamiento->tipo->accommodation_type_name); ?>
                                        @endforeach




                                        @if(is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada))
                                            <div class="mix {{str_slug($curso->centro->pais->name)}} {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}">
                                                <div class="fotocurso" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/{{$curso->image_portada}}');">
                                        @elseif(is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                                            <div class="mix {{str_slug($curso->centro->pais->name)}}  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}">
                                                <div class="fotocurso" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/{{$curso->centro->center_image_portada}}');">
                                        @elseif(!is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                                            @if(count($fotoscursoname))
                                                <div class="mix {{str_slug($curso->centro->pais->name)}}  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}">
                                                    <div class="fotocurso" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/{{$fotoscursoname[rand(0,count($fotoscursoname)-1)]}}'); background-size: cover; background-position: center center;">
                                            @elseif(!count($fotoscursoname) && count($fotoscentroname))
                                                <div class="mix {{str_slug($curso->centro->pais->name)}}  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}">
                                                    <div class="fotocurso" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/{{$fotoscentroname[rand(0,count($fotoscentroname)-1)]}}'); background-size: cover;">
                                            @else
                                                <div class="mix {{str_slug($curso->centro->pais->name)}}  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}">
                                                    <div class="fotocurso" style="background: #E5E5E5;">
                                            @endif
                                        @endif
                                            </div>
                                            <div class="fichacurso">
                                                <a href="../{!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug) !!}.html">
                                                    <h4>{!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_name', $curso->id, $curso->course_name) !!}</h4>
                                                        <p>{{$curso->centro->pais->name}}</p>
                                                        <p class="especialidades">
                                                            @foreach($curso->especialidades as $e)
                                                                {{--$e->especialidad->name--}}
                                                                <span>{{$e->SubespecialidadesName}}</span>
                                                            @endforeach
                                                        </p>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="gap"></div>
                                    <div class="gap"></div><div class="gap"></div>
                                </div>
                            </div>
                        @else
                            <div id="cursos-lista" class="container">
                                <div class="fail-message"><span>Ningún programa coicide con los filtros aplicados</span></div>
                                <div class="slides">
                                    @foreach($cursos as $curso)
                                        <?php
                                        $fotoscentro = '';
                                        $fotoscentroname = array();
                                        $path = public_path() ."/assets/uploads/center/" . $curso->centro->center_images;
                                        $folder = "/assets/uploads/center/" . $curso->centro->center_images;

                                        if (is_dir($path)) {
                                            $results = scandir($path);
                                            foreach ($results as $result) {
                                                if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                                                $file = $path . '/' . $result;

                                                if (is_file($file)) {
                                                    $fotoscentroname[] = $result;

                                                }
                                            }
                                        }
                                        ?>
                                        <?php
                                        $fotoscurso = '';
                                        $fotoscursoname = array();
                                        $path = public_path() ."/assets/uploads/course/" . $curso->course_images;
                                        $folder = "/assets/uploads/course/" . $curso->course_images;

                                        if (is_dir($path)) {
                                            $results = scandir($path);
                                            foreach ($results as $result) {
                                                if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                                                $file = $path . '/' . $result;

                                                if (is_file($file)) {
                                                    $fotoscursoname[] = $result;
                                                }
                                            }
                                        }
                                        ?>
                                        @if(is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada))
                                            <div class="mix {{str_slug($curso->centro->pais->name)}}">
                                                <div class="fotocurso" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/{{$curso->image_portada}}');">
                                        @elseif(is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                                            <div class="mix {{str_slug($curso->centro->pais->name)}}">
                                                <div class="fotocurso" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/{{$curso->centro->center_image_portada}}');">
                                        @elseif(!is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                                            @if(count($fotoscursoname))
                                                <div class="mix {{str_slug($curso->centro->pais->name)}}">
                                                    <div class="fotocurso" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/{{$fotoscursoname[rand(0,count($fotoscursoname)-1)]}}'); background-size: cover; background-position: center center;">
                                            @elseif(!count($fotoscursoname) && count($fotoscentroname))
                                                <div class="mix {{str_slug($curso->centro->pais->name)}}">
                                                    <div class="fotocurso" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/{{$fotoscentroname[rand(0,count($fotoscentroname)-1)]}}'); background-size: cover;">
                                            @else
                                                <div class="mix {{str_slug($curso->centro->pais->name)}}">
                                                    <div class="fotocurso" style="background: #E5E5E5;">
                                            @endif
                                        @endif
                                            </div>
                                            <div class="fichacurso">
                                                <a href="/{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $categoria->id, $categoria->slug) !!}/{!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug) !!}.html">
                                                    <h4>{!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_name', $curso->id, $curso->course_name) !!}</h4>
                                                        <p>{{$curso->centro->pais->name}}</p>
                                                        <p class="especialidades">
                                                            @foreach($curso->especialidades as $e)
                                                                {{--$e->especialidad->name--}}
                                                                <span>{{$e->SubespecialidadesName}}</span>
                                                            @endforeach
                                                        </p>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="gap"></div>
                                    <div class="gap"></div><div class="gap"></div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    @include('web.britishsummerv2.includes.copyright')
                </div>
            </div>
            </div>
    </main>

<!-- Modal -->
<div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
            </div>
            <div class="modal-body">
                <div id="respuesta">
                    <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                    <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                        <div class="msg"></div>
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="form-group">
                                    <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                    <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                    <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                    <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                    <input type="hidden" id="curso" value="@if($categorianame != ''){{$categorianame}} - {!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!} @if($subcategoriadet != '')- {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoriadet->id, $subcategoriadet->name_web) !!}@endif @else{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!} - {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}@if($subcategoriadet != '') - {!! Traductor::getWeb(App::getLocale(), 'SubcategoriaDetalle', 'name_web', $subcategoriadet->id, $subcategoriadet->name_web) !!}@endif @endif">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>

                <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop


@section('extra_footer')

<script src="//cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js" type="text/javascript"></script>
<script src="/assets/{{ConfigHelper::config('tema')}}/js/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
<script src="/assets/{{ConfigHelper::config('tema')}}/js/filters.js" type="text/javascript"></script>


<script>
    $('#viewlist').click( function(){
        if(!$('.mix').hasClass('one')) {
            $('.fichacurso').hide().delay(300).fadeIn(200);
        }
        $('.mix, .gap').addClass('one').removeClass('two');
        $('.layout').siblings().removeClass('active');
        $(this).addClass('active');
    });
    $('#viewcolsbig').click( function(){
        $('.mix, .gap').addClass('two').removeClass('one');
        $('.layout').removeClass('active');
        $(this).addClass('active');
    });
    $('#viewcolssmall').click( function(){
        $('.mix, .gap').removeClass('two').removeClass('one');
        $('.layout').removeClass('active');
        $(this).addClass('active');
    });
    $(document).ready(function () {
        $('.lista #contenido').css({'padding-top': $('#sidebar').height()+'px'});

        $('#alojas').multiselect({
            includeSelectAllOption: true,
            selectAllText: 'Todos los alojamientos',
            selectAllValue: '.alojamientos-all',
            onInitialized: function(select, container) {
                $('.filter-group').find('input[value=".alojamientos-all"]').addClass('all');
                mezclar();
            }

        });

        $('#paises').multiselect({
            includeSelectAllOption: true,
            selectAllText: 'Todos los paises',
            selectAllValue: '.paises-all',
            onInitialized: function(select, container) {
                $('.filter-group').find('input[value=".paises-all"]').addClass('all');
                mezclar();
            }

        });
    });


    $('#sortPromo').click(function(){
        $('#cursos-lista').mixItUp('sort', 'promo:desc');
    });
    $('#sortName').click(function(){
        $('#cursos-lista').mixItUp('sort', 'name:asc');
    });
    $('#sortPais').click(function(){
        $('#cursos-lista').mixItUp('sort', 'pais:asc');
    });

</script>

@stop


