@extends('web.britishsummerv2.baseweb')

@section('title')
    @if($categorianame != '')
        {{ConfigHelper::config('nombre')}} - {{$categorianame}}. {!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}
    @else
        {{ConfigHelper::config('nombre')}} - {!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!}. {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}
    @endif
@stop

@section('extra_meta')
    @if($categorianame != '')
        <meta name="DC.title" lang="{{App::getLocale()}}" content="{{ConfigHelper::config('nombre')}} - {{$categorianame}}. {!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}" />
        <meta name="subject" lang="{{App::getLocale()}}" content="{{$categorianame}}. {!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}" />
        <meta name="description" lang="{{App::getLocale()}}" content="{{$categorianame}}. {!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}@if(Lang::has('web.categorias.'.$subcategoria->slug.'-desc')). {!! strip_tags(trans('web.categorias.'.$subcategoria->slug.'-desc')) !!}@endif" />
    @else
        <meta name="DC.title" lang="{{App::getLocale()}}" content="{{ConfigHelper::config('nombre')}} - {!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!}. {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}" />
        <meta name="subject" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!}. {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}" />
        <meta name="description" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!}. {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}@if(Lang::has('web.categorias.'.$subcategoria->slug.'-desc')). {!! strip_tags(trans('web.categorias.'.$subcategoria->slug.'-desc')) !!}@endif" />
    @endif



    <meta name="Keywords" lang="{{App::getLocale()}}" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')
    <!-- Color style -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/{{$clase}}.css" rel="stylesheet">
@stop


@section('container')
    <div class="headerbg" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/{{rand(1, 3)}}.png') repeat;">
        <div class="container" id="header" style="position: relative;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo">
                        <h1 class="slogan">
                                    @if($categorianame != '')
                                        <span>
                                            <ul class="breadcrumb">
                                                <li class="active"><a href="/{{$categorianameslug}}">{{$categorianame}}</a></li>
                                            </ul>
                                        </span>
                                        {!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}
                                    @else
                                        <span>
                                            <ul class="breadcrumb">
                                                <li class="active"><a href="/{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $categoria->id, $categoria->slug) !!}">{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!}</a></li>
                                            </ul>
                                        </span>
                                        {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}
                                    @endif

                                    <small>@if(Lang::has('web.categorias.'.$subcategoria->slug.'-desc')){!! trans('web.categorias.'.$subcategoria->slug.'-desc') !!}@endif</small>
                                </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="headerbgoverlay"></div>
    </div>
    <main class="cd-main-content">
        <div class="container" id="contenido">
            <div class="row">
                <div class="col-sm-8 col-xs-12 {{$clase}} categorias">
                    <div class="introseccion">
                            @if(Lang::has('web.categorias.'.$subcategoria->slug.'-intro')){!! trans('web.categorias.'.$subcategoria->slug.'-intro') !!}@endif
                    </div>

                            @if($categorianame != '')
                                @foreach($subcategoria->subcategorias as $sc)
                                    @if(\VCN\Models\Cursos\Curso::where('subcategory_id',$sc->id)->where('course_language', $operador, $idioma)->where('activo_web', 1)->count())
                                        <a href="{{Request::url()}}/{!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $sc->id, $sc->slug) !!}">
                                            <div class="col-sm-12 col-xs-12 categoria">
                                                <h3>
                                                    {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $sc->id, $sc->name_web) !!}
                                                    {{--<small>@if(Lang::has('web.categorias.'.$sc->slug.'-desc')){!! trans('web.categorias.'.$sc->slug.'-desc') !!}@endif</small>--}}
                                                </h3>
                                                <p><small>{{$numcursos = \VCN\Models\Cursos\Curso::where('subcategory_id',$sc->id)->where('course_language', $operador, $idioma)->where('activo_web', 1)->count()}} cursos</small></p>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            @else
                                @foreach($subcategoria->subcategoriasdet as $scd)
                                    @if(\VCN\Models\Cursos\Curso::where('subcategory_det_id',$scd->id)->where('course_language', $operador, $idioma)->where('activo_web', 1)->count())
                                        <a href="{{Request::url()}}/{!! Traductor::getWeb(App::getLocale(), 'SubcategoriaDetalle', 'slug', $scd->id, $scd->slug) !!}">
                                            <div class="col-sm-12 col-xs-12 categoria">
                                                <h3>
                                                    {!! Traductor::getWeb(App::getLocale(), 'SubcategoriaDetalle', 'name_web', $scd->id, $scd->name_web) !!}
                                                    {{--<small>@if(Lang::has('web.categorias.'.$scd->slug.'-desc')){!! trans('web.categorias.'.$scd->slug.'-desc') !!}@endif</small>--}}
                                                </h3>
                                                <p><small>{{$numcursos = \VCN\Models\Cursos\Curso::where('subcategory_det_id',$scd->id)->where('course_language', $operador, $idioma)->where('activo_web', 1)->count()}} cursos</small></p>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            @endif

                    </div>
                    <!-- Start right sidebar -->
                    <div class="col-sm-3 col-sm-offset-1 wrapper-bg" id="sidebar">
                        @if(Lang::has('web.categorias.'.$subcategoria->slug.'-sidebar'))
                            <div class="widget clearfix filtros">
                                <div class="box">
                                    {!! trans('web.categorias.'.$subcategoria->slug.'-sidebar') !!}
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- End right sidebar -->
                </div>

            <div class="row">
                    @include('web.britishsummerv2.includes.copyright')
            </div>
        </div>
    </main>

<!-- Modal -->
<div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
            </div>
            <div class="modal-body">
                <div id="respuesta">
                    <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                    <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                        <div class="msg"></div>
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="form-group">
                                    <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                    <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                    <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                    <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                    <input type="hidden" id="curso" value="@if($categorianame != ''){{$categorianame}} - {!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}@else{!! Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $categoria->id, $categoria->name_web) !!} - {!! Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcategoria->id, $subcategoria->name_web) !!}@endif">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>

                <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@stop

@section('extra_footer')

<script type="text/javascript">
        $(document).ready(function() {
            $('.msg').hide();
            $("#plusinfoenviar").click(function() {
                console.log('validar');
                if ($('#name').val() == ''){
                    $('.msg').html('Debes indicar un nombre de contacto');
                    $('.msg').show();
                    return false;
                }
                if ($('#email').val() == ''){
                    $('.msg').html('Debes indicar un teléfono o un email de contacto');
                    $('.msg').show();
                    return false;
                }

                if ($('#email').val() != ''){
                    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
                    if (re.test($('#name').val())) {
                        $('.msg').html('El email no tiene un formato correcto');
                        $('.msg').show();
                        return false
                    }

                }

                if (!/^([0-9])*$/.test($('#tel').val())){
                    $('.msg').html('El campo teléfono tiene que ser numérico');
                    $('.msg').show();
                    return false
                }


                post_data = {'name':$('#name').val(), 'tel':$('#tel').val(),'email':$('#email').val(), 'curso':$('#curso').val()};
                $.ajax({
                    type: "POST",
                    url: "/assets/{{ConfigHelper::config('tema')}}/includes/plusinfosend-{{ConfigHelper::config('sufijo')}}.php",
                    data: post_data,
                    success: function(msg){
                        $("#respuesta").html(msg);
                        $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                        ga('send', 'Solicitud', 'button', 'click', 'Aprender ingles en el extranjero', 1);
                    },
                    error: function(){
                        alert("error!!");
                    }
                });
            });



        });

        $('form input').blur(function () {
            $('.msg').hide();
        });


    </script>
@stop
