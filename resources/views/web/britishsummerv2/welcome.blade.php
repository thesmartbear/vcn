@extends('web.britishsummerv2.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{urldecode(trans('web.seo-'.ConfigHelper::config('sufijo').'.subject'))}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
        @endif
        @stop

        @section('extra_head')
                <!-- Link Swiper's CSS -->
        <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/bs.css" rel="stylesheet">
        <link href="/assets/{{ConfigHelper::config('tema')}}/css/swiper/swiper.min.css" rel="stylesheet">
    <style>
        @if(ConfigHelper::config('propietario') == 1)
        .swiper-slide{
            width: 17%;
        }
        .swiper-slide-double{
            width: 32%;
        }
        @elseif(ConfigHelper::config('propietario') == 2)
        .swiper-slide{
            width: 20%;
        }
        .swiper-slide-double{
            width: 40%;
        }
        @endif


        video#bgvid {
            position: fixed;
            top: 50%;
            left: 50%;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: -100;
            -ms-transform: translateX(-50%) translateY(-50%);
            -moz-transform: translateX(-50%) translateY(-50%);
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
            background: url('/assets/{{ConfigHelper::config('tema')}}/home/academico2017.jpg') no-repeat;
            background-size: cover;
        }

        @media screen and (max-device-width: 767px) {
            #bgvid {
                display: none;
            }
        }


    </style>

@stop

@section('container')
    <main class="cd-main-content" style="padding-top: 0;">

        <div class="bghome">
            {{--<div class="bg-slide" data-index="6" style="background: url('/assets/{{ConfigHelper::config('tema')}}/home/landedblog.jpg') no-repeat; background-position: center center; background-size: cover;"></div>--}}
            <div class="bg-slide" data-index="1" style="background: url('/assets/{{ConfigHelper::config('tema')}}/home/ingles2.jpg') no-repeat; background-position: center center; background-size: cover;"></div>
            <div class="bg-slide" data-index="2" style="background: url('/assets/{{ConfigHelper::config('tema')}}/home/otrosidiomas2.jpg') no-repeat; background-position: center center; background-size: cover;"></div>
            {{--<div class="bg-slide active" data-index="3" style="background: url('/assets/{{ConfigHelper::config('tema')}}/home/academico2017.jpg') no-repeat; background-position: center center; background-size: cover;"></div>--}}
            <div class="bg-slide active" data-index="3" style="background: url('/assets/{{ConfigHelper::config('tema')}}/home/academico2017.jpg') no-repeat; background-position: center center; background-size: cover;">
                <video playsinline autoplay muted loop poster="/assets/{{ConfigHelper::config('tema')}}/home/academico2017.jpg" id="bgvid">
                    <source src="/assets/{{ConfigHelper::config('tema')}}/home/academico2015.mp4" type="video/mp4">
                </video>
            </div>
            @if(ConfigHelper::config('propietario') == 1)
                <div class="bg-slide" data-index="4" style="background: url('/assets/{{ConfigHelper::config('tema')}}/home/maxsurf.jpg') no-repeat; background-position: center center; background-size: cover;"></div>
                <div class="bg-slide" data-index="5" style="background: url('/assets/{{ConfigHelper::config('tema')}}/home/colectivos.jpg') no-repeat; background-position: center center; background-size: cover;"></div>
            @elseif(ConfigHelper::config('propietario') == 2)
                <div class="bg-slide" data-index="4" style="background: url('/assets/{{ConfigHelper::config('tema')}}/home/coloniascic.jpg') no-repeat; background-position: center center; background-size: cover;"></div>
            @endif

        </div>
        <!-- Swiper -->
        <div class="swiper-home">
            <div class="swiper-container">
                <div class="swiper-wrapper">

                    <!-- Landed link -->
                    {{--
                    @if(ConfigHelper::config('propietario') == 1)
                        <div class="swiper-slide landed" data-index="6" data-bg="/assets/{{ConfigHelper::config('tema')}}/home/landedblog.jpg"><h1><a class="seccion" href="http://www.landedblog.com" target="_blank"><b>LANDED</b><br /><small>{!! trans('web.landedslogan') !!}</small></a><a class="icons btn" href="http://www.landedblog.com" target="_blank"><i class="fa fa-camera-retro"></i> <i class="fa fa-video-camera"></i> <i class="fa fa-picture-o"></i> <i class="fa fa-comments"></i></a></h1></div>
                    @elseif(ConfigHelper::config('propietario') == 2)
                        <div class="swiper-slide landed" data-index="6" data-bg="/assets/{{ConfigHelper::config('tema')}}/home/landedblog.jpg"><h1><a class="seccion" href="http://www.landedblog.com/iccic/" target="_blank"><b>LANDED</b><br /><small>{!! trans('web.landedslogan') !!}</small></a><a class="icons btn" href="http://www.landedblog.com/iccic/" target="_blank"><i class="fa fa-camera-retro"></i> <i class="fa fa-video-camera"></i> <i class="fa fa-picture-o"></i> <i class="fa fa-comments"></i></a></h1></div>
                    @endif
                    --}}
                    <div class="swiper-slide swiper-slide-double active" data-index="3" data-bg="/assets/{{ConfigHelper::config('tema')}}/home/academico2017.jpg">
                        <div class="slogan">
                            {!! trans('web.academicopromo') !!}
                        </div>
                        <h1><a class="seccion seccion-promo" href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', '3', 'curso-escolar-en-el-extranjero')!!}">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', '3', 'Año escolar en el extranjero')!!}</a><br /><a class="btn btn-white btn-outlined" href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', '3', 'curso-escolar-en-el-extranjero')!!}">{!! trans('web.categorias.verprogramas') !!}</a></h1>

                    </div>
                    <div class="swiper-slide" data-index="1" data-bg="/assets/{{ConfigHelper::config('tema')}}/home/ingles2.jpg"><h1><a class="seccion" href="/{!! trans('web.categorias.ingles-slug') !!}">{!! trans('web.categorias.ingles') !!}</a><br /><a class="btn btn-white btn-outlined" href="/{!! trans('web.categorias.ingles-slug') !!}">{!! trans('web.categorias.verprogramas') !!}</a></h1></div>
                    <div class="swiper-slide" data-index="2" data-bg="/assets/{{ConfigHelper::config('tema')}}/home/otrosidiomas2.jpg"><h1><a class="seccion" href="/{!! trans('web.categorias.idiomas-slug') !!}">{!! trans('web.categorias.idiomas') !!}</a><br /><a class="btn btn-white btn-outlined" href="/{!! trans('web.categorias.idiomas-slug') !!}">{!! trans('web.categorias.verprogramas') !!}</a></h1></div>

                    @if(ConfigHelper::config('propietario') == 1)
                        <div class="swiper-slide" data-index="4" data-bg="/assets/{{ConfigHelper::config('tema')}}/home/maxsurf.jpg"><h1><a class="seccion" href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', '4', 'campamentos-verano-ingles')!!}">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', '4', 'Campamentos de verano + inglés en España')!!}</a><br /><a class="btn btn-white btn-outlined" href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', '4', 'campamentos-verano-ingles')!!}">{!! trans('web.categorias.verprogramas') !!}</a></h1></div>
                        <div class="swiper-slide" data-index="5" data-bg="/assets/{{ConfigHelper::config('tema')}}/home/colectivos.jpg">
                            <h1>
                                @if(App::getLocale() == 'es')
                                    <a class="seccion" href="/assets/catalogos/bs/BS_2015_colectivos-y-escuelas.pdf" target="_blank">
                                        @elseif(App::getLocale() == 'ca')
                                            <a class="seccion" href="/assets/catalogos/bs/BS_2015_col-lectius-i-escoles.pdf" target="_blank">
                                                @endif
                                                {!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', '5', 'Viajes educativos para colectivos y escuelas')!!}
                                            </a>
                                            <br />
                                            @if(App::getLocale() == 'es')
                                                <a class="btn btn-white btn-outlined" href="/assets/catalogos/bs/BS_2015_colectivos-y-escuelas.pdf" target="_blank">
                                                    @elseif(App::getLocale() == 'ca')
                                                        <a class="btn btn-white btn-outlined" href="/assets/catalogos/bs/BS_2015_col-lectius-i-escoles.pdf" target="_blank">
                                                            @endif
                                                            {!! trans('web.categorias.verprogramas') !!}
                                                        </a>
                            </h1>
                        </div>
                    @elseif(ConfigHelper::config('propietario') == 2)
                        <div class="swiper-slide" data-index="4" data-bg="/assets/{{ConfigHelper::config('tema')}}/home/coloniascic.jpg"><h1><a class="seccion" href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', '6', 'summer-camps-ingles')!!}">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', '6', 'Campamentos de verano en inglés')!!}</a><br /><a class="btn btn-white btn-outlined" href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', '6', 'summer-camps-ingles')!!}">{!! trans('web.categorias.verprogramas') !!}</a></h1></div>
                    @endif

                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </main>

    <!-- Modal -->
    <div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
                </div>
                <div class="modal-body">
                    <div id="respuesta">
                        <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                        <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                            <div class="msg"></div>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                        <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                        <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                        <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                        <input type="hidden" id="curso" value="home">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>

                    <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @stop

    @section('extra_footer')
    <!-- Swiper JS -->
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/swiper/swiper.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            @if(ConfigHelper::config('propietario') == 1)
            //slidesPerView: 6,
            slidesPerView: 'auto',
            @elseif(ConfigHelper::config('propietario') == 2)
            slidesPerView: 'auto',
            @endif
            spaceBetween: 0,
            breakpoints: {
                1171: {
                    @if(ConfigHelper::config('propietario') == 1)
                    slidesPerView: 5,
                    @elseif(ConfigHelper::config('propietario') == 2)
                    slidesPerView: 4,
                    @endif
                    spaceBetween: 0
                },
                1170: {
                    slidesPerView: 1,
                    spaceBetween: 0
                }
            }
        });




        $('.swiper-slide').on('mouseover', function () {
            indice = $(this).data('index');
            $('.swiper-slide').removeClass('active');
            $(this).addClass('active');
            $('.bg-slide').each(function () {
                if($(this).data('index') == indice) {
                    $(this).addClass('active');
                }else{
                    $(this).removeClass('active');
                }
            });
        });

        $('.swiper-slide').on('mouseout', function () {
            indice = $('.swiper-slide-double').data('index');
            $(this).removeClass('active');
            $('.swiper-slide[data-index='+indice+']').addClass('active');
            $('.bg-slide').each(function () {
                if($(this).data('index') == indice) {
                    $(this).addClass('active');
                }else{
                    $(this).removeClass('active');
                }
            });
        });

        $(document).ready(function () {
            bgs();
        });
        $(window).resize(function (){
            bgs();
            if($(window).width() > 1171) {
                @if(ConfigHelper::config('propietario') == 1)
                    $('.swiper-slide').css({'width': '17%'});
                    $('.swiper-slide-double').css({'width': '32%'});
                    swiper.updateSlidesSize();
                @elseif(ConfigHelper::config('propietario') == 2)
                    $('.swiper-slide').css({'width': '20%'});
                    $('.swiper-slide-double').css({'width': '40%'});
                    swiper.updateSlidesSize();
                @endif
            }
        });


        $(document).ready(function () {
            $('.msg').hide();
            $("#plusinfoenviar").click(function () {
                //console.log('validar');
                if ($('#name').val() == '') {
                    $('.msg').html('Debes indicar un nombre de contacto');
                    $('.msg').show();
                    return false;
                }
                if ($('#email').val() == '') {
                    $('.msg').html('Debes indicar un teléfono o un email de contacto');
                    $('.msg').show();
                    return false;
                }

                if ($('#email').val() != '') {
                    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
                    if (re.test($('#name').val())) {
                        $('.msg').html('El email no tiene un formato correcto');
                        $('.msg').show();
                        return false
                    }

                }

                if (!/^([0-9])*$/.test($('#tel').val())) {
                    $('.msg').html('El campo teléfono tiene que ser numérico');
                    $('.msg').show();
                    return false
                }


                post_data = {
                    'name': $('#name').val(),
                    'tel': $('#tel').val(),
                    'email': $('#email').val(),
                    'curso': $('#curso').val()
                };
                $.ajax({
                    type: "POST",
                    url: "/assets/{{ConfigHelper::config('tema')}}/includes/plusinfosend-{{ConfigHelper::config('sufijo')}}.php",
                    data: post_data,
                    success: function (msg) {
                        //console.log(msg);
                        $("#respuesta").html(msg);
                        $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                        ga('send', 'Solicitud', 'button', 'click', 'home', 1);
                    },
                    error: function () {
                        alert("error!!");
                    }
                });
            });

        });

        $('form input').blur(function () {
            $('.msg').hide();
        });

        var vid = document.getElementById("bgvid");
        function vidFade() {
            vid.classList.add("stopfade");
        }
        vid.addEventListener('ended', function() {
            vid.play();
            // to capture IE10
            vidFade();
        });

        var isMobile = false;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Opera Mobile|Kindle|Windows Phone|PSP|AvantGo|Atomic Web Browser|Blazer|Chrome Mobile|Dolphin|Dolfin|Doris|GO Browser|Jasmine|MicroB|Mobile Firefox|Mobile Safari|Mobile Silk|Motorola Internet Browser|NetFront|NineSky|Nokia Web Browser|Obigo|Openwave Mobile Browser|Palm Pre web browser|Polaris|PS Vita browser|Puffin|QQbrowser|SEMC Browser|Skyfire|Tear|TeaShark|UC Browser|uZard Web|wOSBrowser|Yandex.Browser mobile/i.test(navigator.userAgent)) {
            isMobile = true;
        }
        $(document).ready(function(){
            console.log(isMobile);
            if (isMobile == true){
                $('#bgvid').css({'display':'none'});
            }else{
                $('#bgvid').css({'display':'block'});
            }
        });

        function bgs() {
            if ($(window).width() < 1171) {
                $('.swiper-slide').each(function () {
                    $(this).css({'background-image': 'url("' + $(this).data('bg') + '")'});
                });
            }else {
                $('.swiper-slide').each(function () {
                    $(this).css({'background-image': 'none'});
                });
            }
        }

    </script>

@stop