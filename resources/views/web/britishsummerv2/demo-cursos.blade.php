@extends('web.britishsummerv2.baseweb')

@section('container')


    <h2>Listado cursos:</h2>

    <ul>
    @foreach(\VCN\Models\Cursos\Curso::all() as $curso)
        <li>{{$curso->name}}</li>
    @endforeach
    </ul>

    <hr>
    Filtrando categoria:
    <ul>
    @foreach(\VCN\Models\Cursos\Curso::where('category_id',1)->get() as $curso)
        <li>{{$curso->name}}</li>
    @endforeach
    </ul>

    Filtrando categorias:
    <ul>
    @foreach(\VCN\Models\Categoria::whereIn('id',[1,2,3])->get() as $cat)
        <li>{{$cat->id}} : {{$cat->name}}</li>
    @endforeach
    </ul>

    <ul>
    @foreach(\VCN\Models\Cursos\Curso::whereIn('category_id',[1,2,3])->get() as $curso)
        <li>{{$curso->name}}</li>
    @endforeach
    </ul>

    <hr>

    <h2>Listado por Proveedor:</h2>

    <br>
    Proveedores:
    @foreach(\VCN\Models\Proveedores\Proveedor::all() as $p)
        {{$p->name}},
    @endforeach

    <br><br>

    Por propietario:
    <ul>
    @foreach(\VCN\Models\Proveedores\Proveedor::where('propietario',3)->get() as $p)
        <li>{{$p->name}}</li>
    @endforeach
    </ul>

    Cursos Por propietario (0):
    <ul>
    @foreach(\VCN\Models\Proveedores\Proveedor::where('propietario',0)->get() as $p)
        @foreach($p->cursos as $curso)
            <li>{{$curso->name}}</li>
        @endforeach
    @endforeach
    </ul>

    Filtrando Por propietario (0) y categoria (1,2,3):
    <ul>
    @foreach(\VCN\Models\Proveedores\Proveedor::where('propietario',0)->get() as $p)
        @foreach($p->cursos()->whereIn('category_id',[1,2,3])->get() as $curso)
            <li>{{$curso->categoria->name}} :: {{$curso->name}}</li>
        @endforeach
    @endforeach
    </ul>


    Cursos con extras:
    <ul>
        @foreach(\VCN\Models\Cursos\Curso::all() as $curso)
            <li><strong>{{$curso->name}}</strong>
                @foreach($curso->extras as $e)
                    (extras: {{$e->course_extras_name}})
                @endforeach
            </li>
        @endforeach
    </ul>


    <hr>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4>{{$curso->name}}</h4>

                <pre>
                    <?php //print_r($curso); ?>
                </pre>

                <ul>
                    <li>Categoría: {{$curso->categoria->name}}</li>
                    <li>Subcategoría: {{$curso->subcategoria->name}}</li>
                    {!! $curso->subcategoria_detalle ? '<li>Detalle Subcategoria: ' . $curso->subcategoria_detalle->name . '</li>' : '' !!}
                    <li>Extras</li>
                    <ul>
                        @foreach($curso->extras as $e)
                            <li>{{$e->course_extras_name}}</li>
                        @endforeach
                    </ul>

                </ul>



                <hr>
            </div>
        </div>
    </div>

@stop
