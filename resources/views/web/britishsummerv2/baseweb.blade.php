<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @section('title')
            {{ConfigHelper::config('nombre')}}
        @show
    </title>

    @yield('extra_meta')


    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Lato:100,400,700,700italic,400italic' rel='stylesheet' type='text/css'>

    <!-- ==============================================
		Favicons
		=============================================== -->
    <link rel="shortcut icon" href="/assets/{{ConfigHelper::config('tema')}}/favicon/favicon{{ConfigHelper::config('sufijo')}}.ico">
    <link rel="apple-touch-icon" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}-114x114.png">


    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" />


    <!-- Font -->
    <link href='//fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/bs2015.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" media="all" href="/assets/{{ConfigHelper::config('tema')}}/css/jquery-jvectormap.css"/>


    <!-- font awesome -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />


    <link href="/assets/{{ConfigHelper::config('tema')}}/css/bootstrap-select.css" rel="stylesheet" type="text/css" />

    <link href="/assets/{{ConfigHelper::config('tema')}}/css/bootstrap-select.css" rel="stylesheet" type="text/css" />


    <link rel="stylesheet" href="/assets/{{ConfigHelper::config('tema')}}/css/magnific-popup.css"> <!-- Resource style -->
    <link rel="stylesheet" href="/assets/{{ConfigHelper::config('tema')}}/css/navstyle.css"> <!-- Resource style -->

    <script src="/assets/{{ConfigHelper::config('tema')}}/js/modernizr.js"></script> <!-- Modernizr -->

    <link rel="stylesheet" href="/assets/{{ConfigHelper::config('tema')}}/css/webstyle.css">

    <link rel="stylesheet" href="/assets/outdatedbrowser/outdatedbrowser.min.css">

    @yield('extra_head')

</head>
<body class="nav-is-fixed">

    <header class="cd-main-header">
        <a id="cd-logo" href="/"><img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" /></a>
        <nav id="cd-top-nav">
            <ul class="cd-header-buttons">
                <li><a href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal"><i class="fa fa-phone"></i> {{trans('web.solicitainfo')}}</a></li>
                @if(ConfigHelper::config('propietario') == 1)
                    @if(App::getLocale() == 'es')
                        <li class="hidden-xs hidden-sm"><a href="//www.britishsummer.com/webim/client.php?locale=sp&style=britishsummer" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('//www.britishsummer.com/webim/client.php?locale=sp&amp;style=britishsummer&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=800,height=600,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;"><i class="fa fa-comments"></i> {{trans('web.chat')}}</a></li>
                    @else
                        <li class="hidden-xs hidden-sm"><a href="//www.britishsummer.com/webim/client.php?locale={{App::getLocale()}}&style=britishsummer" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('//www.britishsummer.com/webim/client.php?locale={{App::getLocale()}}&amp;style=britishsummer&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=800,height=600,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;"><i class="fa fa-comments"></i> {{trans('web.chat')}}</a></li>
                    @endif
                @endif
                <li><a href="/{{trans('web.catalogo-slug')}}">{{trans('web.catalogo')}}</a></li>
                <li><a href="/{{trans('web.contacto-slug')}}">{{trans('web.contacto')}}</a></li>
                <li><a class="cd-search-trigger" href="#cd-search">{{trans('web.buscar')}}<span></span></a></li>
            </ul> <!-- cd-header-buttons -->
        </nav>
        <nav id="cd-top-nav-small">
            <ul class="cd-header-buttons">
                <li><a href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal"><i class="fa fa-phone"></i></a></li>
                <li><a href="/catalogo.html"><i class="fa fa-file-pdf-o"></i></a></li>
                <li><a href="/contacto.html"><i class="fa fa-map-marker"></i></a></li>
                <li><a class="cd-search-trigger" href="#cd-search">{{trans('web.buscar')}}<span></span></a></li>
            </ul> <!-- cd-header-buttons -->
        </nav>
        <a id="cd-menu-trigger" href="#0"><span class="cd-menu-text">Menu</span><span class="cd-menu-icon"></span></a>
    </header>

@yield('container')

    <div class="cd-overlay"></div>

@include('web.britishsummerv2.includes.menu'.ConfigHelper::config('sufijo'))


    <div id="outdated"></div>

    <script src="/assets/outdatedbrowser/outdatedbrowser.min.js"></script>

    <script src='https://code.jquery.com/jquery-2.1.1.min.js' type='text/javascript'></script>
    <script src='https://code.jquery.com/jquery-migrate-1.2.1.min.js' type='text/javascript'></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


    <script src="/assets/{{ConfigHelper::config('tema')}}/js/jquery.cookie.js"></script>
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/jquery.cookiecuttr.js"></script>


    <!-- detect mobile -->
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/detectmobilebrowser.js" type="text/javascript"></script>

    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/bootstrap-select.js"></script>

    <script src="/assets/{{ConfigHelper::config('tema')}}/js/jquery.mobile.custom.min.js"></script>
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/main.js"></script> <!-- navigation -->

    <script src="/assets/{{ConfigHelper::config('tema')}}/js/jquery.magnific-popup.min.js"></script>




    @yield('extra_footer')


        <script>
            $(document).ready(function () {

                outdatedBrowser({
                    bgColor: '#f25648',
                    color: '#ffffff',
                    lowerThan: 'transform',
                    languagePath: 'assets/outdatedbrowser/lang/{{App::getLocale()}}.html'
                })

                // activate cookie cutter
                if($('html').attr('lang') == 'es'){
                    cookieText = "Utilizamos cookies propias y de terceros para mejorar nuestros servicios y realizar anal&iacute;ticas web. Si continua navegando, consideramos que acepta su uso.<br />Puede cambiar la configuraci&oacute;n u obtener m&aacute;s informaci&oacute;n en ";
                    cookieTextLink = "Pol&iacute;tica de Cookies";
                    cookieLink = './politica-de-cookies.html';
                    buttonText = 'Acepto';
                }else if($('html').attr('lang') == 'ca'){
                    cookieText = "Utilitzem cookies pr&ograve;pies i de tercers per millorar nostres serveis i realitzar anal&iacute;tiques web. Si continua navegant, considerem que accepta el seu &uacute;s.<br />Pot canviar la configuraci&oacute; o obtenir m&eacute;s informaci&oacute; a ";
                    cookieTextLink = "<br />Pol&iacute;tica de Cookies";
                    cookieLink = './ca/politica-de-cookies.html';
                    buttonText = 'Acepto';
                }else if($('html').attr('lang') == 'en'){
                    cookieText = "We use cookies on this website, you can ";
                    cookieTextLink = "<br />read about them here";
                    cookieLink = './politica-de-cookies.html';
                    buttonText = 'Accept cookies';
                }

                $.cookieCuttr({
                    cookieDeclineButton: false,
                    cookieAnalyticsMessage: cookieText,
                    cookieWhatAreLinkText: cookieTextLink,
                    cookieWhatAreTheyLink: cookieLink,
                    cookieExpires: 365,
                    cookieAcceptButtonText: buttonText
                });
            });



        @if (strpos(Request::server ("SERVER_NAME"), 'britishsummer.com') !== false)
            if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept") {

                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-633952-18', 'britishsummer.com');
                ga('send', 'pageview');


                // Remarketing
                document.write('<div style="height:0;position:absolute;width: 0px !important;">');

                /* <![CDATA[ */

                var google_conversion_id = 991933354;
                var google_conversion_label = "xM5aCP7qxwQQquf-2AM";
                var google_custom_params = window.google_tag_params;
                var google_remarketing_only = true;
                /* ]]> */

                document.write('<sc'+'ript' + ' type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></sc' + 'ript>');
                document.write('<noscript>');
                document.write('<div style="display:inline; height:0;">');
                document.write('<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/991933354/?value=1.000000&amp;label=xM5aCP7qxwQQquf-2AM&amp;guid=ON&amp;script=0"/>');
                document.write('</div>');
                document.write('</noscript>');
                document.write('</div>');

                document.write('<!-- Google Tag Manager -->');
                document.write('<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T6PHKG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>');
                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-T6PHKG');
                document.write('<!-- End Google Tag Manager -->');



                document.write('<!-- Facebook Pixel Code -->');
                !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                        document,'script','https://connect.facebook.net/en_US/fbevents.js');

                fbq('init', '565613066936032');
                fbq('track', "PageView");
                document.write('<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=565613066936032&ev=PageView&noscript=1"/></noscript>');
                document.write('<!-- End Facebook Pixel Code -->');

            }
        @endif

        @if(Session::get('vcn.compra-login'))
            $('#bookingModal').modal('show');
        @endif

        </script>


</body>
</html>