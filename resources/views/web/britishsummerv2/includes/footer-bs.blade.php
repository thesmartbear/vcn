<div id="main-footer">
    <div>
        <div class="row">

            <div class="social-div">
                <ul class="social">
                    <li><a href="//www.facebook.com/britishsummerfan" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="//www.twitter.com/britishsm" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="//www.youtube.com/britishsummersm" target="_blank"><i class="fa fa-youtube-play"></i></li>
                    <li><a href="https://instagram.com/britishsummeres/" target="_blank"><i class="fa fa-instagram"></i></li>
                    <li><a href="https://plus.google.com/106203615715916398922/posts" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="https://www.pinterest.com/britishsummer/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                    <li><a href="//www.linkedin.com/company/british-summer" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
            <div class="btns-div">
                <a class="btn-contact" href="#"><span>CONTACTO</span></a>
                <a class="btn-clientes" href="https://www.britishsummer.com/clientes/"><span>&Aacute;REA DE CLIENTES</span></a>
                @if(App::getLocale() == 'es')
                    <a class="btn-idioma" href="/ca"><span>CAT</span></a>
                @elseif(App::getLocale() == 'ca')
                    <a class="btn-idioma" href="/es"><span>ES</span></a>
                @endif

            </div>

            <ul class="secondary">
                <li><a href="/catalogo.html" class="white-txt">CAT&Aacute;LOGO</a></li>
                <li><a href="/inscripcion.html">HOJAS INSCRIPCI&Oacute;N</a></li>
                <li class="separator"></li>
                <li><a href="/quienes-somos.html">QUI&Eacute;NES SOMOS</a></li>
                <li><a href="/trabaja-con-nosotros.html">TRABAJO</a></li>
                <li><a href="/colaboradores.html">COLABORADORES</a></li>
                <li class="separator"></li>
                <li><a href="/blog-aprender-ingles-extranjero" target="_blank">BLOG</a></li>
                <li><a href="http://www.landedblog.com" target="_blank" class="destacado"><strong>{!! trans('web.landedslogan') !!}</strong></a></li>
            </ul>
        </div>
    </div>

</div>
</div>