<div id="inside-menu" style="display: none;">
    <div class="container-fluid">
        <div class="row addmargintop30">
            <div class="col-md-12 text-center">
                <i class="fa fa-times" id="menu-close"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <div class="col">
                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/summer-camps-angles"><h5 style="color: #8e44ad;">CAMPAMENTS D'ESTIU <span>en anglès</span></h5></a>
                            <h6>Summer Camps. De 6 a 15 anys</h6>
                        </div>
                        <ul>
                            <li><a href="/summer-camps-angles/l-armentera.html">L'ARMENTERA <small>Osona. De 6 a 10 anys</small></a></li>
                            <li><a href="/summer-camps-angles/els-pins.html">ELS PINS <small>Maresme. De 10 a 15 anys</small></a></li>
                            <li><a href="/summer-camps-angles/la-solana.html">LA SOLANA <small>La Cerdanya. De 10 a 15 anys</small></a></li>
                        </ul>
                        <p class="addmargintop20"><a href="/summer-camps-angles"><strong>QUÈ ÉS UNA COLÓNIA CIC A CATALUNYA?</strong></a></p>
                    </div>
                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/summer-camps-angles"><h5 style="color: #8e44ad;">DAY CAMP <span>en anglès</span></h5></a>
                            <h6>De 10 a 13 anys</h6>
                        </div>
                        <ul>
                            <li><a href="/summer-camps-angles/active-english.html">ACTIVE ENGLISH</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="col">
                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/aprendre-angles-a-l-estranger"><h5 style="color: #1abc9c;">APRENDRE ANGLÈS A L'ESTRANGER</h5></a>
                            <h6>Totes les edats</h6>
                        </div>
                        <ul>
                            <li><a href="/aprendre-angles-a-l-estranger/joves/grupos-con-monitor/">PROGRAMES D'ESTIU AMB MONITOR</a></li>
                            <li><a href="/aprendre-angles-a-l-estranger/joves/cursos-especials/">CURSOS MOLT SINGULARS/ESPECIALITZATS</a></li>
                            <li><a href="/aprendre-angles-a-l-estranger/adults-professionals/">ADULTS I PROFESSIONALS</a></li>
                            <li><a href="/aprendre-angles-a-l-estranger/tota-la-familia/">TOTA LA FAMÍLIA</a></li>

                        </ul>
                    </div>


                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/aprendre-altres-idiomes-a-l-estranger"><h5 style="color: #888535;">ALTRES IDIOMES A L'ESTRANGER</h5></a>
                            <h6>Totes les edats</h6>
                        </div>
                        <ul>
                            <li><a href="/aprendre-altres-idiomes-a-l-estranger/joves/">De 8 a 18 anys</a></li>
                            <li><a href="/aprendre-altres-idiomes-a-l-estranger/adults-professionals/">De 18 a 99 anys</a></li>
                            <li><a href="/aprendre-altres-idiomes-a-l-estranger/tota-la-familia/">TOTA LA FAMÍLIA</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-xs-4">
                <div class="col">
                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/curs-escolar-a-l-estranger"><h5 style="color: #d35400;">ANY ESCOLAR A L'ESTRANGER</h5></a>
                            <h6>Primària, Secundària i Batxillerat</h6>
                        </div>

                        <ul>
                            <li><a href="/curs-escolar-a-l-estranger/curs-escolar/">Any Escolar</a></li>
                            <li><a href="/curs-escolar-a-l-estranger/semestre-escolar/">Semestre Escolar</a></li>
                            <li><a href="/curs-escolar-a-l-estranger/trimestre-escolar/">Trimestre Escolar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>