<nav id="cd-lateral-nav">
    <ul class="cd-navigation">
        <li class="item-has-children">
                <a href="#0">{{trans('web.categorias.ingles')}}</a>
                    <ul class="sub-menu">
                    @foreach(\VCN\Models\Categoria::whereIn('id', [1, 2, 7])
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })->orderBy('id','ASC')->get() as $cat)

                            @if(\VCN\Models\Cursos\Curso::where('category_id', $cat->id)->where('activo_web', 1)->where('course_language','LIKE', '%Inglés%')
                            ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                                })->count())

                                <li><a href="/{{trans('web.categorias.ingles-slug')}}/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a></li>

                            @endif
                    @endforeach
                    </ul>
        </li> <!-- item-has-children -->

        <li class="item-has-children">
            <a href="#0">{{trans('web.categorias.idiomas')}}</a>
                <ul class="sub-menu">
                @foreach(\VCN\Models\Categoria::whereIn('id', [1, 2, 7])
                    ->where(function ($query) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })->orderBy('id','ASC')->get() as $cat)

                        @if(\VCN\Models\Cursos\Curso::where('category_id', $cat->id)->where('activo_web', 1)->where('course_language','NOT LIKE', 'Inglés')
                           ->where(function ($query) {
                           return $query
                               ->where('propietario', 0)
                               ->orWhere('propietario', ConfigHelper::config('propietario'));
                               })->count())

                            <li><a href="/{{trans('web.categorias.idiomas-slug')}}/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a></li>

                        @endif
                @endforeach
                </ul>
        </li> <!-- item-has-children -->

        <li class="item-has-children">
            @foreach(\VCN\Models\Categoria::where('slug', 'curso-escolar-en-el-extranjero')
                ->where(function ($query) {
                    return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })->orderBy('id','ASC')->get() as $cat)

                <a href="#0">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a>
                <ul class="sub-menu">

                    @if(count($cat->subcategorias))
                        @foreach($cat->subcategorias as $subcat)
                            @if(\VCN\Models\Cursos\Curso::where('subcategory_id', $subcat->id)->where('activo_web', 1)
                            ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                                })->count())

                                <li><a href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}/{!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $subcat->id, $subcat->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcat->id, $subcat->name_web)!!}</a></li>

                            @endif
                        @endforeach
                    @endif
                </ul>
            @endforeach

        </li> <!-- item-has-children -->

        <li class="item-has-children">
                @foreach(\VCN\Models\Categoria::where('slug', 'campamentos-verano-ingles')
                    ->where(function ($query) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })->orderBy('id','ASC')->get() as $cat)

                <a href="#0">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a>
                <ul class="sub-menu">

                    @if(count($cat->subcategorias))
                        @foreach($cat->subcategorias as $subcat)
                            @if(\VCN\Models\Cursos\Curso::where('subcategory_id', $subcat->id)->where('activo_web', 1)
                            ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                                })->count())

                                <li><a href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}/{!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $subcat->id, $subcat->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcat->id, $subcat->name_web)!!}</a></li>

                            @endif
                        @endforeach
                    @endif

                        <li><a class="cd-nav-item text-uppercase" href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}"><b>{{trans('web.categorias.nuestrasclases')}}</b></a></li>
                </ul>
                @endforeach

        </li> <!-- item-has-children -->


        {{--
        <li class="item-has-children">
            @foreach(\VCN\Models\Categoria::where('slug', 'colectivos-y-escuelas')
                ->where(function ($query) {
                    return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })->orderBy('id','ASC')->get() as $cat)



                    @if(count($cat->subcategorias))
                    <a href="#0">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a>
                    <ul class="sub-menu">
                        @foreach($cat->subcategorias as $subcat)
                            @if(\VCN\Models\Cursos\Curso::where('subcategory_id', $subcat->id)->where('activo_web', 1)
                            ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                                })->count())

                                <li><a href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}/{!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $subcat->id, $subcat->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcat->id, $subcat->name_web)!!}</a></li>
                            @endif
                        @endforeach
                    </ul>
                    @endif
            @endforeach

        </li> <!-- item-has-children -->

        --}}

        <li class="item">
            @foreach(\VCN\Models\Categoria::where('slug', 'colectivos-y-escuelas')
                ->where(function ($query) {
                    return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })->orderBy('id','ASC')->get() as $cat)
                @if(App::getLocale() == 'es')
                    <a href="/assets/catalogos/bs/BS_2015_colectivos-y-escuelas.pdf" target="_blank">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a>
                @elseif(App::getLocale() == 'ca')
                    <a href="/assets/catalogos/bs/BS_2015_col-lectius-i-escoles.pdf" target="_blank">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a>
                @endif
            @endforeach
        </li>


        <ul class="cd-navigation cd-single-item-wrapper">
            {{--<li><a href="#" target="_blank"><strong>/{{trans('web.quienes-somos')}}</strong></a></li>--}}
            {{--<li><a href="#" target="_blank"><strong>/{{trans('web.trabajo')}}</strong></a></li>--}}
            <li><a href="//www.britishsummer.com/be-bs/" target="_blank">Be Bs. Concursos</a></li>
            <li><a href="//www.britishsummer.com/blog-aprender-ingles-extranjero" target="_blank">I Love Brit Blog</a></li>
            <li><a href="http://www.landedblog.com/" target="_blank">{!! trans('web.landedslogan') !!}</a></li>
        </ul>

        <ul class="cd-navigation cd-single-item-wrapper">
            <li><a href="/{{trans('web.inscripcion-slug')}}">{{trans('web.inscripcion')}}</a></li>
            <li class="hidden-lg hidden-md"><a href="//www.britishsummer.com/webim/client.php?locale={{App::getLocale()}}&style=britishsummer" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('//www.britishsummer.com/webim/client.php?locale={{App::getLocale()}}&amp;style=britishsummer&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=800,height=600,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;"><i class="fa fa-comments"></i> {{trans('web.chat')}}</a></li>
        </ul>

        <div class="cd-navigation socials">
            <a href="//www.facebook.com/britishsummer" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="//www.twitter.com/britishsm" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="//www.youtube.com/britishsummersm" target="_blank"><i class="fa fa-youtube-play"></i></a>
            <a href="https://instagram.com/britishsummeres/" target="_blank"><i class="fa fa-instagram"></i></a>
            <a href="https://plus.google.com/106203615715916398922/posts" target="_blank"><i class="fa fa-google-plus"></i></a>
            <a href="https://www.pinterest.com/britishsummer/" target="_blank"><i class="fa fa-pinterest"></i></a>
            <a href="//www.linkedin.com/company/british-summer" target="_blank"><i class="fa fa-linkedin"></i></a>
        </div>
        <ul class="cd-navigation cd-single-item-wrapper">
            @if (Auth::guest())
                <li><a href="//{{ConfigHelper::config('sufijo')}}.estudiaryviajar.com/auth/login"><i class="fa fa-lock"></i> {{trans('web.aclientes')}}</a></li>
            @else
                <li><a href="//{{ConfigHelper::config('sufijo')}}.estudiaryviajar.com/manage"><i class="fa fa-lock"></i> {{trans('web.aclientes')}}</a></li>
                <li><a href="//{{ConfigHelper::config('sufijo')}}.estudiaryviajar.com/auth/logout"><i class="fa fa-lock"></i> {{trans('web.salir')}}</a></li>
            @endif


            @if(App::getLocale() == 'es')
                <li><a href="/ca"><i class="fa fa-globe"></i> <span>CATALÀ</span></a></li>
            @elseif(App::getLocale() == 'ca')
                <li><a href="/es"><i class="fa fa-globe"></i> <span>ESPAÑOL</span></a></li>
            @endif
        </ul>


    </ul> <!-- primary-nav -->
</nav> <!-- cd-nav -->

<div id="cd-search" class="cd-search">
    <form action="{{route('web.buscar')}}" method="post" enctype="multipart/form-data" class="searchbox" autocomplete="off" >
        <input type="text" placeholder="{{trans('web.buscar')}}" name="search"  id="search" class="searchbox-input" required>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    </form>
</div>