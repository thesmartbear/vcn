<nav id="cd-lateral-nav">
    <ul class="cd-navigation">

        <li class="item-has-children">
            <a href="#0">{{trans('web.categorias.ingles')}}</a>
            <ul class="sub-menu">
                @foreach(\VCN\Models\Categoria::whereIn('id', [1, 2, 7])
                    ->where(function ($query) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })->orderBy('id','ASC')->get() as $cat)

                    @if(\VCN\Models\Cursos\Curso::where('category_id', $cat->id)->where('activo_web', 1)->where('course_language','LIKE', '%Inglés%')
                        ->where(function ($query) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                            })->count())

                        <li><a href="/{{trans('web.categorias.ingles-slug')}}/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a></li>

                    @endif
                @endforeach
            </ul>
        </li> <!-- item-has-children -->

        <li class="item-has-children">
            <a href="#0">{{trans('web.categorias.idiomas')}}</a>
            <ul class="sub-menu">
                @foreach(\VCN\Models\Categoria::whereIn('id', [1, 2, 7])
                    ->where(function ($query) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })->orderBy('id','ASC')->get() as $cat)

                    @if(\VCN\Models\Cursos\Curso::where('category_id', $cat->id)->where('activo_web', 1)->where('course_language','NOT LIKE', 'Inglés')
                        ->where(function ($query) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                            })->count())

                        <li><a href="/{{trans('web.categorias.idiomas-slug')}}/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a></li>

                    @endif
                @endforeach
            </ul>
        </li> <!-- item-has-children -->

        <li class="item-has-children">
            @foreach(\VCN\Models\Categoria::where('slug', 'curso-escolar-en-el-extranjero')
                ->where(function ($query) {
                    return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })->orderBy('id','ASC')->get() as $cat)

                <a href="#0">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a>
                <ul class="sub-menu">

                    @if(count($cat->subcategorias))
                        @foreach($cat->subcategorias as $subcat)
                            @if(\VCN\Models\Cursos\Curso::where('subcategory_id', $subcat->id)->where('activo_web', 1)
                            ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                                })->count())

                                <li><a href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}/{!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $subcat->id, $subcat->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcat->id, $subcat->name_web)!!}</a></li>

                            @endif
                        @endforeach
                    @endif
                </ul>
            @endforeach

        </li> <!-- item-has-children -->

        <li class="item-has-children">
            @foreach(\VCN\Models\Categoria::where('slug', 'summer-camps-ingles')
                ->where(function ($query) {
                    return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                })->orderBy('id','ASC')->get() as $cat)

                <a href="#0">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a>
                <ul class="sub-menu">

                    @if(count($cat->subcategorias))
                        @foreach($cat->subcategorias as $subcat)
                            @if(\VCN\Models\Cursos\Curso::where('subcategory_id', $subcat->id)->where('activo_web', 1)
                            ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                                })->count())

                                <li><a href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}/{!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'slug', $subcat->id, $subcat->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $subcat->id, $subcat->name_web)!!}</a></li>

                            @endif
                        @endforeach
                    @endif

                    <li><a class="cd-nav-item text-uppercase" href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}"><b>{{trans('web.categorias.nuestrasclases')}}</b></a></li>
                </ul>
            @endforeach

        </li> <!-- item-has-children -->


        <ul class="cd-navigation cd-single-item-wrapper">
            {{--<li><a href="#" target="_blank"><strong>/{{trans('web.quienes-somos')}}</strong></a></li>--}}
            {{--<li><a href="#" target="_blank"><strong>/{{trans('web.trabajo')}}</strong></a></li>--}}
            <li><a href="//viatges.iccic.edu/be-cicviatges/" target="_blank">Be CIC Viatges. CONCURSOS</a></li>
            <li><a href="http://www.landedblog.com/iccic/" target="_blank">{!! trans('web.landedslogan') !!}</a></li>
        </ul>

        <ul class="cd-navigation cd-single-item-wrapper">
            <li><a href="/{{trans('web.inscripcion-slug')}}">{{trans('web.inscripcion')}}</a></li>
        </ul>

        <div class="cd-navigation socials">
            <a href="https://es-es.facebook.com/CIC-Escola-dIdiomes-1426848570864710/" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="//twitter.com/cicviatges" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="https://instagram.com/cicviatges" target="_blank"><i class="fa fa-instagram"></i></a>
        </div>
        <ul class="cd-navigation cd-single-item-wrapper">
            @if (Auth::guest())
                <li><a href="//{{ConfigHelper::config('sufijo')}}.estudiaryviajar.com/auth/login"><i class="fa fa-lock"></i> {{trans('web.aclientes')}}</a></li>
            @else
                <li><a href="//{{ConfigHelper::config('sufijo')}}.estudiaryviajar.com/manage"><i class="fa fa-lock"></i> {{trans('web.aclientes')}}</a></li>
                <li><a href="//{{ConfigHelper::config('sufijo')}}.estudiaryviajar.com/auth/logout"><i class="fa fa-lock"></i> {{trans('web.salir')}}</a></li>
            @endif


            @if(App::getLocale() == 'es')
                <li><a href="/ca"><i class="fa fa-globe"></i> <span>CATALÀ</span></a></li>
            @elseif(App::getLocale() == 'ca')
                <li><a href="/es"><i class="fa fa-globe"></i> <span>ESPAÑOL</span></a></li>
            @endif
        </ul>


    </ul> <!-- primary-nav -->
</nav> <!-- cd-nav -->

<div id="cd-search" class="cd-search">
    <form action="{{route('web.buscar')}}" method="post" enctype="multipart/form-data" class="searchbox" autocomplete="off" >
        <input type="text" placeholder="{{trans('web.buscar')}}" name="search"  id="search" class="searchbox-input" required>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    </form>
</div>