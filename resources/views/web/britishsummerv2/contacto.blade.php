@extends('web.britishsummerv2.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')
    <!-- Link Swiper's CSS -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/bs.css" rel="stylesheet">
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/swiper/swiper.min.css" rel="stylesheet">

@stop

@section('container')
    <div class="headerbg" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/wall4.png') repeat;">
        <div class="container" id="header" style="position: relative;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo">
                        <h1 class="slogan">
                            <span></span>
                                {{trans('web.contacto')}}
                                <br />
                                <span></span>
                                <small>2016</small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="headerbgoverlay"></div>
    </div>

    <main class="cd-main-content">
        <div class="container" id="contenido">
            @if(ConfigHelper::config('propietario') == 1)
                <div class="row">
                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="head oficinas">Oficinas
                                <span></span>
                            </h3>
                        </div>
                        <div class="col-sm-4" id="mapa_info">
                            <p class="barcelona"><strong>BARCELONA</strong><br>
                                V&iacute;a Augusta, 33<br>
                                08006 Barcelona<br>
                                Tel. 93 200 88 88<br>
                                Fax 93 202 23 71<br>
                                <a href="mailto:infobs@britishsummer.com">infobs@britishsummer.com</a></p>

                            <p class="girona"><strong>GIRONA</strong><br>
                                Carrer Migdia, 25
                                <br>
                                17002 Girona<br>
                                Tel. 972 414 902<br>
                                <a href="mailto:girona@britishsummer.com">girona@britishsummer.com</a></p>

                            <p class="madrid"><strong>MADRID</strong><br>
                                Paseo de la Castellana <br>
                                136 bajos<br>
                                28046 MADRID<br>
                                Tel.: 91 345 95 65<br>
                                <a href="mailto:madrid@britishsummer.com">madrid@britishsummer.com</a></p>

                            <p class="sevilla"><strong>SEVILLA</strong><br>
                                Pza. Cristo de Burgos <br>
                                21. Bajo A<br>
                                41003 Sevilla<br>
                                Tel.: 95 421 07 85<br>
                                <a href="mailto:sevilla@britishsummer.com">sevilla@britishsummer.com</a></p>
                        </div>
                        <div class="col-sm-8 pull-right" style="float: left;">
                            <div id="mapa" style="width: 100%; height: 450px;"></div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-4 col-sm-offset-1">
                    <h3 class="head">{{trans('web.contacto')}}</h3>
                    <form id="contactform" action="" method="post" class="validateform" name="leaveContact">
                        <div id="sendmessage">
                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {!!trans('web.mensajegracias')!!}
                            </div>
                        </div>

                        <div class="formlist">
                            <div class="form-group field">
                                <label class="col-md-4 control-label" for="oficina">{{trans('web.oficina')}} <span>*</span></label>
                                <div class="col-md-8">
                                    <select class="form-control" name="oficina" data-rule="required" data-msg="Por favor, selecciona una oficina">
                                        <option value=""></option>
                                        <option value="infobs@britishsummer.com">Barcelona</option>
                                        <option value="girona@britishsummer.com">Girona</option>
                                        <option value="madrid@britishsummer.com">Madrid</option>
                                        <option value="sevilla@britishsummer.com">Sevilla</option>
                                    </select>
                                    <div class="validation"></div>
                                </div>
                            </div>

                            <div class="form-group field">
                                <label class="col-md-4 control-label" for="name">{{trans('web.nombre')}} <span>*</span></label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="name" data-rule="maxlen:4" data-msg="Tienes que introducir al menos 4 caracteres" />
                                    <div class="validation"></div>
                                </div>
                            </div>

                            <div class="form-group field">
                                <label class="col-md-4 control-label" for="email">{{trans('web.email')}} <span>*</span></label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="email" data-rule="email" data-msg="Por favor, introduce un email válido" />
                                    <div class="validation"></div>
                                </div>
                            </div>

                            <div class="form-group field">
                                <label class="col-md-4 control-label" for="message">{{trans('web.mensaje')}} <span>*</span></label>
                                <div class="col-md-8">
                                    <textarea class="form-control" rows="6" name="message" data-rule="required" data-msg="Por favor, escribe un mensaje"></textarea>
                                    <div class="validation"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-4">
                                    <p class="text-muted obligatorio"><small>*{{trans('web.obligatorio')}}</small></p>
                                </div>
                                <div class="col-md-8">
                                    <input type="button" value="{{trans('web.enviarmensaje')}}" class="btn btn-primary btn-block" id="enviarcontacto" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @elseif(ConfigHelper::config('propietario') == 2)
                <div class="row">

                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="head oficinas">{{trans('web.oficinas')}}</h4>
                            </div>
                            <div class="col-sm-12 contactbig">
                                <p><strong>Departament d’Estades Lingüístiques<br />
                                    CIC escola idiomes</strong><br />
                                    Vía Augusta, 205<br>
                                    08021 Barcelona<br>
                                    Tel. 93 200 11 33<br>
                                    Fax 93 209 05 75<br>
                                    <a href="mailto:viatges@iccic.edu">viatges@iccic.edu</a></p>
                            </div>

                            <div class="col-sm-12">
                                <h4 class="head horario">{{trans('web.horario')}}</h4>
                            </div>
                            <?
                                $pagina = \VCN\Models\CMS\Pagina::where('name', 'horario')
                                    ->where(function ($query) {
                                        return $query
                                                ->where('propietario', 0)
                                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                                    })
                                    ->first();
                            ?>
                            <div class="col-sm-12 contactbig">
                                {!! Traductor::getWeb(App::getLocale(), 'Pagina', 'contenido', 5, $pagina->contenido) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <h4 class="head">{{trans('web.contacto')}}</h4>
                        <form id="contactform" action="" method="post" class="validateform" name="leaveContact">
                            <div id="sendmessage">
                                <div class="alert alert-info">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    {!! trans('web.mensajegracias') !!}
                                </div>
                            </div>

                            <div class="formlist">
                                <div class="form-group field">
                                    <label class="col-md-4 control-label" for="name">{{trans('web.nombre')}} <span>*</span></label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text" name="name" data-rule="maxlen:4" data-msg="Tienes que introducir al menos 4 caracteres" />
                                        <div class="validation"></div>
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-4 control-label" for="email">{{trans('web.email')}} <span>*</span></label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text" name="email" data-rule="email" data-msg="Por favor, introduce un email válido" />
                                        <div class="validation"></div>
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label class="col-md-4 control-label" for="message">{{trans('web.mensaje')}} <span>*</span></label>
                                    <div class="col-md-8">
                                        <textarea class="form-control" rows="6" name="message" data-rule="required" data-msg="Por favor, escribe un mensaje"></textarea>
                                        <div class="validation"></div>
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <div class="col-md-4">
                                        <p class="text-muted obligatorio"><small>*{{trans('web.obligatorio')}}</small></p>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="button" value="{{trans('web.enviarmensaje')}}" class="btn btn-primary btn-block" id="enviarcontacto" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
        </div>
    </main>

    <!-- Modal -->
    <div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
                </div>
                <div class="modal-body">
                    <div id="respuesta">
                        <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                        <form method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                            <div class="msg"></div>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                        <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                        <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                        <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                        <input type="hidden" id="curso" value="{{trans('web.contacto')}}">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>

                    <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('extra_footer')
    <!-- Contact validation js -->
    <script>

        $(document).ready(function() {
            "use strict";

            //Contact
            $('#enviarcontacto').click(function(){

                var f = $('#contactform').find('.formlist'),
                        ferror = false,
                        emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;


                f.find('input:not([type="button"])').each(function(){ // run all inputs

                    var i = $(this); // current input
                    var rule = i.attr('data-rule');

                    if( rule !== undefined ){
                        var ierror=false; // error flag for current input
                        var pos = rule.indexOf( ':', 0 );
                        if( pos >= 0 ){
                            var exp = rule.substr( pos+1, rule.length );
                            rule = rule.substr(0, pos);
                        }else{
                            rule = rule.substr( pos+1, rule.length );
                        }

                        switch( rule ){
                            case 'required':
                                if( i.val()==='' ){ ferror=ierror=true; }
                                break;

                            case 'maxlen':
                                if( i.val().length<parseInt(exp) ){ ferror=ierror=true; }
                                break;

                            case 'email':
                                if( !emailExp.test(i.val()) ){ ferror=ierror=true; }
                                break;

                            case 'checked':
                                if( !i.attr('checked') ){ ferror=ierror=true; }
                                break;

                            case 'regexp':
                                exp = new RegExp(exp);
                                if( !exp.test(i.val()) ){ ferror=ierror=true; }
                                break;
                        }
                        i.next('.validation').html( ( ierror ? (i.attr('data-msg') !== undefined ? i.attr('data-msg') : 'wrong Input') : '' ) ).show('blind');
                    }
                });
                f.find('textarea').each(function(){ // run all inputs

                    var i = $(this); // current input
                    var rule = i.attr('data-rule');

                    if( rule !== undefined ){
                        var ierror=false; // error flag for current input
                        var pos = rule.indexOf( ':', 0 );
                        if( pos >= 0 ){
                            var exp = rule.substr( pos+1, rule.length );
                            rule = rule.substr(0, pos);
                        }else{
                            rule = rule.substr( pos+1, rule.length );
                        }

                        switch( rule ){
                            case 'required':
                                if( i.val()==='' ){ ferror=ierror=true; }
                                break;

                            case 'maxlen':
                                if( i.val().length<parseInt(exp) ){ ferror=ierror=true; }
                                break;
                        }
                        i.next('.validation').html( ( ierror ? (i.attr('data-msg') != undefined ? i.attr('data-msg') : 'wrong Input') : '' ) ).show('blind');
                    }
                });

                f.find('select').each(function(){ // run all selects
                    var i = $(this); // current input
                    var rule = i.attr('data-rule');

                    if( rule !== undefined ){
                        var ierror=false; // error flag for current input
                        var pos = rule.indexOf( ':', 0 );
                        if( pos >= 0 ){
                            var exp = rule.substr( pos+1, rule.length );
                            rule = rule.substr(0, pos);
                        }else{
                            rule = rule.substr( pos+1, rule.length );
                        }

                        switch( rule ){
                            case 'required':
                                if( i.val()==='' ){ ferror=ierror=true; }
                                break;
                        }
                        i.next('.validation').html( ( ierror ? (i.attr('data-msg') != undefined ? i.attr('data-msg') : 'wrong Input') : '' ) ).show('blind');
                    }
                });


                if( ferror ){
                    return false;
                }else{
                    var str = $('#contactform').serialize();
                    console.log(str);
                }


                $.ajax({
                    type: "POST",
                    url: "/assets/{{ConfigHelper::config('tema')}}/includes/contact-{{ConfigHelper::config('sufijo')}}.php",
                    data: str,
                    success: function(msg){
                        console.log(msg);
                        $("#sendmessage").addClass("show");
                        $('#contactform')[0].reset();
                        $("#errormessage").ajaxComplete(function(event, request, settings){

                            if(msg == 'OK')	{
                                $("#sendmessage").addClass("show");
                                $('#contactform')[0].reset();

                            }else{
                                $("#sendmessage").addClass("show");
                                var result = msg;
                            }
                            console.log(result);
                            $(this).html(result);
                        });
                    }
                });

            });

        });
    </script>

    <!-- mapa -->
    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/maps/jquery-jvectormap-1.2.2.min.js"></script>
    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/maps/map_es.js"></script>
    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/contacto.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.msg').hide();
            $("#plusinfoenviar").click(function() {
                console.log('validar');
                if ($('#name').val() == ''){
                    $('.msg').html('Debes indicar un nombre de contacto');
                    $('.msg').show();
                    return false;
                }
                if ($('#email').val() == ''){
                    $('.msg').html('Debes indicar un teléfono o un email de contacto');
                    $('.msg').show();
                    return false;
                }

                if ($('#email').val() != ''){
                    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
                    if (re.test($('#name').val())) {
                        $('.msg').html('El email no tiene un formato correcto');
                        $('.msg').show();
                        return false
                    }

                }

                if (!/^([0-9])*$/.test($('#tel').val())){
                    $('.msg').html('El campo teléfono tiene que ser numérico');
                    $('.msg').show();
                    return false
                }


                post_data = {'name':$('#name').val(), 'tel':$('#tel').val(),'email':$('#email').val(), 'curso':$('#curso').val()};
                $.ajax({
                    type: "POST",
                    url: "/assets/{{ConfigHelper::config('tema')}}/includes/plusinfosend-{{ConfigHelper::config('sufijo')}}.php",
                    data: post_data,
                    success: function(msg){
                        $("#respuesta").html(msg);
                        $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                        ga('send', 'Solicitud', 'button', 'click', 'Contacto', 1);
                    },
                    error: function(){
                        alert("error!!");
                    }
                });
            });



        });

        $('form input').blur(function () {
            $('.msg').hide();
        });


    </script>
@stop
