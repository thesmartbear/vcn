@extends('layouts.base')

@section('container')

    <style>
        .container {
            vertical-align: middle;
            font-family: 'Lato';
        }

        .content {
            display: inline-block;
        }

        .title {
            font-size: 96px;
            margin-bottom: 40px;
        }

        .quote {
            font-size: 24px;
        }
    </style>

    <div class="container">
        <div class="content">
            <div class="title">{!! Html::image('/assets/logos/'.ConfigHelper::config('logoweb'), ConfigHelper::config('nombre'), array('class' => 'logo-default')) !!}</div>

            @foreach($menuWeb as $m1)
                @if($m1['route'])
                    {!! Html::linkRoute($m1['route'], trans($m1['nom']), $m1['view']) !!}
                @else
                    {!! Html::link($m1['url'], trans($m1['nom']), $m1['view']) !!}
                @endif
            @endforeach

            {{-- {!! Html::linkRoute('web.view', 'Translated title', ['page-slug']) !!} --}}

            <hr>

            <style type="text/css" media="screen">
                .navbar-locales .active { text-decoration: underline; }
            </style>

            Locale:{!! App::getLocale() !!}


            <ul class="navbar-locales">
                    <li class="{{App::getLocale()=='ca'?'active':''}}">
                    <a href="/ca" rel="alternate" hreflang="ca">
                        Català
                    </a>
                </li>
                    <li class="{{App::getLocale()=='es'?'active':''}}">
                    <a href="/es" rel="alternate" hreflang="es">
                        Español
                    </a>
                </li>
            </ul>

            Localization:<br><br>
            web.test.test1 :
            {!! trans('web.test.test1') !!}
            <hr>
            web.test.test2 :
            {!! trans('web.test.test2') !!}
            <hr>
            web.test.test3 :
            {!! trans('web.test.test3') !!}
        </div>
    </div>
@stop
