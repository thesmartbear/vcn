@extends('web.britishsummerv2.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')
        <!-- Link Swiper's CSS -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/bs.css" rel="stylesheet">
<link href="/assets/{{ConfigHelper::config('tema')}}/css/swiper/swiper.min.css" rel="stylesheet">

@stop

@section('container')
    <div class="headerbg" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/wall4.png') repeat;">
        <div class="container" id="header" style="position: relative;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo">
                        <h1 class="slogan">
                            <span></span>
                            {{trans('web.catalogo')}}
                            <br />
                            <span></span>
                            <small>2017</small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="headerbgoverlay"></div>
    </div>

    <main class="cd-main-content">
        <div class="container addmarginbottom60" id="contenido">
            @if(ConfigHelper::config('propietario') == 1)
                <div class="row">
                    <div class="col-sm-4">
                        <img class="img-responsive catalogo" src="/assets/catalogos/bs/{{App::getlocale()}}/bs.jpg">
                        <h4>Catálogo completo</h4>
                        <p><i class="fa fa-download"></i> <a href="/assets/catalogos/bs/BS_2017.pdf" target="_blank">Castellano</a><br />
                            <i class="fa fa-download"></i> <a href="/assets/catalogos/bs/BS_2017_cat.pdf" target="_blank">Català</a></p>
                    </div>

                    <div class="col-sm-4"> <img class="img-responsive catalogo" src="/assets/catalogos/bs/{{App::getlocale()}}/maxcamps.jpg">
                        <h4>Max Camps</h4>
                        <p><i class="fa fa-download"></i> <a href="/assets/catalogos/bs/BS_2017_maxcamps.pdf" target="_blank">Castellano</a><br />
                            <i class="fa fa-download"></i> <a href="/assets/catalogos/bs/BS_2017_maxcamps_cat.pdf" target="_blank">Català</a></p>
                    </div>
                    <div class="col-sm-4"> <img class="img-responsive catalogo" src="/assets/catalogos/bs/{{App::getlocale()}}/jovenes.jpg">
                        <h4>Jóvenes</h4>
                        <p>
                            <i class="fa fa-download"></i> <a href="/assets/catalogos/bs/BS_2017_jovenes.pdf" target="_blank">Castellano</a><br />
                        <i class="fa fa-download"></i> <a href="/assets/catalogos/bs/BS_2017_joves.pdf" target="_blank">Català</a></p>
                    </div>
                </div>
                <div class="row addmargintop60">
                    <div class="col-sm-4 col-sm-offset-4"> <img class="img-responsive catalogo" src="/assets/catalogos/bs/{{App::getlocale()}}/escolar.jpg">
                        <h4>Año Escolar 2017-2018</h4>
                        <p><i class="fa fa-download"></i> <a href="/assets/catalogos/bs/BS_2017_escolar.pdf" target="_blank">Castellano</a><br />
                            <i class="fa fa-download"></i> <a href="/assets/catalogos/bs/BS_2017_escolar_cat.pdf" target="_blank">Català</a></p>
                    </div>

                    <div class="col-sm-4"> <img class="img-responsive catalogo" src="/assets/catalogos/bs/{{App::getlocale()}}/adultos-familias.jpg">
                        <h4>Adultos y Familias</h4>
                        <p><i class="fa fa-download"></i> <a href="/assets/catalogos/bs/BS_2017_adultos-familias.pdf" target="_blank">Castellano</a><br />
                            <i class="fa fa-download"></i> <a href="/assets/catalogos/bs/BS_2017_adults-families.pdf" target="_blank">Català</a></p>
                    </div>
                </div>
                <div class="row addmargintop60">
                    <div class="col-sm-4  col-sm-offset-4"> <img class="img-responsive catalogo" src="/assets/catalogos/bs/{{App::getlocale()}}/colectivos.jpg">
                        <h4>Colectivos y Escuelas</h4>
                        <p><i class="fa fa-download"></i> <a href="/assets/catalogos/bs/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Castellano</a><br>
                            <i class="fa fa-download"></i> <a href="/assets/catalogos/bs/BS_2015_col-lectius-i-escoles.pdf" target="_blank">Català</a></p>
                    </div>
                </div>
            @elseif(ConfigHelper::config('propietario') == 2)
                <div class="row">
                    <div class="col-sm-4">
                        <img class="img-responsive catalogo" src="/assets/catalogos/cic/{{App::getlocale()}}/cic.jpg">
                        <h4>Catàleg complet</h4>
                        <p><i class="fa fa-download"></i> <a href="/assets/catalogos/cic/CIC_2017.pdf" target="_blank">Català</a></p>
                    </div>
                    <div class="col-sm-4"> <img class="img-responsive catalogo" src="/assets/catalogos/cic/{{App::getlocale()}}/summercamps.jpg">
                        <h4>Summer Camps / Day Camps</h4>
                        <p><i class="fa fa-download"></i> <a href="/assets/catalogos/cic/CIC_2017_colonies.pdf" target="_blank">Català</a>
                        </p>
                    </div>
                    <div class="col-sm-4"> <img class="img-responsive catalogo" src="/assets/catalogos/cic/{{App::getlocale()}}/jovenes.jpg">
                        <h4>Joves</h4>
                        <p><i class="fa fa-download"></i> <a href="/assets/catalogos/cic/CIC_2017_joves.pdf" target="_blank">Català</a><br />
                            <i class="fa fa-download"></i> <a href="/assets/catalogos/cic/CIC_2017_jovenes.pdf" target="_blank">Castellano</a></p>
                    </div>


                </div>
                <div class="row addmargintop60">

                    <div class="col-sm-4 col-sm-offset-4"> <img class="img-responsive catalogo" src="/assets/catalogos/cic/{{App::getlocale()}}/escolar.jpg">
                        <h4>Any Escolar</h4>
                        <p><i class="fa fa-download"></i> <a href="/assets/catalogos/cic/CIC_2017_escolar_cat.pdf" target="_blank">Català</a><br />
                            <i class="fa fa-download"></i> <a href="/assets/catalogos/cic/CIC_2017_escolar.pdf" target="_blank">Castellano</a></p>
                    </div>
                    <div class="col-sm-4"> <img class="img-responsive catalogo" src="/assets/catalogos/cic/{{App::getlocale()}}/adultos-familias.jpg">
                        <h4>Adults i Famílies</h4>
                        <p><i class="fa fa-download"></i> <a href="/assets/catalogos/cic/CIC_2017_adults-families.pdf" target="_blank">Català</a><br />
                            <i class="fa fa-download"></i> <a href="/assets/catalogos/cic/CIC_2017_adultos-familias.pdf" target="_blank">Castellano</a></p>
                    </div>
                </div>
            @endif
        </div>
    </main>

    <!-- Modal -->
    <div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
                </div>
                <div class="modal-body">
                    <div id="respuesta">
                        <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                        <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                            <div class="msg"></div>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                        <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                        <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                        <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                        <input type="hidden" id="curso" value="{{trans('web.catalogo')}}">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>

                    <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('extra_footer')
@stop