@extends('web.dippy.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} - {{$curso->course_seo_title}}
@stop


@section('extra_meta')
    <meta name="DC.title" lang="{{App::getLocale()}}" content="{{$curso->course_seo_title}}" />
    <meta name="Subject" lang="{{App::getLocale()}}" content="{!!Traductor::getWeb(App::getLocale(), 'Curso', 'name', $curso->id, $curso->name)!!}" />
    <meta name="Description" lang="{{App::getLocale()}}" content="{{$curso->course_seo_description}}" />
    <meta name="Keywords" lang="{{App::getLocale()}}" content="@if($curso->course_seo_tags != ''){{$curso->course_seo_tags}}@else{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}@endif" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop


@section('extra_head')
<!-- Color style -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/dippy.css" rel="stylesheet">

<script type="text/javascript">
    var $url = "{{route('ajax.info', $curso->id)}}";
</script>

{!! Html::style('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') !!}


@stop


@section('container')

    <?php
    $fotoscentro = '';
    $fotoscentroname = array();
    $path = public_path() ."/assets/uploads/center/" . $curso->centro->center_images;
    $folder = "/assets/uploads/center/" . $curso->centro->center_images;

    if (is_dir($path)) {
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

            $file = $path . '/' . $result;

            if (is_file($file)) {
                $fotoscentro .= '

                        <div class="col-md-4 col-sm-6 col-sx-12 fotos">
                            <div class="foto"><a data-fancybox="fotos" href="' . $folder . '/' . $result . '"><img class="img-responsive" src="' . $folder . '/thumb/' . $result . '" alt=""></a></div>
                        </div>';
                $fotoscentroname[] = $result;

            }
        }
    }
    ?>
    <?php
    $fotoscurso = '';
    $fotoscursoname = array();
    $path = public_path() ."/assets/uploads/course/" . $curso->course_images;
    $folder = "/assets/uploads/course/" . $curso->course_images;

    if (is_dir($path)) {
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

            $file = $path . '/' . $result;

            if (is_file($file)) {
                $fotoscurso .= '
                    <div class="col-md-4 col-sm-6 col-sx-12 fotos">
                            <div class="foto"><a data-fancybox="fotos" href="' . $folder . '/' . $result . '" ><img class="img-responsive" src="' . $folder . '/thumb/' . $result . '" alt=""></a></div>
                    </div>';
                $fotoscursoname[] = $result;
            }
        }
    }
    ?>



    <?php $overlay = false; ?>
    @if(is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada))
        <div class="headerbg white" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/{{$curso->image_portada}}'); background-size: cover;" id="intro-img">
    @elseif(is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
        <div class="headerbg white" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/{{$curso->centro->center_image_portada}}'); background-size: cover;" id="intro-img">
    @elseif(!is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
        @if(count($fotoscursoname))
            <div class="headerbg white" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/{{$fotoscursoname[rand(0,count($fotoscursoname)-1)]}}'); background-size: cover; background-position: center center;" id="intro-img">
        @elseif(!count($fotoscursoname) && count($fotoscentroname))
            <div class="headerbg white" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/{{$fotoscentroname[rand(0,count($fotoscentroname)-1)]}}'); background-size: cover;" id="intro-img">
        @else
            <div class="headerbg white" style="background: #E5E5E5 url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/{{rand(1,3)}}.png') repeat;" id="intro-img">
            <?php $overlay = true; ?>
        @endif
    @endif


        <div class="container curso" id="header" style="position: relative;">
            <div id="scroll-down">
                <div class="text">SCROLL</div>
                <span></span>
                <span></span>
                <span></span>
                <p class="arrow">
                    <span></span>
                    <span></span>
                    <span></span>
                </p>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="titulo">
                        <h1 class="slogan fixed" id="slogan">
                            {!!Traductor::getWeb(App::getLocale(), 'Curso', 'name', $curso->id, $curso->name)!!}
                            <small>@if($curso->centro->pais->name != 'España') {{Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name)}} @else {{Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name)}} @endif</small>
                        </h1>
                        <div class="especialidades">
                            {{$curso->especialidades_list}}

                        </div>
                        <div class="headerinfo">
                            <ul>
                                <li>
                                    <i class="flaticon flaticon-image-file-sketch"></i> {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_age_range', $curso->id, $curso->course_age_range)!!}
                                </li>
                                <li><i class="flaticon flaticon-favorites-star-sketch"></i> {{trans('web.'.$curso->course_language)}}</li>
                                <?php $alojas = array(); ?>
                                @if(count($curso->alojamientos))
                                    @foreach($curso->alojamientos as $alojamiento)
                                        <?php $alojas[] = Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $alojamiento->tipo->id, $alojamiento->tipo->accommodation_type_name); ?>
                                    @endforeach
                                    @foreach(array_unique($alojas) as $alojatipo)
                                        <li><i class="flaticon flaticon-home-sketch"></i> {!!$alojatipo!!}</li>
                                    @endforeach
                                @endif
                            </ul>
                            @if ($curso->course_summary != '' || $curso->course_summary != null)
                                <div class="introduccion">
                                    {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_summary', $curso->id, $curso->course_summary)!!}
                                </div>
                            @endif
                        </div>



                    </div>


                </div>

            </div>
        </div>

        @if($overlay == true)
            <div class="headerbgoverlay"></div>
        @endif

    </div>
    <main class="cd-main-content curso">

        @if( !ConfigHelper::config('web_registro') || (ConfigHelper::config('web_registro') && auth()->user()) )
        <div class="container" id="contenido">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="content-titulo">
                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'name', $curso->id, $curso->name)!!}
                        <small>@if($curso->centro->pais->name != 'España') {{Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name)}} @else {{Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name)}} @endif</small>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9 col-xs-8">
                    @if ($curso->course_promo == 1)
                        <div id="promocion">
                            <div class="icon"></div>
                            {!!Traductor::getWeb(App::getLocale(), 'Curso', 'promo_texto', $curso->id, $curso->promo_texto)!!}
                        </div>
                    @endif
                </div>
                <div class="col-sm-3 col-xs-4 text-right">
                    {{-- <a href="#plusinfomodal" class="btn btn-dippy btn-black hidden-print"
                       data-toggle="modal" data-target="#plusinfomodal">{!! trans('web.curso.solicitarplaza') !!}</a> --}}
                    <div class="btn-arrows">
                    {{-- Booking --}}
                    @include('comprar.button')

                    </div>

                </div>
            </div>
            <div class="row">

                @if ($fotoscentro != '' || $fotoscurso != '')
                    <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
                        <a class="btn btn-primary btn-sm hidden-sm hidden-md hidden-lg" id="fotos">{!! trans('web.curso.fotos') !!}</a>
                    </div>
                @endif

                <div class="col-sm-12">
                    <!-- Nav tabs -->
                    <div class="navtabscontent">
                        <ul class="nav nav-tabs" role="tablist" id="cursotabs">
                            <?php $prog = false; ?>
                            @if($curso->course_content != '' || $curso->course_content != null  || $curso->frase != '' || $curso->frase != null || $curso->requisitos != '' || $curso->requisitos != null)
                                <li class="active"><a href="#{!! trans('web.curso.programa') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.programa') !!}</a></li>
                                <?php $prog = true; ?>
                            @endif
                            @if($curso->centro->name != '' || $curso->centro->name != null || $curso->centro->description != '' || $curso->centro->description != null || $curso->centro->foods != '' || $curso->centro->foods != null || $curso->centro->center_video != '' || $curso->centro->center_video != null)
                                <li @if($prog == false)class="active"@endif><a href="#{!! trans('web.curso.centro') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.centro') !!}</a></li>
                            @endif
                            @if(count($curso->alojamientos))
                                <li><a href="#{!! trans('web.curso.alojamiento') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.alojamiento') !!}</a></li>
                            @endif
                            @if($curso->course_activities != null || $curso->course_activities != '' || $curso->course_excursions != '' || $curso->course_excursions != null || $curso->centro->center_excursions != '' || $curso->center_excursions != null || $curso->centro->center_activities != '' || $curso->centro->center_activities != null)
                                <li><a href="#{!! trans('web.curso.actividades') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.actividades') !!}</a></li>
                            @endif
                            @if($curso->monitor_name != null || $curso->monitor_name != '')
                                @if($curso->category_id == 4 || $curso->category_id == 6)
                                    <li><a href="#{!! trans('web.curso.coordinador-tab') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.coordinador') !!}</a></li>
                                @else
                                    <li><a href="#{!! trans('web.curso.monitor') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.monitor') !!}</a></li>
                                @endif
                            @endif
                            <li><a href="#{!! trans('web.curso.fechas-precios') !!}" role="tab" data-toggle="tab">{!! trans('web.curso.fechasyprecios') !!}</a></li>
                            @if($curso->course_video != '' || $curso->centro->course_video != null)
                                <li><a href="#{!! str_slug(trans('web.curso.videos')) !!}" role="tab" data-toggle="tab">{!! trans('web.curso.videos') !!}</a></li>
                            @endif
                        </ul>
                    </div>
                    @if ($fotoscentro != '' || $fotoscurso != '')
                        <a class="btn btn-primary btn-sm hidden-xs" id="fotos">{!! trans('web.curso.fotos') !!}</a>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-xs-12" id="infocurso">
                    <!-- Tab panes -->
                    <div class="tab-content">
                                        @if($curso->course_content != '' || $curso->course_content != null  || $curso->requisitos != '' || $curso->requisitos != null)
                                            <div class="tab-pane active" id="{!! trans('web.curso.programa') !!}">
                                                @if ($curso->frase != '' || $curso->frase != null)
                                                    <div class="info-general">
                                                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'frase', $curso->id, $curso->frase)!!}
                                                    </div>
                                                @endif

                                                @if ($curso->course_content != '' || $curso->course_content != null)
                                                    {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_content', $curso->id, $curso->course_content)!!}
                                                @endif

                                                <div class="idioma">
                                                    @if ($curso->course_language != '' || $curso->course_language != null)
                                                        <p><strong>{!!trans('web.curso.idiomarequerido')!!}:</strong>
                                                            @foreach(explode(',',$curso->course_language) as $nidioma)
                                                                <?php $nivelidioma[] = trans('web.'.trim($nidioma)); ?>
                                                            @endforeach
                                                            {{implode(',',$nivelidioma)}}
                                                        </p>
                                                    @endif

                                                    @if ($curso->course_minimun_language != '' || $curso->course_minimun_language != null)
                                                        <p><strong>{!!trans('web.curso.nivel')!!}:</strong> {{trans('web.'.$curso->course_minimun_language)}}</p>
                                                    @endif

                                                    @if ($curso->course_language_sessions != '' || $curso->course_language_sessions != null)
                                                        <p><strong>{!!trans('web.curso.sesiones')!!}:</strong> {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_language_sessions', $curso->id, $curso->course_language_sessions)!!}</p>
                                                    @endif
                                                </div>

                                                @if ($curso->requisitos != '' || $curso->requisitos != null)
                                                    <h4>{!! trans('web.curso.requisitos') !!}</h4>
                                                    {!!Traductor::getWeb(App::getLocale(), 'Curso', 'requisitos', $curso->id, $curso->requisitos)!!}
                                                @endif




                                                <?php
                                                $path = public_path() ."/assets/uploads/pdf/" . $curso->course_images;
                                                $folder = "/assets/uploads/pdf/" . $curso->course_images;

                                                if(is_dir($path))
                                                {
                                                    echo '<h4>'.trans('web.curso.timetable').'</h4>';
                                                    $results = scandir($path);
                                                    foreach ($results as $result) {
                                                        if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0]==='.') continue;

                                                        $file = $path . '/' . $result;

                                                        if( is_file($file) )
                                                        {
                                                            echo '<a href="'.$folder . '/' . $result.'" target="_blank"><i class="fa fa-download"></i> '.$result.'</a><br />';
                                                        }
                                                    }
                                                }
                                                ?>

                                                @if ($curso->centro->internet == 1)
                                                    <div class="internet">
                                                        <p><i class="fa fa-laptop"></i> {!! trans('web.curso.internet-disponible') !!}</p>
                                                    </div>
                                                @endif
                                                @if ($curso->centro->comentarios != '' || $curso->centro->comentarios != null)
                                                    <div class="internetopciones">
                                                        <p>{!!$curso->centro->comentarios!!}</p>
                                                    </div>
                                                @endif

                                                @if($curso->course_video != '' || $curso->centro->course_video != null)
                                                    <div class="videowrapper well">
                                                        <iframe height="300" width="500" src="https://www.youtube.com/embed/{{$curso->course_video}}" frameborder="0" allowfullscreen=""></iframe>
                                                    </div>
                                                @endif
                                                @if ($curso->course_provider_url != '' || $curso->course_provider_url != null)
                                                    <hr>
                                                    <p><i class="fa fa-info-circle"></i> {!! trans('web.curso.masinfoproveedor1') !!}
                                                    <a href="{{route('web.proveedor', $curso->id)}}" target="_blank">{!! trans('web.curso.masinfoproveedor2') !!}</a>
                                                    </p>
                                                @endif
                                            </div>
                                        @endif

                                        @if($curso->centro->name != '' || $curso->centro->name != null || $curso->centro->description != '' || $curso->centro->description != null || $curso->centro->foods != '' || $curso->centro->foods != null || $curso->centro->center_video != '' || $curso->centro->center_video != null || $curso->centro->settingup != '' || $curso->centro->settingup != null)
                                            <div class="tab-pane @if($prog == false) active @endif" id="{!! trans('web.curso.centro') !!}">
                                                <h3>{!!Traductor::getWeb(App::getLocale(), 'Centro', 'name', $curso->centro->id, $curso->centro->name)!!}<br />
                                                    <small><b>{!!Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name)!!}</b>. {!!Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name)!!}</small>
                                                </h3>
                                                @if($curso->centro->description != '' || $curso->centro->description != null)
                                                    {!!Traductor::getWeb(App::getLocale(), 'Centro', 'description', $curso->centro->id, $curso->centro->description)!!}
                                                @endif
                                                @if($curso->centro->settingup != '' || $curso->centro->settingup != null)
                                                    <h4>{!! trans('web.curso.instalaciones') !!}</h4>
                                                    {!!Traductor::getWeb(App::getLocale(), 'Centro', 'settingup', $curso->centro->id, $curso->centro->settingup)!!}
                                                @endif
                                                @if($curso->centro->foods != '' || $curso->centro->foods != null)
                                                    <h4>{!! trans('web.curso.comidas') !!}</h4>
                                                    {!!Traductor::getWeb(App::getLocale(), 'Centro', 'food', $curso->centro->id, $curso->centro->foods)!!}
                                                @endif
                                                @if($curso->centro->transport != '' || $curso->centro->transport != null)
                                                    <h4>{!! trans('web.curso.transporte') !!}</h4>
                                                    {!!Traductor::getWeb(App::getLocale(), 'Centro', 'transport', $curso->centro->id, $curso->centro->transport)!!}
                                                @endif

                                                @if($curso->centro->center_video != '' || $curso->centro->center_video != null)
                                                    <div class="videowrapper well">
                                                        <iframe height="300" width="500" src="https://www.youtube.com/embed/{{$curso->centro->center_video}}" frameborder="0" allowfullscreen=""></iframe>
                                                    </div>
                                                @endif
                                                @if($curso->centro->address != '' || $curso->centro->address != null)
                                                    <h4>{!!Traductor::getWeb(App::getLocale(), 'Centro', 'address', $curso->centro->id, $curso->centro->address)!!}</h4>
                                                    <iframe style="margin-top: 30px;"
                                                            width="100%"
                                                            height="450"
                                                            frameborder="0" style="border:0"
                                                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCNPWo91Kd3D656Z5R2U6K1vFlx65KxpE8&q={{strip_tags($curso->centro->address)}}&language={{App::getLocale()}}&zoom=12" allowfullscreen>
                                                    </iframe>
                                                @endif
                                            </div>
                                        @endif

                                        @if(count($curso->alojamientos))
                                            <div class="tab-pane" id="{!! trans('web.curso.alojamiento') !!}">
                                                @foreach($curso->alojamientos as $alojamiento)

                                                    <h4>{!!Traductor::getWeb(App::getLocale(), 'Alojamiento', 'name', $alojamiento->id, $alojamiento->name)!!}</h4>
                                                    <p>{!!Traductor::getWeb(App::getLocale(), 'Alojamiento', 'accommodation_description', $alojamiento->id, $alojamiento->accommodation_description)!!}</p>

                                                    <?php
                                                    $fotosaloja = '';
                                                    $fotosalojaname = array();
                                                    $path = public_path() ."/assets/uploads/alojamiento/" . $alojamiento->image_dir;
                                                    $folder = "/assets/uploads/alojamiento/" . $alojamiento->image_dir;

                                                    if (is_dir($path)) {
                                                        $results = scandir($path);
                                                        foreach ($results as $result) {
                                                            if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                                                            $file = $path . '/' . $result;

                                                            if (is_file($file)) {
                                                                $fotosaloja .= '
                                                                    <div class="col-md-4">
                                                                        <a data-fancybox="aloja" href="'.$folder.'/'.$result.'"><div style="position:relative; overflow:hidden; padding-bottom:100%;"><img class="img-responsive full-width" style="position: absolute;" src="'.$folder.'/'.$result.'" alt=""></div></a>
                                                                    </div>';
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    @if($fotosaloja != '')
                                                        <div id="fotos-aloja" class="hidden-print">
                                                            <div class="row">
                                                                {!!$fotosaloja!!}
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @endif

                                        @if($curso->course_activities != null || $curso->course_activities != '' || $curso->course_excursions != '' || $curso->course_excursions != null || $curso->centro->center_excursions != '' || $curso->center_excursions != null || $curso->centro->center_activities != '' || $curso->centro->center_activities != null)
                                            <div class="tab-pane" id="{!! trans('web.curso.actividades') !!}">
                                                @if($curso->course_activities != null || $curso->course_activities != '' || $curso->centro->center_activities != null || $curso->centro->center_activities != '')
                                                    <h3>{!! trans('web.curso.actividades') !!}</h3>
                                                    @if($curso->course_activities != null || $curso->course_activities != '')
                                                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_activities', $curso->id, $curso->course_activities)!!}
                                                    @elseif(($curso->course_activities == null || $curso->course_activities == '') && ($curso->centro->center_activities != null || $curso->centro->center_activities != ''))
                                                        {!!Traductor::getWeb(App::getLocale(), 'Centro', 'center_activities', $curso->centro->id, $curso->centro->center_activities)!!}
                                                    @endif
                                                @endif
                                                @if($curso->course_excursions != null || $curso->course_excursions != '' || $curso->centro->center_excursions != null || $curso->centro->center_excursions != '')
                                                    <h3>{!! trans('web.curso.excursiones') !!}</h3>
                                                    @if($curso->course_excursions != null || $curso->course_excursions != '')
                                                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_excursions', $curso->id, $curso->course_excursions)!!}
                                                    @elseif(($curso->course_excursions == null || $curso->course_excursions == '') && ($curso->centro->center_excursions != null || $curso->centro->center_excursions != ''))
                                                        {!!Traductor::getWeb(App::getLocale(), 'Centro', 'center_excursions', $curso->centro->id, $curso->centro->center_excursions)!!}
                                                    @endif
                                                @endif
                                            </div>
                                        @endif


                                        @if($curso->monitor_name != null || $curso->monitor_name != '')
                                                @if($curso->category_id == 4 || $curso->category_id == 6)
                                                    <div class="tab-pane" id="{!! trans('web.curso.coordinador-tab') !!}">
                                                @else
                                                    <div class="tab-pane" id="{!! trans('web.curso.monitor') !!}">
                                                @endif
                                                <h4>{!! $curso->monitor_name !!}</h4>
                                                <div class="row">
                                                @if($curso->monitor_foto != null || $curso->monitor_foto != '')
                                                    <div class="col-sm-4  col-xs-12">
                                                        <img class="img-responsive img-thumbnail" src="{{$curso->monitor_foto}}" />
                                                    </div>
                                                @endif
                                                @if($curso->monitor_desc != null || $curso->monitor_desc != '')
                                                    <div class="col-sm-8  col-xs-12">
                                                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'monitor_desc', $curso->id, $curso->monitor_desc)!!}
                                                    </div>
                                                @endif
                                                </div>
                                            </div>
                                        @endif



                                        <div class="tab-pane fechas-precios" id="{!! trans('web.curso.fechas-precios') !!}">
                                            {{-- convocatorias abiertas --}}
                                            {{-- precio curso: fecha + duracion => fecha_fin y precio --}}

                                            {{-- OJO CON VARIAS CONVOCATORIAS ABIERTAS A LA VEZ (se supone que no debe ser) PERO HABRÍA QUE CAMBIAR EL SCRIPT --}}

                                            @if(count($curso->convocatoriasAbiertas))
                                                @foreach($curso->convocatoriasAbiertas as $ca)
                                                    @if($ca->convocatory_open_status == 1)
                                                        <h2>{!!Traductor::getWeb(App::getLocale(), 'Abierta', 'convocatory_open_name', $ca->id, $ca->convocatory_open_name)!!}</h2>
                                                        <h6 class="separator">{!! trans('web.curso.curso') !!}</h6>

                                                        <script type="text/javascript">
                                                            var $startDate = "{{$ca->convocatory_open_valid_start_date}}";
                                                            var $endDate = "{{$ca->convocatory_open_valid_end_date}}";
                                                            var $dayDate = "{{$ca->convocatory_open_start_day}}";
                                                        </script>

                                                        <table class="table tabla-precios">
                                                            <thead>
                                                            <tr class="thead">
                                                                <td>{!! trans('web.curso.finicio') !!}</td>
                                                                <td>{!! trans('web.curso.duracion') !!}</td>
                                                                <td>{!! trans('web.curso.ffin') !!}</td>
                                                                <td align="right">{!! trans('web.curso.precio') !!}</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>

                                                                <td width="45%">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                        {!! Form::text('booking-cabierta-fecha_ini', '', array( 'id'=>'booking-cabierta-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'datetime form-control')) !!}
                                                                    </div>
                                                                </td>
                                                                <td width="20%">
                                                                    {!! Form::select('booking-cabierta-semanas', ConfigHelper::getSemanas($curso->duracion_name), 0, array( 'id'=> 'booking-cabierta-semanas', 'class' => 'form-control')) !!}
                                                                </td>
                                                                <td width="15%">
                                                                    {!! Form::text('booking-cabierta-fecha_fin', '', array( 'id'=>'booking-cabierta-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
                                                                </td>
                                                                <td width="20%" align='right'>
                                                                    <span class="booking-cabierta-precio" id="booking-cabierta-precio"></span>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                        <h6 class="separator">{!! trans('web.curso.alojamiento') !!}</h6>
                                                        {{-- precio alojamiento: fecha + duracion => fecha_fin y precio --}}
                                                        <table class="table tabla-precios">
                                                                <thead>
                                                                <tr class="thead">
                                                                    <td>{!! trans('web.curso.alojamiento') !!}</td>
                                                                    <td>{!! trans('web.curso.finicio') !!}</td>
                                                                    <td>{!! trans('web.curso.duracion') !!}</td>
                                                                    <td>{!! trans('web.curso.ffin') !!}</td>
                                                                    <td align="right">{!! trans('web.curso.precio') !!}</td>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td width="20%">
                                                                        {!! Form::select('booking-alojamiento', $curso->alojamientos->pluck('name','id'), 0, array( 'id'=> 'booking-alojamiento', 'class' => 'form-control')) !!}
                                                                    </td>
                                                                    <td width="25%">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                            {!! Form::text('booking-alojamiento-fecha_ini', '', array( 'id'=>'booking-alojamiento-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'datetime form-control')) !!}
                                                                        </div>
                                                                    </td>
                                                                    <td width="20%">
                                                                        {!! Form::select('booking-alojamiento-semanas', ConfigHelper::getSemanas($curso->duracion_name), 0, array( 'id'=> 'booking-alojamiento-semanas', 'class' => 'form-control')) !!}
                                                                    </td>
                                                                    <td width="15%">
                                                                        {!! Form::text('booking-alojamiento-fecha_fin', '', array( 'id'=>'booking-alojamiento-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
                                                                    </td>
                                                                    <td width="20%" align='right'>
                                                                        <span id="booking-alojamiento-precio"></span>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>



                                                        <h6>{!!trans('web.curso.alojamientoextrasobligatorios')!!}</h6>
                                                        <div id="booking-alojamiento-extras"></div>

                                                        <h6>{!!trans('web.curso.extrasobligatorios')!!}</h6>
                                                        <div id="booking-cabierta-extras">
                                                            <ul class="incluye" id="extras-obligatorios">
                                                                @foreach($curso->extras_obligatorios as $extra)
                                                                    <li>{{$extra->name}}: <b>{{ConfigHelper::parseMoneda($extra->precio, $extra->moneda)}}</b></li>
                                                                @endforeach
                                                            </ul>
                                                            <ul class="incluye" id="extras-genericos-obligatorios">
                                                                @foreach($curso->extras_genericos_obligatorios as $extra)
                                                                    <li>{{$extra->name}}: <b>{{ConfigHelper::parseMoneda($extra->precio, $extra->moneda)}}</b></li>
                                                                @endforeach
                                                            </ul>
                                                            <ul class="incluye" id="extras-centro-obligatorios">
                                                                @foreach($curso->extras_centro_obligatorios as $extra)
                                                                    <li>{{$extra->name}}: <b>{{ConfigHelper::parseMoneda($extra->precio, $extra->moneda)}}</b></li>
                                                                @endforeach
                                                            </ul>
                                                        </div>

                                                        <div id="booking-subtotal-div" class="row preciosubtotal collapse">
                                                            <div class="col-sm-4">
                                                                <h5>{!! trans('web.curso.subtotal') !!}</h5>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <div id="booking-subtotal" class="text-black text-right pull-right"></div>
                                                            </div>
                                                        </div>


                                                        <div class="row preciototal">
                                                            <div class="col-sm-4">
                                                                <h5>{!! trans('web.curso.total') !!}</h5>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <div id="booking-total" class="text-black text-right pull-right"><small>{!!trans('web.curso.selecciona')!!}</small></div>
                                                            </div>
                                                        </div>


                                                        <h4 class="divisas">{!!trans('web.curso.divisas')!!}</h4>
                                                        <ul class="divisas">
                                                            @foreach($curso->divisas_txt as $divisa)
                                                                <li>{{$divisa}}</li>
                                                            @endforeach
                                                        </ul>


                                                        <h4 class="separator">{!!trans('web.curso.precioincluye')!!}</h4>
                                                        <ul class="incluye">
                                                            @foreach($ca->incluyes as $cai)
                                                                @if($cai->incluye->tipo == 0)
                                                                    <li>{!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cai->incluye->id, $cai->incluye->name) !!}</li>
                                                                @elseif($cai->incluye->tipo == 1 && $cai->valor != 0)
                                                                    <li>{{$cai->valor}} {!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cai->incluye->id, $cai->incluye->name) !!}</li>
                                                                @endif
                                                            @endforeach
                                                            @if($ca->incluye_horario != '')<li>{!! trans('web.curso.incluyeclasesidioma') !!} {!! Traductor::getWeb(App::getLocale(), 'Abierta', 'incluye_horario', $ca->id, $ca->incluye_horario) !!}</li>@endif
                                                            @if($ca->convocatory_open_price_include != ''){!! strip_tags(Traductor::getWeb(App::getLocale(), 'Abierta', 'convocatory_open_price_include', $ca->id, $ca->convocatory_open_price_include),'<p><li><a><b><strong><em>') !!}@endif
                                                        </ul>

                                                    @endif
                                                @endforeach

                                            @endif


                                            <?php $plazas_disponibles = 0; ?>
                                            <?php $divisas = array(); ?>
                                            <?php $mostrardivisa = true; ?>
                                            @if(count($curso->convocatoriasCerradas) && $curso->convocatoriasCerradas->contains('convocatory_semiopen', 0))
                                                <div class="row">
                                                    @foreach($curso->alojamientos as $aloja)
                                                        <div class="col-sm-12"><h6 class="separator">{!!Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $aloja->accommodation_type_id, $aloja->accommodation_name)!!}</h6></div>
                                                    <?php
                                                        $convos = $curso->convocatoriasCerradas;
                                                        $convosordenadas = $convos->sortBy(function($convos) {
                                                            return sprintf('%-12s%s', $convos->convocatory_close_start_date, $convos->convocatory_close_duration_weeks);
                                                        });
                                                    ?>

                                                        @foreach($convosordenadas as $cc)

                                                            @if($cc->activo_web == 1)

                                                                @if($cc->alojamiento_id == $aloja->id)
                                                                    @if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 0)
                                                                        <div class="col-sm-4 precio">
                                                                            <?php
                                                                                if(isset($cc->precio_auto)){
                                                                                    if($cc->precio_auto->proveedor_moneda_id == $cc->moneda->id){
                                                                                        $mostrardivisa = false;
                                                                                    }
                                                                                    $divisas[$cc->precio_auto->proveedor_moneda_id] = '1 '.$cc->moneda_name.' = '.VCN\Models\Monedas\Moneda::where('id',$cc->precio_auto->proveedor_moneda_id)->pluck('currency_rate').' '.VCN\Models\Monedas\Moneda::where('id',$cc->precio_auto->proveedor_moneda_id)->pluck('currency_name');
                                                                                }else{
                                                                                    $mostrardivisa = false;
                                                                                }
                                                                            ?>



                                                                            <!-- Calcular Precios alojamiento -->
                                                                            <?php $precio_aloja = $aloja->calcularPrecio( Carbon::parse($cc->convocatory_close_start_date)->format('d/m/Y'), Carbon::parse($cc->convocatory_close_end_date)->format('d/m/Y'), $cc->convocatory_close_duration_weeks) ?>

                                                                            <h5>{{date('d.m.Y', strtotime($cc->convocatory_close_start_date))}} <span>{!! trans('web.curso.al') !!}</span> {{date('d.m.Y', strtotime($cc->convocatory_close_end_date))}}</h5>
                                                                            <p class="duracion">{{$cc->convocatory_close_duration_weeks}} {{trans_choice('web.curso.'.ConfigHelper::getPrecioDuracionUnitCerrada($cc->duracion_fijo),$cc->convocatory_close_duration_weeks)}}</p>
                                                                            @if($cc->dto_early == null || $cc->dto_early == '')
                                                                                @if($precio_aloja['importe'] != 0)
                                                                                    <!-- Precio curso -->
                                                                                    <p><b><span class="text-capitalize">{!! trans('web.curso.programa') !!}</span>: {{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b>
                                                                                    <!-- Precio alojamiento -->
                                                                                    <br /><b><span class="text-capitalize">{!! trans('web.curso.alojamiento') !!}</span>: {{ ConfigHelper::parseMoneda($precio_aloja['importe'], $precio_aloja['moneda']) }}</b></p>
                                                                                @else
                                                                                    <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b></p>
                                                                                @endif
                                                                            @else
                                                                                @if((strtotime(date('Y-m-d')) >= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->desde)) && (strtotime(date('Y-m-d')) <= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->hasta)))
                                                                                    <p><s>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</s></p>
                                                                                    <p><img class="earlybird" src="/assets/britishsummer/img/earlybird.png"> <b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price-(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->importe), \VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->moneda_name) }}</b></p>
                                                                                @else
                                                                                    @if($precio_aloja['importe'] != 0)
                                                                                        <!-- Precio curso -->
                                                                                        <p><b><span class="text-capitalize">{!! trans('web.curso.programa') !!}</span>: {{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b>
                                                                                        <!-- Precio alojamiento -->
                                                                                        <br /><b><span class="text-capitalize">{!! trans('web.curso.alojamiento') !!}</span>: {{ ConfigHelper::parseMoneda($precio_aloja['importe'], $precio_aloja['moneda']) }}</b></p>
                                                                                    @else
                                                                                        <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b></p>
                                                                                    @endif
                                                                                @endif
                                                                            @endif

                                                                            <a class="verincluye collapsed" data-toggle="collapse" data-target="#incluye-{{$cc->id}}" aria-expanded="false" aria-controls="collapseExample">
                                                                                {!!trans('web.curso.precioincluye')!!}
                                                                            </a>
                                                                            <div class="collapse" id="incluye-{{$cc->id}}">
                                                                                <ul class="incluye">
                                                                                    @foreach($cc->incluyes as $cci)
                                                                                        @if($cci->incluye->tipo == 0)
                                                                                            <li>{!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cci->incluye->id, $cci->incluye->name) !!}</li>
                                                                                        @elseif($cci->incluye->tipo == 1 && $cci->valor != 0)
                                                                                            <li>{{$cci->valor}} {!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cci->incluye->id, $cci->incluye->name) !!}</li>
                                                                                        @endif
                                                                                    @endforeach
                                                                                    @if($cc->incluye_horario != '')<li>{!! trans('web.curso.incluyeclasesidioma') !!} {!! Traductor::getWeb(App::getLocale(), 'Cerrada', 'incluye_horario', $cc->id, $cc->incluye_horario) !!}</li>@endif
                                                                                    @if($cc->convocatory_close_price_include != ''){!! strip_tags(Traductor::getWeb(App::getLocale(), 'Cerrada', 'convocatory_close_price_include', $cc->id, $cc->convocatory_close_price_include),'<p><li><a><b><strong><em>') !!}@endif
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endforeach

                                                    @endforeach
                                                </div>
                                            @endif


                                            @if($curso->convocatoriasCerradas->contains('convocatory_semiopen', 1))
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        @if(count($curso->convocatoriasCerradas) && $curso->convocatoriasCerradas->contains('convocatory_semiopen', 0))
                                                            <h4 class="separator">{!! trans('web.curso.planb') !!}</h4>
                                                            <p>{!! trans('web.curso.planbfrase') !!}</p>
                                                        @endif
                                                    </div>
                                                    <?php
                                                    $convosSC = $curso->convocatoriasCerradas;
                                                    $convosSCordenadas = $convosSC->sortBy(function($convosSC) {
                                                        return sprintf('%-12s%s', $convosSC->convocatory_close_start_date, $convosSC->convocatory_close_duration_weeks);
                                                    });
                                                    ?>
                                                    @foreach($convosSCordenadas as $cc)
                                                        @if($cc->activo_web == 1)
                                                            @if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 1)
                                                                <?php
                                                                    if(isset($cc->precio_auto)){
                                                                        if($cc->precio_auto->proveedor_moneda_id == $cc->moneda->id){
                                                                            $mostrardivisa = false;
                                                                        }
                                                                        $divisas[$cc->precio_auto->proveedor_moneda_id] = '1 '.$cc->moneda_name.' = '.VCN\Models\Monedas\Moneda::where('id',$cc->precio_auto->proveedor_moneda_id)->pluck('currency_rate').' '.VCN\Models\Monedas\Moneda::where('id',$cc->precio_auto->proveedor_moneda_id)->pluck('currency_name');
                                                                    }else{
                                                                        $mostrardivisa = false;
                                                                    }
                                                                ?>
                                                                <div class="col-sm-6 precio">
                                                                    @if($cc->alojamiento_id != '' || $cc->alojamiento_id != 0)<h6>{!! Traductor::getWeb(App::getLocale(), 'Alojamiento', 'name', $cc->alojamiento_id, \VCN\Models\Alojamientos\Alojamiento::find($cc->alojamiento_id)->name) !!}</h6>@endif
                                                                    <h5><span>{!! trans('web.curso.entre') !!}</span> {{date('d.m.Y', strtotime($cc->convocatory_close_start_date))}} <span>{!! trans('web.curso.yel') !!}</span> {{date('d.m.Y', strtotime($cc->convocatory_close_end_date))}}</h5>
                                                                    <p class="duracion">{{$cc->convocatory_close_duration_weeks}} {{trans_choice('web.curso.'.ConfigHelper::getPrecioDuracionUnitCerrada($cc->duracion_fijo),$cc->convocatory_close_duration_weeks)}}</p>

                                                                        @if($cc->dto_early == null || $cc->dto_early == '')
                                                                            <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b></p>
                                                                        @else
                                                                            @if((strtotime(date('Y-m-d')) >= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->desde)) && (strtotime(date('Y-m-d')) <= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->hasta)))
                                                                                <p><s>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</s></p>
                                                                                <p><img class="earlybird" src="/assets/britishsummer/img/earlybird.png"> <b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price-(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->importe), \VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->moneda_name) }}</b></p>
                                                                            @else
                                                                                <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b></p>
                                                                            @endif
                                                                        @endif

                                                                    <a class="verincluye collapsed" data-toggle="collapse" data-target="#incluye-{{$cc->id}}" aria-expanded="false" aria-controls="collapseExample">
                                                                        {!!trans('web.curso.precioincluye')!!}
                                                                    </a>
                                                                    <div class="collapse" id="incluye-{{$cc->id}}">
                                                                        <ul class="incluye">
                                                                            @foreach($cc->incluyes as $cci)
                                                                                @if($cci->incluye->tipo == 0)
                                                                                    <li>{!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cci->incluye->id, $cci->incluye->name) !!}</li>
                                                                                @elseif($cci->incluye->tipo == 1 && $cci->valor != 0)
                                                                                    <li>{{$cci->valor}} {!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cci->incluye->id, $cci->incluye->name) !!}</li>
                                                                                @endif
                                                                            @endforeach
                                                                            @if($cc->incluye_horario != '')<li>{!! trans('web.curso.incluyeclasesidioma') !!} {!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'incluye_horario', $cc->id, $cc->incluye_horario) !!}</li>@endif
                                                                            @if($cc->convocatory_close_price_include != ''){!! strip_tags(Traductor::getWeb(App::getLocale(), 'Cerrada', 'convocatory_close_price_include', $cc->id, $cc->convocatory_close_price_include),'<p><li><a><b><strong><em>') !!}@endif
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </div>
                                            @endif


                                            @if($curso->es_convocatoria_multi == 1)
                                                {!!Traductor::getWeb(App::getLocale(), 'Curso', 'preciosyfechas', $curso->id, $curso->preciosyfechas)!!}
                                            @endif


                                            @if($mostrardivisa == true)
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h4 class="divisas">{!!trans('web.curso.divisas')!!}</h4>
                                                    <ul class="divisas">
                                                        @if(count($divisas) > 0)
                                                            <?php $divisas = array_unique($divisas); ?>
                                                        @endif
                                                        @foreach($divisas as $divisa)
                                                            <li>{{$divisa}}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @endif

                                            @if(count($curso->extras))
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h4 class="separator">{!! trans('web.curso.extras') !!}</h4>
                                                        @foreach($curso->extras as $e)
                                                            <h6>{!!Traductor::getWeb(App::getLocale(), 'CursoExtra', 'course_extras_name', $e->id, $e->course_extras_name)!!}</h6>
                                                            <p>{!!Traductor::getWeb(App::getLocale(), 'CursoExtra', 'course_extras_description', $e->id, $e->course_extras_description)!!}</p>
                                                            @if($e->course_extras_price > 0)
                                                                <p><b>{{ ConfigHelper::parseMoneda($e->course_extras_price, $e->moneda) }}@if($e->course_extras_unit) / {{trans_choice('web.curso.'.$e->tipo->name, 1)}} @endif</b></p>
                                                            @else
                                                                <p><b>{!! trans('web.curso.incluido') !!}</b></p>
                                                            @endif
                                                        @endforeach
                                                        {{--
                                                        @foreach($curso->extrasGenericos as $eg)
                                                            <h6>{{$eg->name}}</h6>
                                                            @if($eg->precio > 0)
                                                                <p><b>{{ ConfigHelper::parseMoneda($eg->precio, $eg->moneda) }}@if($eg->generico->generic_unit_id == 1) / {{trans_choice('web.curso.'.$eg->generico->tipo->name, 1)}} @endif</b></p>
                                                            @else
                                                                <p><b>{!! trans('web.curso.incluido') !!}</b></p>
                                                            @endif
                                                        @endforeach
                                                        --}}
                                                    </div>
                                                </div>
                                            @endif

                                        </div>

                                        @if($curso->course_video != '' || $curso->centro->course_video != null)
                                            <div class="tab-pane" id="{!! str_slug(trans('web.curso.videos')) !!}">
                                                    <div class="row">
                                                        <div class="videowrapper well">
                                                            <iframe height="300" width="500" src="https://www.youtube.com/embed/{{$curso->course_video}}" frameborder="0" allowfullscreen=""></iframe>
                                                        </div>
                                                    </div>
                                            </div>
                                        @endif



                                    </div>
                                    <!-- end tabs -->
                                </div>
            </div>
            <div id="fotos-curso" class="hidden-print" style="display: none !important;">
                    {!! $fotoscentro !!}
                    {!! $fotoscurso !!}
            </div>

            <div class="row">
                @include('web.dippy.includes.copyright')
            </div>
        </div>
        @else

            <div class="container" id="contenido">
                @include('web.dippy.includes.login')
            </div>

        @endif

        @include('web.dippy.includes.footer')

    </main>

    <div class="loading">Loading&#8230;</div>


    <!-- Modal -->
    <div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img class="modal-logo" src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" />
                </div>
                <div class="modal-body">
                    <div id="respuesta">
                        <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                        <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                            <div class="msg"></div>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                        <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                        <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                        <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                        <input type="hidden" id="curso" value="{{$curso->name}}">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});">Enviar</button>

                    <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    {{-- Booking --}}
    @include('comprar.modal')

    @stop

    @section('extra_footer')
                <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js"></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.17.0/TweenMax.min.js"></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>

    <script src="/assets/{{ConfigHelper::config('tema')}}/js/bootstrap-tabcollapse.js" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#comprar-error').click( function(){
                alert('Ya tiene un booking online iniciado. Tiene que completar o cancelar el que tiene en curso.');
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.msg').hide();
            $("#plusinfoenviar").click(function () {
                //console.log('validar');
                if ($('#name').val() == '') {
                    $('.msg').html('Debes indicar un nombre de contacto');
                    $('.msg').show();
                    return false;
                }
                if ($('#email').val() == '') {
                    $('.msg').html('Debes indicar un teléfono o un email de contacto');
                    $('.msg').show();
                    return false;
                }

                if ($('#email').val() != '') {
                    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
                    if (re.test($('#name').val())) {
                        $('.msg').html('El email no tiene un formato correcto');
                        $('.msg').show();
                        return false
                    }

                }

                if (!/^([0-9])*$/.test($('#tel').val())) {
                    $('.msg').html('El campo teléfono tiene que ser numérico');
                    $('.msg').show();
                    return false
                }


                post_data = {
                    'name': $('#name').val(),
                    'tel': $('#tel').val(),
                    'email': $('#email').val(),
                    'curso': $('#curso').val()
                };
                $.ajax({
                    type: "POST",
                    url: "/assets/{{ConfigHelper::config('tema')}}/includes/plusinfosend-{{ConfigHelper::config('sufijo')}}.php",
                    data: post_data,
                    success: function (msg) {
                        //console.log(msg);
                        $("#respuesta").html(msg);
                        $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                        ga('send', 'Solicitud', 'button', 'click', "{{$curso->name}}", 1);
                    },
                    error: function () {
                        alert("error!!");
                    }
                });
            });





        $('form input').blur(function () {
            $('.msg').hide();
        });


        $(function () {
            var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');

            $('.nav-tabs a').click(function (e) {
                $(this).tab('show');
                window.location.hash = this.hash;
                //.scrollTop($(window).height());
                $('html,body').animate({
                    scrollTop: $(window).height()
                }, 1000);
            });
        });






        $('#fotos-curso [data-fancybox="fotos"]').fancybox({
            // Options will go here
            infobar : true,
            buttons : true,
            slideShow  : true,
            fullScreen : true,
            thumbs     : true,
            closeBtn   : true,
            thumbs : {
                showOnStart   : true, // Display thumbnails on opening
                hideOnClosing : true   // Hide thumbnail grid when closing animation starts
            },
        });


        $('#fotos-aloja [data-fancybox="aloja"]').fancybox({
            // Options will go here
            infobar : true,
            buttons : true,
            slideShow  : true,
            fullScreen : true,
            thumbs     : true,
            closeBtn   : true,
            thumbs : {
                showOnStart   : true, // Display thumbnails on opening
                hideOnClosing : true   // Hide thumbnail grid when closing animation starts
            },
        });


        $('a#fotos').on('click', function(){
            $('#fotos-curso [data-fancybox="fotos"]').first().trigger('click');
        })

        $('#cursotabs').tabCollapse();



        // init ScrollMagic Controller
        var controller = new ScrollMagic.Controller();


        var scene = new ScrollMagic.Scene({
            triggerElement: "#intro-img",
            offset: $(window).height(),
            triggerHook: "onLeave",
            duration: $(window).height(),
        })
                .setPin("#intro-img",{pushFollowers: true})
                .on("update", updateText)
                //.addIndicators({name: "2-----"}) // add indicators (requires plugin)
                .addTo(controller);

        var scene2 = new ScrollMagic.Scene({
            triggerElement: ".cd-main-content", // point of execution
            triggerHook: "onEnter",
            offset: ($(window).height()),
            duration: $(".cd-main-content").height() // pin the element for a total of 400px
        })
                .setPin(".cd-main-content") // the element we want to pin
                //.addIndicators({name: "3-----"}) // add indicators (requires plugin)
                .addTo(controller);


        function headerbg(){
            var scroll = $(window).scrollTop();

            if (scroll >= 50) {
                $(".cd-main-header").removeClass("clearHeader");
                $(".cd-main-header").removeClass("clearHeader");
            } else {
                $(".cd-main-header").addClass("clearHeader");
            }
        }
        $(window).scroll(function() {
            headerbg();
        });


        function updateText (e) {
            var opacity = 1 - (e.scrollPos/100);
            $(".titulo").css({'opacity': opacity});
        }

        headerbg();


            $('.loading').fadeOut();

        });

    </script>

    @if($curso->rs_pinterest != '' && ConfigHelper::config('propietario') == 1)
        <script async defer src="//assets.pinterest.com/js/pinit.js"></script>
    @endif

    @if($errors->first())
        <script type="text/javascript">
            $('#bookingModal').modal('show');
        </script>
    @endif

    <script src="assets/js/manage-web.js"></script>
    <script src="assets/plugins/moment-with-locales.min.js"></script>
    <script src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

@stop
