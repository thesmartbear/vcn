@extends('web.dippy.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop


@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{urldecode(trans('web.seo-'.ConfigHelper::config('sufijo').'.subject'))}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
@stop

@section('extra_head')
    <!-- Link Swiper's CSS -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/dippy.css" rel="stylesheet">

    {!! Html::style('assets/plugins/multiselect/css/bootstrap-multiselect.css') !!}
    {!! Html::style('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') !!}


    <style>

        .btn-group .multiselect {
            padding: 15px 5px;
            background: none;
            border: 4px solid black;
        }

        .btn-dippy.btn-home{
            color: #333;
            border: 4px solid #d7dd3b;
            background-color: #d7dd3b;
            padding: 15px 30px;
        }

    </style>

@stop

@section('container')
    <div class="fullscreen background" style="background-image:url('/assets/dippy/home/dippyhomebg4.jpg');" data-img-width="1600" data-img-height="1064">
        <div class="content-a">
            <div class="content-b">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        {!! Form::open(['url' => route('web.buscar.pais'), 'method' => 'POST', 'class' => 'form-inline']) !!}
                            <div class="form-group">
                                <select class="form-control" id="select-especialidad-filtro" name="especialidad[]" multiple="multiple">
                                    @foreach(\VCN\Models\CMS\EspecialidadWeb::where('activo',1)->get() as $esp)
                                        <option value="{{$esp->id}}">{{$esp->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control" id="select-pais-filtro" name="pais[]" multiple="multiple">
                                    @foreach(\VCN\Models\Pais::where('activo_web',1)->get() as $esp)
                                        <option value="{{$esp->id}}">{{$esp->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control" id="select-edad-filtro" name="edad[]" multiple="multiple">
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">+18</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-dippy btn-home">buscar</button>
                        {!! Form::close() !!}
                    </div>
                </div>

                <div id="steps">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2><span>Practica lo que más te gusta</span><br />en un ambiente de inmersión lingüística total</h2>
                            </div>
                        </div>
                        <div class="row steps">
                            <div class="col-sm-4">
                                <span class="number">1</span> Busca
                                <p>Cuéntanos qué es lo que más te interesa</p>
                            </div>
                            <div class="col-sm-4">
                                <span class="number">2</span> Elige
                                <p>Comprueba nuestra selección especial de aventuras para ti</p>
                            </div>
                            <div class="col-sm-4">
                                <span class="number">3</span> Reserva
                                <p><i class="fa fa-chevron-circle-right"></i> <b>Directamente.</b> Utilízanos solo como buscador de experiencias.</p>
                                <p><i class="fa fa-chevron-circle-right"></i> <b>A través de Dippy</b> para usar todo nuestro potencial.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @if( isset($cursos) && $cursos->count() )

        <main class="cd-main-content lista">

            <div class="container" id="contenido">

                <div class="row">
                    <div class="col-sm-12 col-xs-12 categorias">
                        <div id="programas">
                            @if($cursos != '')
                                <div id="cursos-lista">
                                    <div class="fail-message"><span>{!! trans('web.sinresultados') !!}</span></div>

                                    <div class="slides">
                                        @foreach($cursos as $curso)
                                            <?php
                                            $fotoscentro = '';
                                            $fotoscentroname = array();
                                            $path = public_path() ."/assets/uploads/center/" . $curso->centro->center_images;
                                            $folder = "/assets/uploads/center/" . $curso->centro->center_images;

                                            if (is_dir($path)) {
                                                $results = scandir($path);
                                                foreach ($results as $result) {
                                                    if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                                                    $file = $path . '/' . $result;

                                                    if (is_file($file)) {
                                                        $fotoscentroname[] = $result;

                                                    }
                                                }
                                            }
                                            ?>
                                            <?php
                                            $fotoscurso = '';
                                            $fotoscursoname = array();
                                            $path = public_path() ."/assets/uploads/course/" . $curso->course_images;
                                            $folder = "/assets/uploads/course/" . $curso->course_images;

                                            if (is_dir($path)) {
                                                $results = scandir($path);
                                                foreach ($results as $result) {
                                                    if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                                                    $file = $path . '/' . $result;

                                                    if (is_file($file)) {
                                                        $fotoscursoname[] = $result;
                                                    }
                                                }
                                            }
                                            ?>

                                            <? $espes = array(); ?>
                                            @foreach($curso->especialidades as $e)
                                                <? $espes[] = str_slug($e->especialidad->name); ?>
                                            @endforeach

                                            <? $alojas = array(); ?>
                                            @foreach($curso->alojamientos as $alojamiento)
                                                <? $alojascurso[] = str_slug($alojamiento->tipo->accommodation_type_name); ?>
                                            @endforeach


                                            <?php $cursolink = "/".str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->pais->id, $curso->pais->name))."/".Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug).".html"; ?>



                                            @if(is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada))
                                                <div class="mix one {{str_slug($curso->centro->pais->name)}} {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug($curso->centro->pais->name)}}">
                                                    @if($curso->course_promo == 1)
                                                        <div class="promo-lista">
                                                            <i class="fa fa-asterisk"></i>
                                                        </div>
                                                    @endif
                                                    <div class="fotocurso" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/{{$curso->image_portada}}');">
                                                        <a href="{{$cursolink}}" class="foto">
                                            @elseif(is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                                                <div class="mix one {{str_slug($curso->centro->pais->name)}}  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug($curso->centro->pais->name)}}">
                                                    @if($curso->course_promo == 1)
                                                        <div class="promo-lista">
                                                            <i class="fa fa-asterisk"></i>
                                                        </div>
                                                    @endif
                                                    <div class="fotocurso" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/{{$curso->centro->center_image_portada}}');">
                                                        <a href="{{$cursolink}}" class="foto">
                                            @elseif(!is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                                                @if(count($fotoscursoname))
                                                    <div class="mix one {{str_slug($curso->centro->pais->name)}}  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug($curso->centro->pais->name)}}">
                                                        @if($curso->course_promo == 1)
                                                            <div class="promo-lista">
                                                                <i class="fa fa-asterisk"></i>
                                                            </div>
                                                        @endif
                                                        <div class="fotocurso" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/{{$fotoscursoname[rand(0,count($fotoscursoname)-1)]}}'); background-size: cover; background-position: center center;">
                                                            <a href="{{$cursolink}}" class="foto">
                                                @elseif(!count($fotoscursoname) && count($fotoscentroname))
                                                    <div class="mix one {{str_slug($curso->centro->pais->name)}}  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug($curso->centro->pais->name)}}">
                                                        @if($curso->course_promo == 1)
                                                            <div class="promo-lista">
                                                                <i class="fa fa-asterisk"></i>
                                                            </div>
                                                        @endif
                                                        <div class="fotocurso" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/{{$fotoscentroname[rand(0,count($fotoscentroname)-1)]}}'); background-size: cover;">
                                                            <a href="{{$cursolink}}" class="foto">
                                                @else
                                                    <div class="mix one {{str_slug($curso->centro->pais->name)}}  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug($curso->centro->pais->name)}}">
                                                        @if($curso->course_promo == 1)
                                                            <div class="promo-lista">
                                                                <i class="fa fa-asterisk"></i>
                                                            </div>
                                                        @endif
                                                        <div class="fotocurso" style="background: #E5E5E5;">
                                                            <a href="{{$cursolink}}" class="foto">
                                                @endif

                                            @endif
                                                            </a>
                                                        </div>

                                                        <div class="fichacurso">
                                                            <div class="row rowpais">
                                                                <small class="col-sm-6 nombrepais">@if($curso->centro->pais->name != 'España') {{Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name)}} @else {{Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name)}} @endif</small>
                                                                @if(count($curso->convocatoriasCerradas) && $curso->convocatoriasCerradas->contains('convocatory_semiopen', 0) && $curso->category_id == 2)
                                                                    <?php $plazasrestantes = 0; ?>
                                                                    @foreach($curso->convocatoriasCerradas->sortBy('convocatory_close_start_date')->sortBy('convocatory_close_duration_weeks') as $cc)
                                                                            @if($cc->activo_web == 1)
                                                                                {{--@if($cc->alojamiento_id == $aloja->id)--}}
                                                                                @if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 0)
                                                                                    <?php $plazasrestantes += $cc->plazas_disponibles; ?>
                                                                                @endif
                                                                            @endif
                                                                    @endforeach

                                                                    {{--
                                                                    @if($plazasrestantes <= 0)
                                                                        <small class="col-sm-6 pull-right text-right grupocerrado plazas">{{trans('web.grupocerrado')}}</small>
                                                                    @elseif($plazasrestantes != 0 && $plazasrestantes < 6)
                                                                        <small class="col-sm-6 pull-right text-right ultimasplazas plazas">{{trans('web.ultimasplazas')}}</small>
                                                                    @endif
                                                                    --}}
                                                                @endif
                                                            </div>
                                                            <h4>
                                                                <a href="{{$cursolink}}" class="nombrecurso">
                                                                    {!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_name', $curso->id, $curso->course_name) !!}
                                                                </a>
                                                            </h4>
                                                            <p class="edades">{!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_age_range', $curso->id, $curso->course_age_range) !!}</p>

                                                            <div class="separator-ficha"></div>

                                                            <? $alojas = array(); ?>
                                                            @foreach($curso->alojamientos as $alojamiento)
                                                                <? $alojas[] = $alojamiento->tipo->accommodation_type_name; ?>
                                                            @endforeach
                                                            <p class="cursoalojas">{{implode(array_unique($alojas),', ')}}</p>

                                                            @if(count($curso->especialidades))
                                                                <p class="especialidades pull-left">
                                                                    @foreach($curso->especialidades as $e)
                                                                        {{--$e->especialidad->name--}}
                                                                        <span>{{$e->SubespecialidadesName}}</span>
                                                                    @endforeach
                                                                </p>
                                                            @endif
                                                        </div>
                                            </div>
                                        @endforeach


                                    </div>
                                </div>

                            @else
                                {!! trans('web.sinresultados') !!}
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </main>


        <div id="results">
        <ul>
            @foreach($cursos as $curso)
                <li data-fitlro_weeks=''><a href="{{str_slug($curso->pais->name)}}/{{$curso->course_slug}}.html">{{$curso->name}}: {{$curso->weeks}}</a></li>
            @endforeach
        </ul>
        </div>
    @endif

    <div id="clients">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3 client">
                    <div class="home-thumb"><img src="/assets/dippy/home/experiencia.jpg" class="img-responsive"/></div>
                    <span class="big"><span class="counter">30</span><small> años</small></span><br>
                    <span class="small">años organizando<br>tus experiencias<br>en el extranjero</span>
                </div>
                <div class="col-sm-6 col-md-3 client">
                    <div class="home-thumb"><img src="/assets/dippy/home/20000.jpg" class="img-responsive"/></div>
                    <span class="big"><span class="counter">20.000</span></span><br>
                    <span class="small">estudiantes han viajado<br>con nosotros</span>
                </div>
                <div class="col-sm-6 col-md-3 client">
                    <div class="home-thumb"><img src="/assets/dippy/home/92.jpg" class="img-responsive"/></div>
                    <span class="big"><span class="counter">92</span>%</span><br>
                    <span class="small">de nuestros clientes<br>quedaron satisfechos<br>o muy satisfechos</span>
                </div>
                <div class="col-sm-6 col-md-3 client">
                    <div class="home-thumb"><img src="/assets/dippy/home/65.jpg" class="img-responsive"/></div>
                    <span class="big"><span class="counter">65</span>%</span><br>
                    <span class="small">de nuestros estudiantes<br>ha repetido alguno de<br>nuestros cursos</span>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img class="modal-logo" src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" />
                </div>
                <div class="modal-body">
                    <div id="respuesta">
                        <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                        <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                            <div class="msg"></div>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                        <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                        <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                        <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                        <input type="hidden" id="curso" value="home">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>

                    <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @stop

    @section('extra_footer')
    <script src="assets/plugins/multiselect/js/bootstrap-multiselect.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="assets/dippy/js/jquery.counterup.js"></script>

    <script src="//cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>

    <script src="/assets/dippy/js/filters.js"></script>

    <!-- Initialize Swiper -->
    <script>

        $(document).ready(function () {
            $('.msg').hide();
            $("#plusinfoenviar").click(function () {
                //console.log('validar');
                if ($('#name').val() == '') {
                    $('.msg').html('Debes indicar un nombre de contacto');
                    $('.msg').show();
                    return false;
                }
                if ($('#email').val() == '') {
                    $('.msg').html('Debes indicar un teléfono o un email de contacto');
                    $('.msg').show();
                    return false;
                }

                if ($('#email').val() != '') {
                    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
                    if (re.test($('#name').val())) {
                        $('.msg').html('El email no tiene un formato correcto');
                        $('.msg').show();
                        return false
                    }

                }

                if (!/^([0-9])*$/.test($('#tel').val())) {
                    $('.msg').html('El campo teléfono tiene que ser numérico');
                    $('.msg').show();
                    return false
                }


                post_data = {
                    'name': $('#name').val(),
                    'tel': $('#tel').val(),
                    'email': $('#email').val(),
                    'curso': $('#curso').val()
                };
                $.ajax({
                    type: "POST",
                    url: "/assets/{{ConfigHelper::config('tema')}}/includes/plusinfosend-{{ConfigHelper::config('sufijo')}}.php",
                    data: post_data,
                    success: function (msg) {
                        //console.log(msg);
                        $("#respuesta").html(msg);
                        $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                        ga('send', 'Solicitud', 'button', 'click', 'home', 1);
                    },
                    error: function () {
                        alert("error!!");
                    }
                });
            });

        });

        $('form input').blur(function () {
            $('.msg').hide();
        });

        var vid = document.getElementById("bgvid");

        if (vid) {
            function vidFade() {
                vid.classList.add("stopfade");
            }

            vid.addEventListener('ended', function () {
                vid.play();
                // to capture IE10
                vidFade();
            });

            var isMobile = false;
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Opera Mobile|Kindle|Windows Phone|PSP|AvantGo|Atomic Web Browser|Blazer|Chrome Mobile|Dolphin|Dolfin|Doris|GO Browser|Jasmine|MicroB|Mobile Firefox|Mobile Safari|Mobile Silk|Motorola Internet Browser|NetFront|NineSky|Nokia Web Browser|Obigo|Openwave Mobile Browser|Palm Pre web browser|Polaris|PS Vita browser|Puffin|QQbrowser|SEMC Browser|Skyfire|Tear|TeaShark|UC Browser|uZard Web|wOSBrowser|Yandex.Browser mobile/i.test(navigator.userAgent)) {
                isMobile = true;
            }
            $(document).ready(function () {
                if (isMobile == true) {
                    $('#bgvid').css({'display': 'none'});
                } else {
                    $('#bgvid').css({'display': 'block'});
                }
            });
        }


        /* fix vertical when not overflow
         call fullscreenFix() if .fullscreen content changes */
        function fullscreenFix(){
            var h = $('body').height();
            // set .fullscreen height
            $(".content-b").each(function(i){
                if($(this).innerHeight() > h){ $(this).closest(".fullscreen").addClass("overflow");
                }
            });
        }
        $(window).resize(fullscreenFix);
        fullscreenFix();

        /* resize background images */
        function backgroundResize(){
            var windowH = $(window).height();
            $(".background").each(function(i){
                var path = $(this);
                // variables
                var contW = path.width();
                var contH = path.height();
                var imgW = path.attr("data-img-width");
                var imgH = path.attr("data-img-height");
                var ratio = imgW / imgH;
                // overflowing difference
                var diff = parseFloat(path.attr("data-diff"));
                diff = diff ? diff : 0;
                // remaining height to have fullscreen image only on parallax
                var remainingH = 0;
                if(path.hasClass("parallax")){
                    var maxH = contH > windowH ? contH : windowH;
                    remainingH = windowH - contH;
                }
                // set img values depending on cont
                imgH = contH + remainingH + diff;
                imgW = imgH * ratio;
                // fix when too large
                if(contW > imgW){
                    imgW = contW;
                    imgH = imgW / ratio;
                }
                //
                path.data("resized-imgW", imgW);
                path.data("resized-imgH", imgH);
                //path.css("background-size", imgW + "px " + imgH + "px");
                path.css("background-size", imgW + "px " + "auto");
            });
        }
        $(window).resize(backgroundResize);
        $(window).focus(backgroundResize);
        backgroundResize();



        $(document).ready(function() {
            $('#select-especialidad-filtro').multiselect({
                selectAllText: 'Todas',
                selectAllValue: '0',
                allSelectedText: "Todas",
                nSelectedText: ' seleccionadas',
                enableClickableOptGroups: false,
                selectAllJustVisible: true,
                disableIfEmpty: true,
                maxHeight: 200,
                nonSelectedText: 'Especialidad',
                buttonWidth: '260px',
            });
            $('#select-pais-filtro').multiselect({
                selectAllText: 'Todas',
                selectAllValue: '0',
                allSelectedText: "Todas",
                nSelectedText: ' países',
                enableClickableOptGroups: false,
                selectAllJustVisible: true,
                disableIfEmpty: true,
                maxHeight: 200,
                nonSelectedText: 'País',
                buttonWidth: '260px',

            });
            $('#select-edad-filtro').multiselect({
                selectAllText: 'Todas',
                selectAllValue: '0',
                allSelectedText: "Todas",
                nSelectedText: ' seleccionadas',
                enableClickableOptGroups: false,
                selectAllJustVisible: true,
                disableIfEmpty: true,
                maxHeight: 200,
                nonSelectedText: 'Edad',
                buttonWidth: '180px',
            });
        });

        function headerbg(){
            var scroll = $(window).scrollTop();

            if (scroll >= 50) {
                $(".cd-main-header").removeClass("clearHeader");
            } else {
                $(".cd-main-header").addClass("clearHeader");
            }
        }
        $(window).scroll(function() {
            headerbg();
        });

        $('.counter').counterUp({
            delay: 100,
            time: 1000
        });

        @if( isset($cursos) && $cursos->count() )
            //get the top offset of the target anchor
            var target_offset = $("#contenido").offset();
            var target_top = target_offset.top;
            $('html, body').animate({scrollTop:target_top-100}, 1500, 'easeInSine');

        @endif


        $('#viewlist').click( function(){
                    if(!$('.mix').hasClass('one')) {
                        $('.fichacurso').hide().delay(300).fadeIn(200);
                    }
                    $('.mix, .gap').addClass('one').removeClass('two');
                    $('.layout').siblings().removeClass('active');
                    $(this).addClass('active');
                });
        $('#viewcolsbig').click( function(){
            $('.mix, .gap').addClass('two').removeClass('one');
            $('.layout').removeClass('active');
            $(this).addClass('active');
        });
        $('#viewcolssmall').click( function(){
            $('.mix, .gap').removeClass('two').removeClass('one');
            $('.layout').removeClass('active');
            $(this).addClass('active');
        });
        $(document).ready(function () {
            $('.lista #contenido').css({'padding-top': $('#sidebar').height()+'px'});
        });


        $('#sortPromo').click(function(){
            $('#cursos-lista').mixItUp('sort', 'promo:desc, name:asc');
            $('.order').removeClass('active');
            $(this).addClass('active');
        });
        $('#sortName').click(function(){
            $('#cursos-lista').mixItUp('sort', 'name:asc');
            $('.order').removeClass('active');
            $(this).addClass('active');
        });
        $('#sortPais').click(function(){
            $('#cursos-lista').mixItUp('sort', 'pais:asc');
            $('.order').removeClass('active');
            $(this).addClass('active');
        });
        $('.foto').hover(
                function(){
                    $(this).addClass('active');
                },
                function () {
                    $(this).removeClass('active');
                }
        );

        $('.nombrecurso').hover(function(e) {
            $(this).parents('.mix').find('.fotocurso').find('a').trigger(e.type);
        });

        if($(window).width() <= '767'){
            $('#viewlist').trigger('click');
            $('.gap').css({'display': 'none', 'visbility': 'hidden'});
        }

    </script>

@stop