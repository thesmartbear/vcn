@extends('web.dippy.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop


@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{urldecode(trans('web.seo-'.ConfigHelper::config('sufijo').'.subject'))}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @stop

    @section('extra_head')
            <!-- Link Swiper's CSS -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/dippy.css" rel="stylesheet">

    {!! Html::style('assets/plugins/multiselect/css/bootstrap-multiselect.css') !!}
    {!! Html::style('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') !!}

    <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
    <meta name="_token" content="{{ csrf_token() }}"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">




    <style>

    </style>

@stop

@section('container')

    <div class="fullscreen-home">
        <div class="fullhome container-fluid">
            <div id="scroll-down">
                <div class="text">SCROLL</div>
                <span></span>
                <span></span>
                <span></span>
                <p class="arrow">
                    <span></span>
                    <span></span>
                    <span></span>
                </p>
            </div>
            <div class="texthome">
                <div class="row">
                    <div class="col-sm-9 col-xs-12 pull-right">
                        <h1><span><i class="underline">inmersión lingüística</i> total</span>practicando lo que más te gusta</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 pull-right text-right">
                        {!! Form::open(['url' => route('web.buscar.vue.api'), 'method' => 'GET', 'class' => 'form-inline', 'id' => 'buscarform']) !!}
                        <div class="form-group">
                            <select class="form-control" id="select-especialidad-filtro" name="especialidad[]" multiple="multiple">
                                @foreach(\VCN\Models\CMS\EspecialidadWeb::where('activo',1)->get() as $esp)
                                    <option value="{{$esp->id}}">{{$esp->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control" id="select-pais-filtro" name="pais[]" multiple="multiple">
                                @foreach(\VCN\Models\Pais::where('activo_web',1)->get() as $esp)
                                    <option value="{{$esp->id}}">{{$esp->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control" id="select-edad-filtro" id="select-edad-filtro" name="edad[]" multiple="multiple">
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">+18</option>
                            </select>
                        </div>
                        <button type="button" onclick="app.fetchList()" class="btn btn-dippy btn-black">buscar</button>
                        <br /><img class="pull-right arrows" src="/assets/dippy/img/arrows.gif" />
                        {!! Form::close() !!}
                    </div>
                </div>


                 <div class="row" id="message">
                     <div class="col-sm-6 pull-right text-center">
                         <div class="message"></div>
                     </div>
                 </div>
            </div>
        </div>
    </div>

    <div id="doodles"><img src="/assets/dippy/img/doodles.png" /></div>
    <div id="app">

        <div class="container-fluid">
            <div class="app">

                <div v-if="cursos.length" class="cursos-group">
                    <div class="row cursos-group-item" v-for="(curso, index) in cursos" v-bind:data-index="index">
                        <div class="course-container">
                            <div class="col-sm-12">
                                <div class="coursnumber text-right">@{{index+1}}/@{{ total }}</div>
                            </div>
                            <div class="col-sm-12">
                                <h2>
                                    <a v-if="usuario" v-bind:href="curso.fullurl">@{{curso.course_name}}</a>
                                    <a v-else href="#login" v-on:click='app.viewCurso(curso)'>@{{curso.course_name}}</a>
                                </h2>
                            </div>
                            <div class="col-sm-5 leftcolumn">
                                <div class="subespecialidades" v-html="curso.subespecialidades_list">@{{curso.subespecialidades_list}}</div>
                                <img class="img-responsive cursoimg" v-bind:src="curso.cursoimg" />
                            </div>
                            <div class="col-sm-7">
                                <div class="infocurso">
                                    <h4>@{{curso.pais_name}}</h4>
                                    <div class="edades">@{{curso.course_age_range}}</div>
                                    <div v-html="curso.especialidades_list" class="especialidades">
                                        @{{curso.especialidades_list}}
                                    </div>
                                    <div v-html="curso.course_summary" class="cursointro">
                                        @{{curso.course_summary}}
                                    </div>
                                </div>
                                <div class="linkcurso">
                                    <span v-if="usuario"><a class="btn btn-dippy-info" v-bind:href="curso.fullurl">más info →</a></span>
                                    <span v-else><a href="#login" class="btn btn-dippy-info" v-on:click='app.viewCurso(curso)'>más info →</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <section id="sectionfixed" class="home">
            <div id="spacer">
                <i id="loading" class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
                <button v-if="cursos.length" @click="fetchMas(5)" class="btn btn-primary btn-xs" id="mas">Más</button>
            </div>

            <div id="steps">
                <div class="container">
                    <div class="row steps">
                        <div class="col-sm-4">
                            <span class="number">1</span><span class="steptitle">Busca</span>
                            <p>Cuéntanos qué es lo que más te interesa.</p>
                        </div>
                        <div class="col-sm-4">
                            <span class="number">2</span><span class="steptitle">Elige</span>
                            <p>Comprueba nuestra selección especial de aventuras para ti.</p>
                        </div>
                        <div class="col-sm-4">
                            <span class="number">3</span><span class="steptitle">Reserva</span>
                            <p><i class="fa fa-chevron-circle-right"></i> <b>Directamente.</b> Utilízanos solo como buscador de experiencias.</p>
                            <p><i class="fa fa-chevron-circle-right"></i> <b>A través de Dippy</b> para usar todo nuestro potencial.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="clients" class="parallax-window" data-parallax="scroll" data-image-src="/assets/dippy/home/map.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 client">
                            <span class="big"><span class="counter">30</span><small> años</small></span><br>
                            <span class="small">años organizando<br>tus experiencias<br>en el extranjero</span>
                        </div>
                        <div class="col-sm-12 client">
                            <span class="big"><span class="counter">20.000</span></span><br>
                            <span class="small">estudiantes han viajado<br>con nosotros</span>
                        </div>
                        <div class="col-sm-12 client">
                            <span class="big"><span class="counter">92</span>%</span><br>
                            <span class="small">de nuestros clientes<br>quedaron satisfechos<br>o muy satisfechos</span>
                        </div>
                    </div>
                </div>
            </div>

            @include('web.dippy.includes.footer')

        </section>
    </div>


    <div class="loading">Loading&#8230;</div>








    <!-- Modal -->
    <div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img class="modal-logo" src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" />
                </div>
                <div class="modal-body">
                    <div id="respuesta">
                        <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                        <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                            <div class="msg"></div>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                        <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                        <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                        <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                        <input type="hidden" id="curso" value="home">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>

                    <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!-- Modal -->


@stop


@section('extra_footer')
    <script src="assets/plugins/multiselect/js/bootstrap-multiselect.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>


    <script src="https://cdn.jsdelivr.net/vue/2.2.6/vue.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.0/vue-resource.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.0/axios.min.js"></script>


    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.17.0/TweenMax.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>

    <script src="assets/dippy/js/jquery.counterup.min.js"></script>
    <script src="assets/dippy/js/parallax.min.js"></script>


    <script>

        $(document).ready(function () {

            //$('.parallax-window').parallax();


            $('.msg').hide();
            $("#plusinfoenviar").click(function () {
                //console.log('validar');
                if ($('#name').val() == '') {
                    $('.msg').html('Debes indicar un nombre de contacto');
                    $('.msg').show();
                    return false;
                }
                if ($('#email').val() == '') {
                    $('.msg').html('Debes indicar un teléfono o un email de contacto');
                    $('.msg').show();
                    return false;
                }

                if ($('#email').val() != '') {
                    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
                    if (re.test($('#name').val())) {
                        $('.msg').html('El email no tiene un formato correcto');
                        $('.msg').show();
                        return false
                    }

                }

                if (!/^([0-9])*$/.test($('#tel').val())) {
                    $('.msg').html('El campo teléfono tiene que ser numérico');
                    $('.msg').show();
                    return false
                }


                post_data = {
                    'name': $('#name').val(),
                    'tel': $('#tel').val(),
                    'email': $('#email').val(),
                    'curso': $('#curso').val()
                };
                $.ajax({
                    type: "POST",
                    url: "/assets/{{ConfigHelper::config('tema')}}/includes/plusinfosend-{{ConfigHelper::config('sufijo')}}.php",
                    data: post_data,
                    success: function (msg) {
                        //console.log(msg);
                        $("#respuesta").html(msg);
                        $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                        ga('send', 'Solicitud', 'button', 'click', 'home', 1);
                    },
                    error: function () {
                        alert("error!!");
                    }
                });
            });



            $('form input').blur(function () {
                $('.msg').hide();
            });

            var vid = document.getElementById("bgvid");

            if (vid) {
                function vidFade() {
                    vid.classList.add("stopfade");
                }

                vid.addEventListener('ended', function () {
                    vid.play();
                    // to capture IE10
                    vidFade();
                });

                var isMobile = false;
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Opera Mobile|Kindle|Windows Phone|PSP|AvantGo|Atomic Web Browser|Blazer|Chrome Mobile|Dolphin|Dolfin|Doris|GO Browser|Jasmine|MicroB|Mobile Firefox|Mobile Safari|Mobile Silk|Motorola Internet Browser|NetFront|NineSky|Nokia Web Browser|Obigo|Openwave Mobile Browser|Palm Pre web browser|Polaris|PS Vita browser|Puffin|QQbrowser|SEMC Browser|Skyfire|Tear|TeaShark|UC Browser|uZard Web|wOSBrowser|Yandex.Browser mobile/i.test(navigator.userAgent)) {
                    isMobile = true;
                }
                $(document).ready(function () {
                    if (isMobile == true) {
                        $('#bgvid').css({'display': 'none'});
                    } else {
                        $('#bgvid').css({'display': 'block'});
                    }
                });
            }




            $('#select-especialidad-filtro').multiselect({
                selectAllText: 'Todas',
                selectAllValue: '0',
                allSelectedText: "Todas",
                nSelectedText: ' seleccionadas',
                enableClickableOptGroups: false,
                selectAllJustVisible: true,
                disableIfEmpty: true,
                nonSelectedText: 'Especialidad',
                //buttonWidth: '260px',
            });
            $('#select-pais-filtro').multiselect({
                selectAllText: 'Todas',
                selectAllValue: '0',
                allSelectedText: "Todas",
                nSelectedText: ' países',
                enableClickableOptGroups: false,
                selectAllJustVisible: true,
                disableIfEmpty: true,
                nonSelectedText: 'País',
                //buttonWidth: '260px',

            });
            $('#select-edad-filtro').multiselect({
                selectAllText: 'Todas',
                selectAllValue: '0',
                allSelectedText: "Todas",
                nSelectedText: ' seleccionadas',
                enableClickableOptGroups: false,
                selectAllJustVisible: true,
                disableIfEmpty: true,
                nonSelectedText: 'Edad',
                //buttonWidth: '180px',
            });


            function headerbg(){
                var scroll = $(window).scrollTop();

                if (scroll >= 50) {
                    $(".cd-main-header").removeClass("clearHeader");
                } else {
                    $(".cd-main-header").addClass("clearHeader");
                }
            }
            $(window).scroll(function() {
                headerbg();
            });

            headerbg();

            @if( isset($cursos) && $cursos->count() )
                //get the top offset of the target anchor
            var target_offset = $("#contenido").offset();
            var target_top = target_offset.top;
            $('html, body').animate({scrollTop:target_top-100}, 1500, 'easeInSine');

            @endif


            $('#viewlist').click( function(){
                        if(!$('.mix').hasClass('one')) {
                            $('.fichacurso').hide().delay(300).fadeIn(200);
                        }
                        $('.mix, .gap').addClass('one').removeClass('two');
                        $('.layout').siblings().removeClass('active');
                        $(this).addClass('active');
                    });
            $('#viewcolsbig').click( function(){
                $('.mix, .gap').addClass('two').removeClass('one');
                $('.layout').removeClass('active');
                $(this).addClass('active');
            });
            $('#viewcolssmall').click( function(){
                $('.mix, .gap').removeClass('two').removeClass('one');
                $('.layout').removeClass('active');
                $(this).addClass('active');
            });



            $('.lista #contenido').css({'padding-top': $('#sidebar').height()+'px'});



            $('#sortPromo').click(function(){
                $('#cursos-lista').mixItUp('sort', 'promo:desc, name:asc');
                $('.order').removeClass('active');
                $(this).addClass('active');
            });
            $('#sortName').click(function(){
                $('#cursos-lista').mixItUp('sort', 'name:asc');
                $('.order').removeClass('active');
                $(this).addClass('active');
            });
            $('#sortPais').click(function(){
                $('#cursos-lista').mixItUp('sort', 'pais:asc');
                $('.order').removeClass('active');
                $(this).addClass('active');
            });
            $('.foto').hover(
                    function(){
                        $(this).addClass('active');
                    },
                    function () {
                        $(this).removeClass('active');
                    }
            );

            $('.nombrecurso').hover(function(e) {
                $(this).parents('.mix').find('.fotocurso').find('a').trigger(e.type);
            });


            $('.counter').counterUp({
                delay: 10,
                time: 1000
            });


            console.log('ready');
            $('.loading').fadeOut();


        });


    </script>


    <script>

        <?php
        if(ConfigHelper::config('web_registro'))
        {
            if( Session::get('vcn.modal-login') || count($errors) > 0 )
            {
                ?>$('#cursodata').show();<?
            }

            if(auth()->user())
            {
                ?>var usuario = true;<?php
            }
            else
            {
                ?>var usuario = false;<?php
            }
        }
        else
        {
            ?>var usuario = true;<?php
        }
        ?>

        function getDocHeight() {
            var D = document;
            return Math.max(
                    D.body.scrollHeight, D.documentElement.scrollHeight,
                    D.body.offsetHeight, D.documentElement.offsetHeight,
                    D.body.clientHeight, D.documentElement.clientHeight
            );
        }

        var app = new Vue({
            el: '#app',
            data: function () {
                return {
                    cursos: [],
                    mas: 0,
                    // scrolled: false
                    usuario: usuario,
                    url: '',
                };
            },
            mounted: function () {
                //this.fetchList();

                //console.log('mounted');

                $('#loading').hide();
                if (Cookies.get('url')) {
                    this.mas = Cookies.get('mas');
                    this.fetchBack(Cookies.get('url'));
                }

                if(window.location.hash)
                {
                    this.fetchUrl()
                }

                window.addEventListener('scroll', this.handleScroll);

                console.log(this.usuario)
            },
            updated: function (){
                console.log('updated');
                console.log(Cookies.get('url'));
                console.log(Cookies.get('mas'));


                this.scenes = [];

                this.controller = new ScrollMagic.Controller({
                    globalSceneOptions: {
                        triggerHook: 'onLeave'
                    }
                });

                var $scenes = $('.cursos-group-item');

                // get all slides
                var slides = document.querySelectorAll(".cursos-group-item");
                //console.log('slides-> ' + slides.length);
                // create scene for every slide
                for (i=0; i<slides.length; i++) {
                    var scene = new ScrollMagic.Scene({
                        triggerElement: slides[i],
                    })
                            .setPin(slides[i])
                        //.addIndicators() // add indicators (requires plugin)
                            .on('start', function(event){
                                if(i == slides.length) {
                                    scene.removePin(true);
                                    scene.update();
                                }
                            })
                            .addTo(this.controller);


                    this.scenes.push(scene);
                }
            },
            destroyed: function () {
                console.log('destroyed');
                window.removeEventListener('scroll', this.handleScroll);
            },
            methods: {
                onLogin: function(curso_id) {

                    $("#modal-login input[name='curso_id']").val(curso_id);
                    $("#modal-login").modal();
                },
                fetchList: function () {
                    //console.log('fetchList');

                    this.mas = 0;

                    if ($('#select-especialidad-filtro').val() != null) {
                        especialidad = '&especialidad=' + $('#select-especialidad-filtro').val();
                    } else {
                        especialidad = '';
                    }
                    if ($('#select-pais-filtro').val() != null) {
                        pais = '&pais=' + $('#select-pais-filtro').val();
                    } else {
                        pais = '';
                    }
                    if ($('#select-edad-filtro').val() != null) {
                        edad = '&edad=' + $('#select-edad-filtro').val();
                    } else {
                        edad = '';
                    }

                    params = "max=0" + especialidad + pais + edad;
                    this.url = '{{route('web.buscar.vue.api')}}' + "?" + params
                    window.location.hash = params

                    this.$http.get(this.url).then(function (response) {
                        console.log('Total: ' + response.data.total);
                        this.cursos = response.data.cursos;
                        this.total = response.data.total;
                        Cookies.set('url', this.url);
                        Cookies.set('mas', this.mas);

                        this.destroyScenes();


                        if(this.total == 0){
                            $('.message').html('Oops! No se han encontrado resultados.');
                        }else{
                            $('.message').html('');
                            $('html, body').animate({scrollTop:$('#app').offset().top}, 1500, 'easeInSine');
                        }



                    });


                },

                fetchMas: function (m) {
                    //console.log('fetchMas');

                    $('#loading').show();
                    this.mas += m;

                    if ($('#select-especialidad-filtro').val() != null) {
                        especialidad = '&especialidad=' + $('#select-especialidad-filtro').val();
                    } else {
                        especialidad = '';
                    }
                    if ($('#select-pais-filtro').val() != null) {
                        pais = '&pais=' + $('#select-pais-filtro').val();
                    } else {
                        pais = '';
                    }
                    if ($('#select-edad-filtro').val() != null) {
                        edad = '&edad=' + $('#select-edad-filtro').val();
                    } else {
                        edad = '';
                    }

                    this.url = '{{route('web.buscar.vue.api')}}' + "?max=" + this.mas + especialidad + pais + edad;

                    this.$http.get(this.url).then(function (response) {
                        $('#loading').fadeOut();
                        this.cursos.push.apply(this.cursos, response.data.cursos);
                        Cookies.set('url', this.url);
                        Cookies.set('mas', this.mas);
                        // this.list = this.list.concat(response.data)

                        this.destroyScenes();
                    })
                },

                handleScroll: function () {

                    if ($(window).scrollTop() >= $('.cursos-group').height()) {
                        if (this.mas <= this.total && this.total != undefined) {
                            app.fetchMas(10);
                            $('#loading').fadeIn();
                            $('#mas').show();
                        } else {
                            $('#loading').hide();
                            $('#mas').hide();
                        }
                    }
                },

                viewCurso: function (curso) {
                    console.log('viewCurso');
                    Cookies.set('url', this.url);
                    Cookies.set('mas', this.mas);
                    $("#modal-login input[name='curso_id']").val(curso.id);
                    if(curso.cursoimg != '') {
                        $('#cursodata').removeClass('nofoto');
                        $('#cursodata').css({'background-image': 'url(' + curso.cursoimg + ')'});
                    }else{
                        $('#cursodata').addClass('nofoto');
                        $('#cursodata').css({'background-image': 'none'});
                    }
                    $('#cursodata .infocurso').html('<h2 class="cursonamemodal">' + curso.course_name + '<small>'+curso.pais_name+'</small></h2>');
                    $('#cursodata').fadeIn(1000);
                    console.log(Cookies.get('url'));
                },


                fetchBack: function (url) {
                    this.url = Cookies.get('url');
                    this.mas = Cookies.get('mas');
                    console.log(Cookies.get('url'));
                    this.$http.get(url + "&back=1").then(function (response) {
                        console.log('Total (fetchBack): ' + response.data.total);
                        this.cursos = response.data.cursos;
                        this.total = response.data.total;
                        console.log(response.data.cursos);
                        /*
                         if (this.mas <= this.total) {
                         $('#loading').fadeIn();
                         $('#mas').show();
                         } else {
                         $('#loading').hide();
                         $('#mas').hide();
                         }
                         */

                        if (getQueryVariable('especialidad', this.url) != false) {
                            $('#select-especialidad-filtro').multiselect('select', getQueryVariable('especialidad', this.url).split(','));
                        }
                        if (getQueryVariable('pais', this.url) != false) {
                            $('#select-pais-filtro').multiselect('select', getQueryVariable('pais', this.url).split(','));
                        }
                        if (getQueryVariable('edad', this.url) != false) {
                            $('#select-edad-filtro').multiselect('select', getQueryVariable('edad', this.url).split(','));
                        }

                        this.destroyScenes();

                        //$('html, body').animate({scrollTop:$('#app').offset().top}, 1500, 'easeInSine');

                    });
                },

                fetchUrl: function () {

                    params = window.location.hash.substr(1)
                    this.url = '{{route('web.buscar.vue.api')}}' + "?" + params

                    this.$http.get(this.url).then(function (response) {
                        console.log('Total (fetchUrl): ' + response.data.total);
                        this.cursos = response.data.cursos;
                        this.total = response.data.total;

                        if (getQueryVariable('especialidad', this.url) != false) {
                            $('#select-especialidad-filtro').multiselect('select', getQueryVariable('especialidad', this.url).split(','));
                        }
                        if (getQueryVariable('pais', this.url) != false) {
                            $('#select-pais-filtro').multiselect('select', getQueryVariable('pais', this.url).split(','));
                        }
                        if (getQueryVariable('edad', this.url) != false) {
                            $('#select-edad-filtro').multiselect('select', getQueryVariable('edad', this.url).split(','));
                        }

                        this.destroyScenes();


                        if(this.total == 0){
                            $('.message').html('Oops! No se han encontrado resultados.');
                        }else{
                            $('.message').html('');
                            $('html, body').animate({scrollTop:$('#app').offset().top}, 1500, 'easeInSine');
                        }

                    });
                },


                destroyScenes: function(){
                    if(typeof this.scenes !== 'undefined' && typeof this.scenes !== null) {
                        //console.log('destroy');
                        for (i = 0; i < this.scenes.length; i++) {
                            this.scenes[i].destroy(true);
                            this.scenes[i] = null;
                        }
                        this.controller.destroy(true);
                        this.controller = null;
                    }
                }

            }
        });

        function getQueryVariable(variable, url) {
            var query = url.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
            return (false);
        }


    </script>

@stop