<div id="modal-login">
                <div id="respuesta" class="row">

                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <button class="close" data-close="alert"></button>
                                <span>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </span>
                        </div>
                        <br>
                    @endif

                    <div class="col-sm-3 col-sm-offset-2 col-xs-6">
                    @if(Session::get('vcn.modal-login'))
                        @if(Session::get('vcn.modal-login-existe'))
                            {!!trans( "auth.compra-p_login_". Session::get('vcn.modal-login-tipo') )!!}
                        @else
                            {!! trans('auth.compra-p_activacion_pwd') !!}
                        @endif
                    @else

                            <h4>Ya tengo un usuario:</h4>
                    @endif

                    {{-- LOGIN --}}
                    {!! Form::open(['method'=>'POST', 'url' => 'auth/login', 'class' => 'form login-form']) !!}

                        {!! csrf_field() !!}

                        {!! Form::hidden('curso_id', old('curso_id')) !!}

                        <div class="row">
                            <div class="col-xs-12">
                                <label><i class="fa fa-envelope"></i> {!! trans('auth.email') !!}</label>
                                {!! Form::text('username', old('username')?:Session::get('vcn.modal-login'), ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'autocomplete'=> 'off', 'required'=> true, 'placeholder'=> trans('auth.email')]) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <label><i class="fa fa-lock"></i> {!! trans('auth.password') !!}</label>
                                {!! Form::password('password', ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'autocomplete'=> 'off', 'required'=> true, 'placeholder'=> trans('auth.password')]) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <a href="{{ route('password.request') }}" id="forget-password" class="forget-password">{!! trans('auth.recordarpass') !!}</a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3 pull-right">
                                {!! Form::submit(trans('auth.entrar'),['class' => 'btn btn-dippy-info btn-xs pull-right']) !!}
                            </div>
                        </div>

                    {!! Form::close() !!}
                    </div>



                    @if(!Session::get('vcn.modal-login'))
                    <div class="col-sm-4 col-sm-offset-1 col-xs-6">
                        <h4>Registro:</h4>
                        {{-- REGISTRO --}}
                        {!! Form::open(['method'=>'POST','route' => 'web.register', 'class' => 'form login-form']) !!}

                            {!! csrf_field() !!}

                            {!! Form::hidden('curso_id', old('curso_id')) !!}

                            {!! trans('auth.compra-p_registro') !!}

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-radio">
                                        <label>
                                            {!! Form::radio('viajero', 1, false, array('required'=> true)) !!} {!! trans('auth.compra-p_viajero') !!}
                                        </label>
                                        <label>
                                            {!! Form::radio('viajero', 2, false) !!} {!! trans('auth.compra-p_tutor') !!}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <label>{!! trans('auth.nombre') !!}</label>
                                    {!! Form::text('nombre', old('nombre'), ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'required'=>true, 'autocomplete'=> 'off', 'placeholder'=> trans('auth.nombre')]) !!}
                                </div>
                                <div class="col-xs-6">
                                    <label>{!! trans('auth.apellidos') !!}</label>
                                    {!! Form::text('apellido', old('apellido'), ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'required'=>true, 'autocomplete'=> 'off', 'placeholder'=> trans('auth.apellidos')]) !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <label>{!! trans('auth.email') !!}</label>
                                    {!! Form::text('email', old('email'), ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'required'=>true, 'autocomplete'=> 'off', 'placeholder'=> trans('auth.email')]) !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <label>{!! trans('auth.movil') !!}</label>
                                    {!! Form::text('movil', old('movil'), ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'autocomplete'=> 'off', 'placeholder'=> trans('auth.movil')]) !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3 pull-right">
                                    {!! Form::submit(trans('auth.registrar'),['class' => 'btn btn-dippy-info btn-xs pull-right']) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                    @endif

                </div>
</div>