<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-xs-6">
                    <ul class="menu">
                        @foreach(VCN\Models\CMS\Pagina::WhereIn('propietario', [0,ConfigHelper::config('propietario')])->where('menu',1)->get() as $pagina)
                            <li><a href="/{!! Traductor::getWeb(App::getLocale(), 'Pagina', 'url', $pagina->id, $pagina->url) !!}.html">{!! Traductor::getWeb(App::getLocale(), 'Pagina', 'titulo', $pagina->id, $pagina->titulo) !!}</a></li>
                        @endforeach
                        <li><a href="/blog">{{trans('web.blog')}}</a></li>
                        <li><a href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal">{{trans('web.contacto')}}</a></li>
                        @if (Auth::guest())
                            <li class="item"><a id="btn-login">login/registro</a></li>
                        @else
                            <li><a href="https://{{$_SERVER['SERVER_NAME']}}/area">{{trans('web.aclientes')}}</a></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    <i class="ifa fa fa-sign-out fa-fw"></i> {{ trans('web.salir') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                            </li>
                        @endif
                    </ul>
            </div>
            <div class="col-sm-3 col-xs-6">
                <ul class="legal">
                    <li><a href="/condiciones-generales.html">Condiciones generales</a></li>
                    <li><a href="/politica-de-cookies.html">Política de cookies</a></li>
                </ul>

            </div>
            <div class="col-sm-5 col-xs-12 logos">
                <div class="social">
                    <a href="https://www.instagram.com/dippy.life/" target="_blank"><i class="fa fa-facebook-square"></i></a>
                    <a href="https://www.facebook.com/DippyLife-221273871720459/" target="_blank"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>