@extends('web.dippy.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')
        <!-- Color style -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/dippy.css" rel="stylesheet">
<link href="/assets/{{ConfigHelper::config('tema')}}/css/dropdowns-enhancement.css" rel="stylesheet">
@stop


@section('container')
    <div class="headerbg" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/{{rand(1, 3)}}.png') repeat;">
        <div class="container" id="header" style="position: relative;">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="titulo">
                        <h1 class="slogan">
                            <span></span>
                            {{trans('web.cursos-en-promocion-titulo')}}
                            <small></small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="headerbgoverlay"></div>

        <div id="sidebar" class="lista">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="pull-left">
                            <div class="widget clearfix filtros">
                                @if($cursos != '')
                                <form id="Filters">

                                    <fieldset class="filter-group checkboxes">
                                        @foreach($cursos as $c)
                                            @foreach(\VCN\Models\Centros\Centro::distinct()->where('id', $c->centro->id)->get() as $p)
                                                <?php $paises[] = \VCN\Models\Pais::distinct('name')->where('id', $p->pais->id)->first()->name; ?>
                                            @endforeach
                                        @endforeach
                                        <div class="btn-group">
                                            <button class="btn btn-primary">País</button>
                                            <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                            <ul class="dropdown-menu bullet pull-center">
                                                <li><input type="checkbox" id="pais-all" value=".pais-all" class="all" checked="checked"><label for="pais-all">Todos</label></li>
                                                @foreach(array_unique($paises) as $pais)
                                                    <li><input type="checkbox" id="{{str_slug($pais)}}" value=".{{str_slug($pais)}}"><label for="{{str_slug($pais)}}">{{$pais}}</label></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </fieldset>

                                    <fieldset class="filter-group checkboxes">
                                        @foreach($cursos as $c)
                                            @foreach($c->alojamientos as $alojamiento)
                                                <? $alojas[] = $alojamiento->tipo->accommodation_type_name; ?>
                                            @endforeach
                                        @endforeach
                                        <div class="btn-group">
                                            <button class="btn btn-primary">Alojamiento</button>
                                            <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                            <ul class="dropdown-menu bullet pull-center">
                                                <li><input type="checkbox" id="alojamiento-all" value=".alojamiento-all" class="all" checked="checked"><label for="alojamiento-all">Todos</label></li>
                                                @foreach(array_unique($alojas) as $aloja)
                                                    <li><input type="checkbox" id="{{str_slug($aloja)}}" value=".{{str_slug($aloja)}}"><label for="{{str_slug($aloja)}}">{{$aloja}}</label></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </fieldset>

                                    <? $especialidades = array(); ?>
                                    @foreach($cursos as $c)
                                        @foreach(\VCN\Models\Cursos\CursoEspecialidad::distinct()->where('curso_id',$c->id)->get() as $e)
                                            <?php $especialidades[] = \VCN\Models\Especialidad::distinct('name')->where('id', $e->especialidad_id)->first()->name; ?>
                                        @endforeach
                                    @endforeach
                                    @if(count($c->especialidades))
                                        <fieldset class="filter-group checkboxes">
                                            <div class="btn-group">
                                                <button class="btn btn-primary">Especialidad</button>
                                                <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                                <ul class="dropdown-menu bullet pull-center">
                                                    <li><input type="checkbox" id="especialidades-all" value=".especialidades-all" class="all" checked="checked"><label for="especialidades-all">Todas</label></li>
                                                    @foreach(array_unique($especialidades) as $especialidad)
                                                        <li><input type="checkbox" id="{{str_slug($especialidad)}}" value=".{{str_slug($especialidad)}}"><label for="{{str_slug($especialidad)}}">{{$especialidad}}</label></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </fieldset>
                                    @endif

                                </form>
                                @endif
                            </div>
                        </div>
                        <div class="controls pull-right text-right">
                            <div class="total-cursos"><i class="fa fa-eye"></i> <span></span></div>
                            <button id="viewcolssmall" class="layout active"><i class="fa fa-th"></i></button>
                            <button id="viewcolsbig" class="layout"><i class="fa fa-th-large"></i></button>
                            <button id="viewlist" class="layout"><i class="fa fa-list"></i></button>
                            <div class="separator"></div>
                            <button id="sortPromo" class="order active"><i class="fa fa-asterisk"></i></button>
                            <button id="sortName" class="order"><i class="fa fa-sort-alpha-asc"></i></button>
                            <button id="sortPais" class="order"><i class="fa fa-map-marker"></i></button>
                            <!--
                            <div class="col-sm-2 pull-right">
                                <button id="Reset" class="btn-block">ver todos</button>
                            </div>
                            -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <main class="cd-main-content lista">

        <div class="container" id="contenido">

            <div class="row">
                <div class="col-sm-12 col-xs-12 {{$clase}} categorias">
                    <div id="programas">
                        @if($cursos != '')
                        <div id="cursos-lista">
                            <div class="fail-message"><span>{!! trans('web.sinresultados') !!}</span></div>

                            <div class="slides">
                                @foreach($cursos as $curso)
                                <?php
                                $fotoscentro = '';
                                $fotoscentroname = array();
                                $path = public_path() ."/assets/uploads/center/" . $curso->centro->center_images;
                                $folder = "/assets/uploads/center/" . $curso->centro->center_images;

                                if (is_dir($path)) {
                                    $results = scandir($path);
                                    foreach ($results as $result) {
                                        if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                                        $file = $path . '/' . $result;

                                        if (is_file($file)) {
                                            $fotoscentroname[] = $result;

                                        }
                                    }
                                }
                                ?>
                                <?php
                                $fotoscurso = '';
                                $fotoscursoname = array();
                                $path = public_path() ."/assets/uploads/course/" . $curso->course_images;
                                $folder = "/assets/uploads/course/" . $curso->course_images;

                                if (is_dir($path)) {
                                    $results = scandir($path);
                                    foreach ($results as $result) {
                                        if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                                        $file = $path . '/' . $result;

                                        if (is_file($file)) {
                                            $fotoscursoname[] = $result;
                                        }
                                    }
                                }
                                ?>

                                <? $espes = array(); ?>
                                @foreach($curso->especialidades as $e)
                                    <? $espes[] = str_slug($e->especialidad->name); ?>
                                @endforeach

                                <? $alojas = array(); ?>
                                @foreach($curso->alojamientos as $alojamiento)
                                    <? $alojascurso[] = str_slug($alojamiento->tipo->accommodation_type_name); ?>
                                @endforeach


                                    @if($curso->categoria->id == 1 || $curso->categoria->id == 2 || $curso->categoria->id == 7)
                                        @if(strpos($curso->course_language, 'Inglés') === false)
                                            <?php $cursolink = "/".trans('web.categorias.idiomas-slug')."/".Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug).".html"; ?>
                                        @else
                                            <?php $cursolink = "/".trans('web.categorias.ingles-slug')."/".Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug).".html"; ?>
                                        @endif
                                    @else
                                        <?php $cursolink = "/".Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $curso->categoria->id, $curso->categoria->slug)."/".Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug).".html"; ?>
                                    @endif




                                @if(is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada))
                                    <div class="mix {{str_slug($curso->centro->pais->name)}} {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug($curso->centro->pais->name)}}">
                                        @if($curso->course_promo == 1)
                                            <div class="promo-lista">
                                                <i class="fa fa-asterisk"></i>
                                            </div>
                                        @endif
                                        <div class="fotocurso" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/thumb/{{$curso->image_portada}}');">
                                            <a href="{{$curso->catweb->seo_url}}/{{Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug)}}.html" class="foto">
                                                @elseif(is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                                                    <div class="mix {{str_slug($curso->centro->pais->name)}}  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug($curso->centro->pais->name)}}">
                                                        @if($curso->course_promo == 1)
                                                            <div class="promo-lista">
                                                                <i class="fa fa-asterisk"></i>
                                                            </div>
                                                        @endif
                                                        <div class="fotocurso" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/thumb/{{$curso->centro->center_image_portada}}');">
                                                            <a href="{{$curso->catweb->seo_url}}/{{Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug)}}.html" class="foto">
                                                                @elseif(!is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                                                                    @if(count($fotoscursoname))
                                                                        <div class="mix {{str_slug($curso->centro->pais->name)}}  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug($curso->centro->pais->name)}}">
                                                                            @if($curso->course_promo == 1)
                                                                                <div class="promo-lista">
                                                                                    <i class="fa fa-asterisk"></i>
                                                                                </div>
                                                                            @endif
                                                                            <div class="fotocurso" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/thumb/{{$fotoscursoname[rand(0,count($fotoscursoname)-1)]}}'); background-size: cover; background-position: center center;">
                                                                                <a href="{{$curso->catweb->seo_url}}/{{Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug)}}.html" class="foto">
                                                                                    @elseif(!count($fotoscursoname) && count($fotoscentroname))
                                                                                        <div class="mix {{str_slug($curso->centro->pais->name)}}  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug($curso->centro->pais->name)}}">
                                                                                            @if($curso->course_promo == 1)
                                                                                                <div class="promo-lista">
                                                                                                    <i class="fa fa-asterisk"></i>
                                                                                                </div>
                                                                                            @endif
                                                                                            <div class="fotocurso" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/thumb/{{$fotoscentroname[rand(0,count($fotoscentroname)-1)]}}'); background-size: cover;">
                                                                                                <a href="{{$curso->catweb->seo_url}}/{{Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug)}}.html" class="foto">
                                                                                                    @else
                                                                                                        <div class="mix {{str_slug($curso->centro->pais->name)}}  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug($curso->centro->pais->name)}}">
                                                                                                            @if($curso->course_promo == 1)
                                                                                                                <div class="promo-lista">
                                                                                                                    <i class="fa fa-asterisk"></i>
                                                                                                                </div>
                                                                                                            @endif
                                                                                                            <div class="fotocurso" style="background: #E5E5E5;">
                                                                                                                <a href="{{$curso->catweb->seo_url}}/{{Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug)}}.html" class="foto">

                                                                                                                    @endif
                                                                                                                    @endif
                                                                                                                </a>
                                                                                                            </div>

                                                                                                            <div class="fichacurso">
                                                                                                                <div class="row">
                                                                                                                    <small class="col-sm-6 nombrepais">@if($curso->centro->pais->name != 'España') {{Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name)}} @else {{Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name)}} @endif</small>

                                                                                                                    @if(count($curso->convocatoriasCerradas) && $curso->convocatoriasCerradas->contains('convocatory_semiopen', 0) && $curso->category_id == 2)
                                                                                                                        <?php $plazasrestantes = 0; ?>
                                                                                                                        @foreach($curso->convocatoriasCerradas->sortBy('convocatory_close_start_date')->sortBy('convocatory_close_duration_weeks') as $cc)
                                                                                                                            @if($cc->activo_web == 1)
                                                                                                                                {{--@if($cc->alojamiento_id == $aloja->id)--}}
                                                                                                                                @if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 0)
                                                                                                                                    <?php $plazasrestantes += $cc->plazas_disponibles; ?>
                                                                                                                                @endif
                                                                                                                            @endif
                                                                                                                        @endforeach
                                                                                                                            @if($plazasrestantes <= 0 && $curso->cerrado != 1)
                                                                                                                                <small class="col-sm-6 pull-right text-right grupocerrado plazas">{{trans('web.grupocerrado')}}</small>
                                                                                                                            @elseif($plazasrestantes != 0 && $plazasrestantes < 6 && $curso->cerrado != 1)
                                                                                                                                <small class="col-sm-6 pull-right text-right ultimasplazas plazas">{{trans('web.ultimasplazas')}}</small>
                                                                                                                            @endif
                                                                                                                    @endif
                                                                                                                </div>
                                                                                                                    <a href="{{$curso->catweb->seo_url}}/{{Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug)}}.html">
                                                                                                                        {!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_name', $curso->id, $curso->course_name) !!}
                                                                                                                    </a>
                                                                                                                </h4>
                                                                                                                <p class="edades">{!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_age_range', $curso->id, $curso->course_age_range) !!}</p>

                                                                                                                <div class="separator-ficha"></div>

                                                                                                                <? $alojas = array(); ?>
                                                                                                                @foreach($curso->alojamientos as $alojamiento)
                                                                                                                    <? $alojas[] = $alojamiento->tipo->accommodation_type_name; ?>
                                                                                                                @endforeach
                                                                                                                <p class="cursoalojas">{{implode(array_unique($alojas),', ')}}</p>

                                                                                                                @if(count($curso->especialidades))
                                                                                                                    <p class="especialidades pull-left">
                                                                                                                        @foreach($curso->especialidades as $e)
                                                                                                                            {{--$e->especialidad->name--}}
                                                                                                                            <span>{{$e->SubespecialidadesName}}</span>
                                                                                                                        @endforeach
                                                                                                                    </p>
                                                                                                                @endif
                                                                                                            </div>

                                                                                                        </div>
                                                                                                        @endforeach
                                                                                                        <div class="gap"></div>
                                                                                                        <div class="gap"></div><div class="gap"></div>
                                                                                            </div>
                                                                                        </div>
                                                                            </div>
                                                                        </div>
                @else
                    {!! trans('web.sinresultados') !!}
                @endif

                <div class="row">
                    @include('web.dippy.includes.copyright')
                </div>

            </div>
        </div>
    </main>

    <!-- Modal -->
    <div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img class="modal-logo" src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" />
                </div>
                <div class="modal-body">
                    <div id="respuesta">
                        <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                        <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                            <div class="msg"></div>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                        <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                        <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                        <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                        <input type="hidden" id="curso" value="{{trans('web.cursos-en-promocion-titulo')}}">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>

                    <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@stop


@section('extra_footer')

    <script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js" type="text/javascript"></script>
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/dropdowns-enhancement.js"></script>
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/filters.js" type="text/javascript"></script>


    <script>
        $('#viewlist').click( function(){
            if(!$('.mix').hasClass('one')) {
                $('.fichacurso').hide().delay(300).fadeIn(200);
            }
            $('.mix, .gap').addClass('one').removeClass('two');
            $('.layout').siblings().removeClass('active');
            $(this).addClass('active');
        });
        $('#viewcolsbig').click( function(){
            $('.mix, .gap').addClass('two').removeClass('one');
            $('.layout').removeClass('active');
            $(this).addClass('active');
        });
        $('#viewcolssmall').click( function(){
            $('.mix, .gap').removeClass('two').removeClass('one');
            $('.layout').removeClass('active');
            $(this).addClass('active');
        });
        $(document).ready(function () {
            $('.lista #contenido').css({'padding-top': $('#sidebar').height()+'px'});
        });


        $('#sortPromo').click(function(){
            $('#cursos-lista').mixItUp('sort', 'promo:desc, name:asc');
            $('.order').removeClass('active');
            $(this).addClass('active');
        });
        $('#sortName').click(function(){
            $('#cursos-lista').mixItUp('sort', 'name:asc');
            $('.order').removeClass('active');
            $(this).addClass('active');
        });
        $('#sortPais').click(function(){
            $('#cursos-lista').mixItUp('sort', 'pais:asc');
            $('.order').removeClass('active');
            $(this).addClass('active');
        });
        $('.foto').hover(
                function(){
                    $(this).addClass('active');
                },
                function () {
                    $(this).removeClass('active');
                }
        );

        $('.nombrecurso').hover(function(e) {
            $(this).parents('.mix').find('.fotocurso').find('a').trigger(e.type);
        });

        if($(window).width() <= '767'){
            $('#viewlist').trigger('click');
            $('.gap').css({'display': 'none', 'visbility': 'hidden'});
        }
    </script>

@stop


