<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @section('title')
            {{ConfigHelper::config('nombre')}}
        @show
    </title>

    @yield('extra_meta')


    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100,400,700,700italic,400italic' rel='stylesheet' type='text/css'>

    <!-- ==============================================
		Favicons
		=============================================== -->
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/{{ConfigHelper::config('tema')}}/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/{{ConfigHelper::config('tema')}}/favicon/favicon-16x16.png">
    <link rel="manifest" href="/assets/{{ConfigHelper::config('tema')}}/favicon/manifest.json">
    <link rel="mask-icon" href="/assets/{{ConfigHelper::config('tema')}}/favicon/safari-pinned-tab.svg" color="#dbf500">
    <meta name="theme-color" content="#ffffff">


    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" />


    <!-- Font -->
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ek+Mukta:200,300,400,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

    <link href="/assets/{{ConfigHelper::config('tema')}}/css/bs2015.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" media="all" href="/assets/{{ConfigHelper::config('tema')}}/css/jquery-jvectormap.css"/>


    <!-- font awesome -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />


    <link href="/assets/{{ConfigHelper::config('tema')}}/css/bootstrap-select.css" rel="stylesheet" type="text/css" />

    <link href="/assets/{{ConfigHelper::config('tema')}}/css/bootstrap-select.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />

    <link rel="stylesheet" href="/assets/{{ConfigHelper::config('tema')}}/css/navstyle.css"> <!-- Resource style -->

    <script src="/assets/{{ConfigHelper::config('tema')}}/js/modernizr.js"></script> <!-- Modernizr -->

    <link rel="stylesheet" href="/assets/{{ConfigHelper::config('tema')}}/css/webstyle.css">

    <link rel="stylesheet" href="/assets/outdatedbrowser/outdatedbrowser.min.css">

    @yield('extra_head')

</head>
<body class="nav-is-fixed">

    <header class="cd-main-header @if(isset($home) || isset($curso)) clearHeader @endif @if(isset($home)) home @endif">

        @if(Session::get('vcn.tema') == Session::get('vcn.tema2'))
        <a id="cd-logo" href="/"><img src="/assets/logos/dippylogo.png" width="180" height="144" alt="{{ConfigHelper::config('nombre')}}" /></a>
        <div class="social">
            <a href="https://www.instagram.com/dippy.life/" target="_blank"><i class="fa fa-facebook-square"></i></a>
            <a href="https://www.facebook.com/DippyLife-221273871720459/" target="_blank"><i class="fa fa-instagram"></i></a>
        </div>
        @else
        <a id="cd-logo-x" href="/"><img src="/assets/logos/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" /></a>
        @endif

        <nav id="cd-top-nav">
            <ul class="cd-header-buttons">
                @foreach(VCN\Models\CMS\Pagina::WhereIn('propietario', [0,ConfigHelper::config('propietario')])->where('menu',1)->get() as $pagina)
                    <li class="item"><a href="/{!! Traductor::getWeb(App::getLocale(), 'Pagina', 'url', $pagina->id, $pagina->url) !!}.html">{!! Traductor::getWeb(App::getLocale(), 'Pagina', 'titulo', $pagina->id, $pagina->titulo) !!}</a></li>
                @endforeach
                <li class="item"><a href="/blog" target="_blank">{{trans('web.blog')}}</a></li>
                <li class="item"><a href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal">{{trans('web.contacto')}}</a></li>
                @if (Auth::guest())
                    <li class="item"><a id="btn-login" class="loginbutton">login/registro</a></li>
                @else
                    <li class="item"><a href="https://{{$_SERVER['SERVER_NAME']}}/area">{{trans('web.aclientes')}}</a></li>
                    <li class="item nomargin">
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="ifa fa fa-sign-out fa-fw"></i> {{ trans('web.salir') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                    </li>
                @endif

            </ul> <!-- cd-header-buttons -->

        </nav>
        <div id="cd-menu-trigger"><i class="fa fa-navicon"></i> </div>


    </header>

@yield('container')

    <div class="cd-overlay"></div>

{{--responsive menu--}}
    <nav id="cd-lateral-nav">
        <ul class="cd-navigation">
            @foreach(VCN\Models\CMS\Pagina::WhereIn('propietario', [0,ConfigHelper::config('propietario')])->where('menu',1)->get() as $pagina)
                <li class="item"><a href="/{!! Traductor::getWeb(App::getLocale(), 'Pagina', 'url', $pagina->id, $pagina->url) !!}.html">{!! Traductor::getWeb(App::getLocale(), 'Pagina', 'titulo', $pagina->id, $pagina->titulo) !!}</a></li>
            @endforeach
            <li class="item"><a href="/blog">{{trans('web.blog')}}</a></li>
            <li class="item"><a href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal">{{trans('web.contacto')}}</a></li>
            @if (Auth::guest())
                <li class="item"><a id="btn-login" class="loginbutton">login/registro</a></li>
            @else
                <li class="item"><a href="https://{{$_SERVER['SERVER_NAME']}}/area">{{trans('web.aclientes')}}</a></li>
                <li class="item"><a href="https://{{$_SERVER['SERVER_NAME']}}/auth/logout"><i class="fa fa-sign-out "></i> {{trans('web.salir')}}</a></li>
            @endif
        </ul> <!-- cd-header-buttons -->
    </nav>

    <div id="outdated"></div>


    <div id="cursodata" class="container-fluid nofoto">
        <div class="infocurso"></div>
        @include('web.dippy.includes.login')
        <div class="row">
            <div class="col-sm-12 text-center addmargintop60">
                <button onclick="javascript:$('#cursodata').fadeOut()" class="btn btn-dippy-info">volver</button>
            </div>
        </div>
    </div>


    <script src="/assets/outdatedbrowser/outdatedbrowser.min.js"></script>

    <!--<script src='https://code.jquery.com/jquery-2.1.1.min.js' type='text/javascript'></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.0/jquery-migrate.min.js' type='text/javascript'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript" ></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


    <script src="/assets/{{ConfigHelper::config('tema')}}/js/jquery.cookie.js"></script>
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/jquery.cookiecuttr.js"></script>


    <!-- detect mobile -->
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/detectmobilebrowser.js" type="text/javascript"></script>

    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/bootstrap-select.js"></script>

    <script src="/assets/{{ConfigHelper::config('tema')}}/js/main.js"></script> <!-- navigation -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>



    <script src="/assets/dippy/js/js.cookie.js"></script>






    @yield('extra_footer')


        <script>
            $(document).ready(function () {

                $('.loginbutton').on('click',function(){
                    $('#cursodata').fadeIn()
                });
                outdatedBrowser({
                    bgColor: '#f25648',
                    color: '#ffffff',
                    lowerThan: 'transform',
                    languagePath: 'assets/outdatedbrowser/lang/{{App::getLocale()}}.html'
                })

                // activate cookie cutter
                if($('html').attr('lang') == 'es'){
                    cookieText = "Utilizamos cookies propias y de terceros para mejorar nuestros servicios y realizar anal&iacute;ticas web. Si continua navegando, consideramos que acepta su uso.<br />Puede cambiar la configuraci&oacute;n u obtener m&aacute;s informaci&oacute;n en ";
                    cookieTextLink = "Pol&iacute;tica de Cookies";
                    cookieLink = './politica-de-cookies.html';
                    buttonText = 'Acepto';
                }else if($('html').attr('lang') == 'ca'){
                    cookieText = "Utilitzem cookies pr&ograve;pies i de tercers per millorar nostres serveis i realitzar anal&iacute;tiques web. Si continua navegant, considerem que accepta el seu &uacute;s.<br />Pot canviar la configuraci&oacute; o obtenir m&eacute;s informaci&oacute; a ";
                    cookieTextLink = "<br />Pol&iacute;tica de Cookies";
                    cookieLink = './ca/politica-de-cookies.html';
                    buttonText = 'Acepto';
                }else if($('html').attr('lang') == 'en'){
                    cookieText = "We use cookies on this website, you can ";
                    cookieTextLink = "<br />read about them here";
                    cookieLink = './politica-de-cookies.html';
                    buttonText = 'Accept cookies';
                }

                $.cookieCuttr({
                    cookieDeclineButton: false,
                    cookieAnalyticsMessage: cookieText,
                    cookieWhatAreLinkText: cookieTextLink,
                    cookieWhatAreTheyLink: cookieLink,
                    cookieExpires: 365,
                    cookieAcceptButtonText: buttonText
                });
            });



        @if (strpos(Request::server ("SERVER_NAME"), 'dippy.life') !== false)
            if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept") {
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-101015188-1', 'auto');
                ga('send', 'pageview');
            }
        @endif

        $('#cd-logo').on('click', function(e){
                    e.preventDefault();
                    Cookies.remove('url');
                    Cookies.remove('mas');
                    console.log(Cookies.get());
                    window.location.href = "/";
        });




        </script>


</body>
</html>