@extends('web.dippy.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} - {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) !!}. {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo) !!} @if($subcategoriadet != ''). {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoriadet->id, $subcategoriadet->titulo) !!}@endif
@stop


@section('extra_meta')
    <meta name="DC.title" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) !!}. {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo) !!} @if($subcategoriadet != ''). {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoriadet->id, $subcategoriadet->titulo) !!}@endif" />
    <meta name="Subject" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) !!}. {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo) !!} @if($subcategoriadet != ''). {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoriadet->id, $subcategoriadet->titulo) !!}@endif" />
    <meta name="Description" lang="{{App::getLocale()}}" content="@if($subcategoriadet != ''){!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_descripcion', $subcategoriadet->id, $subcategoriadet->seo_descripcion) !!}@else{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_descripcion', $subcategoria->id, $subcategoria->seo_descripcion) !!} @endif" />
    <meta name="Keywords" lang="{{App::getLocale()}}" content="@if($subcategoriadet != ''){!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_keywords', $subcategoriadet->id, $subcategoriadet->seo_keywords) !!}@else{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_keywords', $subcategoria->id, $subcategoria->seo_keywords) !!}@endif" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop


@section('extra_head')
<!-- Color style -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/dippy.css" rel="stylesheet">

@stop


@section('container')
    <div class="headerbg" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/{{rand(1, 3)}}.png') repeat;">
        <div class="container" id="header" style="position: relative;">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="titulo">
                        <h1 class="slogan">
                            <span></span>
                            <span>
                                <ul class="breadcrumb">
                                    <li @if($subcategoriadet != '')class="active"@endif><a href="/{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url) !!}">{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) !!}</a></li>
                                    @if($subcategoriadet != '')<li class="active"><a href="/{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url) !!}/{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $subcategoria->id, $subcategoria->seo_url) !!}">{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo) !!}</a></li>@endif
                                </ul>
                            </span>
                            @if($subcategoriadet != ''){!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoriadet->id, $subcategoriadet->titulo) !!}@else{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo) !!}@endif
                             <small>
                                 @if($subcategoriadet != ''){!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_corta', $subcategoriadet->id, $subcategoriadet->desc_corta) !!}@else{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_corta', $subcategoria->id, $subcategoria->desc_corta) !!}@endif
                             </small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="headerbgoverlay"></div>

    <div id="sidebar" class="lista">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                <div class="pull-left">
                    <div class="widget clearfix filtros">
                        <form id="Filters">
                            <?php $menor = $subcategoria->es_menor; ?>

                            @if($menor == 1)
                                <fieldset class="filter-group checkboxes">
                                    <div class="btn-group">
                                        <button class="btn btn-primary">{{trans('web.edad')}}</button>
                                        <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                        <ul class="dropdown-menu bullet pull-center">
                                            <li><input type="checkbox" id="{{trans('web.edad')}}-all" value=".{{trans('web.edad')}}-all" class="all" checked="checked"><label for="{{trans('web.edad')}}-all">{{trans('web.todas')}}</label></li>
                                            @for($edad=6; $edad <=19; $edad++)
                                                <li><input type="checkbox" id="{{trans('web.edad')}}-{{$edad}}" value=".{{trans('web.edad')}}-{{$edad}}"><label for="{{trans('web.edad')}}-{{$edad}}">{{$edad}}</label></li>
                                            @endfor
                                        </ul>
                                    </div>
                                </fieldset>
                            @endif

                            <?php $paises = array(); ?>
                            <?php $ciudad = false; ?>
                            <fieldset class="filter-group checkboxes">
                                <?php if($subcategoriadet != ''){
                                     $cursos =  $subcategoriadet->cursos;
                                }else{
                                    $cursos =  $subcategoria->cursos;
                                    }
                                ?>

                                @foreach($cursos as $c)
                                    @foreach(\VCN\Models\Centros\Centro::distinct()->where('id', $c->centro->id)->get() as $p)
                                        @if($p->pais->name != 'España')
                                            <?php $paises[] = Traductor::getWeb(App::getLocale(), 'Pais', 'name', $p->pais->id, $p->pais->name); ?>
                                        @else
                                            <?php $ciudad = true; ?>
                                            <?php $paises[] = Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $p->ciudad->id, $p->ciudad->city_name); ?>
                                        @endif
                                    @endforeach
                                @endforeach
                                <div class="btn-group">
                                    <button class="btn btn-primary">@if($ciudad == false) {{trans('web.pais')}} @else {{trans('web.ciudad')}} @endif</button>
                                    <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                    <ul class="dropdown-menu bullet pull-center">
                                            <li><input type="checkbox" id="pais-all" value=".pais-all" class="all" checked="checked"><label for="pais-all">{{trans('web.todos')}}</label></li>
                                        @foreach(array_unique($paises) as $pais)
                                            <li><input type="checkbox" id="{{str_slug($pais)}}" value=".{{str_slug($pais)}}"><label for="{{str_slug($pais)}}">{{$pais}}</label></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </fieldset>

                            <?php $alojas = array(); ?>
                            <fieldset class="filter-group checkboxes">
                                @foreach($cursos as $c)
                                    @foreach($c->alojamientos as $alojamiento)
                                        <?php $alojas[] = Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $alojamiento->tipo->id, $alojamiento->tipo->accommodation_type_name); ?>
                                    @endforeach
                                @endforeach
                                <div class="btn-group">
                                    <button class="btn btn-primary">{{trans('web.alojamiento')}}</button>
                                    <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                    <ul class="dropdown-menu bullet pull-center">
                                        <li><input type="checkbox" id="alojamiento-all" value=".alojamiento-all" class="all" checked="checked"><label for="alojamiento-all">{{trans('web.todos')}}</label></li>
                                        @foreach(array_unique($alojas) as $aloja)
                                            <li><input type="checkbox" id="{{str_slug($aloja)}}" value=".{{str_slug($aloja)}}"><label for="{{str_slug($aloja)}}">{{$aloja}}</label></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </fieldset>

                            <?php $especialidades = array(); ?>
                            @foreach($cursos as $c)
                                @foreach(\VCN\Models\Cursos\CursoEspecialidad::distinct()->where('curso_id',$c->id)->get() as $e)
                                    <?php $especialidades[] = Traductor::getWeb(App::getLocale(), 'Especialidad', 'name', $e->especialidad->id, $e->especialidad->name); ?>
                                @endforeach
                            @endforeach
                            @if(count($especialidades))
                                <fieldset class="filter-group checkboxes">
                                    <div class="btn-group">
                                        <button class="btn btn-primary">{{trans('web.especialidad')}}</button>
                                        <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                        <ul class="dropdown-menu bullet pull-center">
                                            <li><input type="checkbox" id="especialidades-all" value=".especialidades-all" class="all" checked="checked"><label for="especialidades-all">{{trans('web.todas')}}</label></li>
                                            @foreach(array_unique($especialidades) as $especialidad)
                                                <li><input type="checkbox" id="{{str_slug($especialidad)}}" value=".{{str_slug($especialidad)}}"><label for="{{str_slug($especialidad)}}">{{$especialidad}}</label></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </fieldset>
                            @endif

                           <?php
                           if($subcategoriadet != ''){
                               $idiomas = explode(',',$subcategoriadet->idioma);
                           }else{
                               $idiomas = explode(',',$subcategoria->idioma);
                           }

                           ?>

                            @if(count($idiomas) > 1)
                                <fieldset class="filter-group checkboxes">
                                    <div class="btn-group">
                                        <button class="btn btn-primary">Idioma</button>
                                        <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                        <ul class="dropdown-menu bullet pull-center">
                                            <li><input type="checkbox" id="idioma-all" value=".idioma-all" class="all" checked="checked"><label for="idioma-all">{{trans('web.todos')}}</label></li>
                                            @foreach($cursos->pluck('course_language')->unique()->toArray() as $idioma)
                                                @if(in_array($idioma,$idiomas))
                                                    <li><input type="checkbox" id="{{str_slug($idioma)}}" value=".{{str_slug($idioma)}}"><label for="{{str_slug($idioma)}}">{{trans('web.idiomas.'.$idioma)}}</label></li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </fieldset>
                           @endif
                        </form>
                    </div>
                </div>
                <div class="controls pull-right text-right">
                        <div class="total-cursos"><i class="fa fa-eye"></i> <span></span></div>
                        <button id="viewcolssmall" class="layout active"><i class="fa fa-th"></i></button>
                        <button id="viewcolsbig" class="layout"><i class="fa fa-th-large"></i></button>
                        <button id="viewlist" class="layout"><i class="fa fa-list"></i></button>
                        <div class="separator"></div>
                        <button id="sortPromo" class="order active"><i class="fa fa-asterisk"></i></button>
                        <button id="sortName" class="order"><i class="fa fa-sort-alpha-asc"></i></button>
                        <button id="sortPais" class="order"><i class="fa fa-map-marker"></i></button>
                    <!--
                    <div class="col-sm-2 pull-right">
                        <button id="Reset" class="btn-block">ver todos</button>
                    </div>
                    -->
                </div>
                </div>

            </div>
        </div>
    </div>
    </div>
    <main class="cd-main-content lista">

        <div class="container" id="contenido">

            <div class="row">
                <div class="col-sm-12 col-xs-12 {{$clase}} categorias">
                    {{--
                    <div class="introseccion">
                        @if($subcategoriadet != '')
                            @if(Lang::has('web.categorias.'.$subcategoriadet->slug.'-intro')){!! trans('web.categorias.'.$subcategoriadet->slug.'-intro') !!}@endif
                        @else
                            @if(Lang::has('web.categorias.'.$subcategoria->slug.'-intro')){!! trans('web.categorias.'.$subcategoria->slug.'-intro') !!}@endif
                        @endif
                    </div>
                    --}}
                    <div id="programas">
                            <div id="cursos-lista">
                                <div class="fail-message"><span>{{trans('web.sinresultados')}}</span></div>
                                <div class="slides">
                                    @foreach($cursos as $curso)
                                        <?php
                                        $fotoscentro = '';
                                        $fotoscentroname = array();
                                        $path = public_path() ."/assets/uploads/center/" . $curso->centro->center_images;
                                        $folder = "/assets/uploads/center/" . $curso->centro->center_images;

                                        if (is_dir($path)) {
                                            $results = scandir($path);
                                            foreach ($results as $result) {
                                                if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                                                $file = $path . '/' . $result;

                                                if (is_file($file)) {
                                                    $fotoscentroname[] = $result;

                                                }
                                            }
                                        }
                                        ?>
                                        <?php
                                        $fotoscurso = '';
                                        $fotoscursoname = array();
                                        $path = public_path() ."/assets/uploads/course/" . $curso->course_images;
                                        $folder = "/assets/uploads/course/" . $curso->course_images;

                                        if (is_dir($path)) {
                                            $results = scandir($path);
                                            foreach ($results as $result) {
                                                if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                                                $file = $path . '/' . $result;

                                                if (is_file($file)) {
                                                    $fotoscursoname[] = $result;
                                                }
                                            }
                                        }
                                        ?>

                                        <?php $espes = array(); ?>
                                        @foreach($curso->especialidades as $e)
                                            <?php $espes[] = str_slug(Traductor::getWeb(App::getLocale(), 'Especialidad', 'name', $e->especialidad->id, $e->especialidad->name)); ?>
                                        @endforeach

                                        <?php $alojascurso = array(); ?>
                                        @foreach($curso->alojamientos as $alojamiento)
                                            <?php $alojascurso[] = str_slug(Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $alojamiento->tipo->id, $alojamiento->tipo->accommodation_type_name)); ?>
                                        @endforeach


                                        <?php $edades = array(); ?>
                                        @if($menor == 1)
                                            @foreach(explode(',',$curso->course_age) as $edad)
                                                <?php $edades[] = trans('web.edad').'-'.trim($edad); ?>
                                            @endforeach
                                            <?php $edades = array_map('trim',$edades); ?>
                                        @endif

                                        <?php $cursolink = '/'.Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url).'/'.Traductor::getWeb(App::getLocale(), 'Curso', 'course_slug', $curso->id, $curso->course_slug).'.html'; ?>


                                        @if(count($idiomas) > 1)
                                            @if(is_array($curso->course_language))
                                                <?php $idiomascurso = implode(' ',$curso->course_language); ?>
                                            @else
                                            <?php $idiomascurso = $curso->course_language; ?>
                                            @endif
                                        @else
                                            <?php $idiomascurso = ''; ?>
                                        @endif

                                        @if(is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada))
                                            <div class="mix @if($curso->centro->pais->name != 'España') {{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}} @else {{str_slug(Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name))}} @endif {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}} {{implode($edades,' ')}} {{str_slug($idiomascurso)}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}}">
                                                @if($curso->course_promo == 1)
                                                    <div class="promo-lista">
                                                        <i class="fa fa-asterisk"></i>
                                                    </div>
                                                @endif
                                                <div class="fotocurso" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/thumb/{{$curso->image_portada}}');">
                                                    <a href="{{$cursolink}}" class="foto">
                                        @elseif(is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                                            <div class="mix @if($curso->centro->pais->name != 'España') {{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}} @else {{str_slug(Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name))}} @endif  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}} {{implode($edades,' ')}} {{str_slug($idiomascurso)}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}}">
                                                @if($curso->course_promo == 1)
                                                    <div class="promo-lista">
                                                        <i class="fa fa-asterisk"></i>
                                                    </div>
                                                @endif
                                                <div class="fotocurso" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/thumb/{{$curso->centro->center_image_portada}}');">
                                                    <a href="{{$cursolink}}" class="foto">
                                        @elseif(!is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
                                            @if(count($fotoscursoname))
                                                <div class="mix @if($curso->centro->pais->name != 'España') {{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}} @else {{str_slug(Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name))}} @endif  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}} {{implode($edades,' ')}} {{str_slug($idiomascurso)}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}}">
                                                    @if($curso->course_promo == 1)
                                                        <div class="promo-lista">
                                                            <i class="fa fa-asterisk"></i>
                                                        </div>
                                                    @endif
                                                    <div class="fotocurso" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/thumb/{{$fotoscursoname[rand(0,count($fotoscursoname)-1)]}}'); background-size: cover; background-position: center center;">
                                                        <a href="{{$cursolink}}" class="foto">
                                            @elseif(!count($fotoscursoname) && count($fotoscentroname))
                                                <div class="mix @if($curso->centro->pais->name != 'España') {{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}} @else {{str_slug(Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name))}} @endif  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}} {{implode($edades,' ')}} {{str_slug($idiomascurso)}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}}">
                                                    @if($curso->course_promo == 1)
                                                        <div class="promo-lista">
                                                            <i class="fa fa-asterisk"></i>
                                                        </div>
                                                    @endif
                                                    <div class="fotocurso" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/thumb/{{$fotoscentroname[rand(0,count($fotoscentroname)-1)]}}'); background-size: cover;">
                                                        <a href="{{$cursolink}}" class="foto">
                                            @else
                                                <div class="mix @if($curso->centro->pais->name != 'España') {{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}} @else {{str_slug(Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name))}} @endif  {{implode(array_unique($espes),' ')}} {{implode(array_unique($alojascurso),' ')}} {{implode($edades,' ')}} {{str_slug($idiomascurso)}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug(Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name))}}">
                                                    @if($curso->course_promo == 1)
                                                        <div class="promo-lista">
                                                            <i class="fa fa-asterisk"></i>
                                                        </div>
                                                    @endif
                                                    <div class="fotocurso" style="background: #E5E5E5;">
                                                        <a href="{{$cursolink}}" class="foto">

                                            @endif
                                        @endif
                                                </a>
                                            </div>

                                            <div class="fichacurso">
                                                <h4 class="nombrecurso">
                                                    <div class="row">
                                                        <small class="col-sm-12 nombrepais">@if($curso->centro->pais->name != 'España') {{Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name)}} @else {{Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name)}} @endif</small>

                                                        @if(count($curso->convocatoriasCerradas) && $curso->convocatoriasCerradas->contains('convocatory_semiopen', 0) && $curso->category_id == 2)
                                                            <?php $plazasrestantes = 0; ?>
                                                            @foreach($curso->convocatoriasCerradas->sortBy('convocatory_close_start_date')->sortBy('convocatory_close_duration_weeks') as $cc)
                                                                @if($cc->activo_web == 1)
                                                                    {{--@if($cc->alojamiento_id == $aloja->id)--}}
                                                                    @if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 0)
                                                                        <?php $plazasrestantes += $cc->plazas_disponibles; ?>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                                @if($plazasrestantes <= 0 && $cerrado != 1)
                                                                    <small class="col-sm-6 pull-right text-right grupocerrado plazas">{{trans('web.grupocerrado')}}</small>
                                                                @elseif($plazasrestantes != 0 && $plazasrestantes < 6 && $cerrado != 1)
                                                                    <small class="col-sm-6 pull-right text-right ultimasplazas plazas">{{trans('web.ultimasplazas')}}</small>
                                                                @endif
                                                        @endif
                                                    </div>

                                                    <a href="{{$cursolink}}">
                                                    {!! Traductor::getWeb(App::getLocale(), 'Curso', 'name', $curso->id, $curso->course_name) !!}
                                                    </a>
                                                </h4>

                                                <p class="edades">{!! Traductor::getWeb(App::getLocale(), 'Curso', 'course_age_range', $curso->id, $curso->course_age_range) !!}</p>

                                                <div class="separator-ficha"></div>

                                                <?php $alojas = array(); ?>
                                                @foreach($curso->alojamientos as $alojamiento)
                                                    <?php $alojas[] = Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $alojamiento->tipo->id, $alojamiento->tipo->accommodation_type_name); ?>
                                                @endforeach
                                                <p class="cursoalojas text-capitalize">{{implode(array_unique($alojas),', ')}}</p>

                                                    @if(count($curso->especialidades))
                                                        <div class="especialidades pull-left"><span>

                                                                <?php $anteriorespe = ''; ?>
                                                                <?php $i = 1; ?>

                                                                @foreach($curso->especialidades->sortBy('name') as $e)

                                                                    @if($e->especialidad->id != $anteriorespe || $anteriorespe == null)
                                                                        {!!Traductor::getWeb(App::getLocale(), 'Especialidad', 'name', $e->especialidad->id, $e->especialidad->name)!!}:
                                                                    @endif

                                                                    @if(($e->especialidad->id == $anteriorespe || $i == 1) && count($curso->especialidades) != $i)
                                                                        {!!Traductor::getWeb(App::getLocale(), 'Subespecialidad', 'name', $e->subespecialidad_id, $e->SubespecialidadesName)!!},
                                                                    @endif

                                                                    @if(count($curso->especialidades) == $i)
                                                                        {!!Traductor::getWeb(App::getLocale(), 'Subespecialidad', 'name', $e->subespecialidad_id, $e->SubespecialidadesName)!!}.<br />
                                                                    @endif

                                                                    <?php $anteriorespe = $e->especialidad->id; ?>
                                                                    <?php $i++; ?>

                                                                @endforeach
                                                        </span></div>

                                                    @endif
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="gap"></div>
                                    <div class="gap"></div><div class="gap"></div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
                <div class="row">
                    @include('web.dippy.includes.copyright')
                </div>

            </div>
    </main>

<!-- Modal -->
<div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <img class="modal-logo" src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" />
            </div>
            <div class="modal-body">
                <div id="respuesta">
                    <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                    <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                        <div class="msg"></div>
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="form-group">
                                    <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                    <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                    <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                    <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                    <input type="hidden" id="curso" value="{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) !!} - {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo) !!} @if($subcategoriadet != '')- {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoriadet->id, $subcategoriadet->titulo) !!}@endif">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>

                <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop


@section('extra_footer')

<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js" type="text/javascript"></script>
<script src="/assets/{{ConfigHelper::config('tema')}}/js/dropdowns-enhancement.js"></script>
<script src="/assets/{{ConfigHelper::config('tema')}}/js/filters.js" type="text/javascript"></script>


<script>
    $('#viewlist').click( function(){
        if(!$('.mix').hasClass('one')) {
            $('.fichacurso').hide().delay(300).fadeIn(200);
        }
        $('.mix, .gap').addClass('one').removeClass('two');
        $('.layout').siblings().removeClass('active');
        $(this).addClass('active');
    });
    $('#viewcolsbig').click( function(){
        $('.mix, .gap').addClass('two').removeClass('one');
        $('.layout').removeClass('active');
        $(this).addClass('active');
    });
    $('#viewcolssmall').click( function(){
        $('.mix, .gap').removeClass('two').removeClass('one');
        $('.layout').removeClass('active');
        $(this).addClass('active');
    });
    $(document).ready(function () {
        $('.lista #contenido').css({'padding-top': $('#sidebar').height()+'px'});
    });


    $('#sortPromo').click(function(){
        $('#cursos-lista').mixItUp('sort', 'promo:desc, name:asc');
        $('.order').removeClass('active');
        $(this).addClass('active');
    });
    $('#sortName').click(function(){
        $('#cursos-lista').mixItUp('sort', 'name:asc');
        $('.order').removeClass('active');
        $(this).addClass('active');
    });
    $('#sortPais').click(function(){
        $('#cursos-lista').mixItUp('sort', 'pais:asc');
        $('.order').removeClass('active');
        $(this).addClass('active');
    });
    $('.foto').hover(
            function(){
                $(this).addClass('active');
            },
            function () {
                $(this).removeClass('active');
            }
    );

    $('.nombrecurso').hover(function(e) {
       $(this).parents('.mix').find('.fotocurso').find('a').trigger(e.type);
    });

    if($(window).width() <= '767'){
        $('#viewlist').trigger('click');
        $('.gap').css({'display': 'none', 'visbility': 'hidden'});
    }

            $(document).ready(function () {
                $('.msg').hide();
                $("#plusinfoenviar").click(function () {
                    //console.log('validar');
                    if ($('#name').val() == '') {
                        $('.msg').html('Debes indicar un nombre de contacto');
                        $('.msg').show();
                        return false;
                    }
                    if ($('#email').val() == '') {
                        $('.msg').html('Debes indicar un teléfono o un email de contacto');
                        $('.msg').show();
                        return false;
                    }

                    if ($('#email').val() != '') {
                        var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
                        if (re.test($('#name').val())) {
                            $('.msg').html('El email no tiene un formato correcto');
                            $('.msg').show();
                            return false
                        }

                    }

                    if (!/^([0-9])*$/.test($('#tel').val())) {
                        $('.msg').html('El campo teléfono tiene que ser numérico');
                        $('.msg').show();
                        return false
                    }


                    post_data = {
                        'name': $('#name').val(),
                        'tel': $('#tel').val(),
                        'email': $('#email').val(),
                        'curso': $('#curso').val()
                    };
                    $.ajax({
                        type: "POST",
                        url: "/assets/{{ConfigHelper::config('tema')}}/includes/plusinfosend-{{ConfigHelper::config('sufijo')}}.php",
                        data: post_data,
                        success: function (msg) {
                            console.log(msg);
                            $("#respuesta").html(msg);
                            $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                            @if(ConfigHelper::config('propietario') == 1)
                                ga('send', 'Solicitud', 'button', 'click', "{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) !!} - {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo) !!} @if($subcategoriadet != '')- {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoriadet->id, $subcategoriadet->titulo) !!}@endif", 1);
                            @endif
                        },
                        error: function () {
                            alert("error!!");
                        }
                    });
                });

            });

    $('form input').blur(function () {
        $('.msg').hide();
    });
</script>

@stop


