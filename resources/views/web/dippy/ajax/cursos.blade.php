<!DOCTYPE html>
<html lang="en">
    <head>
        <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
        <meta name="_token" content="{{ csrf_token() }}"/>

        <script src="https://cdn.jsdelivr.net/vue/2.2.6/vue.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.0/vue-resource.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.0/axios.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        {{-- <script type="text/javascript" src="js/main.js"></script> --}}

        <title>Test Vuejs</title>

        <style type="text/css" media="screen">
            .list-group-item {
                height: 80px;
                border: 1px solid black;
                margin-bottom: 6px;
            }
        </style>
    </head>
    <body>

        <div id="app">
            <h4>Cursos:</h4>
            <span v-if="!list.length"><i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw"></i></span>
            <ul v-else class="list-group">
                <li class="list-group-item animated bounce" v-for="curso in list">
                    @{{curso.id}} :: @{{curso.course_name}} @{{curso.course_name}}
                </li>
                <i id="loading" class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
            </ul>

            <button @click="fetchMas(5)" class="btn btn-primary btn-xs">Más</button>
        </div>

        <script>

            function getDocHeight() {
                var D = document;
                return Math.max(
                    D.body.scrollHeight, D.documentElement.scrollHeight,
                    D.body.offsetHeight, D.documentElement.offsetHeight,
                    D.body.clientHeight, D.documentElement.clientHeight
                );
            }

            var app = new Vue({
                el: '#app',
                data: function() {
                    return {
                        list: [],
                        mas: 0,
                        scrolled: false
                    };
                },
                mounted: function() {
                    this.fetchList();
                    window.addEventListener('scroll', this.handleScroll);
                },
                destroyed () {
                    window.removeEventListener('scroll', this.handleScroll);
                },
                methods: {
                    fetchList: function() {
                        this.$http.get('{{route('web.buscar.vue.api')}}').then(function (response) {
                            this.list = response.data
                        });
                    },
                    fetchMas: function(m) {

                        this.mas += m
                        var url = '{{route('web.buscar.vue.api')}}' + "?max=" + this.mas

                        this.$http.get(url).then(function (response) {
                            $('#loading').fadeOut();
                            this.list.push.apply(this.list, response.data)
                            // this.list = this.list.concat(response.data)
                        })
                    },
                    handleScroll () {

                        if($(window).scrollTop() + $(window).height() == getDocHeight())
                        {
                            app.fetchMas(10)
                            $('#loading').fadeIn();
                        }

                        // if(window.scrollY + window.screenY > window.innerHeight && !this.scrolled)
                        // {
                        //     console.log("scroll")
                        //     this.scrolled = false
                        //     app.fetchMas(10)
                        //     $('#loading').fadeIn()
                        //     this.scrolled = window.scrollY > 0;
                        // }
                    },
                }
            })
        </script>

    </body>
</html>