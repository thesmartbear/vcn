@extends('layouts.base')

@section('container')

    <style>
        .container {
            vertical-align: middle;
            font-family: 'Lato';
        }

        .content {
            display: inline-block;
        }

        .title {
            font-size: 2em;
            margin-bottom: 40px;
        }

        .quote {
            font-size: 1.5em;
        }
    </style>

    <div class="container">
        <div class="content">
            <div class="title">{{ConfigHelper::config('web')}}</div>
        </div>
    </div>
@stop
