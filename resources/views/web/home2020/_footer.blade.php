{{-- FOOTER --}}
<footer class="container-fluid">

    {{-- SCROLL TO TOP --}}
    <div id="scrollToTop">
        <i class="fal fa-arrow-up"></i>
    </div>
    
    {{-- FOOTER - MENU --}}

    {{-- <div class="row no-gutters pb-5 pt-5 optionsFooterWhite">
        <div class="col-12 col-lg-6">
            <div class="row no-gutters optionsFooter">
                <div class="col-lg-6"></div>
                <div class="col-lg-3"> --}}
                    
    <div class="row no-gutters pb-0 pb--lg-2 pt-0 pt-lg-2 footer-cols">

        <div class="col-md-6">
            <div class="row">
                <div class="d-none d-lg-block col-lg-2 mr-3 ml-3"></div>

                @foreach(VCN\Models\CMS\CategoriaWeb::arbol('menu_secundario', 0, 3) as $menucat)
                 
                    <div class="col-lg-3">
                        <div class="title">
                            <a class="amain" {!! $menucat['enlace'] !!}>
                                {{-- {{$menucat['id']}}:> --}}
                                {!!Traductor::getTrans('CategoriaWeb', 'titulo', $menucat['id'], $menucat['titulo'])!!}
                            </a>
                        </div>
                        
                        @if(isset($menucat['subcategorias']) && count($menucat['subcategorias']))
                            <p>
                                {{-- <a {!! $menucat['enlace'] !!}">{!!Traductor::getTrans('CategoriaWeb', 'titulo', $menucat['id'], $menucat['titulo'])!!}</a> --}}
                                <ul class="sub-menu">
                                    @foreach($menucat['subcategorias'] as $subcat)
                                        <li>
                                            {{-- {{$subcat['id']}}:> --}}
                                            <a {!! $subcat['enlace'] !!}>@if($subcat['titulo'] != ''){!!Traductor::getTrans('CategoriaWeb', 'titulo', $subcat['id'], $subcat['titulo'])!!} @else {!!Traductor::getTrans('CategoriaWeb', 'name', $subcat['id'], $subcat['name'])!!}@endif</a>
                                            @if(isset($subcat['subcategoriasdetalle']) && count($subcat['subcategoriasdetalle']))
                                            <ul class="sub-menu">
                                            @foreach($subcat['subcategoriasdetalle'] as $subcat2)
                                                <li>
                                                    <a {!! $subcat2['enlace'] !!}>
                                                        {{-- {{$subcat2['id']}}:> --}}
                                                        @if($subcat2['titulo'] != '')
                                                            _{!!Traductor::getTrans('CategoriaWeb', 'titulo', $subcat2['id'], $subcat2['titulo'])!!} 
                                                        @else
                                                            _{!!Traductor::getTrans('CategoriaWeb', 'name', $subcat2['id'], $subcat2['name'])!!}
                                                        @endif
                                                    </a>
                                                </li>
                                            @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </p>
                        @endif
                    </div>
                    
                @endforeach

            </div>
        </div>

        <div class="col-md-5">
            <div class="row no-gutters optionsFooter">
                
                @foreach(VCN\Models\CMS\CategoriaWeb::arbol('menu_secundario', 3, 1) as $menucat)
                    
                    <div class="col-lg-3"> 
                        <div class="title">
                            <a class="amain" {!! $menucat['enlace'] !!}>
                                {!!Traductor::getTrans('CategoriaWeb', 'titulo', $menucat['id'], $menucat['titulo'])!!}
                            </a>
                        </div>
                        
                        @if(isset($menucat['subcategorias']) && count($menucat['subcategorias']))
                            <p>
                                <a {!! $menucat['enlace'] !!}">{!!Traductor::getTrans('CategoriaWeb', 'titulo', $menucat['id'], $menucat['titulo'])!!}</a>
                                <ul class="sub-menu">
                                    @foreach($menucat['subcategorias'] as $subcat)
                                        <li>
                                            {{-- <a {!! $subcat['enlace'] !!}>@if($subcat['titulo'] != ''){!!Traductor::getTrans('CategoriaWeb', 'titulo', $subcat['id'], $subcat['titulo'])!!} @else {!!Traductor::getTrans('CategoriaWeb', 'name', $subcat['id'], $subcat['name'])!!}@endif</a> --}}
                                            @if(isset($subcat['subcategoriasdetalle']) && count($subcat['subcategoriasdetalle']))
                                            <ul class="sub-menu">
                                            @foreach($subcat['subcategoriasdetalle'] as $subcat2)
                                                <li>
                                                    <a {!! $subcat2['enlace'] !!}>
                                                        @if($subcat2['titulo'] != '')
                                                            _{!!Traductor::getTrans('CategoriaWeb', 'titulo', $subcat2['id'], $subcat2['titulo'])!!} 
                                                        @else
                                                            _{!!Traductor::getTrans('CategoriaWeb', 'name', $subcat2['id'], $subcat2['name'])!!}
                                                        @endif
                                                    </a>
                                                </li>
                                            @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </p>
                        @endif
                    </div>
                    
                @endforeach

                {{-- PÁGINAS (menu_secundario)--}}                    
                <div class="col-lg-3"> 
                    <div class="title">
                        @lang("web.about")
                    </div>

                    @foreach(VCN\Models\CMS\Pagina::WhereIn('propietario', [0,ConfigHelper::config('propietario')])->where('menu_secundario',1)->get() as $pagina)
                        <p><a href="{!! Traductor::trans('Pagina', 'url', $pagina) !!}.html">{!! Traductor::trans('Pagina', 'titulo', $pagina) !!}</a></p>
                    @endforeach

                    <p>-</p>
                    @switch(ConfigHelper::config('sufijo'))

                        @case("cic")
                            
                            <p><a href="//viatges.iccic.edu/be-cicviatges/" target="_blank">Be CIC Viatges. CONCURSOS</a></p>
                            <p><a href="http://www.landedblog.com/iccic/" target="_blank">{!! trans('web.landedslogan') !!}</a></p>
                        
                            @break

                        @case("sf")
                            <p><a href="https://www.studyfuera.com/blog" target="_blank">Blog</a></p>
                        
                            @break
                        
                        
                        @default
                        
                            <p><a href="//www.britishsummer.com/be-bs/" target="_blank">Be Bs. Concursos</a></p>
                            <p><a href="//www.britishsummer.com/blog-aprender-ingles-extranjero" target="_blank">I Love Brit Blog</a></p>
                            <p><a href="//www.britishsummer.com/el-blog-del-verano.html">BLOG DEL VERANO</a></p>
                            
                    @endswitch

                </div>

                {{-- CONTACTO --}}
                <div class="col-lg-3">
                    <div class="title">
                        {{trans('web.contacto')}}
                    </div>

                    @switch(ConfigHelper::config('sufijo'))

                        @case('cic')
                            <p>93 200 11 33</p>
                            @break
                        
                        @case('sf')
                            <p>55 4538 2090</p>
                            <p>
                                <a href="https://api.whatsapp.com/send?phone=525545382090">
                                    <i class="fa fa-whatsapp"></i> +52 55 4538 2090
                                </a>
                            </p>
                            @break

                        @default
                            <p>932 008 888 <br> 913 459 565</p>
                            {{-- <p>
                            @if(ConfigHelper::config('chat'))
                                @include('chat.web')
                            @else
                                @if(ConfigHelper::config('propietario') == 1)
                                    @if(App::getLocale() == 'es')
                                        <a href="//www.britishsummer.com/webim/client.php?locale=sp&style=britishsummer" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('//www.britishsummer.com/webim/client.php?locale=sp&amp;style=britishsummer&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=800,height=600,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;"><i class="fa fa-comments"></i> {{trans('web.chat')}}</a>
                                    @else
                                        <a href="//www.britishsummer.com/webim/client.php?locale={{App::getLocale()}}&style=britishsummer" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('//www.britishsummer.com/webim/client.php?locale={{App::getLocale()}}&amp;style=britishsummer&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=800,height=600,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;"><i class="fa fa-comments"></i> {{trans('web.chat')}}</a>
                                    @endif
                                @endif
                            @endif
                            </p> --}}
                            <p>
                                <a href="https://api.whatsapp.com/send?phone=34653961554">
                                    <i class="fa fa-whatsapp"></i> 653 96 15 54
                                </a>
                            </p>

                    @endswitch
                    
                    
                    {{-- <p>escribir email</p> --}}
                    {{-- <p>chat instantaneo</p> --}}
                    {{-- <p>te llamamos?</p> --}}
                    
                </div>

                <div class="col-lg-2">
                    <a class="amain" href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{{trans('web.catalogo-slug')}}">{{trans('web.catalogo')}}</a>
                </div>
            </div>
        </div>

    </div>

    {{-- ENLACES --}}
    @php
    /*
    <div class="row no-gutters pb-5 pt-5 optionsFooterWhite">
        <div class="col-12 col-lg-6">
            <div class="row no-gutters optionsFooter">
                <div class="col-lg-6"></div>
                <div class="col-lg-3">
                    <div class="title">
                        <a class="underline" href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{{trans('web.catalogo-slug')}}">{{trans('web.catalogo')}}</a>
                    </div>
                </div>
                {{-- <div class="col-lg-3"></div> --}}
                {{-- <div class="col-lg-3">
                    <div class="title">
                        <span class="underline">@lang("web.buscar")
                        <form id="searchMenuFooter" action="{{route('web.buscar')}}" method="post" enctype="multipart/form-data" class="w-100" autocomplete="off" >
                            <input type="text" placeholder="{{trans('web.buscar')}}" name="search"  id="searchInputFooter" class="searchbox-input" required>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        </form>
                        </span>
                    </div>
                </div> --}}
            </div>

        </div>
        {{-- <div class="col-12 col-lg-6">
            <div class="row no-gutters optionsFooter">
                <div class="col-lg-3"></div>
                <div class="col-lg-3"> 
                    <div class="title">
                        <a class="underline" href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{{trans('web.contacto-slug')}}">{{trans('web.contacto')}}</a>
                    </div>                       
                </div>
                <div class="col-lg-3"></div>
                <div class="col-lg-3"></div>
            </div>
        </div> --}}
    </div>
    */
    @endphp 
     
    {{-- REDES SOCIALES --}}
    {{-- <div class="row no-gutters pt-3">
        <div class="d-none d-lg-block col-lg-3 col-xl-4"></div>
        <div class="d-none d-lg-block col-lg-3 col-xl-4"></div>
    </div> --}}

    {{-- FOOTER PÁGINAS INFO --}}
    <div class="row no-gutters pt-5 title optionsFooterWhite">
        
        <div class="col-1 col-lg-1"></div>
        <div class="col-10 col-lg-5">
            <div class="row no-gutters optionsFooter">
                <div class="col-12 col-lg-3"></div>
                <div class="col-12 col-lg-5">
                    {{-- <p class="titleBig">suscribete</p> --}}
                    {{-- <span>Política de cookies</span>
                    <span class="ml-5">Aviso legal</span> --}}

                    @switch(ConfigHelper::config('sufijo'))

                        @case('cic')
                            <div class="row no-gutters">
                                <div class="col-5 col-lg-5">
                                    <a class="amain" href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/aviso-legal.html">{{trans('web.aviso-legal')}}</a>
                                </div>
                                <div class="col-7 col-lg-7">
                                    <a class="amain" class="ml-5" href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/politica-de-cookies.html">{{trans('web.politica-de-cookies')}}</a>
                                </div>
                            </div>
                        @break

                        @default
                        <div class="row no-gutters">
                            <div class="col-5 col-lg-5">
                                <a class="amain" href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/aviso-legal.html">{{trans('web.aviso-legal')}}</a>
                            </div>
                            <div class="col-7 col-lg-7">
                                <a class="amain" href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/politica-de-cookies.html">{{trans('web.politica-de-cookies')}}</a>
                            </div>
                        </div>
                        
                    @endswitch

                </div>
                <div class="col-12 col-lg-3">
                    <div class="d-flex mx-5 mb-5 footer-rrss">

                    
                        @switch(ConfigHelper::config('sufijo'))
    
                            @case('cic')
    
                                <div class="socialIconContainer"><a href="https://es-es.facebook.com/CIC-Escola-dIdiomes-1426848570864710/" target="_blank"><i class="fa fa-facebook"></i></a></div>
                                <div class="socialIconContainer"><a href="//twitter.com/cicviatges" target="_blank"><i class="fa fa-twitter"></i></a></div>
                                <div class="socialIconContainer"><a href="https://www.youtube.com/channel/UCZXmgNRCnJiBBy_eWZ5o0Mg" target="_blank"><i class="fa fa-youtube-play"></i></a></div>
                                <div class="socialIconContainer"><a href="https://instagram.com/cicviatges" target="_blank"><i class="fa fa-instagram"></i></a></div>
    
                                @break

                            @case('sf')
    
                                <div class="socialIconContainer"><a href="https://instagram.com/studyfuera/" target="_blank"><i class="fa fa-instagram"></i></a></div>
                                <div class="socialIconContainer"><a href="https://www.instagram.com/studyfuera_careers/" target="_blank"><i class="fa fa-instagram"></i></a></div>
                                <div class="socialIconContainer"><a href="https://www.youtube.com/channel/UCsKPLtW0ojihu1j7hnZq6cg" target="_blank"><i class="fa fa-youtube-play"></i></a></div>
                                @break
    
                            @default  
                                <div class="socialIconContainer"><a href="//www.facebook.com/britishsummer" target="_blank"><i class="fa fa-facebook"></i></a></div>
                                <div class="socialIconContainer"><a href="//www.twitter.com/britishsm" target="_blank"><i class="fa fa-twitter"></i></a></div>
                                <div class="socialIconContainer"><a href="//www.youtube.com/britishsummersm" target="_blank"><i class="fa fa-youtube-play"></i></a></div>
                                <div class="socialIconContainer"><a href="https://instagram.com/britishsummeres/" target="_blank"><i class="fa fa-instagram"></i></a></div>
                                <div class="socialIconContainer"><a href="https://plus.google.com/106203615715916398922/posts" target="_blank"><i class="fa fa-google-plus"></i></a></div>
                                <div class="socialIconContainer"><a href="https://www.pinterest.com/britishsummer/" target="_blank"><i class="fa fa-pinterest"></i></a></div>
                                <div class="socialIconContainer"><a href="//www.linkedin.com/company/british-summer" target="_blank"><i class="fa fa-linkedin"></i></a></div>
    
                        @endswitch
                        
                        <div class="footer-logo">
                            <a id="cd-logo" href="/"><img src="/assets/logos/b-{{ConfigHelper::config('logoweb')}}" alt="{{ConfigHelper::config('nombre')}}" /></a>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-10 col-lg-6">
            
            <div class="row no-gutters optionsFooter">
                
                <div class="col-lg-5"></div>
                

                <div class="col-lg-3 text-lg-right">
                    {{-- <p class="titleBig">
                        @if(isset($weblangs))
                            @foreach ($weblangs as $weblang => $linklang)
                                @if($weblang != App::getLocale())
                                    <a href="{{$linklang}}" class="language" data-idioma="{{$weblang}}"/></i> <span>{{ConfigHelper::getIdiomaWeb($weblang)}}</span></a>
                                @endif
                            @endforeach
                        @else
                            @if(App::getLocale() == 'es')
                                <a href="/ca" class="language" data-idioma="ca"><span>{{ConfigHelper::getIdiomaWeb('ca')}}</span></a>
                            @elseif(App::getLocale() == 'ca')
                                <a href="/es" class="language" data-idioma="es"><span>{{ConfigHelper::getIdiomaWeb('es')}}</span></a>
                            @endif
                        @endif
                    </p> --}}
                    {{-- <span>By British Summer</span> --}}
                    <span>by {{ConfigHelper::config('nombre')}} - {{Carbon::now()->year}} &copy;</span>
                </div>
                {{-- <div class="col-lg-3"></div> --}}
            </div>
        </div>

    </div>
</footer>