<script src="//cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js" type="text/javascript"></script>
<script src="/assets/{{ConfigHelper::config('tema')}}/js/dropdowns-enhancement.js"></script>
<script src="/assets/{{ConfigHelper::config('tema')}}/js/filters.js" type="text/javascript"></script>

<script>
    $('#viewlist').click( function(){
        if(!$('.mix').hasClass('one')) {
            $('.fichacurso').hide().delay(300).fadeIn(200);
        }
        $('.mix, .gap').addClass('one').removeClass('two');
        $('.layout').siblings().removeClass('active');
        $(this).addClass('active');
    });
    $('#viewcolsbig').click( function(){
        $('.mix, .gap').addClass('two').removeClass('one');
        $('.layout').removeClass('active');
        $(this).addClass('active');
    });
    $('#viewcolssmall').click( function(){
        $('.mix, .gap').removeClass('two').removeClass('one');
        $('.layout').removeClass('active');
        $(this).addClass('active');
    });
    $(document).ready(function () {
        $('.lista #contenido').css({'padding-top': $('#sidebar').height()+'px'});
    });


    $('#sortPromo').click(function(){
        $('#cursos-lista').mixItUp('sort', 'promo:desc, name:asc');
        $('.order').removeClass('active');
        $(this).addClass('active');
    });
    $('#sortName').click(function(){
        $('#cursos-lista').mixItUp('sort', 'name:asc');
        $('.order').removeClass('active');
        $(this).addClass('active');
    });
    $('#sortPais').click(function(){
        $('#cursos-lista').mixItUp('sort', 'pais:asc');
        $('.order').removeClass('active');
        $(this).addClass('active');
    });
    $('.foto').hover(
            function(){
                $(this).addClass('active');
            },
            function () {
                $(this).removeClass('active');
            }
    );

    $('.nombrecurso').hover(function(e) {
       $(this).parents('.mix').find('.fotocurso').find('a').trigger(e.type);
    });

    if($(window).width() <= '767'){
        $('#viewlist').trigger('click');
        $('.gap').css({'display': 'none', 'visbility': 'hidden'});
    }

</script>