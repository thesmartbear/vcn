@php
    $img = $bloque->imagen_webp ?: "$assets/assets/imgs/jumping-girl.jpg";
@endphp

@if($bloque->color_fondo)
<style>
    .colorBg-{{$bloqueId}} {
        background-color: {{ $bloque->color_fondo }} !important;
    }
</style>
@endif

<div class="container-fluid d-flex container7 bloqueDiv colorBg-{{$bloqueId}}" id="bloque-{{$bloqueId}}">
    <div class="row no-gutters flex-fill">
        <div class="col-1 col-lg-3"></div>
        <div class="col-11 col-lg-3">
            <div class="content">
                <h1>{!! $bloque->getTranslate('home_titulo') !!}</h1>
                <p class="mt-4">{!! $bloque->getTranslate('home_titulo2') !!}</p>
                @if($bloque->home_boton_activo)
                    <div class="linkContainer colorTxt noCentered mt-2"><a class="colorTxt" {!! $bloque->home_enlace !!}>{!! $bloque->home_enlace_txt !!}</a></div>
                @endif
            </div>
        </div>
        <div class="col-12 col-lg-5 pt-5 p-lg-0 text-center">
            <img src="{{ $img }}" class="img-fluid" alt="" />
        </div>
        <div class="d-none d-lg-block col-lg-1"></div>
    </div>
</div>