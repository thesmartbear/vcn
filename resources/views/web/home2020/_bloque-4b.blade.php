<div class="container-fluid d-flex container9 bloqueDiv" id="bloque-{{$bloqueId}}">
    <div class="row no-gutters flex-fill">
        <div class="col-lg-6 imgBg"></div>
        <div class="col-lg-5">
            <div class="content">
                <h1>BLA DI BLUM<br/>DI BLAM BLAM</h1>
                <p class="mt-4">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur, doloribus odit repellendus quia rem veniam autem aut</p>
                @if($bloque->home_boton_activo)
                <div class="linkContainer noCentered"><a href="#" class="">más info</a></div>
                @endif
            </div>
        </div>
        <div class="d-none d-lg-block col-lg-1"></div>
    </div>
</div>