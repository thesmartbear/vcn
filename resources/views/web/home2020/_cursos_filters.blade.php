<style>
    .filter-edad {
        margin-right: -110px !important;
    }
</style>
<div id="sidebar" class="lista lista-menu">
    <div class="container">
        <div class="row">
            <div class="col-sm-2"></div>            
            <div class="col-sm-10">
                <div class="pull-left">
                    
                    <div class="widget clearfix filtros">

                        @php
                            $lang = App::getLocale();
                        @endphp

                        @if($cursos)
                        <form id="Filters">
                            
                            {{-- @if($menor == 1) --}}
                                <fieldset class="filter-group checkboxes">
                                    <div class="btn-group">
                                        <button class="btn btn-primary">{{trans('web.edad')}}</button>
                                        <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                        <ul class="dropdown-menu bullet pull-center filter-edad">
                                            {{-- <li><input type="checkbox" id="edad-all" value=".edad-all" class="all" checked="checked"><label for="edad-all">{{trans('web.todas')}}</label></li> --}}
                                            @for($edad=6; $edad <=19; $edad++)
                                                <li><input type="checkbox" id="edad-{{$edad}}" value=".edad-{{$edad}}"><label for="edad-{{$edad}}">{{$edad}}</label></li>
                                            @endfor
                                        </ul>
                                    </div>
                                </fieldset>
                            {{-- @endif --}}

                            @if($fcats ?? false)
                                <fieldset class="filter-group checkboxes">
                                    <div class="btn-group">
                                        <button class="btn btn-primary">{{trans('web.subcategorias')}}</button>
                                        <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                        <ul class="dropdown-menu bullet pull-center">
                                            {{-- <li><input type="checkbox" id="fcat-all" value=".fcat-all" class="all" checked="checked"><label for="fcat-all">{{trans('web.todas')}}</label></li> --}}
                                            @foreach($fcats as $fid => $f)
                                                <li><input type="checkbox" id="{{$fid}}" value=".{{$fid}}"><label for="{{$fid}}">{{$f}}</label></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </fieldset>
                            @endif
                    
                            <?php
                                $ciudad = false;
                                $paises = [];
                            ?>
                            <fieldset class="filter-group checkboxes">
                                @foreach($cursos as $curso)
                                    @php
                                        $dataweb = $curso->dataweb->$lang;

                                        $pais = $dataweb->pais;
                                        if($pais != 'España' || $pais != 'Espanya')
                                        {
                                            $paises[] = $pais;
                                        }
                                        else
                                        {
                                            $pais = $dataweb->ciudad;

                                            $ciudad = true;
                                            $paises[] = $ciudad;
                                        }
                                    @endphp
                                    

                                    {{-- @foreach(\VCN\Models\Centros\Centro::distinct()->where('id', $c->centro->id)->get() as $p)
                                        @php
                                            if($p->pais->name != 'España')
                                            {
                                                $paises[] = Traductor::trans('Pais', 'name', $p->pais); //Traductor::getWeb(App::getLocale(), 'Pais', 'name', $p->pais->id, $p->pais->name);
                                            }
                                            else
                                            {
                                                $ciudad = true;
                                                $paises[] = Traductor::trans('Ciudad', 'city_name', $p->ciudad); //Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $p->ciudad->id, $p->ciudad->city_name);
                                            }
                                        @endphp
                                    @endforeach --}}
                                @endforeach            
                                
                                <div class="btn-group">
                                    <button class="btn btn-primary">@if($ciudad == false) {{trans('web.pais')}} @else {{trans('web.ciudad')}} @endif</button>
                                    <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                    <ul class="dropdown-menu bullet pull-center">
                                        {{-- <li><input type="checkbox" id="pais-all" value=".pais-all" class="all" checked="checked"><label for="pais-all">{{trans('web.todos')}}</label></li> --}}
                                        @foreach(array_unique($paises) as $pais)
                                            <li><input type="checkbox" id="{{str_slug($pais)}}" value=".{{str_slug($pais)}}"><label for="{{str_slug($pais)}}">{{$pais}}</label></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </fieldset>
                    
                            <?php $alojas = array(); ?>
                            <fieldset class="filter-group checkboxes">
                                @foreach($cursos as $curso)
                                    @php
                                        $dataweb = $curso->dataweb->$lang;
                                        $alojas = array_merge( $alojas, explode(', ', $dataweb->alojas) );
                                    @endphp
                                @endforeach

                                <div class="btn-group">
                                    <button class="btn btn-primary">{{trans('web.alojamiento')}}</button>
                                    <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                    <ul class="dropdown-menu bullet pull-center">
                                        {{-- <li><input type="checkbox" id="alojamiento-all" value=".alojamiento-all" class="all" checked="checked"><label for="alojamiento-all">{{trans('web.todos')}}</label></li> --}}
                                        @foreach(array_unique($alojas) as $aloja)
                                            <li><input type="checkbox" id="{{str_slug($aloja)}}" value=".{{str_slug($aloja)}}"><label for="{{str_slug($aloja)}}">{{$aloja}}</label></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </fieldset>

                            @if($ficha->desc_lateral ?? false)
                            <br>
                            @endif
                    
                            <?php $especialidades = array(); ?>
                            @foreach($cursos as $c)
                                @foreach(\VCN\Models\Cursos\CursoEspecialidad::distinct()->where('curso_id',$c->id)->get() as $e)
                                    <?php $especialidades[] = Traductor::getWeb(App::getLocale(), 'Especialidad', 'name', $e->especialidad->id, $e->especialidad->name); ?>
                                @endforeach
                            @endforeach
                            
                            @if(count($especialidades))
                                <fieldset class="filter-group checkboxes">
                                    <div class="btn-group">
                                        <button class="btn btn-primary">{{trans('web.especialidad')}}</button>
                                        <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                        <ul class="dropdown-menu bullet pull-center">
                                            {{-- <li><input type="checkbox" id="especialidades-all" value=".especialidades-all" class="all" checked="checked"><label for="especialidades-all">{{trans('web.todas')}}</label></li> --}}
                                            @foreach(array_unique($especialidades) as $especialidad)
                                                <li><input type="checkbox" id="{{str_slug($especialidad)}}" value=".{{str_slug($especialidad)}}"><label for="{{str_slug($especialidad)}}">{{$especialidad}}</label></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </fieldset>
                            @endif
                    
                            @if(isset($idiomas) && count($idiomas) > 1)
                                <fieldset class="filter-group checkboxes">
                                    <div class="btn-group">
                                        <button class="btn btn-primary">Idioma</button>
                                        <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><span class="caret"></span></button>
                                        <ul class="dropdown-menu bullet pull-center">
                                            {{-- <li><input type="checkbox" id="idioma-all" value=".idioma-all" class="all" checked="checked"><label for="idioma-all">{{trans('web.todos')}}</label></li> --}}
                                            @foreach(array_unique($idiomas) as $idioma)
                                                <li><input type="checkbox" id="{{str_slug($idioma)}}" value=".{{str_slug($idioma)}}"><label for="{{str_slug($idioma)}}">{{trans('web.idiomas.'.$idioma)}}</label></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </fieldset>
                            @endif
                        </form>
                        @endif
                        
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2"></div>            
            <div class="col-sm-10">
                <div class="controls pull-left text-left">
                    <div class="total-cursos"><i class="fa fa-eye"></i> <span></span></div>
                    {{-- <button id="viewcolssmall" class="layout active"><i class="fa fa-th"></i></button>
                    <button id="viewcolsbig" class="layout"><i class="fa fa-th-large"></i></button>
                    <button id="viewlist" class="layout"><i class="fa fa-list"></i></button>
                    <div class="separator"></div>
                    <button id="sortPromo" class="order active"><i class="fa fa-asterisk"></i></button>
                    <button id="sortName" class="order"><i class="fa fa-sort-alpha-asc"></i></button>
                    <button id="sortPais" class="order"><i class="fa fa-map-marker"></i></button> --}}
                <!--
                <div class="col-sm-2 pull-right">
                    <button id="Reset" class="btn-block">ver todos</button>
                </div>
                -->
                </div>
            </div>
        </div>
    </div>
</div>