@extends('web.home2020.baseweb-menu')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')
    <!-- Link Swiper's CSS -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/bs.css" rel="stylesheet">
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/swiper/swiper.min.css" rel="stylesheet">

@stop

@section('container')
    <div class="headerbg">
        <div class="container" id="header" style="position: relative;">
            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-xs-10">
                    <div class="titulo">
                        <h1 class="slogan">
                            <span></span>
                                {{trans('web.contacto')}}
                                <br />
                                <span></span>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="headerbgoverlay"></div>
    </div>

    <main class="cd-main-content">
        <div class="container" id="contenido">
            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="head oficinas">{{trans('web.oficinas')}}</h4>
                        </div>
                        <div class="col-sm-12 contactbig">
                            <br /><br /><br />
                            <p><strong>Studyfuera</strong><br />
                                Montes Urales 455, Piso 1 y 2 <br> 
                                Virreyes, Lomas de Chapultepec, Miguel Hidalgo<br />
                                11000 Ciudad de México, CDMX<br />
                                Tel. 55 4538 2090<br />
                                <a href="mailto:info@studyfuera.com">info@studyfuera.com</a></p>
                        </div>


                        @php
                            $pagina = \VCN\Models\CMS\Pagina::where('name', 'horario')
                                ->where(function ($query) {
                                    return $query
                                            ->where('propietario', 0)
                                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                                })
                                ->first();
                        @endphp

                        @if($pagina)
                            <div class="col-sm-12">
                                <h4 class="head horario">{{trans('web.horario')}}</h4>
                            </div>
                            <div class="col-sm-12 contactbig">
                                {!! Traductor::getWeb(App::getLocale(), 'Pagina', 'contenido', 5, $pagina->contenido) !!}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-sm-4">
                    <h4 class="head">{{trans('web.contacto')}}</h4>
                    <form id="contactform" action="" method="post" class="validateform" name="leaveContact">
                        <div id="sendmessage">
                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                {!! trans('web.mensajegracias') !!}
                            </div>
                        </div>
                        <br /><br /><br />
                        <div class="formlist">
                            <div class="form-group field">
                                <label class="col-md-4 control-label" for="name">{{trans('web.nombre')}} <span>*</span></label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="name" data-rule="maxlen:4" data-msg="Tienes que introducir al menos 4 caracteres" />
                                    <div class="validation"></div>
                                </div>
                            </div>

                            <div class="form-group field">
                                <label class="col-md-4 control-label" for="email">{{trans('web.email')}} <span>*</span></label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="email" data-rule="email" data-msg="Por favor, introduce un email válido" />
                                    <div class="validation"></div>
                                </div>
                            </div>

                            <div class="form-group field">
                                <label class="col-md-4 control-label" for="message">{{trans('web.mensaje')}} <span>*</span></label>
                                <div class="col-md-8">
                                    <textarea class="form-control" rows="6" name="message" data-rule="required" data-msg="Por favor, escribe un mensaje"></textarea>
                                    <div class="validation"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="checkbox" id="lopd" name="lopd" data-rule="required" data-msg="Por favor, acepte">
                                <small>
                                    {!! Form::hidden('_token', Session::token()) !!}
                                    @if(config('app.timezone') == "America/Mexico_City")
                                        Sí, autorizo el uso de mis datos personales de acuerdo con la Política de Privacidad de Studyfuera.
                                    @else
                                        @lang('web.formulario.lopd', ['plataforma'=> ConfigHelper::plataformaApp()])
                                    @endif
                                </small>
                            </div>

                            <div class="form-group field">
                                <div class="col-md-4">
                                    <p class="text-muted obligatorio"><small>*{{trans('web.obligatorio')}}</small></p>
                                </div>
                                <div class="col-md-8">
                                    <input type="button" value="{{trans('web.enviarmensaje')}}" class="btn btn-primary btn-block" id="enviarcontacto" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>

    <!-- Modal -->
    @include('web._partials.plusinfomodal', ['hidden'=> trans('web.contacto')])
    
@stop

@section('extra_footer')
    <!-- Contact validation js -->
    <script>

        $(document).ready(function() {
            "use strict";

            //Contact
            $('#enviarcontacto').click(function(e){

                e.preventDefault();
                
                var f = $('#contactform').find('.formlist'),
                        ferror = false,
                        emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;


                f.find('input:not([type="button"])').each(function(){ // run all inputs

                    var i = $(this); // current input
                    var rule = i.attr('data-rule');

                    if( rule !== undefined ){
                        var ierror=false; // error flag for current input
                        var pos = rule.indexOf( ':', 0 );
                        if( pos >= 0 ){
                            var exp = rule.substr( pos+1, rule.length );
                            rule = rule.substr(0, pos);
                        }else{
                            rule = rule.substr( pos+1, rule.length );
                        }

                        switch( rule ){
                            case 'required':
                                if( i.val()==='' ){ ferror=ierror=true; }
                                break;

                            case 'maxlen':
                                if( i.val().length<parseInt(exp) ){ ferror=ierror=true; }
                                break;

                            case 'email':
                                if( !emailExp.test(i.val()) ){ ferror=ierror=true; }
                                break;

                            case 'checked':
                                if( !i.attr('checked') ){ ferror=ierror=true; }
                                break;

                            case 'regexp':
                                exp = new RegExp(exp);
                                if( !exp.test(i.val()) ){ ferror=ierror=true; }
                                break;
                        }
                        i.next('.validation').html( ( ierror ? (i.attr('data-msg') !== undefined ? i.attr('data-msg') : 'wrong Input') : '' ) ).show('blind');
                    }
                });
                f.find('textarea').each(function(){ // run all inputs

                    var i = $(this); // current input
                    var rule = i.attr('data-rule');

                    if( rule !== undefined ){
                        var ierror=false; // error flag for current input
                        var pos = rule.indexOf( ':', 0 );
                        if( pos >= 0 ){
                            var exp = rule.substr( pos+1, rule.length );
                            rule = rule.substr(0, pos);
                        }else{
                            rule = rule.substr( pos+1, rule.length );
                        }

                        switch( rule ){
                            case 'required':
                                if( i.val()==='' ){ ferror=ierror=true; }
                                break;

                            case 'maxlen':
                                if( i.val().length<parseInt(exp) ){ ferror=ierror=true; }
                                break;
                        }
                        i.next('.validation').html( ( ierror ? (i.attr('data-msg') != undefined ? i.attr('data-msg') : 'wrong Input') : '' ) ).show('blind');
                    }
                });

                f.find('select').each(function(){ // run all selects
                    var i = $(this); // current input
                    var rule = i.attr('data-rule');

                    if( rule !== undefined ){
                        var ierror=false; // error flag for current input
                        var pos = rule.indexOf( ':', 0 );
                        if( pos >= 0 ){
                            var exp = rule.substr( pos+1, rule.length );
                            rule = rule.substr(0, pos);
                        }else{
                            rule = rule.substr( pos+1, rule.length );
                        }

                        switch( rule ){
                            case 'required':
                                if( i.val()==='' ){ ferror=ierror=true; }
                                break;
                        }
                        i.next('.validation').html( ( ierror ? (i.attr('data-msg') != undefined ? i.attr('data-msg') : 'wrong Input') : '' ) ).show('blind');
                    }
                });


                if( ferror ){
                    return false;
                }else{
                    var str = $('#contactform').serialize();
                    console.log(str);
                }


                $.ajax({
                    type: "POST",
                    url: "{{route('web.contacto')}}",
                    data: str,
                    success: function(msg){
                        console.log(msg);
                        $("#sendmessage").addClass("show");
                        $('#contactform')[0].reset();
                        $("#errormessage").ajaxComplete(function(event, request, settings){

                            if(msg == 'OK')	{
                                $("#sendmessage").addClass("show");
                                $('#contactform')[0].reset();

                            }else{
                                $("#sendmessage").addClass("show");
                                var result = msg;
                            }
                            console.log(result);
                            //$(this).html(result);
                        });
                    }
                });

            });

        });
    </script>

    <!-- mapa -->
    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/maps/jquery-jvectormap-1.2.2.min.js"></script>
    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/maps/map_es.js"></script>
    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/contacto.js"></script>

@stop
