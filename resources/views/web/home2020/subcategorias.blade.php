@extends('web.home2020.baseweb-menu')

@section('title')
    {{ConfigHelper::config('nombre')}} - {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) !!}. {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo) !!}
@stop

@section('extra_meta')
    <meta name="DC.title" lang="{{App::getLocale()}}" content="{{ConfigHelper::config('nombre')}} - {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_titulo', $categoria->id, $categoria->seo_titulo) !!}. {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_titulo', $subcategoria->id, $subcategoria->seo_titulo) !!}" />
    <meta name="subject" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_titulo', $categoria->id, $categoria->seo_titulo) !!}. {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_titulo', $subcategoria->id, $subcategoria->seo_titulo) !!}" />
    <meta name="description" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_descripcion', $subcategoria->id, $subcategoria->seo_descripcion) !!}" />
    <meta name="Keywords" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_keywords', $subcategoria->id, $subcategoria->seo_keywords) !!}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')
    {{-- <!-- Color style -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/{{$clase}}.css" rel="stylesheet"> --}}
@stop


@section('container')

    @php

        $ficha = $subcategoria;

        $menor = $ficha->es_menor;
        $cerrado = $ficha->es_cerrado;
        $cursos = $cursos ?? $ficha->cursos;
        $idiomas = explode(',',$ficha->idioma);

        $fcats = $ficha->hijos_filtro;
        $filCats = implode(' ', array_keys($fcats));

        $clasecol = "col-md-10 col-xs-10";
        if($ficha->desc_lateral)
        {
            $clasecol = "col-md-8 col-xs-10";
        }

        $csscolor = $ficha->color_texto ?: "#99B4FF";
        $csscolor .= " !important";
    @endphp
    
    <style>
        #cursos-lista a
        {
            color: {{$csscolor}};
        }
        #cursos-lista a:hover{
            color: {{$csscolor}};
        }
    
        #Filters .btn-group .btn-primary{
            background-color: {{$csscolor}};
        }

        #sidebar-right .box {
            background-color: {{$csscolor}};
        }
        #sidebar-right-mv .box {
            background-color: {{$csscolor}};
        }

        .titulo-sub{
            color: {{$csscolor}};
        }
    </style>

    <div class="headerbg">
        <div class="container" id="header" style="position: relative;">
            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-xs-10">
                    <div class="titulo titulo-cat">
                        <h1 class="slogan">
                            <span>
                                <ul class="breadcrumb">
                                    <li class="active"><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url) !!}">{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) !!}</a></li>
                                </ul>
                            </span>
                            {{-- {{$subcategoria->id}} --}}
                            {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo) !!}
                            <small class="titulo-sub">@if(Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_corta', $subcategoria->id, $subcategoria->desc_corta)){!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_corta', $subcategoria->id, $subcategoria->desc_corta) !!}@endif</small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="headerbgoverlay"></div>
        
        @include('web.home2020._cursos_filters', ['cursos'=> $cursos])

    </div>
    
    <main class="cd-main-content">
        <div class="container" id="contenido">
            <div class="row">
                <div class="col-sm-2 col-xs-2 hidden-xs hidden-xm"></div>
                <div class="col-sm-1 col-xs-1 visible-xs visible-xm"></div>

                <div class="{{$clasecol}} {{$clase}}">
                    
                    @if($subcategoria->descripcion)
                    <div class="introseccion categorias">
                        @if(Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'descripcion', $subcategoria->id, $subcategoria->descripcion)){!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'descripcion', $subcategoria->id, $subcategoria->descripcion) !!}@endif
                    </div>
                    @endif 

                    <!-- Start right sidebar -->
                    @if($subcategoria->desc_lateral)
                    <div class="visible-xs visible-md" id="sidebar-right-mv">
                        <div class="box">
                            {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_lateral', $subcategoria->id, $subcategoria->desc_lateral) !!}
                        </div>
                    </div>
                    @endif
                    <!-- End right sidebar -->
                    
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 categorias">
                            @include('web.home2020._cursos_list', ['cursos'=> $cursos])
                        </div>
                    </div>

                    <h4 class="row">
                        <br>
                        <div class="playfair">
                            {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_cursos', $subcategoria->id, $subcategoria->desc_cursos) !!}
                        </div>
                    </h4>


                </div>
                
                <!-- Start right sidebar -->
                @if($subcategoria->desc_lateral)
                <div class="col-md-2 wrapper-bg {{$clase}}" id="sidebar-right">
                    <div class="box">
                        {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_lateral', $subcategoria->id, $subcategoria->desc_lateral) !!}
                    </div>
                </div>
                @endif
                <!-- End right sidebar -->
            </div>

            {{-- <div class="row">
                @include('web.home2020.includes.copyright')
            </div> --}}
        </div>
    </main>


    <!-- Modal -->
    <?php
        $hidden = Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) ." - ". Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo);
    ?>
    @include('web._partials.plusinfomodal', ['hidden'=> $hidden])

@stop

@section('extra_footer')

    @include('web.home2020._cursos_js')

@stop