<ul>

    {{-- PÁGINAS (menu (principal))--}}
    @foreach(VCN\Models\CMS\Pagina::WhereIn('propietario', [0,ConfigHelper::config('propietario')])->where('menu',1)->get() as $pagina)
        <li><a href="/{!! Traductor::trans('Pagina', 'url', $pagina) !!}.html">{!! Traductor::trans('Pagina', 'titulo', $pagina) !!}</a></li>
    @endforeach

    {{-- @switch(ConfigHelper::config('sufijo'))

        @case("cic")
            
            <li><a href="//viatges.iccic.edu/be-cicviatges/" target="_blank">Be CIC Viatges. CONCURSOS</a></li>
            <li><a href="http://www.landedblog.com/iccic/" target="_blank">{!! trans('web.landedslogan') !!}</a></li>
        
            @break
        
        
        @default
        
            <li><a href="//www.britishsummer.com/be-bs/" target="_blank">Be Bs. Concursos</a></li>
            <li><a href="//www.britishsummer.com/blog-aprender-ingles-extranjero" target="_blank">I Love Brit Blog</a></li>
            <li><a href="//www.britishsummer.com/el-blog-del-verano.html">BLOG DEL VERANO</a></li>
            
    @endswitch --}}
    
    {{-- <p class="mt-4"></p> --}}
    
    {{-- login --}}
    @switch(ConfigHelper::config('sufijo'))

        @case("sf")
            @if (Auth::guest())
                <li><a href="//studyfuera.estudiaryviajar.com/auth/login">{{trans('web.login')}}</a></li>
            @else
                <li><a href="//studyfuera.estudiaryviajar.com/manage">{{trans('web.aclientes')}}</a></li>
                {{-- <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ trans('web.salir') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li> --}}
            @endif
        @break

        @default
            @if (Auth::guest())
                <li><a href="//{{ConfigHelper::config('sufijo')}}.estudiaryviajar.com/auth/login">{{trans('web.login')}}</a></li>
            @else
                <li><a href="//{{ConfigHelper::config('sufijo')}}.estudiaryviajar.com/manage">{{trans('web.aclientes')}}</a></li>
                {{-- <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ trans('web.salir') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li> --}}
            @endif
        @break

    @endswitch

    <li><a class="underline" href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{{trans('web.contacto-slug')}}">{{trans('web.contacto')}}</a></li>

    <?php
        $sufijo = ConfigHelper::config('sufijo');
        $whatsapp = null;
        if($sufijo != "cic")
        {
            $whatsapp = ConfigHelper::config('whatsapp_pre');
            $whatsapp .= ConfigHelper::config('whatsapp');
        }
    ?>

    @if($whatsapp)
        <li><a class="underline" href="https://api.whatsapp.com/send?phone={{$whatsapp}}">Whatsapp</a></li>
    @endif
    

    {{-- idiomas --}}
    @if(isset($weblangs))
        @foreach ($weblangs as $weblang => $linklang)
            @if($weblang != App::getLocale())
                <li><a href="{{$linklang}}" class="language" data-idioma="{{$weblang}}"/><span>{{ConfigHelper::getIdiomaWeb($weblang)}}</span></a></li>
            @endif
        @endforeach
    @else
        @if(App::getLocale() == 'es')
            <li><a href="/ca" class="language" data-idioma="ca"><span>{{ConfigHelper::getIdiomaWeb('ca')}}</span></a></li>
        @elseif(App::getLocale() == 'ca')
            <li><a href="/es" class="language" data-idioma="es"><span>{{ConfigHelper::getIdiomaWeb('es')}}</span></a></li>
        @endif
    @endif
    
    @if($responsive ?? false)
        <li><a class="underline" href="/">Home</a></li>
    @endif

    {{-- @if(ConfigHelper::config('chat'))
        <li class="hidden-xs hidden-sm">
            @include('chat.web')
        </li>
    @else
    
        @if(ConfigHelper::config('propietario') == 1)
            @if(App::getLocale() == 'es')
                <li class="hidden-xs hidden-sm"><a href="//www.britishsummer.com/webim/client.php?locale=sp&style=britishsummer" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('//www.britishsummer.com/webim/client.php?locale=sp&amp;style=britishsummer&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=800,height=600,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;">{{trans('web.chat')}}</a></li>
            @else
                <li class="hidden-xs hidden-sm"><a href="//www.britishsummer.com/webim/client.php?locale={{App::getLocale()}}&style=britishsummer" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('//www.britishsummer.com/webim/client.php?locale={{App::getLocale()}}&amp;style=britishsummer&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=800,height=600,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;">{{trans('web.chat')}}</a></li>
            @endif
        @endif

    @endif --}}

</ul>