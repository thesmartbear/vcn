<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @section('title')
            {{ConfigHelper::config('nombre')}}
        @show
    </title>

    @yield('extra_meta')

    @php
        $tema = ConfigHelper::config('tema');
    @endphp

    @if(isset($weblangs))
        @foreach ($weblangs as $weblang => $linklang)
            <link rel="alternate" hreflang="{{$weblang}}" href="{{$linklang}}" />
        @endforeach
    @endif

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100,400,700,700italic,400italic' rel='stylesheet' type='text/css'>

    <!-- ==============================================
		Favicons
		=============================================== -->
    <link rel="shortcut icon" href="/assets/{{$tema}}/favicon/favicon{{ConfigHelper::config('sufijo')}}.ico">
    <link rel="apple-touch-icon" href="/assets/{{$tema}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/{{$tema}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/{{$tema}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}-114x114.png">


    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" />
    
    @php
        $assets = "/assets/home2020/welcome";
    @endphp
    <link rel="stylesheet" href="{{$assets}}/scss/build/menu.css">

    <!-- Font -->
    <link href='//fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css' defer>
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css' defer>
    <link href='//fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css' defer>
    <link href="/assets/{{$tema}}/css/bs2015.css" rel="stylesheet" type="text/css" defer />

    <link rel="stylesheet" media="all" href="/assets/{{$tema}}/css/jquery-jvectormap.css" defer/>


    <!-- font awesome -->
    {{-- <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> --}}
    {{-- {!! Html::style('assets/font-awesome/css/font-awesome.min.css') !!} --}}
    {{-- <link href="/assets/fontawesome/css/all.css" rel="stylesheet" defer>  --}}


    <link href="/assets/{{$tema}}/css/bootstrap-select.css" rel="stylesheet" type="text/css" defer />

    <link rel="stylesheet" href="/assets/{{$tema}}/css/magnific-popup.css"> <!-- Resource style -->
    {{-- <link rel="stylesheet" href="/assets/{{$tema}}/css/navstyle.css"> <!-- Resource style --> --}}


    <link rel="stylesheet" href="/assets/{{$tema}}/css/webstyle.css" defer>
    <link rel="stylesheet" href="/assets/{{$tema}}/css/adapt2020.css" defer>
    
    <link rel="stylesheet" href="/assets/outdatedbrowser/outdatedbrowser.min.css">
    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/assets/{{$tema}}/css/dropdowns-enhancement.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'user' => Auth::user(),
            'csrfToken' => csrf_token(),
            'vapidPublicKey' => config('webpush.vapid.public_key'),
            'pusher' => [
                'key' => config('broadcasting.connections.pusher.key'),
                'cluster' => config('broadcasting.connections.pusher.options.cluster'),
            ],
        ]) !!};
    </script>
    
    @yield('extra_head')

    @stack('css_extra')

    {!! ConfigHelper::config('code_header') !!}

</head>
<body>

    @include('web._partials.google_body')

    {{-- SCROLL TO TOP --}}
    <div id="scrollToTop">
        <i class="fal fa-arrow-up"></i>
    </div>

    {{-- MENU start --}}
    {{-- MENU btn responsive --}}
    <div class="menuBtnsContainer visible-xs visible-sm visible-md hidden-lg">
        <div class="menuTxt">
            <i id="close" class="fal fa-times"></i>
            @lang("web.home.menu")
        </div>
    </div>

    {{-- MENU responsive --}}
    @include('web.home2020._menu-responsive', ['logo'=> false, 'div'=> 'hidden hidden-lg'])
    
    {{-- MENU big --}}
    @include('web.home2020._menu', ['logo'=> true, 'div'=> 'hidden-xs hidden-sm hidden-md visible-lg'])
    {{-- MENU end --}}

    {{-- @section('header')
    <header class="cd-main-header">
    </header>
    @show --}}

    @yield('container')

    <div class="footer-wrapper">
        @include("web.home2020._footer")
    </div>

    {{-- <div class="cd-overlay"></div> --}}

    {{-- @include('web.home2020.includes.menu'.ConfigHelper::config('sufijo')) --}}

    {{-- <div id="outdated"></div> --}}
    @if(Session::get('redirected') == true)
    <!-- Modal -->
    <div class="modal fade" id="redirected" tabindex="-1" role="dialog" aria-labelledby="redirected" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img class="modal-logo" src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" />
                </div>
                <div class="modal-body">
                    <div id="respuesta">
                        <h2>Oops!</h4>
                            <div class="row">
                                <div class="col-sm-12">
                                    El programa que estabas buscando ya no existe, pero te ofrecemos una selección que puede interesarte.
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal" type="button">cerrar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endif

    <script src="/assets/{{$tema}}/js/modernizr.js"></script> <!-- Modernizr -->

    <script src="/assets/outdatedbrowser/outdatedbrowser.min.js"></script>

    <script src='https://code.jquery.com/jquery-2.1.1.min.js' type='text/javascript'></script>
    <script src='https://code.jquery.com/jquery-migrate-1.2.1.min.js' type='text/javascript'></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <!-- detect mobile -->
    <script src="/assets/{{$tema}}/js/detectmobilebrowser.js" type="text/javascript"></script>

    <script type="text/javascript" src="/assets/{{$tema}}/js/bootstrap-select.js"></script>

    <script src="/assets/{{$tema}}/js/jquery.mobile.custom.min.js"></script>
    {{-- <script src="/assets/{{$tema}}/js/main.js"></script> <!-- navigation --> --}}

    <script src="/assets/{{$tema}}/js/jquery.magnific-popup.min.js"></script>

    @include('web._partials.cookies2020')
    {{-- @include('web._partials.cookies') --}}
    @include('web._partials.google_head')

    @yield('extra_footer')

    <script type="text/javascript">

        $(window).scroll(function() {
            var height = $(window).scrollTop();
            if (height > 1000) {
                $('#scrollToTop').fadeIn();
            } else {
                $('#scrollToTop').fadeOut();
            }
        });

        $("#scrollToTop").click(function(event) {
            event.preventDefault();
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        });

        @if(Session::get('redirected') == true)
        $('#redirected').modal('show');
        @endif


        $('.language').on('click', function(e){
            e.preventDefault();
            var idioma = $(this).data('idioma');
            var segments = $(this).attr('href').split('{{ConfigHelper::config('web')}}');
            var codigo = segments[1];
            console.log(segments);
            console.log(codigo);

            if(codigo.includes('/es') || codigo.includes('/ca')) {
                $(location).attr('href', $(this).attr('href'));
            }else{
                var url = segments[0] + '{{ConfigHelper::config('web')}}/' + idioma + segments[1];
                $(location).attr('href', url);

            }
        });
    </script>

    <script src="https://kit.fontawesome.com/bfb19d8d13.js" crossorigin="anonymous"></script>
    {{-- <script src="{{mix('/js/app.js')}}"></script> --}}

    
    @stack('scripts')

    {!! ConfigHelper::config('code_footer') !!}

</body>
</html>