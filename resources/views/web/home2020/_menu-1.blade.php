<p class="mt-5 menu-m-top"></p>
<ul>

    @php
        $menuPrincipal = \VCN\Models\CMS\CategoriaWeb::arbol('menu_principal');
    @endphp

    {{-- 3 NIVELES --}}

    @foreach($menuPrincipal as $menucat)
        @if(isset($menucat['subcategorias']) && count($menucat['subcategorias']))
            <li class="menu-item item-has-children" data-id="{{ $menucat['id'] }}">
                <a {!! $menucat['enlace'] !!}>
                    {{-- {{ $menucat['id'] }}: --}}
                    {!!Traductor::getTrans('CategoriaWeb', 'titulo', $menucat['id'], $menucat['titulo'])!!}
                </a>
                <ul class="sub-menu ml-3" style="display:none;">
                    @foreach($menucat['subcategorias'] as $subcat)
                        <li>
                            <a {!! $subcat['enlace'] !!}>
                                {{-- {{ $subcat['id'] }}: --}}
                                @if($subcat['titulo'] != '')
                                    {!!Traductor::getTrans('CategoriaWeb', 'titulo', $subcat['id'], $subcat['titulo'])!!} 
                                @else
                                    {!!Traductor::getTrans('CategoriaWeb', 'name', $subcat['id'], $subcat['name'])!!}
                                @endif
                            </a>

                            {{-- 3r nivel --}}
                            @if(isset($subcat['subcategoriasdetalle']) && count($subcat['subcategoriasdetalle']))
                            <ul class="sub-menu2 ml-3">
                            @foreach($subcat['subcategoriasdetalle'] as $subcat2)
                                <li>
                                    <a {!! $subcat2['enlace'] !!}>
                                        {{-- {{ $subcat2['id'] }}: --}}
                                        @if($subcat2['titulo'] != '')
                                            _{!!Traductor::getTrans('CategoriaWeb', 'titulo', $subcat2['id'], $subcat2['titulo'])!!} 
                                        @else
                                            _{!!Traductor::getTrans('CategoriaWeb', 'name', $subcat2['id'], $subcat2['name'])!!}
                                        @endif
                                    </a>
                                </li>
                            @endforeach
                            </ul>
                            @endif

                        </li>
                    @endforeach
                    
                    @if($responsive ?? false)
                        <li><a {!! $menucat['enlace'] !!}>@lang("web.categorias.vertodos")</a></li>
                    @endif
                </ul>
            </li>
        @else
            <li class="menu-item" data-id="{{ $menucat['id'] }}">
                <a {!! $menucat['enlace'] !!}>
                    {!!Traductor::getTrans('CategoriaWeb', 'titulo', $menucat['id'], $menucat['titulo'])!!}
                </a>
            </li>
        @endif
    @endforeach

</ul>