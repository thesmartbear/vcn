<div id="programas">
    <div id="cursos-lista">
        @if(!count($cursos))
            <div class="fail-message"><span>{{trans('web.sinresultados')}}</span></div>
        @endif
        <div class="slides">
            @foreach($cursos as $curso)
                
                @php
                $lang = App::getLocale();
                $dataweb = $curso->dataweb->$lang;

                $plazasrestantes = $curso->dataweb->plazas ?? null;

                $fotoscentro = $dataweb->fotos_centro;
                $fotoscurso = $dataweb->fotos_curso;

                $idiomas = $idiomas ?? [];

                $cerrado = $cerrado ?? false;
                $categoria = $categoria ?: $curso->catweb;

                $cursolink = $dataweb->cursolink;
                $homelink = $categoria->home_link;

                $csscolor = $dataweb->pais_color ?? "#99B4FF";
                $csscolor = "color: $csscolor !important;";

                $idiomascurso = "";
                if(count($idiomas) > 1)
                {
                    $idiomasc = explode(',', $curso->course_language);
                    foreach($idiomasc as $idi)
                    {
                        $idiomascurso = str_slug($idi) . " ";
                    }
                }
                @endphp
            
                <div class="mix {{$dataweb->mix}} {{$dataweb->espes}} {{$dataweb->alojascurso}} {{$dataweb->edades}} {{$dataweb->catsweb}} {{$idiomascurso}}" data-promo="{{$curso->course_promo}}" data-name="{{$curso->course_slug}}" data-pais="{{str_slug($dataweb->pais)}}">
                    
                    {{-- @if($curso->course_promo == 1)
                        <div class="promo-lista">
                            <i class="fa fa-asterisk"></i>
                        </div>
                    @endif --}}
                    
                    {{-- <div class="fotocurso" style="{{$dataweb->web_style_bg}}">
                        <a href="{{$homelink}}/{{$cursolink}}" class="foto"></a>
                    </div> --}}

                    <div class="fichacurso">
                        <h4 class="nombrecurso">
                            <div class="row">
                                <small class="col-sm-12 curso-pais">
                                    @if($dataweb->pais != 'España') {{$dataweb->pais}} @else {{$dataweb->ciudad}} @endif
                                </small>

                                @if($curso->category_id == 2)
                                    @if($plazasrestantes)
                                        
                                        @if($plazasrestantes <= 0 && $cerrado != 1)
                                            <small class="col-sm-6 pull-right text-right grupocerrado plazas">{{trans('web.grupocerrado')}}</small>
                                        @elseif($plazasrestantes != 0 && $plazasrestantes < 6 && $cerrado != 1)
                                            <small class="col-sm-6 pull-right text-right ultimasplazas plazas">{{trans('web.ultimasplazas')}}</small>
                                        @endif

                                    @endif
                                @endif
                            </div>
                            <a href="{{$homelink}}/{{$cursolink}}" style="{{$csscolor}}">
                                {{-- {{$curso->id}} --}}
                                {{$dataweb->name}}
                            </a>
                        </h4>

                        <p class="curso-edades" style="{{$csscolor}}">{!! $dataweb->course_age_range !!}</p>

                        {{-- <div class="separator-ficha"></div> --}}

                        <p class="curso-alojas" style="{{$csscolor}}">{{$dataweb->alojas}}</p>

                            @if( $dataweb->especialidades )
                                @php
                                    $especialidades = (array) $dataweb->especialidades;
                                    $subespecialidades = (array) $dataweb->subespecialidades;
                                @endphp

                                <div class="curso-especialidades"><span style="{{$csscolor}}">

                                    <?php
                                        $anteriorespe = '';
                                        $i = 1;
                                    ?>

                                    @foreach($especialidades as $eid => $e)

                                        @if($eid != $anteriorespe || $anteriorespe == null)
                                            {!! $e !!}:
                                        @endif

                                        @if(($eid == $anteriorespe || $i == 1) && count($especialidades) != $i)
                                            {!! $dataweb->subespecialidades->$eid !!},
                                        @endif

                                        @if(count($especialidades) == $i)
                                            {!! $dataweb->subespecialidades->$eid !!}.<br />
                                        @endif

                                        <?php
                                            $anteriorespe = $eid;
                                            $i++;
                                        ?>

                                    @endforeach

                                </span></div>
                            @endif
                    </div>

                </div>
            
            @endforeach 

            <div class="gap"></div>
            <div class="gap"></div><div class="gap"></div>
        </div>
    </div>
</div>