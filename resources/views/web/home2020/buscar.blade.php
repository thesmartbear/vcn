@extends('web.home2020.baseweb-menu')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')
        <!-- Color style -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/{{$clase}}.css" rel="stylesheet">
<link href="/assets/{{ConfigHelper::config('tema')}}/css/dropdowns-enhancement.css" rel="stylesheet">
@stop


@section('container')
    <div class="headerbg">
        <div class="container" id="header" style="position: relative;">
            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-xs-10">
                    <div class="titulo">
                        <h1 class="slogan">
                            <span></span>
                            {{trans('web.hasbuscado')}} {{$search}}
                            <small></small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="headerbgoverlay"></div> --}}

        @include('web.home2020._cursos_filters', ['cursos'=> $cursos])

    </div>
    
    <main class="cd-main-content">
        <div class="container" id="contenido">
            <div class="row">
                <div class="col-sm-2 col-xs-2 hidden-xs hidden-xm"></div>
                <div class="col-sm-1 col-xs-1 visible-xs visible-xm"></div>
    
                @php
                    $clasecol = "col-md-10 col-xs-10";
                @endphp
                <div class="{{$clasecol}} {{$clase}}">
    
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 categorias">
                            @include('web.home2020._cursos_list', ['cursos'=> $cursos, 'categoria'=> null])
                        </div>
                    </div>
    
                </div>
                
            </div>
        </div>
    </main>

@stop

@section('extra_footer')

    @include('web.home2020._cursos_js')

@stop



