<script>
    //=== POSICION Y COLOR BG DIVS
    
    //compobar q un div es visible
    function elementVisible(element) {
        var visible = true;

        var element2 =  $(".menuLeft");
        
        var windowTop = $(document).scrollTop();
        var windowBottom = windowTop + window.innerHeight;
        
        var margen = element2.height() / 2;

        var elementPositionTop = element.offset().top + margen;
        var elementPositionBottom = elementPositionTop + element.height();
        
        if (elementPositionTop >= windowBottom || elementPositionBottom <= windowTop) {
            visible = false;
        }
        return visible;
    }

    //comprobar q element1 esta encima de element2
    function elementHover(element1, element2) {
        var hover = false;
        
        var windowTop = $(document).scrollTop();
        var windowBottom = windowTop + window.innerHeight;
        
        var element1PositionTop = element1.offset().top;
        var element1PositionBottom = element1PositionTop + element1.height();

        var margen = element1.height() / 2;
        
        var element2PositionTop = element2.offset().top + margen;
        var element2PositionBottom = element2PositionTop + element2.height();
        
        if( element1PositionBottom > element2PositionTop )
        {
            hover = true
        }
        
        if( element1PositionTop > element2PositionTop && element1PositionTop < element2PositionBottom )
        {
            hover = true
        }

        if( element1PositionTop > element2PositionBottom )
        {
            hover = false
        }
        
        return hover;
    }

    function getBackgroundColor($el) {
        var bgColor = "#ffffff";
        while($el[0].tagName.toLowerCase() != "html") {
            bgColor = $el.css("background-color");
            if(bgColor != "rgba(0, 0, 0, 0)" && bgColor != "transparent") {
            break;
            }
            $el = $el.parent();
        }
        
        return bgColor;
    }

    function getBrightness(element) {

        var r,g,b,brightness,
            colour = getBackgroundColor(element);

        if (colour.match(/^rgb/)) {
        colour = colour.match(/rgba?\(([^)]+)\)/)[1];
        colour = colour.split(/ *, */).map(Number);
        r = colour[0];
        g = colour[1];
        b = colour[2];
        } else if ('#' == colour[0] && 7 == colour.length) {
        r = parseInt(colour.slice(1, 3), 16);
        g = parseInt(colour.slice(3, 5), 16);
        b = parseInt(colour.slice(5, 7), 16);
        } else if ('#' == colour[0] && 4 == colour.length) {
        r = parseInt(colour[1] + colour[1], 16);
        g = parseInt(colour[2] + colour[2], 16);
        b = parseInt(colour[3] + colour[3], 16);
        }

        brightness = (r * 299 + g * 587 + b * 114) / 1000;
        return brightness;
    }

    function esBrillante(brillo)
    {
        let esBrillante = true;
        if (brillo < 125) 
        {
            //oscuro
            esBrillante = false;
        }

        return esBrillante;
    }

    function getBrilloBloque($elm)
    {
        let brillo = 0;

        color = getBackgroundColor($elm)
        // console.log(color)
        brillo = getBrightness($elm)
        // console.log(brillo)

        return brillo;
    }

    // ===== init menu-js =====

    let $color = "white";
    const $css = "color";
    const $menu = ".menuLeft a";

    const $menuColor = $($menu).css($css);
    var bloques = [];

    $ml = ".menuLeft";
    $mlz = $($ml).css('z-index');

    function initBloques()
    {
        $(".bloqueDiv").each( function( index ) {

            let brillo = 0;
            let $elmId = $(this).attr('id');

            let b = getBrilloBloque( $(this) );

            bloques[$elmId] = esBrillante(b);

        });

        // console.log(bloques);

        //intro : si no ha movido el ratón
        let bintro = bloques["bloque-intro"];
        $($menu).css($css, (bintro ? $menuColor : $color ));
        $(".menuLeft .fa-search").css($css, (bintro ? $menuColor : $color ));
        
    }

    initBloques();
    $(".menuLeft").css('transition', "none");

    // ===== scroll =====
    $(window).on('scroll resize', function () {

        $(".bloqueDiv").each( function( index )
        {
            let $elm = $(this);

            /* if( elementVisible($elm) )
            {
                let $bid = $elm.attr('id');
                let b = bloques[$bid];
                // console.log("Brillo: " + b)

                $($menu).css($css, (b ? $menuColor : $color ));
                $(".menuLeft .fa-search").css($css, (b ? $menuColor : $color ));
            } */

            if( elementHover( $('.menuLeft'), $elm ) )
            {
                let $bid = $elm.attr('id');
                let b = bloques[$bid];

                // console.log($bid);

                $($menu).css($css, (b ? $menuColor : $color ));
                $(".menuLeft .fa-search").css($css, (b ? $menuColor : $color ));
            }

            //z-index
            var wt = $(document).scrollTop();
            if(wt > 4500)
            {
                $($ml).addClass('menu-z');
            }
            else
            {
                $($ml).removeClass('menu-z');
            }

            //footer
            $elm = $("footer");
            // let bfooter = elementVisible($elm);
            let bfooter = elementHover( $('.menuLeft'), $elm );
            if(bfooter)
            {
                $($menu).css($css, (bfooter ? $color : $menuColor ));
                $(".menuLeft .fa-search").css($css, (bfooter ? $color : $menuColor ));
            }

        });

    });

</script>