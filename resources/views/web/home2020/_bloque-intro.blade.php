@if($intro->color_fondo ?? false)
<style>
    .colorBg-{{$bloqueId}} {
        background-color: {{ $intro->color_fondo }} !important;
    }
</style>
@endif

<style>
    .bloque-intro-wrap {
        position: relative;
        display: none;
        z-index: 99;
        float: left;
    }
    .bloque-intro-img {
        position: absolute;
        bottom: 22px;
        width: 320px;
        min-height: 320px;
        height: auto;
    }
</style>

<div class="container-fluid container1 bloqueDiv introBloque colorBg-{{$bloqueId}}" id="bloque-{{$bloqueId}}">
    <div class="row no-gutters">

        {{-- @foreach($menuPrincipal as $bloque)
            
            @php
                $img = $bloque->home_imagen_webp ?: "$assets/assets/imgs/travelbags.jpg";
            @endphp

            <style>
                #intro-{{$bloque->id}} {
                    background: url({!! $img !!});
                    
                }
            </style>

            <div class="introDiv animate__animated animate__fadeInRight" id="intro-{{$bloque['id']}}">
                <h2>intro-{{$bloque['id']}}</h2>
            </div>
        @endforeach --}}

        <div class="col-1 col-lg-3"></div>
        
        <div class="col-10 col-lg-8 blockText">
            
            <div class="txtSecondColor mb-5 pb-3">
                <p class="introTitle">
                    {!! $intro ? Traductor::getTrans('CategoriaWeb', 'home_titulo', $intro->id, $intro->home_titulo) : "" !!}
                </p>
            </div>      
            <div class="mt-5 text-wrap">
                
                {{-- MENU principal --}}
                @foreach($menuPrincipal as $menucat)

                    @php
                        $titulo = Traductor::getTrans('CategoriaWeb', 'titulo', $menucat->id, $menucat->titulo);
                        $img = $menucat->home_imagen_webp ?: "$assets/assets/imgs/travelbags.jpg";
                        // $img = "$assets/assets/imgs/travelbags.jpg";
                        // $wl = strlen($titulo);
                        // $wl = $wl > 800 ? 120 : $wl;
                    @endphp

                    <p> 
                        <a id="intro-txt-{{$loop->iteration}}" data-id="{{ $menucat->id }}" data-loop="{{ $loop->iteration }}" class="introTxt txtPrimaryColor ffRegular" {!! $menucat->home_enlace !!}>
                            {!! $titulo !!}
                        </a>

                        <div class="bloque-intro-wrap" id="bloque-intro-{{$loop->iteration}}">
                            <div class="bloque-intro-img bloque-intro-img-{{$loop->iteration}} animate__animated animate__pulse"
                                style="background: url({!! $img !!}) no-repeat left bottom; background-size: contain;"
                                >
                            </div>
                        </div>

                    </p>
                    
                @endforeach
                
            </div>
        </div>

        <div class="d-none d-lg-block col-lg-1"></div>
    </div>
</div>

{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
<style>
    .introDiv {
        display: none;
        position: absolute;
        z-index: 1999;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
</style> --}}

@push('scripts')
<script>
    $(document).ready(function(){
        $(".introTxt").hover( function(e) {
            // $('.introDiv').hide();
            // let catid = $(this).data('id');
            // let div = "#intro-" + catid
            
            let lid = $(this).data('loop');
            let div = "#bloque-intro-" + lid
            let divTxt = "#intro-txt-" + lid

            $(".introTxt").removeClass('txtThirdColor ffSemiBold').addClass('txtPrimaryColor ffRegular');
            $(divTxt).removeClass('txtPrimaryColor ffRegular').addClass('txtThirdColor ffSemiBold');
            
            let wt = $(divTxt).width();
            // wt = wt + "px !important";
            $(div).css("left", wt + 32);
            $(".bloque-intro-wrap").hide();

            $(div).show();


        }, function() { //mouseout
            // $('.introDiv').hide();
            // $(".introTxt").removeClass('txtThirdColor ffSemiBold').addClass('txtPrimaryColor ffRegular');

            $(".bloque-intro-wrap").hide();
        });
        // $(".introTxt").first().trigger("mouseover");
    });
</script>
@endpush