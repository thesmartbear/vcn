<script src="https://cdnjs.cloudflare.com/ajax/libs/color-thief/2.3.0/color-thief.umd.js"></script>
<style>
    .color-thief-image {
        width: 50px;
        height: 50px;
        position: fixed;
        top: 0;
        z-index: 999;
        overflow: hidden;
        display: none;
    }
</style>
<img id="colorThiefImg" class="color-thief-image" src="">

<script>
    //=== POSICION Y COLOR BG DIVS
    
    //compobar q un div es visible
    function elementVisible(element) {
        var visible = true;
        
        var windowTop = $(document).scrollTop();
        var windowBottom = windowTop + window.innerHeight;
        var elementPositionTop = element.offset().top;
        var elementPositionBottom = elementPositionTop + element.height();
        
        if (elementPositionTop >= windowBottom || elementPositionBottom <= windowTop) {
            visible = false;
        }
        return visible;
    }

    //comprobar q element1 esta encima de element2
    function elementHover(element1, element2) {
        var hover = false;
        
        var windowTop = $(document).scrollTop();
        var windowBottom = windowTop + window.innerHeight;
        
        var element1PositionTop = element1.offset().top;
        var element1PositionBottom = element1PositionTop + element1.height();

        var element2PositionTop = element2.offset().top;
        var element2PositionBottom = element2PositionTop + element2.height();
        
        if( element1PositionBottom > element2PositionTop )
        {
            hover = true
        }
        
        if( element1PositionTop > element2PositionTop && element1PositionTop < element2PositionBottom )
        {
            hover = true
        }

        if( element1PositionTop > element2PositionBottom )
        {
            hover = false
        }
        
        return hover;
    }

    function getBackgroundColor($el) {
        var bgColor = "#ffffff";
        while($el[0].tagName.toLowerCase() != "html") {
            bgColor = $el.css("background-color");
            if(bgColor != "rgba(0, 0, 0, 0)" && bgColor != "transparent") {
            break;
            }
            $el = $el.parent();
        }
        
        return bgColor;
    }

    function getBrightness(element) {

        var r,g,b,brightness,
            colour = getBackgroundColor(element);

        if (colour.match(/^rgb/)) {
        colour = colour.match(/rgba?\(([^)]+)\)/)[1];
        colour = colour.split(/ *, */).map(Number);
        r = colour[0];
        g = colour[1];
        b = colour[2];
        } else if ('#' == colour[0] && 7 == colour.length) {
        r = parseInt(colour.slice(1, 3), 16);
        g = parseInt(colour.slice(3, 5), 16);
        b = parseInt(colour.slice(5, 7), 16);
        } else if ('#' == colour[0] && 4 == colour.length) {
        r = parseInt(colour[1] + colour[1], 16);
        g = parseInt(colour[2] + colour[2], 16);
        b = parseInt(colour[3] + colour[3], 16);
        }

        brightness = (r * 299 + g * 587 + b * 114) / 1000;
        return brightness;
    }

    function getBrightnessFromColor(array)
    {
        r = array[0]
        g = array[1]
        b = array[2]
        
        brightness = (r * 299 + g * 587 + b * 114) / 1000;
        return brightness;
    }

    function esBrillante(brillo)
    {
        let esBrillante = true;
        if (brillo < 125) 
        {
            //oscuro
            esBrillante = false;
        }

        return esBrillante;
    }

    function getBgImage(element)
    {
        var bg_url = element.css('background-image');
        bg_url = /^url\((['"]?)(.*)\1\)$/.exec(bg_url);
        bg_url = bg_url ? bg_url[2] : "";

        return bg_url;
    }

    function getBrilloBloque($elm)
    {
        let brillo = 0;

        let $bgi = null
        let $elmimg = $elm.find('.imgBg')
        if( $elmimg.length )
        {
            $bgi = getBgImage($elmimg)
        }
        else if($elm.hasClass('imgBg'))
        {
            $bgi = getBgImage($elm)
        }
        
        //tipo de fondo
        if($bgi)
        {
            //tipo: imagen de fondo
            // console.log($bgi);

            $('#colorThiefImg').prop('src', $bgi);
            const img = document.querySelector('#colorThiefImg');
            if(img)
            {
                try {
                    color = colorThief.getColor(img);
                    // console.log(color)
                    brillo = getBrightnessFromColor(color)
                    // console.log(brillo)
                }
                catch(err)
                {
                    // console.log(err)
                    console.log(img);
                    brillo = 0;
                }

            }
        }
        else
        {
            //tipo: slider :: init??? : como va cambiando de foto habría q mirar la q está activa???

            let $elmsli = $elm.find('.slider');
            if( $elmsli.length )
            {
                const img = document.querySelector('.slick-current img');
                color = colorThief.getColor(img);
                // console.log(color)
                brillo = getBrightnessFromColor(color)
                // console.log(brillo)
            }
            else
            {
                //tipo: color de fondo

                color = getBackgroundColor($elm)
                // console.log(color)
                brillo = getBrightness($elm)
                // console.log(brillo)
            }
        }

        return brillo;
    }

    // ===== init menu-js =====

    let $color = "white";
    const $css = "color";
    const $menu = ".menuLeft a";

    const $menuColor = $($menu).css($css);
    const colorThief = new ColorThief();

    var bloques = [];

    function initBloques()
    {
        $(".bloqueDiv").each( function( index ) {

            let brillo = 0;
            let $elmId = $(this).attr('id');

            let b = getBrilloBloque( $(this) );

            bloques[$elmId] = esBrillante(b);

        });

        // console.log(bloques);

        //intro : si no ha movido el ratón
        let bintro = bloques["bloque-intro"];
        if(!bintro)
        {
            $($menu).css($css, $color);
            $(".menuLeft .fa-search").css($css, $color);
        }
        else
        {
            $($menu).css($css, $menuColor);
            $(".menuLeft .fa-search").css($css, $menuColor);
        }
    }

    initBloques();
    $(".menuLeft").css('transition', "none");

    // ===== scroll =====
    $(window).on('scroll resize', function () {

        $(".bloqueDiv").each( function( index )
        {
            let $elm = $(this);

            if( elementVisible($elm) )
            {
                let $bid = $elm.attr('id');
                let b = bloques[$bid];
                // console.log("Brillo: " + b)
                if(!b)
                {
                    $($menu).css($css, $color);
                    $(".menuLeft .fa-search").css($css, $color);
                }
                else
                {
                    $($menu).css($css, $menuColor);
                    $(".menuLeft .fa-search").css($css, $menuColor);
                }
            }

            //footer
            $elm = $("footer");
            if( elementVisible($elm) )
            {
                $($menu).css($css, $color);
            }
            
            // if( elementHover( $('.menuLeft'), $elm ) )
            // {
            //     $color = esBrillante($elm) ? "red" : "yellow";
            // }

        });

    });

</script>