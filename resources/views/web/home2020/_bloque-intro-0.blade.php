@php
    $menuPrincipal = \VCN\Models\CMS\CategoriaWeb::arbolPadres('menu_principal');
    $intro = \VCN\Models\CMS\CategoriaWeb::where('name','home-intro')->first() ?: $menuPrincipal->first();
@endphp 

@if($intro->color_fondo)
<style>
    .colorBg-{{$bloqueId}} {
        background-color: {{ $intro->color_fondo }} !important;
    }
</style>
@endif

<div class="container-fluid container1 bloqueDiv introBloque colorBg-{{$bloqueId}}" id="bloque-{{$bloqueId}}">
    <div class="row no-gutters">

        @foreach($menuPrincipal as $bloque)
            
            @php
                $img = $bloque->home_imagen_webp ?: "$assets/assets/imgs/travelbags.jpg";
            @endphp

            <style>
                #intro-{{$bloque->id}} {
                    background: url({!! $img !!});
                    background-size: cover;
                    background-repeat: no-repeat;
                }
            </style>

            <div class="introDiv animate__animated animate__fadeInRight" id="intro-{{$bloque['id']}}">
                {{-- <div class="row no-gutters">
                    <div class="col-1 col-lg-3"></div>
                    <div class="col-10 col-lg-8 blockText">
                        <p>{!! $bloque->getTranslate('home_titulo2') !!}</p>
                    </div>
                </div> --}}
            </div>
        @endforeach

        <div class="col-1 col-lg-3"></div>
        
        <div class="col-10 col-lg-8 blockText">
            
            <div class="txtSecondColor mb-5 pb-3">
                {{-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et, aperiam ea cumque deleniti consequatur aliquam est, ratione temporibus vel laboriosam.</p> --}}
                <p class="introTitle">
                    {{-- {!! $intro ? $intro->home_titulo : "" !!} --}}
                    {!! $intro ? Traductor::getTrans('CategoriaWeb', 'home_titulo', $intro->id, $intro->home_titulo) : "" !!}
                </p>
            </div>      
            <div class="mt-5 text-wrap">
                
                {{-- MENU principal --}}
                @foreach($menuPrincipal as $menucat)
                        
                    <p>
                        <a id="intro-txt-{{$menucat->id}}" data-id="{{ $menucat->id }}" class="introTxt txtPrimaryColor ffRegular" {!! $menucat->home_enlace !!}>
                            {!!Traductor::getTrans('CategoriaWeb', 'titulo', $menucat->id, $menucat->titulo)!!}
                        </a>
                    </p>
                    
                @endforeach
                
            </div>
        </div>

        <div class="d-none d-lg-block col-lg-1"></div>
    </div>
</div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
<style>
    .introBloque {
        min-height: 760px;
    }

    .introDiv {
        display: none;
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
</style>