@extends('web.home2020.baseweb-menu')

@section('title')
    {{ConfigHelper::config('nombre')}} - {{$curso->course_seo_title}}
@stop


@section('extra_meta')
    <meta name="DC.title" lang="{{App::getLocale()}}" content="{{$curso->course_seo_title}}" />
    <meta name="Subject" lang="{{App::getLocale()}}" content="{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo)!!} > {!!Traductor::getWeb(App::getLocale(), 'Subcategoria', 'name_web', $curso->subcategoria->id, $curso->subcategoria->name_web)!!} > @if($curso->subcategoria_detalle){!!Traductor::getWeb(App::getLocale(), 'SubcategoriaDetalle', 'name_web', $curso->subcategoria_detalle->id, $curso->subcategoria_detalle->name_web)!!} > @endif{!!Traductor::getWeb(App::getLocale(), 'Curso', 'name', $curso->id, $curso->name)!!}" />
    <meta name="Description" lang="{{App::getLocale()}}" content="{{$curso->course_seo_description}}" />
    <meta name="Keywords" lang="{{App::getLocale()}}" content="@if($curso->course_seo_tags != ''){{$curso->course_seo_tags}}@else{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}@endif" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop


@section('extra_head')
<!-- Color style -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/{{$clase}}.css" rel="stylesheet">

<script type="text/javascript">
    var $url = "{{route('ajax.info', $curso->id)}}";
</script>

{!! Html::style('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') !!}


@stop

@push('css_extra')

{{-- #99B4FF --}}
@php
    $csscolor = $categoria->color_texto ?: "#99B4FF";
    $csscolor .= " !important";
@endphp

<style>
    .enviar {
        color: {{$csscolor}};
    }

    .info-general {
        color: {{$csscolor}};
    }

    .curso-menu a {
        color: {{$csscolor}};
    }
    .sidebar-curso {
        background-color: {{$csscolor}};
    }
    
    /* .pibtn {
        color: {{$csscolor}};
    } */
    em {
        background-color: {{$csscolor}};
    }
</style>


@endpush



@section('container')

    @php
        $fotoscentro = $curso->fotos_centro;
        $fotoscurso = $curso->fotos_curso;
        $fotosinstagram = $curso->fotos_instagram;

        $fotoportada = null;
        if($fotoscurso)
        {
            $fotoportada = $fotoscurso[rand(0,count($fotoscurso)-1)]['img'];
        }
        elseif($fotoscentro)
        {
            $fotoportada = $fotoscentro[rand(0,count($fotoscentro)-1)]['img'];
        }
        elseif($fotosinstagram)
        {
            $fotoportada = $fotosinstagram[rand(0,count($fotosinstagram)-1)]['img'];
        }

        $wprog = false;
        if($curso->course_summary != '' || $curso->course_summary != null || $curso->course_content != '' || $curso->course_content != null  || $curso->frase != '' || $curso->frase != null || $curso->requisitos != '' || $curso->requisitos != null)
        {
            $wprog = true;
        }

        $wcentro = ($curso->centro->name != '' || $curso->centro->name != null || $curso->centro->description != '' || $curso->centro->description != null || $curso->centro->foods != '' || $curso->centro->foods != null || $curso->centro->center_video != '' || $curso->centro->center_video != null);
        $waloja = count($curso->alojamientos) > 0;
        $wactiv = ($curso->course_activities != null || $curso->course_activities != '' || $curso->course_excursions != '' || $curso->course_excursions != null || $curso->centro->center_excursions != '' || $curso->center_excursions != null || $curso->centro->center_activities != '' || $curso->centro->center_activities != null);
        $wmonitor = $curso->monitor;
        $wfotos = ($fotoscentro || $fotoscurso || $fotosinstagram);
    @endphp
    
    <?php $overlay = false; ?>
    <?php /*
    @if(is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada))
        <div class="headerbg white" style="background-image: url('/assets/uploads/course/{{$curso->course_images}}/{{$curso->image_portada}}'); background-size: cover;" id="intro-img">
    @elseif(is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
        <div class="headerbg white" style="background-image: url('/assets/uploads/center/{{$curso->centro->center_images}}/{{$curso->centro->center_image_portada}}'); background-size: cover;" id="intro-img">
    @elseif(!is_file(public_path() ."/assets/uploads/course/" . $curso->course_images . "/" .$curso->image_portada) && !is_file(public_path() ."/assets/uploads/center/" . $curso->centro->center_images . "/" .$curso->centro->center_image_portada))
        @if($fotoportada)
            <div class="headerbg white" style="background-image: url('{{$fotoportada}}'); background-size: cover; background-position: center center;" id="intro-img">
        @else
            <div class="headerbg white" style="background: #E5E5E5 url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/{{rand(1,3)}}.png') repeat;" id="intro-img">
            <?php $overlay = true; ?>
        @endif
    @endif

    */ ?>
    <div class="headerbg white" style="background: #fff;">
        
        <div class="container curso" id="header" style="position: relative;">
            <div class="row">
                <div class="col-xs-2 col-sm-2"></div>
                <div class="col-xs-10 col-sm-7">
                    <div class="titulo">
                        <h1 class="slogan-curso" id="slogan">
                            <span>
                                <ul class="breadcrumb">
                                    <li><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url)!!}">{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo)!!}</a></li>
                                    @if($subcategoria != '')
                                        <li @if($subcategoria_detalle == '')class="activo"@endif><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url)!!}/{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $subcategoria->id, $subcategoria->seo_url)!!}">{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo)!!}</a></li>
                                        @if($subcategoria_detalle != '')
                                            <li class="active"><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url)!!}/{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $subcategoria->id, $subcategoria->seo_url)!!}/{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $subcategoria_detalle->id, $subcategoria_detalle->seo_url)!!}">{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria_detalle->id, $subcategoria_detalle->titulo)!!}</a></li>
                                        @endif
                                    @endif
                                </ul>
                            </span>
                            {!!Traductor::getWeb(App::getLocale(), 'Curso', 'name', $curso->id, $curso->name)!!}<br />

                            <div class="curso-menu">

                                @if($wprog)
                                <a href="#wprog">{!! trans('web.curso.programa') !!}</a>
                                @endif
        
                                @if($wcentro)
                                <a href="#wcentro">{!! trans('web.curso.centro') !!}</a>
                                @endif
        
                                @if($waloja)
                                <a href="#waloja">{!! trans('web.curso.alojamiento') !!}</a>
                                @endif
        
                                @if($wactiv)
                                <a href="#wactiv">{!! trans('web.curso.actividades') !!}</a>
                                @endif
        
                                <a href="#wprecios">{!! trans('web.curso.fechasyprecios') !!}</a>
        
                                @if($wfotos)
                                <a href="#wfotos">{!! trans('web.curso.fotos') !!}</a>
                                @endif
        
                            </div>

                        </h1>
                    </div>

                </div>
                <!-- Start right sidebar -->
                <div class="col-sm-3 col-xs-12 pull-right sidebar-curso" id="sidebar">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <div class="box">
                                @if ($curso->course_promo == 1)
                                    <div id="promocion">
                                        <div class="icon"></div>
                                        {!!Traductor::getWeb(App::getLocale(), 'Curso', 'promo_texto', $curso->id, $curso->promo_texto)!!}
                                    </div>
                                @endif
                                <div class="widget clearfix info-basica">
                                    <div id="intro-curso">
                                        <div id="details">
                                            <ul>
                                                <li><i class="fa fa-map-marker-alt"></i> @if($curso->centro->pais->name != 'España') {{Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name)}} @else {{Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name)}} @endif</li>
                                                <li>
                                                    <i class="fa fa-user"></i> {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_age_range', $curso->id, $curso->course_age_range)!!}
                                                </li>
                                                <li><i class="fa fa-globe"></i> {{trans('web.'.$curso->course_language)}}</li>
                                                <?php $alojas = array(); ?>
                                                @if(count($curso->alojamientos))
                                                    @foreach($curso->alojamientos as $alojamiento)
                                                        <?php $alojas[] = Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $alojamiento->tipo->id, $alojamiento->tipo->accommodation_type_name); ?>
                                                    @endforeach
                                                    @foreach(array_unique($alojas) as $alojatipo)
                                                        <li><i class="fa fa-bed"></i> {!!$alojatipo!!}</li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                        <div id="espes">
                                            <ul>
                                                <?php //$anteriorespe = ''; ?>
                                                <?php //$i = 1; ?>
                                                @foreach($curso->especialidades->sortBy('name') as $e)

                                                    {{-- @if($e->especialidad->id != $anteriorespe || $anteriorespe == null)
                                                        <li><i class="fa fa-star"></i><strong>{!!Traductor::getWeb(App::getLocale(), 'Especialidad', 'name', $e->especialidad->id, $e->especialidad->name)!!}</strong><br />
                                                    @endif
                                                    
                                                    @if(count($curso->especialidades) == $i)
                                                        {!!Traductor::getWeb(App::getLocale(), 'Subespecialidad', 'name', $e->subespecialidad_id, $e->SubespecialidadesName)!!}.</li>
                                                    @endif
                                                    
                                                    @if( ($e->especialidad->id == $anteriorespe || $i == 1) && count($curso->especialidades) != $i)
                                                        {!!Traductor::getWeb(App::getLocale(), 'Subespecialidad', 'name', $e->subespecialidad_id, $e->SubespecialidadesName)!!},
                                                    @endif --}}

                                                    <li>
                                                        <i class="fa fa-star"></i><strong>{!!Traductor::getWeb(App::getLocale(), 'Especialidad', 'name', $e->especialidad->id, $e->especialidad->name)!!}</strong>
                                                        {{$e->subespecialidades_name_lang}}
                                                    </li>


                                                    <?php //$anteriorespe = $e->especialidad->id; ?>
                                                    <?php //$i++; ?>

                                                @endforeach
                                            </ul>
                                        </div>

                                        {{-- Booking --}}
                                        @include('comprar.button')

                                        {{-- Form --}}
                                        @include('web._partials.formulario2020', ['curso_id'=> $curso->id, 'curso'=> $curso])

                                        <div id="utils">
                                            <a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{!! trans('web.catalogo-slug') !!}">{!! trans('web.curso.catalogo') !!}</a>
                                            <a class="second" href="getpdf/{{$curso->id}}" target="_blank">{!! trans('web.curso.descargar') !!}</a>
                                        </div>

                                        {{-- <a href="#plusinfomodal" class="btn btn-primary plusinfolink pibtn hidden-print"
                                           data-toggle="modal" data-target="#plusinfomodal">{!! trans('web.curso.solicitarplaza') !!}</a> --}}

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12" id="sidebar-pinterest">
                            @if($curso->rs_pinterest != '' && ConfigHelper::config('propietario') == 1)
                                @desktop
                                    <a data-pin-do="embedBoard" data-pin-lang="{{App::getLocale()}}" data-pin-board-width="212" data-pin-scale-height="150" data-pin-scale-width="100" href="{{$curso->rs_pinterest}}"></a>
                                @elsedesktop
                                    <div class="text-right">
                                        <a data-pin-do="embedBoard" data-pin-lang="{{App::getLocale()}}" data-pin-board-width="130" data-pin-scale-height="100" data-pin-scale-width="100" href="{{$curso->rs_pinterest}}"></a>
                                    </div>
                                @enddesktop
                            @endif
                        </div>
                    </div>
                </div>
                <!-- End right sidebar -->
            </div>

        </div>
        @if($overlay == true)
            <div class="headerbgoverlay" style="background-color: #fff; opacity: 1;"></div>
        @endif
    </div>
    
    <main class="cd-main-content curso">
        <div class="container" id="contenido">

            <div class="row">
                <div class="col-xs-2 col-sm-2"></div>
                <div class="col-sm-6 col-xs-10" id="infocurso">
                    
                    @if($wprog)
                        <br>
                        <h2 id="wprog">{!! trans('web.curso.programa') !!}</h2>

                        @if ($curso->course_summary != '' || $curso->course_summary != null)
                            <div class="introduccion">
                                {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_summary', $curso->id, $curso->course_summary)!!}
                            </div>
                        @endif

                        @if ($curso->frase != '' || $curso->frase != null)
                            <div class="info-general">
                                {!!Traductor::getWeb(App::getLocale(), 'Curso', 'frase', $curso->id, $curso->frase)!!}
                            </div>
                        @endif

                        @if ($curso->course_content != '' || $curso->course_content != null)
                            {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_content', $curso->id, $curso->course_content)!!}
                        @endif

                        <div class="idioma">
                            @if ($curso->course_language != '' || $curso->course_language != null)
                                <p><strong>{!!trans('web.curso.idiomarequerido')!!}:</strong>
                                @foreach(explode(',',$curso->course_language) as $nidioma)
                                    <?php $nivelidioma[] = trans('web.'.trim($nidioma)); ?>
                                @endforeach
                                    {{implode(',',$nivelidioma)}}
                                </p>
                            @endif

                            @if ($curso->course_minimun_language != '' || $curso->course_minimun_language != null)
                                <p><strong>{!!trans('web.curso.nivel')!!}:</strong> {{trans('web.'.$curso->course_minimun_language)}}</p>
                            @endif

                            @if ($curso->course_language_sessions != '' || $curso->course_language_sessions != null)
                                <p><strong>{!!trans('web.curso.sesiones')!!}:</strong> {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_language_sessions', $curso->id, $curso->course_language_sessions)!!}</p>
                            @endif
                        </div>

                        @if ($curso->requisitos != '' || $curso->requisitos != null)
                            <h4>{!! trans('web.curso.requisitos') !!}</h4>
                            {!!Traductor::getWeb(App::getLocale(), 'Curso', 'requisitos', $curso->id, $curso->requisitos)!!}
                        @endif

                        <?php
                        $path = public_path() ."/assets/uploads/pdf/" . $curso->course_images;
                        $folder = "/assets/uploads/pdf/" . $curso->course_images;

                        if(is_dir($path))
                        {
                            echo '<h4>'.trans('web.curso.timetable').'</h4>';
                            $results = scandir($path);
                            foreach ($results as $result) {
                                if ($result === '.' || $result === '..' || $result == '.DS_Store' || $result[0]==='.') continue;

                                $file = $path . '/' . $result;

                                if( is_file($file) )
                                {
                                    echo '<a href="'.$folder . '/' . $result.'" target="_blank"><i class="fa fa-download"></i> '.$result.'</a><br />';
                                }
                            }
                        }
                        ?>

                        @if ($curso->centro->internet == 1)
                            <div class="internet">
                                <p><i class="fa fa-laptop"></i> {!! trans('web.curso.internet-disponible') !!}</p>
                            </div>
                        @endif
                        @if ($curso->centro->comentarios != '' || $curso->centro->comentarios != null)
                            <div class="internetopciones">
                                <p>{!!$curso->centro->comentarios!!}</p>
                            </div>
                        @endif

                        @if($curso->course_video != '' || $curso->centro->course_video != null)
                            <div class="videowrapper well">
                                <iframe height="300" width="500" src="https://www.youtube.com/embed/{{$curso->course_video}}" frameborder="0" allowfullscreen=""></iframe>
                            </div>
                        @endif
                        @if ($curso->course_provider_url != '' || $curso->course_provider_url != null)
                            <hr>
                            <p><i class="fa fa-info-circle"></i> {!! trans('web.curso.masinfoproveedor1') !!} <a href="{!!$curso->course_provider_url!!}" target="_blank">{!! trans('web.curso.masinfoproveedor2') !!}</a></p>
                        @endif
                    @endif

                    @if($wcentro)
                        <h2 id="wcentro">{!! trans('web.curso.centro') !!}</h2>
                        <h3>{!!Traductor::getWeb(App::getLocale(), 'Centro', 'name', $curso->centro->id, $curso->centro->name)!!}<br />
                            <small><b>{!!Traductor::getWeb(App::getLocale(), 'Ciudad', 'city_name', $curso->centro->ciudad->id, $curso->centro->ciudad->city_name)!!}</b>. {!!Traductor::getWeb(App::getLocale(), 'Pais', 'name', $curso->centro->pais->id, $curso->centro->pais->name)!!}</small>
                        </h3>
                        @if($curso->centro->description != '' || $curso->centro->description != null)
                            {!!Traductor::getWeb(App::getLocale(), 'Centro', 'description', $curso->centro->id, $curso->centro->description)!!}
                        @endif
                        @if($curso->centro->settingup != '' || $curso->centro->settingup != null)
                            <h4>{!! trans('web.curso.instalaciones') !!}</h4>
                            {!!Traductor::getWeb(App::getLocale(), 'Centro', 'settingup', $curso->centro->id, $curso->centro->settingup)!!}
                        @endif
                        @if($curso->centro->foods != '' || $curso->centro->foods != null)
                            <h4>{!! trans('web.curso.comidas') !!}</h4>
                            {!!Traductor::getWeb(App::getLocale(), 'Centro', 'food', $curso->centro->id, $curso->centro->foods)!!}
                        @endif
                        @if($curso->centro->transport != '' || $curso->centro->transport != null)
                            <h4>{!! trans('web.curso.transporte') !!}</h4>
                            {!!Traductor::getWeb(App::getLocale(), 'Centro', 'transport', $curso->centro->id, $curso->centro->transport)!!}
                        @endif

                        @if($curso->centro->center_video != '' || $curso->centro->center_video != null)
                            <div class="videowrapper well">
                                <iframe height="300" width="500" src="//www.youtube.com/embed/{{$curso->centro->center_video}}" frameborder="0" allowfullscreen=""></iframe>
                            </div>
                        @endif
                        @if($curso->centro->address != '' || $curso->centro->address != null)
                            <h4>{!!Traductor::getWeb(App::getLocale(), 'Centro', 'address', $curso->centro->id, $curso->centro->address)!!}</h4>
                            <iframe style="margin-top: 30px;"
                                    width="100%"
                                    height="450"
                                    frameborder="0" style="border:0"
                                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCNPWo91Kd3D656Z5R2U6K1vFlx65KxpE8&q={{strip_tags($curso->centro->address)}}&language={{App::getLocale()}}&zoom=13" allowfullscreen>
                            </iframe>
                        @endif
                    @endif

                    @if($waloja)
                        <h2 id="waloja">{!! trans('web.curso.alojamiento') !!}</h2>
                        @foreach($curso->alojamientos as $alojamiento)

                            <h4>{!!Traductor::getWeb(App::getLocale(), 'Alojamiento', 'name', $alojamiento->id, $alojamiento->name)!!}</h4>
                            <p>{!!Traductor::getWeb(App::getLocale(), 'Alojamiento', 'accommodation_description', $alojamiento->id, $alojamiento->accommodation_description)!!}</p>

                            <?php
                            $fotosaloja = '';
                            $fotosalojaname = array();
                            $path = public_path() ."/assets/uploads/alojamiento/" . $alojamiento->image_dir;
                            $folder = "/assets/uploads/alojamiento/" . $alojamiento->image_dir;

                            if (is_dir($path)) {
                                $results = scandir($path);
                                foreach ($results as $result) {
                                    if ($result === '.'  ?? $result === '..'  ?? $result[0] === '.') continue;

                                    $file = $path . '/' . $result;

                                    if (is_file($file)) {
                                        $fotosaloja .= '
                                            <div class="col-md-4">
                                                <a rel="group" href="'.$folder.'/'.$result.'"><div style="position:relative; overflow:hidden; padding-bottom:100%;"><img class="img-responsive full-width" style="position: absolute;" src="'.$folder.'/'.$result.'" alt=""></div></a>
                                            </div>';
                                    }
                                }
                            }
                            ?>
                            @if($fotosaloja != '')
                                <div id="fotos-aloja" class="hidden-print">
                                    <div class="row">
                                        {!!$fotosaloja!!}
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif

                    @if($wactiv)
                        <h2 id="wactiv">{!! trans('web.curso.actividades') !!}</h2>
                        @if($curso->course_activities != null || $curso->course_activities != '' || $curso->centro->center_activities != null || $curso->centro->center_activities != '')
                            <h3>{!! trans('web.curso.actividades') !!}</h3>
                            @if($curso->course_activities != null || $curso->course_activities != '')
                                {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_activities', $curso->id, $curso->course_activities)!!}
                            @elseif(($curso->course_activities == null || $curso->course_activities == '') && ($curso->centro->center_activities != null || $curso->centro->center_activities != ''))
                                {!!Traductor::getWeb(App::getLocale(), 'Centro', 'center_activities', $curso->centro->id, $curso->centro->center_activities)!!}
                            @endif
                        @endif
                        @if($curso->course_excursions != null || $curso->course_excursions != '' || $curso->centro->center_excursions != null || $curso->centro->center_excursions != '')
                            <h3>{!! trans('web.curso.excursiones') !!}</h3>
                            @if($curso->course_excursions != null || $curso->course_excursions != '')
                                {!!Traductor::getWeb(App::getLocale(), 'Curso', 'course_excursions', $curso->id, $curso->course_excursions)!!}
                            @elseif(($curso->course_excursions == null || $curso->course_excursions == '') && ($curso->centro->center_excursions != null || $curso->centro->center_excursions != ''))
                                {!!Traductor::getWeb(App::getLocale(), 'Centro', 'center_excursions', $curso->centro->id, $curso->centro->center_excursions)!!}
                            @endif
                        @endif
                    @endif

                    @if($wmonitor)
                        <h2 id="wmonitor">
                        @if($curso->category_id == 4 || $curso->category_id == 6)
                            {!! trans('web.curso.coordinador') !!}
                        @else
                            <h2>{!! trans('web.curso.monitor') !!}
                        @endif
                        </h2>

                        <h4>{!! $curso->monitor->name !!}</h4>
                        <div class="row">
                        @if($curso->monitor->foto != null || $curso->monitor->foto != '')
                            <div class="col-sm-4  col-xs-12">
                                <img class="img-responsive img-thumbnail" src="{{$curso->monitor->foto}}" />
                            </div>
                        @endif
                        @if($curso->monitor->notas)
                            <div class="col-sm-8  col-xs-12">
                                {!!Traductor::getWeb(App::getLocale(), 'Monitor', 'notas', $curso->monitor->id, $curso->monitor->notas)!!}
                            </div>
                        @endif
                        </div>
                    @endif

                    <h2 id="wprecios">{!! trans('web.curso.fechasyprecios') !!}</h2>
                    @include('web.home2020._curso-precios')

                    @if($wfotos)
                        <h2 id="wfotos">{!! trans('web.curso.fotos') !!}</h2>
                        <div id="fotos-curso" class="hidden-print">
                            <div class="row">

                                @if($fotoscentro)
                                @foreach($fotoscentro as $foto)

                                    <div class="col-md-3 col-sm-6 col-sx-12 fotos">
                                        <div class="foto fotocentro">
                                            <a rel="group" href="{{$foto['img']}}">
                                                <img src="{{$foto['thumb']}}" alt="{{$foto['name']}}">
                                            </a>
                                        </div>
                                    </div>

                                @endforeach
                                @endif

                                @if($fotoscurso)
                                @foreach($fotoscurso as $foto)

                                    <div class="col-md-3 col-sm-6 col-sx-12 fotos-grid">
                                        <div class="foto-grid">
                                            <a rel="group" href="{{$foto['img']}}">
                                                <img src="{{$foto['thumb']}}" alt="{{$foto['name']}}">
                                            </a>
                                        </div>
                                    </div>
                                
                                @endforeach
                                @endif

                                @if($fotosinstagram)
                                @foreach($fotosinstagram as $foto)

                                    <div class="col-md-3 col-sm-6 col-sx-12 fotos">
                                        <div class="foto fotoig">
                                            <a rel="group" href="{{$foto['img']}}">
                                                <img src="{{$foto['thumb']}}" alt="{{$foto['name']}}">
                                            </a>
                                        </div>
                                    </div>
                                
                                @endforeach
                                @endif

                            </div>
                            @if($curso->course_video != '' || $curso->centro->course_video != null)
                            <div class="row">
                                <div class="videowrapper well">
                                    <iframe height="300" width="500" src="https://www.youtube.com/embed/{{$curso->course_video}}" frameborder="0" allowfullscreen=""></iframe>
                                </div>
                            </div>
                            @endif
                        </div>
                    @endif
                </div>
            </div>

            {{-- <div class="row">
                @include('web.home2020.includes.copyright')
            </div> --}}
       </div>
    </main>

    <!-- Modal -->
    @include('web._partials.plusinfomodal', ['hidden'=> $curso->name])

    {{-- Booking --}}
    @include('comprar.modal')

@stop

@section('extra_footer')

    {{-- Booking --}}
    @include('comprar.scripts')

    <script src="/assets/{{ConfigHelper::config('tema')}}/js/bootstrap-tabcollapse.js" type="text/javascript"></script>

    <script type="text/javascript">
        
        var offsetSidebar0 = 130;
            var offsetSidebar1 = -290;
        // @if ($curso->course_promo == 1)
        //     var offsetSidebar0 = 130;
        //     var offsetSidebar1 = -320;
        // @endif

        $(function () {
            var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');

            var offsetExtra = $('#espes').height();
            offsetSidebar1 -= offsetExtra;

            $('.nav-tabs a').click(function (e) {
                $(this).tab('show');
                window.location.hash = this.hash;
                $('html,body').scrollTop(0);
            });

            $('#app_formulario_curso').hover(function() {
                    $('#sidebar').css({'margin-top': offsetSidebar1});
                }, function(){
                    // $('#sidebar').css({'margin-top': '130px'});
            });

        });

        $(document).scroll(function () {
            if($(window).width() > '767'){
                var y = $(document).scrollTop();
                if (y >= 10) {
                    // $('#intro-img').css({'height': '200px'});
                    // $('#cursotabs').css({'top': '7px'});
                    $('#sidebar').css({'margin-top': offsetSidebar1});
                    // $('.headerbgoverlay').css({'height': '300px'});
                    if(y>=20){
                        // $('#contenido').css({'margin-top': '350px'});
                    }
                } else {
                    // $('#intro-img').css({'height': '300px'});
                    // $('#cursotabs').css({'top': '157px'});
                    // $('#contenido').css({'margin-top': '480px'});
                    $('#sidebar').css({'margin-top': offsetSidebar0});
                    // $('.headerbgoverlay').css({'height': '450px'});
                }
            }
        });



        $('#fotos-curso').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image',
            gallery: {
                enabled: true
            },
            titleSrc: 'title'
        });


        $('#fotos-aloja').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image',
            gallery: {
                enabled: true
            },
            titleSrc: 'title'
        });

        $('#cursotabs').tabCollapse();



        function squareThumbs() {
            var thumbs = document.getElementsByTagName("img");

            for (i = 0; i < thumbs.length; i++) {
                if (thumbs[i].parentNode.className.indexOf("thumbnail") != -1){
                    thumbs[i].style.height = thumbs[i].clientWidth + "px";
                }
            }
        }
        squareThumbs();
        window.onresize = squareThumbs;

        $(document).on('click', 'a[href^="#w"]', function (event) {
            event.preventDefault();

            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top - 160
            }, 500);
        });
    </script>

    @if($curso->rs_pinterest != '' && ConfigHelper::config('propietario') == 1)
        <script async defer src="//assets.pinterest.com/js/pinit.js"></script>
    @endif

    {!! Html::script('assets/js/manage-web.js') !!}
    {!! Html::script('assets/plugins/moment-with-locales.min.js') !!}
    {!! Html::script('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}

@stop