@if($bloque->color_fondo)
<style>
    .colorBg-{{$bloqueId}} {
        background-color: {{ $bloque->color_fondo }} !important;
    }
</style>
@endif

<div class="container-fluid container12 bloqueDiv colorBg-{{$bloqueId}}" id="bloque-{{$bloqueId}}">
    <div class="row no-gutter">
        <div class="col-1 col-lg-5"></div>
        <div class="col-11 col-lg-6">
            <div class="content">
                <h1>{!! $bloque->getTranslate('home_titulo') !!}</h1>
                <p class="mb-5">
                    {!! $bloque->getTranslate('home_titulo2') !!}
                </p>
                @if($bloque->home_boton_activo)
                <div class="linkContainer noCentered colorTxt">
                    <a class="colorTxt" {!! $bloque->home_enlace !!}>{!! $bloque->home_enlace_txt !!}</a>
                </div>
                @endif
            </div>
        </div>
        <div class="d-none d-lg-block col-lg-1"></div>
    </div>
</div>