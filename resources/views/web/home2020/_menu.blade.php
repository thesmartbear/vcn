<div class="menuLeft ui {{$div ?? "d-none d-lg-block"}}">
    @if($logo ?? true)
        <a id="cd-logo" href="/"><img src="/assets/logos/n-{{ConfigHelper::config('logoweb')}}" alt="{{ConfigHelper::config('nombre')}}" /></a>
        {{-- <h3>BRITISH <b>SUMMER</b></h3> --}}
    @endif
    
    @include('web.home2020._menu-1')

    <div class="secondMenuContainer">
        
        <div class="secondMenu mt-5 text-left float-right">

            {{-- <input type="search" name="" id="searchMenu" class="w-100"> --}}
            {{-- <form id="searchMenu" action="{{route('web.buscar')}}" method="post" enctype="multipart/form-data" class="w-100" autocomplete="off" >
                <input type="text" placeholder="{{trans('web.buscar')}}" name="search"  id="searchInput" class="searchbox-input" required>
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            </form> --}}

            <div class="clearfix"></div>
            
            <p class="searchBtnMenuLeft"><i class="fas fa-search"></i></p>

            @include('web.home2020._menu-2')

        </div>
    </div>

</div>

<style>
    
</style>

<div id="searchMenu" class="searchMenu" style="z-index: 9999;">
    <i id="searchMenuClose" class="fal fa-times"></i>
    <form id="searchMenuForm" action="{{route('web.buscar')}}" method="post" enctype="multipart/form-data" class="w-50" autocomplete="off" >
        <input type="text" placeholder="{{trans('web.buscar')}}" name="search"  id="searchInput" class="searchbox-input" required>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    </form> 
</div>

@push("scripts")
<script> 
    $(document).ready(function(){
        $('#close').hide();
        $showMenu = false;
        $currentScrollPosition = 0;
    });
    
    $('.menuBtnsContainer').click(function(){
        if ($showMenu == false) {
            $currentScrollPosition = $(window).scrollTop();
            $(window).scrollTop(0);
            $('body').css('overflow', 'hidden');
            $showMenu = true;
            
            $('#open').hide();
            $('#close').fadeIn();

            $('.menuContainer').hide();
            $('.menuContainer').removeClass('d-none');
            $('.menuContainer').removeClass('hidden');
            $('.menuContainer').fadeIn('fast');
        } else {
            $('body').css('overflow', 'auto');
            $showMenu = false;
            
            $('#open').fadeIn();
            $('#close').hide();

            $('.menuContainer').fadeOut('fast');
            // $('.menuContainer').removeClass('d-block').addClass('d-none');
            // $('.menuContainer').removeClass('hidden').addClass('visible-xs visible-sm visible-md');
            $(window).scrollTop($currentScrollPosition);
        }
    });

    var searchVisible = false;
    function toggleSearch(fontSize="2em", marginTop="65px")
    {
        $('#searchInput').css("font-size", fontSize);
        $('#searchMenuForm').css("margin-top", marginTop);

        $('#searchMenu').toggle('fast');
        searchVisible = !searchVisible;
        if(searchVisible)
        {
            $('#searchInput').focus();
            $("body").css("overflow", "hidden");
        }
        else
        {
            $("body").css("overflow", "auto");
        }
    }

    $('.searchBtnMenuLeft').click(function(){
        toggleSearch();
    });

    $("#searchMenuClose").click(function(){
        toggleSearch();
    });

    $('.searchBtnMenuResponsive').click(function(){
        // $('#searchMenuResponsive').toggle('fast');
        toggleSearch("1em", "120px");
    });

    /*
    $(".menu-item").hover(function(e) {
        $('.introDiv').hide();
        let catid = $(this).data('id');
        let div = "#intro-" + catid
        let divTxt = "#intro-txt-" + catid

        $(".introTxt").removeClass('txtThirdColor ffSemiBold').addClass('txtPrimaryColor ffRegular');
        $(divTxt).removeClass('txtPrimaryColor ffRegular').addClass('txtThirdColor ffSemiBold');

        $(div).show();
    });
    */
    // $(".menu-item").first().trigger("mouseover");

    // $('.item-has-children').hover( function() {
    $('.menu-item').hover( function() {
        //mouseenter

        if( $(this).children('.sub-menu').is(":visible") )
        {
            $(this).children('.sub-menu').slideUp();
        }
        else
        {
            $('.item-has-children').children('.sub-menu').slideUp('slow');
            $(this).children('.sub-menu').slideDown(1600);
        }

    });

</script>
@endpush