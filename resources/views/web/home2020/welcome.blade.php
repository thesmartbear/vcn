<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>{{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}</title>

    @php
        $assets = "/assets/home2020/welcome";
        $tema = ConfigHelper::config('tema');
    @endphp
    @include('web.home2020._welcome_head')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" defer/>
    {{-- <link href="/assets/fontawesome/css/all.css" rel="stylesheet" defer>  --}}
    
</head>
<body> 

    @include('web._partials.google_body')

    {{-- MENU start --}}
    {{-- MENU btn responsive --}}
    <div class="menuBtnsContainer d-block d-lg-none">
        <a id="logo-responsive" href="/"><img src="/assets/logos/n-{{ConfigHelper::config('logoweb')}}" alt="{{ConfigHelper::config('nombre')}}" /></a>
        <div class="menuTxt">
            <i id="close" class="fal fa-times"></i>
            @lang("web.home.menu")
        </div>
    </div>

    {{-- MENU responsive --}}
    @include('web.home2020._menu-responsive')
    
    {{-- MENU big --}}
    @include('web.home2020._menu')
    {{-- MENU end --}}

    {{-- BLOQUES --}}
    {{--
    1 => 'Full', //"YA LO TIENES TODO?"
    2 => 'Fifty Texto+Foto', //BE HAPPY
    3 => 'Fifty Foto+Texto', //BE HIPPY
    4 => 'Fifty full', //TE VAS EN GRUPO / BLA DI BLUM
    5 => '60/40 F+T', //LET'S GO FAMILY!
    6 => 'Texto', 
    7 => 'Logos',
    // 8 => 'Slider'
    --}}
    
    @include('web.home2020._bloque-intro', ['bloqueId'=> 'intro', 'intro'=> $intro])
    
    @if($slides && $slides->count())
        @include('web.home2020._bloque-slider', ['bloqueId'=> 'slides', 'slides'=> $slides])
    @endif

    {{-- @include('web.home2020._bloque-4', ['bloqueId'=> 3])
    @include('web.home2020._bloque-6', ['bloqueId'=> 4])
    @include('web.home2020._bloque-1', ['bloqueId'=> 5])
    @include('web.home2020._bloque-2', ['bloqueId'=> 6])
    @include('web.home2020._bloque-5', ['bloqueId'=> 7])

    @include('web.home2020._bloque-4b', ['bloqueId'=> 8])
    
    @include('web.home2020._bloque-7', ['bloqueId'=> 9])
    @include('web.home2020._bloque-3', ['bloqueId'=> 10]) --}}

    <style>
        .bloqueDiv{
            width: 100%;
            overflow-x: hidden;
        }
    </style>
    
    @foreach($webcats as $wc)
        @php
            $t = $wc->home_bloque ?: 1;
        @endphp
        {{-- {{$wc->id}}:{{$t}} --}}
        @include("web.home2020._bloque-". $t, ['bloqueId' => $wc->id, 'bloque'=> $wc])
    @endforeach
    
    {{-- FOOTER --}}
    @include("web.home2020._footer")
        
    <script src="{{$assets}}/libs/js/jquery.min.js"></script>
    <script src="{{$assets}}/libs/js/bootstrap.min.js" defer></script>
    {{-- <script src="{{$assets}}/libs/js/backgroundCheck.js" defer></script> --}}
    <script src="{{$assets}}/libs/js/slick.js" defer></script>
    <script src="https://kit.fontawesome.com/bfb19d8d13.js" crossorigin="anonymous"></script>

    @include('web._partials.cookies2020')
    {{-- @include('web._partials.cookies') --}}
    @include('web._partials.google_head')

    <script> 
        $(document).ready(function(){
            var slickOpts = {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                //adaptiveHeight: true,
                //centerMode: true,
                easing: 'swing', // see http://api.jquery.com/animate/
                autoplay: true,
                autoplaySpeed: 2000,
                speed: 700,
                dots: true,
                customPaging: function(slick,index) {
                    return '<a>' + (index + 1) + '</a>';
                }
            };
            // Init slick carousel
            $('#carousel').slick(slickOpts);
        });

        $(window).scroll(function() {
            var height = $(window).scrollTop();
            if (height > 1000) {
                $('#scrollToTop').fadeIn();
            } else {
                $('#scrollToTop').fadeOut();
            }
        });

        $("#scrollToTop").click(function(event) {
            event.preventDefault();
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        });

    </script>

    {{-- Detector de color del menú lateral --}}
    @include('web.home2020._menu-js')

    @stack('scripts')

    {!! ConfigHelper::config('code_footer') !!}

</body>
</html>