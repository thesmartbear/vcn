<div class="container-fluid d-flex container10 bloque" id="bloque-{{$bloqueId}}">
    <div class="row no-gutters flex-fill">
        <div class="col-1 col-md-1 col-lg-2"></div>
        <div class="col-11 col-md-11 col-lg-8">
            <div class="content">
                <h1>{!! $bloque->getTranslate('home_titulo') !!}</h1>
                <div class="textBlock">
                    <p class="mt-4">{!! $bloque->getTranslate('home_titulo2') !!}</p>
                    @if($bloque->home_boton_activo)
                    <div class="linkContainer noCentered mt-3 colorTxt">
                        <a class="ffRegular txtPrimaryColor" {!! $bloque->home_enlace !!}>{!! $bloque->home_enlace_txt !!}</a>    
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="d-none d-lg-block col-lg-2"></div>
        <div class="col-1 col-lg-2 col-xl-3"></div>
        <div class="col-10 col-lg-8 col-xl-6">  
            <div class="logos">     
                <img src="{{$assets}}/assets/imgs/logo1.png" alt="" class="img-fluid" />
                <img src="{{$assets}}/assets/imgs/logo2.png" alt="" class="img-fluid" />
                <img src="{{$assets}}/assets/imgs/logo3.png" alt="" class="img-fluid" />
                <img src="{{$assets}}/assets/imgs/logo4.png" alt="" class="img-fluid" />
                <img src="{{$assets}}/assets/imgs/logo5.png" alt="" class="img-fluid" />
            </div>
        </div>
        <div class="col-1 col-lg-2 col-xl-3"></div>
    </div>
</div>