@if(count($curso->convocatoriasAbiertas))
    @foreach($curso->convocatoriasAbiertas as $ca)
        @if($ca->convocatory_open_status == 1)
            <h2>{!!Traductor::getWeb(App::getLocale(), 'Abierta', 'convocatory_open_name', $ca->id, $ca->convocatory_open_name)!!}</h2>
            <h6 class="separator">{!! trans('web.curso.curso') !!}</h6>

            <script type="text/javascript">
                var $startDate = "{{$ca->convocatory_open_valid_start_date}}";
                var $endDate = "{{$ca->convocatory_open_valid_end_date}}";
                var $dayDate = "{{$ca->convocatory_open_start_day}}";
            </script>

            <table class="table tabla-precios">
                <thead>
                <tr class="thead">
                    <td>{!! trans('web.curso.finicio') !!}</td>
                    <td>{!! trans('web.curso.duracion') !!}</td>
                    <td>{!! trans('web.curso.ffin') !!}</td>
                    <td align="right">{!! trans('web.curso.precio') !!}</td>
                </tr>
                </thead>
                <tbody>
                <tr>

                    <td width="45%">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            {!! Form::text('booking-cabierta-fecha_ini', '', array( 'id'=>'booking-cabierta-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'datetime form-control',
                                'data-start' => $ca->convocatory_open_valid_start_date,
                                'data-end' => $ca->convocatory_open_valid_end_date,
                                'data-day' => $ca->convocatory_open_start_day
                            )) !!}
                        </div>
                    </td>
                    <td width="20%">
                        {!! Form::select('booking-cabierta-semanas', ConfigHelper::getSemanas($curso->duracion_name), 0, array( 'id'=> 'booking-cabierta-semanas', 'class' => 'form-control')) !!}
                    </td>
                    <td width="15%">
                        {!! Form::text('booking-cabierta-fecha_fin', '', array( 'id'=>'booking-cabierta-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
                    </td>
                    <td width="20%" align='right'>
                        <span class="booking-cabierta-precio" id="booking-cabierta-precio"></span>
                    </td>
                </tr>
                </tbody>
            </table>

            <h6 class="separator">{!! trans('web.curso.alojamiento') !!}</h6>
            {{-- precio alojamiento: fecha + duracion => fecha_fin y precio --}}
            <table class="table tabla-precios">
                    <thead>
                    <tr class="thead">
                        <td>{!! trans('web.curso.alojamiento') !!}</td>
                        <td>{!! trans('web.curso.finicio') !!}</td>
                        <td>{!! trans('web.curso.duracion') !!}</td>
                        <td>{!! trans('web.curso.ffin') !!}</td>
                        <td align="right">{!! trans('web.curso.precio') !!}</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td width="20%">
                            {!! Form::select('booking-alojamiento', $curso->alojamientos->pluck('name','id'), 0, array( 'id'=> 'booking-alojamiento', 'class' => 'form-control')) !!}
                        </td>
                        <td width="25%">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                {!! Form::text('booking-alojamiento-fecha_ini', '', array( 'id'=>'booking-alojamiento-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'datetime form-control')) !!}
                            </div>
                        </td>
                        <td width="20%">
                            {!! Form::select('booking-alojamiento-semanas', ConfigHelper::getSemanas($curso->duracion_name), 0, array( 'id'=> 'booking-alojamiento-semanas', 'class' => 'form-control')) !!}
                        </td>
                        <td width="15%">
                            {!! Form::text('booking-alojamiento-fecha_fin', '', array( 'id'=>'booking-alojamiento-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
                        </td>
                        <td width="20%" align='right'>
                            <span id="booking-alojamiento-precio"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>



            <h6>{!!trans('web.curso.alojamientoextrasobligatorios')!!}</h6>
            <div id="booking-alojamiento-extras"></div>

            <h6>{!!trans('web.curso.extrasobligatorios')!!}</h6>
            <div id="booking-cabierta-extras">
                <ul class="incluye" id="extras-obligatorios">
                    @foreach($curso->extras_obligatorios as $extra)
                        <li>{!! Traductor::getWeb(App::getLocale(), 'CursoExtra', 'course_extras_name', $extra->id, $extra->name) !!}
                            <b>{{ConfigHelper::parseMoneda($extra->precio, $extra->moneda)}}</b></li>
                    @endforeach
                </ul>
                <ul class="incluye" id="extras-genericos-obligatorios">
                    @foreach($curso->extras_genericos_obligatorios as $extra)
                        <li>{!! Traductor::getWeb(App::getLocale(), 'Extra', 'generic_name', $extra->generics_id, $extra->name) !!}
                            <b>{{ConfigHelper::parseMoneda($extra->precio, $extra->moneda)}}</b></li>
                    @endforeach
                </ul>
                <ul class="incluye" id="extras-centro-obligatorios">
                    @foreach($curso->extras_centro_obligatorios as $extra)
                        <li>{!! Traductor::getWeb(App::getLocale(), 'CentroExtra', 'center_extras_name', $extra->id, $extra->name) !!}
                            <b>{{ConfigHelper::parseMoneda($extra->precio, $extra->moneda)}}</b></li>
                    @endforeach
                </ul>
            </div>

            <div id="booking-subtotal-div" class="row preciosubtotal collapse">
                <div class="col-sm-4">
                    <h5>{!! trans('web.curso.subtotal') !!}</h5>
                </div>
                <div class="col-sm-8">
                    <div id="booking-subtotal" class="text-black text-right pull-right"></div>
                </div>
            </div>


            <div class="row preciototal">
                <div class="col-sm-4">
                    <h5>{!! trans('web.curso.total') !!}</h5>
                </div>
                <div class="col-sm-8">
                    <div id="booking-total" class="text-black text-right pull-right"><small>{!!trans('web.curso.selecciona')!!}</small></div>
                </div>
            </div>


            <h4 class="divisas">{!!trans('web.curso.divisas')!!}</h4>
            <ul class="divisas">
                @foreach($curso->divisas_txt_web as $divisa)
                    <li>{{$divisa}}</li>
                @endforeach
            </ul>


            <h4 class="separator">{!!trans('web.curso.precioincluye')!!}</h4>
            <ul class="incluye">
                @foreach($ca->incluyes as $cai)
                    @if($cai->incluye->tipo == 0)
                        <li>{!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cai->incluye->id, $cai->incluye->name) !!}</li>
                    @elseif($cai->incluye->tipo == 1 && $cai->valor != 0)
                        <li>{{$cai->valor}} {!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cai->incluye->id, $cai->incluye->name) !!}</li>
                    @endif
                @endforeach
                @if($ca->incluye_horario != '')<li>{!! trans('web.curso.incluyeclasesidioma') !!} {!! Traductor::getWeb(App::getLocale(), 'Abierta', 'incluye_horario', $ca->id, $ca->incluye_horario) !!}</li>@endif
                @if($ca->convocatory_open_price_include != ''){!! strip_tags(Traductor::getWeb(App::getLocale(), 'Abierta', 'convocatory_open_price_include', $ca->id, $ca->convocatory_open_price_include),'<p><li><a><b><strong><em>') !!}@endif
            </ul>

        @endif
    @endforeach

@endif


<?php $plazas_disponibles = 0; ?>

@if(count($curso->convocatoriasCerradas) && $curso->convocatoriasCerradas->contains('convocatory_semiopen', 0))
    <div class="row">
        @foreach($curso->alojamientos as $aloja)
            <div class="col-sm-12"><h6 class="separator">{!!Traductor::getWeb(App::getLocale(), 'AlojamientoTipo', 'accommodation_type_name', $aloja->accommodation_type_id, $aloja->accommodation_name)!!}</h6></div>
        <?php
            $convos = $curso->convocatoriasCerradas;
            $convosordenadas = $convos->sortBy(function($convos) {
                return sprintf('%-12s%s', $convos->convocatory_close_start_date, $convos->convocatory_close_duration_weeks);
            });
        ?>

            @foreach($convosordenadas as $cc)

                @if($cc->activo_web == 1)

                    @if($cc->alojamiento_id == $aloja->id)
                        @if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 0)
                            <div class="col-sm-4 precio">

                                {{-- PLAZAS POR ALOJAMIENTO --}}
                                @if($cc->plazas_disponibles <= 0 && $cerrado != 1)
                                    <div class="plazasdisponibles"><p class="bg-danger">{{trans('web.grupocerrado')}}</p></div>
                                @elseif($cc->plazas_disponibles <= 5 && $cerrado != 1)
                                    <div class="plazasdisponibles"><p class="bg-warning">{{$cc->plazas_disponibles}} {{trans_choice('web.plazasdisponiblesnum',$cc->plazas_disponibles)}}</p></div>
                                @elseif($cerrado != 1)
                                    <div class="plazasdisponibles"><p class="bg-success">{{trans('web.plazasdisponibles')}}</p></div>
                                @endif

                                <!-- Calcular Precios alojamiento -->
                                <?php $precio_aloja = $aloja->calcularPrecio( Carbon::parse($cc->convocatory_close_start_date)->format('d/m/Y'), Carbon::parse($cc->convocatory_close_end_date)->format('d/m/Y'), $cc->convocatory_close_duration_weeks) ?>

                                <h5>{{date('d.m.Y', strtotime($cc->convocatory_close_start_date))}} <span>{!! trans('web.curso.al') !!}</span> {{date('d.m.Y', strtotime($cc->convocatory_close_end_date))}}</h5>
                                <p class="duracion">{{$cc->convocatory_close_duration_weeks}} {{trans_choice('web.curso.'.ConfigHelper::getPrecioDuracionUnitCerrada($cc->duracion_fijo),$cc->convocatory_close_duration_weeks)}}</p>
                                @if($cc->dto_early == null || $cc->dto_early == '')
                                    @if($precio_aloja['importe'] != 0)
                                        <!-- Precio curso -->
                                        <p><b><span class="text-capitalize">{!! trans('web.curso.programa') !!}</span>: {{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b>
                                        <!-- Precio alojamiento -->
                                        <br /><b><span class="text-capitalize">{!! trans('web.curso.alojamiento') !!}</span>: {{ ConfigHelper::parseMoneda($precio_aloja['importe'], $precio_aloja['moneda']) }}</b></p>
                                    @else
                                        <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b></p>
                                    @endif
                                @else
                                    @if((strtotime(date('Y-m-d')) >= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->desde)) && (strtotime(date('Y-m-d')) <= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->hasta)))
                                        <p><s>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</s></p>
                                        <p><img class="earlybird" src="/assets/britishsummer/img/earlybird.png"> <b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price-(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->importe), \VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->moneda_name) }}</b></p>
                                    @else
                                        @if($precio_aloja['importe'] != 0)
                                            <!-- Precio curso -->
                                            <p><b><span class="text-capitalize">{!! trans('web.curso.programa') !!}</span>: {{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b>
                                            <!-- Precio alojamiento -->
                                            <br /><b><span class="text-capitalize">{!! trans('web.curso.alojamiento') !!}</span>: {{ ConfigHelper::parseMoneda($precio_aloja['importe'], $precio_aloja['moneda']) }}</b></p>
                                        @else
                                            <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b></p>
                                        @endif
                                    @endif
                                @endif

                                <a class="verincluye collapsed" data-toggle="collapse" data-target="#incluye-{{$cc->id}}" aria-expanded="false" aria-controls="collapseExample">
                                    {!!trans('web.curso.precioincluye')!!}
                                </a>
                                <div class="collapse" id="incluye-{{$cc->id}}">
                                    <ul class="incluye" style="background-color: {{$csscolor}};">
                                        @foreach($cc->incluyes as $cci)
                                            @if($cci->incluye->tipo == 0)
                                                <li>{!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cci->incluye->id, $cci->incluye->name) !!}</li>
                                            @elseif($cci->incluye->tipo == 1 && $cci->valor != 0)
                                                <li>{{$cci->valor}} {!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cci->incluye->id, $cci->incluye->name) !!}</li>
                                            @endif
                                        @endforeach
                                        @if($cc->incluye_horario != '')<li>{!! trans('web.curso.incluyeclasesidioma') !!} {!! Traductor::getWeb(App::getLocale(), 'Cerrada', 'incluye_horario', $cc->id, $cc->incluye_horario) !!}</li>@endif
                                        @if($cc->convocatory_close_price_include != ''){!! strip_tags(Traductor::getWeb(App::getLocale(), 'Cerrada', 'convocatory_close_price_include', $cc->id, $cc->convocatory_close_price_include),'<p><li><a><b><strong><em>') !!}@endif
                                    </ul>
                                </div>
                            </div>
                        @endif
                    @endif
                @endif
            @endforeach

        @endforeach
    </div>
@endif


@if($curso->convocatoriasCerradas->where('activo_web',1)->contains('convocatory_semiopen', 1))
    <div class="row">
        <div class="col-sm-12">
            @if(count($curso->convocatoriasCerradas) && $curso->convocatoriasCerradas->contains('convocatory_semiopen', 0))
                <h4 class="separator">{!! trans('web.curso.planb') !!}</h4>
                <p>{!! trans('web.curso.planbfrase') !!}</p>
            @endif
        </div>
        <?php
        $convosSC = $curso->convocatoriasCerradas;
        $convosSCordenadas = $convosSC->sortBy(function($convosSC) {
            return sprintf('%-12s%s', $convosSC->convocatory_close_start_date, $convosSC->convocatory_close_duration_weeks);
        });
        ?>
        @foreach($convosSCordenadas as $cc)
            @if($cc->activo_web == 1)
                @if($cc->convocatory_close_status == 1 && $cc->convocatory_semiopen == 1)
                    <div class="col-sm-6 precio">
                        @if($cc->alojamiento_id != '' || $cc->alojamiento_id != 0)<h6>{!! Traductor::getWeb(App::getLocale(), 'Alojamiento', 'name', $cc->alojamiento_id, \VCN\Models\Alojamientos\Alojamiento::find($cc->alojamiento_id)->name) !!}</h6>@endif
                        <h5><span>{!! trans('web.curso.entre') !!}</span> {{date('d.m.Y', strtotime($cc->convocatory_close_start_date))}} <span>{!! trans('web.curso.yel') !!}</span> {{date('d.m.Y', strtotime($cc->convocatory_close_end_date))}}</h5>
                        <p class="duracion">{{$cc->convocatory_close_duration_weeks}} {{trans_choice('web.curso.'.ConfigHelper::getPrecioDuracionUnitCerrada($cc->duracion_fijo),$cc->convocatory_close_duration_weeks)}}</p>

                            @if($cc->dto_early == null || $cc->dto_early == '')
                                <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b></p>
                            @else
                                @if((strtotime(date('Y-m-d')) >= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->desde)) && (strtotime(date('Y-m-d')) <= strtotime(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->hasta)))
                                    <p><s>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</s></p>
                                    <p><img class="earlybird" src="/assets/britishsummer/img/earlybird.png"> <b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price-(\VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->importe), \VCN\Models\Descuentos\DescuentoEarly::find($cc->dto_early)->moneda_name) }}</b></p>
                                @else
                                    <p><b>{{ ConfigHelper::parseMoneda($cc->convocatory_close_price, $cc->moneda_name) }}</b></p>
                                @endif
                            @endif

                        <a class="verincluye collapsed" data-toggle="collapse" data-target="#incluye-{{$cc->id}}" aria-expanded="false" aria-controls="collapseExample">
                            {!!trans('web.curso.precioincluye')!!}
                        </a>
                        <div class="collapse" id="incluye-{{$cc->id}}">
                            <ul class="incluye">
                                @foreach($cc->incluyes as $cci)
                                    @if($cci->incluye->tipo == 0)
                                        <li>{!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cci->incluye->id, $cci->incluye->name) !!}</li>
                                    @elseif($cci->incluye->tipo == 1 && $cci->valor != 0)
                                        <li>{{$cci->valor}} {!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'name', $cci->incluye->id, $cci->incluye->name) !!}</li>
                                    @endif
                                @endforeach
                                @if($cc->incluye_horario != '')<li>{!! trans('web.curso.incluyeclasesidioma') !!} {!! Traductor::getWeb(App::getLocale(), 'Cursoincluye', 'incluye_horario', $cc->id, $cc->incluye_horario) !!}</li>@endif
                                @if($cc->convocatory_close_price_include != ''){!! strip_tags(Traductor::getWeb(App::getLocale(), 'Cerrada', 'convocatory_close_price_include', $cc->id, $cc->convocatory_close_price_include),'<p><li><a><b><strong><em>') !!}@endif
                            </ul>
                        </div>
                    </div>
                @endif
            @endif
        @endforeach
    </div>
@endif


@if($curso->es_convocatoria_multi == 1)
    {!!Traductor::getWeb(App::getLocale(), 'Curso', 'preciosyfechas', $curso->id, $curso->preciosyfechas)!!}
@endif


@if(count($curso->divisas_txt_web)>0)
    <h4 class="divisas">{!!trans('web.curso.divisas')!!}</h4>
    <ul class="divisas">
        @foreach($curso->divisas_txt_web as $divisa)
            <li>{{$divisa}}</li>
        @endforeach
    </ul>
@endif

@if( count($curso->extras) || count($curso->extrasGenericos) )
    <div class="row">
        <div class="col-sm-12">
            <h4 class="separator">{!! trans('web.curso.extras') !!}</h4>
            @foreach($curso->extras as $e)
                <h6>{!!Traductor::getWeb(App::getLocale(), 'CursoExtra', 'course_extras_name', $e->id, $e->course_extras_name)!!}</h6>
                <p>{!!Traductor::getWeb(App::getLocale(), 'CursoExtra', 'course_extras_description', $e->id, $e->course_extras_description)!!}</p>
                @if($e->course_extras_price > 0)
                    <p><b>{{ ConfigHelper::parseMoneda($e->course_extras_price, $e->moneda) }}@if($e->course_extras_unit) / {!!Traductor::getWeb(App::getLocale(), 'ExtraUnidad', 'name', $e->course_extras_unit_id, trans_choice('web.curso.'.$e->tipo_name, 1))!!} @endif</b></p>
                @else
                    <p><b>{!! trans('web.curso.incluido') !!}</b></p>
                @endif
            @endforeach
            
            @foreach($curso->extrasGenericos as $eg)
                <h6>{!!Traductor::getWeb(App::getLocale(), 'Extra', 'generic_name', $eg->generics_id, $eg->name )!!}</h6>
                @if($eg->precio > 0)
                    <p><b>{{ ConfigHelper::parseMoneda($eg->precio, $eg->moneda) }}@if($eg->generico->generic_unit == 1) /  {!!Traductor::getWeb(App::getLocale(), 'ExtraUnidad', 'name', $eg->generico->generic_unit_id, trans_choice('web.curso.'.$eg->generico->tipo_name, 1))!!} @endif</b></p>
                @else
                    <p><b>{!! trans('web.curso.incluido') !!}</b></p>
                @endif
            @endforeach
            
        </div>
    </div>
@endif

@if(ConfigHelper::config('propietario') == 1)
    @if($curso->web_pie)
        <p class="financiacion">
            {!! $curso->web_pie_txt !!}
        </p>
    @endif
@endif