@php
    $img = $bloque->imagen_webp ?: "$assets/assets/imgs/travelbags.jpg";
@endphp

<style>
    #bloque-{{$bloqueId}} {
        background-image: url({!! $img !!})
    }
</style>

@if($bloque->color_fondo)
<style>
    .colorBg-{{$bloqueId}} {
        background-color: {{ $bloque->color_fondo }} !important;
    }
</style>
@endif

<div class="container-fluid d-flex container6 imgBg bloqueDiv colorBg-{{$bloqueId}}" id="bloque-{{$bloqueId}}">
    <div class="row no-gutters flex-fill">
        <div class="col-12">
            <div class="content">
                <h1>{!! $bloque->getTranslate('home_titulo') !!}</h1>
                <p class="mt-4 mb-3">{!! $bloque->getTranslate('home_titulo2') !!}</p>
                @if($bloque->home_boton_activo)
                    <div class="linkContainer"><a {!! $bloque->home_enlace !!}>{!! $bloque->home_enlace_txt !!}</a></div>
                @endif
            </div>
        </div>
    </div>
</div>