<link rel="stylesheet" href="{{$assets}}/libs/css/bootstrap.min.css" defer>
<link rel="stylesheet" href="{{$assets}}/libs/css/slick.css" defer>
<link rel="stylesheet" href="{{$assets}}/libs/css/slick-theme.css" defer>
<link rel="stylesheet" href="{{$assets}}/scss/build/styles.css" defer>
<link rel="stylesheet" href="{{$assets}}/scss/build/menu.css" defer>

<style>
/*
.cc-cookies {
	position: fixed;
	width: 100%;
	left: 0;
	bottom: 0;
	padding: 0.5em 5%;
	background: rgba(0,0,0,0.95);
	color: #fff;
	font-size: 13px;
	font-weight: 700;
	z-index: 99999;
	text-align: left;
	color: #fff;
	padding: 30px 20px;
	letter-spacing:1px;
	
}
.cc-cookie-accept, .cc-cookie-decline {
	padding: 0px 6px;
	margin: 0 20px;
	text-transform:uppercase;
	border: 1px solid;
	float: right;
}
*/
/* .cc-cookie-accept {
	color: green;
}
.cc-cookie-decline {
	color: yellow;
} */

</style>

<meta name="csrf-token" content="{{ csrf_token() }}">

{!! ConfigHelper::config('code_header') !!}