<div class="container-fluid container2 p-0 m-0 bloqueDiv" id="bloque-{{$bloqueId}}">
    <div class="row no-gutters">
        <div class="col-12">
            <div id="carousel" class="slider">
                
                {{-- <div class="slider-item"> <img class="img-fluid mx-auto" src="{{$assets}}/assets/imgs/image1.jpg" /><div class="title text-center">El año de tu vida</div></div>
                <div class="slider-item"> <img class="img-fluid mx-auto" src="{{$assets}}/assets/imgs/image2.jpg" /><div class="title text-center">Iremos a Londres ?</div></div>
                <div class="slider-item"> <img class="img-fluid mx-auto" src="{{$assets}}/assets/imgs/image3.jpg" /><div class="title text-center">Cursos inmersivos</div></div>
                <div class="slider-item"> <img class="img-fluid mx-auto" src="{{$assets}}/assets/imgs/image4.jpg" /><div class="title text-center">Nativamente :D</div></div> --}}
               
                @foreach($slides as $slide)
                    @php
                        $img = $slide->imagen_webp ?: "$assets/assets/imgs/image1.jpg";
                        $txt = $slide->getTranslate('home_titulo') ?: $slide->name;
                    @endphp
                    <div class="slider-item"> <img class="img-fluid mx-auto" src="{{$img}}" /><div class="title text-center">{{$txt}}</div></div>
                @endforeach
              </div>
        </div>
    </div>
</div>