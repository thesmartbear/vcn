@extends('web.home2020.baseweb-menu')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
        @endif
        @stop

        @section('extra_head')
                <!-- Link Swiper's CSS -->
        <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/bs.css" rel="stylesheet">
        <link href="/assets/{{ConfigHelper::config('tema')}}/css/swiper/swiper.min.css" rel="stylesheet">

@stop

@section('container')

    <div class="headerbg">
        <div class="container" id="header">
            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-xs-10">
                    <div class="titulo">
                        <h1 class="slogan">
                            <span></span>
                            {{-- {{trans('web.catalogo')}} --}}
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="headerbgoverlay"></div> --}}
    </div>

    <?php
        $oficina = ConfigHelper::default_catalogos();

        $modal = null;
        if($oficina)
        {
            $modal = "data-toggle='modal' data-target='#modalCatalogo'";
        }
    ?>


<div id="app_catalogo_info">
     
    @if($oficina)

        <!-- Modal -->
        <div class="modal fade" id="modalCatalogo" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
                    </div>

                    <form action="#" method="post" id="frmOficina" v-on:submit.prevent="sendInfo">
                    <div class="modal-body">

                            <h2>@lang('web.catalogo_info.texto')</h2>

                            <div class="row">
                                
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div id="group-nombre" class="form-group">
                                        {!! Form::label('nombre', trans('web.catalogo_info.nombre')) !!}
                                        {!! Form::text('nombre', '', ['id'=>'nombre', 'class'=>'form-control']) !!}
                                    </div>
                                    <div id="group-telefono" class="form-group">
                                        {!! Form::label('telefono', '', trans('web.catalogo_info.telefono')) !!}
                                        {!! Form::tel('telefono', '', ['id'=>'telefono', 'class'=>'form-control']) !!}
                                    </div>
                                    <div id="group-email" class="form-group">
                                        {!! Form::label('email', '', trans('web.catalogo_info.email')) !!}
                                        {!! Form::email('email', '', ['id'=>'email', 'class'=>'form-control']) !!}
                                    </div>

                                    <div id="group-lopd" class="form-group">
                                        <input type="checkbox" id="lopd1" name="lopd1" data-rule='checked' data-msg="Por favor, acepte">
                                        <span>@lang('web.formulario.lopd1')</span>
                                        <br>
                                        <input type="checkbox" id="lopd2" name="lopd2" data-rule='checked' data-msg="Por favor, acepte">
                                        <span>@lang('web.formulario.lopd2', ['plataforma'=> ConfigHelper::plataformaApp()])</span>
                                    </div>

                                </div>

                            </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary enviar">Enviar</button>

                        <p class="text-center"><br /><small><a href="/proteccion-de-datos.html" target="_blank">@lang('web.lopd')</a></small></p>
                    </div>
                    </form>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    @endif
    
    <main class="cd-main-content">

        <div class="container addmarginbottom60" id="contenido">

            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-sm-8 col-xs-10 pagina">
                    @foreach(\VCN\Models\CMS\Catalogo::activos()->where('plataforma_id', ConfigHelper::config('propietario'))->where('idioma',App::getLocale())->get() as $cat)

                        <div class="col-sm-4">
                            <img class="img-responsive catalogo" src="{{$cat->imagen}}">
                            <h4>{{$cat->name}}</h4>

                            <p>
                                <?php
                                    $catalogos = \VCN\Models\CMS\Catalogo::activos()->where('grupo',$cat->grupo)->get();
                                ?>
                                @foreach($catalogos as $c)
                                    <i class="fa fa-download"></i>
                                    {!! ConfigHelper::catalogo_link($c->pdf, $c->name, ConfigHelper::getIdiomaContacto($c->idioma), $modal) !!}
                                    <br>
                                @endforeach
                            </p>

                        </div>

                    @endforeach
                </div>
            </div>

        </div>

    </main>
</div>

    <!-- Modal -->
    <div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
                </div>
                <div class="modal-body">
                    <div id="respuesta">
                        <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                        <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                            <div class="msg"></div>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                        <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                        <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                        <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                        <input type="hidden" id="curso" value="{{trans('web.catalogo')}}">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>

                    <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@stop

@section('extra_footer')
<script src="https://cdn.jsdelivr.net/vue/2.2.6/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.0/vue-resource.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.0/axios.min.js"></script>

<script type="text/javascript">
var vmCatalogo = new Vue({
    el: '#app_catalogo_info',
    data: function() {
        return {
            enviado: false,
            catalogo: "",
            idioma: '{{ConfigHelper::idioma()}}',
            nombre: null,
            telefono: null,
            email: null,
            href: null,
        };
    },
    mounted: function() {

    },
    destroyed () {

    },

    methods: {

        clickInfo: function(event) {

            // console.log(event);

            this.href = $(event.target).data('href');
            
            this.catalogo = $(event.target).data('info');
            this.idioma = $(event.target).data('lang');

            if(this.enviado)
            {
                // window.open(this.href, '_blank');
                this.sendInfo(event);
            }
            else
            {
                $('#modalCatalogo').modal('show');
            }
        },
        sendInfo: function(event) {

            if(!this.enviado)
            {
                var nom = $('#nombre').val();
                var tlf = $('#telefono').val();
                var email = $('#email').val();

                var lopd1 = $('#lopd1').prop('checked');
                var lopd2 = $('#lopd2').prop('checked');

                if(tlf=="" || email=="")
                {
                    if(tlf=="")
                    {
                        $('#group-telefono').addClass('has-error');
                    }

                    if(email=="")
                    {
                        $('#group-email').addClass('has-error');
                    }

                    if(!lopd1 || !lopd2)
                    {
                        $('#group-lopd span').css('color', '#a94442');
                    }

                    return;
                }

                var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
                if(tlf!="" && !filter.test(tlf))
                {
                    console.log(tlf);
                    $('#group-telefono').addClass('has-error');
                    return;
                }

                this.nombre = nom;
                this.telefono = tlf;
                this.email = email;

                $('#modalCatalogo').modal("hide");
            }

            data = {
                _token: '{{Session::token()}}',
                nombre: this.nombre, telefono: this.telefono, email: this.email,
                catalogo: this.catalogo,
                idioma: this.idioma,
            };

            gtag('event', 'catalogo', {
                'event_category': 'descarga',
                'event_label': this.idioma
            });

            this.$http.post('{{route('web.catalogo-info')}}', data).then(function (response) {
                console.log(response.data);
            });

            this.enviado = true;
            window.open(this.href, '_blank');
        }
        
    }
});
</script>

@stop