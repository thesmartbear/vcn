
<div class="menuContainer container-fluid {{$div ?? "d-none d-lg-none"}}">
    <div class="menu">
        
        @if($logo ?? true)
            {{-- <h3>BRITISH <b>SUMMER</b></h3> --}}
            <a id="cd-logo" href="/"><img src="/assets/logos/n-{{ConfigHelper::config('logoweb')}}" alt="{{ConfigHelper::config('nombre')}}" /></a>
        @endif 

        @include('web.home2020._menu-1', ['responsive'=> true])
        
        <div class="secondMenuContainer">
            <div class="secondMenu mt-5 text-left float-right">

                {{-- <form id="searchMenuResponsive" action="{{route('web.buscar')}}" method="post" enctype="multipart/form-data" class="w-100" autocomplete="off" >
                    <input type="text" placeholder="{{trans('web.buscar')}}" name="search"  id="searchInputResponsive" class="searchbox-input" required>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                </form> --}}
    
                <div class="clearfix"></div>
                
                <i class="fas fa-search searchBtnMenuResponsive"></i>
                
                @include('web.home2020._menu-2', ['responsive'=> true])

            </div>
        </div>

    </div>
</div>