@php
    $img = $bloque->imagen_webp ?: "$assets/assets/imgs/jumping-girl-dark.jpg";
@endphp

@if($bloque->color_fondo)
<style>
    .colorBg-{{$bloqueId}} {
        background-color: {{ $bloque->color_fondo }} !important;
    }
</style>
@endif

<div class="container-fluid d-flex container11 p-5 bloqueDiv" id="bloque-{{$bloqueId}}">
    <div class="row no-gutters flex-fill">
        <div class="col-1"></div>
        <div class="col-lg-5 pb-0 pt-4 pb-lg-5 pt-lg-5 contentImg">
            <img src="{{$img}}" class="img-fluid" />
        </div>              
        <div class="col-lg-5 d-flex pt-5 colorBg-{{$bloqueId}}">
            <div class="content">
                <h1>{!! $bloque->getTranslate('home_titulo') !!}</h1>
                <p class="mt-4">{!! $bloque->getTranslate('home_titulo2') !!}</p>
                {{-- <p class="mt-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta cumque, reiciendis dolorum temporibus aliquam ipsam, asperiores impedit eos facilis, possimus omnis dolor? Illo sunt autem accusantium aspernatur, maxime tempore iure?</p>
                <p class="mt-2 mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta cumque, reiciendis dolorum temporibus aliquam ipsam, asperiores impedit eos facilis, possimus omnis dolor? Illo sunt autem accusantium aspernatur, maxime tempore iure?</p> --}}
                @if($bloque->home_boton_activo)
                    <div class="linkContainer colorTxt noCentered"><a class="colorTxt" {!! $bloque->home_enlace !!}>{!! $bloque->home_enlace_txt !!}</a></div>
                @endif
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>
</div>