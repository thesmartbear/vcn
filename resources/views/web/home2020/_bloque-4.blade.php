@php
    $img = $bloque->imagen_webp ?: "$assets/assets/imgs/girl-sea.jpg";
@endphp

<style>
    #bloque-{{$bloqueId}} .imgBg {
        background-image: url({!! $img !!})
    }
</style>

@if($bloque->color_fondo)
<style>
    .colorBg-{{$bloqueId}} {
        background-color: {{ $bloque->color_fondo }} !important;
    }
</style>
@endif

<div class="container-fluid d-flex container3 bloqueDiv" id="bloque-{{$bloqueId}}">
    <div class="row flex-fill">
        <div class="col-12 col-lg-6 imgBg">
        </div>
        <div class="col-12 col-lg-4 rightSide1 colorBg-{{$bloqueId}}">
            <div class="content">
                <h1>{!! $bloque->getTranslate('home_titulo') !!}</h1>
                <p>{!! $bloque->getTranslate('home_titulo2') !!}</p>
                @if($bloque->home_boton_activo)
                <div class="linkContainer noCentered colorTxt">
                    <a class="colorTxt" {!! $bloque->home_enlace !!}>{!! $bloque->home_enlace_txt !!}</a>
                </div>
                @endif
            </div>
        </div>
        <div class="d-none d-lg-block col-lg-2 rightSide2 colorBg-{{$bloqueId}}"></div>
    </div>
</div>