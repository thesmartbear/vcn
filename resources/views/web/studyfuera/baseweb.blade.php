<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @section('title')
            {{ConfigHelper::config('nombre')}}
        @show
    </title>

    @yield('extra_meta')


    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100,400,700,700italic,400italic' rel='stylesheet' type='text/css'>

    <!-- ==============================================
		Favicons
		=============================================== -->
    <link rel="shortcut icon" href="/assets/{{ConfigHelper::config('tema')}}/favicon/favicon.ico">
    <link rel="apple-touch-icon" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon-114x114.png">


    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" />


    <!-- Font -->
    <link href='//fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/icons.css" rel="stylesheet" type="text/css" />


    <!-- font awesome -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />


    <link href="/assets/{{ConfigHelper::config('tema')}}/css/bootstrap-select.css" rel="stylesheet" type="text/css" />


    <link rel="stylesheet" href="/assets/{{ConfigHelper::config('tema')}}/css/magnific-popup.css"> <!-- Resource style -->
    <link rel="stylesheet" href="/assets/{{ConfigHelper::config('tema')}}/css/navstyle.css"> <!-- Resource style -->

    <script src="/assets/{{ConfigHelper::config('tema')}}/js/modernizr.js"></script> <!-- Modernizr -->

    <link rel="stylesheet" href="/assets/{{ConfigHelper::config('tema')}}/css/skin/sf.css">
    <link rel="stylesheet" href="/assets/{{ConfigHelper::config('tema')}}/css/webstyle.css">

    @yield('extra_head')

</head>
<body class="nav-is-fixed">

    @include('web._partials.google_body')

    <header class="cd-main-header">
        <a id="cd-logo" href="/"><img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" /></a>
        <nav id="cd-top-nav">
            <ul class="cd-header-buttons">
                <li><a href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal"><i class="fa fa-phone"></i> {{trans('web.solicitainfo')}}</a></li>
                <li><a href="/{{trans('web.contacto-slug')}}">{{trans('web.contacto')}}</a></li>
                @if (Auth::guest())
                    <li><a href="//studyfuera.estudiaryviajar.com/auth/login">{{trans('web.aclientes')}}</a></li>
                @else
                    <li><a href="//studyfuera.estudiaryviajar.com/manage">{{trans('web.aclientes')}}</a> | <a href="//studyfuera.estudiaryviajar.com/auth/login">{{trans('web.salir')}}</a></li>
                @endif
                <li><a class="cd-search-trigger" href="#cd-search">{{trans('web.buscar')}}<span></span></a></li>
            </ul> <!-- cd-header-buttons -->
        </nav>
        <nav id="cd-top-nav-small">
            <ul class="cd-header-buttons">
                <li><a href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal"><i class="fa fa-phone"></i></a></li>
                <li><a href="/contacto.html"><i class="fa fa-map-marker"></i></a></li>
                @if (Auth::guest())
                    <li><a href="//studyfuera.estudiaryviajar.com/auth/login">{{trans('web.aclientes')}}</a></li>
                @else
                    <li><a href="//studyfuera.estudiaryviajar.com/manage">{{trans('web.aclientes')}}</a> | <a href="//studyfuera.estudiaryviajar.com/auth/login">{{trans('web.salir')}}</a></li>
                @endif
                <li><a class="cd-search-trigger" href="#cd-search">{{trans('web.buscar')}}<span></span></a></li>
            </ul> <!-- cd-header-buttons -->
        </nav>
        <a id="cd-menu-trigger" href="#0"><span class="cd-menu-text">Menú</span><span class="cd-menu-icon"></span></a>
    </header>

@yield('container')

    <div class="cd-overlay"></div>

@include('web.studyfuera.includes.menu')



    <script src='https://code.jquery.com/jquery-2.1.1.min.js' type='text/javascript'></script>
    <script src='https://code.jquery.com/jquery-migrate-1.2.1.min.js' type='text/javascript'></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <!-- detect mobile -->
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/detectmobilebrowser.js" type="text/javascript"></script>

    <script type="text/javascript" src="/assets/{{ConfigHelper::config('tema')}}/js/bootstrap-select.js"></script>

    <script src="/assets/{{ConfigHelper::config('tema')}}/js/jquery.mobile.custom.min.js"></script>
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/main.js"></script> <!-- navigation -->

    <script src="/assets/{{ConfigHelper::config('tema')}}/js/jquery.magnific-popup.min.js"></script>

    @include('web._partials.cookies')
    @include('web._partials.google_head')

    @yield('extra_footer')

    @stack('scripts')

</body>
</html>