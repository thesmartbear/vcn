@extends('web.studyfuera.baseweb')
@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
@stop

@section('extra_head')
<!-- Color style -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/sf.css" rel="stylesheet">

        <style>
            :target:before {
                content:"";
                display:block;
                height:250px; /* fixed header height*/
                margin:-250px 0 0; /* negative fixed header height */
            }
        </style>
@stop


@section('container')
    <div class="headerbg" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/{{rand(1, 3)}}.png') repeat;">
        <div class="container" id="header">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo">
                        <h1 class="slogan">
                            <span></span>
                            {!! Traductor::getWeb(App::getLocale(), 'Pagina', 'name', $pagina->id, $pagina->titulo) !!}
                            <br />
                            <span></span>
                            <small>@if(Lang::has('web.'.$pagina->url.'-elige')){!! trans('web.'.$pagina->url.'-desc') !!}@endif</small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="headerbgoverlay"></div>
    </div>

    <main class="cd-main-content">
        <div class="container" id="contenido">
            <div class="row">
                <div class="col-sm-8 col-xs-12 pagina">
                    {!! Traductor::getWeb(App::getLocale(), 'Pagina', 'contenido', $pagina->id, $pagina->contenido) !!}
                </div>

                @include('web.studyfuera.includes.copyright')

                    <!-- Start right sidebar -->
                    <div class="col-sm-3 col-sm-offset-1 wrapper-bg" id="sidebar">
                        @if(Lang::has('web.categorias.'.$pagina->url.'-sidebar'))
                            <div class="widget clearfix filtros">
                                <div class="box">
                                    {!! trans('web.categorias.'.$pagina->url.'-sidebar') !!}
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- End right sidebar -->
            </div>
        </div>
    </main>
<!-- Modal -->
<div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
            </div>
            <div class="modal-body">
                <div id="respuesta">
                    <p>Llámanos al 93 200 88 88<br />o déjanos un teléfono de contacto</p><h4>Te ayudaremos a tomar la mejor decisión para que tu hijo/a aprenda inglés</h4>
                    <form action="plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                        <div class="msg"></div>
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="form-group">
                                    <label for="name">Nombre</label>
                                    <input type="text" class="form-control" id="name" placeholder="Nombre">
                                </div>
                                <div class="form-group">
                                    <label for="tel">Teléfono</label>
                                    <input type="text" class="form-control" id="tel" placeholder="teléfono de contacto">
                                    <input type="hidden" id="curso" value="{!! Traductor::getWeb(App::getLocale(), 'Pagina', 'titulo', $pagina->id, $pagina->titulo) !!}">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary enviar" id="plusinfoenviar" type="button">Enviar</button>

                <p class="text-center"><br /><small><a href="/aviso-legal.html#privacidad" target="_blank">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</a></small></p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('extra_footer')

<script type="text/javascript">
    $(document).ready(function() {
        $('.msg').hide();
        $("#plusinfoenviar").click(function() {
            console.log('validar');
            if ($('#name').val() == ''){
                $('.msg').html('Debes indicar un nombre de contacto');
                $('.msg').show();
                return false;
            }
            if ($('#email').val() == ''){
                $('.msg').html('Debes indicar un teléfono o un email de contacto');
                $('.msg').show();
                return false;
            }

            if ($('#email').val() != ''){
                var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
                if (re.test($('#name').val())) {
                    $('.msg').html('El email no tiene un formato correcto');
                    $('.msg').show();
                    return false
                }

            }

            if (!/^([0-9])*$/.test($('#tel').val())){
                $('.msg').html('El campo teléfono tiene que ser numérico');
                $('.msg').show();
                return false
            }


            post_data = {'name':$('#name').val(), 'tel':$('#tel').val(),'email':$('#email').val(), 'curso':$('#curso').val()};
            $.ajax({
                type: "POST",
                url: "/assets/{{ConfigHelper::config('tema')}}/includes/plusinfosend-{{ConfigHelper::config('sufijo')}}.php",
                data: post_data,
                success: function(msg){
                    $("#respuesta").html(msg);
                    $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                    ga('send', 'Solicitud', 'button', 'click', '{{$pagina->titulo}}}', 1);
                },
                error: function(){
                    alert("error!!");
                }
            });
        });


    });

    $('form input').blur(function () {
        $('.msg').hide();
    });
    </script>
@stop
