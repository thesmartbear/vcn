@extends('web.studyfuera.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{urldecode(trans('web.seo-'.ConfigHelper::config('sufijo').'.subject'))}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')
    <!-- Link Swiper's CSS -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/sf.css" rel="stylesheet">
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/swiper/swiper.min.css" rel="stylesheet">

@stop

@section('container')
    <main class="cd-main-content" style="padding-top: 0;">
        <div class="bghome">
            <?php $x = 1; ?>
            @foreach(\VCN\Models\Categoria::where(function ($query) {
                 return $query
                 ->where('propietario', 0)
                 ->orWhere('propietario', ConfigHelper::config('propietario'));
                 })->orderBy('id','ASC')->get() as $cat)

                @if(\VCN\Models\Cursos\Curso::where('category_id', $cat->id)->where('activo_web', 1)
                    ->where(function ($query) {
                        return $query
                        ->where('propietario', 0)
                        ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })->count())

                    <div class="bg-slide @if($x == 1) active @endif" data-index="{{$x}}" style="background: url('/assets/{{ConfigHelper::config('tema')}}/home/{{$cat->slug}}.jpg') no-repeat; background-position: center center; background-size: cover;"></div>
                    <? $x++; ?>
                @endif
            @endforeach
        </div>
        <!-- Swiper -->
        <div class="swiper-home">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php $x = 1; ?>
                    @foreach(\VCN\Models\Categoria::where(function ($query) {
                         return $query
                         ->where('propietario', 0)
                         ->orWhere('propietario', ConfigHelper::config('propietario'));
                         })->orderBy('id','ASC')->get() as $cat)

                        @if(\VCN\Models\Cursos\Curso::where('category_id', $cat->id)->where('activo_web', 1)
                            ->where(function ($query) {
                                return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                                })->count())

                                <div class="swiper-slide" data-index="{{$x}}" data-bg="/assets/{{ConfigHelper::config('tema')}}/home/{{$cat->slug}}.jpg"><h1><a class="seccion" href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}">{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'name_web', $cat->id, $cat->name_web)!!}</a><br /><a class="btn btn-white btn-outlined" href="/{!!Traductor::getWeb(App::getLocale(), 'Categoria', 'slug', $cat->id, $cat->slug)!!}">{!! trans('web.categorias.verprogramas') !!}</a></h1></div>
                            <? $x++; ?>
                        @endif
                    @endforeach
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </main>

    <!-- Modal -->
    <div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
                </div>
                <div class="modal-body">
                    <div id="respuesta">
                        <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                        <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                            <div class="msg"></div>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                        <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                        <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                        <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                        <input type="hidden" id="curso" value="home">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" >Enviar</button>

                    <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('extra_footer')
    <!-- Swiper JS -->
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/swiper/swiper.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            slidesPerView: {{$x-1}},
            spaceBetween: 0,
            breakpoints: {
                1171: {
                    slidesPerView: {{$x-1}},
                    spaceBetween: 0
                },
                1170: {
                    slidesPerView: 1,
                    spaceBetween: 0
                }
            }
        });


        function bgs() {
            if ($(window).width() < 1171) {
                $('.swiper-slide').each(function () {
                    $(this).css({'background-image': 'url("' + $(this).data('bg') + '")'});
                });
            } else {
                $('.swiper-slide').each(function () {
                    $(this).css({'background-image': 'none'});
                });
            }
        }

        $('.swiper-slide').on('mouseover', function () {
            indice = $(this).data('index');
            $('.bg-slide').each(function () {
                if($(this).data('index') == indice) {
                    $(this).addClass('active');
                }else{
                    $(this).removeClass('active');
                }
            });
        });

        $(document).ready(function () {
            bgs();
        });
        $(window).resize(function (){
           bgs();
        });


        $(document).ready(function () {
            $('.msg').hide();
            $("#plusinfoenviar").click(function () {
                //console.log('validar');
                if ($('#name').val() == '') {
                    $('.msg').html('Debes indicar un nombre de contacto');
                    $('.msg').show();
                    return false;
                }
                if ($('#email').val() == '') {
                    $('.msg').html('Debes indicar un teléfono o un email de contacto');
                    $('.msg').show();
                    return false;
                }

                if ($('#email').val() != '') {
                    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
                    if (re.test($('#name').val())) {
                        $('.msg').html('El email no tiene un formato correcto');
                        $('.msg').show();
                        return false
                    }

                }

                if (!/^([0-9])*$/.test($('#tel').val())) {
                    $('.msg').html('El campo teléfono tiene que ser numérico');
                    $('.msg').show();
                    return false
                }


                post_data = {
                    'name': $('#name').val(),
                    'tel': $('#tel').val(),
                    'email': $('#email').val(),
                    'curso': $('#curso').val()
                };
                $.ajax({
                    type: "POST",
                    url: "/assets/{{ConfigHelper::config('tema')}}/includes/plusinfosend-{{ConfigHelper::config('sufijo')}}.php",
                    data: post_data,
                    success: function (msg) {
                        //console.log(msg);
                        $("#respuesta").html(msg);
                        $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                        ga('send', 'Solicitud', 'button', 'click', 'home', 1);
                    },
                    error: function () {
                        alert("error!!");
                    }
                });
            });

        });

        $('form input').blur(function () {
            $('.msg').hide();
        });
    </script>

@stop
