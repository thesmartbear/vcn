@extends('web.studyfuera.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')
        <!-- Link Swiper's CSS -->
<link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/sf.css" rel="stylesheet">
<link href="/assets/{{ConfigHelper::config('tema')}}/css/swiper/swiper.min.css" rel="stylesheet">

@stop

@section('container')
    <div class="headerbg" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/wall4.png') repeat;">
        <div class="container" id="header" style="position: relative;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo">
                        <h1 class="slogan">
                            <span></span>
                            {{trans('web.catalogo')}}
                            <br />
                            <span></span>
                            <small>2017</small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="headerbgoverlay"></div>
    </div>

    <main class="cd-main-content">
        <div class="container" id="contenido">
                <div class="row addmargintop60">
                    <div class="col-sm-4 col-sm-offset-1">
                        <img class="img-responsive" src="/assets/catalogos/sf/studyfuera2017.jpg">
                    </div>
                    <div class="col-sm-4 col-sm-offset-1 addmargintop60">
                        <h4><i class="fa fa-download"></i> <a href="/assets/catalogos/sf/studyfuera2017.pdf" target="_blank">Descargar catálogo en pdf</a></h4>
                    </div>
                </div>
        </div>
    </main>

    <!-- Modal -->
    <div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
                </div>
                <div class="modal-body">
                    <div id="respuesta">
                        <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                        <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                            <div class="msg"></div>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                        <input type="text" class="form-control" id="name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                        <input type="text" class="form-control" id="tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                        <input type="text" class="form-control" id="email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                        <input type="hidden" id="curso" value="{{trans('web.catalogo')}}">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" >Enviar</button>

                    <p class="text-center"><br /><small>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.protecciondatos') !!}</small></p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('extra_footer')
@stop