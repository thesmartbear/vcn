<div class="col-sm-12">
    <div class="copyright" id="copy">
        <p>&copy; {{date('Y')}} StudyFuera es una marca comercial de Mextranjero, S. de R.L.</p>
        <ul class="legal">
            <li><a href="/aviso-legal.html">{{trans('web.aviso-legal')}}</a></li>
            <li><a href="/politica-de-cookies.html">{{trans('web.politica-de-cookies')}}</a></li>
        </ul>
    </div>
</div>