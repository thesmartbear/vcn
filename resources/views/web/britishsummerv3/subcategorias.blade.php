@extends('web.britishsummerv3.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} - {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) !!}. {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo) !!}
@stop

@section('extra_meta')
    <meta name="DC.title" lang="{{App::getLocale()}}" content="{{ConfigHelper::config('nombre')}} - {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_titulo', $categoria->id, $categoria->seo_titulo) !!}. {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_titulo', $subcategoria->id, $subcategoria->seo_titulo) !!}" />
    <meta name="subject" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_titulo', $categoria->id, $categoria->seo_titulo) !!}. {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_titulo', $subcategoria->id, $subcategoria->seo_titulo) !!}" />
    <meta name="description" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_descripcion', $subcategoria->id, $subcategoria->seo_descripcion) !!}" />
    <meta name="Keywords" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_keywords', $subcategoria->id, $subcategoria->seo_keywords) !!}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('extra_head')
    <!-- Color style -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/{{$clase}}.css" rel="stylesheet">
@stop


@section('container')
    <div class="headerbg" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/{{rand(1, 3)}}.png') repeat;">
        <div class="container" id="header" style="position: relative;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo">
                        <h1 class="slogan">
                            <span>
                                <ul class="breadcrumb">
                                    <li class="active"><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $categoria->id, $categoria->seo_url) !!}">{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) !!}</a></li>
                                </ul>
                            </span>
                            {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo) !!}
                            <small>@if(Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_corta', $subcategoria->id, $subcategoria->desc_corta)){!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_corta', $subcategoria->id, $subcategoria->desc_corta) !!}@endif</small>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="headerbgoverlay"></div>
    </div>
    <main class="cd-main-content">
        <div class="container" id="contenido">
            <div class="row">
                <div class="col-sm-8 col-xs-12 {{$clase}} categorias">
                    <div class="introseccion">
                            @if(Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'descripcion', $subcategoria->id, $subcategoria->descripcion)){!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'descripcion', $subcategoria->id, $subcategoria->descripcion) !!}@endif
                    </div>
                    @foreach($subcategoria->hijos_activos->sortBy('orden') as $scd)
                        @if($scd->numcursos > 0)
                            <a href="{{Request::url()}}/{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $scd->id, $scd->seo_url) !!}">
                                <div class="col-sm-12 col-xs-12 categoria">
                                    <h3>
                                        {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $scd->id, $scd->titulo) !!}
                                        <small>@if(Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_corta', $scd->id, $scd->desc_corta)){!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_corta', $scd->id, $scd->desc_corta) !!}@endif</small>
                                    </h3>
                                    <p><small>{{$scd->numcursos}} cursos</small></p>
                                </div>
                            </a>
                        @endif
                    @endforeach


                    </div>
                    <!-- Start right sidebar -->
                    <div class="col-sm-3 col-sm-offset-1 wrapper-bg" id="sidebar">
                        @if(Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_lateral', $subcategoria->id, $subcategoria->desc_lateral))
                            <div class="widget clearfix filtros">
                                <div class="box">
                                    {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_lateral', $subcategoria->id, $subcategoria->desc_lateral) !!}
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- End right sidebar -->
                </div>

            <div class="row">
                    @include('web.britishsummerv3.includes.copyright')
            </div>
        </div>
    </main>


    <!-- Modal -->
    <?php
        $hidden = Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) ." - ". Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $subcategoria->id, $subcategoria->titulo);
    ?>
    @include('web._partials.plusinfomodal', ['hidden'=> $hidden])

@stop