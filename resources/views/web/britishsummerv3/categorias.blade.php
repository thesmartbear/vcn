@extends('web.britishsummerv3.baseweb')

@section('extra_head')
        <!-- Color style -->
    <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/{{$clase}}.css" rel="stylesheet">

    @if(in_array('4',explode(',',$categoria->categorias)))
        <style>
            :target:before {
                content:"";
                display:block;
                height:250px; /* fixed header height*/
                margin:-250px 0 0; /* negative fixed header height */
            }

            .affix{
                top: 300px;
                width: 100%;
                position: fixed;
            }
            .affix-top{
                position: static;
            }
            .affix-bottom {
                position: absolute;
            }
        </style>
    @endif
@stop


@section('title')
    {{ConfigHelper::config('name')}} - {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) !!}
@stop

@section('extra_meta')
    <meta name="DC.title" lang="{{App::getLocale()}}" content="{{ConfigHelper::config('nombre')}} - {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_titulo', $categoria->id, $categoria->seo_titulo) !!}" />
    <meta name="subject" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_titulo', $categoria->id, $categoria->seo_titulo) !!}" />
    <meta name="description" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_descripcion', $categoria->id, $categoria->seo_descripcion) !!}" />
    <meta name="Keywords" lang="{{App::getLocale()}}" content="{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_keywords', $categoria->id, $categoria->seo_keywords) !!}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('container')

<div class="headerbg" style="background: url('/assets/{{ConfigHelper::config('tema')}}/img/patterns/wall4.png') repeat;">
    <div class="container" id="header" style="position: relative;">
        <div class="row">
            <div class="col-xs-12">
                <div class="titulo">
                    <h1 class="slogan">
                        <span></span>
                        {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo) !!}
                        <br />
                        <span></span>
                        <small>{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_corta', $categoria->id, $categoria->desc_corta) !!}</small>
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="headerbgoverlay"></div>
</div>
<main class="cd-main-content">
    <div class="container" id="contenido">
        <div class="row">
            <div class="col-sm-8 col-xs-12 {{$clase}} categorias" @if(in_array('4',explode(',',$categoria->categorias))) style="margin-top: 0;" @endif>
                @if(!in_array('4',explode(',',$categoria->categorias)))
                    @if($categoria->descripcion)
                    <div class="introseccion">
                        {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'descripcion', $categoria->id, $categoria->descripcion) !!}
                    </div>
                    @endif
                @endif

                @foreach($categoria->hijos_activos->sortBy('orden') as $sc)
                    @if($sc->numcursos > 0)
                        <a href="{{Request::url()}}/{!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $sc->id, $sc->seo_url) !!}" class="titular">
                            <div class="col-sm-12 col-xs-12 categoria">
                                <h3>
                                    {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $sc->id, $sc->titulo) !!}
                                    <small>@if($sc->desc_corta) {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_corta', $sc->id, $sc->desc_corta) !!} @endif</small>
                                </h3>
                                <p><small>{{$sc->numcursos}} cursos</small></p>
                            </div>
                        </a>
                    @endif
                @endforeach

                <div class="row">
                    <div class="col-xs-12">
                        @if(in_array('4',explode(',',$categoria->categorias)))
                            @if($categoria->descripcion)
                                <div class="introseccion">
                                    {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'descripcion', $categoria->id, $categoria->descripcion) !!}
                                </div>
                            @endif
                        @endif
                    </div>
                </div>

                <h4>
                    <br>
                    {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_cursos', $categoria->id, $categoria->desc_cursos) !!}
                </h4>

            </div>
            
            <!-- Start right sidebar -->
            <div class="col-sm-3 col-sm-offset-1 wrapper-bg {{$clase}}" id="sidebar">
                @if($categoria->desc_lateral)
                    <div class="widget clearfix filtros">
                        <div class="box">
                            {!! Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'desc_lateral', $categoria->id, $categoria->desc_lateral) !!}
                        </div>
                    </div>
                @endif
            </div>
            <!-- End right sidebar -->
        </div>
        <div class="row">
            @include('web.britishsummerv3.includes.copyright')
        </div>
    </div>
</main>


<!-- Modal -->
@include('web._partials.plusinfomodal', ['hidden'=> Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $categoria->id, $categoria->titulo)])

@stop

@section('extra_footer')

    <script type="text/javascript">
        
        @if(in_array('4',explode(',',$categoria->categorias)))
            $(".filtros .box ul").affix({
                offset: {
                    top: 0,
                }
            });
        @endif

    </script>

@stop
