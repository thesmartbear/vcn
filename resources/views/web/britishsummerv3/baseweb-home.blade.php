<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @section('title')
            {{ConfigHelper::config('nombre')}}
        @show
    </title>

    @yield('extra_meta')

    @php
        $tema = ConfigHelper::config('tema');
    @endphp

    @if(isset($weblangs))
        @foreach ($weblangs as $weblang => $linklang)
            <link rel="alternate" hreflang="{{$weblang}}" href="{{$linklang}}" />
        @endforeach
    @endif

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100,400,700,700italic,400italic' rel='stylesheet' type='text/css'>
    {{-- <link rel="preload" href="https://fonts.googleapis.com/css?family=Lato:100,400,700,700italic,400italic" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,400,700,700italic,400italic"></noscript> --}}

    {{--
    <link rel="preload" href="stylescss" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="stylescss"></noscript>
    --}}

    <!-- ==============================================
		Favicons
		=============================================== -->
    <link rel="shortcut icon" href="/assets/{{$tema}}/favicon/favicon{{ConfigHelper::config('sufijo')}}.ico">
    <link rel="apple-touch-icon" href="/assets/{{$tema}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/{{$tema}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/{{$tema}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}-114x114.png">


    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" />
    {{-- <link rel="preload" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"></noscript> --}}


    <!-- Font -->
    <link href='//fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    {{-- <link rel="preload" href="//fonts.googleapis.com/css?family=Pacifico" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="//fonts.googleapis.com/css?family=Pacifico"></noscript> --}}
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
    {{-- <link rel="preload" href="//fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700"></noscript> --}}
    <link href='//fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
    {{-- <link rel="preload" href="//fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100"></noscript> --}}
    <link href="/assets/{{$tema}}/css/bs2015.css" rel="stylesheet" type="text/css" />
    {{-- <link rel="preload" href="/assets/{{$tema}}/css/bs2015.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="/assets/{{$tema}}/css/bs2015.css"></noscript> --}}

    <link rel="stylesheet" media="all" href="/assets/{{$tema}}/css/jquery-jvectormap.css"/>
    {{-- <link rel="preload" href="/assets/{{$tema}}/css/jquery-jvectormap.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="/assets/{{$tema}}/css/jquery-jvectormap.css"></noscript> --}}


    <!-- font awesome -->
    {{-- <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> --}}

    {{-- <link href="/assets/{{$tema}}/css/bootstrap-select.css" rel="stylesheet" type="text/css" /> --}}
    {{-- <link href="/assets/{{$tema}}/css/bootstrap-select.css" rel="stylesheet" type="text/css" /> --}}

    {{-- <link rel="stylesheet" href="/assets/{{$tema}}/css/magnific-popup.css"> <!-- Resource style --> --}}
    
    <link rel="stylesheet" href="/assets/{{$tema}}/css/navstyle.css"> <!-- Resource style -->
    {{-- <link rel="preload" href="/assets/{{$tema}}/css/navstyle.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="/assets/{{$tema}}/css/navstyle.css"></noscript> --}}


    <link rel="stylesheet" href="/assets/{{$tema}}/css/webstyle.css">
    {{-- <link rel="preload" href="/assets/{{$tema}}/css/webstyle.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="/assets/{{$tema}}/css/webstyle.css"></noscript> --}}

    <link rel="stylesheet" href="/assets/outdatedbrowser/outdatedbrowser.min.css">
    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'user' => Auth::user(),
            'csrfToken' => csrf_token(),
            'vapidPublicKey' => config('webpush.vapid.public_key'),
            'pusher' => [
                'key' => config('broadcasting.connections.pusher.key'),
                'cluster' => config('broadcasting.connections.pusher.options.cluster'),
            ],
        ]) !!};
    </script>
    
    @yield('extra_head')

</head>
<body class="nav-is-fixed">

    @include('web._partials.google_body')

    @section('header')
    <header class="cd-main-header">
        <a id="cd-logo" href="/"><img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" /></a>
        <nav id="cd-top-nav">
            <ul class="cd-header-buttons">
                <li><a href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal"><i class="fa fa-phone"></i> {{trans('web.solicitainfo')}}</a></li>
                
                {{-- @if(ConfigHelper::config('chat'))
                    <li class="hidden-xs hidden-sm">
                        @include('chat.web')
                    </li>
                @else
                
                    @if(ConfigHelper::config('propietario') == 1)
                        @if(App::getLocale() == 'es')
                            <li class="hidden-xs hidden-sm"><a href="//www.britishsummer.com/webim/client.php?locale=sp&style=britishsummer" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('//www.britishsummer.com/webim/client.php?locale=sp&amp;style=britishsummer&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=800,height=600,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;"><i class="fa fa-comments"></i> {{trans('web.chat')}}</a></li>
                        @else
                            <li class="hidden-xs hidden-sm"><a href="//www.britishsummer.com/webim/client.php?locale={{App::getLocale()}}&style=britishsummer" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('//www.britishsummer.com/webim/client.php?locale={{App::getLocale()}}&amp;style=britishsummer&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=800,height=600,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;"><i class="fa fa-comments"></i> {{trans('web.chat')}}</a></li>
                        @endif
                    @endif

                @endif --}}

                <li><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{{trans('web.catalogo-slug')}}">{{trans('web.catalogo')}}</a></li>
                <li><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{{trans('web.contacto-slug')}}">{{trans('web.contacto')}}</a></li>
                <li><a class="cd-search-trigger" href="#cd-search">{{trans('web.buscar')}}<span></span></a></li>
            </ul> <!-- cd-header-buttons -->
        </nav>
        <nav id="cd-top-nav-small">
            <ul class="cd-header-buttons">
                <li><a href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal"><i class="fa fa-phone"></i></a></li>
                <li><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{{trans('web.catalogo-slug')}}"><i class="fa fa-file-pdf-o"></i></a></li>
                <li><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{{trans('web.contacto-slug')}}"><i class="fa fa-map-marker"></i></a></li>
                <li><a class="cd-search-trigger" href="#cd-search">{{trans('web.buscar')}}<span></span></a></li>
            </ul> <!-- cd-header-buttons -->
        </nav>
        <a id="cd-menu-trigger" href="#0"><span class="cd-menu-text">Menu</span><span class="cd-menu-icon"></span></a>
    </header>
    @show

@yield('container')

    <div class="cd-overlay"></div>

    @include('web.britishsummerv3.includes.menu'.ConfigHelper::config('sufijo'))

    <div id="outdated"></div>
    @if(Session::get('redirected') == true)
   <!-- Modal -->
    <div class="modal fade" id="redirected" tabindex="-1" role="dialog" aria-labelledby="redirected" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <img class="modal-logo" src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" />
                </div>
                <div class="modal-body">
                    <div id="respuesta">
                        <h2>Oops!</h4>
                            <div class="row">
                                <div class="col-sm-12">
                                    El programa que estabas buscando ya no existe, pero te ofrecemos una selección que puede interesarte.
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal" type="button">cerrar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endif

    <script src="/assets/{{$tema}}/js/modernizr.js"></script> <!-- Modernizr -->

    <script src="/assets/outdatedbrowser/outdatedbrowser.min.js"></script>

    <script src='https://code.jquery.com/jquery-2.1.1.min.js' type='text/javascript'></script>
    <script src='https://code.jquery.com/jquery-migrate-1.2.1.min.js' type='text/javascript'></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <!-- detect mobile -->
    <script src="/assets/{{$tema}}/js/detectmobilebrowser.js" type="text/javascript"></script>

    <script type="text/javascript" src="/assets/{{$tema}}/js/bootstrap-select.js"></script>

    <script src="/assets/{{$tema}}/js/jquery.mobile.custom.min.js"></script>
    <script src="/assets/{{$tema}}/js/main.js"></script> <!-- navigation -->

    <script src="/assets/{{$tema}}/js/jquery.magnific-popup.min.js"></script>

    @include('web._partials.cookies')
    @include('web._partials.google_head')

    @yield('extra_footer')

    <script type="text/javascript">

        @if(Session::get('redirected') == true)
        $('#redirected').modal('show');
        @endif


        $('.language').on('click', function(e){
            e.preventDefault();
            var idioma = $(this).data('idioma');
            var segments = $(this).attr('href').split('{{ConfigHelper::config('web')}}');
            var codigo = segments[1];
            console.log(segments);
            console.log(codigo);

            if(codigo.includes('/es') || codigo.includes('/ca')) {
                $(location).attr('href', $(this).attr('href'));
            }else{
                var url = segments[0] + '{{ConfigHelper::config('web')}}/' + idioma + segments[1];
                $(location).attr('href', url);

            }
        });
    </script>

    {{-- <script src="{{mix('/js/app.js')}}"></script> --}}
    @stack('scripts')

</body>
</html>