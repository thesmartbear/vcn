@extends('web.britishsummerv3.baseweb-home')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop



<?php
$i = 1;
$slides = '';
$bgslides = '';
$video = '';
$inpromo = array();

// $promos = [];

if(!empty($promos)){
    foreach($promos as $promo){
        $style = '';
        $target = '';

        if($i == 1){
            $active = ' active';
        }else{
            $active = '';
        }
        if($promo->seccion_id == 0 ){
            $promoimg = $promo->panel_imagen;
            $promourl = Traductor::getWeb(App::getLocale(), 'Promo', 'panel_url', $promo->id, $promo->panel_url);
            $promotitulo = Traductor::getWeb(App::getLocale(), 'Promo', 'panel_titulo', $promo->id, $promo->panel_titulo);
            if($promo->targetblank_panel != 0){
                $target = ' target="_blank"';
            }

            if($promoimg != ''){
                $style = ' style="background: url('.$promoimg.') no-repeat; background-position: center center; background-size: cover;"';
            }else{
                if($promo->panel_color != ''){
                    $style= ' style="background: '.$promo->panel_color.';"';
                }else{
                    $style = '';
                }
            }

        }elseif(isset($promo->seccion)){
            $inpromo[] = $promo->seccion_id;
            if($promo->panel_imagen != ''){
                $promoimg = $promo->panel_imagen;
            }else if($promo->panel_imagen == '' && $promo->panel_color == ''){
                $promoimg = $promo->seccion->imagen;
            }else{
                $promoimg = '';
            }
            $promourl = Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $promo->seccion->id, $promo->seccion->seo_url);
            $promotitulo = Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $promo->seccion->id, $promo->seccion->titulo);
        }



        if(isset($promo->seccion)){
            if($promo->video_url != ''){
                $video .= '<video playsinline autoplay muted loop poster="'.$promoimg.'" id="bgvid">
                               <source src="'.Traductor::getWeb(App::getLocale(), 'Promo', 'video_url', $promo->id, $promo->video_url).'" type="video/mp4">
                           </video>';
            }else if($promo->seccion->id != 0 && $promo->seccion->video_url != ''){
                $video .= '<video playsinline autoplay muted loop poster="'.$promoimg.'" id="bgvid">
                                <source src="'.Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'video_url', $promo->seccion->id, $promo->seccion->video_url).'" type="video/mp4">
                            </video>';
            }else{
                $video ='';
            }

            if($promo->panel_color != ''){
                $video ='';
            }


            if($promoimg != ''){
                $style = ' style="background: url('.$promoimg.') no-repeat; background-position: center center; background-size: cover;"';
            }else{
                if($promo->panel_color != ''){
                    $style= ' style="background: '.$promo->panel_color.';"';
                }elseif($promo->seccion->color){
                    $style= ' style="background: '.$promo->seccion->color.';"';
                }else{
                    $style = '';
                }
            }
        }


        $bgslides .= '<div class="bg-slide'.$active.'" data-index="'.$i.'"'.$style.'>';
        $bgslides .= $video;
        $bgslides .= '</div>';


        $slides .= '<div class="swiper-slide swiper-slide-double'.$active.' " data-index="'.$i.'" data-bg="'.$promoimg.'">';
            $slides .= '<div class="slogan">';
            if($promo->promo1_titulo != ''){
                $slides .= '<div class="sloganitem"><h2>'.Traductor::getWeb(App::getLocale(), 'Promo', 'promo1_titulo', $promo->id, $promo->promo1_titulo).'</h2>';
                if($promo->promo1_url != ''){
                    $slides .= '<a class="btn btn-white btn-outlined btn-promo" href="'.Traductor::getWeb(App::getLocale(), 'Promo', 'promo1_url', $promo->id, $promo->promo1_url).'">'.trans('web.categorias.ver').'</a>';
                }
                $slides .= '</div>';
            }
            if($promo->promo2_titulo != ''){
                $slides .= '<div class="sloganitem"><h2>'.Traductor::getWeb(App::getLocale(), 'Promo', 'promo2_titulo', $promo->id, $promo->promo2_titulo).'</h2>';
                if($promo->promo2_url != ''){
                    $slides .= '<a class="btn btn-white btn-outlined btn-promo" href="'.Traductor::getWeb(App::getLocale(), 'Promo', 'promo2_url', $promo->id, $promo->promo2_url).'">'.trans('web.categorias.ver').'</a>';
                }
                $slides .= '</div>';
            }
            if($promo->promo3_titulo != ''){
                $slides .= '<div class="sloganitem"><h2>'.Traductor::getWeb(App::getLocale(), 'Promo', 'promo3_titulo', $promo->id, $promo->promo3_titulo).'</h2>';
                if($promo->promo3_url != ''){
                    $slides .= '<a class="btn btn-white btn-outlined btn-promo" href="'.Traductor::getWeb(App::getLocale(), 'Promo', 'promo3_url', $promo->id, $promo->promo3_url).'">'.trans('web.categorias.ver').'</a>';
                }
                $slides .= '</div>';
            }
            $slides .= '</div>';
            
            if($promo->seccion_id != 0 ){
                $slides .= '
                    <h2 class="h1">
                        <a class="seccion seccion-promo" href="/'.$promourl.'">'.$promotitulo.'</a>
                        <br />
                        <a class="btn btn-white btn-outlined" href="/'.$promourl.'">'.trans('web.categorias.verprogramas').'</a>
                    </h2>';
            }else{
                $slides .= '
                    <h2 class="h1">
                        <a class="seccion seccion-promo" href="'.$promourl.'"'.$target.'>'.$promotitulo.'</a>
                        <br />
                        <a class="btn btn-white btn-outlined" href="'.$promourl.'"'.$target.'>'.trans('web.categorias.ver').'</a>
                    </h2>';
            }
        $slides .= '</div>';
        $i++;
    }
}

$i = count($promos)+1;




foreach ($webcats as $wc){
    $video = '';
    $tag = "h1";
    if($i == 1){
        $active = ' active';
    }else{
        $active = '';
        $tag = "h2";
    }

    if($wc->nivel == 1 && !in_array($wc->id,$inpromo)){
        if ($wc->es_link == 1){
            $url = Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'link', $wc->id, $wc->link);
        }else{
            $url = Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $wc->id, $wc->seo_url);
        }
        if ($wc->link_blank == 1){
            $link = 'href="'.$url.'" target="_blank"';
        }else{
            $link = 'href="'.$url.'"';
        }
        if($wc->video_url != ''){
            $video .= '<video playsinline autoplay muted loop poster="'.$wc->imagen_webp.'" id="bgvid">
                            <source src="'.Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'video_url', $wc->id, $wc->video_url).'" type="video/mp4">
                        </video>';
        }
        $bgslides .= '<div class="bg-slide'.$active.'" data-index="'.$i.'" style="background: url('.$wc->imagen_webp.') no-repeat; background-position: center center; background-size: cover;">'.$video.'</div>';
        $slides .= '<div class="swiper-slide'.$active.'" data-index="'.$i.'" data-bg="'.$wc->imagen_webp.'"><'.$tag.' class="h1"><a class="seccion" '.$link.'>'.Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'name', $wc->id, $wc->name).'</a><br /><a class="btn btn-white btn-outlined" '.$link.'>'.trans('web.categorias.verprogramas').'</a></'.$tag.'></div>';
        $i++;
    }
}

    $slidesnum = $i-1;
    $slidesnum = $slidesnum ?: 1;
    if(!empty($promos) && count($promos) == 1){
        $swiperslide = 100/($slidesnum+1);
        $swiperslidedouble = (100/($slidesnum+1))*2;
    }else{
        $swiperslide = 100/$slidesnum;
        $swiperslidedouble = 100/$slidesnum;
    }
    
?>

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{urldecode(trans('web.seo-'.ConfigHelper::config('sufijo').'.subject'))}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
        @endif
        @stop

        @section('extra_head')
                <!-- Link Swiper's CSS -->
        <link href="/assets/{{ConfigHelper::config('tema')}}/css/skin/bs.css" rel="stylesheet">
        <link href="/assets/{{ConfigHelper::config('tema')}}/css/swiper/swiper.min.css" rel="stylesheet">
    <style>

        .swiper-slide{
            width: {{$swiperslide}}%;
        }
        .swiper-slide-double{
            width: {{$swiperslidedouble}}%;
        }


        video#bgvid {
            position: fixed;
            top: 50%;
            left: 50%;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: -100;
            -ms-transform: translateX(-50%) translateY(-50%);
            -moz-transform: translateX(-50%) translateY(-50%);
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
            background-size: cover;
        }

        @media screen and (max-device-width: 767px) {
            #bgvid {
                display: none;
            }
        }


    </style>

@stop

@section('container')
    <main class="cd-main-content" style="padding-top: 0;">

        <div class="bghome">
            {!! $bgslides !!}
        </div>
        <!-- Swiper -->
        <div class="swiper-home">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    {!! $slides !!}
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </main>

    @include('web._partials.plusinfomodal', ['hidden'=> 'home'])

    @stop

    @section('extra_footer')
    <!-- Swiper JS -->
    <script src="/assets/{{ConfigHelper::config('tema')}}/js/swiper/swiper.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            slidesPerView: 'auto',
            spaceBetween: 0,
            breakpoints: {
                1171: {
                    slidesPerView: {{$slidesnum}},
                    spaceBetween: 0
                },
                1170: {
                    slidesPerView: 1,
                    spaceBetween: 0
                }
            }
        });




        $('.swiper-slide').on('mouseover', function () {
            indice = $(this).data('index');
            $('.swiper-slide').removeClass('active');
            $(this).addClass('active');
            $('.bg-slide').each(function () {
                if($(this).data('index') == indice) {
                    $(this).addClass('active');
                }else{
                    $(this).removeClass('active');
                }
            });
        });

        $('.swiper-slide').on('mouseout', function () {
            @if(!empty($promo))
                indice = 1; //$('.swiper-slide-double').data('index');
            @else
                indice = 0;
            @endif
            $(this).removeClass('active');
            $('.swiper-slide[data-index='+indice+']').addClass('active');
            $('.bg-slide').each(function () {
                if($(this).data('index') == indice) {
                    $(this).addClass('active');
                }else{
                    $(this).removeClass('active');
                }
            });
        });

        $(document).ready(function () {
            bgs();
        });
        $(window).resize(function (){
            bgs();
            if($(window).width() > 1171) {
                $('.swiper-slide').css({'width': '{{$swiperslide}}%'});
                $('.swiper-slide-double').css({'width': '{{$swiperslidedouble}}%'});
                swiper.updateSlidesSize();
            }
        });

        var vid = document.getElementById("bgvid");
        function vidFade() {
            vid.classList.add("stopfade");
        }
        vid.addEventListener('ended', function() {
            vid.play();
            // to capture IE10
            vidFade();
        });

        var isMobile = false;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Opera Mobile|Kindle|Windows Phone|PSP|AvantGo|Atomic Web Browser|Blazer|Chrome Mobile|Dolphin|Dolfin|Doris|GO Browser|Jasmine|MicroB|Mobile Firefox|Mobile Safari|Mobile Silk|Motorola Internet Browser|NetFront|NineSky|Nokia Web Browser|Obigo|Openwave Mobile Browser|Palm Pre web browser|Polaris|PS Vita browser|Puffin|QQbrowser|SEMC Browser|Skyfire|Tear|TeaShark|UC Browser|uZard Web|wOSBrowser|Yandex.Browser mobile/i.test(navigator.userAgent)) {
            isMobile = true;
        }
        $(document).ready(function(){
            if (isMobile == true){
                $('#bgvid').css({'display':'none'});
            }else{
                $('#bgvid').css({'display':'block'});
            }
        });

        function bgs() {
            if ($(window).width() < 1171) {
                $('.swiper-slide').each(function () {
                    $(this).css({'background-image': 'url("' + $(this).data('bg') + '")'});
                });
            }else {
                $('.swiper-slide').each(function () {
                    $(this).css({'background-image': 'none'});
                });
            }
        }

    </script>

@stop