@extends('layouts.base')

@section('title')
    {{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}
@stop

@section('extra_meta')
    <meta name="DC.title" content="{{ConfigHelper::config('nombre')}} · {{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Subject" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.subject')}}" />
    <meta name="Description" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.'.ConfigHelper::config('sufijo').'description')}}" />
    <meta name="Keywords" content="{{trans('web.seo-'.ConfigHelper::config('sufijo').'.keywords')}}" />
    @if(ConfigHelper::config('propietario') == 2)
        <meta name="robots" content="NOINDEX, NOFOLLOW" />
    @endif
@stop

@section('container')

    <style>
        .container {
            vertical-align: middle;
            font-family: 'Lato';
        }

        .content {
            display: inline-block;
        }

        .title {
            font-size: 96px;
            margin-bottom: 40px;
        }

        .quote {
            font-size: 24px;
        }
    </style>

    <div class="container">
        <div class="content">
            <div class="title">{!! Html::image('/assets/logos/'.App::getLocale().'/'.ConfigHelper::config('logoweb'), ConfigHelper::config('nombre'), array('class' => 'logo-default')) !!}</div>
{{--
            @foreach($menuWeb as $m1)
                @if($m1['route'])
                    {!! Html::linkRoute($m1['route'], trans($m1['nom']), $m1['view']) !!}
                @else
                    {!! Html::link($m1['url'], trans($m1['nom']), $m1['view']) !!}
                @endif
            @endforeach
--}}
            {{-- {!! Html::linkRoute('web.view', 'Translated title', ['page-slug']) !!} --}}

            <hr>

            <style type="text/css" media="screen">
                .navbar-locales .active { text-decoration: underline; }
            </style>

            Idiomas:

            <ul class="navbar-locales">
                <li class="{{App::getLocale()=='ca'?'active':''}}"  style="display: inline-block;">
                    <a href="/ca" rel="alternate" hreflang="ca">
                        Català
                    </a>
                </li>
                <li class="{{App::getLocale()=='es'?'active':''}}"  style="display: inline-block;">
                    <a href="/es" rel="alternate" hreflang="es">
                        Español
                    </a>
                </li>
            </ul>

            <ul style="margin-top: 60px;">
                @foreach(\VCN\Models\Categoria::where('propietario',0)->orWhere('propietario',ConfigHelper::config('propietario'))->get() as $categoria)
                    <li><strong><a href="{{$categoria->slug}}">{{$categoria->name_web}}</strong></a></li>
                @endforeach
            </ul>
        </div>
    </div>
@stop
