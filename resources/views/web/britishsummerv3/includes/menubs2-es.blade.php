<div id="nav-utilities">
    <ul>
        <li><i class="fa fa-info-circle"></i> Solicita información</li>

        <li><i class="fa fa-comments"></i> Chat</li>

        <li><i class="fa fa-download"></i> <a href="catalogos.html">Catálogos</a></li>

        @if(ConfigHelper::config('web') == Request::getHost())
            @if (Auth::guest())
                <li><i class="fa fa-lock"></i> <a href="//{{ConfigHelper::config('sufijo')}}.estudiaryviajar.com/auth/login">Admin</a></li>
            @else
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="ifa fa fa-sign-out fa-fw"></i> {{ trans('web.salir') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            @endif
        @else
            <li><a href="#">Área de Clientes</a></li>
        @endif
    </ul>
</div>
<nav class="cd-nav">
    <ul id="cd-primary-nav" class="cd-primary-nav is-fixed">
        <li class="has-children">
            <a href="/aprender-ingles-en-el-extranjero">Inglés en el extranjero</a>
            <ul class="cd-secondary-nav is-hidden">
                <li class="go-back"><a href="#0">Menu</a></li>
                <li class="see-all"><a href="/aprender-ingles-en-el-extranjero">APRENDER INGL&Eacute;S EN EL EXTRANJERO<span>Todas las edades</span></a></li>

                    @foreach(\VCN\Models\Categoria::whereIn('id', [1, 2, 7])
                        ->where(function ($query) {
                            return $query
                                ->where('propietario', 0)
                                ->orWhere('propietario', ConfigHelper::config('propietario'));
                        })->orderBy('id','ASC')->get() as $cat)

                        @if(count($cat->subcategorias))
                            <li class="has-children">
                                <a href="/aprender-ingles-en-el-extranjero/{{$cat->slug}}">{{$cat->name_web}}</a>

                                    <ul class="is-hidden">
                                        <li class="go-back"><a href="#0">{{$cat->name_web}}</a></li>
                                        <li class="see-all"><a href="/aprender-ingles-en-el-extranjero/{{$cat->slug}}">Ver todos los programas</a></li>
                                        @foreach($cat->subcategorias as $subcat)
                                            @if(count($subcat->subcategoriasdet))
                                                <li class="has-children">
                                                    <a href="#0">{{$subcat->name_web}}</a>
                                                    <ul class="is-hidden">
                                                        <li class="go-back"><a href="#0">{{$subcat->name_web}}</a></li>
                                                        <li class="see-all"><a href="/aprender-ingles-en-el-extranjero/{{$cat->slug}}/{{$subcat->slug}}">Ver todos los programas</a></li>
                                                        @foreach($subcat->subcategoriasdet as $subcatdet)
                                                            <li><a href="/aprender-ingles-en-el-extranjero/{{$cat->slug}}/{{$subcat->slug}}/{{$subcatdet->slug}}">{{$subcatdet->name_web}}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @else
                                                <li><a href="/aprender-ingles-en-el-extranjero/{{$cat->slug}}/{{$subcat->slug}}">{{$subcat->name_web}}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                            </li>
                        @else
                            <li><a href="/aprender-ingles-en-el-extranjero/{{$cat->slug}}">{{$cat->name_web}}</a></li>
                        @endif

                    @endforeach

            </ul>
        </li>

        <li class="has-children">
            <a href="/aprender-otros-idiomas-en-el-extranjero">Otros idiomas</a>
            <ul class="cd-secondary-nav is-hidden">
                <li class="go-back"><a href="#0">Menu</a></li>
                <li class="see-all"><a href="/aprender-otros-idiomas-en-el-extranjero">APRENDER OTROS IDIOMAS EN EL EXTRANJERO<span>ALEMÁN, CHINO, FRANCÉS, ITALIANO, JAPONÉS, RUSO...</span></a></li>

                @foreach(\VCN\Models\Categoria::whereIn('id', [1, 2, 7])
                    ->where(function ($query) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })->orderBy('id','ASC')->get() as $cat)

                    @if(count($cat->subcategorias))
                        <li class="has-children">
                            <a href="/aprender-otros-idiomas-en-el-extranjero/{{$cat->slug}}">{{$cat->name_web}}</a>

                            <ul class="is-hidden">
                                <li class="go-back"><a href="#0">{{$cat->name_web}}</a></li>
                                <li class="see-all"><a href="/aprender-otros-idiomas-en-el-extranjero/{{$cat->slug}}">Ver todos los programas</a></li>
                                @foreach($cat->subcategorias as $subcat)
                                    @if(count($subcat->subcategoriasdet))
                                        <li class="has-children">
                                            <a href="#0">{{$subcat->name_web}}</a>
                                            <ul class="is-hidden">
                                                <li class="go-back"><a href="#0">{{$subcat->name_web}}</a></li>
                                                <li class="see-all"><a href="/aprender-otros-idiomas-en-el-extranjero/{{$cat->slug}}/{{$subcat->slug}}">Ver todos los programas</a></li>
                                                @foreach($subcat->subcategoriasdet as $subcatdet)
                                                    <li><a href="/aprender-otros-idiomas-en-el-extranjero/{{$cat->slug}}/{{$subcat->slug}}/{{$subcatdet->slug}}">{{$subcatdet->name_web}}</a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else
                                        <li><a href="/aprender-otros-idiomas-en-el-extranjero/{{$cat->slug}}/{{$subcat->slug}}">{{$subcat->name_web}}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li><a href="/aprender-otros-idiomas-en-el-extranjero/{{$cat->slug}}">{{$cat->name_web}}</a></li>
                    @endif

                @endforeach

            </ul>
        </li>

        <li class="has-children">
            <a href="/campamentos-verano-ingles">Campamentos</a>
            <ul class="cd-nav-icons is-hidden">
                <li class="go-back"><a href="#0">Menu</a></li>
                <li class="see-all"><a href="/campamentos-verano-ingles">CAMPAMENTOS DE VERANO +inglés EN ESPAÑA<span>MAX CAMPS. De 7 a 17 años</span></a></li>

                @foreach(\VCN\Models\Categoria::where('slug', 'campamentos-verano-ingles')
                    ->where(function ($query) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })->orderBy('id','ASC')->get() as $cat)

                    @if(count($cat->subcategorias))
                        @foreach($cat->subcategorias as $subcat)
                            <li>
                                <a class="cd-nav-item" href="/campamentos-verano-ingles/{{$subcat->slug}}">
                                    <h3>{{$subcat->name_web}}</h3>
                                    <p>{{$subcat->name_web}}</p>
                                </a>
                            </li>
                        @endforeach
                    @endif

                @endforeach
                <li><a class="cd-nav-item" href="/campamentos-verano-ingles"><h3>NUESTRAS CLASES</h3><p> </p></a></li>
            </ul>
        </li>

        <li class="has-children">
            <a href="/curso-escolar-en-el-extranjero">Año Escolar</a>
            <ul class="cd-secondary-nav is-hidden">
                <li class="go-back"><a href="#0">Menu</a></li>
                <li class="see-all"><a href="/curso-escolar-en-el-extranjero">AÑO ESCOLAR EN EL EXTRANJERO<span>Primaria, secundaria y bachillerato</span></a></li>

                @foreach(\VCN\Models\Categoria::where('slug', 'curso-escolar-en-el-extranjero')
                    ->where(function ($query) {
                        return $query
                            ->where('propietario', 0)
                            ->orWhere('propietario', ConfigHelper::config('propietario'));
                    })->orderBy('id','ASC')->get() as $cat)

                    @if(count($cat->subcategorias))

                                @foreach($cat->subcategorias as $subcat)
                                    @if(count($subcat->subcategoriasdet))
                                        <li class="has-children smallmenu">
                                            <a href="#0">{{$subcat->name_web}}</a>
                                            <ul class="is-hidden">
                                                <li class="go-back"><a href="#0">{{$subcat->name_web}}</a></li>
                                                <li class="see-all"><a href="/curso-escolar-en-el-extranjero/{{$cat->slug}}/{{$subcat->slug}}">Ver todos los programas</a></li>
                                                @foreach($subcat->subcategoriasdet as $subcatdet)
                                                    <li><a href="/curso-escolar-en-el-extranjero/{{$cat->slug}}/{{$subcat->slug}}/{{$subcatdet->slug}}">{{$subcatdet->name_web}}</a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else
                                        <li><a href="/curso-escolar-en-el-extranjero/{{$cat->slug}}/{{$subcat->slug}}">{{$subcat->name_web}}</a></li>
                                    @endif
                                @endforeach

                    @else
                        <li><a href="/curso-escolar-en-el-extranjero/{{$cat->slug}}">{{$cat->name_web}}</a></li>
                    @endif

                @endforeach

            </ul>
        </li>



        <li class="has-children">
            <a href="/assets/catalogos/BS_2015_colectivos-y-escuelas.pdf">Colectivos y Escuelas</a>
            <ul class="cd-nav-icons is-hidden">
                <li class="go-back"><a href="#0">Menu</a></li>
                <li class="see-all"><a href="./catalogos/BS_2015_colectivos-y-escuelas.pdf">VIAJES EDUCATIVOS PARA COLECTIVOS Y ESCUELAS</a></li>
                <li>
                    <a class="cd-nav-item item-1" href="/catalogos/BS_2015_colectivos-y-escuelas.pdf">
                        <h3>Short Stays</h3>
                        <p>This is the item description</p>
                    </a>
                </li>

                <li>
                    <a class="cd-nav-item item-2" href="/catalogos/BS_2015_colectivos-y-escuelas.pdf">
                        <h3>Summer Courses</h3>
                        <p>This is the item description</p>
                    </a>
                </li>

                <li>
                    <a class="cd-nav-item item-3" href="/catalogos/BS_2015_colectivos-y-escuelas.pdf">
                        <h3>Trimestre Escolar</h3>
                        <p>This is the item description</p>
                    </a>
                </li>

                <li>
                    <a class="cd-nav-item item-4" href="/catalogos/BS_2015_colectivos-y-escuelas.pdf">
                        <h3>Campamentos de idiomas</h3>
                        <p>This is the item description</p>
                    </a>
                </li>
            </ul>
        </li>

        <li class="has-children">
            <a href="javascript:void(0);">Contacto</a>
            <ul class="cd-secondary-nav full-width is-hidden">
                <li class="go-back"><a href="#0">Menu</a></li>
                <li class="see-all"><a href="javascript:void(0);">CONTACTO<span></span></a></li>
                    <li class="full-width"><div class="full-width">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="head oficinas">Oficinas
                                                <span></span>
                                            </h4>
                                        </div>
                                        <div class="col-sm-4" id="mapa_info">
                                            <p class="barcelona"><strong>BARCELONA</strong><br>
                                                V&iacute;a Augusta, 33<br>
                                                08006 Barcelona<br>
                                                Tel. 93 200 88 88<br>
                                                Fax 93 202 23 71<br>
                                                <a href="mailto:infobs@britishsummer.com">infobs@britishsummer.com</a></p>

                                            <p class="girona"><strong>GIRONA</strong><br>
                                                Carrer Migdia, 25
                                                <br>
                                                17002 Girona<br>
                                                Tel. 972 414 902<br>
                                                <a href="mailto:girona@britishsummer.com">girona@britishsummer.com</a></p>

                                            <p class="madrid"><strong>MADRID</strong><br>
                                                Paseo de la Castellana <br>
                                                136 bajos<br>
                                                28046 MADRID<br>
                                                Tel.: 91 345 95 65<br>
                                                <a href="mailto:madrid@britishsummer.com">madrid@britishsummer.com</a></p>

                                            <p class="sevilla"><strong>SEVILLA</strong><br>
                                                Pza. Cristo de Burgos <br>
                                                21. Bajo A<br>
                                                41003 Sevilla<br>
                                                Tel.: 95 421 07 85<br>
                                                <a href="mailto:sevilla@britishsummer.com">sevilla@britishsummer.com</a></p>
                                        </div>
                                        <div class="col-sm-8 pull-right" style="float: left;">
                                            <div id="mapa" style="width: 100%; height: 450px;"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-4 col-sm-offset-1">
                                    <h4 class="head">Contacto<span></span></h4>
                                    <form id="contactform" action="" method="post" class="validateform" name="leaveContact">
                                        <div id="sendmessage">
                                            <div class="alert alert-info marginbot35">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                <strong>Tu mensaje ha sido enviado.</strong><br />
                                                En breve nos pondremos en contacto contigo.
                                                <br />Gracias por confiar en nosotros.
                                            </div>
                                        </div>

                                        <div class="formlist">
                                            <div class="form-group field">
                                                <label class="col-md-4 control-label" for="oficina">Oficina <span>*</span></label>
                                                <div class="col-md-8">
                                                    <select class="form-control" name="oficina" data-rule="required" data-msg="Por favor, selecciona una oficina">
                                                        <option value=""></option>
                                                        <option value="infobs@britishsummer.com">Barcelona</option>
                                                        <option value="girona@britishsummer.com">Girona</option>
                                                        <option value="madrid@britishsummer.com">Madrid</option>
                                                        <option value="sevilla@britishsummer.com">Sevilla</option>
                                                    </select>
                                                    <div class="validation"></div>
                                                </div>
                                            </div>

                                            <div class="form-group field">
                                                <label class="col-md-4 control-label" for="name">Nombre <span>*</span></label>
                                                <div class="col-md-8">
                                                    <input class="form-control" type="text" name="name" data-rule="maxlen:4" data-msg="Tienes que introducir al menos 4 caracteres" />
                                                    <div class="validation"></div>
                                                </div>
                                            </div>

                                            <div class="form-group field">
                                                <label class="col-md-4 control-label" for="email">Email <span>*</span></label>
                                                <div class="col-md-8">
                                                    <input class="form-control" type="text" name="email" data-rule="email" data-msg="Por favor, introduce un email válido" />
                                                    <div class="validation"></div>
                                                </div>
                                            </div>

                                            <div class="form-group field">
                                                <label class="col-md-4 control-label" for="message">Mensaje <span>*</span></label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" rows="6" name="message" data-rule="required" data-msg="Por favor, escribe un mensaje"></textarea>
                                                    <div class="validation"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <p class="text-muted"><small>*campos obligatorios</small></p>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="submit" value="enviar mensaje" class="btn btn-default btn-block" name="Submit" />
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div></li>
            </ul>
        </li>


    </ul> <!-- primary-nav -->
</nav> <!-- cd-nav -->

<div id="cd-search" class="cd-search">
    <form action="{{route('web.buscar')}}" method="post" enctype="multipart/form-data" class="searchbox" autocomplete="off" >
        <input type="text" placeholder="Buscar......" name="search"  id="search" class="searchbox-input" required>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    </form>
</div>