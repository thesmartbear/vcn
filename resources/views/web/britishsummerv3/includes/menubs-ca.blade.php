<div id="inside-menu" style="display: none;">
    <div class="container-fluid">
        <div class="row addmargintop30">
            <div class="col-md-12 text-center">
                <i class="fa fa-times" id="menu-close"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <div class="col">
                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/campaments-estiu-angles"><h5 style="color: #8e44ad;">CAMPAMENTS D'ESTIU + anglès A ESPANYA</h5></a>
                            <h6>Max Camps. De 7 a 17 anys</h6>
                        </div>
                        <ul>
                            @foreach(\VCN\Models\Categoria::where('id', \VCN\Models\Traducciones\Traduccion::where(['idioma'=> 'ca', 'campo'=> 'slug', 'traduccion'=> 'campaments-estiu-angles'])->first()->modelo_id)
                            ->where(function ($query) {
                                return $query
                                    ->where('propietario', 0)
                                    ->orWhere('propietario', ConfigHelper::config('propietario'));
                            })
                            ->first() as $categoria)
                                {{$categoria}}

                            @endforeach
                        </ul>
                        <p class="addmargintop20"><a href="/campaments-estiu-angles/"><strong>LES NOSTRES CLASSES</strong></a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="col">
                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/aprendre-angles-a-l-estranger"><h5 style="color: #1abc9c;">APRENDRE ANGLÈS A L'ESTRANGER</h5></a>
                            <h6>Totes les edats</h6>
                        </div>
                        <ul>
                            <li><a href="/aprendre-angles-a-l-estranger/joves/grups-amb-monitor/">PROGRAMES D'ESTIU AMB MONITOR</a></li>
                            <li><a href="/aprendre-angles-a-l-estranger/joves/cursos-especials/">CURSOS MOLT SINGULARS/ESPECIALITZATS</a></li>
                            <li><a href="/aprendre-angles-a-l-estranger/adults-professionals/">ADULTS I PROFESSIONALS</a></li>
                            <li><a href="/aprendre-angles-a-l-estranger/tota-la-familia/">TOTA LA FAMÍLIA</a></li>

                        </ul>
                    </div>


                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/aprendre-altres-idiomes-a-l-estranger"><h5 style="color: #888535;">ALTRES IDIOMES A L'ESTRANGER</h5></a>
                            <h6>Totes les edats</h6>
                        </div>
                        <ul>
                            <li><a href="/aprendre-altres-idiomes-a-l-estranger/joves/">De 8 a 18 anys</a></li>
                            <li><a href="/aprendre-altres-idiomes-a-l-estranger/adults-professionals/">De 18 a 99 anys</a></li>
                            <li><a href="/aprendre-altres-idiomes-a-l-estranger/tota-la-familia/">TOTA LA FAMÍLIA</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-xs-4">
                <div class="col">
                    <div class="caja">
                        <div class="tituloseccion">
                            <a href="/curs-escolar-a-l-estranger"><h5 style="color: #d35400;">ANY ESCOLAR A L'ESTRANGER</h5></a>
                            <h6>Primària, Secundària i Batxillerat</h6>
                        </div>

                        <ul>
                            <li><a href="/curs-escolar-a-l-estranger/curs-escolar/">Any Escolar</a></li>
                            <li><a href="/curs-escolar-a-l-estranger/semestre-escolar/">Semestre Escolar</a></li>
                            <li><a href="/curs-escolar-a-l-estranger/trimestre-escolar/">Trimestre Escolar</a></li>
                        </ul>
                    </div>

                    <div class="caja">
                        <div class="tituloseccion">
                            <h5 style="color: #2980b9;">VIATGES EDUCATIUS PER A COL·LECTIUS I ESCOLES</h5>
                        </div>
                        <ul>
                            <li><a href="/catalogos/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Short Stays</a></li>
                            <li><a href="/catalogos/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Summer Courses</a></li>
                            <li><a href="/catalogos/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Trimestre Escolar</a></li>
                            <li><a href="/catalogos/BS_2015_colectivos-y-escuelas.pdf" target="_blank">Campaments d'idiomes</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>