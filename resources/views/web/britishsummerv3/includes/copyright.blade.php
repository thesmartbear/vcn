<div class="col-sm-12">
    <div class="copyright" id="copy">
        @if(ConfigHelper::config('propietario') == 1)
        <p>&copy; {{date('Y')}} BRITISH SUMMER EXPERIENCES, S.L.  GC 003625 </p>
        <ul class="legal">
            <li><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/aviso-legal.html">{{trans('web.aviso-legal')}}</a></li>
            <li><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/politica-de-cookies.html">{{trans('web.politica-de-cookies')}}</a></li>
        </ul>
        @elseif(ConfigHelper::config('propietario') == 2)
            <p>&copy; {{date('Y')}} Institució Cultural del CIC · CIC Escola Idiomes</p>
            <ul class="legal">
                <li><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/aviso-legal.html">{{trans('web.aviso-legal')}}</a></li>
                <li><a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/politica-de-cookies.html">{{trans('web.politica-de-cookies')}}</a></li>
            </ul>
        @endif
    </div>
</div>