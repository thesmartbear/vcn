@if(ConfigHelper::config('propietario') == 1)
    <div id="chat">
        <?php
            $link = "//www.britishsummer.com/webim/client.php?locale=sp&amp;style=britishsummer";
            if(App::locale() == "ca")
            {
                $link = "//www.britishsummer.com/webim/client.php?locale=ca&amp;style=britishsummer";
            }
        ?>
        <a href="{{$link}}" target="_blank"  @if(ConfigHelper::config('propietario') == 1) onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &amp;&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('//www.britishsummer.com/webim/client.php?locale=sp&amp;style=britishsummer&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=800,height=600,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;" @endif>
            <div class="chatinside">
                <div class="icon-chat"></div>
            </div>
        </a>
    </div>
@endif
<div id="plusinfo">
    <div class="plusinfoinside">
        <div class="close">+</div>
        <div class="plusinfotext">
            <div class="icon-info"></div>
            <p>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.plusinfotext') !!}</p>
            <a style="background: transparent url(/assets/britishsummer/img/plusinfo-{{App::getLocale()}}.png) no-repeat center center;" class="plusinfobtn" href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal" @if(ConfigHelper::config('propietario') == 1) onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Verde Flotante', eventValue: 1});" @endif></a>
        </div>
    </div>
</div>