@if(view()->exists(ConfigHelper::view(ConfigHelper::config('tema').'.404')))
    @include('web.'.ConfigHelper::config('tema').'.404'))
@else

<!DOCTYPE html>
<html>
    <head>
        <title>Error 404</title>

        <!-- ==============================================
		Favicons
		=============================================== -->
        <link rel="shortcut icon" href="/assets/{{ConfigHelper::config('tema')}}/favicon/favicon{{ConfigHelper::config('sufijo')}}.ico">
        <link rel="apple-touch-icon" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/assets/{{ConfigHelper::config('tema')}}/favicon/apple-touch-icon-{{ConfigHelper::config('sufijo')}}-114x114.png">

        <link href="//fonts.googleapis.com/css?family=Lato:100,300,400,900" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #222;
                display: table;
                font-weight: 300;
                font-family: 'Lato';
                background: #f5f5f5 url('/assets/{{ConfigHelper::config('tema')}}/img/disconnected.svg') 95% top no-repeat;
            }

            .container {
                text-align: left;
                display: table-cell;
                vertical-align: middle;
            }

            .content{
                max-width: 80vw;
                margin: 0 25% 0 10%;
            }

            p {
                font-weight: 100;
                font-size: 1.1em;
            }

            .title {
                font-size: 60px;
                margin-bottom: 20px;
                color: #444;
                font-weight: 800;
            }
            .logo{
                margin-bottom: 80px;
            }
            h2{
                font-weight: 100;
            }
            h1{
                font-weight: 100;
                color: #B0BEC5;
            }
            h4{
                font-weight: 400;
            }
            ul{
                list-style: none;
                padding: 0;
            }
            ul li{
                line-height: 1.5em;
            }
            ul li a:link, ul li a:visited{
                text-decoration: none;
                font-weight: 300;
                color: #222;
            }
            ul li a:active, ul li a:hover{
                color: #000000;
                font-weight: 400;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                @if(ConfigHelper::config('logoweb') != '')
                    <p class="logo"><a href="/"><img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" /></a></p>
                @endif

                <h1 class="title">Oops!</h1>
                <h2>{{trans('web.error404rich.title')}}</h2>
                <h4>{{trans('web.error404rich.code')}}</h4>
                <p>{{trans('web.error404rich.links')}}</p>
                <ul>
                        <li class="item">
                            <a href="/">Home</a>
                        </li>
                    @foreach(VCN\Models\CMS\CategoriaWeb::arbol() as $menucat)
                        <li class="item">
                            <?php
                            if($menucat['es_link'] == 1){
                                $url = (App::getLocale() != ConfigHelper::config('idioma') ? '/'.App::getLocale().'/' : '/') . Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'link', $menucat['id'], $menucat['link']);
                            }else{
                                $url = (App::getLocale() != ConfigHelper::config('idioma') ? '/'.App::getLocale() : '') .'/'.Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $menucat['id'], $menucat['seo_url']);
                            }
                            if ($menucat['link_blank'] == 1){
                                $link = 'href="'.$url.'" target="_blank"';
                            }else{
                                $link = 'href="'.$url.'"';
                            }
                            ?>
                            <a {!! $link !!}>{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $menucat['id'], $menucat['titulo'])!!}</a>
                        </li> <!-- item-has-children -->
                    @endforeach
                        <li class="item">
                            <a href="@if(App::getLocale() != ConfigHelper::config('idioma'))/{{App::getLocale()}}@endif/{{trans('web.contacto-slug')}}">{{trans('web.contacto')}}</a>
                        </li>
                </ul>
            </div>
        </div>
    </body>
</html>
@endif
