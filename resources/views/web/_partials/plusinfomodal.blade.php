<!-- Modal -->
<div class="modal fade" id="plusinfomodal" tabindex="-1" role="dialog" aria-labelledby="plusinfoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
            </div>
            <div class="modal-body">
                <div id="respuesta">
                    <h2>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.titulo') !!}</h2><h4>{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.txt') !!}</h4>
                    <form action="/plusinfosend.php" method="post" enctype="multipart/form-data" name="plusinfoform" id="plusinfoform">
                        <div class="msg"></div>
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="form-group">
                                    <label for="name">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombre') !!}</label>
                                    <input type="text" class="form-control" id="info_name" name="info_name" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.nombrecampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="tel">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefono') !!}</label>
                                    <input type="text" class="form-control" id="info_tel" name="info_tel" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.telefonocampo') !!}">
                                </div>
                                <div class="form-group">
                                    <label for="email">{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.email') !!}</label>
                                    <input type="text" class="form-control" id="info_email" name="info_email" placeholder="{!! trans('web.masinfo'.ConfigHelper::config('sufijo').'.emailcampo') !!}">
                                    <input type="hidden" id="info_curso" name="info_curso" value="{{$hidden}}">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">

                <div id="group-lopd" class="form-group">
                    <input type="checkbox" id="info_lopd1" name="info_lopd1">
                    <span>@lang('web.formulario.lopd1')</span>
                    <br>
                    <input type="checkbox" id="info_lopd2" name="info_lopd2">
                    <span>@lang('web.formulario.lopd2', ['plataforma'=> ConfigHelper::plataformaApp()])</span>
                </div>
                        
                <button class="btn btn-primary enviar" id="plusinfoenviar" type="button" @if(ConfigHelper::config('propietario') == 1)onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});"@endif>Enviar</button>                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@push('scripts')
<script type="text/javascript">
$(document).ready(function () {

    $('form input').blur(function () {
        $('.msg').hide();
    });
    
    $('.msg').hide();
    $("#plusinfoenviar").click(function () {
        
        if ($('#info_name').val() == '') {
            $('.msg').html('Debes indicar un nombre de contacto');
            $('.msg').show();
            return false;
        }
        if ($('#info_email').val() == '') {
            $('.msg').html('Debes indicar un teléfono o un email de contacto');
            $('.msg').show();
            return false;
        }

        if ($('#info_email').val() != '') {
            var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
            if (re.test($('#info_name').val())) {
                $('.msg').html('El email no tiene un formato correcto');
                $('.msg').show();
                return false
            }

        }

        if (!/^([0-9])*$/.test($('#info_tel').val())) {
            $('.msg').html('El campo teléfono tiene que ser numérico');
            $('.msg').show();
            return false
        }

        if ( !$('#info_lopd1').prop('checked') ) {
            $('.msg').html('Debes aceptar lopd');
            $('.msg').show();
            return false;
        }

        if ( !$('#info_lopd2').prop('checked') ) {
            $('.msg').html('Debes aceptar lopd');
            $('.msg').show();
            return false;
        }

        post_data = {
            "_token": $("meta[name='csrf-token']").attr("content"),
            'name': $('#info_name').val(),
            'tel': $('#info_tel').val(),
            'email': $('#info_email').val(),
            'curso': $('#info_curso').val()
        };
        
        console.log(post_data);

        $.ajax({
            type: "POST",
            // url: "/assets/{{ConfigHelper::config('tema')}}/includes/plusinfosend-{{ConfigHelper::config('sufijo')}}.php",
            url: "{{ route('web.informacion' )}}",
            data: post_data,
            success: function (data) {
                $("#respuesta").html(data.msg);
                $(".modal-footer").html('<button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true">cerrar</button>');
                ga('send', 'Solicitud', 'button', 'click', 'home', 1);
            },
            error: function () {
                alert("error!!");
            }
        });
    });

});
</script>
@endpush