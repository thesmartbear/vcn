<style>
@media (min-width: 767px) {
    .navbar-nav .dropdown-menu .caret {
        transform: rotate(-90deg);
    }
}
/* .dropdown-toggle-menu .dropdown-menu {display: block;}
.dropdown-menu {height: auto !important;} */
.dropdown-toggle-menu:hover .dropdown-menu {display: block;}
</style>

<?php
$estructura = ConfigHelper::config('web_estructura');
$p = ConfigHelper::config('propietario');
$menu = "menu_principal";

$padres = VCN\Models\CMS\CategoriaWeb::where('estructura',$estructura)->where($menu,1)->whereIn('home_propietario',[0,$p])->where('activo',1)->where('category_id',0)->orderBy('orden')->get();

$menu = "menu";
$paginas = VCN\Models\CMS\Pagina::where($menu,1)->whereIn('propietario',[0,$p])->where('activo',1)->orderBy('orden')->get();

?>

<header class="cd-main-header">
    <a id="cd-logo" href="/"><img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" /></a>

    {{--<nav id="cd-top-nav">
        <ul class="cd-header-buttons">
            <li><a href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal"><i class="fa fa-phone"></i> {{trans('web.solicitainfo')}}</a></li>
            <li><a href="/{{trans('web.contacto-slug')}}">{{trans('web.contacto')}}</a></li>
            @if (Auth::guest())
                <li><a href="/auth/login">{{trans('web.aclientes')}}</a></li>
            @else
                <li><a href="/manage">{{trans('web.aclientes')}}</a> | <a href="//studyfuera.estudiaryviajar.com/auth/login">{{trans('web.salir')}}</a></li>
            @endif
            <li><a class="cd-search-trigger" href="#cd-search">{{trans('web.buscar')}}<span></span></a></li>
        </ul> <!-- cd-header-buttons -->
    </nav>--}}

    <nav id="cd-top-nav">
    <ul class="nav navbar-nav">

        @foreach($padres as $menu1)

            {{-- recursivo --}}
            @include('web._partials.menu_item', ['menu'=> $menu1])

        @endforeach

    </ul>
    </nav>


    {{-- <nav id="cd-top-nav-small">
        <ul class="cd-header-buttons">
            <li><a href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal"><i class="fa fa-phone"></i></a></li>
            <li><a href="/contacto.html"><i class="fa fa-map-marker"></i></a></li>
            @if (Auth::guest())
                <li><a href="/auth/login">{{trans('web.aclientes')}}</a></li>
            @else
                <li><a href="/manage">{{trans('web.aclientes')}}</a> | <a href="//studyfuera.estudiaryviajar.com/auth/login">{{trans('web.salir')}}</a></li>
            @endif
            <li><a class="cd-search-trigger" href="#cd-search">{{trans('web.buscar')}}<span></span></a></li>
        </ul> <!-- cd-header-buttons -->
    </nav> --}}

    <nav id="cd-top-nav-small">
        <ul class="nav navbar-nav">
    
            @foreach($padres as $menu1)
    
                {{-- recursivo --}}
                @include('web._partials.menu_item', ['menu'=> $menu1])
    
            @endforeach
    
        </ul>
        </nav>

    <a id="cd-menu-trigger" href="#0"><span class="cd-menu-text">Menú</span><span class="cd-menu-icon"></span></a>
</header>


{{--
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">

    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">MENÚ</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a href="/">
                <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" />
            </a>

        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">

                @foreach($padres as $menu1)

                    @include('web._partials.menu_item', ['menu'=> $menu1])

                @endforeach

            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>--}}
