@php
    $tema = isset($tema) ? $tema : Session::get('vcn.tema');    
@endphp

<script src="/assets/{{$tema}}/js/jquery.cookie.js"></script>
<script src="/assets/{{$tema}}/js/jquery.cookiecuttr.js"></script>

<link rel="stylesheet" href="/assets/outdatedbrowser/outdatedbrowser.min.css">
<script src="/assets/outdatedbrowser/outdatedbrowser.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        outdatedBrowser({
            bgColor: '#f25648',
            color: '#ffffff',
            lowerThan: 'transform',
            languagePath: 'assets/outdatedbrowser/lang/{{App::getLocale()}}.html'
        })

        // activate cookie cutter
        if($('html').attr('lang') == 'ca'){
            // cookieText = "Utilitzem cookies pròpies i de tercers per millorar els nostres serveis, elaborar informació estadística i analitzar els seus hàbits de navegació. Això ens permet personalitzar el contingut que oferim i mostrar publicitat relacionada amb les seves preferències. Al clicar a 'Entès' ACCEPTA EL SEU ÚS. <br>També pot REBUTJAR la instal·lació de cookies clicant a 'Rebutjar' o obtenir m&eacute;s informaci&oacute; a ",
            cookieText = "Utilitzem cookies pròpies i de tercers per millorar els nostres serveis, elaborar informació estadística i analitzar els seus hàbits de navegació per personalitzar el contingut que oferim i mostrar publicitat relacionada amb les seves preferèncie. L'ús de cookies i funcions de tercers incloses en la nostra pàgina web pot comportar la transferència internacional de les seves dades a tercers països on no hi ha un nivell de protecció de dades equivalent a l'europeu.<br>Al clicar a 'Entès' ACCEPTA EL SEU ÚS. <br>També pot REBUTJAR la instal·lació de cookies clicant a 'Rebutjar' o obtenir m&eacute;s informaci&oacute; a ",
            cookieTextLink = "Pol&iacute;tica de Cookies";
            cookieLink = './ca/politica-de-cookies.html';
            buttonText = 'Entès';
            buttonText2 = 'Rebutjar';
        }else if($('html').attr('lang') == 'en'){
            cookieText = "We use cookies on this website, you can ";
            cookieTextLink = "<br />read about them here";
            cookieLink = './politica-de-cookies.html';
            buttonText = 'Accept cookies';
            buttonText2 = 'Decline';
        }else{
            cookieText = "Utilizamos cookies propias y de terceros para mejorar nuestros servicios, elaborar información estadística y analizar sus hábitos de navegación para personalizar el contenido y mostrarle publicidad relacionada con sus preferencias. El uso de cookies y funciones de terceros incluidas en nuestra página web puede conllevar la transferencia internacional de sus datos a terceros países donde no existe un nivel de protección de datos equivalente al europeo.<br>Al clicar en 'Entendido' ACEPTA SU USO. <br>También puede RECHAZAR la instalación de cookies clicando en 'Rechazar' u obtener m&aacute;s informaci&oacute;n en ",
            cookieTextLink = "Pol&iacute;tica de Cookies";
            cookieLink = './politica-de-cookies.html';
            buttonText = 'Entendido';
            buttonText2 = 'Rechazar';
        }

        $.cookieCuttr({
            // cookieResetButton: true,
            cookieDeclineButton: true,
            cookieAnalyticsMessage: cookieText,
            cookieWhatAreLinkText: cookieTextLink,
            cookieWhatAreTheyLink: cookieLink,
            cookieExpires: 365,
            cookieAcceptButtonText: buttonText,
            cookieDeclineButtonText: buttonText2
        });
    });
</script>

@if( ConfigHelper::getGoogle("facebook") )

<script type="text/javascript">
if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept")
{
    document.write('<!-- Facebook Pixel Code -->');
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', {{ConfigHelper::getGoogle('facebook')}}); //'565613066936032'
    fbq('track', "PageView");
    document.write('<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=565613066936032&ev=PageView&noscript=1"/></noscript>');
    document.write('<!-- End Facebook Pixel Code -->');
}
</script>

@endif

@if(ConfigHelper::config('chat_smartsupp'))
    
    <!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
    if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept")
    {
        /* var _smartsupp = _smartsupp || {};
        // _smartsupp.key = '731df5337f967751cbed530f51a8b5404859e000';
        _smartsupp.key = '{{ConfigHelper::config('chat_smartsupp')}}';
        // _smartsupp.offsetX = 200;
        window.smartsupp||(function(d) {
        var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
        s=d.getElementsByTagName('script')[0];c=d.createElement('script');
        c.type='text/javascript';c.charset='utf-8';c.async=true;
        c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document); */
    }
    </script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept")
        {
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            // s1.src='https://embed.tawk.to/5f6dfd7e4704467e89f24b46/default';
            s1.src='{{ConfigHelper::config('chat_smartsupp')}}';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        } 
    </script>
    <!--End of Tawk.to Script-->
    
@endif

@if(ConfigHelper::config('simplybook'))
<script src="//widget.simplybook.it/v2/widget/widget.js"></script>
<script>
    if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept")
    {
        {!! ConfigHelper::config('simplybook') !!}
    }
</script>
@endif