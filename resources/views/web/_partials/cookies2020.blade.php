@php
    $tema = isset($tema) ? $tema : Session::get('vcn.tema');    
@endphp
<div id="cc-cookies"></div>

<script src="/assets/{{$tema}}/js/jquery.cookie.js"></script>
{{-- <link href="/assets/js/jquery.ihavecookies.css" rel="stylesheet"> --}}
<script type="text/javascript" src="/assets/js/jquery.ihavecookies.min.js"></script>

<script type="text/javascript">

    if($('html').attr('lang') == 'ca')
    {
        var options = {
            title: '&#x1F36A; Pol&iacute;tica de Cookies',
            message: "Utilitzem cookies pròpies i de tercers per millorar els nostres serveis, elaborar informació estadística i analitzar els seus hàbits de navegació per personalitzar el contingut que oferim i mostrar publicitat relacionada amb les seves preferèncie. L'ús de cookies i funcions de tercers incloses en la nostra pàgina web pot comportar la transferència internacional de les seves dades a tercers països on no hi ha un nivell de protecció de dades equivalent a l'europeu.<br>Al clicar a 'Entès' ACCEPTA EL SEU ÚS. <br>També pot configurar la instal·lació de cookies clicant a 'Configurar' o obtenir m&eacute;s informaci&oacute; a ",
            delay: 200,
            expires: 30,
            link: './ca/politica-de-cookies.html',
            onAccept: function(){
                var myPreferences = $.fn.ihavecookies.cookie();
                console.log('Yay! The following preferences were saved...');
                console.log(myPreferences);
            },
            uncheckBoxes: true,
            acceptBtnLabel: 'Entès',
            advancedBtnLabel: 'Configurar',
            moreInfoLabel: "Pol&iacute;tica de Cookies",
            cookieTypesTitle: 'Selecciona les cookies que vols acceptar',
            fixedCookieTypeLabel: 'Técnicas',
            fixedCookieTypeDesc: 'Esencials',
            cookieTypes: [
                {
                    type: 'Analitiques',
                    value: 'analytics',
                    // description: 'Cookies related to site visits, browser types, etc.'
                },
                {
                    type: 'Marketing',
                    value: 'marketing',
                    // description: 'Cookies related to marketing, e.g. newsletters, social media, etc'
                }
            ],
        }
    }
    else
    {
        var options = {
            title: '&#x1F36A; Pol&iacute;tica de Cookies',
            message: "Utilizamos cookies propias y de terceros para mejorar nuestros servicios, elaborar información estadística y analizar sus hábitos de navegación para personalizar el contenido y mostrarle publicidad relacionada con sus preferencias. El uso de cookies y funciones de terceros incluidas en nuestra página web puede conllevar la transferencia internacional de sus datos a terceros países donde no existe un nivel de protección de datos equivalente al europeo.<br>Al clicar en 'Entendido' ACEPTA SU USO. <br>También puede configurar la instalación de cookies haciendo click en 'Configurar' u obtener m&aacute;s informaci&oacute;n en ",
            delay: 200,
            expires: 30,
            link: './politica-de-cookies.html',
            onAccept: function(){
                var myPreferences = $.fn.ihavecookies.cookie();
                console.log('Yay! The following preferences were saved...');
                console.log(myPreferences);
            },
            uncheckBoxes: true,
            acceptBtnLabel: 'Entendido',
            advancedBtnLabel: 'Configurar',
            moreInfoLabel: 'Pol&iacute;tica de Cookies',
            cookieTypesTitle: 'Seleccionar las cookies que quiere aceptar',
            fixedCookieTypeLabel: 'Técnicas',
            fixedCookieTypeDesc: 'Esenciales',
            cookieTypes: [
                {
                    type: 'Analíticas',
                    value: 'analytics',
                    // description: 'Cookies related to site visits, browser types, etc.'
                },
                {
                    type: 'Marketing',
                    value: 'marketing',
                    // description: 'Cookies related to marketing, e.g. newsletters, social media, etc'
                }
            ],
        }
    }

    $(document).ready(function() {
        $("#cc-cookies").ihavecookies(options);

        if ($.fn.ihavecookies.preference('analytics') === true)
        {
            // console.log('This should run because analytics is accepted.');
        }

        if ($.fn.ihavecookies.preference('marketing') === true)
        {
            // console.log('This should run because marketing is accepted.');
        }
    });

</script>

{{-- FACEBOOK: Marketing --}}
@if( ConfigHelper::getGoogle("facebook") )

    <script type="text/javascript">
    if ($.fn.ihavecookies.preference('marketing') === true)
    {
        document.write('<!-- Facebook Pixel Code -->');
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', {{ConfigHelper::getGoogle('facebook')}}); //'565613066936032'
        fbq('track', "PageView");
        document.write('<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=565613066936032&ev=PageView&noscript=1"/></noscript>');
        document.write('<!-- End Facebook Pixel Code -->');
    }
    </script>

@endif

{{-- CHAT: Marketing --}}
@if(ConfigHelper::config('chat_smartsupp'))
    
    <!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
    // if ($.fn.ihavecookies.preference('marketing') === true)
    {
        /* var _smartsupp = _smartsupp || {};
        // _smartsupp.key = '731df5337f967751cbed530f51a8b5404859e000';
        _smartsupp.key = '{{ConfigHelper::config('chat_smartsupp')}}';
        // _smartsupp.offsetX = 200;
        window.smartsupp||(function(d) {
        var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
        s=d.getElementsByTagName('script')[0];c=d.createElement('script');
        c.type='text/javascript';c.charset='utf-8';c.async=true;
        c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document); */
    }
    </script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        if ($.fn.ihavecookies.preference('analytics') === true)
        {
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            // s1.src='https://embed.tawk.to/5f6dfd7e4704467e89f24b46/default';
            s1.src='{{ConfigHelper::config('chat_smartsupp')}}';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        } 
    </script>
    <!--End of Tawk.to Script-->
    
@endif

{{-- SIMPLIBOOK: Marketing => no! --}}
@if(ConfigHelper::config('simplybook'))
<script src="//widget.simplybook.it/v2/widget/widget.js"></script>
<script>
    // if ($.fn.ihavecookies.preference('marketing') === true)
    {
        {!! ConfigHelper::config('simplybook') !!}
    }
</script>
@endif

{{-- OUTDATED BROWSER --}}
<link rel="stylesheet" href="/assets/outdatedbrowser/outdatedbrowser.min.css">
<script src="/assets/outdatedbrowser/outdatedbrowser.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        outdatedBrowser({
            bgColor: '#f25648',
            color: '#ffffff',
            lowerThan: 'transform',
            languagePath: 'assets/outdatedbrowser/lang/{{App::getLocale()}}.html'
        })
    });
</script>

<style>
#cc-cookies {
    position: fixed;
	left: 0;
	bottom: 0;
    z-index: 99999;
}

footer, body {
    overflow-x: hidden;
    width: 100% !important;
}

#gdpr-cookie-message {
    position: fixed;
    width: 100%;
	left: 0;
	bottom: 0;
	padding: 0.5em 5%;
    background: #263746;
	color: #fff;
    font-family: "Poppins-Regular";
	font-size: 12px;
	z-index: 999999;
	text-align: left;
	color: #fff;
	padding: 12px;
}

/* @media (max-width: 576px) {
    #gdpr-cookie-message {
        width: 85%;
        bottom: 115px;
    }
} */

#gdpr-cookie-message button {
    border: none;
    background: #E94055;
    color: white;
    font-family: "Poppins-Regular";
    font-size: 14px;
    padding: 7px;
    border-radius: 3px;
    margin: 0 12px;
    cursor: pointer;
    transition: all 0.3s ease-in;
}

button#gdpr-cookie-advanced {
    background: white;
    color: #E94055 !important;
}
button#gdpr-cookie-accept {
    background: #E94055 !important;
    color: white;
}
#gdpr-cookie-message a {
    color: #E94055 !important;
}

#gdpr-cookie-message p, #gdpr-cookie-message ul {
    color: white;
    font-size: 15px;
    line-height: 1.5em;
}
#gdpr-cookie-message p:last-child {
    margin-bottom: 0;
    text-align: right;
}
#gdpr-cookie-message li {
    width: 20%;
    display: inline-block;
}
</style>