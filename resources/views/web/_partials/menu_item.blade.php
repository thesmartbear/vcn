<?php

    if($menu->es_link == 1)
    {
        $url = Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'link', $menu->id, $menu->link);
    }
    else
    {
        $url = Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'seo_url', $menu->id, $menu->seo_url);
    }

    $url = isset($url_padre) ? $url_padre ."/". $url : "/". App::getLocale() ."/". $url;
    $url = preg_replace('/(\/+)/','/',$url);

    if ($menu->link_blank == 1)
    {
        $link = 'href='.$url.' target="_blank"';
    }
    else
    {
        $link = 'href='.$url;
    }

    $conHijos = isset($hijos) ? $hijos : true;
?>

<li class='dropdown-toggle-menu'>
@if($conHijos && $menu->hijos_activos->count())

    <a data-target="{{$url}}" href="{{$url}}" class="dropdown-toggle" data-toggle="dropdown">
        {!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $menu->id, $menu->titulo)!!} 
        <b class="caret"></b>
    </a>

    <ul class="dropdown-menu">

        @foreach($menu->hijos_activos->sortBy('orden') as $menu2)

            @include('web._partials.menu_item', ['menu'=> $menu2, 'url_padre'=> $url])

        @endforeach
        
    </ul>

@else

    <a {!! $link !!}>{!!Traductor::getWeb(App::getLocale(), 'CategoriaWeb', 'titulo', $menu->id, $menu->titulo)!!}</a>
    
@endif
</li>