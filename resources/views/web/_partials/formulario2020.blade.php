<div id="app_formulario_curso">
<div class="portlet portlet-sortable box blue-hoki">
    <div class="portlet-title">
        <div class="caption">
            {!! trans('web.formulario.titulo') !!}
            {{-- <span>
                {!! trans('web.formulario.subtitulo') !!}
            </span> --}}
        </div>
    </div>
    <div class="portlet-body">

    <form action="#" method="post" id="frmFormulario" v-on:submit.prevent="sendInfo">

        <div class="row">
            <div class="col-sm-12 group-all">
                <div id="group-nom" class="form-group">
                    {{-- <label for="name">{!! trans('web.nombre') !!}</label> --}}
                    {!! Form::text('nom', '', ['id'=>'nom', 'class'=>'form-control', 'placeholder'=> trans('web.nombre'), 'required'=> true]) !!}
                </div>
                <div id="group-tel" class="form-group">
                    {{-- <label for="tel">{!! trans('web.telefono') !!}</label> --}}
                    {!! Form::tel('tel', '', ['id'=>'tel', 'class'=>'form-control', 'placeholder'=> trans_choice('web.movil', (config('app.timezone') == "America/Mexico_City" ? 2:1)), 'required'=> true]) !!}
                </div>
                <div id="group-email" class="form-group">
                    {{-- <label for="email">{!! trans('web.email') !!}</label> --}}
                    {!! Form::email('email', '', ['id'=>'email', 'class'=>'form-control', 'placeholder'=> trans('web.email'), 'required'=> true,
                        'oninvalid'=> "this.setCustomValidity('". trans('web.formulario.error.email') ."')",
                        'oninput'=> "setCustomValidity('')",
                        ]) !!}
                </div>
                <div id="group-pregunta" class="form-group">
                    <label for="pregunta">{!! trans('web.formulario.pregunta') !!}</label>
                    <div class="form-radio">
                        <label>
                            {!! Form::radio('viajar', 1, false, array('required'=> true)) !!} SI
                        </label>
                        <label>
                            {!! Form::radio('viajar', 0, false) !!} NO
                        </label>
                    </div>
                </div>
                <div id="group-fechanac" class="form-group">
                    {{-- <label for="fechanac">{!! trans('web.formulario.fechanac') !!}</label> --}}
                    {{-- {!! Form::date('fechanac', '', ['id'=>'fechanac', 'class'=>'form-control', 'placeholder'=> trans('web.formulario.fechanac'), 'required'=> true]) !!} --}}
                    @include('includes.form_input_datetime', [ 'campo'=> 'fechanac', 'texto'=> trans('web.formulario.fechanac')])
                </div>
                <div id="group-ciudad" class="form-group">
                    <label for="ciudad">{!! trans('web.ciudad') !!}</label>
                    {!! Form::text('ciudad', '', ['id'=>'ciudad', 'class'=>'form-control', 'placeholder'=> trans('web.ciudad'), 'required'=> true]) !!}
                </div>

            </div>
        </div>

        <input type="hidden" id="curso_id" value="{{$curso_id}}">
        <input type="hidden" id="curso" value="{{$curso->name}}">

        <div class="row">
            <div class="col-sm-12">
                <div id="group-lopd" class="form-group">

                    <input type="checkbox" id="lopd" required> <small class="lopdtext">
                    @if(config('app.timezone') == "America/Mexico_City")
                        Sí, autorizo el uso de mis datos personales de acuerdo con la Política de Privacidad de Studyfuera.
                    @else
                        @lang('web.formulario.lopd', ['plataforma'=> ConfigHelper::plataformaApp()])
                    @endif
                    </small>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary enviar">Enviar</button>
                </div>
            </div>
        </div>

    </form>
        
    </div>
</div>
</div>

@push('scripts')
<!-- Modal -->
<div class="modal fade" id="modalFormulario" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{$curso->name}}</h4>
            </div>
            <div class="modal-body">
                <p>{!! trans('web.formulario.gracias') !!}</p>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-primary enviar" data-dismiss="modal" onClick="ga('send', 'event', { eventCategory: 'Lead', eventAction: 'Solicita Info', eventLabel: 'Boton Enviar', eventValue: 1});">Cerrar</button> --}}

                <button type="button" class="btn btn-primary enviar" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>


<script src="https://cdn.jsdelivr.net/vue/2.2.6/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.0/vue-resource.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.0/axios.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {

    $('.datetime').datetimepicker({
        locale: 'es',
        format: 'L',
    });

});

var vmFormCurso = new Vue({
    el: '#app_formulario_curso',
    data: function() {
        return {
            enviado: false,
            curso_id: 0,
            curso: "",
            idioma: '{{ConfigHelper::idioma()}}',
            plataforma: '{{ConfigHelper::plataformaApp()}}',
            nombre: null,
            telefono: null,
            email: null,
            viajar: null,
            fechanac: null,
            ciudad: null,
            curso_link: window.location.href,
            curso_pais: "{{ $curso ? $curso->centro->pais->name : '-' }}",
            curso_categoria: "{{ $curso ? $curso->categoria->name : '-' }}",
            curso_track: "{{ $curso ? $curso->ganalytics : '' }}"
        };
    },
    mounted: function() {

    },
    destroyed () {

    },

    methods: {

        sendInfo: function(event) {

            // if(!this.enviado)
            {
                var nom = $('#nom').val();
                var tlf = $('#tel').val();
                var email = $('#email').val();
                var viajar = $("input[name='viajar']").val();
                var fechanac = $('#fechanac').val();
                var ciudad = $('#ciudad').val();
                var curso_id = $('#curso_id').val();
                var curso = $('#curso').val();
                
                if(tlf=="" || email=="")
                {
                    if(tlf=="")
                    {
                        $('#group-tel').addClass('has-error');
                    }

                    if(email=="")
                    {
                        $('#group-email').addClass('has-error');
                    }

                    return;
                }

                this.nombre = nom;
                this.telefono = tlf;
                this.email = email;
                this.viajar = viajar;
                this.fechanac = fechanac;
                this.ciudad = ciudad;
                this.curso_id = curso_id;
                this.curso = curso;
            }

            data = {
                _token: '{{Session::token()}}',
                nombre: this.nombre, telefono: this.telefono, email: this.email,
                viajar: this.viajar, fechanac: this.fechanac, ciudad: this.ciudad,
                curso_id: this.curso_id, curso: this.curso, curso_link: this.curso_link,
                idioma: this.idioma,
            };

            console.log(data);

            this.$http.post('{{route('web.curso-info')}}', data).then(function (response) {
                console.log(response.data);

                gtag('event', 'enviar', {
                    'event_category': 'formulario',
                    'event_label': this.curso_pais
                });

                gtag('event', 'enviar', {
                    'event_category': 'formulario',
                    'event_label': this.curso_categoria
                });

                if(this.curso_track)
                {
                    gtag('event', 'enviar', {
                        'event_category': 'formulario',
                        'event_label': this.curso_track
                    });
                }
            });

            this.enviado = true;

            $('#frmFormulario').trigger("reset");
            $("#modalFormulario").modal("show");
        }
        
    }
});
</script>
@endpush