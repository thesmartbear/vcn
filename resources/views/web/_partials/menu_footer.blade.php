<?php
$estructura = ConfigHelper::config('web_estructura');
$p = ConfigHelper::config('propietario');

$menu1 = VCN\Models\CMS\CategoriaWeb::where('estructura',$estructura)->where('menu_principal',1)->whereIn('propietario',[0,$p])->where('activo',1)->where('category_id',0)->orderBy('orden')->get();

$menu2 = VCN\Models\CMS\CategoriaWeb::where('estructura',$estructura)->where('menu_secundario',1)->whereIn('propietario',[0,$p])->where('activo',1)->where('category_id',0)->orderBy('orden')->get();

// $test = VCN\Models\CMS\CategoriaWeb::whereIn('propietario',[0,$p])->where('activo',1)->where('category_id',0)->orderBy('orden')->get();

$menu = "menu_secundario";
$paginas = VCN\Models\CMS\Pagina::where($menu,1)->whereIn('propietario',[0,$p])->where('activo',1)->orderBy('orden')->get();

?>

<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-xs-6">
                <ul class="menu">
                    
                    {{-- MENÚ PRINCIPAL --}}
                    @foreach($menu1 as $m)
                        @include('web._partials.menu_item', ['menu'=> $m, 'hijos'=> false])
                    @endforeach

                </ul>
            </div>
            
            <div class="col-sm-3 col-xs-6">
                
                <ul class="menu">
                
                    {{-- MENÚ SECUNDARIO --}}    
                    @foreach($menu2 as $m)
                        @include('web._partials.menu_item', ['menu'=> $m, 'hijos'=> false])
                    @endforeach

                </ul>

            </div>

            <div class="col-sm-3 col-xs-6">
                
                <ul class="menu">

                    {{-- PAGINAS --}}    
                    @foreach($paginas as $p)
                        
                        <?php
                        if($p->tipo == 1)
                        {
                            $url = Traductor::getWeb(App::getLocale(), 'Pagina', 'url', $p->id, $p->url);
                        }
                        else
                        {
                            $url = $p->url;
                        }

                        $link = 'href="'.$url.'"';
                        ?>

                        <li>
                        <a {!! $link !!}>{!!Traductor::getWeb(App::getLocale(), 'Pagina', 'titulo', $p->id, $p->titulo)!!}</a>
                        </li>

                    @endforeach

                </ul>

            </div>

            <div class="col-sm-3 col-xs-6">
                <ul class="menu">
                    
                    <li><a href="/blog">{{trans('web.blog')}}</a></li>
                    <li><a href="#plusinfomodal" data-toggle="modal" data-target="#plusinfomodal">{{trans('web.contacto')}}</a></li>
                    
                    @if (Auth::guest())
                        <li class="item"><a id="btn-login">login/registro</a></li>
                    @else
                        <li><a href="https://{{$_SERVER['SERVER_NAME']}}/area">{{trans('web.aclientes')}}</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="ifa fa fa-sign-out fa-fw"></i> {{ trans('web.salir') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                            {{-- <a href="https://{{$_SERVER['SERVER_NAME']}}/auth/logout"><i class="fa fa-sign-out "></i>{{trans('web.salir')}}</a></li> --}}
                    @endif

                </ul>

                <div class="social">
                    <a href="https://www.instagram.com/dippy.life/" target="_blank"><i class="fa fa-facebook-square"></i></a>
                    <a href="https://www.facebook.com/DippyLife-221273871720459/" target="_blank"><i class="fa fa-instagram"></i></a>
                </div>

                <hr>

                <ul class="legal">
                    <li><a href="/condiciones-generales.html">Condiciones generales</a></li>
                    <li><a href="/politica-de-cookies.html">Política de cookies</a></li>
                </ul>
            </div>

        </div>
    </div>
</div>