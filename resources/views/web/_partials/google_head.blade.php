@php
$ga = ConfigHelper::getGoogle();
$gtm = ConfigHelper::getGoogle("gtm");
@endphp

@if( $ga )

    <script async src="https://www.googletagmanager.com/gtag/js?id={{ $ga }}"></script>

    <script>
    // if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept")
    if ($.fn.ihavecookies.preference('analytics') === true)
    {
        // <!-- Global site tag (gtag.js) - Google Analytics -->
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{ConfigHelper::getGoogle() }}');
        gtag('config', '{{ ConfigHelper::getGoogle('gads') }}');
    }
    </script>

@endif

@if( $gtm )

    <!-- Google Tag Manager -->
    <script>
        // if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept")
        // if ($.fn.ihavecookies.preference('analytics') === true)
        {
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','{{ $gtm }}');
        }
    </script>

@endif
    