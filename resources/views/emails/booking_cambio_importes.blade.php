@extends('layouts.email')


@section('contenido')

Se han realizado cambios en un Booking facturado:

Booking: {{$booking->id  ?? '-'}}
Viajero: {{$booking->viajero->full_name  ?? '-'}}

@if(isset($booking))
    <a href="{{$base_url}}{{route('manage.bookings.ficha',$booking->id, false)}}">Ver Booking</a>
@else
    <a href="#">Ver Booking</a>
@endif

@stop