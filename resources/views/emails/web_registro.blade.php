@extends('layouts.email')


@section('contenido')

Nuevo registro web

Nombre: {{$ficha->full_name  ?? '-'}}
Email: {{$ficha->email  ?? '-'}}
Móvil: {{$ficha->movil  ?? '-'}}

@if(isset($ficha->user))
    @if($ficha->user->es_viajero)
        Viajero: <a href="{{$base_url}}{{route('manage.viajeros.ficha',$ficha->id,false)}}">{{$ficha->full_name}}</a>
    @elseif($ficha->user->es_tutor)
        Tutor: <a href="{{$base_url}}{{route('manage.tutores.ficha',$ficha->id,false)}}">{{$ficha->full_name}}</a>
    @endif
@else
    Viajero/Tutor: enlace
@endif


Have a nice day!

@stop