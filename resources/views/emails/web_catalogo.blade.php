@extends('layouts.email')


@section('contenido')

Solicitud de información de Catálogo recibida:

Nombre: {{$nombre  ?? '-'}}
Teléfono: {{$telefono  ?? '-'}}
E-mail: {{$email  ?? '-'}}
Catálogo: {{$catalogo  ?? '-'}}
Idioma: {{$idioma  ?? '-'}}

@stop