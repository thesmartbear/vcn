@extends('layouts.email')

@section('contenido')

    @component('mail::layout')

        {{-- Header --}}
        @slot('header')
            @component('mail::header', ['url' => config('app.url')])
                {{ $asunto  ?? "%ASUNTO%" }}
            @endcomponent
        @endslot

        {{-- Body --}}
        {!! $body  ?? "%BODY%" !!}

        {{-- Subcopy --}}
        {{--@slot('subcopy')
            @component('mail::subcopy')

            @endcomponent
        @endslot--}}

        {{-- Footer --}}
        @slot('footer')
            @component('mail::footer')
                © {{ date('Y') }}
            @endcomponent
        @endslot

    @endcomponent

@stop