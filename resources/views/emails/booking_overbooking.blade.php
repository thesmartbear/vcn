@extends('layouts.email')


@section('contenido')

Viajero: {{$booking->viajero->full_name  ?? '-'}}

Plazas disponibles Vuelo: {{$plazasv  ?? '-'}}
Plazas disponibles Alojamientos: {{$plazasa  ?? '-'}}
Overbooking Alojamiento: {{isset($booking) ? ($booking->ovbkg_pa?"SI":"NO") : "-"}}
Overbooking Vuelo: {{isset($booking) ? ($booking->ovbkg_pv?"SI":"NO") : "-"}}
AUTORIZADO POR: {{isset($booking) ? ($booking->ovbkg_auth ?: "-") : "-"}}
ONLINE: {{isset($booking) ? ($booking->es_online?"SI":"NO") : "-"}}

@if(isset($booking))
    <a href="{{route('manage.bookings.ficha',[$booking->id,false])}}">Ver Booking</a>
@else
    <a href="#">Ver Booking</a>
@endif

@stop
