@extends('layouts.email')


@section('contenido')

Hola {{$hola->full_name  ?? '-'}},

La teva pre-reserva pel programa {{ $booking->programa  ?? '-' }}} caducarà d'aquí a {{ isset($booking) ? ($booking->es_online?1:2) : "x"}} dia/dies.

Si encara segueixes interessat/da, si us plau feu el pagament i envieu el full d'inscripció juntament amb el comprovant de transferència per a formalitzar la inscripció.
Si tens algun dubte o per alguna cosa no t'interessa el curs, truca al {{$booking->viajero->oficina->telefono  ?? '-'}}.


Ens agradaria molt comptar amb tu aquest estiu!

{{$booking->viajero->asignado->full_name  ?? 'asignado'}}

@stop