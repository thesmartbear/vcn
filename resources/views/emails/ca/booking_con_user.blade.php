@extends('layouts.email')


@section('contenido')

    Hem tramès la vostra sol·licitud d'inscripció a {{ $booking->programa}} per en/la {{$booking->viajero->full_name}}.
    Podeu accedir a la vostra àrea de clients per veure mes detalls (des de la nostre web, fent clic en 'Area de clients' en el menú).

    <br><br>

    @if($booking->es_online)
        @if(!$booking->es_online_tpv)
            @if($booking->es_online_comprobante)
                De seguida que acusem recepció de la seva transferència, confirmarem la seva inscripció
            @else
                Recordar enviar-nos el comprovant de pagament per email a {{ConfigHelper::config('email')}}. Si no, la seva sol·licitud d'inscripció caducarà en 5 dies.
            @endif
        @endif
        <br><br>
    @endif

    Moltes gràcies!

@stop