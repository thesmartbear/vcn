@extends('layouts.email')


@section('contenido')

    Ja podeu accedir a la informació a través de l'àrea de clients de la nostra web <a href="{{$web}}">{{$web}}</a>. Heu de tenir en compte que la primera vegada que hi accediu haureu de canviar la contrasenya.

    <br><br>
    USUARI: {{$user->username}}
    <br>
    CONTRASENYA TEMPORAL: {{$password}}

    <br><br>
    A partir d'ara us anirem publicant informació sobre el programa de forma gradual. Rebreu un correu cada vegada que hi hagi alguna cosa nova.
    <br><br>
    Moltes gràcies!

@stop