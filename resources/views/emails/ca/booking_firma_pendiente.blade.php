@extends('layouts.email')

<?php
$p = $booking->plataforma;
$web = ConfigHelper::config('area_url',$p) ?: ConfigHelper::config('web',$p);
$nom = ConfigHelper::config('name',$p);
?>

@section('contenido')

Estem pendents de rebre la teva signatura per la inscripció de el/la {{$booking->viajero->full_name  ?? '-'}} al programa de {{ $booking->programa  ?? '-'}} .
Agrairem que revisis les teves dades i les condicions i signis el full d’inscripció.

Per això hauràs de:
- Entrar a la teva àrea des de <a href="{{$web  ?? '#'}}">{{$web  ?? 'web'}}</a> (l'usuari és l'email. Hi ha un un botó 'Has oblidat la teva contrasenya?' per si de cas).
- Trobaràs a la teva àrea un requadre groc i un botó verd per poder signar. Fer clic al botó verd 'Signatura'.
- Llavors veuràs un requadre blanc on podràs signar. A sota del requadre, surt una visualització de la inscripció amb les condicions.

Recomanem que et connectis a la teva àrea des de un tablet o un mòbil, ja que serà més fàcil signar amb el dit que amb el ratolí.


{{$nom  ?? "plataforma"}}

@stop