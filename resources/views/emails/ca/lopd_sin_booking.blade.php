@extends('layouts.email')

<?php
$es_viajero = $ficha->es_viajero?1:0;
$id = $ficha->id;

$id = rand(10000,99999) . $id;
$token = ConfigHelper::token();
// $ruta = $base_url . route('lopd.set', [ $es_viajero, $id,'token'=> $token ], false);
$ruta = $plataforma->area_url ."/set-lopd/". $es_viajero ."/". $id ."?token=" . $token;

$boton = "ÉS CLAR QUE SÍ! VULL SEGUIR REBENT EMAILS I CORREUS POSTALS DE ".$plataforma->name;

$mas = "";
if($plataforma->id == 1)
{
    $mas = " i fins i tot convidar-vos a algun aniversari (l'any que ve farem...35 anys!)";
}

$firma = "British Summer Experiences, SL - Via Augusta, 33, Entresuelo 2ª, 08006 Barcelona.";
if($plataforma->id == 2)
{
    $firma = "Institució Cultural del CIC - Via Augusta, 205, 08021 Barcelona.";
}
?>

@section('contenido')

@if(isset($asunto))
Asunto: {{$asunto}}<hr>
@endif

Segur que estàs rebent molts mails com aquest ... així que et donem les GRÀCIES per obrir-lo. El que volem explicar-te és IMPORTANT.
<br><br>
Aquest divendres 25 de maig entra en vigor el nou reglament europeu de protecció de dades  i necessitem que tornis a donar-nos el teu consentiment per a rebre els nostres correus electrònics i correus postals. Prometem que seguiran sent ben poquets! Ens encanta enviar-vos el nostre nou catàleg un cop l'any i de tant en tant informar-vos de les novetats{{$mas}}.

<br><br>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{$ruta}}" style="height:38px;v-text-anchor:middle;width:650px;" arcsize="11%" strokecolor="#23ac11" fillcolor="#23ac11">
    <w:anchorlock/>
    <center style="color:#ffffff;font-family:sans-serif;font-size:13px;font-weight:bold;">{{$boton}}</center>
  </v:roundrect>
<![endif]--><a href="{{$ruta}}"
style="background-color:#23ac11;border:1px solid #23ac11;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:38px;text-align:center;text-decoration:none;width:650px;-webkit-text-size-adjust:none;mso-hide:all;">{{$boton}}</a></div>

<br>
<hr>
El responsable del tractament de les teves dades és {{$plataforma->factura_pie}}. Amb la teva acceptació explícita, ens autoritzes al tractament de les dades personals que ens vas facilitar en el seu moment, tant del / la participant així com dels tutors.

<br><br>
<strong>Política de protecció de dades de caràcter personal</strong>
D'acord al reglament general de protecció de dades (RGPD) i la Llei de Serveis de la Societat de la Informació i de Comerç Electrònic (LSSI-CE), les dades de caràcter personal que pugui contenir aquest missatge són objecte de tractament per part de {{$plataforma->factura_pie}} amb l'objectiu d'enviar informació sol·licitada, novetats i actualitzacions. Has rebut aquest correu perquè has facilitat voluntàriament la teva adreça de correu electrònic. Si vols saber més sobre el nostre compromís amb la teva privacitat o exercir els teus drets, si us plau consulta la nostra política de privacitat <a href="https://{{$plataforma->web}}/{{$ficha->idioma_contacto}}/Proteccion_de_datos_personales.html">política de privacitat</a>.
<br>
<i>{{$firma}}</i>

@stop