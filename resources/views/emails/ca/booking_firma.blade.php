@extends('layouts.email')

@section('contenido')

    Hi ha hagut canvis en la seva inscripció i es requereix tornar a signar el full d'inscripció reflectint els canvis.

    El trobarà en l'àrea de client: <a href="{{$web  ?? '#'}}">{{$web  ?? 'web'}}</a>.

@stop