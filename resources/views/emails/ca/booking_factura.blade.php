@extends('layouts.email')


@section('contenido')

Us recordem que abans del {{ isset($booking) ? (Carbon::parse($booking->course_start_date)->subDays(30)->format('d/m/Y')) : "-"}} heu de fer efectiu el pagament final del programa.


L'import que us queda per pagar és de {{ isset($booking) ? ConfigHelper::parseMoneda($booking->saldo_pendiente) : "-"}}.

El pagament s'ha de fer via transferència bancària al número de compte {{ isset($booking) ? ($booking->oficina?$booking->oficina->txtIban($booking):"-") : "-"}} de {{ isset($booking) ? ($booking->oficina?$booking->oficina->banco:"-") : "-"}}.
Recordeu enviar-nos el comprovant de l'ingrés amb el nom del participant i nom del programa al correu {{ isset($booking) ? ($booking->oficina?$booking->oficina->email:"-") : "-"}}.

Us adjuntem la factura.


Nom i cognom: {{$booking->viajero->full_name  ?? '-'}}
Programa: {{ isset($booking) ? ($booking->programa) : "-"}}
Data d'inici: {{$booking->curso_start_date  ?? '-'}}
Data final: {{$booking->curso_end_date  ?? '-'}}


Salutacions!

@stop