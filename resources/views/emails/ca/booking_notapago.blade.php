@extends('layouts.email')


@section('contenido')

{!! $intro ?? 'intro' !!}

Import a pagar: {{$importe ?? '-'}}.

El pagament s'ha de fer via transferència bancària al número de compte {{ isset($booking) ? ($booking->oficina ? $booking->oficina->txtIban($booking) : '-') : '-' }} de {{ isset($booking) ? ($booking->oficina ? $booking->oficina->banco : '-') : '-' }}.

Recordeu enviar-nos el comprovant de l'ingrés amb el nom del participant i nom del programa al correu {{ isset($booking) ? ($booking->oficina ? $booking->oficina->email : '-') : '-' }}.


Nom i cognom: {{$booking->viajero->full_name ?? '-'}}
Programa: {{ $booking->programa ?? '-'}}
Data d'inici: {{$booking->curso_start_date ?? '-'}}
Data final: {{$booking->curso_end_date ?? '-'}}


Salutacions!

@stop