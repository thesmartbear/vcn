@extends('layouts.email')


@section('contenido')

    Hem tramès la vostra sol·licitud d'inscripció a {{ $booking->programa ?? '-'}} per en/la {{$booking->viajero->full_name ?? '-'}}.
    Rebreu ara un email amb les dades per accedir a la vostra àrea de clients.

    @if(isset($booking))

        @if( $booking->curso->es_convocatoria_multi )
            Especialitats:
            <ul>
            @foreach($booking->multis->sortBy('n') as $multi)
                <li>Setmana {{$multi->n}}: {{$multi->especialidad_name}}</li>
            @endforeach
            </ul>
        @endif

        @if($booking->es_online)
            @if(!$booking->es_online_tpv)
                @if($booking->es_online_comprobante)
                    De seguida que acusem recepció de la seva transferència, confirmarem la seva inscripció.
                @else
                    Recordar enviar-nos el comprovant de pagament per email a {{ConfigHelper::config('email')}}. Si no, la seva sol·licitud d'inscripció caducarà en 5 dies.
                @endif
            @endif
        @endif
    @else
        De seguida que acusem recepció de la seva transferència, confirmarem la seva inscripció. / Recordar enviar-nos el comprovant de pagament per email a -. Si no, la seva sol·licitud d'inscripció caducarà en 5 dies.
    @endif

    Moltes gràcies!

@stop