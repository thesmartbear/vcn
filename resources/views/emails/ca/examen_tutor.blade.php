@extends('layouts.email')


@section('contenido')

Hola {{$destino->fname ?? "-"}}!
{{$ficha->name ?? "-"}} ja te activat el seu test d’anglès online.
Ens permetrà veure el seu nivell de gramàtica, vocabulari i comprensió lectora.
Comptar uns 45 minuts per fer-ho. Al finalitzar el test, s’informarà de la nota i del nivell corresponent.

S’accedeix al test fent <a href="{{$examen_link ?? '-'}}">clic aquí</a>.
Si l’enllaç anterior no funciona, recomanem copiar i enganxar la URL següent al navegador: {{$examen_link ?? "-"}}.
S’ha d’entrar usuari i contrasenya per accedir al test. Els hem enviat ara en un correu electrònic separat.

Si teniu qualsevol pregunta en el moment de realitzar el test, estem disponibles per ajudar-vos per email ({{$oficina_email ?? "-"}}) o telèfon ({{$oficina_tlf ?? "-"}}).
Salutacions!

@stop