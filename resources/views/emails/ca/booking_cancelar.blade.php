@extends('layouts.email')


@section('contenido')

    {{$booking->viajero->full_name  ?? 'viajero'}} ha cancel·lat.

    Booking: <a href="{{$base_url}}{{route('manage.bookings.ficha',$booking->id, false)}}">{{ isset($booking)  ? ($booking->id) : "-"}}</a>
    Viatger: <a href="{{$base_url}}{{route('manage.viajeros.ficha',$booking->viajero_id, false)}}">{{ isset($booking)  ? ($booking->viajero->name) : "-"}}</a>
    Programa: {{ isset($booking)  ? ($booking->programa) : "-"}}
    Data inscripció: {{isset($booking)  ? ($booking->created_at->format('d/m/Y')) : "-"}}
    <br>
    Tenía contractat Assegurança de cancel·lació: {{isset($booking)  ? ($booking->es_seguro_cancelacion?"Si":"No") : "-"}}
    <br>
    Pendent refund: {{isset($booking)  ? ($booking->es_refund?"Si":"No") : "-"}}
    <br>
    Booking facturat total o parcialment: {{isset($booking)  ? ($booking->tiene_facturas?"Si":"No") : "-"}}

@stop