@extends('layouts.email')

@section('contenido')

El document que heu pujat per {{$doc->name  ?? '-'}} no es correcte.


Si us plau, entrau a la vostra área de client <a href="{{$web  ?? '#'}}">{{$web  ?? 'web'}}</a> i pujeu el document correcte.

Si teniu qualsevol dubte, podeu contactar amb nosaltres per email ({{$booking->asignado->email  ?? '-'}}) o pe telèfon ({{$booking->viajero->oficina->telefono  ?? '-'}}).


Gràcies!

@stop