@extends('layouts.email')


@section('contenido')

    Benvolguts amics,

    Volem convidar-vos (pares/mares i nois/es) a les reunions informatives del programa en el que esteu inscrits i on podreu aclarir els dubtes que tingueu. A més podreu conèixer d'altres participants.

    Detalls de la sessions informatives del programa {{ $booking->programa  ?? '-'}}:
    @if(isset($booking))
        @foreach($booking->reuniones as $reunion)
            @if($reunion->notas)
                {!! $reunion->notas !!}<br>
            @endif
            Data: {{$reunion->fecha->format('d/m/Y')}}
            Hora: {{$reunion->hora}}
            Lloc: {{$reunion->lugar}}
            <hr>
        @endforeach
    @else
        Fecha: -
        Hora: -
        Lugar: -
    @endif
    Us hi esperem!
@stop