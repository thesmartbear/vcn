@extends('layouts.email')


@section('contenido')

    Us confirmem que hem rebut el pagament de {{$pago->importe_moneda  ?? '-'}} pel programa {{$booking->curso->name  ?? '-'}} de la/d'en {{$booking->viajero->full_name  ?? '-'}}.
    Adjuntem rebut.

@stop