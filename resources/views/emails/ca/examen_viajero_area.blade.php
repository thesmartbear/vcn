@extends('layouts.email')


@section('contenido')

Hola {{$destino->fname ?? "-"}}!
Ja pots fer el teu test d’anglès online.
Ens permetrà veure el teu nivell de gramàtica, vocabulari i comprensió lectora.
Compte uns 45 minuts per fer-ho. Al finalitzar el test, et donarem la teva nota i el nivell corresponent.

S’accedeix al test fent <a href="{{$examen_link ?? '-'}}">clic aquí</a>.
Si l’enllaç anterior no funciona, copia i enganxa la URL següent al navegador: {{$examen_link ?? "-"}}.
Et demanarà usuari (el teu email) i contrasenya per entrar.
En el cas que no recordis la contrasenya, pots establir una de nova aquí: <a href="{{$web  ?? '#'}}/auth/password/email">{{$web  ?? 'web'}}/auth/password/email</a>

Si tens qualsevol pregunta en el moment de realitzar el test, estem disponibles per ajudar-te per email ({{$oficina_email ?? "-"}}) o telèfon ({{$oficina_tlf ?? "-"}}).
Salutacions!  

@stop