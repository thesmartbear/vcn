@extends('layouts.email')

<?php
$ruta = $plataforma->area_url;
$boton = "ACTUALITZAR AQUESTES DATES ARA";
$web = $plataforma->web;

$seguro = "la Institució Cultural del CIC, AVI Internacional";
$organiza = "British Summer Experiences S.L. o la Institució Cultural del CIC";
$blog = "www.landedblog.com";
$firma = "British Summer Experiences, SL - Via Augusta, 33, Entresuelo 2ª, 08006 Barcelona.";
$banco = "; i el Banc de Sabadell (en el cas de sol·licitar finançament)";
if($plataforma->id == 2)
{
    $seguro = "BRITISH SUMMER EXPERIENCES, SL, AVI Internacional";
    $organiza = "INSTITUCIÓ CULTURAL DEL CIC o pel BRITISH SUMMER EXPERIENCES, SL";
    $blog = "www.landedblog.com/iccic";
    $firma = "Institució Cultural del CIC - Via Augusta, 205, 08021 Barcelona.";
    $banco = "";

}
?>

@section('contenido')

@if(isset($asunto))
Asunto: {{$asunto}}<hr>
@endif

Aquest divendres 25 de maig entra en vigor el Reglament (UE) 2016/679 de protecció de dades personals. Per aquest motiu necessitem la teva autorització explícita pel tractament de les dades personals que ens heu facilitat tant del / la participant així com dels tutors. El responsable d'aquest tractament és {{$plataforma->factura_pie}} i les finalitats són:
<br>
<br>- la gestió de la inscripció, 
<br>- l’enviament d’informació sobre els nostres productes i serveis per qualsevol via inclosa l'electrònica.
<br>- l’ús de la imatge del / de la participant en el marc de la seva participació en els nostres programes així com a la realització de qüestionaris sobre el programa realitzat. 

<br><br>

<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{$ruta}}" style="height:38px;v-text-anchor:middle;width:600px;" arcsize="11%" strokecolor="#23ac11" fillcolor="#23ac11">
    <w:anchorlock/>
    <center style="color:#ffffff;font-family:sans-serif;font-size:13px;font-weight:bold;">{{$boton}}</center>
  </v:roundrect>
<![endif]--><a href="{{$ruta}}"
style="background-color:#23ac11;border:1px solid #23ac11;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:38px;text-align:center;text-decoration:none;width:600px;-webkit-text-size-adjust:none;mso-hide:all;">{{$boton}}</a></div>

<br><br>
Trigaràs menys d'1 minut per actualitzar aquestes dades i és imprescindible per poder gestionar correctament les inscripcions i la comunicació amb vosaltres. Podreu revisar i canviar aquestes autoritzacions en qualsevol moment des de la vostra àrea de client.
<br>
Si no recordes la teva contrasenya per entrar a la teva àrea de client, només has de fer clic a "Has oblidat la teva contrasenya?" a la nostra secció de "Àrea de client", a baix de tot del menú dret de la nostra web (https://{{$web}}) o directament des d'aquest enllaç: <a href="{{$ruta}}">{{$ruta}}</a> (si no et deixa fer clic a l'enllaç, ho pots copiar i pegar al teu navegador).

<br><br>
<strong>Clàusula informativa sobre protecció de dades personals</strong>
<br>
En compliment del Reglament (UE) 2016/679 de protecció de dades personals, us informem del tractament de les vostres dades personals de les que són responsable {{$plataforma->name}}, per a les següents finalitats: Gestió de la inscripció, així com per enviar informació sobre els nostres productes i serveis per qualsevol via inclosa l'electrònica. Les vostres dades es tracten en base al vostre consentiment i l'execució del contracte de serveis. Els destinataris de la informació són: empreses col·laboradores de {{$plataforma->name}} a la ciutat de destí; {{$seguro}} (per a gestió de l'assegurança mèdica). Aquestes cessions són necessàries per a la prestació del servei. Si no autoritza aquesta cessió, no podrà prestar-se el servei. També les agències de viatges col·laboradores (en cas que {{$plataforma->name}} tramiti la compra dels bitllets de viatge){{$banco}}. El termini de conservació previst és el legal establert en la normativa i en tot cas mentre no retiri el consentiment prestat / sol·liciti la supressió. Us informem dels vostres drets d'accés, rectificació, supressió, portabilitat de dades, limitació i oposició, així com la revocació del consentiment prestat, si s'escau, contactant amb {{$plataforma->factura_pie}} o per correu electrònic a {{$plataforma->email}}. També teniu dret a fer una reclamació davant les autoritats de protecció de dades.

<br><br>
<strong>Utilització de la imatge dels consumidors i realització de qüestionaris.</strong>
<br>
Els pares o representants de l'estudiant autoritzen expressament a {{$plataforma->factura_pie}} per a la realització de fotografies i altres materials audiovisuals als alumnes, i el seu ús posterior en el marc de la seva participació en els programes organitzats per {{$organiza}} (entitat amb la qual es comparteixen els programes de joves). Aquestes imatges podran ser publicades als diferents suports d’ambdúes organitzacions: pàgina web, xarxes socials (Facebook, Twitter, Instagram i similars), catàleg de cursos, blog {{$blog}} o com a material publicitari, sempre i quan no existeixi una oposició explícita prèvia per part del consumidor.

<br>
L'autorització es pot revocar en qualsevol moment, així com exercitar els drets d'accés, rectificació, cancel·lació i oposició dirigint-se per escrit i adjuntant còpia del DNI a {{$plataforma->factura_pie}}.
<br>
En el cas que aquesta decisió causés danys i / o perjudicis a {{$plataforma->factura_pie}}, aquest podrà exigir indemnització al consumidor.
<br>
Així mateix en el cas de participants menors d’edat, els pares o tutors legals autoritzen {{$plataforma->name}} a realitzar qüestionaris als fills o filles participants.

<br>
<i>{{$firma}}</i>

@stop