@extends('layouts.email')


@section('contenido')

Hola {{$destino->fname ?? "-"}}!
Ja està activat el teu test d’anglès online.
Ens permetrà veure el teu nivell de gramàtica, vocabulari i comprensió lectora.
Compte uns 45 minuts per fer-ho. Al finalitzar el test, et donarem la teva nota i el nivell corresponent.

S’accedeix al test fent <a href="{{$examen_link ?? '-'}}">clic aquí</a>.
En el cas que l’enllaç anterior no funcioni, copia i enganxa la URL següent al navegador: {{$examen_link ?? "-"}}.
Et demanarà usuari (el teu email) i contrasenya per entrar. Te’ls acabem d’enviar ara en un correu electrònic separat.

Si tens qualsevol pregunta en el moment de realitzar el test, estem disponibles per ajudar-te per email ({{$oficina_email ?? "-"}}) o telèfon ({{$oficina_tlf ?? "-"}}).
Salutacions! 

@stop