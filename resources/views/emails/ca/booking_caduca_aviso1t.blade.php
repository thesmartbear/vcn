@extends('layouts.email')


@section('contenido')

Hola {{$hola->full_name  ?? '-'}},

La pre-reserva per en/la {{$booking->viajero->full_name  ?? '-'}} en el programa {{ $booking->programa  ?? '-'}} caducarà d'aquí a {{ isset($booking) ? ($booking->es_online?1:2) : "x"}} dia/dies.

Si encara seguiu interessats/des, si us plau feu el pagament i envieu el full d'inscripció juntament amb el comprovant de transferència per a  formalitzar la inscripció.
Si teniu algun dubte o per alguna cosa no us interessa el curs truqueu al {{$booking->viajero->oficina->telefono  ?? '-'}}.

Ens agradaria molt comptar amb vosaltres aquest estiu!

{{$booking->viajero->asignado->full_name  ?? 'asignado'}}

@stop