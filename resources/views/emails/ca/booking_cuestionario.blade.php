@extends('layouts.email')


@section('contenido')

    Hola!

    Et volem demanar un petit favor ... Ens pots contestar el qüestionari de {{$cuestionario->name  ?? '-'}} que tens a la teva àrea de client?
    Per nosaltres es molt important tenir la teva opinió i només et portarà 1 minutet.

    Entra a la teva àrea de client (<a href="{{$web  ?? '#'}}">{{$web  ?? 'web'}}</a>) i fes clic al nom del qüestionari dins el requadre blau de la dreta.

 
    En cas de no recordar la contrasenya (si, ens passa a tots i totes...), pots demanar una nova aquí: <a href="{{$web  ?? '#'}}/auth/password/email">{{$web  ?? 'web'}}/auth/password/email</a>
    <br>
    A l'instant rebràs un email amb un enllaç per a establir la teva nova contrasenya i poder accedir a la teva àrea de seguida.

@stop