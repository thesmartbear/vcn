@extends('layouts.email')

<?php
$p = $booking->plataforma;
$web = ConfigHelper::config('area_url',$p)?:ConfigHelper::config('web',$p);
//$web = $base_url . route('area.comprar.booking', $booking->id, false);
?>

@section('contenido')

    S'acaba de crear una pre-inscripció en el programa de {{ $booking->programa  ?? '-'}} per {{$booking->viajero->full_name  ?? '-'}}.
    Per formalitzar-la, és necessari completar les dades i realitzar el pagament del dipòsit al compte.
    Teniu el plaç de 2 dies per fer-ho abans que caduqui la pre-inscripció.

    <strong>Com completar la inscripció?</strong>

    Fer clic aquí o copiar/pegar la següent url en el vostre navegador: <a href="{{$web  ?? '#'}}">{{$web  ?? 'web'}}</a> per accedir a la vostra àrea personal.
    Podeu accedir amb el vostre correu electrònic i la contrasenya que us hem enviat.
    Si no recordeu la contrasenya, la podeu recuperar a la mateixa pàgina fent clic a "Has oblidat la teva contrasenya?".


    Un cop dins la vostra àrea personal, només haureu d'omplir les dades seguint els passos següents:
    Pas 1: dades del curs
    Pas 2: dades del viatger
    Pas 3: dades dels tutors
    Pas 4: revisar dades i escollir forma de pagament.
    Pas 5: realitzar pagament per transferència o targeta.

    Al finalitzar, rebreu un missatge de confirmació.

@stop