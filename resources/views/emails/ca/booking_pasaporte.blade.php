@extends('layouts.email')

<?php
$p = $booking->plataforma?:1;
$web = ConfigHelper::config('area_url',$p)?:ConfigHelper::config('web',$p);
?>

@section('contenido')

Estem a punt d'emetre els bitllets d'avió i necessitem que ens confirmeu les dades del passaport d'en/la {{$booking->viajero->full_name}}. Aquesta informació és d'EXTREMA IMPORTÀNCIA ja que qualsevol error pot significar que en/na {{$booking->viajero->full_name}} no pugui viatjar amb el bitllet previst i possiblement s'hagi d'emetre un nou bitllet, amb el corresponent càrrec a l'estudiant.
<br>
Us adjuntem una llista de les corresponents dades que figuren en el nostre sistema, que seran les que facilitarem a la companyia aèria. Si detecteu qualsevol error, podeu modificar les dades entrant en la vostra àrea de client des de <a href="{{$web}}">{{$web}}</a>.<br>
Us recordem que tant el nom com els dos cognoms han de constar tal i com figuren en el propi passaport.
<br><br>

Programa: {{ $booking->programa}}
<br>
Nom: {{$booking->viajero->name}}
<br>
Cognoms: {{$booking->viajero->lastname}} {{$booking->viajero->lastname2}}
<br>
Data de naixement: {{Carbon::parse($booking->viajero->fechanac)->format('d/m/Y')}}
<br>
Nº de passaport: {{$booking->viajero->datos->pasaporte}}
<br>
Data d'expedició: {{Carbon::parse($booking->viajero->datos->pasaporte_emision)->format('d/m/Y')}}
<br>
Data de caducitat: {{Carbon::parse($booking->viajero->datos->pasaporte_caduca)->format('d/m/Y')}}
<br>
País d'expedició: {{$booking->viajero->datos->pasaporte_pais}}
<br>
Nacionalitat: {{$booking->viajero->datos->nacionalidad}}

@stop