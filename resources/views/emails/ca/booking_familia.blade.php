@extends('layouts.email')


@section('contenido')

    Us avisem que s'han actualitzat les dades de la família amfitriona del/la {{$booking->viajero->full_name}}. Podeu trobar la informació detallada de la família en la vostra àrea de clients, a l'apartat 'Allotjament'.

@stop