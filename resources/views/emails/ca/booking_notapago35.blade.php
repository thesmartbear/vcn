@extends('layouts.email')


@section('contenido')

Us recordem que abans del {{Carbon::parse($booking->course_start_date)->subDays(30)->format('d/m/Y')}} heu de fer efectiu el pagament final del programa.
<br>
L'import que us queda per pagar és de {{ConfigHelper::parseMoneda($booking->saldo_pendiente)}}.
<br>
El pagament s'ha de fer via transferència bancària al número de compte {{$booking->oficina?$booking->oficina->txtIban($booking):"-"}} de {{$booking->oficina?$booking->oficina->banco:"-"}}. Recordeu enviar-nos el comprovant de l'ingrés amb el nom del participant i nom del programa al correu {{$booking->oficina?$booking->oficina->email:"-"}}.
<br><br>
Us adjuntem la nota de pagament i us recordem que la podeu trobar també a la vostra àrea de client.

<br><br>
Nom i cognom: {{$booking->viajero->full_name}}
<br>
Programa: {{ $booking->programa}}
<br>
Data d'inici: {{$booking->curso_start_date}}
<br>
Data final: {{$booking->curso_end_date}}

<br><br><br>
Salutacions!

@stop