@extends('layouts.email')


@section('contenido')

Hola {{$destino->fname ?? "-"}}!
{{$ficha->name ?? "-"}} ja te activat el seu test d’anglès online.
Ens permetrà veure el seu nivell de gramàtica, vocabulari i comprensió lectora.
Comptar uns 45 minuts per fer-ho. Al finalitzar el test, s’informarà de la nota i del nivell corresponent.

S’accedeix al test fent <a href="{{$examen_link ?? '-'}}">clic aquí</a>.
Si l’enllaç anterior no funciona, recomanem copiar i enganxar la URL següent al navegador: {{$examen_link ?? "-"}}.
Se us demanarà usuari (el vostre email) i contrasenya per entrar.
En el cas que no recordeu la contrasenya (ens passa a tots i totes molt sovint), podeu establir una de nova aquí: <a href="{{$web  ?? '#'}}/auth/password/email">{{$web  ?? 'web'}}/auth/password/email</a>

Si teniu qualsevol pregunta en el moment de realitzar el test, estem disponibles per ajudar-vos per email ({{$oficina_email ?? "-"}}) o telèfon ({{$oficina_tlf ?? "-"}}).
Salutacions!

@stop