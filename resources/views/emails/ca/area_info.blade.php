@extends('layouts.email')


@section('contenido')

    Per fi tenim habilitada <a href="{{$weba}}">l'àrea de clients</a>.
    <br>

    A partir d'ara hi podreu accedir sempre que volgueu a través de la secció "àrea de clients" que trobareu al menú de la nostra web <a href="{{$web}}">{{$web}}</a>. Heu de tenir en compte que la primera vegada que hi accediu haureu de canviar la contrasenya.

    <br><br>
    USUARI: {{$user->username}}
    <br>
    CONTRASENYA TEMPORAL: {{$password}}

    <br><br>
    A partir d'ara us anirem publicant informació sobre el programa de forma gradual.
    <br>Rebreu un correu cada vegada que hi hagi alguna cosa nova.

    <br><br>
    Moltes gràcies!

@stop