@extends('layouts.email')


@section('contenido')

@if(isset($tarea))
    Hola {{$tarea->asignado->full_name}}, {{$tarea->user?$tarea->user->full_name:"-"}} te acaba de asignar una tarea de booking.
@else
    Hola -, - te acaba de asignar una tarea de booking.
@endif

@if(isset($tarea))
    <a href="{{route('manage.bookings.ficha', $tarea->incidencia->booking_id)}}">
        Booking {{$tarea->incidencia->booking_id  ?? '-'}}
    </a>
    {{$tarea->notas  ?? '-'}} :: {{Carbon::parse($tarea->fecha)->format('d/m/Y h:i')}}
@else
    <a href="#">
        Booking -
    </a>
    tarea :: fecha
@endif


Have a nice day!

@stop