@extends('layouts.email')

@section('contenido')

Hola {{$booking->asignado->full_name  ?? 'asignado'}},

Tienes una pre-reserva que caducará mañana.

Viajero: {{$booking->viajero->full_name  ?? '-'}}
Tutores: {{$booking->viajero->tutor1_name  ?? '-'}} y {{$booking->viajero->tutor2_name  ?? '-'}}

Programa: {{$booking->programa  ?? '-'}}
Convocatoria: {{$booking->convocatoria->name  ?? '-'}}

El viajero y sus tutores recibieron un email de aviso automático ayer. Si todavía no han confirmado nada,
hoy sería un buen día para llamarles y preguntar cuando la quieren confirmar.

Suerte!

@stop