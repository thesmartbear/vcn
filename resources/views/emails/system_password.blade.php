@extends('layouts.email')


@section('contenido')

    @component('mail::layout')

        {{-- Header --}}
        @slot('header')
            @component('mail::header', ['url' => config('app.url')])
                {{$asunto}}
            @endcomponent
        @endslot

        {{-- Body.ini --}}
        Hemos recibido tu solicitud de recuperación de contraseña.

        Por razones de seguridad, este enlace SÓLO ES VÁLIDO UNA VEZ.
        
        Si por alguna razón, no acabas el proceso de recuperación de contraseña después de haber hecho clic en el enlace por primera vez, entonces tendrás que volver a pedir un nuevo enlace de recuperación.
        
        {{-- Body.end --}}
        
        @slot('footer')
            @component('mail::footer')
                © {{ date('Y') }}
            @endcomponent
        @endslot

    @endcomponent

@stop