@extends('layouts.email')


@section('contenido')

@if(isset($tarea))
    Hola {{$tarea->asignado->full_name}}, {{$tarea->user?$tarea->user->full_name:"-"}} te acaba de asignar una tarea.
@else
    Hola -, - te acaba de asignar una tarea.
@endif

@if(isset($tarea))
    <a href="{{route('manage.viajeros.ficha',$tarea->viajero_id)}}">
        {{$tarea->viajero->full_name}}
    </a>
    {{$tarea->notas}} :: {{Carbon::parse($tarea->fecha)->format('d/m/Y h:i')}}
@else
    <a href="#">
        Viajero
    </a>
    {{$tarea->notas  ?? "tarea"}} :: fecha
@endif


Have a nice day!

@stop