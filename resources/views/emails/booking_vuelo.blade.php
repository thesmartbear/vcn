@extends('layouts.email')


@section('contenido')

Hola,
{{$user->full_name}} acaba de asignar/cambiar Vuelo en el <a href="{{$base_url}}{{route('manage.bookings.ficha',$booking->id,false)}}#vuelo">Booking {{$booking->id}}</a> del Viajero {{$booking->viajero->full_name}}.

<br><br>
Booking: <a href="{{$base_url}}{{route('manage.bookings.ficha',$booking->id,false)}}#vuelo">{{$booking->id}}</a>
<br><br>

@if($booking->vuelo)

    @if($booking->vuelo->transporte_requisitos)
    <br>Requisitos especiales: {{$booking->vuelo->transporte_requisitos}}
    @endif

    @if($booking->vuelo->transporte_otro)
    <br>Vuelo Alternativo: {{$booking->vuelo->transporte_detalles}}
    @endif

    @if($booking->vuelo->transporte_recogida)
    <br>Aporte: {{$booking->vuelo->transporte_recogida}}
    @endif

@endif

<br><br><br>
Have a nice day!

@stop