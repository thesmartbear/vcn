@extends('layouts.email')


@section('contenido')

Datos modificados {{$viajero->full_name  ?? '-'}} [{{ $tab  ?? "tab" }}]:

{{$datos  ?? "..."}}

@stop