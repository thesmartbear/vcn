@extends('layouts.email')


@section('contenido')

# Descarga CATÁLOGO landing web:

Origen: {{$ficha->landing->name ?? "-"}}

Nombre: {{$ficha->nombre ?? "-"}}
Teléfono: {{$ficha->telefono ?? "-"}}
Email: {{$ficha->email ?? "-"}}
F.Nacimiento: {{$ficha->fechanac_dmy ?? "-"}}
Viajero: {{ ($ficha->es_viajero ? "SI" : "NO") ?? "-"}}
@if($ficha->es_viajero ?? false)
Tutor: {{$ficha->tutor_nombre ?? "-"}} [{{$ficha->tutor_telefono ?? "-"}}]
@endif
Destino: {{$ficha->destino ?? "-"}}
Duración: {{$ficha->duracion ?? "-"}}

+INFO: {{$ficha->info_ip ?? "-"}}
GEO: {{$ficha->info_geo ?? "-"}} :: {!! $ficha->info_mapa ?? "-" !!}

@stop