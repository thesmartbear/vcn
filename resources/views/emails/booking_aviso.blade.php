@extends('layouts.email')

@section('contenido')

Booking {{$booking->id  ?? '-'}}:
Curso: <a href="{{$web  ?? '#'}}/manage/bookings/ficha/{{$booking->id  ?? '-'}}">{{$booking->curso->name  ?? '-'}}</a>
Convocatoria: {{$booking->convocatoria_name ?? "-"}}
Viajero: <a href="{{$web  ?? '#'}}/manage/viajeros/ficha/{{$booking->viajero_id  ?? '-'}}">{{$booking->viajero->full_name  ?? '-'}}</a>
Viaje:
@if(isset($booking))
    Del {{Carbon::parse($booking->course_start_date)->format('d/m/Y')}} al {{Carbon::parse($booking->course_end_date)->format('d/m/Y')}}
@else
    Del - al -
@endif
Estado Booking: {{$booking->status_name  ?? '-'}}
Oficina: {{$booking->oficina_name ?? '-'}}
Asignado: {{$booking->asignado_name ?? '-'}}
<br>
@if(isset($booking))
    @if($booking->es_online)
        @if(!$booking->es_online_tpv)
            @if($booking->es_online_comprobante)
                Pago por transferencia. Verificar comprobante de pago colgado en booking.
            @else
                Reclamar pago. No se adjuntó ningún comprobante de pago.
            @endif
        @endif
    @endif
@else
    Pago por transferencia. Verificar comprobante de pago colgado en booking. / Reclamar pago. No se adjuntó ningún comprobante de pago.
@endif

@stop