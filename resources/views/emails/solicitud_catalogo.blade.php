@extends('layouts.email')


@section('contenido')

Enviar catálogo al viajero <a href="{{route('manage.viajeros.ficha', $ficha->viajero_id)}}">{{$ficha->viajero->full_name}}</a>

Marcado por: {{$ficha->catalogo_envio_log}}


¡Gracias!

@stop