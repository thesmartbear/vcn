@extends('layouts.email')


@section('contenido')

    @component('mail::layout')

        {{-- Header --}}
        @slot('header')
            @component('mail::header', ['url' => config('app.url')])
                {{$asunto}}
            @endcomponent
        @endslot

        {{-- Body.ini --}}
        Oficina: {{$oficina  ?? '-'}}
        Nombre: {{$name  ?? '-'}}
        Email: {{$email  ?? '-'}}
        Mensaje: {{$message  ?? "..."}}

        {{-- Body.end --}}
        @slot('footer')
            @component('mail::footer')
                © {{ date('Y') }}
            @endcomponent
        @endslot

    @endcomponent

@stop