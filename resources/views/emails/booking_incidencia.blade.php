@extends('layouts.email')


@section('contenido')

@if(isset($incidencia))
    Hola {{$incidencia->asignado->full_name}}, {{$incidencia->user->full_name}} te acaba de asignar una incidencia.
@else
    Hola -, - te acaba de asignar una incidencia.
@endif

@if(isset($incidencia))
    <a href="{{route('manage.bookings.ficha',$incidencia->booking_id)}}">
        {{$incidencia->booking->viajero->full_name}}
    </a>
    {{$incidencia->notas}} :: {{Carbon::parse($incidencia->fecha)->format('d/m/Y h:i')}}
@else
    <a href="#">
        Viajero
    </a>
    incidencia :: fecha
@endif


Have a nice day!

@stop