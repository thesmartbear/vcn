@extends('layouts.email')


@section('contenido')

    Nuevo Documento específico adjunto:

    Booking {{$booking->id  ?? '-'}}:
    Curso: <a href="{{$web  ?? '#'}}/manage/bookings/ficha/{{$booking->id  ?? '-'}}">{{$booking->curso->name  ?? "curso"}}</a>
    Viajero: <a href="{{$web  ?? '#'}}/manage/viajeros/ficha/{{$booking->viajero_id  ?? '-'}}">{{$booking->viajero->full_name  ?? 'viajero'}}</a>
    Documento: {{$doc->name  ?? "doc"}}

@stop