@extends('layouts.email')


@section('contenido')

Benvolguts,
<br>
Us fem arribar la informació dels vols per l'estada d'anglès a Bournemouth:
<br><br>
Companyia: British Airways
<br><br>
Vol d'anada: 03/07/2016
Sortida aeroport Barcelona: 07:10h
Arribada aeroport Heathrow: 08:30h
<br><br>
Vol de tornada: 17/07/2016
Sortida aeroport Heathrow: 16:30h
Arribada aeroport Barcelona: 19:45h
<br><br>
La informació dels vols és orientativa, en cas que hi hagi qualsevol canvi per part de la companyia, us informarem.
<br><br>
El preu del vol és de 436€, que caldrà que aboneu per transferència abans del 19/04/2016 al número de compte de British Summer: La Caixa IBAN: ES70 2100 7332 0902 0014 7281.
<br><br>
Si teniu qualsevol consulta, no dubteu en contactar-nos.
<br><br>
Moltes gràcies!

@stop