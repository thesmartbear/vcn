@extends('layouts.email')


@section('contenido')

@component('mail::layout')

    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{$asunto}}
        @endcomponent
    @endslot

    {{-- Body.ini --}}
    Esta persona acaba de pedirnos más información a través de nuestra web. Contactarla lo antes posible para ayudarla.

    Nombre: {{$nombre  ?? '-'}}
    Teléfono: {{$telefono  ?? '-'}}
    E-mail: {{$email  ?? '-'}}
    @if(isset($viajar))
        Viajero: {{ $viajar?"SI":"NO" }}
    @else
        Viajero: -
    @endif
    FechaNac: {{$fechanac  ?? '-'}}
    Ciudad: {{$ciudad  ?? '-'}}
    Curso: {{$curso_id  ?? '-'}}:: <a href="{{$curso_link  ?? '#'}}">{{$curso  ?? '-'}}</a>
    Idioma: {{$idioma  ?? '-'}}
    {{-- Body.end --}}

    @component('mail::button', ['url' => $curso_link])
        Ver Curso
    @endcomponent

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')

        @endcomponent
    @endslot

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }}
        @endcomponent
    @endslot

@endcomponent

@stop