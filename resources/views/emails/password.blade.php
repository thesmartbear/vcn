@extends('layouts.email')


@section('contenido')

    Haz clic en el enlace siguiente para cambiar la contraseña de acceso al área de clientes: {{ url('auth/password/reset/'.$token) }}

    <br><br>
    Por razones de seguridad, este enlace SÓLO ES VÁLIDO UNA VEZ.
    <br>
    Si por alguna razón, no acabas el proceso de recuperación de contraseña después de haber hecho clic en el enlace por primera vez, entonces tendrás que volver a pedir un nuevo enlace de recuperación.

@stop