@extends('layouts.email')


@section('contenido')

Nombre Viajero: {{$booking->viajero->full_name  ?? '-'}}
Programa: {{ isset($booking)  ? ($booking->convocatoria?$booking->convocatoria->name:"-") : "-"}}
Vuelo: {{$booking->vuelo->name  ?? '-'}}
Usuario que ha hecho el cambio: {{$user->full_name  ?? '-'}}
<br>
Cambios:
@if(isset($cambios))
<ul>
    @foreach($cambios as $kc=>$vc)
        <li>
            {{$kc}}: {{$inicial[$kc]}} => {{$vc}}
        </li>
    @endforeach
</ul>

<a href="{{$base_url}}{{route('manage.bookings.ficha',$booking->id, false)}}">Ver Booking</a>
@else
    ...

    <a href="#">Ver Booking</a>
@endif

@stop