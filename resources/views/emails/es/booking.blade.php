@extends('layouts.email')


@section('contenido')

    Hemos tramitado vuestra solicitud de inscripción a {{ $booking->programa ?? '-' }} para {{$booking->viajero->full_name ?? '-'}}.
    Ahora recibiréis un email con vuestros datos de acceso al área de clientes de nuestra web.

    @if(isset($booking))
    
        @if( $booking->curso->es_convocatoria_multi )
            Especialidades:
            <ul>
            @foreach($booking->multis->sortBy('n') as $multi)
                <li>Semana {{$multi->n}}: {{$multi->especialidad_name}}</li>
            @endforeach
            </ul>
        @endif

        @if($booking->es_online)
            @if(!$booking->es_online_tpv)
                @if($booking->es_online_comprobante)
                    Cuando acusemos recepción de su transferencia, su inscripción será confirmada.
                @else
                    Recuerda enviarnos el comprobante de pago por email a {{ConfigHelper::config('email')}}. Sino, su solicitud de inscripción caducará en 5 días.
                @endif
            @endif
        @endif
    @else
        Cuando acusemos recepción de su transferencia, su inscripción será confirmada. / Recuerda enviarnos el comprobante de pago por email a -. Sino, su solicitud de inscripción caducará en 5 días.
    @endif

    ¡Muchas gracias!

@stop