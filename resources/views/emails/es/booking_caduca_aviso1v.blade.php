@extends('layouts.email')


@section('contenido')

Hola {{$hola->full_name  ?? '-'}},

Tu pre-reserva para el programa de {{ $booking->programa  ?? '-'}} caduca en {{ isset($booking) ? ($booking->es_online?1:2) : "x"}} día/días. 

Si aún sigues interesado/a, por favor realiza el pago y envíanos la hoja de inscripción junto con el comprobante de transferencia para formalizar la inscripción.
Si tienes alguna duda o por algo no te interesa, por favor llama al {{$booking->viajero->oficina->telefono  ?? '-'}}.


¡Ya sabes que nos encantará contar contigo este verano!

{{$booking->viajero->asignado->full_name  ?? 'asignado'}}

@stop