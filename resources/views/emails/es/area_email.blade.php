@extends('layouts.email')


@section('contenido')

    Ya podéis acceder a la información a través del área de clientes de nuestra web <a href="{{$web}}">{{$web}}</a>. Tened en cuenta que la primera vez que accedáis deberéis cambiar la contraseña.

    <br><br>
    USUARIO: {{$user->username}}
    <br>
    CONTRASEÑA TEMPORAL: {{$password}}

    <br><br>
    A partir de ahora iremos publicando información sobre el programa de forma gradual. Recibiréis un correo cada vez que publicamos algo nuevo.
    <br><br>
    ¡Muchas gracias!

@stop