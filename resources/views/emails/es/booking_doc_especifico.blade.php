@extends('layouts.email')

@section('contenido')

El documento que habéis subido para {{$doc->name  ?? '-'}} no es correcto.


Por favor, entrar en vuestra área de cliente <a href="{{$web  ?? '#'}}">{{$web  ?? 'web'}}</a> y subir el documento correcto.

Si tenéis cualquier duda, podéis contactar con nosotros por email ({{$booking->asignado->email  ?? '-'}}) o por teléfono ({{$booking->viajero->oficina->telefono  ?? '-'}}).



¡Gracias!

@stop