@extends('layouts.email')

<?php
$p = $booking->plataforma;
$web = ConfigHelper::config('area_url',$p)?:ConfigHelper::config('web',$p);
//$web = $base_url . route('area.comprar.booking', $booking->id, false);
?>

@section('contenido')

    Se acaba de crear una pre-inscripción en el programa de {{ $booking->programa  ?? '-'}} para {{$booking->viajero->full_name  ?? '-'}}.
    Para formalizarla, es necesario completar los datos y realizar el pago del depósito a cuenta.
    Hay un plazo de 2 días para hacerlo antes de que caduque la pre-inscripción.

    <strong>¿Cómo completar la inscripción?</strong>

    Hacer clic aquí o copiar/pegar la url siguiente en vuestro navegador: <a href="{{$web  ?? '#'}}">{{$web  ?? 'web'}}</a> para acceder a vuestro área personal.
    Podéis acceder con vuestro email y la contraseña que os hemos enviado.
    Si no os acordáis de la contraseña, la podéis recuperar en la misma página haciendo clic en "¿Has olvidado tu contraseña?".


    Una vez dentro de vuestro área personal, sólo tendréis que rellenar los datos según los pasos siguientes:
    Paso 1: datos del curso
    Paso 2: datos del viajero
    Paso 3: datos de los tutores
    Paso 4: revisar datos y escoger método de pago
    Paso 5: realizar pago por transferencia o tarjeta.

    Al final del proceso, recibiréis un mensaje de confirmación.

@stop