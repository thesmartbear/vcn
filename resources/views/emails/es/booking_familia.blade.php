@extends('layouts.email')


@section('contenido')

Os avisamos que se han actualizado los datos de la familia de {{$booking->viajero->full_name}}. Podéis encontrar la información detallada de la familia en vuestra área de clientes, en el apartado 'Alojamiento'.

@stop