@extends('layouts.email')


@section('contenido')

    Os confirmamos que hemos recibido el pago de {{$pago->importe_moneda  ?? '-'}} para el programa {{$booking->curso->name  ?? '-'}} de {{$booking->viajero->full_name  ?? '-'}}.
    Adjuntamos recibo de pago.

@stop