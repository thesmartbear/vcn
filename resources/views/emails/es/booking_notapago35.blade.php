@extends('layouts.email')


@section('contenido')

Os recordamos que antes del {{Carbon::parse($booking->course_start_date)->subDays(30)->format('d/m/Y')}} se debe realizar el pago final del programa.
<br>
El importe que os queda por pagar es de {{ConfigHelper::parseMoneda($booking->saldo_pendiente)}}.
<br>
El pago debe hacerse vía transferencia bancaria al número de cuenta {{$booking->oficina?$booking->oficina->txtIban($booking):"-"}} de {{$booking->oficina?$booking->oficina->banco:"-"}}. Recordad enviarnos el comprobante del ingreso con el nombre del participante y nombre del programa al correo {{$booking->oficina?$booking->oficina->email:"-"}}.
<br><br>
Os adjuntamos la nota de pago correspondiente. Y os recordamos que también la podéis encontrar en vuestra área de clientes.

<br><br>
Nombre y apellido: {{$booking->viajero->full_name}}
<br>
Programa: {{ $booking->programa}}
<br>
Fecha de inicio: {{$booking->curso_start_date}}
<br>
Fecha final: {{$booking->curso_end_date}}

<br><br><br>
¡Saludos!

@stop