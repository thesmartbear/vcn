@extends('layouts.email')


@section('contenido')

¡Hola {{$destino->fname ?? "-"}}!
Ya está activado tu test de inglés online.
Nos permitirá ver tu nivel de gramática, vocabulario y comprensión lectora.
Cuenta unos 45 minutos para hacerlo. Al finalizar el test, te daremos tu nota y el nivel correspondiente. 

Se accede al test haciendo <a href="{{$examen_link ?? '-'}}">clic aquí</a>.
En el caso de que el enlace anterior no funcione, copia y pega la url siguiente en tu navegador: {{$examen_link ?? "-"}}.
Te pedirán usuario (tu email) y contraseña para entrar. 
En el caso (bastante habitual) de no acordarte de tu contraseña (pues si, nos pasa a todxs muy a menudo), puedes establecer una nueva contraseña aquí: <a href="{{$web  ?? '#'}}/auth/password/email">{{$web  ?? 'web'}}/auth/password/email</a>

Si tienes cualquier pregunta en el momento de realizar el test, estamos disponibles para ayudarte por email ({{$oficina_email ?? "-"}}) o teléfono ({{$oficina_tlf ?? "-"}}).

¡Saludos!

@stop