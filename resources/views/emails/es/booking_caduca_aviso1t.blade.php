@extends('layouts.email')


@section('contenido')

Hola {{$hola->full_name  ?? '-'}},

La pre-reserva para {{$booking->viajero->full_name  ?? '-'}} en el programa de {{$booking->programa  ?? '-'}} caduca en {{ isset($booking) ? ($booking->es_online?1:2) : "x"}} día/días. 

Si aún seguís interesados/as por favor realizad el pago y enviadnos la hoja de inscripción junto con el comprobante de transferencia para formalizar la inscripción.
Si tenéis alguna duda o por algo no os interesa por favor llamad al {{$booking->viajero->oficina->telefono  ?? '-'}}. 

¡Ya sabéis que nos encantará contar con vosotros este verano!

{{$booking->viajero->asignado->full_name  ?? 'asignado'}}

@stop