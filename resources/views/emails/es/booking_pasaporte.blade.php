@extends('layouts.email')

<?php
$p = $booking->plataforma?:1;
$web = ConfigHelper::config('area_url',$p)?:ConfigHelper::config('web',$p);
?>

@section('contenido')

Estamos a punto de emitir billetes de avión y necesitamos confirmación de los datos de pasaporte de {{$booking->viajero->full_name}}. Esta información es MUY IMPORTANTE ya que cualquier error puede provocar que {{$booking->viajero->full_name}} no pueda viajar con el billete previsto y posiblemente se tendría que emitir un nuevo billete con cargo al estudiante.
<br>
Os informamos a continuación de los datos que tenemos en nuestro sistema, que son los que se enviarán a las compañías aéreas. Si detectáis cualquier error, podéis modificar los datos entrando en vuestra área de cliente desde <a href="{{$web}}">{{$web}}</a>.<br>
Os recordamos que nombre y apellidos deben constar tal como aparecen en el pasaporte. 
<br><br>

Programa: {{ $booking->programa}}
<br>
Nombre: {{$booking->viajero->name}}
<br>
Apellidos: {{$booking->viajero->lastname}} {{$booking->viajero->lastname2}}
<br>
Fecha de nacimiento: {{Carbon::parse($booking->viajero->fechanac)->format('d/m/Y')}}
<br>
Nº de pasaporte: {{$booking->viajero->datos->pasaporte}}
<br>
Fecha de emisión: {{Carbon::parse($booking->viajero->datos->pasaporte_emision)->format('d/m/Y')}}
<br>
Fecha de caducidad: {{Carbon::parse($booking->viajero->datos->pasaporte_caduca)->format('d/m/Y')}}
<br>
País de emisión: {{$booking->viajero->datos->pasaporte_pais}}
<br>
Nacionalidad: {{$booking->viajero->datos->nacionalidad}}

@stop