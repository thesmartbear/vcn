@extends('layouts.email')


@section('contenido')

¡Hola {{$destino->fname ?? "-"}}!
Ya está activado el test de inglés online para {{$viajero->name ?? "-"}}.
Nos permitirá ver el nivel de gramática, vocabulario y comprensión lectora.
Contar unos 45 minutos para hacerlo. Al finalizar el test, se informará de la nota y del nivel correspondiente. 

Se accede al test haciendo <a href="{{$examen_link ?? '-'}}">clic aquí</a>.
En el caso de que el enlace anterior no funcione, recomendamos copiar y pegar la url siguiente en el navegador: {{$examen_link ?? "-"}}. 
Para poder acceder al test, pediremos usuario y contraseña, enviados ahora en un email separado. 
En el caso (bastante habitual) de no acordarte de tu contraseña (pues si, nos pasa a todxs muy a menudo), puedes establecer una nueva contraseña aquí: <a href="{{$web  ?? '#'}}/auth/password/email">{{$web  ?? 'web'}}/auth/password/email</a>

Si tenéis cualquier pregunta en el momento de realizar el test, estamos disponibles para ayudaros por email ({{$oficina_email ?? "-"}}) o teléfono ({{$oficina_tlf ?? "-"}}).

¡Saludos!

@stop