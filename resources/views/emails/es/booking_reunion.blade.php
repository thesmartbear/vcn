@extends('layouts.email')


@section('contenido')

    Queridos amigos,

    Os queremos invitar (padres/madres e hijos/as) a las reuniones informativas del programa en el que estáis inscritos y donde podréis aclarar todas las dudas que tengáis. Además podréis conocer otros participantes.
    
    Detalles de la sesiones informativas del programa {{ $booking->programa  ?? '-'}}:
    @if(isset($booking))
        @foreach($booking->reuniones as $reunion)
            @if($reunion->notas)
                {!! $reunion->notas !!}<br>
            @endif
            Fecha: {{$reunion->fecha->format('d/m/Y')}}
            Hora: {{$reunion->hora}}
            Lugar: {{$reunion->lugar}}
            <hr>
        @endforeach
    @else
        Fecha: -
        Hora: -
        Lugar: -
    @endif
    ¡Os esperamos!
@stop