@extends('layouts.email')


@section('contenido')

    {{$booking->viajero->full_name  ?? 'viajero'}} ha cancelado.

    Booking: <a href="{{$base_url}}{{route('manage.bookings.ficha',$booking->id, false)}}">{{ isset($booking)  ? ($booking->id) : "-"}}</a>
    Viajero: <a href="{{$base_url}}{{route('manage.viajeros.ficha',$booking->viajero_id, false)}}">{{ isset($booking)  ? ($booking->viajero->name) : "-"}}</a>
    Programa: {{ isset($booking)  ? ($booking->programa) : "-"}}
    Fecha inscripción: {{isset($booking)  ? ($booking->created_at->format('d/m/Y')) : "-"}}
    <br>
    Tenía contratado Seguro cancelación: {{isset($booking)  ? ($booking->es_seguro_cancelacion?"Si":"No") : "-"}}
    <br>
    Pendiente refund: {{isset($booking)  ? ($booking->es_refund?"Si":"No") : "-"}}
    <br>
    Booking facturado total o parcialmente: {{isset($booking)  ? ($booking->tiene_facturas?"Si":"No") : "-"}}

@stop