@extends('layouts.email')

<?php
$es_viajero = $ficha->es_viajero?1:0;
$id = $ficha->id;

$id = rand(10000,99999) . $id;
$token = ConfigHelper::token();
// $ruta = $base_url . route('lopd.set', [ $es_viajero, $id,'token'=> $token ], false);
$ruta = $plataforma->area_url ."/set-lopd/". $es_viajero ."/". $id ."?token=" . $token;

$boton = "CLARO QUE SÍ, QUIERO SEGUIR RECIBIENDO EMAILS Y CORREOS POSTALES DE ".$plataforma->name;

$mas = "";
if($plataforma->id == 1)
{
    $mas = " e incluso invitarte a algún aniversario (el año que viene cumplimos ¡35 años!)";
}

$firma = "British Summer Experiences, SL - Via Augusta, 33, Entresuelo 2ª, 08006 Barcelona.";
if($plataforma->id == 2)
{
    $firma = "Institució Cultural del CIC - Via Augusta, 205, 08021 Barcelona.";
}

?>

@section('contenido')

@if(isset($asunto))
Asunto: {{$asunto}}<hr>
@endif

Seguro que estás recibiendo muchos mails como éste... así que te damos las GRACIAS por abrirlo. Lo que queremos contarte es IMPORTANTE. 
<br><br>
Este viernes 25 de mayo entra en vigor el nuevo reglamento europeo de protección de datos y necesitamos que vuelvas a darnos tu consentimiento para recibir nuestros emails y correos postales. ¡Prometemos que van a ser igual de poquitos que hasta ahora! Nos encanta enviaros nuestro nuevo catálogo una vez al año y de vez en cuando informarte de las novedades{{$mas}}.

<br><br>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{$ruta}}" style="height:38px;v-text-anchor:middle;width:720px;" arcsize="11%" strokecolor="#23ac11" fillcolor="#23ac11">
    <w:anchorlock/>
    <center style="color:#ffffff;font-family:sans-serif;font-size:13px;font-weight:bold;">{{$boton}}</center>
  </v:roundrect>
<![endif]--><a href="{{$ruta}}"
style="background-color:#23ac11;border:1px solid #23ac11;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:38px;text-align:center;text-decoration:none;width:720px;-webkit-text-size-adjust:none;mso-hide:all;">{{$boton}}</a></div>

<br>
<hr>
El responsable del tratamiento de tus datos es {{$plataforma->factura_pie}}. Con tu aceptación explícita, nos autorizas al tratamiento de los datos personales que nos facilitaste en su momento tanto del / la participante así como de los tutores.

<br><br>
<strong>Política de Protección de Datos de Carácter Personal</strong>
Conforme al reglamento general de protección de datos (RGPD) y la Ley de Servicios de la Sociedad de la Información y de Comercio Electrónico (LSSI-CE), los datos de carácter personal que pueda contener este mensaje son objeto de tratamiento por parte de {{$plataforma->factura_pie}} con el objetivo de enviar información solicitada, novedades y actualizaciones. Recibes este correo porque has facilitado voluntariamente tu dirección de correo electrónico. Si quieres saber más sobre nuestro compromiso con tu privacidad o ejercer tus derechos, por favor consulta nuestra <a href="https://{{$plataforma->web}}/{{$ficha->idioma_contacto}}/Proteccion_de_datos_personales.html">política de privacidad</a>.
<br>
<i>{{$firma}}</i>

@stop