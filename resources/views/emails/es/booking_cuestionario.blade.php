@extends('layouts.email')


@section('contenido')

    ¡Hola!

    Te queremos pedir algo... ¿Nos puedes hacer el favor de contestar el cuestionario de {{$cuestionario->name  ?? '-'}}?
    Nos ayudará mucho tener tu opinión. Es muy rápido (menos de 1 minuto) y fácil.

    Entra en tu área de cliente (<a href="{{$web  ?? '#'}}">{{$web  ?? 'web'}}</a>) y haz clic en  el nombre del cuestionario dentro del recuadro azul de la derecha.


    En caso de no recordar la contraseña (si, nos pasa a tod@s...), puedes pedir una nueva aquí: <a href="{{$web  ?? '#'}}/auth/password/email">{{$web  ?? 'web'}}/auth/password/email</a>

    Al instante recibirás un email con un enlace para establecer tu nueva contraseña y poder acceder a tu área enseguida. 

@stop