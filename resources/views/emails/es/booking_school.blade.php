@extends('layouts.email')


@section('contenido')

Os avisamos que se han actualizado los datos de la Escuela de {{$booking->viajero->full_name}}. Podéis encontrar la información detallada de la Escuela en vuestra área de clientes, en el apartado 'Escuela'.

@stop