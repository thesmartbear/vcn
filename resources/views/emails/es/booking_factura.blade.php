@extends('layouts.email')


@section('contenido')

Os recordamos que antes del {{ isset($booking) ? (Carbon::parse($booking->course_start_date)->subDays(30)->format('d/m/Y')) : "-"}} se debe realizar el pago final del programa.


El importe que os queda por pagar es de {{ isset($booking) ? ConfigHelper::parseMoneda($booking->saldo_pendiente) : "-"}}.

El pago debe hacerse vía transferencia bancaria al número de cuenta {{ isset($booking) ? ($booking->oficina?$booking->oficina->txtIban($booking):"-") : "-"}} de {{ isset($booking) ? ($booking->oficina?$booking->oficina->banco:"-") : "-"}}.
Recordad enviarnos el comprobante del ingreso con el nombre del participante y nombre del programa al correo {{ isset($booking) ? ($booking->oficina?$booking->oficina->email:"-") : "-"}}.

Os adjuntamos la factura correspondiente.


Nombre y apellido: {{$booking->viajero->full_name  ?? '-'}}
Programa: {{ isset($booking) ? ($booking->programa) : "-"}}
Fecha de inicio: {{$booking->curso_start_date  ?? '-'}}
Fecha final: {{$booking->curso_end_date  ?? '-'}}


¡Saludos!


@stop