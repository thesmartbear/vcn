@extends('layouts.email')


@section('contenido')

{!! $intro ?? 'intro' !!}

Importe a pagar: {{$importe ?? 'importe'}}

El pago debe hacerse vía transferencia bancaria al número de cuenta {{ isset($booking) ? ($booking->oficina ? $booking->oficina->txtIban($booking) : '-') : '-' }} de {{ isset($booking) ? ($booking->oficina ? $booking->oficina->banco : '-') : '-' }}.

Recordad enviarnos el comprobante del ingreso con el nombre del participante y nombre del programa al correo {{ isset($booking) ? ($booking->oficina ? $booking->oficina->email : '-') : '-' }}.


Nombre y apellido: {{$booking->viajero->full_name ?? '-'}}
Programa: {{ $booking->programa ?? '-'}}
Fecha de inicio: {{$booking->curso_start_date ?? '-'}}
Fecha final: {{$booking->curso_end_date ?? '-'}}


¡Saludos!

@stop