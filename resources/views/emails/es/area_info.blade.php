@extends('layouts.email')


@section('contenido')

    Por fin hemos habilitado el <a href="{{$weba}}">área de clientes</a>.
    <br>
    Podéis acceder siempre que queráis a través de la sección "área de clientes" de nuestra web <a href="{{$web}}">{{$web}}</a>. Debéis tener en cuenta que la primera vez que se accede se ha de cambiar la contraseña.

    <br><br>
    USUARIO: {{$user->username}}
    <br>
    CONTRASEÑA TEMPORAL: {{$password}}

    <br><br>
    A partir de ahora os iremos publicando información sobre el programa de forma gradual.
    <br>Recibiréis un correo cada vez que publiquemos alguna novedad.

    <br><br>
    ¡Muchas gracias!

@stop