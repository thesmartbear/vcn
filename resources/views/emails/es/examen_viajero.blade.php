@extends('layouts.email')


@section('contenido')

¡Hola {{$destino->fname ?? "-"}}!
Ya está activado tu test de inglés online.
Nos permitirá ver tu nivel de gramática, vocabulario y comprensión lectora.
Cuenta unos 45 minutos para hacerlo. Al finalizar el test, te daremos tu nota y el nivel correspondiente. 

Se accede al test haciendo <a href="{{$examen_link ?? '-'}}">clic aquí</a>.
En el caso de que el enlace anterior no funcione, copia y pega la url siguiente en tu navegador: {{$examen_link ?? "-"}}.
Te pedirán usuario (tu email) y contraseña para entrar. Te los acabamos de enviar ahora en un email separado. 

Si tienes cualquier pregunta en el momento de realizar el test, estamos disponibles para ayudarte por email ({{$oficina_email ?? "-"}}) o teléfono ({{$oficina_tlf ?? "-"}}).

¡Saludos!

@stop