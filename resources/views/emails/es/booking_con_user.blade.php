@extends('layouts.email')


@section('contenido')

    Hemos tramitado vuestra solicitud de inscripción a {{ $booking->programa}} para {{$booking->viajero->full_name}}. Podéis ver más detalles desde vuestro área de clientes (hacer clic en 'Área de clientes desde el menú de nuestra web).
    <br><br>

    @if($booking->es_online)
        @if(!$booking->es_online_tpv)
            @if($booking->es_online_comprobante)
                Cuando acusemos recepción de su transferencia, su inscripción será confirmada.
            @else
                Recuerda enviarnos el comprobante de pago por email a {{ConfigHelper::config('email')}}. Sino, su solicitud de inscripción caducará en 5 días.
            @endif
            <br><br>
        @endif
    @endif

    ¡Muchas gracias!

@stop