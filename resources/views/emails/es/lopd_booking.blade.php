@extends('layouts.email')

<?php
$ruta = $plataforma->area_url;
$boton = "ACTUALIZAR ESTOS DATOS AHORA";
$web = $plataforma->web;

$seguro = "la Institución Cultural del CIC, AVI Internacional";
$organiza = "British Summer Experiences S.L. o la Institución Cultural del CIC";
$blog = "www.landedblog.com";
$firma = "British Summer Experiences, SL - Via Augusta, 33, Entresuelo 2ª, 08006 Barcelona.";
$banco = "; y el Banco de Sabadell (en el caso de solicitar financiación)";
if($plataforma->id == 2)
{
    $seguro = "BRITISH SUMMER EXPERIENCES, SL, AVI Internacional";
    $organiza = "INSTITUCIÓN CULTURAL DEL CIC o BRITISH SUMMER EXPERIENCES, SL";
    $blog = "www.landedblog.com/iccic";
    $firma = "Institució Cultural del CIC - Via Augusta, 205, 08021 Barcelona.";
    $banco = "";
}
?>

@section('contenido')

@if(isset($asunto))
Asunto: {{$asunto}}<hr>
@endif

Este viernes 25 de mayo entra en vigor el Reglamento (UE) 2016/679 de protección de datos personales. Necesitamos por ello tu autorización explícita para el tratamiento de los datos personales que nos habéis facilitado tanto del/la participante así como de los tutores. Si, es obligatorio aunque ya nos la habéis dado al firmar nuestras condiciones generales de venta...
El responsable del tratamiento de dichos datos es {{$plataforma->factura_pie}} y las finalidades son:
<br>
<br>- la gestión de la inscripción;
<br>- el envío de información sobre nuestros productos y servicios;
<br>- el uso de la imagen del/la participante en el marco de su participación en nuestros programas así como a la realización de cuestionarios sobre el programa realizado. 

<br><br>

<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{$ruta}}" style="height:38px;v-text-anchor:middle;width:600px;" arcsize="11%" strokecolor="#23ac11" fillcolor="#23ac11">
    <w:anchorlock/>
    <center style="color:#ffffff;font-family:sans-serif;font-size:13px;font-weight:bold;">{{$boton}}</center>
  </v:roundrect>
<![endif]--><a href="{{$ruta}}"
style="background-color:#23ac11;border:1px solid #23ac11;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:38px;text-align:center;text-decoration:none;width:600px;-webkit-text-size-adjust:none;mso-hide:all;">{{$boton}}</a></div>

<br><br>
Os llevará menos de 1 minuto actualizar estos datos y es imprescindible para poder gestionar correctamente las inscripciones y la comunicación con vosotros. Podréis revisar y cambiar estas autorizaciones en cualquier momento desde vuestra área de cliente.  
<br>
Si no recordáis vuestra contraseña para entrar en vuestra área de cliente, sólo tenéis que hacer clic en ‘¿Has olvidado tu contraseña?’ en nuestro apartado de ‘Área de cliente’, abajo de todo del menú derecho de nuestra web (https://{{$web}}). O directamente desde este enlace: <a href="{{$ruta}}">{{$ruta}}</a> (si no os deja hacer clic en el enlace o lo podéis copiar y pegar en vuestro navegador).  

<br><br>
<strong>Cláusula informativa sobre protección de datos personales</strong>
<br>
En cumplimiento del Reglamento (UE)2016/679 de protección de datos personales, le informamos del tratamiento de sus datos personales del que es responsable {{$plataforma->name}}, para las siguientes finalidades: Gestión de la inscripción, así como para enviarle información sobre nuestros productos y servicios por cualquier vía incluida la electrónica. Sus datos se tratan en base a su consentimiento y la ejecución del contrato de servicios. Los destinatarios de la información son: empresas colaboradoras de {{$plataforma->name}} en la ciudad de destino; {{$seguro}} (para gestión del seguro médico). Estas cesiones son necesarias para la prestación del servicio. Si no autoriza esta cesión, no podrá prestarse el servicio. También están incluidas en este supuesto las agencias de viajes colaboradoras (en caso de que {{$plataforma->name}} tramite la compra de los billetes de viaje){{$banco}}. El plazo de conservación previsto es el legal establecido en la normativa y en todo caso mientras no retire el consentimiento prestado / solicite la supresión. Le informamos de sus derechos de acceso, rectificación, supresión, portabilidad de datos, limitación y oposición, así como la revocación del consentimiento prestado en su caso, contactando con {{$plataforma->factura_pie}} o por e-mail a {{$plataforma->email}}. También tiene derecho a realizar una reclamación ante las autoridades de protección de datos.

<br><br>
<strong>Utilización de la imagen de los consumidores y realización de cuestionarios.</strong>
<br>
Los padres o representantes del estudiante autorizan expresamente a {{$plataforma->factura_pie}} para la realización de fotografías y otros materiales audiovisuales a los alumnos, y su utilización posterior en el marco de su participación en los programas organizados por {{$organiza}} (entidad con la que se comparten los programas de jóvenes). Dichas imágenes podrán ser publicadas en los diferentes soportes de ambas organizaciones: página web, redes sociales (Facebook, Twitter y similares), catálogo de cursos y el blog {{$blog}}, o como material publicitario siempre y cuando no exista una oposición explícita previa por parte del consumidor.
<br>
Dicha autorización podrá revocarse en cualquier momento, así como ejercitar los derechos de acceso, rectificación, cancelación y oposición dirigiéndose por escrito y adjuntando copia del DNI a {{$plataforma->factura_pie}}.
<br>
En el caso de que esta decisión causara daños y/o perjuicios el consumidor deberá indemnizar a {{$plataforma->factura_pie}}.
<br>
También se autoriza a los participantes menores de edad a realizar un cuestionario sobre el programa realizado.

<br>
<i>{{$firma}}</i>
@stop