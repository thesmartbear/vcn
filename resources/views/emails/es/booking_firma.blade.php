@extends('layouts.email')

@section('contenido')

    Ha habido cambios en su inscripción y se requiere volver a firmar la hoja de inscripción reflejando los cambios.

    La encontrará en el área de cliente: <a href="{{$web  ?? '#'}}">{{$web  ?? 'web'}}</a>.

@stop