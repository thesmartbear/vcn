@extends('layouts.email')

<?php
$p = $booking->plataforma;
$web = ConfigHelper::config('area_url',$p)?:ConfigHelper::config('web',$p);
$nom = ConfigHelper::config('name',$p);
?>

@section('contenido')

Estamos pendientes de recibir tu firma para la inscripción de {{$booking->viajero->full_name  ?? '-'}} al programa de {{ $booking->programa  ?? '-'}} .
Por favor, revisa tus datos y las condiciones y firma la hoja de inscripción.

Los pasos a seguir con los siguientes:
- Entrar en tu área de cliente desde <a href="{{$web  ?? '#'}}">{{$web  ?? 'web'}}</a> (el usuario es tu email. Hay un un botón '¿Has olvidado tu contraseña?' por si acaso).
- Encontrarás en tu área un recuadro amarillo y un botón verde para poder firmar. Hacer clic en el botón verde 'Firma'.
- Entonces verás un recuadro blanco donde podrás firmar. Debajo del recuadro, sale una visualización de la inscripción con las condiciones.

Te recomendamos que te conectes a tu área desde un tablet o un móvil, ya que será más fácil firmar con el dedo que con el ratón.


{{$nom  ?? "firma"}}

@stop