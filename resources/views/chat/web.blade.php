<?php
    $user = auth()->user();
    $chat = $user ? ($user->chat ?: null) : null;
    $chatId = $chat ? $chat->id : 0;
    $chatAbierto = 0;
    if($chat && $chat->status<10)
    {
        $chatAbierto = 1;
    }
?>

<div id="appChat" v-cloak>
    <a href="javascript:void(0)" @click="openChat"><i class="fa fa-comments"></i> CHAT</a>
    
    <div id="chat-web" class="chat_window" v-show="chatAbierto">
        <div class="top_menu">
            <div class="title" v-if="!chatForm">
                Chat [@{{userId}}] @{{chatId}} :: @{{chatStatus}}
                <div class="subtitle">
                    @{{chatTxt}}
                    <button class="btn btn-secondary btn-info btn-sm" @click="checkStatus(0)" v-show="chatStart"><i class="fa fa-refresh fa-fw"></i></button>
                    <button class="btn btn-secondary btn-danger btn-sm" @click="checkStatus(12)" v-show="chatStart">Cerrar</button>
                </div>
            </div>
            <div class="title" v-else>@lang("web.chatweb.formulario")</div>
        </div>

        {{-- <ul class="messages"></ul> --}}
        <div ref="chatMensajes" class="messages" v-show="chatVer">
            <chat-mensaje v-for="mensaje in mensajes" :key="mensaje.id" :mensaje="mensaje" :finalizado="false"></chat-mensaje>
        </div>

        <div class="bottom_wrapper clearfix" v-show="chatVer">
            <div class="message_input_wrapper">
                <input
                    id="btn-input"
                    type="text"
                    name="message"
                    class="message_input"
                    placeholder="Escribir mensaje..."
                    v-model="newMensaje"
                    @keyup.enter="sendMensaje"
                >
            </div>
            <div class="send_message">
                <div class="text" @click="sendMensaje">Enviar</div>
            </div>
        </div>

        <div class="chat_form" v-show="chatForm">
            <div class="mensaje" v-if="formEnviar">{!! trans('web.chatweb.formulario') !!}</div>
            <div class="mensaje" v-else>{!! trans('web.chatweb.gracias') !!}</div>
            
            <form action="" method="post" enctype="multipart/form-data" name="chatform" @submit="sendFormulario" v-if="formEnviar">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="form-group">
                            <label for="name">{!! trans('web.chatweb.nombre') !!}</label>
                            <input type="text" class="form-control" id="name" v-model="formName" placeholder="{!! trans('web.chatweb.nombre') !!}" required>
                        </div>
                        <div class="form-group">
                            <label for="email">{!! trans('web.chatweb.email') !!}</label>
                            <input type="text" class="form-control" id="email" v-model="formEmail" placeholder="{!! trans('web.chatweb.email') !!}" required>
                        </div>
                    </div>
                </div>
                <div id="group-lopd" class="form-group">
                    <input type="checkbox" id="lopd1" name="lopd1" required>
                    <label for="lopd1">@lang('web.formulario.lopd1')</label>
                    <br>
                    <input type="checkbox" id="lopd2" name="lopd2" required>
                    <label for="lopd2">@lang('web.formulario.lopd2', ['plataforma'=> ConfigHelper::plataformaApp()])</label>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary enviar" type="submit">@lang('web.chatweb.enviar')</button>                
                </div>
            </form>
        </div>
        
    </div>
    
</div>

@push('scripts')
<script type="text/javascript">
    
    const appChat = new Vue({
        el: '#appChat',
        data: () => ({
            userId: {{$user ? $user->id : 0}},
            chatAbierto: {{$chatAbierto}},
            chatId: {{$chatId}},
            chatStatus: 0,
            chatTxt: "Conectando...",
            interval: null,
            chatForm: false,
            chatVer: false,
            chatStart: false,
            mensajes: [],
            newMensaje: "",
            formName: "",
            formEmail: "",
            formPage: "pendiente",
            formEnviar: true,
        }),
        mounted() {
            this.chatTxt = this.chatId ? "Esperando..." : this.chatTxt
            this.checkStatus();

            if (window.Echo) {
                this.listen()
            }
            
        },
        methods: {
            openChat() {
                this.chatAbierto = this.chatAbierto ? false : true
                if(!this.chatId)
                {
                    this.createChat()
                }

                this.chatId = "?"
            },
            createChat() {

                this.mensajes = []
                this.chatAbierto = true
                this.chatForm = false
                
                axios.post("{{route('web.chat.create')}}").then( response => {
                    
                    let data = response.data
                    console.log(data)

                    if(data.chatStatus == 20)
                    {
                        this.chatStatus = data.chatStatus
                        this.chatStart = false
                        this.chatForm = true
                        this.chatVer = false
                        return;
                    }

                    this.userId = data.userId
                    this.chatId = data.chatId
                    this.chatTxt = data.chatTxt
                    this.chatStatus = data.chatStatus
                    this.chatStart = true

                    this.invervalStatus()
                    // this.checkStatus()

                }).catch( error => {
                    console.log(error);
                });
            },
            invervalStatus(clear=false) {

                if(clear)
                {
                    console.log("clearInterval")
                    clearInterval(this.interval)
                    this.interval = 0
                    this.chatId = 0
                    this.chatStart = false
                    
                    return
                }

                if(this.interval>0) return

                this.interval = setInterval(() => {

                    this.checkStatus();

                }, 3000);

            },
            checkStatus(status=0) {

                // if(!this.chatId) return
                
                let data = {
                    'chatId': this.chatId,
                }

                if(status>0)
                {
                    data = {
                        'chatId': this.chatId,
                        'status': status
                    }
                }

                axios.post("{{route('web.chat.status')}}", data).then( response => {
                    
                    let data = response.data
                    
                    this.userId = data.userId
                    this.chatTxt = data.chatTxt
                    this.chatStatus = data.chatStatus
                    this.mensajes = data.mensajes
                    
                    this.chatVer = true
                    this.chatStart = true
                    this.chatScroll()
                    
                }).catch( error => {
                    console.log(error);
                    this.intervalStatus(1)
                });
            },
            sendMensaje() {

                if(this.newMensaje==="") return;

                let data = {
                    'chatId': this.chatId,
                    'mensaje': this.newMensaje,
                    'userId': this.userId
                }
                
                axios.post("{{route('web.chat.mensaje')}}", data).then( response => {
                    let data = response.data
                    console.log(data)
                    this.newMensaje = "";
                    this.mensajes = data.mensajes
                    
                    this.chatScroll()

                }).catch( error => {
                    console.log(error);
                    this.intervalStatus(1)
                });
                
            },
            sendFormulario(e) {
                
                let data = {
                    'nombre': this.formName,
                    'email': this.formEmail,
                    'page': this.formPage,
                }

                axios.post("{{route('web.chat.formulario')}}", data).then( response => {
                    this.formEnviar = false
                }).catch( error => {
                    // this.formEnviar = false
                });
                
                e.preventDefault()
            },
            chatScroll() {
                let caja = this.$refs.chatMensajes
                caja.scrollTop = caja.scrollHeight
            },
            listen() {
                console.log("listen")
            }

        },
        watch: {
            chatStatus: function (newStatus, oldStatus) {

                if(newStatus == oldStatus)
                {
                    // return;
                }

                // this.chatStatus = newStatus

                if(this.chatStatus == 20 || this.chatStatus == 21 || this.chatStatus == 22)
                {
                    this.chatAbierto = true
                    return;
                }

                //chat finalizado
                if(this.chatStatus > 10)
                {
                    clearInterval(this.interval)
                    this.interval = 0
                    this.chatId = 0
                    this.chatStart = false
                    this.chatAbierto = false
                }
            }
        }
    });
</script>
@endpush