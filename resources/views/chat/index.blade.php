@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.chat.index') !!}
@stop

@section('container')

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-comments fa-fw"></i> Chats
        </div>
        <div class="panel-body">

            {!! Datatable::table()
                ->addColumn([
                    'fecha'     => 'Fecha',
                    'cliente'   => 'Cliente',
                    'status'    => 'Estado',
                    'options'   => ''
                ])
                ->setUrl(route('manage.chat.index', $user->id))
                ->setOptions(
                    "aoColumnDefs", array(
                        [ "bSortable" => false, "targets" => [3]],
                        [ "targets" => [0],
                            "render"=> "function(date, type, full) {return moment(date).isValid()?moment(date).format('DD/MM/YYYY'):'-';}" ],
                    )
                )
                ->render() !!}

        </div>
    </div>

@stop