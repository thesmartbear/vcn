@extends('layouts.manage')

@section('breadcrumb')
    {!! Breadcrumbs::render('manage.chat.agente', $chat->id) !!}
@stop


@section('container')

<?php
    $chatId = $chat ? $chat->id : 0;
    $chatAbierto = $chatId ? 1 : 0;
    $chatFin = $chat->status>10 ? 1 : 0;
?>

<div id="appChat" class="chat_wrapper" v-cloak>
<div id="chat-agente" class="chat_window" v-show="{{$chatAbierto}}">
    <div class="top_menu">
        <div class="title">
            Chat [@{{userId}}] @{{chatId}} :: @{{chatStatus}} [{{$chat->cliente->username}}]
            <div class="subtitle">
                @{{chatTxt}}
                <button class="btn btn-secondary btn-danger btn-sm" @click="checkStatus(11)" v-show="chatStatus==1">Cerrar</button>
            </div>
        </div>
    </div>
    
    {{-- <ul class="messages"></ul> --}}
    <div ref="chatMensajes" class="messages" v-show="chatVer">
        <chat-mensaje v-for="mensaje in mensajes" :key="mensaje.id" :mensaje="mensaje" :finalizado="chatFin"></chat-mensaje>
    </div>
    
    <div class="bottom_wrapper clearfix" v-show="chatStatus==1">
        <div class="message_input_wrapper">
            <input
                id="btn-input"
                type="text"
                name="message"
                class="message_input"
                placeholder="Escribir mensaje..."
                v-model="newMensaje"
                @keyup.enter="sendMensaje"
            >
        </div>
        <div class="send_message">
            <div class="text" @click="sendMensaje">Enviar</div>
        </div>
    </div>
</div>
</div>
@stop

@push('scripts')
<script type="text/javascript">
    
    const appChat = new Vue({
        el: '#appChat',
        data: () => ({
            userId: {{$user ? $user->id : 0}},
            chatAbierto: {{$chatAbierto}},
            chatId: {{$chatId}},
            chatStatus: 0,
            chatTxt: "{{$chat->status_text}}",
            interval: null,
            chatOff: false,
            chatVer: false,
            chatFin: {{$chatFin}},
            mensajes: [],
            newMensaje: "",
        }),
        mounted() {
            // this.chatTxt = this.chatId ? "" : this.chatTxt
            this.checkStatus();

            if (window.Echo) {
                this.listen()
            }
            
        },
        methods: {
            intervalStatus(clear=false) {

                if(clear)
                {
                    clearInterval(this.interval)
                    this.interval = 0
                    this.chatId = 0
                    // this.chatAbierto = false
                    this.chatOff = true
                    
                    return
                }

                if(this.interval>0) return

                this.interval = setInterval(() => {

                    this.checkStatus();

                }, 3000);

            },
            checkStatus($status=0) {

                // if(this.chatFin)
                // {
                //     this.chatVer = true
                //     return
                // }
                if(!this.chatId) return
                
                let data = {
                    'chatId': this.chatId,
                    'status': $status,
                }

                axios.post("{{route('web.chat.status')}}", data).then( response => {
                    
                    let data = response.data
                    console.log(data)

                    this.userId = data.userId
                    this.chatTxt = data.chatTxt
                    this.chatStatus = data.chatStatus
                    this.mensajes = data.mensajes
                    this.chatVer = true

                    this.chatScroll()
                    
                }).catch( error => {
                    this.intervalStatus(1);
                });
            },
            sendMensaje() {

                if(this.newMensaje==="") return;

                let data = {
                    'chatId': this.chatId,
                    'mensaje': this.newMensaje,
                    'userId': this.userId
                }
                
                axios.post("{{route('web.chat.mensaje')}}", data).then( response => {
                    let data = response.data
                    console.log(data)
                    this.newMensaje = "";
                    this.mensajes = data.mensajes

                    this.chatScroll()

                }).catch( error => {
                    console.log(error);
                    this.intervalStatus(1);
                });
                
            },
            chatScroll() {
                let caja = this.$refs.chatMensajes
                caja.scrollTop = caja.scrollHeight
            },
            listen() {
                console.log("listen")
            }

        },
        watch: {
            chatStatus: function (newStatus, oldStatus) {

                this.intervalStatus()
                
                if(newStatus == oldStatus)
                {
                    return;
                }

                this.chatStatus = newStatus

                if(this.chatStatus == 20 || this.chatStatus == 21 || this.chatStatus == 22)
                {
                    this.chatAbierto = true
                    this.chatOff = true
                    return;
                }

                //chat finalizado
                if(this.chatStatus > 10)
                {
                    this.intervalStatus(1);
                }
            }
        }
    });
</script>
@endpush