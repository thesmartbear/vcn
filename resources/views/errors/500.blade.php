<!DOCTYPE html>
<html>
    <head>
        <title>Error 500</title>

        <link href="//fonts.googleapis.com/css?family=Lato:300,100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #222;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 150px;
                margin-bottom: 40px;
                color: #B0BEC5;
                font-weight: 300;
            }
            .logo{
                margin-bottom: 80px;
            }
            h2{
                font-weight: 100;
            }
            h1{
                font-weight: 100;
                color: #B0BEC5;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                @if(ConfigHelper::config('logoweb') != '')
                    <p class="logo"><a href="/"><img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" alt="{{ConfigHelper::config('nombre')}}" /></a></p>
                @endif
                <div class="title">{!! trans('web.ups') !!}</div>
                <h1>Error 500</h1>
                <h2>{!! trans('web.error500') !!}</h2>
            </div>
        </div>
    </body>
</html>
