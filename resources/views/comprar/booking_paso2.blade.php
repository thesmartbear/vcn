{{-- DATOS VIAJERO --}}
<?php
$hayHijos = false;
$style_hijos = "";
$usuario = auth()->user();
?>
@if($usuario->es_tutor && !$ficha->viajero_id)

    <?php
    $hayHijos = $usuario->ficha->viajeros->count()>0;
    ?>

    @if($hayHijos)

        <?php
            $style_hijos = "display: none;";
        ?>

        @lang('area.booking.datos_opcion'):<br>

        <ul id="booking-viajero-hijos" class="booking-hijos">
        @foreach($usuario->ficha->viajeros as $hijo)

            <li><a class="btn btn-success" href="{{ route('area.comprar.booking.viajero', [$ficha->id, $hijo->id]) }}" >{{$hijo->full_name}}</a></li>

        @endforeach

            <li><a class="btn btn-warning" id="booking-viajero-nuevo" href="#" >@lang('area.booking.datos_nuevo')</a></li>
        </ul>



    @endif
@endif


{{-- DATOS NUEVO --}}
<div id="booking-viajero" style="{{$style_hijos}}">

<div class="portlet light bordered">
    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'name', 'texto'=> trans('area.nombre'), 'required'=> true, 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'lastname', 'texto'=> trans('area.apellido1'), 'required'=> true, 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-4">
            @include('includes.form_input_text', [ 'campo'=> 'lastname2', 'texto'=> trans('area.apellido2'), 'ficha'=> $ficha->datos])
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-3">
            {!! Form::label('sexo', 'Sexo *') !!}:<br>
            <label class="radio-inline">
            {!! Form::radio('sexo', '1', ($ficha->datos->sexo==1), array('required'=> true)) !!} <i class="fa fa-male"></i>
            </label>
            <label class="radio-inline">
            {!! Form::radio('sexo', '2', ($ficha->datos->sexo==2)) !!} <i class="fa fa-female"></i>
            </label>
            <span class="help-block">{{ $errors->first('sexo') }}</span>
        </div>
        <div class="col-md-3">
            @include('includes.form_input_datetime', [ 'campo'=> 'fechanac', 'texto'=> trans('area.fnacimiento'), 'required'=>true, 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-6">
            {{-- @include('includes.form_select2', [ 'campo'=> 'paisnac', 'texto'=> "País Nac.", 'select'=> ConfigHelper::getPaises(), 'ficha'=> $ficha->datos]) --}}
            @include('includes.form_select_pais', [ 'campo'=> 'paisnac', 'texto'=> trans('area.pnacimiento'), 'ficha'=> $ficha->datos])
        </div>
    </div>
</div>

<div class="portlet light bordered">
<div class="portlet-title tabbable-line">
    <div class="caption font-blue-sharp">@lang('area.datosrelevantes')</div>
    <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
</div>
<div class="portlet-body flip-scroll">
    <div class="form-group row">
        <div class="col-md-5">
            @include('includes.form_select2', [ 'campo'=> 'nacionalidad', 'texto'=> trans('area.nacionalidad').' *', 'select'=> [""=>""] + \VCN\Models\Nacionalidad::pluck('name','name')->toArray(), 'ficha'=> $ficha->datos, 'required'=> true])
        </div>
        @if(ConfigHelper::config('nif'))
        <div class="col-md-2">
            @include('includes.form_select', [ 'campo'=> 'tipodoc', 'texto'=> trans('area.tipo'), 'select'=> ConfigHelper::getTipoDoc(), 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-5">
            @include('includes.form_input_text', [ 'campo'=> 'documento', 'texto'=> trans('area.dni'), 'ficha'=> $ficha->datos])
        </div>
        @endif
    </div>

    <div class="form-group row">
        <div class="col-md-3">
            @include('includes.form_input_text', [ 'campo'=> 'pasaporte', 'texto'=> trans('area.pasaporte'), 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-3">
            @include('includes.form_select_pais', [ 'campo'=> 'pasaporte_pais', 'texto'=> trans('area.paispasaporte'), 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-3">
            @include('includes.form_input_datetime', [ 'campo'=> 'pasaporte_emision', 'texto'=> trans('area.femision'), 'ficha'=> $ficha->datos])
        </div>
        <div class="col-md-3">
            @include('includes.form_input_datetime', [ 'campo'=> 'pasaporte_caduca', 'texto'=> trans('area.fcaducidad'), 'ficha'=> $ficha->datos])
        </div>
    </div>
</div>
</div>

{{-- <div class="portlet light bordered">
<div class="portlet-title tabbable-line">
    <div class="caption font-blue-sharp">¿Cómo nos has conocido?</div>
    <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
</div>
<div class="portlet-body flip-scroll">
    <div class="form-group row">
        <div class="col-md-4">
            @include('includes.form_select2', [ 'campo'=> 'origen_id', 'texto'=> 'Origen', 'select'=> $conocidos])
        </div>
        <div class="col-md-4">
            @include('includes.form_select2', [ 'campo'=> 'suborigen_id', 'texto'=> 'Sub-Origen', 'select'=> $subconocidos])
        </div>
        <div class="col-md-4">
            @include('includes.form_select2', [ 'campo'=> 'suborigendet_id', 'texto'=> 'Sub-Origen Detalle', 'select'=> $subconocidosdet])
        </div>
    </div>
</div>
</div> --}}

<div class="portlet light bordered">

    <div class="portlet-title tabbable-line">
        <div class="caption font-blue-sharp">@lang('area.datoscontacto')</div>
        <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
    </div>
    <div class="portlet-body flip-scroll">

        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_select', [ 'campo'=> 'idioma_contacto', 'texto'=> trans('area.idiomacontacto'), 'select'=> ConfigHelper::getIdiomaContacto(), 'valor'=> $ficha->datos?$ficha->datos->idioma_contacto:""])
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'email', 'texto'=> trans('area.viajero_info.email'), 'ficha'=> $ficha->datos])
            </div>
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'phone', 'texto'=> trans('area.viajero_info.telefono'), 'ficha'=> $ficha->datos])
            </div>
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'movil', 'texto'=> trans('area.viajero_info.movil'), 'ficha'=> $ficha->datos])
            </div>
        </div>

        <div class="form-group">
            <small>@lang('area.viajero_info.txt')</small>
        </div>

        <div class="form-group row">
            <div class="col-md-2">
                @include('includes.form_select', [ 'campo'=> 'tipovia', 'texto'=> trans('area.tipo'), 'select'=> ConfigHelper::getTipoVia(), 'required'=> true, 'ficha'=> $ficha->datos])
            </div>
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'direccion', 'texto'=> trans('area.direccion'), 'required'=> true, 'ficha'=> $ficha->datos])
            </div>
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'ciudad', 'texto'=> trans('area.ciudad'), 'required'=> true, 'ficha'=> $ficha->datos])
            </div>
            <div class="col-md-2">
                @include('includes.form_input_text', [ 'campo'=> 'cp', 'texto'=> trans('area.cp'), 'required'=> true, 'ficha'=> $ficha->datos])
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                @include('includes.form_select2', [ 'campo'=> 'provincia', 'texto'=> trans('area.provincia') .'*', 'select'=>[""=>""]+ \VCN\Models\Provincia::pluck('name','name')->toArray(), 'required'=> true, 'ficha'=> $ficha->datos])

            </div>
            <div class="col-md-6">
                @include('includes.form_select_pais', [ 'campo'=> 'pais', 'texto'=> trans('area.pais') .'*', 'ficha'=> $ficha->datos, 'required'=> true])
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-8">
                @include('includes.form_input_text', [ 'campo'=> 'telefonos', 'texto'=> trans('area.otrostelfs'), 'ficha'=> $ficha->datos])
            </div>
        </div>

        @if($ficha->es_adulto)
            <div class="form-group row">
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'emergencia_contacto', 'texto'=> trans('area.emergencia'), 'ficha'=> $ficha->datos])
                </div>
                <div class="col-md-6">
                    @include('includes.form_input_text', [ 'campo'=> 'emergencia_telefono', 'texto'=> trans('area.tlfemergencia'), 'ficha'=> $ficha->datos])
                </div>
            </div>
        @endif

        <hr>
        <div class="form-group row">
            <div class="col-md-3">
                @include('includes.form_input_text', [ 'campo'=> 'rs_instagram', 'texto'=> 'Instagram', 'ficha'=> $ficha->datos])
            </div>
        </div>

    </div>
    </div>

    <div class="portlet light bordered">
    <div class="portlet-title tabbable-line">
        <div class="caption font-blue-sharp">@lang('area.datosacademicos')</div>
        <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
    </div>
    <div class="portlet-body flip-scroll">
        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_select', [ 'campo'=> 'idioma', 'texto'=> trans('area.idioma'), 'select'=> ConfigHelper::getIdioma(), 'ficha'=> $ficha->datos ])
            </div>
            <div class="col-md-4">
                @include('includes.form_select', [ 'campo'=> 'idioma_nivel', 'texto'=> trans('area.idiomanivel'), 'select'=> ConfigHelper::getIdiomaNivel(), 'ficha'=> $ficha->datos ])
            </div>
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'titulacion', 'texto'=> trans('area.titulacion'), 'ficha'=> $ficha->datos ])
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                @include('includes.form_input_text', [ 'campo'=> 'escuela', 'texto'=> trans('area.escuela'), 'ficha'=> $ficha->datos ])
            </div>
            <div class="col-md-3">
                @include('includes.form_select', [ 'campo'=> 'escuela_curso', 'texto'=> trans('area.cursoactual'), 'select'=> ConfigHelper::getEscuelaCurso(), 'ficha'=> $ficha->datos ])
            </div>
            @if($ficha->es_grado_ext)
            <div class="col-md-3">
                @include('includes.form_select', [ 'campo'=> 'grado_ext', 'texto'=> trans('area.gradoext'), 'select'=> ConfigHelper::getGradoExt(), 'required'=> true ])
            </div>
            @endif
        </div>

        @include('includes.form_div_bool', [ 'campo'=> 'ingles_academia', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.inglesacademia') ])

        @if($ficha->viajero->es_cic)
        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_checkbox', [ 'campo'=> 'cic', 'texto'=> trans('area.inglescic')])
            </div>
            <div id="cic_nivel_div" class="col-md-8" style="display:none;">
                @include('includes.form_select', [ 'campo'=> 'cic_nivel', 'texto'=> trans('area.nivel'), 'select'=> ConfigHelper::getCICNivel(), 'ficha'=> $ficha->datos ])
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                @include('includes.form_select', [ 'campo'=> 'cic_thau', 'texto'=> trans('area.inglesthau'), 'select'=> ConfigHelper::getCicThau(), 'ficha'=> $ficha->datos ])
            </div>
        </div>
        @endif

        <div class="form-group">
            {{-- <div class="input-group"> --}}
                <span>@lang('area.inglesextranjero')</span>
                <div class="form-radio">
                    <label class="radio-inline">
                        {!! Form::radio('curso_anterior_bool', '1', ($ficha->datos->curso_anterior!="")) !!} SI
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('curso_anterior_bool', '0', ($ficha->datos->curso_anterior=="")) !!} NO
                    </label>
                </div>

                <div id="curso_anterior_div" style="display:none;">
                    {!! Form::label('curso_anterior', "Especificar") !!}:
                    @include('includes.form_input_text', [ 'campo'=> 'curso_anterior', 'ficha'=> $ficha->datos, 'placeholder'=> 'Lugar' ])
                </div>
                <span class="help-block">{{ $errors->first('curso_anterior') }}</span>
            {{-- </div> --}}
        </div>
        <br>
        <div class="form-group row">
            <div class="col-md-3">
                @include('includes.form_checkbox', [ 'campo'=> 'trinity_exam', 'texto'=> trans('area.trinity'), 'ficha'=> $ficha->datos ])
            </div>
            <div id="trinity_exam_div" style="display:none;">
                <div class="col-md-2">
                    @include('includes.form_input_text', [ 'campo'=> 'trinity_any', 'texto'=> trans('area.año'), 'ficha'=> $ficha->datos ])
                </div>
                <div class="col-md-4">
                    @include('includes.form_select', [ 'campo'=> 'trinity_nivel', 'texto'=> trans('area.nivel'), 'select'=> ConfigHelper::getTrinityNivel(), 'ficha'=> $ficha->datos ])
                </div>
            </div>
        </div>

        <br>

        <div class="form-group">
            @include('includes.form_textarea', [ 'campo'=> 'hobby', 'texto'=> 'Hobby', 'ficha'=> $ficha->datos])
        </div>
    </div>
    </div>

    <div class="portlet light bordered">
    <div class="portlet-title tabbable-line">
        <div class="caption font-blue-sharp">Hermanos</div>
        <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
    </div>
    <div class="portlet-body flip-scroll">

        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'h1_nom', 'texto'=> trans('area.hermano',['num' => 1]), 'ficha'=> $ficha->datos ])
            </div>
            <div class="col-md-3">
                @include('includes.form_input_text', [ 'campo'=> 'h1_fnac', 'texto'=> trans('area.hermanofnacimiento',['num' => 1]), 'ficha'=> $ficha->datos ])
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'h2_nom', 'texto'=> trans('area.hermano',['num' => 2]), 'ficha'=> $ficha->datos ])
            </div>
            <div class="col-md-3">
                @include('includes.form_input_text', [ 'campo'=> 'h2_fnac', 'texto'=> trans('area.hermanofnacimiento',['num' => 2]), 'ficha'=> $ficha->datos ])
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'h3_nom', 'texto'=> trans('area.hermano',['num' => 3]), 'ficha'=> $ficha->datos ])
            </div>
            <div class="col-md-3">
                @include('includes.form_input_text', [ 'campo'=> 'h3_fnac', 'texto'=> trans('area.hermanofnacimiento',['num' => 3]), 'ficha'=> $ficha->datos ])
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4">
                @include('includes.form_input_text', [ 'campo'=> 'h4_nom', 'texto'=> trans('area.hermano',['num' => 4]), 'ficha'=> $ficha->datos ])
            </div>
            <div class="col-md-3">
                @include('includes.form_input_text', [ 'campo'=> 'h4_fnac', 'texto'=> trans('area.hermanofnacimiento',['num' => 4]), 'ficha'=> $ficha->datos ])
            </div>
        </div>

    </div>
    </div>

    <div class="portlet light bordered">
    <div class="portlet-title tabbable-line">
        <div class="caption font-blue-sharp">@lang('area.datosmedicos')
        {{-- <br><span class="badge badge-help">{{trans('area.datos_medicos')}}</span> --}}
        </div>

        <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
    </div>
    <div class="portlet-body flip-scroll">

        <div class="alert alert-warning" role="alert">
          {{trans('area.datos_medicos')}}
        </div>
        <hr>

        @include('includes.form_div_bool', [ 'campo'=> 'alergias', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.alergia'), 'required'=> true ])

        @include('includes.form_div_bool', [ 'campo'=> 'enfermedad', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.enfermedad'), 'required'=> true ])

        @include('includes.form_div_bool', [ 'campo'=> 'medicacion', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.medicacion'), 'required'=> true ])

        @include('includes.form_div_bool', [ 'campo'=> 'tratamiento', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.tratamiento'), 'required'=> true ])

        @include('includes.form_div_bool', [ 'campo'=> 'dieta', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.dieta'), 'required'=> true ])

        @include('includes.form_div_bool', [ 'campo'=> 'animales', 'ficha'=> $ficha->datos, 'nombre'=> trans('area.animales') ])

    </div>
    </div>

    @if($ficha->curso->categoria && $ficha->curso->categoria->es_info_campamento)
        @include('manage.bookings.booking.paso2_campamento', ['ficha'=> $ficha, 'required'=> true])
    @endif

    <div class="portlet light bordered">
    <div class="portlet-title tabbable-line">
        <div class="caption font-blue-sharp">@lang('area.observaciones')
        {{-- <br><span class="badge badge-help">{{trans('area.datos_observaciones')}}</span> --}}
        </div>

        <div class="tools"><a href="" class="collapse"></a><a href="" class="fullscreen"></a></div>
    </div>
    <div class="portlet-body flip-scroll">

        <div class="alert alert-warning" role="alert">
          {{trans('area.datos_observaciones')}}
        </div>
        <hr>
        
        <div class="form-group">
            @include('includes.form_textarea', [ 'campo'=> 'notas', 'texto'=> trans('area.observaciones') ])
        </div>
    </div>
    </div>

</div>{{-- booking-viajero-nuevo --}}

@include('includes.script_boolean', ['campo'=> 'alergias', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'medicacion', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'enfermedad', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'tratamiento', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'dieta', 'required'=>true ])
@include('includes.script_boolean', ['campo'=> 'animales', 'required'=>0 ])
@include('includes.script_boolean', ['campo'=> 'curso_anterior', 'required'=>0 ])
@include('includes.script_boolean', ['campo'=> 'ingles_academia', 'required'=>0 ])

<script type="text/javascript">
$(document).ready(function() {

    $("#booking-viajero-nuevo").click( function(e) {
        $("#booking-viajero-hijos").hide();
        $("#booking-viajero").fadeIn();
    });

    @if($ficha->viajero->es_cic)
        @if($ficha->datos->cic)
            $('#cic_nivel_div').show();
        @endif

        $("#cic").click(function(){
            if( $(this).is(':checked') )
            {
                $('#cic_nivel_div').show();
            }
            else
            {
                $('#cic_nivel_div').hide();
            }
        });
    @endif

    @if($ficha->datos->ingles)
        $('#ingles_academia_div').show();
    @endif

    $("#ingles").click(function(){
        if( $(this).is(':checked') )
        {
            $('#ingles_academia_div').show();
        }
        else
        {
            $('#ingles_academia_div').hide();
        }
    });

    @if($ficha->datos->trinity_exam)
        $('#trinity_exam_div').show();
    @endif

    $("#trinity_exam").click(function(){
        if( $(this).is(':checked') )
        {
            $('#trinity_exam_div').show();
        }
        else
        {
            $('#trinity_exam_div').hide();
        }
    });

});
</script>