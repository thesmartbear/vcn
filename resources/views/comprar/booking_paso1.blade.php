<?php
$style_alojamiento = "display:none;";
$style_transporte = "display:none;";
$style_extras = "display:none;";
$class_extras = "";


if($ficha->convocatoria)
{
    $style_alojamiento = "";

    if($ficha->convocatory_close_id)
    {
        $style_transporte = "";
    }
}

if($ficha->convocatoria && $ficha->alojamiento)
{
    $style_transporte = "";

    if($ficha->convocatory_open_id && !$ficha->vuelos_web )
    {
        $style_extras = "";
    }
}

//Abierta + Sin Alojamiento
if($ficha->convocatory_open_id && !$ficha->alojamiento )
{
    $style_transporte = "";

    if($ficha->convocatory_open_id && !$ficha->vuelos_web )
    {
        $style_extras = "";
    }
}

if($ficha->vuelo || $ficha->transporte_no)
{
    $style_extras = "";
}

if(!$ficha->vuelos)
{
    $style_extras = "";
    $class_extras = "no-fade";
}

$bSinPlazas = false;
$hayPlazas = true;

$booking_corporativo = $ficha->es_corporativo;
if($booking_corporativo)
{
    $booking_corporativo = $booking->convocatoria ? $booking->convocatoria->id : null;
    $convocatoriasCerradasSolo = $booking->curso->convocatoriasCerradasSolo->where('id',$booking_corporativo);
    $convocatoriasSemiCerradas = $booking->curso->convocatoriasSemiCerradas->where('id',$booking_corporativo);
}
else
{
    $convocatoriasCerradasSolo = $booking->curso->convocatoriasCerradasSolo->where('activo_web',1);
    $convocatoriasSemiCerradas = $booking->curso->convocatoriasSemiCerradas->where('activo_web',1);
}
?>

{{-- CONVOCATORIA --}}
<h2 class="booking-title">{{trans('area.booking.elige_fechas')}}</h2>
@if($booking->curso->es_convocatoria_cerrada)

    {{-- CONVOCATORIA CERRADA --}}
    @if( $convocatoriasCerradasSolo->count() )
    <table class="table">
        <tbody>

            @foreach($convocatoriasCerradasSolo as $cc)

                <?php
                    $hayPlazas = false;

                    $plazas_alojamiento = 1;
                    if($cc->alojamiento)
                    {
                        $pa = $cc->getPlazas($cc->alojamiento->id);
                        if($pa)
                        {
                           $plazas_alojamiento = $pa->plazas_disponibles;
                        }
                    }

                    if($plazas_alojamiento>0)
                    {
                        $hayPlazas = true;
                    }
                    else
                    {
                        if($ficha->getEsPlazaOnlineAlojamiento($cc->id, $cc->alojamiento_id))
                        {
                            $hayPlazas = true;
                            $plazas_alojamiento = 1;
                        }
                    }
                ?>

                <tr>
                    <td>
                    @if($plazas_alojamiento<1)
                        {{-- <i class="{{$ficha->convocatoria_class($cc->id)}} booking-disabled"></i> --}}
                        <i class="booking-disabled"></i>
                        <span class="booking-disabled">
                            {{$cc->duracion}} {{$cc->getDuracionName($cc->duracion)}} {{$cc->alojamiento?" en ".$cc->alojamiento->tipo->name:trans('area.booking.no_alojamiento')}} del {{$cc->start_date}} al {{$cc->end_date}}
                        </span>
                        <i>&nbsp;@lang('area.booking.no_plazas')</i>
                        @if($booking_corporativo)
                            <br>
                            @lang('area.booking.corporativo_no_plazas')
                        @endif
                    @else
                        <i class="{{$ficha->convocatoria_class($cc->id)}} booking-ccerrada" id="ccerrada-{{$cc->id}}"
                            data-id='{{$cc->id}}' data-semiopen="{{$cc->convocatory_semiopen}}"
                            data-alojamiento="{{$cc->alojamiento_id?$cc->alojamiento_id:0}}"
                            data-start="{{$cc->convocatory_close_start_date}}"
                            data-end="{{$cc->convocatory_close_end_date}}"
                            data-day="{{$cc->convocatory_semiopen_start_day}}"></i>

                        {{$cc->duracion}} {{$cc->getDuracionName($cc->duracion)}} {{$cc->alojamiento?" en ".$cc->alojamiento->tipo->name:trans('area.booking.no_alojamiento')}} del {{$cc->start_date}} al {{$cc->end_date}}
                    @endif

                    </td>

                    <td align='right'>
                        <span class="booking-ccerrada-precio" id="ccerrada-precio-{{$cc->id}}" data-moneda="{{$cc->convocatory_close_currency_id}}">

                            {{ ConfigHelper::parseMoneda($cc->precio, $cc->moneda_name) }}

                        </span>
                    </td>
                </tr>

            @endforeach

        </tbody>
    </table>
    @endif

    @if($booking->curso->es_convocatoria_semicerrada)

        @if(!$booking->curso->es_convocatoria_semicerrada_solo && !$booking_corporativo)
            <h3 class="booking-title">@lang('area.booking.planb')</h3>
            <h4>
                {!! trans('area.booking.planb_fechas') !!}
            </h4>

        @endif

        {{-- CONVOCATORIA SEMICERRADA --}}
        @foreach($convocatoriasSemiCerradas as $cc)

            <?php
                $hayPlazas = false;

                $plazas_alojamiento = 1;
                if($cc->alojamiento)
                {
                    $pa = $cc->getPlazas($cc->alojamiento->id);
                    if($pa)
                    {
                       $plazas_alojamiento = $pa->plazas_disponibles;
                    }
                }

                if($plazas_alojamiento>0)
                {
                    $hayPlazas = true;
                }
                else
                {
                    if($ficha->getEsPlazaOnlineAlojamiento($cc->id, $cc->alojamiento_id))
                    {
                        $hayPlazas = true;
                        $plazas_alojamiento = 1;
                    }
                }
            ?>

            <table class="table csemi">
            <tbody>
                <tr>
                    <td class="csname">
                    @if($plazas_alojamiento<1)
                        {{-- <i class="{{$ficha->convocatoria_class($cc->id)}} booking-disabled"></i> --}}
                        <i class="booking-disabled"></i>
                        <span class="booking-disabled">{{$cc->duracion}} {{$cc->getDuracionName($cc->duracion)}} en {{$cc->alojamiento?$cc->alojamiento->tipo->name:'?'}} @lang("area.booking.escoger") {{$cc->start_date}} y {{$cc->end_date}}</span>
                        <i>&nbsp;@lang('area.booking.no_plazas')</i>
                        @if($booking_corporativo)
                            <br>
                            @lang('area.booking.corporativo_no_plazas')
                        @endif
                    @else
                        <i class="{{$booking->convocatoria_class($cc->id)}} booking-ccerrada" id="ccerrada-{{$cc->id}}"
                            data-id='{{$cc->id}}' data-semiopen="{{$cc->convocatory_semiopen}}"
                            data-alojamiento="{{$cc->alojamiento_id?$cc->alojamiento_id:0}}"
                            data-start="{{$cc->convocatory_close_start_date}}"
                            data-end="{{$cc->convocatory_close_end_date}}"
                            data-day="{{$cc->convocatory_semiopen_start_day}}"></i>

                        <span>{{$cc->duracion}} {{$cc->getDuracionName($cc->duracion)}} en {{$cc->alojamiento?$cc->alojamiento->tipo->name:'?'}} @lang("area.booking.escoger") {{$cc->start_date}} y {{$cc->end_date}}</span>
                    @endif
                    </td>

                    <td align='right'>
                        <span class="booking-ccerrada-precio" id="ccerrada-precio-{{$cc->id}}" data-moneda="{{$cc->convocatory_close_currency_id}}">

                            {{ ConfigHelper::parseMoneda($cc->precio, $cc->moneda_name) }}

                        </span>
                    </td>
                </tr>
            </tbody>
            </table>
        @endforeach

        <hr>
        <div id="booking-semiopen" class="form-group row">
            <div class="col-md-3">
                {!! Form::label('course_start_date', 'Escoge tu Fecha de Inicio') !!}
            </div>
            <div class="col-md-3">
                {!! Form::text('course_start_date', Carbon::parse($booking->course_start_date)->format('d/m/Y'), array( 'id'=>'course_start_date', 'placeholder' => 'Fecha Inicio', 'class' => 'form-control', 'required'=>true)) !!}
                <span class="help-block">{{ $errors->first('course_start_date') }}</span>
            </div>
            <div class="col-md-1">
                <a id="booking-ccerrada-semi" href="#" class="btn btn-warning">Calcular</a>
            </div>
            <div class="col-md-5">
                {!! Form::label('course_end_date', 'Fecha Fin') !!}:
                <span id='course_end_date'>{{Carbon::parse($booking->course_end_date)->format('d/m/Y')}}</span>
            </div>
        </div>

    @endif


    {{-- ALOJAMIENTOS --}}
    @if($booking->curso->alojamientos && $booking->convocatoria)

    <div id="paso1-alojamiento" style='{{$style_alojamiento}}'>
    @if($booking->convocatoria->alojamiento_id!=0)
        <h2 class="booking-title">{{trans('area.booking.elige_alojamiento')}}</h2>
    @endif

    <table class="table">
        <tbody>

        @if($ficha->convocatoria)
        @if($ficha->convocatoria->alojamiento_id==0)
                <tr>
                <td>
                    <i class="{{$booking->alojamiento_class(0, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}} booking-ccerrada-alojamiento"
                        id="ccerrada-alojamiento-0"
                        data-id="0"
                        data-precio="0"
                        data-moneda="0"
                        >
                    </i>

                    @lang('area.booking.no_alojamiento')

                </td>

                <td align="right">
                    <span id="ccerrada-alojamiento-precio-0">
                        {{ConfigHelper::parseMoneda(0)}}
                    </span>
                </td>

                </tr>

            @foreach($ficha->curso->alojamientos as $alojamiento)

                <?php
                    $alojamiento_precio = $alojamiento->calcularPrecio(
                        Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y'),
                        Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y'), $ficha->duracion, $ficha->duracion_fijo);

                    $pa = $ficha->convocatoria ? $ficha->convocatoria->getPlazas($alojamiento->id) : null;

                    $plazas_alojamiento = $pa ? 1 : 0;
                    
                    if($pa)
                    {
                       $plazas_alojamiento = $pa->plazas_disponibles;
                    }

                    if(!$plazas_alojamiento)
                    {
                        $plazas_alojamiento = $ficha->convocactoria ? $ficha->getEsPlazaOnlineAlojamiento($ficha->convocactoria->id, $alojamiento->id) : 0;
                    }

                    $hayPlazas = $plazas_alojamiento;
                ?>

                <tr>

                <td>
                    @if($plazas_alojamiento<1)
                        <i class="booking-disabled {{$ficha->alojamiento_class($alojamiento->id, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}}"></i>
                        <span class="booking-disabled">
                            {{$alojamiento->name}}<i>( {{$alojamiento->tipo_name}} )</i>
                        </span>
                        <i>&nbsp;@lang('area.booking.no_plazas')</i>
                        @if($booking_corporativo)
                            <br>
                            @lang('area.booking.corporativo_no_plazas')
                        @endif
                    @else
                        <i class="{{$ficha->alojamiento_class($alojamiento->id, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}} booking-ccerrada-alojamiento"
                            id="ccerrada-alojamiento-{{$alojamiento->id}}"
                            data-id="{{$alojamiento->id}}"
                            data-precio="{{$alojamiento_precio['importe']}}"
                            data-moneda="{{$alojamiento_precio['moneda']}}"
                            >
                        </i>
                        {{$alojamiento->name}} <i>( {{$alojamiento->tipo_name}} )</i>
                    @endif

                </td>

                <td align="right">
                    <span id="ccerrada-alojamiento-precio-{{$alojamiento->id}}">
                        {{ConfigHelper::parseMoneda($alojamiento_precio['importe'],$alojamiento_precio['moneda'])}}
                    </span>
                </td>

                </tr>

            @endforeach

        @else

            <?php
                //alojamiento asignado:
                $alojamiento = $ficha->convocatoria->alojamiento;

                $alojamiento_precio = $alojamiento->calcularPrecio(
                    Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y'),
                    Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y'), $ficha->duracion);

                $plazas_alojamiento = 1;
                $pa = $ficha->convocatoria?$ficha->convocatoria->getPlazas($alojamiento->id):null;
                if($pa)
                {
                   $plazas_alojamiento = $pa->plazas_disponibles;
                }

                if(!$plazas_alojamiento)
                {
                    $plazas_alojamiento = $ficha->getEsPlazaOnlineAlojamiento($ficha->convocatory_close_id, $ficha->alojamiento_id);
                }

                $hayPlazas = $plazas_alojamiento;
            ?>

            @if($alojamiento_precio)
            <tr>

            <td>
                @if($plazas_alojamiento<1)
                    <i class="booking-disabled {{$ficha->alojamiento_class($alojamiento->id, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}}"></i>
                    <span class="booking-disabled">
                        {{$alojamiento->name}}<i>( {{$alojamiento->tipo_name}} )</i>
                    </span>
                    <i>&nbsp;@lang('area.booking.no_plazas')</i>
                    @if($booking_corporativo)
                        <br>
                        @lang('area.booking.corporativo_no_plazas')
                    @endif
                @else

                    <i class="{{$ficha->alojamiento_class($alojamiento->id, $ficha->convocatoria?$ficha->convocatoria->alojamiento_id:0)}} booking-ccerrada-alojamiento"
                        id="ccerrada-alojamiento-{{$alojamiento->id}}"
                        data-id="{{$alojamiento->id}}"
                        data-precio="{{$alojamiento_precio['importe']}}"
                        data-moneda="{{$alojamiento_precio['moneda']}}"
                        >
                    </i>
                    {{$alojamiento->name}} <i>( {{$alojamiento->tipo_name}} )</i>

                @endif

            </td>

            <td align="right">
                <span id="ccerrada-alojamiento-precio-{{$alojamiento->id}}">
                    {{ConfigHelper::parseMoneda($alojamiento_precio['importe'],$alojamiento_precio['moneda'])}}
                </span>
            </td>

            </tr>
            @endif

        @endif

        @else
            <div class="note note-warning">
                <h4 class="block">@lang("area.booking.atencion")</h4>
                <p>@lang("area.booking.atencion_convo")</p>
            </div>
        @endif

        </tbody>

    </table>
    </div>
    @endif

    @if(!$hayPlazas)
        <div class="note note-danger">
            <h4 class="block"></h4>
            <p>
                <h4><strong>@lang('area.booking.no_plazas_alert')</strong></h4>
            </p>
            @if(!$booking_corporativo)
            <p>
                <a href="{{route('area.comprar.curso', [0,$ficha->id])}}" class="btn btn-danger pull-right">{{trans('area.booking.btn_cambiar')}}</a>
            </p>
            @endif
            <div class="clearfix"></div>
        </div>
    @endif

@elseif($booking->curso->es_convocatoria_abierta)

    {{-- OCULTO PA ES ALGO INTERNO --}}
    <div class="hidden">
    <table class="table">
        <thead>
        <tr class="thead">
            <td>Fechas</td>
        </tr>
        </thead>
        <tbody>

        @foreach($ficha->curso->convocatoriasAbiertas as $co)
            <tr>
                <td>
                    <i class="{{$ficha->convocatoria_class($co->id)}} booking-cabierta" id="cabierta-{{$co->id}}"
                        data-id='{{$co->id}}'
                        data-start="{{$co->convocatory_open_valid_start_date}}"
                        data-end="{{$co->convocatory_open_valid_end_date}}"
                        data-day="{{$co->convocatory_open_start_day}}"></i>

                    Del {{$co->start_date}} al {{$co->end_date}}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    </div>
    {{-- OCULTO PA ES ALGO INTERNO --}}

    {{-- CONVOCATORIA ABIERTA --}}
    <table class="table">
        <thead>
        <tr class="thead">
            <td>@lang('area.finicio')</td>
            <td>@lang('area.ffin')</td>
            <td>@lang('area.duracion')</td>
            <td align='right'>@lang('area.precio')</td>
        </tr>
        </thead>
        <tbody>
        <tr>

            <td>
                {!! Form::text('booking-cabierta-fecha_ini', $ficha->course_start_date?Carbon::parse($ficha->course_start_date)->format('d/m/Y'):"", array( 'id'=>'booking-cabierta-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('booking-cabierta-fecha_ini') }}</span>
                <span id="booking-cabierta-alerta" class='booking-alerta'>{{isset($ficha->alertas['cabierta'])?$ficha->alertas['cabierta']:""}}</span>
                <span id="booking-cabierta-precio-detalle" class='booking-precio'>
                    @if($ficha->curso_precio_extra>0)
                        {{trans('area.booking.precio_base').": ". ConfigHelper::parseMoneda( ($ficha->course_price), $ficha->curso_moneda)}}
                        {{" + ". trans('area.booking.extra_temporada') .": ". ConfigHelper::parseMoneda( ($ficha->curso_precio_extra), $ficha->curso_moneda)}}
                    @endif
                </span>
            </td>
            <td>
                {!! Form::text('booking-cabierta-fecha_fin', $ficha->course_end_date?Carbon::parse($ficha->course_end_date)->format('d/m/Y'):"", array( 'id'=>'booking-cabierta-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
            </td>
            <td>
                {!! Form::select('booking-cabierta-semanas', $semanas, $ficha->duracion, array( 'id'=> 'booking-cabierta-semanas', 'class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('booking-cabierta-semanas') }}</span>
            </td>
            <td align='right'>
                <span class="booking-cabierta-precio" id="booking-cabierta-precio">
                    @if($ficha->convocatoria)
                        {{ ConfigHelper::parseMoneda( ($ficha->course_total_amount), $ficha->curso_moneda) }}
                    @endif
                </span>
            </td>
        </tr>

        {{-- DESCUENTOS --}}
        @if($descuento)
        <tr>
            <td colspan="3">@lang('descuentoaplicado') [{{$descuento->name}}]:</td>
            <td align='right'>
                <span id="booking-cabierta-precio-dto">
                    {{-- ConfigHelper::parseMoneda( (($ficha->course_price * $ficha->duracion) * ($descuentos[0]/100)), $ficha->curso_moneda) --}}
                    {{ ConfigHelper::parseMoneda( $ficha->center_discount_amount, $ficha->curso_moneda) }}
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">@lang('area.booking.precio_pagar'):</td>
            <td align='right'>
                <span id="booking-cabierta-precio-dtopagar">
                    {{ ConfigHelper::parseMoneda( ($ficha->course_total_amount-$ficha->center_discount_amount), $ficha->curso_moneda) }}
                </span>
            </td>
        </tr>
        @endif

        </tbody>
    </table>

    {{-- ALOJAMIENTOS --}}
    <div id="paso1-alojamiento" style='{{$style_alojamiento}}'>

        <h2 class="booking-title">{{trans('area.booking.elige_alojamiento')}}</h2>

        <table class="table">
            <thead>
            <tr class="thead">
                <td>@lang('area.alojamiento')</td>
                <td>@lang('area.finicio')</td>
                <td>@lang('area.ffin')</td>
                <td>@lang('area.duracion')</td>
                <td class="col-md-2" align="right"><td>@lang('area.precio')</td></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    {!! Form::select('booking-cabierta-alojamiento', [""=>trans('area.booking.no_alojamiento')] + $ficha->curso->alojamientos->pluck('name','id')->toArray(), $ficha->accommodation_id,
                            array(
                                'id'=> 'booking-cabierta-alojamiento',
                                'class' => 'form-control',
                                'data-day' => ($ficha->alojamiento?$ficha->alojamiento->start_day:null),
                            )
                        )
                    !!}
                    <span class="help-block">{{ $errors->first('booking-alojamiento') }}</span>
                    <span id="booking-alojamiento-alerta" class='booking-alerta'>{{isset($ficha->alertas['alojamiento'])?$ficha->alertas['alojamiento']:""}}</span>
                    <span id="booking-alojamiento-precio-detalle" class='booking-precio'>
                        @if($ficha->alojamiento_precio_extra>0)
                            {{"Precio base: ". ConfigHelper::parseMoneda( ($ficha->accommodation_price), $ficha->curso_moneda)}}
                            {{" + Extra temporada: ". ConfigHelper::parseMoneda( ($ficha->alojamiento_precio_extra), $ficha->curso_moneda)}}
                        @endif
                    </span>
                </td>
                <td>
                    {!! Form::text('booking-alojamiento-fecha_ini', $ficha->accommodation_start_date?Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y'):"", array( 'id'=>'booking-alojamiento-fecha_ini', 'placeholder' => 'Fecha Inicio', 'class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('booking-alojamiento-fecha_ini') }}</span>
                </td>
                <td>
                    {!! Form::text('booking-alojamiento-fecha_fin', $ficha->accommodation_end_date?Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y'):"", array( 'id'=>'booking-alojamiento-fecha_fin', 'placeholder' => 'Fecha Fin', 'class' => 'form-control', 'readonly'=> true)) !!}
                </td>
                <td>
                    {!! Form::select('booking-alojamiento-semanas', $semanas, $ficha->accommodation_weeks, array( 'id'=> 'booking-alojamiento-semanas', 'class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('booking-alojamiento-semanas') }}</span>
                </td>
                <td align='right'>
                    <span class="booking-alojamiento-precio" id="booking-alojamiento-precio">
                        {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
                    </span>
                </td>
            </tr>
            </tbody>
        </table>

    </div>


@elseif($booking->curso->es_convocatoria_multi)

    <?php
        $plazas_multi = $ficha->convocatoria?$ficha->convocatoria->hayPlazasDisponibles($ficha->id):0;
    ?>

    @if(!$plazas_multi)
        <div class="note note-danger">
            <h4 class="block">@lang('area.booking.atencion')</h4>
            <i>&nbsp;@lang('area.booking.no_plazas')</i>
            @if($booking_corporativo)
                <br>
                @lang('area.booking.corporativo_no_plazas')
            @endif
        </div>
    @endif

    {{-- CONVOCATORIA MULTI --}}
    <table class='table'>
        <thead>
            <tr>
                <td>@lang('area.finicio')</td>
                <th>{{ trans_choice('area.Semanas', $ficha->multis->count()) }}</th>
                <td>@lang('area.ffin')</td>
                <td align='right'>@lang('area.precio')</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class="col-md-4">
                        @include('includes.form_select', ['campo'=> 'booking-multi-fecha_ini', 'valor'=> $multi['mfdesde_id'], 'texto'=> null, 'select'=> $multi['mfdesde']])
                    </div>
                </td>
                <td>
                    @include('includes.form_select', ['campo'=> 'booking-multi-semanas', 'valor'=> $ficha->weeks, 'texto'=> null, 'select'=> $multi['msemanas']])
                </td>
                <td><span id="booking-multi-fecha_fin">{{$ficha->course_end_date?Carbon::parse($ficha->course_end_date)->format('d/m/Y'):"-"}}</span></td>
                <td align='right'><span id="booking-multi-precio">{{ ConfigHelper::parseMoneda($ficha->course_price, $ficha->convocatoria?$ficha->convocatoria->moneda_name:null) }}</span></td>
            </tr>
        </tbody>
    </table>

    {{-- Especialidades --}}
    <h2 class="booking-title">{{trans('area.booking.elige_especialidad')}}</h2>
    <table class='table'>
        <thead>
            <tr>
                <th>{{ trans_choice('area.Semanas', $ficha->multis->count()) }}</th>
                <th>@lang('area.especialidad')</th>
                <td align='right'>@lang('area.precio')</td>
            </tr>
        </thead>
        <tbody id='booking-multi-especialidades-table'>
            @foreach($ficha->multis as $esp)

            <?php
                $plazas = $ficha->convocatoria->getPlazasSemana($esp->semana->semana);
            ?>

            <tr>
                <td class='col-md-1'>
                    @lang('area.semana') {{$esp->n}} [{{$esp->semana->semana}}]
                </td>
                <td>
                    {!! Form::select('booking-multi-especialidad-'.$esp->id, $ficha->convocatoria->especialidadesBySemana($esp->semana->semana), $esp->especialidad_id, array( 'id'=> 'booking-multi-especialidad-'.$esp->id, 'class' => 'booking-multi-especialidad form-control', 'data-id'=> $esp->id, 'data-semana'=> $esp->n )) !!}
                </td>
                <td align='right' id="booking-multi-especialidad-precio-{{$esp->id}}">{{ ConfigHelper::parseMoneda($esp->precio, $ficha->convocatoria->moneda_name) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endif

{{-- TRANSPORTE --}}
@if( $booking->curso->es_convocatoria_cerrada || $booking->curso->es_convocatoria_abierta)
<div id="paso1-transporte" style='{{$style_transporte}}'>

    <hr>
    <h2 class="booking-title">{{trans('area.booking.elige_transporte')}}</h2>

    @if(!$ficha->vuelos_web)
    <div class="alert alert-danger" role="alert" style="text-align:justify;">
        {{trans('area.booking.sin_vuelos')}}
    </div>

    <div class="transporte_no" id="transporte_no_div"
        @if($ficha->transporte_otro)
            style="display:none;"
        @endif
    >
    @include('includes.form_checkbox', [ 'campo'=> 'transporte_no', 'texto'=> trans('area.booking.no_transporte') ])
    </div>
    @endif

    <div class="booking-vuelo-table">
    <div id="booking-vuelo-table"
        @if($ficha->transporte_no)
            style="display:none;"
        @endif
        >

        <?php
            $bSinPlazas = $ficha->transporte_no?false:true;
            $transporte_vuelos = false;
            if($ficha->vuelos_web)
            {
                $v = $ficha->vuelos_web->first()->vuelo->transporte;
                if($v=="Vuelo" || $v==0 || is_null($v))
                {
                    $transporte_vuelos = true;
                }
            }
        ?>

        @if($ficha->vuelos_web)

            @if( ConfigHelper::config('tpv_aeropuertos') && $transporte_vuelos )

                {{-- BCN/MAD/OTRO ??? --}}
                {!! Form::hidden('aporte', null) !!}

                <?php
                    $bcn = null; //Barcelona (BCN), Barcelona
                    $mad = null; //Barajas (MAD), Madrid
                    $otro = null;

                    $bSinPlazas_bcn = true;
                    $bSinPlazas_mad = true;
                    $bSinPlazas_otro = true;

                    foreach( $ficha->vuelos_web as $vuelo )
                    {
                        $plazasv = $vuelo->vuelo->plazas_disponibles;
                        if($plazasv<1)
                        {
                            if($ficha->getEsPlazaOnlineVuelo($vuelo->vuelo_id))
                            {
                                $plazasv = 1;
                            }
                        }

                        if($vuelo->vuelo->es_bcn && !$bcn)
                        {
                            $bcn = $vuelo;
                            $bSinPlazas_bcn = $plazasv>0?false:true;
                        }

                        if($vuelo->vuelo->es_madrid && !$mad)
                        {
                            $mad = $vuelo;
                            $bSinPlazas_mad = $plazasv>0?false:true;
                        }

                        if($vuelo->vuelo->es_otro && !$otro)
                        {
                            $otro = $vuelo;
                            $bSinPlazas_otro = $plazasv>0?false:true;
                        }
                    }

                    //Si no hay bcn => mad
                    $bcn = $bcn?:$mad;
                    //Si no hay mad => bcn
                    $mad = $mad?:$bcn;
                    $otro = $otro?:$bcn;

                    $bSinPlazas = $bSinPlazas_bcn && $bSinPlazas_mad && $bSinPlazas_otro;
                ?>

                {{-- SOLO SI HAY PLAZAS --}}
                @if(!$bSinPlazas)
                    <h4 class="booking-title">@lang('area.booking.datos_aeropuerto')</h4>
                    <table class="table vuelos">

                        <tbody>

                            {{-- BCN --}}
                            <?php
                                $vuelo = $bcn;
                                $vuelo_name = "Barcelona (BCN), Barcelona";

                                $aporte = "";
                                if($vuelo && $vuelo->vuelo && !$vuelo->vuelo->es_bcn)
                                {
                                    $aporte = "BCN";
                                }

                                $classv = "fa fa-circle-thin";
                                if($vuelo && $vuelo->vuelo_id==$ficha->vuelo_id )
                                {
                                    if( $bcn==$mad )
                                    {
                                        if($ficha->transporte_recogida==$aporte )
                                        {
                                            $classv = "fa fa-check-circle-o";
                                        }
                                    }
                                    elseif($ficha->transporte_recogida=="BCN" || $ficha->transporte_recogida=="" )
                                    {
                                        $classv = "fa fa-check-circle-o";   
                                    }
                                }
                                elseif($ficha->transporte_recogida=="BCN" )
                                {
                                    $classv = "fa fa-check-circle-o";
                                }
                            ?>
                            <tr>
                                <td>
                                    @if($vuelo && $vuelo->vuelo)
                                    <?php
                                        $plazasv = $vuelo->vuelo->plazas_disponibles;
                                        if($plazasv<1)
                                        {
                                            if($ficha->getEsPlazaOnlineVuelo($vuelo->vuelo_id))
                                            {
                                                $plazasv = 1;
                                            }
                                        }
                                    ?>
                                    @if($plazasv<1)
                                        <i class="fa fa-circle-thin obligatorio"></i>
                                        <span class="booking-disabled">
                                            {{$vuelo_name}}
                                        </span>
                                        <i>&nbsp;@lang('area.booking.no_plazas')</i>
                                        @if($booking_corporativo)
                                            <br>
                                            @lang('area.booking.corporativo_no_plazas')
                                        @endif
                                    @else
                                        <i class="{{$classv}} booking-vuelo-aeropuerto" data-aeropuerto='bcn' data-aporte="{{$aporte}}" id="vuelo-bcn-{{$vuelo->vuelo->id}}" data-id='{{$vuelo->vuelo->id}}' data-tipo='vuelo'></i>
                                        {{-- {{$vuelo->vuelo->id}}:{{$vuelo->vuelo->plazas_disponibles}} --}}
                                        {{$vuelo_name}}
                                    @endif
                                    @endif
                                </td>
                            </tr>

                            {{-- MADRID --}}
                            <?php
                                $vuelo = $mad;
                                $vuelo_name = "Barajas (MAD), Madrid";

                                $aporte = "";
                                if($vuelo && $vuelo->vuelo && !$vuelo->vuelo->es_madrid)
                                {
                                    $aporte = "MADRID";
                                }

                                $classv = "fa fa-circle-thin";
                                if($vuelo && $vuelo->vuelo_id==$ficha->vuelo_id )
                                {
                                    if( $bcn==$mad )
                                    {
                                        if($ficha->transporte_recogida==$aporte )
                                        {
                                            $classv = "fa fa-check-circle-o";
                                        }
                                    }
                                    elseif($ficha->transporte_recogida=="MADRID" || $ficha->transporte_recogida=="" )
                                    {
                                        $classv = "fa fa-check-circle-o";   
                                    }
                                }
                                elseif($ficha->transporte_recogida=="MADRID" )
                                {
                                    $classv = "fa fa-check-circle-o";
                                }
                            ?>
                            <tr>
                                <td>
                                    @if($vuelo && $vuelo->vuelo)
                                    <?php
                                        $plazasv = $vuelo->vuelo->plazas_disponibles;
                                        if($plazasv<1)
                                        {
                                            if($ficha->getEsPlazaOnlineVuelo($vuelo->vuelo_id))
                                            {
                                                $plazasv = 1;
                                            }
                                        }
                                    ?>
                                    @if($plazasv<1)
                                        <i class="fa fa-circle-thin obligatorio"></i>
                                        <span class="booking-disabled">
                                            {{$vuelo_name}}
                                        </span>
                                        <i>&nbsp;@lang('area.booking.no_plazas')</i>
                                        @if($booking_corporativo)
                                            <br>
                                            @lang('area.booking.corporativo_no_plazas')
                                        @endif
                                    @else
                                        <i class="{{$classv}} booking-vuelo-aeropuerto" data-aeropuerto='mad' data-aporte="{{$aporte}}" id="vuelo-mad-{{$vuelo->vuelo->id}}" data-id='{{$vuelo->vuelo->id}}' data-tipo='vuelo'></i>
                                        {{-- {{$vuelo->vuelo->id}}:{{$vuelo->vuelo->plazas_disponibles}} --}}
                                        {{$vuelo_name}}
                                    @endif
                                    @endif
                                </td>
                            </tr>

                            {{-- OTRO --}}
                            <?php
                                $vuelo = $otro;
                                if( ($otro==$bcn || $otro==$mad) && !$otro->vuelo->es_otro)
                                {
                                    $vuelo_name = "OTRO";
                                    $aporte = "OTRO";

                                    $classv = "fa fa-circle-thin";
                                    if($vuelo && $vuelo->vuelo_id==$ficha->vuelo_id && $ficha->transporte_recogida!='' && $ficha->transporte_recogida!='BCN' && $ficha->transporte_recogida!='MADRID'  )
                                    {
                                        $classv = "fa fa-check-circle-o";
                                    }

                                    $vuelo_otro = "";
                                    if($ficha->transporte_recogida && $ficha->transporte_recogida != "OTRO")
                                    {
                                        $vuelo_otro = " (". $ficha->transporte_recogida .")";
                                    }
                                }
                                else
                                {
                                    $vuelo_name = $vuelo->vuelo->aeropuerto;
                                    $aporte = "";
                                    $vuelo_otro = "";

                                    $classv = $ficha->vuelo_class($vuelo->vuelo->id);
                                }
                            ?>
                            <tr>
                                <td>
                                    @if($vuelo && $vuelo->vuelo)                                
                                    <?php
                                        $plazasv = $vuelo->vuelo->plazas_disponibles;
                                        if($plazasv<1)
                                        {
                                            if($ficha->getEsPlazaOnlineVuelo($vuelo->vuelo_id))
                                            {
                                                $plazasv = 1;
                                            }
                                        }
                                    ?>
                                    @if($plazasv<1)
                                        <i class="fa fa-circle-thin obligatorio"></i>
                                        <span class="booking-disabled">
                                            {{$vuelo_name}}
                                        </span>
                                        <i>&nbsp;@lang('area.booking.no_plazas')</i>
                                        @if($booking_corporativo)
                                            <br>
                                            @lang('area.booking.corporativo_no_plazas')
                                        @endif
                                    @else
                                        <i class="{{$classv}} booking-vuelo-aeropuerto" data-aeropuerto='otro' data-aporte="{{$aporte}}" id="vuelo-otro-{{$vuelo->vuelo->id}}" data-id='{{$vuelo->vuelo->id}}' data-tipo='vuelo'></i>
                                        {{-- {{$vuelo->vuelo->id}}:{{$vuelo->vuelo->plazas_disponibles}} --}}
                                        {{$vuelo_name}} <span id="vuelo_otro">{{$vuelo_otro}}</span>
                                    @endif
                                    @endif
                                </td>
                            </tr>

                        </tbody>

                    </table>
                @endif

            @else

                <table class="table vuelos">
                    <thead>
                    <tr class="thead">
                        <th>
                            @if($ficha->es_campamento_cic)
                            
                            @else
                                @if($transporte_vuelos)
                                    @lang("area.booking.datos_aeropuerto")
                                @else
                                    @lang("area.booking.datos_ciudad")
                                @endif
                            @endif

                            {{-- @if(!$ficha->vuelos)
                                [No hay vuelos para la Convocatoria o no hay Convocatoria seleccionada.]
                            @endif --}}
                        </th>
                        {{-- <td>Precio</td> --}}
                    </tr>
                    </thead>

                    <tbody>
                        @if($ficha->vuelos_web)

                            @foreach( $ficha->vuelos_web as $vuelo )
                                <?php
                                    $plazasv = $vuelo->vuelo->plazas_disponibles;
                                    if($plazasv<1)
                                    {
                                        if($ficha->getEsPlazaOnlineVuelo($vuelo->vuelo_id))
                                        {
                                            $plazasv = 1;
                                        }
                                    }
                                ?>
                                <tr>
                                    <td>
                                        @if($plazasv<1)
                                            <i class="fa fa-check-circle-o obligatorio"></i>
                                            <span class="booking-disabled">
                                                {{$vuelo->vuelo->aeropuerto}} [{{$vuelo->vuelo->name}}]
                                            </span>
                                            <i>&nbsp;@lang('area.booking.no_plazas')</i>
                                            @if($booking_corporativo)
                                                <br>
                                                @lang('area.booking.corporativo_no_plazas')
                                            @endif
                                        @else
                                            <?php
                                                $bSinPlazas = false;
                                            ?>
                                            <i class="{{$ficha->vuelo_class($vuelo->vuelo->id)}} booking-vuelo" id="vuelo-{{$vuelo->vuelo->id}}" data-id='{{$vuelo->vuelo->id}}' data-tipo='vuelo'></i>
                                            {{$vuelo->vuelo->aeropuerto}} [{{$vuelo->vuelo->name}}]
                                        @endif
                                    </td>

                                    {{-- <td></td> --}}

                                </tr>
                            @endforeach
                        @endif
                    </tbody>

                </table>
            @endif

            @if($ficha->vuelos_web && $bSinPlazas)
                @if($booking_corporativo)
                    @lang('area.booking.corporativo_no_plazas')
                    <br>
                @else
                    <div class="alert alert-danger" role="alert" style="text-align:justify;">
                        @lang('area.booking.no_vuelos')
                    </div>
                @endif
            @endif

            @if($transporte_vuelos)
                <div id="div_transporte_requisitos">
                    @include('includes.form_input_text', [ 'campo'=> 'transporte_requisitos', 'texto'=> trans('area.booking.transporte_requisitos')])
                </div>

                @if( !ConfigHelper::config('tpv_aeropuertos') )
                <br />
                @include('includes.form_checkbox', [ 'campo'=> 'transporte_recogida_bool',
                    'texto'=> trans('area.booking.transporte_recogida_bool'), 'valor'=>$ficha->transporte_recogida ])
                <div id="div_transporte_recogida_bool"
                    @if(!$ficha->transporte_recogida)
                        style="display:none;"
                    @endif
                >

                    @include('includes.form_input_text', [ 'campo'=> 'transporte_recogida',
                        'texto'=> trans('area.booking.transporte_recogida')])
                </div>
                @endif
            @endif

        @endif

        @if(!$ficha->vuelos_web || $booking->curso->es_convocatoria_abierta)
            <div id="transporte_otro_div"
                @if($ficha->transporte_no)
                    style="display:none;"
                @endif
            >
            <hr>
            @include('includes.form_checkbox', [ 'campo'=> 'transporte_otro', 'texto'=> trans('area.booking.transporte_otro') ])
            <div id="div_transporte_otro"
                @if(!$ficha->transporte_otro)
                    style="display:none;"
                @endif
            >
                @include('includes.form_input_text', [ 'campo'=> 'transporte_detalles', 'texto'=> trans('area.booking.datos_aeropuerto')])
            </div>
            </div>
        @endif

    </div>
    </div>

</div>
@endif

<div id="paso1-extras" style='{{$style_extras}}' class="{{$class_extras}}">
    @include('comprar.booking_curso-extras')
</div>

<hr>
{{-- @include('comprar.booking_codigo') --}}

{{-- RESTO DE LA VISTA. EN OTROS BOOKINGS NO VA AQUÍ. SE HACE ASÍ POR LOS BOTONES --}}
<hr>

@include('comprar.booking_total', ['ficha'=> $booking])

<div class="row margintop20">
    <div class="col-sm-2">
        <a href="{{route('area.comprar.cancelar', $booking->id)}}" class="btn btn-danger">@lang('area.booking.cancelar')</a>
    </div>
    <div class="col-sm-10">
        {!! Form::submit(trans('area.siguiente'),['id'=> "btn-siguiente_$paso", 'class' => 'btn btn-success pull-right siguiente']) !!}
        {{-- @if(!$booking->tpv_tiempo_agotado && $hayPlazas && !$bSinPlazas)
            {!! Form::submit(trans('area.siguiente'),['id'=> "btn-siguiente_$paso", 'class' => 'btn btn-success pull-right siguiente']) !!}
        @endif --}}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        @if( $ficha->curso->es_convocatoria_multi )

            $('#btn-siguiente_1').click( function(e) {
                e.preventDefault();

                var $iEsp = 0;
                $('.booking-multi-especialidad option:selected').each(function() {
                    if($(this).val() == 0)
                    {
                        $iEsp++;
                    }
                });

                if( $iEsp > 0 )
                {
                    var iEsp = $('.booking-multi-especialidad option[value="0"]').length;
                    bootbox.alert("{{trans('area.booking.especialidades_error')}}" +iEsp);
                }
                else
                {
                    $('#frmBooking').unbind("submit").submit();
                }

            });

        @endif


        @if( $ficha->curso->es_convocatoria_abierta )
            
            $('#btn-siguiente_1').click( function(e) {
                e.preventDefault();

                $err = false;
                $err_fechas = false;
                $err_alojamiento = false;
                
                bfi = $('#booking-cabierta-fecha_ini').val();
                bff = $('#booking-cabierta-fecha_fin').val();
                if(bfi == "" && bff == "")
                {
                    $err = true;
                    $err_fechas = true;
                }

                ba = $('#booking-cabierta-alojamiento').val();
                bai = $('#booking-alojamiento-fecha_ini').val();
                baf = $('#booking-alojamiento-fecha_fin').val();
                if(ba > 0 && bfi == "" && bff == "")
                {
                    $err = true;
                    $err_alojamiento = true;
                }

                ba = $('#booking-cabierta-alojamiento').val();
                bai = $('#booking-alojamiento-fecha_ini').val();
                baf = $('#booking-alojamiento-fecha_fin').val();
                if(ba > 0 && bfi == "" && bff == "")
                {
                    $err = true;
                    $err_alojamiento = true;
                }
                
                if( $err )
                {
                    $errTxt = "{{trans('area.booking.paso1_error')}}:";
                    $errTxt_fechas = "{{trans('area.booking.paso1_error_fechas')}}";
                    $errTxt_alojamiento = "{{trans('area.booking.paso1_error_alojamiento')}}";
                    
                    error_txt = "";
                    if($err_fechas)
                    {
                        error_txt += " "+ $errTxt_fechas;
                    }

                    if($err_alojamiento)
                    {
                        if(error_txt != "")
                        {
                            error_txt += ", ";
                        }

                        error_txt += $errTxt_alojamiento;
                    }

                    error_txt = $errTxt + error_txt;

                    bootbox.alert( error_txt );
                }
                else
                {
                    $('#frmBooking').unbind("submit").submit();
                }

            });

        @endif

        
        @if( $ficha->curso->es_convocatoria_cerrada && !$booking_corporativo )
            
            $('#btn-siguiente_1').click( function(e) {
                e.preventDefault();

                $err = false;
                $err_fechas = false;
                $err_alojamiento = false;
                $err_transporte = false;

                bfi = $("input[name='booking-ccerrada_id']").val();
                if(bfi == 0)
                {
                    $err = true;
                    $err_fechas = true;
                }

                @if($ficha->curso->es_convocatoria_semicerrada)
                    
                    bf = $('#course_start_date').val();
                    if(bf == "")
                    {
                        $err = true;
                        $err_fechas = true;
                    }

                @endif


                ba = $("input[name='booking-alojamiento_id']").val();
                if(ba == "" || ba == 0)
                {
                    $err = true;
                    $err_alojamiento = true;
                }

                
                @if(!$ficha->vuelos_web)
                    $err_transporte = false;
                @else
                    $err_transporte = true;
                @endif

                bt = $("input[name='booking-vuelo_id']").val();
                if( $err_transporte && bt==0 )
                {
                    $err = true;
                    $err_transporte = true;
                }
                

                if( $err )
                {
                    $errTxt = "{{trans('area.booking.paso1_error')}}:";
                    $errTxt_fechas = "{{trans('area.booking.paso1_error_fechas')}}";
                    $errTxt_alojamiento = "{{trans('area.booking.paso1_error_alojamiento')}}";
                    $errTxt_transporte = "{{trans('area.booking.paso1_error_transporte')}}";

                    error_txt = "";
                    if($err_fechas)
                    {
                        error_txt += " "+ $errTxt_fechas;
                    }

                    if($err_alojamiento)
                    {
                        if(error_txt != "")
                        {
                            error_txt += ", ";
                        }

                        error_txt += $errTxt_alojamiento;
                    }

                    if($err_transporte)
                    {
                        if(error_txt != "")
                        {
                            error_txt += ", ";
                        }

                        error_txt += $errTxt_transporte;
                    }

                    error_txt = $errTxt + error_txt;

                    bootbox.alert( error_txt );
                }
                else
                {
                    $('#frmBooking').unbind("submit").submit();
                }

            });

        @endif

        

    });

</script>