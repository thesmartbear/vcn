@extends('web.britishsummerv3.baseweb')

@section('title')
    {{ConfigHelper::config('nombre')}} - {{$curso->course_seo_title}}
@stop

@section('header')
@stop

@section('container')

<div class="container">

    {{-- <div class="row">
        <br><br>
        <br><br>

        {!! trans('web.curso.comprar-multi') !!}
    </div> --}}

    <div class="row">
        <div class="col-sm-8 col-xs-12">

            {{-- Booking --}}
            @include('comprar.button', ['deseoOff'=> true])

            {{-- Booking --}}
            @include('comprar.modal')

        </div>
    </div>
</div>

@stop

@section('extra_footer')
    {{-- Booking --}}
    @include('comprar.scripts')

    @if(Session::get('vcn.modal'))
        <script type="text/javascript">
            $('#bookingModal').modal('show');
        </script>
    @endif
@stop