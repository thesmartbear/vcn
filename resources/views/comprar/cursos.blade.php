@extends('layouts.area')

@section('breadcrumb')
    {!! Breadcrumbs::render('area.comprar.cursos') !!}
@stop

@section('content')


    <div class="form-group row">
        <div class="col-md-3">
        {!! Form::label('categorias', 'Categoría') !!}
        @include('includes.form_input_cargando',['id'=> 'categorias-cargando'])
        <br>
        {!! Form::select('categorias', $categorias, 0, array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-categorias'))  !!}
        @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'cursos', 'online'=> true])
        @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategorias', 'online'=> true])
        {{-- @include('includes.script_filtros', ['filtro'=> 'categorias', 'destino'=> 'subcategoriasdet', 'online'=> true]) --}}
        </div>

        <div class="col-md-3">
        {!! Form::label('subcategorias', 'SubCategoría') !!}
        @include('includes.form_input_cargando',['id'=> 'subcategorias-cargando'])
        <br>
        {!! Form::select('subcategorias', $subcategorias, 0, array('class'=>'select2', 'data-style'=>'red', 'id'=>'filtro-subcategorias'))  !!}
        @include('includes.script_filtros', ['filtro'=> 'subcategorias', 'destino'=> 'cursos', 'online'=> true])
        {{-- @include('includes.script_filtros', ['filtro'=> 'subcategorias', 'destino'=> 'subcategoriasdet', 'online'=> true]) --}}
        </div>

        <div class="col-md-6">
        {!! Form::label('cursos', 'Curso') !!}<br>
        @include('includes.form_input_cargando',['id'=> 'cursos-cargando'])

        {!! Form::select('cursos', $cursos, 0, array('class'=>'select2', 'data-style'=>'orange', 'id'=>'filtro-cursos'))  !!}
        </div>
    </div>

    <hr>

    <div class="form-group row">
        <div class="col-sm-6">
            <button onclick="goBack()" class="btn btn-danger">Cancelar</button>
        </div>
        <div class="col-sm-6">
            <a id="btn-curso" href="#curso" class="btn btn-success pull-right">Seleccionar</a>
        </div>
    </div>

<script type="text/javascript">
    var $url = "{{ route('area.comprar.curso') }}";
    $(document).ready(function(){
        $('#btn-curso').click( function(e) {
            e.preventDefault();

            $curso_id = $('#filtro-cursos').val();

            if(!$curso_id)
            {
                bootbotx.alert("Debe seleccionar un curso");
            }
            else
            {
                window.location.href = $url +"/"+ $curso_id;
            }

        });
    });
</script>

@stop