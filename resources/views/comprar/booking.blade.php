@extends('layouts.area')

@section('breadcrumb')
    {!! Breadcrumbs::render('area.comprar', $booking->id) !!}
@stop


@section('content')

<div class="modal fade" id="modalBooking-loading" style="top: 20% !important;">
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            Booking: @lang("Recalculando...")
        </div>
        <div class="modal-body">

            <i class="fa fa-spinner fa-spin fa-2x"></i>

        </div>
        <div class="modal-footer">

        </div>
    </div>
</div>
</div>

@if($booking->tpv_tiempo_agotado)

<div class="booking">
    @include('comprar.booking-header')
    <br>
</div>

@else

<div class="booking">

    {{-- <hr>
    Fase {{$booking->fase}}

    -> CONVOCATORIA:
    @if($booking->curso->es_convocatoria_semicerrada_solo)
        SEMICERRADA
    @elseif($booking->curso->es_convocatoria_cerrada)
        CERRADA

        @if($booking->curso->es_convocatoria_semicerrada)
            + SEMICERRADA
        @endif

    @elseif($booking->curso->es_convocatoria_abierta)
        ABIERTA

    @elseif($booking->curso->es_convocatoria_multi)
        MULTI

    @endif --}}

    {{-- @if($ficha->fase1 && $ficha->fase<4)
        <hr>
        @include('comprar.booking_total', ['ficha'=> $booking])
    @endif

    @if($booking->descuento)
        <hr>
        <div class="descuento col-md-12">Descuento especial de {{ $booking->descuento->name }}</div>
    @endif --}}

    <div class="row">
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <?php $paso = 1; ?>
                    <li role="presentation" class="{{$booking->fase==$paso?'active':''}}">
                        <div class="label">{{trans('area.booking.datos_curso')}}</div>
                        <a class="withlabel" href="#step{{$paso}}" data-toggle="tab" aria-controls="step{{$paso}}" role="tab" title="@lang("Paso") {{$paso}}">
                            <span class="round-tab"><i class="fa fa-graduation-cap"></i></span>
                        </a>
                    </li>

                    <?php $paso = 2; ?>
                    <li role="presentation" class="{{$booking->fase==$paso?'active':''}} {{$booking->fase>=$paso?'':'disabled'}}">
                        <div class="label">{{trans('area.booking.datos_viajero')}}</div>
                        <a href="#step{{$paso}}" data-toggle="tab" aria-controls="step{{$paso}}" role="tab" title="@lang("Paso") {{$paso}}">
                            <span class="round-tab"><i class="fa fa-user"></i></span>
                        </a>
                    </li>
                    <?php $paso = 3; ?>
                    <li role="presentation" class="{{$booking->fase==$paso?'active':''}} {{$booking->fase>=$paso?'':'disabled'}}">
                        <div class="label">{{trans('area.booking.datos_tutores')}}</div>
                        <a href="#step{{$paso}}" data-toggle="tab" aria-controls="step{{$paso}}" role="tab" title="@lang("Paso") {{$paso}}">
                            <span class="round-tab"><i class="fa fa-users"></i></span>
                        </a>
                    </li>

                    <?php $paso = 4; ?>
                    <li role="presentation" class="{{$booking->fase==$paso?'active':''}} {{$booking->fase>=$paso?'':'disabled'}}">
                        <div class="label">{{trans('area.booking.datos_pago')}}</div>
                        <a href="#step{{$paso}}" data-toggle="tab" aria-controls="step{{$paso}}" role="tab" title="@lang("Paso") {{$paso}}">
                            <span class="round-tab"><i class="fa fa-credit-card"></i></span>
                        </a>
                    </li>

                </ul>
            </div>

            <div class="tab-content col-md-12">

                @include('comprar.booking-header')
                <br>

                <?php $paso = 1; ?>
                <div class="tab-pane {{$booking->fase==$paso?'active':''}}" role="tabpanel" id="step{{$paso}}">

                    {!! Form::open(array('id'=> 'frmBooking', 'method'=> 'post', 'url' => route('area.comprar.booking.post'))) !!}
                        {!! Form::hidden('paso', $paso) !!}
                        {!! Form::hidden('booking-id',$booking->id) !!}
                        {!! Form::hidden('booking-ccerrada_id',$booking->convocatory_close_id) !!}
                        {!! Form::hidden('booking-cabierta_id',$booking->convocatory_open_id) !!}
                        {!! Form::hidden('booking-cmulti_id',$booking->convocatory_multi_id) !!}
                        {!! Form::hidden('booking-alojamiento_id',$booking->accommodation_id) !!}
                        {!! Form::hidden('booking-vuelo_id',$booking->vuelo_id) !!}

                        @if($booking->descuento)
                            <div class="descuento col-md-12"> {{trans('area.descuentoespecial')}}: {{ $booking->descuento->name }}</div>
                            <hr>
                        @endif

                        @include("comprar.booking_paso$paso", ['ficha'=> $booking])
                        
                        {{-- ESTE LO TIENE TODO EN EL INCLUDE POR EL TEMA DE PLAZAS --}}

                    {!! Form::close() !!}

                </div>

                <?php $paso = 2; ?>
                @if($booking->fase>=$paso)
                <div class="tab-pane {{$booking->fase==$paso?'active':''}}" role="tabpanel" id="step{{$paso}}">
                    <h3 class="booking-title notable">@lang('area.booking.datos_viajero')</h3>

                    {!! Form::open(array('id'=> 'frmValidar', 'method'=> 'post', 'url' => route('area.comprar.booking.post'))) !!}
                        {!! Form::hidden('paso', $paso) !!}
                        {!! Form::hidden('booking-id',$booking->id) !!}
                        {!! Form::hidden('booking-ccerrada_id',$booking->convocatory_close_id) !!}
                        {!! Form::hidden('booking-cabierta_id',$booking->convocatory_open_id) !!}
                        {!! Form::hidden('booking-cmulti_id',$booking->convocatory_multi_id) !!}
                        {!! Form::hidden('booking-alojamiento_id',$booking->accommodation_id) !!}

                        @include("comprar.booking_paso$paso", ['ficha'=> $booking])

                        <div class="row margintop20">
                            <div class="col-sm-2">
                                <a href="{{route('area.comprar.cancelar', $booking->id)}}" class="btn btn-danger">@lang('area.booking.cancelar')</a>
                            </div>
                            <div class="col-sm-10">
                                @if(!$booking->tpv_tiempo_agotado)
                                    {!! Form::submit(trans('area.siguiente'),['class' => 'btn btn-success pull-right siguiente']) !!}
                                @endif
                            </div>
                        </div>


                    {!! Form::close() !!}

                </div>
                @endif

                <?php $paso = 3; ?>
                @if($booking->fase>=$paso)
                <div class="tab-pane {{$booking->fase==$paso?'active':''}}" role="tabpanel" id="step{{$paso}}">
                    <h3 class="booking-title notable">@lang('area.booking.datos_tutores')</h3>

                    {!! Form::open(array('id'=> 'frmValidar3', 'method'=> 'post', 'url' => route('area.comprar.booking.post'))) !!}
                        {!! Form::hidden('paso', $paso) !!}
                        {!! Form::hidden('booking-id',$booking->id) !!}
                        {!! Form::hidden('booking-ccerrada_id',$booking->convocatory_close_id) !!}
                        {!! Form::hidden('booking-cabierta_id',$booking->convocatory_open_id) !!}
                        {!! Form::hidden('booking-cmulti_id',$booking->convocatory_multi_id) !!}
                        {!! Form::hidden('booking-alojamiento_id',$booking->accommodation_id) !!}

                        @include("comprar.booking_paso$paso", ['ficha'=> $booking])

                        <div class="row margintop20">
                            <div class="col-sm-2">
                                <a href="{{route('area.comprar.cancelar', $booking->id)}}" class="btn btn-danger">@lang('area.booking.cancelar')</a>
                            </div>
                            <div class="col-sm-10">
                                @if(!$booking->tpv_tiempo_agotado)

                                    {!! Form::submit(trans('area.siguiente'),['class' => 'btn btn-success pull-right siguiente']) !!}
                                    
                                    @if( Session::get('vcn.booking-nif') )
                                        {!! trans('area.nota_nif_no') !!}
                                        <script>
                                        $(document).ready(function(){

                                            bootbox.confirm({
                                                title: "NOTA",
                                                message: "{!! trans('area.nota_nif') !!}",
                                                buttons: {
                                                    confirm: {
                                                    label: "{{trans('area.nota_nif_seguir')}}",
                                                    className: 'btn-warning'
                                                    },
                                                    cancel: {
                                                    label: "{{trans('area.nota_nif_volver')}}",
                                                    className: 'btn-success'
                                                    }
                                                },
                                                callback: function (result) {
                                                    if(result)
                                                    {
                                                        $('#booking-nif').prop('checked', true);
                                                        $('#frmValidar3').submit();
                                                    }
                                                }
                                            });
                                        });
                                        </script>
                                    @endif

                                @endif
                            </div>
                        </div>


                    {!! Form::close() !!}

                </div>
                @endif

                <?php $paso = 4; ?>
                @if($booking->fase>=$paso)
                <div class="tab-pane {{$booking->fase==$paso?'active':''}}" role="tabpanel" id="step{{$paso}}">
                    <h3 class="booking-title notable">@lang('area.booking.datos_resumen')</h3>

                    {!! Form::open(array('id'=> 'formBooking', 'method'=> 'post', 'url' => route('area.comprar.booking.post'))) !!}
                        {!! Form::hidden('paso', $paso) !!}
                        {!! Form::hidden('booking-id',$booking->id) !!}
                        {!! Form::hidden('booking-ccerrada_id',$booking->convocatory_close_id) !!}
                        {!! Form::hidden('booking-cabierta_id',$booking->convocatory_open_id) !!}
                        {!! Form::hidden('booking-cmulti_id',$booking->convocatory_multi_id) !!}
                        {!! Form::hidden('booking-alojamiento_id',$booking->accommodation_id) !!}

                        @include("comprar.booking_paso$paso", ['ficha'=> $booking])

                    {!! Form::close() !!}

                </div>
                @endif

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>

@endif

<script type="text/javascript">
function habilitarBoton(check1, check2)
{
    if(check1 && check2)
    {
        $('#btn-pagar-ok').removeClass("disabled");

        $('#btn-pagar-ok').attr('data-original-title', "{!! trans('area.pagar') !!}").tooltip('fixTitle');
    }
    else
    {
        $('#btn-pagar-ok').addClass("disabled");
        $('#btn-pagar-ok').attr('data-original-title', "{!! trans('area.pagar_aceptar') !!}").tooltip('fixTitle');
    }
}

$(document).ready(function () {
    //Initialize tooltips
    //$('.nav-tabs > li a[title]').tooltip();
    
    // $('.siguiente').attr('disabled',true);

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $('#btn-pagar-ok').data('label', "{{trans('area.pagar_aceptar')}}");
    $('#btn-pagar-ok').addClass("disabled");

    $('#condiciones').change( function() {
        var c1 = $(this).is(':checked');

        @if(ConfigHelper::config('es_rgpd'))
            var c2 = $('#lopd_check1').is(':checked');
        @else
            var c2 = true;
        @endif

        habilitarBoton(c1,c2);
        
    });

    $('#lopd_check1').change( function() {
        var c1 = $('#condiciones').is(':checked');
        var c2 = $(this).is(':checked');

        habilitarBoton(c1,c2);
    });

});
// function nextTab(elem) {
//     $(elem).next().find('a[data-toggle="tab"]').click();
// }
// function prevTab(elem) {
//     $(elem).prev().find('a[data-toggle="tab"]').click();
// }
</script>
@include('includes.script_booking', ['ficha'=> $booking])

@stop