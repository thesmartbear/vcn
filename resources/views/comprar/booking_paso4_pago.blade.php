<?php
$esBanco = $booking->es_pago_banco;
$esTPV = $booking->es_pago_tpv;
?>

{{-- PAGO x TRANSFERENCIA --}}
@if(ConfigHelper::es_booking_online_transferencia())
    <div class="form-group">
        <a id='btn-pagar-banco' data-label="{{trans('area.pagar_banco')}}" class="btn btn-lg btn-pagar"><i class="fa fa-money"></i> {{trans('area.pagar_banco')}}</a>
    </div>

    <div id="div_pago_banco" style="display: none;">
        <div class="note note-info booking-note">

            <div class="form-group">
                @lang('area.booking.pago_instrucciones')
            </div>

            <div class="form-group margintop20">
                @lang('area.booking.pago_cuenta', ['importe'=> ConfigHelper::parseMoneda($booking->online_reserva)])
                <br>

                {{$booking->banco_ccc}}

                <hr>

                <strong>@lang('area.booking.pago_comprobante'):</strong>

                <p><i>@lang('area.booking.pago_comprobante_archivos')</i></p>

                <div class="dropzone" id="myDropzone"></div>
                @include('comprar.script_dropzone', ['name'=> 'myDropzone', 'url'=> route('comprar.pago.adjunto', $ficha->id), 'max_filesize'=> 2, 'max_files'=> 1])

                <br>
                <p>{!! __('area.booking.pago_comprobante_error', ['email'=> $booking->banco_email]) !!}</p>
            </div>

            {{-- <div class="row margintop60">
                <div class="col-sm-6">
                    <a href="{{route('area.comprar.cancelar')}}" class="btn btn-danger">@lang('area.booking.cancelar')</a>
                </div>
                <div class="col-sm-6 pull-right">
                    <button id="btn-pagar-banco_ok" type="submit" data-label="{{trans('area.pagar_banco')}}" class="btn btn-success pull-right btn-pagar" name="tpv0"><i class="fa fa-money"></i> {{trans('area.pagar')}}</button>
                </div>
            </div> --}}


            {!! Form::hidden('comprobante', false) !!}

        </div>

        <script type="text/javascript">
            $(document).ready(function() {

                $('#btn-pagar-banco').click( function(e) {
                    $('#div_pago_tpv').hide();
                    $('#div_pago_banco').show();

                    $('#forma_pago').val(0);

                    // $('html,body').animate({scrollTop: $("#div_pago_banco").offset().top}, 'slow');
                });

                $('#btn-pagar-tpv').click( function(e) {
                    $('#div_pago_banco').hide();
                    $('#div_pago_tpv').show();

                    $('#forma_pago').val(1);

                    // $('html,body').animate({scrollTop: $("#div_pago_tpv").offset().top}, 'slow');
                });

                $('#btn-pagar-ok').click(function(e) {

                    e.preventDefault();

                    if($(this).hasClass('disabled'))
                    {
                        return;
                    }

                    checkLOPD(btnPagar);

                });

                @if(!$esTPV)
                    $('#btn-pagar-banco').click();
                @endif
            });

            function btnPagar()
            {
                if($('#forma_pago').val() == 'x')
                {
                    bootbox.alert("{!! trans('area.booking.datos_formapago') !!}");
                    return;
                }

                if($('#forma_pago').val() == 1)
                {
                    $("#formBooking").submit();
                    return;
                }

                if(hayAdjunto)
                {
                    $("#formBooking").submit();
                    return;
                }
                else
                {
                    var $msg = "{{trans('area.booking.pago_comprobante_falta')}}";
                    bootbox.confirm({
                        message: $msg,
                        buttons: {
                            confirm: {
                                label: "{{trans('area.booking.aceptar')}}",
                                className: 'btn-success'
                            },
                            cancel: {
                                label: "{{trans('area.booking.volver')}}",
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {

                            if(result)
                            {
                                $("#formBooking").submit();
                            }
                        }
                    });
                }
            }
        </script>

    </div>
@endif

<br>

{{-- PAGO x TPV --}}
@if($esTPV)
    <div class="form-group">
        <a id='btn-pagar-tpv' data-label="{{trans('area.pagar_tpv')}}" class="btn btn-lg btn-pagar"><i class="fa fa-credit-card"></i> {{trans('area.pagar_tpv')}}</a>
    </div>

    <div id="div_pago_tpv" style="display: none;">
        <div class="note note-info booking-note">

            <div class="margintop20">
                @lang('area.booking.pago_click')
            </div>

            {{-- <div class="row margintop20">
                <div class="col-sm-6">
                    <a href="{{route('area.comprar.cancelar')}}" class="btn btn-danger">@lang('area.booking.cancelar')n</a>
                </div>
                <div class="col-sm-6 pull-right">
                    <button type="submit" name="btn-tpv1" id="btn-pagar-tpv" data-label="{{trans('area.pagar')}}" disabled class="btn btn-success pull-right btn-pagar"><i class="fa fa-credit-card"></i> {{trans('area.pagar')}}</button>
                </div>
            </div> --}}

        </div>
    </div>
@endif