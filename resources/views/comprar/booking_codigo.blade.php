<div class="panel panel-warning">
    <div class="panel-heading">
        <i class="fa fa-flag fa-tag"></i> @lang('area.booking.codigo')
    </div>
    <div class="panel-body">

        <div id="app_booking_codigo" class="form-group row">
            <div class="col-md-3">
                <input id="codigo" placeholder="" name="codigo" type="text" class="form-control" v-model="codigo">
            </div>
            <div class="col-md-2">
                <a id="codigo-actualizar" v-if="actualizar" href="#" class="btn btn-success" v-on:click.prevent="sendValidar">@lang('area.actualizar')</a>
                <a id="codigo-validar" v-else href="#" class="btn btn-warning" v-on:click.prevent="sendValidar">@lang('area.booking.validar')</a>
            </div>
            <div class="col-md-3">
                <span v-if="actualizar">@lang('area.booking.codigo_actualizar')</span>
            </div>
            <div class="col-md-4"><span id="booking-codigo-result"></span></div>
        </div>

    </div>
</div>

@push('scripts')
<script src="https://cdn.jsdelivr.net/vue/2.2.6/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.0/vue-resource.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.0/axios.min.js"></script>

<script type="text/javascript">

    var vmCodigo = new Vue({
            el: '#app_booking_codigo',
            data: function () {
                return {
                    codigo: null,
                    codigo_result: "",
                    actualizar: {{$ficha->es_codigo ? 1 : 0}}
                };
            },
            mounted: function () {

            },
            destroyed() {

            },

            methods: {

                sendValidar: function(event) {

                    if(this.codigo == "" || !this.codigo)
                    {
                        return;
                    }

                    data = {
                        _token: '{{Session::token()}}',
                        codigo: this.codigo,
                        actualizar: this.actualizar,
                    };

                    this.$http.post('{{route('comprar.codigo', $ficha->id)}}', data).then(function (response) {
                        console.log(response.data);

                        this.codigo = "";

                        if(response.data.descuento )
                        {
                            this.codigo_result = response.data.result;
                            precioTotal();
                            // $("#codigo-validar").hide();
                            this.actualizar = 1
                        }
                        else
                        {
                            this.codigo_result = response.data.result;
                        }

                        $("#booking-codigo-result").html(this.codigo_result)
                    });
                }

            },
        });

</script>
@endpush