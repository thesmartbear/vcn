<?php
$max_duracion = $ficha->convocatoria?$ficha->convocatoria->duracion:100;
?>

<hr>
<h2 class="booking-title">Elige tus opciones</h2>
<table class="table">
<tbody>

{{-- EXTRAS CURSO --}}
@if($ficha->curso->extras->count())

    {{-- EXTRAS CURSO OBLIGATORIOS --}}
    @foreach( $ficha->curso->extras->where('course_extras_required',1) as $extra )
        <tr>
            <td>
                <i class="{{$ficha->extra_class_curso($extra->id)}} booking-extra obligatorio" id="extra-curso-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='curso'></i>
                {{$extra->name}} <i>(obligatorio)</i>
            </td>

            @include('includes.bookings_td_unidades', ['prefix'=> 'extra-curso', 'tipo'=>0])
            @include('includes.bookings_td_precio', ['prefix'=> 'extra-curso-precio', 'tipo'=>0])

        </tr>
    @endforeach

    {{-- EXTRAS CURSO NO OBLIGATORIOS --}}
    @foreach( $ficha->curso->extras->where('course_extras_required',0) as $extra )
        <tr>
            <td>
                <i class="{{$ficha->extra_class_curso($extra->id)}} booking-extra" id="extra-curso-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='curso'></i>
                {{$extra->name}}
            </td>

            @include('includes.bookings_td_unidades', ['prefix'=> 'extra-curso', 'tipo'=>0])
            @include('includes.bookings_td_precio', ['prefix'=> 'extra-curso-precio', 'tipo'=>0])

        </tr>
    @endforeach

@endif


{{-- EXTRAS CENTRO --}}
@if($ficha->centro->extras->count())

    {{-- EXTRAS CENTRO OBLIGATORIOS --}}
    @foreach( $ficha->centro->extras->where('center_extras_required',1) as $extra )
        <tr>
            <td>
                <i class="{{$ficha->extra_class_centro($extra->id)}} booking-extra obligatorio" id="extra-centro-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='centro'></i>
                {{$extra->name}} <i>(obligatorio)</i>
            </td>

            @include('includes.bookings_td_unidades', ['prefix'=> 'extra-centro', 'tipo'=>1])
            @include('includes.bookings_td_precio', ['prefix'=> 'extra-centro-precio', 'tipo'=>1])

        </tr>
    @endforeach

    {{-- EXTRAS CENTRO NO OBLIGATORIOS --}}
    @foreach( $ficha->centro->extras->where('center_extras_required',0) as $extra )
        <tr>
            <td>
                <i class="{{$ficha->extra_class_centro($extra->id)}} booking-extra" id="extra-centro-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='centro'></i>
                {{$extra->name}}
            </td>

            @include('includes.bookings_td_unidades', ['prefix'=> 'extra-centro', 'tipo'=>1])
            @include('includes.bookings_td_precio', ['prefix'=> 'extra-centro-precio', 'tipo'=>1])

        </tr>
    @endforeach

@endif

{{-- EXTRAS ALOJAMIENTO --}}
@if($ficha->alojamiento && $ficha->alojamiento->cuotas->count())

    {{-- EXTRAS ALOJAMIENTO OBLIGATORIOS --}}
    @if($ficha->alojamiento)
    @foreach( $ficha->alojamiento->cuotas->where('required',1) as $extra )
        <tr>
            <td>
                <i class="{{$ficha->extra_class_alojamiento($extra->id)}} booking-extra obligatorio" id="extra-alojamiento-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='alojamiento'></i>
                {{$extra->name}} <i>(obligatorio)</i>
            </td>

            @include('includes.bookings_td_unidades', ['prefix'=> 'extra-alojamiento', 'tipo'=>3])
            @include('includes.bookings_td_precio', ['prefix'=> 'extra-alojamiento-precio', 'tipo'=>3])

        </tr>
    @endforeach

    {{-- EXTRAS ALOJAMIENTO NO OBLIGATORIOS --}}
    @foreach( $ficha->alojamiento->cuotas->where('required',0) as $extra )
        <tr>
            <td>
                <i class="{{$ficha->extra_class_alojamiento($extra->id)}} booking-extra" id="extra-alojamiento-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='alojamiento'></i>
                {{$extra->name}}
            </td>

            @include('includes.bookings_td_unidades', ['prefix'=> 'extra-alojamiento', 'tipo'=>3])
            @include('includes.bookings_td_precio', ['prefix'=> 'extra-alojamiento-precio', 'tipo'=>3])

        </tr>
    @endforeach
    @endif

@endif

{{-- EXTRAS GENERICOS --}}
@if( $ficha->curso->extrasGenericos->count() )

    {{-- EXTRAS GENERICOS OBLIGATORIOS --}}
    @foreach( $ficha->curso->extrasGenericos as $extra )

        @if($extra->generico->generic_required)

        <tr>
            <td>
                <i class="{{$ficha->extra_class_generico($extra->id)}} booking-extra obligatorio" id="extra-generico-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='generico'></i>
                {{$extra->name}} <i>(obligatorio)</i>
            </td>

            @include('includes.bookings_td_unidades', ['prefix'=> 'extra-generico', 'tipo'=>2])
            @include('includes.bookings_td_precio', ['prefix'=> 'extra-generico-precio', 'tipo'=>2])

        </tr>

        @endif

    @endforeach

    {{-- EXTRAS GENERICOS NO OBLIGATORIOS --}}
    @foreach( $ficha->curso->extrasGenericos as $extra )

        @if(!$extra->generico->generic_required)

            <tr>
                <td>
                    <i class="{{$ficha->extra_class_generico($extra->id)}} booking-extra" id="extra-generico-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='generico'></i>
                    {{$extra->name}}
                </td>

                @include('includes.bookings_td_unidades', ['prefix'=> 'extra-generico', 'tipo'=>2])
                @include('includes.bookings_td_precio', ['prefix'=> 'extra-generico-precio', 'tipo'=>2])

            </tr>

        @endif

    @endforeach

@endif

{{-- EXTRAS OTROS --}}
@if($ficha->extras_otros->count())
    @foreach( $ficha->extrasOtros as $extra )

        <tr>
            <td>
                <i class="booking-extra-otro {{$ficha->extra_class_otro($extra->id)}} obligatorio" id="extra-otro-{{$extra->id}}" data-id='{{$extra->id}}' data-tipo='otro'></i>
                {{$extra->name}} [{{$extra->notas}}]
            </td>

            <td>1</td>
            <td align='right'>{{$extra->precio}} {{$extra->moneda_name}}</td>

        </tr>

    @endforeach
@endif

</tbody>
</table>

{{-- SEGURO CANCELACIÓN AUTO --}}
@if(ConfigHelper::config('seguro'))

    <?php
    $seguro = $ficha->cancelacion_seguro;
    $texto = trans('area.booking.seguro_cancelacion');
    $seguro_name = "";
    if($seguro)
    {
        $seguro_name = $seguro->full_name;
        $pdf = $ficha->cancelacion_pdf;
        $seguro_name = $pdf ? "<a href='/$pdf' target='_blank'>$seguro_name</a>" : $seguro_name;
    }
    ?>

    <hr>
    <h2 class="booking-title">@lang('area.booking.datos_cancelacion')</h2>
    <div class="seguro">
        @include('includes.form_checkbox', [ 'campo'=> 'booking-seguro-ajax', 'valor'=> $ficha->cancelacion, 'texto'=> $texto ])
    </div>
    <span id="booking-seguro-name">
        @if($seguro)
            {!! $seguro_name !!}
        @endif
    </span>

@endif