<div class="row" style="margin-top:40px">

    <div class="col-sm-8">
        <h3 class="text-success">{{trans('area.curso')}}: {{ $booking->programa}}</h3>

        @if($booking->convocatoria)
            <h5>@lang('area.convocatoria'): {{$booking->convocatoria->name}}</h5>
        @endif 

        <h4>@lang('area.pais'): {{$booking->centro->pais_name}}</h4>
        {{-- {{$booking->categoria->name}} - {{$booking->subcategoria->name}} --}}
    </div>

    @if(!$booking->tpv_tiempo_agotado)
    <div class="col-sm-4">
         @if(!$booking->es_corporativo)
            <a href="{{route('area.comprar.curso', [0,$ficha->id])}}" class="btn btn-danger pull-right">{{trans('area.booking.btn_cambiar')}}</a>
         @endif
    </div>
    @endif

</div>

@if($booking->ovbkg_id)
<div class="row">
    <div class="note note-warning">
        <strong>@lang('area.booking.overbooking')</strong>
    </div>
</div>
@endif

<hr>

{{-- @if($booking->curso->es_convocatoria_cerrada) --}}
    @if($booking->tpv_tiempo_agotado)
        <div class="note note-danger">
            <h4 class="block">@lang('area.booking.atencion')</h4>
            <p>
                {!! trans('area.compra_tiempo_agotado') !!}
            </p>
        </div>

        <div class="row margintop20">
            <div class="col-sm-2">
                <a href="{{route('area.comprar.cancelar', $booking->id)}}" class="btn btn-danger">@lang('area.booking.cancelar')</a>
            </div>
            <div class="col-sm-10">
                <a href="{{route('area.comprar.curso', [0,$ficha->id,true])}}" class="btn btn-success pull-right siguiente">{{trans('area.booking.btn_reset')}}</a>
            </div>
        </div>
    @else
        <i class="fa fa-hourglass-half"></i> {!! trans_choice('area.compra_tiempo', $booking->tpv_tiempo, ['minutos'=> $booking->tpv_tiempo] ) !!}
    @endif
    <hr>
{{-- @endif --}}