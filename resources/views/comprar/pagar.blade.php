@extends('layouts.area')


@section('breadcrumb')
    {!! Breadcrumbs::render('area.index') !!}
@stop


@section('content')

    <h2 class="text-capitalize text-success">Hola, {{mb_strtolower($usuario->ficha->full_name)}}</h2> {{-- ficha devuelve viajero o tutor en función de lo q sea --}}

    <hr>

    <hr>

    Booking: {{$booking->id}}

    <hr>
    Ahora conectará con Redsys!!:
    {!! $form !!}

@stop