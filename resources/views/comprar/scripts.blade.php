@if(ConfigHelper::es_booking_online())
    @if(Session::get('vcn.compra-login'))
        <script type="text/javascript">
            $('#bookingModal').modal('show');
        </script>
    @endif
@endif

<script type="text/javascript">
    $(document).ready(function () {
        $('#comprar-error').click( function(e){
            alert('Ya tiene un booking online iniciado. Tiene que completar o cancelar el que tiene en curso.');
        });

        $("#booking-deseo").click( function(e){

            e.preventDefault();

            $("#bookingModal input[name='deseo']").val(true);
            $('#bookingModal').modal('show');
        });
    });
</script>