<script type="text/javascript">
    var url = "{{ $url }}";
    var token = "{{ Session::token() }}";

    var maxFS = "{{ isset($max_filesize)?$max_filesize:30 }}"; //MB
    var maxF = "{{ isset($max_files)?$max_files:25 }}";

    var div = "div#" + "{{$name}}";

    var hayAdjunto = false;

    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone( div, {
        url: url,
        params: {
            _token: token
        },
        paramName: "file",
        maxFilesize: maxFS,
        maxFiles: maxF,
        error: function(file, response) {
        },
        // addRemoveLinks: false,
        init: function() {
            this.on("maxfilesexceeded", function(file){
                // alert("No puede adjuntar mas archivos.");
                this.removeFile(file);
            });
        },
        success: function(file, response) {

            if(response.result)
            {
                hayAdjunto = true;
                $("input[name='comprobante']").val(response.file_name);

                if (file.previewElement) {
                  return file.previewElement.classList.add("dz-success");
                }
            }
            else
            {
                this.removeFile(file);
            }
        },
        removedfile: function(file) {
            var name = file.name;
            $.ajax({
                type: "POST",
                url: url,
                data: "f="+name+"&_token="+token,
                dataType: 'html'
            });
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
        dictDefaultMessage: "{{ trans('area.dropzone', ['max'=> $max_filesize, 'maxf'=> $max_files]) }}",
        dictFallbackMessage: "Tu navegador no soporta drag'n'drop.",
        dictInvalidFileType: "Tipo de archivo no permitido.",
        dictCancelUpload: "Cancelar",
        dictCancelUploadConfirmation: "¿Está seguro de cancelar?",
        dictRemoveFile: "Borrar archivo",
        dictRemoveFileConfirmation: null,
        dictMaxFilesExceeded: "No puede adjuntar mas archivos.",
    });
 </script>