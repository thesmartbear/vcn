{{-- Modal Booking --}}
@if(ConfigHelper::es_booking_online())
<div class="modal fade" id="bookingModal" tabindex="-1" role="dialog" aria-labelledby="bookingLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <img src="/assets/logos/{{App::getLocale()}}/{{ConfigHelper::config('logoweb')}}" width="216" height="40" />
            </div>
            <div class="modal-body">
                <div id="respuesta">

                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <button class="close" data-close="alert"></button>
                                <span>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </span>
                        </div>
                        <br>
                    @endif

                    {{-- MODAL1: EMAIL --}}
                    @if( !Session::get('vcn.compra-login') )

                        {!! Form::open(['method'=>'POST','route' => 'comprar.email', 'class' => 'form login-form']) !!}

                            {!! Form::hidden('compra', $curso->id) !!}
                            {!! Form::hidden('compra-corporativo', isset($convo_id)?$convo_id:0) !!}
                            {!! Form::hidden('deseo', Session::get('vcn.compra-deseo',false)) !!}

                            <div class="row">
                                <div class="col-xs-12">
                                    {!! trans('auth.compra-p_email') !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <label><i class="fa fa-envelope"></i> {!! trans('auth.email') !!}</label>
                                    {!! Form::text('email', old('email'), ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'required'=>true, 'autocomplete'=> 'off', 'placeholder'=> trans('auth.email')]) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 pull-right">
                                    {!! Form::submit(trans('auth.entrar'),['class' => 'btn btn-success pull-right']) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}

                    @else

                        @if( Session::get('vcn.compra-login') == 'login' )

                            {{-- MODAL CONTRASEÑA --}}
                            {!! Form::open(['url' => 'auth/login', 'class' => 'form login-form']) !!}

                                {!! csrf_field() !!}

                                {!! Form::hidden('compra', $curso->id) !!}
                                {!! Form::hidden('compra-login', Session::get('vcn.compra-login')) !!}
                                {!! Form::hidden('compra-ficha', Session::get('vcn.compra-ficha')) !!}
                                {!! Form::hidden('username', Session::get('vcn.compra-email')) !!}
                                {!! Form::hidden('deseo', Session::get('vcn.compra-deseo',false)) !!}

                                <div class="row">
                                    <div class="col-xs-12">
                                        @if( Session::get('vcn.compra-activacion') )
                                            {!! trans('auth.compra-p_activacion_pwd') !!}
                                        @else
                                            {!!trans( "auth.compra-p_login_". Session::get('vcn.compra-ficha') )!!}
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <label><i class="fa fa-lock"></i> {!! trans('auth.password') !!}</label>
                                        {!! Form::password('password', ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'required'=>true, 'autocomplete'=> 'off', 'placeholder'=> trans('auth.password')]) !!}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <a href="{{ route('password.request') }}" id="forget-password" class="forget-password">{!! trans('auth.recordarpass') !!}</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3 pull-right">
                                        {!! Form::submit(trans('auth.entrar'),['class' => 'btn btn-success pull-right']) !!}
                                    </div>
                                </div>

                            {!! Form::close() !!}

                        @elseif( Session::get('vcn.compra-login') == 'registro' )

                            {{-- MODAL REGISTRO --}}
                            {!! Form::open(['method'=>'POST','route' => 'comprar.user', 'class' => 'form login-form']) !!}

                                {!! csrf_field() !!}

                                {!! Form::hidden('compra', $curso->id) !!}
                                {!! Form::hidden('compra-corporativo', Session::get('vcn.compra-corporativo')) !!}
                                {!! Form::hidden('compra-login', Session::get('vcn.compra-login')) !!}
                                {!! Form::hidden('username', Session::get('vcn.compra-email')) !!}
                                {!! Form::hidden('deseo', Session::get('vcn.compra-deseo',false)) !!}

                                {!! trans('auth.compra-p_registro') !!}

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-radio">
                                            <label class="radio-inline">
                                                {!! Form::radio('viajero', 1, false, array('required'=> true)) !!} {!! trans('auth.compra-p_viajero') !!}
                                            </label>
                                            <label class="radio-inline">
                                                {!! Form::radio('viajero', 2, false) !!} {!! trans('auth.compra-p_tutor') !!}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <label>{!! trans('auth.nombre') !!}</label>
                                        {!! Form::text('nombre', old('nombre'), ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'required'=>true, 'autocomplete'=> 'off', 'placeholder'=> trans('auth.nombre')]) !!}
                                    </div>
                                    <div class="col-xs-6">
                                        <label>{!! trans('auth.apellidos') !!}</label>
                                        {!! Form::text('apellido', old('apellido'), ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'required'=>true, 'autocomplete'=> 'off', 'placeholder'=> trans('auth.apellidos')]) !!}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <label>{!! trans('auth.movil') !!}</label>
                                        {!! Form::text('movil', old('movil'), ['class'=> 'form-control form-control-solid placeholder-no-fix form-group', 'required'=>true, 'autocomplete'=> 'off', 'placeholder'=> trans('auth.movil')]) !!}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3 pull-right">
                                        {!! Form::submit(trans('auth.registrar'),['class' => 'btn btn-success pull-right']) !!}
                                    </div>
                                </div>

                            {!! Form::close() !!}
                        @endif

                    @endif

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endif