<table class="table table-bordered table-striped table-condensed">
    <thead>
        <tr>
            <th colspan='2'>Concepto</th>
            <th>Importe</th>
        </tr>
    </thead>
    <tbody>

        <tr>
            <td colspan='2'>
                Curso {{ $ficha->curso->es_convocatoria_multi?$ficha->convocatoria->name:$ficha->curso->name}} :
                {{$ficha->convocatoria?$ficha->convocatoria->name:"-"}}
                del {{$ficha->curso_start_date}} al {{$ficha->curso_end_date}}
            </td>
            <td align="right">
                {{ConfigHelper::parseMoneda($ficha->course_total_amount, $ficha->curso_moneda)}}
            </td>
        </tr>

        <tr>
            <td colspan='2'>
            Alojamiento {{$ficha->alojamiento?$ficha->alojamiento->name:"-"}}
            del {{Carbon::parse($ficha->accommodation_start_date)->format('d/m/Y')}} al {{Carbon::parse($ficha->accommodation_end_date)->format('d/m/Y')}}
            </td>
            <td align='right'>
                {{ ConfigHelper::parseMoneda($ficha->accommodation_total_amount, $ficha->alojamiento_moneda) }}
            </td>
        </tr>

        @if($ficha->extras->count())

            <tr><th colspan="3">Extras</th></tr>

            @foreach($ficha->extrasCurso as $extra)
                @include('includes.bookings_tr_extra', ['extra'=> $extra])
            @endforeach

            @foreach($ficha->extrasCentro as $extra)
                @include('includes.bookings_tr_extra', ['extra'=> $extra])
            @endforeach

            @foreach($ficha->extras_alojamiento as $extra)
                @include('includes.bookings_tr_extra', ['extra'=> $extra])
            @endforeach

            @foreach($ficha->extrasGenerico as $extra)
                @include('includes.bookings_tr_extra', ['extra'=> $extra])
            @endforeach

            @foreach($ficha->extrasCancelacion as $extra)
                @include('includes.bookings_tr_extra', ['extra'=> $extra])
            @endforeach

            @foreach($ficha->extrasOtros as $extra)
            <tr>
                <td>{{$extra->name}} [{{$extra->notas}}]</td>
                <td>
                    <i>{{ ConfigHelper::parseMoneda($extra->precio, $extra->moneda_name) }}</i>
                    x{{$extra->unidades}}
                </td>
                <td align="right">{{ ConfigHelper::parseMoneda(($extra->precio * $extra->unidades), $extra->moneda_name) }}</td>
            </tr>
            @endforeach

        @endif

        <tr>
            <td colspan='2'>SubTotal</td>
            <td align="right">
                @foreach($ficha->precio_total['subtotal_txt'] as $subtotal)
                    {{$subtotal}}<br>
                @endforeach
            </td>
        </tr>

        @if($ficha->divisa_variacion)
        <tr>
            <td colspan='2'>Variación Divisa</td>
            <td align="right">{{ConfigHelper::parseMoneda($ficha->divisa_variacion)}}</td>
        </tr>
        @endif

        @if($ficha->precio_total['descuento_especial'] > 0)
        <tr>
            <td colspan='2'>Descuento especial:</td>
            <td align="right">{{$ficha->precio_total['descuento_especial_txt']}}</td>
        </tr>
        @endif

        @if($ficha->descuentos->count()>0)
        <tr>
            <th colspan="3">Descuentos</th></tr>
            @foreach($ficha->descuentos as $descuento)
            <tr>
                <td>{{$descuento->notas}}</td>
                <td colspan='2' align="right">{{ ConfigHelper::parseMoneda($descuento->importe, $descuento->moneda->name) }}</td>
            </tr>
            @endforeach
        @endif

        <tr>
            <th colspan='2'>
                TOTAL
            </th>
            <td align="right"><strong>{{$ficha->precio_total['total_txt']}}</strong></td>
        </tr>

        <tr>
            <td colspan='3' align="right">
                <h4><strong>
                    {{ ConfigHelper::parseMoneda(($ficha->precio_total['total']-$ficha->pagos->sum('importe')), Session::get('vcn.moneda')) }}
                </strong></h4>
            </td>
        </tr>

        @if(!$ficha->es_cancelado && !$ficha->es_refund)
        <tr>
            <td colspan="3">
                Cambio aplicado:<br>
                @foreach($ficha->monedas_usadas_txt as $mu)
                    <small>{{$mu}}</small><br>
                @endforeach
            </td>
        </tr>
        @endif



    </tbody>
</table>