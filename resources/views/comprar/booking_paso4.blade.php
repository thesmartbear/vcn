<div id="booking-paso4">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-success"><i class="fa fa-book fa-fw"></i> Booking: {{$ficha->id}}</h3>
            <h4><i class="fa fa-user fa-fw"></i> @lang('area.Viajero'): {{$ficha->viajero->full_name}}</h4>
        </div>
    </div>

    <div class="row margintop60">
        <div class="col-md-12">

            {{-- <h4>Viajero: {{$ficha->viajero->full_name}}</h4>
            <hr> --}}

            @include('comprar.booking_total_desglose')

            {{-- <hr>
            @include('comprar.booking_total', ['ficha'=> $booking]) --}}

            @if(!$booking->tpv_tiempo_agotado)
                @if($booking->es_corporativo_pago)

                    <hr>
                    <div class="note note-success">
                        <p>
                            {!! __('area.booking.pago_importe', ['importe'=> ConfigHelper::parseMoneda($booking->online_reserva)])  !!}
                        </p>
                    </div>

                    <h2 class="booking-title">{{trans('area.booking.datos_formapago')}}</h2>
                    <br>
                    @include("comprar.booking_paso4_pago", ['ficha'=> $booking])

                @endif
            @endif

            {{-- CONDICIONES --}}
            <h2 class="booking-title">@lang('area.booking.datos_condiciones')</h2>
            <div class="form-group booking-condiciones">
                {!! $ficha->txt_condiciones !!}
            </div>
            <p>
            {{-- El/Los tutores han leído y aceptado las {!! $condiciones !!} --}}
            @include('includes.form_checkbox', [ 'campo'=> 'condiciones', 'texto'=> trans('area.booking.acepto'), 'required'=> true])
            *@lang('area.booking.acepto2')
            </p>

            @if(ConfigHelper::config('es_rgpd'))
            {{-- LOPD --}}
            <div class="form-group">
                @include('includes.form_checkbox', [ 'campo'=> 'lopd_check1', 'texto'=> trans('area.booking.lopd_check1', ['responsable'=> ConfigHelper::config('name')]), 'required'=> true, 'html'=> true])
            </div>

            <div class="form-group">
                @include('includes.form_checkbox', [ 'campo'=> 'lopd_check2', 'texto'=> trans('area.booking.lopd_check2', ['plataforma'=> $booking->plataforma_name]), 'html'=> true])
            </div>

            <div class="form-group">
                @include('includes.form_checkbox', [ 'campo'=> 'lopd_check3', 'texto'=> trans('area.booking.lopd_check3'), 'html'=> true])
            </div>
            
            <hr>
            @endif

            @if( $booking->es_corporativo_overbooking )
                <div class="note note-warning">
                    @lang('area.booking.corporativo_no_plazas')
                </div>
            @endif
            
            <div class="row margintop20">
                <div class="col-sm-2">
                    <a href="{{route('area.comprar.cancelar', $booking->id)}}" class="btn btn-danger">@lang('area.booking.cancelar')</a>
                </div>
                <div class="col-sm-10">
                    {!! Form::hidden('forma_pago', 'x', array('id'=> 'forma_pago')) !!}
                    <button type="submit" id="btn-pagar-ok" data-label="{{trans('area.pagar')}}" class="btn btn-success siguiente"><i class="fa fa-credit-card"></i> {{trans('area.pagar')}}</button>
                </div>
            </div>


        </div>
    </div>

</div>
