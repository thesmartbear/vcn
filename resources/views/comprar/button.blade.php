@if(ConfigHelper::es_booking_online())

    <?php

        $href = "#bookingModal";
        if(auth()->user())
        {
            $href = route('area.comprar.compra', [$curso->id, 0] );
            if(isset($convo_id))
            {
                $href = route('area.comprar.compra', [$curso->id, ($convo_id ?: 0)] );
            }
        }
        
        if(!$curso->hay_plazas)
        {
            $href = "#";
        }

    ?>

    @if(auth()->user())
        {{-- @if(auth()->user()->compra_en_curso)
            <a id="comprar-error" href="#" class="btn btn-primary plusinfolink pibtn btn-curso hidden-print">{!! trans('web.curso.comprar') !!}</a>
        @else
            <a href="{{route('area.comprar.compra', $curso->id)}}" class="btn btn-primary plusinfolink pibtn hidden-print">{!! trans('web.curso.comprar') !!}</a>
        @endif --}}

        @if( $href == "#")
            <a data-label="<h4>{{trans('area.booking.no_plazas')}}</h4>" data-html="true" readonly href="{{$href}}" class="btn btn-primary plusinfolink pibtn hidden-print">{!! trans('web.curso.comprar') !!}</a>
        @else
            <a href="{{$href}}" class="btn btn-primary plusinfolink pibtn hidden-print">{!! trans('web.curso.comprar') !!}</a>
        @endif

        @if(auth()->user()->esDeseo($curso->id))
            <span class="btn btn-success hidden-print" disabled="disabled"><i class='fa fa-star'></i></span>
        @elseif(!isset($deseoOff))
            <a href="{{route('area.deseos', $curso->id)}}" class="btn btn-primary plusinfolink pibtn hidden-print">{!! trans('web.curso.deseo') !!}</a>
        @endif
    
    @else

        @if( $href == "#")
            <a data-label="<h4>{{trans('area.booking.no_plazas')}}</h4>" data-html="true" readonly href="{{$href}}" class="btn btn-primary plusinfolink pibtn hidden-print">{!! trans('web.curso.comprar') !!}</a>
        @else
            
            <a href="{{$href}}" class="btn btn-primary plusinfolink pibtn btn-curso hidden-print" data-toggle="modal" data-target="{{$href}}">{!! trans('web.curso.comprar') !!}</a>
            
        @endif

        @if(!isset($deseoOff))
            <a id="booking-deseo" href="#." class="btn btn-primary plusinfolink pibtn btn-curso hidden-print">{!! trans('web.curso.deseo') !!}</a>
        @endif
    @endif

    @if( Session::get('mensaje') )

        <div class="note note-success">
            <p>
                {!! Session::get('mensaje') !!}
            </p>
        </div>

    @endif

<br><br>
@endif