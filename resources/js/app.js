
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.moment = require('moment');

window.Vue = require('vue');
window.VueTimeago = require('vue-timeago');
Vue.use(window.VueTimeago, {
    locale: 'es-ES',
    locales: { 'es-ES': require('vue-timeago/locales/es-ES.json') }
  })
  
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('notification', require('./components/Notification.vue').default);
Vue.component('notifications-button', require('./components/NotificationsButton.vue').default);
Vue.component('notifications-dropdown', require('./components/NotificationsDropdown.vue').default);

// Vue.component('chat-mensaje', require('./components/ChatMensaje.vue').default);

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

// window.onload = function () {
//     const app = new Vue({
//         el: '#appPusher'
//     });
// }

if(window.Laravel && window.AppPusher)
{
  const app = new Vue({
    el: '#appPusher'
  });
}