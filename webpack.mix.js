let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


var siteName = 'vcn'; // set your siteName here
var userName = 'carlituxman';

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .version()
   .browserSync({
        proxy: 'https://' + siteName + '.loc',
        host: siteName + '.loc',
        open: 'ui',
        port: 8000,
        files: ['resources/views/**/*.php', 'app/**/*.php', 'routes/**/*.php', 'public/js/*.js', 'public/css/*.css'],
        https: {
            key:
                '/Users/' +
                userName +
                '/.config/valet/Certificates/' +
                siteName +
                '.loc.key',
            cert:
                '/Users/' +
                userName +
                '/.config/valet/Certificates/' +
                siteName +
                '.loc.crt'
        }
   });