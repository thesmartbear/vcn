<?php

use VCN\Helpers\ExternalApiHelper;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
|
*/

Route::group(['prefix' => 'auth'], function(){
    Auth::routes();
    //Auth::routes(['verify' => true]);
});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('auth');
//Resources?

//Area : archivos
Route::get('/files/viajeros/{viajero_id}/{file?}', 'Manage\ManageController@serveFileViajeros')->middleware('auth.area');
Route::get('/files/bookings/{viajero_id}/{file?}',  ['as' =>'file.booking', 'uses' => 'Manage\ManageController@serveFileBookings'] )->middleware('auth.area');
Route::get('/files/viajeros/{viajero_id}/{booking_id}/{file?}', 'Manage\ManageController@serveFileViajerosBooking')->middleware('auth.area');
Route::get('/files/convocatorias/facturas/{file?}', 'Manage\ManageController@serveFileFacturas')->middleware('auth.area');
Route::get('/manage/cursos/pocket-guide-pdf/{id}/{lang}', ['as' =>'manage.cursos.pocket-guide', 'uses' => 'Manage\Cursos\CursosController@getPocketGuidePdf'])->middleware('auth.area');

Route::get('/files/firmar/{viajero_id}/{file?}',  ['as' =>'file.firma', 'uses' => 'Manage\ManageController@serveFileFirma'] )->middleware('auth.area');

//chat
// Route::get('/chat/test/{chatId?}', 'Web\ChatController@test')->name('web.chat.test');
// Route::post('/chat/create', 'Web\ChatController@create')->name('web.chat.create');
// Route::post('/chat/status', 'Web\ChatController@checkStatus')->name('web.chat.status');
// Route::post('/chat/mensaje', 'Web\ChatController@postMensaje')->name('web.chat.mensaje');
// Route::post('/chat/formulario', 'Web\ChatController@postFormulario')->name('web.chat.formulario');

Route::get('/ac/tests', function(){
    return ExternalApiHelper::AC_contactsAdd();
});

/*
|--------------------------------------------------------------------------
| Ejemplo Routes Translate
|--------------------------------------------------------------------------
 */


// Route::get('/lang/{lang?}', ['as' => 'cambio.idioma', 'uses' => 'Web\WebController@cambioIdioma']);

Route::get('/iccic/cataleg.html', function(){
    return Redirect::to('cataleg.html');
});
// Route::get('{slug?}.php', ['as' =>'web.oldweb', 'uses' => 'Web\WebController@oldWeb']);
// Route::get('/translations', '\Barryvdh\TranslationManager\Controller@getIndex');
// Route::get('/translations/view/{group?}', '\Barryvdh\TranslationManager\Controller@getView');

Route::get('/ajax/{curso_id}', ['as' =>'ajax.info', 'uses' => 'Web\WebController@ajaxInfo']);

Route::post('/register', ['as' =>'web.register', 'uses' => 'Web\WebController@postRegister']);

Route::get('/info/{curso_id}', ['as' =>'web.proveedor', 'uses' => 'Web\WebController@getProveedor']);

Route::localizedGroup(function () {

    Route::get('/test-vue', ['as' =>'web.buscar.vue', 'uses' => 'Web\WebController@buscarByPaisVue']);
    Route::get('/test-vue-api', ['as' =>'web.buscar.vue.api', 'uses' => 'Web\WebController@buscarByPaisVueApi']);

    Route::transGet('web.catalogo-slug', ['as' =>'web.catalogo', 'uses' => 'Web\WebController@getCatalogo']);
    Route::transGet('web.contacto-slug', ['as' =>'web.contacto', 'uses' => 'Web\WebController@getContacto']);
    Route::transGet('web.inscripcion-slug', ['as' =>'web.inscripcion', 'uses' => 'Web\WebController@getInscripcion']);

    Route::post('web.catalogo-info', ['as' =>'web.catalogo-info', 'uses' => 'Web\WebController@postCatalogoInfo']);
    Route::post('web.curso-info', ['as' =>'web.curso-info', 'uses' => 'Web\WebController@postCursoInfo']);
    Route::post('web.contacto', ['as' =>'web.contacto', 'uses' => 'Web\WebController@postContacto']);
    Route::post('web.informacion', ['as' =>'web.informacion', 'uses' => 'Web\WebController@postInformacion']);

    Route::get('/landing/{slug}', ['as' =>'web.landing', 'uses'=> 'Web\WebController@getLanding']);
    Route::get('/landing/{slug}/catalogo', ['as' =>'web.landing.catalogo', 'uses'=> 'Web\WebController@getLandingCatalogo']);
    Route::post('/landing/{landing}', ['as' =>'web.landing.post', 'uses'=> 'Web\WebController@postLanding']);

    Route::get('/catalogos/{pdf?}', ['as' =>'web.pdf', 'uses' => 'Web\WebController@pdf']);
    Route::get('/translations', '\Barryvdh\TranslationManager\Controller@getIndex');
    Route::get('/translations/view/{group?}', '\Barryvdh\TranslationManager\Controller@getView');
    Route::get('/', ['as' =>'web.index', 'uses'=> 'Web\WebController@index']);

    Route::transPost('routes.buscar', ['as' =>'web.buscar.pais', 'uses' => 'Web\WebController@buscarByPais']);

    Route::transPost('web.buscar-slug', ['as' =>'web.buscar', 'uses' => 'Web\WebController@buscar']);
    Route::transGet('web.buscar-slug', ['as' =>'web.buscar', 'uses' => 'Web\WebController@buscar']);
    Route::transGet('web.cursos-en-promocion', ['as' =>'web.promoscursos', 'uses' => 'Web\WebController@promoscursos']);
    Route::get('{page?}.html', ['as' =>'web.pagina', 'uses' => 'Web\WebController@pagina']);

    // /pais/{pais?}
    // Route::get('/pais/{pais?}', ['as' =>'web.pais', 'uses' => 'Web\WebController@getPais']);
    // /pais/{pais?}/{course_slug?}.html
    // Route::transGet('routes.pais_curso', ['as' =>'web.pais-curso', 'uses' => 'Web\WebController@getPaisCurso']);

    //Estas rutas son configurables por plataforma: para categorias, especialidades y pais:
    // Route::get('/{cat?}/{course_slug?}.html', ['as' =>'web.curso', 'uses' => 'Web\WebController@curso']);
    // Route::get('/{cat?}/getpdf/{id?}', ['as' =>'web.getPdfcurso', 'uses' => 'Web\WebController@getPdfcurso']);
    // Route::get('/{cat?}', ['as' =>'web.categoria', 'uses' => 'Web\WebController@categoria']);
    // Route::get('/{cat?}/{subcat?}', ['as' =>'web.subcategoria', 'uses' => 'Web\WebController@subcategoria']);
    // Route::get('/{cat?}/{subcat?}/{subcatdet?}', ['as' =>'web.subcategoria', 'uses' => 'Web\WebController@subcategoriadetalle']);

    //Estructura nueva (e2): ejemplo: http://viajaconnosotros.dev/nueva/aprender-ingles-en-el-extranjero/jovenes/grupos-con-monitor/carlingford-camp.html
    Route::get('/wn/{query?}', ['as' =>'web.wn', 'uses' => 'Web\WebController@getCategoriaWeb'])->where('query','.+');

    $v = ConfigHelper::config('web_estructura');
    if( $v == 2)
    {
        
    }
    elseif( $v == 1)
    {

    }

    //e2: Todas estas desaparacen:
    Route::get('/{param1?}/{slug?}.html', ['as' =>'web.curso', 'uses' => 'Web\WebController@raizCurso', 'middleware'  => [ 'web','visitas' ] ]);
    Route::get('/{param1?}', ['as' =>'web.raiz', 'uses' => 'Web\WebController@raiz']);
    Route::get('/{param1?}/{param2?}', ['as' =>'web.raiz.sub', 'uses' => 'Web\WebController@raizSub']);

    Route::get('/{param1?}/getpdf/{id?}', ['as' =>'web.getPdfcurso', 'uses' => 'Web\WebController@raizPdfcurso']);
    Route::get('/{param1?}/{param2?}/{param3?}', ['as' =>'web.raiz.det', 'uses' => 'Web\WebController@raizSubDet']);


    Route::transGet('routes.view', [
        'as' => 'web.view',
        'uses' => 'Web\WebController@view',
    ]);




    // Route::transGet('routes.about', function () {
    //     // return view('about');
    //     return trans('messages.about');
    // });
    // Route::transGet('routes.view', function ($id) {
    //     dd($id);
    //     return view('page', compact('id'));
    // });



});

// Route::get('/v/{id?}', ['as' =>'web.view', 'uses' => 'Web\WebController@view']);