<?php

// Manage
Breadcrumbs::register('manage.index', function($breadcrumbs)
{
    $breadcrumbs->push('Manage', route('manage.index'));
});

Breadcrumbs::register('manage.index.misdatos', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Mis Datos', route('manage.index.misdatos'));
});
// Manage > Resumen
Breadcrumbs::register('manage.index.resumen', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Resumen', route('manage.index.resumen'));
});
Breadcrumbs::register('manage.index.ventas', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Ventas', route('manage.index.ventas'));
});

// Manage > Informes
Breadcrumbs::register('manage.informes', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Informes', route('manage.index'));
});
Breadcrumbs::register('manage.informes.booking-grupos', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Booking Grupos', route('manage.informes.booking-grupos'));
});
Breadcrumbs::register('manage.informes.pagos', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Pagos', route('manage.informes.pagos'));
});
Breadcrumbs::register('manage.informes.menores12', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Menores 12', route('manage.informes.menores12'));
});
Breadcrumbs::register('manage.informes.seguros', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Seguros', route('manage.informes.seguros'));
});
Breadcrumbs::register('manage.informes.avisos', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Avisos', route('manage.informes.avisos.reserva'));
});
Breadcrumbs::register('manage.informes.ventas', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Ventas', route('manage.informes.ventas'));
});
Breadcrumbs::register('manage.informes.facturacion', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Facturación > Listado Facturación', route('manage.informes.facturacion'));
});
Breadcrumbs::register('manage.informes.facturacion-precios', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Facturación > Control Precios', route('manage.informes.facturacion-precios'));
});
Breadcrumbs::register('manage.informes.plazas-vuelos', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Plazas > Plazas Vuelos', route('manage.informes.plazas-vuelos'));
});
Breadcrumbs::register('manage.informes.plazas-alojamientos', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Plazas > Plazas Alojamientos', route('manage.informes.plazas-alojamientos'));
});
Breadcrumbs::register('manage.informes.reunion', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Info Reunión', route('manage.informes.reunion'));
});
Breadcrumbs::register('manage.informes.escuelas', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Escuela y Academias', route('manage.informes.escuelas'));
});
Breadcrumbs::register('manage.informes.aportes', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Aportes', route('manage.informes.aportes'));
});
Breadcrumbs::register('manage.informes.vuelos-asignacion', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push('Asignación Vuelos', route('manage.informes.vuelos-asignacion'));
});
Breadcrumbs::register('manage.informes.ruta', function($breadcrumbs, $nombre, $ruta)
{
    $breadcrumbs->parent('manage.informes');
    $breadcrumbs->push($nombre, route($ruta));
});


// Manage > Bookings
Breadcrumbs::register('manage.bookings.nuevo', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.bookings.index');
    // $breadcrumbs->push('Booking Nuevo', route('manage.bookings.nuevo'));
});
// Manage > Inscripciones (Bookings)
Breadcrumbs::register('manage.bookings.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Inscripciones', route('manage.bookings.index'));
});
Breadcrumbs::register('manage.bookings.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.bookings.index');
    $breadcrumbs->push("Booking : ". $ficha->viajero->full_name ." : $ficha->fecha", route('manage.bookings.ficha', $ficha));
});
Breadcrumbs::register('manage.bookings.incidencias.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.bookings.ficha',$ficha->booking);
    $breadcrumbs->push('Incidencia', route('manage.bookings.incidencias.ficha',$ficha->id));
});
Breadcrumbs::register('manage.bookings.incidencias.tareas.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.bookings.incidencias.ficha',$ficha->incidencia);
    $breadcrumbs->push('Tarea');
});

// Manage > CMS
Breadcrumbs::register('manage.cms.paginas.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('CMS Páginas', route('manage.cms.paginas.index'));
});
Breadcrumbs::register('manage.cms.categorias.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('CMS Categorías Web', route('manage.cms.categorias.index'));
});
Breadcrumbs::register('manage.cms.promos.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('CMS Promos', route('manage.cms.promos.index'));
});
Breadcrumbs::register('manage.cms.especialidades.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('CMS Especialidades Web', route('manage.cms.especialidades.index'));
});

// Manage > Viajeros
Breadcrumbs::register('manage.viajeros.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Viajeros', route('manage.viajeros.index'));
});
Breadcrumbs::register('manage.viajeros.buscar', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.viajeros.index');
    $breadcrumbs->push('Búsqueda', route('manage.viajeros.buscar'));
});
Breadcrumbs::register('manage.viajeros.nuevo', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.viajeros.index');
    $breadcrumbs->push('Nuevo', route('manage.viajeros.nuevo'));
});
Breadcrumbs::register('manage.viajeros.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.viajeros.index');
    $breadcrumbs->push($ficha->full_name, route('manage.viajeros.ficha', $ficha->id));
});
Breadcrumbs::register('manage.viajeros.ficha.datos', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.viajeros.ficha',$ficha);
    $breadcrumbs->push('Otros Datos', route('manage.viajeros.ficha.datos', $ficha->id));
});

// Manage > Tutores
Breadcrumbs::register('manage.tutores.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Tutores', route('manage.tutores.index'));
});

// Manage > Inscritos
Breadcrumbs::register('manage.inscritos.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Instritos', route('manage.inscritos.index'));
});

// Manage > Solicitudes
Breadcrumbs::register('manage.solicitudes.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Solicitudes', route('manage.solicitudes.index'));
});

// Manage > Proveedores
Breadcrumbs::register('manage.proveedores.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Proveedores', route('manage.proveedores.index'));
});
Breadcrumbs::register('manage.proveedores.nuevo', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.proveedores.index');
    $breadcrumbs->push('Nuevo', route('manage.proveedores.nuevo'));
});
Breadcrumbs::register('manage.proveedores.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.proveedores.index');
    $breadcrumbs->push($ficha->name, route('manage.proveedores.ficha', $ficha->id));
});

// Manage > Centros
Breadcrumbs::register('manage.centros.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Centros', route('manage.centros.index'));
});
Breadcrumbs::register('manage.centros.nuevo', function($breadcrumbs, $proveedor)
{
    if($proveedor)
    {
        $breadcrumbs->parent('manage.proveedores.index');
        $breadcrumbs->push( $proveedor->name, route('manage.proveedores.ficha',$proveedor->id));
        $breadcrumbs->push('Nuevo Centro', route('manage.centros.nuevo'));
    }
    else
    {
        $breadcrumbs->parent('manage.centros.index');
        $breadcrumbs->push('Nuevo', route('manage.centros.nuevo'));
    }

});
Breadcrumbs::register('manage.centros.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.proveedores.ficha',$ficha->proveedor);
    $breadcrumbs->push("Centro: ". $ficha->name, route('manage.centros.ficha', $ficha->id));
});
Breadcrumbs::register('manage.centros.familias.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.centros.index');
    $breadcrumbs->push("Centro: ". $ficha->centro->name, route('manage.centros.ficha', $ficha->centro->id));
    $breadcrumbs->push("Familia: ". $ficha->name, route('manage.centros.familias.ficha', $ficha->id));
});

Breadcrumbs::register('manage.centros.schools.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.centros.index');
    $breadcrumbs->push("Centro: ". $ficha->centro->name, route('manage.centros.ficha', $ficha->centro->id));
    $breadcrumbs->push("School: ". $ficha->name, route('manage.centros.schools.ficha', $ficha->id));
});

// Manage > Alojamientos
Breadcrumbs::register('manage.alojamientos.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Alojamientos', route('manage.alojamientos.index'));
});
Breadcrumbs::register('manage.alojamientos.nuevo', function($breadcrumbs, $centro)
{
    if($centro)
    {
        $breadcrumbs->parent('manage.centros.index');
        $breadcrumbs->push($centro->name, route('manage.centros.ficha',$centro->id));
        $breadcrumbs->push('Nuevo Alojamiento', route('manage.alojamientos.nuevo'));
    }
    else
    {
        $breadcrumbs->parent('manage.alojamientos.index');
        $breadcrumbs->push('Nuevo', route('manage.alojamientos.nuevo'));
    }
});
Breadcrumbs::register('manage.alojamientos.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.centros.ficha',$ficha->centro);
    $breadcrumbs->push("Alojamiento: ". $ficha->name, route('manage.alojamientos.ficha', $ficha->id));
});

// Manage > Convocatorias Cerradas
Breadcrumbs::register('manage.convocatorias.cerradas.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Convocatorias Cerradas', route('manage.convocatorias.cerradas.index'));
});
Breadcrumbs::register('manage.convocatorias.cerradas.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.convocatorias.cerradas.index');
    $breadcrumbs->push($ficha->name, route('manage.convocatorias.cerradas.index'));
});

// Manage > Convocatorias Abiertas
Breadcrumbs::register('manage.convocatorias.abiertas.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Convocatorias Abiertas', route('manage.convocatorias.abiertas.index'));
});
Breadcrumbs::register('manage.convocatorias.abiertas.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.convocatorias.abiertas.index');
    $breadcrumbs->push($ficha->name, route('manage.convocatorias.abiertas.index'));
});

// Manage > Vuelos
Breadcrumbs::register('manage.convocatorias.vuelos.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Vuelos', route('manage.convocatorias.vuelos.index'));
});
Breadcrumbs::register('manage.convocatorias.vuelos.ficha', function($breadcrumbs, $ficha)
{
    // $breadcrumbs->parent('manage.convocatorias.cerradas.ficha',$ficha->convocatoria);
    $breadcrumbs->parent('manage.convocatorias.vuelos.index');
    $breadcrumbs->push("Vuelo: ". $ficha->name, route('manage.convocatorias.vuelos.ficha', $ficha->id));
});

// Manage > Cursos
Breadcrumbs::register('manage.cursos.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Cursos', route('manage.cursos.index'));
});
Breadcrumbs::register('manage.cursos.nuevo', function($breadcrumbs, $centro)
{
    if($centro)
    {
        $breadcrumbs->parent('manage.centros.index');
        $breadcrumbs->push( $centro->name, route('manage.centros.ficha',$centro->id));
        $breadcrumbs->push('Nuevo Curso', route('manage.cursos.nuevo'));
    }
    else
    {
        $breadcrumbs->parent('manage.cursos.index');
        $breadcrumbs->push('Nuevo', route('manage.cursos.nuevo'));
    }
});
Breadcrumbs::register('manage.cursos.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.centros.ficha',$ficha->centro);
    $breadcrumbs->push("Curso: ". $ficha->name, route('manage.cursos.ficha', $ficha->id));
});

Breadcrumbs::register('manage.categorias.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push("Categoría: ". $ficha->name, route('manage.categorias.ficha', $ficha->id));
});

Breadcrumbs::register('manage.subcategorias.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.categorias.ficha',$ficha->categoria);
    $breadcrumbs->push("SubCategoría: ". $ficha->name, route('manage.subcategorias.ficha', $ficha->id));
});

Breadcrumbs::register('manage.chat.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Chats', route('manage.chat.index'));
});

Breadcrumbs::register('manage.chat.agente', function($breadcrumbs, $chatId)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Chats', route('manage.chat.index'));
    $breadcrumbs->push('Chat', route('manage.chat.agente', $chatId));
});

// Manage > system
Breadcrumbs::register('manage.system.admins.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Administradores', route('manage.system.admins.index'));
});
Breadcrumbs::register('manage.system.admins.nuevo', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.system.admins.index');
    $breadcrumbs->push('Nuevo');
});
Breadcrumbs::register('manage.system.admins.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.system.admins.index');
    $breadcrumbs->push($ficha->full_name);
});

Breadcrumbs::register('manage.system.documentos.modelo', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent($ficha->modelo_route, $ficha->modelo_clase);
});
Breadcrumbs::register('manage.system.documentos.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.system.documentos.modelo', $ficha);
    $breadcrumbs->push($ficha->name);
});

Breadcrumbs::register('manage.system.avisos.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Avisos', route('manage.system.avisos.index'));
});
Breadcrumbs::register('manage.system.avisos.nuevo', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.system.avisos.index');
    $breadcrumbs->push('Nuevo', route('manage.system.admins.nuevo'));
});
Breadcrumbs::register('manage.system.avisos.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.system.avisos.index');
    $breadcrumbs->push($ficha->name, route('manage.system.avisos.index'));
});

Breadcrumbs::register('manage.system.avisos.docs.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Docs Avisos', route('manage.system.avisos.docs.index'));
});
Breadcrumbs::register('manage.system.avisos.docs.nuevo', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.system.avisos.docs.index');
    $breadcrumbs->push('Nuevo', route('manage.system.admins.nuevo'));
});
Breadcrumbs::register('manage.system.avisos.docs.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.system.avisos.docs.index');
    $breadcrumbs->push($ficha->name, route('manage.system.avisos.docs.index'));
});

Breadcrumbs::register('manage.system.roles.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Roles', route('manage.system.roles.index'));
});
Breadcrumbs::register('manage.system.roles.nuevo', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.system.roles.index');
    $breadcrumbs->push('Nuevo', route('manage.system.roles.nuevo'));
});
Breadcrumbs::register('manage.system.roles.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.system.roles.index');
    $breadcrumbs->push($ficha->name, route('manage.system.roles.index'));
});
Breadcrumbs::register('manage.system.plataformas.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Plataformas', route('manage.system.plataformas.index'));
});
Breadcrumbs::register('manage.system.plataformas.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.system.plataformas.index');
    $breadcrumbs->push($ficha->name, route('manage.system.plataformas.index'));
});

Breadcrumbs::register('manage.system.campos.index', function($breadcrumbs)
{
    $breadcrumbs->parent('manage.index');
    $breadcrumbs->push('Campos', route('manage.system.campos.index'));
});
Breadcrumbs::register('manage.system.campos.ficha', function($breadcrumbs, $ficha)
{
    $breadcrumbs->parent('manage.system.campos.index');
    $breadcrumbs->push(($ficha->full_name ?: $ficha->name), route('manage.system.campos.index'));
});