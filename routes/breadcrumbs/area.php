<?php

// Area
Breadcrumbs::register('area.index', function($breadcrumbs)
{
    if(Session::get('vcn.manage.area.home'))
    {
        $breadcrumbs->push(trans('area.areaclientes'), Session::get('vcn.manage.area.home'));
    }
    else
    {
        $breadcrumbs->push(trans('area.areaclientes'), route('area.index'));
    }
});
Breadcrumbs::register('area.booking', function($breadcrumbs, $id)
{
    $breadcrumbs->parent('area.index');

    $ruta = Session::get('vcn.manage.area.home','/area');
    $breadcrumbs->push("Booking $id", $ruta);
});
Breadcrumbs::register('area.booking.familia', function($breadcrumbs, $booking)
{
    $breadcrumbs->parent('area.booking', $booking->id);
    $breadcrumbs->push('Familia');
});
Breadcrumbs::register('area.booking.school', function($breadcrumbs, $booking)
{
    $breadcrumbs->parent('area.booking', $booking->id);
    $breadcrumbs->push(trans('area.escuela'), route('area.booking', $booking->id));
});

Breadcrumbs::register('area.deseos', function($breadcrumbs)
{
    $breadcrumbs->parent('area.index');

    $ruta = Session::get('vcn.manage.area.home','/area');
    $breadcrumbs->push(__("area.Mis Favoritos"), $ruta);
});

Breadcrumbs::register('area.exams', function($breadcrumbs)
{
    $breadcrumbs->parent('area.index');

    $ruta = Session::get('vcn.manage.area.home','/area');
    $breadcrumbs->push(__("area.Examenes"), $ruta);
});

Breadcrumbs::register('area.comprar', function($breadcrumbs, $id)
{
    $breadcrumbs->parent('area.index');

    $ruta = Session::get('vcn.manage.area.home','/area');
    $breadcrumbs->push("Booking Online $id", $ruta);
});
Breadcrumbs::register('area.comprar.cursos', function($breadcrumbs)
{
    $breadcrumbs->parent('area.index');

    $ruta = Session::get('vcn.manage.area.home','/area');
    $breadcrumbs->push("Cursos", $ruta);
});
Breadcrumbs::register('area.booking.pocketguide', function($breadcrumbs, $booking)
{
    $breadcrumbs->parent('area.booking', $booking->id);
    $breadcrumbs->push('Pocket Guide', route('area.booking', $booking->id));
});