<?php

/*
|--------------------------------------------------------------------------
| General Routes
|--------------------------------------------------------------------------
|
|
*/

// Authentication routes...
// Route::get('auth/login', 'Auth\AuthController@getLogin');
// Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
// Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);
// Route::get('auth/register', ['as' =>'auth/register', 'uses' => 'Auth\AuthController@getRegister']);
// Route::post('auth/register', ['as' =>'auth/register', 'uses' => 'Auth\AuthController@postRegister']);

// // Password reset link request routes...
// Route::get('auth/password/email', ['as' =>'auth/password', 'uses' => 'Auth\PasswordController@getEmail']);
// Route::post('auth/password/email', ['as' =>'auth/password.post', 'uses' => 'Auth\PasswordController@postEmail']);

// Password reset routes...
// Route::get('auth/password/reset/{token}', ['as' =>'auth/password.reset', 'uses' => 'Auth\PasswordController@getReset']);
// Route::post('auth/password/reset/{token}', ['as' =>'auth/password.reset.post', 'uses' => 'Auth\PasswordController@postReset']);

// Notifications
// Route::post('notifications', 'NotificationController@store');
Route::get('notifications', 'NotificationController@index');
Route::patch('notifications/{id}/read', 'NotificationController@markAsRead');
Route::post('notifications/mark-all-read', 'NotificationController@markAllRead');
Route::post('notifications/{id}/dismiss', 'NotificationController@dismiss');
// Route::get('/notifications/list', 'NotificationController@getListado');
Route::get('/notifications/list', ['as' =>'notifications.list', 'uses' => 'NotificationController@getListado']);

// Push Subscriptions
Route::post('subscriptions', 'PushSubscriptionController@update');
Route::post('subscriptions/delete', 'PushSubscriptionController@destroy');

/*
|--------------------------------------------------------------------------
| Manage Routes
|--------------------------------------------------------------------------
|
|
*/

Route::get('/set-lopd/{tipo}/{id}', ['as' =>'lopd.set', 'uses' => 'Manage\ManageController@setLOPD']);

Route::group(
    [
      'namespace'   => 'Web',
      'prefix'  => 'manage/chat',
      'as'      => 'manage.chat.',
      // 'middleware'  => [ 'auth' ]
    ],
    function() {

        Route::get('/test/{chatId?}', 'ChatController@test')->name('test');
        Route::get('/index/{user_id?}', 'ChatController@getIndex')->name('index');
        Route::get('/{chatId}', 'ChatController@getAgente')->name('agente');
    
    });

Route::group(
  [
    'namespace'   => 'Web\CMS',
    'prefix'  => 'manage/cms',
    'as'      => 'manage.cms.',
    // 'middleware'  => [ 'auth' ]
  ],
  function() {

    Route::get('/paginas', ['as' =>'paginas.index', 'uses' => 'PaginaController@getIndex']);
    Route::get('/paginas/nuevo', ['as' =>'paginas.nuevo', 'uses' => 'PaginaController@getNuevo']);
    Route::get('/paginas/ficha/{id}', ['as' =>'paginas.ficha', 'uses' => 'PaginaController@getUpdate']);
    Route::post('/paginas/ficha/{id}', ['as' =>'paginas.ficha', 'uses' => 'PaginaController@postUpdate']);
    Route::get('/paginas/borrar/{id}', ['as' =>'paginas.delete', 'uses' => 'PaginaController@destroy']);

    Route::get('/catalogos', ['as' =>'catalogos.index', 'uses' => 'CatalogoController@getIndex']);
    Route::get('/catalogos/nuevo', ['as' =>'catalogos.nuevo', 'uses' => 'CatalogoController@getNuevo']);
    Route::get('/catalogos/ficha/{id}', ['as' =>'catalogos.ficha', 'uses' => 'CatalogoController@getUpdate']);
    Route::post('/catalogos/ficha/{id}', ['as' =>'catalogos.ficha', 'uses' => 'CatalogoController@postUpdate']);
    Route::get('/catalogos/borrar/{id}', ['as' =>'catalogos.delete', 'uses' => 'CatalogoController@destroy']);

    Route::get('/categorias', ['as' =>'categorias.index', 'uses' => 'CategoriasWebController@getIndex']);
    Route::get('/categorias/nuevo', ['as' =>'categorias.nuevo', 'uses' => 'CategoriasWebController@getNuevo']);
    Route::get('/categorias/ficha/{id}', ['as' =>'categorias.ficha', 'uses' => 'CategoriasWebController@getUpdate']);
    Route::post('/categorias/ficha/{id}', ['as' =>'categorias.ficha', 'uses' => 'CategoriasWebController@postUpdate']);
    Route::get('/categorias/borrar/{id}', ['as' =>'categorias.delete', 'uses' => 'CategoriasWebController@destroy']);

    Route::get('/categorias301', ['as' =>'categorias.index301', 'uses' => 'CategoriasWebController@getIndex301']);

    Route::get('/especialidades', ['as' =>'especialidades.index', 'uses' => 'EspecialidadesWebController@getIndex']);
    Route::get('/especialidades/nuevo', ['as' =>'especialidades.nuevo', 'uses' => 'EspecialidadesWebController@getNuevo']);
    Route::get('/especialidades/ficha/{id}', ['as' =>'especialidades.ficha', 'uses' => 'EspecialidadesWebController@getUpdate']);
    Route::post('/especialidades/ficha/{id}', ['as' =>'especialidades.ficha', 'uses' => 'EspecialidadesWebController@postUpdate']);
    Route::get('/especialidades/borrar/{id}', ['as' =>'especialidades.delete', 'uses' => 'EspecialidadesWebController@destroy']);

    Route::get('/promos', ['as' =>'promos.index', 'uses' => 'PromosController@getIndex']);
    Route::get('/promos/nuevo', ['as' =>'promos.nuevo', 'uses' => 'PromosController@getNuevo']);
    Route::get('/promos/ficha/{id}', ['as' =>'promos.ficha', 'uses' => 'PromosController@getUpdate']);
    Route::post('/promos/ficha/{id}', ['as' =>'promos.ficha', 'uses' => 'PromosController@postUpdate']);
    Route::get('/promos/borrar/{id}', ['as' =>'promos.delete', 'uses' => 'PromosController@destroy']);

    Route::group(
      [
        'prefix'  => 'landings',
        'as'      => 'landings.',
      ],
      function() {
      
        Route::get('/', ['as' =>'index', 'uses' => 'LandingController@getIndex']);
        Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'LandingController@getNuevo']);
        Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'LandingController@getUpdate']);
        Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'LandingController@postUpdate']);
        Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'LandingController@destroy']);

        Route::get('/{landing?}/informe', ['as' =>'informe', 'uses' => 'LandingController@getInforme']);
        Route::get('/informe/{form}', ['as' =>'informe.form', 'uses' => 'LandingController@getInformeForm']);

        Route::group(
          [
            // 'prefix'  => 'testimonios',
            // 'as'      => 'testimonios.',
          ],
          function() {
          
            Route::resource('testimonios', 'LandingTestimonioController')->except([
                'show','destroy'
            ]);//->parameters(['testimonios' => 'testimonio']);
            Route::get('/testimonios/{testimonio}/destroy', "LandingTestimonioController@destroy")->name('testimonios.destroy');
            
            Route::resource('destinos', 'LandingDestinoController')->except([
                'show','destroy'
            ]);//->parameters(['destinos' => 'destino']);
            Route::get('/destinos/{destino}/destroy', "LandingDestinoController@destroy")->name('destinos.destroy');

            Route::resource('programas', 'LandingProgramaController')->except([
                'show','destroy'
            ]);//->parameters(['programas' => 'programa']);
            Route::get('/programas/{testimonio}/destroy', "LandingProgramaController@destroy")->name('programas.destroy');
    
        });

    });

  });

Route::group(
    [
        'namespace'   => 'Manage',
        'prefix'      => 'manage',
        'as'          => 'manage.',
        // 'middleware'  => [ 'auth' ]
    ],

    function()
    {
      Route::get('/', ['as' =>'index', 'uses' => 'ManageController@getIndex']);
      
      Route::get('/test', ['as' =>'test', 'uses' => 'ManageController@test']);
      Route::post('/test', ['as' =>'test.post', 'uses' => 'ManageController@testPost']);

      Route::get('/resumen', ['as' =>'index.resumen', 'uses' => 'ManageController@getResumen']);
      Route::get('/ventas', ['as' =>'index.ventas', 'uses' => 'ManageController@getVentas']);
      Route::get('/contactos', ['as' =>'index.contactos', 'uses' => 'ManageController@getContactos']);

      Route::get('/mis-datos', ['as' =>'index.misdatos', 'uses' => 'ManageController@getDatos']);
      Route::post('/mis-datos', ['as' =>'index.misdatos', 'uses' => 'ManageController@postDatos']);
      
      Route::post('/traduccion/{idioma}/{modelo}/{id}', ['as' =>'traducciones.post', 'uses' => 'Traducciones\TraduccionesController@postUpdate']);
      Route::get('/autocomplete/{list?}', ['as' =>'autocomplete', 'uses'=> 'ManageController@getAutocomplete']);

      Route::get('/ajax/avisos/tareas', ['as' =>'ajax.avisos.tareas', 'uses' => 'ManageController@getAvisoTareas']);
      Route::post('/ajax/avisos/tareas', ['as' =>'ajax.avisos.tareas', 'uses' => 'ManageController@postAvisoTareas']);

      Route::group(
        [
          'namespace'   => 'System',
          'prefix'  => 'system',
          'as'      => 'system.',
        ],
        function() {

          Route::get('/admins', ['as' =>'admins.index', 'uses' => 'SystemController@getAdminsIndex']);
          Route::get('/admins/nuevo', ['as' =>'admins.nuevo', 'uses' => 'SystemController@getAdminsNuevo']);
          Route::get('/admins/ficha/{id}', ['as' =>'admins.ficha', 'uses' => 'SystemController@getAdminsUpdate']);
          Route::post('/admins/ficha/{id}', ['as' =>'admins.ficha', 'uses' => 'SystemController@postAdminsUpdate']);
          Route::get('/admins/borrar/{id}', ['as' =>'admins.delete', 'uses' => 'SystemController@destroyAdmin']);
          Route::get('/admins/ficha/{id}/actividad', ['as' =>'admins.actividad', 'uses' => 'SystemController@getAdminsActivity']);
          Route::get('/admins/login/{id}', ['as' =>'admins.login', 'uses' => 'SystemController@loginUser']);
          Route::get('/admins/logout', ['as' =>'admins.logout', 'uses' => 'SystemController@logoutUser']);

          Route::get('/contactos/{es_proveedor?}', ['as' =>'contactos.index', 'uses' => 'SystemController@getContactosIndex']);
          Route::get('/contactos/ficha/{id?}', ['as' =>'contactos.ficha', 'uses' => 'SystemController@getContactosUpdate']);
          Route::post('/contactos/ficha/{id}', ['as' =>'contactos.ficha', 'uses' => 'SystemController@postContactosUpdate']);
          Route::get('/contactos/borrar/{id}', ['as' =>'contactos.delete', 'uses' => 'SystemController@destroyContactos']);
          Route::post('/contactos/asignar', ['as' =>'contactos.asignar', 'uses' => 'SystemController@postContactosAsignar']);

          Route::get('/roles', ['as' =>'roles.index', 'uses' => 'SystemController@getRolesIndex']);
          Route::get('/roles/nuevo', ['as' =>'roles.nuevo', 'uses' => 'SystemController@getRolesNuevo']);
          Route::get('/roles/ficha/{id}', ['as' =>'roles.ficha', 'uses' => 'SystemController@getRolesUpdate']);
          Route::post('/roles/ficha/{id}', ['as' =>'roles.ficha', 'uses' => 'SystemController@postRolesUpdate']);
          Route::get('/roles/borrar/{id}', ['as' =>'roles.delete', 'uses' => 'SystemController@destroyRole']);

          Route::get('/oficinas', ['as' =>'oficinas.index', 'uses' => 'SystemController@getOficinasIndex']);
          Route::get('/oficinas/json', ['as' =>'oficinas.json', 'uses' => 'SystemController@getOficinasJSON']);
          Route::get('/oficinas/nuevo', ['as' =>'oficinas.nuevo', 'uses' => 'SystemController@getOficinasNuevo']);
          Route::get('/oficinas/ficha/{id}', ['as' =>'oficinas.ficha', 'uses' => 'SystemController@getOficinasUpdate']);
          Route::post('/oficinas/ficha/{id}', ['as' =>'oficinas.ficha', 'uses' => 'SystemController@postOficinasUpdate']);
          Route::get('/oficinas/borrar/{id}', ['as' =>'oficinas.delete', 'uses' => 'SystemController@destroyOficina']);

          Route::get('/emails', ['as' =>'emails.index', 'uses' => 'SystemMailController@getIndex']);
          Route::get('/emails/nuevo/{tipo?}', ['as' =>'emails.nuevo', 'uses' => 'SystemMailController@getNuevo']);
          Route::get('/emails/ficha/{id}', ['as' =>'emails.ficha', 'uses' => 'SystemMailController@getUpdate']);
          Route::post('/emails/ficha/{id}', ['as' =>'emails.ficha', 'uses' => 'SystemMailController@postUpdate']);
          Route::get('/emails/render/{id}/{idioma?}', ['as' =>'emails.render', 'uses' => 'SystemMailController@getRender']);
          Route::get('/emails/preview/{id}/{booking_id?}', ['as' =>'emails.preview', 'uses' => 'SystemMailController@getPreview']);
          Route::get('/emails/test/{id}/{idioma?}', ['as' =>'emails.test', 'uses' => 'SystemMailController@getTest']);

          Route::get('/avisos', ['as' =>'avisos.index', 'uses' => 'AvisosController@getIndex']);
          Route::get('/avisos/nuevo/{tipo?}', ['as' =>'avisos.nuevo', 'uses' => 'AvisosController@getNuevo']);
          Route::get('/avisos/ficha/{id}', ['as' =>'avisos.ficha', 'uses' => 'AvisosController@getUpdate']);
          Route::post('/avisos/ficha/{id}', ['as' =>'avisos.ficha', 'uses' => 'AvisosController@postUpdate']);

          Route::get('/avisos/sync', ['as' =>'avisos.sync', 'uses' => 'AvisosController@getSync']);
          Route::post('/avisos/sync', ['as' =>'avisos.sync.post', 'uses' => 'AvisosController@postSync']);
          Route::get('/avisos/sync/logs', ['as' =>'avisos.sync.logs', 'uses' => 'AvisosController@getSyncLogs']);

          Route::get('/avisos/borrar/{id}', ['as' =>'avisos.delete', 'uses' => 'AvisosController@delete']);
          Route::get('/avisos/activar/{id}/{estado}', ['as' =>'avisos.activo', 'uses' => 'AvisosController@setActivo']);
          Route::get('/avisos/ficha/{id}/preview/{ficha_id}', ['as' =>'avisos.simulacion.preview', 'uses' => 'AvisosController@getSimulacionPreview']);
          Route::get('/avisos/ficha/{id}/simulacion/{avisado?}', ['as' =>'avisos.simulacion', 'uses' => 'AvisosController@getSimulacion']);
          Route::get('/avisos/log/{id}', ['as' =>'avisos.log', 'uses' => 'AvisosController@getLog']);

          Route::get('/avisos/docs', ['as' =>'avisos.docs.index', 'uses' => 'AvisosController@getDocsIndex']);
          Route::get('/avisos/docs/nuevo/{tipo?}', ['as' =>'avisos.docs.nuevo', 'uses' => 'AvisosController@getDocsNuevo']);
          Route::get('/avisos/docs/ficha/{id}', ['as' =>'avisos.docs.ficha', 'uses' => 'AvisosController@getDocsUpdate']);
          Route::get('/avisos/docs/ficha/{id}/preview/{booking_id?}', ['as' =>'avisos.docs.preview', 'uses' => 'AvisosController@getDocsPreview']);
          Route::get('/avisos/docs/ficha/{id}/render/{idioma?}', ['as' =>'avisos.docs.render', 'uses' => 'AvisosController@getDocsRender']);
          Route::post('/avisos/docs/ficha/{id}', ['as' =>'avisos.docs.ficha', 'uses' => 'AvisosController@postDocsUpdate']);
          Route::get('/avisos/docs/borrar/{id}', ['as' =>'avisos.docs.delete', 'uses' => 'AvisosController@deleteDocs']);
          Route::get('/avisos/docs/activar/{id}/{estado}', ['as' =>'avisos.docs.activo', 'uses' => 'AvisosController@setActivoDocs']);

          Route::get('/avisos/listado', ['as' =>'avisos.listado', 'uses' => 'AvisosController@getListado']);

          Route::post('/doc/upload/delete/{doc_id}',array('as'=>'docs.upload.delete','uses'=>'DocumentosController@postDelete'));
          Route::post('/doc/upload/{modelo}/{modelo_id}',array('as'=>'docs.upload','uses'=>'DocumentosController@postUpload'));
          Route::get('/doc/borrar/{doc_id}/{tipo?}', ['as' =>'docs.delete', 'uses' => 'DocumentosController@destroy']);
          Route::get('/doc/visible/{doc_id}', ['as' =>'docs.visible', 'uses' => 'DocumentosController@setVisible']);
          Route::post('/doc/plataforma/{doc_id}', ['as' =>'docs.plataforma', 'uses' => 'DocumentosController@setPlataforma']);
          Route::get('/doc/list/{modelo}/{modelo_id}/{tipo?}', ['as' =>'docs.index', 'uses' => 'DocumentosController@getIndex']);
          Route::get('/doc/ficha/{id}', ['as' =>'docs.ficha', 'uses' => 'DocumentosController@getUpdate']);
          Route::post('/doc/ficha/{id}', ['as' =>'docs.ficha', 'uses' => 'DocumentosController@postUpdate']);
          Route::post('/doc/ficha/{id}/langs', ['as' =>'docs.ficha.langs', 'uses' => 'DocumentosController@postUpdateLangs']);

          //Cuestionarios
          Route::get('/cuestionarios', ['as' =>'cuestionarios.index', 'uses' => 'CuestionariosController@getIndex']);
          Route::get('/cuestionarios/nuevo', ['as' =>'cuestionarios.nuevo', 'uses' => 'CuestionariosController@getNuevo']);
          Route::get('/cuestionarios/ficha/{id}', ['as' =>'cuestionarios.ficha', 'uses' => 'CuestionariosController@getUpdate']);
          Route::post('/cuestionarios/ficha/{id}', ['as' =>'cuestionarios.ficha', 'uses' => 'CuestionariosController@postUpdate']);
          Route::get('/cuestionarios/borrar/{id}', ['as' =>'cuestionarios.delete', 'uses' => 'CuestionariosController@delete']);
          Route::get('/cuestionarios/resultados', ['as' =>'cuestionarios.resultados', 'uses' => 'CuestionariosController@getResultados']);
          Route::get('/cuestionarios/informes', ['as' =>'cuestionarios.informes', 'uses' => 'CuestionariosController@getInformes']);
          Route::get('/cuestionarios/reclamar/{id}/{modelo?}/{modelo_id?}', ['as' =>'cuestionarios.reclamar', 'uses' => 'CuestionariosController@getReclamar']);
          Route::get('/cuestionarios/respuesta/{id}', ['as' =>'cuestionarios.respuesta', 'uses' => 'CuestionariosController@getRespuesta']);

          Route::get('/cuestionarios/editor', ['as' =>'cuestionarios.editor', 'uses' => 'CuestionariosController@getRespuestaAjax']);
          Route::get('/cuestionarios/editorsave', ['as' =>'cuestionarios.editorsave', 'uses' => 'CuestionariosController@getSaveAjax']);
          Route::get('/cuestionarios/editorsaveoffline', ['as' =>'cuestionarios.editorsaveoffline', 'uses' => 'CuestionariosController@getSaveOfflineAjax']);
          Route::get('/cuestionarios/cambiarestado', ['as' =>'cuestionarios.cambiarestado', 'uses' => 'CuestionariosController@getCambiarEstadoAjax']);
          Route::get('/cuestionarios/cambiarestadosave', ['as' =>'cuestionarios.cambiarestadosave', 'uses' => 'CuestionariosController@getCambiarEstadoSaveAjax']);

          Route::post('/cuestionarios/vinculado', ['as' =>'cuestionarios.vinculados', 'uses' => 'CuestionariosController@postVinculado']);
          Route::get('/cuestionarios/vinculado/{id}/delete', ['as' =>'cuestionarios.vinculados.delete', 'uses' => 'CuestionariosController@deleteVinculado']);

          //Cuestionarios -> respuestas

          //Plataforma (nuevo y borrar no => manual )
          Route::get('/plataformas', ['as' =>'plataformas.index', 'uses' => 'PlataformasController@getIndex']);
          Route::get('/plataformas/{id}', ['as' =>'plataformas.ficha', 'uses' => 'PlataformasController@getUpdate']);
          Route::post('/plataformas/{id}', ['as' =>'plataformas.ficha', 'uses' => 'PlataformasController@postUpdate']);

          Route::get('/campos', ['as' =>'campos.index', 'uses' => 'CamposController@getIndex']);
          Route::get('/campos/{id}', ['as' =>'campos.ficha', 'uses' => 'CamposController@getUpdate']);
          Route::post('/campos/{id}', ['as' =>'campos.ficha', 'uses' => 'CamposController@postUpdate']);

        });

      Route::group(
        [
          'namespace'   => 'Prescriptores',
          'prefix'  => 'prescriptores',
          'as'      => 'prescriptores.',
        ],
        function() {

          Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'PrescriptoresController@getNuevo']);
          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'PrescriptoresController@getUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'PrescriptoresController@postUpdate']);
          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'PrescriptoresController@destroy']);
          Route::get('/ficha/{id}/ventas/{any}', ['as' =>'ventas', 'uses' => 'PrescriptoresController@getVentas']);
          Route::get('/ficha/{id}/ventas-detalle/{any}', ['as' =>'ventas-detalle', 'uses' => 'PrescriptoresController@getVentasDetalle']);

          Route::group(
            [
              'prefix'    => 'categorias',
              'as'        => 'categorias.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'PrescriptoresController@getIndexCategoria']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'PrescriptoresController@getNuevoCategoria']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'PrescriptoresController@getUpdateCategoria']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'PrescriptoresController@postUpdateCategoria']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'PrescriptoresController@destroyCategoria']);

            });

          Route::group(
            [
              'prefix'    => 'categorias-comision',
              'as'        => 'categorias-comision.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'PrescriptoresController@getIndexCategoriaCom']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'PrescriptoresController@getNuevoCategoriaCom']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'PrescriptoresController@getUpdateCategoriaCom']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'PrescriptoresController@postUpdateCategoriaCom']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'PrescriptoresController@destroyCategoriaCom']);

            });

          Route::get('/{oficina_id?}', ['as' =>'index', 'uses' => 'PrescriptoresController@getIndex']);

        });

      Route::group(
            [
              'namespace'   => 'Informes',
              'prefix'    => 'generar',
              'as'        => 'generar.',
            ],
            function() {

              Route::get('/notapago/{booking_id}/{lang?}', ['as' =>'notapago', 'uses' => 'GeneraDocumentosController@getNotaPago']);
              Route::get('/recibo/{booking_id}/{lang?}', ['as' =>'recibo', 'uses' => 'GeneraDocumentosController@getRecibo']);
              Route::get('/proveedor/{booking_id}', ['as' =>'booking-proveedor', 'uses' => 'GeneraDocumentosController@getProveedor']);

        });

      Route::group(
        [
          'namespace'   => 'Informes',
          'prefix'  => 'informes',
          'as'      => 'informes.',
        ],
        function() {

          Route::get('/filtros', ['as' =>'filtros.ajax', 'uses' => 'InformesController@getFiltrosJson']);

          Route::get('/contacto/inscritos', ['as' =>'datos.inscritos', 'uses' => 'InformesController@getDatosInscritos']);
          Route::get('/datos/estudiantes', ['as' =>'datos.estudiantes', 'uses' => 'InformesController@getDatosEstudiantes']);

          Route::get('/pagos-grupo', ['as' =>'pagos.grupo', 'uses' => 'InformesController@getPagosGrupo']);
          Route::get('/pagos-envio', ['as' =>'pagos.envio', 'uses' => 'InformesController@getPagosEnvio']);
          Route::get('/pagos', ['as' =>'pagos', 'uses' => 'InformesController@getPagos']);
          Route::get('/doc-especificos', ['as' =>'doc-especificos', 'uses' => 'InformesController@getDocsEspecificos']);

          Route::get('/menores-12', ['as' =>'menores12', 'uses' => 'InformesController@getMenores12']);
          Route::get('/seguro', ['as' =>'seguros', 'uses' => 'InformesController@getSeguros']);
          Route::get('/avisos-reserva', ['as' =>'avisos.reserva', 'uses' => 'InformesController@getAvisosReserva']);
          Route::get('/ventas', ['as' =>'ventas', 'uses' => 'InformesController@getVentas']);
          Route::get('/ventas-analisis/{todas?}/{curso?}', ['as' =>'ventas-analisis', 'uses' => 'InformesController@getVentasAnalisis']);
          Route::get('/ventas-graficos', ['as' =>'ventas-graficos', 'uses' => 'InformesController@getVentasGraficos']);

          Route::get('/control-divisa', ['as' =>'control-divisa', 'uses' => 'InformesController@getControlDivisa']);
          Route::get('/no-repetidores', ['as' =>'no-repetidores', 'uses' => 'InformesController@getNoRepetidores']);
          Route::get('/facturado-cobrado', ['as' =>'facturado-cobrado', 'uses' => 'InformesController@getFacturadoCobrado']);

          Route::get('/booking-grupos', ['as' =>'booking-grupos', 'uses' => 'InformesController@getBookingGrupos']);
          Route::get('/booking-grupos/pdf', ['as' =>'booking-grupos.pdf', 'uses' => 'InformesController@getBookingGruposPdf']);

          Route::get('/convocatorias/multi', ['as' =>'convocatorias-multi', 'uses' => 'InformesController@getConvocatoriasMulti']);

          Route::get('/facturacion/{tipo?}/{user_id?}/{oficina_id?}', ['as' =>'facturacion', 'uses' => 'InformesController@getFacturacion']);
          Route::get('/facturacion-precios', ['as' =>'facturacion-precios', 'uses' => 'InformesController@getFacturacionPrecios']);
          Route::get('/facturacion-fechas', ['as' =>'facturacion-fechas', 'uses' => 'InformesController@getFacturacionFechas']);
          Route::get('/facturacion-347', ['as' =>'facturacion-347', 'uses' => 'InformesController@getFacturacion347']);

          Route::get('/plazas-vuelos', ['as' =>'plazas-vuelos', 'uses' => 'InformesController@getPlazasVuelos']);
          Route::get('/plazas-alojamientos', ['as' =>'plazas-alojamientos', 'uses' => 'InformesController@getPlazasAlojamientos']);
          Route::get('/plazas-semanas', ['as' =>'plazas-semanas', 'uses' => 'InformesController@getPlazasSemanas']);
          Route::get('/plazas-semanas/pdf', ['as' =>'plazas-semanas.pdf', 'uses' => 'InformesController@getPlazasSemanasPdf']);
          Route::get('/reunion', ['as' =>'reunion', 'uses' => 'InformesController@getReunion']);
          Route::get('/escuelas', ['as' =>'escuelas', 'uses' => 'InformesController@getEscuelas']);
          Route::get('/aportes', ['as' =>'aportes', 'uses' => 'InformesController@getAportes']);
          Route::get('/ventas-prescriptor', ['as' =>'ventas-prescriptor', 'uses' => 'InformesController@getVentasPrescriptor']);
          Route::get('/ventas-prescriptores', ['as' =>'ventas-prescriptores', 'uses' => 'InformesController@getVentasPrescriptores']);
          Route::get('/vuelos-asignacion', ['as' =>'vuelos-asignacion', 'uses' => 'InformesController@getVuelosAsignacion']);
          Route::get('/vuelos-presalida', ['as' =>'vuelos-presalida', 'uses' => 'InformesController@getVuelosPreSalida']);
          Route::get('/vuelos-pasajeros', ['as' =>'vuelos-pasajeros', 'uses' => 'InformesController@getVuelosPasajeros']);
          Route::get('/vuelos-api', ['as' =>'vuelos-api', 'uses' => 'InformesController@getVuelosApi']);
          Route::get('/vuelos-api2', ['as' =>'vuelos-api2', 'uses' => 'InformesController@getVuelosApi2']);
          Route::get('/vuelos-proveedor', ['as' =>'vuelos-proveedor', 'uses' => 'InformesController@getVuelosProveedor']);
          Route::get('/vuelos-parrilla', ['as' =>'vuelos-parrilla', 'uses' => 'InformesController@getVuelosParrilla']);

          Route::get('/avi', ['as' =>'avi', 'uses' => 'InformesController@getListadoAVI']);
          Route::get('/vuelos-agencia', ['as' =>'vuelos-agencia', 'uses' => 'InformesController@getVuelosAgencia']);
          Route::get('/ventas-origen', ['as' =>'ventas-origen', 'uses' => 'InformesController@getVentasOrigen']);

          Route::get('/aeropuerto', ['as' =>'aeropuerto', 'uses' => 'InformesController@getListadoAeropuerto']);
          Route::get('/aeropuerto/pdf', ['as' =>'aeropuerto.pdf', 'uses' => 'InformesController@getListadoAeropuertoPdf']);
          Route::get('/monitores', ['as' =>'monitores', 'uses' => 'InformesController@getListadoMonitores']);
          Route::get('/monitores/pdf', ['as' =>'monitores.pdf', 'uses' => 'InformesController@getListadoMonitoresPdf']);

          Route::get('/orla', ['as' =>'orla', 'uses' => 'InformesController@getOrla']);
          Route::get('/orla/pdf', ['as' =>'orla.pdf', 'uses' => 'InformesController@getOrlaPdf']);

          Route::get('/area/no-conexion', ['as' =>'area.no-conexion', 'uses' => 'InformesController@getAreaNoconexion']);
          Route::get('/viajeros/datos', ['as' =>'viajeros.datos', 'uses' => 'InformesController@getViajerosDatos']);

          Route::get('/booking-individuales', ['as' =>'bookings-individuales', 'uses' => 'InformesController@getBookingsIndividuales']);
          Route::get('/bookings-origen', ['as' =>'bookings-origen', 'uses' => 'InformesController@getBookingsOrigen']);
          Route::get('/margenes', ['as' =>'margenes', 'uses' => 'InformesController@getMargenes']);

          Route::get('/incidencias', ['as' =>'bookings-incidencias', 'uses' => 'InformesController@getIncidencias']);

          Route::get('/cic/niveles', ['as' =>'cic-niveles', 'uses' => 'InformesController@getCicNiveles']);
          Route::get('/cic/camps', ['as' =>'cic-camps', 'uses' => 'InformesController@getCicCamps']);

          Route::get('/checklist', ['as' =>'checklist', 'uses' => 'InformesController@getChecklist']);
          Route::get('/descuentos', ['as' =>'descuentos', 'uses' => 'InformesController@getDescuentos']);
          Route::get('/devoluciones', ['as' =>'devoluciones', 'uses' => 'InformesController@getDevoluciones']);

          Route::get('/trafico', ['as' =>'trafico', 'uses' => 'InformesController@getTrafico']);
          Route::get('/actividad', ['as' =>'actividad', 'uses' => 'InformesController@getActividadUsers']);
          Route::get('/edad', ['as' =>'edad', 'uses' => 'InformesController@getEdad']);
          Route::get('/trasvase', ['as' =>'trasvase', 'uses' => 'InformesController@getTrasvase']);

          Route::get('/contactos', ['as' =>'contactos', 'uses' => 'InformesController@getContactos']);
          Route::get('/catalogos', ['as' =>'envio-catalogos', 'uses' => 'InformesController@getEnvioCatalogos']);
          
          Route::get('/analisis/solicitudes', ['as' =>'anal-solicitudes', 'uses' => 'InformesController@getAnalSolicitudes']);

        });

      Route::group(
            [
              'prefix'    => 'agencias',
              'as'        => 'agencias.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'AgenciasController@getIndex']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'AgenciasController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'AgenciasController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'AgenciasController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'AgenciasController@destroy']);

            });

      Route::group(
            [
              'namespace'   => 'Monitores',
              'prefix'    => 'monitores',
              'as'        => 'monitores.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'MonitoresController@getIndex']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'MonitoresController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'MonitoresController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'MonitoresController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'MonitoresController@destroy']);

              Route::get('/{id}/relaciones', ['as' =>'relaciones', 'uses' => 'MonitoresController@getRelaciones']);
              Route::get('/{id}/user', ['as' =>'user', 'uses' => 'MonitoresController@setUser']);
              Route::get('/relaciones/{idrel}/borrar/{todas?}', ['as' =>'relaciones.delete', 'uses' => 'MonitoresController@destroyRelacion']);

            });

      Route::group(
        [
          'namespace'   => 'Solicitudes',
          'prefix'  => 'solicitudes',
          'as'      => 'solicitudes.',
        ],
        function() {

          Route::get('/nuevo/{viajero_id}', ['as' =>'nuevo', 'uses' => 'SolicitudesController@getNuevo']);
          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'SolicitudesController@getUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'SolicitudesController@postUpdate']);
          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'SolicitudesController@destroy']);
          Route::get('/set-catalogo/{solicitud}', ['as' =>'set-catalogo', 'uses' => 'SolicitudesController@setCatalogo']);

          Route::get('/viajero/{viajero_id}', ['as' =>'index.viajero', 'uses' => 'SolicitudesController@getIndexByViajero']);

          Route::group(
            [
              'prefix'    => 'status',
              'as'        => 'status.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'StatusController@getIndex']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'StatusController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'StatusController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'StatusController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'StatusController@destroy']);

            });

          Route::group(
            [
              'prefix'    => 'checklist',
              'as'        => 'checklist.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'StatusChecklistsController@getIndex']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'StatusChecklistsController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'StatusChecklistsController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'StatusChecklistsController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'StatusChecklistsController@destroy']);

            });

          //Route::get('/filtros', ['as' =>'filtros', 'uses' => 'SolicitudesController@getFiltros']);
          Route::get('/filtros-dtt', ['as' =>'filtros.dtt', 'uses' => 'SolicitudesController@dttFiltros']);
          Route::get('/ajax/{id}/checklist', ['as' =>'ajax.checklist', 'uses' => 'SolicitudesController@ajaxGetChecklist']);
          Route::post('/ajax/{id}/checklist', ['as' =>'ajax.checklist', 'uses' => 'SolicitudesController@ajaxChecklist']);
          Route::post('/ajax/{id}/status', ['as' =>'ajax.status', 'uses' => 'SolicitudesController@ajaxStatus']);
          Route::get('/{status_id?}/{user_id?}/{oficina_id?}', ['as' =>'index', 'uses' => 'SolicitudesController@getIndex']);
          Route::get('/by/{status_id?}/{user_id?}/{oficina_id?}', ['as' =>'indexby', 'uses' => 'SolicitudesController@getIndexBy']);

        });

      Route::group(
        [
          'namespace'   => 'Bookings',
          'prefix'  => 'bookings',
          'as'      => 'bookings.',
        ],
        function() {

          Route::get('/nuevo/cursos/{viajero_id}/{booking_id?}/{presupuesto?}', ['as' =>'nuevo.cursos', 'uses' => 'BookingsController@dttCursos']);
          Route::get('/nuevo/{viajero_id}/{booking_id?}', ['as' =>'nuevo', 'uses' => 'BookingsController@getNuevo']);
          Route::get('/cambiar/{booking_id}', ['as' =>'cambiar', 'uses' => 'BookingsController@getCambiar']);
          Route::post('/online/{booking_id}', ['as' =>'online', 'uses' => 'BookingsController@setOnline']);

          Route::post('/crear/{id}', ['as' =>'crear.post', 'uses' => 'BookingsController@postCrear']);
          Route::get('/crear/{id}/v/{viajero_id}/c/{curso_id}/{presupuesto?}', ['as' =>'crear', 'uses' => 'BookingsController@getCrear']);

          Route::post('/ajax/{id}', ['as' =>'ajax', 'uses' => 'BookingsController@ajaxPost']);

          Route::post('/extra/add/{id}', ['as' =>'extra.add', 'uses' => 'BookingsController@extraAdd']);
          Route::post('/extra/borrar', ['as' =>'extra.delete', 'uses' => 'BookingsController@extraDestroy']);

          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'BookingsController@getUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'BookingsController@postUpdate']);
          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'BookingsController@destroy']);

          Route::post('/ficha/{id}/seguro', ['as' =>'ficha.seguro', 'uses' => 'BookingsController@postUpdateExtras']);

          Route::get('/cancelar/{id}', ['as' =>'cancelar', 'uses' => 'BookingsController@cancelar']);
          Route::get('/devolucion/{id}', ['as' =>'devolucion', 'uses' => 'BookingsController@devolucion']);
          Route::get('/pagado/{id}/{estado?}', ['as' =>'pagado', 'uses' => 'BookingsController@pagado']);
          Route::get('/asociar/{id}', ['as' =>'asociar', 'uses' => 'BookingsController@asociar']);

          Route::get('/ajax/{id}/checklist', ['as' =>'ajax.checklist', 'uses' => 'BookingsController@ajaxGetChecklist']);
          Route::post('/ajax/{id}/checklist', ['as' =>'ajax.checklist', 'uses' => 'BookingsController@ajaxChecklist']);
          Route::post('/ajax/{id}/status', ['as' =>'ajax.status', 'uses' => 'BookingsController@ajaxStatus']);
          Route::get('/viajero/{viajero_id}', ['as' =>'index.viajero', 'uses' => 'BookingsController@getIndexByViajero']);
          Route::get('/convocatoria/{convocatory_id}/alojamiento/{alojamiento_id}', ['as' =>'index.alojamiento', 'uses' => 'BookingsController@getIndexByAlojamiento']);

          Route::get('/ovbkg/{id}', ['as' =>'overbooking', 'uses' => 'BookingsController@overbooking']);
          Route::get('/vuelo-ok/{id}', ['as' =>'vuelo_ok', 'uses' => 'BookingsController@vueloOk']);
          Route::get('/pdf/{id}', ['as' =>'pdf.ficha', 'uses' => 'BookingsController@getPdf']);
          Route::get('/notapago35/{id}', ['as' =>'notapago35', 'uses' => 'BookingsController@getNotaPago35']);
          Route::get('/notapago35/{id}/mail', ['as' =>'notapago35.mail', 'uses' => 'BookingsController@getNotaPago35Mail']);

          Route::post('/vuelo/{id}', ['as' =>'vuelo', 'uses' => 'BookingsController@postVuelo']);
          Route::post('/fechas/{id}/{tipo}', ['as' =>'fechas', 'uses' => 'BookingsController@postFechas']);

          Route::get('/monedas/{booking_id}', ['as' =>'monedas', 'uses' => 'BookingsController@getMonedas']);
          Route::post('/monedas/{booking_id}', ['as' =>'monedas', 'uses' => 'BookingsController@postMonedas']);

          Route::post('/ficha/{id}/familia/{asignacion_id?}', ['as' =>'familias', 'uses' => 'BookingsController@postFamilia']);
          Route::get('/familia/asignacion/{asignacion_id}/borrar', ['as' =>'familias.delete', 'uses' => 'BookingsController@deleteFamilia']);
          Route::get('/familia/asignacion/{asignacion_id}/estado', ['as' =>'familias.activo', 'uses' => 'BookingsController@setFamiliaActivo']);
          Route::get('/familia/asignacion/{asignacion_id}/enviar', ['as' =>'familias.mail', 'uses' => 'BookingsController@mailFamilia']);

          Route::post('/ficha/{id}/school/{asignacion_id?}', ['as' =>'schools', 'uses' => 'BookingsController@postSchool']);
          Route::get('/school/asignacion/{asignacion_id}/borrar', ['as' =>'schools.delete', 'uses' => 'BookingsController@deleteSchool']);
          Route::get('/school/asignacion/{asignacion_id}/estado', ['as' =>'schools.activo', 'uses' => 'BookingsController@setSchoolActivo']);
          Route::get('/school/asignacion/{asignacion_id}/enviar', ['as' =>'schools.mail', 'uses' => 'BookingsController@mailSchool']);


          //Facturas
          Route::post('{booking_id}/facturas/datos', ['as' =>'facturas.datos', 'uses' => 'BookingsController@postFacturaDatos']);
          Route::get('/{booking_id}/facturas', ['as' =>'facturas.index', 'uses' => 'BookingFacturasController@getIndex']);
          Route::get('/facturas/borrar/{factura}', ['as' =>'facturas.delete', 'uses' => 'BookingFacturasController@delete']);
          Route::get('/facturas/convocatorias/{model}/{model_id}', ['as' =>'facturas.index.bymodel', 'uses' => 'BookingFacturasController@getIndexByModel']);
          Route::post('/facturar/convocatorias/{model}/{model_id}', ['as' =>'facturar.bymodel', 'uses' => 'BookingFacturasController@facturarByModel']);
          Route::get('/facturas/{id}/pdf', ['as' =>'facturas.pdf', 'uses' => 'BookingFacturasController@getPdf']);

          Route::get('/{booking_id}/facturar/opcion/{opcion}', ['as' =>'facturar.opcion', 'uses' => 'BookingsController@setFacturarTipo']);
          Route::get('/facturar/{model}/{model_id}/{abono?}', ['as' =>'facturar', 'uses' => 'BookingsController@facturar']);
          Route::get('/facturar-abono/{factura_id}/{cantidad?}', ['as' =>'facturar.abono', 'uses' => 'BookingsController@facturarAbono']);
          Route::post('/facturas/{booking_id}', ['as' =>'facturas', 'uses' => 'BookingsController@facturas']);
          Route::post('/facturar-parcial/{booking_id}', ['as' =>'facturar.parcial', 'uses' => 'BookingsController@facturarParcial']);

          Route::get('/presupuesto/{viajero_id}', ['as' =>'presupuesto', 'uses' => 'BookingsController@getPresupuesto']);

          Route::get('/{booking_id}/firmar/{doc}', ['as' =>'firma.solicitar', 'uses' => 'BookingsController@getFirmaSolicitar']);
          
          Route::get('/{booking}/mail/examen', ['as' =>'examenes.mail', 'uses' => 'BookingsController@mailExamenes']);
          Route::get('/{booking}/examen-asks', ['as' =>'examenes.asks', 'uses' => 'BookingsController@getExamenRespuestas']);

          Route::group(
            [
              'prefix'    => 'incidencias',
              'as'        => 'incidencias.',
            ],
            function() {

              Route::get('/resumen/{filtro?}/{user_id?}/{oficina_id?}', ['as' =>'index.resumen', 'uses' => 'BookingIncidenciasController@getIndexResumen']);

              Route::get('/{booking_id}/index', ['as' =>'index', 'uses' => 'BookingIncidenciasController@getIndex']);
              Route::get('/ajax/{id}', ['as' =>'ajax.ficha', 'uses' => 'BookingIncidenciasController@ajaxGetUpdate']);

              Route::get('/{id}', ['as' =>'ficha', 'uses' => 'BookingIncidenciasController@getUpdate']);
              Route::post('/{id}', ['as' =>'ficha', 'uses' => 'BookingIncidenciasController@postUpdate']);
              Route::get('/{id}/resolver/{estado?}', ['as' =>'resolver', 'uses' => 'BookingIncidenciasController@setCompleta']);

              Route::group(
              [
                'prefix'    => 'tareas',
                'as'        => 'tareas.',
              ],
              function() {

                Route::get('/nuevo/{incidencia_id}', ['as' =>'nuevo', 'uses' => 'BookingTareasController@getNuevo']);
                Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'BookingTareasController@getUpdate']);
                Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'BookingTareasController@postUpdate']);
                Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'BookingTareasController@destroy']);

                Route::get('/completar/{id}/{estado?}', ['as' =>'completar', 'uses' => 'BookingTareasController@setCompleta']);

                // Route::get('/resumen/{filtro?}/{user_id?}/{oficina_id?}', ['as' =>'index.resumen', 'uses' => 'BookingTareasController@getIndexResumen']);

                Route::get('/ajax/{id}', ['as' =>'ajax.ficha', 'uses' => 'BookingTareasController@ajaxGetUpdate']);

                Route::get('/{incidencia_id}', ['as' =>'index', 'uses' => 'BookingTareasController@getIndex']);

              });

              Route::group(
              [
                'prefix'    => 'logs',
                'as'        => 'logs.',
              ],
              function() {

                Route::get('/{incidencia_id?}/f/{todos?}', ['as' =>'index', 'uses' => 'BookingIncidenciaLogsController@getIndex']);
                Route::get('/nuevo/{incidencia_id}', ['as' =>'nuevo', 'uses' => 'BookingIncidenciaLogsController@getNuevo']);
                Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'BookingIncidenciaLogsController@getUpdate']);
                Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'BookingIncidenciaLogsController@postUpdate']);
                Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'BookingIncidenciaLogsController@destroy']);

              });

            });

          Route::group(
            [
              'prefix'    => 'logs',
              'as'        => 'logs.',
            ],
            function() {

              Route::get('/{booking_id?}/{filtro?}', ['as' =>'index', 'uses' => 'BookingLogsController@getIndex']);

            });

          Route::group(
            [
              'prefix'    => 'pagos',
              'as'        => 'pagos.',
            ],
            function() {

              Route::get('/{booking_id?}', ['as' =>'index', 'uses' => 'BookingPagosController@getIndex']);
              Route::get('/nuevo/{booking_id}', ['as' =>'nuevo', 'uses' => 'BookingPagosController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'BookingPagosController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'BookingPagosController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'BookingPagosController@destroy']);

              Route::get('/pdf/{id}', ['as' =>'pdf', 'uses' => 'BookingPagosController@getPdf']);

            });

          Route::group(
            [
              'prefix'    => 'descuentos',
              'as'        => 'descuentos.',
            ],
            function() {

              Route::get('/{booking_id?}', ['as' =>'index', 'uses' => 'BookingDescuentosController@getIndex']);
              Route::get('/nuevo/{booking_id}', ['as' =>'nuevo', 'uses' => 'BookingDescuentosController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'BookingDescuentosController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'BookingDescuentosController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'BookingDescuentosController@destroy']);

            });

          Route::group(
            [
              'prefix'    => 'devoluciones',
              'as'        => 'devoluciones.',
            ],
            function() {

              Route::get('/{booking_id?}', ['as' =>'index', 'uses' => 'BookingDevolucionesController@getIndex']);
              Route::get('/nuevo/{booking_id}', ['as' =>'nuevo', 'uses' => 'BookingDevolucionesController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'BookingDevolucionesController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'BookingDevolucionesController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'BookingDevolucionesController@destroy']);

            });

          Route::group(
            [
              'prefix'    => 'status',
              'as'        => 'status.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'StatusController@getIndex']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'StatusController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'StatusController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'StatusController@postUpdate']);
              Route::get('borrar/{id}', ['as' =>'delete', 'uses' => 'StatusController@destroy']);

            });

          Route::group(
            [
              'prefix'    => 'checklist',
              'as'        => 'checklist.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'StatusChecklistsController@getIndex']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'StatusChecklistsController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'StatusChecklistsController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'StatusChecklistsController@postUpdate']);
              Route::get('borrar/{id}', ['as' =>'delete', 'uses' => 'StatusChecklistsController@destroy']);

            });

          //Route::get('/filtros', ['as' =>'filtros', 'uses' => 'BookingsController@getFiltros']);
          Route::get('/filtros-dtt', ['as' =>'filtros.dtt', 'uses' => 'BookingsController@dttFiltros']);
          Route::get('/{viajero_id?}/{user_id?}/{oficina_id?}', ['as' =>'index', 'uses' => 'BookingsController@getIndex']);

        });

      Route::group(
        [
          'namespace'   => 'Inscritos',
          'prefix'  => 'inscritos',
          'as'      => 'inscritos.',
        ],
        function() {

          Route::get('/lead/{id}', ['as' =>'lead', 'uses' => 'InscritosController@setLead']);
          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'InscritosController@getUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'InscritosController@postUpdate']);

          Route::get('/{status_id?}/{asignado_a?}/{oficina_id?}', ['as' =>'index', 'uses' => 'InscritosController@getIndex']);
          // Route::get('/any/{any?}/{status_id?}', ['as' =>'index', 'uses' => 'InscritosController@getIndex']);

        });

      Route::group(
        [
          'namespace'   => 'Leads',
          'prefix'  => 'tutores',
          'as'      => 'tutores.',
        ],
        function() {

          Route::get('/area/{id}', ['as' =>'area', 'uses' => 'TutoresController@getArea']);

          Route::get('/', ['as' =>'index', 'uses' => 'TutoresController@getIndex']);
          Route::get('/nuevo/{viajero_id?}', ['as' =>'nuevo', 'uses' => 'TutoresController@getNuevo']);
          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'TutoresController@destroy']);

          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'TutoresController@postUpdate']);
          Route::get('/ficha/{id}/{viajero_id?}', ['as' =>'ficha', 'uses' => 'TutoresController@getUpdate']);

          Route::post('/ficha-asignar/{id}', ['as' =>'ficha.asignar', 'uses' => 'TutoresController@postUpdateAsignar']);
          Route::get('/borrar-relacion/{id}', ['as' =>'delete.relacion', 'uses' => 'TutoresController@destroyRelacion']);

          Route::get('/autocomplete', ['as' =>'autocomplete', 'uses'=> 'TutoresController@getAutocomplete']);

          Route::get('/buscar/{tipo?}/{query?}', ['as' =>'buscar', 'uses' => 'TutoresController@getIndexBuscar']);

          Route::get('/ficha/{id}/trafico', ['as' =>'trafico', 'uses' => 'TutoresController@getTrafico']);

          // Route::get('/{viajero_id?}', ['as' =>'index', 'uses' => 'TutoresController@getIndex']);
          
          Route::group(
            [
              'prefix'    => 'logs',
              'as'        => 'logs.',
            ],
            function() {

              Route::get('/{tutor_id?}', ['as' =>'index', 'uses' => 'TutorLogsController@getIndex']);
              // Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'TutorLogsController@destroy']);

            });

        });

      Route::group(
        [
          'namespace'   => 'Leads',
          'prefix'  => 'viajeros',
          'as'      => 'viajeros.',
        ],
        function() {

          Route::get('/area/{id}', ['as' =>'area', 'uses' => 'ViajerosController@getArea']);

          Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'ViajerosController@getNuevo']);
          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ViajerosController@getUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ViajerosController@postUpdate']);
          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'ViajerosController@destroy']);
          Route::get('/archivar/{id}/{estado?}', ['as' =>'archivar', 'uses' => 'ViajerosController@archivar']);

          Route::get('/excel/{status_id?}', ['as' =>'index.excel', 'uses' => 'ViajerosController@getIndexExcel']);
          Route::get('/excel-download/{optout?}', ['as' =>'index.excel.download', 'uses' => 'ViajerosController@getIndexExcelDownload']);
          Route::get('/buscar/{tipo}/{query?}', ['as' =>'buscar', 'uses' => 'ViajerosController@getIndexBuscar']);

          // Route::get('/datos/{id}', ['as' =>'ficha.datos', 'uses' => 'ViajerosController@getDatos']);
          // Route::post('/datos/{id}', ['as' =>'ficha.datos', 'uses' => 'ViajerosController@postDatos']);

          Route::get('/inscribir/{id}', ['as' =>'inscribir', 'uses' => 'ViajerosController@setInscrito']);
          Route::get('/tutor/set/{rel_id}/{rel}', ['as' =>'set.tutor', 'uses' => 'ViajerosController@setTutor']);
          Route::get('/tutor/{tutor_id?}', ['as' =>'index.tutor', 'uses' => 'ViajerosController@getIndexByTutor']);

          Route::post('upload/delete/{viajero_id}',array('as'=>'upload.delete','uses'=>'ViajeroArchivosController@postDelete'));
          Route::post('upload/{viajero_id}/{visible}/{booking_id?}',array('as'=>'upload','uses'=>'ViajeroArchivosController@postUpload'));
          Route::get('archivo/borrar/{file_id}', ['as' =>'archivo.delete', 'uses' => 'ViajeroArchivosController@destroy']);
          Route::get('archivo/visible/{file_id}/{booking?}', ['as' =>'archivo.visible', 'uses' => 'ViajeroArchivosController@setVisible']);
          Route::get('archivo/status/{file_id}/{status?}', ['as' =>'archivo.status', 'uses' => 'ViajeroArchivosController@setStatus']);

          Route::get('/ficha/{id}/facturas', ['as' =>'facturas', 'uses' => 'ViajerosController@getFacturas']);
          Route::get('/ficha/{id}/trafico', ['as' =>'trafico', 'uses' => 'ViajerosController@getTrafico']);

          Route::post('/ficha/{viajero}/exam', ['as' =>'exam', 'uses' => 'ViajerosController@postExam']);

          Route::group(
            [
              'prefix'    => 'logs',
              'as'        => 'logs.',
            ],
            function() {

              Route::get('/{viajero_id?}/f/{todos?}', ['as' =>'index', 'uses' => 'ViajeroLogsController@getIndex']);
              Route::get('/nuevo/{viajero_id}', ['as' =>'nuevo', 'uses' => 'ViajeroLogsController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ViajeroLogsController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ViajeroLogsController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'ViajeroLogsController@destroy']);

            });

          Route::group(
            [
              'prefix'    => 'tareas',
              'as'        => 'tareas.',
            ],
            function() {

              Route::get('/nuevo/{viajero_id}', ['as' =>'nuevo', 'uses' => 'ViajeroTareasController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ViajeroTareasController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ViajeroTareasController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'ViajeroTareasController@destroy']);

              Route::get('/completar/{id}/{estado?}/{home?}', ['as' =>'completar', 'uses' => 'ViajeroTareasController@setCompleta']);

              Route::get('/resumen/{filtro?}/{user_id?}/{oficina_id?}', ['as' =>'index.resumen', 'uses' => 'ViajeroTareasController@getIndexResumen']);

              Route::get('/ajax/{id}', ['as' =>'ajax.ficha', 'uses' => 'ViajeroTareasController@ajaxGetUpdate']);

              Route::get('/{viajero_id}', ['as' =>'index', 'uses' => 'ViajeroTareasController@getIndex']);

            });

          Route::group(
            [
              'prefix'    => 'origenes',
              'as'        => 'origenes.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'OrigenesController@getIndex']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'OrigenesController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'OrigenesController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'OrigenesController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'OrigenesController@destroy']);
              Route::get('/ficha/{id}/ventas/{any}', ['as' =>'ventas', 'uses' => 'OrigenesController@getVentas']);
              Route::get('/ficha/{id}/ventas-detalle/{any}', ['as' =>'ventas-detalle', 'uses' => 'OrigenesController@getVentasDetalle']);
            });

          Route::group(
            [
              'prefix'  => 'suborigenes',
              'as'      => 'suborigenes.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'SuborigenesController@getIndex']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'SuborigenesController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'SuborigenesController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'SuborigenesController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'SuborigenesController@destroy']);
              Route::get('/ficha/{id}/ventas/{any}', ['as' =>'ventas', 'uses' => 'SuborigenesController@getVentas']);
              Route::get('/ficha/{id}/ventas-detalle/{any}', ['as' =>'ventas-detalle', 'uses' => 'SuborigenesController@getVentasDetalle']);

            });

          Route::group(
            [
              'prefix'  => 'suborigendetalles',
              'as'      => 'suborigendetalles.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'SuborigenDetallesController@getIndex']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'SuborigenDetallesController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'SuborigenDetallesController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'SuborigenDetallesController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'SuborigenDetallesController@destroy']);
              Route::get('/ficha/{id}/ventas/{any}', ['as' =>'ventas', 'uses' => 'SuborigenDetallesController@getVentas']);
              Route::get('/ficha/{id}/ventas-detalle/{any}', ['as' =>'ventas-detalle', 'uses' => 'SuborigenDetallesController@getVentasDetalle']);

            });

          // Route::get('/{status_id?}/{filtro?}/{filtro_id?}', ['as' =>'index', 'uses' => 'ViajerosController@getIndex']);
          Route::get('/{status_id?}/{asign_to?}/{oficina_id?}', ['as' =>'index', 'uses' => 'ViajerosController@getIndex']);

        });

      Route::group(
        [
          'prefix'  => 'paises',
          'as'      => 'paises.',
        ],
        function() {

          Route::get('/', ['as' =>'index', 'uses' => 'ManageController@getPaises']);
          Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'ManageController@getPaisNuevo']);

          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@getPaisUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@postPaisUpdate']);

        });

      Route::group(
        [
          'prefix'  => 'nacionalidades',
          'as'      => 'nacionalidades.',
        ],
        function() {

          Route::get('/', ['as' =>'index', 'uses' => 'ManageController@getNacionalidades']);
          Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'ManageController@getNacionalidadNuevo']);

          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@getNacionalidadUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@postNacionalidadUpdate']);

        });

      Route::group(
        [
          'prefix'  => 'ciudades',
          'as'      => 'ciudades.',
        ],
        function() {

          Route::get('/', ['as' =>'index', 'uses' => 'ManageController@getCiudades']);
          Route::get('/nuevo/{pais_id?}/{provincia_id?}', ['as' =>'nuevo', 'uses' => 'ManageController@getCiudadNuevo']);

          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@getCiudadUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@postCiudadUpdate']);

        });

      Route::group(
        [
          'prefix'  => 'provincias',
          'as'      => 'provincias.',
        ],
        function() {

          Route::get('/', ['as' =>'index', 'uses' => 'ManageController@getProvincias']);
          Route::get('/nuevo/{pais_id?}', ['as' =>'nuevo', 'uses' => 'ManageController@getProvinciaNuevo']);

          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@getProvinciaUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@postProvinciaUpdate']);

        });

      Route::group(
        [
          'namespace'   => 'Monedas',
          'prefix'  => 'monedas',
          'as'      => 'monedas.',
        ],
        function() {

          Route::get('/', ['as' =>'index', 'uses' => 'MonedasController@getIndex']);
          Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'MonedasController@getNuevo']);

          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'MonedasController@getUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'MonedasController@postUpdate']);
          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'MonedasController@destroy']);

          Route::group(
          [
            'prefix'  => 'cambios',
            'as'      => 'cambios.',
          ],
          function() {

            Route::get('/', ['as' =>'index', 'uses' => 'CambiosController@getIndex']);
            Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'CambiosController@getNuevo']);
            Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'CambiosController@getUpdate']);
            Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'CambiosController@postUpdate']);
            Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'CambiosController@destroy']);

            Route::group(
            [
              'prefix'  => 'monedas',
              'as'      => 'monedas.',
            ],
            function() {

              Route::get('/{cambio_id}/monedas', ['as' =>'index', 'uses' => 'CambiosController@getMonedas']);
              Route::post('/{cambio_id}/monedas', ['as' =>'ficha', 'uses' => 'CambiosController@postMonedas']);

            });

          });

        });

      Route::group(
        [
          'prefix'  => 'categorias',
          'as'      => 'categorias.',
        ],
        function() {

          Route::get('/', ['as' =>'index', 'uses' => 'ManageController@getCategorias']);
          Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'ManageController@getCategoriaNuevo']);

          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@getCategoriaUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@postCategoriaUpdate']);

          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'ManageController@destroyCategoria']);

        });

      Route::group(
        [
          'prefix'  => 'subcategorias',
          'as'      => 'subcategorias.',
        ],
        function() {

          Route::get('/', ['as' =>'index', 'uses' => 'ManageController@getSubcategorias']);
          Route::get('/nuevo/{categoria_id?}', ['as' =>'nuevo', 'uses' => 'ManageController@getSubcategoriaNuevo']);
          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@getSubcategoriaUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@postSubcategoriaUpdate']);

          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'ManageController@destroySubcategoria']);

          // Route::get('/json/multi', ['as' =>'json.multi', 'uses' => 'ManageController@getSubcategoriasJsonMulti']);

        });

      Route::group(
        [
          'prefix'  => 'subcategoria-detalles',
          'as'      => 'subcategoria-detalles.',
        ],
        function() {

          Route::get('/', ['as' =>'index', 'uses' => 'ManageController@getSubcategoriasDet']);
          Route::get('/nuevo/{categoria_id?}', ['as' =>'nuevo', 'uses' => 'ManageController@getSubcategoriaDetNuevo']);
          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@getSubcategoriaDetUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@postSubcategoriaDetUpdate']);

          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'ManageController@destroySubcategoriaDet']);

        });

      Route::group(
        [
          'prefix'  => 'especialidades',
          'as'      => 'especialidades.',
        ],
        function() {

          Route::get('/', ['as' =>'index', 'uses' => 'ManageController@getEspecialidades']);
          Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'ManageController@getEspecialidadNuevo']);

          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@getEspecialidadUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@postEspecialidadUpdate']);

        });

      Route::group(
        [
          'prefix'  => 'subespecialidades',
          'as'      => 'subespecialidades.',
        ],
        function() {

          Route::get('/', ['as' =>'index', 'uses' => 'ManageController@getSubespecialidades']);
          Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'ManageController@getSubespecialidadNuevo']);

          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@getSubespecialidadUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@postSubespecialidadUpdate']);

        });

      Route::group(
            [
              'prefix'  => 'extras',
              'as'      => 'extras.',
            ],
            function() {

              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'ExtrasController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ExtrasController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ExtrasController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'ExtrasController@destroy']);

              Route::get('/{curso_id?}', ['as' =>'index', 'uses' => 'ExtrasController@getIndex']);


            });

      Route::group(
        [
          'prefix'  => 'unidades',
          'as'      => 'unidades.',
        ],
        function() {

          Route::get('/', ['as' =>'index', 'uses' => 'ManageController@getUnidades']);
          Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'ManageController@getUnidadesNuevo']);
          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@getUnidadesUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ManageController@postUnidadesUpdate']);
          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'ManageController@deleteUnidades']);

        });

      Route::group(
        [
          'namespace'   => 'Descuentos',
          'prefix'  => 'descuentos',
          'as'      => 'descuentos.',
        ],
        function() {

          Route::group(
            [
              'prefix'  => 'tipos',
              'as'      => 'tipos.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'DescuentoTiposController@getIndex']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'DescuentoTiposController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'DescuentoTiposController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'DescuentoTiposController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'DescuentoTiposController@destroy']);

              Route::post('/aplicar', ['as' =>'aplicar', 'uses' => 'DescuentoTiposController@aplicar']);
              Route::post('/crear', ['as' =>'crear', 'uses' => 'DescuentoTiposController@crear']);

            });

          Route::group(
            [
              'prefix'  => 'early-bird',
              'as'      => 'early-bird.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'DescuentoEarlysController@getIndex']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'DescuentoEarlysController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'DescuentoEarlysController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'DescuentoEarlysController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'DescuentoEarlysController@destroy']);
            });

        });


      Route::group(
        [
          'prefix'  => 'proveedores',
          'as'      => 'proveedores.',
        ],
        function() {

          Route::get('/', ['as' =>'index', 'uses' => 'ProveedoresController@getIndex']);
          Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'ProveedoresController@getNuevo']);
          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ProveedoresController@getUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ProveedoresController@postUpdate']);
          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'ProveedoresController@delete']);
          Route::get('/ficha/{id}/ventas/{any}/{centro?}', ['as' =>'ventas', 'uses' => 'ProveedoresController@getVentas']);

        });

      Route::group(
        [
          'namespace'   => 'Reuniones',
          'prefix'  => 'reuniones',
          'as'      => 'reuniones.',
        ],
        function() {

          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ReunionesController@getUpdate']);

          Route::get('/curso/{curso_id}', ['as' =>'index.curso', 'uses' => 'ReunionesController@getIndexByCurso']);
          Route::get('/convocatoria/{convocatoria_id}/{tipo}', ['as' =>'index.convocatoria', 'uses' => 'ReunionesController@getIndexByConvocatoria']);

          Route::post('/curso/{id?}', ['as' =>'ficha.curso', 'uses' => 'ReunionesController@postUpdateCurso']);
          Route::post('/convocatoria/{id?}', ['as' =>'ficha.convocatoria', 'uses' => 'ReunionesController@postUpdateConvocatoria']);

          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'ReunionesController@delete']);

          Route::get('/enviar/{convocatoria_id}/{convocatoria_tipo}/{todos?}', ['as' =>'enviar', 'uses' => 'ReunionesController@getEnviar']);
          Route::get('/enviar-curso/{curso_id}/{todos?}', ['as' =>'enviar.curso', 'uses' => 'ReunionesController@getEnviarCurso']);
          Route::get('/cambios/{id}', ['as' =>'cambios', 'uses' => 'ReunionesController@getCambios']);

        });

      Route::group(
        [
          'namespace'   => 'Cursos',
          'prefix'  => 'cursos',
          'as'      => 'cursos.',
        ],
        function() {

          Route::get('/nuevo/{curso_id?}', ['as' =>'nuevo', 'uses' => 'CursosController@getNuevo']);
          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'CursosController@getUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'CursosController@postUpdate']);
          Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'CursosController@destroy']);

          Route::get('/proveedor/{proveedor_id?}', ['as' =>'index.proveedor', 'uses' => 'CursosController@getIndexByProveedor']);
          // Route::get('/pocket-guide-pdf/{id}/{lang}', ['as' =>'pocket-guide', 'uses' => 'CursosController@getPocketGuidePdf']);

          Route::post('/upload/delete/{tipo}/{id}',array('as'=>'upload.delete','uses'=>'CursosController@postUploadDelete'));
          Route::post('/upload/{tipo}/{id}',array('as'=>'upload','uses'=>'CursosController@postUpload'));
          Route::get('/borrar-file/{tipo}/{id}/{file}', ['as' =>'delete.file', 'uses' => 'CursosController@deleteFile']);
          Route::get('/foto/portada/{id}/{file}', ['as' =>'foto.portada', 'uses' => 'CursosController@setFotoPortada']);

          Route::get('/firmas/{curso_id}', ['as' =>'firmas', 'uses' => 'CursosController@getFirmas']);

          Route::group(
            [
              'prefix'  => 'especialidades',
              'as'      => 'especialidades.',
            ],
            function() {

              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'CursosController@destroyEspecialidad']);
              Route::post('/{curso_id}', ['as' =>'post', 'uses' => 'CursosController@postEspecialidad']);

            });

          Route::group(
            [
              'prefix'  => 'extras',
              'as'      => 'extras.',
            ],
            function() {

              Route::get('/nuevo/{curso_id}', ['as' =>'nuevo', 'uses' => 'CursoExtrasController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'CursoExtrasController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'CursoExtrasController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'CursoExtrasController@destroy']);

              Route::get('/{curso_id?}', ['as' =>'index', 'uses' => 'CursoExtrasController@getIndex']);

            });

            Route::group(
            [
              'prefix'  => 'genericos',
              'as'      => 'genericos.',
            ],
            function() {

              Route::post('/add/{curso_id}', ['as' =>'add', 'uses' => 'CursoExtraGenericosController@add']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'CursoExtraGenericosController@destroy']);

              Route::get('/{curso_id?}', ['as' =>'index', 'uses' => 'CursoExtraGenericosController@getIndex']);

            });

            // Route::group(
            // [
            //   'prefix'  => 'curso-incluyes',
            //   'as'      => 'curso-incluyes.',
            // ],
            // function() {

            //   Route::post('/add/{curso_id}', ['as' =>'add', 'uses' => 'IncluyesController@add']);
            //   Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'IncluyesController@add@destroy']);

            //   Route::get('/{curso_id?}', ['as' =>'index', 'uses' => 'IncluyesController@getIndex']);

            // });

            Route::group(
            [
              'prefix'  => 'incluyes',
              'as'      => 'incluyes.',
            ],
            function() {

              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'IncluyesController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'IncluyesController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'IncluyesController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'IncluyesController@destroy']);

              Route::get('/', ['as' =>'index', 'uses' => 'IncluyesController@getIndex']);

            });

            Route::get('/{centro_id?}', ['as' =>'index', 'uses' => 'CursosController@getIndex']);

        });

      Route::group(
        [
          'namespace'   => 'Centros',
          'prefix'  => 'centros',
          'as'      => 'centros.',
        ],
        function() {

          Route::get('/nuevo/{proveedor_id?}', ['as' =>'nuevo', 'uses' => 'CentrosController@getNuevo']);
          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'CentrosController@getUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'CentrosController@postUpdate']);
          Route::get('borrar/{id}', ['as' =>'delete', 'uses' => 'CentrosController@destroy']);

          Route::post('upload/delete/{id}',array('as'=>'upload.delete','uses'=>'CentrosController@postFotoUploadDelete'));
          Route::post('upload/{id}',array('as'=>'upload','uses'=>'CentrosController@postFotoUpload'));
          Route::get('foto/borrar/{id}/{file}', ['as' =>'foto.delete', 'uses' => 'CentrosController@deleteFoto']);
          Route::get('foto/portada/{id}/{file}', ['as' =>'foto.portada', 'uses' => 'CentrosController@setFotoPortada']);

          Route::group(
            [
              'prefix'  => 'descuentos',
              'as'      => 'descuentos.',
            ],
            function() {

              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'CentroDescuentosController@getNuevo']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'CentroDescuentosController@destroy']);
              Route::get('/{centro_id}', ['as' =>'index', 'uses' => 'CentroDescuentosController@getIndex']);

              Route::post('/add/{centro_id}', ['as' =>'add', 'uses' => 'CentroDescuentosController@add']);

            });

          Route::group(
            [
              'prefix'  => 'extras',
              'as'      => 'extras.',
            ],
            function() {

              Route::get('/nuevo/{centro_id}', ['as' =>'nuevo', 'uses' => 'CentroExtrasController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'CentroExtrasController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'CentroExtrasController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'CentroExtrasController@destroy']);

              // Route::post('/add/{centro_id}', ['as' =>'add', 'uses' => 'CentroExtrasController@add']);

              Route::get('/{centro_id?}', ['as' =>'index', 'uses' => 'CentroExtrasController@getIndex']);


            });

          Route::group(
            [
              'prefix'  => 'familias',
              'as'      => 'familias.',
            ],
            function() {

              Route::get('/nuevo/{centro_id}', ['as' =>'nuevo', 'uses' => 'FamiliasController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'FamiliasController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'FamiliasController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'FamiliasController@destroy']);

              Route::get('/ficha/{id}/historial', ['as' =>'historial', 'uses' => 'FamiliasController@getHistorial']);
              Route::get('/ficha/{id}/pdf', ['as' =>'pdf', 'uses' => 'FamiliasController@getPdf']);

              Route::post('/upload/delete/{id}',array('as'=>'upload.delete','uses'=>'FamiliasController@postFotoUploadDelete'));
              Route::post('/upload/{id}',array('as'=>'upload','uses'=>'FamiliasController@postFotoUpload'));
              Route::get('/foto/borrar/{id}/{file}', ['as' =>'foto.delete', 'uses' => 'FamiliasController@deleteFoto']);
              Route::get('/foto/portada/{id}/{file}', ['as' =>'foto.portada', 'uses' => 'FamiliasController@setFotoPortada']);

              Route::get('/{centro_id?}', ['as' =>'index', 'uses' => 'FamiliasController@getIndex']);

            });

          Route::group(
            [
              'prefix'  => 'schools',
              'as'      => 'schools.',
            ],
            function() {

              Route::get('/nuevo/{centro_id}', ['as' =>'nuevo', 'uses' => 'SchoolsController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'SchoolsController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'SchoolsController@postUpdate']);
              Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'SchoolsController@destroy']);

              Route::get('/ficha/{id}/historial', ['as' =>'historial', 'uses' => 'SchoolsController@getHistorial']);
              Route::get('/ficha/{id}/pdf', ['as' =>'pdf', 'uses' => 'SchoolsController@getPdf']);

              Route::post('/upload/delete/{id}',array('as'=>'upload.delete','uses'=>'SchoolsController@postFotoUploadDelete'));
              Route::post('/upload/{id}',array('as'=>'upload','uses'=>'SchoolsController@postFotoUpload'));
              Route::get('/foto/borrar/{id}/{file}', ['as' =>'foto.delete', 'uses' => 'SchoolsController@deleteFoto']);
              Route::get('/foto/portada/{id}/{file}', ['as' =>'foto.portada', 'uses' => 'SchoolsController@setFotoPortada']);

              Route::get('/{centro_id?}', ['as' =>'index', 'uses' => 'SchoolsController@getIndex']);

            });

          Route::get('/{proveedor_id?}', ['as' =>'index', 'uses' => 'CentrosController@getIndex']);

        });

      Route::group(
        [
          'namespace'   => 'Convocatorias',
          'prefix'  => 'convocatorias',
          'as'      => 'convocatorias.',
        ],
        function() {

          Route::group(
            [
              'prefix'  => 'cerradas',
              'as'      => 'cerradas.',
            ],
            function() {

              Route::get('/nuevo/{curso_id?}', ['as' =>'nuevo', 'uses' => 'ConvocatoriaCerradasController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ConvocatoriaCerradasController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ConvocatoriaCerradasController@postUpdate']);
              Route::get('borrar/{id}', ['as' =>'delete', 'uses' => 'ConvocatoriaCerradasController@destroy']);

              Route::get('/info', ['as' =>'index.info', 'uses' => 'ConvocatoriaCerradasController@getIndexInfo']);
              Route::get('{id}/info', ['as' =>'ajax.info', 'uses' => 'ConvocatoriaCerradasController@ajaxInfo']);
              Route::get('/{curso_id?}', ['as' =>'index', 'uses' => 'ConvocatoriaCerradasController@getIndex']);

              Route::post('{id}/precio', ['as' =>'precio', 'uses' => 'ConvocatoriaCerradasController@postPrecio']);
              Route::post('{id}/cambio', ['as' =>'cambio', 'uses' => 'ConvocatoriaCerradasController@postCambio']);

              Route::get('{id}/divisa/{reset?}', ['as' =>'divisa', 'uses' => 'ConvocatoriaCerradasController@setDivisa']);

            });

          Route::group(
            [
              'prefix'  => 'vuelos',
              'as'      => 'vuelos.',
            ],
            function() {

              Route::get('/nuevo/{convo_id?}', ['as' =>'nuevo', 'uses' => 'VuelosController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'VuelosController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'VuelosController@postUpdate']);
              Route::get('borrar/{id}', ['as' =>'delete', 'uses' => 'VuelosController@destroy']);

              Route::get('bookings/{id}', ['as' =>'bookings', 'uses' => 'VuelosController@getBookings']);

              Route::get('/{vuelo_id}/convocatorias', ['as' =>'index.convocatorias', 'uses' => 'VuelosController@getConvocatorias']);
              Route::post('/asignar', ['as' =>'asignar', 'uses' => 'VuelosController@postAsignar']);
              Route::get('/borrar-asignar/{id}/{tab?}', ['as' =>'delete.asignar', 'uses' => 'VuelosController@destroyAsignacion']);

              Route::group(
                [
                  'prefix'  => 'etapas',
                  'as'      => 'etapas.',
                ],
                function() {

                  Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'VuelosController@postUpdateEtapa']);
                  Route::get('/borrar/{id}', ['as' =>'delete', 'uses' => 'VuelosController@destroyEtapa']);

                });

              Route::get('/{convocatoria_id?}', ['as' =>'index', 'uses' => 'VuelosController@getIndex']);

            });

          Route::group(
            [
              'prefix'  => 'plazas',
              'as'      => 'plazas.',
            ],
            function() {

              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ConvocatoriaCerradasController@postPlazas']);

            });

          Route::group(
            [
              'prefix'  => 'abiertas',
              'as'      => 'abiertas.',
            ],
            function() {

              Route::get('/nuevo/{curso_id?}', ['as' =>'nuevo', 'uses' => 'ConvocatoriaAbiertasController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ConvocatoriaAbiertasController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ConvocatoriaAbiertasController@postUpdate']);
              Route::get('borrar/{id}', ['as' =>'delete', 'uses' => 'ConvocatoriaAbiertasController@destroy']);
              Route::get('/{curso_id?}', ['as' =>'index', 'uses' => 'ConvocatoriaAbiertasController@getIndex']);

            });

          /*Route::group(
            [
              'prefix'  => 'costes',
              'as'      => 'costes.',
            ],
            function() {

              Route::get('/nuevo/{convocatoria_id?}', ['as' =>'nuevo', 'uses' => 'ConvocatoriaCostesController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ConvocatoriaCostesController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ConvocatoriaCostesController@postUpdate']);
              Route::get('borrar/{id}', ['as' =>'delete', 'uses' => 'ConvocatoriaCostesController@destroy']);
              Route::get('/{convocatoria_id?}', ['as' =>'index', 'uses' => 'ConvocatoriaCostesController@getIndex']);

            });*/

          Route::group(
            [
              'prefix'  => 'precios',
              'as'      => 'precios.',
            ],
            function() {

              Route::get('/ficha/{id}/{extras?}', ['as' =>'ficha', 'uses' => 'ConvocatoriaPreciosController@getUpdate']);
              Route::post('/ficha/{id}/{extras?}', ['as' =>'ficha', 'uses' => 'ConvocatoriaPreciosController@postUpdate']);
              Route::get('borrar/{id}/{extras?}', ['as' =>'delete', 'uses' => 'ConvocatoriaPreciosController@destroy']);
              Route::get('/{convocatoria_id?}/{extras?}', ['as' =>'index', 'uses' => 'ConvocatoriaPreciosController@getIndex']);

            });

          Route::group(
            [
              'prefix'  => 'ofertas',
              'as'      => 'ofertas.',
            ],
            function() {

              Route::get('/nuevo/{convocatoria_id?}', ['as' =>'nuevo', 'uses' => 'ConvocatoriaOfertasController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ConvocatoriaOfertasController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ConvocatoriaOfertasController@postUpdate']);
              Route::get('borrar/{id}', ['as' =>'delete', 'uses' => 'ConvocatoriaOfertasController@destroy']);
              Route::get('/{convocatoria_id?}', ['as' =>'index', 'uses' => 'ConvocatoriaOfertasController@getIndex']);

            });

          //Convocatoria Multi
          Route::group(
            [
              'prefix'  => 'multis',
              'as'      => 'multis.',
            ],
            function() {

              Route::get('/nuevo/{id?}', ['as' =>'nuevo', 'uses' => 'ConvocatoriaMultiController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ConvocatoriaMultiController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'ConvocatoriaMultiController@postUpdate']);
              Route::get('borrar/{id}', ['as' =>'delete', 'uses' => 'ConvocatoriaMultiController@destroy']);
              Route::get('/{curso_id?}', ['as' =>'index', 'uses' => 'ConvocatoriaMultiController@getIndex']);

              //semanas
              Route::get('/semanas/borrar/{id}', ['as' =>'semanas.delete', 'uses' => 'ConvocatoriaMultiController@destroySemana']);
              Route::post('/semanas/{convocatory_id}', ['as' =>'semanas.post', 'uses' => 'ConvocatoriaMultiController@postSemana']);

              //especialidades
              Route::get('/especialidades/borrar/{id}', ['as' =>'especialidades.delete', 'uses' => 'ConvocatoriaMultiController@destroyEspecialidad']);
              Route::post('/especialidades/{convocatory_id}', ['as' =>'especialidades.post', 'uses' => 'ConvocatoriaMultiController@postEspecialidad']);

            });

        });

      Route::group(
        [
          'namespace'   => 'Alojamientos',
          'prefix'  => 'alojamientos',
          'as'      => 'alojamientos.',
        ],
        function() {

          Route::get('/nuevo/{centro_id?}', ['as' =>'nuevo', 'uses' => 'AlojamientosController@getNuevo']);
          Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'AlojamientosController@getUpdate']);
          Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'AlojamientosController@postUpdate']);
          Route::get('borrar/{id}', ['as' =>'delete', 'uses' => 'AlojamientosController@destroy']);

          Route::get('/json', ['as' =>'json', 'uses' => 'AlojamientosController@getListJson']);

          Route::post('upload/delete/{id}',array('as'=>'upload.delete','uses'=>'AlojamientosController@postFotoUploadDelete'));
          Route::post('upload/{id}',array('as'=>'upload','uses'=>'AlojamientosController@postFotoUpload'));
          Route::get('foto/borrar/{id}/{file}', ['as' =>'foto.delete', 'uses' => 'AlojamientosController@deleteFoto']);
          Route::get('foto/portada/{id}/{file}', ['as' =>'foto.portada', 'uses' => 'AlojamientosController@setFotoPortada']);

          Route::group(
            [
              'prefix'  => 'tipos',
              'as'      => 'tipos.',
            ],
            function() {

              Route::get('/', ['as' =>'index', 'uses' => 'AlojamientoTiposController@getIndex']);
              Route::get('/nuevo', ['as' =>'nuevo', 'uses' => 'AlojamientoTiposController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'AlojamientoTiposController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'AlojamientoTiposController@postUpdate']);
              Route::get('borrar/{id}', ['as' =>'delete', 'uses' => 'AlojamientoTiposController@destroy']);

            });

          Route::group(
            [
              'prefix'  => 'precios',
              'as'      => 'precios.',
            ],
            function() {

              Route::get('/ficha/{id}/{extras?}', ['as' =>'ficha', 'uses' => 'AlojamientoPreciosController@getUpdate']);
              Route::post('/ficha/{id}/{extras?}', ['as' =>'ficha', 'uses' => 'AlojamientoPreciosController@postUpdate']);
              Route::get('borrar/{id}/{extras?}', ['as' =>'delete', 'uses' => 'AlojamientoPreciosController@destroy']);

              Route::get('/{alojamiento_id?}/{extras?}', ['as' =>'index', 'uses' => 'AlojamientoPreciosController@getIndex']);

            });

          Route::group(
            [
              'prefix'  => 'cuotas',
              'as'      => 'cuotas.',
            ],
            function() {

              Route::get('/nuevo/{alojamiento_id?}', ['as' =>'nuevo', 'uses' => 'AlojamientoCuotasController@getNuevo']);
              Route::get('/ficha/{id}', ['as' =>'ficha', 'uses' => 'AlojamientoCuotasController@getUpdate']);
              Route::post('/ficha/{id}', ['as' =>'ficha', 'uses' => 'AlojamientoCuotasController@postUpdate']);
              Route::get('borrar/{id}', ['as' =>'delete', 'uses' => 'AlojamientoCuotasController@destroy']);

              Route::get('/{alojamiento_id?}', ['as' =>'index', 'uses' => 'AlojamientoCuotasController@getIndex']);

            });

          Route::get('/proveedor/{proveedor_id?}', ['as' =>'index.proveedor', 'uses' => 'AlojamientosController@getIndexByProveedor']);
          Route::get('/curso/{curso_id?}', ['as' =>'index.curso', 'uses' => 'AlojamientosController@getIndexByCurso']);
          Route::get('/{centro_id?}', ['as' =>'index', 'uses' => 'AlojamientosController@getIndex']);

        });

        //Exams
        Route::group(
            [
                'namespace' => 'Exams',
                // 'prefix'    => 'exams',
                // 'as'        => 'exams.',
            ],
            function() {

                Route::resource('exams', 'ExamenController')->except([
                    'show','destroy'
                ]);//->parameters(['exams' => 'examen']);
                Route::get('/exams/{exam}/destroy', "ExamenController@destroy")->name('exams.destroy');
                Route::get('/exams/{exam}/preview', "ExamenController@getPreview")->name('exams.preview');
                Route::get('/exams/{id}/respuestas', "ExamenController@getRespuestas")->name('exams.asks');
                
                Route::get('/exams/respuesta/{respuesta}', "ExamenController@getRespuesta")->name('exams.ask');
                Route::post('/exams/{exam}/respuesta', "ExamenController@postRespuesta")->name('exams.ask.post');
                Route::get('/exams/respuesta/{respuesta}/destroy', "ExamenController@destroyRespuesta")->name('exams.ask.destroy');

                Route::post('/exams/vinculado', ['as' =>'exams.vinculados', 'uses' => 'ExamenController@postVinculado']);
                Route::get('/exams/vinculado/{id}/delete', ['as' =>'exams.vinculados.delete', 'uses' => 'ExamenController@deleteVinculado']);
                Route::get('/exams/reclamar/{exam}/{modelo?}/{modelo_id?}', ['as' =>'exams.reclamar', 'uses' => 'ExamenController@getReclamar']);
            
            });
    }
  );