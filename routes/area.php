<?php

/*
|--------------------------------------------------------------------------
| Area Cliente
|--------------------------------------------------------------------------
|
|
*/

// === Compra ===
Route::post('/online-booking/email', ['as' =>'comprar.email', 'uses' => 'Area\ComprasController@postEmail']);
Route::post('/online-booking/user', ['as' =>'comprar.user', 'uses' => 'Area\ComprasController@postUser']);

Route::get('/area/online-booking/pagar/{booking_id}', ['as' =>'comprar.pago', 'uses' => 'Area\ComprasController@pagar']);
Route::post('/area/online-booking/pagar/{booking_id}', ['as' =>'comprar.pago.notificacion', 'uses' => 'Area\ComprasController@pagoNotificacion']);
Route::get('/area/online-booking/{booking_id}/ok', ['as' =>'comprar.pago.ok', 'uses' => 'Area\ComprasController@pagoOK']);
Route::get('/area/online-booking/{booking_id}/ko', ['as' =>'comprar.pago.ko', 'uses' => 'Area\ComprasController@pagoKO']);

Route::post('/area/online-booking/{booking_id}/doc/',array('as'=>'comprar.pago.adjunto','uses'=>'Area\ComprasController@pagoUpload'));
// Route::post('/area/online-booking/doc/{doc_id}/borrar',array('as'=>'comprar.pago.adjunto.borrar','uses'=>'Area\ComprasController@pagoUploadDelete'));

Route::get('/booking-corporativo/{curso_id}/{convo_id}', ['as' =>'comprar.corporativo', 'uses' => 'Area\ComprasController@getCorporativo']);
Route::get('/booking-multi/{convo_id}', ['as' =>'comprar.multi', 'uses' => 'Area\ComprasController@getBookingMulti']);
Route::post('/area/online-booking/{booking_id}/codigo/',array('as'=>'comprar.codigo','uses'=>'Area\ComprasController@ajaxCodigo'));

// === Compra ===


Route::group(
  [
    'namespace'   => 'Area',
    'prefix'  => 'area',
    'as'      => 'area.',
    'middleware'  => [ 'auth.area' ]
  ],
  function() {

    Route::get('/', ['as' =>'index', 'uses' => 'AreaController@getIndex']);
    Route::get('/cursos', ['as' =>'cursos', 'uses' => 'AreaController@getCursos']);
    Route::get('/wishlist/{curso_id?}', ['as' =>'deseos', 'uses' => 'AreaController@getDeseos']);
    Route::get('/wishlist/{id}/borrar', ['as' =>'deseos.borrar', 'uses' => 'AreaController@getDeseosBorrar']);
    
    Route::get('/exams', ['as' =>'exams', 'uses' => 'AreaController@getExams']);
    Route::get('/exams/{exam}/{id?}/{tipo?}', "AreaController@getExam")->name('exams.ask');
    Route::post('/exams/{exam}', "AreaController@postExam")->name('exams.ask.post');

    Route::get('/booking/{id}', ['as' =>'booking', 'uses' => 'AreaController@getBooking']);
    Route::get('/booking/familia/{asig_id}', ['as' => 'booking.familia', 'uses' => 'AreaController@getFamilia']);
    Route::get('/booking/familia/getpdf/{asig_id}', ['as' => 'booking.familia-pdf', 'uses' => 'AreaController@getFamiliaPdf']);
    Route::get('/booking/school/{asig_id}', ['as' => 'booking.school', 'uses' => 'AreaController@getSchool']);
    Route::get('/booking/school/getpdf/{asig_id}', ['as' => 'booking.school-pdf', 'uses' => 'AreaController@getSchoolPdf']);
    Route::get('/booking/pocketguide/{id}', ['as' => 'booking.pocketguide', 'uses' => 'AreaController@getPocketguide']);
    Route::get('/pago-pdf/{pago_id}', ['as' =>'pagos.pdf', 'uses' => 'AreaController@getPagoPdf']);
    Route::get('/booking/{id}/firmar', ['as' =>'booking.firmar', 'uses' => 'AreaController@getFirmar']);
    Route::post('/booking/{id}/firmar', ['as' =>'booking.firmar.post', 'uses' => 'AreaController@postFirmar']);

    Route::post('/booking/{id}/adjuntar', ['as' =>'booking.adjuntar.post', 'uses' => 'AreaController@postAdjuntar']);

    Route::get('/pendientes/{user_id?}', ['as' =>'pendientes', 'uses' => 'AreaController@getPendientes']);
    Route::get('/login-swap', ['as' =>'login.swap', 'uses' => 'AreaController@swapLogin']);

    Route::get('/forms/{form_id}/{booking_id}', ['as' => 'forms', 'uses' => 'AreaController@getForm']);
    Route::post('/forms/{form_id}/{booking_id}', ['as' => 'forms.post', 'uses' => 'AreaController@postForm']);
    Route::get('/forms/{form_id}/{booking_id}/{test_id}', ['as' => 'forms.resultado', 'uses' => 'AreaController@getFormResultados']);

    //monitor
    Route::get('/monitor/bookings', ['as' =>'monitor.bookings', 'uses' => 'AreaController@getMonitorBookings']);
    Route::get('/booking/{id}/pdfmonitor', ['as' => 'booking.pdfmonitor', 'uses' => 'AreaController@getPdfMonitor']);
    //Form :: Respuestas


    Route::group(
        [
          // 'namespace'   => 'Datos',
          'prefix'  => 'datos',
          'as'      => 'datos.',
        ],
        function()
        {
            Route::get('/datos', ['as' =>'index', 'uses' => 'AreaController@getDatos']);
            Route::get('/datos/hijos', ['as' =>'hijos', 'uses' => 'AreaController@getHijos']);
            Route::get('/datos/tutores', ['as' =>'tutores', 'uses' => 'AreaController@getTutores']);
            Route::get('/password', ['as' =>'password', 'uses' => 'AreaController@getPasswordCambiar']);
            Route::post('/password', ['as' =>'password.post', 'uses' => 'AreaController@postPasswordCambiar']);

            Route::post('/datos', ['as' =>'post', 'uses' => 'AreaController@postDatos']);
            Route::post('/datos/hijos/{viajero_id}', ['as' =>'hijos.post', 'uses' => 'AreaController@postHijos']);
        }
    );

    Route::group(
        [
          // 'namespace'   => 'Area',
          'prefix'  => 'online-booking',
          'as'      => 'comprar.',
        ],
        function()
        {
            //Proceso booking
            Route::get('/inscripcion/{booking_id}/viajero/{id}', ['as' =>'booking.viajero', 'uses' => 'ComprasController@setViajero']);
            Route::get('/inscripcion/{booking_id?}', ['as' =>'booking', 'uses' => 'ComprasController@getUpdate']);

            Route::post('/inscripcion', ['as' =>'booking.post', 'uses' => 'ComprasController@postUpdate']);
            Route::get('/cancelar/{booking_id?}', ['as' =>'cancelar', 'uses' => 'ComprasController@getCancelar']);
            Route::get('/curso/{curso_id?}/{booking_id?}/{reset?}', ['as' =>'curso', 'uses' => 'ComprasController@getCurso']);

            Route::get('/{curso_id?}/{convo_id?}', ['as' =>'compra', 'uses' => 'ComprasController@getCompra']);

        }
    );

  });